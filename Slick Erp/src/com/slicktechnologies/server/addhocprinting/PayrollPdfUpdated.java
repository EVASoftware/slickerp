package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.appengine.api.datastore.ReadPolicy.Consistency;
import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.AvailedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.EmployeeOvertimeHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipPrintHelper;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.smartgwt.client.widgets.form.validator.IsBooleanValidator;

public class PayrollPdfUpdated {
	public Document document;

	PaySlipPrintHelper payslip;
	Company comp;
	PaySlip ps;
	Employee emp;
	Branch branch;
	EmployeeInfo info;
	EmployeeAdditionalDetails empAddDetObj;
	int noOfLinesallowence = 8;
	int noOfLinesdeduction=8;
	ProcessConfiguration processConfig;
	List<ArticleType> articletype= new ArrayList<ArticleType>();
	boolean rateColumnFlag=false;
	EmploymentCardPdf cell=new EmploymentCardPdf();
	
	private Font font16boldul, font12bold, font8bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold,font7,font7bold;
	final static String footertext = "This is system generated pdf. It does not require signature";
	float[] columnWidths2={2.5f,0.5f,4.5f,2.5f,0.5f,2.5f};
	float[] columnWidths3={2.5f,0.5f,4.5f};
	float[] columnWidths4={2.6f,0.5f,2.5f};
	float[] columnWidths5={3.0f,1.5f,1.5f,3.0f,1.5f,1.5f};
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	int firstBreakPoint=20;
	float blankLines=0;
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	double totalnetpayble=0;
	/** date 15.11.2018 added by komal to show leave details on payslip **/
	LeaveBalance leaveBalance;
	boolean leaveBalanceFlag = false;
	
	/**
	 * Date : 30-11-2018 By ANIL
	 * is
	 */
	boolean isHideCompanyAddress;
	boolean isEmployeeNameOnLeftSide;
	boolean isHideProjectName;
	boolean isHideFatherOrSpouseName;
	boolean isHideEmailId;
	boolean isHideHappayNo;
	boolean isHidePaidLeave;
	boolean isHideWeeklyOff;
	boolean isHideOtherHoliday;
	boolean isHidePaidDays;
	boolean isHideExtraDays;
	boolean isChangeDaysWorkedLabel;
	boolean isShowBonusInEarningList;
	boolean isAlignLeftNoteText;
	boolean isOnlyForOrion;
	String hrEmailId;
	boolean isShowEsic;				//Updated BY: Viraj Date: 04-06-2019 Description: To show Esic No
	/**
	 * @author Anil , Date : 23-02-2019
	 */
	int noOfLineAddAllowance=8;
	
	/**@author Amol
	 * Date 10-6-2019
	 */
	boolean isHideDepartmentName=false;
	boolean isShowCompanyPfNumberandEsicNumber=false;
	
	
	
	
	
	
	public void setPaySlip(long count,PaySlipPrintHelper payslip) {
		/**
		 * @author Anil , Date :28-02-2019
		 * if we print multiple salary slip at once from statutory report form
		 * first salary slip printed was default others are as per below flag 
		 */
		isOnlyForOrion=false;
		noOfLinesallowence = 8;
		noOfLinesdeduction=8;
		noOfLineAddAllowance=8;
		
		ObjectifyService.reset();
	    ofy().consistency(Consistency.STRONG);
		ofy().clear();
		
		// Load PaySlip
		ps = ofy().load().type(PaySlip.class).id(count).now();
		logger.log(Level.SEVERE," inside PDF payslip :::  "+ps.getCompanyId());
		// Load Company
		if (ps.getCompanyId() == null){
			comp = ofy().load().type(Company.class).first().now();
		}
		else{
			comp = ofy().load().type(Company.class).filter("companyId", ps.getCompanyId()).first().now();
		}
	
		// Load Company
		if (ps.getCompanyId() == null){
			emp = ofy().load().type(Employee.class).filter("count", ps.getEmpid()).first().now();
		}
		else{
			emp = ofy().load().type(Employee.class).filter("companyId", ps.getCompanyId()).filter("count", ps.getEmpid()).first().now();
		}
		if (emp != null) {
			articletype = emp.getArticleTypeDetails();
		}
		
		if(emp!=null){
			info=ofy().load().type(EmployeeInfo.class).filter("companyId", ps.getCompanyId()).filter("empCount", ps.getEmpid()).first().now();
		}
			branch=ofy().load().type(Branch.class).filter("companyId", ps.getCompanyId()).filter("buisnessUnitName", ps.getBranch()).first().now();
		
		
		this.payslip=payslip;
		
		
		if (ps.getCompanyId() != null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", ps.getCompanyId()).filter("processName", "PaySlip").filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if(processConfig!=null){
				System.out.println("PROCESS CONFIG NOT NULL");
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideRateColumn")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						System.out.println("PROCESS CONFIG FLAG FALSE");
						rateColumnFlag=true;
					}
					/** date 15.11.2018 added by komal to show leave details on payslip **/
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowLeaveBalance")&&processConfig.getProcessList().get(k).isStatus()==true){
						leaveBalanceFlag = true;
					}
					/**
					 * Date : 30-11-2018 By ANIL
					 */
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideCompanyAddress")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideCompanyAddress = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("EmployeeNameOnLeftSide")&&processConfig.getProcessList().get(k).isStatus()==true){
						isEmployeeNameOnLeftSide = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideProjectName")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideProjectName = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideFatherOrSpouseName")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideFatherOrSpouseName = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideEmailId")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideEmailId = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideHappayNo")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideHappayNo = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HidePaidLeave")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHidePaidLeave = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideWeeklyOff")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideWeeklyOff = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideOtherHoliday")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideOtherHoliday = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HidePaidDays")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHidePaidDays = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideExtraDays")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideExtraDays = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ChangeDaysWorkedLabel")&&processConfig.getProcessList().get(k).isStatus()==true){
						isChangeDaysWorkedLabel = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowBonusInEarningList")&&processConfig.getProcessList().get(k).isStatus()==true){
						isShowBonusInEarningList = true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("AlignLeftNoteText")&&processConfig.getProcessList().get(k).isStatus()==true){
						isAlignLeftNoteText = true;
					}
					
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("OnlyForOrion")&&processConfig.getProcessList().get(k).isStatus()==true){
						isOnlyForOrion = true;
					}
					/**
					 * Updated By: Viraj
					 * Date: 04-06-2019
					 * Description: To show Esic No
					 */
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowEsicNo")&&processConfig.getProcessList().get(k).isStatus()==true){
						isShowEsic = true;
					}
					/** Ends **/

					/**@author Amol
					 * Date 10-6-2019
					 * to hide the father Spouse Name
					 */
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideDepartmentName")&&processConfig.getProcessList().get(k).isStatus()==true){
						isHideDepartmentName= true;
					}
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowPfNumberAndEsicNumberAsCompany")&&processConfig.getProcessList().get(k).isStatus()==true){
						isShowCompanyPfNumberandEsicNumber= true;
					}
					

					
					
					/**
					 * End
					 */
				}
			}
		}
		
		/**
		 * Date : 30-11-2018 By ANIL
		 */
		if(comp!=null&&comp.getArticleTypeDetails()!=null&&comp.getArticleTypeDetails().size()!=0){
			for(ArticleType type:comp.getArticleTypeDetails()){
				if(type.getDocumentName().trim().equalsIgnoreCase("Payslip")
						&&type.getArticleTypeName().trim().equalsIgnoreCase("Email")
						&&type.getArticlePrint().trim().equalsIgnoreCase("Yes")){
					hrEmailId=type.getArticleTypeValue();
				}
			}
		}
		
		/** date 15.11.2018 added by komal to show leave details on payslip **/
		if(leaveBalanceFlag){
			leaveBalance=ofy().load().type(LeaveBalance.class).filter("companyId", ps.getCompanyId())
							.filter("empInfo.empCount", ps.getEmpid()).first().now();
		}
		// Added By Priyanka - 11-03-2021
		if(ps!=null)
		empAddDetObj = ofy().load().type(EmployeeAdditionalDetails.class).filter("companyId", ps.getCompanyId()).filter("empInfo.empCount", ps.getEmpid()).first().now();
		
	
	}
	
	public PayrollPdfUpdated() {
		
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		
		/**
		 * @author Anil , Date :23-02-2019
		 */
		isOnlyForOrion=false;
		noOfLinesallowence = 8;
		noOfLinesdeduction=8;
		noOfLineAddAllowance=8;
	}

	public void createPdf() {
		createHeader();
		
		if(isOnlyForOrion==true){
			createOrionEmployeeDetails();
		}else{
			createemployeeDetails();
		}
		createallowenceTable();
		createfootertab();
	}

	

	private void createfootertab() {
		
		
		PdfPTable footertab=new PdfPTable(1);
		footertab.setWidthPercentage(100);
		
		String amtInWordsVal="";
		amtInWordsVal = SalesInvoicePdf.convert(totalnetpayble);
		
		Phrase netpaybleinwords=new Phrase(" Rs.  "+amtInWordsVal +" Only",font9bold);
		PdfPCell netpayleinwordscell=new PdfPCell(netpaybleinwords);
		netpayleinwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		netpayleinwordscell.setBorder(0);
		footertab.addCell(netpayleinwordscell);
		
		Phrase footerblank=new Phrase(" ",font8bold);
		PdfPCell footerblankcell=new PdfPCell(footerblank);
		footerblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerblankcell.setBorder(0);
		footertab.addCell(footerblankcell);
		
//		footertab.addCell(footerblankcell);
//		footertab.addCell(footerblankcell);
		if(isAlignLeftNoteText&&hrEmailId!=null){
			Phrase footeremail=new Phrase(hrEmailId,font8);
			PdfPCell footeremailcell=new PdfPCell(footeremail);
			footeremailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			footeremailcell.setBorder(0);
			footertab.addCell(footeremailcell);
		}
		
		Phrase declaration=new Phrase("This is a computer generated salary slip and requires no physical signature or stamp.",font8);
		PdfPCell declarationcell=new PdfPCell(declaration);
		if(isAlignLeftNoteText){
			declarationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		}else{
			declarationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		declarationcell.setBorder(0);
		footertab.addCell(declarationcell);
		
		
		PdfPTable footerouter=new PdfPTable(1);
		footerouter.setWidthPercentage(100);
		
		PdfPCell footerCell =new PdfPCell(footertab);
		footerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		footerCell.setPaddingBottom(5);
		footerouter.addCell(footerCell);
		
		
		try {
			document.add(footerouter);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	private void createallowenceTable() {
		
		/**
		 * Date : 30-11-2018 BY ANIL
		 * Adding Bonus Component in earning list
		 */
		if(isShowBonusInEarningList&&ps.getBonus()!=0){
			CtcComponent comp=new CtcComponent();
			comp.setName("Bonus");
			comp.setShortName("Bonus");
			comp.setActualAmount(Math.round(ps.getBonus()));
			double perDayBonusAmt=ps.getBonus()/ps.getPaidDays();
			double monthlyRate=(perDayBonusAmt*ps.getMonthlyDays())*12;
			comp.setAmount(monthlyRate);
			ps.getEarningList().add(comp);
		}
		/**
		 * End
		 */
		
		/**
		 * @author Anil ,Date : 15-04-2019
		 * Including OT in Gross earninng
		 */
//		if(ps.isOtIncludedInGross()&&ps.getOvertimeSalary()!=0){
//			/**
//			 * @author Anil , Date : 12-06-2019
//			 * showing ot bifurcation
//			 */
//			if(ps.getEmpOtHistoryList()!=null&&ps.getEmpOtHistoryList().size()!=0){
//				ArrayList<CtcComponent> otList=new ArrayList<CtcComponent>();
//				for(EmployeeOvertimeHistory obj:ps.getEmpOtHistoryList()){
//					if(otList.size()!=0){
//						boolean otValFlag=false;
//						for(CtcComponent ctcComp:otList){
//							if(obj.isPayOtAsOther()==true){
//								if(ctcComp.getName().equals(obj.getCorrespondanceName())){
//									otValFlag=true;
//									ctcComp.setActualAmount(Math.round(ctcComp.getActualAmount()+obj.getOtAmount()));
//								}
//							}else{
//								if(ctcComp.getName().equals("Overtime")){
//									otValFlag=true;
//									ctcComp.setActualAmount(Math.round(ctcComp.getActualAmount()+obj.getOtAmount()));
//								}
//							}
//						}
//						if(otValFlag==false){
//							CtcComponent comp=new CtcComponent();
//							comp.setShortName("OT");
//							if(obj.isPayOtAsOther()==true){
//								comp.setName(obj.getCorrespondanceName());
//							}else{
//								comp.setName("Overtime");
//							}
//							comp.setActualAmount(Math.round(obj.getOtAmount()));
//							otList.add(comp);
//						}
//					}else{
//						CtcComponent comp=new CtcComponent();
//						comp.setShortName("OT");
//						if(obj.isPayOtAsOther()==true){
//							comp.setName(obj.getCorrespondanceName());
//						}else{
//							comp.setName("Overtime");
//						}
//						comp.setActualAmount(Math.round(obj.getOtAmount()));
//						otList.add(comp);
//					}
//				}
//				if(otList.size()!=0){
//					ps.getEarningList().addAll(otList);
//				}
//				
//			}else{
//				CtcComponent comp=new CtcComponent();
//				comp.setName("Overtime");
//				comp.setShortName("OT");
//				comp.setActualAmount(Math.round(ps.getOvertimeSalary()));
//	//			double perDayBonusAmt=ps.getBonus()/ps.getPaidDays();
//	//			double monthlyRate=(perDayBonusAmt*ps.getMonthlyDays())*12;
//	//			comp.setAmount(monthlyRate);
//				ps.getEarningList().add(comp);
//			}
//		}
		/**
		 * 
		 */
		
		
		/**
		 * date : 28-08-2018 By ANIL
		 * removing arrears element of revised components 
		 * and Other arrears which are independent
		 */
		System.out.println("EARN LIST SIZE :: "+ps.getEarningList().size());
		ArrayList<CtcComponent> revisedArrearsList=new ArrayList<CtcComponent>();
		ArrayList<CtcComponent> otherArrarsList=new ArrayList<CtcComponent>();
		
		for(int i=0;i<ps.getEarningList().size();i++){
			CtcComponent comp=ps.getEarningList().get(i);
			if(comp.isArrears()==true&&comp.getArrearOf()!=null&&!comp.getArrearOf().equals("")){
				revisedArrearsList.add(comp);
				ps.getEarningList().remove(comp);
				i--;
			}else if(comp.isArrears()==true&&(comp.getArrearOf()==null||comp.getArrearOf().equals(""))){
				otherArrarsList.add(comp);
				ps.getEarningList().remove(comp);
				i--;
			}
		}
		System.out.println("REVISED ARREARS LIST :: "+revisedArrearsList.size());
		System.out.println("OTHER ARREARS LIST :: "+otherArrarsList.size());
		System.out.println("EARN LIST SIZE :: "+ps.getEarningList().size());
		
		/**
		 * End
		 */
		
		PdfPTable allowencetitle=new PdfPTable(4);
//		PdfPTable allowencetitle=new PdfPTable(3); 
		allowencetitle.setWidthPercentage(100);
		try {
//			allowencetitle.setWidths(new float[]{40,30,30});
//			allowencetitle.setWidths(new float[]{31,23,23,23});
			allowencetitle.setWidths(new float[]{37,21,21,21});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Phrase allowances=new Phrase("Allowances",font8bold);
		PdfPCell allowancescell=new PdfPCell(allowances);
		allowancescell.setBorder(0);
		allowancescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		allowencetitle.addCell(allowancescell);
		
		Phrase rate=new Phrase("Rate",font8bold);
		PdfPCell ratecell=new PdfPCell(rate);
		ratecell.setBorder(0);
		ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		allowencetitle.addCell(ratecell);
		
		Phrase amount=new Phrase("Amount",font8bold);
		PdfPCell amountcell=new PdfPCell(amount);
		amountcell.setBorder(0);
		amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		allowencetitle.addCell(amountcell);
		
		/**
		 * Date : 28-08-2018 By ANIL
		 */
		Phrase arrears=new Phrase("Arrears",font8bold);
		PdfPCell arrearsCell=new PdfPCell(arrears);
		arrearsCell.setBorder(0);
		arrearsCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		allowencetitle.addCell(arrearsCell);
		/**
		 * End
		 */
		
		
		PdfPTable allowencedetails=new PdfPTable(4);
//		PdfPTable allowencedetails=new PdfPTable(3); 
		allowencedetails.setWidthPercentage(100);
		try {
//			allowencedetails.setWidths(new float[]{40,30,30});
//			allowencedetails.setWidths(new float[]{31,23,23,23});
			allowencedetails.setWidths(new float[]{37,21,21,21});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i <ps.getEarningList().size(); i++) {
			noOfLinesallowence = noOfLinesallowence - 1;
			Phrase basic=new Phrase(ps.getEarningList().get(i).getName(),font8);
			PdfPCell basiccell=new PdfPCell(basic);
			basiccell.setBorder(0);
			basiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
			allowencedetails.addCell(basiccell);
			
			double calrate=Math.round(ps.getEarningList().get(i).getAmount()/12);
			
			Phrase rateval=new Phrase(df.format(calrate)+"",font8);
			PdfPCell ratevalcell=new PdfPCell(rateval);
			ratevalcell.setBorder(0);
			ratevalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			allowencedetails.addCell(ratevalcell);
			
			Phrase amountval=new Phrase(df.format(ps.getEarningList().get(i).getActualAmount())+"",font8);
	//		Phrase amountval=new Phrase(payslip.getEarningList().get(i).getAmount()+"",font8);
			PdfPCell amountvalcell=new PdfPCell(amountval);
			amountvalcell.setBorder(0);
			amountvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			allowencedetails.addCell(amountvalcell);
			
			
			/**
			 * Date : 28-08-2018 By ANIL
			 */
			Phrase arrearsPh=null;
			CtcComponent arrearsComp=getArrarsComp(ps.getEarningList().get(i),revisedArrearsList);
			if(arrearsComp!=null){
				arrearsPh=new Phrase(df.format(arrearsComp.getActualAmount()),font8);
			}else{
				arrearsPh=new Phrase("0.0",font8);
			}
//			Phrase arrearsPh=new Phrase(df.format(calrate)+"",font8);
			PdfPCell arrearsCellVl=new PdfPCell(arrearsPh);
			arrearsCellVl.setBorder(0);
			arrearsCellVl.setHorizontalAlignment(Element.ALIGN_RIGHT);
			allowencedetails.addCell(arrearsCellVl);
			
			/**
			 * end
			 */
			
		}
		
		/***
		 * 
		 */
		
		
		for (int i = 0; i <otherArrarsList.size(); i++) {
			noOfLinesallowence = noOfLinesallowence - 1;
			Phrase basic=new Phrase(otherArrarsList.get(i).getName(),font8);
			PdfPCell basiccell=new PdfPCell(basic);
			basiccell.setBorder(0);
			basiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
			allowencedetails.addCell(basiccell);
			
			Phrase rateval=new Phrase("",font8);
			PdfPCell ratevalcell=new PdfPCell(rateval);
			ratevalcell.setBorder(0);
			ratevalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			allowencedetails.addCell(ratevalcell);
			
			Phrase arrearsPh=null;
			arrearsPh=new Phrase(" ",font8);
			PdfPCell arrearsCellVl=new PdfPCell(arrearsPh);
			arrearsCellVl.setBorder(0);
			arrearsCellVl.setHorizontalAlignment(Element.ALIGN_RIGHT);
			allowencedetails.addCell(arrearsCellVl);
			
			Phrase amountval=new Phrase(df.format(otherArrarsList.get(i).getActualAmount())+"",font8);
	//		Phrase amountval=new Phrase(payslip.getEarningList().get(i).getAmount()+"",font8);
			PdfPCell amountvalcell=new PdfPCell(amountval);
			amountvalcell.setBorder(0);
			amountvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			allowencedetails.addCell(amountvalcell);
			
		}
		
		
		
		/***
		 * 
		 */
		
		//Allowance Table complet
		int remainingLines=0;
		if (noOfLinesallowence != 0) {
			remainingLines = 10 - (10 - noOfLinesallowence);
		}
		System.out.println("remainingLines" + remainingLines);
		for (int j = 0; j < remainingLines; j++) {
			PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			pCell.setBorder(0);
//			pCell.setColspan(3);
			pCell.setColspan(4);
//			pCell.setBorderWidthBottom(0);
			allowencedetails.addCell(pCell);
		}
		
//		Phrase blank=new Phrase(" ",font8);
//		PdfPCell blankCell=new PdfPCell(blank);
//		blankCell.setBorder(0);
//		blankCell.setColspan(3);
//		allowencedetails.addCell(blankCell);
//		allowencedetails.addCell(blankCell);
//		allowencedetails.addCell(blankCell);
//		allowencedetails.addCell(blankCell);
//		
		
		
		
		//Gross Heading
		Phrase grosspay=new Phrase("Gross Pay",font8bold);
		PdfPCell grosspaycell=new PdfPCell(grosspay);
		grosspaycell.setBorder(0);
		grosspaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		allowencedetails.addCell(grosspaycell);
		
		//Total Gross Rate
		double totalrate=0;
		for (int i = 0; i <ps.getEarningList().size(); i++) {
			totalrate=totalrate+Math.round(ps.getEarningList().get(i).getAmount()/12);
			
		}
		Phrase totalrateph=new Phrase(df.format(totalrate)+"",font8bold);
		PdfPCell totalratecell=new PdfPCell(totalrateph);
		totalratecell.setBorder(0);
		totalratecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		allowencedetails.addCell(totalratecell);
		
		//Actual Gross Amount
		double revisedArrearsAmt=0;
		double othersArrearsAmt=0;
		for(CtcComponent comp:revisedArrearsList){
			revisedArrearsAmt+=comp.getActualAmount();
		}
		for(CtcComponent comp:otherArrarsList){
			othersArrearsAmt+=comp.getActualAmount();
		}
		
		double totalamount=0;
		for (int i = 0; i <ps.getEarningList().size(); i++) {
			totalamount=totalamount+ps.getEarningList().get(i).getActualAmount();
		}
		Phrase totalamountph=new Phrase(df.format(totalamount)+"",font8bold);
		PdfPCell totalamountcell=new PdfPCell(totalamountph);
		totalamountcell.setBorder(0);
		totalamountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		allowencedetails.addCell(totalamountcell);
		
		Phrase arrearsTotPh=null;
		if((revisedArrearsAmt+othersArrearsAmt)!=0){
			arrearsTotPh=new Phrase(df.format((revisedArrearsAmt+othersArrearsAmt)),font8bold);
		}else{
			arrearsTotPh=new Phrase("",font8);
		}
		PdfPCell totalArrearsCell=new PdfPCell(arrearsTotPh);
		totalArrearsCell.setBorder(0);
		totalArrearsCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		allowencedetails.addCell(totalArrearsCell);
		
		
		
		//total allowence
		
		PdfPTable deductiontitle=new PdfPTable(2); 
		deductiontitle.setWidthPercentage(100);
		try {
			deductiontitle.setWidths(new float[]{60,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase deduction=new Phrase("Deduction",font8bold);
		PdfPCell deductioncell=new PdfPCell(deduction);
		deductioncell.setBorder(0);
		deductioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		deductiontitle.addCell(deductioncell);
		
		Phrase dedAmt=new Phrase("Amount",font8bold);
		PdfPCell dedAmtcell=new PdfPCell(dedAmt);
		dedAmtcell.setBorder(0);
		dedAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		deductiontitle.addCell(dedAmtcell);
		
		PdfPTable deductiondetail=new PdfPTable(2); 
		deductiondetail.setWidthPercentage(100);
		try {
			deductiondetail.setWidths(new float[]{60,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * Rahul Verma added on 18 July 2018
		 * Desc: To Club short name value
		 */
		HashSet<String> shortNameListOfDeduction=new HashSet<String>();
		for (int i = 0; i < ps.getDeductionList().size(); i++) {
			if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()&&(ps.getDeductionList().get(i).getName().equalsIgnoreCase("PL ESIC")||ps.getDeductionList().get(i).getName().equalsIgnoreCase("Bonus ESIC"))){
				shortNameListOfDeduction.add(ps.getDeductionList().get(i).getName());
			}else{
				shortNameListOfDeduction.add(ps.getDeductionList().get(i).getShortName().trim());
			}
		}
		/**
		 * Ends 
		 */
		for (String shortName : shortNameListOfDeduction) {
			String shortNameData=shortName;
			double actualAmountTotal=0;
			for (int i = 0; i < ps.getDeductionList().size(); i++) {
				if(!shortNameData.equalsIgnoreCase("Esic")&&shortNameData.trim().equalsIgnoreCase(ps.getDeductionList().get(i).getShortName().trim())){
					actualAmountTotal=actualAmountTotal+ps.getDeductionList().get(i).getActualAmount();
				}
				
				if(shortNameData.equalsIgnoreCase("ESIC")&&shortNameData.trim().equalsIgnoreCase(ps.getDeductionList().get(i).getShortName().trim())
						&&!ps.getDeductionList().get(i).getName().equalsIgnoreCase("PL ESIC")&&!ps.getDeductionList().get(i).getName().equalsIgnoreCase("Bonus ESIC")){
					actualAmountTotal=ps.getDeductionList().get(i).getActualAmount();
				}
				
				if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()&&shortNameData.equalsIgnoreCase("PL ESIC")&&ps.getDeductionList().get(i).getName().equalsIgnoreCase("PL ESIC")){
					actualAmountTotal=ps.getDeductionList().get(i).getActualAmount();
				}
				if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()&&shortNameData.equalsIgnoreCase("Bonus ESIC")&&ps.getDeductionList().get(i).getName().equalsIgnoreCase("Bonus ESIC")){
					actualAmountTotal=ps.getDeductionList().get(i).getActualAmount();
				}
			}
			noOfLinesdeduction=noOfLinesdeduction-1;
			
			if(isChangeDaysWorkedLabel){
				if(shortNameData.equalsIgnoreCase("PT")){
					shortNameData="Professional Tax";
				}else if(shortNameData.equalsIgnoreCase("PF")){
					shortNameData="PF Contribution";
				}
			}
			Phrase deductionval=new Phrase(shortNameData,font8);
			PdfPCell deductionvalcell=new PdfPCell(deductionval);
			deductionvalcell.setBorder(0);
			deductionvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			deductiondetail.addCell(deductionvalcell);
			
//			Phrase dedvalAmt=new Phrase(df.format(payslip.getDeductionList().get(i).getAmount()),font8);
			Phrase dedvalAmt=new Phrase(df.format(actualAmountTotal),font8);
			PdfPCell dedvalAmtcell=new PdfPCell(dedvalAmt);
			dedvalAmtcell.setBorder(0);
			dedvalAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			deductiondetail.addCell(dedvalAmtcell);
		}
		
		int remainingLinesded=0;
		if (noOfLinesdeduction != 0) {
			remainingLinesded = 10 - (10 - noOfLinesdeduction);
		}
		System.out.println("remainingLines" + remainingLinesded);
		for (int j = 0; j < remainingLinesded; j++) {
			PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			pCell.setBorder(0);
			pCell.setColspan(2);
//			pCell.setBorderWidthBottom(0);
			deductiondetail.addCell(pCell);
		}
		
		
		
//		Phrase blankded=new Phrase(" ",font8);
//		PdfPCell blankdedCell=new PdfPCell(blankded);
//		blankdedCell.setBorder(0);
//		blankdedCell.setColspan(2);
//		deductiondetail.addCell(blankdedCell);
//		deductiondetail.addCell(blankdedCell);
//		deductiondetail.addCell(blankdedCell);
//		allowencedetails.addCell(blankCell);
		
		Phrase total=new Phrase("Total",font8bold);
		PdfPCell totalcell=new PdfPCell(total);
		totalcell.setBorder(0);
		totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		deductiondetail.addCell(totalcell);
		
		double totaldeduct=0;
		for (int i = 0; i < ps.getDeductionList().size(); i++){
//			totaldeduct=totaldeduct+payslip.getDeductionList().get(i).getAmount();
			totaldeduct=totaldeduct+ps.getDeductionList().get(i).getActualAmount();
		}
		
		Phrase totalded=new Phrase(df.format(totaldeduct),font8bold);
		PdfPCell totaldedcell=new PdfPCell(totalded);
		totaldedcell.setBorder(0);
		totaldedcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		deductiondetail.addCell(totaldedcell);
		
		PdfPCell netpayblecell =null;
		if(isOnlyForOrion=true) {
			totalnetpayble=(totalamount+revisedArrearsAmt+othersArrearsAmt)-totaldeduct;
			Phrase netpayble=new Phrase ("Net Payable : "+df.format(totalnetpayble),font9bold);
			netpayblecell =new PdfPCell(netpayble);
			netpayblecell.setColspan(2);
			netpayblecell.setPaddingLeft(5);		
		} else {
			totalnetpayble=(totalamount+revisedArrearsAmt+othersArrearsAmt)-totaldeduct;
			Phrase netpayble=new Phrase ("Net Payable : "+df.format(totalamount+revisedArrearsAmt+othersArrearsAmt)+" - "+df.format(totaldeduct)+" = "+df.format(totalnetpayble),font9bold);
			 netpayblecell =new PdfPCell(netpayble);
			netpayblecell.setColspan(2);
			netpayblecell.setPaddingLeft(5);
		}
		
		/**
		 * @author Anil ,Date :23-02-2019
		 * 
		 */
		
		PdfPCell titleout3=null;
		PdfPCell additionalAllowanceCell=null;
		if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()==true){
			netpayblecell.setColspan(3);
			EmploymentCardPdf cell=new EmploymentCardPdf();
			
			PdfPTable additionalHeadTbl=new PdfPTable(2);
			additionalHeadTbl.setWidthPercentage(100);
			try {
				additionalHeadTbl.setWidths(new float[]{70,30});
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			PdfPTable additionalTbl=new PdfPTable(2);
			additionalTbl.setWidthPercentage(100);
			try {
				additionalTbl.setWidths(new float[]{70,30});
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			additionalHeadTbl.addCell(cell.getCell("Additional Allowance", font8bold,Element.ALIGN_LEFT , 0, 0, 0)).setBorder(0);
			additionalHeadTbl.addCell(cell.getCell("Amount", font8bold,Element.ALIGN_RIGHT , 0, 0, 0)).setBorder(0);
			
			if(ps.getBonus()!=0){
				additionalTbl.addCell(cell.getCell("Bonus", font8,Element.ALIGN_LEFT , 0, 0, 0)).setBorder(0);
				additionalTbl.addCell(cell.getCell(ps.getBonus()+"", font8,Element.ALIGN_RIGHT , 0, 0, 0)).setBorder(0);
				noOfLineAddAllowance--;
			}
			
			if(ps.getPaidLeaves()!=0){
				additionalTbl.addCell(cell.getCell("PL", font8,Element.ALIGN_LEFT , 0, 0, 0)).setBorder(0);
				additionalTbl.addCell(cell.getCell(ps.getPaidLeaves()+"", font8,Element.ALIGN_RIGHT , 0, 0, 0)).setBorder(0);
				noOfLineAddAllowance--;
			}
			
			
			int remLines=0;
			if (noOfLineAddAllowance != 0) {
				remLines = 10 - (10 - noOfLineAddAllowance);
			}
			System.out.println("remainingLines" + remLines);
			for (int j = 0; j < remLines; j++) {
				PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
				pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				pCell.setBorder(0);
				pCell.setColspan(2);
				additionalTbl.addCell(pCell);
			}
			
			Phrase addAllPh=new Phrase("Total",font8bold);
			PdfPCell totAddAllCell=new PdfPCell(addAllPh);
			totAddAllCell.setBorder(0);
			totAddAllCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			additionalTbl.addCell(totAddAllCell);
			
			double additionalAllowance=ps.getBonus()+ps.getPaidLeaves();
			
			Phrase totalAddAllPh=new Phrase(df.format(additionalAllowance),font8bold);
			PdfPCell totAddAllValCell=new PdfPCell(totalAddAllPh);
			totAddAllValCell.setBorder(0);
			totAddAllValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			additionalTbl.addCell(totAddAllValCell);
			
			/***
			 * 
			 */
			titleout3=new PdfPCell(additionalHeadTbl);
			titleout3.setPaddingLeft(5);
			titleout3.setPaddingRight(5);
			
			additionalAllowanceCell=new PdfPCell(additionalTbl);
			additionalAllowanceCell.setPaddingLeft(5);
			additionalAllowanceCell.setPaddingRight(5);
			additionalAllowanceCell.setPaddingBottom(2);
			
			
			/**
			 * 
			 */
		}
		
		
		/**
		 * 
		 * 
		 * 
		 */
		
//		PdfPTable outertab=new PdfPTable(2); 
		PdfPTable outertab=null;
		if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()==true){
			outertab=new PdfPTable(3);
		}else{
			outertab=new PdfPTable(2);
		}
		outertab.setWidthPercentage(100);
		try {
			if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()){
				outertab.setWidths(new float[]{48,26,26});
			}else{
				outertab.setWidths(new float[]{50,50});
			}
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPCell titleout1=new PdfPCell(allowencetitle);
		titleout1.setPaddingLeft(5);
		titleout1.setPaddingRight(5);
		PdfPCell titleout2=new PdfPCell(deductiontitle);
		titleout2.setPaddingLeft(5);
		titleout2.setPaddingRight(5);
		
		PdfPCell allowencedetailout=new PdfPCell(allowencedetails);
		allowencedetailout.setPaddingLeft(5);
		allowencedetailout.setPaddingRight(5);
		allowencedetailout.setPaddingBottom(2);
		PdfPCell deductiondetaildetailout=new PdfPCell(deductiondetail);
		deductiondetaildetailout.setPaddingLeft(5);
		deductiondetaildetailout.setPaddingRight(5);
		deductiondetaildetailout.setPaddingBottom(2);
		
		
		outertab.addCell(titleout1);
		if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()==true){
			outertab.addCell(titleout3);
		}
		outertab.addCell(titleout2);
		
		
		outertab.addCell(allowencedetailout);
		if(ps.isPaidLeaveAndBonusAsAdditionalAllowance()==true){
			outertab.addCell(additionalAllowanceCell);
		}
		outertab.addCell(deductiondetaildetailout);
		
		
		outertab.addCell(netpayblecell);
		
		try {
			document.add(outertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private CtcComponent getArrarsComp(CtcComponent ctcComponent,ArrayList<CtcComponent> revisedArrearsList) {
		// TODO Auto-generated method stub
		for(CtcComponent comp:revisedArrearsList){
			if(comp.getArrearOf().equals(ctcComponent.getName())){
				return comp;
			}
		}
		return null;
	}
	
	
	
	
	private void createOrionEmployeeDetails() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(6);
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(new float[] {10,2,44,15,2,27});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell empname=cell.getCell("Emp Name", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		empname.setBorderWidthRight(0);
		empname.setBorderWidthBottom(0);
		empname.setBorderWidthTop(0);
		
		PdfPCell coln=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
		coln.setBorder(0);
		PdfPCell empname1=cell.getCell(ps.getEmployeeName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		empname1.setBorder(0);
		
		 PdfPCell pfcode=cell.getCell("PF Number", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		 pfcode.setBorder(0);
		 PdfPCell col4=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
		 col4.setBorder(0);
	     PdfPCell pfcode1=cell.getCell(ps.getPfNumber(), font8, Element.ALIGN_LEFT, 0, 0, 0);
	     pfcode1.setBorderWidthLeft(0);
	     pfcode1.setBorderWidthBottom(0);
	     pfcode1.setBorderWidthTop(0);
		
		

		outerTbl.addCell(empname);
		outerTbl.addCell(coln);
		outerTbl.addCell(empname1);
		//outerTbl.addCell(blank);
		outerTbl.addCell(pfcode);
		outerTbl.addCell(col4);
		outerTbl.addCell(pfcode1);
		
		
		 PdfPCell empcode=cell.getCell("Emp Code", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		 empcode.setBorderWidthRight(0);
		 empcode.setBorderWidthBottom(0);
		 empcode.setBorderWidthTop(0);
		 PdfPCell col3=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
		 col3.setBorder(0);
		 PdfPCell empcode1=cell.getCell(ps.getEmpid()+"", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		 empcode1.setBorder(0);
		
		 PdfPCell uan=cell.getCell("UAN", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		 uan.setBorder(0);
		 PdfPCell col6=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
		 col6.setBorder(0);
	     PdfPCell uan1=cell.getCell(ps.getUanNo(), font8, Element.ALIGN_LEFT, 0, 0, 0);
	     uan1.setBorderWidthLeft(0);
	     uan1.setBorderWidthBottom(0);
	     uan1.setBorderWidthTop(0);
		 
		 outerTbl.addCell(empcode);
		 outerTbl.addCell(col3);
		 outerTbl.addCell(empcode1);
		
		 outerTbl.addCell(uan);
		 outerTbl.addCell(col6);
		 outerTbl.addCell(uan1);
		 
		 
		     PdfPCell desg=cell.getCell("Designation", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		     desg.setBorderWidthRight(0);
		     desg.setBorderWidthBottom(0);
		     desg.setBorderWidthTop(0);
			 PdfPCell col5=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
			 col5.setBorder(0);
			 PdfPCell desg1=cell.getCell(ps.getEmployeedDesignation(), font8, Element.ALIGN_LEFT, 0, 0, 0);
			 desg1.setBorder(0);

			  PdfPCell bankacc=cell.getCell("Bank A/C No", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			     bankacc.setBorder(0);;
				 PdfPCell col8=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
				 col8.setBorder(0);
			     PdfPCell bankacc1=cell.getCell(ps.getAccountNumber(), font8, Element.ALIGN_LEFT, 0, 0, 0);
			     bankacc1.setBorderWidthLeft(0);
			     bankacc1.setBorderWidthBottom(0);
			     bankacc1.setBorderWidthTop(0);
				 
			 
			 
			    outerTbl.addCell(desg);
				outerTbl.addCell(col5);
				outerTbl.addCell(desg1);
			
				outerTbl.addCell(bankacc);
				outerTbl.addCell(col8);
				outerTbl.addCell(bankacc1);
			 
				 PdfPCell dep=cell.getCell("Department", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
				 dep.setBorderWidthRight(0);
				 dep.setBorderWidthBottom(0);
				 dep.setBorderWidthTop(0);
				 PdfPCell col7=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
				 col7.setBorder(0);
				 PdfPCell dep1=cell.getCell(ps.getDepartment(), font8, Element.ALIGN_LEFT, 0, 0, 0);
				 dep1.setBorder(0);
				 PdfPCell adhar=cell.getCell("Aadhar No", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			     adhar.setBorder(0);
				 PdfPCell col10=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
				 col10.setBorder(0);
			     PdfPCell adhar1=cell.getCell(ps.getAadharNo(), font8, Element.ALIGN_LEFT, 0, 0, 0);
			     adhar1.setBorderWidthLeft(0);
			     adhar1.setBorderWidthBottom(0);
			     adhar1.setBorderWidthTop(0);
               
				    outerTbl.addCell(dep);
					outerTbl.addCell(col7);
					outerTbl.addCell(dep1);
					//outerTbl.addCell(blank3);
					outerTbl.addCell(adhar);
					outerTbl.addCell(col10);
					outerTbl.addCell(adhar1);
				 
		
					 PdfPCell branch=cell.getCell("Branch", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
					 branch.setBorderWidthRight(0);
					 branch.setBorderWidthBottom(0);
					 branch.setBorderWidthTop(0);
					 PdfPCell col9=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
					 col9.setBorder(0);
					 PdfPCell branch1=cell.getCell(ps.getBranch(), font8, Element.ALIGN_LEFT, 0, 0, 0);
					 branch1.setBorder(0);	
					 PdfPCell pan=cell.getCell("Pan No", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
					 pan.setBorder(0);
					 PdfPCell col12=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
					 col12.setBorder(0);
				     PdfPCell pan1=cell.getCell(ps.getPanNumber(), font8, Element.ALIGN_LEFT, 0, 0, 0);
				     pan1.setBorderWidthLeft(0);
				     pan1.setBorderWidthBottom(0);
				     pan1.setBorderWidthTop(0);
					 
					 
				    
					    outerTbl.addCell(branch);
						outerTbl.addCell(col9);
						outerTbl.addCell(branch1);
				        outerTbl.addCell(pan);
						outerTbl.addCell(col12);
						outerTbl.addCell(pan1);
					 
		
						 PdfPCell jod=cell.getCell("Joining Date", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
						 jod.setBorderWidthRight(0);
						 jod.setBorderWidthBottom(0);
						 jod.setBorderWidthTop(0);
						 PdfPCell col11=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
						 col11.setBorder(0);
						 PdfPCell jod1=cell.getCell(fmt.format(ps.getDoj()), font8, Element.ALIGN_LEFT, 0, 0, 0);
						 jod1.setBorder(0);
						 
						    PdfPCell workingdays=cell.getCell("Working Days", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
							workingdays.setBorder(0);
							PdfPCell coln2=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
							coln2.setBorder(0);
							PdfPCell workingdays1=cell.getCell(ps.getPaidDays()+"", font8, Element.ALIGN_LEFT, 0, 0, 0);
							workingdays1.setBorderWidthLeft(0);
							workingdays1.setBorderWidthBottom(0);
							workingdays1.setBorderWidthTop(0);
						    outerTbl.addCell(jod);
							outerTbl.addCell(col11);
							outerTbl.addCell(jod1);
						    outerTbl.addCell(workingdays);
							outerTbl.addCell(coln2);
							outerTbl.addCell(workingdays1);
							
							
							
							
							
						
							if(ps.getUnpaidDay()!=0){
								String unpaidDayLbl="";
								if(isChangeDaysWorkedLabel){
									unpaidDayLbl="Leave Without Pay";
								}else{
									unpaidDayLbl="Unpaid Days";
								}
								
								PdfPCell am=cell.getCell("  ", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
								am.setBorderWidthRight(0);
								am.setBorderWidthBottom(0);
								am.setBorderWidthTop(0);
								PdfPCell col13=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0);
								col13.setBorder(0);
								PdfPCell pm=cell.getCell("  ", font8, Element.ALIGN_LEFT, 0, 0, 0);
								pm.setBorder(0);
								
								PdfPCell unpaisdays=cell.getCell(unpaidDayLbl, font8bold, Element.ALIGN_LEFT, 0, 0, 0);
								unpaisdays.setBorder(0);
								PdfPCell col14=cell.getCell(":", font8, Element.ALIGN_CENTER, 0, 0, 0);
								col14.setBorder(0);
								PdfPCell phUnpaidVl=cell.getCell(ps.getUnpaidDay()+"", font8, Element.ALIGN_LEFT, 0, 0, 0);
								phUnpaidVl.setBorderWidthLeft(0);
								phUnpaidVl.setBorderWidthBottom(0);
								phUnpaidVl.setBorderWidthTop(0);
								
								outerTbl.addCell(am);
								outerTbl.addCell(col13);
								outerTbl.addCell(pm);
							    outerTbl.addCell(unpaisdays);
								outerTbl.addCell(col14);
								outerTbl.addCell(phUnpaidVl);
										
							
							
								
							}
							try {
								document.add(outerTbl);
							} catch (DocumentException e) {
								e.printStackTrace();
							}
		
		
		
		
		
		
	}

	private void createemployeeDetails() {
		
		PdfPTable employeetab=new PdfPTable(2);
		employeetab.setWidthPercentage(100);
		
		try {
			employeetab.setWidths(new float[]{19,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase attendanceph=new Phrase("Emp Name  "+ ps.getEmployeeName(),font8bold);
		PdfPCell attendancecell=new PdfPCell(attendanceph);
		attendancecell.setBorder(0);
		attendancecell.setColspan(2);
		attendancecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		if(isEmployeeNameOnLeftSide){
			employeetab.addCell(attendancecell);
		}
		
		Phrase empname=new Phrase("Emp Code   "+ps.getEmpid(),font8bold);
		PdfPCell empnamecell=new PdfPCell(empname);
		empnamecell.setBorder(0);
		empnamecell.setColspan(2);
		empnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(empnamecell);
		
		
		if(!isHideProjectName){
			Phrase projectPh=new Phrase("Project ",font8bold);
			PdfPCell projectCell=new PdfPCell(projectPh);
			projectCell.setBorder(0);
			projectCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab.addCell(projectCell);
		
			String projectName="";
			if(ps.getProjectName()!=null){
				projectName=ps.getProjectName();
			}
			
			/**
			 * @author Anil
			 * @since 01-09-2020
			 */
			if(ps.getSiteLocation()!=null&&!ps.getSiteLocation().equals("")){
				projectName=projectName+" / "+ps.getSiteLocation();
			}
			
			Phrase projectValPh=new Phrase(projectName,font8);
			PdfPCell projectValCell=new PdfPCell(projectValPh);
			projectValCell.setBorder(0);
			projectValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab.addCell(projectValCell);
		}
		
		/**
		 * Date : 11-09-2018 By ANIL
		 */
		
		if(!isHideFatherOrSpouseName){
			Phrase fathername=new Phrase("Father/Spouse Name",font8bold);
			PdfPCell fathernamecell=new PdfPCell(fathername);
			fathernamecell.setBorder(0);
			fathernamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab.addCell(fathernamecell);
			
			Phrase fathernameval=null;
//			if(ps.getGender()!=null&&!ps.getGender().equals("")
//					&&ps.getGender().trim().equalsIgnoreCase("Female")
//					&&ps.getSpouseName()!=null&&!ps.getSpouseName().equals("")){
//				fathernameval=new Phrase(ps.getSpouseName(),font8);
//			}else{
//				fathernameval=new Phrase(ps.getFatherName(),font8);
//			}
			/**
			 *  Date : 26-02-2021 By Priyanka
			 *  Des : Print father name on salary slip from employee if not present then
			 *   from Emp add. Details
			 */
			
			
			if(emp.getGender()!=null&&!emp.getGender().equals("")){
				
				String fatherhasbandName = "";
				if(emp.getHusbandName()!=null && !emp.getHusbandName().equals("")){
					fatherhasbandName = emp.getHusbandName();
				}
				else{
					if(empAddDetObj!=null){
						for(int i=0; i<empAddDetObj.getEmpFamDetList().size(); i++)
						{
							if(emp.getGender().equals("Male")){
								if(empAddDetObj.getEmpFamDetList().get(i).getFamilyRelation().trim().equalsIgnoreCase("Father")){
									fatherhasbandName = empAddDetObj.getEmpFamDetList().get(i).getFname().trim();
								}
							}
							else{
								if(emp.getGender().equals("Female")){
									if(empAddDetObj.getEmpFamDetList().get(i).getFamilyRelation().trim().equalsIgnoreCase("Father") ||
											empAddDetObj.getEmpFamDetList().get(i).getFamilyRelation().trim().equalsIgnoreCase("Husband")){
										fatherhasbandName = empAddDetObj.getEmpFamDetList().get(i).getFname().trim();
									}
								}
							}
						}

					}
				}
				
				fathernameval=new Phrase(fatherhasbandName,font8);
			}
			else{
				fathernameval=new Phrase("",font8);
			}
			
			
			
			PdfPCell fathernamevalcell=new PdfPCell(fathernameval);
			fathernamevalcell.setBorder(0);
			fathernamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab.addCell(fathernamevalcell);
		}
		
		/**
		 * End
		 */
		
		
		Phrase designationph=new Phrase("Designation",font8bold);
		PdfPCell designationcell=new PdfPCell(designationph);
		designationcell.setBorder(0);
		designationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(designationcell);
		
		Phrase designationvalph=new Phrase(ps.getEmployeedDesignation(),font8);
		PdfPCell designationvalcell=new PdfPCell(designationvalph);
		designationvalcell.setBorder(0);
		designationvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(designationvalcell);
		
       /**@author Amol
        *Date 10-6-2019
        *for hide the Department Name raised by Rahul 
        */
		if(!isHideDepartmentName){
		Phrase departmentph=new Phrase("Department",font8bold);
		PdfPCell departmentcell=new PdfPCell(departmentph);
		departmentcell.setBorder(0);
		departmentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(departmentcell);
		
		Phrase departmentvalph=new Phrase(ps.getDepartment(),font8);
		PdfPCell departmentvalcell=new PdfPCell(departmentvalph);
		departmentvalcell.setBorder(0);
		departmentvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(departmentvalcell);
		}
		
		
		Phrase unitph=new Phrase("Branch",font8bold);
		PdfPCell unitcell=new PdfPCell(unitph);
		unitcell.setBorder(0);
		unitcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(unitcell);
		
		Phrase unitvalph=new Phrase(ps.getBranch(),font8);
		PdfPCell unitvalcell=new PdfPCell(unitvalph);
		unitvalcell.setBorder(0);
		unitvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab.addCell(unitvalcell);
		
		Phrase joiningdateph=new Phrase("Joining Date",font8bold);
		PdfPCell joiningdatecell=new PdfPCell(joiningdateph);
		joiningdatecell.setBorder(0);
		joiningdatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		employeetab.addCell(joiningdatecell);
		
		Phrase joiningdatevalph=null;
		if(ps.getDoj()!=null){
			 joiningdatevalph=new Phrase(fmt.format(ps.getDoj()),font8);
		}
		else{
		 joiningdatevalph=new Phrase("",font8);
		}
		PdfPCell joiningdatevalcell=new PdfPCell(joiningdatevalph);
		joiningdatevalcell.setBorder(0);
		joiningdatevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		employeetab.addCell(joiningdatevalcell);
		
		
		PdfPTable employeetab2=new PdfPTable(5);
		employeetab2.setWidthPercentage(100);
		
		try {
			employeetab2.setWidths(new float[]{19,26,5,20,30});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase pfnumberph=new Phrase("PF Number",font8bold);
		PdfPCell pfnumbercell=new PdfPCell(pfnumberph);
		pfnumbercell.setBorder(0);
		pfnumbercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(pfnumbercell);
		
		Phrase pfnumbervalph=null;
		/**@author Amol 
		 * Date 10-6-2019
		 * added a company pf number and employee PF Number
		 */
		String CompanyPFNumberEmployeePfNumber = null;
		if(isShowCompanyPfNumberandEsicNumber){
			
	    if(comp.getPfGroupNum()!=null&&emp.getPPFNaumber()!=null&&!comp.getPfGroupNum().equals("")&&!emp.getPPFNaumber().equals("")){
	    	
		     CompanyPFNumberEmployeePfNumber=comp.getPfGroupNum()+"/"+emp.getPPFNaumber();
		     pfnumbervalph=new Phrase(CompanyPFNumberEmployeePfNumber,font8);
		     logger.log(Level.SEVERE," Inside Pf Number IF");
	    }else{	
	    	 pfnumbervalph=new Phrase("",font8);
	    	 logger.log(Level.SEVERE," Inside Pf Number ELSE ");
	    }
	       }else{
		if(ps.getPfNumber()!=null){
		   pfnumbervalph=new Phrase(ps.getPfNumber(),font8);
		}else{
		   pfnumbervalph=new Phrase("",font8);
		}
		}
		
		PdfPCell pfnumbervalcell=new PdfPCell(pfnumbervalph);
		pfnumbervalcell.setBorder(0);
		if(isShowCompanyPfNumberandEsicNumber){
			pfnumbervalcell.setColspan(4);
		}
		pfnumbervalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(pfnumbervalcell);
		
		
		Phrase blankph=new Phrase(" ",font8);
		PdfPCell blankcell=new PdfPCell(blankph);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		if(!isShowCompanyPfNumberandEsicNumber){
			employeetab2.addCell(blankcell);
		}
//		employeetab2.addCell(blankcell);
//		
//		if(isShowCompanyPfNumberandEsicNumber){
//			
//			employeetab2.addCell(blankcell);
//			employeetab2.addCell(blankcell);
//		}
//		
		/**
		 * Updated By: Viraj
		 * Date: 0-06-2019
		 * Description: To add ESIC no above uan no with process config(if true it will show)
		 */
		if(isShowEsic) {
			Phrase esicNoPh=new Phrase("ESIC No",font8bold);
			PdfPCell esicNoPhCell=new PdfPCell(esicNoPh);
			esicNoPhCell.setBorder(0);
			esicNoPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab2.addCell(esicNoPhCell);
			
			Phrase ESICvalph=null;
			String showesicnumber=null;
			PdfPCell ESICvalcell =null;
			if(isShowCompanyPfNumberandEsicNumber){
				if(emp.getEmployeeESICcode()!=null&&branch.getEsicCode()!=null&&!emp.getEmployeeESICcode().equals("")&&!branch.getEsicCode().equals("")){
					showesicnumber=branch.getEsicCode()+"/"+emp.getEmployeeESICcode();
					ESICvalph=new Phrase(showesicnumber,font8);
				}else{
					ESICvalph=new Phrase("",font8);
				}
				
			} else {
				if (ps.getEsicNumber() != null) {
					ESICvalph = new Phrase(ps.getEsicNumber(), font8);
				} else {
					ESICvalph = new Phrase("", font8);
				}
				
			}
			ESICvalcell = new PdfPCell(ESICvalph);
			ESICvalcell.setBorder(0);
			if (isShowCompanyPfNumberandEsicNumber) {
				ESICvalcell.setColspan(4);
			}
			ESICvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab2.addCell(ESICvalcell);
		}
		
//		if(isShowCompanyPfNumberandEsicNumber){
//			employeetab2.addCell(blankcell);
//			employeetab2.addCell(blankcell);
//			employeetab2.addCell(blankcell);
//		}
		
		
		
		
		
		/** Ends **/
		
		Phrase UANph=new Phrase("UAN",font8bold);
		PdfPCell UANcell=new PdfPCell(UANph);
		UANcell.setBorder(0);
		UANcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(UANcell);
		
		Phrase UANvalph=null;
		if(ps.getUanNo()!=null){
			 UANvalph=new Phrase(ps.getUanNo(),font8);
		}
		else{
			 UANvalph=new Phrase("",font8);
		}
		PdfPCell UANvalcell=new PdfPCell(UANvalph);
		UANvalcell.setBorder(0);
		UANvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(UANvalcell);
		
		if(isShowEsic) {
			employeetab2.addCell(blankcell);
		}
		/**
		 * Date : 11-09-2018 By ANIL
		 */
		
		employeetab2.addCell(joiningdatecell);
		employeetab2.addCell(joiningdatevalcell);
		
		if(!isShowEsic) {
			employeetab2.addCell(blankcell);
		}
		
		Phrase bankacph=new Phrase("Bank A/C No",font8bold);
		PdfPCell bankaccell=new PdfPCell(bankacph);
		bankaccell.setBorder(0);
		bankaccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(bankaccell);
		Phrase bankacvalph=null;
		if(ps.getAccountNumber()!=null){
			 bankacvalph=new Phrase(ps.getAccountNumber(),font8);
		}
		else{
			bankacvalph=new Phrase("",font8);
		}
		PdfPCell bankacvalcell=new PdfPCell(bankacvalph);
		bankacvalcell.setBorder(0);
		bankacvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(bankacvalcell);
		
		if(isShowEsic) {
			employeetab2.addCell(blankcell);
		}
		/**
		 * End
		 */
		
		Phrase panph=new Phrase("Pan No",font8bold);
		PdfPCell pancell=new PdfPCell(panph);
		pancell.setBorder(0);
		pancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(pancell);
		
		
		Phrase panvalph=null;
		if(ps.getPanNumber()!=null){
			 panvalph=new Phrase(ps.getPanNumber(),font8);
		}
		else{
			 panvalph=new Phrase(ps.getPanNumber(),font8);
		}
		
		PdfPCell panvalcell=new PdfPCell(panvalph);
		panvalcell.setBorder(0);
		panvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(panvalcell);
		
		if(!isShowEsic) {
			employeetab2.addCell(blankcell);
		}
		
		
		Phrase adharph=new Phrase("Aadhar No",font8bold);
		PdfPCell adharcell=new PdfPCell(adharph);
		adharcell.setBorder(0);
		adharcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(adharcell);
		
		Phrase adharcardvalph=null;
		if(ps.getAadharNo()!=null){
			 adharcardvalph=new Phrase(ps.getAadharNo(),font8);
		}
		else{
			 adharcardvalph=new Phrase(ps.getAadharNo(),font8);
		}
		PdfPCell adharcardvalcell=new PdfPCell(adharcardvalph);
		adharcardvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		adharcardvalcell.setBorder(0);
		adharcardvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetab2.addCell(adharcardvalcell);
		
		if(isShowEsic) {
			employeetab2.addCell(blankcell);
		}
		
		////
		if(!isHideEmailId){
			Phrase emailph=new Phrase("E-mail",font8bold);
			PdfPCell emailcell=new PdfPCell(emailph);
			emailcell.setBorder(0);
			emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab2.addCell(emailcell);
			
			String email="";
			if(ps.getEmpEmail()!=null&&!ps.getEmpEmail().equalsIgnoreCase("ABC@123.com")){
				email=ps.getEmpEmail();
			}
			Phrase emailvalph=null;
			emailvalph=new Phrase(email,font8);
			
			PdfPCell emailvalcell=new PdfPCell(emailvalph);
			emailvalcell.setBorder(0);
			emailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab2.addCell(emailvalcell);
			
			if(!isShowEsic) {
				employeetab2.addCell(blankcell);
			}
		}
		
		if(!isHideHappayNo){
			Phrase happayPh=new Phrase("Happay No ",font8bold);
			PdfPCell happayCell=new PdfPCell(happayPh);
			happayCell.setBorder(0);
			happayCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab2.addCell(happayCell);
			Phrase happayVlPh=null;
			if(ps.getHappayCardNumber()!=null){
				 happayVlPh=new Phrase(ps.getHappayCardNumber(),font8);
			}
			else{
				happayVlPh=new Phrase("",font8);
			}
			PdfPCell happayCardCell=new PdfPCell(happayVlPh);
			happayCardCell.setBorder(0);
			happayCardCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			employeetab2.addCell(happayCardCell);
			
			if(isShowEsic) {
				employeetab2.addCell(blankcell);
			}
		
		}
		if(isHideEmailId||isHideHappayNo){
			if(isHideEmailId&&!isHideHappayNo){
				employeetab2.addCell(blankcell);
				employeetab2.addCell(blankcell);
				employeetab2.addCell(blankcell);
			}else if(!isHideEmailId&&isHideHappayNo){
				employeetab2.addCell(blankcell);
				employeetab2.addCell(blankcell);
				
			}
		}
		
		////
		
		PdfPTable empmaintab=new PdfPTable(1);
		empmaintab.setWidthPercentage(100);
		
		PdfPCell tab1cell=new PdfPCell(employeetab);
		tab1cell.setBorder(0);
		empmaintab.addCell(tab1cell);
		
		PdfPCell tab2cell=new PdfPCell(employeetab2);
		tab2cell.setBorder(0);
		empmaintab.addCell(tab2cell);
		
		/// employee tab complet
		
		PdfPTable leavtab=new PdfPTable(2);
		leavtab.setWidthPercentage(100);
		
		try {
			leavtab.setWidths(new float[]{60,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(!isEmployeeNameOnLeftSide){
			leavtab.addCell(attendancecell);
		}
		
		if(!isHidePaidLeave){
			Phrase plph=new Phrase("Paid Leave",font8bold);
			PdfPCell plcell=new PdfPCell(plph);
			plcell.setBorder(0);
			plcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(plcell);
			/**
			 * Updated By: Viraj
			 * Date: 04-06-2019
			 * Description: To store availed leave in paid leave  
			 */
			if(isShowEsic) {
				double availedLeave = 0;
				for(int i=0;i < ps.getLeaveBalList().size();i++) {
					if(ps.getLeaveBalList().get(i).getShortName().equalsIgnoreCase("PL")) {
						availedLeave = ps.getLeaveBalList().get(i).getAvailedDays();
					}
				}
				Phrase plvalph=new Phrase(df.format(availedLeave)+"",font8);
				PdfPCell plvalcell=new PdfPCell(plvalph);
				plvalcell.setBorder(0);
				plvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				leavtab.addCell(plvalcell);
			} else {
				Phrase plvalph=new Phrase(df.format(ps.getEarnedLeave())+"",font8);
				PdfPCell plvalcell=new PdfPCell(plvalph);
				plvalcell.setBorder(0);
				plvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				leavtab.addCell(plvalcell);
			}
		}
		
		if(!isHideWeeklyOff){
			Phrase weeklyofph=new Phrase("Weekly Off",font8bold);
			PdfPCell weeklyofcell=new PdfPCell(weeklyofph);
			weeklyofcell.setBorder(0);
			weeklyofcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(weeklyofcell);
			
			Phrase weeklyofvalph=new Phrase(df.format(ps.getWeekDays()) + "",font8);
			PdfPCell weeklyofvalcell=new PdfPCell(weeklyofvalph);
			weeklyofvalcell.setBorder(0);
			weeklyofvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			leavtab.addCell(weeklyofvalcell);
		}
		
		if(!isHideOtherHoliday){
			Phrase holidayph=new Phrase("Other Holiday",font8bold);
			PdfPCell holidaycell=new PdfPCell(holidayph);
			holidaycell.setBorder(0);
			holidaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(holidaycell);
			
			Phrase holidayvalph=new Phrase(df.format(ps.getHoliday()) + "",font8);
			PdfPCell holidayvalcell=new PdfPCell(holidayvalph);
			holidayvalcell.setBorder(0);
			holidayvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			leavtab.addCell(holidayvalcell);
		}
		String daysWorkedLbl="";
		if(isChangeDaysWorkedLabel){
			daysWorkedLbl="Working Days";
		}else{
			daysWorkedLbl="Days Worked";
		}
		
		Phrase workedph=new Phrase(daysWorkedLbl,font8bold);
		PdfPCell workedcell=new PdfPCell(workedph);
		workedcell.setBorder(0);
		workedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leavtab.addCell(workedcell);
		
		/**
		 * @author Anil , Date : 29-06-2019
		 * earlier under worked days,we were showing paid days but as per the rahul tiwari requirement and discussed with nitin sir
		 * will show only working days excluding weekly off and holidays
		 * @author Anil , Date : 02-10-2019
		 * As Elgible days contains paid daya in it ,so subtracting paid leave from it
		 */
//		Phrase workedvalph=new Phrase(df.format(ps.getPaidDays())+"",font8);
		Phrase workedvalph=new Phrase(df.format(ps.getEligibleDays())+"",font8);
		PdfPCell workedvalcell=new PdfPCell(workedvalph);
		workedvalcell.setBorder(0);
		workedvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		leavtab.addCell(workedvalcell);
		
		if(!isHidePaidDays){
			Phrase daypaidph=new Phrase("Days Paid",font8bold);
			PdfPCell daypaidcell=new PdfPCell(daypaidph);
			daypaidcell.setBorder(0);
			daypaidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(daypaidcell);
			
			/**
			 * @author Anil , Date : 29-06-2019
			 * earlier under Days paid,we were showing monthly days but as per the rahul tiwari requirement and discussed with nitin sir
			 * will show only paid days 
			 */
//			Phrase daypaidvalph=new Phrase(df.format(ps.getMonthlyDays()) + "",font8);
			Phrase daypaidvalph=new Phrase(df.format(ps.getPaidDays()) + "",font8);
			PdfPCell daypaidvalcell=new PdfPCell(daypaidvalph);
			daypaidvalcell.setBorder(0);
			daypaidvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			leavtab.addCell(daypaidvalcell);
		}
		
		
		if(ps.getUnpaidDay()!=0){
			String unpaidDayLbl="";
			if(isChangeDaysWorkedLabel){
				unpaidDayLbl="Leave Without Pay";
			}else{
				unpaidDayLbl="Unpaid Days";
			}
			
			Phrase phUnpaid=new Phrase(unpaidDayLbl,font8bold);
			PdfPCell clUnpaid=new PdfPCell(phUnpaid);
			clUnpaid.setBorder(0);
			clUnpaid.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(clUnpaid);
			
			Phrase phUnpaidVl=new Phrase(df.format(ps.getUnpaidDay()) + "",font8);
			PdfPCell clUnpaidVl=new PdfPCell(phUnpaidVl);
			clUnpaidVl.setBorder(0);
			clUnpaidVl.setHorizontalAlignment(Element.ALIGN_RIGHT);
			leavtab.addCell(clUnpaidVl);
		}
		
		if(!isHideExtraDays){
			Phrase extraDayPh=new Phrase("Extra Day",font8bold);
			PdfPCell extraDayCell=new PdfPCell(extraDayPh);
			extraDayCell.setBorder(0);
			extraDayCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(extraDayCell);
			
			Phrase extraDayValPh=new Phrase(ps.getExtraDay() + "",font8);
			PdfPCell extraDayValCell=new PdfPCell(extraDayValPh);
			extraDayValCell.setBorder(0);
			extraDayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			leavtab.addCell(extraDayValCell);
		}
		
		/**
		 *@author Anil ,Date : 15-04-2019 
		 */
		
		if(ps.isOtIncludedInGross()&&ps.getOverTimehrs()!=0){
			Phrase otPh=new Phrase("OT(In Hours)",font8bold);
			PdfPCell otCell=new PdfPCell(otPh);
			otCell.setBorder(0);
			otCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			leavtab.addCell(otCell);
			
			/**
			 * @author Anil , Date : 14-06-2019
			 * updating OT HOURS
			 */
			double otHours=ps.getOverTimehrs();
			double ot=0;
//			boolean otHisFlag=false;
			if(ps.getEmpOtHistoryList()!=null&&ps.getEmpOtHistoryList().size()!=0){
//				otHisFlag=true;
				for(EmployeeOvertimeHistory eoh:ps.getEmpOtHistoryList()){
					if(eoh.isPayOtAsOther()){
						ot=ot+eoh.getOtHours();
					}
				}
			}
			/**
			 * @author Anil , Date : 28-06-2019
			 * 
			 */
			
			otHours=otHours-ot;
			
//			if(ot!=0){
//				otHours=ot;
//			}else{
//				if(otHisFlag){
//					otHours=ot;
//				}
//			}
			
			
			Phrase otValPh=new Phrase(otHours + "",font8);
			PdfPCell otValCell=new PdfPCell(otValPh);
			otValCell.setBorder(0);
			otValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			leavtab.addCell(otValCell);
		}
		
		/**
		 * 
		 */
		
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);
		
		
		/** date 15.11.2018 added by komal to show leave details on payslip **/
		if(leaveBalanceFlag){
		PdfPTable leaveTable = new PdfPTable(5);
		leaveTable.setWidthPercentage(100);
		try {
			leaveTable.setWidths(new float[]{36,16,16,16,16});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/**
		 * @author Anil ,Date : 09-03-2019
		 * Showinng leave info
		 */
		if(ps.getLeaveBalList()!=null&&ps.getLeaveBalList().size()!=0){
//		if(leaveBalance != null){
//			if(leaveBalance.getAllocatedLeaves().size() >0){
				leaveTable.addCell(getCell("Attendance/Leave", font7bold, Element.ALIGN_LEFT));
				leaveTable.addCell(getCell("Opening", font7bold, Element.ALIGN_LEFT));
				leaveTable.addCell(getCell("Earned", font7bold, Element.ALIGN_CENTER));
				leaveTable.addCell(getCell("Taken", font7bold, Element.ALIGN_LEFT));
				leaveTable.addCell(getCell("Closing", font7bold, Element.ALIGN_LEFT));
//			}
	
			for(AllocatedLeaves leaves : ps.getLeaveBalList()){
				if(leaves.getShortName().equalsIgnoreCase("PL")){// || (leaves.getPaidLeave() != null && leaves.getPaidLeave().equals(true)
				//	if(!(leaves.getShortName().equalsIgnoreCase("WO") ||  leaves.getShortName().equalsIgnoreCase("W/O"))){
					leaveTable.addCell(getCell(leaves.getShortName(), font7, Element.ALIGN_LEFT));
//					leaveTable.addCell(getCell(leaves.getMaxDays()+"", font7, Element.ALIGN_LEFT));
					leaveTable.addCell(getCell(leaves.getOpeningBalance()+"", font7, Element.ALIGN_LEFT));
					leaveTable.addCell(getCell(leaves.getEarned()+"", font7, Element.ALIGN_CENTER));
					leaveTable.addCell(getCell(leaves.getAvailedDays()+"", font7, Element.ALIGN_LEFT));
					leaveTable.addCell(getCell(leaves.getBalance()+"", font7, Element.ALIGN_LEFT));
					
				//	}
				}
			}
		}
		
		
		
		
			PdfPCell tabcell2=new PdfPCell(leaveTable);
			tabcell2.setBorder(0);
			rightTable.addCell(tabcell2);
		}
		PdfPCell tabcell1=new PdfPCell(leavtab);
		tabcell1.setBorder(0);
		rightTable.addCell(tabcell1);
		PdfPTable mainheader=new PdfPTable(2);
		mainheader.setWidthPercentage(100);
		try {
			mainheader.setWidths(new float[]{60,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell leftcell=new PdfPCell(empmaintab);
		leftcell.setBorderWidthRight(0);
		leftcell.setPaddingLeft(5);
		leftcell.setPaddingBottom(5);
		mainheader.addCell(leftcell);
		
		PdfPCell rightcell=new PdfPCell(rightTable);
		rightcell.setBorderWidthLeft(0);
		rightcell.setPaddingRight(5);
		rightcell.setPaddingBottom(5);
		mainheader.addCell(rightcell);
		
		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createHeader() {

		//Date 16/11/2017 By Jayshree to add the logo
		DocumentUpload logodocument =comp.getLogo();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setWidthPercentage(100f);
		
		try {
			logoTable.setWidths(new float[]{15,75});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if (imageSignCell != null) {
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		} else {
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}
		
		
		//Date 16/11/2017
		//By Jayshree 
		//to add the web site in header part
		PdfPTable headTab = new PdfPTable(1);
		headTab.setWidthPercentage(100f);

		String businessunit=null;
		businessunit=comp.getBusinessUnitName();
		Phrase companyNameph = new Phrase(businessunit.toUpperCase(),font12bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headTab.addCell(companyNameCell);

		if(!isHideCompanyAddress){
			Phrase companyAddph = new Phrase(
					comp.getAddress().getCompleteAddress(), font9);
			PdfPCell companyAddCell = new PdfPCell(companyAddph);
	//		companyAddCell.setColspan(3);
			companyAddCell.setBorder(0);
			companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headTab.addCell(companyAddCell);
		}else{
			/**
			 * 14-12-2018 added by amol
			 */
			Phrase companyAddph = new Phrase(" ", font9);
			PdfPCell companyAddCell = new PdfPCell(companyAddph);
			companyAddCell.setRowspan(2);
			companyAddCell.setBorder(0);
			companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headTab.addCell(companyAddCell);
		}
		
		String salaryPeriod=ps.getSalaryPeriod();
		String[] res=salaryPeriod.split("-",0);
		String month =res[1].trim();
		String year =res[0].trim();
			
		String title1 = "";
		title1 = "Payslip For The Month Of "+month+"-"+year;
		
		
		if(!isOnlyForOrion){
			Phrase headblank = new Phrase(title1, font9bold);
			PdfPCell headblankCell = new PdfPCell(headblank);
			System.out.println("HEADER if false");
			headblankCell.setBorder(0);
			headblankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headTab.addCell(headblankCell);
		}
		
		
		/**
		 * 
		 * 
		 */
		PdfPTable headerTable = null;
		
		if(isOnlyForOrion){

			headerTable = new PdfPTable(3);
			headerTable.setWidthPercentage(100f);
	
			try {
				headerTable.setWidths(new float[] {20,60,20});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			/**
			 * Date : 24-08-2018 By ANIL
			 * Added FORM XIX 
			 */
			Phrase phFrm=new Phrase("FORM XIX",font8);
			PdfPCell frmCell=new PdfPCell(phFrm);
			frmCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			frmCell.setColspan(3);
			frmCell.setBorder(0);
			headerTable.addCell(frmCell);
			/**
			 * End
			 */
			
			PdfPCell logoout = new PdfPCell(logoTable);
			logoout.setBorderWidthBottom(0);
			logoout.setBorderWidthRight(0);
	        logoout.setHorizontalAlignment(Element.ALIGN_RIGHT);
			headerTable.addCell(logoout);
//	
			PdfPCell headOut = new PdfPCell(headTab);
			headOut.setBorderWidthBottom(0);
			headOut.setBorderWidthLeft(0);
			headOut.setBorderWidthRight(0);
			headerTable.addCell(headOut);
			
			Phrase 	headOutph=new Phrase("  ",font9);
			PdfPCell headOut3 = new PdfPCell(headOutph);
			headOut3.setBorderWidthBottom(0);
			headOut3.setBorderWidthLeft(0);
			headerTable.addCell(headOut3);
			
			
			//end Here
			
			
			Phrase 	logoout2ph=new Phrase("  ",font9);
			PdfPCell logoout2 = new PdfPCell(logoout2ph);
			logoout2.setBorderWidthBottom(0);
			logoout2.setBorderWidthRight(0);
			logoout2.setBorderWidthTop(0);
			headerTable.addCell(logoout2);
//	
			Phrase 	headOut2ph=new Phrase("  ",font9);
			PdfPCell headOut2 = new PdfPCell(headOut2ph);
			headOut2.setBorderWidthBottom(0);
			headOut2.setBorderWidthLeft(0);
			headOut2.setBorderWidthTop(0);
			headOut2.setColspan(2);
			headerTable.addCell(headOut2);
			
			/**
			 * Date : 21-12-2018 By AMOL
			 */
			System.out.println("Added...");
			Phrase headblank = new Phrase(title1, font9bold);
			PdfPCell headblankCell = new PdfPCell(headblank);
			headblankCell.setColspan(3);
			headblankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headerTable.addCell(headblankCell);
			
			/**
			 * End
			 */
		
		}else{
			headerTable = new PdfPTable(2);
			headerTable.setWidthPercentage(100f);
	
			try {
				headerTable.setWidths(new float[] {15,75});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			/**
			 * Date : 24-08-2018 By ANIL
			 * Added FORM XIX 
			 */
			Phrase phFrm=new Phrase("FORM XIX",font8);
			PdfPCell frmCell=new PdfPCell(phFrm);
			frmCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			frmCell.setColspan(2);
			frmCell.setBorder(0);
			headerTable.addCell(frmCell);
			/**
			 * End
			 */
			
			
			
			PdfPCell logoout = new PdfPCell(logoTable);
	//		logoout.addElement(logoTable);
			logoout.setBorderWidthBottom(0);
			logoout.setBorderWidthRight(0);
			logoout.setHorizontalAlignment(Element.ALIGN_RIGHT);
			headerTable.addCell(logoout);
	
			PdfPCell headOut = new PdfPCell(headTab);
	//		headOut.addElement(headTab);
			headOut.setBorderWidthBottom(0);
			headOut.setBorderWidthLeft(0);
			headerTable.addCell(headOut);
			//end Here
			
			
			Phrase 	logoout2ph=new Phrase("  ",font8);
			PdfPCell logoout2 = new PdfPCell(logoout2ph);
			logoout2.setBorderWidthBottom(0);
			logoout2.setBorderWidthRight(0);
			logoout2.setBorderWidthTop(0);
			headerTable.addCell(logoout2);
	
			Phrase 	headOut2ph=new Phrase("  ",font8);
			PdfPCell headOut2 = new PdfPCell(headOut2ph);
			headOut2.setBorderWidthBottom(0);
			headOut2.setBorderWidthLeft(0);
			headOut2.setBorderWidthTop(0);
			headerTable.addCell(headOut2);
		}

		try {
			document.add(headerTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
// End by jayshree
		
	}
	private PdfPCell getCell(String value , Font font , int alignment){
		Phrase phrase = new Phrase(value , font);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(alignment);
		cell.setBorder(0);
		return cell;		
	}
}
