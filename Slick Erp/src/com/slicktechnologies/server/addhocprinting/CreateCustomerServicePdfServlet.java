package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.Service;

public class CreateCustomerServicePdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2172600991685901617L;
	public static ArrayList<Service> serviceList = new ArrayList<Service>();
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {

		  
			CustomerServicePdf servicepdf = new CustomerServicePdf();
			servicepdf.document = new Document();
			Document document = servicepdf.document;
			PdfWriter.getInstance(document, response.getOutputStream()); 
			document.open();

			String stringid = request.getParameter("Id");
			

			if (stringid != null) {
				stringid = stringid.trim();
				Long count = Long.parseLong(stringid);
				System.out.println("ppppppppppppppppppooooooooooo" + count);

				servicepdf.setCustomerservice(count);
				servicepdf.createPdf();
				document.close();

			} else {
				System.out.println("ELSE PART :: "+serviceList.size());
				if(serviceList.size()!=0){
					for(Service service:serviceList){
						servicepdf.setCustomerservice(service.getId());
						servicepdf.createPdf();
						try {
							document.add(Chunk.NEXTPAGE);
						} catch (DocumentException e) {
							e.printStackTrace();
						}
					}
					document.close();
				}
				serviceList=new ArrayList<Service>();

			}
		   
	   
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }
	
	
	
	
	
}
