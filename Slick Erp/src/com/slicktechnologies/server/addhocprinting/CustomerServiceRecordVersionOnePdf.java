package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.PestTreatmentDetails;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class CustomerServiceRecordVersionOnePdf {
	
	
	Logger logger=Logger.getLogger("CustomerServiceRecordVersionOnePdf.class");
	public Document document;
	CompanyPayment comppayment;
	Company comp;
	Customer cust;
	Service service;
	List<Service> serviceList = new ArrayList<Service>();
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private Font font16boldul, font12bold, font8bold, font8, font12boldul,
	font12, font16bold, font10, font10bold, font14bold, font14, font9 ,font9bold,font13bold;
	Branch branchDt=null;
	boolean checkBranchdata = false;
	
	CustomerBranchDetails custbranch;
	private PdfPCell imageSignCell;
	boolean companyAsaletter=false;
	
	PdfUtility pdf=new PdfUtility();
	int maxRow=6; //3  // 10
	ServiceProject project;
	int maxRowForCheckList=5; //5
	int clCount;
	boolean printPestTreatmentFlag;
	boolean thaiFontFlag=false;
	
	//Branch branchDt = null;
	
	public CustomerServiceRecordVersionOnePdf(){
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 10);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font13bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL);
		font9 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font9bold=new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	}
	
	public void setpdfCustomerServRecord(Long count) {
		// loading service
		service = ofy().load().type(Service.class).id(count).now();

if (service.getCompanyId() != null) {
			
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")){
						thaiFontFlag=true;
						break;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font16boldul = new Font(boldFont, 17);
				font12bold = new Font(boldFont, 13);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font9 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 13);
				font12 = new Font(regularFont, 13);
				//font11bold = new Font(boldFont, 12);
				font10bold = new Font(boldFont, 10);
				//font7 = new Font(regularFont, 8, Font.NORMAL);
				font14bold = new Font(boldFont, 15);
				font13bold = new Font(boldFont, 13);
				font9bold = new Font(boldFont, 8);
				//font6bold = new Font(boldFont, 7);
				
				font16bold = new Font(boldFont, 16);
				//font11 = new Font(regularFont, 10);
				font10 = new Font(regularFont, 10);
				//font13 = new Font(regularFont, 9);
				//font7bold = new Font(boldFont, 7);
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
		// loading customer
		if (service.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.filter("companyId", service.getCompanyId()).first().now();

		// loading company
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", service.getCompanyId()).first().now();

		if (service.getCompanyId() != null) {

		}
		branchDt = ofy().load().type(Branch.class)
				.filter("companyId", service.getCompanyId())
				.filter("buisnessUnitName", service.getBranch()).first().now();
		
	
//        custbranch=ofy().load().type(CustomerBranchDetails.class)
//				.filter("companyId", comp.getCompanyId()).first().now();
        
		custbranch= ofy().load().type(CustomerBranchDetails.class)
				.filter("cinfo.count",service.getPersonInfo().getCount())
				.filter("companyId", service.getCompanyId()).first().now();
          
          
		if (service != null) {
			project = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId())
					.filter("contractId", service.getContractCount())
					.filter("serviceId", service.getCount()).first().now();
		}

		
		
//		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", service.getCompanyId()).filter("processName", "Service").filter("configStatus", true).list();
//		if(processConfigList!=null){
//			for(ProcessConfiguration processConf:processConfigList){
//				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintPhotoOnSRCopy")&&ptDetails.isStatus()==true){
//						printPhotoOnSRCopyFlag=true;
//					}
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintMaterialOnSRCopy")&&ptDetails.isStatus()==true){
//						printMaterialOnSRCopyFlag=true;
//					}
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintFindingsOnSRCopy")&&ptDetails.isStatus()==true){
//						printFindingsOnSRCopyFlag=true;
//					}
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintChecklistOnSRCopy")&&ptDetails.isStatus()==true){
//						printChecklistOnSRCopyFlag=true;
//					}
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintCustomerRemark")&&ptDetails.isStatus()==true){
//						printCustomerRemarkFlag=true;
//					}
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintTechnicianRemark")&&ptDetails.isStatus()==true){
//						printTechnicianRemarkFlag=true;
//					}
//					if(ptDetails.getProcessType().equalsIgnoreCase("PrintPestTreatmentOnSRCopy")&&ptDetails.isStatus()==true){
//						printPestTreatmentFlag=true;
//						printMaterialOnSRCopyFlag=false;
//					}
//				}
//			}
//		}
		
		
		
		if((service.getServiceImage1()==null||service.getServiceImage1().getUrl().equals(""))
				&&(service.getServiceImage2()==null||service.getServiceImage2().getUrl().equals(""))
				&&(service.getServiceImage3()==null||service.getServiceImage3().getUrl().equals(""))
				&&(service.getServiceImage4()==null||service.getServiceImage4().getUrl().equals(""))){
			
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(service !=null && service.getBranch() != null && service.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",service.getCompanyId()).filter("buisnessUnitName", service.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", service.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
	}
	
	public void createPdf() {
		logger.log(Level.SEVERE,"INSIDE CREATE PDF CUSTOMERSERICERECORD PDF");
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead", comp.getCompanyId())){
			companyAsaletter=true;
		}
		
		
		if(companyAsaletter){
			if (comp.getUploadHeader() != null) {
				createCompanyNameAsHeader(document, comp);
				blankCells();
			}
		}else{
			createHeader();
		}
		createTitle();
		createaAddress();
		
		try {
			document.add(createPestTreatmentTable());
		}catch(Exception e){
			e.printStackTrace();	
		}
		
		try {
			document.add(createMaterialTable());
		}catch(Exception e){
			e.printStackTrace();	
		}
		
		try {
			document.add(createPhotosTable());
		}catch(Exception e){
			e.printStackTrace();	
		}
		
		createFooterDetailTab();
		
	}
	
	private void createCompanyNameAsHeader(Document doc, Company comp2) {
		DocumentUpload document = comp.getUploadHeader();
		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

	private void blankCells() {
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
//			document.add(blank);
//			document.add(blank);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createCompanyNameAsFooter(Document doc, Company comp2) {
		DocumentUpload document = comp.getUploadFooter();
		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void createHeader() {
		

		
		
		PdfPTable headerTAB=new PdfPTable(2);
		headerTAB.setWidthPercentage(100);
		try {
			headerTAB.setWidths(new float[]{25,75});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable headertable=new PdfPTable(1);
		headertable.setWidthPercentage(100);
		headertable.setSpacingAfter(10);
		
		//Set Company Name
		String businessunit=null;
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			businessunit=branchDt.getCorrespondenceName();
		}else{
			businessunit =comp.getBusinessUnitName()+" "+"SERVICE REPORT";
		}
		
		
		
		Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
				font13bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
		companyNameCell.setBorderWidthBottom(0);
		companyNameCell.setBorderWidthLeft(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		headertable.addCell(companyNameCell);
		
		

		//Email ID
		String emailid="";
		if(checkBranchdata) {
			emailid = branchDt.getEmail();
		} else {
				emailid = comp.getEmail();
				System.out.println("server method " + emailid);
			} 
		
		Phrase companyemailph = new Phrase("E-Mail : "+emailid, font9);
		PdfPCell companyemailCell = new PdfPCell(companyemailph);
		companyemailCell.setBorderWidthBottom(0);
		companyemailCell.setBorderWidthTop(0);
		companyemailCell.setBorderWidthLeft(0);
		companyemailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		headertable.addCell(companyemailCell);
		
		
		//WebSite
		String website="";
		if(comp.getWebsite()==null || comp.getWebsite().equals(""))
		{
			website="";
		}
		else
		{
			
			website="Website : "+comp.getWebsite();
		}
		Phrase companyWebph = new Phrase(website, font8);
		PdfPCell companyWebCell = new PdfPCell(companyWebph);
		companyWebCell.setBorderWidthBottom(0);
		companyWebCell.setBorderWidthTop(0);
		companyWebCell.setBorderWidthLeft(0);
		companyWebCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		headertable.addCell(companyWebCell);
		
		
		
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setSpacingAfter(5);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{8,92});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		DocumentUpload logodocument ;
		if(checkBranchdata) {
			logodocument = branchDt.getLogo();
		} else {
			logodocument =comp.getLogo();
		}
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		if(logodocument!=null){
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setImage(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if(imageSignCell != null)
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		}
		else
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}

//		PdfPTable titletab = new PdfPTable(1);
//		titletab.setWidthPercentage(100f);
//		titletab.setSpacingAfter(5);
//		
//		Phrase title =new Phrase("",font9bold);
//		PdfPCell titlecell=new PdfPCell(title);
//		titlecell.setBorder(0);
//		titlecell.setPaddingRight(3);
//		titlecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		titletab.addCell(titlecell);
		
		
		PdfPCell logocell=new PdfPCell(logoTable);
		logocell.setBorderWidthRight(0);
		
		PdfPCell headerCell=new PdfPCell(headertable);
		headerCell.setBorderWidthLeft(0);
		headerCell.setBorderWidthRight(0);
		
//		PdfPCell titlcell=new PdfPCell(titletab);
//		titlcell.setBorderWidthLeft(0);
		
		headerTAB.addCell(logocell);
		headerTAB.addCell(headerCell);
//		headerTAB.addCell(titlcell);
		
		
		
		
		try {
			document.add(headerTAB);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

				
	}
	
    private void createTitle(){
    	PdfPTable headertab2=new PdfPTable(5);
		headertab2.setWidthPercentage(100);
		try {
			headertab2.setWidths(new float[]{14f,15f,49f,9f,13f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		Phrase custid1=new Phrase("Customer ID :",font10bold);
		PdfPCell custidcell1=new PdfPCell(custid1);
		custidcell1.setBorderWidthRight(0);
		custidcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		custidcell1.setPaddingBottom(5);
		headertab2.addCell(custidcell1);
		
		
		Phrase custid=new Phrase(service.getCustomerId()+"",font10);
		PdfPCell custidcell=new PdfPCell(custid);
		custidcell.setBorderWidthRight(0);
		custidcell.setBorderWidthLeft(0);
		custidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custidcell.setPaddingBottom(5);
		headertab2.addCell(custidcell);
		
		
		
		Phrase servRecord=new Phrase("SERVICE RECORD",font10bold);
		PdfPCell servRecordcell=new PdfPCell(servRecord);
		servRecordcell.setBorderWidthRight(0);
		servRecordcell.setBorderWidthLeft(0);
		servRecordcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servRecordcell.setPaddingBottom(5);
		headertab2.addCell(servRecordcell);
		
		Phrase date1=new Phrase("Date :",font10bold);
		PdfPCell datecell1=new PdfPCell(date1);
		datecell1.setBorderWidthLeft(0);
		datecell1.setBorderWidthRight(0);
		datecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		datecell1.setPaddingBottom(5);
		headertab2.addCell(datecell1);
		
		Phrase date=new Phrase(fmt.format(service.getServiceDate()),font10);
		PdfPCell datecell=new PdfPCell(date);
		datecell.setBorderWidthLeft(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		datecell.setPaddingBottom(5);
		headertab2.addCell(datecell);
		
		
		
		
		try {
			document.add(headertab2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
	private void createaAddress() {
		
		PdfPTable mainTable=new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable billingaddessTable=new PdfPTable(1);
		billingaddessTable.setWidthPercentage(100);
//		billingaddessTable.setSpacingAfter(5);
		
		
//		Phrase serviceph=new Phrase("This is to certify that "+service.getProductName()+" Service was provided at the premises of ",font8bold);
//		PdfPCell servicecell=new PdfPCell(serviceph);
//		servicecell.setBorder(0);
//		servicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		billingaddessTable.addCell(servicecell);
		
//		String custBranch="";
//		/**
//		 * @author Anil
//		 * @since 26-06-2020
//		 * No need to load customer branch entity for just printing customer branch name 
//		 * it is already stored in service entity
//		 */
////		if(custbranch!=null&&custbranch.getBranch()!=null){
////			custBranch=custbranch.getBranch();
////		}else{
////			custBranch="";
////		}
//		
//		if(service.getServiceBranch()!=null&&!service.getServiceBranch().equals("")&&!service.getServiceBranch().equals("Service Address")){
//			custBranch=service.getServiceBranch();
//		}
//		/**
//		 * @author Vijay Date :- 18-11-2020
//		 * Des :- Updated code if customer branch is blank then mapping customer name and
//		 * also added location to show premises details if exist
//		 */
//		boolean flag = false;
//		if(custBranch.equals("")) {
//			custBranch = service.getCustomerName();
//			flag = true;
//		}
		
//		Phrase custbranch=new Phrase("Customer Name : "+custBranch,font8);
//		PdfPCell custbranchcell=new PdfPCell(custbranch);
//		custbranchcell.setBorder(0);
////		custnamecell.setPaddingTop(8);
//		custbranchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		billingaddessTable.addCell(custbranchcell);
		
		
//		if(flag==false) {
		
		
		Phrase custname=new Phrase("Customer Name : "+service.getCustomerName(),font8);
		PdfPCell custnamecell=new PdfPCell(custname);
//		custnamecell.setBorder(0);
//		custnamecell.setPaddingTop(8);
		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(custnamecell);
//		}
		
		String custBr="";
		if(service.getServiceBranch()!=null&&!service.getServiceBranch().equals("")&&!service.getServiceBranch().equals("Service Address")){
			custBr=custbranch.getPocName();
			String[] nameArray = custBr.split("\\s+");
			if(nameArray.length>0){
				custBr=nameArray[0];
				}
		}else{
			custBr=cust.getFullname();
			String[] nameArray = custBr.split("\\s+");
			if(nameArray.length>0){
				custBr=nameArray[0];
				}
		}
		String output="";
		try{
			output = custBr.substring(0,1).toUpperCase() + custBr.substring(1).toLowerCase();
		}catch(Exception e){
			
		}
		
		
		
		Phrase custCellNo=new Phrase("Contact Person : "+output,font8);
		PdfPCell custCellNocell=new PdfPCell(custCellNo);
		custCellNocell.setBorderWidthBottom(0);
		custCellNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(custCellNocell);
		
		Long phoneNo;
		if(service.getServiceBranch()!=null&&!service.getServiceBranch().equals("")&&!service.getServiceBranch().equals("Service Address")){
			phoneNo=custbranch.getCellNumber1();
			
		}else{
			phoneNo=cust.getCellNumber1();
		}
		
		Phrase custCellNo1=new Phrase("Phone : "+phoneNo,font8);
		PdfPCell custCellNocell1=new PdfPCell(custCellNo1);
		custCellNocell1.setBorderWidthTop(0);
		custCellNocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(custCellNocell1);
		
		Phrase custadd=new Phrase("Address : "+service.getAddress().getCompleteAddress(),font8);
		PdfPCell custaddcell=new PdfPCell(custadd);
		//custaddcell.setBorder(0);
		custaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(custaddcell);
		
		
		
		
		
		
		PdfPTable serviceDetailTable=new PdfPTable(2);
		serviceDetailTable.setWidthPercentage(100);
//		serviceDetailTable.setSpacingAfter(5);
		try {
			serviceDetailTable.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase serviceId=new Phrase("Service Id : "+service.getCount(),font8);
		PdfPCell serviceIdcell=new PdfPCell(serviceId);
		serviceIdcell.setBorder(0);
		serviceIdcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceIdcell);
		
		Phrase serviceNo=new Phrase("Service No : "+service.getServiceSerialNo(),font8);
		PdfPCell serviceNocell=new PdfPCell(serviceNo);
		serviceNocell.setBorder(0);
		serviceNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceNocell);
		
		
		Phrase serviceBranch=new Phrase("Branch : "+service.getBranch(),font8);
		PdfPCell serviceBranchcell=new PdfPCell(serviceBranch);
		serviceBranchcell.setBorder(0);
		serviceBranchcell.setColspan(2);
		serviceBranchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceBranchcell);
		
		Phrase blank=new Phrase("",font8bold);
		PdfPCell blankcell=new PdfPCell(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		serviceDetailTable.addCell(blankcell);
		

		Phrase technicianName=new Phrase("Technician Name : "+service.getEmployee(),font8);
		PdfPCell technicianNamecell=new PdfPCell(technicianName);
		technicianNamecell.setBorder(0);
		technicianNamecell.setColspan(2);
		technicianNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(technicianNamecell);
		
		

		Phrase mobNo=new Phrase("Mobile No : "+service.getCellNumber(),font8);
		PdfPCell mobNocell=new PdfPCell(mobNo);
		mobNocell.setBorder(0);
		mobNocell.setColspan(2);
		mobNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(mobNocell);
		
//		String serviceDuration="";
		String serviceDuration=getServiceDuration();
//		logger.log(Level.SEVERE, "Serv Duration 1111 "+serviceDuration);
		Phrase serviceDurationPhrase=new Phrase("Service Duration : "+serviceDuration,font8);
		PdfPCell serviceDurationcell=new PdfPCell(serviceDurationPhrase);
		serviceDurationcell.setBorder(0);
		serviceDurationcell.setColspan(2);
		serviceDurationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceDurationcell);
		
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

		String serviceCompleteDate="";
		if(service.getCompletedDate_time()!=null){
//			serviceCompleteDate=service.getCompletedDate_time();
			try {
				Date completionDate = dateformat.parse(service.getCompletedDate_time());
				serviceCompleteDate = dateformat.format(completionDate);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}else{
			serviceCompleteDate="";
		}
		/**
		 * @author Vijay Chougule Date 02-10-2020 
		 * Des :- updated code time should be print from completion Time
		 * if you took time from date then getting issue time in other country for eg umas client
		 */
		String completedTime = service.getCompletedTime();
		logger.log(Level.SEVERE, "serviceCompleteDate "+serviceCompleteDate);
		logger.log(Level.SEVERE, "completionTime "+completedTime);

//		String completionTime=getServiceCompleteTime();
		logger.log(Level.SEVERE, "completionTime ");
		Phrase completionDate=new Phrase("Completion Date & Time : "+serviceCompleteDate+" "+completedTime,font8);
		PdfPCell completionDatecell=new PdfPCell(completionDate);
		completionDatecell.setBorder(0);
		completionDatecell.setColspan(2);
		completionDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(completionDatecell);
		
		
		
		PdfPCell addcell=new PdfPCell(billingaddessTable);
		addcell.setFixedHeight(90);
		PdfPCell prodcell=new PdfPCell(serviceDetailTable);
		prodcell.setFixedHeight(90);
		
		
		mainTable.addCell(addcell);
		mainTable.addCell(prodcell);
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	private String getServiceDuration() {
		String serviceDuration = "";
		SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date reportedTime = null;
		if (service.getReportedDate_time() != null) {
			logger.log(Level.SEVERE, "Reported Timee " + service.getReportedDate_time());
			try {
				logger.log(Level.SEVERE, "reportedTime in iff " +  serverTimeSdf.parse(service.getReportedDate_time()));
				reportedTime = serverTimeSdf.parse(service.getReportedDate_time());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "reportedTime in iff " + reportedTime);
		}
		else {
			reportedTime = null;
		}

		Date CompletedTime = null;
		if (service.gettCompletedDate_time() != null) {
			logger.log(Level.SEVERE, "Completion Time" + service.gettCompletedDate_time());
			try {
				logger.log(Level.SEVERE, "CompletedTime in ifff " +  serverTimeSdf.parse(service.gettCompletedDate_time()));
				CompletedTime = serverTimeSdf.parse(service
						.gettCompletedDate_time());
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			CompletedTime = null;
		}

		long timeDiff = 0;
		if (CompletedTime != null && reportedTime != null) {
			timeDiff = CompletedTime.getTime() - reportedTime.getTime();
			logger.log(Level.SEVERE, "timeDiff 111 " + timeDiff);
		} else {
			timeDiff = 0;
		}
		long diffSec = timeDiff / 1000;
		
	    long hrs = timeDiff / (60 * 60 * 1000);
	    long min = diffSec / 60;
	    long sec = diffSec % 60;
	    
	    String durMin="",durHrs="",durSec="";
	    
	    String hrsinString=String.valueOf(hrs);
	    int hrslength=hrsinString.length();
	    
	    if(hrslength==1){
	    	durHrs="0"+hrs;
	    }else{
	    	durHrs=hrs+"";
	    }
	    
	    String minInString=String.valueOf(min);
	    int minLength =minInString.length();
	    
	    if(minLength==1){
	    	durMin="0"+min;
	    }else{
	    	durMin=min+"";
	    }
	    
	    String secInString =String.valueOf(sec);
	    int seclength= secInString.length();
	    
	    if(seclength==1){
	    	durSec="0"+sec;
	    }else{
	    	durSec=sec+"";
	    }
	    
		serviceDuration = (durHrs+ ":"+durMin+ ":"+durSec);
		
		logger.log(Level.SEVERE, "Service Duration111 " + serviceDuration);
		return serviceDuration;

	}
	
  
    public PdfPTable createPestTreatmentTable(){
//    	servRecordcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		servRecordcell.setPaddingBottom(5);
    	PdfPTable tbl = new PdfPTable(4);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[]{25f,25f,25f,25f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		tbl.addCell(pdf.getCell("PEST TREATMENTS", font9bold, Element.ALIGN_CENTER, 0, 4, 0)).setPaddingBottom(5);
		
		tbl.addCell(pdf.getCell("Treatment Areas", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Method of application", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Pest Target", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Type of Service", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		HashMap<Integer,Vector<String>> dataMap=new HashMap<Integer,Vector<String>>();
		Integer row=1;
		
		if(service.getPestTreatmentList()!=null&&service.getPestTreatmentList().size()!=0){
			
			int totalCount=service.getPestTreatmentList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(pest.getTreatmentArea());
					vecLis.add(pest.getMethodOfApplication());
					vecLis.add(pest.getPestTarget());
					vecLis.add(service.getProductName());
					dataMap.put(row, vecLis);
					row++;
					
				}
				
				for(int i=0;i<remainingRow;i++){
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(" ");
					vecLis.add(" ");
					vecLis.add(" ");
					vecLis.add(" ");
					dataMap.put(row, vecLis);
					row++;
				}
			}else{
				for(int i=0;i<maxRow;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(pest.getTreatmentArea());
					vecLis.add(pest.getMethodOfApplication());
					vecLis.add(pest.getPestTarget());
					vecLis.add(service.getProductName());
					dataMap.put(row, vecLis);
					row++;
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				Vector<String> vecLis=new Vector<String>();
				vecLis.add(" ");
				vecLis.add(" ");
				vecLis.add(" ");
				vecLis.add(" ");
				dataMap.put(row, vecLis);
				row++;
			}
		}
		
		
//		row=1;
//		if(service.getServiceType()!=null&&service.getServiceType().equals("")){
//		//if(int i=row;i<maxRow;i++){
//		 dataMap.get(row).add(service.getServiceType());
//		 row++;
//		//}
//		}else{
//			for(int i=0;i<maxRow;i++){
//				dataMap.get(row).add(" ");
//				row++;
//			}
//		}
		
		
		for (Map.Entry<Integer,Vector<String>> entry : dataMap.entrySet()){  
			logger.log(Level.SEVERE,"Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
            Vector<String> list =entry.getValue();
            tbl.addCell(pdf.getCell(list.get(0), font8, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(1), font8, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(2), font8, Element.ALIGN_CENTER, 0, 0, 0));
			
			tbl.addCell(pdf.getCell(list.get(3), font8, Element.ALIGN_CENTER, 0, 0, 0));
			//tbl.addCell(pdf.getCell(list.get(4), font8, Element.ALIGN_RIGHT, 0, 0, 0));
		}
		return tbl;
    
    }
	
	
    public PdfPTable createMaterialTable(){
    	PdfPTable tbl = new PdfPTable(4);
		tbl.setWidthPercentage(100);
		
		try {
			tbl.setWidths(new float[]{25f,25f,25f,25f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		tbl.addCell(pdf.getCell("CHEMICAL USED IN TREATMANT", font9bold, Element.ALIGN_CENTER, 0, 4, 0)).setPaddingBottom(5);
		
		//tbl.addCell(pdf.getCell("Pest Target", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Chemical Use for Service", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Quantity Used", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Mixing ratio", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Lot. No.", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		
		if(project!=null&&project.getProdDetailsList()!=null&&project.getProdDetailsList().size()!=0){
			
			int totalCount=project.getProdDetailsList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			logger.log(Level.SEVERE,"createMaterialTable totalCount "+totalCount+" remainingRow "+remainingRow);
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					tbl.addCell(pdf.getCell(material.getName(), font8, Element.ALIGN_CENTER, 0, 0, 0));
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						tbl.addCell(pdf.getCell((material.getReturnQuantity())+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
					}else{
						tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
					}
//					tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					
					/**
					 * @author Anil @since 30-09-2021
					 * Adding Mixing no. and lot no. field for innovative raised by Rahul Tiwari
					 */
					String mixingNo="";
					String lotNo="";
					if(material.getMixingRatio()!=null){
						mixingNo=material.getMixingRatio();
					}
					if(material.getLotNo()!=null){
						lotNo=material.getLotNo();
					}
					
					tbl.addCell(pdf.getCell(mixingNo, font8, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdf.getCell(lotNo, font8, Element.ALIGN_CENTER, 0, 0, 0));
				}
				
				for(int i=0;i<remainingRow;i++){
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0));
				}
			}else{
				for(int i=0;i<maxRow;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					tbl.addCell(pdf.getCell(material.getName(), font8, Element.ALIGN_CENTER, 0, 0, 0));
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						tbl.addCell(pdf.getCell((material.getReturnQuantity())+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
					}else{
						tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
					}
//					tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					
					/**
					 * @author Anil @since 30-09-2021
					 * Adding Mixing no. and lot no. field for innovative raised by Rahul Tiwari
					 */
					String mixingNo="";
					String lotNo="";
					if(material.getMixingRatio()!=null){
						mixingNo=material.getMixingRatio();
					}
					if(material.getLotNo()!=null){
						lotNo=material.getLotNo();
					}
					
					tbl.addCell(pdf.getCell(mixingNo, font8, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdf.getCell(lotNo, font8, Element.ALIGN_CENTER, 0, 0, 0));
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0));
			}
		}
		
		return tbl;
    }
    
    
	
    public PdfPTable createPhotosTable(){
    	PdfPTable tbl = new PdfPTable(4);
		tbl.setWidthPercentage(100);
		
		tbl.addCell(pdf.getCell("PHOTOS", font9bold, Element.ALIGN_CENTER, 0, 4, 0)).setPaddingBottom(5);

		tbl.addCell(pdf.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
		tbl.addCell(pdf.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 2, 0));

		if(service.getServiceImage1()!=null&&!service.getServiceImage1().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage1(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		
		if(service.getServiceImage2()!=null&&!service.getServiceImage2().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage2(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		
		if(service.getServiceImage3()!=null&&!service.getServiceImage3().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage3(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		
		if(service.getServiceImage4()!=null&&!service.getServiceImage4().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage4(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		return tbl;
    }
	
    private void createFooterDetailTab() {
    	
    	
    	PdfPTable footerTab= new PdfPTable(2);
		footerTab.setWidthPercentage(100);
		try {
			footerTab.setWidths(new float[]{66.66f,33.33f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
    	
    	PdfPTable leftTab= new PdfPTable(1);
		leftTab.setWidthPercentage(100);
		
		/**
    	 * Technician and Customer Remark
    	 */
    	String techRemark =" ";
    	if(service.getTechnicianRemark()!=null&&!service.getTechnicianRemark().equals("")){
    		techRemark=service.getTechnicianRemark();
    	}else{
    		techRemark=" ";
    	}
		
		Phrase invoiceto = new Phrase("Technician Remark", font8bold);
		PdfPCell invoicetocell = new PdfPCell(invoiceto);
		invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicetocell.setBorderWidthBottom(0);
		leftTab.addCell(invoicetocell);
		
		Phrase invoiceto1 = new Phrase(techRemark, font8);
		PdfPCell invoicetocell1 = new PdfPCell(invoiceto1);
		invoicetocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicetocell1.setBorderWidthTop(0);
		leftTab.addCell(invoicetocell1);
		
		
		String custRemark =" ";
    	if(service.getServiceCompleteRemark()!=null&&!service.getServiceCompleteRemark().equals("")){
    		if(!custRemark.equals("")){
    			custRemark=service.getServiceCompleteRemark();
    		}else{
    			custRemark=service.getServiceCompleteRemark();
    		}
    	}else{
    		custRemark=" ";
    	}
    	
    	Phrase remark = new Phrase("Customer Remark ", font8bold);
		PdfPCell remarkcell= new PdfPCell(remark);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell.setBorderWidthBottom(0);
		leftTab.addCell(remarkcell);
		
		Phrase remark1 = new Phrase(custRemark, font8);
		PdfPCell remarkcell1 = new PdfPCell(remark1);
		remarkcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell1.setBorderWidthTop(0);//invoicetocell.setBorderWidthTop(0);
		leftTab.addCell(remarkcell1);
    	
    	
    	
    	
    	String custRating =" ";
    	if(service.getCustomerFeedback()!=null&&!service.getCustomerFeedback().equals("")){
    		custRating=service.getCustomerFeedback()+"\n";
    	}else{
    		custRating=" ";
    	}
    	
    	Phrase rating = new Phrase("Customer Rating ", font8bold);
		PdfPCell ratingcell = new PdfPCell(rating);
		ratingcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		ratingcell.setBorderWidthBottom(0);
		leftTab.addCell(ratingcell);
		
		Phrase rating1 = new Phrase(custRating, font8);
		PdfPCell ratingcell1 = new PdfPCell(rating1);
		ratingcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		ratingcell1.setBorderWidthTop(0);
		leftTab.addCell(ratingcell1);
    	
    	
    	
//    	
//    	PdfPCell techTopCell=pdf.getCell("Technician Remark", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
//		techTopCell.setBorderWidthBottom(0);
//		PdfPCell techBottomCell=pdf.getCell(techRemark,font8, Element.ALIGN_LEFT, 4, 0, 0);
//		techBottomCell.setBorderWidthTop(0);
////			techBottomCell.setRowspan(5);
//			techBottomCell.setRowspan(2);
//			
//			
//		
//		PdfPCell custTopCell=pdf.getCell("Customer Remark ", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
//		custTopCell.setBorderWidthBottom(0);
//		PdfPCell custBottomCell=pdf.getCell(custRemark, font8, Element.ALIGN_LEFT, 4, 0, 0);
//		custBottomCell.setBorderWidthTop(0);
//	
////			custBottomCell.setRowspan(5);
//			custBottomCell.setRowspan(2);
//			
//
//			PdfPCell custTopCellone=pdf.getCell("Customer Rating", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
//			custTopCell.setBorderWidthBottom(0);
//			PdfPCell custBottomCellone=pdf.getCell(custRating, font8, Element.ALIGN_LEFT, 4, 0, 0);
//			custBottomCell.setBorderWidthTop(0);
//		
////				custBottomCell.setRowspan(5);
//				custBottomCell.setRowspan(2);
//		
		
		
		PdfPTable rightTab= new PdfPTable(1);
		rightTab.setWidthPercentage(100);
	
		/**
		 * Signature Table
		 */
		
		String signPart="";	
		if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")&&service.getCustomerSignNumber()!=null&&!service.getCustomerSignNumber().equals("")){
			logger.log(Level.SEVERE,"INSIDE IF 1 "+service.getCustomerSignName());
			signPart="("+service.getCustomerSignName()+"/"+service.getCustomerSignNumber()+")";
		}else if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")){
			signPart="("+service.getCustomerSignName()+")";
		}else if(service.getCustomerSignNumber()!=null&&!service.getCustomerSignNumber().equals("")){
			signPart="("+service.getCustomerSignNumber()+")";
		}else{
			signPart="";
		}
		
		if(!signPart.equals("")){
			signPart=signPart+"\n";
		}
		
		Phrase auth=new Phrase(signPart+"Authorised Signatory",font8bold);
		PdfPCell authcell=new PdfPCell(auth);
		authcell.setBorderWidthTop(0);
//		authcell.setPaddingRight(40);
		authcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell imageSignCell = null;
		Image image2 = null;
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			if(service.getCustomerSignature()!=null&&!service.getCustomerSignature().getUrl().equals("")){
				try {
					image2 = Image.getInstance(new URL(hostUrl+ service.getCustomerSignature().getUrl()));
					logger.log(Level.SEVERE, "screen" + image2 + "");
				} catch (Exception e) {
					image2 = Image.getInstance(new URL(service.getCustomerSignature().getUrl()));
					logger.log(Level.SEVERE, "Android " + image2 + "");
				}
			}

			if(image2!=null){
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(100f);
				imageSignCell = new PdfPCell(image2);
			}else{
				imageSignCell = new PdfPCell(new Phrase(" "));
//				authcell.setPaddingTop(110);
//				authcell.setPaddingTop(70);
			}
			
			imageSignCell.setBorderWidthBottom(0);
//			imageSignCell.setPaddingTop(100);
			imageSignCell.setPaddingTop(50);
			imageSignCell.setPaddingRight(35);
//			imageSignCell.setPaddingBottom(20);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setBorderWidthLeft(0);
			imageSignCell.setBorderWidthRight(0);
			imageSignCell.setBorderWidthTop(0);
//			imageSignCell.setRowspan(11);
			//imageSignCell.setRowspan(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		rightTab.addCell(imageSignCell);
		rightTab.addCell(authcell);
				
		
		PdfPCell leftCell = new PdfPCell(leftTab);
		PdfPCell rightCell = new PdfPCell(rightTab);
		
		
		footerTab.addCell(leftCell);
		footerTab.addCell(rightCell);
	
	try {
		document.add(footerTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    }
    
    
 public void loadMultipleSRCopies(List<Service> serviceList){
		logger.log(Level.SEVERE,"Service list Size-->"+serviceList.size());
    	loadBasicData(serviceList.get(0).getCompanyId(),serviceList.get(0).getPersonInfo().getCount(),serviceList.get(0).getBranch(),serviceList.get(0));
    	for(Service service:serviceList){
    		this.service=service;
    		clCount=0;
    		maxRow=6; //3
    		maxRowForCheckList=5; //5
    		//photoFlag=true;
    		if (this.service != null) {
    			project = ofy().load().type(ServiceProject.class)
    					.filter("companyId", service.getCompanyId())
    					.filter("contractId", service.getContractCount())
    					.filter("serviceId", service.getCount()).first().now();
    		}
    		
    		if((service.getServiceImage1()==null||service.getServiceImage1().getUrl().equals(""))
    				&&(service.getServiceImage2()==null||service.getServiceImage2().getUrl().equals(""))
    				&&(service.getServiceImage3()==null||service.getServiceImage3().getUrl().equals(""))
    				&&(service.getServiceImage4()==null||service.getServiceImage4().getUrl().equals(""))){
    			//photoFlag=false;
    		}
    		
			
			createPdf();
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
		}
}

	private void loadBasicData(long companyId,int customerId,String branch ,Service service) {
		this.service=service;
		cust = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", companyId).first().now();
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		branchDt = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", branch).first().now();
		
		
		//service = ofy().load().type(Service.class).id(companyId).now();
		if (service.getCompanyId() != null) {
			
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")){
						thaiFontFlag=true;
						break;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font16boldul = new Font(boldFont, 17);
				font12bold = new Font(boldFont, 13);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font9 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 13);
				font12 = new Font(regularFont, 13);
				//font11bold = new Font(boldFont, 12);
				font10bold = new Font(boldFont, 10);
				//font7 = new Font(regularFont, 8, Font.NORMAL);
				font14bold = new Font(boldFont, 15);
				font13bold = new Font(boldFont, 13);
				font9bold = new Font(boldFont, 8);
				//font6bold = new Font(boldFont, 7);
				
				font16bold = new Font(boldFont, 16);
				//font11 = new Font(regularFont, 10);
				font10 = new Font(regularFont, 10);
				//font13 = new Font(regularFont, 9);
				//font7bold = new Font(boldFont, 7);
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(service !=null && service.getBranch() != null && service.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",service.getCompanyId()).filter("buisnessUnitName", service.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", service.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		
	
	}


}
