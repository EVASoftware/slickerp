package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CreateServiceInvoicePdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5819231415645836738L;

	/**
	 * Added by Rahul Verma Date : 23 Oct 2017
	 * 
	 */
	ProcessConfiguration processConfig;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		try {
			String stringid = request.getParameter("Id");
			String type = request.getParameter("type");//date 28/3/2018 by jayshree add new parameter
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			Invoice invoiceentity = ofy().load().type(Invoice.class).id(count)
					.now();
			boolean isMultipleBillingDocflag = loadMultipleBillingDoc(invoiceentity);
			
			//Ashwini Patil Date:02-05-2023
			String filename="Invoice - "+invoiceentity.getCount()+" - "+invoiceentity.getPersonInfo().getFullName()+" - "+fmt.format(invoiceentity.getInvoiceDate())+".pdf";
			if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", invoiceentity.getCompanyId()))
				response.setHeader("Content-Disposition", "attachment; filename=" + filename);
			System.out.println("filename="+filename);
			 
			if (isMultipleBillingDocflag) {
				boolean contractIdPresent=false;//add by jayshree
				try{
					System.out.println("contractIdPresent"+invoiceentity.getContractCount());
				if(invoiceentity.getContractCount()!=0){
					contractIdPresent=true;
				}else{
					contractIdPresent=false;
				}
				}catch(Exception e){
					contractIdPresent=false;
				}
				System.out.println("contractIdPresent:::"+contractIdPresent);
				if(contractIdPresent){
				
				System.out.println("SINGLE CONTRACT INVOICE IN Ayandev");
				// ServiceInvoicePdf invpdf=new ServiceInvoicePdf();
				ServiceGSTInvoiceForAyendev invpdf = new ServiceGSTInvoiceForAyendev();
				invpdf.document = new Document();
				Document document = invpdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,
						response.getOutputStream()); // write the pdf in
														// response

				if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
					writer.setPageEvent(new PdfCancelWatermark());
				}
				// ************rohan added this condition for removing draft
				// from proforma invoice

				else if (!invoiceentity.getStatus().equals("Approved")
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.PROFORMAINVOICE))
						&& (!invoiceentity.getStatus().equals(
								BillingDocument.BILLINGINVOICED))) {
					writer.setPageEvent(new PdfWatermark());
				}

				document.open();
				String preprintStatus = request.getParameter("preprint");
				System.out.println("ppppppppppppppppppooooooooooo" + count);
				invpdf.setInvoice(count);
				invpdf.createPdf(preprintStatus);
				document.close();
				}
				else{
					System.out.println("MultipleBilling"+type);
					if(type.equalsIgnoreCase("MultipleBilling")){
					MultipleBillingSingleInvoicePdf mulinvpdf = new MultipleBillingSingleInvoicePdf();
						mulinvpdf.document = new Document();
						Document document = mulinvpdf.document;
						PdfWriter writer = PdfWriter.getInstance(document,
								response.getOutputStream()); // write the pdf in
																// response

						if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
							writer.setPageEvent(new PdfCancelWatermark());
						}
						// ************rohan added this condition for removing
						// draft from proforma invoice

						else if (!invoiceentity.getStatus().equals("Approved")
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.PROFORMAINVOICE))
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.BILLINGINVOICED))) {
							writer.setPageEvent(new PdfWatermark());
						}

						document.open();
						String preprintStatus = request
								.getParameter("preprint");
						System.out.println("ppppppppppppppppppooooooooooo"
								+ count);
						mulinvpdf.setInvoice(count);
						mulinvpdf.createPdf(preprintStatus);
						document.close();
					}
					
				}
				
				

			} else {
				if (invoiceentity.isMultipleOrderBilling()) {

					
					
					System.out.println("MULTIPLE CONTRACT INVOICE");
					MultipleOrdersInvoicePdf invpdf = new MultipleOrdersInvoicePdf();
					invpdf.document = new Document();
					Document document = invpdf.document;
					PdfWriter writer = PdfWriter.getInstance(document,
							response.getOutputStream()); // write the pdf in
															// response

					if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
						writer.setPageEvent(new PdfCancelWatermark());
					} else if (!invoiceentity.getStatus().equals("Approved")) {
						writer.setPageEvent(new PdfWatermark());
					}

					document.open();
					String preprintStatus = request.getParameter("preprint");
					System.out.println("ppppppppppppppppppooooooooooo" + count);
					invpdf.setInvoice(count);
					invpdf.createPdf(preprintStatus);
					document.close();
					
					
					 
					
				} else {

					/**
					 * rohan added this condition to implement GST
					 */

					SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));

					boolean flag = false;
					try {
						flag = invoiceentity.getInvoiceDate().after(
								sdf.parse("30 Jun 2017"));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println("invoice Date "
							+ invoiceentity.getInvoiceDate());
					if (flag) {
						/**
						 * Date 28/3/2018
						 * By jayshree
						 * Des.to print multiple billing
						 */
						if(type.equalsIgnoreCase("SingleBilling")){
						
						System.out.println("SINGLE CONTRACT INVOICE");
						// ServiceInvoicePdf invpdf=new ServiceInvoicePdf();
						
						ServiceGSTInvoice invpdf = new ServiceGSTInvoice();
						invpdf.document = new Document();
						Document document = invpdf.document;
						PdfWriter writer = PdfWriter.getInstance(document,
								response.getOutputStream()); // write the pdf in
																// response

						if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
							writer.setPageEvent(new PdfCancelWatermark());
						}
						// ************rohan added this condition for removing
						// draft from proforma invoice

						else if (!invoiceentity.getStatus().equals("Approved")
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.PROFORMAINVOICE))
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.BILLINGINVOICED))) {
							writer.setPageEvent(new PdfWatermark());
						}

						document.open();
						String preprintStatus = request
								.getParameter("preprint");
						System.out.println("ppppppppppppppppppooooooooooo"
								+ count);
						invpdf.setInvoice(count);
						invpdf.createPdf(preprintStatus);
						document.close();
					}
						
					} else {
						System.out.println("SINGLE CONTRACT INVOICE");
						ServiceInvoicePdf invpdf = new ServiceInvoicePdf();
						invpdf.document = new Document();
						Document document = invpdf.document;
						PdfWriter writer = PdfWriter.getInstance(document,
								response.getOutputStream()); // write the pdf in
																// response

						if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
							writer.setPageEvent(new PdfCancelWatermark());
						}
						// ************rohan added this condition for removing
						// draft from proforma invoice

						else if (!invoiceentity.getStatus().equals("Approved")
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.PROFORMAINVOICE))
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.BILLINGINVOICED))) {
							writer.setPageEvent(new PdfWatermark());
						}

						document.open();
						String preprintStatus = request
								.getParameter("preprint");
						System.out.println("ppppppppppppppppppooooooooooo"
								+ count);
						invpdf.setInvoice(count);
						invpdf.createPdf(preprintStatus);
						document.close();
					}
				}
			}
		}

		catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private boolean loadMultipleBillingDoc(Invoice invoiceentity) {
		// TODO Auto-generated method stub

		HashSet<Integer> billCount = new HashSet<Integer>();
		for (BillingDocumentDetails billingDoc : invoiceentity.getArrayBillingDocument()) {
			billCount.add(billingDoc.getBillId());
		}
		if (billCount.size() > 1) {
			return true;
		}
		return false;
	}

//	private boolean loadProcessConfig(Invoice invoiceentity) {
//		// TODO Auto-generated method stub
//		boolean returnValue = false;
//		if (invoiceentity.getCompanyId() != null) {
//			processConfig = ofy().load().type(ProcessConfiguration.class)
//					.filter("companyId", invoiceentity.getCompanyId())
//					.filter("processName", "Invoice")
//					.filter("configStatus", true).first().now();
//			if (processConfig != null) {
//
//				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
//					if (processConfig.getProcessList().get(k).getProcessType()
//							.trim()
//							.equalsIgnoreCase("OnlyForIntegratedPestControl")
//							&& processConfig.getProcessList().get(k).isStatus() == true) {
//						returnValue = true;
//						break;
//					}
//				}
//			}
//		}
//
//		return returnValue;
//	}

}
