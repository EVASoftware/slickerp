package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;


public class ServiceComplaintPdf {


	public Document document;

	Company comp;
	Customer cust;
	Complain complain;
	Phrase chunk;
	PdfPCell pdfsrno1, pdfdate1, pdftime1, pdfuser1, pdfremark1;

	float[] colwidth = { 1.5f, 0.3f, 3.0f, 1.5f, 0.3f, 3.0f };
	float[] tblcolwidth = { 0.7f, 0.9f, 0.9f, 1.9f, 3.0f };

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	private PdfPCell custlandcell;

	public ServiceComplaintPdf() {

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}

	public void setComplain(Long count) {

		complain = ofy().load().type(Complain.class).id(count).now();

		if (complain.getCompanyId() != null) {

			comp = ofy().load().type(Company.class)
					.filter("companyId", complain.getCompanyId()).first().now();
		}

		if (complain.getCompanyId() != null) {

			cust = ofy().load().type(Customer.class)
					.filter("companyId", complain.getCompanyId())
					.filter("count", complain.getPersoninfo().getCount())
					.first().now();
		}
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf() {

//		System.out.print("inside Service complain create pdf method");
		createCompanyandCustHeading();
		createTicketHeading();
		createDescriptionHeading();
		createReferenceDetails();
		createServiceHeading();
		createSalesHeading();
		
		
		if(complain.getPic()!=null){
		
		createProductInfoHeading();
		createProductInformation();
		}
		
		
		if (complain.getComplainList() != null
				&& complain.getComplainList().size() != 0) {
			createComplainHistory();
			createComplainHistoryHeading();
		}
		System.out.println("Finished with Part 9");

	}

	public void createCompanyandCustHeading()

	{
		Phrase companyName = new Phrase("                  "
				+ comp.getBusinessUnitName(), font12boldul);
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);

		// System.out.print("Company paragraph has been created..");
		// System.out.print("Company paragraph consist of : "+ p);

		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(p);

		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),
				font10);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font10);
		}

		// **************************************************8888888888888888888888888888888888888888888888888888

		String complandmark1 = "";
		Phrase landmar1 = null;

		String localit1 = "";
		Phrase complocality1 = null;

		if (comp.getAddress().getLandmark() != null) {
			complandmark1 = comp.getAddress().getLandmark();
			landmar1 = new Phrase(complandmark1, font10);
		}

		if (comp.getAddress().getLocality() != null) {
			localit1 = comp.getAddress().getLocality();
			complocality1 = new Phrase(localit1, font10);
		}

		Phrase cityState = new Phrase(comp.getAddress().getCity() + " - "
				+ comp.getAddress().getPin(), font10);

		Phrase branch = null;
		if (cust.getBranch() != null) {

			branch = new Phrase("Branch : " + cust.getBranch(), font10);
		} else {
			branch = new Phrase(cust.getBranch());
		}

		PdfPCell branchcell = new PdfPCell();
		branchcell.addElement(branch);
		branchcell.setBorder(0);

		PdfPCell custlandcell = new PdfPCell();
		custlandcell.addElement(landmar1);
		custlandcell.setBorder(0);

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(complocality1);
		custlocalitycell.setBorder(0);

		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);

		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);
		if (adressline2 != null) {
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
		}

		// ************************************************************************************************************

		String contactinfo = "Mobile: " + comp.getContact().get(0).getCellNo1();
		if (comp.getContact().get(0).getCellNo2() != 0)
			contactinfo = contactinfo + ","
					+ comp.getContact().get(0).getCellNo2();
		if (comp.getContact().get(0).getLandline() != 0) {
			contactinfo = contactinfo + "     " + " Phone: 0"
					+ comp.getContact().get(0).getLandline();
		}

		// ************************************************************************************************************

		Phrase contactnos = new Phrase(contactinfo, font9);
		Phrase email = new Phrase("email: "
				+ comp.getContact().get(0).getEmail(), font9);

		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);

		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(addressline1cell);
		if (!comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!comp.getAddress().getLandmark().equals("")) {
			companytable.addCell(custlandcell);
		}
		if (!comp.getAddress().getLocality().equals("")) {
			companytable.addCell(custlocalitycell);
		}
		companytable.addCell(custcitycell);
		companytable.addCell(branchcell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);

		PdfPCell companycell = new PdfPCell();
		companycell.addElement(companytable);

		// *************************************************************1111111111111111111111111111111111111111111111111

		Phrase blankPhase = new Phrase("", font9);
		PdfPCell blankcell = new PdfPCell(blankPhase);
		blankcell.setBorder(0);

		if (comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(blankcell);
		}
		if (comp.getAddress().getLandmark().equals("")) {
			companytable.addCell(blankcell);
		}
		if (comp.getAddress().getLocality().equals("")) {
			companytable.addCell(blankcell);
		}

		// ***********************************************************22222222222222222222222222222222222222222222222

		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);

		if (comp.getContact() != null && comp.getCompanyId() != null) {
			Phrase realcontact = new Phrase("Contact Person: "
					+ comp.getPocName(), font9);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			companytable.addCell(realcontactcell);
		}

		/****************************************** Customer information ****************************************/

		
		String custName123="";
		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
			custName123=cust.getCompanyName().trim();
		}
		else{
			custName123=cust.getFullname().trim();
		}
		
		
		
		Phrase custName = new Phrase("To" + ",  " + custName123,
				font12boldul);
		Paragraph p1 = new Paragraph();
		p1.add(Chunk.NEWLINE);
		p1.add(custName);

		PdfPCell custheadingcell = new PdfPCell();
		custheadingcell.setBorder(0);
		custheadingcell.addElement(p1);

		Phrase addressline1 = new Phrase(cust.getAdress().getAddrLine1(),
				font10);
		Paragraph p2 = new Paragraph();
		p2.add(addressline1);
		p2.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custaddressline1cell = new PdfPCell();
		custaddressline1cell.addElement(p2);
		custaddressline1cell.setBorder(0);

		Phrase addressline2 = null;

		if (cust.getAdress().getAddrLine2() != null) {

			addressline2 = new Phrase(comp.getAddress().getAddrLine2(), font10);

		}

		Paragraph p3 = new Paragraph();
		p3.add(addressline2);
		p3.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custaddressline2cell = new PdfPCell();
		custaddressline2cell.addElement(p3);
		custaddressline2cell.setBorder(0);

		Phrase custlandmark = null;

		if (cust.getAdress().getLandmark() != null) {

			custlandmark = new Phrase(cust.getAdress().getLandmark(), font10);
		}

		Paragraph custlandmarkpara = new Paragraph();
		custlandmarkpara.add(custlandmark);
		custlandmarkpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custlandmarkcell = new PdfPCell();
		custlandmarkcell.setBorder(0);
		custlandmarkcell.addElement(custlandmarkpara);

		Phrase custlocality = null;

		if (cust.getAdress().getLocality() != null) {

			custlocality = new Phrase(cust.getAdress().getLocality(), font10);
		}

		Paragraph custlocalitypara = new Paragraph();
		custlocalitypara.add(custlocality);
		custlocalitypara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custlocalitycell1 = new PdfPCell();
		custlocalitycell1.setBorder(0);
		custlocalitycell1.addElement(custlocalitypara);

		Phrase custcity = null;

		if (cust.getAdress().getCity() + " - " + comp.getAddress().getPin() != null) {

			custcity = new Phrase(cust.getAdress().getCity() + " - "
					+ comp.getAddress().getPin(), font10);
		}

		Paragraph custcitypara = new Paragraph();
		custcitypara.add(custcity);
		custcitypara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custcitycell1 = new PdfPCell();
		custcitycell1.setBorder(0);
		custcitycell1.addElement(custcitypara);

		Phrase custstate = null;

		if (cust.getAdress().getState() != null) {

			custstate = new Phrase(cust.getAdress().getState(), font10);
		}

		Paragraph custstatepara = new Paragraph();
		custstatepara.add(custstate);
		custstatepara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custstatecell = new PdfPCell();
		custstatecell.setBorder(0);
		custstatecell.addElement(custstatepara);

		Phrase custcountry = null;

		if (cust.getAdress().getCountry() != null) {

			custcountry = new Phrase(cust.getAdress().getCountry(), font10);
		}

		Paragraph custcountrypara = new Paragraph();
		custcountrypara.add(custcountry);
		custcountrypara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custcountrycell = new PdfPCell();
		custcountrycell.setBorder(0);
		custcountrycell.addElement(custcountrypara);

		// **************************************************************************************************************

		String contactinfo1 = "Mobile: "
				+ cust.getContact().get(0).getCellNo1();
		if (comp.getContact().get(0).getCellNo2() != 0)
			contactinfo1 = contactinfo1 + ","
					+ cust.getContact().get(0).getCellNo2();
		if (comp.getContact().get(0).getLandline() != 0) {
			contactinfo1 = contactinfo1 + "     " + " Phone: 0"
					+ cust.getContact().get(0).getLandline();
		}

		Phrase contactnos1 = new Phrase(contactinfo, font9);
		Paragraph p4 = new Paragraph();
		p4.add(contactnos1);

		PdfPCell contact1cell = new PdfPCell();
		contact1cell.addElement(p4);
		contact1cell.setBorder(0);

		Phrase custemail = new Phrase("email: "
				+ comp.getContact().get(0).getEmail(), font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.setBorder(0);
		custemailcell.addElement(custemail);

		PdfPTable customertable = new PdfPTable(1);
		customertable.addCell(custheadingcell);
		customertable.addCell(custaddressline1cell);

		if (!comp.getAddress().getAddrLine2().equals("")) {
			customertable.addCell(custaddressline2cell);
		}
		if (!comp.getAddress().getLandmark().equals("")) {
			customertable.addCell(custlandmarkcell);
		}
		if (!comp.getAddress().getLocality().equals("")) {
			customertable.addCell(custlocalitycell1);
		}

		customertable.addCell(custcitycell1);
		customertable.addCell(custstatecell);
		customertable.addCell(custcountrycell);

		customertable.addCell(contact1cell);

		// customertable.addCell(custmobile2cell);
		// customertable.addCell(landlinecell);

		customertable.addCell(custemailcell);

		customertable.setHorizontalAlignment(Element.ALIGN_RIGHT);
		customertable.setWidthPercentage(100);

//		if (cust.getContact() != null && comp.getCompanyId() != null) {
//			Phrase realcontact = new Phrase("Contact Person: "
//					+ comp.getPocName(), font9);
//			realcontact.add(Chunk.NEWLINE);
//			PdfPCell realcontactcell = new PdfPCell();
//			realcontactcell.addElement(realcontact);
//			realcontactcell.setBorder(0);
//			customertable.addCell(realcontactcell);

			PdfPCell customercell = new PdfPCell();
			customercell.addElement(customertable);

			PdfPTable compandcusttable = new PdfPTable(2);
			compandcusttable.setWidthPercentage(100);
			compandcusttable.addCell(companycell);
			compandcusttable.addCell(customercell);

			try {
				document.add(compandcusttable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// ***************************************************************************************************************

//		}

	}

	public void createTicketHeading() {

		Phrase ticketid = new Phrase("Ticket Id : " + complain.getCount(),font12);
		Paragraph p5 = new Paragraph();
		p5.add(ticketid);

		PdfPCell ticketidcell = new PdfPCell();
		ticketidcell.addElement(p5);
		ticketidcell.setBorder(0);

		Phrase custsupport = new Phrase("Customer Support",font12);
		Paragraph p6 = new Paragraph();
		p6.add(custsupport);
		p6.setAlignment(Element.ALIGN_CENTER);

		PdfPCell custsupportcell = new PdfPCell();
		custsupportcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		custsupportcell.setBorder(0);
		custsupportcell.addElement(p6);

		Phrase ticketdate = new Phrase("Ticket Date : "
				+ fmt.format(complain.getDate()),font12);
		Paragraph p7 = new Paragraph();
		p7.add(ticketdate);
		p7.setAlignment(Element.ALIGN_RIGHT);

		PdfPCell ticketdatecell = new PdfPCell();
		ticketdatecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		ticketdatecell.setBorder(0);
		ticketdatecell.addElement(p7);

		PdfPTable titletable = new PdfPTable(3);
		titletable.setWidthPercentage(100f);

		titletable.addCell(ticketidcell);
		titletable.addCell(custsupportcell);
		titletable.addCell(ticketdatecell);

		PdfPCell title1cell = new PdfPCell();
		title1cell.addElement(titletable);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.setWidthPercentage(100f);
		parenttable.addCell(title1cell);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// ************************************************************00000000000000000000000000000000000000000

	public void createReferenceDetails() {

		Phrase referencedetails = new Phrase("Reference Details", font12bold);
		Paragraph p8 = new Paragraph();
		p8.add(referencedetails);

		PdfPCell referencedetailscell = new PdfPCell();
		referencedetailscell.addElement(p8);
		referencedetailscell.setBorder(0);
		referencedetailscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable referencetable = new PdfPTable(1);
		referencetable.addCell(referencedetailscell);
		referencetable.setWidthPercentage(100f);

		PdfPCell reference1cell = new PdfPCell();
		reference1cell.addElement(referencetable);

		PdfPTable parent1table = new PdfPTable(1);
		parent1table.setWidthPercentage(100);
		parent1table.addCell(reference1cell);

		try {
			document.add(parent1table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	// ***********************************************************00000000000000000000000000000000000000000000

	public void createServiceHeading() {

		// ******************************************For Static
		// Entity***********************************************

		Phrase ecid = new Phrase("Contract Id", font9);
		Paragraph p9 = new Paragraph();
		p9.add(ecid);
		p9.setAlignment(Element.ALIGN_LEFT);

		PdfPCell ecidcell = new PdfPCell();
		ecidcell.addElement(p9);
		ecidcell.setBorder(0);
		ecidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase esid = new Phrase("Service Id", font9);
		Paragraph p10 = new Paragraph();
		p10.add(esid);
		p10.setAlignment(Element.ALIGN_LEFT);

		PdfPCell esidcell = new PdfPCell();
		esidcell.addElement(p10);
		esidcell.setBorder(0);
		esidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase servengineer = new Phrase("Service Engineer", font9);
		Paragraph p11 = new Paragraph();
		p11.add(servengineer);
		p11.setAlignment(Element.ALIGN_LEFT);

		PdfPCell servengineercell = new PdfPCell();
		servengineercell.addElement(p11);
		servengineercell.setBorder(0);
		servengineercell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase qid = new Phrase("Quotation Id", font9);
		Paragraph p12 = new Paragraph();
		p12.add(qid);
		p12.setAlignment(Element.ALIGN_LEFT);

		PdfPCell qidcell = new PdfPCell();
		qidcell.addElement(p12);
		qidcell.setBorder(0);
		qidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase cid = new Phrase("Contract Id", font9);
//		Paragraph p13 = new Paragraph();
//		p13.add(cid);
//		p13.setAlignment(Element.ALIGN_LEFT);
//
//		PdfPCell cidcell = new PdfPCell();
//		cidcell.addElement(p13);
//		cidcell.setBorder(0);
//		cidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase sid = new Phrase("Service Id", font9);
//		Paragraph p14 = new Paragraph();
//		p14.add(sid);
//		p14.setAlignment(Element.ALIGN_LEFT);
//
//		PdfPCell sidcell = new PdfPCell();
//		sidcell.addElement(p14);
//		sidcell.setBorder(0);
//		sidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase poc = new Phrase("POC Name",font9);
		PdfPCell poccell = new PdfPCell();
		poccell.addElement(poc);
		poccell.setBorder(0);
		poccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase pemail = new Phrase("Email Id",font9);
		PdfPCell pemailcell = new PdfPCell();
		pemailcell.addElement(pemail);
		pemailcell.setBorder(0);
		pemailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase pcel = new Phrase("Mobile",font9);
		PdfPCell pcelcell = new PdfPCell();
		pcelcell.addElement(pcel);
		pcelcell.setBorder(0);
		pcelcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blank1 = new Phrase();
		PdfPCell blank1cell = new PdfPCell();
		blank1cell.addElement(blank1);
		blank1cell.setBorder(0);
		
		
//		******************************************For Dynamic
//		 Entity***********************************************

		Phrase conid = null;

		if (complain.getExistingContractId() != null) {

			conid = new Phrase("" + complain.getExistingContractId(),
					font10);

		}

		Paragraph conidpara = new Paragraph();
		conidpara.add(conid);
		conidpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell conidcell = new PdfPCell();
		conidcell.addElement(conidpara);
		conidcell.setBorder(0);
		conidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase servid1 = null;

		if (complain.getExistingServiceId() != null) {

			servid1 = new Phrase("" + complain.getExistingServiceId(),
					font10);

		}

		Paragraph servid1para = new Paragraph();
		servid1para.add(servid1);
		servid1para.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell servidcell = new PdfPCell();
		servidcell.addElement(servid1para);
		servidcell.setBorder(0);
		servidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase serveng = null;

		if (complain.getServiceEngForExistingService() != null) {

			serveng = new Phrase(""
					+ complain.getServiceEngForExistingService(), font10);

		}

		Paragraph servengpara = new Paragraph();
		servengpara.add(serveng);
		servengpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell servengcell = new PdfPCell();
		servengcell.addElement(servengpara);
		servengcell.setBorder(0);
		servengcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase quotationid = null;

		if (complain.getQuatationId() != null) {

			quotationid = new Phrase("" + complain.getQuatationId(), font10);

		}

		Paragraph quotationidpara = new Paragraph();
		quotationidpara.add(quotationid);
		quotationidpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell quotationidcell = new PdfPCell();
		quotationidcell.addElement(quotationidpara);
		quotationidcell.setBorder(0);
		quotationidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase contractid = null;
//
//		if (complain.getContrtactId() != null) {
//
//			contractid = new Phrase("" + complain.getContrtactId(), font10);
//
//		}
//
//		Paragraph contractidpara = new Paragraph();
//		contractidpara.add(contractid);
//
//		PdfPCell contractidcell = new PdfPCell();
//		contractidcell.addElement(contractidpara);
//		contractidcell.setBorder(0);
//		contractidcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		Phrase serviceid = null;
//
//		if (complain.getServiceId() != null) {
//
//			serviceid = new Phrase("" + complain.getServiceId(), font10);
//
//		}
//		Paragraph serviceidpara = new Paragraph();
//		serviceidpara.add(serviceid);
//
//		PdfPCell serviceidcell = new PdfPCell();
//		serviceidcell.addElement(serviceidpara);
//		serviceidcell.setBorder(0);
//		serviceidcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		
		Phrase p1 = null;
		
		if(complain.getPersoninfo().getPocName() !=null){
			
			p1 = new Phrase("" + complain.getPersoninfo().getPocName(),font9);
			
		}
		
		PdfPCell p1cell = new PdfPCell();
		p1cell.addElement(p1);
		p1cell.setBorder(0);
		p1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase e12 = null;

		if(complain.getPersoninfo().getEmail() !=null ){
			
			e12 = new Phrase("" + complain.getPersoninfo().getEmail(),font9);
			
		}
		
		PdfPCell e12cell = new PdfPCell();
		e12cell.addElement(e12);
		e12cell.setBorder(0);
		e12cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase c1 = null;
		
		if(complain.getPersoninfo().getCellNumber() !=null){
			
			c1 = new Phrase("" + complain.getPersoninfo().getCellNumber(),font9);	
			
		}
		
		PdfPCell c1cell = new PdfPCell();
		c1cell.addElement(c1);
		c1cell.setBorder(0);
		c1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase bl1 = new Phrase();
		PdfPCell bl1PCell = new PdfPCell();
		bl1PCell.addElement(bl1);
		bl1PCell.setBorder(0);
		
		
		
		// **************************************************777777777777777777777777777777777777777777777777777777777777777

		Phrase colon = new Phrase(":");
		Paragraph colpara = new Paragraph();
		colpara.add(colon);
		colpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell colcell = new PdfPCell();
		colcell.addElement(colpara);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***************************************************7777777777777777777777777777777777777777777777777777777777777

		
		PdfPTable idtable = new PdfPTable(6);
		try {
			idtable.setWidths(colwidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		idtable.setWidthPercentage(100f);

		idtable.addCell(ecidcell);
		idtable.addCell(colcell);
		idtable.addCell(conidcell);

		idtable.addCell(esidcell);
		idtable.addCell(colcell);
		idtable.addCell(servidcell);

		idtable.addCell(servengineercell);
		idtable.addCell(colcell);
		idtable.addCell(servengcell);

		idtable.addCell(qidcell);
		idtable.addCell(colcell);
		idtable.addCell(quotationidcell);
		
		idtable.addCell(poccell);
		idtable.addCell(colcell);
		idtable.addCell(p1cell);
		
		idtable.addCell(pemailcell);
		idtable.addCell(colcell);
		idtable.addCell(e12cell);
		
		idtable.addCell(pcelcell);
		idtable.addCell(colcell);
		idtable.addCell(c1cell);
		
		idtable.addCell(blank1cell);
		idtable.addCell(blank1cell);
		idtable.addCell(blank1cell);
		

//		idtable.addCell(cidcell);
//		idtable.addCell(colcell);
//		idtable.addCell(contractidcell);
//
//		idtable.addCell(sidcell);
//		idtable.addCell(colcell);
//		idtable.addCell(serviceidcell);

		idtable.setWidthPercentage(100f);

		PdfPCell idcell = new PdfPCell();
		idcell.addElement(idtable);

		PdfPTable parent2table = new PdfPTable(1);
		parent2table.addCell(idcell);
		parent2table.setWidthPercentage(100f);

		try {
			document.add(parent2table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	public void createSalesHeading() {

		// ******************************************For static
		// entity2***********************************

		Phrase salesperson = new Phrase("Sales Person", font9);
		Paragraph salespersonpara = new Paragraph();
		salespersonpara.add(salesperson);
		salespersonpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell salespersoncell = new PdfPCell();
		salespersoncell.addElement(salespersonpara);
		salespersoncell.setBorder(0);
		salespersoncell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase branch = new Phrase("Branch", font9);
		Paragraph branchpara = new Paragraph();
		branchpara.add(branch);
		branchpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell branchcell = new PdfPCell();
		branchcell.addElement(branchpara);
		branchcell.setBorder(0);
		branchcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase assignto = new Phrase("Assigned To", font9);
		Paragraph assigntopara = new Paragraph();
		assigntopara.add(assignto);
		assigntopara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell assigntocell = new PdfPCell();
		assigntocell.addElement(assigntopara);
		assigntocell.setBorder(0);
		assigntocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase duedate = new Phrase("Due Date", font9);
		Paragraph duedatepara = new Paragraph();
		duedatepara.add(duedate);
		duedatepara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell duedatecell = new PdfPCell();
		duedatecell.addElement(duedatepara);
		duedatecell.setBorder(0);
		duedatecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase priority = new Phrase("Priority", font9);
		Paragraph prioritypara = new Paragraph();
		prioritypara.add(priority);
		prioritypara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell prioritycell = new PdfPCell();
		prioritycell.addElement(prioritypara);
		prioritycell.setBorder(0);
		prioritycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase status = new Phrase("Status", font9);
		Paragraph statuspara = new Paragraph();
		statuspara.add(status);
		statuspara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell statuscell = new PdfPCell();
		statuscell.addElement(statuspara);
		statuscell.setBorder(0);
		statuscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase billable = new Phrase("Billable", font9);
		Paragraph bilpara = new Paragraph();
		bilpara.add(billable);
		bilpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell billablecell = new PdfPCell();
		billablecell.addElement(bilpara);
		billablecell.setBorder(0);
		billablecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank = new Phrase("");
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ******************************************************************

		Phrase colon1 = new Phrase(":");
		Paragraph colon1para = new Paragraph();
		colon1para.add(colon1);
		colon1para.setAlignment(Element.ALIGN_LEFT);

		PdfPCell colon1cell = new PdfPCell();
		colon1cell.addElement(colon1para);
		colon1cell.setBorder(0);
		colon1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ******************************************************************

		// *********************************************For Dynamic
		// Entity2************************************************

		Phrase sp = null;

		if (complain.getSalesPerson() != null) {

			sp = new Phrase("" + complain.getSalesPerson(), font9);
		}

		Paragraph p15 = new Paragraph();
		p15.add(sp);
		p15.setAlignment(Element.ALIGN_LEFT);

		PdfPCell spcell = new PdfPCell();
		spcell.addElement(p15);
		spcell.setBorder(0);
		spcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase br = null;

		if (complain.getBranch() != null) {

			br = new Phrase("" + complain.getBranch(), font9);

		}

		Paragraph p16 = new Paragraph();
		p16.add(br);
		p16.setAlignment(Element.ALIGN_LEFT);

		PdfPCell brcell = new PdfPCell();
		brcell.addElement(p16);
		brcell.setBorder(0);
		brcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase ass = null;

		if (complain.getAssignto() != null) {

			ass = new Phrase("" + complain.getAssignto(), font9);

		}

		Paragraph p17 = new Paragraph();
		p17.add(ass);
		p17.setAlignment(Element.ALIGN_LEFT);

		PdfPCell asscell = new PdfPCell();
		asscell.addElement(p17);
		asscell.setBorder(0);
		asscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dd = null;

		if (complain.getDueDate() != null) {

			dd = new Phrase("" + fmt.format(complain.getDueDate()), font9);

		}

		Paragraph p18 = new Paragraph();
		p18.add(dd);
		p18.setAlignment(Element.ALIGN_LEFT);

		PdfPCell ddcell = new PdfPCell();
		ddcell.addElement(p18);
		ddcell.setBorder(0);
		ddcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase pri = null;

		if (complain.getPriority() != null) {

			pri = new Phrase("" + complain.getPriority(), font9);

		}

		PdfPCell pricell = new PdfPCell();
		pricell.addElement(pri);
		pricell.setBorder(0);
		pricell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase st = null;

		if (complain.getCompStatus() != null) {

			st = new Phrase("" + complain.getCompStatus(), font9);

		}

		PdfPCell stcell = new PdfPCell();
		stcell.addElement(st);
		stcell.setBorder(0);
		stcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bill = null;

		if (complain.getBbillable() == true) {

			bill = new Phrase("Yes", font9);

		} else {
			bill = new Phrase("No", font9);
		}

		PdfPCell billcell = new PdfPCell();
		billcell.addElement(bill);
		billcell.setBorder(0);
		billcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl = new Phrase("");
		PdfPCell blcell = new PdfPCell();
		blcell.addElement(bl);
		blcell.setBorder(0);
		blankcell.setHorizontalAlignment(0);

		PdfPTable sptable = new PdfPTable(6);

		try {
			sptable.setWidths(colwidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		sptable.addCell(salespersoncell);
		sptable.addCell(colon1cell);
		sptable.addCell(spcell);

		sptable.addCell(branchcell);
		sptable.addCell(colon1cell);
		sptable.addCell(brcell);

		sptable.addCell(assigntocell);
		sptable.addCell(colon1cell);
		sptable.addCell(asscell);

		sptable.addCell(duedatecell);
		sptable.addCell(colon1cell);
		sptable.addCell(ddcell);

		sptable.addCell(prioritycell);
		sptable.addCell(colon1cell);
		sptable.addCell(pricell);

		sptable.addCell(statuscell);
		sptable.addCell(colon1cell);
		sptable.addCell(stcell);

		sptable.addCell(billablecell);
		sptable.addCell(colon1cell);
		sptable.addCell(billcell);

		sptable.addCell(blankcell);
		sptable.addCell(blankcell);
		sptable.addCell(blankcell);

		sptable.setWidthPercentage(100f);

		PdfPCell sp1cell = new PdfPCell();
		sp1cell.addElement(sptable);

		PdfPTable parent3table = new PdfPTable(1);
		parent3table.addCell(sp1cell);
		parent3table.setWidthPercentage(100);

		try {
			document.add(parent3table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createProductInfoHeading() {

		if(complain.getPic()!= null)
		{	
		Phrase Productinfo = new Phrase("Product Information", font12bold);
		Paragraph pipara = new Paragraph();
		pipara.add(Productinfo);
		pipara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell picell = new PdfPCell();
		picell.addElement(pipara);
		picell.setBorder(0);
		picell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable pitable = new PdfPTable(1);
		pitable.addCell(picell);
		pitable.setWidthPercentage(100f);

		PdfPCell pi1cell = new PdfPCell();
		pi1cell.addElement(pitable);

		PdfPTable parent4table = new PdfPTable(1);
		parent4table.addCell(pi1cell);
		parent4table.setWidthPercentage(100);

		try {
			document.add(parent4table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

	public void createProductInformation() {

		// ******************************************For static
		// entity2******************************************

		Phrase prodid = new Phrase("Product Id", font9);
		Paragraph prodidpara = new Paragraph();
		prodidpara.add(prodid);
		prodidpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell prodidcell = new PdfPCell();
		prodidcell.addElement(prodidpara);
		prodidcell.setBorder(0);
		prodidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase prodname = new Phrase("Product Name", font9);
		Paragraph prodnamepara = new Paragraph();
		prodnamepara.add(prodname);
		prodnamepara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell prodnamecell = new PdfPCell();
		prodnamecell.addElement(prodnamepara);
		prodnamecell.setBorder(0);
		prodnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase prodcode = new Phrase("Product Code", font9);
		// Paragraph prodcodepara = new Paragraph();
		// prodcodepara.add(prodcode);
		// prodcodepara.setAlignment(Element.ALIGN_LEFT);
		//
		// PdfPCell prodcodecell = new PdfPCell();
		// prodcodecell.addElement(prodcodepara);
		// prodcodecell.setBorder(0);
		// prodcodecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ********************************************************************

		Phrase colon2 = new Phrase(":");
		Paragraph colon2para = new Paragraph();
		colon2para.add(colon2);
		colon2para.setAlignment(Element.ALIGN_LEFT);

		PdfPCell colon2cell = new PdfPCell();
		colon2cell.addElement(colon2para);
		colon2cell.setBorder(0);
		colon2cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ********************************************************************

		// ***************************************For dynamic
		// entity2*************************************************

		Phrase pid = null;

		if (complain.getPic() != null) {

			pid = new Phrase("" + complain.getPic().getProdID(), font9);

		}

		Paragraph pidpara = new Paragraph();
		pidpara.add(pid);
		pidpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell pidcell = new PdfPCell();
		pidcell.addElement(pidpara);
		pidcell.setBorder(0);
		pidcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase pn = null;

		if (complain.getPic() != null) {

			pn = new Phrase("" + complain.getPic().getProductName(), font9);

		}

		Paragraph pnpara = new Paragraph();
		pnpara.add(pn);
		pnpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell pncell = new PdfPCell();
		pncell.addElement(pnpara);
		pncell.setBorder(0);
		pncell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase pc = null;
		//
		// if (complain.getPic().getProductCode() != null) {
		//
		// pc = new Phrase("" + complain.getPic().getProductCode(), font9);
		//
		// }
		//
		// Paragraph pcpara = new Paragraph();
		// pcpara.add(pc);
		// pcpara.setAlignment(Element.ALIGN_LEFT);
		//
		// PdfPCell pccell = new PdfPCell();
		// pccell.addElement(pcpara);
		// pccell.setBorder(0);
		// pccell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable pi1table = new PdfPTable(6);

		try {
			pi1table.setWidths(colwidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		pi1table.addCell(prodidcell);
		pi1table.addCell(colon2cell);
		pi1table.addCell(pidcell);

		pi1table.addCell(prodnamecell);
		pi1table.addCell(colon2cell);
		pi1table.addCell(pncell);

		// pi1table.addCell(prodcodecell);
		// pi1table.addCell(colon2cell);
		// pi1table.addCell(pccell);

		pi1table.setWidthPercentage(100f);

		PdfPCell productinfocell = new PdfPCell();
		productinfocell.addElement(pi1table);

		PdfPTable parent5table = new PdfPTable(1);
		parent5table.addCell(productinfocell);
		parent5table.setWidthPercentage(100);

		try {
			document.add(parent5table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createComplainHistory() {

	
		Phrase ch = new Phrase("Complain History", font12bold);

		Paragraph chpara = new Paragraph();
		chpara.add(ch);
		chpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell chcell = new PdfPCell();
		chcell.addElement(chpara);
		chcell.setBorder(0);
		chcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable chtable = new PdfPTable(1);
		chtable.addCell(chcell);
		chtable.setWidthPercentage(100f);

		PdfPCell ch1cell = new PdfPCell();
		ch1cell.addElement(chtable);

		PdfPTable parent6table = new PdfPTable(1);
		parent6table.addCell(ch1cell);
		parent6table.setWidthPercentage(100);

		try {
			document.add(parent6table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	public void createComplainHistoryHeading() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);

		PdfPTable table1 = new PdfPTable(5);

		try {
			table1.setWidths(tblcolwidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		table1.setWidthPercentage(100f);

		Phrase srno = new Phrase("SR.NO.", font1);
		Phrase date = new Phrase("DATE", font1);
		Phrase time = new Phrase("TIME", font1);
		Phrase user = new Phrase("USER", font1);
		Phrase remark = new Phrase("REMARK", font1);

		PdfPCell srnocell = new PdfPCell(srno);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell datecell = new PdfPCell(date);
		datecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell timecell = new PdfPCell(time);
		timecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell usercell = new PdfPCell(user);
		usercell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell remarkcell = new PdfPCell(remark);
		remarkcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table1.addCell(srnocell);
		table1.addCell(datecell);
		table1.addCell(timecell);
		table1.addCell(usercell);
		table1.addCell(remarkcell);

		for (int i = 0; i < this.complain.getComplainList().size(); i++) {

			chunk = new Phrase(
					complain.getComplainList().get(i).getSrNo() + "", font8);
			pdfsrno1 = new PdfPCell(chunk);
			pdfsrno1.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(fmt.format(complain.getComplainList().get(i)
					.getDate())
					+ "", font8);
			pdfdate1 = new PdfPCell(chunk);
			pdfdate1.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(
					complain.getComplainList().get(i).getTime() + "", font8);
			pdftime1 = new PdfPCell(chunk);
			pdftime1.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(complain.getComplainList().get(i).getUserName()
					+ "", font8);
			pdfuser1 = new PdfPCell(chunk);
			pdfuser1.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (complain.getComplainList().get(i).getRemark() != null) {
				chunk = new Phrase(complain.getComplainList().get(i)
						.getRemark()
						+ "", font8);
				pdfremark1 = new PdfPCell(chunk);
				pdfremark1.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			table1.addCell(pdfsrno1);
			table1.addCell(pdfdate1);
			table1.addCell(pdftime1);
			table1.addCell(pdfuser1);
			table1.addCell(pdfremark1);

			// PdfPCell comhiscell=new PdfPCell();
			// comhiscell.addElement(table1);
			//
			// PdfPTable parent7table=new PdfPTable(1);
			// parent7table.addCell(comhiscell);
			// parent7table.setWidthPercentage(100);
		}

		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createDescriptionHeading() {

		Phrase description = new Phrase();
		Paragraph descpara = new Paragraph("Description", font12bold);
		descpara.setAlignment(Element.ALIGN_LEFT);
		descpara.add(Chunk.NEWLINE);
		
//		descpara.add(Chunk.NEWLINE);
//		descpara.add(Chunk.NEWLINE);
//		descpara.add(Chunk.NEWLINE);
//		descpara.add(Chunk.NEWLINE);
		
		
		PdfPCell descriptioncell = new PdfPCell();
		descriptioncell.addElement(descpara);
		descriptioncell.setBorder(0);
		
		Phrase desc = null;
		if(complain.getDestription() !=null){
		
			desc = new Phrase("" + complain.getDestription(),font10);
			
		}
		
		PdfPCell desccell = new PdfPCell(desc);
		desccell.setBorder(0);
		
		PdfPTable desctable = new PdfPTable(1);
		desctable.addCell(descriptioncell);
		desctable.addCell(desccell);
		desctable.setWidthPercentage(100f);

		PdfPCell desc1cell = new PdfPCell();
		desc1cell.addElement(desctable);

		PdfPTable desc1table = new PdfPTable(1);
		desc1table.addCell(desc1cell);
		desc1table.setWidthPercentage(100);

		try {
			document.add(desc1table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
}
