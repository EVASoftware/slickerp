package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.LoggedIn;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class InvoicePdf {


	public Document document;

	Company comp;
	Customer cust;
	Invoice invoiceentity;
	Contract con;
//	CustomerPayment custPayment;
	List<Service> service = new ArrayList<Service>();

	Phrase colon;
	Phrase blank;
	Phrase chunk;
	String functionalYear=null;
	PdfPCell pdftype, pdfarea, pdfrate, pdfper, pdftprice, pdfttlamt,
			pdftaxpercent;

	 float[] colwidth = { 0.5f, 9.5f};
	 float[] col1width = { 1.5f,1.5f,7.0f};
	// float[] tblcol1width = { 1.5f, 1.5f, 7.0f };
	// float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f };

	private Font font16boldul, font12bold, font8bold, font8, font9bold,font9boldUNDERLINE,
			font12boldul, font12, font10bold, font10, font14bold, font9,font16bold,
			font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	
	DecimalFormat df = new DecimalFormat("0.00");

	private PdfPCell custlandcell;

	public InvoicePdf(){

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldUNDERLINE = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	}

	public void setinvoicepdf(Long count) {

		invoiceentity = ofy().load().type(Invoice.class).id(count).now();

		if (invoiceentity.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getCustomerId())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getCustomerId()).first()
					.now();

		if (invoiceentity.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (invoiceentity.getCompanyId() != null)
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount()).first()
					.now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("count", invoiceentity.getContractCount()).first()
					.now();
		
		//    rohan load service Entity for printing services .
		
		if (invoiceentity.getCompanyId() != null)
			service = ofy().load().type(Service.class)
					.filter("contractCount", invoiceentity.getContractCount()).list();
		else
			service = ofy().load().type(Service.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("contractCount", invoiceentity.getContractCount()).list();
		
		System.out.println("Service list size "+service.size());

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String status) {

		System.out.println("status value "+status);
		if(status.equals("yes"))
		{
			createBlank1Heading();
		}
		else if(status.equals("no"))
		{
			createBlank1Heading();
		    	if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
		}
		
		
		
		System.out.println("blank heading is created");
		createFrequencyInMonthTable();
		createCustomerInfoHeading();
		createPaymentHeading();
		createContractTable();
		createRsHeading();
		createServiceHeading();
		createAcceptanceHeading();
		
		createBaseInformation();
		
//		createReceipt1Heading();
//		System.out.println("createReceipt1Heading");
//		createReceipt2Heading();
//		System.out.println("createReceipt2Heading");
//		createReceipt3Heading();
//		System.out.println("createReceipt3Heading");
//		createReceipt4Heading();
//		System.out.println("createReceipt4Heading");
		createTandNheading();
		
		termsAndConditions();

		String amountInWord = ServiceInvoicePdf.convert(1400);

	}

	
private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	
	private void termsAndConditions() {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(colwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		table.setSpacingBefore(10f);
		
		
		Phrase title = new Phrase("Terms and Conditions" , font16bold);
		Paragraph para = new Paragraph(title);
		para.setAlignment(Element.ALIGN_CENTER);
		
		Phrase p1 = new Phrase("The client undertakes to ensure the co-operation of the residents / employees / members of the society / corporate/residence as the case may be for moving the furniture to enable our representative to carry out the treatment All rooms / cabins / flats in the house / society / corporate as the case may be should be made available for servicing as and when our Authorised Service Technician calls on his visit. Failure to do so will be treated as a service rendered and no refund shall accrue on his account.",font8);
		PdfPCell p1Cell =  new PdfPCell(p1);
		p1Cell.setBorder(0);
		p1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p2 = new Phrase("Every visit by our Authorised Service Technician will be made within reasonable time from receipt of a complaint and will be made only during working hours of our Service Department and no visits will be made on Holidays. In the event such a service is provided additional amount will be charged.",font8);
		PdfPCell p2Cell =  new PdfPCell(p2);
		p2Cell.setBorder(0);
		p2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p3 = new Phrase("If at any time during the contract period the �Pest� against which the service is provided reappears subject to compliances and observations of the conditions and  instructions  thereto  in  the  contracted premises  the company shall carry out re-service at no additional cost.",font8);
		PdfPCell p3Cell =  new PdfPCell(p3);
		p3Cell.setBorder(0);
		p3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p4 = new Phrase("The company shall not be liable for damages, death, injuries or any consequential damage of what  so  ever nature to any person or property, by using the service.",font8);
		PdfPCell p4Cell =  new PdfPCell(p4);
		p4Cell.setBorder(0);
		p4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p5 = new Phrase("The contract is for providing services in the premises covered under the contract. In the event of change of address a fresh contract needs to be entered into for providing the service as the case may be.",font8);
		PdfPCell p5Cell =  new PdfPCell(p5);
		p5Cell.setBorder(0);
		p5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p6 = new Phrase("This contract is neither terminable before the expiry period nor transferable.",font8);
		PdfPCell p6Cell =  new PdfPCell(p6);
		p6Cell.setBorder(0);
		p6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p7 = new Phrase("Contract charges are payable by Cash or Cheque / DD favouring �Om Pest control Services� in  advance and payment shall accompany the signed copy of the contract and such payment shall be released before  the  commencement  of  the contract period.",font8);
		PdfPCell p7Cell =  new PdfPCell(p7);
		p7Cell.setBorder(0);
		p7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p8 = new Phrase("In the event of any complaint, please call us on Service Center number & provide your Name, Address & Invoice number and date to enable our technician to verify & provide efficient service.",font8);
		PdfPCell p8Cell =  new PdfPCell(p8);
		p8Cell.setBorder(0);
		p8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p9 = new Phrase("This contract supersedes all prior agreements, undertakings or arrangements, oral or written between the  parties on the subject matter.",font8);
		PdfPCell p9Cell =  new PdfPCell(p9);
		p9Cell.setBorder(0);
		p9Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p10 = new Phrase("The company shall not be held responsible for any damages to the interior and / or exterior structures of the building / rooms / walls / cabins / flats as the case may be which may occur while performing the duty to provide service in the matter.",font8);
		PdfPCell p10Cell =  new PdfPCell(p10);
		p10Cell.setBorder(0);
		p10Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p11 = new Phrase("The client undertakes to keep a representative and / or the person present at all times the  treatment  is  being carried out as a matter of safety and / or the security to avoid any allegations of what so ever nature.",font8);
		PdfPCell p11Cell =  new PdfPCell(p11);
		p11Cell.setBorder(0);
		p11Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		Phrase p12 = new Phrase("The company will not be responsible for failure of service in the event the instructions given are not followed or the treated areas are disturbed / washed, painted or uprooted.",font8);
		PdfPCell p12Cell =  new PdfPCell(p12);
		p12Cell.setBorder(0);
		p12Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		
		Phrase pv1 = new Phrase("1.",font8);
		PdfPCell pv1Cell =  new PdfPCell(pv1);
		pv1Cell.setBorder(0);
		Phrase pv2 = new Phrase("2.",font8);
		PdfPCell pv2Cell =  new PdfPCell(pv2);
		pv2Cell.setBorder(0);
		Phrase pv3 = new Phrase("3.",font8);
		PdfPCell pv3Cell =  new PdfPCell(pv3);
		pv3Cell.setBorder(0);
		Phrase pv4 = new Phrase("4.",font8);
		PdfPCell pv4Cell =  new PdfPCell(pv4);
		pv4Cell.setBorder(0);
		Phrase pv5 = new Phrase("5.",font8);
		PdfPCell pv5Cell =  new PdfPCell(pv5);
		pv5Cell.setBorder(0);
		Phrase pv6 = new Phrase("6.",font8);
		PdfPCell pv6Cell =  new PdfPCell(pv6);
		pv6Cell.setBorder(0);
		Phrase pv7 = new Phrase("7.",font8);
		PdfPCell pv7Cell =  new PdfPCell(pv7);
		pv7Cell.setBorder(0);
		Phrase pv8 = new Phrase("8.",font8);
		PdfPCell pv8Cell =  new PdfPCell(pv8);
		pv8Cell.setBorder(0);
		Phrase pv9 = new Phrase("9.",font8);
		PdfPCell pv9Cell =  new PdfPCell(pv9);
		pv9Cell.setBorder(0);
		Phrase pv10 = new Phrase("10.",font8);
		PdfPCell pv10Cell =  new PdfPCell(pv10);
		pv10Cell.setBorder(0);
		Phrase pv11 = new Phrase("11.",font8);
		PdfPCell pv11Cell =  new PdfPCell(pv11);
		pv11Cell.setBorder(0);
		Phrase pv12 = new Phrase("12.",font8);
		PdfPCell pv12Cell =  new PdfPCell(pv12);
		pv12Cell.setBorder(0);
		
		
		table.addCell(pv1Cell);table.addCell(p1Cell);
		table.addCell(pv2Cell);table.addCell(p2Cell);
		table.addCell(pv3Cell);table.addCell(p3Cell);
		table.addCell(pv4Cell);table.addCell(p4Cell);
		table.addCell(pv5Cell);table.addCell(p5Cell);
		table.addCell(pv6Cell);table.addCell(p6Cell);
		table.addCell(pv7Cell);table.addCell(p7Cell);
		table.addCell(pv8Cell);table.addCell(p8Cell);
		table.addCell(pv9Cell);table.addCell(p9Cell);
		table.addCell(pv10Cell);table.addCell(p10Cell);
		table.addCell(pv11Cell);table.addCell(p11Cell);
		table.addCell(pv12Cell);table.addCell(p12Cell);
		
		
		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankCell =  new PdfPCell(blank);
		blankCell.setBorder(0);
		
		Phrase dash = new Phrase("*", font8);
		PdfPCell dashCell =  new PdfPCell(dash);
		dashCell.setBorder(0);

		table.addCell(blankCell);table.addCell(blankCell);
		table.addCell(blankCell);table.addCell(blankCell);
		Phrase pp1 = new Phrase("Instructions / Condition to be observed / complied by client for making the Job Success",font12bold);
		PdfPCell pp1Cell =  new PdfPCell(pp1);
		pp1Cell.setBorder(0);
		
		
		//   1st part 
		
		PdfPTable sample1part = new PdfPTable(1);
		sample1part.setWidthPercentage(100f);
		
		
		Phrase pp2 = new Phrase("Termite Management Service in Apartments / flats :",font10bold);
		PdfPCell pp2Cell =  new PdfPCell(pp2);
		pp2Cell.setBorder(0);
		sample1part.addCell(pp1Cell);
		sample1part.addCell(pp2Cell);
		
		PdfPTable sample1part1 = new PdfPTable(2);
		sample1part1.setWidthPercentage(100f);
		
		try {
			sample1part1.setWidths(colwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		Phrase x1 = new Phrase("The treatment involves drilling holes at the junction of wall & floor it is essential  to  keep  free  space  of   at least 2  feet from the wall to facilitate smooth execution of the job.",font8);
		PdfPCell x1Cell =  new PdfPCell(x1);
		x1Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x1Cell.setBorder(0);
		Phrase x2 = new Phrase("We request you to shift & move articles touching to the wall before our technician starts the treatment. Our Service technicians are instructed not to handle the owner�s property.",font8);	
		PdfPCell x2Cell =  new PdfPCell(x2);
		x2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x2Cell.setBorder(0);
		Phrase x3 = new Phrase("Always provide the information to our technicians / service person on location of the electrical wiring / conducts, telephone cables. This will help us to prevent any damages while drilling operations. We do not take any responsibility for the damage caused to the property in absence of above information.",font8);
		PdfPCell x3Cell =  new PdfPCell(x3);
		x3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x3Cell.setBorder(0);
		Phrase x4 = new Phrase("Electricity for drilling and water to mix chemical had to be made available in sufficient quantity by the owner.",font8);
		PdfPCell x4Cell =  new PdfPCell(x4);
		x4Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x4Cell.setBorder(0);
		Phrase x5 = new Phrase("Owners are requested to keep electrician available, if termite infestation is noticed in electrical junction boxes.",font8);
		PdfPCell x5Cell =  new PdfPCell(x5);
		x5Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x5Cell.setBorder(0);
		Phrase x6 = new Phrase("The equipments used in drilling creates noise, pesticides do have slight odour, clients are requested to bear with the same. If you have any patients / pets / children in the house, do not allow them to remain present while the treatment is in progress.",font8);
		PdfPCell x6Cell =  new PdfPCell(x6);
		x6Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x6Cell.setBorder(0);
		Phrase x7 = new Phrase("In order to get the best results from the treatment it is necessary to treat the entire area of the apartment / flat / building / cabin. Re-occurrence of the termites is possible if part of the area is not made available for the treatment.",font8);
		PdfPCell x7Cell =  new PdfPCell(x7);
		x7Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x7Cell.setBorder(0);
		Phrase x8 = new Phrase("Please note that sometimes you may see the live termites immediately after completion of the treatment. Note it takes little time to kill the existing live infestation.",font8);
		PdfPCell x8Cell =  new PdfPCell(x8);
		x8Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x8Cell.setBorder(0);
		Phrase x9 = new Phrase("We kindly request you to provide the ladder & stool to treat the infestation at higher elevations. Removal & treatment to the false ceiling blocks is necessary if infestation is noticed in the false ceiling area.",font8);
		PdfPCell x9Cell =  new PdfPCell(x9);
		x9Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x9Cell.setBorder(0);
		Phrase x10 = new Phrase("The treatments are scheduled as per the mutually agreed time frame. We should not be held responsible if the premises are not made available for the treatment.",font8);
		PdfPCell x10Cell =  new PdfPCell(x10);
		x10Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x10Cell.setBorder(0);
		Phrase x11 = new Phrase("OPCS shall not by any means be or become liable to the owners for any damage suffered by the owner directly or indirectly through the presence of pest in the premise contracted. The only liability of Om Pest Control Services will be restricted to treat the said premises and ensure the contract of the pest for which owner has signed contract with OPCS.",font8);
		PdfPCell x11Cell =  new PdfPCell(x11);
		x11Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		x11Cell.setBorder(0);
		
		sample1part1.addCell(dashCell);sample1part1.addCell(x1Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x2Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x3Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x4Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x5Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x6Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x7Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x8Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x9Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x10Cell);
		sample1part1.addCell(dashCell);sample1part1.addCell(x11Cell);
		
		
	//  2nd part 
		
		PdfPTable sample2part = new PdfPTable(1);
		sample2part.setWidthPercentage(100f);
		
		
	    Phrase pp10 = new  Phrase("Rodent Management service.",font10bold);
		PdfPCell pp10Cell =  new PdfPCell(pp10);
		pp10Cell.setBorder(0);
		sample2part.addCell(pp10Cell);
		
		
		PdfPTable sample2part1 = new PdfPTable(2);
		sample2part1.setWidthPercentage(100f);
		
		try {
			sample2part1.setWidths(colwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase pp11 = new  Phrase("Do not disturb the poison bait stations / traps or tamper them as they are placed at the strategic locations / rodent runways.",font8);
		PdfPCell pp11Cell =  new PdfPCell(pp11);
		pp11Cell.setBorder(0);
		Phrase pp12 = new  Phrase("Keep us informed if you notice any dead rat or rat trapped in the glue board.",font8);
		PdfPCell pp12Cell =  new PdfPCell(pp12);
		pp12Cell.setBorder(0);
		Phrase pp13 = new  Phrase("We will recommend proofing measures to prevent entry and movement of rats within your premises.",font8);
		PdfPCell pp13Cell =  new PdfPCell(pp13);
		pp13Cell.setBorder(0);
		
		sample2part1.addCell(dashCell);sample2part1.addCell(pp11Cell);
		sample2part1.addCell(dashCell);sample2part1.addCell(pp12Cell);
		sample2part1.addCell(dashCell);sample2part1.addCell(pp13Cell);
		
		//   3rd  part 
		
		PdfPTable sample = new PdfPTable(1);
		sample.setWidthPercentage(100f);
		
		Phrase ppp12 = new  Phrase("GD service (Gel service)",font10bold);
		PdfPCell ppp12Cell =  new PdfPCell(ppp12);
		ppp12Cell.setBorder(0);
		ppp12Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		sample.addCell(ppp12Cell);
		
		PdfPTable sample1 = new PdfPTable(2);
		sample1.setWidthPercentage(100f);
		
		try {
			sample1.setWidths(colwidth);
		} catch (DocumentException e1) 
		{
			e1.printStackTrace();
		}
		
		Phrase ppp21 = new  Phrase("Do not clean / remove the GEL spots from the place of application.",font8);
		PdfPCell ppp21Cell =  new PdfPCell(ppp21);
		ppp21Cell.setBorder(0);
		Phrase ppp22 = new  Phrase("Do not wash these spots.",font8);
		PdfPCell ppp22Cell =  new PdfPCell(ppp22);
		ppp22Cell.setBorder(0);
		Phrase ppp23 = new  Phrase("Do not use any pesticide or chemicals which may repel cockroaches whenever the GEL applied.",font8);
		PdfPCell ppp23Cell =  new PdfPCell(ppp23);
		ppp23Cell.setBorder(0);
		
		sample1.addCell(dashCell);sample1.addCell(ppp21Cell);
		sample1.addCell(dashCell);sample1.addCell(ppp22Cell);
		sample1.addCell(dashCell);sample1.addCell(ppp23Cell);
		
		try {
			document.add(Chunk.NEXTPAGE);
			document.add(para);
		
			document.add(table);
			document.add(sample1part);
			document.add(sample1part1);
			document.add(sample2part);
			document.add(sample2part1);
			document.add(sample);
			document.add(sample1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void createFrequencyInMonthTable() {
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(new float[] { 2.2f,7.8f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase frequency = new Phrase("Frequency in Month/Day ", font10);
		PdfPCell frequencycell = new PdfPCell(frequency);
		frequencycell.setBorder(0);
		frequencycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(frequencycell);
		
		
		Phrase blank = new Phrase("", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	
		
		SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Collections.sort(service, new Comparator<Service>() {

			@Override
			public int compare(Service o1, Service o2) {
				Integer count1 = o1.getCount();
				Integer count2 = o2.getCount();
				
				return count1.compareTo(count2);
			}
		});
		
		
		
		
		
		
		System.out.println("Rohan 123::::"+ service.size());
		
		System.out.println("con list size::"+con.getItems().size());
		for(int i=0;i<con.getItems().size();i++)
		{
			String dateLine = null ; 
			for(int j=0;j<service.size();j++)
			{
				if(con.getItems().get(i).getProductCode().equals(service.get(j).getProduct().getProductCode()))
				{
					
					if(dateLine==null)
					{
						dateLine =sdf.format(service.get(j).getServiceDate());
					}
					else
					{
						dateLine =dateLine+" , "+sdf.format(service.get(j).getServiceDate());
					}
				}
			
			}
			
			// for printing data 
			
			System.out.println("rohan "+con.getItems().get(i).getProductCode()+" : "+dateLine);
			Phrase cell = new Phrase(con.getItems().get(i).getProductCode()+" : "+dateLine,font8);
			PdfPCell pdfcell =  new PdfPCell(cell);
			pdfcell.setBorder(0);
			table.addCell(pdfcell);
			
			if(con.getItems().size() >i+1 || i +1< con.getItems().size())
			{
				table.addCell(blankCell);
			}
			
			
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	//  rohan commented this code a per om pest required and same method(with same methid name)for output as they required 
//	public void createFrequencyInMonthTable() {
//		PdfPTable table = new PdfPTable(10);
//		table.setWidthPercentage(100f);
//		table.setSpacingAfter(5f);
//		table.setSpacingBefore(5f);
//		try {
//			table.setWidths(new float[] { 30, 4, 12, 4, 12, 4, 12, 4, 12, 4 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		Phrase blank = new Phrase(" ", font10);
//		PdfPCell blankcell = new PdfPCell(blank);
//		blankcell.setBorder(0);
//
//		PdfPCell cell1=null;
//		PdfPCell cell2=null;
//		PdfPCell cell3=null;
//		PdfPCell cell4=null;
//		SimpleDateFormat sdf = new SimpleDateFormat("MMM-yyyy");
//		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//		
//		Collections.sort(service, new Comparator<Service>() {
//
//			@Override
//			public int compare(Service o1, Service o2) {
//				Integer count1 = o1.getCount();
//				Integer count2 = o2.getCount();
//				
//				return count1.compareTo(count2);
//			}
//		});
//		System.out.println("Rohan 123::::"+ service.size());
//		
//		
//		if(service.size()>3)
//		{
//					
//					Phrase ph0 = new Phrase(sdf.format(service.get(0).getServiceDate()),font10);
//				    cell1 = new PdfPCell(ph0);
//				    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//				
//				
//					Phrase ph1 = new Phrase(sdf.format(service.get(1).getServiceDate()),font10);
//					cell2 = new PdfPCell(ph1);
//					cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//				
//				
//					Phrase ph2 = new Phrase(sdf.format(service.get(2).getServiceDate()),font10);
//					 cell3 = new PdfPCell(ph2);
//					 cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
//				
//				
//					Phrase ph3 = new Phrase(sdf.format(service.get(3).getServiceDate()),font10);
//					 cell4 = new PdfPCell(ph3);
//					 cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		}
//		else if(service.size()==3)
//		{
//			Phrase ph0 = new Phrase(sdf.format(service.get(0).getServiceDate()),font10);
//		    cell1 = new PdfPCell(ph0);
//		    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph1 = new Phrase(sdf.format(service.get(1).getServiceDate()),font10);
//			cell2 = new PdfPCell(ph1);
//			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph2 = new Phrase(sdf.format(service.get(2).getServiceDate()),font10);
//			 cell3 = new PdfPCell(ph2);
//			 cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph3 = new Phrase(" ",font10);
//			 cell4 = new PdfPCell(ph3);
//			 cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		else if(service.size()==2)
//		{
//
//			Phrase ph0 = new Phrase(sdf.format(service.get(0).getServiceDate()),font10);
//		    cell1 = new PdfPCell(ph0);
//		    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph1 = new Phrase(sdf.format(service.get(1).getServiceDate()),font10);
//			cell2 = new PdfPCell(ph1);
//			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph2 = new Phrase(" ",font10);
//			 cell3 = new PdfPCell(ph2);
//			 cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph3 = new Phrase(" ",font10);
//			 cell4 = new PdfPCell(ph3);
//			 cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		else
//		{
//			Phrase ph0 = new Phrase(sdf.format(service.get(0).getServiceDate()),font10);
//		    cell1 = new PdfPCell(ph0);
//		    cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph1 = new Phrase(" ",font10);
//			cell2 = new PdfPCell(ph1);
//			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph2 = new Phrase(" ",font10);
//			 cell3 = new PdfPCell(ph2);
//			 cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		
//			Phrase ph3 = new Phrase(" ",font10);
//			 cell4 = new PdfPCell(ph3);
//			 cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		
//
//		Phrase frequency = new Phrase("Frequency in Month/Day ", font10);
//		PdfPCell frequencycell = new PdfPCell(frequency);
//		frequencycell.setBorder(0);
//		frequencycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		table.addCell(frequencycell);
//		table.addCell(blankcell);
//		table.addCell(cell1);
//
//		table.addCell(blankcell);
//		table.addCell(cell2);
//
//		table.addCell(blankcell);
//		table.addCell(cell3);
//
//		table.addCell(blankcell);
//		table.addCell(cell4);
//
//		table.addCell(blankcell);
//
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//
//	}

	private void createBlank1Heading() {

		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
//		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(bltable);
		cell.setBorderWidthTop(0);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createCustomerInfoHeading() {

		System.out.println("print customerinfo");
		
		
		
		Phrase customerCorrespondance = null;
		PdfPCell customerCorrespondanceCell = null;
		if(cust.getCustCorresponence()!=null)
		{
			customerCorrespondance = new Phrase(cust.getCustCorresponence(),font8);
			
			customerCorrespondanceCell = new PdfPCell(customerCorrespondance);
			customerCorrespondanceCell.setBorder(0);
		}
		
		
		
		Phrase custname = new Phrase("CUSTOMER NAME:", font9bold);
		PdfPCell custnamecell = new PdfPCell(custname);
		custnamecell.setBorder(0);
		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cname = null;
		if (cust.isCompany() != null ) {
			cname = new Phrase("" + cust.getCompanyName(), font9);
		} else {
			if (cust.getFullname() != null) {
				cname = new Phrase("" + cust.getFullname(), font9);
			}
		}

		PdfPCell cnamecell = new PdfPCell(cname);
		cnamecell.setBorder(0);
		cnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase custaddress = new Phrase("ADDRESS:", font9bold);
		PdfPCell custaddcell = new PdfPCell(custaddress);
		custaddcell.setBorder(0);
		custaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null) {

			if (!cust.getAdress().getAddrLine2().equals(" ")) {
				if (!cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + "\n"
							+ cust.getAdress().getAddrLine2() + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + "\n"
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (!cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (!cust.getAdress().getLocality().equals("")) {
				custFullAdd1 = custAdd1 + "\n" 
						+ cust.getAdress().getLocality()+" "
						+ cust.getAdress().getPin() +"\n"
						+ cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity();

			} else {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}
		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		
		Phrase pname = null;
		PdfPCell pnamecell=null;
		if(cust.isCompany()==true){
				pname = new Phrase("CTC: " + cust.getFullname(), font8);
				 pnamecell = new PdfPCell(pname);
					pnamecell.setBorder(0);
					pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		
		
		
		Phrase mobno = null;
		if (cust.getCellNumber2()!= null && cust.getCellNumber2()!=0) {

			mobno = new Phrase("CONTACT NO.: " + cust.getCellNumber1()+" , "+cust.getCellNumber2(), font9);
		}
		else
		{
			mobno = new Phrase("CONTACT NO.: " + cust.getCellNumber1(), font9);
		}

		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(1);
		
		if(cust.getCustCorresponence()!=null)
		{
			lefttable.addCell(customerCorrespondanceCell);
		}
		
		lefttable.addCell(custnamecell);
		lefttable.addCell(cnamecell);
		lefttable.addCell(custaddcell);
		lefttable.addCell(custAddInfoCell);
		
		if(cust.isCompany()==true){
			lefttable.addCell(pnamecell);
		}
		
		lefttable.addCell(mobcell);
		lefttable.setWidthPercentage(100f);

		PdfPCell leftcell = new PdfPCell(lefttable);
		// leftcell.setBorder(0);
		leftcell.setBorderWidthLeft(0);
		leftcell.setBorderWidthRight(0);

		/***************************************************************************************/

		Phrase blank = new Phrase(" ", font9);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankcell.setBorder(0);

		Phrase bill=null;
		Phrase bill1=null;
			bill = new Phrase("INVOICE NO. : ", font9);
			
//			 new code   rohan commented this code for separation of billing -  non billing 	
			
			
			if( con.getNumberRange()!=null && con.getNumberRange().equalsIgnoreCase("NonBilling"))
			{
				String year = getFunnctionalYear(invoiceentity.getInvoiceDate());
				
				if(invoiceentity.getRefNumber()!=null && !invoiceentity.getRefNumber().trim().equals(""))
					{
						bill1 = new Phrase(year+" / "+invoiceentity.getRefNumber().trim(),font9bold);
					}
					else
					{	
						bill1 = new Phrase((invoiceentity.getCount()% 10000)+"-"+year, font9bold);
					}
			}
			else
			{
				if(invoiceentity.getRefNumber()!=null && !invoiceentity.getRefNumber().trim().equals(""))
					{
						bill1 = new Phrase(invoiceentity.getRefNumber().trim(),font9bold);
					}
					else
					{	
						bill1 = new Phrase((invoiceentity.getCount()% 10000)+"", font9bold);
					}
			}
			
			
			
			// old code    rohan commented this code for separation of billing -  non billing 	
//			if(invoiceentity.getRefNumber()!=null && !invoiceentity.getRefNumber().trim().equals(""))
//			{
//				bill1 = new Phrase(invoiceentity.getRefNumber().trim(),font9bold);
//			}
//			else
//			{	
//				bill1 = new Phrase((invoiceentity.getCount()% 10000)+"", font9bold);
//			}
		
		//  rohan added this code 
		
		PdfPCell billcell = new PdfPCell(bill);
		billcell.setBorder(0);
		billcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bill1cell = new PdfPCell(bill1);
		bill1cell.setBorder(0);
		bill1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable table6 = new PdfPTable(2);
		table6.setWidthPercentage(100f);
		
		table6.addCell(billcell);
		table6.addCell(bill1cell);
		
		PdfPCell table6Cell = new PdfPCell(table6);
		table6Cell.setBorder(0);

		// Phrase date = new Phrase("DATE:",font10bold);
		// PdfPCell datecell = new PdfPCell(date);
		// datecell.setBorder(0);
		// datecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dt = null;
		Phrase dt1 = null;

//		System.out.println("vvvv date " + invoiceentity.getInvoiceDate());
		
		if (invoiceentity.getInvoiceDate() != null) {
			dt = new Phrase("DATE: ", font9);
			dt1 = new Phrase(fmt.format(invoiceentity.getInvoiceDate()), font9bold);		
			}

		
		
		//   rohan added this code 
		
		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell dtcell1 = new PdfPCell(dt1);
		dtcell1.setBorder(0);
		dtcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table123 = new PdfPTable(2);
		table123.setWidthPercentage(100f);
		
		table123.addCell(dtcell);
		table123.addCell(dtcell1);
		
		PdfPCell tableCell = new PdfPCell(table123);
		tableCell.setBorder(0);

		// Phrase code = new Phrase("CUSTOMER CODE"+"/"+"JOB NO.:",font8bold);
		// PdfPCell codecell = new PdfPCell(code);
		// codecell.setBorder(0);
		// codecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase custcode = null;
		Phrase custcode1 = null;
		if (invoiceentity.getCustomerId() != 0l) {

			custcode = new Phrase("CUSTOMER CODE" + "/" + "JOB NO.: ", font9);
			
			custcode1 = new Phrase(invoiceentity.getCustomerId()+"",font9bold);

		}
		

		PdfPCell custcodecell = new PdfPCell(custcode);
		custcodecell.setBorder(0);
		custcodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell custcodecell1 = new PdfPCell(custcode1);
		custcodecell1.setBorder(0);
		custcodecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	//   rohan added this  **************
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		
		table.addCell(custcodecell);
		table.addCell(custcodecell1);
		
		PdfPCell tableCell123 = new PdfPCell(table);
		tableCell123.setBorder(0);
		
		
		// Phrase quot = new
		// Phrase("QUOTATION"+"/"+"RENEWAL NO. & DATE:",font10bold);
		// PdfPCell quotcell = new PdfPCell(quot);
		// quotcell.setBorder(0);
		// quotcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase quot = null;
		Phrase quot1 = null;
		if (con.getRefContractCount() !=null) {
			
			String refNum="";
			if(con.getRefContractCount()!=-1){
				refNum=con.getCount()+"";
			}else{
				if(con.getQuotationCount()!=-1){
					refNum=con.getQuotationCount()+"";
				}else{
					refNum=con.getCount()+"";
				}
			}

			quot = new Phrase("QUOTATION/RENEWAL NO. & DATE: ", font9);
			
			quot1 = new Phrase(refNum+ " / " + fmt.format(con.getCreationDate()), font9bold);

		} else {
			if (con.getRefContractCount() != null) {

				quot = new Phrase("QUOTATION" + "/" + con.getQuotationCount(),
						font9);
				
				quot = new Phrase(con.getQuotationCount()+"",
						font9bold);

			}
		}

		PdfPCell quotcell = new PdfPCell(quot);
		quotcell.setBorder(0);
		quotcell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		rohan added this code 
		
		PdfPCell quotcell1 = new PdfPCell(quot1);
		quotcell1.setBorder(0);
		quotcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table2 = new PdfPTable(2);
		table2.setWidthPercentage(100f);
		
		table2.addCell(quotcell);
		table2.addCell(quotcell1);
		
		PdfPCell tableCell2 = new PdfPCell(table2);
		tableCell2.setBorder(0);
		
		Phrase contract = null;
		Phrase contract1 = null;
		if(con.getStartDate() !=null && con.getEndDate() !=null){
			contract = new Phrase("CONTRACT PERIOD:",font9);
			contract1 = new Phrase(fmt1.format(con.getStartDate())+" To "+fmt1.format(con.getEndDate()),font9bold);
		}
		
		
		
		PdfPCell contr1cell = new PdfPCell(contract);
		contr1cell.setBorder(0);
		contr1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		//  rohan added this code 
		
		PdfPCell contr1cell1 = new PdfPCell(contract1);
		contr1cell1.setBorder(0);
		contr1cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table1 = new PdfPTable(2);
		table1.setWidthPercentage(100f);
		
		table1.addCell(contr1cell);
		table1.addCell(contr1cell1);
		
		PdfPCell tableCell1 = new PdfPCell(table1);
		tableCell1.setBorder(0);

//		Phrase area = new Phrase("AREA:", font9);
//		PdfPCell areacell = new PdfPCell(area);
//		areacell.setBorder(0);
//		areacell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase area = null;
		Phrase area1 = null;
		if(con.getPremisesDesc() !=null){
			area = new Phrase("AREA:", font9);
			area1 = new Phrase(con.getPremisesDesc(), font9bold);
		}
		else{
			area = new Phrase("AREA:", font9);
		}
	
		
		
	//  rohan added this code 
		
		PdfPCell areacell = new PdfPCell(area);
		areacell.setBorder(0);
		areacell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell areacell1 = new PdfPCell(area1);
		areacell1.setBorder(0);
		areacell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			PdfPTable table1234 = new PdfPTable(2);
			table1234.setWidthPercentage(100f);
			
			table1234.addCell(areacell);
			table1234.addCell(areacell1);
			
			PdfPCell table1234Cell = new PdfPCell(table1234);
			table1234Cell.setBorder(0);

		
		
		
		
		

		/******************************************************************************/
		// Phrase area1 = nulll;
		// if(){
		//
		//
		//
		// }

		// Phrase premise = new Phrase("Premise",font10);
		// PdfPCell precell = new PdfPCell(premise);
		// precell.setBorder(0);
		// precell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*************************************************************************/

		// Phrase reference = new Phrase("CUSTOMER REF. BY:",font10bold);
		// PdfPCell referencecell = new PdfPCell(reference);
		// referencecell.setBorder(0);
		// referencecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reference = null;
		Phrase reference1 = null;
		if (con.getReferedBy() != null) {

			reference = new Phrase("CUSTOMER REF. BY:",font9);
			
			reference1 = new Phrase(con.getReferedBy(),font9bold);

		}

		else {

			reference = new Phrase("", font9);

		}

	//  rohan added this code 
		
		PdfPCell referencecell = new PdfPCell(reference);
		referencecell.setBorder(0);
		referencecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell reference1cell = new PdfPCell(reference1);
		reference1cell.setBorder(0);
		reference1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table5 = new PdfPTable(2);
		table5.setWidthPercentage(100f);
		
		table5.addCell(referencecell);
		table5.addCell(reference1cell);
		
		PdfPCell table5Cell = new PdfPCell(table5);
		table5Cell.setBorder(0);

//		Phrase em = null;
//		if (cust.getEmail() != null) {
//
//			em = new Phrase("EMAIL: " + cust.getEmail(), font9);
//
//		}

//		PdfPCell emcell = new PdfPCell(em);
//		emcell.setBorder(0);
//		emcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase colon = new Phrase(":");
		// PdfPCell colcell = new PdfPCell(colon);
		// colcell.setBorder(0);
		// colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(table6Cell);
		// righttable.addCell(blankcell);
		righttable.addCell(tableCell);
		// righttable.addCell(blankcell);
//		righttable.addCell(tableCell);
		// righttable.addCell(blankcell);
		righttable.addCell(tableCell2);
//		righttable.addCell(blankcell);
		righttable.addCell(tableCell1);
//		righttable.addCell(blankcell);
		righttable.addCell(table1234Cell);
		// righttable.addCell(blankcell);
		righttable.addCell(table5Cell);
		// righttable.addCell(blankcell);
//		righttable.addCell(emcell);
		righttable.setWidthPercentage(100f);
		;

		PdfPCell rightcell = new PdfPCell(righttable);
		// rightcell.setBorder(0);
		rightcell.setBorderWidthLeft(0);
		rightcell.setBorderWidthRight(0);

		/***************************************************************************/

		PdfPTable parenttable = new PdfPTable(2);
		parenttable.addCell(leftcell);
		parenttable.addCell(rightcell);
		parenttable.setWidthPercentage(100);

		try {
			parenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createPaymentHeading() {

		Phrase pay = null;
		if (comp.getBusinessUnitName() != null) {

			pay = new Phrase("PAYMENT DETAIL : Cheque shoud be in favour of '"
					+ comp.getBusinessUnitName()+"'", font10bold);
		}

		PdfPCell paycell = new PdfPCell(pay);
		// paycell.setBorder(0);
		paycell.setBorderWidthLeft(0);
		paycell.setBorderWidthRight(0);
		paycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable paytable = new PdfPTable(1);
		paytable.addCell(paycell);
		paytable.setWidthPercentage(100f);

		PdfPCell cell1 = new PdfPCell(paytable);
		cell1.setBorder(0);

		PdfPTable table1 = new PdfPTable(1);
		table1.addCell(cell1);
		table1.setWidthPercentage(100);

		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createRsHeading() {

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankcell.setBorder(0);
		blankcell.setBorderWidthTop(0);
		blankcell.setBorderWidthRight(0);
		blankcell.setBorderWidthLeft(0);

		Phrase rs = new Phrase("Rs. (in words): ", font10);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		//
		String amountInWord = ServiceInvoicePdf.convert(invoiceentity
				.getInvoiceAmount());

		String amountInWordWithLowerCase="";
		String[] spliturl=amountInWord.split(" ");
		
		System.out.println("RRR   spliturl.length"+spliturl.length);
		for(int i=1; i < spliturl.length;i++)
		{
			amountInWordWithLowerCase = amountInWordWithLowerCase+spliturl[i].toLowerCase()+" ";
		}
		
		Phrase inwords = new Phrase(spliturl[0]+" "+amountInWordWithLowerCase + "only.", font10bold);
		PdfPCell inwordscell = new PdfPCell(inwords);
		inwordscell.setBorderWidthTop(0);
		inwordscell.setBorderWidthRight(0);
		inwordscell.setBorderWidthLeft(0);
		inwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable table2 = new PdfPTable(2);
		table2.addCell(rscell);
		table2.addCell(inwordscell);
		table2.addCell(blankcell);
		table2.addCell(blankcell);
		table2.setWidthPercentage(100f);
		// table2.setSpacingAfter(5f);

		try {
			table2.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell perentcell = new PdfPCell(table2);
		perentcell.setBorderWidthTop(0);
		perentcell.setBorderWidthRight(0);
		perentcell.setBorderWidthLeft(0);

		PdfPTable parenttbl = new PdfPTable(1);
		parenttbl.addCell(perentcell);
		parenttbl.setWidthPercentage(100f);

		try {
			document.add(parenttbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createServiceHeading() {

		Phrase service = new Phrase("SERVICE CENTER", font16bold);
		PdfPCell sercell = new PdfPCell(service);
		sercell.setBorder(0);
		sercell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase mob = null;
		if (comp.getLandline() != null && comp.getCellNumber1() != null
				&& comp.getCellNumber2() != null) {

			mob = new Phrase( "022-"+comp.getLandline() + " / " + ""
					+ comp.getCellNumber1() + " / " + ""
					+"022-"+ comp.getCellNumber2(),
					font14bold);

		}

		PdfPCell mobcell = new PdfPCell(mob);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase email = null;
		if (comp.getEmail() != null && comp.getWebsite() != null) {
//			comp.getEmail()
			email = new Phrase("E-mail : " +"sales@opcs.in"  + "  /  "
					+ "Website : " + comp.getWebsite(), font14bold);

		}

		PdfPCell emailcell = new PdfPCell(email);
		emailcell.setBorder(0);
		emailcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable sertable = new PdfPTable(1);
		sertable.addCell(sercell);
		sertable.addCell(mobcell);
		sertable.addCell(emailcell);
		sertable.setWidthPercentage(100f);

		PdfPCell cell3 = new PdfPCell(sertable);
		cell3.setBorderWidthLeft(0);
		cell3.setBorderWidthRight(0);

		PdfPTable table3 = new PdfPTable(1);
		table3.addCell(cell3);
		table3.setWidthPercentage(100);

		try {
			document.add(table3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createAcceptanceHeading() {

		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankcell.setBorder(0);
		// blankcell.setBorderWidthTop(0);
		// blankcell.setBorderWidthRight(0);
		// blankcell.setBorderWidthLeft(0);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("For " + comp.getBusinessUnitName(),
					font12bold);
		}

		PdfPCell compcell = new PdfPCell(company);
		compcell.setBorder(0);
		compcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/************************************************************************************/

		Phrase autho = new Phrase("Authorised Signatory", font10bold);
		PdfPCell authocell = new PdfPCell(autho);
		authocell.setBorder(0);
		authocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name = null;
		name = new Phrase("Name :", font10);

		PdfPCell namecell = new PdfPCell(name);
		namecell.setBorder(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		System.out.println("RRRRRRRR Login persons  "+con.getCreatedBy());
		Phrase namevalue = null;
		if (con.getCreatedBy()!= null) {
			namevalue = new Phrase(con.getCreatedBy(), font10);
		}
		else
		{
			namevalue = new Phrase(" ", font10);
		}
		PdfPCell namevaluecell = new PdfPCell(namevalue);
		namevaluecell.setBorderWidthLeft(0);
		namevaluecell.setBorderWidthRight(0);
		namevaluecell.setBorderWidthTop(0);
		namevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/******************************************************************************/

		Phrase dt = null;
		dt = new Phrase("Date :", font10);

		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dtvalue = null;
		if (invoiceentity.getInvoiceDate() != null) {
			dtvalue = new Phrase(fmt.format(invoiceentity.getInvoiceDate()),
					font10);
		}

		PdfPCell dtvaluecell = new PdfPCell(dtvalue);
		dtvaluecell.setBorderWidthLeft(0);
		dtvaluecell.setBorderWidthRight(0);
		dtvaluecell.setBorderWidthTop(0);
		dtvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/*************************************************************************************/

		PdfPTable nametable = new PdfPTable(3);
		nametable.setWidthPercentage(100f);

		nametable.addCell(namecell);
		nametable.addCell(namevaluecell);
		nametable.addCell(blankcell);
		nametable.addCell(dtcell);
		nametable.addCell(dtvaluecell);
		nametable.addCell(blankcell);

		try {
			nametable.setWidths(new float[] { 15, 65, 20 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell namevalueCell = new PdfPCell(nametable);
		namevalueCell.setBorder(0);

		/*************************************************************************************/

		PdfPTable left1table = new PdfPTable(1);
		left1table.addCell(blankcell);
		left1table.addCell(compcell);
		left1table.addCell(blankcell);
		left1table.addCell(blankcell);
		left1table.addCell(blankcell);
		left1table.addCell(authocell);
		left1table.addCell(blankcell);
		left1table.addCell(namevalueCell);
		left1table.addCell(blankcell);

		left1table.setWidthPercentage(100f);
		left1table.setSpacingAfter(5f);

		PdfPCell left1cell = new PdfPCell(left1table);
		left1cell.setBorderWidthLeft(0);
		left1cell.setBorderWidthRight(0);
		left1cell.setBorderWidthTop(0);

		/************************************************************************************/

		Phrase acceptance = new Phrase("Customer's Acceptance", font9bold);
		PdfPCell accepcell = new PdfPCell(acceptance);
		accepcell.setBorder(0);
		accepcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase tt = new Phrase(
				"I/We agreed to the terms and conditions of the service contract mentioned overleaf. We agreed that the service contract is based on the information provided above. ",
				font8);

		Paragraph custaccpara = new Paragraph();
		custaccpara.add(tt);
		custaccpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(tt);
		titlecell.setBorder(0);

		Phrase sign = new Phrase("Signature of the Customer", font10bold);
		PdfPCell signcell = new PdfPCell(sign);
		signcell.setBorder(0);
		signcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		/**********************************************************************/

		// Phrase name1 = null;
		// if(cust.getName() !=null){
		// name1 = new Phrase("Name : "+cust.getName(),font10);
		// }
		//
		// PdfPCell name1cell = new PdfPCell(name1);
		// name1cell.setBorder(0);

		Phrase name1 = null;
		name1 = new Phrase("Name :", font10);

		PdfPCell name1cell = new PdfPCell(name1);
		name1cell.setBorder(0);
		name1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name1value = null;
//		if(cust.isCompany()!=null){
//			name1value = new Phrase("" + cust.getCompanyName(), font10);
//		}else{
			name1value = new Phrase(" ", font10);
//		}
		PdfPCell name1valuecell = new PdfPCell(name1value);
		name1valuecell.setBorderWidthLeft(0);
		name1valuecell.setBorderWidthRight(0);
		name1valuecell.setBorderWidthTop(0);
		name1valuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**********************************************************************/

		Phrase dt1 = null;
		dt1 = new Phrase("Date :", font10);

		PdfPCell dt1cell = new PdfPCell(dt1);
		// dt1cell.setBorderWidthLeft(0);
		// dt1cell.setBorderWidthRight(0);
		// dt1cell.setBorderWidthTop(0);
		dt1cell.setBorder(0);
		dt1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dtblank1 = null;
		dtblank1 = new Phrase(" ", font10);

		PdfPCell dt1blankcell = new PdfPCell(dtblank1);
		dt1blankcell.setBorderWidthLeft(0);
		dt1blankcell.setBorderWidthRight(0);
		dt1blankcell.setBorderWidthTop(0);
		dt1blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/******************************************************************/

		Phrase contact = null;
		contact = new Phrase("Contact No:", font10);

		PdfPCell contcell = new PdfPCell(contact);
		contcell.setBorder(0);
		contcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase contvalue = null;
//		if (cust.getCellNumber1() != null) {
			contvalue = new Phrase(" ", font10);
//		}

		PdfPCell contvaluecell = new PdfPCell(contvalue);
		contvaluecell.setBorderWidthLeft(0);
		contvaluecell.setBorderWidthRight(0);
		contvaluecell.setBorderWidthTop(0);
		contvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/************************************************************************/

		PdfPTable name1table = new PdfPTable(3);
		name1table.setWidthPercentage(100f);

		name1table.addCell(name1cell);
		name1table.addCell(name1valuecell);
		name1table.addCell(blankcell);
		name1table.addCell(contcell);
		name1table.addCell(contvaluecell);
		name1table.addCell(blankcell);
		name1table.addCell(dt1cell);
		name1table.addCell(dt1blankcell);
		name1table.addCell(blankcell);

		try {
			name1table.setWidths(new float[] { 23, 60, 17 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell name1valueCell = new PdfPCell(name1table);
		name1valueCell.setBorder(0);

		/********************************************************************/

		PdfPTable right1table = new PdfPTable(1);
		right1table.addCell(accepcell);
		// right1table.addCell(blankcell);
		right1table.addCell(titlecell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(signcell);
		right1table.addCell(blankcell);
		// right1table.addCell(name1cell);
		// right1table.addCell(contcell);
		// right1table.addCell(dt1cell);
		right1table.addCell(name1valueCell);
		right1table.addCell(blankcell);
		right1table.setSpacingAfter(5f);

		PdfPCell right1cell = new PdfPCell(right1table);
		// right1cell.setBorder(0);
		right1cell.setBorderWidthTop(0);
		right1cell.setBorderWidthLeft(0);
		right1cell.setBorderWidthRight(0);

		PdfPTable parent1table = new PdfPTable(2);
		parent1table.addCell(left1cell);
		parent1table.addCell(right1cell);
		parent1table.setWidthPercentage(100);

		try {
			parent1table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(parent1table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/***************************************************************************************/

//	public void createReceipt1Heading() {
//
//		Phrase blank = new Phrase(" ");
//		PdfPCell blankcell = new PdfPCell(blank);
//		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		blankcell.setBorder(0);
//
//		Phrase receipt = new Phrase("RECEIPT", font10bold);
//		PdfPCell rescell = new PdfPCell(receipt);
//		rescell.setBorder(0);
//		rescell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPTable restable = new PdfPTable(1);
//		restable.addCell(rescell);
//		restable.setWidthPercentage(100f);
//
//		PdfPCell centercell = new PdfPCell(restable);
//		centercell.setBorder(0);
//
//		PdfPTable centertable = new PdfPTable(1);
//		centertable.addCell(centercell);
//		centertable.setWidthPercentage(100);
//		centertable.setSpacingAfter(5f);
//
//		try {
//			document.add(centertable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//
//		Phrase rs1 = new Phrase("Received with thanks the sum of Rs.", font9);
//		PdfPCell rs1cell = new PdfPCell(rs1);
//		rs1cell.setBorder(0);
//		rs1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase blank1 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			blank1 = new Phrase(custPayment.getPaymentReceived() + " ", font9);
//		} else {
//			blank1 = new Phrase(" ");
//		}
//
//		PdfPCell blank1cell = new PdfPCell(blank1);
//		blank1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank1cell.setBorderWidthTop(0);
//		blank1cell.setBorderWidthRight(0);
//		blank1cell.setBorderWidthLeft(0);
//
//		/*******************************************************/
//
//		Phrase rs2 = new Phrase("Rupees", font9);
//		PdfPCell rs2cell = new PdfPCell(rs2);
//		rs2cell.setBorder(0);
//		rs2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase blank2 = new Phrase(" ");
//
//		Phrase blank2 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			String amountInWord = ServiceInvoicePdf.convert(custPayment
//					.getPaymentReceived());
//			blank2 = new Phrase(amountInWord + " Only.", font9);
//		} else {
//			blank2 = new Phrase(" ");
//		}
//
//		PdfPCell blank2cell = new PdfPCell(blank2);
//		blank2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank2cell.setBorderWidthTop(0);
//		blank2cell.setBorderWidthRight(0);
//		blank2cell.setBorderWidthLeft(0);
//
//		PdfPTable parent2table = new PdfPTable(4);
//		parent2table.addCell(rs1cell);
//		parent2table.addCell(blank1cell);
//		parent2table.addCell(rs2cell);
//		parent2table.addCell(blank2cell);
//		parent2table.setWidthPercentage(100);
//
//		try {
//			parent2table.setWidths(new float[] { 30, 15, 10, 45 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		try {
//			document.add(parent2table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//	}

	/***********************************************************************************/

//	public void createReceipt2Heading() {
//
//		Phrase pest = new Phrase("For Pest Control Branch ", font9);
//		PdfPCell pestcell = new PdfPCell(pest);
//		pestcell.setBorder(0);
//		pestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase blank1 = new Phrase(" ");
//
//		Phrase blank1 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			blank1 = new Phrase(custPayment.getBranch(), font9);
//		} else {
//			blank1 = new Phrase(" ");
//		}
//
//		PdfPCell blank1cell = new PdfPCell(blank1);
//		blank1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank1cell.setBorderWidthTop(0);
//		blank1cell.setBorderWidthRight(0);
//		blank1cell.setBorderWidthLeft(0);
//
//		PdfPTable centertable = new PdfPTable(2);
//		centertable.addCell(pestcell);
//		centertable.addCell(blank1cell);
//		centertable.setWidthPercentage(100f);
//
//		try {
//			centertable.setWidths(new float[] { 40, 60 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		Phrase blank12 = new Phrase(" ");
//		PdfPCell blank1cell1 = new PdfPCell(blank12);
//		blank1cell1.setBorder(0);
//
//		PdfPTable blanktble = new PdfPTable(1);
//		blanktble.addCell(blank1cell1);
//		blanktble.setWidthPercentage(100f);
//
//		PdfPCell centercell = new PdfPCell(centertable);
//		centercell.setBorder(0);
//
//		PdfPCell blankcll = new PdfPCell(blanktble);
//		blankcll.setBorder(0);
//
//		PdfPTable parent3table = new PdfPTable(2);
//		parent3table.addCell(centercell);
//		parent3table.addCell(blankcll);
//		parent3table.setWidthPercentage(100f);
//		try {
//			parent3table.setWidths(new float[] { 60, 40 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		try {
//			document.add(parent3table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//
//	}

//	public void createReceipt3Heading() {
//
//		Phrase vide = new Phrase("vide Cash / Cheque No. ", font9);
//		PdfPCell videcell = new PdfPCell(vide);
//		videcell.setBorder(0);
//		videcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase blank2 = new Phrase(" ");
//
//		Phrase blank2 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			if (custPayment.getPaymentMethod().equalsIgnoreCase("Cash")) {
//				blank2 = new Phrase("");
//			} else {
//				blank2 = new Phrase(custPayment.getChequeNo(), font9);
//			}
//
//		} else {
//			blank2 = new Phrase(" ");
//		}
//
//		PdfPCell blank2cell = new PdfPCell(blank2);
//		blank2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank2cell.setBorderWidthTop(0);
//		blank2cell.setBorderWidthRight(0);
//		blank2cell.setBorderWidthLeft(0);
//
//		Phrase dated = new Phrase("Dated", font9);
//		PdfPCell datedcell = new PdfPCell(dated);
//		datedcell.setBorder(0);
//		datedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase blank3 = new Phrase(" ");
//
//		Phrase blank3 = null;
//		if (custPayment.getStatus().equals("Closed")) 
//		{
//			if (custPayment.getPaymentMethod().equalsIgnoreCase("Cash")) {
//				blank3 = new Phrase("");
//			} 
//			else if(custPayment.getPaymentMethod().equalsIgnoreCase("Cheque"))
//			{
//				blank3 = new Phrase(fmt.format(custPayment.getChequeDate())+"", font9);
//			}
//			else
//			{
//				blank3 = new Phrase(" ", font9);
//			}
//
//		} else {
//			blank3 = new Phrase(" ");
//		}
//		PdfPCell blank3cell = new PdfPCell(blank3);
//		blank3cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		blank3cell.setBorderWidthTop(0);
//		blank3cell.setBorderWidthRight(0);
//		blank3cell.setBorderWidthLeft(0);
//
//		Phrase bank = new Phrase("Bank", font9);
//		PdfPCell bankcell = new PdfPCell(bank);
//		bankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		bankcell.setBorder(0);
//
//		// Phrase blank4 = new Phrase(" ");
//		Phrase blank4 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			if (custPayment.getBankName() != null) {
//				blank4 = new Phrase(custPayment.getBankName() + "", font9);
//			} else {
//				blank4 = new Phrase(" ");
//			}
//		} else {
//			blank4 = new Phrase(" ");
//		}
//
//		PdfPCell blank4cell = new PdfPCell(blank4);
//		blank4cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank4cell.setBorderWidthTop(0);
//		blank4cell.setBorderWidthRight(0);
//		blank4cell.setBorderWidthLeft(0);
//
//		PdfPTable parent4table = new PdfPTable(6);
//		parent4table.addCell(videcell);
//		parent4table.addCell(blank2cell);
//		parent4table.addCell(datedcell);
//		parent4table.addCell(blank3cell);
//		parent4table.addCell(bankcell);
//		parent4table.addCell(blank4cell);
//
//		parent4table.setWidthPercentage(100);
//
//		try {
//			parent4table.setWidths(new float[] { 20, 15, 10, 15, 10, 30 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		try {
//			document.add(parent4table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//	}

//	public void createReceipt4Heading() {
//
//		Phrase branch = new Phrase("Branch", font9);
//		PdfPCell branchcell = new PdfPCell(branch);
//		branchcell.setBorder(0);
//		branchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase blank5 = new Phrase(" ");
//		Phrase blank5 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			if (custPayment.getBankBranch() != null) {
//				blank5 = new Phrase(custPayment.getBankBranch() + "", font9);
//			} else {
//				blank5 = new Phrase(" ");
//			}
//		} else {
//			blank5 = new Phrase(" ");
//		}
//
//		PdfPCell blank5cell = new PdfPCell(blank5);
//		// blank5cell.setBorder(0);
//		blank5cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank5cell.setBorderWidthTop(0);
//		blank5cell.setBorderWidthRight(0);
//		blank5cell.setBorderWidthLeft(0);
//
//		Phrase name = new Phrase("  Name", font9);
//		PdfPCell namecell = new PdfPCell(name);
//		namecell.setBorder(0);
//		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase blank6 = new Phrase(" ");
//
//		Phrase blank6 = null;
//		if (custPayment.getStatus().equals("Closed")) {
//			if (!custPayment.getBankName().equals("")) {
//				blank6 = new Phrase(comp.getBusinessUnitName() + "", font9);
//			} else {
//				blank6 = new Phrase(" ");
//			}
//		} else {
//			blank6 = new Phrase(" ");
//		}
//		PdfPCell blank6cell = new PdfPCell(blank6);
//		// blank6cell.setBorder(0);
//		blank6cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blank6cell.setBorderWidthTop(0);
//		blank6cell.setBorderWidthRight(0);
//		blank6cell.setBorderWidthLeft(0);
//
//		PdfPTable parent5table = new PdfPTable(4);
//		parent5table.addCell(branchcell);
//		parent5table.addCell(blank5cell);
//		parent5table.addCell(namecell);
//		parent5table.addCell(blank6cell);
//		parent5table.setWidthPercentage(100f);
//		parent5table.setSpacingAfter(5f);
//
//		try {
//			parent5table.setWidths(new float[] { 10, 40, 10, 40 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		PdfPCell parentcell = new PdfPCell();
//		parentcell.addElement(parent5table);
//		parentcell.setBorderWidthTop(0);
//		parentcell.setBorderWidthRight(0);
//		parentcell.setBorderWidthLeft(0);
//
//		PdfPTable parenttable = new PdfPTable(1);
//		parenttable.setWidthPercentage(100f);
//		parenttable.addCell(parentcell);
//
//		try {
//			document.add(parenttable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//
//	}
	
	
	//   rohan added this method for om pest for new changes  
	
	public void createBaseInformation()
	{
		
		Paragraph para =new Paragraph("Payment Terms :",font8bold);
		para.setAlignment(Element.ALIGN_LEFT);
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(col1width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPCell commentsCell =null;
		PdfPCell payTermPercentageCell = null;
		PdfPCell daysCell =null;
		for(int i=0;i<con.getPaymentTermsList().size();i++)
		{
			Phrase comment = new Phrase(con.getPaymentTermsList().get(i).getPayTermComment(),font8);
			commentsCell = new PdfPCell(comment);
			commentsCell.setBorder(0);
			
			Phrase percentage = new Phrase(con.getPaymentTermsList().get(i).getPayTermPercent()+" %", font8);
			payTermPercentageCell = new PdfPCell(percentage);
			payTermPercentageCell.setBorder(0);
			
			
			Phrase days = new Phrase(con.getPaymentTermsList().get(i).getPayTermDays()+" Days",font8);
			daysCell = new PdfPCell(days);
			daysCell.setBorder(0);
			table.addCell(daysCell);
			table.addCell(payTermPercentageCell);
			table.addCell(commentsCell);
			
			
		}
		
		Paragraph notePara = new Paragraph("'SUCCESS OF OUR SERVICE DEPENDS ON YOUR CO-OPERATION'",font12bold);
		
		notePara.setAlignment(Element.ALIGN_CENTER);
		
		try {
			document.add(para);
			document.add(table);
			document.add(notePara);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	

	public void createTandNheading() {

		Phrase tn = new Phrase("\n"+"\n"+"Special Notes" + ":-", font9bold);
		PdfPCell tncell = new PdfPCell(tn);
		tncell.setBorder(0);
		tncell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String title1 = "";
		title1 = "- No Warranty of the service if full payment is not clear."+"\n"
				+"- Service tax amount is applicable as per the current  Government terms/percentage."+"\n"
				+"- Customer need to be get done every service on given frequency of the AMC period mentioned in the invoice, if not then the "+
				"\n"+"  service will be lapsed."+"\n"
				+"- Cheque payment should be in current date and should not be bounced for any reason then customer have to pay extra Rs. 200 "+"\n"+"  as Penalty charges plus Bank�s cheque bouncing charges also."+"\n"
				+"- In one time service there will be no guarantee / warranty or extra service provide by us.";
		
		Phrase tt1 = new Phrase(title1, font9);
		PdfPCell tt1cell = new PdfPCell(tt1);
		tt1cell.setBorder(0);
		tt1cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);

		PdfPTable tntable = new PdfPTable(1);
		tntable.addCell(tncell);
		tntable.addCell(tt1cell);
		tntable.setWidthPercentage(100f);

		PdfPCell cell5 = new PdfPCell(tntable);
		cell5.setBorder(0);

		PdfPTable table5 = new PdfPTable(1);
		table5.addCell(cell5);
		table5.setWidthPercentage(100);

		try {
			document.add(table5);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void createContractTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);

		try {
			table.setWidths(new float[] { 40, 18, 10, 28, 14 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase contarct = new Phrase("TYPE OF CONTRACT", font1);
		Phrase area = new Phrase("AREA / QUANTITY", font1);
		Phrase rate = new Phrase("RATE", font1);
		Phrase per = new Phrase("PER", font1);
		Phrase price = new Phrase("AMOUNT", font1);

		PdfPCell contarctcell = new PdfPCell(contarct);
		contarctcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell areacell = new PdfPCell(area);
		areacell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell ratecell = new PdfPCell(rate);
		ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell percell = new PdfPCell(per);
		percell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(contarctcell);
		table.addCell(areacell);
		table.addCell(ratecell);
		table.addCell(percell);
		table.addCell(pricecell);

		for (int i = 0; i < this.invoiceentity.getSalesOrderProducts().size(); i++) 
		{
			chunk = new Phrase(invoiceentity.getSalesOrderProducts().get(i)
					.getProdName()
					+ "", font8);
			pdftype = new PdfPCell(chunk);
			pdftype.setHorizontalAlignment(Element.ALIGN_CENTER);

			System.out.println("rohan invoice "+this.invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber());
			for(int j=0 ;j< con.getItems().size(); j++){
				System.out.println("rohan product "+con.getItems().get(j).getProductSrNo()+"-"+con.getItems().get(j).getPremisesDetails());
			if(this.invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber()==con.getItems().get(j).getProductSrNo())
			{
				chunk = new Phrase(con.getItems().get(j).getPremisesDetails(), font8);
			
			
			
			pdfarea = new PdfPCell(chunk);
			pdfarea.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(invoiceentity.getSalesOrderProducts().get(i).getPrice())
					+ "", font8);
			
			pdfrate = new PdfPCell(chunk);
			pdfrate.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(" ", font8);
			pdfper = new PdfPCell(chunk);
			pdfper.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(invoiceentity.getSalesOrderProducts().get(i)
					.getTotalAmount())
					+ "", font8);
			pdftprice = new PdfPCell(chunk);
			pdftprice.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdftype);
			table.addCell(pdfarea);
			table.addCell(pdfrate);
			table.addCell(pdfper);
			table.addCell(pdftprice);
			}
			}
		}

		Phrase blankTB = new Phrase(" ", font10);
		PdfPCell blankTBcell = new PdfPCell(blankTB);
		blankTBcell.setBorderWidthTop(0);
		blankTBcell.setBorderWidthBottom(0);

		Phrase blankLR = new Phrase(" ", font10);
		PdfPCell blankLRcell = new PdfPCell(blankLR);
		blankLRcell.setBorderWidthLeft(0);
		blankLRcell.setBorderWidthRight(0);
		
		Phrase taxblank1 = new Phrase(" ", font10);
		PdfPCell taxblankcell1 = new PdfPCell(taxblank1);
		taxblankcell1.setBorderWidthRight(0);
		//    code for sub total 
		
		Phrase subtotal = new  Phrase("SUB TOTAL",font8bold);
		PdfPCell subtotalCell =new PdfPCell(subtotal);
		subtotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		subtotalCell.setBorderWidthLeft(0);
		double total=0;
		for (int i = 0; i < this.invoiceentity.getSalesOrderProducts().size(); i++) {
			total =total+this.invoiceentity.getSalesOrderProducts().get(i).getTotalAmount();
		}
		
		chunk = new Phrase(df.format(total), font8bold);
		PdfPCell subtotalAmtCell = new PdfPCell(chunk);
		subtotalAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(blankTBcell);
		table.addCell(taxblankcell1);
		table.addCell(blankLRcell);
		table.addCell(subtotalCell);
		table.addCell(subtotalAmtCell);

		if (invoiceentity.getBillingTaxes().size() != 0) {
			Phrase taxblank = new Phrase(" ", font10);
			PdfPCell taxblankcell = new PdfPCell(taxblank);
			taxblankcell.setBorderWidthRight(0);

			PdfPCell pdftaxCell = null;
			PdfPCell pdftaxAmtCell = null;
			

			for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {
System.out.println("invoiceentity.getBillingTaxes().size()"+invoiceentity.getBillingTaxes().size()+"-"+invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()+"-"+invoiceentity.getBillingTaxes().get(i).getPayableAmt());
//		rohan added code for tax byfurgation 


if((invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equals("VAT")||invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equals("CST"))&& (invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()!=0.0)){
	
	System.out.println("1st loop"+invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent());
	
	String str = invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent();
	Phrase strPhrase = new Phrase(str,font8);
	PdfPCell strCell = new PdfPCell(strPhrase);
	strCell.setBorderWidthLeft(0);
	strCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	double taxAmt1 = invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()*invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
	 
	Phrase taxAmt1Phrase =new Phrase(df.format(taxAmt1),font1);
	PdfPCell taxAmt1Cell = new PdfPCell(taxAmt1Phrase);
	taxAmt1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	table.addCell(blankTBcell);
	table.addCell(taxblankcell);
	table.addCell(blankLRcell);
	table.addCell(strCell);
	table.addCell(taxAmt1Cell);
	
}



if((invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()!=0.0)&&(invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&& invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()==15)){	
	
	 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+invoiceentity.getBillingTaxes().get(i).getTaxChargeName());
	
	System.out.println("2nd loop 5678== "+invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent());
	
	String str = "Service Tax "+ " @ "+" 14%";
	Phrase phrase =new Phrase(str,font1);
	PdfPCell pdfpCell = new PdfPCell(phrase);
	pdfpCell.setBorderWidthLeft(0);
	pdfpCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	double taxAmt1 = 14* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
	String blidnk=" ";
	
	
	Phrase myPh =new Phrase(df.format(taxAmt1),font1);
	PdfPCell myPhCell = new PdfPCell(myPh);
	myPhCell.setHorizontalAlignment(Element.ALIGN_CENTER);

	
	table.addCell(blankTBcell);
	table.addCell(taxblankcell);
	table.addCell(blankLRcell);
	table.addCell(pdfpCell);
	table.addCell(myPhCell);
	
	
	
	 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
	 Phrase str123Ph = new Phrase(str123,font1);
	 PdfPCell str123Cell = new PdfPCell(str123Ph);
	 str123Cell.setBorderWidthLeft(0);
	 str123Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 
	 double taxAmt = 0.5* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
	 Phrase taxAmtPh = new Phrase(df.format(taxAmt),font1);
	 PdfPCell taxAmtCell = new PdfPCell(taxAmtPh);
	 taxAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 
	 	table.addCell(blankTBcell);
		table.addCell(taxblankcell);
		table.addCell(blankLRcell);
		table.addCell(str123Cell);
		table.addCell(taxAmtCell);
	 
		 String kkc = "Krishi Kalyan Cess"+ " @ "+" 0.5%";
		 Phrase kkcPh = new Phrase(kkc,font1);
		 PdfPCell kkcCell = new PdfPCell(kkcPh);
		 kkcCell.setBorderWidthLeft(0);
		 kkcCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
		 double kkctaxAmt = 0.5* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
		 Phrase kkctPh = new Phrase(df.format(kkctaxAmt),font1);
		 PdfPCell kkctPhCell = new PdfPCell(kkctPh);
		 kkctPhCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		 
			table.addCell(blankTBcell);
			table.addCell(taxblankcell);
			table.addCell(blankLRcell);
			table.addCell(kkcCell);
			table.addCell(kkctPhCell);
		
}
 
else if((invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()!=0.0)&& (invoiceentity.getBillingTaxes().get(i).getTaxChargeName().equals("Service Tax")&& invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()==14.5)){

 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+invoiceentity.getBillingTaxes().get(i).getTaxChargeName());

System.out.println("2nd loop 890== "+invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent());

String str = "Service Tax "+ " @ "+" 14%";
Phrase strPh = new Phrase(str,font1);
PdfPCell strPhCell = new PdfPCell(strPh);
strPhCell.setBorderWidthLeft(0);
strPhCell.setBorderWidthTop(0);
strPhCell.setBorderWidthBottom(0);

double taxAmt1 = 14* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
Phrase taxAmt1Ph = new Phrase(df.format(taxAmt1),font1);
PdfPCell taxAmt1Cell = new PdfPCell(taxAmt1Ph);
taxAmt1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

String blidnk=" ";

table.addCell(blankTBcell);
table.addCell(taxblankcell);
table.addCell(blankLRcell);
table.addCell(strPhCell);
table.addCell(taxAmt1Cell);
 
 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
 Phrase str123Ph = new Phrase(str123,font1);
 PdfPCell str123Cell = new PdfPCell(str123Ph);
 str123Cell.setBorderWidthLeft(0);
 
 double taxAmt = 0.5* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
 Phrase taxAmtPh = new Phrase(df.format(taxAmt),font1);
 PdfPCell taxAmtCell = new PdfPCell(taxAmtPh);
 taxAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
 
 table.addCell(blankTBcell);
 table.addCell(taxblankcell);
 table.addCell(blankLRcell);
 table.addCell(str123Cell);
 table.addCell(taxAmtCell);
 
 
}

else if(invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()!=0.0){
	
 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+invoiceentity.getBillingTaxes().get(i).getTaxChargeName());

System.out.println("2nd loop 1234 == "+invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent());

String str = invoiceentity.getBillingTaxes().get(i).getTaxChargeName().trim()+ " @ "+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent();
Phrase strPh = new Phrase(str,font1);
PdfPCell strCell = new PdfPCell(strPh);
strCell.setBorderWidthLeft(0);

double taxAmt1 = invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()
		* invoiceentity.getBillingTaxes().get(i).getTaxChargeAssesVal() / 100;
Phrase taxAmt1Ph = new Phrase(df.format(taxAmt1),font1);
PdfPCell taxAmt1Cell = new PdfPCell(taxAmt1Ph);
taxAmt1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);


table.addCell(blankTBcell);
table.addCell(taxblankcell);
table.addCell(blankLRcell);
table.addCell(strCell);
table.addCell(taxAmt1Cell);

} 

//   changes ends here 





//   old code commented by rohan

//      if (invoiceentity.getBillingTaxes().get(i).getTaxChargePercent() != 0) {
//
//		chunk = new Phrase(invoiceentity.getBillingTaxes().get(i).getTaxChargeName()+ " @ "+invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()+"%", font8);
//					pdftaxCell = new PdfPCell(chunk);
//					pdftaxCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//					pdftaxCell.setBorderWidthLeft(0);
//					
//					chunk = new Phrase(df.format(invoiceentity.getBillingTaxes().get(i)
//							.getPayableAmt()), font8);
//					pdftaxAmtCell = new PdfPCell(chunk);
//					pdftaxAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//					
//					table.addCell(blankTBcell);
//					table.addCell(taxblankcell);
//					table.addCell(blankLRcell);
//					table.addCell(pdftaxCell);
//					table.addCell(pdftaxAmtCell);
//				}
			}

			
			Phrase blankT = new Phrase(" ", font8);
			PdfPCell blankTcell = new PdfPCell(blankT);
			blankTcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			blankTcell.setBorderWidthTop(0);

			Phrase blankR = new Phrase(" ", font8);
			PdfPCell blankRcell = new PdfPCell(blankR);
			blankRcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			blankRcell.setBorderWidthRight(0);

			if((invoiceentity.getDiscount()!=0))
			{
			Phrase discount = new Phrase("DISCOUNT", font8bold);
			PdfPCell discountCell = new PdfPCell(discount);
			discountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountCell.setBorderWidthLeft(0);

			chunk = new Phrase(invoiceentity.getDiscount()+"", font8bold);
			PdfPCell discAmtCell = new PdfPCell(chunk);
			discAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(blankTBcell);
			table.addCell(taxblankcell);
			table.addCell(blankLRcell);
			table.addCell(discountCell);
			table.addCell(discAmtCell);
			
			}
			

			Phrase tamt = new Phrase("TOTAL", font8bold);
			PdfPCell tamtcell = new PdfPCell(tamt);
			tamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tamtcell.setBorderWidthLeft(0);

			chunk = new Phrase(df.format(invoiceentity.getInvoiceAmount()), font8bold);
			PdfPCell pdfttlamt = new PdfPCell(chunk);
			pdfttlamt.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(blankTcell);
			table.addCell(blankRcell);
			table.addCell(blankLRcell);
			table.addCell(tamtcell);
			table.addCell(pdfttlamt);

		}

		// Phrase stax = new Phrase("SERVICE TAX",font8);
		// PdfPCell staxcell = new PdfPCell(stax);
		// staxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// Phrase tamt = new Phrase("TOTAL",font8);
		// PdfPCell tamtcell = new PdfPCell(tamt);
		// tamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		//
		// if((invoiceentity.getSalesOrderProducts().get(0).getServiceTax()
		// !=null)){
		//
		// chunk = new
		// Phrase(invoiceentity.getSalesOrderProducts().get(0).getServiceTax()+"");
		// pdftaxpercent = new PdfPCell(chunk);
		// pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// }
		//
		// table.addCell(blankcell);
		// table.addCell(staxcell);
		// table.addCell(blankcell);
		// table.addCell(blankcell);
		// table.addCell(pdftaxpercent);
		// // table.addCell(blankcell);
		//
		//
		// chunk = new Phrase(invoiceentity.getInvoiceAmount()+"",font8);
		// pdfttlamt = new PdfPCell(chunk);
		// pdfttlamt.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// // String
		// amountInWord=ServiceInvoicePdf.convert(invoiceentity.getInvoiceAmount());
		// // Phrase inwords = new Phrase(amountInWord,font8);
		// // PdfPCell inwordscell = new PdfPCell(inwords);
		// // inwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// table.addCell(blankcell);
		// table.addCell(blankcell);
		// table.addCell(blankcell);
		// table.addCell(tamtcell);
		// table.addCell(pdfttlamt);
		// // table.addCell(blankcell);

		PdfPCell contcell = new PdfPCell();
		contcell.addElement(table);
		contcell.setBorder(0);

		PdfPTable parent = new PdfPTable(1);
		parent.addCell(contcell);
		parent.setWidthPercentage(100);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	
   	public String getFunnctionalYear(Date date)
	{
		
		System.out.println("in side methos");
		
		
		
		 Calendar now = Calendar.getInstance();
		 
		 
		 now.setTime(date);
//		 now.add(Calendar.DATE, 360);
		 
	     int year=now.get(Calendar.YEAR);
	     
	     int month = now.get(Calendar.MONTH)+1;
	     System.out.println("year "+year);
	     System.out.println("Month "+month);
	     
	     
	     String stmonthValue = comp.getStartMonth();
	     int monthValue=0;
	     
	     if(stmonthValue.equals("JAN"))
	     {
	    	 monthValue=1;
	     }
	     if(stmonthValue.equals("FEB"))
	     {
	    	 monthValue=2;
	     }
	     if(stmonthValue.equals("MAR"))
	     {
	    	 monthValue=3;
	     }
	     if(stmonthValue.equals("APR"))
	     {
	    	 monthValue=4;
	     }
	     if(stmonthValue.equals("MAY"))
	     {
	    	 monthValue=5;
	     }
	     if(stmonthValue.equals("JUN"))
	     {
	    	 monthValue=6;
	     }
	     if(stmonthValue.equals("JUL"))
	     {
	    	 monthValue=7;
	     }
	     if(stmonthValue.equals("AUG"))
	     {
	    	 monthValue=8;
	     }
	     if(stmonthValue.equals("SEP"))
	     {
	    	 monthValue=9;
	     }
	     if(stmonthValue.equals("OCT"))
	     {
	    	 monthValue=10;
	     }
	     if(stmonthValue.equals("NOV"))
	     {
	    	 monthValue=11;
	     }
	     if(stmonthValue.equals("DEC"))
	     {
	    	 monthValue=12;
	     }
	     
	     System.out.println("month value form company "+monthValue);
	     
	     if (month < monthValue) {
	         System.out.println("Financial Year : " + (year - 1) + "-" + year);
	         functionalYear=((year - 1) + "-" + year);
	     } else {
	         System.out.println("Financial Year : " + year + "-" + (year + 1));
	         functionalYear=year + "-" + (year + 1);
	     }
	     
	     return functionalYear;
	}

   	
   	/**
   	 * Rohan added this method for sending customize document in Invoice Email Pdf
   	 * @param invDetails
   	 * @param comp2
   	 * @param c
   	 * @param servcont
   	 * All the parameters comes form Email 
   	 * 
   	 * This method is used for creating pdf in invoice Email  
   	 */
	public void createPdfForEmail(Invoice invDetails, Company comp2,
			Customer c, Contract servcont) {
		
		invoiceentity = invDetails;
		comp = comp2;
		cust = c;
		con = servcont;
		
		if (invoiceentity.getCompanyId() != null)
			service = ofy().load().type(Service.class)
					.filter("contractCount", invoiceentity.getContractCount()).list();
		else
			service = ofy().load().type(Service.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("contractCount", invoiceentity.getContractCount()).list();
		
		System.out.println("Service list size "+service.size());

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		
	} 

}
