package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class CreatePcambIntimationLetterpdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3802790338179030724L;

	

	protected  void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException ,IOException 
	  {
		    response.setContentType("application/pdf");
	  
	  try{
		   String stringid = request.getParameter("Id");
			stringid = stringid.trim();            
			Long count = Long.parseLong(stringid);  
			   
			PcambIntimationLetterpdf pdf = new PcambIntimationLetterpdf();
			
			pdf.document= new Document();
			  Document document = pdf.document;
			  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
			 
			  document.open();
			  pdf.setIntimationLetter(count);
			  pdf.createPdf(); 
			  document.close();			    
	  }catch (DocumentException e) {
		  e.printStackTrace();
	  }
	}
}
