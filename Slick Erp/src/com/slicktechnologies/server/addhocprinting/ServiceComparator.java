package com.slicktechnologies.server.addhocprinting;

import java.util.Comparator;

import com.slicktechnologies.shared.Service;



public class ServiceComparator implements Comparator<Service> {

	@Override
	public int compare(Service o1, Service o2) {
		return o1.getServiceDate().compareTo(o2.getServiceDate());
	}

}
