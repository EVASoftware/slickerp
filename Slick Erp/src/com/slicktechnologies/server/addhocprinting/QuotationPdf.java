package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.FormField;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.BahtText;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class QuotationPdf {
	int var = 0;
	Sales qp;
	ArrayList<Service> service;
	List<SalesLineItem> products;
	List<PaymentTerms> payTermsLis;
	List<ProductOtherCharges> prodCharges;
	List<ProductOtherCharges> prodTaxes;
	Customer cust;
	SuperProduct sup;
	Company comp;
	Quotation quotEntity;
	Contract contEntity;
	List<ArticleType> articletypecust;// Date8/12/2017 By Jayshree Change the
										// Name
	List<ArticleType> articletypecomp;// Date8/12/2017 By Jayshree Change the
										
	ArrayList <ArticleType> branchWiseFilteredArticleList;
	
	
	boolean nubofprod = false;// Date 28/12/2017 By Jayshree To set the no of
								// product
	double total = 0;
	public Document document;
	public GenricServiceImpl impl;
	boolean upcflag = false;
	/**Date 26-6-2020 by Amol address and cell no should be print from customer branch if customer branch is selected raised by
	 Rahul Tiwari for Narmdada pest Control***/
	boolean addcellNoFlag = false;
	boolean rexFlag = false;
	/**
	 * Rahul Added this for Number of line management on 25 Nov 2017
	 */
	int noOfLines = 10;// Date 8/12/2017 By Jayshree change no of lines 12 to 10
	int prouductCount = 0;
	/**
	 * ENDS
	 */

	// rohan added this flag for printing quotation desc on first page
	boolean printDescOnFirstPageFlag = false;

	boolean printProductPremisesFlag = false;
	boolean printProductRemarkWithPremiseFlag = false; //Ashwini Patil Date:5-04-2024 Ultra pest want to print remark on premise line
	boolean doNotPrintBankDetails = false;

	// rohan added this flag for friends pest control
	boolean onlyForFriendsPestControl = false;
	//

	/**
	 * rohan added this flag for universal pest control This is used to print
	 * vat no and other article information
	 */

	Boolean UniversalFlag = false;
	
	/**
	 * ends here
	 */

	/**
	 * Rohan added this for Universal pest for printing
	 */
	Boolean multipleCompanyName = false;
	/**
	 * ends here
	 */
	/**
	 * Date 12/1/2018
	 * Dev By Jayshree;
	 * 1)To Check the process congigration for company email and Branch email
	 * 2)To Check the process congigration for company address and Branch Address
	 */
	boolean checkEmailId=false;
	boolean contractTypeAsPremisedetail = false;
	boolean hoEmail = false;
	boolean headerWithOnlycompanyDetail = false;
	boolean printAttnInPdf = false;
	boolean checkheaderLeft = false;
	boolean checkheaderRight = false;
	/***Date 5-2-2019 
	 * added bt amol*****/
	boolean hideGstinNo=false;
	float[] columnHalfWidth = { 1f, 1f };
	float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
	float[] columnStateCodeCollonWidth = { 3.5f, 2f, 0.2f, 1f };
	/**
	 * Ends for Jayshree
	 */

	List<State> stateList;

	// ******************rohan taken variables ************8
	int both = 0;
	int discAmt = 0;
	int discPer = 0;
	int nothing = 0;
	ArrayList<ProductOtherCharges> productChargeArr=new ArrayList<ProductOtherCharges>();
	ProcessConfiguration processConfig;
	public int serviceno = 1;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font11bold, font7, font10bold,// font10,
			font12boldul, font12, font14bold, font9, font6bold, font12boldUnderline,font16bold,	font11,font10,font7bold,font13,font11Regular;

	
	/** Total Amount */
	// Added by Ajinkya
	double totalAmount;
	Phrase blankCell = new Phrase(" ", font8); // Added by Ajinkya

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	Logger logger = Logger.getLogger("NameOfYourLogger");
	ArrayList<SuperProduct> stringlis = new ArrayList<SuperProduct>();
	CompanyPayment comppayment;
	int firstBreakPoint = 6;// By jayshree Date 21/11/2017 to check the first
							// page products
	Phrase chunk;
	int flag = 0;
	int count = 0;
	float blankLines;

	// float[] columnWidths7 = { 0.5f, 2.5f, 1.0f, 2.0f, 1.0f, 1.0f };//
	// Commmented by Jayshree
	/** Updated By: Viraj Date: 15-03-2019 Description: Changed column size of discount and amount **/
	float[] columnWidths7 = { 0.4f, 2.6f, 0.5f, 1.7f, 0.6f, 0.8f ,0.6f,0.8f};// Add BY
																	// Jayshree0.5f, 3.0f, 0.9f, 2.0f, 0.8f, 0.8f

	float[] columnWidths = { 0.5f, 2.4f, 1.5f, 1.0f, 1.0f, 1.0f, 0.8f, 0.7f,
			1.2f, 0.6f, 0.8f };

	float[] columnWidthsfordicsAMt = { 0.6f, 2.0f, 1.1f, 0.8f, 1.0f, 1.2f,
			1.0f, 0.8f, 1.1f, 0.8f, 0.8f, 0.8f };

	float[] columnWidths1 = { 0.5f, 1f, 2f, 4f };
	float[] columnWidths2 = { 1.2f, 0.2f, 1.5f, 1.2f, 0.2f, 1.5f };
	float[] columnWidths3 = { 1.2f, 0.2f, 2.5f, 1.2f, 0.2f, 0.5f };// Date
																	// 9/12/2017
																	// By
																	// Jayshree
																	// Set The
																	// column
																	// width
	float[] column16CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.3f };
	float[] columnWidthsForSTandEndDate = { 4f, 3f, 4f };

	//DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df = new DecimalFormat("#,###.00");
	DecimalFormat decimalformat = new DecimalFormat("0.00"); //Ashwini Patil

	final static String disclaimerText = "I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";

	/**
	 * Date 16-01-2018 By Vijay
	 * For do not Product Description and do not service details process config flag
	 */
	boolean DoNotProductDescriptionFlag=false;
	boolean DoNotProductServicesTableFlag=false;
	/**
	 * ends here
	 */
	/** date 06-02-2018 added by komal for consolidate price **/
	boolean consolidatePrice = false;
	/**
	 * nidhi
	 * 9-08-2018
	 * for print serial no on pdf
	 */
	boolean printModelSerailNoFlag =false;
	
	/**
	 * Date 22-11-2018 By Vijay 
	 * Des :- if process configuration is active then GST Number will not display
	 * and GST Number will display if GST applicable or not applicable as per nitin sir
	 */
	boolean gstNumberPrintFlag = true;
	
	
	/**
	 * @author Anil , Date : 02-03-2019
	 * Printing product description if process configuration is active 
	 */
	boolean productDescFlag=false;
	
	/**
	 * @author Abhinav Bihade
	 * @since 20/12/2019
	 * As per Rahul Tiwari's Requirement 
	 * Orkin : Hide Rate and Discount column from contract and Tax Invoice pdf 
	 */
	boolean hideRateAndDiscount=false;
	Branch branchDt = null;
	
	
	CustomerBranchDetails customerBranch=null;
//	float[] columnWidths9 = { 0.4f, 2.6f, 0.5f, 0.6f, 0.8f ,0.6f, 0.9f, 0.8f, 0.8f};

	float[] columnWidths9 = { 0.4f, 2.6f, 0.5f, 0.6f, 1.0f ,0.6f, 1f, 0.7f, 0.8f};
	
	float[] columnWidths10 = { 0.4f, 2.6f, 0.5f, 0.5f, 0.6f, 0.8f ,0.6f, 0.9f, 0.8f, 0.8f};

	boolean donotprintTotalflag = false;

//	ArrayList<String> custbranchlist;
	
	
//	/**
//	 * @author niles
//	 * @since 14-07-2020
//	 * For sai care raised by Vaishnavi and Nitin sir
//	 * Hide total amount, tax details and net payable
//	 */
//	boolean hideTotalAmountDetailsFlag=false;
	
	
	
	/**
	 * @author Abhinav Bihade
	 * @since 05/02/2020
	 *As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
	 * in case if somebody manages 2 companies under same link of ERP s/w
	 */
	
	private ArrayList<ArticleType> getArticleBranchList(ArrayList<ArticleType> articleTypeDetails, String branch) {
		ArrayList<ArticleType> artilist =new ArrayList<ArticleType>();
		for(ArticleType at:articleTypeDetails){
			logger.log(Level.SEVERE,"at.getArticleDescription() " +at.getArticleDescription()+" / "+branch);
//			if(at.getArticleDescription().equals(branch)){
//				artilist.add(at);
//				logger.log(Level.SEVERE,"Inside Branch As Company Method:" +artilist);
//			}
			if(at.getArticleDescription()!=null && !at.getArticleDescription().equals("") ){
				if(at.getArticleDescription().equals(branch)){
					artilist.add(at);
				}
				
				logger.log(Level.SEVERE,"Inside Branch As Company Method:" +artilist);
			}
			else{
				artilist.add(at);
			}
			
			
		}
		
		return artilist;
		
	}

	/**
	 * @author Vijay Chougule Date - 28-07-2020
	 * Des :- For PSTPL to calculate Total no of services and print on invoice 
	 */ 
	 boolean complainServiceWithTurnAroundTimeFlag = false;

	/**
	 * @author Anil @since 22-07-2021
	 * Thai font flag
	 */
	boolean thaiFontFlag=false;
	
	boolean pc_RemoveBankDetail=false;
	
	PdfUtility pdfUtility=new PdfUtility();
	
	boolean pocNameFlag = false; //
	boolean custbranchmailFlag = false;
	
	String qtylabel="Qty";
	
	ServerAppUtility serverAppUtility = new ServerAppUtility();
	
	boolean AmountInWordsHundreadFormatFlag=false;
	boolean PC_RemoveSalutationFromCustomerOnPdfFlag = false;
	
	//By Ashwini Patil
	List<CustomerBranchDetails> customerBranchList;
	List<CustomerBranchDetails> selectedBranches;
	boolean multipleServiceBranchesFlag=false;	
	HashMap<String,ArrayList<String>> branchWiseProductMap=new HashMap<String,ArrayList<String>>();//27-12-2022
	
	

	//copied from ServiceGSTInvoice by Ashwini Patil
	BahtText bahtText=new BahtText();
	boolean thaiPdfFlag=false;
//	boolean pageBreakFlag=false;
//	List<CompanyPayment> compPayList=null;	
//	String invoiceTitle="";
//	String copyTitle="";
	String companyCountry="";

	
	boolean PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag = false;
	
	Font nameAddressBoldFont=new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);//Ashwini Patil
	Font nameAddressFont=new Font(Font.FontFamily.HELVETICA, 7);//Ashwini Patil
	Font nameAddressFont6 = new Font(Font.FontFamily.HELVETICA, 8);
	
	
	boolean PC_DoNotPrintQtyFlag= false;
	boolean PC_DoNotPrintContractPeriodFlag = false;
	boolean PC_DoNotPrintRateFlag = false;
	boolean PC_DoNotPrintDiscFlag = false;

	boolean PC_DoNotPrintWarrantyFlag = false;
	boolean reduceHeaderSpaceFlag = false;
	
	boolean qtyColumnFlag = false;
	boolean unitOfMeasurementColumnFlag = false;

	boolean isSociety=false;
	
	public QuotationPdf() {

		// Date 16/11/2017
		// By jayshree
		// font size increases by one
		font16boldul = new Font(Font.FontFamily.HELVETICA, 17, Font.BOLD);
		// new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);// Change
																		// font
																		// size
																		// by
																		// jayshree
		font8 = new Font(Font.FontFamily.HELVETICA, 8);// Change font size by
														// jayshree
		font9 = new Font(Font.FontFamily.HELVETICA, 8);// Change font size by
														// jayshree
		font12boldul = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 13);
		font11bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		// font10=new Font(Font.FontFamily.HELVETICA,10,Font.NORMAL);
		font7 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);// Change
																		// font
																		// size
																		// by
																		// jayshree
		font6bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font11 = new Font(Font.FontFamily.HELVETICA, 10);
		font10 = new Font(Font.FontFamily.HELVETICA, 7);
		font13 = new Font(Font.FontFamily.HELVETICA, 9);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font11Regular = new Font(Font.FontFamily.HELVETICA, 11);
		// End
		impl = new GenricServiceImpl();

		/**
		 * Date : 21-08-2017 by Anil
		 */
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		/**
		 * End
		 */
		
		/** Date 17-01-2018 By vijay ***/
		font12boldUnderline = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD| Font.UNDERLINE);


	}

	// PLASE TO APPLY COMPANY SPECIFEC SETTINGS

	public void getContract(long count, String preprint) {

		System.out.println("print status =======" + preprint);
		// Load Contract

		qp = ofy().load().type(Contract.class).id(count).now();
		
		
		if (qp.getCompanyId() != null) {
			
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
						thaiFontFlag=true;
//						break;
					}
					if(obj.getProcessType().equalsIgnoreCase(AppConstants.PC_AMOUNTINWORDSHUNDREADSTRUCTURE)&&obj.isStatus()==true){
						AmountInWordsHundreadFormatFlag = true;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font16boldul = new Font(boldFont, 17);
				font12bold = new Font(boldFont, 13);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font9 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 13);
				font12 = new Font(regularFont, 13);
				font11bold = new Font(boldFont, 12);
				font10bold = new Font(boldFont, 11);
				font7 = new Font(regularFont, 8, Font.NORMAL);
				font14bold = new Font(boldFont, 15);
				font9bold = new Font(boldFont, 8);
				font6bold = new Font(boldFont, 7);
				
				font16bold = new Font(boldFont, 16);
				font11 = new Font(regularFont, 10);
				font10 = new Font(regularFont, 7);
				font13 = new Font(regularFont, 9);
				font7bold = new Font(boldFont, 7);
				
				BaseFont tahomaFont=BaseFont.createFont("Tahoma Regular font.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahomaBoldFont=BaseFont.createFont("TAHOMAB0.TTF",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				nameAddressFont=new Font(tahomaFont, 9);
				nameAddressBoldFont=new Font(tahomaFont, 9);
				nameAddressFont6=new Font(tahomaFont, 7);
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
		
		// makeServices();
		// Load Customer
		if (qp.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId())
					.filter("companyId", qp.getCompanyId()).first().now();
		if (qp.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();
		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();
		

		if (qp.getCompanyId() != null)
			contEntity = ofy().load().type(Contract.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count", qp.getCount()).first().now();
		else
			contEntity = ofy().load().type(Contract.class)
					.filter("count", qp.getCount()).first().now();
		
		
		/*@author Abhinav Bihade
		 * @since 20/01/2020
		 *As per Rahul Tiwari's Requirement Bitco : Print customer branch address in contract Pdf Tested by Sonu Porel 
		 */
		
		
		ArrayList<String> custbranchlist=getCustomerBranchList(contEntity.getItems());
	
	/**
	 * @author Ashwini Patil
	 * @since 20-01-2022
	 * to manage multiple branch selected
	 */
	if(custbranchlist!=null){
		if(custbranchlist.size()==1){
			if(custbranchlist.contains("Service Address")==false){
				logger.log(Level.SEVERE,"In Side AList1:");
				customerBranch= ofy().load().type(CustomerBranchDetails.class)
							.filter("cinfo.count",contEntity.getCinfo().getCount())
							.filter("companyId", contEntity.getCompanyId())
							.filter("buisnessUnitName", custbranchlist.get(0)).first().now();
				
				logger.log(Level.SEVERE,"AList1:" +customerBranch);
				logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());				
			}
		}
		else{
			System.out.println("there are multiple service branches");
			multipleServiceBranchesFlag=true;
			selectedBranches = getSelectedBranches(contEntity);
			if(!custbranchlist.contains("Service Address")){
				if(selectedBranches!=null){
					customerBranch=selectedBranches.get(0);
					System.out.println("customerBranch= "+customerBranch.getBusinessUnitName());
				}
			}
			if(selectedBranches!=null){
				List<SalesLineItem> itemList=contEntity.getItems();
				for(CustomerBranchDetails branch:selectedBranches){
					System.out.println("ashwini selectedBranche="+branch.getBusinessUnitName());
					HashSet<String> productSet=new HashSet<String>();
					for(SalesLineItem itemObj:itemList){
						System.out.println("ashwini item in for="+itemObj.getProductName());
						if(itemObj.getCustomerBranchSchedulingInfo()!=null){
							System.out.println("ashwini itemObj.getCustomerBranchSchedulingInfo()!=null");
							ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
							for(BranchWiseScheduling obj:branchSchedulingList){
								System.out.println("obj.getBranchName()="+obj.getBranchName()+" ischeck"+obj.isCheck());
								if(obj.isCheck()==true&&obj.getBranchName().equals(branch.getBusinessUnitName())){
									productSet.add(itemObj.getProductName());
								}
							}
						}
					}
					ArrayList<String> productList=new ArrayList<String>(productSet);
					branchWiseProductMap.put(branch.getBusinessUnitName(), productList);
				}
				HashSet<String> productSet=new HashSet<String>();
				for(SalesLineItem itemObj:itemList){
					System.out.println("ashwini item in for="+itemObj.getProductName());
					if(itemObj.getCustomerBranchSchedulingInfo()!=null){
						System.out.println("ashwini itemObj.getCustomerBranchSchedulingInfo()!=null");
						ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
						for(BranchWiseScheduling obj:branchSchedulingList){
							System.out.println("obj.getBranchName()="+obj.getBranchName()+" ischeck"+obj.isCheck());
							if(obj.isCheck()==true&&obj.getBranchName().equals("Service Address")){
								productSet.add(itemObj.getProductName());
							}
						}
					}
				}
				ArrayList<String> productList=new ArrayList<String>(productSet);
				branchWiseProductMap.put("Service Address", productList);
				
			}
			
		}
	}
			
	
	
		
		
	/**
	 * @author Abhinav Bihade
	 * @since 05/02/2020
	 *As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
	 * in case if somebody manages 2 companies under same link of ERP s/w
	 */
	String branchName="";
	if(contEntity.getBranch()!=null){
		branchName=contEntity.getBranch();
	}
//	else if(quotEntity.getBranch()!=null){
//		branchName=quotEntity.getBranch();
//	}
	
//	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
//		branchWiseFilteredArticleList =getArticleBranchList(comp.getArticleTypeDetails(),branchName);
//		logger.log(Level.SEVERE,"Inside Branch As Company1: " +branchWiseFilteredArticleList.size());
//	}
			
		
		/**
		 * 
		 * 
		 * 
		 */
		
		

		stateList = ofy().load().type(State.class)
				.filter("companyId", contEntity.getCompanyId()).list();

		/************************************ discount Flag *******************************/

		// rohan added this code for payment details
		if (qp.getCompanyId() != null) {
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", qp.getCompanyId()).first().now();
		}
		// ends here

		if (qp.getCompanyId() != null) {
			for (int i = 0; i < contEntity.getItems().size(); i++) {
				if ((contEntity.getItems().get(i).getDiscountAmt() > 0)
						&& (contEntity.getItems().get(i)
								.getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (contEntity.getItems().get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (contEntity.getItems().get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}

			}
		}
		System.out.println("Disc nothing" + nothing + "-per" + discPer + "-AMt"
				+ discAmt + "-both" + both);

		/************************************ Letter Head Flag *******************************/

		if (qp.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Contract")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;

					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAddressAndCellFromCustomerBranch")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						addcellNoFlag = true;

					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyNameFontSize10")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						rexFlag = true;
					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"printDescOnFirstPageFlagForContractAndQuotation")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printDescOnFirstPageFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}
					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("DoNotPrintBankDetails")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						doNotPrintBankDetails = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("OnlyForFriendsPestControl")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						onlyForFriendsPestControl = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printProductPremisesFlag = true;
					}
					/**
					 * By Jayshree Date 28/12/2017 To set only four product
					 * process config done
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyFourProduct")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						nubofprod = true;
					}
					
					/**
					 * Date 12/1/2018 
					 * Dev.By jayshree
					 * 1)Des.To check the branch and company mail id
					 * 2)Des.To check the branch and company mail Address
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					
					//End For Jayshree
					
					/**
					 * Date 16-01-2018 By vijay for Product Description and Product services details table 
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("DoNotProductDescription")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						DoNotProductDescriptionFlag = true; 
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("DoNotProductServicesTable")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						DoNotProductServicesTableFlag = true; 
					}
					
					/**
					 * ends here
					 */
					/** date 06-02-2018 added by komal for consolidate price **/
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ConsolidatePrice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						consolidatePrice = true;
					}
					/**
					 * end komal
					 */
					
					/**
					 * Date 6-4-2018 By jayshree To add the cotract type as
					 * premise detail
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("TypeAsPremiseDetail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						contractTypeAsPremisedetail = true;
					}
					
					/**
					 * Date 31-3-2018 By jayshree To add the ho no process
					 * config add
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintHOEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hoEmail = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("HeaderWithOnlyComapanyDetail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						headerWithOnlycompanyDetail = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAttnInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printAttnInPdf = true;
					}
					
					/**
					 * Date 6-4-2018 Dev.By jayshree Des.To check the company
					 * heading alingment
					 */

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderLeft = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtRight")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderRight = true;
					}
					
					// End For Jayshree
					
					Config numberRangeConfig=null;
					if(contEntity.getNumberRange()!=null && !contEntity.getNumberRange().equals("")){
						numberRangeConfig = ofy().load().type(Config.class).filter("companyId", contEntity.getCompanyId())
											.filter("name", contEntity.getNumberRange()).filter("type", 91).first().now();
					}
					if(numberRangeConfig!=null && !numberRangeConfig.isGstApplicable()){ //Ashwini Patil Date:25-01-2024 Pest o shield reported and issue than for non billing invoices Gst number is getting printed
						gstNumberPrintFlag = false;
					}
					
					
					/*** Date 23-11-2018 By Vijay For GST Number Print or not ****/ 
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("EnableDoNotPrintGSTNumber")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						gstNumberPrintFlag = false;
					}
					/**
					 * ends here
					 */
					
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("COMPLAINSERVICEWITHTURNAROUNDTIME")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						complainServiceWithTurnAroundTimeFlag = true;
					}
					
					/** Added by : Priyanka  for Om pest control  need poc and branch email from customer branch **/
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_CustomerBranchPOC")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pocNameFlag = true; //custbranchmailFlag
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_CustomerBranchEmail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						custbranchmailFlag = true; //custbranchmailFlag
					}
					
					
					/** Added by : Priyanka  for life line Service need Dynamic process configuration **/
					if (processConfig.getProcessList().get(k).getProcessType().trim().contains("ChangeQty-")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						try {
							String label = processConfig.getProcessList().get(k).getProcessType().trim();
							String[] nameArray = label.split("-");
							if (nameArray.length > 0) {
								qtylabel = nameArray[1];
							}
							break;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
				}
			}
			
			/**
			 * nidhi
			 * 9-08-2018
			 */
			printModelSerailNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "PrintModelNoAndSerialNo", qp.getCompanyId());
		
			PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag = serverAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT, qp.getCompanyId());
		}
		
		
		/**
		 * Date 06-04-2018 by vijay required updated code 
		 * duplicate product ids not querrying  here so in print service details same product name issue resolved
		 */

		HashSet<Integer> productId = new HashSet<Integer>();
		
		for (int i = 0; i < contEntity.getItems().size(); i++) {
			productId.add(contEntity.getItems().get(i).getPrduct().getCount());
		}

		Iterator iteratorPordId = productId.iterator();
		
		while(iteratorPordId.hasNext()){
			
			int prodId = (int) iteratorPordId.next();

			sup = ofy()
					.load()
					.type(SuperProduct.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count",prodId).first().now();

			SuperProduct superprod = new SuperProduct();

			superprod.setCount(sup.getCount());
			superprod.setProductName(sup.getProductName());
//			superprod.setComment(sup.getComment() + " "
//					+ sup.getCommentdesc());

			/**
			 * Date 16-01-2018 BY vijay above old code commented
			 * and below new code added for all descriptions
			 */
			String desc ="";
			if(sup.getCommentdesc1()!=null){
				desc = sup.getComment()+"\n"+sup.getCommentdesc()+"\n"+sup.getCommentdesc1(); //Ashwini Patil Date:30-07-2022 added \n
			}
			else{
				desc = sup.getComment()+"\n"+sup.getCommentdesc();
			}
			if(sup.getCommentdesc2()!=null)
				desc = desc+"\n"+sup.getCommentdesc2();

			superprod.setComment(desc);
			
			superprod.setProductCode(sup.getProductCode());
			
			/**
			 * ends here
			 */
			stringlis.add(superprod);
			
		}
		/**
		 * ends here
		 */
		
		/***
		 * Date 06-04-2018 by vijay for below code querrying multiple time even if same product in product list so need this
		 * Updatecode above
		 */

//		for (int i = 0; i < contEntity.getItems().size(); i++) {
//
//			System.out.println("Con entity result size===="
//					+ contEntity.getItems().size());
//
//			if (qp.getCompanyId() != null) {
//
//				sup = ofy()
//						.load()
//						.type(SuperProduct.class)
//						.filter("companyId", qp.getCompanyId())
//						.filter("count",
//								contEntity.getItems().get(i).getPrduct()
//										.getCount()).first().now();
//
//				SuperProduct superprod = new SuperProduct();
//
//				superprod.setCount(sup.getCount());
//				superprod.setProductName(sup.getProductName());
////				superprod.setComment(sup.getComment() + " "
////						+ sup.getCommentdesc());
//
//				/**
//				 * Date 16-01-2018 BY vijay above old code commented
//				 * and below new code added for all descriptions
//				 */
//				String desc ="";
//				if(sup.getCommentdesc1()!=null){
//					desc = sup.getComment()+" "+sup.getCommentdesc()+" "+sup.getCommentdesc1();
//				}
//				else{
//					desc = sup.getComment()+" "+sup.getCommentdesc();
//				}
//				if(sup.getCommentdesc2()!=null)
//					desc = desc+" "+sup.getCommentdesc2();
//
//				superprod.setComment(desc);
//				
//				superprod.setProductCode(sup.getProductCode());
//				
//				/**
//				 * ends here
//				 */
//				stringlis.add(superprod);
//
//			}
//
//			else {
//				sup = ofy().load().type(SuperProduct.class)
//						.filter("count", contEntity.getItems().get(i).getId())
//						.first().now();
//
//				SuperProduct superprod = new SuperProduct();
//
//				superprod.setCount(sup.getCount());
//				superprod.setProductName(sup.getProductName());
//				superprod.setComment(sup.getComment() + " "
//						+ sup.getCommentdesc());
//
//				stringlis.add(superprod);
//
//			}
//		}
		
		/**
		 * ends here
		 */

		products = qp.getItems();
		System.out.println("product 1111" + qp.getItems().size());
		prodCharges = qp.getProductCharges();
		prodTaxes = qp.getProductTaxes();
		payTermsLis = qp.getPaymentTermsList();

		/**
		 * Date 8/12/2017 By Jayshree To add the article type arraylist
		 */
		articletypecust = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletypecust.addAll(cust.getArticleTypeDetails());
		}

		articletypecomp = new ArrayList<ArticleType>();
		if (comp.getArticleTypeDetails().size() != 0) {
			articletypecomp.addAll(comp.getArticleTypeDetails());
		}

		// End By Jayshree
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 * @author Abhinav Bihade
		 * @since 20/12/2019
		 * As per Rahul Tiwari's Requirement 
		 * Orkin : Hide Rate and Discount column from contract and Tax Invoice pdf 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideRateAndDiscountColumn", comp.getCompanyId())){
		hideRateAndDiscount=true;
		logger.log(Level.SEVERE,"Inside ProcessConfig:" +hideRateAndDiscount);
		}
		
		/**
		 * @author Vijay Date :- 24-11-2020
		 * Des :- Bug :- if paymen mode selected in contract level it should print the same on PDF but it was showing defualt payment mode
		 * so updated the code
		 */
		if(contEntity!=null && qp instanceof Contract) {
			if(contEntity.getPaymentMode()!=null && !contEntity.getPaymentMode().equals("")) {
			List<String> paymentDt = Arrays.asList(contEntity.getPaymentMode().trim().split("/"));
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", contEntity.getCompanyId()).first()
						.now();
				logger.log(Level.SEVERE,"payId " +payId);
				logger.log(Level.SEVERE,"contract level payment mode" +comppayment);

			}
			}
		}
		/**
		 * ends here
		 */
		
		
//		/**
//		 * nidhi
//		 * 06-04-2018
//		 * for branch as a company process configration
//		 * 
//		 */
//		logger.log(Level.SEVERE,"get for check configration --");
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
//			
//			logger.log(Level.SEVERE,"Process active --");
//			if(contEntity !=null && contEntity.getBranch() != null && contEntity.getBranch().trim().length() > 0){
//				
//				branchDt = ofy().load().type(Branch.class).filter("companyId",contEntity.getCompanyId()).filter("buisnessUnitName", contEntity.getBranch()).first().now();
//				
//				
//				if(branchDt != null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
//					
//					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());	
//					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
//					
//					if(paymentDt.get(0).trim().matches("[0-9]+")){
//						
//						
//						
//						int payId = Integer.parseInt(paymentDt.get(0).trim());
//						
//						comppayment = ofy().load().type(CompanyPayment.class)
//								.filter("count", payId)
//								.filter("companyId", contEntity.getCompanyId()).first()
//								.now();
//						
//						
//						if(comppayment != null){
//							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
//						}
//						
//					}
//					
//					
//				}
//			}
//		}
//		/**
//		 * end
//		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(contEntity !=null && contEntity.getBranch() != null && contEntity.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",contEntity.getCompanyId()).filter("buisnessUnitName", contEntity.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", contEntity.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
							}
						
						}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
	
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, comp.getCompanyId())){
			PC_RemoveSalutationFromCustomerOnPdfFlag = true;
		}
		
		/**
		 * @author Ashwini Patil
		 * @since 28-01-2022
		 * If country is selected as thai land then we will print thai specific format designed for Innovative
		 */
		if(comp!=null){
			companyCountry=comp.getAddress().getCountry().trim();
		}
		if(companyCountry!=null&&!companyCountry.equals("")){
			System.out.println("companyCountry="+companyCountry);
			if(companyCountry.equalsIgnoreCase("Thailand")||companyCountry.trim().equalsIgnoreCase("ประเทศไทย")){			
				System.out.println("in companyCountry=thailand. flags set to true");
				thaiPdfFlag=true;
				thaiFontFlag=true;
//				pageBreakFlag=true;
//				recieptPdf=false;
			}
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			branchWiseFilteredArticleList =getArticleBranchList(comp.getArticleTypeDetails(),branchName);
			logger.log(Level.SEVERE,"Inside Branch As Company1: " +branchWiseFilteredArticleList.size());
		}
		

		PC_DoNotPrintQtyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_DONOTPRINTQUANTITY, contEntity.getCompanyId());
		
		for(SalesLineItem lineitem : contEntity.getItems()){
			if(lineitem.getArea()!=null && !lineitem.getArea().equalsIgnoreCase("NA") && !lineitem.getArea().equalsIgnoreCase("N A")
					&& !lineitem.getArea().equals("")){
				if(!PC_DoNotPrintQtyFlag){
					qtyColumnFlag = true;
				}
			}
		}
		PC_DoNotPrintWarrantyFlag=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT,"PC_DoNotPrintProductWarranty" , contEntity.getCompanyId());
		
		//Ashwini Patil Date:5-08-2024 If the customer is society then do not print M/S against it as per ultima requirement
		if((cust!=null&&cust.getCategory()!=null&&cust.getCategory().equalsIgnoreCase("Society"))||(cust!=null&&cust.getType()!=null&&cust.getType().equalsIgnoreCase("Society")))
			isSociety=true;
	}

	public void getQuotation(long count, String preprint) {

		System.out.println("print status **************" + preprint);

		// Load Contract
		qp = ofy().load().type(Quotation.class).id(count).now();
		
		if (qp.getCompanyId() != null) {
			
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")){
						thaiFontFlag=true;
//						break;
					}
					if(obj.getProcessType().equalsIgnoreCase(AppConstants.PC_AMOUNTINWORDSHUNDREADSTRUCTURE)&&obj.isStatus()==true){
						AmountInWordsHundreadFormatFlag = true;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font16boldul = new Font(boldFont, 17);
				font12bold = new Font(boldFont, 13);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font9 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 13);
				font12 = new Font(regularFont, 13);
				font11bold = new Font(boldFont, 12);
				font10bold = new Font(boldFont, 11);
				font7 = new Font(regularFont, 8, Font.NORMAL);
				font14bold = new Font(boldFont, 15);
				font9bold = new Font(boldFont, 8);
				font6bold = new Font(boldFont, 7);
				
				font16bold = new Font(boldFont, 16);
				font11 = new Font(regularFont, 10);
				font10 = new Font(regularFont, 7);
				font13 = new Font(regularFont, 9);
				font7bold = new Font(boldFont, 7);
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
		// makeServices();
		// Load Customer
		if (qp.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId())
					.filter("companyId", qp.getCompanyId()).first().now();
		if (qp.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();
		

		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (qp.getCompanyId() != null)
			quotEntity = ofy().load().type(Quotation.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count", qp.getCount()).first().now();
		else
			quotEntity = ofy().load().type(Quotation.class)
					.filter("count", qp.getCount()).first().now();

		stateList = ofy().load().type(State.class)
				.filter("companyId", quotEntity.getCompanyId()).list();

		// rohan added this code for payment details
		if (qp.getCompanyId() != null) {
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", qp.getCompanyId()).first().now();
		}
		// ends here
		
		
		
		
		//=============Ashwini Patil Date:27-12-2022=====================
		
		ArrayList<String> customerbranchlistFromQuotation=getCustomerBranchList(quotEntity.getItems());
		
		/**
		 * @author Ashwini Patil
		 * @since 20-01-2022
		 * to manage multiple branch selected
		 */
		if(customerbranchlistFromQuotation!=null){
			if(customerbranchlistFromQuotation.size()==1){
				if(customerbranchlistFromQuotation.contains("Service Address")==false){
					logger.log(Level.SEVERE,"In Side AList1:");
					customerBranch= ofy().load().type(CustomerBranchDetails.class)
								.filter("cinfo.count",quotEntity.getCinfo().getCount())
								.filter("companyId", quotEntity.getCompanyId())
								.filter("buisnessUnitName", customerbranchlistFromQuotation.get(0)).first().now();
					
					logger.log(Level.SEVERE,"AList1:" +customerBranch);
					logger.log(Level.SEVERE,"AList2:" +customerbranchlistFromQuotation.size());				
				}
			}
			else{
				System.out.println("there are multiple service branches");
				multipleServiceBranchesFlag=true;
				selectedBranches = getSelectedBranchesInQuotation(quotEntity);
				if(!customerbranchlistFromQuotation.contains("Service Address")){
					if(selectedBranches!=null){
						customerBranch=selectedBranches.get(0);
						System.out.println("customerBranch= "+customerBranch.getBusinessUnitName());
					}
				}
				if(selectedBranches!=null){
					List<SalesLineItem> itemList=quotEntity.getItems();
					for(CustomerBranchDetails branch:selectedBranches){
						System.out.println("ashwini selectedBranche="+branch.getBusinessUnitName());
						HashSet<String> productSet=new HashSet<String>();
						for(SalesLineItem itemObj:itemList){
							System.out.println("ashwini item in for="+itemObj.getProductName());
							if(itemObj.getCustomerBranchSchedulingInfo()!=null){
								System.out.println("ashwini itemObj.getCustomerBranchSchedulingInfo()!=null");
								ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
								for(BranchWiseScheduling obj:branchSchedulingList){
									System.out.println("obj.getBranchName()="+obj.getBranchName()+" ischeck"+obj.isCheck());
									if(obj.isCheck()==true&&obj.getBranchName().equals(branch.getBusinessUnitName())){
										productSet.add(itemObj.getProductName());
									}
								}
							}
						}
						ArrayList<String> productList=new ArrayList<String>(productSet);
						branchWiseProductMap.put(branch.getBusinessUnitName(), productList);
					}
					HashSet<String> productSet=new HashSet<String>();
					for(SalesLineItem itemObj:itemList){
						System.out.println("ashwini item in for="+itemObj.getProductName());
						if(itemObj.getCustomerBranchSchedulingInfo()!=null){
							System.out.println("ashwini itemObj.getCustomerBranchSchedulingInfo()!=null");
							ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
							for(BranchWiseScheduling obj:branchSchedulingList){
								System.out.println("obj.getBranchName()="+obj.getBranchName()+" ischeck"+obj.isCheck());
								if(obj.isCheck()==true&&obj.getBranchName().equals("Service Address")){
									productSet.add(itemObj.getProductName());
								}
							}
						}
					}
					ArrayList<String> productList=new ArrayList<String>(productSet);
					branchWiseProductMap.put("Service Address", productList);
					
				}
				
			}
		}
		
		//=============Ashwini Patil Date:27-12-2022=====================

		/************************************ discount Flag *******************************/

		if (qp.getCompanyId() != null) {
			for (int i = 0; i < quotEntity.getItems().size(); i++) {
				if ((quotEntity.getItems().get(i).getDiscountAmt() > 0)
						&& (quotEntity.getItems().get(i)
								.getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (quotEntity.getItems().get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (quotEntity.getItems().get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}
			}
		}
		System.out.println("Disc nothing" + nothing + "-per" + discPer + "-AMt"
				+ discAmt + "-both" + both);

		if (qp.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Quotation")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;

					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"printDescOnFirstPageFlagForContractAndQuotation")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printDescOnFirstPageFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printProductPremisesFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}
					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("DoNotPrintBankDetails")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						doNotPrintBankDetails = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("OnlyForFriendsPestControl")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						onlyForFriendsPestControl = true;
					}

					/**
					 * By Jayshree Date 28/12/2017 To set only four product
					 * process config done
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyFourProduct")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						nubofprod = true;
					}
					
					/**
					 * Date 12/1/2018 
					 * Dev.By jayshree
					 * 1)Des.To check the branch and company mail id
					 * 2)Des.To check the branch and company mail Address
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					/**
					 * Ends for Jayshree
					 */
					

					/**
					 * Date 16-01-2018 By vijay for Product Description and Product services details table 
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("DoNotProductDescription")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						DoNotProductDescriptionFlag = true; 
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("DoNotProductServicesTable")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						DoNotProductServicesTableFlag = true; 
					}
					
					/**
					 * ends here
					 */
					
					
					/**
					 * Date 6-4-2018 By jayshree To add the cotract type as
					 * premise detail
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("TypeAsPremiseDetail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						contractTypeAsPremisedetail = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("HeaderWithOnlyComapanyDetail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						headerWithOnlycompanyDetail = true;
					}
					
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintHOEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hoEmail = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAttnInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printAttnInPdf = true;
					}
					
					/**
					 * Date 6-4-2018 Dev.By jayshree Des.To check the company
					 * heading alingment
					 */

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderLeft = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtRight")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderRight = true;
					}
					// End For Jayshree
					Config numberRangeConfig=null;
					if(quotEntity.getQuotationNumberRange()!=null && !quotEntity.getQuotationNumberRange().equals("")){
						numberRangeConfig = ofy().load().type(Config.class).filter("companyId", quotEntity.getCompanyId())
											.filter("name", quotEntity.getQuotationNumberRange()).filter("type", 91).first().now();
					}
					if(numberRangeConfig!=null && !numberRangeConfig.isGstApplicable()){ //Ashwini Patil Date:25-01-2024 Pest o shield reported and issue than for non billing invoices Gst number is getting printed
						gstNumberPrintFlag = false;
					}
					/*** Date 23-11-2018 By Vijay For GST Number Print or not ****/ 
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("EnableDoNotPrintGSTNumber")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						gstNumberPrintFlag = false;
					}
					/**
					 * ends here
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}
					
//					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideTotalAmountDetails")
//							&& processConfig.getProcessList().get(k).isStatus() == true) {
//						hideTotalAmountDetailsFlag = true;
//					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_RemoveBankDetail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pc_RemoveBankDetail = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_CustomerBranchPOC")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pocNameFlag = true; //custbranchmailFlag
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_CustomerBranchEmail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						custbranchmailFlag = true; //custbranchmailFlag
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().contains("ChangeQty-")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						try {
							String label = processConfig.getProcessList().get(k).getProcessType().trim();
							String[] nameArray = label.split("-");
							if (nameArray.length > 0) {
								qtylabel = nameArray[1];
							}
							break;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("printProductRemarkWithPremise")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printProductRemarkWithPremiseFlag = true; 
					}
					
					
				}
			}
		}

		
		/**
		 * Date 06-04-2018 by vijay required updated code 
		 * duplicate product ids not querrying  here so in print service details same product name issue resolved
		 */

		HashSet<Integer> productId = new HashSet<Integer>();
		
		for (int i = 0; i < quotEntity.getItems().size(); i++) {
			productId.add(quotEntity.getItems().get(i).getPrduct().getCount());
		}

		Iterator iteratorPordId = productId.iterator();
		
		while(iteratorPordId.hasNext()){
			
			int prodId = (int) iteratorPordId.next();

			sup = ofy()
					.load()
					.type(SuperProduct.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count",prodId).first().now();

			SuperProduct superprod = new SuperProduct();

			superprod.setCount(sup.getCount());
			superprod.setProductName(sup.getProductName());
//			superprod.setComment(sup.getComment() + " "
//					+ sup.getCommentdesc());

			/**
			 * Date 16-01-2018 BY vijay above old code commented
			 * and below new code added for all descriptions
			 */
			String desc ="";
			if(sup.getCommentdesc1()!=null){
				desc = sup.getComment()+" "+sup.getCommentdesc()+" "+sup.getCommentdesc1();
			}
			else{
				desc = sup.getComment()+" "+sup.getCommentdesc();
			}
			if(sup.getCommentdesc2()!=null)
				desc = desc+" "+sup.getCommentdesc2();

			superprod.setComment(desc);
			
			superprod.setProductCode(sup.getProductCode());
			
			/**
			 * ends here
			 */
			stringlis.add(superprod);
			
		}
		/**
		 * ends here
		 */
               /** Added by Sheetal : 31-03-2022
		 * Des : if payment mode selected in quotation then that will print on pdf
		 *      and if not selected then default payment will print
		 **/
		if(quotEntity!=null && qp instanceof Quotation) {
			if(quotEntity.getPaymentModeName()!=null && !quotEntity.getPaymentModeName().equals("")) {
			List<String> paymentDt = Arrays.asList(quotEntity.getPaymentModeName().trim().split("/"));
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", quotEntity.getCompanyId()).first()
						.now();
			
				logger.log(Level.SEVERE,"Quotation level payment mode" +comppayment);

			}
			}
		}
		/***
		 * Date 06-04-2018 by vijay for below code querrying multiple time even if same product in product list so need this
		 * Updatecode above
		 */

		
//		for (int i = 0; i < quotEntity.getItems().size(); i++) {
//
//			System.out.println("quotEntity entity result size===="
//					+ quotEntity.getItems().size());
//
//			if (qp.getCompanyId() != null) {
//
//				sup = ofy()
//						.load()
//						.type(SuperProduct.class)
//						.filter("companyId", qp.getCompanyId())
//						.filter("count",
//								quotEntity.getItems().get(i).getPrduct()
//										.getCount()).first().now();
//
//				SuperProduct superprod = new SuperProduct();
//
//				superprod.setCount(sup.getCount());
//				superprod.setProductName(sup.getProductName());
////				superprod.setComment(sup.getComment() + " "
////						+ sup.getCommentdesc());
//
//				/**
//				 * Date 16-01-2018 BY vijay above old code commented
//				 * and below new code added for description
//				 */
//				String desc ="";
//				if(sup.getCommentdesc1()!=null){
//					System.out.println("sup.getCommentdesc1()!=null");
//					desc = sup.getComment()+" "+sup.getCommentdesc()+" "+sup.getCommentdesc1();
//				}
//				else{
//					System.out.println("Else");
//					desc = sup.getComment()+" "+sup.getCommentdesc();
//				}
//				System.out.println("descripti =="+desc);
//				if(sup.getCommentdesc2()!=null)
//					desc = desc+" "+sup.getCommentdesc2();
//
//				System.out.println("Desc ==="+desc);
//				superprod.setComment(desc);
//				
//				superprod.setProductCode(sup.getProductCode());
//
//				/**
//				 * ends here
//				 */
//				
//				stringlis.add(superprod);
//
//			}
//
//			else {
//				sup = ofy().load().type(SuperProduct.class)
//						.filter("count", quotEntity.getItems().get(i).getId())
//						.first().now();
//
//				SuperProduct superprod = new SuperProduct();
//
//				superprod.setCount(sup.getCount());
//				superprod.setProductName(sup.getProductName());
//				superprod.setComment(sup.getComment() + " "
//						+ sup.getCommentdesc());
//
//				stringlis.add(superprod);
//
//			}
//		}
		
		/**
		 * ends here
		 */

		products = qp.getItems();
		prodCharges = qp.getProductCharges();
		prodTaxes = qp.getProductTaxes();
		payTermsLis = qp.getPaymentTermsList();

		// Date 8-12-2017
		// By Jayshree
		// To add the article type Arraylist
		articletypecust = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletypecust.addAll(cust.getArticleTypeDetails());
		}
		articletypecomp = new ArrayList<ArticleType>();
		if (comp.getArticleTypeDetails().size() != 0) {
			articletypecomp.addAll(comp.getArticleTypeDetails());
		}

		// End By Jayshree
		System.out.println(" cust size ()=="
				+ cust.getArticleTypeDetails().size());
		System.out.println(" comp size ()=="
				+ comp.getArticleTypeDetails().size());

		// articletype=cust.getArticleTypeDetails();
		// articletype=comp.getArticleTypeDetails();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 * nidhi
		 * 06-04-2018
		 * for branch as a company process configration
		 * 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(quotEntity !=null && quotEntity.getBranch() != null && quotEntity.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",quotEntity.getCompanyId()).filter("buisnessUnitName", quotEntity.getBranch()).first().now();
				
				 if(branchDt != null){
						comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				 }
				 
				if(branchDt!=null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", quotEntity.getCompanyId()).first()
								.now();
						
						
//						if(comppayment != null){
//							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
//						}
						
					}
					
					
				}
			}
		}
		
		/**
		 * end
		 */
		
		/**
		 * @author Abhinav Bihade
		 * @since 05/02/2020
		 *As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
		 * in case if somebody manages 2 companies under same link of ERP s/w
		 */
		String branchName="";
//		if(contEntity.getBranch()!=null){
//			branchName=contEntity.getBranch();
//		}
//		else 
			if(quotEntity.getBranch()!=null){
			branchName=quotEntity.getBranch();
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			branchWiseFilteredArticleList =getArticleBranchList(comp.getArticleTypeDetails(),branchName);
			logger.log(Level.SEVERE,"Inside Branch As Company2: " +branchWiseFilteredArticleList.size());
		}
		
		/** Added by Priyanka - load customer branch **/
		ArrayList<String> custbranchlist=getCustomerBranchList(quotEntity.getItems());
		
		if(custbranchlist!=null&&custbranchlist.size()==1&& custbranchlist.contains("Service Address")==false){
				logger.log(Level.SEVERE,"In Side AList1:");
				customerBranch= ofy().load().type(CustomerBranchDetails.class)
							.filter("cinfo.count",quotEntity.getCinfo().getCount())
							.filter("companyId", quotEntity.getCompanyId())
							.filter("buisnessUnitName", custbranchlist.get(0)).first().now();
				
				logger.log(Level.SEVERE,"AList1:" +customerBranch);
				logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());
			}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, comp.getCompanyId())){
			PC_RemoveSalutationFromCustomerOnPdfFlag = true;
		}
		
		
		PC_DoNotPrintQtyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.QUOTATION, AppConstants.PC_DONOTPRINTQUANTITY, quotEntity.getCompanyId());
		PC_DoNotPrintContractPeriodFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.QUOTATION, AppConstants.PC_DONOTPRINTCONTRACTPERIOD, quotEntity.getCompanyId());
		PC_DoNotPrintRateFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.QUOTATION, AppConstants.PC_DONOTPRINTRATE, quotEntity.getCompanyId());
		PC_DoNotPrintDiscFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.QUOTATION, AppConstants.PC_DONOTPRINTDISCOUNT, quotEntity.getCompanyId());
	
		reduceHeaderSpaceFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.QUOTATION, "PC_ReduceHeaderSpace", quotEntity.getCompanyId());
	for(SalesLineItem lineitem : quotEntity.getItems()){
			if(lineitem.getArea()!=null && !lineitem.getArea().equalsIgnoreCase("NA") && !lineitem.getArea().equalsIgnoreCase("N A")
					&& !lineitem.getArea().equals("")){
				if(!PC_DoNotPrintQtyFlag){
					qtyColumnFlag = true;
				}
			}
			if(lineitem.getUnitOfMeasurement()!=null && !lineitem.getUnitOfMeasurement().equals("")){
				unitOfMeasurementColumnFlag = true;
			}
		}
		PC_DoNotPrintWarrantyFlag=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.QUOTATION,"PC_DoNotPrintProductWarranty" , quotEntity.getCompanyId());
	//Ashwini Patil Date:5-08-2024 If the customer is society then do not print M/S against it as per ultima requirement
			if((cust!=null&&cust.getCategory()!=null&&cust.getCategory().equalsIgnoreCase("Society"))||(cust!=null&&cust.getType()!=null&&cust.getType().equalsIgnoreCase("Society")))
				isSociety=true;
	}

	public void createPdf(String preprint) {
		System.out.println("My name is  ++++++++++++++++++++++" + preprint);

		if (qp instanceof Contract) {
			System.out.println("111111111");
			if (upcflag == false && preprint.equals("plane")) {
				System.out.println("2222222222222222222222222222222");
				System.out.println("contract inside plane");
				// createLogo(document,comp);// Date 16/11/2017 Comment the
				// method by Jayshree
				if (headerWithOnlycompanyDetail) {
					createCompanyAsheader();
					createHeadingOfbillingandServiceAddress();
					createCustomerDetailsForOnlyCompanyHeader();
				} else {
					createCompanyHedding();
				}
			} else {
				if (preprint.equals("yes")) {
					System.out.println("333333333333333333333333333333333");
					System.out.println("inside prit yes");
					createBlankforUPC();
				}
				if (preprint.equals("no")) {
					System.out.println("4444444444444444444444444444444444444444444");
					System.out.println("inside prit no");
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}
					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					createBlankforUPC();
				}
			}

		} 
		else {
			
			/**
			 * @author Vijay Date :- 30-03-2023
			 * Des :- Bitco pest company introduction will print on 1st page
			 */
			boolean companyIntroductionFlag = false;
			if(qp instanceof Quotation && quotEntity!=null){
				logger.log(Level.SEVERE, "companyIntroduction"+comp.getCompanyIntroduction());
				logger.log(Level.SEVERE, "companyIntroduction"+comp.getBusinessUnitName());
				
				Company companyEntity = ofy().load().type(Company.class).filter("companyId", quotEntity.getCompanyId()).first().now();
				
				logger.log(Level.SEVERE, "companyIntroduction"+companyEntity.getCompanyIntroduction());
				logger.log(Level.SEVERE, "companyIntroduction"+companyEntity.getBusinessUnitName());

//				Paragraph para = addCompanyIntroduction(companyEntity.getCompanyIntroduction());
				
				ArrayList<Paragraph> paraList = pdfUtility.addCompanyIntroduction(companyEntity.getCompanyIntroduction(),font10bold,font11Regular);

				try {
					if(paraList!=null){

						for(Paragraph para:paraList){
						
						Paragraph blank = new Paragraph();
						blank.add(Chunk.NEWLINE);
						
						if (upcflag == false && preprint.equals("plane")) {
							System.out.println("55555555555555555555555555555555");
//							if (headerWithOnlycompanyDetail == true) {
//								createCompanyAsheader();
//							} else {
//								createCompanyHedding();
//							}
							try {
								document.add(blank);
								document.add(blank);
								document.add(blank);
								document.add(blank);
								document.add(blank);
								document.add(blank);
							} catch (Exception e) {
							}
							
						} else {
							if (preprint.equals("yes")) {
								System.out.println("666666666666666666666666666666666");
								try {
									document.add(blank);
									document.add(blank);
									document.add(blank);
									document.add(blank);
									document.add(blank);
									document.add(blank);
								} catch (Exception e) {
								}
									
							}
							if (preprint.equals("no")) {
								System.out.println("777777777777777777777777777777777777");
								if (comp.getUploadHeader() != null) {
									createCompanyNameAsHeader(document, comp);
								}
								if (comp.getUploadFooter() != null) {
									createCompanyNameAsFooter(document, comp);
								}
								
								if(comp.getUploadHeader()!=null && comp.getUploadFooter() != null){
									try {
										document.add(blank);
										document.add(blank);
										document.add(blank);
										document.add(blank);
										document.add(blank);
										document.add(blank);
									} catch (Exception e) {
									}
								}
							}
							System.out.println("88888888888888888888888888888");
						}
						
						document.add(para);
//						companyIntroductionFlag = true;
						Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
						try {
							document.add(nextpage);
						} catch (DocumentException e1) {
							e1.printStackTrace();
						}

					}
					}
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			
			if(companyIntroductionFlag){
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
			}
			
			/**
			 * ends here
			 */
			
			System.out.println("upc flag**********88888" + upcflag);
			if (upcflag == false && preprint.equals("plane")) {
				System.out.println("55555555555555555555555555555555");
				if (headerWithOnlycompanyDetail == true) {
					createCompanyAsheader();
					createHeadingOfbillingandServiceAddress();
					/**
					 * @author Anil @since 23-07-2021
					 */
//					createCustomerDetailsForOnlyCompanyHeaderquo();
					createCustomerDetailsForOnlyCompanyHeader();
				} else {
					createCompanyHedding();
				}
			} else {
				if (preprint.equals("yes")) {
					System.out.println("666666666666666666666666666666666");
					createBlankforUPC();
				}
				if (preprint.equals("no")) {
					System.out.println("777777777777777777777777777777777777");
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}
					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}

					createBlankforUPC();
				}
				System.out.println("88888888888888888888888888888");
			}

		}
		
		
		if (printProductPremisesFlag) {
			System.out.println("inside nothing");
			createProductWithoutDiscDetail();
		} else {
			System.out.println("inside nothing");
			createProductWithoutDiscDetail();
		}
		termsConditionsInfo();
		footerInfo(preprint);
		
		/**
		 * Updated By: Viraj
		 * Date: 04-03-2019
		 * Description: To show remaining products in the next page
		 */
		if(noOfLines <= 0 && prouductCount != 0) {
			createAnnextureTable();
		}
		/** Ends **/
		createProductDescription(preprint);
		
		//Ashwini Patil Date:28-02-2023
		if (qp instanceof Contract) {
			serverAppUtility.createTermAndConditionsPage(document, "Contract",qp.getCompanyId());
		}
		else {			
			createTermsAndConditionPage(document,quotEntity.getTermsAndConditionlist(),"Quotation",qp.getCompanyId(),preprint);
		}
		logger.log(Level.SEVERE,"end of createPdf");
	}

	

	private Paragraph addCompanyIntroduction(String stringvalue) {
		// TODO Auto-generated method stub
			try {
//				Company companyEntity = ofy().load().type(Company.class).filter("companyId", quotEntity.getCompanyId()).first().now();
//				logger.log(Level.SEVERE, "companyIntroduction"+comp.getCompanyIntroduction());
//				logger.log(Level.SEVERE, "companyIntroduction"+comp.getBusinessUnitName());
//				
//				logger.log(Level.SEVERE, "companyIntroduction"+companyEntity.getCompanyIntroduction());
//				logger.log(Level.SEVERE, "companyIntroduction"+companyEntity.getBusinessUnitName());

				String companyIntroduction = stringvalue;
				if(companyIntroduction!=null && !companyIntroduction.equals("")) {
					
					String [] introduction = companyIntroduction.split("<b>");
					Phrase alldata = new Phrase();

					for(int i=0;i<introduction.length;i++){
						
						if(introduction[i].contains("</b>")){
							String [] strbolddata = introduction[i].split("</b>");
							
							Chunk chk2 = new Chunk(strbolddata[0], font10bold);
							Chunk chk3 = new Chunk(strbolddata[1], font11Regular);

							alldata.add(chk2);
							alldata.add(chk3);

						}
						else{
							Chunk chk1 = new Chunk(introduction[i], font11Regular);
							alldata.add(chk1);

						}
						
					}
					Paragraph para2 = new Paragraph();
					para2.add(alldata);
					
					return para2;
					
//					try {
//						document.add(para2);
//						Paragraph blank =new Paragraph();
//					    blank.add(Chunk.NEWLINE);
//						document.add(blank);
//
//					} catch (DocumentException e) {
//						e.printStackTrace();
//					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
			
			
	}


	private void createCustomerDetailsForOnlyCompanyHeaderquo() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font7bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font7bold);
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name
		
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
					custName = cust.getCompanyName().trim();
				}
				else{
					custName = "M/S " + " " + cust.getCompanyName().trim();
				}
			} else if (cust.getSalutation() != null
					&& !cust.getSalutation().equals("")) {
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here

		Phrase nameCellVal = new Phrase(fullname, font7bold);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////////Ajinkya Added this for Ultra pest Control

		Phrase email = new Phrase("Email", font7bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * By Jayshree Date 28/12/2017 to check null condition
		 */
		/** Added By Priyanka - For om pest control **/
		String emailVal = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			emailVal = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				emailVal = cust.getEmail();
			} else {
				emailVal ="";
			}
		}
		
		
		
		Phrase emailValOne = new Phrase(emailVal, font10);
		PdfPCell emailValCell = new PdfPCell(emailValOne);
		// nameCellValCell.addElement(nameCellVal);asd
		emailValCell.setBorder(0);
		emailValCell.setColspan(4);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo = new Phrase("Phone", font7bold);
		PdfPCell mobNoCell = new PdfPCell(mobNo);
		mobNoCell.setBorder(0);
		mobNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String phone="";
		String landline="";
		
			if(cust.getCellNumber1()!=null && cust.getCellNumber1()!=0){
				System.out.println("pn11");
			phone=cust.getCellNumber1()+"";
			}
			if(cust.getCellNumber2()!=null && cust.getCellNumber2()!=0)
			{
				if(!phone.trim().isEmpty())
					{
					phone=phone+" / "+cust.getCellNumber2()+"";
					}
				else{
					phone=cust.getCellNumber2()+"";
					}
			System.out.println("pn33"+phone);
			}
			if(cust.getLandline()!=0 && cust.getLandline()!=null)
			{
				if(!phone.trim().isEmpty()){
					phone=phone+" / "+cust.getLandline()+"";
				}
				else{
					phone=cust.getLandline()+"";
				}
			System.out.println("pn44"+phone);
			}

		Phrase mobVal = new Phrase(phone, font10);
		PdfPCell mobValCell = new PdfPCell(mobVal);
		// nameCellValCell.addElement(nameCellVal);asd
		mobValCell.setBorder(0);
		mobValCell.setColspan(4);//add Calspane by jayshree
		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase landlineph = new Phrase("LandLine", font10bold);
//		PdfPCell landlineCell = new PdfPCell(landlineph);
//		landlineCell.setBorder(0);
//		landlineCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
////		String landline = "";
////		if (cust.getLandline() != 0) {
////			landline = cust.getLandline() + "";
////		} else {
////			landline = "";
////		}
//
//		Phrase landlineVal = new Phrase(landline, font10);
//		PdfPCell landlinevalCells = new PdfPCell(landlineVal);
//		// nameCellValCell.addElement(nameCellVal);asd
//		landlinevalCells.setBorder(0);
//		landlinevalCells.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ///////////////// End Here

		Phrase address = new Phrase("Address", font7bold);// Date 9/12/2017 By
															// Jayshree increse
															// the Font size
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * Date 13-09-2017 added by vijay for customer address first it will
		 * check from contract if in contract customer address is available then
		 * it will print else it will get customer address from customer below
		 * line old code commented
		 */
		// String adrsValString = cust.getAdress().getCompleteAddress().trim();

		String adrsValString = "";
		
			adrsValString = cust.getAdress().getCompleteAddress().trim();
		
			/**
			 * @author Vijay Date :- 21-01-2021
			 * Des :- if billing address stored in quotation showing from quotation entity or else as old process from customer
			 * Requirement from Rahul PTSPL 
			 */
			if(qp!=null ){
				Quotation quotation = (Quotation) qp;
				if(quotation.getBillingAddress()!=null && quotation.getBillingAddress().getAddrLine1()!=null && 
						!quotation.getBillingAddress().getAddrLine1().equals("")){
					adrsValString = quotation.getBillingAddress().getCompleteAddress();
					logger.log(Level.SEVERE,"Billing address ="+adrsValString);
					if(quotEntity!=null){
						logger.log(Level.SEVERE,"Billing address ="+ quotEntity.getBillingAddress().getCompleteAddress());
	
					}
				}
			}
			
		Phrase addressVal = new Phrase(adrsValString, font10);// Date 9/12/2017
																// By Jayshree
																// increse the
																// Font size
		PdfPCell addressValCell = new PdfPCell(addressVal);
		// addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String gstTinStr = "";
		ServerAppUtility serverAppUtility = new ServerAppUtility();
		gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,null, "Quotation");//Date 1/2/2018 By jayshree add new parameter
	
		/**
		 * Date 23-11-2018 By VIjay 
		 * for GST Number not print with process Config 
		 */
		PdfPCell gstTinCell = null;
		PdfPCell gstTinValCell = null;
		PdfPCell stateCodeCell = null;
		PdfPCell colon3Cell = null;
		PdfPCell stateCodeValCell = null;
		
		if(gstNumberPrintFlag){
		Phrase gstTin = new Phrase("GSTIN", font7bold);
//		PdfPCell gstTinCell = new PdfPCell(gstTin);
		gstTinCell = new PdfPCell(gstTin);

		// gstTinCell.addElement(gstTin);
		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTinVal = new Phrase(gstTinStr, font10);
//		PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
		gstTinValCell = new PdfPCell(gstTinVal);

		// gstTinValCell.addElement(gstTinVal);
		gstTinValCell.setBorder(0);
		gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * Date13/12/2017 By Jayshree To Add state code
		 */

		/**
	 * Date 1/2/2018 By jayshree 
	 * Des.remove the statecode if GSTIN is not present
	 */
	Phrase stateCode=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	stateCode = new Phrase("State Code", font7bold);//Date 9/12/2017 By Jayshree To increse the fontsize by one
	}
	else{
		stateCode = new Phrase(" ", font7bold);
	}
//	PdfPCell stateCodeCell = new PdfPCell(stateCode);
	stateCodeCell = new PdfPCell(stateCode);

	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase colon3=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	 colon3 = new Phrase(":", font7bold);
	}
	else{
		colon3 = new Phrase(" ", font7bold);
	}
//	PdfPCell colon3Cell = new PdfPCell(colon3);
	colon3Cell = new PdfPCell(colon3);

	colon3Cell.setBorder(0);
	colon3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String stCo = "";
	for (int i = 0; i < stateList.size(); i++) {
		if (stateList.get(i).getStateName().trim()
				.equalsIgnoreCase(cust.getAdress().getState().trim())) {
			stCo = stateList.get(i).getStateCode().trim();
			break;
		}
	}
	
	Phrase stateCodeVal=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	 stateCodeVal = new Phrase(stCo, font10);//Date 9/12/2017 By Jayshree To increse the fontsize by one
	}else{
		stateCodeVal = new Phrase(" ", font10);
	}
	//End By Jayshree
//		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		stateCodeValCell = new PdfPCell(stateCodeVal);

		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		}
		/**
		 * ends here
		 */
		Phrase attn = new Phrase("Attn", font7bold);
		PdfPCell attnCell = new PdfPCell(attn);
		// gstTinCell.addElement(gstTin);
		attnCell.setBorder(0);
		attnCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase attnVal = new Phrase(quotEntity.getCinfo().getPocName(),
//				font10);
		//5-08-2024 need salutation before poc name as per ultima search
		Phrase attnVal = null;
		if (cust.getSalutation() != null&& !cust.getSalutation().equals(""))
				attnVal=new Phrase(cust.getSalutation() + " "+quotEntity.getCinfo().getPocName(),
						font10);
		else
				attnVal=new Phrase(quotEntity.getCinfo().getPocName(),
							font10);
		
		PdfPCell attnValCell = new PdfPCell(attnVal);
		// gstTinValCell.addElement(gstTinVal);
		attnValCell.setBorder(0);
		attnValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attnCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attnValCell);
		}
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);

		PdfPTable colonTable2 = new PdfPTable(6);
		colonTable2.setWidthPercentage(100);
		try {
			colonTable2.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f, 0.2f,
					2.0f });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable email_MobNumber = new PdfPTable(6);// Date 3/01/2017 By
														// jayshree Change colon
														// width
		email_MobNumber.setWidthPercentage(100);
		try {
			email_MobNumber.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f,
					0.2f, 2.0f });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon2 = new Phrase(":", font7bold);
		PdfPCell colon2Cell = new PdfPCell(colon2);
		colon2Cell.setBorder(0);
		colon2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		email_MobNumber.addCell(emailCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(emailValCell);
		email_MobNumber.addCell(mobNoCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(mobValCell);

//		email_MobNumber.addCell(landlineCell);
//		email_MobNumber.addCell(colon2Cell);
//		email_MobNumber.addCell(landlinevalCells);

//		/**
//		 * Date : 12 Mar 2018
//		 * Dev : Jayshree
//		 * Description : Remove GST for Customer if no taxes present.
//		 */
//		if(quotEntity.getProductTaxes().size()!=0){
		/**
		 * Date 23-11-2018 By VIjay 
		 * for GST Number not print with process Config 
		 */
		if(gstNumberPrintFlag){			
		
		colonTable2.addCell(gstTinCell);
		colonTable2.addCell(colon2Cell);
		colonTable2.addCell(gstTinValCell);
		/**
		 * Date 13/12/2017 By Jayshree add the cells in tab
		 */
		colonTable2.addCell(stateCodeCell);
		colonTable2.addCell(colon3Cell);//Rename cell by jayshree
		colonTable2.addCell(stateCodeValCell);
		}
		// End By Jayshree
		PdfPCell cell1 = new PdfPCell(colonTable);
		cell1.setBorder(0);

		PdfPCell email_Cell = new PdfPCell(email_MobNumber);
		email_Cell.setBorder(0);

		PdfPCell cell12 = new PdfPCell(colonTable2);
		cell12.setBorder(0);
		
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);

		PdfPTable pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// pdfStateTable.addCell(stateValCell);
		// pdfStateTable.addCell(stateCodeCell);
		// pdfStateTable.addCell(colonCell);
		// pdfStateTable.addCell(stateCodeValCell);
		// End by jayshree
		PdfPCell state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell); // stateTableCell.addElement(statetable);

		PdfPCell stateCell_stateCode = new PdfPCell(colonTable);
		stateCell_stateCode.setBorder(0);

		part1Table.addCell(cell1);
		/**
		 * Date 13/12/2017 By Jayshree des. comment this to remove state and
		 * state code
		 */
		// part1Table.addCell(stateCell_stateCode);

		part1Table.addCell(email_Cell);
		part1Table.addCell(cell12);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		/* Ends Part 1 */

		/* Part 2 Start */
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font7bold);//ashwini patil
		PdfPCell name2Cell = new PdfPCell(name2);
		// name2Cell.addElement();
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name2CellVal = new Phrase(fullname, font7bold);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////// Ajinkya added this
		Phrase email1 = new Phrase("Email", font7bold);
		PdfPCell email1Cell = new PdfPCell(email1);
		email1Cell.setBorder(0);
		email1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * Date 28/12/2017 By Jayshree To handle null condition
		 */
		String emailVal2 = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			emailVal2 = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				emailVal2 = cust.getEmail();
			} else {
				emailVal2 ="";
			}
		}
		
	
		Phrase email1ValTwo = new Phrase(emailVal2, font10);
		PdfPCell email1ValCell = new PdfPCell(email1ValTwo);
		// nameCellValCell.addElement(nameCellVal);asd
		email1ValCell.setBorder(0);
		email1ValCell.setColspan(4);
		email1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo2 = new Phrase("Mobile", font7bold);
		PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
		mobNo2Cell.setBorder(0);
		mobNo2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

			String phone2="";
		
			if(cust.getCellNumber1()!=null && cust.getCellNumber1()!=0){
				System.out.println("pn11");
			phone2=cust.getCellNumber1()+"";
			}
			if(cust.getCellNumber2()!=null && cust.getCellNumber2()!=0)
			{
				if(!phone2.trim().isEmpty())
					{
					phone2=phone2+" / "+cust.getCellNumber2()+"";
					}
				else{
					phone2=cust.getCellNumber2()+"";
					}
			System.out.println("pn33"+phone2);
			}
			if(cust.getLandline()!=0 && cust.getLandline()!=null)
			{
				if(!phone2.trim().isEmpty()){
					phone2=phone2+" / "+cust.getLandline()+"";
				}
				else{
					phone2=cust.getLandline()+"";
				}
			System.out.println("pn44"+phone2);
			}
		

		Phrase mobNo2Val = new Phrase(phone2, font10);
		PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);
		// nameCellValCell.addElement(nameCellVal);asd
		mobNo2ValCell.setBorder(0);
		mobNo2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase landlineph2 = new Phrase("LandLine", font7bold);
		PdfPCell landlineCell2 = new PdfPCell(landlineph2);
		landlineCell2.setBorder(0);
		landlineCell2.setHorizontalAlignment(Element.ALIGN_LEFT);

		String landline2 = "";
		if (cust.getLandline() != 0) {
			landline2 = cust.getLandline() + "";
		} else {
			landline2 = "";
		}

		Phrase landlineVal2 = new Phrase(landline2, font10);
		PdfPCell landlinevalCells2 = new PdfPCell(landlineVal2);
		// nameCellValCell.addElement(nameCellVal);asd
		landlinevalCells2.setBorder(0);
		landlinevalCells2.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////////

		Phrase address2 = new Phrase("Address", font7bold);// Date 9/12/2017 By
															// Jayshree increse
															// the Font size
		PdfPCell address2Cell = new PdfPCell(address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ///////////////////////// Ajinkya added branch code for customer
		// Branch On Date : 15/07/2017 //////////////////////////////
		// String adrsValString1 = "";
		/**
		 * Date :17-11-2017 Below Code Commented By Manisha
		 */
		// //////////////////////////////
		// String customerPoc = null;
		// if (!cust.getSecondaryAdress().getAddrLine1().equals("")
		// && customerbranchlist.size() == 0) {
		// System.out.println("Inside service Addrss");
		//
		// // old code commented by vijay new code added below
		// // adrsValString1 = cust.getSecondaryAdress().getCompleteAddress()
		// // .trim();
		//
		// /**
		// * Date 13-09-2017 added by vijay for customer address first it will
		// * check from contract if in contract customer address is available
		// * then it will print else it will get customer address from
		// * customer
		// *
		// */
		//
		// if (con.getCustomerServiceAddress() != null
		// && con.getCustomerServiceAddress().getAddrLine1() != null
		// && !con.getCustomerServiceAddress().getAddrLine1()
		// .equals("")
		// && con.getCustomerServiceAddress().getCountry() != null
		// && !con.getCustomerServiceAddress().getCountry().equals("")) {
		// adrsValString1 = con.getCustomerServiceAddress()
		// .getCompleteAddress().trim();
		// } else {
		// adrsValString1 = cust.getSecondaryAdress().getCompleteAddress()
		// .trim();
		// }
		// customerPoc = invoiceentity.getPersonInfo().getPocName();
		// } else {
		// System.out.println("Inside Customer branch  "
		// + customerbranchlist.size());
		// for (int i = 0; i < customerbranchlist.size(); i++) {
		// // if()
		// adrsValString1 = customerbranchlist.get(i).getAddress()
		// .getCompleteAddress();
		// customerPoc = customerbranchlist.get(i).getPocName();
		// }
		//
		// }
		/** ends **/

		/**
		 * Date :17-11-2017 By :Manisha To get the service address on pdf..!!
		 * 
		 */

		String adrsValString1 = "";
		String customerPoc = null;

		Address serviceAddress = null;

//		if (invoiceentity.getCustomerBranch() != null
//				&& !invoiceentity.getCustomerBranch().equals("")) {
//			CustomerBranchDetails branch = ofy()
//					.load()
//					.type(CustomerBranchDetails.class)
//					.filter("companyId", comp.getCompanyId())
//					.filter("buisnessUnitName",
//							invoiceentity.getCustomerBranch()).filter("cinfo.count", invoiceentity.getPersonInfo().getCount()).first().now();
//			/** Date 18-01-2018 By vijay above query added customer id filter for getting specific customer branch **/
//			if (branch != null) {
//				serviceAddress = branch.getAddress();
//				System.out.println("heeeeeeeelllllllooo" + serviceAddress);
//
//				adrsValString1 = branch.getAddress().getCompleteAddress();
//
//			}
//
//		} else {

			
				serviceAddress = cust.getSecondaryAdress();
				adrsValString1 = cust.getSecondaryAdress().getCompleteAddress();
				
				/**
				 * @author Vijay Date :- 21-01-2021
				 * Des :- if Service address stored in quotation showing from quotation entity or else as old process from customer
				 * Requirement from Rahul PTSPL 
				 */
				if(qp!=null ){
					Quotation quotation = (Quotation) qp;
					if(quotation.getServiceAddress()!=null && quotation.getServiceAddress().getAddrLine1()!=null && 
							!quotation.getServiceAddress().getAddrLine1().equals("")){
						adrsValString1 = quotation.getServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address ="+adrsValString1);

					}
				}
				
//			}
//		}

//		if (!cust.getSecondaryAdress().getAddrLine1().equals("")
//				&& customerbranchlist.size() == 0) {
//			customerPoc = invoiceentity.getPersonInfo().getPocName();
//		}
//
//		System.out.println("Inside Customer branch  "
//				+ customerbranchlist.size());
//		for (int i = 0; i < customerbranchlist.size(); i++) {
//
//			customerPoc = customerbranchlist.get(i).getPocName();
//		}

		/** ends for Manisha **/

		Phrase address2Val = new Phrase(adrsValString1, font10);// Date
																// 9/12/2017 By
																// Jayshree
																// increse the
																// Font size
		PdfPCell address2ValCell = new PdfPCell(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String gstTin2Str = "";
//		if (invoiceentity.getGstinNumber() != null
//				&& !invoiceentity.getGstinNumber().equalsIgnoreCase("")) {
//			gstTin2Str = invoiceentity.getGstinNumber().trim();
//		} else {
			ServerAppUtility serverAppUlAppUtility = new ServerAppUtility();

			gstTin2Str = serverAppUlAppUtility.getGSTINOfCustomer(cust,
					null,"Quotation");

//		}

		Phrase gstTin2 = new Phrase("GSTIN", font7bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font10);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2 = new Phrase("Attn", font7bold);
		PdfPCell attn2Cell = new PdfPCell(attn2);
		attn2Cell.setBorder(0);
		attn2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2Val = null;
		if (custName != null) {
			attn2Val = new Phrase(customerPoc, font10);
		} else {
			attn2Val = new Phrase("", font10);
		}
		PdfPCell attn2ValCell = new PdfPCell(attn2Val);
		attn2ValCell.setBorder(0);
		attn2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attn2Cell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attn2ValCell);
		}
		colonTable.addCell(address2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);

		PdfPTable colonTable22 = new PdfPTable(3);
		colonTable22.setWidthPercentage(100);
		try {
			colonTable22.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable email_MobNumber2 = new PdfPTable(6);
		email_MobNumber2.setWidthPercentage(100);
		try {
			email_MobNumber2.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f,
					0.2f, 2.0f });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		email_MobNumber2.addCell(emailCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(emailValCell);

		email_MobNumber2.addCell(mobNoCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(mobValCell);

//		email_MobNumber2.addCell(landlineCell2);
//		email_MobNumber2.addCell(colonCell);
//		email_MobNumber2.addCell(landlinevalCells2);

		// colonTable22.addCell(gstTin2Cell);
		// colonTable22.addCell(colonCell);
		// colonTable22.addCell(gstTin2ValCell);

		PdfPCell cell2 = new PdfPCell(colonTable);
		cell2.setBorder(0);

		PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
		email_MobNumber2Cell.setBorder(0);

		PdfPCell cell22 = new PdfPCell(colonTable22);
		cell22.setBorder(0);

		Phrase state2 = new Phrase("State", font7bold);// Date 9/12/2017 By
														// Jayshree To increse
														// the fontsize by one
		PdfPCell state2Cell = new PdfPCell(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stateName = "";
		if (!cust.getSecondaryAdress().getAddrLine1().equals("")) {
			stateName = cust.getSecondaryAdress().getState().trim();
		} 
//		else {
//			for (int i = 0; i < customerbranchlist.size(); i++) {
//				// if()
//				stateName = customerbranchlist.get(i).getAddress().getState()
//						.trim();
//			}
//
//		}
		Phrase state2Val = new Phrase(stateName, font7bold);// Date 9/12/2017 By
															// Jayshree To
															// increse the
															// fontsize by one
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// stateValCell.addElement(stateVal);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String st2Co = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(stateName.trim())) {
				st2Co = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font7bold);// Date
																	// 9/12/2017
																	// By
																	// Jayshree
																	// To
																	// increse
																	// the
																	// fontsize
																	// by one
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(st2Co, font7bold);// Date 9/12/2017 By
															// Jayshree To
															// increse the
															// fontsize by one
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);


		PdfPCell stateCell_stateCode2 = new PdfPCell(colonTable);
		stateCell_stateCode2.setBorder(0);

		part2Table.addCell(cell2);
		/**
		 * Date 13/12/2017 Dev.By Jayshree Des.To remove the state code Comment
		 * this
		 */
		// part2Table.addCell(stateCell_stateCode2);
		// End By Jayshree
		part2Table.addCell(email_MobNumber2Cell);
		part2Table.addCell(cell22);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);
		/* Part 2 Ends */
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
	
	}

	private void createCustomerDetailsForOnlyCompanyHeader() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		/* Start Part 1 */
		PdfPTable leftTbl = new PdfPTable(1);
		leftTbl.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font7bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable custDetBillTbl = new PdfPTable(3);
		custDetBillTbl.setWidthPercentage(100);
		try {
			custDetBillTbl.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font7bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custName = "";
		if (cust.getCustPrintableName() != null&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
					custName = cust.getCompanyName().trim();
				}
				else{
					custName = "M/S " + " " + cust.getCompanyName().trim();
				}
			} else if (cust.getSalutation() != null&& !cust.getSalutation().equals("")) {
				custName = cust.getSalutation() + " "+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here

		Phrase nameCellVal = new Phrase(fullname, font7bold);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);


		

		Phrase address = new Phrase("Address", font7bold);
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * Date 13-09-2017 added by vijay for customer address first it will
		 * check from contract if in contract customer address is available then
		 * it will print else it will get customer address from customer below
		 * line old code commented
		 */
	
		String adrsValString = "";

		Address serviceAddress = null;
		String custFullAdd1 = "";
		String countryName ="";
		if (contEntity!=null&&contEntity.getBillingAddress() != null) {
			serviceAddress = contEntity.getBillingAddress();
			custFullAdd1 = contEntity.getBillingAddress().getCompleteAddress();
			logger.log(Level.SEVERE,"Quotation Service Address::"+custFullAdd1);
			countryName = contEntity.getBillingAddress().getCountry();
		} else if (quotEntity!=null&&quotEntity.getBillingAddress() != null) {
			serviceAddress = quotEntity.getBillingAddress();
			custFullAdd1 = quotEntity.getBillingAddress().getCompleteAddress();
			logger.log(Level.SEVERE,"Quotation Service Address::"+custFullAdd1);
			countryName = contEntity.getBillingAddress().getCountry();

		}else {
			serviceAddress = cust.getAdress();
			custFullAdd1 = cust.getAdress().getCompleteAddress();
			logger.log(Level.SEVERE,"Quotation Service Address::"+custFullAdd1);
			countryName = cust.getAdress().getCountry();
		}
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
		custFullAdd1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", custFullAdd1);
		
		Phrase address2Val = new Phrase (custFullAdd1,font7);
		PdfPCell addressValCell = new PdfPCell(address2Val);
		
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase attn = new Phrase("Attn", font7bold);
		PdfPCell attnCell = new PdfPCell(attn);
		attnCell.setBorder(0);
		attnCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String pocName="";
		if(contEntity!=null&&contEntity.getCinfo().getPocName()!=null&&!contEntity.getCinfo().getPocName().equals("")){
			pocName=contEntity.getCinfo().getPocName();
		}else if(quotEntity!=null&&quotEntity.getCinfo().getPocName()!=null&&!quotEntity.getCinfo().getPocName().equals("")){
			pocName=quotEntity.getCinfo().getPocName();
		}
		
//		Phrase attnVal = new Phrase(contEntity.getCinfo().getPocName(),font10);
		//5-08-2024 need salutation before poc name as per ultima search
		Phrase attnVal = null;
		if (cust.getSalutation() != null&& !cust.getSalutation().equals(""))
				attnVal=new Phrase(cust.getSalutation() + " "+contEntity.getCinfo().getPocName(),
								font10);
		else
				attnVal=new Phrase(contEntity.getCinfo().getPocName(),
									font10);
		
		
		PdfPCell attnValCell = new PdfPCell(attnVal);
		attnValCell.setBorder(0);
		attnValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		custDetBillTbl.addCell(nameCell);
		custDetBillTbl.addCell(colonCell);
		custDetBillTbl.addCell(nameCellValCell);
		
		if (printAttnInPdf) {
			/**
			 * @author Anil @since 23-07-2021
			 * If poc name is not added then dont print the blank label also
			 */
			if(pocName!=null&&!pocName.equals("")){
				custDetBillTbl.addCell(attnCell);
				custDetBillTbl.addCell(colonCell);
				custDetBillTbl.addCell(attnValCell);
			}
		}
		custDetBillTbl.addCell(addressCell);
		custDetBillTbl.addCell(colonCell);
		custDetBillTbl.addCell(addressValCell);
		
		
		/**
		 * @author EVA25 @since 19-08-2021
		 */
		System.out.println("Article Information Priyanka");
//		String documentName="";
//		if(quotEntity!=null){
//			documentName="Quotation";
//		}else{
//			documentName="Contract";
//		}
		
		
		
		
//		/**
//		 * Date 23-11-2018 By VIjay 
//		 * for GST Number not print with process Config 
//		 */		
//		PdfPCell gstTinCell = null;
//		PdfPCell gstTinValCell = null;
//		PdfPCell stateCodeCell = null;
//		PdfPCell stateCodeValCell = null;
//		PdfPCell colon3Cell = null;
//		
//		String custGstinNum = "";
//		String stateCd = "";
//		if (gstNumberPrintFlag) {
//			
//			ServerAppUtility serverAppUtility = new ServerAppUtility();
//			custGstinNum = serverAppUtility.getGSTINOfCustomer(cust, null,"Contract");// Date 1/2/2018 By jayshree add new parameter
//
//			Phrase gstTin = new Phrase("GSTIN", font7bold);
//			gstTinCell = new PdfPCell(gstTin);
//			gstTinCell.setBorder(0);
//			gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//			Phrase gstTinVal = new Phrase(custGstinNum, font10);
//			gstTinValCell = new PdfPCell(gstTinVal);
//			gstTinValCell.setBorder(0);
//			gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			/**
//			 * Date13/12/2017 By Jayshree To Add state code
//			 */
//
//			/**
//			 * Date 1/2/2018 By jayshree Des.remove the statecode if GSTIN is
//			 * not present
//			 */
//			Phrase stateCode = null;
//			if (custGstinNum != null && !custGstinNum.equals("")) {
//				stateCode = new Phrase("State Code", font7bold);
//			} else {
//				stateCode = new Phrase(" ", font7bold);
//			}
//			stateCodeCell = new PdfPCell(stateCode);
//			stateCodeCell.setBorder(0);
//			stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//			Phrase colon3 = null;
//			if (custGstinNum != null && !custGstinNum.equals("")) {
//				colon3 = new Phrase(":", font7bold);
//			} else {
//				colon3 = new Phrase(" ", font7bold);
//			}
//			colon3Cell = new PdfPCell(colon3);
//
//			colon3Cell.setBorder(0);
//			colon3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//			for (int i = 0; i < stateList.size(); i++) {
//				if (stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim())) {
//					stateCd = stateList.get(i).getStateCode().trim();
//					break;
//				}
//			}
//
//			Phrase stateCodeVal = null;
//			if (custGstinNum != null && !custGstinNum.equals("")) {
//				stateCodeVal = new Phrase(stateCd, font10);
//			} else {
//				stateCodeVal = new Phrase(" ", font10);
//			}
//			
//			stateCodeValCell = new PdfPCell(stateCodeVal);
//
//			stateCodeValCell.setBorder(0);
//			stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		}

		PdfPTable custGstinTbl = new PdfPTable(3);
		custGstinTbl.setWidthPercentage(100);
		try {
			//custGstinTbl.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f, 0.2f,2.0f }); //columnCollonWidth
			custGstinTbl.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase colon2 = new Phrase(":", font7bold);
		PdfPCell colon2Cell = new PdfPCell(colon2);
		colon2Cell.setBorder(0);
		colon2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		System.out.println("Article type size"+cust.getArticleTypeDetails().size());
		if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
			System.out.println("Article type size"+cust.getArticleTypeDetails().size());
			for(ArticleType type:cust.getArticleTypeDetails()){
				System.out.println("type.getDocumentName()==="+type.getDocumentName());
				if(type.getDocumentName().equalsIgnoreCase("Contract")&&type.getArticlePrint().equalsIgnoreCase("Yes")||type.getDocumentName().equalsIgnoreCase("Quotation")&&type.getArticlePrint().equalsIgnoreCase("Yes")){
					if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")){
						continue;
					}
					System.out.println("type.getArticleTypeName()==="+type.getArticleTypeName());
					Phrase articlname = new Phrase(type.getArticleTypeName(), font8);
					PdfPCell articlnameCell = new PdfPCell(articlname);
					articlnameCell.setBorder(0);
					articlnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					System.out.println("type.getArticleTypeValue()==="+type.getArticleTypeValue());
					Phrase articlnameCellVal = new Phrase(type.getArticleTypeValue(), font8);
					PdfPCell articlnameCellValCell = new PdfPCell(articlnameCellVal);
					articlnameCellValCell.setBorder(0);
					articlnameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					custGstinTbl.addCell(articlnameCell);
					custGstinTbl.addCell(colonCell);
					custGstinTbl.addCell(articlnameCellValCell);
					
//					custDetBillTbl.addCell(pdfUtility.getCell(type.getArticleTypeName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//					custDetBillTbl.addCell(colonCell);
//					custDetBillTbl.addCell(pdfUtility.getCell(type.getArticleTypeValue(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				}
			}
		}
		
//		/**
//		 * Date 23-11-2018 By VIjay 
//		 * for GST Number not print with process Config 
//		 */		
//		if(gstNumberPrintFlag){
//			/**
//			 * @author Anil @since 23-07-2021
//			 * if GSTIN num is not added then we will not print lable as well
//			 */
//			if(custGstinNum!=null&&!custGstinNum.equals("")){
//				custGstinTbl.addCell(gstTinCell);
//				custGstinTbl.addCell(colon2Cell);
//				custGstinTbl.addCell(gstTinValCell);
//				
//				if(stateCd!=null&&!stateCd.equals("")){
//					custGstinTbl.addCell(stateCodeCell);
//					custGstinTbl.addCell(colon3Cell);
//					custGstinTbl.addCell(stateCodeValCell);
//				}else{
//					custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//					custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//					custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				}
//			}else{
//				custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				
//				custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				custGstinTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//			}
//		}
		
		
		Phrase email = new Phrase("Email", font7bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
		String emailOne ="";
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			emailOne = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				emailOne = cust.getEmail();
			} else {
				emailOne ="";
			}
		}
		 
		
//		
//		if (cust.getEmail() != null) {
//			email1Val1 = cust.getEmail();
//		} else {
//			email1Val1 ="";
//		}
		//Phrase email1Val = new Phrase();
		Phrase email1ValOne = new Phrase(emailOne,font10);
		
		
//		Phrase emailVal = null;
//		if (cust.getEmail() != null) {
//			emailVal = new Phrase(cust.getEmail(), font10);
//		} else {
//			emailVal = new Phrase("", font10);
//		}
		
		PdfPCell emailValCell = new PdfPCell(email1ValOne);
		emailValCell.setBorder(0);
		emailValCell.setColspan(4);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo = new Phrase("Phone", font7bold);
		PdfPCell mobNoCell = new PdfPCell(mobNo);
		mobNoCell.setBorder(0);
		mobNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String phone="";
		
		if (cust.getCellNumber1() != null && cust.getCellNumber1() != 0) {
			System.out.println("pn11");
			phone = cust.getCellNumber1() + "";
			
			/**
			 * @author Vijay Date :- 22-09-2021
			 * Des :- Adding country code(from country master) in cell number.
			 */
			phone = serverAppUtility.getMobileNoWithCountryCode(phone, countryName, comp.getCompanyId());
			
		}
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0) {
			if (!phone.trim().isEmpty()) {
//				phone = phone + " / " + cust.getCellNumber2() + "";
				/**
				 * @author Vijay Date :- 22-09-2021
				 * Des :- Adding country code(from country master) in cell number.
				 */
				String cellNumber2 = serverAppUtility.getMobileNoWithCountryCode(cust.getCellNumber2()+"", countryName, comp.getCompanyId());
				phone = phone + " / " + cellNumber2 + "";

			} else {
				phone = cust.getCellNumber2() + "";
			}
			System.out.println("pn33" + phone);
		}
		if (cust.getLandline() != 0 && cust.getLandline() != null) {
			if (!phone.trim().isEmpty()) {
				phone = phone + " / " + cust.getLandline() + "";
			} else {
				phone = cust.getLandline() + "";
			}
			System.out.println("pn44" + phone);
		}

		Phrase mobVal = new Phrase(phone, font10);
		PdfPCell mobValCell = new PdfPCell(mobVal);
		mobValCell.setBorder(0);
		mobValCell.setColspan(4);//add Calspane by jayshree
		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable custEmailPhnTbl = new PdfPTable(6);
		custEmailPhnTbl.setWidthPercentage(100);
		try {
			custEmailPhnTbl.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f,
					0.2f, 2.0f });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		/**
		 * @author Anil @since 23-07-2021
		 * If customer email and phone is not added then we will not print label as well
		 */
		
		//emailOne
		if(emailOne!=null&& !emailOne.equals("")){
		//if(cust.getEmail()!=null&& !cust.getEmail().equals("")){
			custEmailPhnTbl.addCell(emailCell);
			custEmailPhnTbl.addCell(colon2Cell);
			custEmailPhnTbl.addCell(emailValCell);
			
			if(phone!=null&&!phone.equals("")){
				custEmailPhnTbl.addCell(mobNoCell);
				custEmailPhnTbl.addCell(colon2Cell);
				custEmailPhnTbl.addCell(mobValCell);
			}else{
				custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
		}else{
			if(phone!=null&&!phone.equals("")){
				custEmailPhnTbl.addCell(mobNoCell);
				custEmailPhnTbl.addCell(colon2Cell);
				custEmailPhnTbl.addCell(mobValCell);
			}else{
				custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
			custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custEmailPhnTbl.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
		}
		
		
		
		
		
		PdfPCell custDetBillCell = new PdfPCell(custDetBillTbl);
		custDetBillCell.setBorder(0);

		PdfPCell custEmailPhnCell = new PdfPCell(custEmailPhnTbl);
		custEmailPhnCell.setBorder(0);

		PdfPCell custGstinCell = new PdfPCell(custGstinTbl);
		custGstinCell.setBorder(0);
		
		custDetBillTbl = new PdfPTable(3);
		custDetBillTbl.setWidthPercentage(100);
		try {
			custDetBillTbl.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		custDetBillTbl.addCell(colonCell);

		PdfPTable pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPCell state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		custDetBillTbl.addCell(state4Cell); 

		PdfPCell stateCell_stateCode = new PdfPCell(custDetBillTbl);
		stateCell_stateCode.setBorder(0);

		leftTbl.addCell(custDetBillCell);
		leftTbl.addCell(custEmailPhnCell);
		leftTbl.addCell(custGstinCell);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(leftTbl);
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);
		
		
		/**
		 * Customer Shipping details 
		 */

		custDetBillTbl = new PdfPTable(3);
		custDetBillTbl.setWidthPercentage(100);
		try {
			custDetBillTbl.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font7bold);
		PdfPCell name2Cell = new PdfPCell(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * @author Anil @since 28-10-2021
		 * If single customer branch is selected then print branch name in service address section instead if customer name
		 * raised by Nitin Sir and Pooname for IndoGulf 
		 */
		String custBranchName=fullname;
		if(customerBranch!=null){
			custBranchName=customerBranch.getBusinessUnitName();
		}
		
		Phrase name2CellVal = new Phrase(custBranchName, font7bold);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase email1 = new Phrase("Email", font7bold);
		PdfPCell email1Cell = new PdfPCell(email1);
		email1Cell.setBorder(0);
		email1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String emailvalue ="";
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null){
			emailvalue = customerBranch.getEmail();
		}else{
			emailvalue="";
			}
		
		}else{
			if (cust.getEmail() != null) {
				emailvalue = cust.getEmail();
			} else {
				emailvalue ="";
			}
		}
		 
		
//		
//		if (cust.getEmail() != null) {
//			email1Val1 = cust.getEmail();
//		} else {
//			email1Val1 ="";
//		}
		//Phrase email1Val = new Phrase();
		Phrase email1Val = new Phrase(emailvalue,font10);
		
		
		
//		Phrase email1Val = null;
//		if (cust.getEmail() != null) {
//			email1Val = new Phrase(cust.getEmail(), font10);
//		} else {
//			email1Val = new Phrase("", font10);
//		}
		
		PdfPCell email1ValCell = new PdfPCell(email1Val);
		email1ValCell.setBorder(0);
		email1ValCell.setColspan(4);
		email1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo2 = new Phrase("Mobile", font7bold);
		PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
		mobNo2Cell.setBorder(0);
		mobNo2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String adrsValString1 = "";
		String customerPoc = null;

		Address serviceAddress1 = null;
		String CountryName2 = "";
		if (contEntity!=null&& contEntity.getCustomerServiceAddress() != null) {
			serviceAddress1 = contEntity.getCustomerServiceAddress();
			adrsValString1 = contEntity.getCustomerServiceAddress().getCompleteAddress();
			CountryName2 = contEntity.getCustomerServiceAddress().getCountry();

		}else if (quotEntity!=null&&quotEntity.getServiceAddress() != null) {
			serviceAddress1 = quotEntity.getServiceAddress();
			adrsValString1 = quotEntity.getServiceAddress().getCompleteAddress();
			CountryName2 = quotEntity.getServiceAddress().getCountry();

		}  else {
			serviceAddress1 = cust.getSecondaryAdress();
			adrsValString1 = cust.getSecondaryAdress().getCompleteAddress();
			CountryName2 = cust.getSecondaryAdress().getCountry();
		}

		
		String phone2 = "";

		if (cust.getCellNumber1() != null && cust.getCellNumber1() != 0) {
			System.out.println("pn11");
			phone2 = cust.getCellNumber1() + "";
			/**
			 * @author Vijay Date :- 22-09-2021
			 * Des :- Adding country code(from country master) in cell number.
			 */
			phone2 = serverAppUtility.getMobileNoWithCountryCode(phone, CountryName2, comp.getCompanyId());
		}
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0) {
			if (!phone2.trim().isEmpty()) {
//				phone2 = phone2 + " / " + cust.getCellNumber2() + "";
				/**
				 * @author Vijay Date :- 22-09-2021
				 * Des :- Adding country code(from country master) in cell number.
				 */
				String cellNumber2 = serverAppUtility.getMobileNoWithCountryCode(cust.getCellNumber2()+"", CountryName2, comp.getCompanyId());
				phone2 = phone2 + " / " + cellNumber2 + "";
				
			} else {
				phone2 = cust.getCellNumber2() + "";
			}
			System.out.println("pn33" + phone2);
		}
		if (cust.getLandline() != 0 && cust.getLandline() != null) {
			if (!phone2.trim().isEmpty()) {
				phone2 = phone2 + " / " + cust.getLandline() + "";
			} else {
				phone2 = cust.getLandline() + "";
			}
			System.out.println("pn44" + phone2);
		}
		

		Phrase mobNo2Val = new Phrase(phone2, font10);
		PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);
		mobNo2ValCell.setBorder(0);
		mobNo2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase landlineph2 = new Phrase("LandLine", font7bold);
		PdfPCell landlineCell2 = new PdfPCell(landlineph2);
		landlineCell2.setBorder(0);
		landlineCell2.setHorizontalAlignment(Element.ALIGN_LEFT);

		String landline2 = "";
		if (cust.getLandline() != 0) {
			landline2 = cust.getLandline() + "";
		} else {
			landline2 = "";
		}

		Phrase landlineVal2 = new Phrase(landline2, font10);
		PdfPCell landlinevalCells2 = new PdfPCell(landlineVal2);
		landlinevalCells2.setBorder(0);
		landlinevalCells2.setHorizontalAlignment(Element.ALIGN_LEFT);


		Phrase address2 = new Phrase("Address", font7bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);


		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
		adrsValString1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString1);

		Phrase addressVal = new Phrase(adrsValString1, font10);
		PdfPCell address2ValCell = new PdfPCell(addressVal);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String gstTin2Str = "";
		ServerAppUtility serverAppUlAppUtility = new ServerAppUtility();
		gstTin2Str = serverAppUlAppUtility.getGSTINOfCustomer(cust,null,"Contract");

		Phrase gstTin2 = new Phrase("GSTIN", font7bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font10);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2 = new Phrase("Attn", font7bold);
		PdfPCell attn2Cell = new PdfPCell(attn2);
		attn2Cell.setBorder(0);
		attn2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2Val = null;
		if (custName != null) {
			attn2Val = new Phrase(customerPoc, font10);
		} else {
			attn2Val = new Phrase("", font10);
		}
		PdfPCell attn2ValCell = new PdfPCell(attn2Val);
		attn2ValCell.setBorder(0);
		attn2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/**
		 * @author Anil @since 28-10-2021
		 * RIght table - Service address
		 */
		custDetBillTbl = new PdfPTable(3);
		custDetBillTbl.setWidthPercentage(100);
		try {
			custDetBillTbl.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		custDetBillTbl.addCell(nameCell);
		custDetBillTbl.addCell(colonCell);
		custDetBillTbl.addCell(name2CellValCell);
		if (printAttnInPdf) {
			/**
			 * @author Anil @since 23-07-2021
			 * If poc name is not added then dont print the blank label also
			 */
			if(customerPoc!=null&&!customerPoc.equals("")){
				custDetBillTbl.addCell(attn2Cell);
				custDetBillTbl.addCell(colonCell);
				custDetBillTbl.addCell(attn2ValCell);
			}
		}
		custDetBillTbl.addCell(address2Cell);
		custDetBillTbl.addCell(colonCell);
		custDetBillTbl.addCell(address2ValCell);

		PdfPTable colonTable22 = new PdfPTable(3);
		colonTable22.setWidthPercentage(100);
		try {
			colonTable22.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		PdfPTable email_MobNumber2 = new PdfPTable(6);
		email_MobNumber2.setWidthPercentage(100);
		try {
			email_MobNumber2.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f,0.2f, 2.0f });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
//		email_MobNumber2.addCell(emailCell);
//		email_MobNumber2.addCell(colonCell);
//		email_MobNumber2.addCell(emailValCell);
//
//		email_MobNumber2.addCell(mobNoCell);
//		email_MobNumber2.addCell(colonCell);
//		email_MobNumber2.addCell(mobValCell);
		
		
		/**
		 * @author Anil @since 23-07-2021
		 * If customer email and phone is not added then we will not print label as well
		 */
		//emailvalue
		 
		if(emailvalue!=null&& !emailvalue.equals("")){
		//if(cust.getEmail()!=null&& !cust.getEmail().equals("")){
			email_MobNumber2.addCell(emailCell);
			email_MobNumber2.addCell(colon2Cell);
			email_MobNumber2.addCell(emailValCell);
			
			if(phone!=null&&!phone.equals("")){
				email_MobNumber2.addCell(mobNoCell);
				email_MobNumber2.addCell(colon2Cell);
				email_MobNumber2.addCell(mobValCell);
			}else{
				email_MobNumber2.addCell(new Phrase(" ", font10));
				email_MobNumber2.addCell(new Phrase(" ", font10));
				email_MobNumber2.addCell(new Phrase(" ", font10));
			}
		}else{
			if(phone!=null&&!phone.equals("")){
				email_MobNumber2.addCell(mobNoCell);
				email_MobNumber2.addCell(colon2Cell);
				email_MobNumber2.addCell(mobValCell);
			}else{
				email_MobNumber2.addCell(new Phrase(" ", font10));
				email_MobNumber2.addCell(new Phrase(" ", font10));
				email_MobNumber2.addCell(new Phrase(" ", font10));
			}
			email_MobNumber2.addCell(new Phrase(" ", font10));
			email_MobNumber2.addCell(new Phrase(" ", font10));
			email_MobNumber2.addCell(new Phrase(" ", font10));
			
		}

		PdfPCell cell2 = new PdfPCell(custDetBillTbl);
		cell2.setBorder(0);

		PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
		email_MobNumber2Cell.setBorder(0);

		PdfPCell cell22 = new PdfPCell(colonTable22);
		cell22.setBorder(0);

		Phrase state2 = new Phrase("State", font7bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stateName = "";
		if (!cust.getSecondaryAdress().getAddrLine1().equals("")) {
			stateName = cust.getSecondaryAdress().getState().trim();
		} 

		Phrase state2Val = new Phrase(stateName, font13);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String st2Co = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim().equalsIgnoreCase(stateName.trim())) {
				st2Co = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font7bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(st2Co, font13);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		custDetBillTbl = new PdfPTable(3);
		custDetBillTbl.setWidthPercentage(100);
		try {
			custDetBillTbl.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		if(st2Co!=null&&!st2Co.equals("")){
			custDetBillTbl.addCell(state2Cell);
			custDetBillTbl.addCell(colonCell);
			custDetBillTbl.addCell(state2ValCell);
		}


		PdfPCell stateCell_stateCode2 = new PdfPCell(custDetBillTbl);
		stateCell_stateCode2.setBorder(0);
		part2Table.addCell(cell2);
		
		part2Table.addCell(email_MobNumber2Cell);
		part2Table.addCell(cell22);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}


	/*@author Abhinav Bihade
	* @since 20/01/2020
	* As per Rahul Tiwari's Requirement Bitco : Print customer branch address in contract Pdf Tested by Sonu Porel 
	*/
	private ArrayList<String> getCustomerBranchList(List<SalesLineItem> itemList) {
		HashSet<String> branchHs=new HashSet<String>();
		
		for(SalesLineItem itemObj:itemList){
			if(itemObj.getCustomerBranchSchedulingInfo()!=null){
				ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
				if(branchSchedulingList!=null) {
					for(BranchWiseScheduling obj:branchSchedulingList){
						if(obj.isCheck()==true){
							branchHs.add(obj.getBranchName());
						}
					}				
				}
			}
		}
		
		if(branchHs!=null&&branchHs.size()!=0){
			ArrayList<String> branchList=new ArrayList<String>(branchHs);
			logger.log(Level.SEVERE,"In Side AList3:"+branchList.size());
			return branchList;
		}
		
		return null;
	}

	private void createHeadingOfbillingandServiceAddress() {
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase billingAddress = new Phrase("Billing Address", font8bold);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(billAdressCell);
		
		Phrase serviceaddress = new Phrase("Service Address", font8bold);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(serviceCell);
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyAsheader() {
		// Date 20/11/2017 By Jayshree
		// to add the logo in Table
		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * Ends For Jayshree
		 */

		/**
		 * Developer : Jayshree Date : 21 Nov 2017 Description : Logo position
		 * adjustment
		 */
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		
		/**
		 *  Added by Priyanka : 02-02-2021
		 *  Des - Branch correspondence name should be print in the place of branch name when branchAsCompany process config is active 
		 *        issue raise by ashwini. 
		 */
		String companyname ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname=branchDt.getCorrespondenceName();
		}else{
			companyname =comp.getBusinessUnitName().trim().toUpperCase();
		}

		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font10bold);
		companynamepara.setAlignment(Element.ALIGN_LEFT);

		
//		Phrase companyName = null;
//		if (comp != null) {
//			companyName = new Phrase(comp.getBusinessUnitName().trim(),
//					font16bold);
//		}
//
//		Paragraph companyNamepara = new Paragraph();
//		companyNamepara.add(companyName);
//		/**
//		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
//		 * alignment
//		 */

		if (checkheaderLeft == true) {
			companynamepara.setAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companynamepara.setAlignment(Element.ALIGN_RIGHT);
		} else {
			companynamepara.setAlignment(Element.ALIGN_CENTER);
		}

		// companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companynamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase companyAddr = null;
		if (comp != null) {
			companyAddr = new Phrase(comp.getAddress().getCompleteAddress().trim(), font12);
		}
		Paragraph companyAddrpara = new Paragraph();
		companyAddrpara.add(companyAddr);
		/**
		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
		 * alignment
		 */

		if (checkheaderLeft == true) {
			companyAddrpara.setAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyAddrpara.setAlignment(Element.ALIGN_RIGHT);
		} else {
			companyAddrpara.setAlignment(Element.ALIGN_CENTER);
		}

		// companyAddrpara.setAlignment(Element.ALIGN_CENTER);
		/**
		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
		 * alignment
		 */
		PdfPCell companyAddrCell = new PdfPCell(companyAddrpara);
		// companyAddrCell.addElement();
		companyAddrCell.setBorder(0);
		if (checkheaderLeft == true) {
			companyAddrCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyAddrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		/**
		 * Ends
		 */

		// companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyGSTTIN = null;
		String gstinValue = "";
		if (UniversalFlag) {
			if (contEntity.getGroup().equalsIgnoreCase("Universal Pest Control Pvt. Ltd.")) {

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
						break;
					}
				}

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (!comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = gstinValue
								+ ","
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
					}
				}
			}
		} else {

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
					break;
				}
			}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = gstinValue
							+ ","
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
				}
			}
		}

		if (!gstinValue.equals("")) {
			companyGSTTIN = new Phrase(gstinValue, font12bold);
		}

		Paragraph companyGSTTINpara = new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyGSTTINCell = new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		/**
		 * Date 12/1/2018 Dev.Jayshree Des.To add the company Email And Branch
		 * Email
		 */
		String branchmail = "";
		ServerAppUtility serverApp = new ServerAppUtility();

		if (qp instanceof Contract) {
			if (checkEmailId == true) {
				branchmail = serverApp.getBranchEmail(comp,
						contEntity.getBranch());
				System.out.println("server method " + branchmail);

			} else {
				branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
				System.out.println("server method 22" + branchmail);
			}
		} else {

			if (checkEmailId == true) {
				branchmail = serverApp.getBranchEmail(comp,
						quotEntity.getBranch());
				System.out.println("server method " + branchmail);

			} else {
				branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
				System.out.println("server method 22" + branchmail);
			}

		}
		String email = null;
		if (branchmail != null) {
			email = "Email : " + branchmail;
		} else {
			email = "Email : ";
		}

		/**
		 * By Jayshree Date 28/12/2017 to handle null condition
		 */

		String website = "";
		if (comp.getWebsite() == null || comp.getWebsite().equals("")) {
			website = " ";
		} else {

			website = "Website : " + comp.getWebsite();
		}
		/**
		 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
		 * add the LandLine no.
		 */

		/**
		 * Comment by jayshree the below code
		 */
		// if (comp.getCellNumber2() != 0) {
		// number = "Phone " + comp.getCellNumber1() + ","
		// + comp.getCellNumber2();
		// } else if (comp.getCellNumber1() != 0) {
		// number = "Phone " + comp.getCellNumber1();
		// } else {
		// number = "Phone ";
		// }
		// String landline = "";
		// if (comp.getLandline() != 0) {
		// System.out.println("comp.getLandline()" + comp.getLandline());
		// landline = "Landline  " + comp.getLandline();
		// } else {
		// landline = "";
		// }
		/**
		 * Above code is Commented bY Jayshree
		 */
		/**
		 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
		 * add the LandLine no.
		 */

		// Date 31--2018 changes done by jayshree
		String number = "";
		String landline = "";

		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			System.out.println("pn11");
			number = comp.getCountryCode() + comp.getCellNumber1() + "";
		}
		if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
			if (!number.trim().isEmpty()) {
				number = number + " , " + comp.getCountryCode()
						+ comp.getCellNumber2() + "";
			} else {
				number = comp.getCountryCode() + comp.getCellNumber2() + "";
			}
			System.out.println("pn33" + number);
		}
		if (comp.getLandline() != 0 && comp.getLandline() != null) {
			if (!number.trim().isEmpty()) {
				number = number + " , " + comp.getStateCode()
						+ comp.getLandline() + "";
			} else {
				number = comp.getStateCode() + comp.getLandline() + "";
			}
			System.out.println("pn44" + number);
		}

		/**
		 * Date 31-3-2018 By jayshree Des.to print the head off.email
		 */
		String hoid = null;
		if (hoEmail == true) {
			hoid = "HO Email : " + comp.getEmail();
		}

		Phrase hoemail = new Phrase(hoid, font11);
		PdfPCell hocell = new PdfPCell(hoemail);
		hocell.setBorder(0);
		if (checkheaderLeft == true) {
			hocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			hocell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			hocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		Phrase branchmailph = new Phrase(email, font11);
		PdfPCell branchmailcell = new PdfPCell(branchmailph);
		branchmailcell.setBorder(0);
		if (checkheaderLeft == true) {
			branchmailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			branchmailcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			branchmailcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		PdfPCell companyphoneCell = null;
		if (number != null && !number.trim().isEmpty()) {
			companyphoneCell = new PdfPCell(new Phrase("Phone " + number,
					font11));
		} else {
			companyphoneCell = new PdfPCell(new Phrase("", font11));
		}
		companyphoneCell.setBorder(0);
		if (checkheaderLeft == true) {
			companyphoneCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyphoneCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			companyphoneCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		PdfPCell companyweb = new PdfPCell(new Phrase("" + website, font11));
		companyweb.setBorder(0);

		if (checkheaderLeft == true) {
			companyweb.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyweb.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			companyweb.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		PdfPTable pdfPTable = new PdfPTable(1);
		// pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		pdfPTable.addCell(branchmailcell);
		if (hoEmail == true) {
			pdfPTable.addCell(hocell);
		}
		pdfPTable.addCell(companyphoneCell);
		pdfPTable.addCell(companyweb);

		// End By jayshree 31-3-2018

		/**
		 * Developer:Jayshree Date 21/11/2017 Description:changes are done to
		 * add the logo and website at proper position
		 */
		PdfPTable mainheader = new PdfPTable(2);
		mainheader.setWidthPercentage(100);

		try {
			mainheader.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		if (imageSignCell != null) {
			PdfPCell leftCell = new PdfPCell(logoTab);
			leftCell.setBorder(0);
			mainheader.addCell(leftCell);

			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			mainheader.addCell(rightCell);
		} else {
			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			mainheader.addCell(rightCell);
		}

		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof Contract) {
			Contract conEntity = (Contract) qp;
			conEnt = conEntity.getContractDate();
		} else {
			Quotation quotEnt = (Quotation) qp;
			conEnt = quotEnt.getCreationDate();
		}

		String countinfo = "ID : " + qp.getCount() + "";

		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font12bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlepdfcell = new PdfPCell(titlephrase);

		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdfcell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlepdfcell.setFixedHeight(20);

		Phrase idphrase = new Phrase(countinfo, font12bold);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell countcell = new PdfPCell(idphrase);

		countcell.setBorderWidthRight(0);
		countcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		countcell.setVerticalAlignment(Element.ALIGN_CENTER);
		countcell.setFixedHeight(20);

		Phrase dateofpdf = new Phrase(creationdateinfo, font12bold);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);

		PdfPCell creationcell = new PdfPCell(dateofpdf);

		creationcell.setBorderWidthLeft(0);
		creationcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creationcell.setVerticalAlignment(Element.ALIGN_CENTER);
		creationcell.setFixedHeight(20);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {

			document.add(titlepdftable);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	private void createProductWithoutDiscDetail() {

		/**
		 * @author Anil
		 * @since 30-06-2020
		 * If service wise rate is active then we have to hide area col
		 * for sai care by Vaishnavi and Nitin sir
		 */
		boolean isHideAreaCol=isServiceWiseRate(qp.getItems());
		
		if(printModelSerailNoFlag && printProductPremisesFlag){
			noOfLines = 12;
		}else {
			if (nubofprod == true) {
				System.out.println("one8");
				noOfLines = 8;
			} else {
				System.out.println("two10");
//				noOfLines = 10;
				/**
				 * @author Anil @since 29-09-2021
				 * increasing line no by 2
				 * for sai care requirment raised by Rutuza and Nitin Sir
				 */
				noOfLines = 12;
			}
		}
		/**
		 * rohan added this code for adding premises in the pdf
		 */
		

		PdfPTable productTable = new PdfPTable(1);
		productTable.setWidthPercentage(100f);

		/**
		 * ends here
		 */

		Font font1 = new Font(Font.FontFamily.HELVETICA, 7 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font7);// Date
																		// 18/12/2017
																		// by
																		// jayshree
																		// increse
																		// font
																		// size
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(8);
		
		/**
		 * @author Vijay Date 21-12-2022
		 * Des :- when do not print flag is true then new as per ankita designed Quotation modification will print
		 */
//		boolean donotprintTotalflag = false;
		if(quotEntity!=null && quotEntity.isDonotPrintTotal()){
			donotprintTotalflag = true;
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				table = new PdfPTable(10);
			}
			else{
				table = new PdfPTable(9);
			}
		}
		else if(qtyColumnFlag && unitOfMeasurementColumnFlag){
			System.out.println("no of column 9");
			table = new PdfPTable(9);
		}
		table.setWidthPercentage(100);
		/**
		 *  Date : 01/03/2021 By - Priyanka
		 *  Des : Need Sr.no, Product Name, Qty, Discount bold on contract pdf requirement by Pestaid Inc raised by Rahul.
		 */
		Phrase srno = new Phrase("Sr No", font7bold);
//		Phrase product = new Phrase("Service Details", font7bold);
		/**@Sheetal :02-03-2022 Renaming Service from Service Details **/
		Phrase product = new Phrase("Service", font7bold);
//		Phrase area = new Phrase("Area", font7); //commented by Ashwini
		
		
		
		Phrase area = new Phrase(qtylabel, font7bold); //Added by Ashwini
		Phrase UOM = new Phrase("UOM",font7bold);
		
		Phrase startdate = new Phrase("Contract Period", font7bold);
		Phrase noservices = new Phrase("Services", font7bold);
		Phrase rate = new Phrase("Rate", font7bold);
		// Phrase total = new Phrase("Total",font1);

		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellarea = new PdfPCell(area);
		cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellUomCell = new PdfPCell(UOM);
		cellUomCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellstartdate = new PdfPCell(startdate);
		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicecell = new PdfPCell(noservices);
		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);

		// PdfPCell celltotal= new PdfPCell(total);
		// celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		
		int colsspan =1;
		if(!qtyColumnFlag){
			++colsspan;
//			++colsspan; // for UOM
		}
		else{
			if(PC_DoNotPrintQtyFlag){
				++colsspan;
			}
		}
		
//		if(PC_DoNotPrintQtyFlag){
//			++colsspan;
//		}
		
		if(PC_DoNotPrintContractPeriodFlag&&!donotprintTotalflag){ //Ashwini Patil Date:8-1-2024  added condition !donotprintTotalflag as print was getting disturbed when PC_DoNotPrintContractPeriodFlag is active and donotprintTotalflag is also active 
			++colsspan;
		}
		logger.log(Level.SEVERE,"PC_DoNotPrintContractPeriodFlag ="+PC_DoNotPrintContractPeriodFlag);
		
		logger.log(Level.SEVERE,"PC_DoNotPrintQtyFlag ="+PC_DoNotPrintQtyFlag);
		logger.log(Level.SEVERE,"PC_DoNotPrintContractPeriodFlag ="+PC_DoNotPrintContractPeriodFlag);
		logger.log(Level.SEVERE,"qtyColumnFlag ="+qtyColumnFlag);		
		logger.log(Level.SEVERE,"unitOfMeasurementColumnFlag ="+unitOfMeasurementColumnFlag);
		logger.log(Level.SEVERE,"isHideAreaCol ="+isHideAreaCol+" colsspan for title="+colsspan);
		cellproduct.setColspan(colsspan);
		
		
		/**
		 * Date21-4-2018
		 * By Jayshree
		 */
		Phrase disph = new Phrase("Discount", font7bold);
		PdfPCell disCell = new PdfPCell(disph);
		disCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase amtph = new Phrase("Amount", font7bold);
		PdfPCell amtCell = new PdfPCell(amtph);
		amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**
		 *  End
		 */
		
		int ratedisccolsspan =1;
		if(PC_DoNotPrintDiscFlag){
			++ratedisccolsspan;
		}
		
		if(PC_DoNotPrintRateFlag){
			++ratedisccolsspan;
		}
		amtCell.setColspan(ratedisccolsspan);
		
		System.out.println("ratedisccolsspan "+ratedisccolsspan);

		if(donotprintTotalflag){
			logger.log(Level.SEVERE,"in donotprintTotalflag if");
			
			if(colsspan>=3){
				colsspan = colsspan-1;
			}
			
			Phrase taxph = new Phrase("Tax", font7bold);
			PdfPCell taxCell = new PdfPCell(taxph);
			taxCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase gstAmtph = new Phrase("GST Amount", font7bold);
			PdfPCell gstAmtCell = new PdfPCell(gstAmtph);
			gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table.addCell(cellsrNo);
			if(isHideAreaCol){
				cellproduct.setColspan(2);
			}
			logger.log(Level.SEVERE,"cellproduct colspan"+cellproduct.getColspan());
			table.addCell(cellproduct);
			if(isHideAreaCol==false){
//				if(!PC_DoNotPrintQtyFlag)
				if(qtyColumnFlag)
				table.addCell(cellarea);
			}
//			table.addCell(cellstartdate);
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				table.addCell(cellUomCell);
			}
			table.addCell(servicecell);
			
			/**
			 * @author Abhinav Bihade
			 * @since 21/12/2019
			 * For Orkin Hide Rate and Discount Column
			 */
			logger.log(Level.SEVERE,"HideRate1:" +hideRateAndDiscount);
			if(!hideRateAndDiscount){
			logger.log(Level.SEVERE,"HideRate2:" +hideRateAndDiscount);
			table.addCell(cellrate);
			
			/**
			 * Date21-4-2018
			 * By Jayshree
			 */
			if(!PC_DoNotPrintDiscFlag){
				table.addCell(disCell);
			}
			}
			else{
				amtCell.setColspan(3);
			}
			
			table.addCell(taxCell);
			table.addCell(gstAmtCell);

			table.addCell(amtCell);
			
			// table.addCell(celltotal);
			try {
//				table.setWidths(columnWidths9); 
				
				if(qtyColumnFlag && unitOfMeasurementColumnFlag){
					table.setWidths(columnWidths10); 
				}
				else{
					table.setWidths(columnWidths9); 

				}

				
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			// Added BY Jayshree
			
		}
		else{
			table.addCell(cellsrNo);
			if(isHideAreaCol){
				cellproduct.setColspan(2);
			}
			table.addCell(cellproduct);
			if(isHideAreaCol==false){
//				if(!PC_DoNotPrintQtyFlag){
				if(qtyColumnFlag){
					System.out.println("cellarea "+cellarea);
					table.addCell(cellarea);
				}
			}
			System.out.println("qtyColumnFlag"+qtyColumnFlag);
			System.out.println("unitOfMeasurementColumnFlag"+unitOfMeasurementColumnFlag);

			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				System.out.println("UOM");
				table.addCell(cellUomCell);
			}
			
//			table.addCell(cellarea);
			if(!PC_DoNotPrintContractPeriodFlag){
				table.addCell(cellstartdate);
			}
			table.addCell(servicecell);
			
			/**
			 * @author Abhinav Bihade
			 * @since 21/12/2019
			 * For Orkin Hide Rate and Discount Column
			 */
			logger.log(Level.SEVERE,"HideRate1:" +hideRateAndDiscount);
			if(!hideRateAndDiscount){
			logger.log(Level.SEVERE,"HideRate2:" +hideRateAndDiscount);
			if(!PC_DoNotPrintRateFlag){
				table.addCell(cellrate);

			}
			
			/**
			 * Date21-4-2018
			 * By Jayshree
			 */
			if(!PC_DoNotPrintDiscFlag){
				table.addCell(disCell);
			}

			}
			else{
				amtCell.setColspan(3);
			}
			
			table.addCell(amtCell);
			
			// table.addCell(celltotal);
			try {
//				table.setWidths(columnWidths7);
				
				if(qtyColumnFlag && unitOfMeasurementColumnFlag){
					table.setWidths(columnWidths9);
				}
				else{
					table.setWidths(columnWidths7);
				}
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			// Added BY Jayshree
			
		}
		
		

		
		/** date 06-02-2018 added by komal for consolidate price**/
		double rateAmountProd = 0;
		double consolidatedTotalAmount=0;
		double consolidatedTax=0;//Ashwini Patil Date:26-08-2024
		double consolidatedDiscount=0;//Ashwini Patil Date:26-08-2024
		for (int j = 0; j < qp.getItems().size(); j++) {
			
			double taxVal = removeAllTaxes(products.get(j).getPrduct());
			double calculatedPrice = products.get(j).getPrice() - taxVal;
			rateAmountProd = rateAmountProd + calculatedPrice;
			if(qp instanceof Contract)
				consolidatedTotalAmount+=products.get(j).getTotalAmount();
			else {
				consolidatedTotalAmount+=calculateTotalPrice(products.get(j));
				
				if(quotEntity!=null&&quotEntity.isDonotPrintTotal()) {
					QuotationPdfFormatVersion2 quotationpdfv2 = new QuotationPdfFormatVersion2();
					
					double totalAmount = quotationpdfv2.getTotalAmount(this.products.get(j));

					if(this.products.get(j).getVatTax().getPercentage()>0 && this.products.get(j).getServiceTax().getPercentage()>0){
						consolidatedTax = consolidatedTax+ (totalAmount*this.products.get(j).getVatTax().getPercentage())/100;
						consolidatedTax = consolidatedTax+ (totalAmount*this.products.get(j).getServiceTax().getPercentage())/100;
						
					}
					else if(this.products.get(j).getVatTax().getPercentage()>0 || this.products.get(j).getServiceTax().getPercentage()>0){
						if(this.products.get(j).getVatTax().getPercentage()>0 ){
							consolidatedTax = consolidatedTax+  (totalAmount*this.products.get(j).getVatTax().getPercentage())/100;
				
						}
						else if(this.products.get(j).getServiceTax().getPercentage()>0){
							consolidatedTax = consolidatedTax+  (totalAmount*this.products.get(j).getServiceTax().getPercentage())/100;
						}
						
					}
				}
				
				double totaldis=0;
				double desper=products.get(j).getPercentageDiscount();
				double areval=0;
				boolean isAreaPresent=false;
				try{
				 areval=Double.parseDouble(products.get(j).getArea());
				 isAreaPresent=true;
				}
				catch(Exception e){
					isAreaPresent=false;
				}
				
				if(isAreaPresent){
					totaldis=(products.get(j).getQty()*products.get(j).getPrice()*areval*desper)/100
							+products.get(j).getDiscountAmt();
				}
				else{
					totaldis=(products.get(j).getQty()*products.get(j).getPrice()*desper)/100+
							products.get(j).getDiscountAmt();
				}
				
				consolidatedDiscount=consolidatedDiscount+totaldis;
				
			}
			
			
			
		}
		logger.log(Level.SEVERE,"consolidatedTotalAmount=" +consolidatedTotalAmount+" consolidatedTax="+consolidatedTax+" consolidatedDiscount="+consolidatedDiscount);
		
		/**
		 * end komal
		 */
		for (int i = 0; i < this.products.size(); i++) {
			logger.log(Level.SEVERE,"Number of lines before subtraction : " +noOfLines);
			if (noOfLines == 0 || noOfLines < 0) {
				prouductCount = i;
				break;
			}
			noOfLines = noOfLines - 1;
			
			logger.log(Level.SEVERE,"Number of lines after subtraction : " +noOfLines);

			PdfPTable table1 = new PdfPTable(8);
			
			if(donotprintTotalflag){
				if(quotEntity!=null && qtyColumnFlag && unitOfMeasurementColumnFlag){
					table1 = new PdfPTable(10);
				}
				else{
					table1 = new PdfPTable(9);
				}
			}
			else if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				table1 = new PdfPTable(9);
			}
			table1.setWidthPercentage(100);

			System.out.println("iiiii-=----" + i);
			Phrase chunk = new Phrase((i + 1) + "", font7);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 06-06-2018 by vijay
			 * Des :- for display product warranty period along with product Name
			 */
			String productNamewithWarrantyinfo="";
			if(products.get(i).getWarrantyPeriod()!=0&&!PC_DoNotPrintWarrantyFlag){
				 productNamewithWarrantyinfo = products.get(i).getProductName()+"\n"+" Warranty Period - "+products.get(i).getWarrantyPeriod();	
			}else{
				productNamewithWarrantyinfo= products.get(i).getProductName();
			}
			/**
			 * ends here
			 */

			Phrase chunk1 = new Phrase(productNamewithWarrantyinfo, font7);
			PdfPCell pdfnamecell = new PdfPCell(chunk1);
//			pdfnamecell.setColspan((int) 1.5);
			pdfnamecell.setColspan(colsspan);

			Phrase chunk2 = new Phrase(products.get(i).getArea() + "", font7);
			PdfPCell pdfareacell = new PdfPCell(chunk2);
			pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			Calendar c = Calendar.getInstance();
			c.setTime(products.get(i).getStartDate());
			c.add(Calendar.DATE, products.get(i).getDuration() - 1);
			Date endDt = c.getTime();
			/**
			 * Date : 05/09/2017 BY ANIL
			 */
			String date = "";
			if(thaiPdfFlag){	
				SimpleDateFormat thai = new SimpleDateFormat("dd-MMM-yyyy");
				logger.log(Level.SEVERE,"In thai pdf flag of contract duration. startDate="+products.get(i).getStartDate()+" startDate hours="+products.get(i).getStartDate().getHours()+"startDate mins="+products.get(i).getStartDate().getMinutes());				
				
				Calendar ct = Calendar.getInstance();
				ct.setTime(products.get(i).getStartDate());
				ct.add(Calendar.MINUTE, 90);
				Date startDate=ct.getTime();
//				logger.log(Level.SEVERE,"startDate after adding 90mins="+startDate+" formatted startdate="+thai.format(startDate));
				logger.log(Level.SEVERE,"startDate after adding 90mins="+startDate+" formatted startdate="+thai.format(startDate));
				
				ct.add(Calendar.DATE, products.get(i).getDuration() - 1);
				Date thaiendDt = ct.getTime();
				logger.log(Level.SEVERE,"calculated thaiendDt after adding 90mins="+thaiendDt+" formatted thaiendDt="+thai.format(thaiendDt));
				
//				if (products.get(i).getEndDate() != null) {		
//					Calendar cte = Calendar.getInstance();
//					cte.setTime(products.get(i).getEndDate());
//					cte.add(Calendar.MINUTE, 90);
//					Date endDate=cte.getTime();
//					logger.log(Level.SEVERE,"enddate after adding 90mins="+endDate+" formatted endDate="+thai.format(endDate));
//					
//					date = thai.format(startDate) + " To "+ thai.format(endDate);
//				} else {
					date = thai.format(startDate) + " To "+ thai.format(thaiendDt);
//				}
				
			}else{
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				if (products.get(i).getEndDate() != null) {
					date = fmt.format(products.get(i).getStartDate()) + " To "+ fmt.format(products.get(i).getEndDate());
				} else {
					date = fmt.format(products.get(i).getStartDate()) + " To "+ fmt.format(endDt);
				}			
			}
			/**
			 * End
			 */
			

			chunk = new Phrase(date, font7);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			/**
			 * @author Vijay Chougule
			 * Des :- if below process config is active then it will calculate the total number of services and will display
			 * else block for normal code which no of services will print from contract entity
			 */
			if(complainServiceWithTurnAroundTimeFlag){
				ContractServiceImplementor conimpl = new ContractServiceImplementor();
				ArrayList<SalesLineItem> saleslineitemlist = new ArrayList<SalesLineItem>();
				saleslineitemlist.add(products.get(i));
				int totalNoOfServices = (int) conimpl.getTotalNoOfServices(saleslineitemlist);
				chunk = new Phrase(totalNoOfServices + "",font7);
				pdfSerCell = new PdfPCell(chunk);
				pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			}
			/**
			 * ends here
			 */
			else{
				chunk = new Phrase(products.get(i).getNumberOfServices() + "",font7);
				pdfSerCell = new PdfPCell(chunk);
				pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;
			 System.out.println("price : " + calculatedPrice);
			/** date 06-02-2018 added by komal for consolidate price**/
			table1.addCell(Pdfsrnocell);
			if(isHideAreaCol){
				pdfnamecell.setColspan(2);
			}
			table1.addCell(pdfnamecell);
			if(isHideAreaCol==false){
//				if(!PC_DoNotPrintQtyFlag){
				if(qtyColumnFlag){
					table1.addCell(pdfareacell);
				}

			}
			String UOm="";
			if(products.get(i).getUnitOfMeasurement()!=null){
				UOm = products.get(i).getUnitOfMeasurement();
			}
			
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				Phrase chunkUOM = new Phrase(UOm + "", font7);
				PdfPCell pdfUOMcell = new PdfPCell(chunkUOM);
				pdfUOMcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table1.addCell(pdfUOMcell);
			}
			
			
			
			if(!donotprintTotalflag){
				if(!PC_DoNotPrintContractPeriodFlag){
					table1.addCell(pdfdatecell);
				}
			}
			
			table1.addCell(pdfSerCell);
			System.out.println("rate : " +rateAmountProd);
			/**
			 * Updated By: Viraj
			 * Date: 04-04-2019
			 * Description: To show consolidated rate in quotation
			 */
			PdfPCell pdfspricecell = null;
			if(qp instanceof Contract){
				if(consolidatePrice || contEntity.isConsolidatePrice()){
					if (i == 0) {
						//Console.log("rate value : " + rateAmountProd);
						chunk = new Phrase(df.format(rateAmountProd)+ "", font7);
						pdfspricecell = new PdfPCell(chunk);
						if(this.products.size() > 1)
						pdfspricecell.setBorderWidthBottom(0);
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						logger.log(Level.SEVERE,"A1:" +hideRateAndDiscount);
						//table1.addCell(pdfspricecell);
						logger.log(Level.SEVERE,"a1" +hideRateAndDiscount);
						
					} else {
						chunk = new Phrase(" ", font7);
						pdfspricecell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfspricecell.setBorderWidthTop(0);
						}else{
							pdfspricecell.setBorderWidthBottom(0);
							pdfspricecell.setBorderWidthTop(0);
						}
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						pdfspricecell.setBorderWidthTop(0);
						logger.log(Level.SEVERE,"B1:" +hideRateAndDiscount);
						//table1.addCell(pdfspricecell);
						logger.log(Level.SEVERE,"b1:" +hideRateAndDiscount);//priyanka
				
					}
				}else{
					//Ashwini Patil Date:22-06-2022
					if(calculatedPrice>0) {
						chunk = new Phrase(df.format(calculatedPrice), font7);
					}else
						chunk = new Phrase(decimalformat.format(calculatedPrice), font7);
					pdfspricecell = new PdfPCell(chunk);
					pdfspricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
					logger.log(Level.SEVERE,"C1:" +hideRateAndDiscount);
//					if(!hideRateAndDiscount){
//					table1.addCell(pdfspricecell);
//					logger.log(Level.SEVERE,"c1:" +hideRateAndDiscount);
//					}
					
			}
			}else{
				if(consolidatePrice || quotEntity.isConsolidatePrice()){
					if (i == 0) {
						//Console.log("rate value : " + rateAmountProd);
						chunk = new Phrase(df.format(rateAmountProd)+ "", font7);
						pdfspricecell = new PdfPCell(chunk);
						if(this.products.size() > 1)
						pdfspricecell.setBorderWidthBottom(0);
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						//table1.addCell(pdfspricecell);
					} else {
						chunk = new Phrase(" ", font7);
						pdfspricecell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfspricecell.setBorderWidthTop(0);
						}else{
							pdfspricecell.setBorderWidthBottom(0);
							pdfspricecell.setBorderWidthTop(0);
						}
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						//table1.addCell(pdfspricecell);
					}
				}else {
					chunk = new Phrase(df.format(calculatedPrice), font7);
					pdfspricecell = new PdfPCell(chunk);
					pdfspricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
					//table1.addCell(pdfspricecell);
				}
			}
			
			if(hideRateAndDiscount){
				pdfspricecell.setColspan(3);
			}
			
			if(!PC_DoNotPrintRateFlag){
				table1.addCell(pdfspricecell);
			}
				/** Ends **/
			/**
			 * end komal
			 */
			
			/**
			 * Date 21-42018
			 * By jayshree
			 * add rate column and 
			 */
			
			boolean isAreaPresent=false;
			double areval=0;
			double totaldis=0;
			double desper=products.get(i).getPercentageDiscount();
			try{
			 areval=Double.parseDouble(products.get(i).getArea());
			 isAreaPresent=true;
			}
			catch(Exception e){
				isAreaPresent=false;
			}
			
			if(isAreaPresent){
				totaldis=(products.get(i).getQty()*products.get(i).getPrice()*areval*desper)/100
						+products.get(i).getDiscountAmt();
			}
			else{
				totaldis=(products.get(i).getQty()*products.get(i).getPrice()*desper)/100+
						products.get(i).getDiscountAmt();
			}
			System.out.println("products.get(i).getQty()"+products.get(i).getQty());
			chunk = new Phrase(decimalformat.format(totaldis), font7); //Ashwini Patil changed decimal format as it was printing discount .00
			PdfPCell pdfdiscell = new PdfPCell(chunk);
			pdfdiscell.setHorizontalAlignment(Element.ALIGN_CENTER);
			logger.log(Level.SEVERE,"D1:" +hideRateAndDiscount);
			if(!hideRateAndDiscount){
			logger.log(Level.SEVERE,"d1:" +hideRateAndDiscount);
			
			if(!PC_DoNotPrintDiscFlag){
				if(qp instanceof Contract){
					table1.addCell(pdfdiscell);
				}else {

					if(consolidatePrice || quotEntity.isConsolidatePrice()){
						if (i == 0) {
							chunk = new Phrase(decimalformat.format(consolidatedDiscount), font7);
							pdfdiscell = new PdfPCell(chunk);
							if(this.products.size() > 1)
								pdfdiscell.setBorderWidthBottom(0);
							pdfdiscell.setHorizontalAlignment(Element.ALIGN_CENTER);
							table1.addCell(pdfdiscell);
						}else {
							chunk = new Phrase(" ", font7);
							pdfdiscell = new PdfPCell(chunk);
							if(i == this.products.size()-1 || noOfLines == 0){							
								pdfdiscell.setBorderWidthTop(0);
							}else{
								pdfdiscell.setBorderWidthBottom(0);
								pdfdiscell.setBorderWidthTop(0);
							}
							pdfdiscell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							table1.addCell(pdfdiscell);
						}	
					}else {
						table1.addCell(pdfdiscell);
					}
					
				}
				
			}

			logger.log(Level.SEVERE,"d2:" +hideRateAndDiscount);
			}
			
			double amount=0;
			if(isAreaPresent){
				amount=areval*products.get(i).getPrice()-totaldis;
			}
			else{
//				amount=products.get(i).getPrice();  //commented by Ashwini
				if(products.get(i).isServiceRate()){
					amount=(products.get(i).getPrice()*products.get(i).getNumberOfServices())-totaldis;
				}else{
					amount=products.get(i).getPrice()-totaldis; //Added by Ashwini
				}
			}
			
			
			String taxName="NA", taxpercent="";
			double Tax1Amount=0, Tax2Amount=0, taxAmount=0;
			boolean IGSTflag = false;

			if(donotprintTotalflag){
				
				QuotationPdfFormatVersion2 quotationpdfv2 = new QuotationPdfFormatVersion2();
				double totalAmount = quotationpdfv2.getTotalAmount(this.products.get(i));

				if(this.products.get(i).getVatTax().getPercentage()>0 && this.products.get(i).getServiceTax().getPercentage()>0){
					taxName = this.products.get(i).getVatTax().getTaxPrintName()+" / "+this.products.get(i).getServiceTax().getTaxPrintName()+"";
					taxpercent = this.products.get(i).getVatTax().getPercentage()+"% / "+this.products.get(i).getServiceTax().getPercentage()+"%";
					Tax1Amount =  (totalAmount*this.products.get(i).getVatTax().getPercentage())/100;
					Tax2Amount = (totalAmount*this.products.get(i).getServiceTax().getPercentage())/100;
					
					taxAmount = Tax1Amount+Tax2Amount;
				}
				else if(this.products.get(i).getVatTax().getPercentage()>0 || this.products.get(i).getServiceTax().getPercentage()>0){
					if(this.products.get(i).getVatTax().getPercentage()>0 ){
						taxName = this.products.get(i).getVatTax().getTaxPrintName();
						taxpercent = this.products.get(i).getVatTax().getPercentage()+" %";
						Tax1Amount = (totalAmount*this.products.get(i).getVatTax().getPercentage())/100;
						taxAmount = Tax1Amount+Tax2Amount;
					}
					else if(this.products.get(i).getServiceTax().getPercentage()>0){
						taxName = this.products.get(i).getServiceTax().getTaxPrintName();
						taxpercent = this.products.get(i).getServiceTax().getPercentage()+ "%";
						taxAmount = (totalAmount*this.products.get(i).getServiceTax().getPercentage())/100;
					}
					
					IGSTflag = true;
				}
				
				
				if(consolidatePrice || quotEntity.isConsolidatePrice()){
					PdfPCell gstAmtCell = new PdfPCell(chunk);
					if (i == 0) {
						logger.log(Level.SEVERE, "i=0 consolidatedTax="+consolidatedTax+" consolidatedTotalAmount="+consolidatedTotalAmount);
						chunk = new Phrase(df.format(consolidatedTax), font7);//Ashwini Patil Date:26-08-2024
						
						gstAmtCell = new PdfPCell(chunk);
						gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						
						
						if(IGSTflag){
							String IGST = taxName+" "+taxpercent;
							table1.addCell(pdfUtility.getPdfCell(IGST, font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
						}
						else{
							PdfPTable taxtable = new PdfPTable(1);
							taxtable.setWidthPercentage(100);
							taxtable.setHorizontalAlignment(Element.ALIGN_CENTER);
							
							taxtable.addCell(pdfUtility.getPdfCell(taxName+"", font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
							taxtable.addCell(pdfUtility.getPdfCell(taxpercent+"", font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

							PdfPCell taxtabecell = new PdfPCell(taxtable);
							taxtabecell.setHorizontalAlignment(Element.ALIGN_CENTER);
							
							if(this.products.size() > 1)
								taxtabecell.setBorderWidthBottom(0);
							table1.addCell(taxtabecell);
						}
						if(this.products.size() > 1)
							gstAmtCell.setBorderWidthBottom(0);
						gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(gstAmtCell);
					}else {
						chunk = new Phrase(" ", font7);
						gstAmtCell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							gstAmtCell.setBorderWidthTop(0);
						}else{
							gstAmtCell.setBorderWidthBottom(0);
							gstAmtCell.setBorderWidthTop(0);
						}
						gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						gstAmtCell.setBorderWidthTop(0);
						
						Phrase blankchunk = new Phrase(" ", font7);
						PdfPCell taxNameCell = new PdfPCell(blankchunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							taxNameCell.setBorderWidthTop(0);
						}else{
							taxNameCell.setBorderWidthBottom(0);
							taxNameCell.setBorderWidthTop(0);
						}
						taxNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						taxNameCell.setBorderWidthTop(0);

						table1.addCell(taxNameCell);
						table1.addCell(gstAmtCell);
						
					}	
				}else {

					if(IGSTflag){
					
						String IGST = taxName+" "+taxpercent;
						table1.addCell(pdfUtility.getPdfCell(IGST, font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

					}
					else{
						PdfPTable taxtable = new PdfPTable(1);
						taxtable.setWidthPercentage(100);
						taxtable.setHorizontalAlignment(Element.ALIGN_CENTER);
						
						taxtable.addCell(pdfUtility.getPdfCell(taxName+"", font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
						taxtable.addCell(pdfUtility.getPdfCell(taxpercent+"", font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

						PdfPCell taxtabecell = new PdfPCell(taxtable);
						taxtabecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						
						table1.addCell(taxtabecell);
					}
					Phrase gstAmtph = new Phrase(decimalformat.format(taxAmount), font7);
					PdfPCell gstAmtCell = new PdfPCell(gstAmtph);
					gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					
					table1.addCell(gstAmtCell);

					amount = amount + taxAmount;
					
				}
			}
			
			/**
			 * Updated By: Viraj
			 * Date: 02-04-2019
			 * Description: To consolidate amount and for quotation was added on 04-04-2019
			 */
			PdfPCell pdfamtcell = null;
			if(qp instanceof Contract){
				if(consolidatePrice || contEntity.isConsolidatePrice()){
					if (i == 0) {
						chunk = new Phrase(df.format(consolidatedTotalAmount), font7);//Ashwini Patil Date:16-08-2022
						pdfamtcell = new PdfPCell(chunk);
						if(this.products.size() > 1)
							pdfamtcell.setBorderWidthBottom(0);
						pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						//table1.addCell(pdfamtcell);
					}else {
						chunk = new Phrase(" ", font7);
						pdfamtcell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfamtcell.setBorderWidthTop(0);
						}else{
							pdfamtcell.setBorderWidthBottom(0);
							pdfamtcell.setBorderWidthTop(0);
						}
						pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						pdfamtcell.setBorderWidthTop(0);//priyanka
						//table1.addCell(pdfamtcell);
					}	
				}else {
					
					//Ashwini Patil Date:22-06-2022
					if(amount>0) 
						chunk = new Phrase(df.format(amount), font7);
					else
						chunk = new Phrase(decimalformat.format(amount), font7);
					pdfamtcell = new PdfPCell(chunk);
					pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					
				}
			}else {
				if(consolidatePrice || quotEntity.isConsolidatePrice()){
					
					if (i == 0) {
						logger.log(Level.SEVERE, "i=0 consolidatedTax="+consolidatedTax+" consolidatedTotalAmount="+consolidatedTotalAmount);
						
						if(quotEntity.isDonotPrintTotal())
							chunk = new Phrase(df.format(consolidatedTotalAmount+consolidatedTax), font7);//Ashwini Patil Date:26-08-2024
						else
							chunk = new Phrase(df.format(consolidatedTotalAmount), font7);//Ashwini Patil Date:16-08-2022
						
						pdfamtcell = new PdfPCell(chunk);
						if(this.products.size() > 1)
							pdfamtcell.setBorderWidthBottom(0);
						pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						//table1.addCell(pdfamtcell);
					}else {
						chunk = new Phrase(" ", font7);
						pdfamtcell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfamtcell.setBorderWidthTop(0);
						}else{
							pdfamtcell.setBorderWidthBottom(0);
							pdfamtcell.setBorderWidthTop(0);
						}
						pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						pdfamtcell.setBorderWidthTop(0);
						//table1.addCell(pdfamtcell);
					}	
				}else {
					chunk = new Phrase(df.format(amount), font7);
					pdfamtcell = new PdfPCell(chunk);
					pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					//table1.addCell(pdfamtcell);
				}
			}
			
			if(hideRateAndDiscount){
				pdfamtcell.setColspan(3);
			}
			
			pdfamtcell.setColspan(ratedisccolsspan);

			
			table1.addCell(pdfamtcell);
			/** Ends **/
			
			//End By
			
			
//			chunk = new Phrase(df.format(calculatedPrice), font7);
//			PdfPCell pdfspricecell = new PdfPCell(chunk);
//			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			double totalVal = (products.get(i).getPrice() - taxVal);
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}
	
			/**
			 * Date 7-4-2018
			 * By Jayshree
			 * Des.changes as per process configration 
			 */
			chunk = new Phrase(" ", font7);
			PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
			Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			Pdfsrnocell2.setBorderWidthRight(0);

			String premises = "";// 1
			String remark="";
			
			if (products.get(i).getRemark() != null
						&& !products.get(i).getRemark().equals("")) {
					remark = "Remark"+ " : " + products.get(i).getRemark();
					if(remark.length()<28) {
						int remaningchars=28-remark.length();
						for(int s=0;s<remaningchars;s++) {
							remark = remark +"  ";
						}
					}
			}
			

			if(contractTypeAsPremisedetail==true){
				System.out.println("contractTypeAsPremisedetail");

				/**
				 *  Date : 25-02-2021 Added by Priyanka.
				 *  Des : Modification in contract Pdf requirement by First Care Services issue raised by Ashwini.
				 */
				if(qp instanceof Contract){
					if (products.get(i).getPremisesDetails() != null&& !products.get(i).getPremisesDetails().equals("")) {
						premises = "Premises " +" : " + products.get(i).getPremisesDetails();
						
					}
					else if(contEntity.getType()!=null &&!contEntity.getType().equals("")){
					premises="Premises " +" : " +contEntity.getType();
					}
					else{
						premises="Premises " +" : " +"N A";
						}
					}
				

				else{

					if (products.get(i).getPremisesDetails() != null
							&& !products.get(i).getPremisesDetails().equals("")) {
						premises = "Premises"+ " : " + products.get(i).getPremisesDetails();
					}
					else if(quotEntity.getType()!=null &&!quotEntity.getType().equals("")){
					premises="Premises"+ " : " + quotEntity.getType();
					}
					else{
						premises="Premises"+ " : " + "N A";
						}
					
				}
			}
			
			else {
				if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premises"+ " : " + products.get(i).getPremisesDetails();
				System.out.println("products.get(i).getPremisesDetails()"+premises);
			} else {
				premises = "Premises"+":"+"NA";
				System.out.println("products.get(i).getPremisesDetails()bbbb");
			}
			}
			
			/**
			 *  End
			 */
		

			count = i;

			Phrase remarkPh = new Phrase(remark, font7);
			PdfPCell remarkCell = new PdfPCell(remarkPh);
			remarkCell.setBorderWidthRight(0);
//			remarkCell.setColspan(2);
			
			if (printProductPremisesFlag || donotprintTotalflag){
				logger.log(Level.SEVERE, "printProductPremisesFlag="+printProductPremisesFlag+" donotprintTotalflag="+donotprintTotalflag);
				
				if(donotprintTotalflag){
					
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
					// premisesCell.setBorderWidthRight(0);
					
					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						if(printProductRemarkWithPremiseFlag) {
							
							premisesCell.setColspan(7);
							premisesCell.setBorderWidthLeft(0);
						}else
							premisesCell.setColspan(10);//old 9 changed on 9-08-2024
					}
					else{
						if(printProductRemarkWithPremiseFlag) {
							logger.log(Level.SEVERE, "premisesCell.setColspan(6)");
							premisesCell.setColspan(7);
							premisesCell.setBorderWidthLeft(0);
						}else
							premisesCell.setColspan(9); //old 7 changed on 15-10-2024

					}
//					premisesCell.setColspan(7);
					
					if(taxAmount==0 || IGSTflag){
						noOfLines = noOfLines - 1;
					}
					else{
						noOfLines = noOfLines - 1;//Ashwini Patil Date:7-06-2024 changing -2 to -1 as 5th item is going to next page. reported by pest mortem
					}
					table1.addCell(Pdfsrnocell2);

					pdfdatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//					table1.addCell(pdfdatecell);//Ashwini Patil commented this on 19-04-2024 as duration was getting printed on premise line
					if(printProductRemarkWithPremiseFlag) {
						logger.log(Level.SEVERE, "remark added to table isHideAreaCol="+isHideAreaCol);
						table1.addCell(remarkCell);
					}
					if(printProductPremisesFlag)
						table1.addCell(premisesCell);
				}
				else{
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
					// premisesCell.setBorderWidthRight(0);
//					premisesCell.setColspan(7);

					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						if(printProductRemarkWithPremiseFlag) {
							premisesCell.setColspan(7);
							premisesCell.setBorderWidthLeft(0);
						}else
							premisesCell.setColspan(8);
					}
					else{
						if(printProductRemarkWithPremiseFlag) {
							premisesCell.setColspan(6);
							premisesCell.setBorderWidthLeft(0);
						}else
							premisesCell.setColspan(7);
					}

					noOfLines = noOfLines - 1;
					table1.addCell(Pdfsrnocell2);
					if(printProductRemarkWithPremiseFlag)
						table1.addCell(remarkCell);
					table1.addCell(premisesCell);
				}
				
				
				logger.log(Level.SEVERE, "Premise -Number of lines after "+noOfLines);
			}
			
			/**
			 * nidhi
			 * for print model and serial number
			 */int cnnt = 0;
				PdfPCell proModelcell = null ,proSerialNocell = null; 
				String proModelNo = "";
				String proSerialNo = "";
				logger.log(Level.SEVERE, "Model Serial num count "+cnnt+" printModelSerailNoFlag "+printModelSerailNoFlag);
			if(printModelSerailNoFlag){
				
				if(contEntity.getItems().get(i).getProModelNo()!=null && 
					contEntity.getItems().get(i).getProModelNo().trim().length() >0){
					proModelNo = contEntity.getItems().get(i).getProModelNo();
				}
				
				
				if(contEntity.getItems().get(i).getProSerialNo()!=null && 
					contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
					proSerialNo = contEntity.getItems().get(i).getProSerialNo();
				}
				
			
				if(proModelNo.length()>0){
					Phrase modelValPhrs = new Phrase(
							"Model No : " + proModelNo, font7);
					proModelcell =  new PdfPCell(modelValPhrs);
					proModelcell.setColspan(3);
//					proModelcell.addElement();
					++cnnt;
				}
				
//				PdfPCell proSerialNocell ; 
				if(proSerialNo.length()>0){
					Phrase serialValPhrs = new Phrase(
							"Serial No: " + proSerialNo, font7);
					proSerialNocell = new PdfPCell(serialValPhrs);
					
					
//					proSerialNocell.addElement();
					++cnnt;
//					table1.addCell(premiseCell);
				}
			}
			
			logger.log(Level.SEVERE, "After Model Serial num count "+cnnt);
			
			/**
			 * Date 14-09-2018 By Vijay
			 * Des :- Getting Server error due to null pointer exception
			 * so if condition added for !=null check
			 */
			if(proSerialNocell!=null && proModelcell!=null){
				if(cnnt>1 ){
				  /*
				   * Added by Ashwini
				   */
					if(proSerialNocell!=null){ 
						proSerialNocell.setColspan(4);
						} 
					/*
					 * end here
					 */
//					proSerialNocell.setColspan(4); //commented by Ashwini
				}else if(proModelNo.length()>0){
//					proModelcell.setColspan(7);  //commented by Ashwini
					/*
					   * Added by Ashwini
					   */
					if(proModelcell!=null){//Added by Ashwini
						proModelcell.setColspan(7);
						}
					/*
					 * end here
					 */
					
				}else {
					proSerialNocell.setColspan(7);
				}
			}
			// End For Jayshree

			// table1.addCell(pdftotalproduct);
			
			if(donotprintTotalflag){
				try {
//					table1.setWidths(columnWidths9);
					
					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						table1.setWidths(columnWidths10); 
					}
					else{
						table1.setWidths(columnWidths9); 

					}

				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			else{
				try {
//					table1.setWidths(columnWidths7);
					
					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						table1.setWidths(columnWidths9);
					}
					else{
						table1.setWidths(columnWidths7);
					}

				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			
			if(cnnt>0&&printModelSerailNoFlag)
			{
				logger.log(Level.SEVERE,cnnt+ "Model Serial Num -Number of lines before "+noOfLines);
				
				noOfLines = noOfLines-1;
				table1.addCell(Pdfsrnocell2);
				if(proModelcell!=null){
					table1.addCell(proModelcell);
				}
				if(proSerialNocell!=null){
					table1.addCell(proSerialNocell);
				}
				logger.log(Level.SEVERE, "Model Serial Num -Number of lines after "+noOfLines);
			}
			PdfPCell premisesHeadingCell = new PdfPCell(table1);
			premisesHeadingCell.setBorder(0);
			
			productTable.addCell(premisesHeadingCell);
			
			System.out.println("Added All");
		}

		/**
		 * Dev : Rahul Verma Date : 25 Nov 2017 Description : For adding blank
		 * space
		 */

		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines > 0) {
			remainingLines = 10 - (10 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);
		for (int j = 0; j < remainingLines; j++) {
			PdfPCell pCell = new PdfPCell(new Phrase(" ", font1));
			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			pCell.setBorderWidthTop(0);
			pCell.setBorderWidthBottom(0);
			productTable.addCell(pCell);
		}

		//Comment By Jayshree
		
//		if (noOfLines == 0 && remainingLines == 0) {
//			Phrase my = new Phrase("Please Refer Annexure For More Details",
//					font7);
//			PdfPCell productTableCell = new PdfPCell(my);
//			productTable.addCell(productTableCell);
//
//		} else {
//
//			PdfPCell productTableCell = new PdfPCell(new Phrase(" ", font7));
//			// productTable.addCell(productTableCell);//comment by jayshree
//			// 7/12/2017 to remove space
//		}
		
		
		/**
		 * Date 28/2/2018
		 * By Jayshree
		 * Des.Add the Changes To print the meaage as per the product size
		 */
		/**
		 * nidhi
		 * 9-08-2018
		 * for map service and model
		 */
		if (printProductPremisesFlag && !printModelSerailNoFlag || donotprintTotalflag){
			if (noOfLines ==0 && remainingLines ==0) {
				if(this.products.size()>5 ){
			
					Phrase my = new Phrase("Please Refer Annexure For More Details",font7);
					PdfPCell productTableCell = new PdfPCell(my);
					productTable.addCell(productTableCell);
				}
			} 
		}else if (printProductPremisesFlag && printModelSerailNoFlag){
			if (noOfLines ==0 && remainingLines ==0) {
				if(this.products.size()>3 ){
			
					Phrase my = new Phrase("Please Refer Annexure For More Details",font7);
					PdfPCell productTableCell = new PdfPCell(my);
					productTable.addCell(productTableCell);
				}
			} 
		}else if (!printProductPremisesFlag && printModelSerailNoFlag){
			if (noOfLines ==0 && remainingLines ==0) {
				if(this.products.size()>5 ){
			
					Phrase my = new Phrase("Please Refer Annexure For More Details",font7);
					PdfPCell productTableCell = new PdfPCell(my);
					productTable.addCell(productTableCell);
				}
			} 
		}
		else{
			if (noOfLines ==0 && remainingLines ==0) {
				if(this.products.size()>10 ){
			
					Phrase my = new Phrase("Please Refer Annexure For More Details",font7);
					PdfPCell productTableCell = new PdfPCell(my);
					productTable.addCell(productTableCell);
				}
			} 
			
		}
		/**
		 * end
		 */
		
		/**
		 * Ends For Rahul
		 */
		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		PdfPCell tablecell = new PdfPCell(table);
		parentTableProd.addCell(tablecell);

		PdfPCell prodtablecell = new PdfPCell(productTable);
		prodtablecell.setBorder(0);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private boolean isServiceWiseRate(List<SalesLineItem> items) {
		for(SalesLineItem object:items){
			if (object.isServiceRate()&&(object.getArea()==null||object.getArea().equals("")||object.getArea().equalsIgnoreCase("NA"))) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Updated By: Viraj
	 * Date: 04-03-2019
	 * Description: To show remaining products in the next page
	 */
	private void createAnnextureTable() {
		/**
		 * @author Anil
		 * @since 30-06-2020
		 * If service wise rate is active then we have to hide area col
		 * for sai care by Vaishnavi and Nitin sir
		 */
		boolean isHideAreaCol=isServiceWiseRate(qp.getItems());
		
		System.out.println("inside annexture");
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable productTable = new PdfPTable(1);
		productTable.setWidthPercentage(100f);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 7 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font7);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(8);
		
		if(donotprintTotalflag){
//			table = new PdfPTable(9);
			
			donotprintTotalflag = true;
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				table = new PdfPTable(10);
			}
			else{
				table = new PdfPTable(9);
			}
			
		}
		else if(qtyColumnFlag && unitOfMeasurementColumnFlag){
			table = new PdfPTable(9);
		}
		
		table.setWidthPercentage(100);
		Phrase srno = new Phrase("Sr No", font7);
//		Phrase product = new Phrase("Service Details", font7);
		/**@Sheetal :02-03-2022 Renaming Service from Service Details **/
		Phrase product = new Phrase("Service", font7bold);
		//Phrase area = null;
		
		Phrase area = new Phrase(qtylabel, font7);
		Phrase UOM = new Phrase("UOM",font7bold);

		Phrase startdate = new Phrase("Contract Period", font7);
		Phrase noservices = new Phrase("Services", font7);
		Phrase rate = new Phrase("Rate", font7);

		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellarea = new PdfPCell(area);
		cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellUomCell = new PdfPCell(UOM);
		cellUomCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellstartdate = new PdfPCell(startdate);
		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicecell = new PdfPCell(noservices);
		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase disph = new Phrase("Discount", font7);
		PdfPCell disCell = new PdfPCell(disph);
		disCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase amtph = new Phrase("Amount", font7);
		PdfPCell amtCell = new PdfPCell(amtph);
		amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		
		int colsspan =1;
		if(!qtyColumnFlag){
			++colsspan;
//			++colsspan; // for UOM
		}
		else{
			if(PC_DoNotPrintQtyFlag){
				++colsspan;
			}
		}
		
		if(PC_DoNotPrintContractPeriodFlag){
			++colsspan;
		}
		cellproduct.setColspan(colsspan);
		
		
		int ratedisccolsspan =1;
		if(PC_DoNotPrintDiscFlag){
			++ratedisccolsspan;
		}
		
		if(PC_DoNotPrintRateFlag){
			++ratedisccolsspan;
		}
		amtCell.setColspan(ratedisccolsspan);
		
		
		if(donotprintTotalflag){
			
			Phrase taxph = new Phrase("Tax", font7bold);
			PdfPCell taxCell = new PdfPCell(taxph);
			taxCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase gstAmtph = new Phrase("GST Amount", font7bold);
			PdfPCell gstAmtCell = new PdfPCell(gstAmtph);
			gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table.addCell(cellsrNo);
			
			if(isHideAreaCol){
				cellproduct.setColspan(2);
			}
			table.addCell(cellproduct);
			if(isHideAreaCol==false){
				if(qtyColumnFlag)
				table.addCell(cellarea);
			}
//			table.addCell(cellstartdate);
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				table.addCell(cellUomCell);
			}
			table.addCell(servicecell);
			table.addCell(cellrate);

			table.addCell(disCell);
			table.addCell(taxCell);
			table.addCell(gstAmtCell);

			
			table.addCell(amtCell);
			
			try {
//				table.setWidths(columnWidths9);
				if(qtyColumnFlag && unitOfMeasurementColumnFlag){
					table.setWidths(columnWidths10);
				}
				else{
					table.setWidths(columnWidths9);
				}

			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
		}
		else{
			
			table.addCell(cellsrNo);
			
			if(isHideAreaCol){
				cellproduct.setColspan(2);
			}
			table.addCell(cellproduct);
			if(isHideAreaCol==false){
				if(qtyColumnFlag)
				table.addCell(cellarea);
			}
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				table.addCell(cellUomCell);
			}
//			table.addCell(cellarea);
			table.addCell(cellstartdate);
			table.addCell(servicecell);
			table.addCell(cellrate);

			table.addCell(disCell);
			table.addCell(amtCell);
			
			try {
//				table.setWidths(columnWidths7);
				if(qtyColumnFlag && unitOfMeasurementColumnFlag){
					table.setWidths(columnWidths9);
				}
				else{
					table.setWidths(columnWidths7);
				}

			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
		}
		
		double rateAmountProd = 0;

		for (int j = 0; j < qp.getItems().size(); j++) {
			double taxVal = removeAllTaxes(products.get(j).getPrduct());
			double calculatedPrice = products.get(j).getPrice() - taxVal;
			rateAmountProd = rateAmountProd + calculatedPrice;
		
		}
		
		for (int i = prouductCount; i < this.products.size(); i++) {

			PdfPTable table1 = new PdfPTable(8);
			
			if(donotprintTotalflag){
//				table1 = new PdfPTable(9);
				donotprintTotalflag = true;
				if(qtyColumnFlag && unitOfMeasurementColumnFlag){
					table = new PdfPTable(10);
				}
				else{
					table = new PdfPTable(9);
				}
			}
			else if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				System.out.println("no of column 9");
				table = new PdfPTable(9);
			}
			table1.setWidthPercentage(100);

			System.out.println("iiiii-=----" + i);
			Phrase chunk = new Phrase((i + 1) + "", font7);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			String productNamewithWarrantyinfo="";
			if(products.get(i).getWarrantyPeriod()!=0&&!PC_DoNotPrintWarrantyFlag){
				 productNamewithWarrantyinfo = products.get(i).getProductName()+"\n"+" Warranty Period - "
						 +products.get(i).getWarrantyPeriod();	
			}else{
				productNamewithWarrantyinfo= products.get(i).getProductName();
			}

			Phrase chunk1 = new Phrase(productNamewithWarrantyinfo, font7);
			PdfPCell pdfnamecell = new PdfPCell(chunk1);
//			pdfnamecell.setColspan((int) 1.5);
			pdfnamecell.setColspan(colsspan);

			Phrase chunk2 = new Phrase(products.get(i).getArea() + "", font7);
			PdfPCell pdfareacell = new PdfPCell(chunk2);
			pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			Calendar c = Calendar.getInstance();
			c.setTime(products.get(i).getStartDate());
			c.add(Calendar.DATE, products.get(i).getDuration() - 1);
			Date endDt = c.getTime();
			
			String date = "";
			if (products.get(i).getEndDate() != null) {
				date = fmt.format(products.get(i).getStartDate()) + " To "
						+ fmt.format(products.get(i).getEndDate());
			} else {
				date = fmt.format(products.get(i).getStartDate()) + " To "
						+ fmt.format(endDt);
			}

			chunk = new Phrase(date, font7);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
					font7);
			pdfSerCell = new PdfPCell(chunk);
			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;
			 System.out.println("price : " + calculatedPrice);
			
			table1.addCell(Pdfsrnocell);
			if(isHideAreaCol){
				pdfnamecell.setColspan(2);
			}
			table1.addCell(pdfnamecell);
			if(isHideAreaCol==false){
				if(qtyColumnFlag){
					table1.addCell(pdfareacell);
				}

			}
//			table1.addCell(pdfareacell);
			
//			table1.addCell(pdfdatecell);
			
			
			String UOm="";
			if(products.get(i).getUnitOfMeasurement()!=null){
				UOm = products.get(i).getUnitOfMeasurement();
			}
			
			if(qtyColumnFlag && unitOfMeasurementColumnFlag){
				Phrase chunkUOM = new Phrase(UOm + "", font7);
				PdfPCell pdfUOMcell = new PdfPCell(chunkUOM);
				pdfUOMcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table1.addCell(pdfUOMcell);
			}
			
			
			if(!donotprintTotalflag){
				if(!PC_DoNotPrintContractPeriodFlag){
					table1.addCell(pdfdatecell);
				}

			}
			
			table1.addCell(pdfSerCell);
			System.out.println("rate : " +rateAmountProd);
			if(qp instanceof Contract){
				if(consolidatePrice || contEntity.isConsolidatePrice()){
					if (i == 0) {
						chunk = new Phrase(df.format(rateAmountProd)+ "", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						if(this.products.size() > 1)
						pdfspricecell.setBorderWidthBottom(0);
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table1.addCell(pdfspricecell);
					} else {
						chunk = new Phrase(" ", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfspricecell.setBorderWidthTop(0);
						}else{
							pdfspricecell.setBorderWidthBottom(0);
							pdfspricecell.setBorderWidthTop(0);
						}
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						pdfspricecell.setBorderWidthTop(0);//priyanka
						table1.addCell(pdfspricecell);
					}
				}else{
					chunk = new Phrase(df.format(calculatedPrice), font7);
					PdfPCell pdfspricecell = new PdfPCell(chunk);
					pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table1.addCell(pdfspricecell);
				}
			}else{
				chunk = new Phrase(df.format(calculatedPrice), font7);
				PdfPCell pdfspricecell = new PdfPCell(chunk);
				pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				table1.addCell(pdfspricecell);
			}
			
			boolean isAreaPresent=false;
			double areval=0;
			double totaldis=0;
			double desper=products.get(i).getPercentageDiscount();
			try{
			 areval=Double.parseDouble(products.get(i).getArea());
			 isAreaPresent=true;
			}
			catch(Exception e){
				isAreaPresent=false;
			}
			
			if(isAreaPresent){
				totaldis=(products.get(i).getQty()*products.get(i).getPrice()*areval*desper)/100
						+products.get(i).getDiscountAmt();
			}
			else{
				totaldis=(products.get(i).getQty()*products.get(i).getPrice()*desper)/100+
						products.get(i).getDiscountAmt();
			}
			System.out.println("products.get(i).getQty()"+products.get(i).getQty());
			
			
			chunk = new Phrase(decimalformat.format(totaldis), font7); //Ashwini Patil changed decimal formatter as it was printing discount .00
			PdfPCell pdfdiscell = new PdfPCell(chunk);
			pdfdiscell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(pdfdiscell);
			
			double amount=0;
			if(isAreaPresent){
				amount=areval*products.get(i).getPrice();
			}
			else{
				if(products.get(i).isServiceRate()){
					amount=(products.get(i).getPrice()*products.get(i).getNumberOfServices())-totaldis;
				}else{
					amount=products.get(i).getPrice()-totaldis; //Added by Ashwini
				}
			}
			
			String taxName="NA", taxpercent="";
			double Tax1Amount=0, Tax2Amount=0, taxAmount=0;
			boolean IGSTflag = false;

		
			/**
			 * @author Abhinav Bihade
			 * @since 24/01/2020
			 * As per Ashwini Bhagwat's, Pepcop- if contract have a more than 5 service products it has continued on next page on contract copy but in case consolidated price is true then,
			 * it's not showing consolidated price on continued page
			 */
			if(qp instanceof Contract){
				//String pdfamtcell = "";
				if(consolidatePrice || contEntity.isConsolidatePrice()){
					if (i == 0) {
						chunk = new Phrase(df.format(amount)+ "", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						if(this.products.size() > 1)
						pdfspricecell.setBorderWidthBottom(0);
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table1.addCell(pdfspricecell);
					} else {
						chunk = new Phrase(" ", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfspricecell.setBorderWidthTop(0);
						}else{
							pdfspricecell.setBorderWidthBottom(0);
							pdfspricecell.setBorderWidthTop(0);
						}
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						pdfspricecell.setBorderWidthTop(0);//priyanka
						table1.addCell(pdfspricecell);
					}
				}else{
					chunk = new Phrase(df.format(amount), font7);
					PdfPCell pdfspricecell = new PdfPCell(chunk);
					pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table1.addCell(pdfspricecell);
				}
			}
			/***Date 6-8-2020 by Amol, Total Amount column is not printed in Quotation pdf annexure part issue raised
			 * by Vaishnavi Mam *****/
			else {
				
				
				
				if(donotprintTotalflag){
					
					QuotationPdfFormatVersion2 quotationpdfv2 = new QuotationPdfFormatVersion2();
					double totalAmount = quotationpdfv2.getTotalAmount(this.products.get(i));
					
					if(this.products.get(i).getVatTax().getPercentage()>0 && this.products.get(i).getServiceTax().getPercentage()>0){
						taxName = this.products.get(i).getVatTax().getTaxPrintName()+" / "+this.products.get(i).getServiceTax().getTaxPrintName()+"";
						taxpercent = this.products.get(i).getVatTax().getPercentage()+"% / "+this.products.get(i).getServiceTax().getPercentage()+"%";
						Tax1Amount =  (totalAmount*this.products.get(i).getVatTax().getPercentage())/100;
						Tax2Amount = (totalAmount*this.products.get(i).getServiceTax().getPercentage())/100;
						
						taxAmount = Tax1Amount+Tax2Amount;
					}
					else if(this.products.get(i).getVatTax().getPercentage()>0 || this.products.get(i).getServiceTax().getPercentage()>0){
						if(this.products.get(i).getVatTax().getPercentage()>0 ){
							taxName = this.products.get(i).getVatTax().getTaxPrintName();
							taxpercent = this.products.get(i).getVatTax().getPercentage()+" %";
							Tax1Amount = (totalAmount*this.products.get(i).getVatTax().getPercentage())/100;
							taxAmount = Tax1Amount+Tax2Amount;
						}
						else if(this.products.get(i).getServiceTax().getPercentage()>0){
							taxName = this.products.get(i).getServiceTax().getTaxPrintName();
							taxpercent = this.products.get(i).getServiceTax().getPercentage()+ "%";
							taxAmount = (totalAmount*this.products.get(i).getServiceTax().getPercentage())/100;
						}
						IGSTflag = true;
					}
					
					if(IGSTflag){
						String IGST = taxName+" "+taxpercent;
						table1.addCell(pdfUtility.getPdfCell(IGST, font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

					}
					else{
						
						PdfPTable taxtable = new PdfPTable(1);
						taxtable.setWidthPercentage(100);
						taxtable.setHorizontalAlignment(Element.ALIGN_CENTER);
						
						taxtable.addCell(pdfUtility.getPdfCell(taxName+"", font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
						taxtable.addCell(pdfUtility.getPdfCell(taxpercent+"", font7, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));

						PdfPCell taxtabecell = new PdfPCell(taxtable);
						taxtabecell.setHorizontalAlignment(Element.ALIGN_CENTER);
						
						table1.addCell(taxtabecell);
						
					}
					
					Phrase gstAmtph = new Phrase(decimalformat.format(taxAmount), font7);
					PdfPCell gstAmtCell = new PdfPCell(gstAmtph);
					gstAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					
					table1.addCell(gstAmtCell);	

				}
				
				if(consolidatePrice || quotEntity.isConsolidatePrice()){
					if (i == 0) {
						chunk = new Phrase(df.format(rateAmountProd), font7);
						PdfPCell pdfamtcell = new PdfPCell(chunk);
						if(this.products.size() > 1)
							pdfamtcell.setBorderWidthBottom(0);
						pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table1.addCell(pdfamtcell);
					}else {
						chunk = new Phrase(" ", font7);
						PdfPCell pdfamtcell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfamtcell.setBorderWidthTop(0);
						}else{
							pdfamtcell.setBorderWidthBottom(0);
							pdfamtcell.setBorderWidthTop(0);
						}
						pdfamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table1.addCell(pdfamtcell);
					}	
				}else {
					chunk = new Phrase(df.format(amount), font7);
					PdfPCell pdfamtcell = new PdfPCell(chunk);
					pdfamtcell.setColspan(ratedisccolsspan);

					pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table1.addCell(pdfamtcell);
				}
			}
			

			
			
			
			
//			chunk = new Phrase(df.format(amount), font7);
//			PdfPCell pdfamtcell = new PdfPCell(chunk);
//			pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table1.addCell(pdfamtcell);
			
			

			double totalVal = (products.get(i).getPrice() - taxVal);
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}

			chunk = new Phrase(" ", font7);
			PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
			Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			Pdfsrnocell2.setBorderWidthRight(0);

			String premises = "";// 1
			
			
			if(contractTypeAsPremisedetail==true){
				System.out.println("contractTypeAsPremisedetail");
//				if(qp instanceof Contract){
//					if (products.get(i).getPremisesDetails() != null
//							&& !products.get(i).getPremisesDetails().equals("")) {
//						premises = "Premise For " + products.get(i).getProductName()
//								+ " : " + products.get(i).getPremisesDetails();
//					}
//					else if(contEntity.getType()!=null &&!contEntity.getType().equals("")){
//					premises="Premise For " + products.get(i).getProductName()
//							+ " : " + contEntity.getType();
//					}
//					else{
//						premises="Premise For " + products.get(i).getProductName()
//								+ " : " + "N A";
//						}
//					}
//				else{
//
//					if (products.get(i).getPremisesDetails() != null
//							&& !products.get(i).getPremisesDetails().equals("")) {
//						premises = "Premise For " + products.get(i).getProductName()
//								+ " : " + products.get(i).getPremisesDetails();
//					}
//					else if(quotEntity.getType()!=null &&!quotEntity.getType().equals("")){
//					premises="Premise For " + products.get(i).getProductName()
//							+ " : " + quotEntity.getType();
//					}
//					else{
//						premises="Premise For " + products.get(i).getProductName()
//								+ " : " + "N A";
//						}
//				}
//			}
//			
//			else {
//				if (products.get(i).getPremisesDetails() != null
//					&& !products.get(i).getPremisesDetails().equals("")) {
//				premises = "Premise For " + products.get(i).getProductName()
//						+ " : " + products.get(i).getPremisesDetails();
//				System.out.println("products.get(i).getPremisesDetails()"+premises);
//			} else {
//				premises = "Premise For " + products.get(i).getProductName()+":"+"NA";
//				System.out.println("products.get(i).getPremisesDetails()bbbb");
//			}
//			}
				
				/**
				 *  Date : 25-02-2021 Added by Priyanka.
				 *  Des : Modification in contract Pdf requirement by First Care Services issue raised by Ashwini.
				 */
				if(qp instanceof Contract){
					if (products.get(i).getPremisesDetails() != null&& !products.get(i).getPremisesDetails().equals("")) {
						premises = "Premises " +" : " + products.get(i).getPremisesDetails();
						
					}
					else if(contEntity.getType()!=null &&!contEntity.getType().equals("")){
					premises="Premises " +" : " +contEntity.getType();
					}
					else{
						premises="Premises " +" : " +"N A";
						}
					}
				

				else{

					if (products.get(i).getPremisesDetails() != null
							&& !products.get(i).getPremisesDetails().equals("")) {
						premises = "Premises"+ " : " + products.get(i).getPremisesDetails();
					}
					else if(quotEntity.getType()!=null &&!quotEntity.getType().equals("")){
					premises="Premises"+ " : " + quotEntity.getType();
					}
					else{
						premises="Premises"+ " : " + "N A";
						}
					
				}
			}
			
			else {
				if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premises"+ " : " + products.get(i).getPremisesDetails();
				System.out.println("products.get(i).getPremisesDetails()"+premises);
			} else {
				premises = "Premises"+":"+"NA";
				System.out.println("products.get(i).getPremisesDetails()bbbb");
			}
			}
			
			/**
			 *  End
			 */

			count = i;

			if (printProductPremisesFlag || donotprintTotalflag) {
				
				if(donotprintTotalflag){
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
//					premisesCell.setColspan(7);
					
					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						premisesCell.setColspan(8);
					}
					else{
						premisesCell.setColspan(7);
					}
					
					if(taxAmount==0 || IGSTflag){
						noOfLines = noOfLines - 1;
					}
					else{
						noOfLines = noOfLines - 2;
					}
					
					pdfdatecell.setHorizontalAlignment(Element.ALIGN_LEFT);

					table1.addCell(Pdfsrnocell2);
					table1.addCell(pdfdatecell);
					table1.addCell(premisesCell);
				}
				else{
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
//					premisesCell.setColspan(7);

					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						premisesCell.setColspan(8);
					}
					else{
						premisesCell.setColspan(7);
					}
					
					noOfLines = noOfLines - 1;
					table1.addCell(Pdfsrnocell2);
					table1.addCell(premisesCell);
				}

				
			}
			
			int cnnt = 0;
				PdfPCell proModelcell = null ,proSerialNocell = null; 
				String proModelNo = "";
				String proSerialNo = "";
			if(printModelSerailNoFlag){
				
				if(contEntity.getItems().get(i).getProModelNo()!=null && 
					contEntity.getItems().get(i).getProModelNo().trim().length() >0){
					proModelNo = contEntity.getItems().get(i).getProModelNo();
				}
				
				
				if(contEntity.getItems().get(i).getProSerialNo()!=null && 
					contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
					proSerialNo = contEntity.getItems().get(i).getProSerialNo();
				}
				
			
				if(proModelNo.length()>0){
					Phrase modelValPhrs = new Phrase(
							"Model No : " + proModelNo, font7);
					proModelcell =  new PdfPCell(modelValPhrs);
					proModelcell.setColspan(3);
					++cnnt;
				}
				 
				if(proSerialNo.length()>0){
					Phrase serialValPhrs = new Phrase(
							"Serial No: " + proSerialNo, font7);
					proSerialNocell = new PdfPCell(serialValPhrs);
					++cnnt;
				}
			}
			
			if(proSerialNocell!=null && proModelcell!=null){
				if(cnnt>1 ){
				  
					if(proSerialNocell!=null){ 
						proSerialNocell.setColspan(4);
						} 
				}else if(proModelNo.length()>0){

					if(proModelcell!=null){
						proModelcell.setColspan(7);
						}
					
				}else {
					proSerialNocell.setColspan(7);
				}
			}
			
			if(donotprintTotalflag){
				try {
//					table1.setWidths(columnWidths9);
					table1.setWidths(columnWidths10);
					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						table1.setWidths(columnWidths10);
					}
					else{
						table1.setWidths(columnWidths7);
					}

				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			else{
				try {
//					table1.setWidths(columnWidths7);

					if(qtyColumnFlag && unitOfMeasurementColumnFlag){
						table1.setWidths(columnWidths9);
					}
					else{
						table1.setWidths(columnWidths7);
					}

				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			
			if(cnnt>0);
			{
				noOfLines = noOfLines-1;
				table1.addCell(Pdfsrnocell2);
				if(proModelcell!=null){
					table1.addCell(proModelcell);
				}
				if(proSerialNocell!=null){
					table1.addCell(proSerialNocell);
				}
			}
			PdfPCell premisesHeadingCell = new PdfPCell(table1);
			premisesHeadingCell.setBorder(0);
			
			productTable.addCell(premisesHeadingCell);
			
			System.out.println("Added All annexture");
		}
		
		try {
			document.add(table);
			document.add(productTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	/** Ends **/
	
	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}

	// ////////////////////// Ajinkya Added Product Details on Date : 18/07/2017
	// //////////////////////////
	private void createProductDetailsVal() {
		System.out.println("Inside Prod Details Val");
		// ///////////////////////////////////////////////// Ajinkya
		if (qp instanceof Contract) {
			
			System.out.println("Inside Instance con");
			System.out.println("Inside  Condn of Instance con and quot ");
			// /////// Ajinkya
			int firstBreakPoint = 5;
			float blankLines = 0;

			if (contEntity.getItems().size() <= firstBreakPoint) {

				int size = firstBreakPoint - contEntity.getItems().size();
				blankLines = size * (100 / 5);
				System.out.println("blankLines size =" + blankLines);
			} else {
				blankLines = 10f;
			}

			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < contEntity.getItems().size(); i++) {

				if (i == 5) {
					break;
				}

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				
				//Ashwini Patil changed font8 to nameAddressFont6
				Phrase serviceName = new Phrase(contEntity.getItems().get(i)
						.getProductName().trim(), nameAddressFont6);
				PdfPCell serviceNameCell = new PdfPCell();
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.addElement(serviceName);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (contEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
					hsnCode = new Phrase(contEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell();
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.addElement(hsnCode);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(contEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell();
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
				uomCell.addElement(uom);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(contEntity.getItems().get(i).getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell();
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
				qtyCell.addElement(qty);
				productTable.addCell(qtyCell);
				
				Phrase rate = new Phrase(df.format(contEntity.getItems().get(i)
						.getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell();
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
				rateCell.addElement(rate);
				productTable.addCell(rateCell);

				double amountValue = contEntity.getItems().get(i).getPrice()
						* contEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
				productTable.addCell(amountCell);

				Phrase disc = new Phrase(df.format(contEntity.getItems().get(i)
						.getDiscountAmt())
						+ "", font8);
				PdfPCell discCell = new PdfPCell();
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
				discCell.addElement(disc);
				productTable.addCell(discCell);

				Phrase taxableValue = new Phrase(df.format(contEntity
						.getItems().get(i).getPrice())
						+ "", font8);
				PdfPCell taxableValueCell = new PdfPCell();
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
				taxableValueCell.addElement(taxableValue);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				if (contEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("IGST")
						|| contEntity.getItems().get(i).getServiceTax()
								.getTaxPrintName().equalsIgnoreCase("IGST")) {

					double taxAmount = getTaxAmount(contEntity.getItems()
							.get(i).getPrice(), contEntity.getItems().get(i)
							.getVatTax().getPercentage());
					double indivTotalAmount = contEntity.getItems().get(i)
							.getPrice()
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(contEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setBorder(0);
					totalCell.addElement(totalPhrase);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

				} else {

					if (contEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {

						double ctaxValue = getTaxAmount(contEntity.getItems()
								.get(i).getPrice(), contEntity.getItems()
								.get(i).getVatTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(contEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(contEntity.getItems()
								.get(i).getPrice(), contEntity.getItems()
								.get(i).getServiceTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(contEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = contEntity.getItems().get(i)
								.getPrice()
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

						// try {
						// document.add(productTable);
						// } catch (DocumentException e) {
						// e.printStackTrace();
						// }

					} else if (contEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("SGST")) {

						double ctaxValue = getTaxAmount(contEntity.getItems()
								.get(i).getPrice(), contEntity.getItems()
								.get(i).getServiceTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(contEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(contEntity.getItems()
								.get(i).getPrice(), contEntity.getItems()
								.get(i).getVatTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(contEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = contEntity.getItems().get(i)
								.getPrice()
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

					}
				}

			}

			PdfPCell productTableCell = null;
			if (contEntity.getItems().size() > firstBreakPoint) {
				Phrase my = new Phrase("Please Refer Annexure For More Details");
				productTableCell = new PdfPCell(my);

			} else {
				productTableCell = new PdfPCell(blankCell);
			}

			// PdfPCell productTableCell = new PdfPCell(blankCell);
			// productTableCell.setBorderWidthBottom(0);
			// productTableCell.setBorderWidthTop(0);
			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			tab.setSpacingBefore(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// //////// End Here

		} else {
			System.out.println("Inside quotation ");
			System.out.println("Else Condn of Instance con and quot ");
			// Ajinkya Start Here
			int firstBreakPoint = 5;
			float blankLines = 0;

			if (quotEntity.getItems().size() <= firstBreakPoint) {
				int size = firstBreakPoint - quotEntity.getItems().size();
				blankLines = size * (100 / 5);
				System.out.println("blankLines size =" + blankLines);
			} else {
				blankLines = 10f;
			}

			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < quotEntity.getItems().size(); i++) {

				// System.out.println("rohan i value "+i+"- blankLines"+"Size"+blankLines+invoiceentity.getSalesOrderProducts().size());
				// if(invoiceentity.getSalesOrderProducts().size()==i+1){
				// productTable.setSpacingAfter(blankLines);
				// }

				if (i == 5) {
					// productTable.setSpacingAfter(blankLines);
					break;
				}

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(quotEntity.getItems().get(i)
						.getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell();
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.addElement(serviceName);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (quotEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
					hsnCode = new Phrase(quotEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell();
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.addElement(hsnCode);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(quotEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell();
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
				uomCell.addElement(uom);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(quotEntity.getItems().get(i).getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell();
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
				qtyCell.addElement(qty);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(quotEntity.getItems().get(i)
						.getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell();
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
				rateCell.addElement(rate);
				productTable.addCell(rateCell);

				double amountValue = quotEntity.getItems().get(i).getPrice()
						* quotEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
				productTable.addCell(amountCell);

				
				Phrase disc = new Phrase(df.format(quotEntity.getItems().get(i)
						.getDiscountAmt())
						+ "", font8);
				PdfPCell discCell = new PdfPCell();
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
				discCell.addElement(disc);
				productTable.addCell(discCell);

				Phrase taxableValue = new Phrase(df.format(quotEntity
						.getItems().get(i).getPrice())
						+ "", font8);
				PdfPCell taxableValueCell = new PdfPCell();
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
				taxableValueCell.addElement(taxableValue);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				if (quotEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("IGST")
						|| quotEntity.getItems().get(i).getServiceTax()
								.getTaxPrintName().equalsIgnoreCase("IGST")) {

					double taxAmount = getTaxAmount(quotEntity.getItems()
							.get(i).getPrice(), quotEntity.getItems().get(i)
							.getVatTax().getPercentage());
					double indivTotalAmount = quotEntity.getItems().get(i)
							.getPrice()
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(quotEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setBorder(0);
					totalCell.addElement(totalPhrase);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

				} else {

					if (quotEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {

						double ctaxValue = getTaxAmount(quotEntity.getItems()
								.get(i).getPrice(), quotEntity.getItems()
								.get(i).getVatTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(quotEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(quotEntity.getItems()
								.get(i).getPrice(), quotEntity.getItems()
								.get(i).getServiceTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(quotEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = quotEntity.getItems().get(i)
								.getPrice()
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

						// try {
						// document.add(productTable);
						// } catch (DocumentException e) {
						// e.printStackTrace();
						// }

					} else if (quotEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("SGST")) {

						double ctaxValue = getTaxAmount(quotEntity.getItems()
								.get(i).getPrice(), quotEntity.getItems()
								.get(i).getServiceTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(quotEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(quotEntity.getItems()
								.get(i).getPrice(), contEntity.getItems()
								.get(i).getVatTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(quotEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = quotEntity.getItems().get(i)
								.getPrice()
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();

						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

					}
				}

			}

			PdfPCell productTableCell = null;
			if (quotEntity.getItems().size() > firstBreakPoint) {
				Phrase my = new Phrase("Please Refer Annexure For More Details");
				productTableCell = new PdfPCell(my);

			} else {
				productTableCell = new PdfPCell(blankCell);
			}

			// PdfPCell productTableCell = new PdfPCell(blankCell);
			// productTableCell.setBorderWidthBottom(0);
			// productTableCell.setBorderWidthTop(0);
			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			tab.setSpacingBefore(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// ENd Here

		}
		// //////////////////////////////////////////////// Ajinkya Stop Here

	}

	// ////////////////////// End Here ////////////////////

	private void createCompanyAddressDetails() {

		// ************************changes made by rohan for M/s
		// ****************

		// *******************changes ends here
		// ********************sss*************

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		/**
		 * Date 16/11/2017 By Jayshree description:to add the salutation in
		 * customer
		 */
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true&&!isSociety) {

				tosir = "To, M/S";
			} else {
				tosir = "To,";
			}

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			} else if (cust.getSalutation() != null)// By jayshree
													// Date16/11/2017
			{
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname.add(custName);
		} else {
			fullname.add(tosir + "   " + custName);
		}
		fullname.setFont(font6bold);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		// End By Jayshree

		String addresslin1 = " ";
		if (!cust.getAdress().getAddrLine1().equals("")) {
			addresslin1 = cust.getAdress().getAddrLine1().trim() + " , ";
		}

		String addressline2 = " ";

		if (!cust.getAdress().getAddrLine2().equals("")) {
			addressline2 = cust.getAdress().getAddrLine2().trim() + " , ";
		}

		String landmark = " ";
		if (!cust.getAdress().getLandmark().equals("")) {
			landmark = cust.getAdress().getLandmark() + " , ";
		}

		String locality = " ";
		if (!cust.getAdress().getLocality().equals("")) {
			locality = cust.getAdress().getLocality() + " , ";
		}

		// Phrase addressline1=new
		// Phrase(addresslin1+addressline2+landmark+locality,font9);
		// PdfPCell fromaddrescell1=new PdfPCell();
		// fromaddrescell1.addElement(addressline1);
		// fromaddrescell1.setBorder(0);

		String city = " ";
		if (!cust.getAdress().getCity().equals("")) {
			city = cust.getAdress().getCity() + ",";
		}
		String state = " ";
		if (!cust.getAdress().getState().equals("")) {
			state = cust.getAdress().getState() + "-";
		}
		String pin = " ";
		if (cust.getAdress().getPin() != 0) {
			pin = cust.getAdress().getPin() + ",";
		}
		String country = " ";
		if (cust.getAdress().getCountry() != null) {
			country = cust.getAdress().getCountry();
		}

		Phrase addressline1 = new Phrase(addresslin1 + addressline2 + landmark
				+ locality + city + state + pin + country, font9);
		PdfPCell fromaddrescell1 = new PdfPCell();
		fromaddrescell1.addElement(addressline1);
		fromaddrescell1.setBorder(0);

		// Phrase blank=new Phrase(" ",font9);
		// PdfPCell blankCell=new PdfPCell();
		// fromaddrescell1.addElement(addressline1);
		// fromaddrescell1.setBorder(0);

		Phrase namePhrase = new Phrase("Customer Details", font6bold);
		PdfPCell headcell = new PdfPCell(namePhrase);
		headcell.setBorder(0);
		headcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// *******************rohan added cust details *****************
		
		String emailOne ="";
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			emailOne = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				emailOne = cust.getEmail();
			} else {
				emailOne ="";
			}
		}
		
		
		Phrase email = new Phrase("Email :" +emailOne, font9);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cellNo = null;
		if (cust.getLandline() != null) {
			cellNo = new Phrase("Cell : " + cust.getCellNumber1()
					+ "   Phone: " + cust.getLandline(), font9);
		} else {
			cellNo = new Phrase("Cell : " + cust.getCellNumber1(), font9);
		}

		PdfPCell cellNoCell = new PdfPCell(cellNo);
		cellNoCell.setBorder(0);
		cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********************changes ends here *********************

		PdfPTable addtable = new PdfPTable(1);
		addtable.setWidthPercentage(100f);
		addtable.addCell(headcell);
		addtable.addCell(custnamecell);
		addtable.addCell(fromaddrescell1);
		addtable.addCell(emailCell);
		addtable.addCell(cellNoCell);

		PdfPCell tablecell = new PdfPCell();
		tablecell.addElement(addtable);

		PdfPTable parentaddtable = new PdfPTable(1);
		parentaddtable.setWidthPercentage(100f);
		parentaddtable.addCell(tablecell);

		try {
			document.add(parentaddtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,725f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
		

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,40f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
		
    	

	}
	
	private void createBlankforUPC() {

		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof Contract) {
			Contract conEntity = (Contract) qp;
			conEnt = conEntity.getContractDate();
		} else {

			Quotation quotEnt = (Quotation) qp;
			conEnt = quotEnt.getQuotationDate();//Date 17/1/2018 by jayshree change creation date to quotation date
		}

		String countinfo = "ID : " + qp.getCount() + "";

		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font12bold);// Date 6/12/2017 By
															// jayshree Change
															// the font size 14
															// to 12
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);

		PdfPCell titlepdfcell = new PdfPCell(titlephrase);
		// titlepdfcell.addElement(titlephrase);//By jayshree comment this to
		// remove space
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.setBorderWidthBottom(0);
		titlepdfcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdfcell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlepdfcell.setFixedHeight(20);// by jayshree set the height fix
		// titlepdfcell.addElement(Chunk.NEWLINE);//By jayshree comment this to
		// remove space

		Phrase idphrase = new Phrase(countinfo, font12bold);// By jayshree
															// Change the font
															// size 14 to 12
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell(idphrase);
		// countcell.addElement(idphrase);//By jayshree comment this to remove
		// space
		countcell.setBorderWidthRight(0);
		countcell.setBorderWidthBottom(0);
		countcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		countcell.setVerticalAlignment(Element.ALIGN_CENTER);
		countcell.setFixedHeight(20);// by jayshree set the height fix

		// countcell.addElement(Chunk.NEWLINE);//By jayshree comment this to
		// remove space

		Phrase dateofpdf = new Phrase(creationdateinfo, font12bold);// By
																	// jayshree
																	// Change
																	// the font
																	// size 14
																	// to 12
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell(dateofpdf);
		// creationcell.addElement(dateofpdf);//By jayshree comment this to
		// remove space
		creationcell.setBorderWidthLeft(0);
		creationcell.setBorderWidthBottom(0);
		creationcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creationcell.setVerticalAlignment(Element.ALIGN_CENTER);
		creationcell.setFixedHeight(20);// by jayshree set the height fix
		// creationcell.addElement(Chunk.NEWLINE);//By jayshree comment this to
		// remove space

		// **********************************ends here
		// *******************************

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);

		try {
			titlepdftable.setWidths(columnWidthsForSTandEndDate);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		// **********************************ends here
		// *******************************
		
		/*
		 * Commented by Ashwini
		 */
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
//
//		try {
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(titlepdftable);
//			if (qp instanceof Contract) {
//				Contract conEntity = (Contract) qp;
//				createContractDetails(conEntity);
//			}
//			createCustomerDetails();
//
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		/*
		 * Date:30/07/2018
		 * Developer:Ashwini
		 * Des:To increase headerspace in printout
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , qp.getCompanyId())){
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			
			try {
				document.add(blank);				
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				
				
				document.add(titlepdftable);
				if (qp instanceof Contract) {
					Contract conEntity = (Contract) qp;
					createContractDetails(conEntity);
				}
				createCustomerDetails();
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		else{
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		try {
			//Ashwini Patil Commented on 3-05-2023
			document.add(blank);
			if(!reduceHeaderSpaceFlag){ // added reduce blank space for header for bitco with process config
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
			}


			
			document.add(titlepdftable);
			if (qp instanceof Contract) {
				Contract conEntity = (Contract) qp;
				createContractDetails(conEntity);
			}
			createCustomerDetails();

		} catch (DocumentException e) {
			e.printStackTrace();
	}
}
		

		
	}
	 
	/**
	 * @author Ashwini Patil
	 * @since 20-02-2022
	 * to get the list of branches selected in contract
	 */
	public List<CustomerBranchDetails> getSelectedBranches(Contract contEntity){
		ArrayList<String> custbranchlist=getCustomerBranchList(contEntity.getItems());
		customerBranchList= ofy().load().type(CustomerBranchDetails.class)
				.filter("cinfo.count",contEntity.getCinfo().getCount())
				.filter("companyId", contEntity.getCompanyId()).list();
		
		List<CustomerBranchDetails> selectedBranches=new ArrayList<CustomerBranchDetails>();
		System.out.println("custbranchlist.size()="+custbranchlist.size());
		System.out.println("customerBranchList.size()="+customerBranchList.size());
		for(int i=0;i<custbranchlist.size();i++){
			System.out.println("in first for");
			for(CustomerBranchDetails c:customerBranchList){
				System.out.println("in second for c.getBusinessUnitName()="+c.getBusinessUnitName()+" and custbranchlist.get("+i+")="+custbranchlist.get(i));
				if(c.getBusinessUnitName().equals(custbranchlist.get(i))){
					System.out.println("in if of for loop");
					selectedBranches.add(c);
				}
			}			
		}
		return selectedBranches;
	}
	
	/**
	 * @author Ashwini Patil
	 * @since 27-12-2022
	 * to get the list of branches selected in quotation
	 */
	public List<CustomerBranchDetails> getSelectedBranchesInQuotation(Quotation quot){
		ArrayList<String> custbranchlist=getCustomerBranchList(quot.getItems());
		customerBranchList= ofy().load().type(CustomerBranchDetails.class)
				.filter("cinfo.count",quot.getCinfo().getCount())
				.filter("companyId", quot.getCompanyId()).list();
		
		List<CustomerBranchDetails> selectedBranches=new ArrayList<CustomerBranchDetails>();
		System.out.println("custbranchlist.size()="+custbranchlist.size());
		System.out.println("customerBranchList.size()="+customerBranchList.size());
		for(int i=0;i<custbranchlist.size();i++){
			System.out.println("in first for");
			for(CustomerBranchDetails c:customerBranchList){
				System.out.println("in second for c.getBusinessUnitName()="+c.getBusinessUnitName()+" and custbranchlist.get("+i+")="+custbranchlist.get(i));
				if(c.getBusinessUnitName().equals(custbranchlist.get(i))){
					System.out.println("in if of for loop");
					selectedBranches.add(c);
				}
			}
			
		}
		return selectedBranches;
	}
	
		
	/**
	 * Date 29/11/2017 Dev.Jayshree Des.To Change the email Contract And
	 * Quotation Format Old Method is comment
	 * 
	 * @param doc
	 * @param comp
	 */

	// patch patch patch
	public void createPdfForEmail(Company comp, Customer cust, Sales qp) {
		// this.comp = comp;
		// this.qp = qp;
		// this.cust = cust;
		// this.products = qp.getItems();
		// this.payTermsLis = qp.getPaymentTermsList();
		// this.prodTaxes = qp.getProductTaxes();
		// this.prodCharges = qp.getProductCharges();
		// this.articletype = cust.getArticleTypeDetails();
		//
		// if (qp instanceof Contract) {
		// Contract contractEntity = (Contract) qp;
		// contEntity = contractEntity;
		// //
		// if (qp.getCompanyId() != null) {
		// processConfig = ofy().load().type(ProcessConfiguration.class)
		// .filter("companyId", qp.getCompanyId())
		// .filter("processName", "Contract")
		// .filter("configStatus", true).first().now();
		// if (processConfig != null) {
		// for (int k = 0; k < processConfig.getProcessList().size(); k++) {
		// if (processConfig.getProcessList().get(k)
		// .getProcessType().trim()
		// .equalsIgnoreCase("CompanyAsLetterHead")
		// && processConfig.getProcessList().get(k)
		// .isStatus() == true) {
		// upcflag = true;
		//
		// }
		// }
		// }
		// }
		// //
		// // }
		// // else{
		// // Quotation quotationEntity=(Quotation)qp;
		// // quotEntity=quotationEntity;
		// // }
		//
		// // *******************rohan chnages here for sending logo to email
		//
		// if (upcflag == false) {
		// createLogo(document, comp);
		// createCompanyHedding();
		// } else {
		//
		// if (comp.getUploadHeader() != null) {
		// createCompanyNameAsHeader(document, comp);
		// }
		//
		// if (comp.getUploadFooter() != null) {
		// createCompanyNameAsFooter(document, comp);
		// }
		// createBlankforUPC();
		// }
		//
		// } else {
		//
		// Quotation quotationEntity = (Quotation) qp;
		// quotEntity = quotationEntity;
		//
		// if (qp.getCompanyId() != null) {
		// processConfig = ofy().load().type(ProcessConfiguration.class)
		// .filter("companyId", qp.getCompanyId())
		// .filter("processName", "Contract")
		// .filter("configStatus", true).first().now();
		// if (processConfig != null) {
		// for (int k = 0; k < processConfig.getProcessList().size(); k++) {
		// if (processConfig.getProcessList().get(k)
		// .getProcessType().trim()
		// .equalsIgnoreCase("CompanyAsLetterHead")
		// && processConfig.getProcessList().get(k)
		// .isStatus() == true) {
		// upcflag = true;
		//
		// }
		// }
		// }
		// }
		//
		// if (upcflag == false) {
		// // createLogo(document,comp);//By jayshree comment the method
		// createCompanyHedding();
		//
		// } else {
		//
		// if (comp.getUploadHeader() != null) {
		// createCompanyNameAsHeader(document, comp);
		// }
		//
		// if (comp.getUploadFooter() != null) {
		// createCompanyNameAsFooter(document, comp);
		// }
		// createBlankforUPC();
		// }
		// }
		// // ********************chnges ends here ************************
		//
		// fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		// fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		// // makeServices();
		// // createLogo(document,comp);
		// // createCompanyHedding();
		// createProductDetail();
		// termsConditionsInfo();
		// footerInfo();

		// ********************************

		// this.comp = comp;
		// this.qp = qp;
		// this.cust = cust;
		// this.products = qp.getItems();
		// this.payTermsLis = qp.getPaymentTermsList();
		// this.prodTaxes = qp.getProductTaxes();
		// this.prodCharges = qp.getProductCharges();
		// this.articletype = cust.getArticleTypeDetails();
		// String preprint="Plane";
		// if (qp instanceof Contract) {
		// System.out.println("111111111");
		// if (upcflag == false && preprint.equals("plane")) {
		//
		//
		// createCompanyHedding();
		// } else {
		//
		// if (preprint.equals("yes")) {
		//
		//
		// createBlankforUPC();
		// }
		//
		// if (preprint.equals("no")) {
		//
		//
		// if (comp.getUploadHeader() != null) {
		// createCompanyNameAsHeader(document, comp);
		// }
		//
		// if (comp.getUploadFooter() != null) {
		// createCompanyNameAsFooter(document, comp);
		// }
		// createBlankforUPC();
		// }
		// }
		//
		// } else {
		//
		//
		//
		// if (upcflag == false && preprint.equals("plane")) {
		//
		// createCompanyHedding();
		// } else {
		//
		// if (preprint.equals("yes")) {
		//
		// createBlankforUPC();
		//
		// }
		// if (preprint.equals("no")) {
		//
		// if (comp.getUploadHeader() != null) {
		// createCompanyNameAsHeader(document, comp);
		// }
		//
		// if (comp.getUploadFooter() != null) {
		// createCompanyNameAsFooter(document, comp);
		// }
		// createBlankforUPC();
		// }
		//
		// }
		//
		// }
		//
		// if (printProductPremisesFlag) {
		//
		// createProductWithoutDiscDetail();
		// } else {
		// createProductWithoutDiscDetail();
		// }
		// termsConditionsInfo();
		// footerInfo();
	}

	// End By Jayshree
	private void createLogo(Document doc, Company comp) {
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

	}

	public void createCompanyHedding() {
		/**
		 * Date 15/11/2017 By Jayshree to add the logo at proper position
		 * changes are done
		 */
		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			imageSignCell = new PdfPCell();
			imageSignCell.setBorder(0);
			imageSignCell.addElement(image2);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
			imageSignCell.setPadding(2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		PdfPTable logoTab = new PdfPTable(2);
		logoTab.setWidthPercentage(100);

		try {
			logoTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		/**
		 *  Added by Priyanka : 02-02-2021
		 *  Des - Branch correspondence name should be print in the place of branch name when branchAsCompany process config is active 
		 *        issue raise by ashwini. 
		 */
		
		String companyName ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyName=branchDt.getCorrespondenceName();
		}else{
			companyName =comp.getBusinessUnitName().trim().toUpperCase();
		}
		
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.setFont(font10bold);
		p.add(companyName);
		/**
		 * By jayshree Date 16/11/2017 To add the imageCell in Table
		 */
		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
			PdfPCell companyHeadingCell = new PdfPCell();
			companyHeadingCell.setBorder(0);
			companyHeadingCell.addElement(p);
			companyHeadingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			logoTab.addCell(companyHeadingCell);
		} else {
			PdfPCell companyHeadingCell = new PdfPCell();
			companyHeadingCell.setBorder(0);
			companyHeadingCell.addElement(p);
			companyHeadingCell.setColspan(2);
			companyHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			logoTab.addCell(companyHeadingCell);
		}
		/**
		 *  By Priynka : 28-02-2021
		 *  Des : First care req. by Ashwini.
		 */
		String custAdd1="";
		String custFullAdd2="";
		
		if(comp.getAddress()!=null){
			
			if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
			
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
				}
			}else{
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1();
				}
			}
			
			if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
				if(comp.getAddress().getPin()!=0){
					custFullAdd2=custAdd1+","+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
				}else{
					custFullAdd2=custAdd1+","+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}else{
				if(comp.getAddress().getPin()!=0){
					custFullAdd2=custAdd1+","+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
				}else{
					custFullAdd2=custAdd1+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}
			
		}	
		
			Phrase addressline=new Phrase(custFullAdd2,font9);
	
			Paragraph addresspara=new Paragraph();
			addresspara.add(addressline);
			addresspara.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell addresscell=new PdfPCell();
			addresscell.addElement(addresspara);
			addresscell.setBorder(0);
	
		Phrase branch = null;
		if (qp instanceof Contract) {
			if (contEntity.getBranch() != null&& !contEntity.getBranch().trim().equals("")) {
				branch = new Phrase("Branch : " + contEntity.getBranch(), font9);
			} else {
				branch = new Phrase(" ", font9);
			}
		} else {
			if (quotEntity.getBranch() != null&& !quotEntity.getBranch().trim().equals("")) {
				branch = new Phrase("Branch : " + quotEntity.getBranch(), font9);
			} else {
				branch = new Phrase(" ", font9);
			}
		}

		PdfPCell branchcell = new PdfPCell();
		branchcell.addElement(branch);
		branchcell.setBorder(0);
		branchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		/**
		 * Date 8/1/2018 By Jayshree New Column add for Phone And Landline
		 */

		String contactinfo = "";
		String landline = "";

		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			System.out.println("pn11");
//			contactinfo = comp.getCountryCode() + comp.getCellNumber1() + "";
			/**
			 * @author Vijay Date :- 22-09-2021
			 * Des :- Adding country code(from country master) in cell number.
			 */
			contactinfo = serverAppUtility.getMobileNoWithCountryCode(comp.getCellNumber1()+"", comp.getAddress().getCountry(), comp.getCompanyId());
			
		}
		if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
			String cellNumber2  = serverAppUtility.getMobileNoWithCountryCode(comp.getCellNumber2()+"", comp.getAddress().getCountry(), comp.getCompanyId());

			if (!contactinfo.trim().isEmpty()) {
//				contactinfo = contactinfo + " / " + comp.getCountryCode()+ comp.getCellNumber2() + "";
				contactinfo = contactinfo + " / " + cellNumber2 + "";

			} else {
				contactinfo = cellNumber2;
			}
			System.out.println("pn33" + contactinfo);
		}
		if (comp.getLandline() != 0 && comp.getLandline() != null) {
			if (!contactinfo.trim().isEmpty()) {
				contactinfo = contactinfo + " / " + comp.getStateCode()+ comp.getLandline() + "";
			} else {
				contactinfo = comp.getStateCode() + comp.getLandline() + "";
			}
			System.out.println("pn44" + contactinfo);
		}
		// End By Jayshree

		Phrase contactnos = new Phrase("Phone : " + contactinfo, font9);
	
		String branchmail = "";
		if (qp instanceof Contract) {

			ServerAppUtility serverApp = new ServerAppUtility();

			if (checkEmailId == true) {
				branchmail = serverApp.getBranchEmail(comp,
						contEntity.getBranch());
				System.out.println("server method con " + branchmail);

			} else {
				branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
				System.out.println("server method con 22" + branchmail);
			}
		} else {

			ServerAppUtility serverApp = new ServerAppUtility();
			
			if(comp!=null && comp.getCompanyId()!=null){
				logger.log(Level.SEVERE, "Company Id  - "+comp.getCompanyId());
			}
			
			if (checkEmailId == true) {
				branchmail = serverApp.getBranchEmail(comp,
						quotEntity.getBranch());
				System.out.println("server method quo " + branchmail);

			} else {
				branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
				System.out.println("server method quo 22" + branchmail);
			}

		}

		// End By jayshree
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		contactcell.setColspan(2);// add by jayshree 18/12/2017

		Phrase emailValPh = new Phrase("Email : " + branchmail, font9);
		PdfPCell emailValcell = new PdfPCell();
		emailValcell.addElement(emailValPh);
		emailValcell.setBorder(0);
		emailValcell.setColspan(2);// add by jayshree 18/12/2017

		/**
		 * Date 31-3-2018 By jayshree des.to add The headOffice email
		 */

		String hoemailid = null;
		if (hoEmail == true) {
			hoemailid = "HO Email : " + comp.getEmail();
		}

		Phrase hoemailValPh = new Phrase(hoemailid, font9);
		PdfPCell hoemailValcell = new PdfPCell();
		hoemailValcell.addElement(hoemailValPh);
		hoemailValcell.setBorder(0);
		hoemailValcell.setColspan(2);//

		Phrase webValPh = null;
		if (comp.getWebsite() != null) {
			webValPh = new Phrase("Website :" + comp.getWebsite(), font9);
		} else {
			webValPh = new Phrase("", font9);
		}
		PdfPCell webValcell = new PdfPCell();
		webValcell.addElement(webValPh);
		webValcell.setBorder(0);
		webValcell.setColspan(2);//
		// End By jayshree

		/**
		 * Date 18/12/2017 by jayshree des.changes are done to increse the
		 * column width
		 */
		PdfPTable companytable = new PdfPTable(1);
		companytable.setWidthPercentage(100);

		// by Jayshree add the logotab
		PdfPCell logoParent = new PdfPCell(logoTab);
		logoParent.setBorder(0);
		//logoParent.setColspan(2);// add by jayshree 18/12/2017
		companytable.addCell(logoParent);
		//companytable.addCell(addressline1cell);  //addresscell
		companytable.addCell(addresscell);
		companytable.addCell(branchcell);
		companytable.addCell(contactcell);
		
		// companytable.addCell(emailCell);//Add By Jayshree 8/1/2018
		companytable.addCell(emailValcell);

		/**
		 * Date 31-3-2018 by jayshree des.add web site and ho email
		 */
		if (hoEmail == true) {
			companytable.addCell(hoemailValcell);
		}
		companytable.addCell(webValcell);

		// end by Jayshree

		// ***************************************************************

		Phrase blankPhase = new Phrase("", font9);
		PdfPCell blankcell = new PdfPCell(blankPhase);
		blankcell.setBorder(0);

		if (comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(blankcell);
		}
		if (comp.getAddress().getLandmark().equals("")) {
			companytable.addCell(blankcell);
		}
		if (comp.getAddress().getLocality().equals("")) {
			companytable.addCell(blankcell);
		}

		// ********************************************************************
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);

		// rohan added this code for Printing sales person name in cotact person
		String salesPerson = "";
		if (qp instanceof Contract) {
			if (qp.getEmployee() != null && !qp.getEmployee().equals("")) {
				salesPerson = qp.getEmployee();
			}
		} else {
			salesPerson = qp.getEmployee();
		}
		/** DATE : 02-02-2021 By Priyanka - Align contact person to left side issue raise by Ashwini **/
		if (!salesPerson.equals("")) {
			Phrase realcontact = new Phrase("Contact Person: " + salesPerson,font9);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			realcontactcell.setBorder(0);
			realcontactcell.setColspan(2);// add by Jayshree 18/12/2017
			companytable.addCell(realcontactcell);
		}

		String tosir = null;
		if (cust.isCompany() == true) {
			if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
				tosir = "To,";
			}
			else{
				tosir = "To, M/S";
			}
		} else {
			tosir = "To,";
		}
		// *******************changes ends here
		// ********************sss*************

		String custName = "";

		// rohan modified this code for printabel name
		// date : 8/11/2016

		/**
		 * Date 21/11/2017 By jayshree To add the Salutation
		 */
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} 
		/**@author Abhinav Bihade
		 * @since 06/01/2020
		 * As per Rahul Tiwari's Reqiurement for Orkin:If I create a customer,
		 * then the salutation that I have used to create a customer
		 * should print on Contract and Invoice pdf
		 */
		else {
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Customer", "ChangeCompanyNameAndFullNameLable", comp.getCompanyId())&&((cust.isCompany() == true)||cust.isCompany() == false))
			{    logger.log(Level.SEVERE,"21st:"+cust.getSalutation());
				tosir=cust.getSalutation();
				if (cust.isCompany() == true && cust.getCompanyName() != null) {
					 logger.log(Level.SEVERE,"31st:"+cust.getCompanyName());
					custName = cust.getCompanyName().trim();
				} else								
				{    
					custName =  cust.getFullname().trim();
					logger.log(Level.SEVERE,"41st:"+cust.getFullname());
				}
			}
		else{
			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			} else if (cust.getSalutation() != null)// By Jayshree to add
													// salutation
			{
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}
		}

		// Phrase tosirphrase= new Phrase(tosir,font12bold);
		//
		// Phrase customername= new Phrase(custName,font12boldul);
		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname.add(custName);
		} else {
			fullname.add(tosir + "   " + custName);
		}
		fullname.setFont(font10bold);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);
		
		Address serviceAddress=null;
		String custFullAdd1="";
		/** date 29.12.2018 added by komal as quotation was not getting printed so
		 *  contEntity not null condition added
		 */
		
		/*** Date Nov 2.11.2019 added by Deepak Salve - Change Biling address and Service Address on PDF *****/
		
		if (contEntity != null && contEntity.getCustomerServiceAddress() != null) {
//			serviceAddress = contEntity.getCustomerServiceAddress(); // Deepak Salve comment this code for Customer address 
			custFullAdd1=contEntity.getCustomerServiceAddress().getCompleteAddress()+""; // Deepak Salve added this line 
//			custFullAdd1 = cust.getAdress().getCompleteAddress(); // Deepak Salve comment this code for Customer address
			serviceAddress=contEntity.getNewcustomerAddress();  // Deepak Salve added this line 
			logger.log(Level.SEVERE,"GST Service Address:: Deepak "+custFullAdd1);
			System.out.println("Custer Address "+serviceAddress);
			System.out.println("Service Address "+custFullAdd1);
		} else {
			serviceAddress = cust.getSecondaryAdress();
			custFullAdd1 = cust.getSecondaryAdress().getCompleteAddress();
			logger.log(Level.SEVERE,"GST Service Address1::"+custFullAdd1);
		}
		 
		/****  End by Deepak Salve *******/
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
		custFullAdd1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", custFullAdd1);
		
		/*
		 * End by Ashwini
		 */
		Phrase address2Val = new Phrase (custFullAdd1,font7);
		PdfPCell address2ValCell = new PdfPCell(address2Val);
	// address2ValCell.addElement(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * Date 8/1/2018 By Jayshree Add The new Cell for Landline And Phone
		 * number
		 */

		String mobileDetails = "";
		if (cust.getCellNumber1() != null && cust.getCellNumber1() != 0) {
			System.out.println("pn11");
//			mobileDetails = cust.getCellNumber1() + "";
			
			/**
			 * @author Vijay Date :- 22-09-2021
			 * Des :- Adding country code(from country master) in cell number.
			 */
			mobileDetails = serverAppUtility.getMobileNoWithCountryCode(cust.getCellNumber1()+"", cust.getAdress().getCountry(), comp.getCompanyId());
	
			
		}
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0) {
			/**
			 * @author Vijay Date :- 22-09-2021
			 * Des :- Adding country code(from country master) in cell number.
			 */
			String cellNumber2 = serverAppUtility.getMobileNoWithCountryCode(cust.getCellNumber2()+"", cust.getAdress().getCountry(), comp.getCompanyId());
	
			if (!mobileDetails.trim().isEmpty()) {
//				mobileDetails = mobileDetails + " / " + cust.getCellNumber2()+ "";
				mobileDetails = mobileDetails + " / " + cellNumber2 + "";

			} else {
//				mobileDetails = cust.getCellNumber2() + "";
				mobileDetails = cellNumber2;

			}
			System.out.println("pn33" + mobileDetails);
		}
		if (cust.getLandline() != 0 && cust.getLandline() != null) {
			if (!mobileDetails.trim().isEmpty()) {
				mobileDetails = mobileDetails + " / " + cust.getLandline() + "";
			} else {
				mobileDetails = cust.getLandline() + "";
			}
			System.out.println("pn44" + mobileDetails);
		}
		/**
		 * Ends
		 */
		/**
		 * Comment by jayshree this above code
		 */
		Phrase custcontact = new Phrase("Phone : " + mobileDetails, font9);
		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);
		/**
		 * Date 28/12/2017 By jayshree to handle null condition
		 */
		
		String custemailval = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			custemailval = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				custemailval = cust.getEmail();
			} else {
				custemailval ="";
			}
		}
		Phrase custemail = new Phrase("Email : " + custemailval, font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);

		int noserv = (int) totalServices();

		PdfPCell infotablecell = null;
		PdfPCell infotablecell1 = null;
		if (qp instanceof Contract) {

			Phrase startdt = new Phrase("Start Date", font9);
			PdfPCell startdtcell = new PdfPCell();
			startdtcell.addElement(startdt);
			startdtcell.setBorder(0);
			startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase startdtValue = null;
			if (qp instanceof Contract) {
				Contract contractEntityy = (Contract) qp;
				if (contractEntityy.getStartDate() != null) {
					startdtValue = new Phrase(fmt.format(contractEntityy
							.getStartDate()), font9);
				}
			} else {
				startdtValue = new Phrase("", font9);
			}

			PdfPCell startdtValuecell = new PdfPCell();
			startdtValuecell.addElement(startdtValue);
			startdtValuecell.setBorder(0);
			startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase enddt = new Phrase("End Date", font9);
			PdfPCell enddtcell = new PdfPCell();
			enddtcell.addElement(enddt);
			enddtcell.setBorder(0);
			enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase enddtValue = null;
			if (qp instanceof Contract) {
				Contract contractEntity = (Contract) qp;
				if (contractEntity.getEndDate() != null) {

					enddtValue = new Phrase(fmt.format(contractEntity
							.getEndDate()), font9);
				}
			} else {
				enddtValue = new Phrase("", font9);
			}

			PdfPCell enddtValuecell = new PdfPCell();
			enddtValuecell.addElement(enddtValue);
			enddtValuecell.setBorder(0);
			enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refNo = new Phrase("Ref No", font9);
			PdfPCell refNocell = new PdfPCell();
			refNocell.addElement(refNo);
			refNocell.setBorder(0);
			refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refNovalueValue = null;
			if (qp instanceof Contract) {
				Contract contractEntity = (Contract) qp;
				if (contractEntity.getRefNo() != null) {
					refNovalueValue = new Phrase(
							contractEntity.getRefNo() + "", font9);
				}
			} else {
				refNovalueValue = new Phrase("", font9);
			}

			PdfPCell refNovalueValuecell = new PdfPCell();
			refNovalueValuecell.addElement(refNovalueValue);
			refNovalueValuecell.setBorder(0);
			refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refDt = new Phrase("Ref Date", font9);
			PdfPCell refDtcell = new PdfPCell();
			refDtcell.addElement(refDt);
			refDtcell.setBorder(0);
			refDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refDtValue = null;
			if (qp instanceof Contract) {
				Contract contractEntity = (Contract) qp;
				if (contractEntity.getRefDate() != null) {
					refDtValue = new Phrase(fmt.format(contractEntity
							.getRefDate()), font9);
				}
			} else {
				refDtValue = new Phrase("", font9);
			}

			PdfPCell refDtValuecell = new PdfPCell();
			refDtValuecell.addElement(refDtValue);
			refDtValuecell.setBorder(0);
			refDtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase seperator = new Phrase(":", font9);
			PdfPCell seperatorcell = new PdfPCell();
			seperatorcell.addElement(seperator);
			seperatorcell.setBorder(0);
			seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase invoicegroupphrase1 = null;
			Phrase invoicegroupphrase3 = null;

			if (!contEntity.getGroup().equals("")) {
				invoicegroupphrase1 = new Phrase("Group", font8);
				invoicegroupphrase3 = new Phrase(contEntity.getGroup(), font8);
			} else {
				invoicegroupphrase1 = new Phrase("", font8);
				invoicegroupphrase3 = new Phrase("", font8);
			}

			PdfPCell invoicegroupcell1 = new PdfPCell();
			invoicegroupcell1.setBorder(0);
			invoicegroupcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			invoicegroupcell1.addElement(invoicegroupphrase1);
			PdfPCell invoicegroupcell3 = new PdfPCell();
			invoicegroupcell3.setBorder(0);
			invoicegroupcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			invoicegroupcell3.setColspan(4);// Date 8/12/2017 by Jayshree add
											// the Calspan
			invoicegroupcell3.addElement(invoicegroupphrase3);

			PdfPTable infotable = new PdfPTable(6);
			infotable.setHorizontalAlignment(100);

			if (contEntity.getStartDate() != null) {
				infotable.addCell(startdtcell);
			}
			if (contEntity.getStartDate() != null) {
				infotable.addCell(seperatorcell);
			}
			if (contEntity.getStartDate() != null) {
				infotable.addCell(startdtValuecell);
			}

			if (contEntity.getEndDate() != null) {
				infotable.addCell(enddtcell);
			}
			if (contEntity.getEndDate() != null) {
				infotable.addCell(seperatorcell);
			}
			if (contEntity.getEndDate() != null) {
				infotable.addCell(enddtValuecell);
			}

			if (!contEntity.getRefNo().equals("")) {
				infotable.addCell(refNocell);
				infotable.addCell(seperatorcell);
				infotable.addCell(refNovalueValuecell);
			}

			if (contEntity.getRefDate() != null) {
				infotable.addCell(refDtcell);
			}
			if (contEntity.getRefDate() != null) {
				infotable.addCell(seperatorcell);
			}
			if (contEntity.getRefDate() != null) {
				infotable.addCell(refDtValuecell);
			}
			if (!contEntity.getGroup().equals("")) {

				infotable.addCell(invoicegroupcell1);
				infotable.addCell(seperatorcell);
				infotable.addCell(invoicegroupcell3);

			}
			// End By Jayshree
			try {
				infotable.setWidths(columnWidths2);
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}

			infotablecell = new PdfPCell(infotable);
			infotablecell.setBorder(0);

		}

		else {

			if (qp instanceof Quotation) {
				Quotation quot = (Quotation) qp;

				Phrase startdt = new Phrase("Start Date", font8);
				PdfPCell startdtcell = new PdfPCell();
				startdtcell.addElement(startdt);
				startdtcell.setBorder(0);
				startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase startdtValue = null;
				if (quot.getStartDate() != null) {
					startdtValue = new Phrase(fmt.format(quot.getStartDate()),
							font8);

				} else {
					startdtValue = new Phrase("", font8);
				}

				PdfPCell startdtValuecell = new PdfPCell();
				startdtValuecell.addElement(startdtValue);
				startdtValuecell.setBorder(0);
				startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase enddt = new Phrase("End Date", font8);
				PdfPCell enddtcell = new PdfPCell();
				enddtcell.addElement(enddt);
				enddtcell.setBorder(0);
				enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase enddtValue = null;
				if (quot.getEndDate() != null) {

					enddtValue = new Phrase(fmt.format(quot.getEndDate()),
							font8);
				} else {
					enddtValue = new Phrase("", font8);
				}

				PdfPCell enddtValuecell = new PdfPCell();
				enddtValuecell.addElement(enddtValue);
				enddtValuecell.setBorder(0);
				enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase seperator = new Phrase(":", font8);
				PdfPCell seperatorcell = new PdfPCell();
				seperatorcell.addElement(seperator);
				seperatorcell.setBorder(0);
				seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);

				Phrase invoicegroupphrase1 = null;
				Phrase invoicegroupphrase3 = null;

				if (!quotEntity.getGroup().equals("")) {
					invoicegroupphrase1 = new Phrase("Group", font8);
					invoicegroupphrase3 = new Phrase(quotEntity.getGroup(),
							font8);
				} else {
					invoicegroupphrase1 = new Phrase("", font8);
					invoicegroupphrase3 = new Phrase("", font8);
				}

				PdfPCell invoicegroupcell1 = new PdfPCell();
				invoicegroupcell1.setBorder(0);
				invoicegroupcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				invoicegroupcell1.addElement(invoicegroupphrase1);

				PdfPCell invoicegroupcell3 = new PdfPCell();
				invoicegroupcell3.setBorder(0);
				invoicegroupcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
				invoicegroupcell3.setColspan(4);// Date 8/12/2017 By jayshree
												// add the colspane value
				invoicegroupcell3.addElement(invoicegroupphrase3);

				PdfPTable infotable1 = null;
				infotable1 = new PdfPTable(6);
				infotable1.setHorizontalAlignment(100);

				if (quot.getEndDate() != null) {
					infotable1.addCell(startdtcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(seperatorcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(startdtValuecell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(enddtcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(seperatorcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(enddtValuecell);
				}

				if (!quotEntity.getGroup().equals("")) {

					infotable1.addCell(invoicegroupcell1);
					infotable1.addCell(seperatorcell);
					infotable1.addCell(invoicegroupcell3);

				}
				try {
					infotable1.setWidths(columnWidths2);
				} catch (DocumentException e2) {
					e2.printStackTrace();
				}

				infotablecell1 = new PdfPCell(infotable1);
				infotablecell1.setBorder(0);

			}

		}

		/**
		 * Date 5/12/2017 By Jayshree Des.To add the customer GSTIN And
		 * Statecode
		 */
		PdfPTable stateTab = new PdfPTable(6);
		stateTab.setWidthPercentage(100);

		try {
			stateTab.setWidths(columnWidths3);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		/** Date 23-11-2018 By VIjay 
		 * for GST Number not print with process Config 
		 */
		
//		if(gstNumberPrintFlag){
//			Phrase gstinph = new Phrase("GSTIN", font8);
//			PdfPCell gstinCell = new PdfPCell(gstinph);
//			gstinCell.setBorder(0);
//			gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//			stateTab.addCell(gstinCell);
//	
//			Phrase gstincol = new Phrase(":", font8);
//			PdfPCell gstincolCell = new PdfPCell(gstincol);
//			gstincolCell.setBorder(0);
//			gstincolCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//			stateTab.addCell(gstincolCell);
//		}
		/**
		 * Date 31/1/2018 By Jayshree Des.Changes are done to remove the state
		 * code if GSTIN is not present
		 */


		String gstTinStr = "";
		if (qp instanceof Contract) {
			if (contEntity.getCustomerGSTNumber() != null&& !contEntity.getCustomerGSTNumber().equals("")) {
				gstTinStr = contEntity.getCustomerGSTNumber().trim();
			} else {
				ServerAppUtility serverAppUtility = new ServerAppUtility();
				gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,contEntity.getNewCustomerBranch(), "Contract");
			}
		} else {
			ServerAppUtility serverAppUtility = new ServerAppUtility();
			gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,quotEntity.getBranch(), "Quotation");
		}

		/** Date 23-11-2018 By VIjay 
		 * for GST Number not print with process Config 
		 */
		if(gstNumberPrintFlag){
			
//			Phrase gstinph = new Phrase("GSTIN", font8);
			Phrase gstinph =null;
			if (gstTinStr != null && !gstTinStr.equals("")) {
				gstinph = new Phrase("GSTIN", font8);
			} else {
				gstinph = new Phrase(" ", font8);
			}
			PdfPCell gstinCell = new PdfPCell(gstinph);
			gstinCell.setBorder(0);
			gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			stateTab.addCell(gstinCell);
	
//			Phrase gstincol = new Phrase(":", font8);
			Phrase gstincol = null;
			if (gstTinStr != null && !gstTinStr.equals("")) {
				gstincol = new Phrase(":", font8);
			} else {
				gstincol = new Phrase(" ", font8);
			}
			PdfPCell gstincolCell = new PdfPCell(gstincol);
			gstincolCell.setBorder(0);
			gstincolCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			stateTab.addCell(gstincolCell);
			
			Phrase gstinVal = new Phrase(gstTinStr, font8);
			PdfPCell gstinValCell = new PdfPCell(gstinVal);
			gstinValCell.setBorder(0);
			gstinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			stateTab.addCell(gstinValCell);
	
			Phrase stateph = null;
			if (gstTinStr != null && !gstTinStr.equals("")) {
				stateph = new Phrase("State Code", font8);
			} else {
				stateph = new Phrase(" ", font8);
			}
			PdfPCell stateCell = new PdfPCell(stateph);
			stateCell.setBorder(0);
			stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			stateTab.addCell(stateCell);
	
			Phrase statecol = null;
			if (gstTinStr != null && !gstTinStr.equals("")) {
				statecol = new Phrase(" : ", font8);
			} else {
				statecol = new Phrase(" ", font8);
			}
			PdfPCell statecolCell = new PdfPCell(statecol);
			statecolCell.setBorder(0);
			
			stateTab.addCell(statecolCell);
	
			String stCo = "";
			if (stateList != null) {
				for (int i = 0; i < stateList.size(); i++) {
					if (stateList.get(i).getStateName().trim()
							.equalsIgnoreCase(cust.getAdress().getState().trim())) {
						stCo = stateList.get(i).getStateCode().trim();
						break;
					}
				}
			}
	
			Phrase statecodeval = null;
			if (gstTinStr != null && !gstTinStr.equals("")) {
				statecodeval = new Phrase(stCo, font8);
			} else {
				statecodeval = new Phrase(" ", font8);
			}
			PdfPCell statecodeCell = new PdfPCell(statecodeval);
			statecodeCell.setBorder(0);
			statecodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			stateTab.addCell(statecodeCell);
	
			// End By Jayshree
		}
		/**
		 * ends here
		 */

		PdfPCell statetabCell = new PdfPCell(stateTab);
		statetabCell.setBorder(0);
		// End By Jayshree
		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		
		
		custtable.addCell(address2ValCell);//added by Ashwini
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);

		if (qp instanceof Contract) {
			custtable.addCell(infotablecell);
		} else {
			if (qp instanceof Quotation) {

				custtable.addCell(infotablecell1);
			}
		}

		// if company is true then the name of contact person

		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Attn                  :" + "  "
					+ cust.getFullname(), font7);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
			
			
		}

		custtable.addCell(statetabCell);// By jayshree to add the statetabCell
										// in custTable

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof Contract) {
			Contract conEntity = (Contract) qp;
			conEnt = conEntity.getContractDate();
		} else {
			Quotation quotEnt = (Quotation) qp;
			conEnt = quotEnt.getQuotationDate();//@author Abhinav Bihade @since 16-03-2020 As per Ashvini bhagwat's Requiremnets Change creationDate to QuotationDate 
		}

		String countinfo = "ID : " + qp.getCount() + "";

		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font12bold);// By Jayshree
															// decrese the font
															// size 14 to 12
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		// titlepdfpara.add(Chunk.NEWLINE);//Date 6/12/2017 comment by jayshree
		// to remove space
		PdfPCell titlepdfcell = new PdfPCell(titlephrase);
		// titlepdfcell.addElement(titlepdfpara);//Date 6/12/2017 comment by
		// jayshree to remove space
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdfcell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlepdfcell.setFixedHeight(20);// Date 6/12/2017 by jayshree to set the
										// height fix
		// titlepdfcell.addElement(Chunk.NEWLINE);//Date 6/12/2017 comment by
		// jayshree to remove space

		Phrase idphrase = new Phrase(countinfo, font12bold);// By Jayshree
															// decrese the font
															// size 14 to 12
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		// idofpdfpara.add(Chunk.NEWLINE);//Date 6/12/2017 comment by jayshree
		// to remove spac
		PdfPCell countcell = new PdfPCell(idphrase);
		// countcell.addElement(idofpdfpara);//Date 6/12/2017 comment by
		// jayshree to remove spac
		countcell.setBorderWidthRight(0);
		countcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		countcell.setVerticalAlignment(Element.ALIGN_CENTER);
		countcell.setFixedHeight(20);// Date 6/12/2017 by jayshree to set the
										// height fix
		// countcell.addElement(Chunk.NEWLINE);//Date 6/12/2017 comment by
		// jayshree to remove spac

		Phrase dateofpdf = new Phrase(creationdateinfo, font12bold);// By
																	// Jayshree
																	// decrese
																	// the font
																	// size 14
																	// to 12
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		// creatndatepara.add(Chunk.NEWLINE);//Date 6/12/2017 comment by
		// jayshree to remove spac
		PdfPCell creationcell = new PdfPCell(dateofpdf);
		// creationcell.addElement(creatndatepara);//Date 6/12/2017 comment by
		// jayshree to remove spac
		creationcell.setBorderWidthLeft(0);
		creationcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creationcell.setVerticalAlignment(Element.ALIGN_CENTER);
		creationcell.setFixedHeight(20);// Date 6/12/2017 by jayshree to set the
										// height fix
		// creationcell.addElement(Chunk.NEWLINE);//Date 6/12/2017 comment by
		// jayshree to remove spac

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {
			document.add(headparenttable);
			document.add(titlepdftable);

		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createCompanyAdress() {
		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),
				font12);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font12);
		}

		Phrase landmark = null;
		Phrase locality = null;
		if (comp.getAddress().getLandmark() != null
				&& comp.getAddress().getLocality().equals("") == false) {

			String landmarks = comp.getAddress().getLandmark() + " , ";
			locality = new Phrase(landmarks + comp.getAddress().getLocality()
					+ " , " + comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		} else {
			locality = new Phrase(comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		}
		Paragraph adressPragraph = new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if (adressline2 != null) {
			adressPragraph.add(adressline2);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(locality);
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);

		// Phrase for phone,landline ,fax and email

		Phrase titlecell = new Phrase("Mobile :", font8bold);
		Phrase titleTele = new Phrase("Phone :", font8bold);
		Phrase titleemail = new Phrase("Email :", font8bold);
		Phrase titlefax = new Phrase("Fax :", font8bold);

		Phrase titleservicetax = new Phrase("Service Tax No: ", font8bold);
		Phrase titlevatatx = new Phrase(" Vat Tax No: ", font8bold);
		String servicetax = comp.getServiceTaxNo();
		String vatttax = comp.getVatTaxNo();
		Phrase servicetaxphrase = null;
		Phrase vattaxphrase = null;

		if (servicetax != null && servicetax.trim().equals("") == false) {
			servicetaxphrase = new Phrase(servicetax, font8);
		}

		if (vatttax != null && vatttax.trim().equals("") == false) {
			vattaxphrase = new Phrase(vatttax, font8);
		}

		// cell number logic
		String stringcell1 = comp.getContact().get(0).getCellNo1() + "";
		String stringcell2 = null;
		Phrase mob = null;
		if (comp.getContact().get(0).getCellNo2() != -1)
			stringcell2 = comp.getContact().get(0).getCellNo2() + "";
		if (stringcell2 != null)
			mob = new Phrase(stringcell1 + " / " + stringcell2, font8);
		else
			mob = new Phrase(stringcell1, font8);

		// LANDLINE LOGIC
		Phrase landline = null;
		if (comp.getContact().get(0).getLandline() != -1)
			landline = new Phrase(comp.getContact().get(0).getLandline() + "",
					font8);

		// fax logic
		Phrase fax = null;
		if (comp.getContact().get(0).getFaxNo() != null)
			fax = new Phrase(comp.getContact().get(0).getFaxNo() + "", font8);
		// email logic
		Phrase email = new Phrase(comp.getContact().get(0).getEmail(), font8);

		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("         "));

		if (landline != null) {

			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("         "));
		}

		if (fax != null) {

			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk(" "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);

		if (servicetaxphrase != null) {
			adressPragraph.add(titleservicetax);
			adressPragraph.add(servicetaxphrase);

		}

		if (vattaxphrase != null) {
			adressPragraph.add(titlevatatx);
			adressPragraph.add(vattaxphrase);
			adressPragraph.add(new Chunk("         "));

		}

		adressPragraph.add(Chunk.NEWLINE);
		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Phrase Ptitle = new Phrase(title, font12bold);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Ptitle);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);

		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createHeaderTable() throws DocumentException {
		Phrase nameTitle = new Phrase("Name :", font8bold);
		Phrase phoneTitle = new Phrase("Phone :", font8bold);
		Phrase adressTitle = new Phrase("Address :", font8bold);
		Phrase emailTitle = new Phrase("Email :", font8bold);

		PdfPCell titlecustnameCell = new PdfPCell();
		titlecustnameCell.addElement(nameTitle);
		titlecustnameCell.setBorder(0);
		PdfPCell titlecustPhonecell = new PdfPCell();
		titlecustPhonecell.addElement(phoneTitle);
		titlecustPhonecell.setBorder(0);
		PdfPCell titleadressCell = new PdfPCell();
		titleadressCell.addElement(adressTitle);
		titleadressCell.setBorder(0);
		PdfPCell titleemailCell = new PdfPCell();
		titleemailCell.addElement(emailTitle);
		titleemailCell.setBorder(0);

		String fullname = cust.getFullname().trim();

		Phrase customername = new Phrase(fullname, font8);
		Phrase phonechunk = new Phrase(cust.getCellNumber1() + "", font8);
		Phrase emailchunk = new Phrase(cust.getEmail().trim() + "", font8);

		Phrase blankphrase = new Phrase("  ");
		PdfPCell blankcell = new PdfPCell(blankphrase);

		Phrase customeradress1 = new Phrase(cust.getAdress().getAddrLine1(),
				font8);
		Phrase customeradress2 = null;
		// Patch
		if (cust.getAdress().getAddrLine2() != null)
			customeradress2 = new Phrase(cust.getAdress().getAddrLine2(), font8);

		Phrase locality = null;

		if (cust.getAdress().getLandmark() != null) {
			locality = new Phrase(cust.getAdress().getLandmark() + " "
					+ cust.getAdress().getLocality() + " "
					+ cust.getAdress().getCity() + " "
					+ cust.getAdress().getPin(), font8);
		} else {
			locality = new Phrase(cust.getAdress().getLocality() + " "
					+ cust.getAdress().getCity() + " "
					+ cust.getAdress().getPin(), font8);
		}

		Phrase blnkphrase = new Phrase(" ");
		PdfPCell blnkcell = new PdfPCell();
		blnkcell.addElement(blnkphrase);
		blnkcell.setBorder(0);
		PdfPCell custnameCell = new PdfPCell();
		custnameCell.addElement(customername);
		custnameCell.setBorder(0);
		PdfPCell custPhonecell = new PdfPCell();
		custPhonecell.addElement(phonechunk);
		custPhonecell.setBorder(0);
		PdfPCell adress1Cell = new PdfPCell();
		adress1Cell.addElement(customeradress1);
		adress1Cell.setBorder(0);
		PdfPCell adress2Cell = new PdfPCell();
		adress2Cell.addElement(customeradress2);
		adress2Cell.setBorder(0);
		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.addElement(emailchunk);
		emailcell.setBorder(0);

		String header;
		header = "ID :";

		Phrase billno = new Phrase(header, font8bold);
		Phrase orederdate = new Phrase("Date :", font8bold);
		Phrase duration = new Phrase("Duration :", font8bold);
		Phrase noofservices = new Phrase("Services:", font8bold);
		Phrase startDate = new Phrase("Start Date :", font8bold);
		Phrase endDate = new Phrase("End Date :", font8bold);
		Phrase refNo = new Phrase("Ref No :", font8bold);
		Phrase refDate = new Phrase("Ref Date :", font8bold);

		PdfPCell titlebillCell = new PdfPCell();
		titlebillCell.addElement(billno);
		PdfPCell titledatecell = new PdfPCell();
		titledatecell.addElement(orederdate);
		PdfPCell titledurationCell = new PdfPCell();
		titledurationCell.addElement(duration);
		PdfPCell titlenoServicesCell = new PdfPCell();
		titlenoServicesCell.addElement(noofservices);
		PdfPCell startdateCell = new PdfPCell();
		startdateCell.addElement(startDate);
		PdfPCell enddateCell = new PdfPCell();
		enddateCell.addElement(endDate);
		PdfPCell refNoCell = new PdfPCell();
		refNoCell.addElement(refNo);
		PdfPCell refDateCell = new PdfPCell();
		refDateCell.addElement(refDate);

		Phrase realqno = null;
		realqno = new Phrase(qp.getCount() + "", font8);

		String date = fmt.format(qp.getCreationDate());
		Phrase realorederdate = new Phrase(date + "", font8);
		realorederdate.trimToSize();
		Phrase relduration = new Phrase(largestduration(), font8);

		int noserv = (int) totalServices();

		Phrase relnoservices = new Phrase(noserv + "", font8);

		Quotation quot = (Quotation) qp;
		String start;
		if (quot.getStartDate() == null) {
			start = "N.A";
		} else {
			start = fmt.format(quot.getStartDate());
		}

		Phrase realstartdate = new Phrase(start, font8);

		if (quot.getEndDate() == null)
			start = "N.A";
		else
			start = fmt.format(quot.getEndDate());

		Phrase realenddate = new Phrase(start, font8);

		Phrase refno = null;
		if (qp instanceof Contract) {
			if (contEntity.getRefNo() != null) {

				refno = new Phrase(contEntity.getRefNo() + "", font8);
			}
		} else {
			refno = new Phrase("", font8);
		}
		Phrase refDt = null;
		if (qp instanceof Contract) {
			if (contEntity != null && contEntity.getRefDate() != null) {
				refDt = new Phrase(fmt.format(contEntity.getRefDate()), font8);
			}
		} else {
			refDt = new Phrase("", font8);
		}

		PdfPCell billCell = new PdfPCell();
		billCell.addElement(realqno);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(realorederdate);
		PdfPCell durationCell = new PdfPCell();
		durationCell.addElement(relduration);
		PdfPCell noServicesCell = new PdfPCell();
		noServicesCell.addElement(relnoservices);
		PdfPCell realstartdateCell = new PdfPCell();
		realstartdateCell.addElement(realstartdate);
		PdfPCell realenddateCell = new PdfPCell();
		realenddateCell.addElement(realenddate);
		PdfPCell refno1cell = new PdfPCell();
		refno1cell.addElement(refno);
		PdfPCell refdt1cell = new PdfPCell();
		refdt1cell.addElement(refDt);

		PdfPTable table = new PdfPTable(2);

		// 1 st row name phone
		table.addCell(titlecustnameCell);
		table.addCell(custnameCell);

		table.addCell(titlecustPhonecell);
		table.addCell(custPhonecell);

		// 2nd row adress and email
		table.addCell(titleadressCell);
		table.addCell(adress1Cell);
		if (customeradress2 != null) {
			table.addCell(blnkcell);
			table.addCell(adress2Cell);
		}
		table.addCell(blnkcell);
		table.addCell(localitycell);
		table.addCell(titleemailCell);
		table.addCell(emailcell);

		table.setWidths(new float[] { 20, 80 });
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);

		PdfPTable billtable = new PdfPTable(4);
		billtable.setWidths(new float[] { 20, 30, 20, 30 });
		billtable.setWidthPercentage(100);
		billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

		// 1 row bill number and date
		billtable.addCell(titlebillCell);
		billtable.addCell(billCell);
		billtable.addCell(titledatecell);
		billtable.addCell(datecell);

		// 2 row duration and start date
		billtable.addCell(titledurationCell);
		billtable.addCell(durationCell);
		billtable.addCell(startdateCell);
		billtable.addCell(realstartdateCell);
		billtable.addCell(refno1cell);
		billtable.addCell(refdt1cell);

		// 3 row no serivces and end date
		billtable.addCell(titlenoServicesCell);
		billtable.addCell(noServicesCell);
		billtable.addCell(enddateCell);
		billtable.addCell(realenddateCell);
		billtable.addCell(refNoCell);
		billtable.addCell(refDateCell);

		int row = 3;
		// 4 row valid untill and blank
		if (qp instanceof Quotation) {
			PdfPCell titlevalid = new PdfPCell();
			PdfPCell realvaliduntil = new PdfPCell();

			Phrase valid = null;
			String validuntil = "";
			if (((Quotation) qp).getValidUntill() == null) {
				validuntil = "";
				valid = new Phrase("", font8bold);
			} else {
				System.out.println("................in side else condition");

				valid = new Phrase("Valid Until", font8bold);
				;
				validuntil = fmt.format(((Quotation) qp).getValidUntill());
			}
			Phrase rvalid = new Phrase(validuntil, font8);
			titlevalid.addElement(valid);
			realvaliduntil.addElement(rvalid);
			billtable.addCell(titlevalid);
			billtable.addCell(realvaliduntil);
			billtable.addCell(blankcell);
			billtable.addCell(blankcell);

			row = 4;
		}
		int noCol = table.getNumberOfColumns();
		// for(int i=0;i<2;i++)
		// {
		// PdfPRow temp=table.getRow(i);
		// PdfPCell[] cells= temp.getCells();
		// for(PdfPCell cell:cells)
		// cell.setBorder(Rectangle.NO_BORDER);
		// }
		//
		for (int i = 0; i < row; i++) {
			PdfPRow temp = billtable.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells) {
				cell.setBorder(Rectangle.NO_BORDER);
			}
		}

		PdfPTable parenttable = new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		parenttable.setWidths(new float[] { 50, 50 });

		PdfPCell custinfocell = new PdfPCell();
		PdfPCell billinfocell = new PdfPCell();

		custinfocell.addElement(table);
		billinfocell.addElement(billtable);

		parenttable.addCell(custinfocell);
		parenttable.addCell(billinfocell);

		document.add(parenttable);
	}

	// public void ceateComments()
	// {
	// Font font1 = new Font(Font.FontFamily.HELVETICA , 8,Font.BOLD);
	// Chunk commentChunk= new Chunk("Description :",font1);
	//
	// Chunk deschunk = null;
	// if(qp.getDescription()!=null)
	// deschunk= new Chunk(qp.getDescription(),font8);
	//
	// Paragraph para = new Paragraph();
	// para.add(Chunk.NEWLINE);
	// para.add(commentChunk);
	// para.add(Chunk.NEWLINE);
	// if(deschunk!=null)
	// para.add(deschunk);
	// para.add(Chunk.NEWLINE);
	// para.add(Chunk.NEWLINE);
	// try {
	// document.add(para);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }

	// private void createProductDetailWithDiscount() {
	//
	// /**
	// * rohan added this code for adding premises in the pdf
	// */
	// PdfPTable premisesTable = new PdfPTable(1);
	// premisesTable.setWidthPercentage(100f);
	// /**
	// * ends here
	// */
	//
	// Font font1 = new Font(Font.FontFamily.HELVETICA , 8|Font.BOLD);
	// Phrase productdetails= new Phrase("Product Details",font1);
	// Paragraph para = new Paragraph();
	// para.add(productdetails);
	// para.setAlignment(Element.ALIGN_CENTER);
	// PdfPTable table = new PdfPTable(12);
	// table.setWidthPercentage(100);
	// Phrase srno = new Phrase("SrNo",font1);
	// // Phrase category = new Phrase("Prod ID",font1);
	// Phrase product = new Phrase("Treatment Details",font1);
	// // Phrase qty = new Phrase("No of Branches",font1);
	// Phrase area = new Phrase("AREA",font1);
	//
	// Phrase startdate = new Phrase("Contract Period",font1);
	// Phrase noservices = new Phrase("Services",font1);
	// Phrase rate= new Phrase ("Rate",font1);
	//
	// Phrase servicetax= null;
	// Phrase servicetax1= null;
	//
	// PdfPCell cellsrNo = new PdfPCell(srno);
	// cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// // PdfPCell cellcategory = new PdfPCell(category);
	// // cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell cellproduct = new PdfPCell(product);
	// cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// // PdfPCell cellqty = new PdfPCell(qty);
	// // cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell cellarea = new PdfPCell(area);
	// cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	//
	// PdfPCell cellstartdate = new PdfPCell(startdate);
	// cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// // PdfPCell durationcell = new PdfPCell(duration);
	// // durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell servicecell = new PdfPCell(noservices);
	// servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell cellrate = new PdfPCell(rate);
	// cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	//
	//
	// PdfPCell cellservicetax1= null;
	// PdfPCell cellservicetax= null;
	// PdfPCell cellservicetax2= null;
	// Phrase servicetax2=null;
	//
	// // if(flag1>0){
	// // cellservicetax1 = new PdfPCell(servicetax1);
	// // cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
	// // }
	// // else if(vat>0 && st>0)
	// // {
	// // System.out.println("in side condition");
	// // servicetax2 = new Phrase("VAT / ST(%)",font1);
	// // cellservicetax2 = new PdfPCell(servicetax2);
	// // cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
	// // }
	// // else {
	// // cellservicetax = new PdfPCell(servicetax);
	// // cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	// //
	// // }
	// // PdfPCell percdisccell=new PdfPCell(percdisctitle);
	// // percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// //
	// // PdfPCell discAmtCell=new PdfPCell(discAmt);
	// // percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// //
	// //
	// // PdfPCell celltotal= new PdfPCell(total);
	// // celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// table.addCell(cellsrNo);
	// table.addCell(cellproduct);
	// // table.addCell(cellqty);
	// table.addCell(cellarea);
	// table.addCell(cellstartdate);
	// // table.addCell(durationcell);
	// table.addCell(servicecell);
	// table.addCell(cellrate);
	//
	// // if(flag1>0){
	// // table.addCell(cellservicetax1);
	// // }else if(vat>0 && st>0){
	// // table.addCell(cellservicetax2);
	// // }else
	// // {
	// // table.addCell(cellservicetax);
	// // }
	// // table.addCell(percdisccell);
	// // table.addCell(discAmtCell);
	// // table.addCell(celltotal);
	// try {
	// table.setWidths(columnWidthsfordicsAMt);
	// } catch (DocumentException e1) {
	// e1.printStackTrace();
	// }
	//
	//
	// PdfPCell tableCell = new PdfPCell(table);
	// premisesTable.addCell(tableCell);
	//
	//
	//
	// ////////////dummy..............change
	// if( this.products.size()<= firstBreakPoint){
	// int size = firstBreakPoint - this.products.size();
	// blankLines = size*(185/17);
	// System.out.println("blankLines size ="+blankLines);
	// System.out.println("blankLines size ="+blankLines);
	// }
	// else{
	// blankLines = 10f;
	// }
	// premisesTable.setSpacingAfter(blankLines);
	//
	//
	//
	// Paragraph pararefer=null;
	//
	// for(int i=0;i<this.products.size();i++)
	// {
	//
	// PdfPTable table1 = new PdfPTable(12);
	// table1.setWidthPercentage(100);
	//
	// try {
	// table1.setWidths(columnWidthsfordicsAMt);
	// } catch (DocumentException e1) {
	// e1.printStackTrace();
	// }
	//
	// System.out.println("iiiii-=----"+i);
	// chunk = new Phrase((i+1)+"",font8);
	// PdfPCell Pdfsrnocell = new PdfPCell(chunk);
	// Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	//
	// // chunk=new Phrase(products.get(i).getPrduct().getCount()+"",font8);
	// // PdfPCell pdfcategcell = new PdfPCell(chunk);
	//
	// chunk = new Phrase(products.get(i).getProductName(),font8);
	// PdfPCell pdfnamecell = new PdfPCell(chunk);
	// pdfnamecell.setColspan((int) 1.5);
	// chunk = new Phrase(products.get(i).getQty()+"",font8);
	// PdfPCell pdfqtycell = new PdfPCell(chunk);
	// pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	//
	// chunk = new Phrase(products.get(i).getArea()+"",font8);
	// PdfPCell pdfAreacell = new PdfPCell(chunk);
	// pdfAreacell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// PdfPCell pdfdatecell ;
	// PdfPCell pdfSerCell;
	//
	// String date =fmt.format(products.get(i).getStartDate());
	// chunk = new Phrase(date,font8);
	// pdfdatecell = new PdfPCell(chunk);
	// pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// chunk = new Phrase(products.get(i).getNumberOfServices()+"",font8);
	// pdfSerCell = new PdfPCell(chunk);
	// pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// chunk = new Phrase(products.get(i).getDuration()+"",font8);
	// PdfPCell pdfdurCell = new PdfPCell(chunk);
	// pdfdurCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// double taxVal=removeAllTaxes(products.get(i).getPrduct());
	// double calculatedPrice=products.get(i).getPrice()-taxVal;
	//
	// chunk = new Phrase(df.format(calculatedPrice),font8);
	// PdfPCell pdfspricecell = new PdfPCell(chunk);
	// pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//
	//
	// PdfPCell pdfservice =null;
	// Phrase vatno=null;
	// Phrase stno=null;
	// if((products.get(i).getVatTax().getPercentage()==0)&&(products.get(i).getServiceTax().getPercentage()>0))
	// // {
	// //
	// // if(products.get(i).getServiceTax().getPercentage()!=0){
	// // if(st==products.size()){
	// // chunk = new
	// Phrase(products.get(i).getServiceTax().getPercentage()+"",font8);
	// // }
	// // else{
	// // stno = new
	// Phrase(products.get(i).getVatTax().getPercentage()+""+" / "+products.get(i).getServiceTax().getPercentage()+"",font8);
	// // }
	// // }
	// // else
	// // chunk = new Phrase("N.A"+"",font8);
	// // if(st==products.size()){
	// // pdfservice = new PdfPCell(chunk);
	// // }else{
	// // pdfservice = new PdfPCell(stno);
	// // }
	// //
	// // pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	// // }
	// // else
	// if((products.get(i).getServiceTax().getPercentage()==0)&&(products.get(i).getVatTax().getPercentage()>0))
	// // {
	// //
	// // if(products.get(i).getVatTax()!=null){
	// // if(vat==products.size()){
	// // System.out.println("rohan=="+vat);
	// // chunk = new
	// Phrase(products.get(i).getVatTax().getPercentage()+"",font8);
	// // }else{
	// // System.out.println("mukesh");
	// // vatno = new
	// Phrase(products.get(i).getVatTax().getPercentage()+""+" / "+products.get(i).getServiceTax().getPercentage(),font8);
	// // }
	// // }
	// // else{
	// // chunk = new Phrase("N.A"+"",font8);
	// // }
	// // if(vat==products.size()){
	// // pdfservice = new PdfPCell(chunk);
	// // }else{ pdfservice = new PdfPCell(vatno);}
	// // pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	// // }
	// // else
	// if((products.get(i).getServiceTax().getPercentage()>0)&&(products.get(i).getVatTax().getPercentage()>0)){
	// // if((products.get(i).getVatTax()!=null)&&
	// (products.get(i).getServiceTax()!=null))
	// //
	// // chunk = new
	// Phrase(products.get(i).getVatTax().getPercentage()+""+" / "+""
	// // +products.get(i).getServiceTax().getPercentage(),font8);
	// // else
	// // chunk = new Phrase("N.A"+"",font8);
	// // pdfservice = new PdfPCell(chunk);
	// // pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	// //
	// // }
	// // else{
	// //
	// // chunk = new Phrase("N.A"+"",font8);
	// // pdfservice = new PdfPCell(chunk);
	// // pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	// // }
	//
	//
	// if(products.get(i).getPercentageDiscount()!=0){
	// chunk=new Phrase(products.get(i).getPercentageDiscount()+"",font8);
	// }
	// else{
	// chunk=new Phrase("0"+"",font8);
	// }
	// PdfPCell realpercdisccell=new PdfPCell(chunk);
	// realpercdisccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	// realpercdisccell.setColspan((int)0.5);
	//
	//
	// //**************disc Amt *********************
	// if(products.get(i).getDiscountAmt()!=0){
	// chunk=new Phrase(products.get(i).getDiscountAmt()+"",font8);
	// }
	// else{
	// chunk=new Phrase("0"+"",font8);
	// }
	// PdfPCell discAmtcell=new PdfPCell(chunk);
	// discAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	// discAmtcell.setColspan((int)0.5);
	//
	//
	// // rohan remove qty from calculations because we have remove that from
	// contract calculations also
	// double totalVal=(products.get(i).getPrice()-taxVal);
	// if(products.get(i).getPercentageDiscount()!=0){
	// totalVal=totalVal-(totalVal*products.get(i).getPercentageDiscount()/100);
	// }
	// else if(products.get(i).getDiscountAmt()!=0)
	// {
	// totalVal=totalVal-products.get(i).getDiscountAmt();
	// }
	//
	// chunk = new Phrase(df.format(totalVal),font8);
	// PdfPCell pdftotalproduct = new PdfPCell(chunk);
	// pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//
	//
	// count=i;
	//
	// table1.addCell(Pdfsrnocell);
	// // table.addCell(pdfcategcell);
	// table1.addCell(pdfnamecell);
	// table1.addCell(pdfqtycell);
	// table1.addCell(pdfAreacell);
	// table1.addCell(pdfdatecell);
	// table1.addCell(pdfdurCell);
	// table1.addCell(pdfSerCell);
	// table1.addCell(pdfspricecell);
	// table1.addCell(pdfservice);
	// // table.addCell(pdfvattax);
	// table1.addCell(realpercdisccell);
	// table1.addCell(discAmtcell);
	// table1.addCell(pdftotalproduct);
	// try {
	// table1.setWidths(columnWidthsfordicsAMt);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	//
	//
	// /**
	// * rohan added code here for premises details
	// * Date : 27/01/2017
	// */
	//
	// PdfPCell tableForloopCell = new PdfPCell(table1);
	//
	// premisesTable.addCell(tableForloopCell);
	//
	// String premises ="";
	// if(products.get(i).getPremisesDetails()!= null &&
	// !products.get(i).getPremisesDetails().equals("")){
	// premises="Premise Details : "+products.get(i).getPremisesDetails();
	// }
	// else
	// {
	// premises="Premise Details : "+"N A";
	// }
	//
	// Phrase premisesPh = new Phrase(premises,font8);
	// PdfPCell premisesCell = new PdfPCell(premisesPh);
	//
	// premisesTable.addCell(premisesCell);
	//
	// /**
	// * ends here
	// */
	//
	// // count=i;
	//
	//
	// if(count==firstBreakPoint){
	//
	// Phrase referannexure=new
	// Phrase("Refer Annexure 1 for additional products ",font8);
	// pararefer= new Paragraph();
	// pararefer.add(referannexure);
	//
	// flag=firstBreakPoint;
	// break;
	// }
	//
	//
	//
	// System.out.println("flag value" + flag);
	// if(firstBreakPoint==flag){
	//
	// termsConditionsInfo();
	// footerInfo();
	// }
	// }
	//
	//
	//
	//
	//
	// PdfPTable parentTableProd=new PdfPTable(1);
	// parentTableProd.setWidthPercentage(100);
	// PdfPCell prodtablecell=new PdfPCell();
	// prodtablecell.addElement(premisesTable);
	// prodtablecell.addElement(pararefer);
	//
	// parentTableProd.addCell(prodtablecell);
	//
	// try {
	// document.add(parentTableProd);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }
	
	/**
	 * Date 17-01-2018 by below method commented by Vijay
	 * this method not used anywhere so
	 */

//	public void createProductDetail() {
//
//		/**
//		 * rohan added this code for adding premises in the pdf
//		 */
//		PdfPTable premisesTable = new PdfPTable(1);
//		premisesTable.setWidthPercentage(100f);
//		/**
//		 * ends here
//		 */
//
//		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
//		Phrase productdetails = new Phrase("Product Details", font1);
//		Paragraph para = new Paragraph();
//		para.add(productdetails);
//		para.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPTable table = new PdfPTable(11);
//		table.setWidthPercentage(100);
//		Phrase srno = new Phrase("Sr No", font1);
//		Phrase product = new Phrase("Item Details", font1);
//		Phrase qty = new Phrase("No of Branches", font1);
//		Phrase area = new Phrase("Area", font1);
//		Phrase startdate = new Phrase("Start Date", font1);
//		Phrase duration = new Phrase("Duration(d)", font1);
//		Phrase noservices = new Phrase("Services", font1);
//		Phrase rate = new Phrase("Rate", font1);
//
//		Phrase servicetax = null;
//		Phrase servicetax1 = null;
//		int flag1 = 0;
//		int vat = 0;
//		int st = 0;
//		for (int i = 0; i < products.size(); i++) {
//			if ((products.get(i).getVatTax().getPercentage() > 0)
//					&& (products.get(i).getServiceTax().getPercentage() == 0)) {
//				servicetax = new Phrase("VAT(%)", font1);
//				System.out.println("Rohan1");
//				vat = vat + 1;
//				System.out.println("phrase value====" + servicetax.toString());
//			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
//					&& (products.get(i).getVatTax().getPercentage() == 0)) {
//				servicetax = new Phrase("ST(%)", font1);
//				System.out.println("Rohan22222222222");
//				st = st + 1;
//			} else if ((products.get(i).getVatTax().getPercentage() > 0)
//					&& (products.get(i).getServiceTax().getPercentage() > 0)) {
//				servicetax1 = new Phrase("VAT / ST(%)", font1);
//				System.out.println("Rohan3333333333333");
//				flag1 = flag1 + 1;
//				System.out.println("flag value;;;;;" + flag1);
//			} else {
//
//				servicetax = new Phrase("TAX(%)", font1);
//				System.out.println("Rohan4444444444444");
//			}
//		}
//
//		Phrase percdisctitle = null;
//		if (discPer > 0) {
//			percdisctitle = new Phrase("% Disc", font1);
//		} else if (discAmt > 0) {
//			percdisctitle = new Phrase("Disc", font1);
//		}
//		Phrase total = new Phrase("Total", font1);
//
//		System.out.println("vat===" + vat);
//		System.out.println("st====" + st);
//		System.out.println("flag1===" + flag1);
//		PdfPCell cellsrNo = new PdfPCell(srno);
//		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		// PdfPCell cellcategory = new PdfPCell(category);
//		// cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell cellproduct = new PdfPCell(product);
//		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell cellqty = new PdfPCell(qty);
//		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell cellarea = new PdfPCell(area);
//		cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell cellstartdate = new PdfPCell(startdate);
//		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell durationcell = new PdfPCell(duration);
//		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell servicecell = new PdfPCell(noservices);
//		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell cellrate = new PdfPCell(rate);
//		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell cellservicetax1 = null;
//		PdfPCell cellservicetax = null;
//		PdfPCell cellservicetax2 = null;
//		Phrase servicetax2 = null;
//
//		if (flag1 > 0) {
//			cellservicetax1 = new PdfPCell(servicetax1);
//			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
//		} else if (vat > 0 && st > 0) {
//			System.out.println("in side condition");
//			servicetax2 = new Phrase("VAT / ST(%)", font1);
//			cellservicetax2 = new PdfPCell(servicetax2);
//			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
//		} else {
//			cellservicetax = new PdfPCell(servicetax);
//			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		}
//		PdfPCell percdisccell = new PdfPCell(percdisctitle);
//		percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell celltotal = new PdfPCell(total);
//		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		table.addCell(cellsrNo);
//		table.addCell(cellproduct);
//		table.addCell(cellqty);
//		table.addCell(cellarea);
//		table.addCell(cellstartdate);
//		table.addCell(durationcell);
//		table.addCell(servicecell);
//		table.addCell(cellrate);
//
//		if (flag1 > 0) {
//			table.addCell(cellservicetax1);
//		} else if (vat > 0 && st > 0) {
//			table.addCell(cellservicetax2);
//		} else {
//			table.addCell(cellservicetax);
//		}
//		table.addCell(percdisccell);
//		table.addCell(celltotal);
//		try {
//			table.setWidths(columnWidths);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		PdfPCell tableCell = new PdfPCell(table);
//		premisesTable.addCell(tableCell);
//
//		// //////////dummy..............change
//		if (this.products.size() <= firstBreakPoint) {
//			int size = firstBreakPoint - this.products.size();
//			blankLines = size * (185 / 17);
//			System.out.println("blankLines size =" + blankLines);
//			System.out.println("blankLines size =" + blankLines);
//		} else {
//			blankLines = 10f;
//		}
//		// premisesTable.setSpacingAfter(blankLines);//comment by jayshree to
//		// remove space after product
//
//		Paragraph pararefer = null;
//
//		for (int i = 0; i < this.products.size(); i++) {
//
//			PdfPTable table1 = new PdfPTable(11);
//			table1.setWidthPercentage(100);
//
//			System.out.println("iiiii-=----" + i);
//			chunk = new Phrase((i + 1) + "", font8);
//			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
//			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			// chunk=new
//			// Phrase(products.get(i).getPrduct().getCount()+"",font8);
//			// PdfPCell pdfcategcell = new PdfPCell(chunk);
//
//			chunk = new Phrase(products.get(i).getProductName(), font8);
//			PdfPCell pdfnamecell = new PdfPCell(chunk);
//			pdfnamecell.setColspan((int) 1.5);
//
//			chunk = new Phrase(products.get(i).getQty() + "", font8);
//			PdfPCell pdfqtycell = new PdfPCell(chunk);
//			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			chunk = new Phrase(products.get(i).getArea() + "", font8);
//			PdfPCell pdfareacell = new PdfPCell(chunk);
//			pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			PdfPCell pdfdatecell;
//			PdfPCell pdfSerCell;
//
//			String date = fmt.format(products.get(i).getStartDate());
//			chunk = new Phrase(date, font8);
//			pdfdatecell = new PdfPCell(chunk);
//			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
//					font8);
//			pdfSerCell = new PdfPCell(chunk);
//			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			chunk = new Phrase(products.get(i).getDuration() + "", font8);
//			PdfPCell pdfdurCell = new PdfPCell(chunk);
//			pdfdurCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			double taxVal = removeAllTaxes(products.get(i).getPrduct());
//			double calculatedPrice = products.get(i).getPrice() - taxVal;
//
//			chunk = new Phrase(df.format(calculatedPrice), font8);
//			PdfPCell pdfspricecell = new PdfPCell(chunk);
//			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
//			PdfPCell pdfservice = null;
//			Phrase vatno = null;
//			Phrase stno = null;
//			if ((products.get(i).getVatTax().getPercentage() == 0)
//					&& (products.get(i).getServiceTax().getPercentage() > 0)) {
//
//				if (products.get(i).getServiceTax().getPercentage() != 0) {
//					if (st == products.size()) {
//						chunk = new Phrase(products.get(i).getServiceTax()
//								.getPercentage()
//								+ "", font8);
//					} else {
//						stno = new Phrase(products.get(i).getVatTax()
//								.getPercentage()
//								+ ""
//								+ " / "
//								+ products.get(i).getServiceTax()
//										.getPercentage() + "", font8);
//					}
//				} else
//					chunk = new Phrase("N.A" + "", font8);
//				if (st == products.size()) {
//					pdfservice = new PdfPCell(chunk);
//				} else {
//					pdfservice = new PdfPCell(stno);
//				}
//
//				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
//					&& (products.get(i).getVatTax().getPercentage() > 0)) {
//
//				if (products.get(i).getVatTax() != null) {
//					if (vat == products.size()) {
//						System.out.println("rohan==" + vat);
//						chunk = new Phrase(products.get(i).getVatTax()
//								.getPercentage()
//								+ "", font8);
//					} else {
//						System.out.println("mukesh");
//						vatno = new Phrase(products.get(i).getVatTax()
//								.getPercentage()
//								+ ""
//								+ " / "
//								+ products.get(i).getServiceTax()
//										.getPercentage(), font8);
//					}
//				} else {
//					chunk = new Phrase("N.A" + "", font8);
//				}
//				if (vat == products.size()) {
//					pdfservice = new PdfPCell(chunk);
//				} else {
//					pdfservice = new PdfPCell(vatno);
//				}
//				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
//					&& (products.get(i).getVatTax().getPercentage() > 0)) {
//				if ((products.get(i).getVatTax() != null)
//						&& (products.get(i).getServiceTax() != null))
//
//					chunk = new Phrase(products.get(i).getVatTax()
//							.getPercentage()
//							+ ""
//							+ " / "
//							+ ""
//							+ products.get(i).getServiceTax().getPercentage(),
//							font8);
//				else
//					chunk = new Phrase("N.A" + "", font8);
//				pdfservice = new PdfPCell(chunk);
//				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//			} else {
//
//				chunk = new Phrase("N.A" + "", font8);
//				pdfservice = new PdfPCell(chunk);
//				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//			}
//
//			if (discPer > 0) {
//				if (products.get(i).getPercentageDiscount() != 0) {
//					chunk = new Phrase(products.get(i).getPercentageDiscount()
//							+ "", font8);
//				} else {
//					chunk = new Phrase("0" + "", font8);
//				}
//			} else if (discAmt > 0) {
//				if (products.get(i).getDiscountAmt() != 0) {
//					chunk = new Phrase(products.get(i).getDiscountAmt() + "",
//							font8);
//				} else {
//					chunk = new Phrase("0" + "", font8);
//				}
//			}
//			PdfPCell realpercdisccell = new PdfPCell(chunk);
//			realpercdisccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			realpercdisccell.setColspan((int) 0.5);
//
//			// rohan remove qty from calculations because we have remove that
//			// from contract calculations also
//			double totalVal = (products.get(i).getPrice() - taxVal);
//			if (products.get(i).getPercentageDiscount() != 0) {
//				totalVal = totalVal
//						- (totalVal * products.get(i).getPercentageDiscount() / 100);
//			}
//
//			chunk = new Phrase(df.format(totalVal), font8);
//			PdfPCell pdftotalproduct = new PdfPCell(chunk);
//			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
//			count = i;
//
//			table1.addCell(Pdfsrnocell);
//			// table.addCell(pdfcategcell);
//			table1.addCell(pdfnamecell);
//			table1.addCell(pdfqtycell);
//			table1.addCell(pdfareacell);
//			table1.addCell(pdfdatecell);
//			table1.addCell(pdfdurCell);
//			table1.addCell(pdfSerCell);
//			table1.addCell(pdfspricecell);
//			table1.addCell(pdfservice);
//			// table1.addCell(pdfvattax);
//			table1.addCell(realpercdisccell);
//			table1.addCell(pdftotalproduct);
//			try {
//				table1.setWidths(columnWidths);
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
//
//			/**
//			 * rohan added code here for premises details Date : 27/01/2017
//			 */
//
//			PdfPCell table123Cell = new PdfPCell(table1);
//
//			premisesTable.addCell(table123Cell);
//
//			String premises = "";
//			if (products.get(i).getPremisesDetails() != null
//					&& !products.get(i).getPremisesDetails().equals("")) {
//				premises = "Premise Details : "
//						+ products.get(i).getPremisesDetails();
//			} else {
//				premises = "Premise Details : " + "N A";
//			}
//
//			Phrase premisesPh = new Phrase(premises, font8);
//			PdfPCell premisesCell = new PdfPCell(premisesPh);
//
//			premisesTable.addCell(premisesCell);
//
//			/**
//			 * ends here
//			 */
//
//			// count=i;
//
//			if (count == firstBreakPoint) {
//
//				Phrase referannexure = new Phrase(
//						"Refer Annexure 1 for additional products ", font8);
//				pararefer = new Paragraph();
//				pararefer.add(referannexure);
//
//				flag = firstBreakPoint;
//				break;
//			}
//
//			System.out.println("flag value" + flag);
//			if (firstBreakPoint == flag) {
//
//				termsConditionsInfo();
//				footerInfo();
//			}
//		}
//
//		PdfPTable parentTableProd = new PdfPTable(1);
//		parentTableProd.setWidthPercentage(100);
//		PdfPCell prodtablecell = new PdfPCell();
//		prodtablecell.addElement(premisesTable);
//		prodtablecell.addElement(pararefer);
//
//		parentTableProd.addCell(prodtablecell);
//
//		try {
//			document.add(parentTableProd);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * ends here
	 */
	
	public void termsConditionsInfo() {
		System.out.println("Rohan in side terms condition info");
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		//Ashwini Patil Date:24-04-2023 validity is to be printed at footer level above gst no
//		PdfPCell validitydatecell = new PdfPCell();
//		if (qp instanceof Quotation) {
//
//			Phrase validDate = new Phrase("", font8);
//
//			if (((Quotation) qp).getValidUntill() != null) {
//				Phrase validityDate = new Phrase("Valid Until:  "
//						+ fmt.format(((Quotation) qp).getValidUntill()) + "",
//						font6bold);
//				validitydatecell.addElement(validityDate);
//				validitydatecell.setBorder(0);
//			} else {
//				validitydatecell.addElement(validDate);
//				validitydatecell.setBorder(0);
//			}
//		}
		// Date 11-11-2017
		// By jayshree
		// Changes are made to change the payment term to billing term
		int np = (int) ((Quotation) qp).getNetpayable();
		String amtInFigure = ServiceInvoicePdf.convert(np);
		// Phrase netpayam=new Phrase("Rupees : "+amtInFigure,font8);

		/**
		 *  Added by Priyanka - 28-02-2021 
		 *  Des : First care Requirement.
		 */
		String paymentTerm="";
//		if(contEntity!=null && contEntity.getPaymentTermsList()!=null&&contEntity.getPaymentTermsList().size()!=0){
//			paymentTerm=contEntity.getPaymentTermsList().get(0).getPayTermComment();
//			
//		}
		/**
		 * @author Anil @since 29-07-2021
		 * Adding Payment terms
		 */
		if(quotEntity!=null&&quotEntity.getPayTerms()!=null&&!quotEntity.getPayTerms().equals("")){
			paymentTerm=quotEntity.getPayTerms();
		}else if(contEntity!=null&&contEntity.getPayTerms()!=null&&!contEntity.getPayTerms().equals("")){
			if(contEntity.getPayTerms().equals(AppConstants.MONTHLYPREVIOUSMONTHBILLINGPERIOD) || 
					contEntity.getPayTerms().equals(AppConstants.MONTHLYPARTIALFIRSTMONTH)){
				paymentTerm="Monthly";
			}
			else{
				paymentTerm=contEntity.getPayTerms();
			}
		}
		
		Phrase payphrase = new Phrase("Payment Term : " +paymentTerm  ,font10);
		PdfPCell paytitle = new PdfPCell(payphrase);
		paytitle.setBorder(0);
		paytitle.setHorizontalAlignment(Element.ALIGN_LEFT);
        
		
		
		int creditDays=0;
		if(contEntity!=null && contEntity.getCreditPeriod()!=null&&!contEntity.getCreditPeriod().equals("")){
			creditDays=contEntity.getCreditPeriod();
		}
	    
		Phrase creditphrase = new Phrase("Credit Period : " +creditDays+" "+"days"  ,font10);
		PdfPCell creditdaycell = new PdfPCell(creditphrase);
		creditdaycell.setBorder(0);
		creditdaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
        	
    	Phrase payphrase1 = new Phrase("Billing Term", font8bold);
    	PdfPCell paytitle1 = new PdfPCell(payphrase1);
//    	paytitle1.addElement(payphrase);
    	paytitle1.setBorder(0);
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);

		try {
			if (this.payTermsLis.size() > 6) {
				table.setWidths(new float[] { 20, 27, 53 });
			}else {
				table.setWidths(new float[] { 10, 17, 73 });
			}
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}

		PdfPCell tablecell = new PdfPCell();
		if (this.payTermsLis.size() > 6) {
			tablecell.addElement(table);
		} else {
			tablecell.addElement(table);
			tablecell.setBorder(0);
		}

		Phrase paytermdays = new Phrase("Days", font8);
		Phrase paytermpercent = new Phrase("Percent", font8);
		Phrase paytermcomment = new Phrase("Comment", font8);

		PdfPCell celldays = new PdfPCell(paytermdays);
		celldays.setBorder(0);
		celldays.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell cellpercent = new PdfPCell(paytermpercent);
		cellpercent.setBorder(0);
		cellpercent.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell cellcomment = new PdfPCell(paytermcomment);
		cellcomment.setBorder(0);
		cellcomment.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(celldays);
		table.addCell(cellpercent);
		table.addCell(cellcomment);

		if (this.payTermsLis.size() <= 6) {
			int size = 6 - this.payTermsLis.size();
			blankLines = size * (80 / 6);
		} else {
			blankLines = 0;
		}
		table.setSpacingAfter(blankLines);

		for (int i = 0; i < this.payTermsLis.size(); i++) {
			Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",font8);
			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(df.format(payTermsLis.get(i).getPayTermPercent()) + "",font8);
			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			if (i == 6) {
				break;
			}

			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);
		}

		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);

		try {
			table1.setWidths(new float[] { 20, 27, 53 });
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		PdfPCell table1cell = new PdfPCell();
		Phrase blank = new Phrase();
		if (this.payTermsLis.size() > 6) {
			table1cell = new PdfPCell(table1);
		} else {
			table1cell = new PdfPCell(blank);
			table1cell.setBorder(0);
		}

		Phrase paytermdays1 = new Phrase("Days", font8);
		Phrase paytermpercent1 = new Phrase("Percent", font8);
		Phrase paytermcomment1 = new Phrase("Comment", font8);

		PdfPCell celldays1;
		PdfPCell cellpercent1;
		PdfPCell cellcomment1;

		if (this.payTermsLis.size() > 6) {
			celldays1 = new PdfPCell(paytermdays1);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			celldays1 = new PdfPCell(blank);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_LEFT);
		}

		if (this.payTermsLis.size() > 6) {
			cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			cellpercent1 = new PdfPCell(blank);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_LEFT);
		}

		if (this.payTermsLis.size() > 6) {
			cellcomment1 = new PdfPCell(paytermcomment1);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			cellcomment1 = new PdfPCell(blank);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_LEFT);
		}
		table1.addCell(celldays1);
		table1.addCell(cellpercent1);
		table1.addCell(cellcomment1);

		for (int i = 6; i < this.payTermsLis.size(); i++) {
			Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",font8);
			PdfPCell pdfdayscell1 = new PdfPCell(chunk);
			pdfdayscell1.setBorder(0);
			pdfdayscell1.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(df.format(payTermsLis.get(i).getPayTermPercent()) + "",font8);
			PdfPCell pdfpercentcell1 = new PdfPCell(chunk);
			pdfpercentcell1.setBorder(0);
			pdfpercentcell1.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
			PdfPCell pdfcommentcell1 = new PdfPCell(chunk);
			pdfcommentcell1.setBorder(0);
			pdfcommentcell1.setHorizontalAlignment(Element.ALIGN_LEFT);

			table1.addCell(pdfdayscell1);
			table1.addCell(pdfpercentcell1);
			table1.addCell(pdfcommentcell1);

			if (i == 12) {
				break;
			}
		}
		

		
		PdfPTable maitable ;
		
		if (this.payTermsLis.size() > 6) {
			maitable= new PdfPTable(2);
			maitable.setWidthPercentage(100);			
			PdfPCell maitabcell1 = new PdfPCell(tablecell);
			PdfPCell maitabcell2 = new PdfPCell(table1cell);
			maitable.addCell(maitabcell1);
			maitable.addCell(maitabcell2);
		}else {
			maitable= new PdfPTable(1);
			maitable.setWidthPercentage(100);
			PdfPCell maitabcell1 = new PdfPCell(tablecell);
			maitable.addCell(maitabcell1);			
		}

		

		PdfPCell mainCell = new PdfPCell(maitable);
		mainCell.setBorder(0);

		// end By jayshree

		Phrase descPhrase = null;
		if (qp.getPaymentTermsList() != null) {

			String titleterms = "Payment Terms";
			Phrase termsphrase = new Phrase(titleterms, font12boldul);
			termsphrase.add(Chunk.NEWLINE);
			PdfPCell termstitlecell = new PdfPCell();
			termstitlecell.addElement(termsphrase);
			termstitlecell.setBorder(0);
			PdfPTable termsTable = new PdfPTable(1);
			termsTable.addCell(termstitlecell);

			descPhrase = new Phrase();
		} else {
			descPhrase = new Phrase("  ", font7);
		}

		PdfPCell desccell = new PdfPCell();
		desccell.addElement(descPhrase);
		desccell.setBorder(0);

		PdfPTable termsTable = new PdfPTable(1);
		termsTable.setWidthPercentage(100);

		// ************rohan added this for valid until *****************
//		commented on 24-04-2023
//		if (qp instanceof Quotation) {
//			termsTable.addCell(validitydatecell);
//		}

		if (qp instanceof Quotation) {
			/** Manisha **/
			logger.log(Level.SEVERE,"QuotationPdf config");
			boolean configflag = false;
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Quotation")
					.filter("configStatus", true).first().now();

			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("RemovePaymentTermsFromQuotationPdf")&& processConfig.getProcessList().get(k).isStatus() == true) {
						configflag = true;
					} else {
						configflag = false;
					}
				}
			}

			if (configflag == true) {
				Phrase termphrase = new Phrase("");
				PdfPCell termCell = new PdfPCell(termphrase);
				termsTable.addCell(termCell);
			} else {
				if(paymentTerm!=null&&!paymentTerm.equals("")){
					termsTable.addCell(paytitle);
					if(creditDays!=0){
						termsTable.addCell(creditdaycell);
					}
				}else{
					if(creditDays!=0){
						termsTable.addCell(creditdaycell);
					}
					termsTable.addCell(paytitle1);
					termsTable.addCell(mainCell);
				}
				
			}

			termsTable.addCell(desccell);
		} else {
			
			if(paymentTerm!=null&&!paymentTerm.equals("")){
				termsTable.addCell(paytitle);
				if(creditDays!=0){
					termsTable.addCell(creditdaycell);
				}
			}else{
				if(creditDays!=0){
					termsTable.addCell(creditdaycell);
				}
				termsTable.addCell(paytitle1);
				termsTable.addCell(mainCell);
			}
			
//			termsTable.addCell(paytitle);
//			termsTable.addCell(creditdaycell);
			termsTable.addCell(desccell);
		}
		
      
		// End By jayshree
		//
		// termsTable.addCell(paytitle);
		// termsTable.addCell(mainTable);
		// termsTable.addCell(desccell);

		PdfPTable chargetaxtable = new PdfPTable(2);
		chargetaxtable.setWidthPercentage(100);
		chargetaxtable.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
		PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
		totalAmtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totalAmtCell.setBorder(0);
		double totalAmt = 0;
		totalAmt = qp.getTotalAmount();

		Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
		PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
		realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		realtotalAmtCell.setBorder(0);
		chargetaxtable.addCell(totalAmtCell);
		chargetaxtable.addCell(realtotalAmtCell);

		// Date 11-11-2017
		// By Jayshree
		// Changes are made to add the discount
		if (qp instanceof Contract) {
			boolean discFlag = false;

			if (contEntity.getDiscountAmt() != 0) {
				discFlag = true;
			}
			if (discFlag == true) {
				Phrase DiscPhrase = new Phrase("Discount   " + "", font1);
				PdfPCell DiscCell = new PdfPCell(DiscPhrase);
				DiscCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				DiscCell.setBorder(0);
				chargetaxtable.addCell(DiscCell);

				/**
				 * Date 19/12/017 By jayshree des.add the decimal format
				 */
				Phrase DiscValPhrase = new Phrase(df.format(contEntity
						.getDiscountAmt()) + "", font1);
				PdfPCell DiscValCell = new PdfPCell(DiscValPhrase);
				DiscValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				DiscValCell.setBorder(0);
				chargetaxtable.addCell(DiscValCell);

				Phrase totalAmtPh = new Phrase("Total Amount   " + "", font1);
				PdfPCell totalAmtcell2 = new PdfPCell(totalAmtPh);
				totalAmtcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
				totalAmtcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtcell2);

				/**
				 * Date 19/12/017 By jayshree des.add the decimal format
				 */
				Phrase totalAmtValPh = new Phrase(df.format(contEntity
						.getFinalTotalAmt()) + "", font1);
				PdfPCell totalAmtValcell2 = new PdfPCell(totalAmtValPh);
				totalAmtValcell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalAmtValcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtValcell2);

			}
		} else {
			boolean discFlag2 = false;

			if (quotEntity.getDiscountAmt() != 0) {
				discFlag2 = true;
			}
			if (discFlag2 == true) {
				Phrase DiscPhrase = new Phrase("Discount   " + "", font1);
				PdfPCell DiscCell = new PdfPCell(DiscPhrase);
				DiscCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				DiscCell.setBorder(0);
				chargetaxtable.addCell(DiscCell);

				/**
				 * Date 19/12/017 By jayshree des.add the decimal format
				 */
				Phrase DiscValPhrase = new Phrase(df.format(quotEntity
						.getDiscountAmt()) + "", font1);
				PdfPCell DiscValCell = new PdfPCell(DiscValPhrase);
				DiscValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				DiscValCell.setBorder(0);
				chargetaxtable.addCell(DiscValCell);

				Phrase totalAmtPh = new Phrase("Total Amount   " + "", font1);
				PdfPCell totalAmtcell2 = new PdfPCell(totalAmtPh);
				totalAmtcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
				totalAmtcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtcell2);

				/**
				 * Date 19/12/017 By jayshree des.add the decimal format
				 */
				Phrase totalAmtValPh = new Phrase(df.format(quotEntity
						.getFinalTotalAmt()) + "", font1);
				PdfPCell totalAmtValcell2 = new PdfPCell(totalAmtValPh);
				totalAmtValcell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalAmtValcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtValcell2);

			}
		}
		// End Changes by jayshree

		int firstBreakPoint = 6;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();
		System.out.println("Rohan cnt value===" + cnt);

		List<String> myList1 = new ArrayList<>();
		List<String> myList2 = new ArrayList<>();

		List<String> myList3 = new ArrayList<>();
		List<String> myList4 = new ArrayList<>();
		if (cnt > firstBreakPoint) {

			Phrase blank1 = null;

			System.out.println("Rohan prodTaxes value==="
					+ this.prodTaxes.size());
			for (int i = 0; i < this.prodTaxes.size(); i++) {
				PdfPCell pdfservicecell;
				PdfPCell pdftaxamtcell;
				double taxAmt = 0;
				double taxAmt1 = 0;
				double taxAmt2 = 0;
				double taxAmt3 = 0;
				double taxAmt4 = 0;

				// *************rohan 24/4/2015*************************
				if (prodTaxes.get(i).getChargePercent() != 0) {

					System.out.println("Rohan inside  Condition  "
							+ prodTaxes.get(i).getChargeName());
					if ((prodTaxes.get(i).getChargeName().equals("VAT"))) {
						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						myList1.add(name);
						myList2.add(df.format(taxAmt));
					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() != 12.36)
							&& (prodTaxes.get(i).getChargePercent() != 14.5)
							&& (prodTaxes.get(i).getChargePercent() != 15)) {

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						String str = " ";
						myList3.add(name);
						myList4.add(df.format(taxAmt1));
						myList4.add(str);
					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 12.36)) {
						System.out.println("Rohan in side 12.36");
						String name = "Service Tax " + "@ 12.00";
						taxAmt2 = (prodTaxes.get(i).getAssessableAmount() * 12) / 100;
						String name1 = "Education Cess " + "@ 2.00";
						taxAmt3 = (taxAmt2 * 2.00) / 100;
						String name2 = "High Education Cess " + "@ 1.00";
						taxAmt4 = (taxAmt2 * 1.00) / 100;
						String str = " ";

						myList3.add(name);
						myList3.add(name1);
						myList3.add(name2);

						myList4.add(df.format(taxAmt2));
						myList4.add(df.format(taxAmt3));
						myList4.add(str);
						myList4.add(df.format(taxAmt4));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 14.5)) {
						System.out.println("Rohan in side 14.5");
						String str = "Service Tax " + " @ " + " 14%";
						double serTaxAmt14 = 14 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						String str123 = "Swachha Bharat Cess" + " @ " + " 0.5%";
						double sbcTaxAmt = 0.5 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						myList3.add(str);
						myList3.add(str123);

						myList4.add(df.format(serTaxAmt14));
						myList4.add(df.format(sbcTaxAmt));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 15)) {
						System.out.println("Rohan in side 15"
								+ prodTaxes.get(i).getChargeName() + "-"
								+ prodTaxes.get(i).getChargePercent());
						String str = "Service Tax " + " @ " + " 14%";
						double serTaxAmt14 = 14 * prodTaxes.get(i)
								.getAssessableAmount() / 100;
						String blidnk = " ";

						String str123 = "Swachha Bharat Cess" + " @ " + " 0.5%";
						double sbcTaxAmt = 0.5 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						String kkc = "Krishi Kalyan Cess" + " @ " + " 0.5%";
						double kkctaxAmt = 0.5 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						myList3.add(str);
						myList3.add(str123);
						myList3.add(kkc);

						myList4.add(df.format(serTaxAmt14));
						myList4.add(df.format(sbcTaxAmt));
						myList4.add(blidnk);
						myList4.add(df.format(kkctaxAmt));
					}

					// Date 04-07-2017 added by vijay for GST
					System.out.println("Rohan GST =="
							+ prodTaxes.get(i).getChargeName());
					if (prodTaxes.get(i).getChargeName()
							.equalsIgnoreCase("CGST")
							|| prodTaxes.get(i).getChargeName()
									.equalsIgnoreCase("SGST")
							|| prodTaxes.get(i).getChargeName()
									.equalsIgnoreCase("IGST")) {

						System.out.println("GST ==");
						System.out.println("2nd loop == "
								+ prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent());

						String str = prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent();
						double rohanTaxValue = prodTaxes.get(i)
								.getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						myList3.add(str);
						myList4.add(df.format(rohanTaxValue));

						System.out.println("Size of mylist2 is () === "
								+ myList2.size());
					}
					// ends here

					// //*****************************************************
				} else {
					pdfservicecell = new PdfPCell(blank1);
					pdfservicecell.setBorder(0);
					pdftaxamtcell = new PdfPCell(blank1);
					pdftaxamtcell.setBorder(0);
				}

				// / rohan added this code
				if (i + 1 == 4) {
					break;
				}

			}

			PdfPCell pdfservicecell = null;

			PdfPTable other1table = new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for (int j = 0; j < myList1.size(); j++) {
				chunk = new Phrase(myList1.get(j), font8);
				pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);

			}

			PdfPCell pdfservicecell1 = null;

			PdfPTable other2table = new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for (int j = 0; j < myList2.size(); j++) {
				chunk = new Phrase(myList2.get(j), font8);
				pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);

			}

			PdfPCell chargescell = null;
			for (int j = 0; j < myList3.size(); j++) {
				chunk = new Phrase(myList3.get(j), font8);
				chargescell = new PdfPCell(chunk);
				chargescell.setBorder(0);
				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(chargescell);

			}

			PdfPCell chargescell1 = null;
			for (int j = 0; j < myList4.size(); j++) {
				chunk = new Phrase(myList4.get(j), font8);
				chargescell1 = new PdfPCell(chunk);
				chargescell1.setBorder(0);
				chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(chargescell1);

			}

			PdfPCell othercell = new PdfPCell(other1table);
			// othercell.addElement(other1table);//By jayshree comment this
			// 7/12/2017
			othercell.setBorder(0);
			chargetaxtable.addCell(othercell);

			PdfPCell othercell1 = new PdfPCell(other2table);
			// othercell1.addElement(other2table);//By jayshree comment this
			// 7/12/2017
			othercell1.setBorder(0);
			chargetaxtable.addCell(othercell1);

			PdfPCell chargecell = null;
			PdfPCell chargeamtcell = null;
			PdfPCell otherchargecell = null;
			for (int i = 0; i < this.prodCharges.size(); i++) {
				Phrase chunk = null;
				double chargeAmt = 0;
				PdfPCell pdfchargeamtcell = null;
				PdfPCell pdfchargecell = null;
				if (prodCharges.get(i).getChargePercent() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName()
							+ " @ " + prodCharges.get(i).getChargePercent(),
							font1);
					pdfchargecell = new PdfPCell(chunk);
					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargePercent()
							* prodCharges.get(i).getAssessableAmount() / 100;
					chunk = new Phrase(df.format(chargeAmt), font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);

				}
				if (prodCharges.get(i).getChargeAbsValue() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
							font1);
					pdfchargecell = new PdfPCell(chunk);

					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargeAbsValue();
					chunk = new Phrase(chargeAmt + "", font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
				}
				pdfchargecell.setBorder(0);
				total = total + chargeAmt;

				if (total != 0) {
					chunk = new Phrase("Other charges total", font1);
					chargecell = new PdfPCell(chunk);
					chargecell.setBorder(0);
					chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

					chunk = new Phrase(df.format(total), font8);
					chargeamtcell = new PdfPCell(chunk);
					chargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					chargeamtcell.setBorder(0);
				} else {

				}

				chunk = new Phrase("Refer other charge details", font1);
				otherchargecell = new PdfPCell(chunk);
				otherchargecell.setBorder(0);
				otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			}
			if (total != 0) {
				chargetaxtable.addCell(chargecell);
				chargetaxtable.addCell(chargeamtcell);
				chargetaxtable.addCell(otherchargecell);
			}

		} else if (cnt <= firstBreakPoint) {

			Phrase blank1 = null;
			for (int i = 0; i < this.prodTaxes.size(); i++) {
				PdfPCell pdfservicecell;
				PdfPCell pdftaxamtcell;
				double taxAmt = 0;
				double taxAmt1 = 0;
				double taxAmt2 = 0;
				double taxAmt3 = 0;
				double taxAmt4 = 0;
				PdfPCell pdftaxamt1cell = null;
				PdfPCell pdftaxamt2cell = null;
				PdfPCell pdftaxamt3cell = null;

				// *************rohan 24/4/2015*************************

				if (prodTaxes.get(i).getChargePercent() != 0) {

					if ((prodTaxes.get(i).getChargeName().equals("VAT"))) {

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						myList1.add(name);
						if(PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
							double taxAmount = serverAppUtility.getLastTwoDecimalOnly(taxAmt);
							myList2.add(taxAmount+"");
						}
						else{
							myList2.add(df.format(taxAmt));
						}

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() != 12.36))

					{

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;

						myList1.add(name);
						
						//Ashwini Patil added roundoff condition
						if(PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
							double taxAmount = serverAppUtility.getLastTwoDecimalOnly(taxAmt1);
							myList2.add(taxAmount+"");
						}
						else{
							myList2.add(df.format(taxAmt1));
						}

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 12.36)) {

						String name = "Service Tax " + "@ 12.00";
						taxAmt2 = (prodTaxes.get(i).getAssessableAmount() * 12) / 100;
						String name1 = "Education Cess " + "@ 2.00";
						taxAmt3 = (taxAmt2 * 2.00) / 100;
						String name2 = "High Education Cess " + "@ 1.00";
						taxAmt4 = (taxAmt2 * 1.00) / 100;
						String str = " ";
						myList1.add(name);
						myList1.add(name1);
						myList1.add(name2);

						//Ashwini Patil Date:25-02-2022 added roundoff condition
						if(PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
							double taxAmount = serverAppUtility.getLastTwoDecimalOnly(taxAmt1);
							myList2.add(df.format(serverAppUtility.getLastTwoDecimalOnly(taxAmt2)));
							myList2.add(df.format(serverAppUtility.getLastTwoDecimalOnly(taxAmt3)));
							myList2.add(str);
							myList2.add(df.format(serverAppUtility.getLastTwoDecimalOnly(taxAmt4)));
						}
						else{
							myList2.add(df.format(taxAmt2));
							myList2.add(df.format(taxAmt3));
							myList2.add(str);
							myList2.add(df.format(taxAmt4));
						}
						

					}

					// Date 04-07-2017 added by vijay for GST
					System.out.println("Rohan GST =="
							+ prodTaxes.get(i).getChargeName());
					if (prodTaxes.get(i).getChargeName()
							.equalsIgnoreCase("CGST")
							|| prodTaxes.get(i).getChargeName()
									.equalsIgnoreCase("SGST")
							|| prodTaxes.get(i).getChargeName()
									.equalsIgnoreCase("IGST")) {

						System.out.println("GST ==");
						System.out.println("2nd loop == "
								+ prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent());

						String str = prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent();
						double rohanTaxValue = prodTaxes.get(i)
								.getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						myList1.add(str);
						
						//Ashwini Patil Date:25-02-2022  added roundoff condition
						if(PC_NO_ROUNDOFF_INVOICE_CONTRACTFlag){
							myList2.add(df.format(serverAppUtility.getLastTwoDecimalOnly(rohanTaxValue)));
						}else
							myList2.add(df.format(rohanTaxValue));

						System.out.println("Size of mylist2 is () === "
								+ myList2.size());
					}
					// ends here

					// *****************************************************
				}
				// else{
				// pdfservicecell = new PdfPCell(blank1);
				// pdfservicecell.setBorder(0);
				// pdftaxamtcell = new PdfPCell(blank1);
				// pdftaxamtcell.setBorder(0);
				// }
			}

			PdfPCell pdfservicecell = null;

			PdfPTable other1table = new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for (int j = 0; j < myList1.size(); j++) {
				chunk = new Phrase(myList1.get(j), font8);
				pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);

			}

			PdfPCell pdfservicecell1 = null;

			PdfPTable other2table = new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for (int j = 0; j < myList2.size(); j++) {
				chunk = new Phrase(myList2.get(j), font8);
				pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);

			}

			PdfPCell othercell = new PdfPCell(other1table);
			// othercell.addElement(other1table);//By jayshree comment this
			// 7/12/2017
			othercell.setBorder(0);
			chargetaxtable.addCell(othercell);

			PdfPCell othercell1 = new PdfPCell(other2table);
			// othercell1.addElement(other2table);//By jayshree comment this
			// 7/12/2017
			othercell1.setBorder(0);
			chargetaxtable.addCell(othercell1);

			for (int i = 0; i < this.prodCharges.size(); i++) {
				Phrase chunk = null;
				double chargeAmt = 0;
				PdfPCell pdfchargeamtcell = null;
				PdfPCell pdfchargecell = null;
				if (prodCharges.get(i).getChargePercent() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName()
							+ " @ " + prodCharges.get(i).getChargePercent(),
							font1);
					pdfchargecell = new PdfPCell(chunk);
					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargePercent()
							* prodCharges.get(i).getAssessableAmount() / 100;
					chunk = new Phrase(df.format(chargeAmt), font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
					chargetaxtable.addCell(pdfchargecell);
					chargetaxtable.addCell(pdfchargeamtcell);

				}
				if (prodCharges.get(i).getChargeAbsValue() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
							font1);
					pdfchargecell = new PdfPCell(chunk);

					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargeAbsValue();
					chunk = new Phrase(chargeAmt + "", font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
					chargetaxtable.addCell(pdfchargecell);
					chargetaxtable.addCell(pdfchargeamtcell);
				}

			}
		}

		/**
		 * Date 16/11/2017 By Jayshree To add the round of in the next page
		 */

		if (qp instanceof Contract) {
			boolean rountoff = false;
			if (contEntity.getRoundOffAmt() != 0) {
				rountoff = true;
			}
			if (rountoff == true) {

				Phrase totalAmtPh = new Phrase("Total Amount   " + "", font1);
				PdfPCell totalAmtcell2 = new PdfPCell(totalAmtPh);
				totalAmtcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
				totalAmtcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtcell2);

				Phrase totalAmtValPh = new Phrase(df.format(contEntity
						.getGrandTotalAmount()) + "", font1);
				PdfPCell totalAmtValcell2 = new PdfPCell(totalAmtValPh);
				totalAmtValcell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalAmtValcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtValcell2);
				
				Phrase rountoffPhrase = new Phrase("Round Off   " + "", font1);
				PdfPCell rountoffCell = new PdfPCell(rountoffPhrase);
				rountoffCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				rountoffCell.setBorder(0);
				chargetaxtable.addCell(rountoffCell);

				Phrase roundoffValPhrase = new Phrase(df.format(contEntity
						.getRoundOffAmt()) + "", font1);
				PdfPCell roundoffValCell = new PdfPCell(roundoffValPhrase);
				roundoffValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				roundoffValCell.setBorder(0);
				chargetaxtable.addCell(roundoffValCell);

			}
		} else {
			boolean rountoff2 = false;
			if (quotEntity.getRoundOffAmt() != 0) {
				rountoff2 = true;
			}
			if (rountoff2 == true) {

				Phrase totalAmtPh = new Phrase("Total Amount   " + "", font1);
				PdfPCell totalAmtcell2 = new PdfPCell(totalAmtPh);
				totalAmtcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
				totalAmtcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtcell2);

				Phrase totalAmtValPh = new Phrase(df.format(quotEntity
						.getGrandTotalAmount()) + "", font1);
				PdfPCell totalAmtValcell2 = new PdfPCell(totalAmtValPh);
				totalAmtValcell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalAmtValcell2.setBorder(0);
				chargetaxtable.addCell(totalAmtValcell2);
				
				
				Phrase rountoffPhrase = new Phrase("Round Off   " + "", font1);
				PdfPCell rountoffCell = new PdfPCell(rountoffPhrase);
				rountoffCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				rountoffCell.setBorder(0);
				chargetaxtable.addCell(rountoffCell);

				Phrase roundoffValPhrase = new Phrase(df.format(quotEntity
						.getRoundOffAmt()) + "", font1);
				PdfPCell roundoffValCell = new PdfPCell(roundoffValPhrase);
				roundoffValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				roundoffValCell.setBorder(0);
				chargetaxtable.addCell(roundoffValCell);

			}
		}
		// End By jayshree

		/****************************************************************************************/

		PdfPTable netpaytable = new PdfPTable(2);
		netpaytable.setWidthPercentage(100);
		// netpaytable.setHorizontalAlignment(Element.ALIGN_BOTTOM);//Comment
		// this by jayshree

		for (int i = 0; i < this.prodCharges.size(); i++) {
			System.out
					.println("======I  " + i + "   size" + prodCharges.size());
			if (i == prodCharges.size() - 1) {
				chunk = new Phrase("Net Payable", font8bold);//font1
				PdfPCell pdfnetpaycell = new PdfPCell(chunk);
				pdfnetpaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
				pdfnetpaycell.setVerticalAlignment(Element.ALIGN_CENTER);
				pdfnetpaycell.setBorder(0);
				Double netPayAmt = (double) 0;

				netPayAmt = ((Quotation) qp).getNetpayable();
				var = (int) netPayAmt.doubleValue();
				chunk = new Phrase(var + "", font8bold);//font8
				
				Phrase chunknetpay;
				/**
				 * @author Vijay Date :- 18-01-2021 
				 * Des :- if below flag is active then round off not required for Innovative client done through process config
				 */
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT, comp.getCompanyId())){

					if(thaiPdfFlag){

						chunk = new Phrase(df.format(netPayAmt) + "", font8);
					}
					
					else
//						chunk = new Phrase(netPayAmt + "", font8);
						chunk = new Phrase(df.format(netPayAmt) + "", font8bold); //Ashwini Patil Date:6-05-2022 As per requirement of Ankita Pest control updating netPay amount format
					
				}
				else{


					if(thaiPdfFlag){
						chunk = new Phrase(df.format(var) + "", font8);
					}
						
					else
						chunk = new Phrase(var + "", font8bold);	
					
				}
			
				PdfPCell pdfnetpayamt = new PdfPCell(chunk);
				pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfnetpayamt.setVerticalAlignment(Element.ALIGN_CENTER);
				pdfnetpayamt.setBorder(0);

				netpaytable.addCell(pdfnetpaycell);
				netpaytable.addCell(pdfnetpayamt);
			}
		}

		if (this.prodCharges.size() == 0) {
			Double netPayAmt = (double) 0;
			Phrase chunknetpaytitle = new Phrase("Net Payable", font8bold);//font1
			PdfPCell netpaytitlecell = new PdfPCell(chunknetpaytitle);
			netpaytitlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			netpaytitlecell.setVerticalAlignment(Element.ALIGN_CENTER);
			netpaytitlecell.setBorder(0);
			netPayAmt = ((Quotation) qp).getNetpayable();
			var = (int) netPayAmt.doubleValue();

			Phrase chunknetpay;
			/**
			 * @author Vijay Date :- 18-01-2021 
			 * Des :- if below flag is active then round off not required for Innovative client done through process config
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT, comp.getCompanyId())){

				if(thaiPdfFlag){

					chunknetpay = new Phrase(df.format(netPayAmt) + "", font8);
				}
				else
					chunknetpay =  new Phrase(df.format(netPayAmt) + "", font8bold);	//Ashwini Patil Date:6-05-2022 As per requirement of Ankita Pest control updating netPay amount format	
			}
			else{

				if(thaiPdfFlag){
					chunknetpay = new Phrase(df.format(var) + "", font8);

				}
				chunknetpay = new Phrase(var + "", font8bold);
			}

//			Phrase chunknetpay = new Phrase(var + "", font8);
			PdfPCell pdfnetpayamt = new PdfPCell(chunknetpay);
			pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			pdfnetpayamt.setVerticalAlignment(Element.ALIGN_CENTER);
			pdfnetpayamt.setBorder(0);
			// chargetaxtable.addCell(netpaytitlecell);
			// chargetaxtable.addCell(pdfnetpayamt);

			netpaytable.addCell(netpaytitlecell);
			netpaytable.addCell(pdfnetpayamt);
		}

		PdfPTable taxinfotable = new PdfPTable(1);
		taxinfotable.setWidthPercentage(100);
		taxinfotable.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxinfotable.addCell(chargetaxtable);

		PdfPTable parenttaxtable = new PdfPTable(2);
		parenttaxtable.setWidthPercentage(100);
		try {
			parenttaxtable.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell termsdatacell = new PdfPCell(termsTable);
		PdfPCell taxdatacell = new PdfPCell(chargetaxtable);

		// termsdatacell.addElement(termsTable);//comment this by jayshree
		// 7/12/2017
		// taxdatacell.addElement(chargetaxtable);//comment this by jayshree
		// 7/12/2017
		if(quotEntity!=null && qp instanceof Quotation){
			if(quotEntity!=null && quotEntity.isDonotPrintTotal()){
				termsdatacell.setColspan(2);
			}
		}
		parenttaxtable.addCell(termsdatacell);
		termsdatacell.setBorder(0);
		
		if(quotEntity!=null && qp instanceof Quotation){
			if(quotEntity!=null && quotEntity.isDonotPrintTotal()==false){
				parenttaxtable.addCell(taxdatacell);
			}
		}else{
			parenttaxtable.addCell(taxdatacell);
		}
		

		try {

			document.add(parenttaxtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// *****************for amt in
		// words****************************************

		double netPayInFigures = 0;

		String amtInWords = "";

		System.out.println("amt" + netPayInFigures);
		Phrase amtwords = new Phrase(amtInWords, font1);
//		Phrase netPay1 = new Phrase("Amount: " + ServiceInvoicePdf.convert(np)
//				+ " Only", font8);
		
		
		logger.log(Level.SEVERE, "Amount "+np);

		/**
		 * @author Vijay Date :- 29-09-2021
		 * Des :- if below process config is active then amount will print in hundread structure  
		 */
		logger.log(Level.SEVERE, "AmountInWordsHundreadFormatFlag"+AmountInWordsHundreadFormatFlag);
//		if(AmountInWordsHundreadFormatFlag){
//
//			amtInWords = EnglishNumberToWords.convert(np);
//		}
//		else{
//			amtInWords = ServiceInvoicePdf.convert(np);
//		}
		//Ashwini Patil Date=15-04-2022 Client reported amount in words is not printing decimal part so new method is written for that and making call to that method from here
		double netpayvalue=((Quotation) qp).getNetpayable();
		amtInWords=pdfUtility.getAmountInWords(netpayvalue, qp.getCompanyId(),cust);
		/**
		 * ends here
		 */
		logger.log(Level.SEVERE, "amtInWords"+amtInWords);
		Phrase netPay1;
		//Ashwini Patil to print amount in words in thai for Innovative client
		if(thaiPdfFlag){			
			double npd=((Quotation) qp).getNetpayable();
			amtInWords = bahtText.getBath(df.format(npd));
			netPay1 = new Phrase(amtInWords,font8);
		}
		else{		
			netPay1 = new Phrase("Amount: " + amtInWords , font7bold);//font8
		}
		System.out.println("amtInWords="+amtInWords);
		
		PdfPCell amtWordsCell = new PdfPCell();
		//amtWordsCell.addElement(amtwords); //commented by ashwini patil
		amtWordsCell.addElement(netPay1);
		amtWordsCell.setPaddingBottom(5f);
		amtWordsCell.setPaddingTop(2f);
		amtWordsCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// amtWordsCell.setFixedHeight(15);
		amtWordsCell.setBorder(0);

		PdfPTable amonttable = new PdfPTable(1);
		amonttable.setWidthPercentage(100);
		amonttable.addCell(amtWordsCell);
		// amonttable.setSpacingAfter(5f);//Comment this code to remove the
		// space by jayshree

		PdfPTable parenttaxtable1 = new PdfPTable(2);
		parenttaxtable1.setWidthPercentage(100);
		try {
			parenttaxtable1.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell termsdatacell2 = new PdfPCell(amonttable);
		PdfPCell taxdatacell2 = new PdfPCell(netpaytable);

		// termsdatacell2.addElement(amonttable);//Comment this by jayshree Date
		// 7/12/2017
		// taxdatacell2.addElement(netpaytable);//Comment this by
		// jayshree7/12/2017
		
		
//		parenttaxtable1.addCell(termsdatacell2);
//		parenttaxtable1.addCell(taxdatacell2);

		
		
		if(quotEntity!=null && qp instanceof Quotation  && quotEntity.isDonotPrintTotal()){
			
//			parenttaxtable1.addCell(pdfUtility.getPdfCell("Additional taxes as applicable", font7bold, Element.ALIGN_CENTER,Element.ALIGN_CENTER, 0, 2, 0,0,1,1,1,1,5,5,0,0));
		}
		else{
			parenttaxtable1.addCell(termsdatacell2);
			parenttaxtable1.addCell(taxdatacell2);
		}
		
		try {
			/**
			 * @author Anil
			 * @since 14-07-2020
			 */
			if(qp instanceof Quotation){
//				if(quotEntity!=null && quotEntity.isDonotPrintTotal()==false){
					document.add(parenttaxtable1);
//				}
			}else{
				document.add(parenttaxtable1);
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// rohan added this code for Adding pre mises details and treatment area

		PdfPTable premisesTable = new PdfPTable(1);
		premisesTable.setWidthPercentage(100f);

		Phrase premisesHeadingPh = new Phrase("Premise Details :", font8bold);
		PdfPCell premisesHeadingCell = new PdfPCell(premisesHeadingPh);
		premisesHeadingCell.setBorder(0);
		premisesTable.addCell(premisesHeadingCell);
		// // by Jayshree service detail comment
		// PdfPTable TreatmentDetailsTable = new PdfPTable(1);
		// TreatmentDetailsTable.setWidthPercentage(100f);
		//
		// Phrase treatementHeadingPh = new
		// Phrase("Service Details:",font8bold);
		// PdfPCell treatementHeadingCell = new PdfPCell(treatementHeadingPh);
		// treatementHeadingCell.setBorder(0);
		// TreatmentDetailsTable.addCell(treatementHeadingCell);

		// for(int i=0;i<this.products.size();i++)
		// {
		// /**
		// * rohan added code here for premises details
		// * Date : 27/01/2017
		// */
		// /**
		// * Comment by jayshree to remove the premises
		// *
		// */
		// // String premises ="";//2
		// // if(products.get(i).getPremisesDetails()!= null &&
		// !products.get(i).getPremisesDetails().equals("")){
		// //
		// premises="Premise For "+products.get(i).getProductName()+" : "+products.get(i).getPremisesDetails();
		// // }
		// // else
		// // {
		// //
		// premises="Premise For "+products.get(i).getProductName()+" : "+"N A";
		// // }
		// //
		// // Phrase premisesPh = new Phrase(premises,font8);
		// // PdfPCell premisesCell = new PdfPCell(premisesPh);
		// // premisesCell.setBorder(0);
		// // premisesTable.addCell(premisesCell);
		//
		// /**
		// * ends here
		// */
		//
		// /**
		// * rohan added code here for premises details
		// * Date : 27/01/2017
		// */
		//
		// /**
		// * Comment by jayshree to remove the premises
		// *
		// */
		//
		// // String treatementDess ="";
		// //
		// // if(products.get(i).getPrduct().getProductType()!= null &&
		// !products.get(i).getPrduct().getProductType().equals("")){
		// //
		// treatementDess="Service "+products.get(i).getProductName()+" : "+products.get(i).getPrduct().getProductType();
		// // }
		// // else
		// // {
		// //
		// treatementDess="Service "+products.get(i).getProductName()+" : "+"N A";
		// // }
		// //
		// // Phrase treatementDessPh = new Phrase(treatementDess,font8);
		// // PdfPCell treatementDessCell = new PdfPCell(treatementDessPh);
		// // treatementDessCell.setBorder(0);
		// // TreatmentDetailsTable.addCell(treatementDessCell);
		// //
		// /**
		// * ends here
		// */
		//
		// if(i+1==3)
		// {
		// break;
		// }
		//
		// }

		/**
		 * Jayshree Commented this
		 */
		// PdfPCell premisescell=new PdfPCell();
		// premisescell.addElement(premisesTable);
		// //
		// PdfPCell descTableCell=new PdfPCell();
		// // descTableCell.addElement(TreatmentDetailsTable);//By Jayshree

		PdfPTable myparentTable = new PdfPTable(1);
		myparentTable.setWidthPercentage(100f);
		// myparentTable.addCell(premisescell);
		// myparentTable.addCell(descTableCell);
		/**
		 * Ends
		 */

		// this code is for terms and conditions
		/** date 05-02-2018 commented by komal for to hide hard code terms and conditions **/
//		if (onlyForFriendsPestControl) {
//			String friends = "1.If you are not satisfied with the treatment within 15 days of treatment, free treatment will be provided. \n"
//					+ "2.You will receive two reminders for each of your treatments by call, email and by SMS. Our obligation limits to reminders only. \n"
//					+ "3.It is essential to avail the treatment within the due dates to validate the warranty. \n"
//					+ "4.Contract period cannot be extended for any reason. \n"
//					+ "5.Once the due date is over the treatment cannot be carry forwarded to extend the contract period. \n"
//					+ "6.It is mandatory to avail the treatment within contract period.Otherwise the treatment will be considered as lapsed.\n"
//					+ "THEREFOR PLEASE INSURE THE SCHEDULE MENTION HERE IS STRICTLY FOLLOWED";
//
//			PdfPTable termsConditionsTable = new PdfPTable(1);
//			termsConditionsTable.setWidthPercentage(100f);
//
//			Phrase terms = new Phrase("Terms and Conditions :", font8bold);
//			PdfPCell termsCell = new PdfPCell(terms);
//			termsCell.setBorder(0);
//			termsConditionsTable.addCell(termsCell);
//
//			Phrase termsConditionPh = new Phrase(friends, font8);
//			PdfPCell termsConditioncell = new PdfPCell(termsConditionPh);
//			termsConditioncell.setBorder(0);
//			termsConditionsTable.addCell(termsConditioncell);
//
//			PdfPCell termsConditionsTableCell = new PdfPCell(
//					termsConditionsTable);
//			myparentTable.addCell(termsConditionsTableCell);
//
//		} else {

			String termsConditions = "";
			PdfPTable termsConditionsTable = new PdfPTable(1);
			termsConditionsTable.setWidthPercentage(100f);

			if (qp instanceof Contract) {
				termsConditions = qp.getDescription();
			} else {
				termsConditions = qp.getDescription();
			}
			
			/**Added by Sheetal : 31-03-2022
			   Des : If QR Code is there it will print along with terms and conditions
			         and if QR code is not there terms and conditions will print as it is**/
			if(comppayment!=null && comppayment.getQrCodeDocument()!=null && comppayment.getQrCodeDocument().getUrl()!=null && !comppayment.getQrCodeDocument().getUrl().equals("")){
				DocumentUpload digitalDocument = comppayment.getQrCodeDocument();
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}
				PdfPCell qrCodeCell = null;
				Image image2 = null;
			    logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
				try {
					logger.log(Level.SEVERE, "comppayment.getQrCodeDocument() "+comppayment.getQrCodeDocument());

					image2 = Image.getInstance(new URL(hostUrl
							+ digitalDocument.getUrl()));
//					image2=Image.getInstance("images/qrcode.jpg");
					image2.scalePercent(35f);
					image2.scaleAbsoluteWidth(100f);

					qrCodeCell = new PdfPCell(image2);
					qrCodeCell.setBorder(0);
					qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("QR code");
				float[] columnWidths = { 2f, 0.60f };
			
				Phrase terms = new Phrase("Remarks :", font8bold);//old label Terms and Conditions Ashwini Patil Date:24-04-2023
				PdfPCell termsCell = new PdfPCell(terms);
				termsCell.setBorder(0);
				termsCell.setBorderWidthBottom(0);
				termsCell.setBorderWidthTop(0);
				
				if(!termsConditions.equalsIgnoreCase("")) {
				termsConditionsTable.addCell(termsCell);//if not data then ne need to print label. Ashwini Patil Date:24-04-2023
				Phrase termsConditionPh = null;
				termsConditionPh = new Phrase(termsConditions, font8);
				PdfPCell termsConditioncell = new PdfPCell(termsConditionPh);
				termsConditioncell.setBorder(0);
				termsConditionsTable.addCell(termsConditioncell);
				}
				PdfPTable pdfTable = new PdfPTable(2);
				pdfTable.setWidthPercentage(100);
				try {
					pdfTable.setWidths(columnWidths);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				pdfTable.addCell(termsConditionsTable);
				
				Phrase phQrCodeTitle = new Phrase("Scan QR Code to Pay",font10bold);
				PdfPCell qrCodeTitleCell=new PdfPCell(phQrCodeTitle);
				qrCodeTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				qrCodeTitleCell.setBorder(0);
				qrCodeTitleCell.setPaddingBottom(4);
				
			    PdfPCell pdfPqrcodecell = new PdfPCell(qrCodeCell);
				pdfPqrcodecell.setBorder(0);
				
				PdfPTable pdfTable2 = new PdfPTable(1);
				pdfTable2.setWidthPercentage(100);
				pdfTable2.addCell(qrCodeTitleCell);
				pdfTable2.addCell(pdfPqrcodecell);

				pdfTable.addCell(pdfTable2);
				
				PdfPCell pdfPcell = new PdfPCell(pdfTable);
				pdfPcell.setBorder(0);
				pdfPcell.setFixedHeight(100f);
			
				myparentTable.setWidthPercentage(100);
				myparentTable.addCell(pdfPcell);
				try {
					document.add(myparentTable);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}else{
				if(!termsConditions.equalsIgnoreCase("")) {
				Phrase terms = new Phrase("Terms and Conditions :", font8bold);
				PdfPCell termsCell = new PdfPCell(terms);
				termsCell.setBorder(0);
				termsConditionsTable.addCell(termsCell);
				System.out.println("Terms and conditions");

				/**
				 * Date 4/12/2017 By jayshree To set the terms and condition
				 * size fix 500 char
				 */
				Phrase termsConditionPh = null;
				if (termsConditions.length() > 500) {
					System.out.println("to check the lenth more 500"
							+ termsConditions.substring(0, 500));
					termsConditionPh = new Phrase(termsConditions.substring(0,
							500), font8);
				} else {
					System.out.println("to check the lenth less 500"
							+ termsConditions);
					termsConditionPh = new Phrase(termsConditions, font8);
				}

				// End By Jayshree
				PdfPCell termsConditioncell = new PdfPCell(termsConditionPh);
				termsConditioncell.setBorder(0);
				termsConditionsTable.addCell(termsConditioncell);

				PdfPCell termsConditionsTableCell = new PdfPCell(
						termsConditionsTable);
				myparentTable.addCell(termsConditionsTableCell);
				try {
					document.add(myparentTable);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			}
	}/**end**/

//			if (!termsConditions.equalsIgnoreCase("")) {
//				Phrase terms = new Phrase("Terms and Conditions :", font8bold);
//				PdfPCell termsCell = new PdfPCell(terms);
//				termsCell.setBorder(0);
//				termsConditionsTable.addCell(termsCell);
//
//				/**
//				 * Date 4/12/2017 By jayshree To set the terms and condition
//				 * size fix 500 char
//				 */
//				Phrase termsConditionPh = null;
//				if (termsConditions.length() > 500) {
//					System.out.println("to check the lenth more 500"
//							+ termsConditions.substring(0, 500));
//					termsConditionPh = new Phrase(termsConditions.substring(0,
//							500), font8);
//				} else {
//					System.out.println("to check the lenth less 500"
//							+ termsConditions);
//					termsConditionPh = new Phrase(termsConditions, font8);
//				}
//
//				// End By Jayshree
//				PdfPCell termsConditioncell = new PdfPCell(termsConditionPh);
//				termsConditioncell.setBorder(0);
////				termsConditioncell.setFixedHeight(50);// Date 16/11/2017 By
//														// jayshree to add the
//														// cell height
//				termsConditionsTable.addCell(termsConditioncell);
//
//				PdfPCell termsConditionsTableCell = new PdfPCell(
//						termsConditionsTable);
//				myparentTable.addCell(termsConditionsTableCell);
////			}
//	//	}
//
//		try {
//			document.add(myparentTable);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

	// ////////////.................................................

	public void footerInfo(String preprint) {

		PdfPTable bankinfotable = new PdfPTable(1);
		bankinfotable.setWidthPercentage(100);
		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankc = new PdfPCell();
		blankc.addElement(blank);
		blankc.setBorder(0);
		System.out.println(" condition===============1");
		
		if (qp instanceof Contract) {

			Phrase confirmation = new Phrase("   I/We hereby confirm ", font8);
			PdfPCell confirmationcell = new PdfPCell();
			confirmationcell.addElement(confirmation);
			confirmationcell.setBorder(0);
			bankinfotable.addCell(confirmationcell);

			bankinfotable.addCell(blankc);
			bankinfotable.addCell(blankc);
			bankinfotable.addCell(blankc);
			//
			Phrase dottedline = new Phrase("---------------------------");
			PdfPCell dottedlinecell = new PdfPCell();
			dottedlinecell.addElement(dottedline);
			dottedlinecell.setBorder(0);
			bankinfotable.addCell(dottedlinecell);

			Phrase custsignphrase = new Phrase("Customer Signature", font8);
			PdfPCell custsigncell = new PdfPCell();
			custsigncell.addElement(custsignphrase);
			custsigncell.setBorder(0);
			bankinfotable.addCell(custsigncell);

		} else {

			/**
			 * Date 5/12/2017 By Jayshree Des.to add the Company gst and state
			 * code
			 */

			PdfPTable gstintab = new PdfPTable(3);
			gstintab.setWidthPercentage(100);

			try {
				gstintab.setWidths(new float[] { 28, 5, 67 });
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/**
			 * Date 23-11-2018 By VIjay 
			 * for GST Number not print with process Config 
			 */
			PdfPTable Statetab = new PdfPTable(4);
			Statetab.setWidthPercentage(100);
			
			
			if(gstNumberPrintFlag){
				
				Phrase gstinph = new Phrase("GSTIN", font8);
				PdfPCell gstinCell = new PdfPCell(gstinph);
				gstinCell.setBorder(0);
				gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
	
				Phrase gstinblank = new Phrase(":", font8);
				PdfPCell gstinblankcell = new PdfPCell(gstinblank);
				gstinblankcell.setBorder(0);
				gstinblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
				ServerAppUtility serverApp = new ServerAppUtility();
				String gstin = "", gstinText = "";
				
				if(branchDt!=null && branchDt.getGSTINNumber()!=null){
					gstinText=branchDt.getGSTINNumber();
					logger.log(Level.SEVERE," Quotation Branch gstin111:"+gstinText);
				}else {
					if(comp.getCompanyGSTTypeText() !=null && comp.getCompanyGSTType().equalsIgnoreCase("GST Applicable")){
						gstin=comp.getCompanyGSTTypeText();
						logger.log(Level.SEVERE," Quotation Company gstin333:"+gstin);
					}else if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
						gstin = serverApp.getGSTINOfCompany(comp, quotEntity.getBranch().trim());
						System.out.println(" QuotationCompany gstin" + gstin);
						logger.log(Level.SEVERE," QuotationCompany gstin111:"+gstin);
					} else {
						gstinText = comp.getCompanyGSTTypeText().trim();
						System.out.println("Company gstintext" + gstinText);
						logger.log(Level.SEVERE,"Quotation Company gstinText111:"+gstinText);
					}
				}
				if (quotEntity.productTaxes.size() == 0) {
					gstin = "";
				}
				Phrase gstinphval = null;
				if(!gstinText.equals("")){
					logger.log(Level.SEVERE,"Inside Quotation Branch GSTIN Value:"+gstinText);
					gstinphval=new Phrase(gstinText, font8);
				}else{
					gstinphval = new Phrase(gstin, font8);
					logger.log(Level.SEVERE,"Quotation Company GSTIN222:"+gstin);
				}
	
				PdfPCell gstincell = new PdfPCell(gstinphval);
				gstincell.setBorder(0);
				gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				if((gstinText!=null&&!gstinText.equals(""))||(gstin!=null&&!gstin.equals(""))){
					gstintab.addCell(gstinCell);
					gstintab.addCell(gstinblankcell);
					gstintab.addCell(gstincell);
				}else{
					gstintab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					gstintab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					gstintab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				}
				
	
				try {
					Statetab.setWidths(new float[] { 40, 5, 15, 40 });
				} catch (DocumentException e) {
					e.printStackTrace();
				}
	
				Phrase statecod = new Phrase("State Code", font8);
				PdfPCell statCell = new PdfPCell(statecod);
				statCell.setBorder(0);
				statCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
	
				Phrase stateblank = new Phrase(":", font8);
				PdfPCell stateblankCell = new PdfPCell(stateblank);
				stateblankCell.setBorder(0);
				stateblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				String stCo = "";
				if (stateList != null) {
					for (int i = 0; i < stateList.size(); i++) {
						if (stateList.get(i).getStateName().trim().equalsIgnoreCase(comp.getAddress().getState().trim())) {
							stCo = stateList.get(i).getStateCode().trim();
							break;
						}
					}
				}
				Phrase statecodVal = new Phrase(stCo, font8);
				PdfPCell statCellVal = new PdfPCell(statecodVal);
				statCellVal.setBorder(0);
				statCellVal.setHorizontalAlignment(Element.ALIGN_LEFT);
				
	
				Phrase statecodbl = new Phrase(" ");
				PdfPCell statCellblVal = new PdfPCell(statecodbl);
				statCellblVal.setBorder(0);
				statCellblVal.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				if((gstinText!=null&&!gstinText.equals(""))||(gstin!=null&&!gstin.equals(""))){
					
					if(stCo!=null&&!stCo.equals("")){
						Statetab.addCell(statCell);
						Statetab.addCell(stateblankCell);
						Statetab.addCell(statCellVal);
						Statetab.addCell(statCellblVal);
					}else{
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					}
				}
				
			
			}
			/**
			 * ends here
			 */


			System.out.println("else condition===============1");

			Phrase typename = null;
			Phrase typevalue = null;

			PdfPTable artictable = new PdfPTable(3);
			artictable.setWidthPercentage(100);
			try {
				artictable.setWidths(new float[] { 40, 5, 55 });
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			artictable.setHorizontalAlignment(Element.ALIGN_LEFT);
			System.out.println("b4 for loop of Articles type == "
					+ this.articletypecomp.size());

			//Ashwini Patil Date24-04-2023
			if (qp instanceof Quotation) {
	
				
				if (((Quotation) qp).getValidUntill() != null) {
					
					
					Phrase validityLabel = new Phrase("Quotation Valid Until",font8); //Ashwini Patil Date:21-06-2024 made Valid Until to Quotation valid until as per ultra request and nitin sir's permission
					PdfPCell validityLabelcell = new PdfPCell(validityLabel);
					validityLabelcell.setBorder(0);
					validityLabelcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					Phrase colonph = new Phrase(":", font8);
					PdfPCell colonCell = new PdfPCell(colonph);
					colonCell.setBorder(0);
					colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					Phrase validityDate = new Phrase(fmt.format(((Quotation) qp).getValidUntill())+"", font8);
					PdfPCell validityDatecell = new PdfPCell(validityDate);
					validityDatecell.setBorder(0);
					validityDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					artictable.addCell(validityLabelcell);
					artictable.addCell(colonCell);
					artictable.addCell(validityDatecell);
					
				}
				
			}
			
			/**
			 * @author Abhinav Bihade
			 * @since 05/02/2020
			 *As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
			 * in case if somebody manages 2 companies under same link of ERP s/w
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())&&branchWiseFilteredArticleList!=null && branchWiseFilteredArticleList.size()!=0){
					logger.log(Level.SEVERE,"Inside Branch As Company Printing1010:" +branchWiseFilteredArticleList);
					for(ArticleType artict :branchWiseFilteredArticleList){
						System.out.println("all docu name if out == "+ artict.getDocumentName());
						if (artict.getArticlePrint().equalsIgnoreCase("YES")&& artict.getDocumentName().equals("Quotation")) {

							System.out.println("all docu name if in == "+ artict.getDocumentName());

							typename = new Phrase(artict.getArticleTypeName(), font8bold);
							typevalue = new Phrase(artict.getArticleTypeValue(), font8);

							PdfPCell tymanecell = new PdfPCell(typename);
							tymanecell.setBorder(0);
							tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

							Phrase typeblank = new Phrase(":" ,font8);
							PdfPCell typeCell = new PdfPCell(typeblank);
							typeCell.setBorder(0);
							typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

							PdfPCell typevalcell = new PdfPCell(typevalue);
							typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
							typevalcell.setBorder(0);


							//Ashwini Patil Date:17-04-2024 Evrim reported that gst getting printed on non billing contract
							if(!gstNumberPrintFlag&&typename.equals("GSTIN")) {
								System.out.println("Dont print GST");
							}else {
								artictable.addCell(tymanecell);
								artictable.addCell(typeCell);
								artictable.addCell(typevalcell);
							}
						}
					}
				}
			}else{
				for (int i = 0; i < this.articletypecomp.size(); i++) {
					if (articletypecomp.get(i).getArticlePrint().equalsIgnoreCase("YES")
							&& articletypecomp.get(i).getDocumentName().equals("Quotation")) {
						typename = new Phrase(articletypecomp.get(i).getArticleTypeName(), font8bold);
						typevalue = new Phrase(articletypecomp.get(i).getArticleTypeValue(), font8);
	
						PdfPCell tymanecell = new PdfPCell(typename);
						tymanecell.setBorder(0);
						tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
						Phrase typeblank = new Phrase(":" ,font8);
						PdfPCell typeCell = new PdfPCell(typeblank);
						typeCell.setBorder(0);
						typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
						PdfPCell typevalcell = new PdfPCell(typevalue);
						typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						typevalcell.setBorder(0);
	
						//Ashwini Patil Date:17-04-2024 Evrim reported that gst getting printed on non billing contract
						if(!gstNumberPrintFlag&&typename.equals("GSTIN")) {
							System.out.println("Dont print GST");
						}else {
							artictable.addCell(tymanecell);
							artictable.addCell(typeCell);
							artictable.addCell(typevalcell);
						}
					}
				}
			}

			/**
			 * By jayshree add the cells in baninfoTable
			 */
			PdfPCell pdfcell = new PdfPCell(gstintab);
			pdfcell.setBorder(0);
			pdfcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell pdfcell2 = new PdfPCell(Statetab);
			pdfcell2.setBorder(0);
			pdfcell2.setHorizontalAlignment(Element.ALIGN_LEFT);

//			/**
//			 * Date 15/3/2018 By Jayshree remove gstin if tax not present
//			 */
			if (gstNumberPrintFlag) {
				bankinfotable.addCell(pdfcell);
				bankinfotable.addCell(pdfcell2);
			}
			PdfPCell pdfcell3 = new PdfPCell(artictable);
			pdfcell3.setBorder(0);
			pdfcell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			bankinfotable.addCell(pdfcell3);
			// End By Jayshree
			
		}
		

		/**
		 * Date 5/12/2017 By Jayshree Des.to add the Company gst and state code
		 */
		PdfPTable descTable = new PdfPTable(1);
		descTable.setWidthPercentage(100f);

		PdfPTable gstinhead = new PdfPTable(1);
		gstinhead.setWidthPercentage(100);

		PdfPTable bankDetailsTable = new PdfPTable(1);
		bankDetailsTable.setWidthPercentage(100f);

		if (qp instanceof Contract) {
			System.out.println("qp instanceof Contract");

			PdfPTable gstintab = new PdfPTable(3);
			gstintab.setWidthPercentage(100);

			try {
				gstintab.setWidths(new float[] { 4.5f, 1f, 4.5f });
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			/** Date 23-11-2018 By VIjay 
			 * for GST Number not print with process Config 
			 */
			
			/*****Date 5-2-2019 
			 * @author Amol added" hideGstinNo" this processconfiguration  for hide the GSTIN number when no tax is selected (tax is N.A)****/
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract", "HideGstinNo",qp.getCompanyId())){
				hideGstinNo=true;
			}
			PdfPTable Statetab = new PdfPTable(4);
			Statetab.setWidthPercentage(100);
			Phrase  gstinph;
			String gstin = "", gstinText = "";
			if(gstNumberPrintFlag){
				if(hideGstinNo && contEntity.getProductTaxes().size()==0){
					gstinph=new Phrase("");
					PdfPCell gstinCell = new PdfPCell(gstinph);
					gstinCell.setBorder(0);
					gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					gstintab.addCell(gstinCell);
					
					Phrase gstinblank = new Phrase("");
					PdfPCell gstinblankcell = new PdfPCell(gstinblank);
					gstinblankcell.setBorder(0);
					gstinblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					gstintab.addCell(gstinblankcell);
					
					Phrase gstinphval = new Phrase("");
					PdfPCell gstincell = new PdfPCell(gstinphval);
					gstincell.setBorder(0);
					gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
					gstintab.addCell(gstincell);

					
				}else{
			    	gstinph = new Phrase("GSTIN", font8bold);
					PdfPCell gstinCell = new PdfPCell(gstinph);
					gstinCell.setBorder(0);
					gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
					Phrase gstinblank = new Phrase(":", font8);
					PdfPCell gstinblankcell = new PdfPCell(gstinblank);
					gstinblankcell.setBorder(0);
					gstinblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
					ServerAppUtility serverApp = new ServerAppUtility();
				
					if(branchDt!=null && branchDt.getGSTINNumber()!=null){
						gstinText=branchDt.getGSTINNumber();
						logger.log(Level.SEVERE,"Branch gstin111:"+gstinText);
					}else {
						if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
							gstin = serverApp.getGSTINOfCompany(comp, contEntity.getBranch().trim());
							logger.log(Level.SEVERE, "GSTIN VALUE "+gstin);
						} else {
							gstinText = comp.getCompanyGSTTypeText().trim();
							System.out.println("Company gstintext" + gstinText);
						}
					}
					
					Phrase gstinphval = null;
					if(!gstinText.equals("")){
						logger.log(Level.SEVERE,"Inside Branch GSTIN Value:"+gstinText);
						gstinphval=new Phrase(gstinText, font8);
					}else{
						gstinphval = new Phrase(gstin, font8);
						logger.log(Level.SEVERE,"Company GSTIN222:"+gstin);
					}
					PdfPCell gstincell = new PdfPCell(gstinphval);
					gstincell.setBorder(0);
					gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					if((gstinText!=null&&!gstinText.equals(""))||(gstin!=null&&!gstin.equals(""))){
						gstintab.addCell(gstinCell);
						gstintab.addCell(gstinblankcell);
						gstintab.addCell(gstincell);
					}else{
						gstintab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						gstintab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						gstintab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					}
				}

				try {
					Statetab.setWidths(new float[] { 4.5f, 1f, 1.5f, 3.0f });
					Statetab.setSpacingAfter(0);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
	
				Phrase statecod = new Phrase("State Code ", font8bold);
				PdfPCell statCell = new PdfPCell(statecod);
				statCell.setBorder(0);
				statCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				Statetab.addCell(statCell);
	
				Phrase stateblank = new Phrase(":", font8);
				PdfPCell stateblankCell = new PdfPCell(stateblank);
				stateblankCell.setBorder(0);
				stateblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				Statetab.addCell(stateblankCell);
	
				String stCo = "";
				if (stateList != null) {
					for (int i = 0; i < stateList.size(); i++) {
						if (stateList.get(i).getStateName().trim().equalsIgnoreCase(comp.getAddress().getState().trim())) {
							stCo = stateList.get(i).getStateCode().trim();
							break;
						}
					}
				}
				Phrase statecodVal2 = new Phrase(stCo, font8);
				PdfPCell statCellVal2 = new PdfPCell(statecodVal2);
				statCellVal2.setBorder(0);// Comment by jayshree
				statCellVal2.setHorizontalAlignment(Element.ALIGN_LEFT);
//				Statetab.addCell(statCellVal2);
	
				Phrase stateblank1 = new Phrase(" ");
				PdfPCell stateblank1cell = new PdfPCell(stateblank1);
				stateblank1cell.setBorder(0);
//				Statetab.addCell(stateblank1cell);
				
				if((gstinText!=null&&!gstinText.equals(""))||(gstin!=null&&!gstin.equals(""))){
					
					if(stCo!=null&&!stCo.equals("")){
						Statetab.addCell(statCell);
						Statetab.addCell(stateblankCell);
						Statetab.addCell(statCellVal2);
						Statetab.addCell(stateblank1cell);
					}else{
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					}
				}else{
					Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					Statetab.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				}
			
			}
		

			boolean articleFlag=false;
			PdfPTable artictable = new PdfPTable(3);
			artictable.setWidthPercentage(100);
			try {
				artictable.setWidths(new float[] { 4.5f, 1f, 4.5f });
				artictable.setSpacingAfter(0);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			artictable.setHorizontalAlignment(Element.ALIGN_LEFT);
			System.out.println("b4 for loop of Articles type == "+ this.articletypecomp.size());
			
			
			/**
			 * @author Abhinav Bihade
			 * @since 05/02/2020
			 *As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
			 * in case if somebody manages 2 companies under same link of ERP s/w
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())&&branchWiseFilteredArticleList!=null && branchWiseFilteredArticleList.size()!=0){
					logger.log(Level.SEVERE,"Inside Branch As Company Printing3030:" +branchWiseFilteredArticleList.size()+" gstNumberPrintFlag="+gstNumberPrintFlag);
					for(ArticleType artict :branchWiseFilteredArticleList){
						if (artict.getArticlePrint().equalsIgnoreCase("YES")&& artict.getDocumentName().equals("Contract")) {

							System.out.println("all docu name if in == "+ artict.getDocumentName());

							Phrase typename = new Phrase(artict.getArticleTypeName(), font8bold);
							Phrase typevalue = new Phrase(artict.getArticleTypeValue(), font8);

							PdfPCell tymanecell = new PdfPCell(typename);
							tymanecell.setBorder(0);				
							tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

							Phrase typeblank = new Phrase(":" ,font8);
							PdfPCell typeCell = new PdfPCell(typeblank);
							typeCell.setBorder(0);
							typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

							PdfPCell typevalcell = new PdfPCell(typevalue);
							typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
							typevalcell.setBorder(0);


							//Ashwini Patil Date:17-04-2024 Evrim reported that gst getting printed on non billing contract
							if(!gstNumberPrintFlag&&typename.equals("GSTIN")) {
								System.out.println("do not print gst");
							}else {
							artictable.addCell(tymanecell);
							artictable.addCell(typeCell);
							artictable.addCell(typevalcell);
							}
							
							articleFlag=true;
						}
					}
			
				}
			}else{
				
				for (int i = 0; i < this.articletypecomp.size(); i++) {
					logger.log(Level.SEVERE,"Inside Branch As Company Printing4040:" +branchWiseFilteredArticleList);
	
					System.out.println("all docu name if out == "+ articletypecomp.get(i).getDocumentName());
					if (articletypecomp.get(i).getArticlePrint().equalsIgnoreCase("YES")&& articletypecomp.get(i).getDocumentName().equals("Contract")) {
	
						System.out.println("all docu name if in == "+ articletypecomp.get(i).getDocumentName());
	
						Phrase typename = new Phrase(articletypecomp.get(i).getArticleTypeName(), font8bold);
						Phrase typevalue = new Phrase(articletypecomp.get(i).getArticleTypeValue(), font8);
	
						PdfPCell tymanecell = new PdfPCell(typename);
						tymanecell.setBorder(0);				
						tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
						Phrase typeblank = new Phrase(":" ,font8);
						PdfPCell typeCell = new PdfPCell(typeblank);
						typeCell.setBorder(0);
						typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
						PdfPCell typevalcell = new PdfPCell(typevalue);
						typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						typevalcell.setBorder(0);
	
						if(!gstNumberPrintFlag&&typename.equals("GSTIN")) {
							System.out.println("do not print gst");
						}else {
						artictable.addCell(tymanecell);
						artictable.addCell(typeCell);
						artictable.addCell(typevalcell);
						}
						
						articleFlag=true;
					}
				}
			}
			
			PdfPCell gstinTabCell = new PdfPCell(gstintab);
			gstinTabCell.setBorder(0);

			PdfPCell stateTabCell = new PdfPCell(Statetab);
			stateTabCell.setBorder(0);
			if (gstNumberPrintFlag) {
				bankDetailsTable.addCell(gstinTabCell);
				bankDetailsTable.addCell(stateTabCell);
			}
			PdfPCell sacCodeCell = new PdfPCell(artictable);
			sacCodeCell.setBorderWidthLeft(0);
			sacCodeCell.setBorderWidthRight(0);
			sacCodeCell.setBorderWidthTop(0);
			
			if(!articleFlag){
				sacCodeCell.setBorderWidthBottom(0);
			}
			bankDetailsTable.addCell(sacCodeCell);
		}

		// End By Jayshree
		if (!doNotPrintBankDetails) {
			if (comppayment != null) {
//
//				Phrase heading = new Phrase("Bank Details", font8bold);
//				PdfPCell headingCell = new PdfPCell(heading);
//				headingCell.setBorder(0);
//				bankDetailsTable.addCell(headingCell);
//				
//				String favourOf = "";
//				if (comppayment.getPaymentFavouring() != null&& !comppayment.getPaymentFavouring().equals("")) {
//					favourOf = "Cheque should be in favour of '"+ comppayment.getPaymentFavouring() + "'";
//				}
//				Phrase favouring = new Phrase(favourOf, font8bold);
//				PdfPCell favouringCell = new PdfPCell(favouring);
//				favouringCell.setBorder(0);
//				bankDetailsTable.addCell(favouringCell);
//
//				float[] columnWidths3 = { 4.5f, 1f, 4.5f };
//				PdfPTable bankDetails3Table = new PdfPTable(3);
//				bankDetails3Table.setWidthPercentage(100f);
//				try {
//					bankDetails3Table.setWidths(columnWidths3);
//				} catch (DocumentException e2) {
//					// TODO Auto-generated catch block
//					e2.printStackTrace();
//				}
//				Phrase bankNamePh = new Phrase("Bank Name", font8bold);
//				PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
//				bankNamePhCell.setBorder(0);
//				bankDetails3Table.addCell(bankNamePhCell);
//
//				Phrase dot = new Phrase(":", font8bold);
//				PdfPCell dotCell = new PdfPCell(dot);
//				dotCell.setBorder(0);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankName = "";
//				if (comppayment.getPaymentBankName() != null
//						&& !comppayment.getPaymentBankName().equals("")) {
//					bankName = comppayment.getPaymentBankName();
//				}
//				Phrase headingValue = new Phrase(bankName, font8);
//				PdfPCell headingValueCell = new PdfPCell(headingValue);
//				headingValueCell.setBorder(0);
//				headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(headingValueCell);
//
//				// this is for branch
//				Phrase bankBranch = new Phrase("Bank Branch", font8bold);
//				PdfPCell bankBranchCell = new PdfPCell(bankBranch);
//				bankBranchCell.setBorder(0);
//				bankDetails3Table.addCell(bankBranchCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankBranchValue = "";
//				if (comppayment.getPaymentBranch() != null
//						&& !comppayment.getPaymentBranch().equals("")) {
//					bankBranchValue = comppayment.getPaymentBranch();
//				}
//				Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
//				PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
//				bankBranchValuePhCell.setBorder(0);
//				bankBranchValuePhCell
//						.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankBranchValuePhCell);
//
//				Phrase bankAc = new Phrase("Bank A/c", font8bold);
//				PdfPCell bankAcCell = new PdfPCell(bankAc);
//				bankAcCell.setBorder(0);
//				bankDetails3Table.addCell(bankAcCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankAcNo = "";
//				if (comppayment.getPaymentAccountNo() != null&& !comppayment.getPaymentAccountNo().equals("")) {
//					bankAcNo = comppayment.getPaymentAccountNo();
//				}
//				Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
//				PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
//				bankAcNoValueCell.setBorder(0);
//				bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankAcNoValueCell);
//
//				Phrase bankIFSC = new Phrase("Bank IFSC", font8bold);
//				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
//				bankIFSCCell.setBorder(0);
//				bankDetails3Table.addCell(bankIFSCCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankIFSCNo = "";
//				if (comppayment.getPaymentIFSCcode() != null&& !comppayment.getPaymentIFSCcode().equals("")) {
//					bankIFSCNo = comppayment.getPaymentIFSCcode();
//				}
//				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
//				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
//				bankIFSCNoValueCell.setBorder(0);
//				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankIFSCNoValueCell);
//
//				PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
//				bankDetails3TableCell.setBorder(0);
//				bankDetailsTable.addCell(bankDetails3TableCell);
		}
		}
//		
//		
		PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
		bankDetailsTableCell.setBorder(0);
		PdfUtility BankDetails = new PdfUtility();
		/**
		 * For Innovative if process PC_RemoveBankDetail is active then do not print GSTIN and Bank details on quotation PDF
		 */
		if(quotEntity!=null){
			if(pc_RemoveBankDetail){
				PdfPTable authorizationTbl = new PdfPTable(1);
				authorizationTbl.setWidthPercentage(100f);
				
				authorizationTbl.addCell(pdfUtility.getCell("Confirmation of Contract", font10bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				authorizationTbl.addCell(pdfUtility.getCell(" ", font12bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				authorizationTbl.addCell(pdfUtility.getCell(" ", font12bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				authorizationTbl.addCell(pdfUtility.getCell("Authorized Sign:", font8bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				authorizationTbl.addCell(pdfUtility.getCell(" ", font12bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				authorizationTbl.addCell(pdfUtility.getCell("Name:", font8bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				authorizationTbl.addCell(pdfUtility.getCell(" ", font12bold,Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				PdfPCell authorizationCell = new PdfPCell(authorizationTbl);
				authorizationCell.setBorder(0);
				descTable.addCell(authorizationCell);
			}else{
				if(comppayment!=null) {
					descTable.addCell(BankDetails.getBankDetails(comppayment));
				}
				else {
					descTable.addCell(bankDetailsTableCell);
				}
			}
		}else{
			if(comppayment!=null) {
				descTable.addCell(BankDetails.getBankDetails(comppayment));
			}
			else {
				descTable.addCell(bankDetailsTableCell);
			}
		}
		
		
		
		Phrase recieverSinature = new Phrase("", font12boldul);
		Phrase blankphrase = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);
		Phrase blankphrase1 = new Phrase(" ");
		Phrase blankphrase2 = new Phrase(" ");
		
		/**
		 * @author Abhinav Bihade
		 * @since 27/12/2019
		 * For Bitco by Rahul Tiwari, "Correspondence Name" should print	 
		 */
		String companyname ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname="For "+  branchDt.getCorrespondenceName();
		}else{
			companyname = "For "+ comp.getBusinessUnitName().trim().toUpperCase();
		}
		
		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font9bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		/**
		 * Date29/11/2017 Dev.Jayshree Des.To add the digital signature changes
		 * are made
		 */
		DocumentUpload digitalsign = comp.getUploadDigitalSign();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + digitalsign.getUrl()));
			image2.scalePercent(13f);
			image2.scaleAbsoluteWidth(100f);// By Jayshree add this 7/12/2017
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String authsign = "AUTHORISED SIGNATORY";
		Paragraph authpara = new Paragraph();
		authpara.setFont(font9bold);
		authpara.add(authsign);
		authpara.setAlignment(Element.ALIGN_CENTER);
		System.out.println("4 condition===============1");
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);

		PdfPCell authsigncell = new PdfPCell();
		authsigncell.addElement(authpara);
		authsigncell.setBorder(0);
		System.out.println("5 condition===============1");
		PdfPTable table = new PdfPTable(1);
		table.addCell(blankcell);// BY Jayshree Add blank cell Date 7/12/2017
		table.addCell(companynamecell);

		// Date 29/11/2017
		// By Jayshree
		// add digitalsign

		if (imageSignCell != null) {
			// table.addCell(blankcell);
			table.addCell(imageSignCell);
			// table.addCell(blankcell);
		} else {

			table.addCell(blankcell);
			table.addCell(blankcell);
		}
		// End By Jayshree
		// table.addCell(blankcell);
		table.addCell(authsigncell);

		table.setWidthPercentage(100);

		System.out.println("6 condition===============1");

		PdfPTable parentbanktable = new PdfPTable(3);
		parentbanktable.setWidthPercentage(100);
		parentbanktable.setSpacingAfter(10f);
		try {
			parentbanktable.setWidths(new float[] { 25, 40, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPCell bankdatacell = new PdfPCell();// change the cell name by
												// jayshree
		PdfPCell customersigncell = new PdfPCell();// change the cell name by
													// jayshree
		PdfPCell authorisedcell = new PdfPCell();// change the cell name by
													// jayshree

		bankdatacell.addElement(descTable);

		System.out.println("7 condition===============1");
		customersigncell.addElement(bankinfotable);
		authorisedcell.addElement(table);

		if(quotEntity!=null){
			if(pc_RemoveBankDetail){
//				parentbanktable.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
				bankdatacell.setColspan(2);
			}else{
				parentbanktable.addCell(customersigncell);
			}
		}else{
			parentbanktable.addCell(customersigncell);
		}
		parentbanktable.addCell(bankdatacell);
		parentbanktable.addCell(authorisedcell);

		System.out.println("8 condition===============1");
		Phrase ref = new Phrase("Annexure 1 ", font8bold);
		Paragraph pararef = new Paragraph();
		pararef.add(ref);

		try {
			System.out.println("9 condition===============1");
			document.add(parentbanktable);
			System.out.println("10 condition===============1");
			logger.log(Level.SEVERE, this.products.size() + "");
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		try {
			createServices(preprint);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}


		int firstBreakPoint = 7;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();
		System.out.println("cnt value===" + cnt);
		
		/**
		 * @author Ashwini Patil
		 * @since 20-01-2022
		 * If more than one branch is selected in contract then below code will generate annexure in the Contract pdf.
		 */
		if(multipleServiceBranchesFlag){
				PdfPTable annexTable=pdfUtility.print_customer_branches(cust, selectedBranches,branchWiseProductMap,0,0);
				try {
					document.newPage();//added on 27-08-2022
					document.add(annexTable);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		logger.log(Level.SEVERE,"end of footerInfo");
	}
	
	private void setDescriptiontoAnnexure() {

		// ******************************************

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_LEFT);

		PdfPTable descriptiontable = new PdfPTable(4);
		descriptiontable.setWidthPercentage(100);
		descriptiontable.setSpacingBefore(10);

		Phrase srno = new Phrase("Sr No", font1);
		Phrase category = new Phrase("Prod ID", font1);
		Phrase product = new Phrase("Prod Name", font1);
		Phrase qty = new Phrase("Description", font1);

		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellprodID = new PdfPCell(category);
		cellprodID.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellprodName = new PdfPCell(product);
		cellprodName.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellDesc = new PdfPCell(qty);
		cellDesc.setHorizontalAlignment(Element.ALIGN_CENTER);

		descriptiontable.addCell(cellsrNo);
		descriptiontable.addCell(cellprodID);
		descriptiontable.addCell(cellprodName);
		descriptiontable.addCell(cellDesc);

		for (int i = 0; i < stringlis.size(); i++) {

			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font8);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(stringlis.get(i).getCount() + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(stringlis.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(stringlis.get(i).getComment(), font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_LEFT);

			descriptiontable.addCell(Pdfsrnocell);
			descriptiontable.addCell(pdfcategcell);
			descriptiontable.addCell(pdfnamecell);
			descriptiontable.addCell(pdfqtycell);

		}
		try {
			descriptiontable.setWidths(columnWidths1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// try {
		//
		// document.newPage();
		// document.add(para);
		// document.add(descriptiontable);
		// } catch (DocumentException e) {
		// e.printStackTrace();
		// }
	}

	public void createServices(String preprint) throws DocumentException {
		if (qp instanceof Contract) {
			int flagContractData = 0;
			ProcessConfiguration processConfig = ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", AppConstants.PROCESSCONFIGCO)
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int i = 0; i < processConfig.getProcessList().size(); i++) {
					if (processConfig
							.getProcessList()
							.get(i)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									AppConstants.PROCESSTYPECONTRACTSERVDATA)
							&& processConfig.getProcessList().get(i).isStatus() == true) {
						flagContractData = 1;
					}
				}
				if (flagContractData == 1 && processConfig != null) {
					displayServices(preprint);
				}
			}
		} else {
			int flagQuotationData = 0;
			ProcessConfiguration processConfig = ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", AppConstants.PROCESSCONFIGQUO)
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int i = 0; i < processConfig.getProcessList().size(); i++) {
					if (processConfig
							.getProcessList()
							.get(i)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									AppConstants.PROCESSTYPEQUOTATIONSERVDATA)
							&& processConfig.getProcessList().get(i).isStatus() == true) {
						flagQuotationData = 1;
					}
				}
				if (flagQuotationData == 1 && processConfig != null) {
					displayServices(preprint);
				}
			}
		}
	}

	private void displayServices(String preprint) throws DocumentException {

		// Add new line

		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		document.add(nextpage);
		if (upcflag == true) {
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		}
		
		/**
		 * Date 16-01-2018 By vijay 
		 * added for Header and footer
		 */
		
		/**
		 * Comment by jayshree
		 * to remove header footer from second page
		 */
		
//		if(preprint.equalsIgnoreCase("no")){
//					
//			if (comp.getUploadHeader() != null) {
//				createCompanyNameAsHeader(document, comp);
//			}
//
//			if (comp.getUploadFooter() != null) {
//				createCompanyNameAsFooter(document, comp);
//			}
//		}
		/**
		 * ends here
		 */
		
		String terms = "Service Details ";
		Phrase term = new Phrase(terms, font12boldUnderline);

		Paragraph para1 = new Paragraph();
		para1.add(term);
		para1.setAlignment(Element.ALIGN_CENTER);

		/**
		 *  Date 11-04-2018 By vijay no need description page in second page as per nitin sir
		 */
//		
//		String decsInfo = "";
//
//		if (qp instanceof Quotation) {
//			if (qp.getDescription() != null) {
//				decsInfo = qp.getDescription();
//			}
//		}
//
//		if (qp instanceof Contract) {
//			if (contEntity.getDescription() != null) {
//				decsInfo = qp.getDescription();
//			}
//		}
//
//		Phrase desphase = new Phrase(decsInfo, font8);
//		Paragraph para2 = new Paragraph();
//		para2.add(desphase);

		/**
		 * end shere
		 */
		
		/** Date 16-01-2018 By vijay added gap of one line ****/
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		document.add(para1);
		document.add(blank);
//		document.add(para2); // by vijay
		// addServiceDetail();

		
		/**
		 * Date 06-04-2018  by vijay below code updated above while querrying product data
		 */
		/**
		 * Date 16-01-2018 By vijay
		 * code updated using hasset becuase product list contain duplicate product so service dates same table duplicate issue
		 */
//		HashSet<String> superProductlist = new HashSet<String>();
//		for(int i=0;i<stringlis.size();i++){
//			superProductlist.add(stringlis.get(i).getProductCode());
//		}
		/**
		 * ends here
		 */
		
		String desc = "";
		for (int i = 0; i < stringlis.size(); i++) {

			Phrase service = new Phrase("Service Product :"
					+ stringlis.get(i).getProductName(), font12bold);

			Paragraph servicePara = new Paragraph();
			servicePara.add(service);

			desc = stringlis.get(i).getComment();
			Phrase prodDesc = new Phrase(desc, font8);
			Paragraph parades = new Paragraph(prodDesc);

			PdfPTable serviceTable = new PdfPTable(4);
			serviceTable.setWidthPercentage(100);

			Phrase serno = new Phrase("Service No", font6bold);
			Phrase serDate = new Phrase("Service Date", font6bold);
			Phrase serStatus = new Phrase("Service Status", font6bold);
			Phrase custBranch = new Phrase("Customer Branch", font6bold);

			PdfPCell sernocell = new PdfPCell(serno);
			PdfPCell serDatecell = new PdfPCell(serDate);
			PdfPCell serStatuscell = new PdfPCell(serStatus);
			PdfPCell custBranchcell = new PdfPCell(custBranch);

			serviceTable.addCell(sernocell);
			serviceTable.addCell(serDatecell);
			serviceTable.addCell(serStatuscell);
			serviceTable.addCell(custBranchcell);
			serviceTable.setSpacingBefore(10f);

			Phrase chunk = null;

			if (qp instanceof Contract) {
				System.out.println("service Schedule list size"
						+ contEntity.getServiceScheduleList().size());

				for (int k = 0; k < contEntity.getServiceScheduleList().size(); k++) {

					if (stringlis.get(i).getCount() == contEntity
							.getServiceScheduleList().get(k)
							.getScheduleProdId()) {
						chunk = new Phrase(contEntity.getServiceScheduleList()
								.get(k).getScheduleServiceNo()
								+ "", font8);
						PdfPCell srno = new PdfPCell(chunk);

						String serviceDt = fmt1.format(contEntity
								.getServiceScheduleList().get(k)
								.getScheduleServiceDate());
						chunk = new Phrase(serviceDt, font8);
						PdfPCell srDate = new PdfPCell(chunk);

						chunk = new Phrase("Scheduled", font8);
						PdfPCell srStatus = new PdfPCell(chunk);

						chunk = new Phrase(contEntity.getServiceScheduleList()
								.get(k).getScheduleProBranch(), font8);
						PdfPCell srBranch = new PdfPCell(chunk);

						serviceTable.addCell(srno);
						serviceTable.addCell(srDate);
						serviceTable.addCell(srStatus);
						serviceTable.addCell(srBranch);
					}
				}
			} else {
				for (int k = 0; k < quotEntity.getServiceScheduleList().size(); k++) {

					if (stringlis.get(i).getCount() == quotEntity
							.getServiceScheduleList().get(k)
							.getScheduleProdId()) {
						chunk = new Phrase(quotEntity.getServiceScheduleList()
								.get(k).getScheduleServiceNo()
								+ "", font8);
						PdfPCell srno = new PdfPCell(chunk);

						chunk = new Phrase(fmt1.format(quotEntity
								.getServiceScheduleList().get(k)
								.getScheduleServiceDate()), font8);
						PdfPCell srDate = new PdfPCell(chunk);

						chunk = new Phrase("Scheduled", font8);
						PdfPCell srStatus = new PdfPCell(chunk);

						chunk = new Phrase(quotEntity.getServiceScheduleList()
								.get(k).getScheduleProBranch(), font8);
						PdfPCell srBranch = new PdfPCell(chunk);

						serviceTable.addCell(srno);
						serviceTable.addCell(srDate);
						serviceTable.addCell(srStatus);
						serviceTable.addCell(srBranch);

					}
				}
			}
			try {
				document.add(servicePara);
				
				/**
				 * Date 16-01-2018 By vijay below code added with process config
				 */
				
//				document.add(parades);
//				document.add(serviceTable);
				
				if(DoNotProductDescriptionFlag==false){
					document.add(parades);
				}
				if(DoNotProductServicesTableFlag==false){
					document.add(serviceTable);
				}
				/**
				 * ends here
				 */
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
	}

	private void createServiceHeader(PdfPTable table) {

		Chunk seviceno = new Chunk("Service Number", this.font8bold);
		PdfPCell servicenumbercell = new PdfPCell();
		servicenumbercell.addElement(seviceno);

		Chunk chunk = new Chunk();
		chunk = new Chunk("Date", this.font8bold);

		PdfPCell datecell = new PdfPCell();
		datecell.addElement(chunk);

		chunk = new Chunk("Status", this.font8bold);

		PdfPCell statuscell = new PdfPCell();
		statuscell.addElement(chunk);

		table.addCell(servicenumbercell);
		table.addCell(datecell);
		// table.addCell(serviceEngineercell);
		// table.addCell(serviceEngineercell1);
		if (qp instanceof Contract) {
			table.addCell(statuscell);
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
		}

	}

	public double total(SalesLineItem salesLineItem) {
		double tax = 0;

		if (salesLineItem.getServiceTax() != null
				&& salesLineItem.getServiceTax().isInclusive() == false)
			tax = tax
					+ (salesLineItem.getServiceTax().getPercentage()
							* salesLineItem.getPrice() / 100);
		if (salesLineItem.getVatTax() != null
				&& salesLineItem.getVatTax().isInclusive() == false)
			tax = tax
					+ (salesLineItem.getVatTax().getPercentage()
							* salesLineItem.getPrice() / 100);
		System.out.println("Returning Total "
				+ (salesLineItem.getPrice() + tax) * salesLineItem.getQty());
		return ((salesLineItem.getPrice() + tax) * salesLineItem.getQty());
	}

	public double totalServices() {
		double sum = 0;
		for (int i = 0; i < products.size(); i++)
			if (products.get(i).getNumberOfServices() != 0)
				sum = sum + products.get(i).getNumberOfServices();
		return sum;
	}

	public String largestduration() {
		int max = 0, duration = 0;

		for (SalesLineItem prod : products) {
			try {
				duration = prod.getDuration();
				if (duration > max) {
					max = duration;
				}
			} catch (Exception e) {

			}
		}
		return max + "";

	}

	public void addtaxparagraph() {
		Paragraph taxpara = new Paragraph();
		Phrase titleservicetax = new Phrase("Service Tax No     :", font8bold);
		Phrase titlevatatx = new Phrase("Vat Tax No :    ", font8bold);
		Phrase titlelbttax = new Phrase("LBT Tax No :    ", font8bold);
		Phrase space = new Phrase("									");
		String serv = null, lbt, vat = null;
		if (comp.getServiceTaxNo() == null)
			serv = "";
		else
			serv = comp.getServiceTaxNo();
		if (comp.getVatTaxNo() == null)
			vat = "";
		else
			vat = comp.getVatTaxNo();

		if (serv.equals("") == false) {
			taxpara.add(titleservicetax);
			Phrase servp = new Phrase(serv, font8);
			taxpara.add(servp);
		}
		taxpara.add(space);

		if (vat.equals("") == false) {
			taxpara.add("                ");
			taxpara.add(titlevatatx);
			Phrase vatpp = new Phrase(vat, font8);
			taxpara.add(vatpp);
		}

		try {
			taxpara.setSpacingAfter(10);
			document.add(taxpara);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void addType() {
		Phrase header = null;
		if (qp instanceof Contract)
			header = new Phrase("Contract", font12boldul);

		else
			header = new Phrase("Quotation", font12boldul);

		Paragraph para = new Paragraph(header);
		para.setAlignment(Element.ALIGN_CENTER);
		// para.setSpacingBefore(10);//By Jayshree comment this code 7/12/2017
		// para.setSpacingAfter(20);//by jayshree comment this code 7/12/2017
		try {
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/*
	 * This method extracts unique products in a hash set form services array
	 * list we will create the returned list.
	 */

	public TreeSet<String> returnUniqueProducts() {
		TreeSet<String> productset = new TreeSet<String>(
				new ProductComparator());
		for (Service s : this.service) {
			productset.add(s.getProduct().getProductName());
		}

		return productset;
	}

	/*
	 * This method takes a product name and returns a vector of services for
	 * that product name.
	 */

	public HashMap<String, ArrayList<Service>> returnServicesWithRespectToProduct() {
		ArrayList<Service> alist = null;
		TreeSet<String> treeset = returnUniqueProducts();
		Iterator<String> prodname = treeset.iterator();
		HashMap<String, ArrayList<Service>> hashServices = new HashMap<String, ArrayList<Service>>();

		// Get the product name form iterator
		while (prodname.hasNext()) {
			String prodName = prodname.next();
			alist = new ArrayList<Service>();
			hashServices.put(prodName, alist);
			for (Service s : this.service) {
				if (prodName.trim().equals(s.getProduct().getProductName()))
					alist.add(s);

			}

			Collections.sort(alist, new ServiceComparator());
		}

		Set<Entry<String, ArrayList<Service>>> xy = hashServices.entrySet();
		Iterator<Entry<String, ArrayList<Service>>> it = xy.iterator();

		return hashServices;
	}

	public void addFooter() {

		Phrase blankphrase = new Phrase(" ");

		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);

		Phrase companyname = new Phrase("For " + comp.getBusinessUnitName(),
				font12);
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companyname);
		companynamecell.setBorder(0);
		Phrase authorizedsignatory = new Phrase("AUTHORISED SIGNATORY" + "",
				font12);
		PdfPCell authorisedcell = new PdfPCell();
		authorisedcell.addElement(authorizedsignatory);
		authorisedcell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(40);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(companynamecell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(authorisedcell);
		table.setHorizontalAlignment(Element.ALIGN_RIGHT);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void addServiceDetail() {
		Phrase header = null;
		header = new Phrase("Service Details", font6bold);
		Paragraph para = new Paragraph(header);
		para.setAlignment(Element.ALIGN_LEFT);
		para.setSpacingBefore(20);
		para.setSpacingAfter(10);
		try {
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// Create Services on the Fly with the help of Contract or Quotation item
	public void makeServices() {
		this.service = new ArrayList<Service>();
		Customer cust = null;
		Address addr = null;

		System.out.println("Inside Service Creation Code");
		for (SalesLineItem item : qp.getItems()) {
			if (item.getPrduct() instanceof ServiceProduct) {
				long noServices = (long) (item.getNumberOfServices() * item
						.getQty());
				int noOfdays = item.getDuration();
				int interval = (int) (noOfdays / item.getNumberOfServices());
				Date servicedate = item.getStartDate();
				Date d = new Date(servicedate.getTime());
				System.out.println("Interval is " + interval);
				System.out.println("Number of Services " + noServices);

				if (qp.getCinfo() != null) {
					if (qp.getCompanyId() != null) {
						cust = ofy().load().type(Customer.class)
								.filter("count", qp.getCinfo().getCount())
								.filter("companyId", qp.getCompanyId()).first()
								.now();
					} else {
						cust = ofy().load().type(Customer.class)
								.filter("count", qp.getCinfo().getCount())
								.first().now();
					}

				}
				// A patch as Neither Customer nor Contract has Customer Address
				if (cust != null)
					addr = cust.getAdress();
				System.out.println("Customer Address "+addr);

				for (int k = 0; k < noServices; k++) {
					Service temp = new Service();

					temp.setPersonInfo(qp.getCinfo());
					temp.setContractCount(qp.getCount());
					temp.setProduct((ServiceProduct) item.getPrduct());
					temp.setBranch(qp.getBranch());
					temp.setStatus(Service.SERVICESTATUSSCHEDULE);
					temp.setEmployee(qp.getEmployee());
					// temp.setContractStartDate(qp.getStartDate());
					// temp.setContractEndDate(qp.getEndDate());
					if (k == 0) {
						temp.setServiceDate(d);
					}

					if (item.getQty() > 1
							&& k % (noServices / item.getQty()) == 0) {
						System.out.println("K Value" + k);
						d = null;
						servicedate = item.getStartDate();
						d = new Date(servicedate.getTime());
					}

					if (k != 0) {
						temp.setServiceDate(d);
					}

					Calendar c = Calendar.getInstance();
					c.setTime(servicedate);
					c.add(Calendar.DATE, interval);
					servicedate = c.getTime();
					Date tempdate = new Date(servicedate.getTime());
					d = GenricServiceImpl.parseDate(tempdate);

					temp.setAddress(addr);
					service.add(temp);

					System.out.println("Created Service With Date "
							+ temp.getServiceDate());
				}
			}
		}
	}

	public void createDefaultText() {
		PdfPTable disclaimrtable = new PdfPTable(1);
		disclaimrtable.setWidthPercentage(100);

		PdfPTable companyinfotable = new PdfPTable(2);
		companyinfotable.setWidthPercentage(60);
		companyinfotable.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase discaimertextphrase = new Phrase(disclaimerText, font8);
		PdfPCell realtext = new PdfPCell(discaimertextphrase);
		realtext.setBorder(0);
		disclaimrtable.addCell(realtext);

		Phrase serviceTaxNr = new Phrase("Service Tax Nr  ", font8);
		Phrase vatTaxNr = new Phrase("VAT Registration Nr  ", font8);
		Phrase licenseNr = new Phrase("License Nr  ", font8);
		Phrase pancardTaxNr = new Phrase("PAN No  ", font8);

		PdfPCell serviceTaxCell = new PdfPCell(serviceTaxNr);
		serviceTaxCell.setBorder(0);
		PdfPCell vatRegNrCell = new PdfPCell(vatTaxNr);
		vatRegNrCell.setBorder(0);
		PdfPCell PANNoCell = new PdfPCell(pancardTaxNr);
		PANNoCell.setBorder(0);
		PdfPCell licenseNrCell = new PdfPCell(licenseNr);
		licenseNrCell.setBorder(0);

		Phrase realservicetax = new Phrase(comp.getServiceTaxNo() + "", font8);
		Phrase realvattax = new Phrase(comp.getVatTaxNo() + "", font8);
		Phrase realpanno = new Phrase(comp.getPanCard() + "", font8);
		Phrase reallicenseno = new Phrase(comp.getLicense() + "", font8);

		PdfPCell realserviceTaxCell = new PdfPCell(realservicetax);
		realserviceTaxCell.setBorder(0);
		PdfPCell realvatRegNrCell = new PdfPCell(realvattax);
		realvatRegNrCell.setBorder(0);
		PdfPCell realPANNoCell = new PdfPCell(realpanno);
		realPANNoCell.setBorder(0);
		PdfPCell reallicenseNrCell = new PdfPCell(reallicenseno);
		reallicenseNrCell.setBorder(0);

		if (comp.getServiceTaxNo() != null) {
			companyinfotable.addCell(serviceTaxCell);
			companyinfotable.addCell(realserviceTaxCell);
		}
		if (comp.getVatTaxNo() != null) {
			companyinfotable.addCell(vatRegNrCell);
			companyinfotable.addCell(realvatRegNrCell);
		}
		if (comp.getPanCard() != null) {
			companyinfotable.addCell(PANNoCell);
			companyinfotable.addCell(realPANNoCell);
		}
		if (comp.getLicense() != null) {
			companyinfotable.addCell(licenseNrCell);
			companyinfotable.addCell(reallicenseNrCell);
		}

		try {
			document.add(Chunk.NEWLINE);
			document.add(companyinfotable);
			document.add(disclaimrtable);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;

		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
//			// Here if both are inclusive then first remove service tax and then
//			// on that amount
//			// calculate vat.
//			double removeServiceTax = (entity.getPrice() / (1 + service / 100));
//
//			// double taxPerc=service+vat;
//			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
//			// below
//			retrServ = (removeServiceTax / (1 + vat / 100));
//			retrServ = entity.getPrice() - retrServ;
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}

	public void settingRemainingRowsToPDF(int flag) {
		System.out.println("Inside ::settingRemainingRowsToPDF" + flag);
		/**
		 * rohan added this code for adding premises in the pdf
		 */

		PdfPTable productTable = new PdfPTable(1);
		productTable.setWidthPercentage(100f);

		/**
		 * ends here
		 */

		Font font1 = new Font(Font.FontFamily.HELVETICA, 7 | Font.BOLD);

		PdfPTable headingtable = new PdfPTable(8);//add by  jayshree
		headingtable.setWidthPercentage(100);
		try {
			headingtable.setWidths(columnWidths7);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("headingtableCell" + e1);
		}
		Phrase srno = new Phrase("Sr No", font7);// Date 18/12/2017 by jayshree
												// increse font size
		PdfPCell srNoCell = new PdfPCell(srno);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(srNoCell);

//		Phrase product = new Phrase("Service Details", font7);
		/**@Sheetal :02-03-2022 Renaming Service from Service Details **/
		Phrase product = new Phrase("Service", font7bold);
		PdfPCell productCell = new PdfPCell(product);
		productCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(productCell);

		Phrase area = new Phrase("Area", font7);
		PdfPCell areaCell = new PdfPCell(area);
		areaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(areaCell);

		Phrase startdate = new Phrase("Contract Period", font7);
		PdfPCell startdateCell = new PdfPCell(startdate);
		startdateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(startdateCell);

		Phrase noservices = new Phrase("Services", font7);
		PdfPCell noservicesCell = new PdfPCell(noservices);
		noservicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(noservicesCell);

		Phrase rate = new Phrase("Rate", font7);
		PdfPCell rateCell = new PdfPCell(rate);
		rateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(rateCell);

		/**
		 * Date21-4-2018
		 * By Jayshree
		 */
		Phrase disph = new Phrase("Discount", font7);
		PdfPCell disCell = new PdfPCell(disph);
		disCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(disCell);
		
		Phrase amtph = new Phrase("Amount", font7);
		PdfPCell amtCell = new PdfPCell(amtph);
		amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingtable.addCell(amtCell);
		
		//End
		PdfPTable table1 = new PdfPTable(8);
		table1.setWidthPercentage(100);
		try {
			table1.setWidths(columnWidths7);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("table1" + e1);
		}
		
		
		for (int i = flag; i < this.products.size(); i++) {

			Phrase chunk = null;
			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font7);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(Pdfsrnocell);

			System.out.println("Test Run1");
			chunk = new Phrase(products.get(i).getProductName(), font7);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table1.addCell(pdfnamecell);

			System.out.println("Test Run2");
			chunk = new Phrase(products.get(i).getArea() + "", font7);
			PdfPCell pdfareacell = new PdfPCell(chunk);
			pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(pdfareacell);

			System.out.println("Test Run3");
			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			Calendar c = Calendar.getInstance();
			c.setTime(products.get(i).getStartDate());
			c.add(Calendar.DATE, products.get(i).getDuration());
			Date endDt = c.getTime();

			// Date 15/11/2017 By jayshree
			// add the changes for end date is not proper
			String date = null;
			fmt = new SimpleDateFormat("dd-MMM-yyyy");
			if (products.get(i).getEndDate() != null) {
				date = fmt.format(products.get(i).getStartDate()) + " To "
						+ fmt.format(products.get(i).getEndDate());
			} else {
				date = fmt.format(products.get(i).getStartDate()) + " To "
						+ fmt.format(endDt);
			}
			// end

			chunk = new Phrase(date, font7);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(pdfdatecell);

			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
					font7);
			pdfSerCell = new PdfPCell(chunk);
			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(pdfSerCell);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;
			
			
			if(qp instanceof Contract){
				if(consolidatePrice || contEntity.isConsolidatePrice()){				
						chunk = new Phrase(" ", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table1.addCell(pdfspricecell);
				}else{
					chunk = new Phrase(df.format(calculatedPrice), font7);
					PdfPCell pdfspricecell = new PdfPCell(chunk);
					pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table1.addCell(pdfspricecell);
				}
			}else{
				chunk = new Phrase(df.format(calculatedPrice), font7);
				PdfPCell pdfspricecell = new PdfPCell(chunk);
				pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				table1.addCell(pdfspricecell);

			}
			/**
			 * Date 21-42018
			 * By jayshree
			 * add rate column and 
			 */
			
			boolean isAreaPresent=false;
			double areval=0;
			double totaldis=0;
			double desper=products.get(i).getPercentageDiscount();
			try{
			 areval=Double.parseDouble(products.get(i).getArea());
			 isAreaPresent=true;
			}
			catch(Exception e){
				isAreaPresent=false;
			}
			
			if(isAreaPresent){
				totaldis=(products.get(i).getQty()*products.get(i).getPrice()*areval*desper)/100
						+products.get(i).getDiscountAmt();
			}
			else{
				totaldis=(products.get(i).getQty()*products.get(i).getPrice()*desper)/100+
						products.get(i).getDiscountAmt();
			}
		System.out.println("products.get(i).getQty()"+products.get(i).getQty());
			chunk = new Phrase(decimalformat.format(totaldis), font7); ////Ashwini Patil changed decimal format as it was printing discount .00
			PdfPCell pdfdiscell = new PdfPCell(chunk);
			pdfdiscell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(pdfdiscell);
			double amount=0;
			if(isAreaPresent){
				amount=areval*products.get(i).getPrice();
			}
			else{
				amount=products.get(i).getPrice();
			}
			
			if(qp instanceof Contract){
				//String pdfamtcell = "";
				if(consolidatePrice || contEntity.isConsolidatePrice()){
					if (i == 0) {
						chunk = new Phrase(df.format(amount)+ "", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						if(this.products.size() > 1)
						pdfspricecell.setBorderWidthBottom(0);
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table1.addCell(pdfspricecell);
					} else {
						chunk = new Phrase(" ", font7);
						PdfPCell pdfspricecell = new PdfPCell(chunk);
						if(i == this.products.size()-1 || noOfLines == 0){							
							pdfspricecell.setBorderWidthTop(0);
						}else{
							pdfspricecell.setBorderWidthBottom(0);
							pdfspricecell.setBorderWidthTop(0);
						}
						pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table1.addCell(pdfspricecell);
					}
				}else{
					chunk = new Phrase(df.format(amount), font7);
					PdfPCell pdfspricecell = new PdfPCell(chunk);
					pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table1.addCell(pdfspricecell);
				}
			}		
			
			
//			chunk = new Phrase(df.format(amount), font7);
//			PdfPCell pdfamtcell = new PdfPCell(chunk);
//			pdfamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table1.addCell(pdfamtcell);
			
			//End
			
//			chunk = new Phrase(df.format(calculatedPrice), font7);
//			PdfPCell pdfspricecell = new PdfPCell(chunk);
//			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			table1.addCell(pdfspricecell);

			double totalVal = (products.get(i).getPrice() - taxVal);
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}
			// Date 16/11/2017
			// By Jayshree to add the premiese Detailin next Page
			chunk = new Phrase(" ", font7);
			PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
			Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			Pdfsrnocell2.setBorderWidthRight(0);
			System.out.println("Test Run3");
			String premises = "";// 12
			if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premises For " + products.get(i).getProductName()
						+ " : " + products.get(i).getPremisesDetails();
			} else {
				premises = "Premises For " + products.get(i).getProductName()
						+ " : " + "N A";
			}

			Phrase premisesPh = new Phrase(premises, font7);
			PdfPCell premisesCell = new PdfPCell(premisesPh);
			// premisesCell.setBorderWidthRight(0);
			premisesCell.setColspan(7);
			// End By jayshree

			count = i;

			System.out.println("Test Run4");
			// Date 16/11/2017
			// By jayshree
			// check process confi for premises
			if (printProductPremisesFlag) {
				System.out.println(" to check the process confi==="
						+ printProductPremisesFlag);
				table1.addCell(Pdfsrnocell2);
				table1.addCell(premisesCell);
			}
			// End by jayshree
			/**
			 * nidhi
			 * for print model and serial number
			 */
				int cnnt = 0;
				PdfPCell proModelcell = null ,proSerialNocell = null; 
				String proModelNo = "";
				String proSerialNo = "";
			if(printModelSerailNoFlag){
				
				if(contEntity.getItems().get(i).getProModelNo()!=null && 
					contEntity.getItems().get(i).getProModelNo().trim().length() >0){
					proModelNo = contEntity.getItems().get(i).getProModelNo();
				}
				
				
				if(contEntity.getItems().get(i).getProSerialNo()!=null && 
					contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
					proSerialNo = contEntity.getItems().get(i).getProSerialNo();
				}
				
			
				if(proModelNo.length()>0){
					Phrase modelValPhrs = new Phrase(
							"Model No : " + proModelNo, font7);
					proModelcell =  new PdfPCell(modelValPhrs);
					proModelcell.setColspan(3);
					++cnnt;
				}
				
//				PdfPCell proSerialNocell ; 
				if(proSerialNo.length()>0){
					Phrase serialValPhrs = new Phrase(
							"Serial No: " + proSerialNo, font7);
					proSerialNocell = new PdfPCell(serialValPhrs);
					
					
					++cnnt;
//					table1.addCell(premiseCell);
				}
			}
			/**
			 * Date 14-09-2018 By Vijay
			 * Des :- Getting Server error due to null pointer exception
			 * so if condition added for !=null check
			 */
			if(proSerialNocell!=null && proModelcell!=null){
				if(cnnt>1 ){
					proSerialNocell.setColspan(4);
				}else if(proModelNo.length()>0){
					proModelcell.setColspan(7);
				}else {
					proSerialNocell.setColspan(7);
					BaseColor col = new BaseColor(10, 5, 3);
					proSerialNocell.setBorderColorTop(col);
				}
			}
			// End For Jayshree

			// table1.addCell(pdftotalproduct);
			try {
				table1.setWidths(columnWidths7);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			/**
			 * Date 04-12-2018 by Vijay
			 * Des :- Bug :- table not displaying data properly
			 */
			if(cnnt>0)
			{
				noOfLines = noOfLines-1;
				table1.addCell(Pdfsrnocell2);
				if(proModelcell!=null){
					table1.addCell(proModelcell);
				}
				if(proSerialNocell!=null){
					table1.addCell(proSerialNocell);
				}
			}
		}
				
		PdfPCell cellHeadingTable = new PdfPCell(headingtable);
		cellHeadingTable.setBorder(0);
		productTable.addCell(cellHeadingTable);
		PdfPCell celltable1 = new PdfPCell(table1);
		celltable1.setBorder(0);
		productTable.addCell(celltable1);
		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("prodtableCell adding" + e);
		}

	}

	private void otherchargestoAnnexur() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		String charge = "Other charge details";
		Chunk prodchunk = new Chunk(charge, font6bold);
		Paragraph parag = new Paragraph();
		parag.add(prodchunk);

		PdfPTable chargetable = new PdfPTable(2);
		chargetable.setWidthPercentage(100);
		// ********************
		double total = 0;
		for (int i = 0; i < this.prodCharges.size(); i++) {
			Phrase chunk = null;
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			if (prodCharges.get(i).getChargePercent() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + " @ "
						+ prodCharges.get(i).getChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargePercent()
						* prodCharges.get(i).getAssessableAmount() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);

			}
			if (prodCharges.get(i).getChargeAbsValue() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
						font1);
				pdfchargecell = new PdfPCell(chunk);

				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargeAbsValue();
				chunk = new Phrase(chargeAmt + "", font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
			}
			pdfchargecell.setBorder(0);

			total = total + chargeAmt;
			chargetable.addCell(pdfchargecell);
			chargetable.addCell(pdfchargeamtcell);

		}

		Phrase totalchunk = new Phrase(
				"Total                                                                      "
						+ "                                  "
						+ df.format(total), font6bold);
		PdfPCell totalcell = new PdfPCell();
		totalcell.addElement(totalchunk);

		PdfPCell chargecell = new PdfPCell();
		chargecell.addElement(chargetable);
		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(70);
		parent.setSpacingBefore(10f);
		parent.addCell(chargecell);
		parent.addCell(totalcell);
		parent.setHorizontalAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(parag);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// rohan added this code for GST PDF of contract and quotation

	private void createHeader() {
		Phrase companyName = null;
		if (comp != null) {
			companyName = new Phrase(comp.getBusinessUnitName().trim(),
					font16boldul);
		}

		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(companyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyAddr = null;
		if (comp != null) {
			companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
					.trim(), font12);
		}
		Paragraph companyAddrpara = new Paragraph();
		companyAddrpara.add(companyAddr);
		companyAddrpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyAddrCell = new PdfPCell();
		companyAddrCell.addElement(companyAddrpara);
		companyAddrCell.setBorder(0);
		companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		if (qp instanceof Contract) {

		} else {

		}

		Phrase companyGSTTIN = null;
		String gstinValue = "";
		if (UniversalFlag) {
			if (qp.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
						break;
					}
				}

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (!comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = gstinValue
								+ ","
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
					}
				}
			}
		} else {

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
					break;
				}
			}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = gstinValue
							+ ","
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
				}
			}
		}

		if (!gstinValue.equals("")) {
			companyGSTTIN = new Phrase(gstinValue, font12bold);
		}

		Paragraph companyGSTTINpara = new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyGSTTINCell = new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pdfPTable = new PdfPTable(1);
		pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		pdfPTable.addCell(companyGSTTINCell);

		try {
			document.add(pdfPTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// rohan added this code
		float[] myWidth = { 1, 3, 20, 17, 3, 30, 17, 3, 20, 1 };
		PdfPTable mytbale = new PdfPTable(10);
		mytbale.setWidthPercentage(100f);
		mytbale.setSpacingAfter(5f);
		mytbale.setSpacingBefore(5f);

		try {
			mytbale.setWidths(myWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase myblank = new Phrase(" ", font7);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero = new Phrase(" ", font7);
		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat1Phrase = new Phrase("Original for Receipient", font7);
		PdfPCell stat1PhraseCell = new PdfPCell(stat1Phrase);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat2Phrase = new Phrase("Duplicate for Supplier/Transporter",
				font7);
		PdfPCell stat2PhraseCell = new PdfPCell(stat2Phrase);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat3Phrase = new Phrase("Triplicate for Supplier", font7);
		PdfPCell stat3PhraseCell = new PdfPCell(stat3Phrase);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		mytbale.addCell(myblankborderZeroCell);
		mytbale.addCell(myblankCell);
		mytbale.addCell(stat1PhraseCell);
		mytbale.addCell(myblankborderZeroCell);
		mytbale.addCell(myblankCell);
		mytbale.addCell(stat2PhraseCell);
		mytbale.addCell(myblankborderZeroCell);
		mytbale.addCell(myblankCell);
		mytbale.addCell(stat3PhraseCell);
		mytbale.addCell(myblankborderZeroCell);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(mytbale);
		tab.addCell(cell);
		try {
			document.add(tab);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// ends here
		String titlepdf = "";

		if (qp instanceof Contract) {
			titlepdf = "Contract";
		} else {
			titlepdf = "Quotation";
		}

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createFooterTaxPart() {
		float[] columnMoreLeftWidths = { 2f, 1f };
		PdfPTable pdfPTaxTable = new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

		float[] column3widths = { 2f, 2f, 6f };
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// heading

		Phrase day = new Phrase("Day", font8bold);
		PdfPCell dayCell = new PdfPCell(day);
		dayCell.setBorder(0);

		Phrase percent = new Phrase("Percent", font8bold);
		PdfPCell percentCell = new PdfPCell(percent);
		percentCell.setBorder(0);

		Phrase comment = new Phrase("Comment", font8bold);
		PdfPCell commentCell = new PdfPCell(comment);
		commentCell.setBorder(0);

		leftTable.addCell(dayCell);
		leftTable.addCell(percentCell);
		leftTable.addCell(commentCell);

		// Values
		for (int i = 0; i < qp.getPaymentTermsList().size(); i++) {
			Phrase dayValue = new Phrase(qp.getPaymentTermsList().get(i)
					.getPayTermDays()
					+ "", font8);
			PdfPCell dayValueCell = new PdfPCell(dayValue);
			dayValueCell.setBorder(0);
			leftTable.addCell(dayValueCell);

			Phrase percentValue = new Phrase(df.format(qp.getPaymentTermsList()
					.get(i).getPayTermPercent())
					+ "", font8);
			PdfPCell percentValueCell = new PdfPCell(percentValue);
			percentValueCell.setBorder(0);
			leftTable.addCell(percentValueCell);

			Phrase commentValue = new Phrase(qp.getPaymentTermsList().get(i)
					.getPayTermComment(), font8);
			PdfPCell commentValueCell = new PdfPCell(commentValue);
			commentValueCell.setBorder(0);
			leftTable.addCell(commentValueCell);
		}

		// try {
		// document.add(leftTable);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.3f };
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.addElement(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax", font6bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		// totalAmount
		Phrase amtB4TaxValphrase = new Phrase("", font6bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < qp.getProductTaxes().size(); i++) {
			if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			}
		}

		Phrase CGSTphrase = new Phrase("CGST", font6bold);
		PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
		CGSTphraseCell.setBorder(0);
		// CGSTphraseCell.addElement(CGSTphrase);

		Phrase CGSTValphrase = new Phrase(cgstTotalVal + "", font7);
		// Paragraph CGSTValphrasePara=new Paragraph();
		// CGSTValphrasePara.add(CGSTValphrase);
		// CGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
		CGSTValphraseCell.setBorder(0);
		CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// CGSTValphraseCell.addElement(CGSTValphrasePara);

		Phrase SGSTphrase = new Phrase("SGST", font6bold);
		PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
		SGSTphraseCell.setBorder(0);
		// SGSTphraseCell.addElement(SGSTphrase);

		Phrase SGSTValphrase = new Phrase(sgstTotalVal + "", font7);
		// Paragraph SGSTValphrasePara=new Paragraph();
		// SGSTValphrasePara.add(SGSTValphrase);
		// SGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
		SGSTValphraseCell.setBorder(0);
		// SGSTValphraseCell.addElement(SGSTValphrasePara);
		SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase IGSTphrase = new Phrase("IGST", font6bold);
		PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
		IGSTphraseCell.setBorder(0);
		// IGSTphraseCell.addElement(IGSTphrase);

		Phrase IGSTValphrase = new Phrase(igstTotalVal + "", font7);
		// Paragraph IGSTValphrasePara=new Paragraph();
		// IGSTValphrasePara.add(IGSTValphrase);
		// IGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
		IGSTValphraseCell.setBorder(0);
		IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// IGSTValphraseCell.addElement(IGSTValphrasePara);

		Phrase GSTphrase = new Phrase("Total GST", font6bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		// GSTphraseCell.addElement(GSTphrase);

		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(totalGSTValue + "", font6bold);
		// Paragraph GSTValphrasePara=new Paragraph();
		// GSTValphrasePara.add(GSTValphrase);
		// GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// GSTValphraseCell.addElement(GSTValphrasePara);

		rightInnerTable.addCell(CGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(CGSTValphraseCell);
		rightInnerTable.addCell(SGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(SGSTValphraseCell);
		rightInnerTable.addCell(IGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(IGSTValphraseCell);
		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell();
		innerRightCell.setBorder(0);
		innerRightCell.addElement(rightInnerTable);

		rightTable.addCell(innerRightCell);

		PdfPCell rightCell = new PdfPCell();
		// rightCell.setBorder(0);
		rightCell.addElement(rightTable);

		PdfPCell leftCell = new PdfPCell();
		// leftCell.setBorder(0);
		leftCell.addElement(leftTable);

		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createFooterLastPart(String preprintStatus) {
		PdfPTable bottomTable = new PdfPTable(2);
		bottomTable.setWidthPercentage(100);
		float[] columnMoreLeftWidths = { 2f, 1f };
		try {
			bottomTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		String amtInWordsVal = "";
		String comment = "";
		double netpayable = 0;
		if (qp instanceof Contract) {
			Contract contractEntity = (Contract) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(contractEntity.getNetpayable());
			

			netpayable = contractEntity.getNetpayable();
			if (contractEntity.getDescription() != null) {
				comment = contractEntity.getDescription();
			}
		} else {
			Quotation quotation = (Quotation) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(quotation.getNetpayable());
		
			netpayable = quotation.getNetpayable();
			if (quotation.getDescription() != null) {
				comment = quotation.getDescription();
			}
		}

		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font6bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtInWordsValCell.setBorderWidthTop(0);
		amtInWordsValCell.setBorderWidthLeft(0);
		amtInWordsValCell.setBorderWidthRight(0);
		leftTable.addCell(amtInWordsValCell);

		Phrase termNcond = new Phrase("Terms and Conditions:", font6bold);
		PdfPCell termNcondCell = new PdfPCell();
		termNcondCell.setBorder(0);
		termNcondCell.addElement(termNcond);

		Phrase termNcondVal = new Phrase(comment, font6bold);
		PdfPCell termNcondValCell = new PdfPCell();
		termNcondValCell.setBorder(0);
		termNcondValCell.addElement(termNcondVal);

		leftTable.addCell(termNcondCell);
		leftTable.addCell(termNcondValCell);

		// rohan added this code for universal pest
		if (UniversalFlag) {
			if (qp.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {
				if (!preprintStatus.equalsIgnoreCase("Plane")) {
					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

						if (comp.getArticleTypeDetails().get(i)
								.getArticlePrint().equalsIgnoreCase("Yes")) {

							Phrase articalType = new Phrase(comp
									.getArticleTypeDetails().get(i)
									.getArticleTypeName()
									+ " : "
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeValue(), font6bold);
							PdfPCell articalTypeCell = new PdfPCell();
							articalTypeCell.setBorder(0);
							articalTypeCell.addElement(articalType);
							leftTable.addCell(articalTypeCell);
						}
					}
				}
			}
		} else {
			if (!preprintStatus.equalsIgnoreCase("Plane")) {
				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

					if (comp.getArticleTypeDetails().get(i).getArticlePrint()
							.equalsIgnoreCase("Yes")) {

						Phrase articalType = new Phrase(comp
								.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue(), font6bold);
						PdfPCell articalTypeCell = new PdfPCell();
						articalTypeCell.setBorder(0);
						articalTypeCell.addElement(articalType);
						leftTable.addCell(articalTypeCell);
					}
				}
			}
		}
		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font6bold);
		PdfPCell blankCell = new PdfPCell();
		blankCell.setBorder(0);
		blankCell.addElement(blank);

		Phrase netPay = new Phrase("Net Payable", font6bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);
		// netPayCell.addElement();

		Phrase netPayVal = new Phrase(netpayable + "", font6bold);
		// Paragraph netPayPara=new Paragraph();
		// netPayPara.add(netPayVal);
		// netPayPara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorder(0);
		// netPayValCell.addElement(netPayPara);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase gstReverseCharge = new Phrase("GST Payable on Reverse Charge",
				font6bold);
		PdfPCell gstReverseChargeCell = new PdfPCell();
		gstReverseChargeCell.setBorder(0);
		gstReverseChargeCell.addElement(gstReverseCharge);

		Phrase gstReverseChargeVal = new Phrase(" ", font7);
		PdfPCell gstReverseChargeValCell = new PdfPCell();
		gstReverseChargeValCell.setBorder(0);
		gstReverseChargeValCell.addElement(gstReverseChargeVal);

		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);
		// innerRightTable.addCell(blankCell);
		// innerRightTable.addCell(blankCell);
		// innerRightTable.addCell(blankCell);
		// innerRightTable.addCell(gstReverseChargeCell);
		// innerRightTable.addCell(colonCell);
		// innerRightTable.addCell(gstReverseChargeValCell);

		PdfPCell rightUpperCell = new PdfPCell();
		rightUpperCell.addElement(innerRightTable);
		rightUpperCell.setBorderWidthLeft(0);
		rightUpperCell.setBorderWidthRight(0);
		rightUpperCell.setBorderWidthTop(0);

		// rohan added this code for universal pest and printing multiple
		// company name

		String companyname = "";
		if (multipleCompanyName) {
			if (qp.getGroup() != null && !qp.getGroup().equals("")) {
				companyname = qp.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}

		} else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		// ends here

		rightTable.addCell(rightUpperCell);
		Phrase companyPhrase = new Phrase("For " + companyname, font6bold);
		Paragraph companyPara = new Paragraph();
		companyPara.add(companyPhrase);
		companyPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companyParaCell = new PdfPCell();
		companyParaCell.addElement(companyPara);
		companyParaCell.setBorder(0);

		rightTable.addCell(companyParaCell);
		rightTable.addCell(blankCell);
		rightTable.addCell(blankCell);
		rightTable.addCell(blankCell);
		Phrase signAuth = new Phrase("Authorised Signatory", font6bold);
		Paragraph signPara = new Paragraph();
		signPara.add(signAuth);
		signPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell signParaCell = new PdfPCell();
		signParaCell.addElement(signPara);
		signParaCell.setBorder(0);
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell();
		lefttableCell.addElement(leftTable);
		PdfPCell righttableCell = new PdfPCell();
		righttableCell.addElement(rightTable);

		bottomTable.addCell(lefttableCell);
		bottomTable.addCell(righttableCell);

		//

		Paragraph para = new Paragraph(
				"Note : This is computer generated hence no signature required.",
				font8);

		//
		try {
			document.add(bottomTable);
			document.add(para);
			// if(invoiceentity.getSalesOrderProducts().size() > 5 ){
			// createxureForRemainingProduct(5);
			// }
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerDetails() {
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		float[] columnHalfWidth = { 1f, 1f };
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		//By Ashwini Patil for fixed height and bottom aligned colon cell
		Phrase colon_bottonaligned = new Phrase(":", font6bold);
		PdfPCell colonCell_bottonaligned  = new PdfPCell(colon_bottonaligned);
		colonCell_bottonaligned.setBorder(0);
		colonCell_bottonaligned.setHorizontalAlignment(Element.ALIGN_LEFT);
//		colonCell_bottonaligned.setVerticalAlignment(Element.ALIGN_BOTTOM);
//		colonCell_bottonaligned.setFixedHeight(16);
		colonCell_bottonaligned.setPaddingTop(5);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font6bold); 
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT); 
		nameCell.setPaddingTop(5);//Ashwini Patil
		
		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017
		// By Jayshree Date 16/11/2017
		// Date 16/11/2017
		// To add the Salutation
		String tosir = null;
		String custName = "";
		/**
		 * @author Ashwini Patil
		 * @since 28-04-2022
		 * Nikhil reported and Nitin Sir asked to Print Company name/Full name/Branch name in service address.
		 * System was printing Correspondance name in Service Address which was wrong
		 */
		String custNameForServiceAddress="";
		
		if(cust.getServiceAddressName()!=null&&!cust.getServiceAddressName().equals("")) {
			custNameForServiceAddress=cust.getServiceAddressName();
		}else if (cust.isCompany() == true && cust.getCompanyName() != null) {
			if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
				custNameForServiceAddress = cust.getCompanyName().trim();
			}
			else{
				custNameForServiceAddress = "M/S " + " " + cust.getCompanyName().trim();
			}
		} else if (cust.getSalutation() != null
				&& !cust.getSalutation().equals("")) {
			custNameForServiceAddress = cust.getSalutation() + " "
					+ cust.getFullname().trim();
		} else {
			custNameForServiceAddress = cust.getFullname().trim();
		}
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			// if(cust.isCompany()==true){
			//
			//
			// tosir="M/S";
			// }
			// else{
			// tosir="";
			// }

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				System.out.println("PC_RemoveSalutationFromCustomerOnPdfFlag "+PC_RemoveSalutationFromCustomerOnPdfFlag);
				if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
					custName = cust.getCompanyName().trim();
				}
				else{
					custName = "M/S" + " " + cust.getCompanyName().trim();
				}
			} else if (cust.getSalutation() != null) {
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here
		/**
		 * Date 28/12/2017 By Jayshree To check the null condition
		 */
		Phrase nameCellVal = null;
		
		//Ashwini Patil changed font7 to nameAddressBoldFont
		if (cust.getCellNumber1() != null && cust.getCellNumber1() != 0) {
			nameCellVal = new Phrase(fullname, nameAddressBoldFont);			//Updated By: Viraj Date: 30-03-2019 Description:To create a new cell for mobile no.
		} else {
			nameCellVal = new Phrase(fullname, nameAddressBoldFont);			//Updated By: Viraj Date: 30-03-2019 Description:To create a new cell for mobile no.
		}
		
		
		
		
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCellValCell.setPaddingTop(5); //Ashwini Patil
		
		// Ends Here by jayshree
		
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description: To create a new cell for mobile
		 */
		Phrase mob = new Phrase("Mobile", font6bold);
		PdfPCell mobCell = new PdfPCell(mob);
//		mobCell.addElement(mob);
		mobCell.setBorder(0);
//		mobCell.setPaddingTop(2);
		mobCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String cellNo1="";
		String pocName1="";
		if(cust.getCellNumber1()!=null){
			cellNo1=cust.getCellNumber1()+"";
		}else{
			cellNo1="";
		}
		
//		/**
//		 * @author Anil @since 20-08-2021
//		 * printing cell no. from branch id process addcellNoFlag is active
//		 */
//		if(addcellNoFlag&&customerBranch!=null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0){
//			cellNo1=customerBranch.getCellNumber1()+"";
//		}
//		
//		if(cust.getFullname()!=null){
//			pocName1=toCamelCase(cust.getFullname());
//		}else{
//			pocName1="";
//		}
//		Phrase mobVal=null;
//		if(addcellNoFlag) {
//			mobVal = new Phrase(cellNo1+" ( "+pocName1+" ) ", font7);
//		}else {
//			mobVal = new Phrase(cellNo1, font7);
//		}
//		
//		PdfPCell mobValCell = new PdfPCell(mobVal);
//	//	mobValCell.addElement(mobVal);
//		mobValCell.setBorder(0);
////		mobValCell.setPaddingTop(2);
//		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		/** Ends **/
//       
		/******Date 2.11.2019 added by Deepak Salve *******/ 
		
		
		Phrase address = new Phrase("Address", font6bold);
		PdfPCell addressCell = new PdfPCell(address);
		System.out.println("First Address Here "+address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String countryName = "";
		
		/*@author Abhinav Bihade
		 * @since 20/01/2020
		 *As per Rahul Tiwari's Requirement Bitco : Print customer branch address in contract Pdf Tested by Sonu Porel 
		 */
		PdfPCell addressValCell=null;
//		Phrase addressVal;
		logger.log(Level.SEVERE,"UpscFlagBranch11111:" +upcflag);
		logger.log(Level.SEVERE,"billing address addcellNoFlag " +addcellNoFlag);
		if(upcflag||addcellNoFlag){
//			if(customerBranch!=null){
//				
//				Phrase addressVal=new Phrase(customerBranch.getBillingAddress().getCompleteAddress().trim(),font7);
//				//serviceAddress1= customerBranch.getAddress();
//				//addressVal = customerBranch.getAddress().getCompleteAddress();
//				 addressValCell = new PdfPCell(addressVal);//chng1
//				 logger.log(Level.SEVERE,"customerBranch billing address  "+addressVal);
////			   		 addressValCell.addElement(addressVal);
//			   		addressValCell.setBorder(0);
//			   		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				logger.log(Level.SEVERE,"UpscFlagBranch:" +upcflag);
//			
//			}
//			else{
			
			String adrsValString1 = "";
			if(cust.getAdress()!=null) {
				logger.log(Level.SEVERE,"Billing Address from customer "+cust.getAdress().getCompleteAddress().trim());
				adrsValString1 = cust.getAdress().getCompleteAddress().trim();
				countryName = cust.getAdress().getCountry();
			}
			
			/**
			 * @author Vijay Date :- 21-01-2021
			 * Des :- if Service address stored in quotation showing from quotation entity or else as old process from customer
			 * Requirement from Rahul PTSPL 
			 */
			if(qp!=null ){
				if (qp instanceof Contract) {
					Contract conEntity = (Contract) qp;
					if(conEntity.getNewcustomerAddress()!=null && conEntity.getNewcustomerAddress().getAddrLine1()!=null && 
							!conEntity.getNewcustomerAddress().getAddrLine1().equals("")){
						adrsValString1 = conEntity.getNewcustomerAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Billing Address $="+adrsValString1);
						countryName = conEntity.getNewcustomerAddress().getCountry();
					}
					
					/**
					 * @author Anil @since 20-08-2021
					 * For creative pest if process PRINTADDRESSANDCELLFROMCUSTOMERBRANCH is active then after it was not printing
					 * branch address
					 */
					if(addcellNoFlag&&customerBranch!=null&&customerBranch.getBillingAddress()!=null){
						logger.log(Level.SEVERE,"Billing Address from customer branch "+customerBranch.getBillingAddress().getCompleteAddress().trim());
						adrsValString1 = customerBranch.getBillingAddress().getCompleteAddress().trim();
						countryName = customerBranch.getBillingAddress().getCountry();

					}
				}
				else {
					Quotation quotation = (Quotation) qp;
					if(quotation.getBillingAddress()!=null && quotation.getBillingAddress().getAddrLine1()!=null && 
							!quotation.getBillingAddress().getAddrLine1().equals("")){
						adrsValString1 = quotation.getBillingAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Billing Address ="+adrsValString1);
						countryName = quotation.getBillingAddress().getCountry();

					}
				}
				
			}
//				Phrase addressVal = new Phrase(cust.getAdress().getCompleteAddress()
//	    				.trim(), font7);
			
				/**
				 * @author Anil
				 * @since 18-01-2022
				 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
				 */
				adrsValString1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString1);
				
				//Ashwini Patil changed font7 to nameAddressFont
				Phrase addressVal = new Phrase(adrsValString1, nameAddressFont);
	    		 addressValCell = new PdfPCell(addressVal);//chng1
	    		System.out.println("Second Address Here222 "+addressVal+"amol");
	    		addressValCell.setBorder(0);
	    		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			}
		}
	else if(contEntity!=null&&contEntity.getNewcustomerAddress()!=null){
//		Phrase addressVal = new Phrase(cust.getAdress().getCompleteAddress()
//				.trim(), font7);
//		 addressValCell = new PdfPCell(addressVal);//chng1
//		System.out.println("Second Address Here "+addressVal);
//		 addressValCell.addElement(addressVal);
//		addressValCell.setBorder(0);
//		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
//        Phrase addressVal=new Phrase(contEntity.getNewcustomerAddress().getCompleteAddress().trim(),font7);
		Phrase addressVal=new Phrase(pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", contEntity.getNewcustomerAddress().getCompleteAddress().trim()),font7);
   		 addressValCell = new PdfPCell(addressVal);//chng1
   		System.out.println("Second Address Here "+addressVal);
//   		 addressValCell.addElement(addressVal);
   		addressValCell.setBorder(0);
   		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
   		countryName = contEntity.getNewcustomerAddress().getCountry();
        	
        }else{
//		Phrase addressVal=new Phrase(contEntity.getNewcustomerAddress().getCompleteAddress().trim(),font7);
//		 addressValCell = new PdfPCell(addressVal);//chng1
//		System.out.println("Second Address Here "+addressVal);
//		 addressValCell.addElement(addressVal);
//		addressValCell.setBorder(0);
//		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
        	
        	
			String adrsValString1 = "";
			if(cust.getAdress()!=null) {
				cust.getAdress().getCompleteAddress().trim();
				countryName = cust.getAdress().getCountry();
			}

        	/**
			 * @author Vijay Date :- 21-01-2021
			 * Des :- if Service address stored in quotation showing from quotation entity or else as old process from customer
			 * Requirement from Rahul PTSPL 
			 */
			if(qp!=null ){
				if (qp instanceof Contract) {
					Contract conEntity = (Contract) qp;
					if(conEntity.getNewcustomerAddress()!=null && conEntity.getNewcustomerAddress().getAddrLine1()!=null && 
							!conEntity.getNewcustomerAddress().getAddrLine1().equals("")){
						adrsValString1 = conEntity.getNewcustomerAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Billing Address $="+adrsValString1);
						countryName = conEntity.getNewcustomerAddress().getCountry();

					}
				}
				else {
					Quotation quotation = (Quotation) qp;
					if(quotation.getBillingAddress()!=null && quotation.getBillingAddress().getAddrLine1()!=null && 
							!quotation.getBillingAddress().getAddrLine1().equals("")){
						adrsValString1 = quotation.getBillingAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Billing Address ="+adrsValString1);
						countryName = quotation.getBillingAddress().getCountry();

					}
				}
				
			}
			
			/**
			 * @author Anil
			 * @since 18-01-2022
			 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
			 */
			adrsValString1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString1);
			
			Phrase addressVal = new Phrase(adrsValString1, font7);
    		 addressValCell = new PdfPCell(addressVal);//chng1
    		System.out.println("Second Address Here "+addressVal);
//    		 addressValCell.addElement(addressVal);
    		addressValCell.setBorder(0);
    		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);	
        	
        	
        }

		
		
		
		/**
		 * @author Anil @since 20-08-2021
		 * printing cell no. from branch id process addcellNoFlag is active
		 */
		if(addcellNoFlag&&customerBranch!=null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0){
			cellNo1=customerBranch.getCellNumber1()+"";
			
			/**
			 * @author Vijay Date :- 22-09-2021
			 * Des :- Adding country code(from country master) in cell number.
			 */
			cellNo1 = serverAppUtility.getMobileNoWithCountryCode(cellNo1, countryName, comp.getCompanyId());
		
		}
		
		if(cust.getFullname()!=null){
			pocName1=toCamelCase(cust.getFullname());
		}else{
			pocName1="";
		}
		/**
		 * @author Vijay Date :- 22-09-2021
		 * Des :- Adding country code(from country master) in cell number.
		 */
		cellNo1 = serverAppUtility.getMobileNoWithCountryCode(cellNo1, countryName, comp.getCompanyId());
	
		Phrase mobVal=null;
		if(addcellNoFlag) {
			mobVal = new Phrase(cellNo1+" ( "+pocName1+" ) ", font7);
		}else {
			mobVal = new Phrase(cellNo1, font7);
		}
		
		PdfPCell mobValCell = new PdfPCell(mobVal);
	//	mobValCell.addElement(mobVal);
		mobValCell.setBorder(0);
//		mobValCell.setPaddingTop(2);
		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/** Ends **/
       
		
	String gstTinStr = "";
	
	if (qp instanceof Contract){
		if (contEntity.getCustomerGSTNumber() != null
				&& !contEntity.getCustomerGSTNumber().equals("")) {
			gstTinStr = contEntity.getCustomerGSTNumber().trim();
		} else {
			ServerAppUtility serverAppUtility = new ServerAppUtility();
			gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,
					contEntity.getNewCustomerBranch(),"Contract");
		}
	}else{
		ServerAppUtility serverAppUtility = new ServerAppUtility();
		gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,quotEntity.getBranch(),"Quotation");
	}
	
	Phrase gstTin = new Phrase(" ", font6bold);
	if(!gstTinStr.equals("")){
		gstTin = new Phrase("GSTIN", font6bold);
	}
	PdfPCell gstTinCell = new PdfPCell(gstTin);
	// gstTinCell.addElement(gstTin);
	gstTinCell.setBorder(0);
	gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	/**
	 * Date 5/12/2017
	 * By Jayshree 
	 * To add the state code
	 */
	
	PdfPTable statetable = new PdfPTable(6);
	statetable.setWidthPercentage(100);
	
	try {
		statetable.setWidths(new float[]{50,10,25,5,15,10});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	Phrase gstTinVal = new Phrase(gstTinStr, font7);
	PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
	// gstTinValCell.addElement(gstTinVal);
	gstTinValCell.setBorder(0);
	gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	if(gstNumberPrintFlag) {//29-01-2024 
	statetable.addCell(gstTinValCell);
	
	Phrase Blnk2=new Phrase(" ");
	PdfPCell blankcell=new PdfPCell(Blnk2);
	blankcell.setBorder(0);
	
	statetable.addCell(blankcell);
	}
	
	
	Phrase stateCode=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
		stateCode = new Phrase("State Code", font6bold);
	}else{
		stateCode = new Phrase(" ", font6bold);
	}
	
	PdfPCell stateCodeCell = new PdfPCell(stateCode);
	// stateCodeCell.addElement(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	statetable.addCell(stateCodeCell);
	
	Phrase Blnk3=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
		Blnk3=new Phrase(":",font7);
	}else{
		Blnk3=new Phrase(" ",font7);
	}
	PdfPCell blankcell3=new PdfPCell(Blnk3);
	blankcell3.setBorder(0);
	statetable.addCell(blankcell3);
	
	String stCo = "";
	if (stateList != null) {
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}
	}
//	Phrase stateCodeVal=null;
//	if(gstTinStr!=null && !gstTinStr.equals("")){
//		stateCodeVal = new Phrase(stCo, font7);
//	}else{
//		stateCodeVal = new Phrase(" ", font7);
//	}
//	
//	//End By Jayshree
//		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
//		// stateCodeValCell.addElement(stateCodeVal);
//		 stateCodeValCell.setBorder(0);
//		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statetable.addCell(stateCodeValCell);
//
//		statetable.addCell(blankcell);
//
//		PdfPCell statecell = new PdfPCell(statetable);
//		statecell.setBorder(0);
		// End By jayshree
		
		
		/**
		 * Date 27/2/2018 
		 * by Jayshree 
		 * Des.To add the customer Email id
		 */
		Phrase emailph=new Phrase("Email",font6bold);
		PdfPCell emailCell=new PdfPCell(emailph);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String custemailval = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			custemailval = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				custemailval = cust.getEmail();
			} else {
				custemailval ="";
			}
		}
		
		
		
		Phrase emailphval=new Phrase(custemailval,font7);
		PdfPCell emailCellval=new PdfPCell(emailphval);
		emailCellval.setBorder(0);
		emailCellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell_bottonaligned);  //Ashwini Patil have changed coloncell
		colonTable.addCell(nameCellValCell);
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description:To create a new cell for mobile
		 */
		colonTable.addCell(mobCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(mobValCell);
		/** Ends **/
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell); 
		/**
		 * Date 28/2/2018 By jayshree add cell
		 */
		if(cust.getEmail()!=null&&!cust.getEmail().equals("")){
			colonTable.addCell(emailCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(emailCellval);
		}
		
		
		/**
		 * @author Priyanka @since 19-08-2021
		 */
		System.out.println("Priyanka Article type");
		String documentName="";
		if(quotEntity!=null){
			documentName="Quotation";
		}else{
			documentName="Contract";
		}
		if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
			System.out.println("Article type size"+cust.getArticleTypeDetails().size());
			for(ArticleType type:cust.getArticleTypeDetails()){
				if(type.getDocumentName().equals(documentName)&&type.getArticlePrint().equalsIgnoreCase("Yes")){
					if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")&&!gstNumberPrintFlag){
						continue;
					}
					
//					custDetBillTbl.addCell(pdfUtility.getCell(type.getArticleTypeName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//					custDetBillTbl.addCell(colonCell);
//					custDetBillTbl.addCell(pdfUtility.getCell(type.getArticleTypeValue(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				     
					Phrase article =null;
					article = new Phrase(type.getArticleTypeName(), font6bold);
					PdfPCell articleCell = new PdfPCell(article);
					articleCell.setBorder(0);
					articleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					Phrase articleVal = new Phrase(type.getArticleTypeValue(), font7);
					PdfPCell articleValCell = new PdfPCell(articleVal);
					articleValCell.setBorder(0);
					articleValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					colonTable.addCell(articleCell);
					colonTable.addCell(colonCell);
					colonTable.addCell(articleValCell);
					
				}
				
			}
		}
		
//		if(gstTinStr!=null&&!gstTinStr.equals("")){
//			colonTable.addCell(gstTinCell);
//			colonTable.addCell(colonCell);
//			colonTable.addCell(statecell);
//		}

		PdfPCell cell1 = new PdfPCell();
		cell1.setBorder(0);
		cell1.addElement(colonTable);

		float[] columnStateCodeCollonWidth = { 35, 5, 15, 45 };
		

		Phrase blak = new Phrase(" ", font8);
		PdfPCell blakCell = new PdfPCell(blak);
		blakCell.setBorder(0);

		
		part1Table.addCell(cell1);

	

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font6bold);
		PdfPCell name2Cell = new PdfPCell();
		name2Cell.addElement(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil @since 28-10-2021
		 * If single customer branch is selected then print branch name in service address section instead if customer name
		 * raised by Nitin Sir and Pooname for IndoGulf 
		 */
		
//		String custBranchName=fullname; 
		String custBranchName=custNameForServiceAddress; //Ashwini Patil Date:28-04-2022 
		
		//Date:20-01-2022 By: Ashwini Patil Description: to print selected Branch name on pdf
//		if(customerBranch!=null){
////			custBranchName=custName+" - "+ customerBranch.getBusinessUnitName()+" branch";
//			custBranchName=customerBranch.getBusinessUnitName();//Ashwini Patil Date:28-04-2022
//		}
		if(customerBranch!=null){
			if(customerBranch.getServiceAddressName()!=null&&!customerBranch.getServiceAddressName().equals(""))
				custBranchName=customerBranch.getServiceAddressName();
			else
				custBranchName=customerBranch.getBusinessUnitName();
		}
		
//		Phrase name2CellVal = new Phrase(fullname, font7);			//Updated By: Viraj Date: 30-03-2019 Description:To create a new cell for mobile no.
		
		//Ashwini Patil changed font7 to nameAddressBoldFont
		Phrase name2CellVal = new Phrase(custBranchName, nameAddressBoldFont);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		name2CellValCell.setPaddingTop(5); //Ashwini Patil
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description: To create a new cell for mobile
		 */
		Phrase mob1 = new Phrase("Mobile", font6bold);
		PdfPCell mob1Cell = new PdfPCell(mob1);
//		mob1Cell.addElement(mob1);
		mob1Cell.setBorder(0);
	//	mob1Cell.setPaddingTop(2);
		mob1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		cust.getCellNumber1()
//		ArrayList<String> custbrancheslist=getCustomerBranchList(contEntity.getItems());
//		logger.log(Level.SEVERE, "custbranchlist size "+custbrancheslist.size());
//		long cellNo=0l;
//		String cellNo="";
//
//		String pocName="";
//		if(addcellNoFlag&&customerBranch!=null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0){
//			logger.log(Level.SEVERE, "custbranch name ");
//		
//			if(customerBranch.getCellNumber1()!=null){
//				cellNo=customerBranch.getCellNumber1()+"";
//			}
////			else{
////				cellNo=0l;
////			}
//			
//			if(customerBranch.getPocName()!=null){
//				pocName=customerBranch.getPocName();
//			}else{
//				pocName="";
//			}
//			logger.log(Level.SEVERE, "cust cell no and poc name "+cellNo+pocName);
//			
//			cellNo = serverAppUtility.getMobileNoWithCountryCode(cellNo+"", countryName, comp.getCompanyId());
//			
//		}else{
//			if(cust.getCellNumber1()!=null){
//				cellNo=cust.getCellNumber1()+"";
//			}
////			else{
////				cellNo=0l;
////			}
//			
//			if(cust.getFullname()!=null){
//				pocName=cust.getFullname();
//			}else{
//				pocName="";
//			}
//			logger.log(Level.SEVERE, "cust cell no and poc name2222 "+cellNo+pocName);
//		}
//		Phrase mob1Val=null;
//		if(addcellNoFlag) {
//			if(pocName!=null&&!pocName.equals("")){
//				mob1Val = new Phrase(cellNo+" ( "+pocName+" ) ", font7);
//			}else{
//				mob1Val = new Phrase(cellNo+"", font7);
//			}
//				
//		}else {
//			mob1Val = new Phrase(cellNo+"", font7);
//		}
//		
//		
//		
//		logger.log(Level.SEVERE, "cust cell no and poc name 3333 "+cellNo+pocName);
//		
//		PdfPCell mob1ValCell = new PdfPCell(mob1Val);
////		mob1ValCell.addElement(mob1Val);
//		mob1ValCell.setBorder(0);
//	//	mob1ValCell.setPaddingTop(2);
//		mob1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		/** Ends **/
		
		/********* 2.11.2019 added by Deepak Salve - Customer Addess and service addess change in PDF *****/
		
		Phrase address2 = new Phrase("Address", font6bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		System.out.println("First Adddress 2 "+address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/*@author Abhinav Bihade
		 * @since 20/01/2020
		 *As per Rahul Tiwari's Requirement Bitco : Print customer branch address in contract Pdf Tested by Sonu Porel 
		*/
		PdfPCell address2ValCell = null;
		logger.log(Level.SEVERE,"Service address addcellNoFlag  " +addcellNoFlag);
		logger.log(Level.SEVERE,"Service address upcflag  " +upcflag);

		String countryName2 = "";
		if(upcflag||addcellNoFlag){
//			if(customerBranch!=null){
//				
//				Phrase address2Val=new Phrase(customerBranch.getAddress().getCompleteAddress().trim(),font7);
//				//serviceAddress1= customerBranch.getAddress();
//				//addressVal = customerBranch.getAddress().getCompleteAddress();
//				 //addressValCell = new PdfPCell(address2Val);//chng1
//				logger.log(Level.SEVERE,"customerBranch  Service address "+address2Val);
//			   		//address2ValCell.addElement(address2Val);
//			   		address2ValCell=new PdfPCell(address2Val);
//			   		address2ValCell.setBorder(0);
//			   		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				logger.log(Level.SEVERE,"UpscFlagBranch:" +upcflag);
//			
//		}
//			else{
			
			String adrsValString = "";
			
			if(cust.getSecondaryAdress()!=null) {
				adrsValString = cust.getSecondaryAdress().getCompleteAddress().trim();
			}
			
			/**
			 * @author Vijay Date :- 21-01-2021
			 * Des :- if billing address stored in quotation showing from quotation entity or else as old process from customer
			 * Requirement from Rahul PTSPL 
			 */
			if(qp!=null ){
				
				if (qp instanceof Contract) {
					Contract conEntity = (Contract) qp;
					if(conEntity.getCustomerServiceAddress()!=null && conEntity.getCustomerServiceAddress().getAddrLine1()!=null && 
							!conEntity.getCustomerServiceAddress().getAddrLine1().equals("")){
						adrsValString = conEntity.getCustomerServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address $="+adrsValString);
						countryName2 = conEntity.getCustomerServiceAddress().getCountry();
					}
					
					//By Ashwini Patil to print branch address if branch is selected in contract
					if(customerBranch!=null)
					{
						adrsValString=customerBranch.getAddress().getCompleteAddress();
					}
					
					/**
					 * @author Anil @since 20-08-2021
					 * For creative pest if process PRINTADDRESSANDCELLFROMCUSTOMERBRANCH is active then after it was not printing
					 * branch address
					 */
					if(addcellNoFlag&&customerBranch!=null&&customerBranch.getAddress()!=null){
						logger.log(Level.SEVERE,"Billing Address from customer branch "+customerBranch.getAddress().getCompleteAddress().trim());
						adrsValString = customerBranch.getAddress().getCompleteAddress().trim();
						countryName2 = customerBranch.getAddress().getCountry();

					}
				}
				else {
					Quotation quotation = (Quotation) qp;
					if(quotation.getServiceAddress()!=null && quotation.getServiceAddress().getAddrLine1()!=null && 
							!quotation.getServiceAddress().getAddrLine1().equals("")){
						adrsValString = quotation.getServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address ="+adrsValString);
						countryName2 = quotation.getServiceAddress().getCountry();

					}
				}
				
			}
			/**
			 * @author Anil
			 * @since 18-01-2022
			 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
			 */
			adrsValString=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString);
			
			//Ashwini Patil changed font7 to nameAddressFont
			Phrase address2Val = new Phrase(adrsValString, nameAddressFont);
//				Phrase address2Val = new Phrase(cust.getSecondaryAdress()//Chng2
//						.getCompleteAddress().trim(), font7);
				 address2ValCell = new PdfPCell(address2Val);
				 logger.log(Level.SEVERE,"Second Address 456 "+address2Val);
				// address2ValCell.addElement(address2Val);
				address2ValCell.setBorder(0);
				address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			}
		}
		
		
		
		
		else if(contEntity!=null&&contEntity.getCustomerServiceAddress()!=null){
//		Phrase address2Val = new Phrase(cust.getSecondaryAdress()//Chng2
//				.getCompleteAddress().trim(), font7);
//		 address2ValCell = new PdfPCell(address2Val);
//		System.out.println("Second Address 2 "+address2Val);
//		// address2ValCell.addElement(address2Val);
//		address2ValCell.setBorder(0);
//		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
//		Phrase address2Val=new Phrase(contEntity.getCustomerServiceAddress().getCompleteAddress().trim(),font7);
		Phrase address2Val=new Phrase(pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", contEntity.getCustomerServiceAddress().getCompleteAddress().trim()),font7);
		 address2ValCell = new PdfPCell(address2Val);
		System.out.println("Second Address 2 "+address2Val);
		// address2ValCell.addElement(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		countryName2 = contEntity.getCustomerServiceAddress().getCountry();

		
        }else{
//		Phrase address2Val=new Phrase(contEntity.getCustomerServiceAddress().getCompleteAddress().trim(),font7);
//		 address2ValCell = new PdfPCell(address2Val);
//		System.out.println("Second Address 2 "+address2Val);
//		// address2ValCell.addElement(address2Val);
//		address2ValCell.setBorder(0);
//		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
        	String adrsValString = "";
			
        	
			if(cust.getAdress()!=null) {
				adrsValString = cust.getAdress().getCompleteAddress().trim();
				countryName2 = cust.getAdress().getCountry();
			}
			
        	/**
			 * @author Vijay Date :- 21-01-2021
			 * Des :- if billing address stored in quotation showing from quotation entity or else as old process from customer
			 * Requirement from Rahul PTSPL 
			 */
			if(qp!=null ){
				if (qp instanceof Contract) {
					
					Contract conEntity = (Contract) qp;
					
					if(conEntity.getCustomerServiceAddress()!=null && conEntity.getCustomerServiceAddress().getAddrLine1()!=null && 
							!conEntity.getCustomerServiceAddress().getAddrLine1().equals("")){
						adrsValString = conEntity.getCustomerServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address $="+adrsValString);
						countryName2 =  conEntity.getCustomerServiceAddress().getCountry();					
						}					
				}
				else {
					Quotation quotation = (Quotation) qp;
					if(quotation.getServiceAddress()!=null && quotation.getServiceAddress().getAddrLine1()!=null && 
							!quotation.getServiceAddress().getAddrLine1().equals("")){
						adrsValString = quotation.getServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address ="+adrsValString);
						countryName2 =  quotation.getServiceAddress().getCountry();

					}
				}
			
			}
			
			/**
			 * @author Anil
			 * @since 18-01-2022
			 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
			 */
			adrsValString=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString);
			
				Phrase address2Val = new Phrase(adrsValString, font7);
				 address2ValCell = new PdfPCell(address2Val);
				 logger.log(Level.SEVERE,"Second Address 456 "+address2Val);
				// address2ValCell.addElement(address2Val);
				address2ValCell.setBorder(0);
				address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			}
				
//		Phrase address2Val = new Phrase(cust.getSecondaryAdress()//Chng2
//				.getCompleteAddress().trim(), font7);
//		 address2ValCell = new PdfPCell(address2Val);
//		System.out.println("Second Address 2 "+address2Val);
//		// address2ValCell.addElement(address2Val);
//		address2ValCell.setBorder(0);
//		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
        }
  
        /********* End by Deepak Salve *****/
        
		String cellNo="";

		String pocName="";
		if(addcellNoFlag&&customerBranch!=null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0){
			logger.log(Level.SEVERE, "custbranch name ");
		
			if(customerBranch.getCellNumber1()!=null){
				cellNo=customerBranch.getCellNumber1()+"";
			}
//			else{
//				cellNo=0l;
//			}
			
			if(customerBranch.getPocName()!=null){
				pocName=customerBranch.getPocName();
			}else{
				pocName="";
			}
			logger.log(Level.SEVERE, "cust cell no and poc name "+cellNo+pocName);
			
			cellNo = serverAppUtility.getMobileNoWithCountryCode(cellNo+"", countryName, comp.getCompanyId());
			
		}else{
			if(cust.getCellNumber1()!=null){
				cellNo=cust.getCellNumber1()+"";
			}
//			else{
//				cellNo=0l;
//			}
			
			if(cust.getFullname()!=null){
				pocName=cust.getFullname();
			}else{
				pocName="";
			}
			logger.log(Level.SEVERE, "cust cell no and poc name2222 "+cellNo+pocName);
		}
		
		/**
		 * @author Vijay Date :- 22-09-2021
		 * Des :- Adding country code(from country master) in cell number.
		 */
		cellNo = serverAppUtility.getMobileNoWithCountryCode(cellNo, countryName, comp.getCompanyId());
	
		Phrase mob1Val=null;
		if(addcellNoFlag) {
			if(pocName!=null&&!pocName.equals("")){
				mob1Val = new Phrase(cellNo+" ( "+pocName+" ) ", font7);
			}else{
				mob1Val = new Phrase(cellNo+"", font7);
			}
				
		}else {
			//By Ashwini Patil to print branch cell number on contract pdf
			if(customerBranch!=null)
				cellNo=customerBranch.getCellNumber1().toString();
			
			mob1Val = new Phrase(cellNo+"", font7);
		}
		
		
		
		logger.log(Level.SEVERE, "cust cell no and poc name 3333 "+cellNo+pocName);
		
		PdfPCell mob1ValCell = new PdfPCell(mob1Val);
//		mob1ValCell.addElement(mob1Val);
		mob1ValCell.setBorder(0);
	//	mob1ValCell.setPaddingTop(2);
		mob1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/** Ends **/
		
		String gstTin2Str = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstTin2Str = cust.getArticleTypeDetails().get(i)
						.getArticleTypeName().trim();
			}
		}

		Phrase gstTin2 = new Phrase("GSTIN", font6bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		// gstTin2Cell.addElement(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font7);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		// gstTin2ValCell.addElement(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		/**
		 * Date 27/2/2018 
		 * by Jayshree 
		 * Des.To add the customer Email id
		 */
		Phrase emailph2=new Phrase("Email",font6bold);
		PdfPCell emailCell2=new PdfPCell(emailph2);
		emailCell2.setBorder(0);
		emailCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String custemailval1 = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			custemailval1 = customerBranch.getEmail();
		}

		}
		//by Ashwini Patil to print branch email on contract pdf if branch selected
		else{
			if(customerBranch!=null){	
				custemailval1 = customerBranch.getEmail();
			}
			else{
				if (cust.getEmail() != null) {
					custemailval1 = cust.getEmail();
				}else {
				custemailval1 ="";
				}
			}
		}
		
		Phrase emailphval2=new Phrase(custemailval1,font7);
		PdfPCell emailCellval2=new PdfPCell(emailphval2);
		emailCellval2.setBorder(0);
		emailCellval2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//End By Jayshree
		
	/**Date 10-4-2019 by Amol 
		 * added a kind attn field**
		 */
		Phrase kindAttn=new Phrase("Kind Attn",font6bold);
		PdfPCell kindAttnCell2=new PdfPCell(kindAttn);
		kindAttnCell2.setBorder(0);
		kindAttnCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	/**
	 * @author Ashwini Patil
	 * @since 20-1-22
	 * annexure
	 */
		Phrase referBranchAnnexure=new Phrase("Check service loactions Annexure for more details",font6bold);
		PdfPCell referBranchAnnexureCell2=new PdfPCell(referBranchAnnexure);
		referBranchAnnexureCell2.setBorder(0);
		referBranchAnnexureCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		referBranchAnnexureCell2.setColspan(3);
	
		String pocname="";
		
		
		if(pocNameFlag){
		
			if(customerBranch!=null){
				logger.log(Level.SEVERE, "custbranch name ");
			    System.out.printf("customer branch in attn",customerBranch );
				
				
				if(customerBranch.getPocName()!=null){
					pocname=customerBranch.getPocName();
				}else{
					pocname="";
				}
				logger.log(Level.SEVERE, "cust  poc name "+pocname);
				System.out.println("cust  poc name"+pocname);
				
			}
		
		}
		else{
			//By Ashwini Patil to set poc name from branch on contract pdf
			if(customerBranch!=null){
				if(customerBranch.getPocName()!=null)
					pocname=customerBranch.getPocName();
			}				
			else{
					if(cust.getFullname()!=null)
						pocname=cust.getFullname();
					else
						pocname="";
				}
			}
			
			
			logger.log(Level.SEVERE, "poc name "+pocname);
		
		Phrase kindAttnvalue=new Phrase(pocname,font7);
		PdfPCell kindAttnvalueval2=new PdfPCell(kindAttnvalue);
		kindAttnvalueval2.setBorder(0);
		kindAttnvalueval2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell_bottonaligned); //Ashwini Patil has changed coloncell
		colonTable.addCell(name2CellValCell);
		/**@Sheetal : 10-02-2022
	    Des : Adding Kind Attn just after name cell requirement by pest-o-shield**/
		
		if(pocname!=null&&!pocname.equals("")){
			colonTable.addCell(kindAttnCell2);
			colonTable.addCell(colonCell);
			colonTable.addCell(kindAttnvalueval2);
		}
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description:To create a new cell for mobile
		 */
		colonTable.addCell(mob1Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(mob1ValCell);
		/** Ends **/
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);
		
		/**
		 * Date 27/2/2018 
		 * by Jayshree 
		 * Des.To add the customer Email id
		 */
		
		if(custemailval1!=null&&!custemailval1.equals("")){
			colonTable.addCell(emailCell2);
			colonTable.addCell(colonCell);
			colonTable.addCell(emailCellval2);
		}
		
//		if(pocname!=null&&!pocname.equals("")){
//			colonTable.addCell(kindAttnCell2);
//			colonTable.addCell(colonCell);
//			colonTable.addCell(kindAttnvalueval2);
//		}
		
		//By Ashwini Patil to add branch annexure table to contract pdf if multiple brnaches are selected
		if(multipleServiceBranchesFlag){
			colonTable.addCell(referBranchAnnexureCell2);
		}
		
		
		
		
		
		//End By Jayshree
		
		
		
		/**
		 * Date 5/12/2017 By Jayshree To Remove the Gstin number
		 */

		// colonTable.addCell(gstTinCell);
		// colonTable.addCell(colonCell);
		// colonTable.addCell(gstTinValCell);
		// End by jayshree
		PdfPCell cell2 = new PdfPCell();
		cell2.setBorder(0);
		cell2.addElement(colonTable);

		Phrase state2 = new Phrase("State", font6bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		// state2Cell.addElement(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2Val = new Phrase(cust.getSecondaryAdress().getState()
				.trim(), font7);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// state2ValCell.addElement(state2Val);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stsecCo = "";
		if (stateList != null) {
			for (int i = 0; i < stateList.size(); i++) {
				if (stateList
						.get(i)
						.getStateName()
						.trim()
						.equalsIgnoreCase(
								cust.getSecondaryAdress().getState().trim())) {
					stsecCo = stateList.get(i).getStateCode().trim();
					break;
				}
			}
		}

		Phrase state2Code = new Phrase("State Code", font6bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		// state2CodeCell.addElement(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(stsecCo, font7);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		// state2CodeValCell.addElement(state2CodeVal);
		// state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable state2table = new PdfPTable(2);
		state2table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);
		PdfPCell state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		colonTable = new PdfPTable(4);
		colonTable.setWidthPercentage(100);
		// float[] columnStateCodeCollonWidth = {35,5,15,45};
		try {
			colonTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2CodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2CodeValCell);
		colonTable.addCell(blakCell);

		state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		PdfPCell state2TableCell = new PdfPCell(state2table);
		state2TableCell.setBorder(0);
		// state2TableCell.addElement(state2table);

		part2Table.addCell(cell2);

		/**
		 * Date 5/12/2017 By jayshree Des.To remove the state, state code,Gstin
		 * Value
		 */
		// part2Table.addCell(state2TableCell);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);//Comment by jayshree
		/* Part 2 Ends */

		Phrase blankCell = new Phrase(" ", font7);

		/**
		 * Date 5/12/2017 Dev.By Jayshree Des.To add the Service address and
		 * billing address heading in table
		 */

		Phrase billingadd = new Phrase("Billing Address", font8bold);
		PdfPCell billingCell = new PdfPCell(billingadd);
		billingCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase serviceadd = new Phrase("Service Address", font8bold);
		PdfPCell serviceCell = new PdfPCell(serviceadd);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		mainTable.addCell(billingCell);
		mainTable.addCell(serviceCell);
		// End by jayshree
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);

		Quotation quotation = (Quotation) qp;
		if(quotation.isDonotprintServiceAddress()){
			PdfPTable mainTable2 = new PdfPTable(1);
			mainTable2.setWidthPercentage(100);

			mainTable2.addCell(billingCell);
			mainTable2.addCell(part1TableCell);
			mainTable2.addCell(blankCell);
			try {
				document.add(mainTable2);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		else{
			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
			
//		try {
//			document.add(mainTable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}

	private String toCamelCase(String s) {
        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts){
            if(part!=null && part.trim().length()>0)
           camelCaseString = camelCaseString + toProperCase(part);
            else
                camelCaseString=camelCaseString+part+" ";   
        }
        return camelCaseString;
     }

	private String toProperCase(String s) {
        String temp=s.trim();
        String spaces="";
        if(temp.length()!=s.length())
        {
        int startCharIndex=s.charAt(temp.indexOf(0));
        spaces=s.substring(0,startCharIndex);
        }
        temp=temp.substring(0, 1).toUpperCase() +
        spaces+temp.substring(1).toLowerCase()+" ";
        return temp;

    }


	private void createProductDetails() {
		PdfPTable productTable = new PdfPTable(16);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column16CollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase srNophrase = new Phrase("Sr No", font8bold);
		PdfPCell srNoCell = new PdfPCell();
		srNoCell.addElement(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

		Phrase servicePhrase = new Phrase("Services", font8bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2

		Phrase hsnCode = new Phrase("SAC", font8bold);
		PdfPCell hsnCodeCell = new PdfPCell();
		hsnCodeCell.addElement(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 3

		Phrase UOMphrase = new Phrase("UOM", font8bold);
		PdfPCell UOMphraseCell = new PdfPCell();
		UOMphraseCell.addElement(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 4

		
		
		Phrase qtyPhrase = new Phrase(qtylabel, font8bold);
		PdfPCell qtyPhraseCell = new PdfPCell();
		qtyPhraseCell.addElement(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 5

		Phrase ratePhrase = new Phrase("Rate", font8bold);
		PdfPCell ratePhraseCell = new PdfPCell();
		ratePhraseCell.addElement(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 6

		Phrase amountPhrase = new Phrase("Amount", font8bold);
		PdfPCell amountPhraseCell = new PdfPCell();
		amountPhraseCell.addElement(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 7

		Phrase dicphrase = new Phrase("Disc", font8bold);
		PdfPCell dicphraseCell = new PdfPCell();
		dicphraseCell.addElement(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 8

		Phrase taxValPhrase = new Phrase("Ass Val", font8bold);
		PdfPCell taxValPhraseCell = new PdfPCell();
		taxValPhraseCell.addElement(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 9

		// PdfPTable cgstcellTable=new PdfPTable(1);
		// cgstcellTable.setWidthPercentage(100);

		Phrase cgstphrase = new Phrase("CGST", font8bold);
		PdfPCell cgstphraseCell = new PdfPCell(cgstphrase);
		// cgstphraseCell.addElement(cgstphrase);
		// cgstphraseCell.setBorder(0);
		cgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// cgstcellTable.addCell(cgstphraseCell);
		cgstphraseCell.setColspan(2);
		// cgstphraseCell.setRowspan(2);

		Phrase sgstphrase = new Phrase("SGST", font8bold);
		PdfPCell sgstphraseCell = new PdfPCell(sgstphrase);
		// sgstphraseCell.setBorder(0);
		sgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// sgstphraseCell.addElement(sgstphrase);
		// sgstcellTable.addCell(sgstphraseCell);
		sgstphraseCell.setColspan(2);
		// sgstphraseCell.setRowspan(2);

		Phrase igstphrase = new Phrase("IGST", font8bold);
		PdfPCell igstphraseCell = new PdfPCell(igstphrase);
		// igstphraseCell.setBorder(0);
		igstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// igstphraseCell.addElement(igstphrase);
		// igstcellTable.addCell(igstphraseCell);
		igstphraseCell.setColspan(2);
		// igstphraseCell.setRowspan(2);

		Phrase totalPhrase = new Phrase("Total", font8bold);
		PdfPCell totalPhraseCell = new PdfPCell();
		totalPhraseCell.addElement(totalPhrase);
		totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		totalPhraseCell.setRowspan(2);// 2

		Phrase cgstpercentphrase = new Phrase("%", font6bold);
		PdfPCell cgstpercentphraseCell = new PdfPCell();
		// cgstpercentphraseCell.setBorderWidthBottom(0);
		// cgstpercentphraseCell.setBorderWidthTop(0);
		// cgstpercentphraseCell.setBorderWidthLeft(0);
		cgstpercentphraseCell.addElement(cgstpercentphrase);
		// innerCgstTable.addCell(cgstpercentphraseCell);

		Phrase cgstamtphrase = new Phrase("Amt", font6bold);
		PdfPCell cgstamtphraseCell = new PdfPCell();
		cgstamtphraseCell.addElement(cgstamtphrase);
		//

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(hsnCodeCell);
		productTable.addCell(UOMphraseCell);
		productTable.addCell(qtyPhraseCell);
		productTable.addCell(ratePhraseCell);
		productTable.addCell(amountPhraseCell);
		productTable.addCell(dicphraseCell);
		productTable.addCell(taxValPhraseCell);

		productTable.addCell(cgstphraseCell);
		productTable.addCell(sgstphraseCell);
		productTable.addCell(igstphraseCell);

		productTable.addCell(totalPhraseCell);

		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createContractDetails(Contract conEntity) {

		float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		float[] columnHalfWidth = { 1f, 1f };
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = { 30, 5, 15, 20, 5, 25 };// By Jayshree
																	// to set
																	// the
																	// column
																	// width
		PdfPTable colonTable = new PdfPTable(6);// By Jayshree to set the column
												// width
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseCharge = new Phrase("Reverse Charge(Y/N)", font6bold);
		PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseChargeVal = new Phrase("No", font7);
		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);

		if (conEntity.getRefNo() != null && !contEntity.getRefNo().equals("")) {
			Phrase RefNo = new Phrase("Ref No", font6bold);
			PdfPCell RefNoCell = new PdfPCell(RefNo);
			RefNoCell.setBorder(0);
			RefNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase RefNoVal = new Phrase(conEntity.getRefNo(), font7);
			PdfPCell RefNoValCell = new PdfPCell(RefNoVal);
			RefNoValCell.setBorder(0);
			RefNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			colonTable.addCell(RefNoCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(RefNoValCell);
		}

		PdfPCell pdfCell = new PdfPCell();
		pdfCell.setBorder(0);
		pdfCell.addElement(colonTable);

		part1Table.addCell(pdfCell);

		Phrase state = new Phrase("State", font6bold);
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(), font7);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCode = new Phrase("State Code", font6bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(" ", font7);
		PdfPCell stateCodeValCell = new PdfPCell();
		stateCodeValCell.addElement(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);

		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);

		PdfPCell stateTableCell = new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		
		
		Phrase startDate = new Phrase("Contract Period", font6bold);
		PdfPCell startDateCell = new PdfPCell(startDate);
		// startDateCell.addElement(startDate);//comment by jayshree
		startDateCell.setBorder(0);
		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// sdf.format(q.getContractStartDate())
		
		Date conStartDate=conEntity.getStartDate();
		if(thaiPdfFlag){	
			logger.log(Level.SEVERE,"In thai pdf flag. conStartDate="+conEntity.getStartDate());				
			
					
			if(conEntity.getItems()!=null&&conEntity.getItems().get(0).getStartDate()!=null&&conEntity.getItems().get(0).getStartDate().getHours()>=22&&conEntity.getItems().get(0).getStartDate().getMinutes()>=30) {
				Calendar ct = Calendar.getInstance();
				ct.setTime(conEntity.getStartDate());
				ct.add(Calendar.MINUTE, 660); //since contract start date always have 1pm time. Need to add 11 hours to it
				conStartDate=ct.getTime();
				logger.log(Level.SEVERE,"conStartDate after adding 660mins="+conStartDate+" formatted startdate="+fmt.format(conStartDate));
				
			}
			
		}
		
		Phrase startDateVal = new Phrase(fmt.format(conStartDate),
				font7);
		PdfPCell startDateValCell = new PdfPCell(startDateVal);
		// startDateValCell.addElement(startDateVal);//comment by jayshree
		startDateValCell.setBorder(0);
		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDate = new Phrase(" To ", font6bold);
		PdfPCell endDateCell = new PdfPCell(endDate);
		// endDateCell.addElement(endDate);//comment by jayshree
		endDateCell.setBorder(0);
		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Date conEndDate=conEntity.getEndDate();
		if(thaiPdfFlag){	
			logger.log(Level.SEVERE,"In thai pdf flag. conEndDate="+conEntity.getEndDate());				
			
			
			//since contract start date always have 1pm time. Need to add 11 hours to it
			
			if(conEntity.getItems()!=null&&conEntity.getItems().get(0).getEndDate()!=null&&conEntity.getItems().get(0).getEndDate().getHours()>=22&&conEntity.getItems().get(0).getEndDate().getMinutes()>=30) {
				Calendar ct = Calendar.getInstance();
				ct.setTime(conEntity.getEndDate());
				ct.add(Calendar.MINUTE, 660);
				conEndDate=ct.getTime();
			}
			logger.log(Level.SEVERE,"conEndDate after adding 660mins="+conEndDate+" formatted startdate="+fmt.format(conEndDate));						
		}		
		
		Phrase endDateVal = new Phrase(fmt.format(conEndDate),
				font7);
		PdfPCell endDateValCell = new PdfPCell(endDateVal);
		// endDateValCell.addElement(endDateVal);//comment by jayshree
		endDateValCell.setBorder(0);
		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable billperiodtable = new PdfPTable(2);
		billperiodtable.setWidthPercentage(100);

		PdfPTable billFromcolonTable = new PdfPTable(3);
		billFromcolonTable.setWidthPercentage(100);
		try {
			billFromcolonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable billTocolonTable = new PdfPTable(3);
		billTocolonTable.setWidthPercentage(100);
		try {
			billTocolonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPCell fromDate = new PdfPCell(billFromcolonTable);
		fromDate.setBorder(0);

		billperiodtable.addCell(fromDate);
		fromDate = new PdfPCell(billTocolonTable);// By jayshree 7/12/2017
													// comment this
		fromDate.setBorder(0);
		// fromDate.addElement(billTocolonTable);//By jayshree 7/12/2017 comment
		// this
		billperiodtable.addCell(fromDate);
		/**
	 * 
	 */
		PdfPTable periodtable = new PdfPTable(2);
		periodtable.setWidthPercentage(100);
		float[] columnrohanrrCollonWidth = { 2.8f, 0.2f, 7.7f };
		PdfPTable concolonTable = new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(startDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(startDateValCell);

		PdfPCell startcolonTableCell = new PdfPCell(colonTable);
		startcolonTableCell.setBorder(0);
		periodtable.addCell(startcolonTableCell);

		float[] columnDate111CollonWidth = { 0.5f, 0.2f, 3.2f };
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDate111CollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(endDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(endDateValCell);

		PdfPCell endcolonTableCell = new PdfPCell(colonTable);
		endcolonTableCell.setBorder(0);
		// endcolonTableCell.addElement(colonTable);//By jayshree 7/12/2017
		// comment this
		periodtable.addCell(endcolonTableCell);

		PdfPCell periodTableCell = new PdfPCell(periodtable);
		periodTableCell.setBorder(0);
		// periodTableCell.addElement(periodtable);//By jayshree 7/12/2017
		// comment this

		PdfPCell concolonTableCell = new PdfPCell(concolonTable);
		concolonTableCell.setBorder(0);
		// concolonTableCell.addElement(concolonTable);//By jayshree 7/12/2017
		// comment this

		PdfPCell billperiodtableCell = new PdfPCell(billperiodtable);
		billperiodtableCell.setBorder(0);
		// billperiodtableCell.addElement(billperiodtable);//By jayshree
		// 7/12/2017 comment this

		part2Table.addCell(concolonTableCell);
		part2Table.addCell(periodTableCell);
		part2Table.addCell(billperiodtableCell);

		PdfPCell part1Cell = new PdfPCell(part1Table);
		part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement(part1Table);//By jayshree 7/12/2017 comment this

		mainTable.addCell(part1Cell);

		part1Cell = new PdfPCell(part2Table);
		// part1Cell.addElement(part2Table);//By jayshree 7/12/2017 comment this

		if (contEntity!=null&&conEntity.getGroup() != null && !contEntity.getGroup().equals("")) {
			Phrase conType = new Phrase("Contract Type ", font6bold);
			PdfPCell conTypeCell = new PdfPCell(conType);
			conTypeCell.setBorder(0);
			conTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase conTypeValue = new Phrase(conEntity.getGroup(), font7);
			PdfPCell conTypeValueCell = new PdfPCell(conTypeValue);
			conTypeValueCell.setBorder(0);
			conTypeValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			float[] columnDaterrCollonWidth = { 1.3f, 0.2f, 3.5f };
			colonTable = new PdfPTable(3);
			colonTable.setWidthPercentage(100);
			try {
				colonTable.setWidths(columnDaterrCollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			colonTable.addCell(conTypeCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(conTypeValueCell);

			PdfPCell myTableCell = new PdfPCell(colonTable);
			myTableCell.setBorder(0);
			part2Table.addCell(myTableCell);

		}
		mainTable.addCell(part1Cell);

		Phrase billingAddress = new Phrase("Billing Address", font8bold);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		// mainTable.addCell(billAdressCell);//Comment by jayshree 7/12/2017
		Phrase serviceaddress = new Phrase("Service Address", font8bold);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// mainTable.addCell(serviceCell);//Comment By jayshree 7/12/2017
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	/**
	 * Date 29/11/2017 Dev.Jayshree Des.to change the format of email Contract
	 * And quotation Pdf Changes are done
	 */
	// public void createPdfFormatForEmail(Sales qp1,Customer cust,Company
	// comp1,Quotation quotEntity,
	// Contract contEntity,CompanyPayment comppayment)
	// {
	//
	// String preprint="Plane";
	// this.qp=qp1;
	// this.cust=cust;
	// this.comp=comp1;
	// this.quotEntity=quotEntity;
	// this.contEntity=contEntity;
	// this.comppayment=comppayment;
	//
	// this.products = qp.getItems();
	// System.out.println("product222"+ qp.getItems());
	// this.prodCharges = qp.getProductCharges();
	// this.prodTaxes = qp.getProductTaxes();
	// this.payTermsLis = qp.getPaymentTermsList();
	// this.articletype=comp.getArticleTypeDetails();
	// ///*****************
	// if (qp instanceof Contract) {
	//
	// if (upcflag == false && preprint.equals("plane")) {
	// createCompanyHedding();
	// } else {
	//
	// if (preprint.equals("yes")) {
	//
	// createBlankforUPC();
	// }
	//
	// if (preprint.equals("no")) {
	//
	//
	// if (comp.getUploadHeader() != null) {
	// createCompanyNameAsHeader(document, comp);
	// }
	//
	// if (comp.getUploadFooter() != null) {
	// createCompanyNameAsFooter(document, comp);
	// }
	// createBlankforUPC();
	// }
	// }
	//
	// } else {
	//
	// if (upcflag == false && preprint.equals("plane")) {
	//
	// createCompanyHedding();
	// } else {
	//
	// if (preprint.equals("yes")) {
	//
	//
	// createBlankforUPC();
	//
	// }
	// if (preprint.equals("no")) {
	//
	// if (comp.getUploadHeader() != null) {
	// createCompanyNameAsHeader(document, comp);
	// }
	//
	// if (comp.getUploadFooter() != null) {
	// createCompanyNameAsFooter(document, comp);
	// }
	// createBlankforUPC();
	// }
	//
	// }
	//
	// }
	//
	// if (printProductPremisesFlag)
	// {
	// createProductWithoutDiscDetail();
	//
	// } else
	// {
	// createProductWithoutDiscDetail();
	//
	// }
	// termsConditionsInfo();
	// footerInfo();
	//
	// }
	// End By Jayshree
	
	
	public void createProductDescription(String preprintStatus) {
		/**
		 * Date : 27-07-2017 By ANIL
		 */
		if(!productDescFlag){
			return;
		}
		/**
		 * End
		 */
		Phrase prodDetailsHeader = new Phrase("Product Details ", font12boldul);
		Paragraph productDeatilsLblPara = new Paragraph();
		productDeatilsLblPara.add(prodDetailsHeader);
		productDeatilsLblPara.add(Chunk.NEWLINE);
		productDeatilsLblPara.add(Chunk.NEWLINE);
		productDeatilsLblPara.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
					
		try {
			document.add(Chunk.NEXTPAGE);
			if (preprintStatus.equals("yes")) {
				try {
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
				} catch (Exception e) {
				}
					
			}
			
			
			if (preprintStatus.equals("no")) {
				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}
				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				
				if(comp.getUploadHeader()!=null && comp.getUploadFooter() != null){
					try {
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
					} catch (Exception e) {
					}
				}
			}
			document.add(productDeatilsLblPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		
		for (int i = 0; i < this.stringlis.size(); i++) {
			
			PdfPTable prodDescriptionTbl = new PdfPTable(2);
			prodDescriptionTbl.setWidthPercentage(100);

			try {
				prodDescriptionTbl.setWidths(new float[] { 40, 60 });
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			Phrase productIdLbl = new Phrase("Product Id : "+ stringlis.get(i).getCount(), font10bold);
			PdfPCell productIdLblCell = new PdfPCell(productIdLbl);
			productIdLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			productIdLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productIdLblCell.setBorder(0);
			prodDescriptionTbl.addCell(productIdLblCell);

			String prodNameValue = stringlis.get(i).getProductName();
			Phrase prodNameLbl = new Phrase("Product Name : " + prodNameValue,font10bold);
			PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
			prodlblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodlblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodlblCell.setBorder(0);

			prodDescriptionTbl.addCell(prodlblCell);

			String prodDescriptionValue = "";
			
			/**
			 * @author Anil,Date : 18-02-2019
			 * issue : product description was not getting printed for service quotation pdf
			 * for bitco raised by Rahul Tiwari
			 */
			System.out.println("INSIDE PRODUCT DESCRIPTION : ");
			SuperProduct prod= stringlis.get(i);
			if(prod.getComment()!=null){
				prodDescriptionValue=prodDescriptionValue+prod.getComment();
				System.out.println("1 :"+prodDescriptionValue);
			}
			
			if(prod.getCommentdesc()!=null){
				prodDescriptionValue=prodDescriptionValue+prod.getCommentdesc();
				System.out.println("2 :"+prodDescriptionValue);
			}
			
			if(prod.getCommentdesc1()!=null){
				prodDescriptionValue=prodDescriptionValue+prod.getCommentdesc1();
				System.out.println("3 :"+prodDescriptionValue);
			}
			
			if(prod.getCommentdesc2()!=null){
				prodDescriptionValue=prodDescriptionValue+prod.getCommentdesc2();
				System.out.println("4 :"+prodDescriptionValue);
			}
			System.out.println("PRODUCT DESCRIPTION : "+prodDescriptionValue);

			Paragraph value = new Paragraph(prodDescriptionValue,font9);
			value.setAlignment(Element.ALIGN_LEFT);

			try {
				document.add(prodDescriptionTbl);
				document.add(value);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

		}

	}
	

	private void createTermsAndConditionPage(Document document,ArrayList<TermsAndConditions> termsAndConditionlist, String documentName, long companyId,String preprintStatus) {
		
		logger.log(Level.SEVERE,"in createTermAndConditionsPage");
		PdfUtility pdfUtility=new PdfUtility();
		Font font10bold,font8;
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font10bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		boolean printTerms=false;
		// TODO Auto-generated method stub
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		List<TermsAndConditions> terms = ofy().load().type(TermsAndConditions.class)
				.filter("companyId",companyId)
				.filter("document", documentName)
				.filter("status", true).list();
		
		if(terms!=null){			
			
			logger.log(Level.SEVERE,"terms size=" +terms.size());
//			
			Comparator<TermsAndConditions> sequenceComparator=new Comparator<TermsAndConditions>() {
				@Override
				public int compare(TermsAndConditions arg0, TermsAndConditions arg1) {
					return arg0.getSequenceNumber().compareTo(arg1.getSequenceNumber());
				}
			};
			Collections.sort(terms, sequenceComparator);
			
			for(TermsAndConditions termsCondition : termsAndConditionlist){
				
				for(TermsAndConditions term:terms){
					System.out.println("termsCondition.getTitle().trim() "+termsCondition.getTitle().trim());
					System.out.println("term.getTitle().trim()"+term.getTitle().trim());
					logger.log(Level.SEVERE,"term.getTitle().trim()"+term.getTitle().trim());
					System.out.println("termsCondition.getTitle().trim().equals(term.getTitle().trim()) "+termsCondition.getTitle().trim().equals(term.getTitle().trim()));
					if(termsCondition.getTitle().trim().equals(term.getTitle().trim())){
						printTerms=true;
						table.addCell(pdfUtility.getCell(term.getTitle(), font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);;
						table.addCell(pdfUtility.getCell(term.getMsg(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);	
						table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);	
					}
									
				}
			}
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
						
			
			
			if(printTerms) {
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				if (preprintStatus.equals("yes")) {
					try {
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
					} catch (Exception e) {
					}
						
				}
				
				
				if (preprintStatus.equals("no")) {
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}
					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					
					if(comp.getUploadHeader()!=null && comp.getUploadFooter() != null){
						try {
							document.add(blank);
							document.add(blank);
							document.add(blank);
							document.add(blank);
							document.add(blank);
							document.add(blank);
						} catch (Exception e) {
						}
					}
				}
			}
			
			try {
				document.add(table);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else
			return;
		
		
	}

	//Ashwini Patil Date:27-06-2023
	public double calculateTotalPrice(SalesLineItem object){
		double total = 0;
		SuperProduct product = object.getPrduct();
		double tax = removeAllTaxes(product);
		double origPrice = object.getPrice() - tax;
		
		
		if (object.isServiceRate()&&(object.getArea()==null||object.getArea().equals("")||object.getArea().equalsIgnoreCase("NA"))) {
			origPrice=origPrice*object.getNumberOfServices();
		}

		

		if ((object.getPercentageDiscount() == null && object.getPercentageDiscount() == 0)&& (object.getDiscountAmt() == 0)) {

			System.out.println("inside both 0 condition");
		
			System.out.println("Get value from area =="+ object.getArea());
			System.out.println("total amount before area calculation =="+ total);
			if (!object.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(object.getArea());
				total = origPrice * area;
				System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+ total);
			} else {
				total = origPrice;
			}
		}
		else if ((object.getPercentageDiscount() != null)&& (object.getDiscountAmt() != 0)) {

			System.out.println("inside both not null condition");

		

			if (!object.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(object.getArea());
				total = origPrice * area;
				total = total- (total * object.getPercentageDiscount() / 100);
//				System.out.println("after discount per total === "+ total);
				total = total - object.getDiscountAmt();
//				System.out.println("after discount AMT total === "+ total);
//				System.out.println(" Final TOTAL   discount per &  discount Amt ==="+ total);
			} else {
//				System.out.println(" normal === total " + total);
				total = origPrice;
				total = total- (total * object.getPercentageDiscount() / 100);
				total = total - object.getDiscountAmt();
			}

		} else {
			System.out.println("inside oneof the null condition");

			if (object.getPercentageDiscount() != null) {
				// System.out.println("inside getPercentageDiscount oneof the null condition");
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);

				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
//					System.out.println("total before discount per ===="+ total);
					total = total- (total * object.getPercentageDiscount() / 100);
//					System.out.println("after discount per total === "+ total);
				} else {
//					System.out.println("old code");
					total = origPrice;
					total = total- (total * object.getPercentageDiscount() / 100);
				}
			} else {
				// System.out.println("inside getDiscountAmt oneof the null condition");
				// total=origPrice-object.getDiscountAmt();

				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
//					System.out.println("total before discount amt ===="+ total);
					total = total - object.getDiscountAmt();
//					System.out.println("after discount amt total === "+ total);

				} else {
					total = total - object.getDiscountAmt();
				}
			}

			// total=total*object.getQty();

		}
		return total;
	}
	
	
}
