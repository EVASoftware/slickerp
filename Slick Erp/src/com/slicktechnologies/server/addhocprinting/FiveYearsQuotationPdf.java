package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
public class FiveYearsQuotationPdf {

	public Document document;
	Company comp;
	Customer cust;
	Quotation fy;
	Phrase blank;
	Phrase colon;
	
//	String str;
	String title;
	
	Phrase chunk;
	PdfPCell pdfno, pdftreatment, pdfpest, pdffrequency, pdfpremise, pdfprice, pdfduration, pdfdiscount;
	// PdfPCell pdfdays, pdfpercent, pdfcomment;
	PdfPCell pdfsubttl,  pdfttlamt;

	// float[] colwidth = { 1.5f, 0.2f, 3.0f, 1.5f, 0.2f, 3.0f };
	float[] colwidth = { 1.0f, 0.1f, 7.0f, 1.5f, 0.1f, 1.5f };
	float[] colwidth123 = {7.0f,1.42f};
	float[] col1width = { 1.0f, 0.1f, 1.0f, 4.0f, 1.0f, 0.1f, 1.0f };
	// float[] tblcol1width = { 1.5f, 1.5f, 7.0f };
	float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f };
	float[] tblcowidth = { 1.5f, 0.2f, 7.f };
	

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,
			font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt12 = new SimpleDateFormat("MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");

	private PdfPCell custlandcell;
	
	
	public FiveYearsQuotationPdf()
	{
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		
	}
	
	public void setfiveyearsquotation(Long count) {
		
		
		System.out.println("in side setfiveyearsquotation method");
		
		
		fy = ofy().load().type(Quotation.class).id(count).now();

		if (fy.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", fy.getCustomerId())
					.filter("companyId", fy.getCompanyId()).first().now();

		if (fy.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", fy.getCustomerId()).first().now();

		if (fy.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", fy.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}
	

	public void createPdf(String status) {
		
		
		System.out.println("status value "+status);
		if(status.equals("yes"))
		{
			createBlankHeading();
			createLogo(document,comp);
		}
		else if(status.equals("no"))
		{
			createBlankHeading();
		    	if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
		}
		
		
		createTitleHeading();
		createReferenceHeading();
		createCustomerDetailsHeading();
		createQuotationTable();
		createOPCSHeading();
		createNoteHeading();
		
		
		
	}

	
    private void createLogo(Document doc, Company comp) {
			//********************logo for server ********************
				DocumentUpload document =comp.getLogo();
				//patch
				String hostUrl; 
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
			}
				
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scalePercent(20f);
					image2.setAbsolutePosition(40f,765f);	
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
			}			
	}	
	
private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	

	public void createBlankHeading() {
		
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
			blank.add(Chunk.NEWLINE);

			try {
				document.add(blank);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			PdfPCell blankcell = new PdfPCell(blank);
			blankcell.setBorder(0);
			blankcell.setBorder(0);

			PdfPTable blanktable = new PdfPTable(1);
			blanktable.addCell(blankcell);
			blanktable.addCell(blankcell);
			blanktable.setWidthPercentage(100f);

			try {
				document.add(blanktable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	public void createTitleHeading(){
		
		String title = "Quotation";
		Phrase quotation = new Phrase(title, font16boldul);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase blank = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blank);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);

//		PdfPCell cell1 = new PdfPCell();
//		cell1.addElement(qtable);
//		cell1.setBorder(0);
//
//		PdfPTable table1 = new PdfPTable(1);
//		table1.addCell(cell1);
//		table1.setWidthPercentage(100);

		try {
			document.add(qtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void createReferenceHeading(){
		
		Phrase ref = new Phrase("Ref. no.", font8);
		PdfPCell refcell = new PdfPCell(ref);
		refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase r = null;

		if (fy.getReferenceNumber() != null) {
			r = new Phrase("" + fy.getReferenceNumber(), font8);
		}

		PdfPCell rcell = new PdfPCell(r);
		rcell.setBorder(0);
		rcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":", font8);
		PdfPCell colcell = new PdfPCell(colon);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date", font8);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase dt = null;
		if (fy.getReferenceDate() != null) {
			dt = new Phrase(fmt.format(fy.getQuotationDate()), font8);
		}

		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		PdfPTable rdttable = new PdfPTable(6);
		rdttable.addCell(refcell);
		rdttable.addCell(colcell);
		rdttable.addCell(rcell);
		rdttable.addCell(datecell);
		rdttable.addCell(colcell);
		rdttable.addCell(dtcell);
		rdttable.setWidthPercentage(100f);
		
		try {
			rdttable.setWidths(colwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			document.add(rdttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}

	public void createCustomerDetailsHeading(){
		
		Phrase to = new Phrase("To", font8);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setBorder(0);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";
		
		
		Phrase designation = null;
		PdfPCell designationCell = null;
		if(cust.getCustCorresponence()!=null)
		{
			designation = new Phrase(cust.getCustCorresponence(),font8);
			
			designationCell = new PdfPCell(designation);
			designationCell.setBorder(0);
		}
//		else
//		{
//			designation = new Phrase(" " ,font8);
//			
//			designationCell = new PdfPCell(designation);
//			designationCell.setBorder(0);
//		}
		
		Phrase custName = null;
		if(cust.isCompany()==true)
		{
			custName = new Phrase(cust.getCompanyName(),font8);
		}
		else
		{
			custName = new Phrase(cust.getFullname(),font8);
		}
		PdfPCell custNameCell =  new PdfPCell(custName);
		custNameCell.setBorder(0);
		

		if (cust.getSecondaryAdress() != null) {

	if(!cust.getSecondaryAdress().getAddrLine2().equals("")){
				
				if(!cust.getSecondaryAdress().getLandmark().equals("")){
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+"\n"+cust.getSecondaryAdress().getAddrLine2()+"\n"+cust.getSecondaryAdress().getLandmark();
				}else{
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+"\n"+cust.getSecondaryAdress().getAddrLine2();
				}
			}else{
				if(!cust.getSecondaryAdress().getLandmark().equals("")){
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+"\n"+cust.getSecondaryAdress().getLandmark();
				}else{
					custAdd1=cust.getSecondaryAdress().getAddrLine1();
				}
			}
			
			if(!cust.getSecondaryAdress().getLocality().equals("")){
				custFullAdd1=custAdd1+","+cust.getSecondaryAdress().getLocality()+"\n"+cust.getSecondaryAdress().getCity()+"-"+cust.getSecondaryAdress().getPin()+","+cust.getSecondaryAdress().getState()+","+cust.getSecondaryAdress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getSecondaryAdress().getCity()+"-"+cust.getSecondaryAdress().getPin()+","+cust.getSecondaryAdress().getState()+","+cust.getSecondaryAdress().getCountry();
			}
		
		}
/**********************************************************************************/
		
		Phrase mobno = null;
		if (cust.getLandline() != null && cust.getLandline()!=0) {
			mobno = new Phrase("Tel No. : " + cust.getCellNumber1()+" ,022-"+cust.getLandline(), font8);
		}
		else
		{
			mobno = new Phrase("Tel No. : " + cust.getCellNumber1(),font8);
		}
		
		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase pname = null;
		PdfPCell pnamecell = null;
		if(cust.isCompany()==true){
			if(!fy.getDesignation().equals(""))
			{
				pname = new Phrase("CTC: " + cust.getFullname()+" - "+fy.getDesignation(), font8);
			}
			else
			{
				pname = new Phrase("CTC: " + cust.getFullname(), font8);
			}

			pnamecell = new PdfPCell(pname);
			pnamecell.setBorder(0);
			pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		
		
		
		PdfPCell cellNo2=null;
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
		{
			Phrase phNo2 = new Phrase("Cell No. : "+cust.getCellNumber2(),font8); 
			cellNo2 = new PdfPCell(phNo2);
			cellNo2.setBorder(0);
		}
		
		
		/*************************************************************************************/
		Phrase sub=null;
		if(fy.getPremisesDesc()!=null)
		{	
		 sub = new Phrase(
				"Sub: Quotation for Termite Management Services for "+fy.getPremisesDesc()+" .",
				font8);
		}
		else
		{
			sub = new Phrase(
					"Sub: Quotation for Termite Management Services.",
					font8);
		}
		PdfPCell subcell = new PdfPCell(sub);
		subcell.setBorder(0);
		subcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase custAddInfo = new Phrase(custFullAdd1, font8);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);

		Phrase dear = new Phrase("Dear Sir / Madam" + "," + "\n", font8);
		PdfPCell dearcell = new PdfPCell(dear);
		dearcell.setBorder(0);
		dearcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Date endDt=null; 
		Date dummyDtVal=null;
		Date actualStartDate=null;
		Date actualEndDat=null;
		for(int i=0;i<fy.getItems().size();i++)
		{
			
				Calendar calendar = Calendar.getInstance();
				
				actualStartDate=fy.getItems().get(i).getStartDate();
				System.out.println("start date "+fy.getItems().get(i).getStartDate());
				
				
				
				calendar.setTime(fy.getItems().get(i).getStartDate());
				calendar.add(Calendar.DATE, fy.getItems().get(i).getDuration());
//				CalendarUtil.addDaysToDate(fy.getItems().get(i).getStartDate(),fy.getItems().get(i).getDuration());
				
				endDt=calendar.getTime();
				System.out.println("end date "+endDt);
				if(dummyDtVal==null)
				{
					dummyDtVal=endDt;
				}
				
				else if(dummyDtVal.compareTo(endDt)==1)
				{
					System.out.println("nothing dummy val is "+dummyDtVal);
				}
				else 
				{
					dummyDtVal=endDt;
					
					System.out.println("in side else "+dummyDtVal);
				}
				
				
		}	
		
				
		
		

		Phrase msg = new Phrase(
				"With reference to your enquiry, undersigned has carried out inspection of your Premise by our representative "+fy.getInspectedBy() +" on Dated "+ fmt.format(fy.getInspectionDate())+"to identify types and sources of pests and severity of infestation. We would like to quote our best rates for the pest management services for your "+fy.getPremisesDesc()+". The Contract period for five years from "+fmt12.format(actualStartDate)+" to "+fmt12.format(dummyDtVal)+" Following are our recommended services along with their respective charges. ",
				font8);
		PdfPCell msgcell = new PdfPCell(msg);
		msgcell.setBorder(0);
		msgcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase gd = new Phrase("General Management(G.D.): ",font10bold);
//        PdfPCell gdcell = new PdfPCell(gd);
//		gdcell.setBorder(0);
//		gdcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable cdtable = new PdfPTable(1);
		cdtable.addCell(toCell);
		if(cust.getCustCorresponence()!=null)
		{
		cdtable.addCell(designationCell);
		}
		cdtable.addCell(custNameCell);
		cdtable.addCell(custAddInfoCell);
		if (cust.isCompany() == true) {
		cdtable.addCell(pnamecell);
		}
		cdtable.addCell(mobcell);
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
		{
			cdtable.addCell(cellNo2);
		}
		cdtable.addCell(blankCell);
		cdtable.addCell(subcell);
		cdtable.addCell(blankCell);
		cdtable.addCell(dearcell);
		cdtable.addCell(msgcell);
//		cdtable.addCell(gdcell);
		
		cdtable.setWidthPercentage(100f);

		PdfPCell cell3 = new PdfPCell(cdtable);
		cell3.setBorder(0);

		PdfPTable table3 = new PdfPTable(1);
		table3.addCell(cell3);
		table3.setWidthPercentage(100);

		try {
			document.add(table3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	public void createOPCSHeading(){
		
		Phrase bl = new Phrase(" ",font8);
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);
		
		Phrase opcs = new Phrase("OPCS Assurance",font8bold);
		PdfPCell opcscell = new PdfPCell(opcs); 
		opcscell.setBorder(0);
		opcscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":");
		PdfPCell colcell = new PdfPCell(colon); 
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase line = new Phrase("In case of pest resurgence during the contarct period, interim calls, if any, would be attended to, without any extra cost. ",font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setBorder(0);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable otable = new PdfPTable(3);
		otable.addCell(opcscell);
		otable.addCell(colcell);
		otable.addCell(linecell);
		otable.setWidthPercentage(100f);
		
		try {
			otable.setWidths(tblcowidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		PdfPCell ocell = new PdfPCell(otable);
//		ocell.setBorder(0);
//		
//		PdfPTable table4 = new PdfPTable(1);
//		table4.addCell(ocell);
//		table4.setWidthPercentage(100);
		
		try {
			document.add(otable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createNoteHeading(){
		
		PdfPCell descCell=null;
		PdfPCell bnoteCell=null;
		if(fy.getDescription()!=null){
			
			Phrase bnote = new  Phrase("Notes : ", font8bold);
			bnoteCell = new PdfPCell(bnote);
			bnoteCell.setBorder(0);
			
			Phrase description =new Phrase(fy.getDescription(),font8);
			descCell = new PdfPCell(description);
			descCell.setBorder(0);
		}
		
//		
//		
//		PdfPCell abcCell = new PdfPCell(abc);
//		abcCell.setBorder(0);
//		abcCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase abc1 = new Phrase(
				"This treatment makes human life easy & regular from pest problem. We assure you best pest management services with advanced technologies. Your positive response will be appreciated."
						+ "\n" + "\n" + "Thanks & Regards, ", font8);
		PdfPCell abc1cell = new PdfPCell(abc1);
		abc1cell.setBorder(0);
		abc1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase abc2 = new Phrase("For Om Pest Control Services " + "\n" + "\n"+ "\n"
				+ "Authorised Signatory. ", font8bold);
		PdfPCell abc2cell = new PdfPCell(abc2);
		abc2cell.setBorder(0);
		abc2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable notetable = new PdfPTable(1);
//		notetable.addCell(blankcell);
//		notetable.addCell(notecell);
//		notetable.addCell(abcCell);
//		notetable.addCell(blankcell);
		if(fy.getDescription()!=null){
			notetable.addCell(bnoteCell);
			notetable.addCell(descCell);
		}
		notetable.addCell(abc1cell);
		notetable.addCell(abc2cell);
		
		notetable.setWidthPercentage(100f);
		
				PdfPCell cell5 = new PdfPCell(notetable);
				cell5.setBorder(0);
		
				PdfPTable table5 = new PdfPTable(1);
				table5.addCell(cell5);
				table5.setWidthPercentage(100);
		
				try {
					document.add(table5);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
	}
	
	
/***************************************************************************************************************/		
		
	public void createQuotationTable(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);
		table.setSpacingBefore(10f);
		
		Phrase no = new Phrase("Sr.No.", font1);
//		Phrase treatment = new Phrase("Treatment", font1);
		Phrase pest = new Phrase("Pest Covered", font1);
		Phrase duration = new Phrase("Period of contract(Days)", font1);
		Phrase frequency = new Phrase("Frequency of services", font1);
		Phrase price = new Phrase("Amount", font1);
//		Phrase discount = new Phrase("Discount(in %)", font1);
//		Phrase discountValue = new Phrase("Amount After Discount", font1);
		
		PdfPCell nocell = new PdfPCell(no);
		nocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pestcell = new PdfPCell(pest);
		pestcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell freqcell = new PdfPCell(frequency);
		freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		PdfPCell disccell = new PdfPCell(discount);
//		disccell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		PdfPCell discountValueCell = new PdfPCell(discountValue);
//		discountValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(nocell);
		table.addCell(pestcell);
		table.addCell(durationcell);
		table.addCell(freqcell);
//		table.addCell(precell);
		table.addCell(pricecell);
//		table.addCell(disccell);
//		table.addCell(discountValueCell);
		
		double afterDiscount=0;
		double prodPrice=0;
for (int i = 0; i < this.fy.getItems().size(); i++){
			
			chunk = new Phrase(i + 1 + "", font8);
			pdfno = new PdfPCell(chunk);
			pdfno.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(fy.getItems().get(i).getComment() + "", font8);
			System.out.println("chunk 2 :" + fy.getItems().get(i).getComment());
			pdfpest = new PdfPCell(chunk);
			pdfpest.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(fy.getItems().get(i).getDuration() + "",font8);
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(
					fy.getItems().get(i).getNumberOfServices() + "", font8);
			System.out.println("chunk 3 :"+ fy.getItems().get(i).getNumberOfServices());
			pdffrequency = new PdfPCell(chunk);
			pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase((fy.getItems().get(i).getPrice()*fy.getItems().get(i).getQty())+"", font8);
			System.out.println("chunk 4 :" + fy.getNetpayable());
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
//			chunk = new Phrase(fy.getItems().get(i).getPercentageDiscount() + "",font8);
//			pdfdiscount = new PdfPCell(chunk);	
//			pdfdiscount.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
			
			
			afterDiscount=afterDiscount+fy.getItems().get(i).getPrice()*(fy.getItems().get(i).getPercentageDiscount()/100);
			
			prodPrice = prodPrice+fy.getItems().get(i).getPrice()*fy.getItems().get(i).getQty();
		
			table.addCell(pdfno);
			table.addCell(pdfpest);
			table.addCell(pdfduration);
			table.addCell(pdffrequency);
			table.addCell(pdfprice);
//			table.addCell(pdfdiscount);
//			table.addCell(pdfdiscount12);
			
		
	}


Phrase blank = new Phrase(" ",font8);
PdfPCell blankcell = new PdfPCell(blank);
//blankcell.setBorder(0);
blankcell.setBorderWidthRight(0);
blankcell.setBorderWidthTop(0);
blankcell.setBorderWidthLeft(0);

Phrase blank12 = new Phrase(" ",font8);
PdfPCell blank12cell = new PdfPCell(blank12);
//blankcell.setBorder(0);
blank12cell.setBorderWidthRight(0);
blank12cell.setBorderWidthTop(0);
//blank12cell.setBorderWidthBottom(0);

//Phrase discount = new Phrase("Discount", font1);
//PdfPCell discountcell = new PdfPCell(discount);
//discountcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//chunk = new Phrase(fy.getPerDiscount() + "",font8);
//pdfdiscount = new PdfPCell(chunk);	
//pdfdiscount.setHorizontalAlignment(Element.ALIGN_CENTER);



//table.addCell(blank12cell);
//table.addCell(blankcell);
//table.addCell(blankcell);
//table.addCell(discountcell);
//table.addCell(pdfdiscount);



//PdfPTable netpay = new PdfPTable(2);
//netpay.setWidthPercentage(100f);
//try {
//	netpay.setWidths(colwidth123);
//} catch (DocumentException e1) {
//	// TODO Auto-generated catch block
//	e1.printStackTrace();
//}

Phrase discount = new Phrase("Discount", font8bold); 
PdfPCell discountCell = new PdfPCell(discount);
discountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
discountCell.setBorderWidthLeft(0);
discountCell.setBorderWidthTop(0);

Phrase discAmt= new Phrase(afterDiscount+"",font8bold);
PdfPCell discAmtCell =  new PdfPCell(discAmt);
discAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);


Phrase famt = new Phrase("Sub Total", font8bold); 
PdfPCell famtcell = new PdfPCell(famt);
famtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
famtcell.setBorderWidthLeft(0);
famtcell.setBorderWidthTop(0);

double finalAmt =prodPrice-afterDiscount;

Phrase finlAnt= new Phrase(df.format(finalAmt),font8bold);
PdfPCell finalAmtCell =  new PdfPCell(finlAnt);
finalAmtCell.setBorderWidthTop(0);
finalAmtCell.setBorderWidthLeft(0);
finalAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

String name=null;
double servalue=0;
for(int i=0;i<fy.getProductTaxes().size();i++){
if(!fy.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("vat")){
name = fy.getProductTaxes().get(i).getChargeName();
servalue =fy.getProductTaxes().get(i).getChargePercent();
System.out.println("charge %"+servalue);
System.out.println("charge name "+name);
}
}
Phrase stax = new Phrase(name+"@"+servalue+"%",font8bold);
PdfPCell staxcell = new PdfPCell(stax);
staxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
staxcell.setBorderWidthLeft(0);
staxcell.setBorderWidthTop(0);

PdfPCell pdftaxpercent=null;
	if((fy.getItems().get(0).getServiceTax().getPercentage() !=0)){

		double taxvalue = finalAmt*(fy.getItems().get(0).getServiceTax().getPercentage()/100);	
			
		chunk = new Phrase(df.format(taxvalue),font8bold);
		pdftaxpercent = new PdfPCell(chunk);
		pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		pdftaxpercent.setBorderWidthTop(0);
		pdftaxpercent.setBorderWidthLeft(0);
	
	}
	else
	{
		chunk = new Phrase(" ",font8);
		pdftaxpercent = new PdfPCell(chunk);
		pdftaxpercent.setBorderWidthTop(0);
		pdftaxpercent.setBorderWidthLeft(0);
		pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
	}

Phrase netAmt= new Phrase("Total Amount",font8bold);
PdfPCell netAmtCell =  new PdfPCell(netAmt);
netAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
netAmtCell.setBorderWidthLeft(0);
netAmtCell.setBorderWidthTop(0);

Phrase netAmtVal= new Phrase(df.format(fy.getNetpayable()),font8bold);
PdfPCell netAmtValCell =  new PdfPCell(netAmtVal);
netAmtValCell.setBorderWidthTop(0);
netAmtValCell.setBorderWidthLeft(0);
netAmtValCell.setHorizontalAlignment(Element.ALIGN_CENTER);


table.addCell(blank12cell);
table.addCell(blankcell);
table.addCell(blankcell);
table.addCell(discountCell);
table.addCell(discAmtCell);
//netpay.addCell(discountCell);
//netpay.addCell(discAmtCell);

table.addCell(blank12cell);
table.addCell(blankcell);
table.addCell(blankcell);
table.addCell(famtcell);
table.addCell(finalAmtCell);

//netpay.addCell(famtcell);
//netpay.addCell(finalAmtCell);

table.addCell(blank12cell);
table.addCell(blankcell);
table.addCell(blankcell);
table.addCell(staxcell);
table.addCell(pdftaxpercent);

//netpay.addCell(staxcell);
//netpay.addCell(staxcell);


table.addCell(blank12cell);
table.addCell(blankcell);
table.addCell(blankcell);
table.addCell(netAmtCell);
table.addCell(netAmtValCell);


//PdfPCell netpayTableCell = new PdfPCell(netpay);


PdfPTable amtInWordTable = new PdfPTable(1);

//  rohan added this code 
String amountInWord = ServiceInvoicePdf.convert(fy.getNetpayable());

String amountInWordWithLowerCase="";
String[] spliturl=amountInWord.split(" ");

System.out.println("RRR   spliturl.length"+spliturl.length);
for(int i=1; i < spliturl.length;i++)
{
	amountInWordWithLowerCase = amountInWordWithLowerCase+spliturl[i].toLowerCase()+" ";
}



Phrase inwords = new Phrase("(In words) Rs. "+spliturl[0]+" "+amountInWordWithLowerCase + "only.", font8bold);
PdfPCell inwordscell = new PdfPCell(inwords);
inwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);
inwordscell.setBorderWidthTop(0);

//amtInWordTable.addCell(inwordscell);
//
//PdfPCell inWordsCell = new PdfPCell(amtInWordTable);
//inWordsCell.setBorderWidthTop(0);
//inWordsCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//PdfPCell itemcell = new PdfPCell();
//itemcell.addElement(table);
//itemcell.setBorder(0);


PdfPTable parent = new PdfPTable(1);
//parent.addCell(itemcell);
//parent.addCell(netpayTableCell);
parent.setWidthPercentage(100);
parent.addCell(inwordscell);


try {
	document.add(table);
	document.add(parent);
} catch (DocumentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

		}
	
}
