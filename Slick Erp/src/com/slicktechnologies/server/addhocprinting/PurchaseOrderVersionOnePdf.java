package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.client.views.contract.OtherChargesTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;

public class PurchaseOrderVersionOnePdf {
/**
 *     Added By : Priyanka Bhagwat
 *     Date : 1/06/2021
 *     Des : New Purchase Order Pdf For Pecoppest.	
 */
	Logger logger=Logger.getLogger("Purchase Order PDF");
	public Document document;
	int totalNoOfLines;
	
	PurchaseOrder po;
	List<ProductDetailsPO> products;
	List<PaymentTerms> payTermsLis;
	List<ProductOtherCharges> prodCharges;
	List<ProductOtherCharges> prodTaxes;
	ArrayList<SuperProduct>stringlis= new ArrayList<SuperProduct>();
	List<Branch> branchList=new ArrayList<Branch>();
	List<ArticleType> articletype;
	Vendor vendor;
	ProcessConfiguration processConfig;
	boolean CompanyNameLogoflag=false;
	Company comp;
	Company company;
	Employee emp;
	int vendorID;
	boolean upcflag;
	Branch branchDt = null;
	ServerAppUtility serverApp = new ServerAppUtility();
	boolean getAddressAsBranch=false;
	int noOfLines = 12;
	int prouductCount=0;
	SuperProduct sup;
	double total=0;
	List<State> stateList;
	private Font font16boldul, font12bold, font8bold,font9bold, font8,font7, font12boldul,font12,font14bold,font10,font10bold,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df2 = new DecimalFormat("0");
	CompanyPayment companyPayment;
	int totalNoOfTaxItem=0;
	int totalOthercharges=0;
	OtherChargesTable otherChargestable;
	int noOfPage=1;
	//int totalLines = 12;
	boolean recursiveFlag=false;
	String companyName="";
	float[] column12CollonWidth = {0.1f/*1*/,0.4f/*2*/+0.2f/*3*/+0.2f/*4*/+0.15f/*5*/+0.3f/*6*/,0.35f/*7*/,0.15f/*8*/,0.15f/*9*/,0.3f/*10*/,0.2f/*11*/,0.3f/*12*/,0.2f/*13*/,0.2f/*14*/,0.2f/*15*/,0.3f/*16*/};
	
	boolean pdfwithoutTotalAmtAndTaxFlag = false;
	
	public PurchaseOrderVersionOnePdf(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		float[] column12CollonWidth = {0.1f/*1*/,0.4f/*2*/+0.2f/*3*/+0.2f/*4*/+0.15f/*5*/+0.3f/*6*/,0.35f/*7*/,0.15f/*8*/,0.15f/*9*/,0.3f/*10*/,0.2f/*11*/,0.3f/*12*/,0.2f/*13*/,0.2f/*14*/,0.2f/*15*/,0.3f/*16*/};
	}
	

	public void setPurchaseOrder(Long count) {
		
		po = ofy().load().type(PurchaseOrder.class).id(count).now();
		
		if (po.getCompanyId() == null)comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", po.getCompanyId()).first().now();
		
		if (po.getCompanyId() != null)company = ofy().load().type(Company.class).filter("companyId", po.getCompanyId()).first().now();
		else
			company = ofy().load().type(Company.class).first().now();
			companyName=company.getBusinessUnitName();
		
		if (po.getCompanyId() == null)
			emp = ofy().load().type(Employee.class).filter("fullname", po.getEmployee()).first().now();
		else
			emp = ofy().load().type(Employee.class).filter("companyId", po.getCompanyId()).filter("fullname", po.getEmployee()).first().now();
				
		if (po.getVendorDetails() != null) {
			ArrayList<VendorDetails> vendorlist = po.getVendorDetails();
			for (int i = 0; i < vendorlist.size(); i++) {
				if (vendorlist.get(i).getStatus() == true) {
					vendorID = vendorlist.get(i).getVendorId();
				}
			}
		}

		if(po.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", po.getCompanyId()).filter("processName", "PurchaseOrder").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
//					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
//					{
//						CompanyNameLogoflag=true;
//					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase(AppConstants.PC_ENABLEPURCHASEPDFFORMATWITHOUTTOTALANDTAXINFO)
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pdfwithoutTotalAmtAndTaxFlag = true;
					}
				}
			}
		}
		
		
		branchDt = ofy().load().type(Branch.class).filter("companyId", po.getCompanyId()).filter("buisnessUnitName", po.getBranch()).first().now();
		  
		vendor = (Vendor) ofy().load().type(Vendor.class).filter("companyId", po.getCompanyId()).filter("count", vendorID).first().now();

		stateList = ofy().load().type(State.class).filter("companyId", po.getCompanyId()).list();
		
       if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(po !=null && po.getBranch() != null && po.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",po.getCompanyId()).filter("buisnessUnitName", po.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						companyPayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", po.getCompanyId()).first()
								.now();
						
						
						if(companyPayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		totalOthercharges=po.getOtherCharges().size();
		totalNoOfTaxItem=po.getProductTaxes().size();
		
		
		ArrayList<ProductDetailsPO> polist=convertOtherChargesToPoitem(po.getOtherCharges());
		if(polist.size()!=0){
		po.getProductDetails().addAll(polist);
		}
		
	}
	
	private ArrayList<ProductDetailsPO> convertOtherChargesToPoitem(ArrayList<OtherCharges> arrayList) {
		ArrayList<ProductDetailsPO> list=new ArrayList<ProductDetailsPO>();
		for(OtherCharges othercharges:po.getOtherCharges()){
			ProductDetailsPO productdetails=new ProductDetailsPO();
					productdetails.setProductCode("");
					productdetails.setProductName(othercharges.getOtherChargeName());
					productdetails.setPrduct(new ItemProduct());
					productdetails.getPrduct().setHsnNumber(othercharges.getHsnSacCode());
					productdetails.setProductQuantity(0);
					productdetails.setProdPrice(othercharges.getAmount());
					productdetails.setUnitOfmeasurement("");
					productdetails.setDiscountAmt(0);
					productdetails.setTotal(othercharges.getAmount());
					productdetails.setPurchaseTax1(othercharges.getTax1());
					productdetails.setPurchaseTax2(othercharges.getTax2());
			
					list.add(productdetails);
		}
		
		
		return list;
	}
	
	public void createPdf(String preprintStatus){
		//totalLines = getTotalNoOfLines();
		if (upcflag == false && preprintStatus.equals("plane")) {
			//createBlankHeading();
			//Createblank();
			//Createtitle();
			createLogo(document,comp);
			Createblank();
		}else{

			if (preprintStatus.equals("yes")){

				createSpcingForHeading();
				//Createtitle();
			}

			if (preprintStatus.equals("no")){
				if(comp.getUploadHeader()!=null){
					createCompanyNameAsHeader(document,comp);
					}
					
					if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
					}
					createSpcingForHeading();
 				    
			}

		}
		
		Createtitle();
		createAddressDetailTab();
		createProductTitleTab();
		createProductDetailsTab();
		
		logger.log(Level.SEVERE,"noOfLines after product table : "+noOfLines+" prouductCount="+prouductCount);
		
		if(noOfLines<=0&&prouductCount!=0) {//prouductCount
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 if(preprintStatus.contains("yes")){
			 createSpcingForHeading();
			
		}else if(preprintStatus.contains("no")){
			createSpcingForHeading();
			if(comp.getUploadHeader()!=null){
		    	createCompanyNameAsHeader(document,comp);
			}
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
			
		}
		Phrase ref = new Phrase(" ", font10bold);
		Paragraph pararef = new Paragraph();
		pararef.add(ref);
		pararef.setSpacingAfter(7f);
	   
	    try {
			document.add(pararef);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    createProductDetailsForAnnextureTab(prouductCount);
//	    createFooterAmountPart();
//		taxTableDetails();
//		createTotalTab();
//		createFooterTab();	
	  }
		
		if(!pdfwithoutTotalAmtAndTaxFlag){
			createFooterAmountPart();
			taxTableDetails();
			createTotalTab();
			createPaymentTermsTab();
		}
		
		createFooterTab();		
}
	
//	public int getTotalNoOfLines(){
//		int totalLines = 0;
//		for(ProductDetailsPO po: po.getProductDetails()){
//			int length=po.getProductName().length();
//			int lines=(int)Math.ceil(length/36.0);
//			totalLines+= lines;
//			System.out.println("TotalLines"+totalLines);
//			}
//		return  totalLines ;
//	}
	
	

	
	private void Createtitle() {
		
		PdfPTable titleTab = new PdfPTable(1);
		titleTab.setWidthPercentage(100);
		
	    Phrase titlephbl = new Phrase(" ", font10);
		PdfPCell titleCellbl = new PdfPCell(titlephbl);
		titleCellbl.setBorder(0);
		titleTab.addCell(titleCellbl);
		
		String pageTitle="";
		pageTitle="PURCHASE ORDER";
		
		Phrase titleph = new Phrase(pageTitle, font12bold);
		PdfPCell titleCell = new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setPaddingBottom(10);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(titleCell);
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	private void createAddressDetailTab() {
		PdfPTable invoiceAddtab = new PdfPTable(2);
		invoiceAddtab.setWidthPercentage(100);
		
		try {
			invoiceAddtab.setWidths(new float[]{21,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase invoiceto = new Phrase(" Invoice To", font8bold);
		PdfPCell invoicetocell = new PdfPCell(invoiceto);
		invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicetocell.setColspan(2);
		invoicetocell.setBorder(0);
		invoiceAddtab.addCell(invoicetocell);
		
		 String companyName ="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyName=branchDt.getCorrespondenceName();
			}else{
				companyName = comp.getBusinessUnitName().trim().toUpperCase();
			}
		
		
//		Phrase compname = new Phrase(comp.getBusinessUnitName(), font8bold);//komal
		Phrase compname = new Phrase(" "+companyName, font8bold);
		PdfPCell compnamecell = new PdfPCell(compname);
		compnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell.setColspan(2);
		compnamecell.setBorder(0);
		invoiceAddtab.addCell(compnamecell);
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(comp.getAddress()!=null){
			
			if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
					custAdd1=comp.getAddress().getAddrLine1()+" "+"\n"+comp.getAddress().getAddrLine2()+" "+"\n"+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1()+" "+"\n"+comp.getAddress().getAddrLine2();
				}
			}else{
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
					custAdd1=comp.getAddress().getAddrLine1()+" "+"\n"+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1();
				}
			}
			
			if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+" "+"\n"+comp.getAddress()+" "+"\n"+comp.getAddress().getPin()+" "+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+" "+"\n"+comp.getAddress().getLocality()+" "+"\n"+comp.getAddress().getCity()+" "+"\n"+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}else{
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+" "+"\n"+comp.getAddress().getPin()+","+comp.getAddress()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+" "+"\n"+comp.getAddress().getCity()+" "+"\n"+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}
			

		}

		
		
		Phrase compadd = new Phrase(comp.getAddress().getCompleteAddress(), font8);
		PdfPCell compaddcell = new PdfPCell(compadd);
		compaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compaddcell.setColspan(2);
		compaddcell.setBorder(0);
		compaddcell.setPaddingLeft(4f);
		invoiceAddtab.addCell(compaddcell);
//		}
		

		
		Phrase colonph = new Phrase(":", font8);
		PdfPCell coloncell = new PdfPCell(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		coloncell.setBorder(0);
		//invoiceAddtab.addCell(coloncell);
		
		
		ServerAppUtility serverApp = new ServerAppUtility();

		String gstin = "", gstinText = "";
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			gstin = serverApp.getGSTINOfCompany(comp, po.getBranch().trim());
			System.out.println("gstin" + gstin);

		} else {
			gstinText = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinText);
		}

		Phrase gstinvalph = null;
		if (!gstin.trim().equals("")) {
			gstinvalph = new Phrase( gstin, font8);
		} else if (!gstinText.trim().equalsIgnoreCase("")) {
			gstinvalph = new Phrase(gstinText, font8);
		} else {
			gstinvalph = new Phrase("", font8);

		}
		
//		PdfPCell gstinvalcell = new PdfPCell(gstinvalph);
//		gstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		gstinvalcell.setBorder(0);
//		invoiceAddtab.addCell(gstinvalcell);
		
		
		Phrase gstinph = new Phrase(" GSTIN  : "+gstinvalph, font8);
		PdfPCell gstincell = new PdfPCell(gstinph);
		gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstincell.setBorder(0);
		gstincell.setColspan(2);
		if(gstin!=null&&!gstin.trim().equals("")){
		invoiceAddtab.addCell(gstincell);
		}
		
		String statename1=" ";
		
		 statename1 =comp.getAddress().getState();
//		PdfPCell statenamevalcell = new PdfPCell(statevalname);
//		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statenamevalcell.setBorder(0);
		//invoiceAddtab.addCell(statenamevalcell);
		
		Phrase statename = new Phrase(" State  : "+statename1, font8);
		PdfPCell statenamecell = new PdfPCell(statename);
		statenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamecell.setBorder(0);
		statenamecell.setColspan(2);
		if(comp.getAddress().getState()!=null&&!comp.getAddress().getState().equals(" ")){
		invoiceAddtab.addCell(statenamecell);
		}
		
		//invoiceAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
//		PdfPTable statecodetab=new PdfPTable(3);
//		statecodetab.setWidthPercentage(100);
//		try {
//			statecodetab.setWidths(new float[]{40,20,40});
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if(getAddressAsBranch){
//			Phrase statevalname = new Phrase(branchDt.getAddress().getState(), font8);
//			PdfPCell statenamevalcell = new PdfPCell(statevalname);
//			statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			statenamevalcell.setBorder(0);
//			statecodetab.addCell(statenamevalcell);
//		}else{
//		Phrase statevalname = new Phrase(comp.getAddress().getState(), font8);
//		PdfPCell statenamevalcell = new PdfPCell(statevalname);
//		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statenamevalcell.setBorder(0);
//		//statecodetab.addCell(statenamevalcell);
////		}
//		Phrase statecode = new Phrase("Code :", font8);
//		PdfPCell statecodecell = new PdfPCell(statecode);
//		statecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statecodecell.setBorder(0);
//		//statecodetab.addCell(statecodecell);
//		
//		String stateCodeStr = serverApp.getStateOfCompany(comp,
//				po.getBranch().trim(), stateList);
//		Phrase stateCode = new Phrase(stateCodeStr, font8);
//
//		PdfPCell stateCodeCell = new PdfPCell(stateCode);
//		stateCodeCell.setBorder(0);
//		statecodetab.addCell(stateCodeCell);
//		
//		/*state code tab complete*/
//		
//		PdfPCell statecodetabcell = new PdfPCell(statecodetab);
//		statecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statecodetabcell.setBorder(0);
//		//invoiceAddtab.addCell(statecodetabcell);
		
		
//		Phrase cinph = new Phrase("CIN", font8);
//		PdfPCell cincell = new PdfPCell(cinph);
//		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cincell.setBorder(0);
//		invoiceAddtab.addCell(cincell);
//		
//		invoiceAddtab.addCell(coloncell);
//		
//		String cinval=null;
//		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//
//			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("CIN")) {
//
//				cinval=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
//			}
//			}
//			
//		Phrase cinvalph = new Phrase(cinval, font8);
//		PdfPCell cinvalphCell = new PdfPCell(cinvalph);
//		cinvalphCell.setBorder(0);
//		invoiceAddtab.addCell(cinvalphCell);
			
		
		Phrase emailph = new Phrase(" E-Mail", font8);
		PdfPCell emailcell = new PdfPCell(emailph);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailcell.setBorder(0);
//		invoiceAddtab.addCell(emailcell);
		
//invoiceAddtab.addCell(coloncell);
		
//		if(getAddressAsBranch){
//			Phrase emailvalph = new Phrase(branchDt.getEmail(), font8);
//			PdfPCell emailvalcell = new PdfPCell(emailvalph);
//			emailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			emailvalcell.setBorder(0);
//			invoiceAddtab.addCell(emailvalcell);
//		}
		Phrase emailvalph = new Phrase(" E-Mail : "+comp.getEmail(), font8);
		PdfPCell emailvalcell = new PdfPCell(emailvalph);
		emailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailvalcell.setBorder(0);
		emailvalcell.setColspan(2);
		if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
		invoiceAddtab.addCell(emailvalcell);
		}
		
		/*invoice to tab complet*/
		
		PdfPTable deliveryAddtab = new PdfPTable(2);
		deliveryAddtab.setWidthPercentage(100);
		
		try {
			deliveryAddtab.setWidths(new float[]{21,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase despatchto = new Phrase(" Despatch To", font8bold);
		PdfPCell despatchtocell = new PdfPCell(despatchto);
		despatchtocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		despatchtocell.setColspan(2);
		despatchtocell.setBorder(0);
		deliveryAddtab.addCell(despatchtocell);
		
		 String companyName1 ="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyName1=branchDt.getCorrespondenceName();
			}else{
				companyName1 = comp.getBusinessUnitName().trim().toUpperCase();
			}
		
		/**Date 22-8-2019 by Amol added a company name**/
//		Phrase compname2 = new Phrase(comp.getBusinessUnitName(), font8bold);//komal
		Phrase compname2 = new Phrase(" "+companyName1, font8bold);
		PdfPCell compnamecell2 = new PdfPCell(compname2);
		compnamecell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell2.setColspan(2);
		compnamecell2.setBorder(0);
		deliveryAddtab.addCell(compnamecell2);
		
		Phrase compadd2 = new Phrase(po.getAdress().getCompleteAddress(), font8);
		PdfPCell compaddcell2 = new PdfPCell(compadd2);
		compaddcell2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		compaddcell2.setColspan(2);
		compaddcell2.setBorder(0);
		compaddcell2.setPaddingLeft(4f);
		deliveryAddtab.addCell(compaddcell2);
		
		
		Phrase devemailph = new Phrase(" E-Mail", font8);
		PdfPCell devemailcell = new PdfPCell(devemailph);
		devemailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devemailcell.setBorder(0);
    //		deliveryAddtab.addCell(devemailcell);
		
	//	deliveryAddtab.addCell(coloncell);
		
		Phrase devemailvalph = new Phrase(" E-Mail  : "+comp.getEmail(), font8);
		PdfPCell devemailvalcell = new PdfPCell(devemailvalph);
		devemailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devemailvalcell.setBorder(0);
		devemailvalcell.setColspan(2);
		deliveryAddtab.addCell(devemailvalcell);
		
		
//		Phrase devgstinph = new Phrase("GSTIN", font8);
//		PdfPCell devgstincell = new PdfPCell(devgstinph);
//		devgstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		devgstincell.setBorder(0);
//		deliveryAddtab.addCell(devgstincell);
//		
//		Phrase devcolonph = new Phrase(":", font8);
//		PdfPCell devcoloncell = new PdfPCell(devcolonph);
//		devcoloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		devcoloncell.setBorder(0);
//		deliveryAddtab.addCell(devcoloncell);
		
		
		ServerAppUtility devserverApp = new ServerAppUtility();

		String devgstin = "", devgstinText = "";
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			devgstin = devserverApp.getGSTINOfCompany(comp, po.getBranch().trim());
			System.out.println("gstin" + gstin);

		} else {
			devgstinText = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinText);
		}

		Phrase devgstinvalph = null;
		if (!gstin.trim().equals("")) {
			gstinvalph = new Phrase( gstin, font8);
		} else if (!gstinText.trim().equalsIgnoreCase("")) {
			gstinvalph = new Phrase(gstinText, font8);
		} else {
			gstinvalph = new Phrase("", font8);

		}
		
		
		
//		PdfPCell gstinvalcell = new PdfPCell(gstinvalph);
//		gstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		gstinvalcell.setBorder(0);
//		invoiceAddtab.addCell(gstinvalcell);
		
		
		Phrase devgstinph = new Phrase(" GSTIN  : "+gstinvalph, font8);
		PdfPCell devgstincell = new PdfPCell(devgstinph);
		devgstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devgstincell.setBorder(0);
		devgstincell.setColspan(2);
		if(gstin!=null&&!gstin.trim().equals("")){
			deliveryAddtab.addCell(gstincell);
		}
		
		String statename2=" ";
		
		 statename2 =comp.getAddress().getState();
//		PdfPCell statenamevalcell = new PdfPCell(statevalname);
//		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statenamevalcell.setBorder(0);
		//invoiceAddtab.addCell(statenamevalcell);
		
		Phrase statenamed = new Phrase(" State  : "+statename2, font8);
		PdfPCell statenamecell1 = new PdfPCell(statenamed);
		statenamecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamecell1.setBorder(0);
		statenamecell1.setColspan(2);
		if(comp.getAddress().getState()!=null&&!comp.getAddress().getState().equals(" ")){
			deliveryAddtab.addCell(statenamecell1);
		}
		
//		PdfPCell devgstinvalcell = new PdfPCell(gstinvalph);
//		devgstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		devgstinvalcell.setBorder(0);
//		deliveryAddtab.addCell(devgstinvalcell);
		
		
//		Phrase devstatename = new Phrase("State", font8);
//		PdfPCell devstatenamecell = new PdfPCell(devstatename);
//		devstatenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		devstatenamecell.setBorder(0);
//		deliveryAddtab.addCell(devstatenamecell);
//		
//		deliveryAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable devstatecodetab=new PdfPTable(3);
		devstatecodetab.setWidthPercentage(100);
		try {
			devstatecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase devstatevalname = new Phrase(comp.getAddress().getState(), font8);
		PdfPCell devstatenamevalcell = new PdfPCell(devstatevalname);
		devstatenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatenamevalcell.setBorder(0);
		//devstatecodetab.addCell(devstatenamevalcell);
		
		Phrase devstatecode = new Phrase("Code :", font8);
		PdfPCell devstatecodecell = new PdfPCell(devstatecode);
		devstatecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatecodecell.setBorder(0);
		//devstatecodetab.addCell(devstatecodecell);
		
		String devstateCodeStr = serverApp.getStateOfCompany(comp,
				po.getBranch().trim(), stateList);
		Phrase devstateCode = new Phrase(devstateCodeStr, font8);

		PdfPCell devstateCodeCell = new PdfPCell(devstateCode);
		devstateCodeCell.setBorder(0);
		//devstatecodetab.addCell(devstateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell devstatecodetabcell = new PdfPCell(devstatecodetab);
		devstatecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatecodetabcell.setBorder(0);
		//deliveryAddtab.addCell(devstatecodetabcell);
		
		
				
		
		PdfPTable leftoutertab=new PdfPTable(1);
		leftoutertab.setWidthPercentage(100);
			
		PdfPCell invoicecell=new PdfPCell(invoiceAddtab);
		invoicecell.setPaddingBottom(5);
		PdfPCell delivery=new PdfPCell(deliveryAddtab);
		delivery.setPaddingBottom(5);
		//PdfPCell vendorcell=new PdfPCell(vendorAddtab);
		//vendorcell.setPaddingBottom(5);
		
		leftoutertab.addCell(invoicecell);
		leftoutertab.addCell(delivery);
		//PdfPCell vendorcell=new PdfPCell(vendorAddtab);
				//vendorcell.setPaddingBottom(5);
		//leftoutertab.addCell(vendorcell);
		
		/*Left Tab complete***/
		
		PdfPTable dateAndVoucherTab=new PdfPTable(2);
		dateAndVoucherTab.setWidthPercentage(100);
		try {
			dateAndVoucherTab.setWidths(new float[]{44,56});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Phrase vouchernoph = new Phrase("Order No : "+po.getCount()+"", font8);
		PdfPCell vouchernocell = new PdfPCell(vouchernoph);
		vouchernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//vouchernocell.setBorderWidthBottom(0);
		vouchernocell.setPaddingLeft(4f);
		vouchernocell.setPaddingTop(4f);
		vouchernocell.setPaddingBottom(4f);
		vouchernocell.setPaddingRight(4f);
		dateAndVoucherTab.addCell(vouchernocell);
		
		Phrase datedph = new Phrase("Date : "+fmt.format(po.getPODate()), font8);
		PdfPCell datedcell = new PdfPCell(datedph);
		datedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//datedcell.setBorderWidthBottom(0);
		datedcell.setPaddingLeft(4f);
		datedcell.setPaddingTop(4f);
		datedcell.setPaddingBottom(4f);
		datedcell.setPaddingRight(4f);
		dateAndVoucherTab.addCell(datedcell);
		
//		Phrase vouchernovalph = new Phrase(po.getCount()+"", font8);
//		PdfPCell vouchernoValcell = new PdfPCell(vouchernovalph);
//		vouchernoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		vouchernoValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(vouchernoValcell);
		
//		Phrase datedValph = new Phrase(fmt.format(po.getPODate()), font8);
//		PdfPCell datedValcell = new PdfPCell(datedValph);
//		datedValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		datedValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(datedValcell);
		
//		Phrase createddayph = new Phrase("Reference No :", font8);
////		Phrase createddayph = new Phrase("Credit Days", font8);
//		PdfPCell createdDaycell = new PdfPCell(createddayph);
//		createdDaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		createdDaycell.setBorderWidthBottom(0);
//		createdDaycell.setPaddingLeft(2f);
//		createdDaycell.setPaddingTop(2f);
//		createdDaycell.setPaddingBottom(2f);
//		dateAndVoucherTab.addCell(createdDaycell);
		
//		Phrase modeph = new Phrase("Terms of Payment", font8);
//		PdfPCell modecell = new PdfPCell(modeph);
//		modecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		modecell.setBorderWidthBottom(0);
//		modecell.setPaddingLeft(2f);
//		modecell.setPaddingTop(2f);
//		modecell.setPaddingBottom(2f);
//		dateAndVoucherTab.addCell(modecell);
		
//		int days=0;
//		for (int i = 0; i <po.getPaymentTermsList().size(); i++) {
//			days=po.getPaymentTermsList().get(0).getPayTermDays();
//		}
		boolean refnoFlag=false;
		boolean creditDayFlag=false;
		String refNo="";
		if(po.getRefOrderNO()!=null){
			refNo=po.getRefOrderNO();
		}
		Phrase createddayValph; 
		if(refNo!=null && !refNo.equals("")){ //Ashwini Patil Date:4-08-2023
			createddayValph = new Phrase("Reference No : "+refNo, font8);
			refnoFlag=true;
		}else
			createddayValph = new Phrase(" ", font8);
//		Phrase createddayValph = new Phrase(po.getCreditDays()+"", font8);
		PdfPCell createdDayValcell = new PdfPCell(createddayValph);
		createdDayValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//createdDayValcell.setBorderWidthTop(0);
		createdDayValcell.setPaddingLeft(4f);
		createdDayValcell.setPaddingTop(4f);
		createdDayValcell.setPaddingBottom(4f);
		createdDayValcell.setPaddingRight(4f);
		
		
		
		
//		double percent=0;
//		String comment=null;
//		int days=0;
//		for (int i = 0; i <po.getPaymentTermsList().size(); i++) {
//			percent=po.getPaymentTermsList().get(0).getPayTermPercent();
//			comment=po.getPaymentTermsList().get(0).getPayTermComment();
//			days=po.getPaymentTermsList().get(0).getPayTermDays();
//		}
		
//		String mode="";
//		if(po.getPaymentMethod()!=null&&!po.getPaymentMethod().equalsIgnoreCase("")){
//			mode=po.getPaymentMethod()+"/"+comment;
//		}else{
//			mode=comment;
//		}
		/** date 14.11.2018 added by komal for sasha **/
		String  percent= "";
		if(po.getCreditDays() != 0){
			percent = po.getCreditDays()+"";
		}
		if(po.getCreditDaysComment() != null){
			percent = percent + " "+po.getCreditDaysComment();
		}

		Phrase modeValph;
		if(percent!=null && !percent.equals("")&&!percent.equals(" ")){ //Ashwini Patil Date:4-08-2023
			modeValph= new Phrase("Credit Days : "+percent, font8);
			System.out.println("Credit Days="+percent+" length="+percent.length());
			creditDayFlag=true;
		}
		else
			modeValph= new Phrase(" ", font8);
//		Phrase modeValph = new Phrase(po.getPaymentMethod()+"/"+days+"/"+df2.format(percent) +"%"+"/"+comment, font8);
		PdfPCell modeValcell = new PdfPCell(modeValph);
		modeValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		modeValcell.setPaddingLeft(4f);
		//modeValcell.setPaddingLeft(4f);
		modeValcell.setPaddingTop(4f);
		modeValcell.setPaddingBottom(4f);
		modeValcell.setPaddingRight(4f);
		
		if(creditDayFlag&&refnoFlag){//Ashwini Patil Date:4-08-2023
			dateAndVoucherTab.addCell(createdDayValcell);		
			dateAndVoucherTab.addCell(modeValcell);
		}else if(creditDayFlag&&!refnoFlag){
			modeValcell.setColspan(2);
			dateAndVoucherTab.addCell(modeValcell);
		}else if(!creditDayFlag&&refnoFlag){
			createdDayValcell.setColspan(2);
			dateAndVoucherTab.addCell(createdDayValcell);
		}
		
//		Phrase supplierph = new Phrase("Supplier's Ref/Order No", font8);
//		PdfPCell supplicell = new PdfPCell(supplierph);
//		supplicell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		supplicell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(supplicell);
//		
//		Phrase otherph = new Phrase("Other Reference", font8);
//		PdfPCell othercell = new PdfPCell(otherph);
//		othercell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		othercell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(othercell);
//		
//		Phrase suppliervalph = new Phrase(" ", font8);
//		PdfPCell supplivalcell = new PdfPCell(suppliervalph);
//		supplivalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		supplivalcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(supplivalcell);
//		
//		Phrase otherValph = new Phrase(" ", font8);
//		PdfPCell otherValcell = new PdfPCell(otherValph);
//		otherValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		otherValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(otherValcell);
//		
//		Phrase despathph = new Phrase(" Other Reference(s)", font8);
//		PdfPCell despathcell = new PdfPCell(despathph);
//		despathcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		despathcell.setBorderWidthBottom(0);
//		despathcell.setPaddingLeft(2f);
//		despathcell.setPaddingTop(2f);
//		despathcell.setPaddingBottom(2f);
//		despathcell.setColspan(2);
//		dateAndVoucherTab.addCell(despathcell);
		
		
//		Phrase destinationph = new Phrase("", font8);
//		PdfPCell destinationcell = new PdfPCell(destinationph);
//		destinationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		destinationcell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(destinationcell);
		
		Phrase despathValph=null;
		if(po.getPOName()!=null){
			 despathValph = new Phrase(po.getPOName(), font8);
		}
		else{
			 despathValph = new Phrase("  ", font8);
		}
		
		Phrase despathValph1;
		if(po.getPOName()!=null && !po.getPOName().equals("")) //Ashwini Patil Date:4-08-2023
			despathValph1= new Phrase("Other Reference(s) : "+po.getPOName(), font8);
		else
			despathValph1= new Phrase(" ", font8);
		
		PdfPCell despathValcell = new PdfPCell(despathValph1);
		despathValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//despathValcell.setBorderWidthTop(0);
		despathValcell.setPaddingLeft(4f);
		despathValcell.setPaddingTop(4f);
		despathValcell.setPaddingBottom(4f);
		despathValcell.setPaddingRight(4f);
		despathValcell.setColspan(2);
		
		if(po.getPOName()!=null && !po.getPOName().equals("")) //Ashwini Patil Date:4-08-2023
		dateAndVoucherTab.addCell(despathValcell);
		
		
		
//		String warehouse=null;
//		for (int i = 0; i <po.getProductDetails().size(); i++) {
//			warehouse=po.getProductDetails().get(0).getItemProductWarehouseName();
//		}
//		
//		Phrase destinationValph=null;
//		if(po.getPoWarehouseName()!=null){
//			
//			 destinationValph = new Phrase(po.getPoWarehouseName(), font8);
//		}
//		else{
//			destinationValph = new Phrase("  ", font8);
//		}
//		PdfPCell destinationValphcell = new PdfPCell(destinationValph);
//		destinationValphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		destinationValphcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(destinationValphcell);
		
		
//		Phrase termsOfDelph = new Phrase("Terms Of Delivery", font8);
//		PdfPCell termsOfDelcell = new PdfPCell(termsOfDelph);
//		termsOfDelcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		termsOfDelcell.setBorderWidthBottom(0);
//		termsOfDelcell.setColspan(2);
//		dateAndVoucherTab.addCell(termsOfDelcell);
		
		Phrase termsOfDelValph=null;
		if(po.getTermsOfDelivery()!=null){
			 termsOfDelValph = new Phrase(po.getTermsOfDelivery(), font8);
		}
		else{
			 termsOfDelValph = new Phrase("", font8);
		}
		Phrase termsOfDelph ;
		if(po.getTermsOfDelivery()!=null && !po.getTermsOfDelivery().equals("")) { //Ashwini Patil Date:4-08-2023
			termsOfDelph= new Phrase("Terms Of Delivery : "+po.getTermsOfDelivery(), font8);

			PdfPCell termsOfDelValcell = new PdfPCell(termsOfDelph);
			termsOfDelValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			//termsOfDelValcell.setBorderWidthTop(0);
			termsOfDelValcell.setPaddingLeft(4f);
			termsOfDelValcell.setPaddingTop(4f);
			termsOfDelValcell.setPaddingBottom(4f);
			termsOfDelValcell.setPaddingRight(4f);
			//despathValcell.setColspan(2);
			termsOfDelValcell.setColspan(2);
			dateAndVoucherTab.addCell(termsOfDelValcell);		
		}
		
		
/* despatch to*/
		
		
		PdfPTable vendorAddtab = new PdfPTable(2);
		vendorAddtab.setWidthPercentage(100);
		
		try {
			vendorAddtab.setWidths(new float[]{21,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase supplier = new Phrase(" Supplier", font8bold);
		PdfPCell suppliercell = new PdfPCell(supplier);
		suppliercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		suppliercell.setColspan(2);
		suppliercell.setBorder(0);
		vendorAddtab.addCell(suppliercell);
	
		
		Phrase compname3 = new Phrase(" "+vendor.getVendorName(), font8bold);//komal
		PdfPCell compnamecell3 = new PdfPCell(compname3);
		compnamecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell3.setColspan(2);
		compnamecell3.setBorder(0);
		vendorAddtab.addCell(compnamecell3);
		
		String custAdd11="";
		String custFullAdd11="";
		
		if(vendor.getPrimaryAddress()!=null){
			
			if(vendor.getPrimaryAddress().getAddrLine2()!=null && !vendor.getPrimaryAddress().getAddrLine2().equals("")){
				if(vendor.getPrimaryAddress().getLandmark()!=null && !vendor.getPrimaryAddress().getLandmark().equals("")){
					custAdd11=vendor.getPrimaryAddress().getAddrLine1()+" "+"\n"+vendor.getPrimaryAddress().getAddrLine2()+" "+"\n"+vendor.getPrimaryAddress().getLandmark();
				}else{
					custAdd11=vendor.getPrimaryAddress().getAddrLine1()+" "+"\n"+vendor.getPrimaryAddress().getAddrLine2();
				}
			}else{
				if(vendor.getPrimaryAddress().getLandmark()!=null && !vendor.getPrimaryAddress().getLandmark().equals("") ){
					custAdd11=vendor.getPrimaryAddress().getAddrLine1()+" "+"\n"+vendor.getPrimaryAddress().getLandmark();
				}else{
					custAdd11=vendor.getPrimaryAddress().getAddrLine1();
				}
			}
			
			if(vendor.getPrimaryAddress().getLocality()!=null&& !vendor.getPrimaryAddress().getLocality().equals("")){
				if(vendor.getPrimaryAddress().getPin()!=0){
					custFullAdd11=custAdd11+" "+"\n"+vendor.getPrimaryAddress()+" "+"\n"+vendor.getPrimaryAddress().getPin()+" "+"\n"+vendor.getPrimaryAddress().getCity()+","+vendor.getPrimaryAddress().getState()+","+vendor.getPrimaryAddress().getCountry();
				}else{
					custFullAdd11=custAdd11+" "+"\n"+vendor.getPrimaryAddress().getLocality()+" "+"\n"+vendor.getPrimaryAddress().getCity()+" "+"\n"+vendor.getPrimaryAddress().getState()+","+vendor.getPrimaryAddress().getCountry();
				}
			}else{
				if(vendor.getPrimaryAddress().getPin()!=0){
					custFullAdd11=custAdd11+" "+"\n"+vendor.getPrimaryAddress().getPin()+","+vendor.getPrimaryAddress().getCity()+", "+vendor.getPrimaryAddress().getState()+", "+vendor.getPrimaryAddress().getCountry();
				}else{
					custFullAdd11=custAdd11+" "+"\n"+vendor.getPrimaryAddress().getCity()+" "+"\n"+vendor.getPrimaryAddress().getState()+","+vendor.getPrimaryAddress().getCountry();
				}
			}
			

		}

		
		
		
		Phrase vendoradd = new Phrase(vendor.getPrimaryAddress().getCompleteAddress(), font8);
		PdfPCell vendoraddcell = new PdfPCell(vendoradd);
		vendoraddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendoraddcell.setColspan(2);
		vendoraddcell.setBorder(0);
		vendoraddcell.setPaddingLeft(4f);
		vendorAddtab.addCell(vendoraddcell);
		
		Phrase vendorPhoneph = new Phrase(" Phone No : "+vendor.getCellNumber1()+"", font8);
		PdfPCell vendorPhonecell = new PdfPCell(vendorPhoneph);
		vendorPhonecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorPhonecell.setColspan(2);
		vendorPhonecell.setBorder(0);
		if(vendor.getCellNumber1()!=0&&vendor.getCellNumber1()!=null){
		vendorAddtab.addCell(vendorPhonecell);
		}
		
		//vendorAddtab.addCell(coloncell);
		
//		Phrase vendorPhonephval = new Phrase(vendor.getCellNumber1()+"", font8);
//		PdfPCell vendorPhonecellval = new PdfPCell(vendorPhonephval);
//		vendorPhonecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
//		vendorPhonecellval.setBorder(0);
//		vendorAddtab.addCell(vendorPhonecellval);
		
		Phrase vendorConph = new Phrase(" Contact No : "+vendor.getCellNumber2()+"", font8);
		PdfPCell vendorConcell = new PdfPCell(vendorConph);
		vendorConcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorConcell.setBorder(0);
		vendorConcell.setColspan(2);
		if(vendor.getCellNumber2()!=0&&vendor.getCellNumber2()!=null){
		vendorAddtab.addCell(vendorConcell);
		}
		//vendorAddtab.addCell(coloncell);
		
//		Phrase vendorConphval = new Phrase(vendor.getCellNumber2()+"", font8);
//		PdfPCell vendorConcellval = new PdfPCell(vendorConphval);
//		vendorConcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
//		vendorConcellval.setBorder(0);
//		vendorAddtab.addCell(vendorConcellval);
		
		Phrase emailVenph = new Phrase(" E-Mail : "+vendor.getEmail(), font8);
		PdfPCell emailVencell = new PdfPCell(emailVenph);
		emailVencell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailVencell.setColspan(2);
		emailVencell.setBorder(0);
		if(vendor.getEmail()!=null&&!vendor.getEmail().equals(" "))
		{
		vendorAddtab.addCell(emailVencell);
		}
		//vendorAddtab.addCell(coloncell);
		
//		Phrase emailVenvalph = new Phrase(vendor.getEmail(), font8);
//		PdfPCell emailVenvalcell = new PdfPCell(emailVenvalph);
//		emailVenvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		emailVenvalcell.setBorder(0);
//		vendorAddtab.addCell(emailVenvalcell);
		
		
		
		
		//vendorAddtab.addCell(coloncell);
		
		
		String gstinvalue=null;
		for (int i = 0; i < vendor.getArticleTypeDetails().size(); i++) {

			if (vendor.getArticleTypeDetails().get(i).getArticleTypeName().contains("GSTIN")) {

				gstinvalue=vendor.getArticleTypeDetails().get(i).getArticleTypeValue();
				break;
			}
			}
	
		Phrase gstinval2=new Phrase (" GSTIN : "+gstinvalue,font8);
		PdfPCell gstinvalcell2 = new PdfPCell(gstinval2);
		gstinvalcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell2.setBorder(0);
		gstinvalcell2.setColspan(2);
		if(gstinvalue!=null&&!gstinvalue.equals(" ")){
		vendorAddtab.addCell(gstinvalcell2);
		}
//		Phrase statevendorname = new Phrase("State", font8);
//		PdfPCell statenamevendorcell = new PdfPCell(statevendorname);
//		statenamevendorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statenamevendorcell.setBorder(0);
//		vendorAddtab.addCell(statenamevendorcell);
//		
//		vendorAddtab.addCell(coloncell);
		
		
		Phrase statevendorvalname = new Phrase(" State : "+vendor.getPrimaryAddress().getState(), font8);
		PdfPCell statenamevendorvalcell = new PdfPCell(statevendorvalname);
		statenamevendorvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevendorvalcell.setBorder(0);
		vendorAddtab.addCell(statenamevendorvalcell);
		
		/* state code table*/
		
		PdfPTable statevendorcodetab=new PdfPTable(3);
		statevendorcodetab.setWidthPercentage(100);
		try {
			statevendorcodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Phrase statevendorvalname = new Phrase(vendor.getPrimaryAddress().getState(), font8);
//		PdfPCell statenamevendorvalcell = new PdfPCell(statevendorvalname);
//		statenamevendorvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		statenamevendorvalcell.setBorder(0);
//		statevendorcodetab.addCell(statenamevendorvalcell);
		
		
//		statevendorcodetab.addCell(statecodecell);
		
		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(vendor.getPrimaryAddress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase VendorstateCode=new Phrase(stCo,font8);
		PdfPCell venstateCodeCell = new PdfPCell(VendorstateCode);
		venstateCodeCell.setBorder(0);
		venstateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//statevendorcodetab.addCell(venstateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell venstatecodecell = new PdfPCell(statevendorcodetab);
		venstatecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		venstatecodecell.setBorder(0);
		//vendorAddtab.addCell(venstatecodecell);
		
		PdfPCell vendorcell=new PdfPCell(vendorAddtab);
		vendorcell.setPaddingBottom(5);
		vendorcell.setColspan(2);
		//leftoutertab.addCell(vendorcell);
		dateAndVoucherTab.addCell(vendorcell);

		
		
		PdfPTable mainout=new PdfPTable(2) ;
		mainout.setWidthPercentage(100);
		
		try {
			mainout.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell leftcell=new PdfPCell(leftoutertab);
		PdfPCell rightCell=new PdfPCell(dateAndVoucherTab);
		
		mainout.addCell(leftcell);
		mainout.addCell(rightCell);
		
		
		try {
			document.add(mainout);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void createProductTitleTab() {
		
//		PdfPTable productTitletab = new PdfPTable(9);
		PdfPTable productTitletab = new PdfPTable(12);
		productTitletab.setWidthPercentage(100);
		
		try {
			productTitletab.setWidths(new float[]{7,9,39,8,9,6,8,10,7,8,8,11}); //   //7,8,42,8,7,7,8,9,7,8,8,11    
			//productTitletab.setWidths(column12CollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(pdfwithoutTotalAmtAndTaxFlag){
			try {
				productTitletab = new PdfPTable(6);
				productTitletab.setWidths(new float[]{1f,1f,4f,1f,1f,1f});     
			} catch (Exception e) {
			}
		}
		productTitletab.setWidthPercentage(100);

		
		Phrase srnoph = new Phrase("Sr.No", font8bold);
		PdfPCell srnocell = new PdfPCell(srnoph);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srnocell.setRowspan(2);
		productTitletab.addCell(srnocell);
		
		Phrase prodcodeph = new Phrase("Product Code", font8bold);
		PdfPCell prodcodecell = new PdfPCell(prodcodeph);
		prodcodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		prodcodecell.setRowspan(2);
		productTitletab.addCell(prodcodecell);
		
		Phrase descriptionph = new Phrase("Description of Goods", font8bold);
		PdfPCell descriptioncell = new PdfPCell(descriptionph);
		descriptioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptioncell.setRowspan(2);
		productTitletab.addCell(descriptioncell);
		
		Phrase hsnSacph = new Phrase("HSN", font8bold);
		PdfPCell hsnSaccell = new PdfPCell(hsnSacph);
		hsnSaccell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnSaccell.setRowspan(2);
		productTitletab.addCell(hsnSaccell);
		
		Phrase rateph = new Phrase("Price", font8bold);
		PdfPCell ratecell = new PdfPCell(rateph);
		ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratecell.setRowspan(2);
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(ratecell);
		}

		
		Phrase quantityph = new Phrase("Qty", font8bold);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		quantitycell.setRowspan(2);
		productTitletab.addCell(quantitycell);
		
		Phrase perph = new Phrase("UOM", font8bold);
		PdfPCell percell = new PdfPCell(perph);
		percell.setHorizontalAlignment(Element.ALIGN_CENTER);
		percell.setRowspan(2);
		productTitletab.addCell(percell);
		
		
		Phrase discPh = new Phrase("Discount", font8bold);
		PdfPCell discCell = new PdfPCell(discPh);
		discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		discCell.setRowspan(2);
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(discCell);
		}

		
		
		
		Phrase cgstpercentphrase1=new Phrase(" ",font8);
		PdfPCell cgstpercentphraseCell1=new PdfPCell(cgstpercentphrase1);
		//cgstpercentphraseCell1.setBorder(0);
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(cgstpercentphraseCell1);
		}

		
		
		Phrase cgstpercentphrase=new Phrase("TAX %",font8bold);
		PdfPCell cgstpercentphraseCell=new PdfPCell(cgstpercentphrase);
		cgstpercentphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cgstpercentphraseCell.setBorder(0);
		//cgstpercentphraseCell.setColspan(3);
		//cgstpercentphraseCell.setBorderWidthRight(0);
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(cgstpercentphrase);
		}

		Phrase cgstpercentphrase2=new Phrase(" ",font8);
		PdfPCell cgstpercentphraseCell2=new PdfPCell(cgstpercentphrase2);
		//cgstpercentphraseCell2.setBorder(0);
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(cgstpercentphraseCell2);
		}

		
//		Phrase cgstRateph = new Phrase("CGST", font8);
//		PdfPCell cgstRatecell = new PdfPCell(cgstRateph);
//		cgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cgstRatecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		cgstRatecell.setColspan(1);
//		productTitletab.addCell(cgstRatecell);
//		
//		Phrase sgstRateph = new Phrase("SGST", font8);
//		PdfPCell sgstRatecell = new PdfPCell(sgstRateph);
//		sgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		sgstRatecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		sgstRatecell.setColspan(1);
//		productTitletab.addCell(sgstRatecell);
//		
//		Phrase igstRateph = new Phrase("IGST", font8);
//		PdfPCell igstRatecell = new PdfPCell(igstRateph);
//		igstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		igstRatecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		igstRatecell.setColspan(1);
//		productTitletab.addCell(igstRatecell);
		
		Phrase amountph = new Phrase("Total", font8bold);
		PdfPCell amountcell = new PdfPCell(amountph);
		amountcell.setHorizontalAlignment(Element.ALIGN_CENTER);//Ashwini Patil Date:4-08-2023
		amountcell.setRowspan(2);
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(amountcell);
		}

		
		
		
//		cgstpercentphraseCell.setColspan(1);
//		cgstpercentphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cgstpercentphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		
		Phrase cgstRateph = new Phrase("CGST", font8bold);
		PdfPCell cgstRatecell = new PdfPCell(cgstRateph);
		cgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cgstRatecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cgstRatecell.setColspan(1);
		//productTitletab.addCell(cgstRatecell);
		
		Phrase sgstRateph = new Phrase("SGST", font8bold);
		PdfPCell sgstRatecell = new PdfPCell(sgstRateph);
		sgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sgstRatecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		sgstRatecell.setColspan(1);
		//productTitletab.addCell(sgstRatecell);
		
		Phrase igstRateph = new Phrase("IGST", font8bold);
		PdfPCell igstRatecell = new PdfPCell(igstRateph);
		igstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		igstRatecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		igstRatecell.setColspan(1);
		//productTitletab.addCell(igstRatecell);
		
		if(!pdfwithoutTotalAmtAndTaxFlag){
			productTitletab.addCell(cgstRatecell);
			//productTable.addCell(cgstamtphraseCell);
			productTitletab.addCell(sgstRatecell);
			//productTable.addCell(cgstamtphraseCell);
			productTitletab.addCell(igstRatecell);
			//productTable.addCell(cgstamtphraseCell);
			
		}

		
		
		try {
			document.add(productTitletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	private void createProductDetailsTab() {
		PdfPTable productDetailstab = new PdfPTable(12);
//		productDetailstab.setWidthPercentage(100);
		try {
			productDetailstab.setWidths(new float[]{7,9,39,8,9,6,8,10,7,8,8,11});
			//productDetailstab.setWidths(column12CollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(pdfwithoutTotalAmtAndTaxFlag){
			try {
				noOfLines = 36; 
				productDetailstab = new PdfPTable(6);
				productDetailstab.setWidths(new float[]{1f,1f,4f,1f,1f,1f});     
			} catch (Exception e) {
			}
		}
		productDetailstab.setWidthPercentage(100);
		
			for(int i = 0; i < po.getProductDetails().size(); i++){
				prouductCount = i;
				if (noOfLines <= 0) {
					prouductCount = i;
					break;
				}
				int lenght=po.getProductDetails().get(i).getProductName().length();
				logger.log(Level.SEVERE,"Length : "+lenght);
				int lines= (int) Math.ceil((lenght/40.0)); //old value length/36 Changed on 16-1-2024
				logger.log(Level.SEVERE,"LINES : "+lines);
//				noOfLines = noOfLines - 1;
				noOfLines = noOfLines - lines;
				
				logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
				
				
				
				
				// Sr.No
				Phrase srnoValph = new Phrase(i+1+"", font8);
				PdfPCell srnoValcell = new PdfPCell(srnoValph);
				srnoValcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				//srnoValcell.setBorderWidthBottom(0);
				//srnoValcell.setBorderWidthTop(0);
				productDetailstab.addCell(srnoValcell);
				
				//Product Code
				Phrase prodcodevalph = new Phrase((po.getProductDetails().get(i).getProductCode()), font8);
				PdfPCell prodcodevalcell = new PdfPCell(prodcodevalph);
				prodcodevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				prodcodevalcell.setPaddingBottom(3f);
				//prodcodevalcell.setBorderWidthBottom(0);
				//prodcodevalcell.setBorderWidthTop(0);
				productDetailstab.addCell(prodcodevalcell);
				
				//Product Name
				Phrase descriptionValph = new Phrase(po.getProductDetails().get(i).getProductName(), font8);
				PdfPCell descriptionValcell = new PdfPCell(descriptionValph);
				descriptionValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				descriptionValcell.setPaddingBottom(3f);
				//descriptionValcell.setBorderWidthBottom(0);
				//descriptionValcell.setBorderWidthTop(0);
				productDetailstab.addCell(descriptionValcell);
				
				
				//Hsn Code
				Phrase hsnSacValph = new Phrase(po.getProductDetails().get(i).getPrduct().getHsnNumber(), font8);
				PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
				hsnSacValcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				hsnSacValcell.setPaddingBottom(3f);
				//hsnSacValcell.setBorderWidthBottom(0);
				//hsnSacValcell.setBorderWidthTop(0);
				productDetailstab.addCell(hsnSacValcell);
				
				
				//Price
				SuperProduct product=po.getProductDetails().get(i).getPrduct();
				double tax=removeAllTaxes(product);
				double origPrice=po.getProductDetails().get(i).getProdPrice()-tax;
				
				Phrase rateph = new Phrase(df.format(origPrice)+"", font8);
				PdfPCell ratecell = new PdfPCell(rateph);
				ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				ratecell.setPaddingBottom(3f);
				//ratecell.setBorderWidthBottom(0);
				//ratecell.setBorderWidthTop(0);
				if(!pdfwithoutTotalAmtAndTaxFlag){
					productDetailstab.addCell(ratecell);
				}

				
				//Quantity
				double quantity =po.getProductDetails().get(i).getProductQuantity();
				
				Phrase quantityph = new Phrase(df2.format(quantity)+"", font8);
				PdfPCell quantitycell = new PdfPCell(quantityph);
				quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
				quantitycell.setPaddingBottom(3f);
				//quantitycell.setBorderWidthBottom(0);
				//quantitycell.setBorderWidthTop(0);
				productDetailstab.addCell(quantitycell);

				
				
				//UOM
				Phrase perph = new Phrase(po.getProductDetails().get(i).getUnitOfmeasurement(), font8);
				PdfPCell percell = new PdfPCell(perph);
				percell.setHorizontalAlignment(Element.ALIGN_CENTER);
				percell.setPaddingBottom(3f);
				//percell.setBorderWidthBottom(0);
				//percell.setBorderWidthTop(0);
				productDetailstab.addCell(percell);
				
				//Discount
				String discountAmt=0+"";
				if(po.getProductDetails().get(i).getDiscountAmt()!=0){
					discountAmt=po.getProductDetails().get(i).getDiscountAmt()+"";
				}
				
				if(po.getProductDetails().get(i).getDiscount()!=0){
					discountAmt = po.getProductDetails().get(i).getDiscount()+"%";
				}
				Phrase discPh = null;
				if(!discountAmt.equals("0")){
					discPh = new Phrase(discountAmt, font8);
				}else{
					discPh = new Phrase("0", font8);
				}
				PdfPCell discCell = new PdfPCell(discPh);
				discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				discCell.setPaddingBottom(3f);
				//discCell.setBorderWidthBottom(0);
				//discCell.setBorderWidthTop(0);
				if(!pdfwithoutTotalAmtAndTaxFlag){
					productDetailstab.addCell(discCell);
				}

				
				
				double cgstper=0;
				double sgstper=0;
				double igstper=0;
				
				if(po.getProductDetails().get(i).getPurchaseTax1()!=null) { //Ashwini Patil Date:1-08-2022 added condition to avoid null pointer exception
				if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("CGST")||
						po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("SGST")){
					cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
					sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
				}
				else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("SGST")||
						po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("CGST")){
					sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
					cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
				}
				else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("IGST")||
						po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("")){
					
					igstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
				}
				else if(po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("IGST")||
						po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase(" ")){
					
					igstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
				}
				}
				
				//CGST
				
				Phrase cgstRateph = null;
				if(cgstper!=0){
					cgstRateph = new Phrase(cgstper+" ", font8);
				}else{
					cgstRateph = new Phrase("NA", font8);
				}
				
				PdfPCell cgstRatecell = new PdfPCell(cgstRateph);
				cgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cgstRatecell.setPaddingBottom(3f);
				//cgstRatecell.setBorderWidthBottom(0);
				//cgstRatecell.setBorderWidthTop(0);
				if(!pdfwithoutTotalAmtAndTaxFlag){
					productDetailstab.addCell(cgstRatecell);
				}

				
				//SGST
				Phrase sgstRateph = null;
				if(cgstper!=0){
					sgstRateph = new Phrase(sgstper+" ", font8);
				}else{
					sgstRateph = new Phrase("NA", font8);
				}
				//Phrase sgstRateph = new Phrase(sgstper+" ", font8);
				PdfPCell sgstRatecell = new PdfPCell(sgstRateph);
				sgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				sgstRatecell.setPaddingBottom(3f);
				//sgstRatecell.setBorderWidthBottom(0);
				//sgstRatecell.setBorderWidthTop(0);
				if(!pdfwithoutTotalAmtAndTaxFlag){
					productDetailstab.addCell(sgstRatecell);
				}

				
				//IGST
				Phrase igstRateph = null;
				if(igstper!=0){
					igstRateph = new Phrase(igstper+" ", font8);
				}else{
					igstRateph = new Phrase("NA", font8);
				}
				PdfPCell igstRatecell = new PdfPCell(igstRateph);
				igstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
				igstRatecell.setPaddingBottom(3f);
				//igstRatecell.setBorderWidthBottom(0);
				//igstRatecell.setBorderWidthTop(0);
				if(!pdfwithoutTotalAmtAndTaxFlag){
					productDetailstab.addCell(igstRatecell);
				}

				

                //TOTAL
				double total = 0;		
				if ((po.getProductDetails().get(i).getDiscount() == null || po.getProductDetails().get(i).getDiscount() == 0)
						&& (po.getProductDetails().get(i).getDiscountAmt() == 0)) {
					total = origPrice * po.getProductDetails().get(i).getProductQuantity();
				} else if ((po.getProductDetails().get(i).getDiscount() != null)
						&& (po.getProductDetails().get(i).getDiscountAmt() != 0)) {
					total = origPrice
							- (origPrice * po.getProductDetails().get(i).getDiscount() / 100);
					total = total - po.getProductDetails().get(i).getDiscountAmt();
					total = total * po.getProductDetails().get(i).getProductQuantity();
				} else {
					if (po.getProductDetails().get(i).getDiscount() != null) {
						total = origPrice
								- (origPrice * po.getProductDetails().get(i).getDiscount() / 100);
					} else {
						total = origPrice - po.getProductDetails().get(i).getDiscountAmt();
					}
					total = total * po.getProductDetails().get(i).getProductQuantity();
				}
				
				Phrase amountph = new Phrase(df.format(Math.round((total)))+"", font8);//komal
				PdfPCell amountcell = new PdfPCell(amountph);
				amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				amountcell.setPaddingBottom(3f);
				//amountcell.setBorderWidthBottom(0);
				//amountcell.setBorderWidthTop(0);
				if(!pdfwithoutTotalAmtAndTaxFlag){
					productDetailstab.addCell(amountcell);

				}


				
				
			}
		
		
		
			try {
				document.add(productDetailstab);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
	
	}

	private double removeAllTaxes(SuperProduct entity) {
		
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		
		if (entity instanceof ServiceProduct) {
			
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getPurchaseTax1()!= null&& prod.getPurchaseTax1().isInclusive() == true) {
				service = prod.getPurchaseTax1().getPercentage();
			}
			if (prod.getPurchaseTax2() != null&& prod.getPurchaseTax2().isInclusive() == true) {
				vat = prod.getPurchaseTax2().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getPurchaseTax2() != null&& prod.getPurchaseTax2().isInclusive() == true) {
				vat = prod.getPurchaseTax2().getPercentage();
			}
			if (prod.getPurchaseTax1() != null&& prod.getPurchaseTax1().isInclusive() == true) {
				service = prod.getPurchaseTax1().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = (entity.getPurchasePrice() / (1 + (vat / 100)));
			retrVat = entity.getPurchasePrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = (entity.getPurchasePrice() / (1 + service / 100));
			retrServ = entity.getPurchasePrice() - retrServ;
		}
		if (service != 0 && vat != 0) {


			ItemProduct prod=(ItemProduct) entity;
			if(prod.getPurchaseTax1().getTaxPrintName()!=null && ! prod.getPurchaseTax1().getTaxPrintName().equals("")
			   && prod.getPurchaseTax2().getTaxPrintName()!=null && ! prod.getPurchaseTax2().getTaxPrintName().equals(""))
			{

				double dot = service + vat;
				retrServ=(entity.getPurchasePrice()/(1+dot/100));
				retrServ=entity.getPurchasePrice()-retrServ;

			}else{
			double removeServiceTax = (entity.getPurchasePrice() / (1 + service / 100));
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPurchasePrice() - retrServ;
			
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}
	
	

	private void taxTableDetails(){
		PdfPTable productDetailstab = new PdfPTable(3);
		productDetailstab.setWidthPercentage(100);
		
		try {

			productDetailstab.setWidths(new float[]{96,23,11});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);

		
		
		Phrase amtChargeableph = new Phrase("  Amount In Words : "+SalesInvoicePdf.convert(po.getNetpayble())+" Rupees Only", font8);
		PdfPCell amtChargeablecell = new PdfPCell(amtChargeableph);
		amtChargeablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(amtChargeablecell);
		
	
		PdfPTable middleTable = new PdfPTable(1);
		middleTable.setWidthPercentage(100);

		
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);
		
			for (int i = 0; i < po.getProductTaxes().size(); i++) {
			double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
			//String cgstTotalVal = "NA",sgstTotalVal = "NA",igstTotalVal = "NA";
			if ( po.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal+  po.getProductTaxes().get(i).getChargePayable();
				/** date 20.9.2018 added by komal for CGST %)*/
				double percent = 0;
				if(po.getProductTaxes().get(i).getChargePercent()!=null){
					percent = po.getProductTaxes().get(i).getChargePercent();
				}
				Phrase IGSTphrase = new Phrase(" IGST "+percent+"%", font8bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				//IGSTphraseCell.setBorderWidthBottom(0);
				//IGSTphraseCell.setBorderWidthTop(0);
				IGSTphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				productDetailstab.addCell(IGSTphraseCell);
				
				
				Phrase IGSTValphrase = new Phrase(df.format(igstTotalVal), font8bold);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				//IGSTValphraseCell.setBorderWidthBottom(0);
				//IGSTValphraseCell.setBorderWidthTop(0);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
				//productDetailstab.addCell(IGSTphraseCell);
				middleTable.addCell(IGSTphraseCell);
				rightTable.addCell(IGSTValphraseCell);

			} else if (po.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal+ po.getProductTaxes().get(i).getChargePayable();
				/** date 20.9.2018 added by komal for CGST %)*/
				double percent = 0;
				if(po.getProductTaxes().get(i).getChargePercent()!=null){
					percent = po.getProductTaxes().get(i).getChargePercent();
				}
				Phrase SGSTphrase = new Phrase(" SGST "+percent+"%", font8bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				//SGSTphraseCell.setBorderWidthBottom(0);
				//SGSTphraseCell.setBorderWidthTop(0);
				SGSTphraseCell.setBorder(0);
				SGSTphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				
				Phrase SGSTValphrase = new Phrase(df.format(sgstTotalVal), font8bold);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				//SGSTValphraseCell.setBorderWidthTop(0);;
				//SGSTValphraseCell.setBorderWidthBottom(0);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				

//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(SGSTphraseCell);
//				productDetailstab.addCell(SGSTphraseCell);
//				productDetailstab.addCell(SGSTValphraseCell);
				
				middleTable.addCell(SGSTphraseCell);
				rightTable.addCell(SGSTValphraseCell);
			}
			else if (po.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal+ po.getProductTaxes().get(i).getChargePayable();
				/** date 20.9.2018 added by komal for CGST %)*/
				double percent = 0;
				if(po.getProductTaxes().get(i).getChargePercent()!=null){
					percent = po.getProductTaxes().get(i).getChargePercent();
				}
				Phrase CGSTphrase = new Phrase(" CGST "+percent+"%",font8bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				//CGSTphraseCell.setBorderWidthBottom(0);
				//CGSTphraseCell.setBorderWidthTop(0);
				CGSTphraseCell.setBorder(0);
				CGSTphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				//productDetailstab.addCell(CGSTphraseCell);
				
				Phrase CGSTValphrase = new Phrase(df.format(cgstTotalVal), font8bold);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				//CGSTValphraseCell.setBorderWidthBottom(0);
				//CGSTValphraseCell.setBorderWidthTop(0);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);
				
//
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(blankcell);
//				productDetailstab.addCell(CGSTphraseCell);
//				productDetailstab.addCell(CGSTValphraseCell);
				
				middleTable.addCell(CGSTphraseCell);
				rightTable.addCell(CGSTValphraseCell);

				
				}
			
			}
			
			double roundOffAmt = 0;
			if(po.getRoundOffAmount()!=0){
				roundOffAmt = po.getRoundOffAmount();
			}
			boolean rountoff = false;
			if (roundOffAmt != 0) {
				rountoff = true;
			}
			if (rountoff == true) {
				Phrase rountoffPhrase = new Phrase("Round Off   " + "", font8bold);
				PdfPCell rountoffCell = new PdfPCell(rountoffPhrase);
				rountoffCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				//rountoffCell.setBorder(0);
				//productDetailstab.addCell(rountoffCell);
				
				Phrase roundoffValPhrase = new Phrase(df.format(roundOffAmt) + "", font8);
				PdfPCell roundoffValCell = new PdfPCell(roundoffValPhrase);
				roundoffValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				//roundoffValCell.setBorder(0);
				//productDetailstab.addCell(roundoffValCell);
				
				middleTable.addCell(rountoffCell);
				rightTable.addCell(roundoffValCell);
		}
		
			
			PdfPCell leftCell = new PdfPCell(leftTable);
			//leftCell.setBorder(0);
			// leftCell.addElement();
			
			PdfPCell middleCell = new PdfPCell(middleTable);
			//middleCell.setBorder(0);
			
			
			PdfPCell rightCell = new PdfPCell(rightTable);
			// rightCell.setBorder(0);
			// rightCell.addElement();
			
			
			
			productDetailstab.addCell(leftCell);
			productDetailstab.addCell(middleCell);
			productDetailstab.addCell(rightCell);
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void createFooterAmountPart() {  // basic total

		double totalAssAmount = 0;

		for(int i = 0; i < po.getProductDetails().size(); i++){
		
			double taxValue = 0;
			if(po.getProductDetails().get(i).getTotal()!=0)
			{
				taxValue = po.getProductDetails().get(i).getTotal();
			}
			
			totalAssAmount = totalAssAmount + taxValue;
		}
		PdfPTable productDetailstab = new PdfPTable(2);
		productDetailstab.setWidthPercentage(100);
		
		try {

			productDetailstab.setWidths(new float[]{119,11});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase totalamount = new Phrase("Total", font8bold);
		PdfPCell totalAmountCell = new PdfPCell(totalamount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalAmountCell);


		System.out.println("totalAssAmount:::" + totalAssAmount);
		Phrase totalValamount = new Phrase(df.format(totalAssAmount), font8);
		PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
		totalAmountValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalAmountValCell);

		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void createTotalTab() {
		
		PdfPTable productDetailstab = new PdfPTable(2);
//		PdfPTable productDetailstab = new PdfPTable(9);
		productDetailstab.setWidthPercentage(100);
		
		try {
//			productDetailstab.setWidths(new float[]{7,12,53,10,10,10,10,7,10});
//			productDetailstab.setWidths(new float[]{6,11,53,9,9,9,9,6,9,9});
			//productDetailstab.setWidths(new float[]{5,8,46,8,7,7,8,7,7,8,8,11});
			productDetailstab.setWidths(new float[]{119,11});
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		productDetailstab.addCell(blankcell);
//		
//		productDetailstab.addCell(blankcell);
//		
//		
//		
//		productDetailstab.addCell(blankcell);
//		productDetailstab.addCell(blankcell);
//		
//		
//		productDetailstab.addCell(blankcell);
//		productDetailstab.addCell(blankcell);
//		productDetailstab.addCell(blankcell);
//		productDetailstab.addCell(blankcell);
		
		Phrase totalph = new Phrase("Net Payable", font8bold);
		PdfPCell totalcell = new PdfPCell(totalph);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalcell);
		
		Phrase totalAmtph = new Phrase(df2.format(po.getNetpayble()), font8);
		PdfPCell totalAmtcell = new PdfPCell(totalAmtph);
		totalAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalAmtcell);
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	private void createCompanyNameAsFooter(Document doc, Company comp2) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 20f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 try
//		 {
//		 Image
//		 image1=Image.getInstance("images/header.jpg");
//		 image1.scalePercent(13f);
//		 image1.scaleAbsoluteWidth(520f);
//		 image1.setAbsolutePosition(40f,20f);
//		 doc.add(image1);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
	}
	
private void createFooterTab() {
		
		PdfPTable leftFootertab = new PdfPTable(1);
		leftFootertab.setWidthPercentage(100);
		
		
		
//		leftFootertab.addCell(blankcell);
//		leftFootertab.addCell(blankcell);
//		leftFootertab.addCell(blankcell);
//		leftFootertab.addCell(blankcell);
		
		String comment=null;
		if(po.getDescription()!=null){
			comment=""+po.getDescription();
		}else{
			comment="";
		}
		
		Phrase remarkph = new Phrase("Remarks : "+"\n"+"\n"+ comment, font8);//komal
		PdfPCell remarkcell = new PdfPCell(remarkph);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell.setBorder(0);
		remarkcell.setBorderWidthBottom(0);
		remarkcell.setPaddingLeft(5f);
		leftFootertab.addCell(remarkcell);
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
		
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		
		PdfPTable outerFootertab = new PdfPTable(1);
		outerFootertab.setWidthPercentage(100);
		
		PdfPCell uppercell = new PdfPCell(leftFootertab);
		uppercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//uppercell.setBorderWidthBottom(0);
		//uppercell.setBorderWidthRight(0);
		
		
		
		if(comment!=null&&!comment.equals("")) //Ashwini Patil Date:4-08-2023 If no remark present then no need to show that label as well as blank cell
			outerFootertab.addCell(uppercell);
		
		Phrase declarationph=new Phrase ("This is a computer-generated document and does not carry a signature. ",font8);
		PdfPCell declarationcell = new PdfPCell(declarationph);
		declarationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		declarationcell.setBorder(0);
		//declarationcell.setColspan(2);
		declarationcell.setPaddingTop(5);
		if(comment==null||comment.equals("")){
			declarationcell.setBorderWidthTop(1);
		}
			
			
		outerFootertab.addCell(declarationcell);
		
		
		try {
			document.add(outerFootertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

private void createTermsAndCondition() {

	
	//int remainingLinesForTerms = 5;

	
	Phrase termNcondVal=null;
	
	
		String comment=null;
		if(po.getDescription()!=null){
			comment=po.getDescription();
		}else{
			comment="";
		}
		
	
	termNcondVal = new Phrase("Remarks: \n" + comment, font10bold);
	

	PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
	termNcondValCell.setBorderWidthBottom(0);
	termNcondValCell.setBorderWidthTop(0);
	
	PdfPTable pdfTable = new PdfPTable(1);
	pdfTable.setWidthPercentage(100);
	pdfTable.addCell(termNcondValCell);

	Phrase blankPhrase = new Phrase(" ", font10bold);
	PdfPCell blank = new PdfPCell(blankPhrase);
	blank.setBorderWidthBottom(0);
	blank.setBorderWidthTop(0);
	
	PdfPCell pdfPcell = new PdfPCell(pdfTable);
	pdfPcell.setBorder(0);

	PdfPTable table1 = new PdfPTable(1);
	table1.setWidthPercentage(100);
	table1.addCell(pdfPcell);
	try {
		document.add(table1);
	} catch (DocumentException e) {
		e.printStackTrace();
	}

}


private void createSpcingForHeading() {
	
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , po.getCompanyId())){
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
			
	}
	else{
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
	}

}

private void Createblank() {
	
    Paragraph blank =new Paragraph();
    blank.add(Chunk.NEWLINE);
    try {
		document.add(blank);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
		
}

private void createCompanyNameAsHeader(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadHeader();

	// patch
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl
				+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 725f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
}

private void createLogo(Document doc, Company comp) {
	//********************logo for server ********************
		DocumentUpload document =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}






private void createProductDetailsForAnnextureTab(int prouductCount2) {

			
		createProductTitleTab();
	
		noOfLines=55;
		logger.log(Level.SEVERE,"Annexture TABLE : "+noOfLines);	
	
	
		PdfPTable productDetailstab = new PdfPTable(12);
//		productDetailstab.setWidthPercentage(100);
		try {
			productDetailstab.setWidths(new float[]{7,9,39,8,9,6,8,10,7,8,8,11});
			//productDetailstab.setWidths(column12CollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(pdfwithoutTotalAmtAndTaxFlag){
			try {
				productDetailstab = new PdfPTable(6);
				productDetailstab.setWidths(new float[]{1f,1f,4f,1f,1f,1f});     
			} catch (Exception e) {
			}
		}
		productDetailstab.setWidthPercentage(100);
	
		for(int i = prouductCount2; i < po.getProductDetails().size(); i++){
			prouductCount = i;
			if (noOfLines <= 0) {
				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
//				logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines + "product no . "+i);
				prouductCount = i;
				//recursiveFlag=true;
				break;
			}
			int lenght=po.getProductDetails().get(i).getProductName().length();
//			logger.log(Level.SEVERE,"Length : "+lenght);
			int lines= (int) Math.ceil((lenght/40.0));//old value length/36 Changed on 16-1-2024
//			logger.log(Level.SEVERE,"LINES : "+lines);
//			noOfLines = noOfLines - 1;
			noOfLines = noOfLines - lines;
			
			// Sr.No
			Phrase srnoValph = new Phrase(i+1+"", font8);
			PdfPCell srnoValcell = new PdfPCell(srnoValph);
			srnoValcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			//srnoValcell.setBorderWidthBottom(0);
			//srnoValcell.setBorderWidthTop(0);
			productDetailstab.addCell(srnoValcell);
			
			//Product Code
			Phrase prodcodevalph = new Phrase((po.getProductDetails().get(i).getProductCode()), font8);
			PdfPCell prodcodevalcell = new PdfPCell(prodcodevalph);
			prodcodevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodcodevalcell.setPaddingBottom(3f);
			//prodcodevalcell.setBorderWidthBottom(0);
			//prodcodevalcell.setBorderWidthTop(0);
			productDetailstab.addCell(prodcodevalcell);
			
			//Product Name
			Phrase descriptionValph = new Phrase(po.getProductDetails().get(i).getProductName(), font8);
			PdfPCell descriptionValcell = new PdfPCell(descriptionValph);
			descriptionValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			descriptionValcell.setPaddingBottom(3f);
			//descriptionValcell.setBorderWidthBottom(0);
			//descriptionValcell.setBorderWidthTop(0);
			productDetailstab.addCell(descriptionValcell);
			
			
			//Hsn Code
			Phrase hsnSacValph = new Phrase(po.getProductDetails().get(i).getPrduct().getHsnNumber(), font8);
			PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
			hsnSacValcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			hsnSacValcell.setPaddingBottom(3f);
			//hsnSacValcell.setBorderWidthBottom(0);
			//hsnSacValcell.setBorderWidthTop(0);
			productDetailstab.addCell(hsnSacValcell);
			
			
			//Price
			SuperProduct product=po.getProductDetails().get(i).getPrduct();
			double tax=removeAllTaxes(product);
			double origPrice=po.getProductDetails().get(i).getProdPrice()-tax;
			
			Phrase rateph = new Phrase(df.format(origPrice)+"", font8);
			PdfPCell ratecell = new PdfPCell(rateph);
			ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			ratecell.setPaddingBottom(3f);
			//ratecell.setBorderWidthBottom(0);
			//ratecell.setBorderWidthTop(0);
			if(!pdfwithoutTotalAmtAndTaxFlag){
				productDetailstab.addCell(ratecell);
			}
			
			//Quantity
			double quantity =po.getProductDetails().get(i).getProductQuantity();
			
			Phrase quantityph = new Phrase(df2.format(quantity)+"", font8);
			PdfPCell quantitycell = new PdfPCell(quantityph);
			quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			quantitycell.setPaddingBottom(3f);
			//quantitycell.setBorderWidthBottom(0);
			//quantitycell.setBorderWidthTop(0);
			productDetailstab.addCell(quantitycell);
			
			
			//UOM
			Phrase perph = new Phrase(po.getProductDetails().get(i).getUnitOfmeasurement(), font8);
			PdfPCell percell = new PdfPCell(perph);
			percell.setHorizontalAlignment(Element.ALIGN_CENTER);
			percell.setPaddingBottom(3f);
			//percell.setBorderWidthBottom(0);
			//percell.setBorderWidthTop(0);
			productDetailstab.addCell(percell);
			
			//Discount
			String discountAmt=0+"";
			if(po.getProductDetails().get(i).getDiscountAmt()!=0){
				discountAmt=po.getProductDetails().get(i).getDiscountAmt()+"";
			}
			
			if(po.getProductDetails().get(i).getDiscount()!=0){
				discountAmt = po.getProductDetails().get(i).getDiscount()+"%";
			}
			Phrase discPh = null;
			if(!discountAmt.equals("0")){
				discPh = new Phrase(discountAmt, font8);
			}else{
				discPh = new Phrase("0", font8);
			}
			PdfPCell discCell = new PdfPCell(discPh);
			discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			discCell.setPaddingBottom(3f);
			//discCell.setBorderWidthBottom(0);
			//discCell.setBorderWidthTop(0);
			if(!pdfwithoutTotalAmtAndTaxFlag){
				productDetailstab.addCell(discCell);
			}

			
			
			double cgstper=0;
			double sgstper=0;
			double igstper=0;
			
			if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("CGST")||
					po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("SGST")){
				cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
				sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
			}
			else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("SGST")||
					po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("CGST")){
				sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
				cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
			}
			else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("IGST")||
					po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("")){
				
				igstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
			}
			else if(po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("IGST")||
					po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase(" ")){
				
				igstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
			}
			
			//CGST
			
			Phrase cgstRateph = null;
			if(cgstper!=0){
				cgstRateph = new Phrase(cgstper+" ", font8);
			}else{
				cgstRateph = new Phrase("NA", font8);
			}
			
			PdfPCell cgstRatecell = new PdfPCell(cgstRateph);
			cgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cgstRatecell.setPaddingBottom(3f);
			//cgstRatecell.setBorderWidthBottom(0);
			//cgstRatecell.setBorderWidthTop(0);
			if(!pdfwithoutTotalAmtAndTaxFlag){
				productDetailstab.addCell(cgstRatecell);
			}

			
			//SGST
			Phrase sgstRateph = null;
			if(cgstper!=0){
				sgstRateph = new Phrase(sgstper+" ", font8);
			}else{
				sgstRateph = new Phrase("NA", font8);
			}
			//Phrase sgstRateph = new Phrase(sgstper+" ", font8);
			PdfPCell sgstRatecell = new PdfPCell(sgstRateph);
			sgstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			sgstRatecell.setPaddingBottom(3f);
			//sgstRatecell.setBorderWidthBottom(0);
			//sgstRatecell.setBorderWidthTop(0);
			productDetailstab.addCell(sgstRatecell);
			
			//IGST
			Phrase igstRateph = null;
			if(igstper!=0){
				igstRateph = new Phrase(igstper+" ", font8);
			}else{
				igstRateph = new Phrase("NA", font8);
			}
			PdfPCell igstRatecell = new PdfPCell(igstRateph);
			igstRatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			igstRatecell.setPaddingBottom(3f);
			//igstRatecell.setBorderWidthBottom(0);
			//igstRatecell.setBorderWidthTop(0);
			if(!pdfwithoutTotalAmtAndTaxFlag){
				productDetailstab.addCell(igstRatecell);
			}

			

            //TOTAL
			double total = 0;		
			if ((po.getProductDetails().get(i).getDiscount() == null || po.getProductDetails().get(i).getDiscount() == 0)
					&& (po.getProductDetails().get(i).getDiscountAmt() == 0)) {
				total = origPrice * po.getProductDetails().get(i).getProductQuantity();
			} else if ((po.getProductDetails().get(i).getDiscount() != null)
					&& (po.getProductDetails().get(i).getDiscountAmt() != 0)) {
				total = origPrice
						- (origPrice * po.getProductDetails().get(i).getDiscount() / 100);
				total = total - po.getProductDetails().get(i).getDiscountAmt();
				total = total * po.getProductDetails().get(i).getProductQuantity();
			} else {
				if (po.getProductDetails().get(i).getDiscount() != null) {
					total = origPrice
							- (origPrice * po.getProductDetails().get(i).getDiscount() / 100);
				} else {
					total = origPrice - po.getProductDetails().get(i).getDiscountAmt();
				}
				total = total * po.getProductDetails().get(i).getProductQuantity();
			}
			
			Phrase amountph = new Phrase(df.format(total)+"", font8);//komal
			PdfPCell amountcell = new PdfPCell(amountph);
			amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			amountcell.setPaddingBottom(3f);
			//amountcell.setBorderWidthBottom(0);
			//amountcell.setBorderWidthTop(0);
			if(!pdfwithoutTotalAmtAndTaxFlag){
				productDetailstab.addCell(amountcell);

			}


			
			
		}
	
	
	
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	
	
}

//Ashwini Patil Date:11-01-2023
private void createPaymentTermsTab(){
	PdfUtility pdfUtility = new PdfUtility();
	PdfPTable termsTable = new PdfPTable(3);
	termsTable.setWidthPercentage(100);
	try {
		termsTable.setWidths(new float[]{1,1,8});
		//productDetailstab.setWidths(column12CollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	termsTable.addCell(pdfUtility.getPdfCell("Payment Terms : ", font8bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 3, 0,Rectangle.BOX,0,0,0,-1,-1)).setPaddingLeft(5f);
	
	
	termsTable.addCell(pdfUtility.getPdfCell("Days", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,Rectangle.BOX,0,0,0,-1,0)).setPaddingLeft(5f);
	termsTable.addCell(pdfUtility.getPdfCell("Percent", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,0,0,-1,-1,-1,-1));
	termsTable.addCell(pdfUtility.getPdfCell("Comment", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,Rectangle.BOX,0,0,0,0,-1));

	ArrayList<PaymentTerms> paymentTermList= po.getPaymentTermsList();
	for(PaymentTerms pt:paymentTermList){
		termsTable.addCell(pdfUtility.getPdfCell(pt.getPayTermDays()+"", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,Rectangle.BOX,0,0,0,-1,0)).setPaddingLeft(5f);
		termsTable.addCell(pdfUtility.getPdfCell(pt.getPayTermPercent()+"", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,0,0,-1,-1,-1,-1));
		termsTable.addCell(pdfUtility.getPdfCell(pt.getPayTermComment()+"", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,Rectangle.BOX,0,0,0,0,-1));
	}
	try {
		if(paymentTermList!=null&&paymentTermList.size()>0)
			document.add(termsTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
