package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
public class ContractPdf {


	Logger logger = Logger.getLogger("Name of logger");

	public Document document;

	Company comp;
	Customer cust;
	Contract con;
	Phrase chunk;
	Service service;

	Phrase blank;
	Phrase col;

	PdfPCell pdfsr, pdftreat, pdfcont, pdfserv, pdfref, pdftprice;

	float[] columnWidth = { (float) 33.33, (float) 33.33, (float) 33.33 };
	float[] colWidth = { 0.5f, 4f, 3f, 2f, 2.9f, 2.4f };
	float[] colWidth1 = { 20f, 10f, 70f };
	float[] colWidth2 = { 20f, 10f, 20f, 50f };

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold;

	public ContractPdf() {
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
	}

	public void setContract(Long id) {

		con = ofy().load().type(Contract.class).id(id).now();

		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first().now();

		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("companyId", con.getCompanyId())
					.filter("count", con.getCustomerId()).first().now();

		if (con.getCompanyId() == null)
			service = ofy().load().type(Service.class)
					.filter("contractCount", con.getCount()).first().now();
		else
			service = ofy().load().type(Service.class)
					.filter("companyId", con.getCompanyId())
					.filter("contractCount", con.getCount()).first().now();

	}

	public void createPdf(String preprintStatus) {

		System.out.println("print createPdf");
		if (preprintStatus.contains("yes")) {
			System.out.println("in side yes condition");
			createLogo(document, comp);
			createSpcingForHeading();
		} else {
			System.out.println("in side no condition");
			createSpcingForHeading();
			if (comp.getUploadHeader() != null) {
				createCompanyNameAsHeader(document, comp);
			}

			if (comp.getUploadFooter() != null) {
				createCompanyNameAsFooter(document, comp);
			}
		}

		System.out.println("Inside create pdf ??????????????????????????????");
		createContractHeading();
		createContractDetail();
		createCustomerDetails();
		createContractTable();
		createPremisesTable();
		createInWordsDetail();
		createDetails();
		createDetails1();
		createTreatmentDetail();

		if (con.getStatus().equals(Contract.APPROVED)) {
			createServiceDues();
		}
		
		// else{
		// createServiceDues();
		// }

		createBlankTable();

		/**************************************/
		createBottom1Details();
		createBottom2Details();

		String amountInWord = ServiceInvoicePdf.convert(con.getNetpayable());

	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createSpcingForHeading() {

		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);

		try {
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	
	public void createContractHeading() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(blank1);
		bl1cell.setBorder(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase head = new Phrase("CONTRACT", font14bold);
		PdfPCell headcell = new PdfPCell(head);
		headcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable headtable = new PdfPTable(3);
		headtable.addCell(blcell);
		headtable.addCell(headcell);
		headtable.addCell(bl1cell);
		headtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(headtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);

		try {
			table.setWidths(columnWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/******************************************************************************************************/
	public void createContractDetail() {

		Phrase conNo = null;
		if (con.getRefNo() != null && con.getCount() != 0) {
			conNo = new Phrase("Contract Number :  " + con.getRefNo() + " / "
					+ con.getCount(), font10bold);
		}

		PdfPCell nocell = new PdfPCell(conNo);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(nocell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorderWidthTop(0);
		leftcell.setBorderWidthLeft(0);
		leftcell.setBorderWidthRight(0);
		/*********************************************************/

		Phrase conDate = null;
		if (con.getContractDate() != null) {
			conDate = new Phrase("Date : " + fmt.format(con.getContractDate()),
					font10bold);
		}

		PdfPCell dtcell = new PdfPCell(conDate);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(dtcell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorderWidthTop(0);
		rightcell.setBorderWidthRight(0);
		rightcell.setBorderWidthLeft(0);
		/*************************************************************/

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**********************************************************************************/

	public void createCustomerDetails() {

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Phrase blank1 = new Phrase(" ");
		PdfPCell blank1cell = new PdfPCell(blank1);
		blank1cell.setBorder(0);

		Phrase customer = new Phrase("Customer Details : ", font12bold);
		PdfPCell custcell = new PdfPCell(customer);
		custcell.setBorder(0);
		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name = null;
		if (cust.getCompanyName() != null && cust.getName() != null) {
			name = new Phrase("" + cust.getCompanyName(), font12);
		} else {
			name = new Phrase("" + cust.getName(), font12);
		}

		PdfPCell namecell = new PdfPCell(name);
		namecell.setBorder(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/********************************************************************/

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null) {
			logger.log(Level.SEVERE,"111111111111111111");
			if (!cust.getAdress().getAddrLine2().equals("")) {
				logger.log(Level.SEVERE,"22222222222222222");
				logger.log(Level.SEVERE,"hiii"+cust.getAdress().getAddrLine2());
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE,"333333333333333");
					custAdd1 = cust.getAdress().getAddrLine1() 
							+ "," + "\n" 
							+ cust.getAdress().getAddrLine2() 
							+ "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE,"4444444444444");
					custAdd1 = cust.getAdress().getAddrLine1()
							+ "," + "\n"
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				logger.log(Level.SEVERE,"5555555555555");
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE,"66666666666666666");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE,"77777777777777777");
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (!cust.getAdress().getLocality().equals("")) {
				custFullAdd1 = custAdd1 + "," + "\n"
						+ cust.getAdress().getLocality() + "," + "\n"
						+ cust.getAdress().getCity() + "," + "Pin :"
						+ cust.getAdress().getPin() + "," + "\n"
						+ cust.getAdress().getState() + "," + "\n"
						+ cust.getAdress().getCountry();

			} else {
				custFullAdd1 = custAdd1 + "\n" 
						+ cust.getAdress().getCity() + "," + "Pin :"
						+ cust.getAdress().getPin() + "," + "\n"
						+ cust.getAdress().getState() + "," + "\n"
						+ cust.getAdress().getCountry();
			}
		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		Phrase mobno = null;
		if (cust.getLandline() != null && cust.getCellNumber1() != null) {

			mobno = new Phrase("" + cust.getLandline() + "   /"
					+ cust.getCellNumber1(), font9);
		}

		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(custcell);
		lefttable.addCell(namecell);
		lefttable.addCell(custAddInfoCell);
		lefttable.addCell(mobcell);
		lefttable.setWidthPercentage(100f);

		PdfPCell leftcell = new PdfPCell(lefttable);
		// leftcell.setBorderWidthLeft(0);
		leftcell.setBorderWidthRight(0);

		/*************************************** for the right side *************************************************/

		Phrase poc = null;
		if (cust.getFullname() != null) {
			poc = new Phrase("Contact Person : " + cust.getFullname(),
					font9bold);
		}

		PdfPCell poccell = new PdfPCell(poc);
		poccell.setBorder(0);
		poccell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase email = null;
		if (cust.getEmail() != null) {
			email = new Phrase("Email : " + cust.getEmail(), font9bold);
		}

		PdfPCell emailcell = new PdfPCell(email);
		emailcell.setBorder(0);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);

		righttable.addCell(blank1cell);
		righttable.addCell(poccell);
		righttable.addCell(emailcell);
		righttable.setWidthPercentage(100f);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorderWidthLeft(0);

		PdfPTable parenttable = new PdfPTable(2);

		parenttable.addCell(leftcell);
		parenttable.addCell(rightcell);

		parenttable.setWidthPercentage(100);

		try {
			parenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createBottom1Details() {

		Phrase lines = new Phrase(
				"  Kindly issue a cheque in favour of V-Care "
						+ "\n"
						+ "  Please quote respective Contract Number in future correspondance with us. "
						+ "\n"
						+ "  Please Ascertain Terms & Conditions mentioned overleaf. ",
				font10);
		
		PdfPCell linecell = new PdfPCell(lines);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable linetable = new PdfPTable(1);
		linetable.addCell(linecell);
		linetable.setWidthPercentage(100);

		try {
			document.add(linetable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createBottom2Details() {

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Phrase blank1 = new Phrase(" ");
		PdfPCell blank1cell = new PdfPCell(blank1);
		blank1cell.setBorder(0);

		Phrase vcare = new Phrase(" For V-Care", font10);
		PdfPCell vcarecell = new PdfPCell(vcare);
		vcarecell.setBorder(0);
		vcarecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase autho = new Phrase(" Authorised Signatory", font10bold);
		PdfPCell authocell = new PdfPCell(autho);
		authocell.setBorder(0);
		authocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.setSpacingBefore(5f);
		lefttable.addCell(vcarecell);
		lefttable.addCell(blankcell);
		lefttable.addCell(blankcell);

		lefttable.addCell(blankcell);
		lefttable.addCell(blankcell);
		lefttable.addCell(authocell);
		lefttable.setSpacingAfter(3f);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorderWidthRight(0);

		/******************************************** for right side *************************************************/

		// Phrase mod = new Phrase("MODERN ABODES PVT LTD",font10);
		// PdfPCell modcell = new PdfPCell(mod);
		// modcell.setBorder(0);
		// modcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable righttable = new PdfPTable(1);
		righttable.setSpacingBefore(5f);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);

		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);
		righttable.setSpacingAfter(3f);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorderWidthLeft(0);

		PdfPTable parenttable = new PdfPTable(2);

		parenttable.addCell(leftcell);
		parenttable.addCell(rightcell);

		parenttable.setWidthPercentage(100);

		try {
			parenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(parenttable);
			
			if (con.getStatus().equals(Contract.APPROVED)) {
			if(con.getServiceScheduleList().size()>20)
			{
				document.newPage();
				getRemainingServices(20);
			}
			}
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createDetails() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		/*********************************************************/

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);

		/************************************************************/
		PdfPTable paytable = new PdfPTable(3);
		paytable.setWidthPercentage(100);
		
		for (int i = 0; i < con.getPaymentTermsList().size(); i++) {
			

			Phrase pay = new Phrase("  Payment Terms", font10bold);
			PdfPCell paycell = new PdfPCell(pay);
			paycell.setBorderWidthRight(0);
			paycell.setBorderWidthBottom(0);
			paycell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase pay1 = new Phrase(" "
					+ con.getPaymentTermsList().get(i).getPayTermPercent()
					+ " % "
					+ con.getPaymentTermsList().get(0).getPayTermComment(),
					font10bold);
			PdfPCell pay1cell = new PdfPCell(pay1);
			pay1cell.setBorder(0);
			pay1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

			
			if (i == 0) {
				paytable.addCell(paycell);
				paytable.addCell(colcell);
				paytable.addCell(pay1cell);
			} else {
				paytable.addCell(blankcell);
				paytable.addCell(colcell);
				paytable.addCell(pay1cell);
			}

			if (i == (con.getPaymentTermsList().size() - 1)) {
				paytable.addCell(blankcell);
				paytable.addCell(blankcell);
				paytable.addCell(blankcell);
			}

			try {
				paytable.setWidths(colWidth1);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

		}

		PdfPCell paytablefinalCell = new PdfPCell(paytable);
		PdfPTable paytablefinal = new PdfPTable(1);
		paytablefinal.addCell(paytablefinalCell);
		paytablefinal.setWidthPercentage(100);

		try {
			document.add(paytablefinal);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createDetails1() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
//		blank.add(Chunk.NEWLINE);
//		blank.add(Chunk.NEWLINE);
//		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		/************************************************************/
		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*************************************************/

		Phrase treatarea = new Phrase("  Treatment Area", font10bold);
		PdfPCell treatcell = new PdfPCell(treatarea);
		treatcell.setBorder(0);
		treatcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable treattable = new PdfPTable(4);
		treattable.setWidthPercentage(100);
		try {
			treattable.setWidths(colWidth2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0; i < con.getItems().size(); i++) {
			

			String productname = con.getItems().get(i).getProductName();
			String premisedet = null;
			if (con.getItems().get(i).getPremisesDetails() != null) {
				premisedet = "-  " + con.getItems().get(i).getPremisesDetails();
			} else {
				premisedet = "";
			}

			Phrase prod = new Phrase(productname, font10bold);
			PdfPCell prodcell = new PdfPCell(prod);
			prodcell.setBorder(0);
			prodcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase pre_data = new Phrase(premisedet, font10bold);
			PdfPCell pre_datacell = new PdfPCell(pre_data);
			pre_datacell.setBorder(0);
			pre_datacell.setHorizontalAlignment(Element.ALIGN_LEFT);

			if (i == 0) {
				treattable.addCell(treatcell);
				treattable.addCell(colcell);
				treattable.addCell(prodcell);
				treattable.addCell(pre_datacell);
			} else {
				treattable.addCell(blankcell);
				treattable.addCell(colcell);
				treattable.addCell(prodcell);
				treattable.addCell(pre_datacell);
			}
			
			if(i==((con.getItems().size())-1)){
				treattable.addCell(blankcell);
				treattable.addCell(blankcell);
				treattable.addCell(blankcell);
				treattable.addCell(blankcell);
				
			}
		}
		
		PdfPCell dummyCell = new PdfPCell(treattable);
		PdfPTable treatablefinal = new PdfPTable(1);
		treatablefinal.addCell(dummyCell);
		treatablefinal.setWidthPercentage(100f);
		try {
			document.add(treatablefinal);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/******************************************************************************************/

	public void createTreatmentDetail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		/***************************************************************/
		 Phrase col = new Phrase(":");
		 PdfPCell colcell = new PdfPCell(col);
		 colcell.setBorder(0);
		 colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/********************************************************************/
		 
		 Phrase treatdetail = new Phrase(" Treatment Details",font10bold);
		 PdfPCell detailcell = new PdfPCell(treatdetail);
		 detailcell.setBorder(0);
		 detailcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase treat1detail = null;
		if (con.getDescription() != null) {
			treat1detail = new Phrase(" " + con.getDescription(), font10bold);
		}

		PdfPCell treat1detailcell = new PdfPCell(treat1detail);
		treat1detailcell.setBorder(0);
		treat1detailcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable treat1table = new PdfPTable(3);
		treat1table.addCell(detailcell);
		treat1table.addCell(colcell);
		treat1table.addCell(treat1detailcell);
		
		treat1table.addCell(blankcell);
		treat1table.addCell(blankcell);
		treat1table.addCell(blankcell);
		
		try {
			treat1table.setWidths(colWidth1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		treat1table.setWidthPercentage(100f);
		
		PdfPCell treat1cell = new PdfPCell(treat1table); 
//		treat1cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(treat1cell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createServiceDues() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
//		blankcell.setBorderWidthTop(0);
		blankcell.setBorder(0);
		
/********************************************************************/		
		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
/********************************************************************/
		
		Phrase dues = new Phrase(" Service Dues              : ", font10bold);
		PdfPCell duescell = new PdfPCell(dues);
//		duescell.setBorder(0);
		duescell.setBorderWidthBottom(0);
		duescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
/*************************************************************************/		
//		Phrase dues1 = null;
//		if(service.getServiceDate() !=null){
//			dues1 =	new Phrase(" " + fmt1.format(service.getServiceDate()), font10bold);
//		}
//		PdfPCell dues1cell = new PdfPCell(dues1);
//		dues1cell.setBorder(0);
////		dues1cell.setBorderWidthBottom(0);
//		dues1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//
//		PdfPTable duestable = new PdfPTable(3);
//		duestable.addCell(duescell);
//		duestable.addCell(colcell);
//		duestable.addCell(dues1cell);
//		 
////		duestable.addCell(blankcell);
//		
//		try {
//			duestable.setWidths(colWidth1);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		
//		duestable.setWidthPercentage(100f);
//		
//		PdfPCell duecell = new PdfPCell(duestable);
//		PdfPTable parenttable = new PdfPTable(1);
//		parenttable.addCell(duecell);
//		parenttable.setWidthPercentage(100);
//
//		try {
//			document.add(parenttable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
/********************************************************************************/		
		
		
		PdfPTable detailtable = new PdfPTable(1);
		detailtable.addCell(duescell);
		detailtable.setWidthPercentage(100);
		
		Phrase serv = null;
		
		PdfPTable table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		
		PdfPTable table2 = new PdfPTable(1);
		table2.setWidthPercentage(100);

		PdfPTable table3 = new PdfPTable(1);
		table3.setWidthPercentage(100);
		
		PdfPTable table4 = new PdfPTable(1);
		table4.setWidthPercentage(100);
		
		for(int i=0; i<con.getServiceScheduleList().size(); i++){
			
			if(con.getServiceScheduleList().get(i).getScheduleServiceDate() !=null){
				serv = new Phrase("    "+(i+1)+ "  : "+ fmt1.format(con.getServiceScheduleList().get(i).getScheduleServiceDate()), font10);	
			}
			
			PdfPCell servcell = new PdfPCell(serv);
			servcell.setBorder(0);
			servcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			if(i<5){
				table1.addCell(servcell);
			}
			else if(i>=5 && i<10){
				table2.addCell(servcell);
			}
			else if(i>=10 && i<15){
				table3.addCell(servcell);
			}
			else if(i>=15 && i<20){
				table4.addCell(servcell);
			}
//			detailtable.addCell(servcell);
			
//			if(i==(con.getServiceScheduleList().size()-1))
//			detailtable.addCell(blankcell);
			
			if(i==20){
				break;
			}
			
		}
		
		PdfPTable parenttable = new PdfPTable(4);
		parenttable.setWidthPercentage(100);
		
		PdfPCell cell1 = new PdfPCell(table1);
		cell1.setBorder(0);
		
		PdfPCell cell2 = new PdfPCell(table2);
		cell2.setBorder(0);
		
		PdfPCell cell3 = new PdfPCell(table3);
		cell3.setBorder(0);
		
		PdfPCell cell4 = new PdfPCell(table4);
		cell4.setBorder(0);
		
		parenttable.addCell(cell1);
		parenttable.addCell(cell2);
		parenttable.addCell(cell3);
		parenttable.addCell(cell4);
		
		
		
		PdfPCell parentcell = new PdfPCell(parenttable);
		parentcell.setBorderWidthTop(0);
		
		detailtable.addCell(parentcell);

		try {
			document.add(detailtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	public void createBlankTable() {

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blankcell);
//		bltable.addCell(blankcell);
//		bltable.addCell(blankcell);
//		bltable.addCell(blankcell);
//		bltable.addCell(blankcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createContractTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(6);

		try {
			table.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		table.setWidthPercentage(100);

		Phrase srno = new Phrase("Sr.", font1);
		Phrase treatment = new Phrase("Treatment Type", font1);
		Phrase contarct = new Phrase("Contract Period", font1);
		Phrase services = new Phrase("Services", font1);
		Phrase reference = new Phrase("Your Ref. No.", font1);
		Phrase price = new Phrase("Amount", font1);

		PdfPCell srnocell = new PdfPCell(srno);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell treatmentcell = new PdfPCell(treatment);
		treatmentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell contarctcell = new PdfPCell(contarct);
		contarctcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicescell = new PdfPCell(services);
		servicescell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell referencecell = new PdfPCell(reference);
		referencecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(srnocell);
		table.addCell(treatmentcell);
		table.addCell(contarctcell);
		table.addCell(servicescell);
		table.addCell(referencecell);
		table.addCell(pricecell);

		for (int i = 0; i < con.getItems().size(); i++) {
			

			chunk = new Phrase(i + 1 + "", font8);
			pdfsr = new PdfPCell(chunk);
			pdfsr.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(con.getItems().get(i).getProductName() + "",
					font8);
			pdftreat = new PdfPCell(chunk);
			pdftreat.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(fmt.format(con.getStartDate()) + " - "
					+ fmt.format(con.getEndDate()), font8);
			pdfcont = new PdfPCell(chunk);
			pdfcont.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(
					con.getItems().get(i).getNumberOfServices() + "", font8);
			pdfserv = new PdfPCell(chunk);
			pdfserv.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (con.getRefNo() != null) {
				chunk = new Phrase(con.getRefNo() + "", font8);
			}
			pdfref = new PdfPCell(chunk);
			pdfref.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(
					df.format(con.getItems().get(i).getPrice()) + "", font8);
			pdftprice = new PdfPCell(chunk);
			pdftprice.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfsr);
			table.addCell(pdftreat);
			table.addCell(pdfcont);
			table.addCell(pdfserv);
			table.addCell(pdfref);
			table.addCell(pdftprice);

		}

		PdfPCell contcell = new PdfPCell();
		contcell.addElement(table);
		contcell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(contcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPremisesTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		Font fontsample = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
		Font fontsample1 = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

		Phrase premises = new Phrase("Premises Details :", font12bold);
		PdfPCell precell = new PdfPCell(premises);
		precell.setBorder(0);
		precell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase area = null;
		if (con.getPremisesDesc() != null) {
			area = new Phrase("" + con.getPremisesDesc(), font10);
		}

		PdfPCell areacell = new PdfPCell(area);
		areacell.setBorder(0);
		areacell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(precell);
		lefttable.addCell(areacell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorderWidthRight(0);

		/************************************************* for center side **********************************************************/

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		// blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankcell.setBorderWidthLeft(0);
		blankcell.setBorderWidthRight(0);
		blankcell.setBorderWidthBottom(0);
		blankcell.setBorderWidthTop(0);

		PdfPTable centertable = new PdfPTable(1);
		centertable.addCell(blankcell);
		centertable.addCell(blankcell);

		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setBorderWidthRight(0);
		centercell.setBorderWidthLeft(0);

		/************************************************ for RIGHT side **********************************************************/

		Phrase total = new Phrase("Total", font1);
		PdfPCell totalcell = new PdfPCell(total);
		totalcell.setBorder(0);
		totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		double amount = 0;

		for (int i = 0; i < con.getItems().size(); i++) {
			amount = amount + con.getItems().get(i).getPrice();
		}

		Phrase ttl = new Phrase("" + amount, font1);
		PdfPCell ttlcell = new PdfPCell(ttl);
		ttlcell.setBorderWidthBottom(0);
		ttlcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase discount = new Phrase("Discount", fontsample);
		PdfPCell discountcell = new PdfPCell(discount);
		discountcell.setBorder(0);
		discountcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dis = new Phrase(""
				+ con.getItems().get(0).getPercentageDiscount(), fontsample);
		PdfPCell discell = new PdfPCell(dis);
		discell.setBorderWidthBottom(0);
		discell.setBorderWidthTop(0);
		discell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		/********************************************* for loop for right table **********************************************************/
		PdfPTable righttable = new PdfPTable(2);
		righttable.addCell(totalcell);
		righttable.addCell(ttlcell);

		righttable.addCell(discountcell);
		righttable.addCell(discell);

		for (int i = 0; i < con.getProductTaxes().size(); i++) {

			Phrase tax = new Phrase(""
					+ con.getProductTaxes().get(i).getChargeName(), fontsample);
			PdfPCell taxcell = new PdfPCell(tax);
			taxcell.setBorder(0);
			taxcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase servtax = new Phrase(
					""
							+ df.format(con.getProductTaxes().get(i)
									.getChargePayable()), fontsample);
			PdfPCell servtaxcell = new PdfPCell(servtax);
			servtaxcell.setBorderWidthBottom(0);
			servtaxcell.setBorderWidthTop(0);
			// servtaxcell.setBorder(0);
			servtaxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			righttable.addCell(taxcell);
			righttable.addCell(servtaxcell);

		}

		for (int i = 0; i < con.getProductCharges().size(); i++) {

			Phrase swach = new Phrase(""
					+ con.getProductCharges().get(i).getChargeName(),
					fontsample);
			PdfPCell swachcell = new PdfPCell(swach);
			swachcell.setBorder(0);
			swachcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase swachtax = new Phrase(""
					+ df.format(con.getProductCharges().get(i)
							.getChargePayable()), fontsample);
			PdfPCell swachtaxcell = new PdfPCell(swachtax);
			swachtaxcell.setBorderWidthBottom(0);
			swachtaxcell.setBorderWidthTop(0);
			// swachtaxcell.setBorder(0);
			swachtaxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			righttable.addCell(swachcell);
			righttable.addCell(swachtaxcell);

		}
		/********************************************** for loop ended here ******************************************************************/

		Phrase grand = new Phrase("Grand Total  ", fontsample1);
		PdfPCell grandcell = new PdfPCell(grand);
		grandcell.setBorder(0);
		grandcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase grandttl = new Phrase("" + con.getNetpayable(), fontsample1);
		PdfPCell grandttlcell = new PdfPCell(grandttl);
		// ttlcell.setBorder(0);
		grandttlcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		righttable.addCell(grandcell);
		righttable.addCell(grandttlcell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorderWidthLeft(0);

		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);
		table.setWidthPercentage(100);

		try {
			table.setWidths(columnWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createInWordsDetail() {

		logger.log(Level.SEVERE, "Grand Total" + con.getNetpayable());
		String amountInWord = ServiceInvoicePdf.convert(con.getNetpayable());
		Phrase rs = new Phrase("Rs.  " + amountInWord + "  Only", font10bold);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(rscell);
		amttable.setSpacingAfter(2f);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
public void getRemainingServices(int value){
		
	System.out.println("in side more than 20 service"+value);
	
		PdfPTable detailtable = new PdfPTable(1);
		detailtable.setWidthPercentage(100);
		
		System.out.println("Service list size "+con.getServiceScheduleList().size());
		for(int i=value; i<con.getServiceScheduleList().size(); i++){
			
			if(con.getServiceScheduleList().get(i).getScheduleServiceDate() !=null){
			Phrase serv = new Phrase("    "+(i+1)+ "  : "+ fmt.format(con.getServiceScheduleList().get(i).getScheduleServiceDate()), font10);	
			PdfPCell servcell = new PdfPCell(serv);
			servcell.setBorder(0);
			servcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			detailtable.addCell(servcell);
			}
		}
		
		

		try {
			document.add(detailtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}	
	}
}
