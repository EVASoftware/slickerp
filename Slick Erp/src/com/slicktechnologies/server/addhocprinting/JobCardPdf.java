package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;

public class JobCardPdf {
	Sales qp;
	ArrayList<Service> service;
	List<SalesLineItem> products;
	
	Customer cust;
	Company comp;
	public  Document document;

	public int serviceno=1;
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	
	public 	JobCardPdf()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		
	}
	
	public void setContract(long count)
	{
		
		//Load Contract 
				qp=ofy().load().type(Contract.class).id(count).now();
				int contractCount=qp.getCount();
				//Load Services
				List<Service>services=null;
				if(qp.getCompanyId()!=null)
				  services=ofy().load().type(Service.class).filter("contractCount",contractCount).
						  filter("companyId",qp.getCompanyId()).list();
				else
					services=ofy().load().type(Service.class).filter("contractCount",contractCount).list();
					
				service=new ArrayList<Service>();
				for(Service ser:services)
					service.add(ser);
			   //Load Customer
			   cust=ofy().load().type(Customer.class).filter("count",qp.getCustomerId()).first().now();
			  //Load Company
			   //comp=ofy().load().type(Company.class).id(qp.getCompanyId()).now();
			   if(qp.getCompanyId()==null)
			        comp=ofy().load().type(Company.class).first().now();
			   else
				   comp=ofy().load().type(Company.class).
						   filter("companyId",qp.getCompanyId()).first().now();
			   products=qp.getItems();
			   
	}
	
	public  void createPdf() {
		 
//	    showLogo(document, comp);  
		createCompanyHedding();
	      createCompanyAdress();
	   
	      
	      try {
			createHeaderTable();
			
			 createTableJobCard();
			 addFooter();
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	    }
	
	public  void createCompanyHedding()
	{
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16boldul);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     try {
			document.add(p);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	public void createCompanyAdress()
	{

		Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font12);
		Phrase adressline2=null;
		if(comp.getAddress().getAddrLine2()!=null)
		{
			adressline2= new Phrase(comp.getAddress().getAddrLine2(),font12);
		}
		
		
		Phrase landmark=null;
		Phrase locality=null;
		if(comp.getAddress().getLandmark()!=null&&comp.getAddress().getLocality().equals("")==false)
		{
			
			String landmarks=comp.getAddress().getLandmark()+" , ";
			locality= new Phrase(landmarks+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		else
		{
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin(),font12);
		}
		Paragraph adressPragraph=new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if(adressline2!=null)
		{
				adressPragraph.add(adressline2);
				adressPragraph.add(Chunk.NEWLINE);
		}
		
		adressPragraph.add(locality);
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);
		
		
		// Phrase for phone,landline ,fax and email
		
		Phrase titlecell=new Phrase("Mob :",font8bold);
		Phrase titleTele = new Phrase("Tele :",font8bold);
		Phrase titleemail= new Phrase("Email :",font8bold);
		Phrase titlefax= new Phrase("Fax :",font8bold);
		
		Phrase titleservicetax=new Phrase("Service Tax No: ",font8bold);
		Phrase titlevatatx=new Phrase("   Vat Tax No: ",font8bold);
		String servicetax=comp.getServiceTaxNo();
		String vatttax=comp.getVatTaxNo();
		Phrase servicetaxphrase=null;
		Phrase vattaxphrase=null;
		
		if(servicetax!=null&&servicetax.trim().equals("")==false)
		{
			servicetaxphrase=new Phrase(servicetax,font8);
		}
			

		if(vatttax!=null&&vatttax.trim().equals("")==false)
		{
			vattaxphrase=new Phrase(vatttax,font8);
		}
		
	
		
		
		// cell number logic
		String stringcell1=comp.getContact().get(0).getCellNo1()+"";
		String stringcell2=null;
		Phrase mob=null;
		if(comp.getContact().get(0).getCellNo2()!=-1)
			stringcell2=comp.getContact().get(0).getCellNo2()+"";
		if(comp.getContact().get(0).getCellNo2()!=0)
		    mob=new Phrase(stringcell1+" / "+stringcell2,font8);
		else
			 mob=new Phrase(stringcell1,font8);
		
		
		//LANDLINE LOGIC
		Phrase landline=null;
		if(comp.getContact().get(0).getLandline()!=-1||comp.getContact().get(0).getLandline()!=0)
			landline=new Phrase(comp.getContact().get(0).getLandline()+"",font8);
		
		
		
		
		// fax logic
		Phrase fax=null;
		if(comp.getContact().get(0).getFaxNo()!=null)
			fax=new Phrase(comp.getContact().get(0).getFaxNo()+"",font8);
		// email logic
		Phrase email= new Phrase(comp.getContact().get(0).getEmail(),font8);
		
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("        "));
		
		
		if(landline!=null)
		{
			
			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("        "));
		}
		
		
		
		if(fax!=null)
		{
			
			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}
		
		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk(" "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);
		
		if(servicetaxphrase!=null)
		{
			adressPragraph.add(titleservicetax);
			adressPragraph.add(servicetaxphrase);
			
		}
		
		if(vattaxphrase!=null)
		{
			adressPragraph.add(titlevatatx);
			adressPragraph.add(vattaxphrase);
			adressPragraph.add(new Chunk("            "));
			
		}
		
		adressPragraph.add(Chunk.NEWLINE);
		String title="";
		if(qp instanceof Contract)
			title="Contract";
			
		else
			title="Quotation";
		
		Phrase Ptitle= new Phrase(title,font12bold);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Ptitle);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		
		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
  public  void createHeaderTable() throws DocumentException
	{
		 Phrase nameTitle = new Phrase("Name :",font8bold);
		 Phrase phoneTitle= new Phrase("Phone :",font8bold);
		 Phrase adressTitle= new Phrase("Address :",font8bold);
		 Phrase emailTitle= new Phrase("Email :",font8bold);
		 
		 PdfPCell titlecustnameCell=new PdfPCell();
		 titlecustnameCell.addElement(nameTitle);
		 titlecustnameCell.setBorder(0);
		 PdfPCell titlecustPhonecell = new PdfPCell();
		 titlecustPhonecell.addElement(phoneTitle);
		 titlecustPhonecell.setBorder(0);
		 PdfPCell titleadressCell = new PdfPCell();
		 titleadressCell.addElement(adressTitle);
		 titleadressCell.setBorder(0);
		 PdfPCell titleemailCell = new PdfPCell();
		 titleemailCell.addElement(emailTitle);
		 titleemailCell.setBorder(0);
		
		 String fullname=cust.getFullname().trim();
		
		Phrase customername = new Phrase(fullname,font8);
		Phrase phonechunk = new Phrase(cust.getCellNumber1()+"",font8);
		Phrase emailchunk = new Phrase(cust.getEmail().trim()+"",font8);
		
		Phrase blankphrase = new Phrase("  ");
		PdfPCell blankcell = new PdfPCell(blankphrase);
		
		Phrase customeradress1= new Phrase(cust.getAdress().getAddrLine1(),font8);
		Phrase customeradress2=null;
		//Patch
		if(cust.getAdress().getAddrLine2()!=null)
		   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font8);
		
		Phrase locality= null;
		
		if(cust.getAdress().getLandmark()!=null)
		{
		    locality=new Phrase(cust.getAdress().getLandmark()+" , "+cust.getAdress().getLocality()+" , "+cust.getAdress().getCity()+" , "+cust.getAdress().getPin(),font8);
		}
		else{
			locality=new Phrase(cust.getAdress().getLocality()+" , "+cust.getAdress().getCity()+" , "+cust.getAdress().getPin(),font8);
		}
		
		Phrase blnkphrase=new Phrase(" ");
		 PdfPCell blnkcell=new PdfPCell();
		 blnkcell.addElement(blnkphrase);
		 blnkcell.setBorder(0);
		 PdfPCell custnameCell=new PdfPCell();
		 custnameCell.addElement(customername);
		 custnameCell.setBorder(0);
		 PdfPCell custPhonecell = new PdfPCell();
		 custPhonecell.addElement(phonechunk);
		 custPhonecell.setBorder(0);
		 PdfPCell adress1Cell = new PdfPCell();
		 adress1Cell.addElement(customeradress1);
		 adress1Cell.setBorder(0);
		 PdfPCell adress2Cell= new PdfPCell();
		 adress2Cell.addElement(customeradress2);
		 adress2Cell.setBorder(0);
		 PdfPCell localitycell = new PdfPCell();
		 localitycell.addElement(locality);
		 localitycell.setBorder(0);
		 PdfPCell emailcell = new PdfPCell();
		 emailcell.addElement(emailchunk);
		 emailcell.setBorder(0);
		
		String header;
		header = "ID :";
		
		Phrase billno = new Phrase(header,font8bold);
		Phrase orederdate= new Phrase("Date :",font8bold);
		Phrase duration= new Phrase("Duration :",font8bold);
		Phrase noofservices= new Phrase("Services:",font8bold);
		Phrase startDate = new Phrase("Start Date :",font8bold);
		Phrase endDate = new Phrase("End Date :",font8bold);
		
		 PdfPCell titlebillCell=new PdfPCell();
		 titlebillCell.addElement(billno);
		 PdfPCell titledatecell = new PdfPCell();
		 titledatecell.addElement(orederdate);
		 PdfPCell titledurationCell = new PdfPCell();
		 titledurationCell.addElement(duration);
		 PdfPCell titlenoServicesCell = new PdfPCell();
		 titlenoServicesCell.addElement(noofservices);
		 PdfPCell startdateCell = new PdfPCell();
		 startdateCell.addElement(startDate);
		 PdfPCell enddateCell = new PdfPCell();
		 enddateCell.addElement(endDate);
		 
		Phrase realqno=null;
	    realqno = new Phrase(qp.getCount()+"",font8);
		
		String date=fmt.format(qp.getCreationDate());
		Phrase realorederdate= new Phrase(date+"",font8);
		realorederdate.trimToSize();
		Phrase relduration= new Phrase(largestduration(),font8);
		
		int noserv=(int) totalServices();
		
		Phrase relnoservices= new Phrase(noserv+"",font8);
		
		Quotation quot=(Quotation) qp;
		String start;
		if(quot.getStartDate()==null){
			start="N.A";
		}
		else{
			start=fmt.format(quot.getStartDate());
		}
		
		Phrase realstartdate = new Phrase(start,font8);
		
		if(quot.getEndDate()==null)
			start="N.A";
		else
			start=fmt.format(quot.getEndDate());
		
		Phrase realenddate = new Phrase(start,font8);
		
		
		
		PdfPCell billCell=new PdfPCell();
		billCell.addElement(realqno);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(realorederdate);
		PdfPCell durationCell = new PdfPCell();
		durationCell.addElement(relduration);
		PdfPCell noServicesCell = new PdfPCell();
		noServicesCell.addElement(relnoservices);
		PdfPCell realstartdateCell = new PdfPCell();
		realstartdateCell.addElement(realstartdate);
		PdfPCell realenddateCell = new PdfPCell();
		realenddateCell.addElement(realenddate);
	     
		
		 PdfPTable table = new PdfPTable(2);
		 
		 // 1 st row name phone
		 table.addCell(titlecustnameCell);
		 table.addCell(custnameCell);
		 
		 table.addCell(titlecustPhonecell);
		 table.addCell(custPhonecell);
		 
		 // 2nd row adress and email
		 table.addCell(titleadressCell);
		 table.addCell(adress1Cell);
		 if(customeradress2!=null){
			 table.addCell(blnkcell);
			 table.addCell(adress2Cell);
		 }
		 table.addCell(blnkcell);
		 table.addCell(localitycell);
		 table.addCell(titleemailCell);
		 table.addCell(emailcell);
		 
		table.setWidths(new float[]{20,80});
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);
		 
		PdfPTable billtable = new PdfPTable(4);
		billtable.setWidths(new float[]{20,30,20,30});
		billtable.setWidthPercentage(100);
		billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

		//1  row bill number and date
		 billtable.addCell(titlebillCell);
		 billtable.addCell(billCell);
		 billtable.addCell(titledatecell);
		 billtable.addCell(datecell);
		 
		//2  row duration   and start date
		 billtable.addCell(titledurationCell);
		 billtable.addCell(durationCell);
		 billtable.addCell(startdateCell);
		 billtable.addCell(realstartdateCell);
		 
		//3  row no serivces   and end date
		 billtable.addCell(titlenoServicesCell);
		 billtable.addCell(noServicesCell);
		 billtable.addCell(enddateCell);
		 billtable.addCell(realenddateCell);
		 int row =3;
		 // 4 row valid untill and blank
		 if(qp instanceof Quotation)
		 {
			 PdfPCell titlevalid = new PdfPCell();
			 PdfPCell realvaliduntil= new PdfPCell();
	 
			 Phrase valid = null;
			 String validuntil="";
			 if(((Quotation) qp).getValidUntill()==null)
			 {
				 validuntil="";
				 valid=new Phrase("",font8bold);
			 }
			 else
			 {
				 valid=new Phrase("Valid Until",font8bold);;
				 validuntil=fmt.format(((Quotation) qp).getValidUntill());
			 }
			 Phrase rvalid = new Phrase(validuntil,font8);
			 titlevalid.addElement(valid);
			 realvaliduntil.addElement(rvalid);
			 billtable.addCell(titlevalid);
			 billtable.addCell(realvaliduntil);
			 billtable.addCell(blankcell);
			 billtable.addCell(blankcell);
			 
			 row =4;
		 }
		 int noCol=table.getNumberOfColumns();
//		 for(int i=0;i<2;i++)
//		 {
//			 PdfPRow temp=table.getRow(i);
//			PdfPCell[] cells= temp.getCells();
//			for(PdfPCell cell:cells)
//				cell.setBorder(Rectangle.NO_BORDER);
//		 }
//		 
		 for(int i=0;i<row;i++)
		 {
			 PdfPRow temp=billtable.getRow(i);
			PdfPCell[] cells= temp.getCells();
			for(PdfPCell cell:cells){
				cell.setBorder(Rectangle.NO_BORDER);
			}
		 }
		 
		PdfPTable parenttable= new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		parenttable.setWidths(new float[]{50,50});
		
		PdfPCell custinfocell = new PdfPCell();
		PdfPCell  billinfocell = new PdfPCell();
		
		custinfocell.addElement(table);
		billinfocell.addElement(billtable);
		parenttable.addCell(custinfocell);
		parenttable.addCell(billinfocell);
	
		document.add(parenttable);
	}
	
	//Since we create service date from scratch it will not reflect the saved date in service kind
	  public void createTableJobCard() throws DocumentException
	  {
	     addServiceDetail();
	    // Add new line
		int i=0;
		Contract con=(Contract) qp;
		
		//Retrive Services
		List<Service>serList=null;
		if(con.getCompanyId()!=null){
		    serList=ofy().load().type(Service.class).filter("contractCount",con.getCount()).filter("companyId",con.getCompanyId())
		    .list();
		}
		else{
			serList=ofy().load().type(Service.class).filter("contractCount",con.getCount()).
		    list();
		}
		
		PdfPTable table = new PdfPTable(6);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);
		table.setSpacingBefore(10);
		createServiceHeader(table);
		
	Comparator<Service> serviceDateComparator = new Comparator<Service>() {
	public int compare(Service s1, Service s2) {
	
	Date date1 = s1.getServiceDate();
	Date date2 = s2.getServiceDate();
	
	//ascending order
	return date1.compareTo(date2);
	}
	};
	Collections.sort(serList, serviceDateComparator);
		
		while(i<serList.size())
		{
			Service service=serList.get(i);
			String prodname=service.getProductName();

			
			Date servicedate=service.getServiceDate();
			String servDate=fmt.format(servicedate);
			
			
				String sno = i+1+"";
				Chunk serno= new Chunk(sno,font8);
				PdfPCell sernocell= new PdfPCell();
				sernocell.addElement(serno);
				
				Chunk chunk= new Chunk();	
				PdfPCell cellservicedate = new PdfPCell();
				chunk = new Chunk(servDate+"",font8);
				cellservicedate.addElement(chunk);
				
				chunk= new Chunk();	
				PdfPCell cellduedate = new PdfPCell();
				chunk = new Chunk(" ");
				cellduedate.addElement(chunk);
			
				
				PdfPCell cellproduct = new PdfPCell();
				chunk = new Chunk(prodname+"",font8);
				cellproduct.addElement(chunk);
				
				PdfPCell cellserviceEng = new PdfPCell();
				chunk = new Chunk("   ");
				cellserviceEng.addElement(chunk);
				
				PdfPCell cellCustomerName = new PdfPCell();
				chunk = new Chunk("      ");
				cellCustomerName.addElement(chunk);
					
				table.addCell(sernocell);
				table.addCell(cellservicedate);
				table.addCell(cellduedate);
				table.addCell(cellproduct);
				table.addCell(cellserviceEng);
				table.addCell(cellCustomerName);
				i++;
			}
	
			document.add(table);
		}
	
	private void createServiceHeader(PdfPTable table)
	{
		 Chunk seviceno = new Chunk("Service Number",this.font8bold);
	     PdfPCell servicenumbercell= new PdfPCell();
	     servicenumbercell.addElement(seviceno);
		 
	     Chunk chunk = new Chunk();
		 chunk = new Chunk("Service Carried On",this.font8bold);
		 PdfPCell duedatecell= new PdfPCell();
		 duedatecell.addElement(chunk);
		 
		 chunk = new Chunk("Service Date ",font8bold);
		 PdfPCell servicedatecell= new PdfPCell();
		 servicedatecell.addElement(chunk);
		 
		 chunk=new Chunk("Product",this.font8bold);
		 PdfPCell products = new PdfPCell();
		 products.addElement(chunk);
		
		chunk  = new Chunk("Attended By",this.font8bold);
		PdfPCell attendedby = new PdfPCell();
		attendedby.addElement(chunk);
		
		chunk = new Chunk("Customer Signature",this.font8bold);
		PdfPCell customersignature = new PdfPCell();
		customersignature.addElement(chunk);
		
		table.addCell(servicenumbercell);
		table.addCell(servicedatecell);
		table.addCell(duedatecell);
		table.addCell(products);
		table.addCell(attendedby);
		table.addCell(customersignature);
	}
	
	public double total(SalesLineItem superProduct)
	{
		double tax=0;
		
		/*if(superProduct.getLbtTax().isInclusive()==false)
			tax=tax+(superProduct.getLbtTax().getPercentage()*superProduct.getPrice()/100);*/
		if(superProduct.getServiceTax().isInclusive()==false)
			tax=tax+(superProduct.getServiceTax().getPercentage()*superProduct.getPrice()/100);
		if(superProduct.getVatTax().isInclusive()==false)
			tax=tax+(superProduct.getVatTax().getPercentage()*superProduct.getPrice()/100);
	   return ((superProduct.getPrice()+tax)*superProduct.getQty());
	}
	
	public double totalServices()
	{
		double sum =0;
		for(int i=0;i<products.size();i++){
			if(products.get(i).getNumberOfServices()!=0){
				sum=sum+products.get(i).getNumberOfServices();
			}
		}
		return sum;
	}
	
	
	public String largestduration()
	{
		int max=0,duration=0;
		
		for(SalesLineItem prod:products)
		{
			try{
			duration =prod.getDuration();
			if(duration >max)
			{
				max=duration;
			}
			}
			catch(Exception e)
			{
				
			}
		}
		return max+" days";
		
	}
	
	public void addtaxparagraph()
	{
		Paragraph taxpara= new Paragraph();
		Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
		Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
		Phrase titlelbttax=new Phrase("LBT Tax No :    ",font8bold);
		Phrase space = new Phrase("									");
		String serv = null,lbt,vat = null;
		if(comp.getServiceTaxNo()==null)
			serv="";
		else
			serv=comp.getServiceTaxNo();
		if(comp.getVatTaxNo()==null)
			vat="";
		else
			vat=comp.getVatTaxNo();
		
		if(serv.equals("")==false)
		{
			taxpara.add(titleservicetax);
			Phrase servp= new Phrase(serv,font8);
			taxpara.add(servp);
		}
		taxpara.add(space);
		
		if(vat.equals("")==false)
		{
			taxpara.add("                ");
			taxpara.add(titlevatatx);
			Phrase vatpp= new Phrase(vat,font8);
			taxpara.add(vatpp);
		}  	 
		
		try {
			taxpara.setSpacingAfter(10);
			document.add(taxpara);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	
	
	public void addServiceDetail()
	{
		Phrase header=null;
		header= new Phrase("Service Details",font12boldul);
		Paragraph para= new Paragraph(header);
		para.setAlignment(Element.ALIGN_LEFT);
		para.setSpacingBefore(20);
		para.setSpacingAfter(10);
		try {
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void addFooter()
	{
		 Phrase blankphrase = new Phrase(" ");
		
		 PdfPCell blankcell=new PdfPCell();
		 blankcell.addElement(blankphrase);
		 blankcell.setBorder(0);
		 
		 Phrase companyname= new Phrase("For "+comp.getBusinessUnitName(),font12bold);
		 PdfPCell companynamecell=new PdfPCell();
		 companynamecell.addElement(companyname);
		 companynamecell.setBorder(0);
		 Phrase authorizedsignatory= new Phrase("AUTHORISED SIGNATORY"+"",font12boldul);
		 PdfPCell authorisedcell=new PdfPCell();
		 authorisedcell.addElement(authorizedsignatory);
		 authorisedcell.setBorder(0);
		 
		 PdfPTable table = new PdfPTable(1);
		 table.setWidthPercentage(40);
		 table.addCell(blankcell);
		 table.addCell(blankcell);
		 table.addCell(companynamecell);
		 table.addCell(blankcell);
		 table.addCell(blankcell);
		 table.addCell(blankcell);
		 table.addCell(authorisedcell);
		 table.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void showLogo(Document doc,Company comp)
	{
		DocumentUpload document =comp.getLogo();
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scaleAbsolute(60f,60f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
	}

}
	
}
