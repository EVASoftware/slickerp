package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class SRFormatVersion1 {

	
	public static ArrayList<Service> servicearaylist;

	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 11);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
	Font font12Ul = new Font(Font.FontFamily.HELVETICA, 12, Font.UNDERLINE);


	public Document document;
	Quotation quotation;
	Company comp;
	Customer cust;
	Branch branchEntity;
	Employee employee;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat decimalformat = new DecimalFormat("0.00");
	
	PdfPCell cellBlank;
	
//	float[] column8SerProdCollonWidth = { 0.67f, 0.68f, 0.35f, 0.28f,
//			0.50f, 0.50f };
	
	float[] column8SerProdCollonWidth = { 0.72f, 0.73f, 0.35f, 0.38f,
			0.80f};
	boolean branchAsCompanyFlag = false;
	
	PdfUtility pdfUtility = new PdfUtility();
	
	Service service;
	Contract contract;
	
	Logger logger = Logger.getLogger("common service:");

	
	public void setService(Long count) {

		service=ofy().load().type(Service.class).id(count).now();
		
		if(service.getCompanyId()==null)
			cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).first().now();
		else
			cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).filter("companyId", service.getCompanyId()).first().now();

		
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
		
		if(service.getCompanyId()!= null){
			contract=ofy().load().type(Contract.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
		}
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		if (service.getCompanyId() != null){
//			serProject = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
//		}else{
//			serProject = ofy().load().type(ServiceProject.class).filter("serviceId", this.service.getCount()).filter("count", this.service.getProjectId()).first().now();
//		}
//		if(this.service.getProduct() != null && this.service.getProduct().getProductCode() != null){
//			if(Service.getFumigationProductCodes().contains(this.service.getProduct().getProductCode())){
//				fumigationFlag = true;
//			}
//			
//		}
	}

	public void createPdf(String preprintStatus) {

		createCustomerInfo();
		createfeedbackTable();
		createlastTable();
	}

	private void createlastTable() {

		float[] columnWidths = { 5f, 5f };
		
		PdfPTable ratingInfotable2 = new PdfPTable(2);
		ratingInfotable2.setWidthPercentage(100);
		try {
			ratingInfotable2.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		Phrase shareRating = new Phrase(" Kindly provide feedback between range of 5 to 1 \n"
				+ " (5 being Excellent and 1 being very poor) ", font10bold);
		PdfPCell cellshareRating = new PdfPCell(shareRating);
		cellshareRating.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellshareRating.setPaddingTop(10);
		cellshareRating.setPaddingBottom(10);
		cellshareRating.setBorderWidthRight(0);

		ratingInfotable2.addCell(cellshareRating);
		
		
		
		Image ratingImg=null;
		try {
			ratingImg = Image.getInstance("images/rating.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		ratingImg.scaleAbsolute(180, 40);
		
		Phrase ratingPhrase=new Phrase("",font10);
		Paragraph ratingPara=new Paragraph();
		ratingPara.setIndentationLeft(10f);
		ratingPara.add(new Chunk(ratingImg, 0, 0, true));				
//		emailPara.add(emailPhrase);
		ratingPara.setAlignment(Element.ALIGN_MIDDLE);
		
		PdfPCell ratingCell=new PdfPCell(ratingPara);
//		ratingCell.setBorder(0);
		ratingCell.setBorderWidthLeft(0);
		ratingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratingInfotable2.addCell(ratingCell);
//		Phrase shareRatingblank = new Phrase(" ", font10bold);a
//		PdfPCell cellshareRatingblank = new PdfPCell(shareRatingblank);
//		cellshareRatingblank.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cellshareRatingblank.setPaddingTop(10);
//		cellshareRatingblank.setPaddingBottom(10);
//
//		ratingInfotable2.addCell(cellshareRatingblank);
		
		Phrase remark = new Phrase(" Customer Remarks : ", font10bold);
		PdfPCell cellremark = new PdfPCell(remark);
		cellremark.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellremark.setPaddingBottom(40);
		cellremark.setColspan(2);
		ratingInfotable2.addCell(cellremark);
		
		
		try {
			document.add(ratingInfotable2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable lastparatable = new PdfPTable(1);
		lastparatable.setWidthPercentage(100);
		
		String para1= "For any service-related concern, call (tollfree No) - 1800 419 8181 OR WhatsApp 9800123456,"+
                          "may write to customercare@orionpest.com as well. Visit www.orionpest.com for more info.";
		
		Phrase feedbackpara = new Phrase(para1, font12bold);
		PdfPCell cellfeedbackpara = new PdfPCell(feedbackpara);
		cellfeedbackpara.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellfeedbackpara.setPaddingTop(5);
		cellfeedbackpara.setPaddingBottom(5);
		cellfeedbackpara.setBorder(0);

		lastparatable.addCell(cellfeedbackpara);
		
		String para2= "Our Branches";

		Phrase ourbranches = new Phrase(para2, font12bold);
		PdfPCell cellourbranches = new PdfPCell(ourbranches);
		cellourbranches.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellourbranches.setPaddingTop(5);
		cellourbranches.setPaddingBottom(5);
		cellourbranches.setBorder(0);
		
//		lastparatable.addCell(cellourbranches);
		
//		try {
//			document.add(lastparatable);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}

	private void createfeedbackTable() {
		// TODO Auto-generated method stub
		
		PdfPTable feddbacktable = new PdfPTable(1);
		feddbacktable.setWidthPercentage(100);
		feddbacktable.setSpacingBefore(5);
		feddbacktable.setSpacingAfter(5);
		
		String para1= "Kindly provide feedback between the range of 1 to 5 (5 being very satisfied and 1 being very poor)";
		
		Phrase feedbackpara = new Phrase(para1, font11bold);
		PdfPCell cellfeedbackpara = new PdfPCell(feedbackpara);
		cellfeedbackpara.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellfeedbackpara.setPaddingTop(5);
		cellfeedbackpara.setPaddingBottom(5);
		cellfeedbackpara.setBorder(0);

//		feddbacktable.addCell(cellfeedbackpara);
		
		try {
			document.add(feddbacktable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createCustomerInfo() {

		
		PdfPTable customerNametable = new PdfPTable(1);
		customerNametable.setWidthPercentage(100);
		
		
		String customerName = service.getPersonInfo().getFullName();
		
		Phrase clientName = new Phrase(" Client Name : "+customerName, font12bold);
		PdfPCell cellclientName = new PdfPCell(clientName);
		cellclientName.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellclientName.setPaddingTop(13);
		cellclientName.setPaddingBottom(13);

		customerNametable.addCell(cellclientName);
		try {
			document.add(customerNametable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		float[] columnWidths = { 6f, 4f };
		
		PdfPTable customerInfotable2 = new PdfPTable(2);
		customerInfotable2.setWidthPercentage(100);
		try {
			customerInfotable2.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		String serviceDate = fmt.format(service.getServiceDate());
		String servicelocation = "";
		if(service.getServiceBranch()!=null && !service.getServiceBranch().equalsIgnoreCase("Service Address")){
			servicelocation = service.getServiceBranch();
		}
		String remark = "";
		if(service.getProductFrequency()!=null){ //3-01-2024
			remark = service.getProductFrequency();
		}
		String technicianName = "";
		if(service.getEmployee()!=null){
			technicianName = service.getEmployee();
		}
		
		
		PdfPTable leftsidetable = new PdfPTable(3);
		float[] columnWidths2 = { 0.7f,0.1f,3f };

		try {
			leftsidetable.setWidths(columnWidths2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		leftsidetable.addCell(pdfUtility.getCell(" Address", font12bold, Element.ALIGN_LEFT, 0,0,0,0));
		leftsidetable.addCell(pdfUtility.getCell(":", font12bold, Element.ALIGN_CENTER, 0,0,0,0));
		leftsidetable.addCell(pdfUtility.getCell(service.getAddress().getCompleteAddress(), font12bold, Element.ALIGN_LEFT, 0,0,0,0));
		PdfPCell addresssCell = new PdfPCell(leftsidetable);
		
		Phrase Address = new Phrase(" Address : "+service.getAddress().getCompleteAddress(), font10bold);
		PdfPCell cellAddress = new PdfPCell(Address);
		cellAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellAddress.setPaddingTop(13);
		cellAddress.setPaddingBottom(10);
		addresssCell.setRowspan(2);
		customerInfotable2.addCell(addresssCell);
		
		Phrase serviceDate = new Phrase(" Service Date : ", font10bold);
		PdfPCell cellserviceDate = new PdfPCell(serviceDate);
		cellserviceDate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellserviceDate.setPaddingTop(10);
		cellserviceDate.setPaddingBottom(10);
		cellserviceDate.setBorderWidthBottom(0);
		customerInfotable2.addCell(cellserviceDate);
		
		Phrase serviceTime = new Phrase(" Service Time : ", font10bold);
		PdfPCell cellserviceTime = new PdfPCell(serviceTime);
		cellserviceTime.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellserviceTime.setPaddingTop(10);
		cellserviceTime.setPaddingBottom(10);
		cellserviceTime.setBorderWidthTop(0);
		customerInfotable2.addCell(cellserviceTime);
		

//		Phrase Location = new Phrase(" Location : "+service.getAddress().getLocality(), font10bold);
//		PdfPCell cellLocation = new PdfPCell(Location);
//		cellLocation.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cellLocation.setPaddingTop(10);
//		cellLocation.setPaddingBottom(10);
//
//		customerInfotable2.addCell(cellLocation);
		
		Phrase state = new Phrase(" State : "+service.getAddress().getState(), font10bold);
		PdfPCell cellstate = new PdfPCell(state);
		cellstate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellstate.setPaddingTop(10);
		cellstate.setPaddingBottom(10);

		customerInfotable2.addCell(cellstate);
		
		
		Phrase OPSCode = new Phrase(" OPS Code : "+servicelocation, font10bold);
		PdfPCell cellOPSCode = new PdfPCell(OPSCode);
		cellOPSCode.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellOPSCode.setPaddingTop(10);
		cellOPSCode.setPaddingBottom(10);
		
		customerInfotable2.addCell(cellOPSCode);
		

		Phrase serviceFrequency = new Phrase(" Service Frequency : "+remark, font10bold);
		PdfPCell cellserviceFrequency = new PdfPCell(serviceFrequency);
		cellserviceFrequency.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellserviceFrequency.setPaddingTop(10);
		cellserviceFrequency.setPaddingBottom(10);

		customerInfotable2.addCell(cellserviceFrequency);
		
		Phrase ServiceBranch = new Phrase(" Service Branch : "+service.getBranch(), font10bold);
		PdfPCell cellServiceBranch= new PdfPCell(ServiceBranch);
		cellServiceBranch.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellServiceBranch.setPaddingTop(10);
		cellServiceBranch.setPaddingBottom(10);
		
		customerInfotable2.addCell(cellServiceBranch);
		
		Phrase productName = new Phrase(" Service Product : "+service.getProductName(), font10bold);
		PdfPCell CellproductName = new PdfPCell(productName);
		CellproductName.setHorizontalAlignment(Element.ALIGN_LEFT);
		CellproductName.setPaddingTop(10);
		CellproductName.setPaddingBottom(10);
//		CellproductName.setColspan(2);

		customerInfotable2.addCell(CellproductName);
		
		String costCenterval="";
		if(service.getCostCenter()!=null)
			costCenterval=service.getCostCenter();
			
		Phrase costCenter = new Phrase(" Cost Center : "+costCenterval, font10bold);
		PdfPCell cellcostCenter= new PdfPCell(costCenter);
		cellcostCenter.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcostCenter.setPaddingTop(10);
		cellcostCenter.setPaddingBottom(10);
		
		customerInfotable2.addCell(cellcostCenter);
		
		String strserviceType ="";
		if(service.getServiceType()!=null){
			strserviceType = service.getServiceType();
		}
		Phrase servicetype = new Phrase(" Service Type : "+strserviceType, font10bold);
		PdfPCell Cellservicetype= new PdfPCell(servicetype);
		Cellservicetype.setHorizontalAlignment(Element.ALIGN_LEFT);
		Cellservicetype.setPaddingTop(10);
		Cellservicetype.setPaddingBottom(10);
		customerInfotable2.addCell(Cellservicetype);

		
		Phrase areaSQFT =null;
		if(service.getQuantity()>0)
			areaSQFT= new Phrase(" Area SQFT : "+service.getQuantity(), font10bold);
		else
			areaSQFT= new Phrase(" Area SQFT : ", font10bold);
		
		PdfPCell cellareaSQFT= new PdfPCell(areaSQFT);
		cellareaSQFT.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellareaSQFT.setPaddingTop(10);
		cellareaSQFT.setPaddingBottom(10);
		
		customerInfotable2.addCell(cellareaSQFT);
		
		Phrase nameofOperator = new Phrase(" Name of Operator : "+technicianName, font10bold);
		PdfPCell cellnameofOperator= new PdfPCell(nameofOperator);
		cellnameofOperator.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellnameofOperator.setPaddingTop(10);
		cellnameofOperator.setPaddingBottom(10);
		
		customerInfotable2.addCell(cellnameofOperator);
		
		String techcianCellNo = "";
		if(technicianName!=null && !technicianName.equals("")){
			Employee employeeEntity = ofy().load().type(Employee.class).filter("companyId", service.getCompanyId())
					.filter("employee", technicianName).first().now();
			if(employeeEntity!=null && employeeEntity.getCellNumber1()!=null && !employeeEntity.getCellNumber1().equals("0")){
				techcianCellNo = employeeEntity.getCellNumber1()+"";
			}
		}
		
		Phrase contractNumber = new Phrase(" Contact Number : "+techcianCellNo, font10bold);
		PdfPCell cellcontractNumber = new PdfPCell(contractNumber);
		cellcontractNumber.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcontractNumber.setPaddingTop(10);
		cellcontractNumber.setPaddingBottom(10);

		customerInfotable2.addCell(cellcontractNumber);
		
		
//		String contractCategory = "";
//		if(contract.getCategory()!=null){
//			contractCategory = contract.getCategory();
//		}
//		Phrase segment = new Phrase(" Segment : "+contractCategory, font10bold);
//		PdfPCell cellsegment = new PdfPCell(segment);
//		cellsegment.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cellsegment.setPaddingTop(10);
//		cellsegment.setPaddingBottom(10);
//
//		customerInfotable2.addCell(cellsegment);
		
		
		
		Phrase orionStampSeal = new Phrase(" Company Stamp & Seal : ", font10bold);
		PdfPCell cellorionStampSeal = new PdfPCell(orionStampSeal);
		cellorionStampSeal.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellorionStampSeal.setPaddingTop(13);
		cellorionStampSeal.setPaddingBottom(50);
		cellorionStampSeal.setColspan(1);

		customerInfotable2.addCell(cellorionStampSeal);
		
		Phrase orionRemark = new Phrase(" Company Remarks : ", font10bold);
		PdfPCell cellorionRemark = new PdfPCell(orionRemark);
		cellorionRemark.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellorionRemark.setPaddingTop(13);
		cellorionRemark.setPaddingBottom(50);
		cellorionRemark.setColspan(1);

		customerInfotable2.addCell(cellorionRemark);
		
		Phrase nameOfAuthPerson = new Phrase(" Authorized Person", font10bold);
		PdfPCell cellnameOfAuthPerson= new PdfPCell(nameOfAuthPerson);
		cellnameOfAuthPerson.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellnameOfAuthPerson.setPaddingTop(10);
		cellnameOfAuthPerson.setPaddingBottom(10);
		
		customerInfotable2.addCell(cellnameOfAuthPerson);
		
		Phrase employeeId = new Phrase(" Employee ID : ", font10bold);
		PdfPCell cellemployeeId = new PdfPCell(employeeId);
		cellemployeeId.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellemployeeId.setPaddingTop(10);
		cellemployeeId.setPaddingBottom(10);

		customerInfotable2.addCell(cellemployeeId);
		
		Phrase branchSealStamp = new Phrase(" Branch Seal & Stamp with Contact Number : ", font10bold);
		PdfPCell cellbranchSealStamp = new PdfPCell(branchSealStamp);
		cellbranchSealStamp.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellbranchSealStamp.setPaddingTop(10);
		cellbranchSealStamp.setPaddingBottom(30);
		cellbranchSealStamp.setColspan(2);

		customerInfotable2.addCell(cellbranchSealStamp);
		
		
		try {
			document.add(customerInfotable2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	 public void loadMultipleSRCopies(List<Service> serviceList, String preprintStatus){
			logger.log(Level.SEVERE,"Service list Size-->"+serviceList.size());
	    	loadBasicData(serviceList.get(0).getCompanyId(),serviceList.get(0).getPersonInfo().getCount(),serviceList.get(0).getBranch(),serviceList.get(0));
	    
	    	ArrayList<Integer> contractidlist = new ArrayList<Integer>();
	    	
	    	for(Service serviceEntity : serviceList){
	    		contractidlist.add(serviceEntity.getContractCount());
	    	}
	    	
	    	List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", serviceList.get(0).getCompanyId()).filter("count IN", contractidlist).list();
	    	int count=1;
	    	for(Service service:serviceList){
	    		this.service=service;
	    		this.contract = getContract(service.getContractCount(),contractlist);
				
				createPdf(preprintStatus);
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				if(count<serviceList.size()) {
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				}
				count++;
				
			}
	}

	private Contract getContract(Integer contractCount,	List<Contract> contractlist) {
		for(Contract contractEntity : contractlist){
			if(contractCount==contractEntity.getCount()){
				return contractEntity;
			}
		}
		return null;
	}

	private void loadBasicData(Long companyId, int count, String branch, Service service2) {

		this.service=service;
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
	}
	 
}
