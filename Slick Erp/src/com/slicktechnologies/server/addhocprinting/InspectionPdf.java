package com.slicktechnologies.server.addhocprinting;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.Inspection;
import com.slicktechnologies.shared.common.inventory.InspectionDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class InspectionPdf {
	
	Inspection insp;
	List<Inspection> purchaseorder;
	List<InspectionDetails> products;
	
	ArrayList<SuperProduct>stringlis= new ArrayList<SuperProduct>();
	
	List<VendorDetails> vendorlist;
	
	Vendor vendor;
	Company comp;
	public Document document;
	int vendorID;
	SuperProduct sup;
/****************************************************************************************/

	int flag=0;
	Phrase chunk;
	int firstBreakPoint = 30;
	int BreakPoint=5;
	float size;
	int count=0;
	int rohan=123;
	float blankLines;
	
	PdfPCell pdfsrno,pdfitemname,pdfgrnqty,pdfaccpqty,pdfrejqty,pdfreasonforrej;
	
	Logger logger=Logger.getLogger("Purchaseorder");
	float[] columnWidths3 = {1.0f, 0.1f, 1.0f, 1.0f, 0.1f, 1.0f,1.0f, 0.1f, 1.0f};
	float[] columnWidths = {0.8f, 2.5f, 1.2f, 1.2f,1.2f,4.0f};
	float[] columnWidths2={1.5f,0.2f,1.2f,1.6f,0.2f,1.4f};
	private Font font16boldul, font12bold, font8bold, font8,font9bold, font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	final static String disclaimerText = "I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";

	public InspectionPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	}

	public void setInspection(long count) {
		// Load Inspection
		insp = ofy().load().type(Inspection.class).id(count).now();
	
		// Load Company
		if (insp.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", insp.getCompanyId()).first().now();
	
		// Load vendor
		if (insp.getVendorInfo() != null) {
					vendorID = insp.getVendorInfo().getCount();
					System.out.println("vend id" + vendorID);
		}
		vendor = (Vendor) ofy().load().type(Vendor.class).filter("count", vendorID).first().now();
		
	   
		
		for(int i=0;i<insp.getItems().size();i++){
			if(insp.getCompanyId()!=null){
				sup=ofy().load().type(SuperProduct.class).filter("companyId",insp.getCompanyId()).
				filter("count", insp.getItems().get(i).getProductID()).first().now();
				
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
				
				stringlis.add(superprod);
			}
			else{
				sup=ofy().load().type(SuperProduct.class).filter("count", insp.getItems().get(i).getProductID()).first().now();
			}
		}
		products = insp.getItems();

	}
	public  void createPdfForEmail(Company comp,Vendor cust ,Inspection insp) {
		 
	    this.comp=comp;
	    this.insp=insp;
	    this.vendor=cust;
	    this.products=insp.getItems();
	    Createblank();
	    createLogo(document,comp);
	    createHeadingInfo();
	    CreateRefDetails();
		createProductInfo();
		footerInfo();
	      
	}	
	
	
	public void createPdf() {
		Createblank();
		createLogo(document,comp);
		createHeadingInfo();
		CreateRefDetails();
		createProductInfo();
		footerInfo();
	}
	
	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
		DocumentUpload document =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,745f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	

	
	private void createHeadingInfo() {

		Phrase companyName = new Phrase("                   "+comp.getBusinessUnitName().toUpperCase(),font12bold);
		//18 spaces added for setting logo 
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);
	
		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(p);
	
		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),font10);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font10);
		}else{
			adressline2 = new Phrase("",font10);
		}
		Phrase landmark = null;
		Phrase locality = null;
		Phrase city = null;
		if (comp.getAddress().getLandmark() != null) {
			 landmark =new Phrase(comp.getAddress().getLandmark(),font10);
		}else{
			landmark =new Phrase("",font10);
		}
		if(comp.getAddress().getLocality()!=null){
			locality = new Phrase(comp.getAddress().getLocality(),font10);
		}else{
			locality = new Phrase("",font10);
		}
		city = new Phrase( comp.getAddress().getCity() + " - "+ comp.getAddress().getPin(), font10);
	
		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);
			
		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);
	
		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);
		
		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		
		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);
	
		String contactinfo = "Cell: " + comp.getContact().get(0).getCellNo1();
		if (comp.getContact().get(0).getCellNo2() != 0)
			contactinfo = contactinfo + ","
					+ comp.getContact().get(0).getCellNo2();
		if (comp.getContact().get(0).getLandline() != 0) {
			contactinfo = contactinfo + "     " + " Tel: "
					+ comp.getContact().get(0).getLandline();
		}
	
		Phrase contactnos = new Phrase(contactinfo, font9);
		Phrase email = new Phrase("Email: "+ comp.getContact().get(0).getEmail(),font9);
	
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);
	
		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(addressline1cell);
		if (!comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if(!comp.getAddress().getLandmark().equals("")){
			companytable.addCell(landmarkcell);
		}
		if(!comp.getAddress().getLocality().equals("")){
			companytable.addCell(localitycell);
		}
	
		companytable.addCell(citycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);
		
		/**
		 * Vendor Info
		 */
	
		String tosir="To, M/S";
		
		String vendorN = vendor.getVendorName();
	
		Phrase vendorNa = new Phrase(vendorN, font14bold);
		Paragraph fulln = new Paragraph();
		fulln.add(Chunk.NEWLINE);
		fulln.setFont(font12bold);
		fulln.add(tosir+"  "+vendorN);
	
		
	
		
		
		
		Phrase vendoradress = new Phrase("                  "+vendor.getPrimaryAddress().getAddrLine1(), font10);
		Phrase vendoradress2 = null;
		if (vendor.getPrimaryAddress().getAddrLine2() != null) {
			vendoradress2 = new Phrase("                  "+vendor.getPrimaryAddress().getAddrLine2(), font10);
		} 
		
		
		Phrase vendorlocality = null;
		Phrase venderlandmek=null;
		if (vendor.getPrimaryAddress().getLandmark() != null) {
			String landmarks = vendor.getPrimaryAddress().getLandmark();
			venderlandmek = new Phrase("                  "+landmarks , font10);
		} else {
			venderlandmek = new Phrase("", font10);
		}
		
		if(vendor.getPrimaryAddress().getLocality()!=null){
			String locality1= vendor.getPrimaryAddress().getLocality();
			vendorlocality=new Phrase("                  "+locality1,font10);
		}else{
			vendorlocality=new Phrase("",font10);
		}
	
		Phrase cityphrase=new Phrase("                  "+vendor.getPrimaryAddress().getCity() + " - "
				+ vendor.getPrimaryAddress().getPin(), font10);
	
		PdfPCell locacell = new PdfPCell();
		locacell.addElement(vendorlocality);
		locacell.setBorder(0);
		
		PdfPCell city1cell = new PdfPCell();
		city1cell.addElement(cityphrase);
		city1cell.setBorder(0);
	//	citycell.addElement(Chunk.NEWLINE);
		
		PdfPCell venaddressline1cell = new PdfPCell();
		PdfPCell venaddressline2cell = new PdfPCell();
		
		venaddressline1cell.addElement(vendoradress);
		venaddressline1cell.setBorder(0);
	//	venaddressline1cell.addElement(Chunk.NEWLINE);
		
		if (vendoradress2 != null) {
			venaddressline2cell.addElement(vendoradress2);
			venaddressline2cell.setBorder(0);
	//		venaddressline2cell.addElement(Chunk.NEWLINE);
		}
		
		PdfPCell venlocalitycell = new PdfPCell();
		venlocalitycell.addElement(venderlandmek);
		venlocalitycell.setBorder(0);
	//	venlocalitycell.addElement(Chunk.NEWLINE);
		
		
		String contactinfon = "Cell: " + vendor.getCellNumber1();
		if (vendor.getCellNumber2() != 0)
			contactinfon = contactinfon + "," + vendor.getCellNumber2();
		if (vendor.getLandline() != 0) {
			contactinfon = contactinfon + "     " + " Tel: "
					+ vendor.getLandline();
		}
	
		Phrase vendorcontact = new Phrase(contactinfon, font9);
		Phrase vendoremail = new Phrase("Email: " + vendor.getEmail(), font9);
	
		PdfPCell vendorcontactcell = new PdfPCell();
		vendorcontactcell.addElement(vendorcontact);
		vendorcontactcell.setBorder(0);
	
		PdfPCell vendoremailcell = new PdfPCell();
		vendoremailcell.addElement(vendoremail);
		vendoremailcell.setBorder(0);
		
		
		PdfPCell vendorNcell = new PdfPCell();
		vendorNcell.addElement(fulln);
		vendorNcell.setBorder(0);
	
		String vendorFullName = "";
//		if (vendor.getMiddleName() != null) {
//			vendorFullName ="Attn: "+vendor.getFirstName().trim() + " "+ vendor.getMiddleName().trim() + " "
//					+ vendor.getLastName().trim();
//		} else {
//			vendorFullName ="Attn: "+ vendor.getFirstName().trim() + " "+ vendor.getLastName().trim();
//		}
	
		// new code added by vijay 
				if (vendor.getfullName()!=null) {
					vendorFullName ="Attn: "+ vendor.getfullName().trim();
				}
		
		Phrase vendorName = new Phrase(vendorFullName, font9);
		Paragraph fullname = new Paragraph();
		fullname.add(vendorName);
	
		PdfPCell vendornamecell = new PdfPCell();
		vendornamecell.addElement(fullname);
		vendornamecell.setBorder(0);
		
//		PdfPCell refcell=null;
//		PdfPCell refcell1=null;
//		PdfPCell refdatecell=null;
//		PdfPCell refdatecell1=null;
//		Phrase refphrase;
//		Phrase refphrase1;
//		
//		if(!insp.getRefOrderNO().equals("")){
//			refphrase=new Phrase("Reference No.",font9);
//			refphrase1=new Phrase(insp.getRefOrderNO(),font9);
//			
//			refcell = new PdfPCell();
//			refcell.addElement(refphrase);
//			refcell.setBorder(0);
//			refcell1 = new PdfPCell();
//			refcell1.addElement(refphrase1);
//			refcell1.setBorder(0);
//		}else{
//			refphrase=new Phrase("",font9);
//			refphrase1=new Phrase("",font9);
//			
//			
//			refcell = new PdfPCell();
//			refcell.addElement(refphrase);
//			refcell.setBorder(0);
//			refcell1 = new PdfPCell();
//			refcell1.addElement(refphrase1);
//			refcell1.setBorder(0);
//		}
//		
//		Phrase refdatephrase;
//		Phrase refdatephrase1;
//		
//		 if(insp.getReferenceDate()!=null){
//			refdatephrase=new Phrase("Reference Date",font9);
//			refdatephrase1=new Phrase(fmt.format(insp.getReferenceDate()),font9);
//			 
//			refdatecell = new PdfPCell();
//			refdatecell.addElement(refdatephrase);
//			refdatecell.setBorder(0);
//			refdatecell1 = new PdfPCell();
//			refdatecell1.addElement(refdatephrase1);
//			refdatecell1.setBorder(0);
//			refdatecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		 }else{
//			refdatephrase=new Phrase("",font9);
//			refdatephrase1=new Phrase("",font9);
//				 
//			refdatecell = new PdfPCell();
//			refdatecell.addElement(refdatephrase);
//			refdatecell.setBorder(0);
//			refdatecell1 = new PdfPCell();
//			refdatecell1.addElement(refdatephrase1);
//			refdatecell1.setBorder(0);
//			refdatecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		 }
//		
//		PdfPCell quocell = null;
//		PdfPCell quocell1 = null;
//		PdfPCell quodateCell=null;
//		PdfPCell quodateCell1=null;
//		
//		Phrase quophrase;
//		Phrase quophrase1;
//		if(insp.getRFQID()!=0){
//			quophrase=new Phrase("Quotation No.",font9);
//			quophrase1=new Phrase(insp.getRFQID()+"",font9);
//			
//			quocell = new PdfPCell();
//			quocell.addElement(quophrase);
//			quocell.setBorder(0);
//			quocell1 = new PdfPCell();
//			quocell1.addElement(quophrase1);
//			quocell1.setBorder(0);
//		}else{
//			
//			quophrase=new Phrase("",font9);
//			quophrase1=new Phrase("",font9);
//				
//			quocell = new PdfPCell();
//			quocell.addElement(quophrase);
//			quocell.setBorder(0);
//			quocell1 = new PdfPCell();
//			quocell1.addElement(quophrase1);
//			quocell1.setBorder(0);
//		}
//		
//		Phrase quodatephrase;
//		Phrase quodatephrase1;
//		if(insp.getRFQDate()!=null){
//			quodatephrase=new Phrase("Quotation Date",font9);
//			quodatephrase1=new Phrase(fmt.format(insp.getRFQDate()),font9);
//			
//			quodateCell = new PdfPCell();
//			quodateCell.addElement(quodatephrase);
//			quodateCell.setBorder(0);
//			quodateCell1 = new PdfPCell();
//			quodateCell1.addElement(quodatephrase1);
//			quodateCell1.setBorder(0);
//			quodateCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			
//			
//		}else{
//			
//			quodatephrase=new Phrase("",font9);
//			quodatephrase1=new Phrase("",font9);
//			
//			quodateCell = new PdfPCell();
//			quodateCell.addElement(quodatephrase);
//			quodateCell.setBorder(0);
//			quodateCell1 = new PdfPCell();
//			quodateCell1.addElement(quodatephrase1);
//			quodateCell1.setBorder(0);
//			quodateCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			
//		}
		
		
		
		 Phrase seperator=new Phrase(":",font9);
		 PdfPCell seperatorcell=new PdfPCell();
		 seperatorcell.addElement(seperator);
		 seperatorcell.setBorder(0);
		 seperatorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable infotable=new PdfPTable(6);
		 infotable.setHorizontalAlignment(100);
		 try {
			infotable.setWidths(columnWidths2);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
	
//		 if(!insp.getRefOrderNO().equals("")){
//		 infotable.addCell(refcell);
//		 infotable.addCell(seperatorcell);
//		 infotable.addCell(refcell1);
//		 }
//		 if(insp.getReferenceDate()!=null){
//		 infotable.addCell(refdatecell);
//		 infotable.addCell(seperatorcell);
//		 infotable.addCell(refdatecell1);
//		 
//		 
//		 }
//		 if(insp.getRFQID()!=0){
//			 infotable.addCell(quocell);
//			 infotable.addCell(seperatorcell);
//			 infotable.addCell(quocell1);
//			 
//		 }
//		 
//		 if(insp.getRFQDate()!=null){
//			 
//			 infotable.addCell(quodateCell);
//			 infotable.addCell(seperatorcell);
//			 infotable.addCell(quodateCell1);
//		 }
//		
//		 PdfPCell  infotablecell1=new PdfPCell(infotable);
//			 infotablecell1.setBorder(0);
		
		/*
		 * Vendor Table
		 */
	
		PdfPTable vendortable = new PdfPTable(1);
		vendortable.setWidthPercentage(100);
		vendortable.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendortable.addCell(vendorNcell);
		vendortable.addCell(venaddressline1cell);
		if (!vendor.getPrimaryAddress().getAddrLine2().equals("")) {
			vendortable.addCell(venaddressline2cell);
		}
		if(!vendor.getPrimaryAddress().getLandmark().equals("")){
			vendortable.addCell(venlocalitycell);
		}
		if(!vendor.getPrimaryAddress().getLocality().equals("")){
			vendortable.addCell(locacell);
		}
		
		vendortable.addCell(city1cell);
		
		vendortable.addCell(vendorcontactcell);
		vendortable.addCell(vendoremailcell);
		vendortable.addCell(vendornamecell);
//		
//		if(insp.getRefOrderNO()!=null||insp.getRFQID()!=0){
//			
//			vendortable.addCell(infotablecell1);
//		}
		
		
		
		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	
		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell vendorinfocell = new PdfPCell();
	
		companyinfocell.addElement(companytable);
		vendorinfocell.addElement(vendortable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(vendorinfocell);
	
		String title = "";
		title = "Inspection/Rejection Note";
	
		String countinfo = "";
		Inspection purentity = (Inspection) insp;
		countinfo = "ID : "+purentity.getCount() + "";
	
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String creationdateinfo = "";
		Inspection purchentity = (Inspection) insp;
		creationdateinfo = "Date: " + fmt.format(purchentity.getCreationDate());
	
		Phrase titlephrase = new Phrase(title, font12bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);
	
		Phrase idphrase = new Phrase(countinfo, font12);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);
	
		Phrase dateofpdf = new Phrase(creationdateinfo, font12);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);
	
		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);
	
		try {
			document.add(headparenttable);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void CreateRefDetails() {
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		Phrase semicolon =new Phrase(":",font1);
		PdfPCell semicolonCell=new PdfPCell(semicolon);
//		semicolonCell.addElement(semicolon);
		semicolonCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		semicolonCell.setBorder(0);
		
		PdfPTable table = new PdfPTable(9);
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase refno = new Phrase("REF. NO.", font1);
		Phrase refdate = new Phrase("REF. DATE", font1);
		Phrase ordrid = new Phrase("ORDER ID",font1);
		Phrase grnid = new Phrase("GRN ID", font1);
		Phrase grndate = new Phrase("GRN DATE", font1);
		Phrase inspectedby = new Phrase("INSPECTED BY", font1);
		
		
	        
	      
	        	 
		PdfPCell cellrefno = new PdfPCell(refno);
		cellrefno.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefno.setBorder(0);
		
		PdfPCell cellrefdate = new PdfPCell(refdate);
		cellrefdate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefdate.setBorder(0);
		
		PdfPCell cellordrid = new PdfPCell(ordrid);
		cellordrid.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellordrid.setBorder(0);
		
		PdfPCell cellgrnid = new PdfPCell(grnid);
		cellgrnid.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrnid.setBorder(0);
		
		PdfPCell cellgrndate = new PdfPCell(grndate);
		cellgrndate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrndate.setBorder(0);
		
		PdfPCell cellinspectedby = new PdfPCell(inspectedby);
		cellinspectedby.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinspectedby.setBorder(0);
		
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(insp.getInspectionRefNo()!=null){
			chunk = new Phrase(insp.getInspectionRefNo(), font8);
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellrefnoval = new PdfPCell(chunk);
		cellrefnoval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefnoval.setBorder(0);
		
		if(insp.getInspectionRefDate()!=null){
			chunk = new Phrase(fmt.format(insp.getInspectionRefDate()) + "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellrefdateval = new PdfPCell(chunk);
		cellrefdateval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefdateval.setBorder(0);
		
		if(insp.getInspectionPOid()!=0){
			chunk = new Phrase(insp.getInspectionPOid()+"", font8);
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellorderidval = new PdfPCell(chunk);
		cellorderidval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellorderidval.setBorder(0);
		
		if(insp.getInspectionGrnId()!=0){
			chunk = new Phrase(insp.getInspectionGrnId()+"", font8);
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellgrnidval = new PdfPCell(chunk);
		cellgrnidval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrnidval.setBorder(0);
		
		if(insp.getInspectionGrnDate()!=null){
			chunk = new Phrase(fmt.format(insp.getInspectionGrnDate()) + "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellgrndateval = new PdfPCell(chunk);
		cellgrndateval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrndateval.setBorder(0);
		
		if(insp.getApproverName()!=null){
			chunk = new Phrase(insp.getApproverName()+ "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellinspectedbyval = new PdfPCell(chunk);
		cellinspectedbyval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinspectedbyval.setBorder(0);
		
		
		
		
		
		
		table.addCell(cellrefno);
		table.addCell(semicolonCell);
		table.addCell(cellrefnoval);
		
		table.addCell(cellrefdate);
		table.addCell(semicolonCell);
		table.addCell(cellrefdateval);
		
		table.addCell(cellordrid);
		table.addCell(semicolonCell);
		table.addCell(cellorderidval);
		
		table.addCell(cellgrnid);
		table.addCell(semicolonCell);
		table.addCell(cellgrnidval);
		
		table.addCell(cellgrndate);
		table.addCell(semicolonCell);
		table.addCell(cellgrndateval);
		
		table.addCell(cellinspectedby);
		table.addCell(semicolonCell);
		table.addCell(cellinspectedbyval);
		
		PdfPCell tablecell =new PdfPCell();
		tablecell.addElement(table);
		
		
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(tablecell);
		
		
		
		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
	}


	public void createProductInfo() {
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
	
		PdfPTable table = new PdfPTable(6);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase srno = new Phrase("SR.NO.", font1);
		Phrase refno = new Phrase("ITEM DETAILS", font1);
		Phrase grnqty = new Phrase("GRN QUANTITY",font1);
		Phrase accqty = new Phrase("ACCEPTED QUANTITY", font1);
		Phrase rejqty = new Phrase("REJECTED QUANTITY", font1);
		Phrase remark = new Phrase("REMARK/REASON FOR REJECT", font1);
		
		
	        
	      
	        	 
		PdfPCell cellsrno = new PdfPCell(srno);
		cellsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcategory = new PdfPCell(refno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(grnqty);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(accqty);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rejqty);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(remark);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	
		table.addCell(cellsrno);
		table.addCell(cellcategory);
		table.addCell(cellrate);
		table.addCell(cellperdisc);
		table.addCell(cellservicetax);
		table.addCell(celltotal);
		
		
		//************************here i have made changes
		//*************calculating spaces here*************
		if( this.products.size()<= firstBreakPoint){
			int size =  firstBreakPoint - this.products.size();
			blankLines = size*(160/15);
		}
		else{
			blankLines = 10f;
		}
		
		table.setSpacingAfter(blankLines);	
	
		Phrase rephrse=null;
		
		for (int i = 0; i < this.products.size(); i++) {
			
			chunk = new Phrase((i+1)+"",font8);
			pdfsrno = new PdfPCell(chunk);
			pdfsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			if(products.get(i).getProductName()!=null){
				chunk = new Phrase(products.get(i).getProductName(), font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfitemname = new PdfPCell(chunk);
			
			if(products.get(i).getProductQuantity()!=0){
				chunk = new Phrase(products.get(i).getProductQuantity() + "", font8);
			}else{
				chunk = new Phrase("");
			}
			
			pdfgrnqty = new PdfPCell(chunk);
			pdfgrnqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 
			if(products.get(i).getAcceptedQty()!=-1){
				 chunk =new Phrase (products.get(i).getAcceptedQty()+"",font8);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfaccpqty = new PdfPCell(chunk);
			pdfaccpqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(products.get(i).getRejectedQty()!=0){
				 chunk =new Phrase (products.get(i).getRejectedQty()+"",font8);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfrejqty = new PdfPCell(chunk);
			pdfrejqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
				
			if(products.get(i).getReasonForReject()!=null){
				chunk = new Phrase(products.get(i).getReasonForReject(), font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfreasonforrej = new PdfPCell(chunk);
			
			table.addCell(pdfsrno);
			table.addCell(pdfitemname);
			table.addCell(pdfgrnqty);
			table.addCell(pdfaccpqty);
			table.addCell(pdfrejqty);
			table.addCell(pdfreasonforrej);
			
			
			
			if(count==this.products.size()|| count==firstBreakPoint){
	    		rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
				flag=firstBreakPoint;
				break;
			}
			System.out.println("flag value" + flag);
			if(firstBreakPoint==flag){
				footerInfo();
			}
			
		}
		
			PdfPCell prodtablecell=new PdfPCell();
			prodtablecell.addElement(table);
			prodtablecell.addElement(rephrse);	
			PdfPTable parentTableProd=new PdfPTable(1);
		    parentTableProd.setWidthPercentage(100);
		    parentTableProd.addCell(prodtablecell);
		       
				
			try {
				document.add(parentTableProd);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
	}
	
	public void footerInfo()
	{
		
		 PdfPTable desctable = new PdfPTable(1);
		 Paragraph descpara=new Paragraph();
		 String description=null;
		 
		 
		 Phrase descheading=null;
		 
		 
		 Phrase paraphr=null;
		 if(!insp.getInspectionDescription().equals("")){
			 description=insp.getInspectionDescription();
			 descheading=new Phrase("DESCRIPTION:",font10boldul);
			 paraphr=new Phrase(description,font8);
		 }
		 descpara.add(descheading);
		 descpara.add(Chunk.NEWLINE);
		 descpara.add(paraphr);
		 descpara.setAlignment(Element.ALIGN_LEFT);
		
		 PdfPCell desccell=new PdfPCell();
		 desccell.addElement(descpara);
		 desccell.setBorder(0);
		 
		 desctable.addCell(desccell);
		 desctable.setWidthPercentage(100);
		 
		 
		
		 String companyname=comp.getBusinessUnitName().trim().toUpperCase();
		 Paragraph companynamepara=new Paragraph();
		 companynamepara.add("FOR "+companyname);
		 companynamepara.setFont(font9bold);
		 companynamepara.setAlignment(Element.ALIGN_CENTER);
		 
	
		 //******************end of document***********************
		 
		 String endDucument = "*************************X--X--X*************************";
		 Paragraph endpara=new Paragraph();
		 endpara.add(endDucument);
		 endpara.setAlignment(Element.ALIGN_CENTER);
		 
		 //***************************************************
		 
		 
		 String authsign="AUTHORISED SIGNATORY";
	     Paragraph authpara=new Paragraph();
	     authpara.add(authsign);
	     authpara.setFont(font9bold);
	     authpara.setAlignment(Element.ALIGN_CENTER);
	     
	     PdfPCell companynamecell=new PdfPCell();
	     companynamecell.addElement(companynamepara);
	     companynamecell.setBorder(0);
	     
	     PdfPCell authsigncell=new PdfPCell();
	     authsigncell.addElement(authpara);
	     authsigncell.setBorder(0);
	
	     Phrase ppp=new Phrase(" ");
	     PdfPCell blacell=new PdfPCell();
	     blacell.addElement(ppp);
	     blacell.setBorder(0);
	     PdfPTable table = new PdfPTable(1);
		 table.addCell(companynamecell);
		 table.addCell(blacell);
		 table.addCell(blacell);
		 table.addCell(blacell);
		 table.addCell(authsigncell);
		 
		 table.setWidthPercentage(100);
		 
		 
		  PdfPTable parentbanktable=new PdfPTable(2);
		  parentbanktable.setSpacingAfter(40f);
		  parentbanktable.setWidthPercentage(100);
	      try {
	    	  parentbanktable.setWidths(new float[]{70,30});
		  } 
	      catch (DocumentException e1) {
			e1.printStackTrace();
		}
	      
	      
	      PdfPCell bankdatacell1 = new PdfPCell();
		  PdfPCell authorisedcell = new PdfPCell();
		  
		  bankdatacell1.addElement(desctable);
		  authorisedcell.addElement(table);
		  
		  parentbanktable.addCell(bankdatacell1);
		  parentbanktable.addCell(authorisedcell);
		  
		  
		  
		  Phrase refphrase=new Phrase("Annexure 1 ",font10bold);
		  Paragraph repara=new Paragraph(refphrase);
		  
		  
		  try {
				document.add(parentbanktable);
				
				if(this.products.size() > 30){
				document.newPage();
				document.add(repara);
				settingRemainingRowsToPDF(31);
				}if(this.products.size() > 90){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(91);
				}if(this.products.size() > 150){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(151);
				}if(this.products.size() > 210){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(211);
				}
				if(this.products.size()>30){
					document.add(endpara);
				}
			} 
			catch (DocumentException e) {
				e.printStackTrace();
			}
		  
		  
		  			 
	}

	private void settingRemainingRowsToPDF(int startindex) {
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		PdfPTable table = new PdfPTable(6);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase srno = new Phrase("SR.NO.", font1);
		Phrase refno = new Phrase("ITEM DETAILS", font1);
		Phrase grnqty = new Phrase("GRN QUANTITY",font1);
		Phrase accqty = new Phrase("ACCEPTED QUANTITY", font1);
		Phrase rejqty = new Phrase("REJECTED QUANTITY", font1);
		Phrase remark = new Phrase("REMARK/REASON FOR REJECT", font1);
		
		
	        
	      
	        	 
		PdfPCell cellsrno = new PdfPCell(srno);
		cellsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcategory = new PdfPCell(refno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(grnqty);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(accqty);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rejqty);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(remark);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	
		table.addCell(cellsrno);
		table.addCell(cellcategory);
		table.addCell(cellrate);
		table.addCell(cellperdisc);
		table.addCell(cellservicetax);
		table.addCell(celltotal);
		
		
		//************************here i have made changes
		//*************calculating spaces here*************
		if( this.products.size()<= firstBreakPoint){
			int size =  firstBreakPoint - this.products.size();
			blankLines = size*(160/15);
			System.out.println("blankLines size ="+blankLines);
			System.out.println("blankLines size ="+blankLines);
		}
		else{
			blankLines = 10f;
		}
		
		table.setSpacingAfter(blankLines);	
	
		Phrase rephrse=null;
		
		for (int i = startindex; i < this.products.size(); i++) {
			
			chunk = new Phrase((i+1)+"",font8);
			pdfsrno = new PdfPCell(chunk);
			pdfsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			if(products.get(i).getProductName()!=null){
				chunk = new Phrase(products.get(i).getProductName(), font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfitemname = new PdfPCell(chunk);
			
			if(products.get(i).getProductQuantity()!=0){
				chunk = new Phrase(products.get(i).getProductQuantity() + "", font8);
			}else{
				chunk = new Phrase("");
			}
			
			pdfgrnqty = new PdfPCell(chunk);
			pdfgrnqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 
			if(products.get(i).getAcceptedQty()!=0){
				 chunk =new Phrase (products.get(i).getAcceptedQty()+"",font8);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfaccpqty = new PdfPCell(chunk);
			pdfaccpqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(products.get(i).getRejectedQty()!=0){
				 chunk =new Phrase (products.get(i).getRejectedQty()+"",font8);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfrejqty = new PdfPCell(chunk);
			pdfrejqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
				
			if(products.get(i).getReasonForReject()!=null){
				chunk = new Phrase(products.get(i).getReasonForReject(), font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfreasonforrej = new PdfPCell(chunk);
			
			table.addCell(pdfsrno);
			table.addCell(pdfitemname);
			table.addCell(pdfgrnqty);
			table.addCell(pdfaccpqty);
			table.addCell(pdfrejqty);
			table.addCell(pdfreasonforrej);
			
			startindex=startindex+1;
			if(startindex==91||startindex==151||startindex==211){
			break;
			}
		}
		
			PdfPCell prodtablecell=new PdfPCell();
			prodtablecell.addElement(table);
			prodtablecell.addElement(rephrse);	
			PdfPTable parentTableProd=new PdfPTable(1);
		    parentTableProd.setWidthPercentage(100);
		    parentTableProd.addCell(prodtablecell);
		       
				
			try {
				document.add(parentTableProd);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}

}
