package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class HygeiaContractPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6156290303548455918L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("Vaishnavi +++++++++++++++" + stringid);

			HygeiaContractPdf conpdf = new HygeiaContractPdf();

			conpdf.document = new Document();
			Document document = conpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream()); // write

			document.open();

			conpdf.setpdfcon(count);
			conpdf.createPdf();

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
