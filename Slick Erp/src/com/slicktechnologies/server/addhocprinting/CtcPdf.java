package com.slicktechnologies.server.addhocprinting;



import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
public class CtcPdf {
	
	
	Company comp;
	Employee emp;
	EmployeeInfo info;
	CTC ctc;
	
//	List<CtcComponent>deductionList;
//	List<CtcComponent>deductionCompContList;
	
	
	public Document document;
	
	private Font font16boldul, font12bold, font8bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold;
	
	float[] columnWidths2={2.5f,0.5f,4.5f,2.5f,0.5f,2.5f};
	float[] columnWidths = {1.0f, 3.5f, 2.0f, 2.0f};
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	int firstBreakPoint=20;
	float blankLines=0;
	Logger logger=Logger.getLogger("CTC Pdf Logger");
	
	double pfMaxFixedValue=0;
	String deductionTypeName;
	
	public CtcPdf() {
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	}
	
	public void setCtc(long count){
		// Load CTC
		ctc = ofy().load().type(CTC.class).id(count).now();
			
		// Load Company
		if (ctc.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", ctc.getCompanyId()).first().now();
		
		
		if (ctc.getCompanyId() == null){
			emp = ofy().load().type(Employee.class).filter("count", ctc.getEmpid()).first().now();
		}
		else{
			emp = ofy().load().type(Employee.class).filter("companyId", ctc.getCompanyId()).filter("count", ctc.getEmpid()).first().now();
		}
		
//		deductionList= ofy().load().type(CtcComponent.class).filter("isDeduction",true).filter("companyId",ctc.getCompanyId()).list();
//		
//		deductionCompContList= ofy().load().type(CtcComponent.class).filter("isDeduction",true).filter("isRecord",true).filter("isNonMonthly",true).filter("companyId",ctc.getCompanyId()).list();
		ArrayList<String> processNameList=new ArrayList<String>();
		processNameList.add("PfMaxFixedValue");
		processNameList.add("EsicForDirect");
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", ctc.getCompanyId()).filter("processName IN", processNameList).filter("configStatus", true).list();
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PfMaxFixedValue")&&ptDetails.isStatus()==true){
						pfMaxFixedValue=Double.parseDouble(ptDetails.getProcessType().trim());
					}
					
					if(processConf.getProcessName().equalsIgnoreCase("EsicForDirect")&&ptDetails.isStatus()==true){
						deductionTypeName=ptDetails.getProcessType().trim();
					}
				}
			}
		}
		
	}

	public void createPdf() {
		try{
			Createblank();
	//		createLogo(document,comp);
			createCompanyAddress();
			createEmployeeDetails();
			creatCtcStructure();
		}catch(Exception e){
			
		}
	}
	
	
/******************************** Create blank Line ********************************************/
	
	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/******************************** Create company Logo ********************************************/
	
	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	/******************************** print company address ********************************************/
	public  PdfPTable createCompanyAddress()
	{
		/**
		 * Date : 20-10-2018 BY ANIL
		 */
		DocumentUpload docu =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+docu.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		
		
		/**
		 * End
		 */
		
		
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
//		String salaryPeriod=payslip.getSalaryPeriod();
//		String[] res=salaryPeriod.split("-",0);
//		String month =res[1].trim();
//		String year =res[0].trim();
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{15,75});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		if (imageSignCell != null) {
			logoTable.addCell(imageSignCell);
			PdfPCell compCell=new PdfPCell();
			compCell.addElement(companyDetails);
			compCell.setBorder(0);
			logoTable.addCell(compCell);
		} else {
			logoTable.addCell(blankCell);
			PdfPCell compCell=new PdfPCell();
			compCell.addElement(companyDetails);
			compCell.setBorder(0);
			logoTable.addCell(compCell);
		}
			
		String title1 = "";
		title1 = "CTC";
	
	
		Phrase titlephrase = new Phrase(title1, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);
	
		PdfPTable titlepdftable = new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(titlepdfcell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(logoTable);
//		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			if(document!=null)
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		return parent;
	}
	
	public PdfPTable createEmployeeDetails(){
		
		/****************************** Employee information *******************************************/

		// Employee Information

		Phrase employeeidtitle = new Phrase("Employee ID", font8bold);
		Phrase employeenametitle = new Phrase("Employee Name", font8bold);
		Phrase empdesignationtitle = new Phrase("Designation", font8bold);
		Phrase empdepartmenttitle = new Phrase("Department", font8bold);
		Phrase employeedojtitle=new Phrase("Date Of Joining",font8bold);
		Phrase employeepannotitle = new Phrase("Pan No", font8bold);
		Phrase employeepfnotitle = new Phrase("PF A/C No.", font8bold);
		Phrase employeeesictitle = new Phrase("ESIC No.", font8bold);

		PdfPCell titleempid = new PdfPCell();
		titleempid.setBorder(0);
		titleempid.addElement(employeeidtitle);
		
		PdfPCell titleempname = new PdfPCell();
		titleempname.setBorder(0);
		titleempname.addElement(employeenametitle);
		
		PdfPCell titleempdesignation = new PdfPCell();
		titleempdesignation.setBorder(0);
		titleempdesignation.addElement(empdesignationtitle);
		
		PdfPCell titleempdepartment = new PdfPCell();
		titleempdepartment.setBorder(0);
		titleempdepartment.addElement(empdepartmenttitle);
		
		PdfPCell titleempdob = new PdfPCell();
		titleempdob.setBorder(0);
		 titleempdob.addElement(employeedojtitle);
		 
		PdfPCell titleemppanno = new PdfPCell();
		titleemppanno.setBorder(0);
		titleemppanno.addElement(employeepannotitle);
		
		PdfPCell titleemppfno = new PdfPCell();
		titleemppfno.setBorder(0);
		titleemppfno.addElement(employeepfnotitle);
		
		PdfPCell titleempesic = new PdfPCell();
		titleempesic.setBorder(0);
		titleempesic.addElement(employeeesictitle);

		Phrase retempid = new Phrase(ctc.getEmpid() + "", font8);
		Phrase retempname = new Phrase(ctc.getEmployeeName(), font8);
		Phrase retempdesignation = new Phrase(ctc.getEmployeedDesignation(), font8);
		Phrase retempdepartment = new Phrase(ctc.getDepartment(), font8);
		Phrase retempdoj=null;
		if(emp.getJoinedAt()!=null){
			retempdoj=new Phrase(fmt.format(emp.getJoinedAt()),font8);
		}else{
			retempdoj=new Phrase(" ",font8);
		}
		Phrase retemppanno = new Phrase(emp.getEmployeePanNo(), font8);
		Phrase retemppfno = new Phrase(emp.getPPFNaumber(), font8);
		Phrase retempesicno = new Phrase(emp.getEmployeeESICcode(), font8);

		PdfPCell employeeidcell = new PdfPCell();
		employeeidcell.setBorder(0);
		employeeidcell.addElement(retempid);
		
		PdfPCell employeenamecell = new PdfPCell();
		employeenamecell.setBorder(0);
		employeenamecell.addElement(retempname);
		
		PdfPCell empdesignationcell = new PdfPCell();
		empdesignationcell.setBorder(0);
		empdesignationcell.addElement(retempdesignation);
		
		PdfPCell empdepartmentcell = new PdfPCell();
		empdepartmentcell.setBorder(0);
		empdepartmentcell.addElement(retempdepartment);
		
		PdfPCell empdojcell = new PdfPCell();
		empdojcell.setBorder(0);
		empdojcell.addElement(retempdoj);
		
		PdfPCell emppannocell = new PdfPCell();
		emppannocell.setBorder(0);
		emppannocell.addElement(retemppanno);
		
		PdfPCell emppfnocell = new PdfPCell();
		emppfnocell.setBorder(0);
		emppfnocell.addElement(retemppfno);
		
		PdfPCell empesicnocell = new PdfPCell();
		empesicnocell.setBorder(0);
		empesicnocell.addElement(retempesicno);

		
		Phrase semicolon =new Phrase(":",font8);
		PdfPCell semicoloncell = new PdfPCell();
		semicoloncell.setBorder(0);
		semicoloncell.addElement(semicolon);
		
		PdfPTable employeedetailstable = new PdfPTable(6);
		employeedetailstable.setWidthPercentage(100);
		try {
			employeedetailstable.setWidths(columnWidths2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		employeedetailstable.addCell(titleempid);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(employeeidcell);
		
		
		employeedetailstable.addCell(titleemppfno);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(emppfnocell);
		
		employeedetailstable.addCell(titleempname);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(employeenamecell);
		
		employeedetailstable.addCell(titleemppanno);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(emppannocell);
		
		employeedetailstable.addCell(titleempdepartment);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empdepartmentcell);
		
		employeedetailstable.addCell(titleempesic);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empesicnocell);
		
		employeedetailstable.addCell(titleempdesignation);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empdesignationcell);
		
		employeedetailstable.addCell(titleempdob);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empdojcell);
		
		
		
		
		
		
		
		
		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell parentcell = new PdfPCell();
		parentcell.addElement(employeedetailstable);
		parent.addCell(parentcell);
		
		try {
			if(document!=null)
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		return parent;
	}
	
	
	
	public PdfPTable creatCtcStructure(){
		Font font1 = new Font(Font.FontFamily.HELVETICA, 10 ,Font.BOLD);
			
		PdfPTable table = new PdfPTable(4);
			
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
			
		//First Row
		
		Phrase srnohead = new Phrase("SR.NO.", font1);
		Phrase particulathead = new Phrase("PARTICULARS", font1);
		Phrase monthlyhead = new Phrase("MONTHLY",font1);
		Phrase yearlyhead = new Phrase("YEARLY", font1);
			
		PdfPCell cellsrno = new PdfPCell(srnohead);
		cellsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellparticular = new PdfPCell(particulathead);
		cellparticular.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellmonthly = new PdfPCell(monthlyhead);
		cellmonthly.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellyearly = new PdfPCell(yearlyhead);
		cellyearly.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellsrno);
		table.addCell(cellparticular);
		table.addCell(cellmonthly);
		table.addCell(cellyearly);
		
		
		
		
		
		
		Phrase srno1 = new Phrase("A)", font1);
		Phrase monthlyPayable = new Phrase("Monthly Payable", font1);
		Phrase blank = new Phrase(" ",font1);
			
		PdfPCell cellsrno1 = new PdfPCell(srno1);
		cellsrno1.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellparticular1 = new PdfPCell(monthlyPayable);
		cellparticular1.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellmonthly1 = new PdfPCell(blank);
		cellmonthly1.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellyearly1 = new PdfPCell(blank);
		cellyearly1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellsrno1);
		table.addCell(cellparticular1);
		table.addCell(cellmonthly1);
		table.addCell(cellyearly1);
		
		//Blank Row
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		List<CtcComponent> ctcList=new ArrayList<CtcComponent>();
		
		for (CtcComponent earnings : ctc.getEarning()) {
			if(earnings.getAmount()!=0){
				ctcList.add(earnings);
			}
		}
		
		double sum = 0;
		double actualSum=0;
		for (CtcComponent earnings : ctcList) {
			
			table.addCell(cellmonthly1);
			
			Phrase earnCompNm = new Phrase(earnings.getName(), font8);
			PdfPCell earnCompNmcell = new PdfPCell();
			earnCompNmcell.addElement(earnCompNm);
			table.addCell(earnCompNmcell);
			
			double earning = earnings.getAmount() / 12;
			sum = sum + earning;
			
			double earning1 = earnings.getAmount();
			actualSum = actualSum + earning1;
			
			
			Phrase earnCompAmt = new Phrase(df.format(earning) + "", font8);
			PdfPCell earnCompAmtcell = new PdfPCell(earnCompAmt);
//			earnCompAmtcell.addElement(earnCompAmt);
			earnCompAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(earnCompAmtcell);
			
			
			
			Phrase actuaEearnCompAmt = new Phrase(df.format(earnings.getAmount())+ "", font8);
			PdfPCell actuaEearnCompAmtcell = new PdfPCell(actuaEearnCompAmt);
			actuaEearnCompAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			actuaEearnCompAmtcell.addElement(actuaEearnCompAmt);
			
			table.addCell(actuaEearnCompAmtcell);

		}
		
		
		//GROSS EARNING ROW
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		Phrase actuaEearnCompAmt0 = new Phrase("(A) Gross Earning", font9bold);
		PdfPCell actuaEearnCompAmtcell0 = new PdfPCell();
		actuaEearnCompAmtcell0.addElement(actuaEearnCompAmt0);
		Phrase actuaEearnCompAmt1 = new Phrase(df.format(sum), font9bold);
		PdfPCell actuaEearnCompAmtcell1 = new PdfPCell(actuaEearnCompAmt1);
//		actuaEearnCompAmtcell1.addElement(actuaEearnCompAmt1);
		actuaEearnCompAmtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase actuaEearnCompAmt2 = new Phrase(df.format(actualSum), font9bold);
		PdfPCell actuaEearnCompAmtcell2 = new PdfPCell(actuaEearnCompAmt2);
//		actuaEearnCompAmtcell2.addElement(actuaEearnCompAmt2);
		actuaEearnCompAmtcell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		table.addCell(cellmonthly1);
		table.addCell(actuaEearnCompAmtcell0);
		table.addCell(actuaEearnCompAmtcell1);
		table.addCell(actuaEearnCompAmtcell2);
		
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		
		//Deduction
		
		Phrase srno2 = new Phrase("B)", font1);
		Phrase monthlyPayable2 = new Phrase("Deduction", font1);
			
		PdfPCell cellsrno2 = new PdfPCell(srno2);
		cellsrno2.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellparticular2 = new PdfPCell(monthlyPayable2);
		cellparticular2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellsrno2);
		table.addCell(cellparticular2);
		table.addCell(cellmonthly1);
		table.addCell(cellyearly1);
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		
		
		
		List<CtcComponent> deducList=new ArrayList<CtcComponent>();
		
		for(CtcComponent deductions:ctc.getDeduction()){
//			/**
//			 * @author Anil ,Date : 06-05-2019
//			 */
//			if(deductions.getAmount()!=null&&deductions.getAmount()!=0){
//				deducList.add(deductions);
//			}
//			/**
//			 * @author Anil , Date : 06-05-2019
//			 * Commented this code it recalculate deductions
//			 */
			
//		for(CtcComponent deductions:deductionList){
			logger.log(Level.SEVERE,"Deduction COMP : "+deductions.getName());
			if(deductions.getIsNonMonthly()==false&&deductions.getIsRecord()==false){
				double dedAmount = 0;
				
				if (deductions.getMaxPerOfCTC() != null&&deductions.getMaxPerOfCTC()!=0) {
					logger.log(Level.SEVERE, "CTC COMPONENT : "+deductions.getCtcComponentName()+"Deduction COMP : "+deductions.getName());
					double dedutionAmountMonthly=getCtcComponentAmountMonthly(ctcList,deductions.getCtcComponentName());
					
					if(deductions.getCondition()!=null&&!deductions.getCondition().equals("")){
						
						if(deductionTypeName!=null&&!deductionTypeName.equals("")
								&&deductions.getShortName().equalsIgnoreCase("ESIC")){
							dedutionAmountMonthly=dedutionAmountMonthly-getCtcComponentAmountMonthly(ctcList,deductionTypeName);
						}
						
						
						logger.log(Level.SEVERE,"CONDITION NOT NULL : "+deductions.getName());
						int applicableAmountAnnualy=deductions.getConditionValue();
//						double applicableAmtMonthly=applicableAmountAnnualy/12;
						logger.log(Level.SEVERE,"CONDITION VAL: "+applicableAmountAnnualy+" COND "+deductions.getCondition());
						logger.log(Level.SEVERE,"ANNUAL: "+applicableAmountAnnualy+"DED "+dedutionAmountMonthly);
						if(deductions.getCondition().trim().equals("<")){
							if(dedutionAmountMonthly<applicableAmountAnnualy){
								System.out.println("AMOUNT <");
								dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
								deductions.setAmount(dedAmount);
								deducList.add(deductions);
							}
						}
						if(deductions.getCondition().trim().equals(">")){
							if(dedutionAmountMonthly>applicableAmountAnnualy){
								System.out.println("AMOUNT >");
								dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
								deductions.setAmount(dedAmount);
								deducList.add(deductions);
							}
						}
						if(deductions.getCondition().trim().equals("=")){
							if(dedutionAmountMonthly==applicableAmountAnnualy){
								System.out.println("AMOUNT =");
								dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
								deductions.setAmount(dedAmount);
								deducList.add(deductions);
							}
						}
						
					}else{
						System.out.println("INSIDE ELSE CONDITION NULL");
						dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
						deductions.setAmount(dedAmount);
						deducList.add(deductions);
					}
				}
				else {
					System.out.println("MAX CTC % NULL");
					if(deductions.getMaxAmount()!=null){
						dedAmount = deductions.getMaxAmount() / 12;
						deductions.setAmount(dedAmount);
						deducList.add(deductions);
					}
				}
			}
			
		}
		
		/**
		 * @author Anil,Date : 06-05-2019
		 */
		CtcComponent pfComponent1=null;
		for(int i=0;i<deducList.size();i++){
			CtcComponent deduction=deducList.get(i);
			if(deduction.getShortName().trim().equalsIgnoreCase("PF")){
				if(pfComponent1==null){
					pfComponent1=deduction;
				}else{
					pfComponent1.setAmount(pfComponent1.getAmount()+deduction.getAmount());
				}
				deducList.remove(i);
				i--;
			}
		}
		if(pfComponent1!=null){
			deducList.add(pfComponent1);
		}
		CtcComponent pfComponent2=null;
		for(int i=0;i<deducList.size();i++){
			CtcComponent deduction=deducList.get(i);
			if(deduction.getShortName().trim().equalsIgnoreCase("LWF")){
				if(pfComponent2==null){
					pfComponent2=deduction;
				}else{
					pfComponent2.setAmount(pfComponent2.getAmount()+deduction.getAmount());
				}
				deducList.remove(i);
				i--;
			}
		}
		if(pfComponent2!=null){
			deducList.add(pfComponent2);
		}
		
		/**
		 * Date : 14-11-2018 BY ANIL
		 */
		if(pfMaxFixedValue!=0){
			System.out.println("PF MAX FIXED AMOUNT : "+pfMaxFixedValue);
			CtcComponent pfComponent=null;
			for(int i=0;i<deducList.size();i++){
				CtcComponent deduction=deducList.get(i);
				if(deduction.getShortName().trim().equalsIgnoreCase("PF")){
					if(pfComponent==null){
						pfComponent=deduction;
					}else{
						pfComponent.setAmount(pfComponent.getAmount()+deduction.getAmount());
					}
					deducList.remove(i);
					i--;
				}
			}
			
			if(pfComponent!=null){
				System.out.println("PF AMOUNT : "+pfComponent.getAmount());
				if(pfComponent.getAmount()>(pfMaxFixedValue*pfComponent.getMaxPerOfCTC()/100)){
					pfComponent.setAmount(pfMaxFixedValue*pfComponent.getMaxPerOfCTC()/100);
				}
				
				System.out.println("AFTER PF AMOUNT : "+pfComponent.getAmount());
				deducList.add(pfComponent);
			}
		}
		/**
		 * End
		 */
		
		
		
		double dedSum=0;
		double yearlyDeudSum=0;
		for(CtcComponent earnings : deducList){
			if(earnings.getAmount()!=0){
			table.addCell(cellmonthly1);
			Phrase earnCompNm = new Phrase(earnings.getShortName(), font8);
//			Phrase earnCompNm = new Phrase(earnings.getName(), font8);
			PdfPCell earnCompNmcell = new PdfPCell();
			earnCompNmcell.addElement(earnCompNm);
			table.addCell(earnCompNmcell);
			
			double earning = earnings.getAmount()*12;
			dedSum = dedSum + earning;
			
			double earning1 = earnings.getAmount();
			yearlyDeudSum = yearlyDeudSum + earning1;
			
			
			Phrase earnCompAmt = new Phrase(df.format(earnings.getAmount()) + "", font8);
			PdfPCell earnCompAmtcell = new PdfPCell(earnCompAmt);
//			earnCompAmtcell.addElement(earnCompAmt);
			earnCompAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(earnCompAmtcell);
			
			
			
			Phrase actuaEearnCompAmt = new Phrase(df.format(earning)+ "", font8);
			PdfPCell actuaEearnCompAmtcell = new PdfPCell(actuaEearnCompAmt);
//			actuaEearnCompAmtcell.addElement(actuaEearnCompAmt);
			actuaEearnCompAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(actuaEearnCompAmtcell);
			}
		}
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		Phrase actuaEearnCompAmt01 = new Phrase("(B) Total Deduction", font9bold);
		PdfPCell actuaEearnCompAmtcell01 = new PdfPCell();
		actuaEearnCompAmtcell01.addElement(actuaEearnCompAmt01);
		
		Phrase actuaEearnCompAmt11 = new Phrase(df.format(yearlyDeudSum)+ "", font9bold);
		PdfPCell actuaEearnCompAmtcell11 = new PdfPCell(actuaEearnCompAmt11);
//		actuaEearnCompAmtcell11.addElement(actuaEearnCompAmt11);
		actuaEearnCompAmtcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase actuaEearnCompAmt21 = new Phrase(df.format(dedSum)+ "", font9bold);
		PdfPCell actuaEearnCompAmtcell21 = new PdfPCell(actuaEearnCompAmt21);
//		actuaEearnCompAmtcell21.addElement(actuaEearnCompAmt21);
		actuaEearnCompAmtcell21.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		table.addCell(cellmonthly1);
		table.addCell(actuaEearnCompAmtcell01);
		table.addCell(actuaEearnCompAmtcell11);
		table.addCell(actuaEearnCompAmtcell21);
		
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		//NET TAKE HOME
		double netAcHomeMOnth=sum-yearlyDeudSum;
		double netAcHomeyearly=actualSum-dedSum;
		
		Phrase srno21 = new Phrase("C)", font1);
		Phrase monthlyPayable21 = new Phrase("Net Take Home (A-B)", font1);
		Phrase srno211 = new Phrase(df.format(netAcHomeMOnth), font1);
		Phrase monthlyPayable211= new Phrase(df.format(netAcHomeyearly), font1);
					
		PdfPCell cellsrno21 = new PdfPCell(srno21);
		cellsrno21.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellparticular21 = new PdfPCell(monthlyPayable21);
		cellparticular21.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellsrno211 = new PdfPCell(srno211);
		cellsrno211.setHorizontalAlignment(Element.ALIGN_RIGHT);
		PdfPCell cellparticular211 = new PdfPCell(monthlyPayable211);
		cellparticular211.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
		table.addCell(cellsrno21);
		table.addCell(cellparticular21);
		table.addCell(cellsrno211);
		table.addCell(cellparticular211);
				
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		
		
		//COMPANY CONTRIBUTION
		
		Phrase srno22 = new Phrase("D)", font1);
		Phrase monthlyPayable22 = new Phrase("Company's Contribution", font1);
					
		PdfPCell cellsrno22 = new PdfPCell(srno22);
		cellsrno22.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellparticular22 = new PdfPCell(monthlyPayable22);
		cellparticular22.setHorizontalAlignment(Element.ALIGN_CENTER);
				
		table.addCell(cellsrno22);
		table.addCell(cellparticular22);
		table.addCell(cellmonthly1);
		table.addCell(cellyearly1);
				
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
				
		
		
		
		
		
		
		
		
		
		List<CtcComponent> compConList=new ArrayList<CtcComponent>();
		for (CtcComponent deductions : ctc.getCompaniesContribution()) {
			double dedAmount = 0;
			
			if (deductions.getMaxPerOfCTC() != null&&deductions.getMaxPerOfCTC()!=0) {
				double dedutionAmountMonthly=0;
				
				dedutionAmountMonthly=getCtcComponentAmountMonthly(ctc.getEarning(),deductions.getCtcComponentName());
				
				if(deductions.getCondition()!=null&&!deductions.getCondition().equals("")){
					if(deductionTypeName!=null&&!deductionTypeName.equals("")
							&&deductions.getShortName().equalsIgnoreCase("ESIC")){
						dedutionAmountMonthly=dedutionAmountMonthly-getCtcComponentAmountMonthly(ctc.getEarning(),deductionTypeName);
					}
					
					int applicableAmountAnnualy=deductions.getConditionValue();
//					double applicableAmtMonthly=applicableAmountAnnualy/12;
					
					if(deductions.getCondition().trim().equals("<")){
						if(dedutionAmountMonthly<applicableAmountAnnualy){
							System.out.println("AMOUNT <");
							dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
							deductions.setAmount(dedAmount);
							compConList.add(deductions);
						}
					}
					if(deductions.getCondition().trim().equals(">")){
						if(dedutionAmountMonthly>applicableAmountAnnualy){
							System.out.println("AMOUNT >");
							dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
							deductions.setAmount(dedAmount);
							compConList.add(deductions);
						}
					}
					if(deductions.getCondition().trim().equals("=")){
						if(dedutionAmountMonthly==applicableAmountAnnualy){
							System.out.println("AMOUNT =");
							dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
							deductions.setAmount(dedAmount);
							compConList.add(deductions);
						}
					}
					
				}else{
					dedAmount=dedutionAmountMonthly*deductions.getMaxPerOfCTC()/100;
					deductions.setAmount(dedAmount);
					compConList.add(deductions);
				}
			}
			else {
				if(deductions.getMaxAmount()!=null){
					dedAmount = deductions.getMaxAmount() / 12;
					deductions.setAmount(dedAmount);
					compConList.add(deductions);
				}
			}
		}
		
		
		/**
		 * @author Anil,Date : 06-05-2019
		 */
		CtcComponent pfComponent3=null;
		for(int i=0;i<compConList.size();i++){
			CtcComponent deduction=compConList.get(i);
			if(deduction.getShortName().trim().equalsIgnoreCase("PF")){
				if(pfComponent3==null){
					pfComponent3=deduction;
				}else{
					pfComponent3.setAmount(pfComponent3.getAmount()+deduction.getAmount());
				}
				compConList.remove(i);
				i--;
			}
		}
		if(pfComponent3!=null){
			compConList.add(pfComponent3);
		}
		CtcComponent pfComponent4=null;
		for(int i=0;i<compConList.size();i++){
			CtcComponent deduction=compConList.get(i);
			if(deduction.getShortName().trim().equalsIgnoreCase("LWF")){
				if(pfComponent4==null){
					pfComponent4=deduction;
				}else{
					pfComponent4.setAmount(pfComponent4.getAmount()+deduction.getAmount());
				}
				compConList.remove(i);
				i--;
			}
		}
		if(pfComponent4!=null){
			compConList.add(pfComponent4);
		}
		
		/**
		 * Date : 14-11-2018 BY ANIL
		 */
		if(pfMaxFixedValue!=0){
			System.out.println("PF MAX FIXED AMOUNT : "+pfMaxFixedValue);
			CtcComponent pfComponent=null;
			for(int i=0;i<compConList.size();i++){
				CtcComponent deduction=compConList.get(i);
				if(deduction.getShortName().trim().equalsIgnoreCase("PF")){
					if(pfComponent==null){
						pfComponent=deduction;
					}else{
						pfComponent.setAmount(pfComponent.getAmount()+deduction.getAmount());
					}
					compConList.remove(i);
					i--;
				}
			}
			
			if(pfComponent!=null){
				System.out.println("PF AMOUNT : "+pfComponent.getAmount());
				if(pfComponent.getAmount()>(pfMaxFixedValue*pfComponent.getMaxPerOfCTC()/100)){
					pfComponent.setAmount(pfMaxFixedValue*pfComponent.getMaxPerOfCTC()/100);
				}
				
				System.out.println("AFTER PF AMOUNT : "+pfComponent.getAmount());
				compConList.add(pfComponent);
			}
		}
		/**
		 * End
		 */
		
		
		double comSum=0;
		double yearlycomSum=0;
		for(CtcComponent earnings : compConList){
			if(earnings.getAmount()!=0){
			
			table.addCell(cellmonthly1);
			
			Phrase earnCompNm = new Phrase(earnings.getShortName(), font8);
//			Phrase earnCompNm = new Phrase(earnings.getName(), font8);
			PdfPCell earnCompNmcell = new PdfPCell();
			earnCompNmcell.addElement(earnCompNm);
			table.addCell(earnCompNmcell);
			
			double earning = earnings.getAmount()*12;
			comSum = comSum + earning;
			
			double earning1 = earnings.getAmount();
			yearlycomSum = yearlycomSum + earning1;
			
			
			Phrase earnCompAmt = new Phrase(df.format(earnings.getAmount()) + "", font8);
			PdfPCell earnCompAmtcell = new PdfPCell(earnCompAmt);
//			earnCompAmtcell.addElement(earnCompAmt);
			earnCompAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(earnCompAmtcell);
			
			
			
			Phrase actuaEearnCompAmt = new Phrase(df.format(earning)+ "", font8);
			PdfPCell actuaEearnCompAmtcell = new PdfPCell(actuaEearnCompAmt);
//			actuaEearnCompAmtcell.addElement(actuaEearnCompAmt);
			actuaEearnCompAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(actuaEearnCompAmtcell);
		}
		}
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		
		Phrase actuaEearnCompAmt013 = new Phrase("(D) Total CC", font9bold);
		PdfPCell actuaEearnCompAmtcell013 = new PdfPCell();
		actuaEearnCompAmtcell013.addElement(actuaEearnCompAmt013);
		
		Phrase actuaEearnCompAmt113 = new Phrase(df.format(yearlycomSum)+ "", font9bold);
		PdfPCell actuaEearnCompAmtcell113 = new PdfPCell(actuaEearnCompAmt113);
//		actuaEearnCompAmtcell113.addElement(actuaEearnCompAmt113);
		actuaEearnCompAmtcell113.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase actuaEearnCompAmt213 = new Phrase(df.format(comSum)+ "", font9bold);
		PdfPCell actuaEearnCompAmtcell213 = new PdfPCell(actuaEearnCompAmt213);
//		actuaEearnCompAmtcell213.addElement(actuaEearnCompAmt213);
		actuaEearnCompAmtcell213.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		table.addCell(cellmonthly1);
		table.addCell(actuaEearnCompAmtcell013);
		table.addCell(actuaEearnCompAmtcell113);
		table.addCell(actuaEearnCompAmtcell213);
		
		
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		
		Phrase srno214 = new Phrase("E)", font1);
		Phrase monthlyPayable214 = new Phrase("CTC Amount (C+D)", font1);
		Phrase srno2114 = new Phrase(df.format(yearlycomSum+netAcHomeMOnth), font1);
		Phrase monthlyPayable2114= new Phrase(df.format(comSum+netAcHomeyearly), font1);
					
		PdfPCell cellsrno214 = new PdfPCell(srno214);
		cellsrno214.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellparticular214 = new PdfPCell(monthlyPayable214);
		cellparticular214.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellsrno2114 = new PdfPCell(srno2114);
		cellsrno2114.setHorizontalAlignment(Element.ALIGN_RIGHT);
		PdfPCell cellparticular2114 = new PdfPCell(monthlyPayable2114);
		cellparticular2114.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
		table.addCell(cellsrno214);
		table.addCell(cellparticular214);
		table.addCell(cellsrno2114);
		table.addCell(cellparticular2114);
				
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		table.addCell(cellmonthly1);
		
		
		PdfPTable parentTableProd=new PdfPTable(1);
	    parentTableProd.setWidthPercentage(100);
	    parentTableProd.addCell(table);
	  
	   
	    
	    
	    try {
	    	if(document!=null)
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	    
	    return parentTableProd;
			
	}
	
	public double getCtcComponentAmountMonthly(List<CtcComponent> earningList,String ctcComponentName){
		double amount=0;
		if(ctcComponentName.equals("Gross Earning")){
			for (CtcComponent earnings : earningList) {
				double earning = earnings.getAmount() / 12;
				amount=amount+earning;
			}
		}else{
			for(int i=0;i<earningList.size();i++){
				if(earningList.get(i).getName().equals(ctcComponentName)){
					amount=earningList.get(i).getAmount()/ 12;
				}
			}
		}
		return amount;
	}
	
	public double getCtcComponentAmountYearly(List<CtcComponent> earningList,String ctcComponentName){
		double amount=0;
		if(ctcComponentName.equals("Gross Earning")){
			for (CtcComponent earnings : earningList) {
				double earning = earnings.getAmount();
				amount=amount+earning;
			}
		}else{
			for(int i=0;i<earningList.size();i++){
				if(earningList.get(i).getName().equals(ctcComponentName)){
					amount=earningList.get(i).getAmount();
				}
			}
		}
		return amount;
	}
	
	
	public void getCompaniesContribution(){
		
		
	}

}
