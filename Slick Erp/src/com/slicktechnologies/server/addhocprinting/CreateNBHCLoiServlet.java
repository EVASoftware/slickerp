package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateNBHCLoiServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 410004055413198992L;

	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//super.doGet(request, response);
		response.setContentType("application/pdf");
	


	
	String stringid = request.getParameter("Id");
	stringid = stringid.trim();
	Long count = Long.parseLong(stringid);
	
		 try{
			 NBHCLoiPdf pdf= new  NBHCLoiPdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setQuotationLoi(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
}
