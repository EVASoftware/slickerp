package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocprinting.nbhc.NBHCContractRenewalPdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

public class CreateContractRenewalPDFServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -682299831112534316L;

	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		//resp.setContentType("application/pdf");
		
		resp.setContentType("application/pdf; charset=UTF-8");
		resp.setCharacterEncoding("UTF-8");
		
		try {
			
			String stringid1 = req.getParameter("ContractId");
			stringid1 = stringid1.trim();
			int count = Integer.parseInt(stringid1);
			
			String stringid = req.getParameter("CompanyId");
			stringid = stringid.trim();
			Long companyId = Long.parseLong(stringid);
			
			String renewalFmt="";
			try{
				renewalFmt = req.getParameter("renewalFmt");
			}catch(Exception e){
				
			}
			
			String callFrom="";
			try{
				callFrom = req.getParameter("callFrom");
			}catch(Exception e){
				
			}
			
			ContractRenewal wo = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", count).first().now();
			
			/**
			 * Nidhi added this code for NBHC CCPM Date : 17-07-2017
			 */
			
			Document document = null;
			boolean flag = Boolean.parseBoolean(req.getParameter("flag"));
			System.out.println("Flag value : "+flag);
			if(flag){
				NBHCContractRenewalPdf wopdf = new NBHCContractRenewalPdf();
				wopdf.document = new Document();
				 document = wopdf.document;
				 
				 PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
					
					 
					 document.open();
						wopdf.setContractRewnewal(wo);
					 
					   String preprintStatus=req.getParameter("preprint");
					   System.out.println("****************"+preprintStatus);
					   if(preprintStatus.contains("yes")){
						   
						   wopdf.createPdf(preprintStatus);
					   }
					   else  if(preprintStatus.contains("no")){
						   
						   wopdf.createPdf(preprintStatus);
					   }
					   else{
						   wopdf.createPdf(preprintStatus);
					   }
					
					document.close();
				 
			}
			else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "ContractRenewalNewFormat", companyId)){
//			else if(renewalFmt!=null&&renewalFmt.equals("New")){
				
				Contract contract = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", count).first().now();
				
				NewContractRenewalPdf newConRenFormat=new NewContractRenewalPdf();
				
				newConRenFormat.document = new Document();
				 document = newConRenFormat.document;
				 
				 PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
					
					 
					 document.open();
					 newConRenFormat.setContractRewnewal(contract);
					 
					   String preprintStatus=req.getParameter("preprint");
					   System.out.println("****************"+preprintStatus);
					   if(preprintStatus.contains("yes")){
						   
						   newConRenFormat.createPdf(preprintStatus);
					   }
					   else  if(preprintStatus.contains("no")){
						   
						   newConRenFormat.createPdf(preprintStatus);
					   }
					   else{
						   newConRenFormat.createPdf(preprintStatus);
					   }
					
					document.close();
				
				
				
				}
		    else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "OnlyforPecopp", companyId)){
			
			
			ContractRenewalPdf wopdf = new ContractRenewalPdf();
			wopdf.document = new Document();
			document = wopdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
//			if(!wo.getStatus().equals("Renewed")){
//				writer.setPageEvent(new PdfWatermark());
//				
//			}
//			else {
////				if(!wo.getStatus().equals("Inprocessed")){	
//				writer.setPageEvent(new PdfCancelWatermark());
//			} 
			 
			 	document.open();
				wopdf.setContractRewnewal(wo);
			 
			   String preprintStatus=req.getParameter("preprint");
			   System.out.println("****************"+preprintStatus);
			   if(preprintStatus.contains("yes")){
				   
				   wopdf.createPdf(preprintStatus);
			   }
			   else  if(preprintStatus.contains("no")){
				   
				   wopdf.createPdf(preprintStatus);
			   }
			   else{
				   wopdf.createPdf(preprintStatus);
			   }
			document.close();
			}
		    else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "PC_PrintRenewalLetterInEnglishAndThai", companyId)){
				
		    	ContractRenewalPdfversionOne crpdf = new ContractRenewalPdfversionOne();
		    	crpdf.document = new Document();
				document = crpdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				
				String preprintStatus=req.getParameter("preprint");
				System.out.println("****************"+preprintStatus);													

				document.open();
				crpdf.setContractRewnewal(wo, preprintStatus);
				if (preprintStatus.contains("Thai")) {
					crpdf.createPdf(preprintStatus);
				} else {
					crpdf.createPdf(preprintStatus);
				}
				document.close();
			}
			/**
			 * Added by sheetal:23-11-2021
			 * Des: Pepcopp wants to print old renewal letter format
			 */
			
		    else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", AppConstants.PC_PDF_ContractRenewal_Old_New_Price_format, companyId)){
				
				
				ContractRenewalPdf wopdf = new ContractRenewalPdf();
				wopdf.document = new Document();
				document = wopdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																			
//				if(!wo.getStatus().equals("Renewed")){
//					writer.setPageEvent(new PdfWatermark());
//					
//				}
//				else {
////					if(!wo.getStatus().equals("Inprocessed")){	
//					writer.setPageEvent(new PdfCancelWatermark());
//				} 
					/**
					 * @author Vijay Date :- 21-03-2023
					 * Des :- contract renewal entry not loaded becuase of contract id current contract id it should be previous contract id
					 * so updated code
					 */
					if(wo==null){
							Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", count).first().now();
							if(contractEntity!=null){
								wo = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", contractEntity.getRefContractCount()).first().now();
							}
					}
					/**
					 * ends here
					 */
				 	document.open();
					wopdf.setContractRewnewal(wo);
				 
				   String preprintStatus=req.getParameter("preprint");
				   System.out.println("****************"+preprintStatus);
				   if(preprintStatus.contains("yes")){
					   
					   wopdf.createPdf(preprintStatus);
				   }
				   else  if(preprintStatus.contains("no")){
					   
					   wopdf.createPdf(preprintStatus);
				   }
				   else{
					   wopdf.createPdf(preprintStatus);
				   }
				document.close();
				}
			/*end*/
		    else{
				
				Contract contract = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", count).first().now();
				NewContractRenewalVersionOnePdf ncrpdf = new NewContractRenewalVersionOnePdf();
				ncrpdf.document = new Document();
				document = ncrpdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				 
				 	document.open();
				 	ncrpdf.setContractRewnewal(contract,wo);
				 	//setContractRewnewal(contract);
				 
				   String preprintStatus=req.getParameter("preprint");
				   System.out.println("****************"+preprintStatus);
				   if(preprintStatus.contains("yes")){
					   
					   ncrpdf.createPdf(preprintStatus,callFrom);
				   }
				   else  if(preprintStatus.contains("no")){
					   
					   ncrpdf.createPdf(preprintStatus,callFrom);
				   }
				   else{
					   ncrpdf.createPdf(preprintStatus,callFrom);
				   }
				document.close();
				}
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
