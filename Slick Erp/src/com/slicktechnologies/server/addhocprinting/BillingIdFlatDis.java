package com.slicktechnologies.server.addhocprinting;

public class BillingIdFlatDis {

	int billingId;
	double flatAmt;
	
	
	public BillingIdFlatDis() {
		// TODO Auto-generated constructor stub
	}
	
	
	public int getBillingId() {
		return billingId;
	}
	public void setBillingId(int billingId) {
		this.billingId = billingId;
	}
	public double getFlatAmt() {
		return flatAmt;
	}
	public void setFlatAmt(double flatAmt) {
		this.flatAmt = flatAmt;
	}
	
	
}
