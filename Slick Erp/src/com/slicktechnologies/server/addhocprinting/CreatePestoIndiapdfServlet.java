package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
public class CreatePestoIndiapdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2642014278383238110L;


	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// super.doGet(request, response);
		System.out.println("Inside Pesto India Servlet");
		ProcessConfiguration processConfig;
		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.
/******************For Pesto India Quotation*************************/
		try {
			String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("Count :" + count);
//			Contract contEntity = ofy().load().type(Contract.class).id(count)
//					.now();
//			Quotation quotEntity = ofy().load().type(Quotation.class).id(count)
//					.now();
//			
			
			  String type=request.getParameter("type");
			  
			  String preprintStatus=request.getParameter("preprint");
			
			 if(type.contains("q")){
				 
			System.out.println("type q");
			PestoIndiaQuotationPdf pdf = new PestoIndiaQuotationPdf();
			pdf.document = new Document();
			Document document = pdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					response.getOutputStream()); // write the pdf in response

			document.open();
			System.out.println("Document Opened");
			pdf.getQuotation(count);// ,preprintStatus);

			System.out.println("Called get Quotation");
			  if(preprintStatus.contains("yes")){
				  
					System.out.println("yes ");
				  pdf.createPdf(preprintStatus);
			  }
			  else
			  {
					System.out.println("no ");
				  pdf.createPdf(preprintStatus);
			  }
			System.out.println("Called Create Pdf");
			document.close();

			System.out.println("Document closed");
			System.out.println("Servlet End");
			 
			}
			 else if(type.contains("c"))
			 {
				 System.out.println("type c");
			PestoIndiaContractPdf pdf = new PestoIndiaContractPdf();
			pdf.document = new Document();
			Document document = pdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					response.getOutputStream()); // write the pdf in response

			document.open();
			System.out.println("Document Opened");
			pdf.getContract(count);// ,preprintStatus);

			System.out.println("Called get Quotation");
			  if(preprintStatus.contains("yes")){
					System.out.println("yes");
				  pdf.createPdf(preprintStatus);
			  }
			  else
			  {
					System.out.println("no ");
				  pdf.createPdf(preprintStatus);
			  }

			System.out.println("Called Create Pdf");
			document.close();

			System.out.println("Document closed");
			System.out.println("Servlet End");
			 
		    }
			 else if(type.contains("i"))
			 {
				 System.out.println("type i");
			PestoIndiaInvoicePdf pdf = new PestoIndiaInvoicePdf();
			pdf.document = new Document();
			Document document = pdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					response.getOutputStream()); // write the pdf in response

			document.open();
			System.out.println("Document Opened");
			pdf.getInvoice(count);// ,preprintStatus);

			System.out.println("Called get Quotation");
			 if(preprintStatus.contains("yes")){
					System.out.println("yes");
				  pdf.createPdf(preprintStatus);
			  }
			  else
			  {
					System.out.println("no");
				  pdf.createPdf(preprintStatus);
			  }

			System.out.println("Called Create Pdf");
			document.close();

			System.out.println("Document closed");
			System.out.println("Servlet End");
			 }
		} catch (Exception e) {

		}

	}
}
