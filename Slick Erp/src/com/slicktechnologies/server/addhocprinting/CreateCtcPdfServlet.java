package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import static com.googlecode.objectify.ObjectifyService.ofy;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;

public class CreateCtcPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3720625556946367408L;
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			CTC ctc = ofy().load().type(CTC.class).id(count).now();
			
			CtcPdf ctcpdf = new CtcPdf();
			
			ctcpdf.document = new Document();
			Document document = ctcpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
																
			if(!ctc.getStatus().equals("Active")){	
				 writer.setPageEvent(new PdfWatermark());
			} 
			 document.open();
			
			ctcpdf.setCtc(count);
			ctcpdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
	}

}
