package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import com.googlecode.objectify.LoadResult;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Loader;
import com.googlecode.objectify.cmd.Query;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.collection.PdfTargetDictionary;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;


public class OrionServiceInvoicePdf {
	
	
	double taxAmountInWords=0;
  Logger logger = Logger.getLogger("OrionServiceInvoicePdf.class");
	  
	  List<SalesLineItem> products;
	  ArrayList<String> dataStringList;
	  ArrayList<Integer> alignmentList;
	  ArrayList<Chunk> chunkList;
	  ArrayList<Font> fontList;
	  boolean serviceWiseBilling=false;
	  
	  int firstBreakPoint = 17;
	  
	  float blankLines;
	  float[] columnMoreLeftWidths = { 2.0F, 1.0F };
	  float[] columnMoreLeftHeaderWidths = { 1.7F, 1.3F };
	  
	  float[] columnMoreRightWidths = { 0.8F, 2.2F };
	  float[] columnMoreRightCheckBoxWidths = { 0.3F, 2.7F };
	  float[] columnHalfWidth = { 1.0F, 1.0F };
	  float[] columnHalfInnerWidth = { 0.65F, 1.0F };
	  
	  float[] columnHalThreefWidth = { 0.5F, 0.5F,1.0f };

	  
	  float[] columnCollonWidth = { 1.8F, 0.2F, 7.5F };
	  float[] columnCollon6Width = { 1.8F, 0.2F, 7.5F, 1.8F, 0.2F, 7.5F };
	  
	  float[] columnCollonGSTWidth = { 0.8F, 0.2F, 1.5F };
	  float[] columnStateCodeCollonWidth = { 3.5F, 2.0F, 0.2F, 1.0F };
	  float[] columnContractPeriodDateCodeCollonWidth = { 3.0F, 1.5F, 0.2F, 2.0F };
	  float[] columnDateCollonWidth = { 1.5F, 0.2F, 1.2F };
	  
	  float[] column6RowCollonWidth = { 0.5F, 0.2F, 1.5F, 0.8F, 0.2F, 0.5F };
	  float[] column16CollonWidth = { 0.1F, 0.4F, 0.2F, 0.15F, 0.15F, 0.3F, 0.3F, 
	    0.15F, 0.25F, 0.15F, 0.25F, 0.15F, 0.25F, 0.15F, 0.25F, 0.3F };
	  float[] columnrohanrrCollonWidth = { 2.8F, 0.2F, 7.7F };
	  





	  float[] column7ProdCollonWidth = { 0.1F, 1.08F, 0.2F, 0.38F, 0.2F, 0.15F, 
	    0.25F };
	  
	  float[] column8SerProdCollonWidth = { 0.1F, 0.92F, 0.16F, 0.2F, 0.38F, 
	    0.2F, 0.15F, 0.25F };
	  float[] column8ProdCollonWidth = { 0.1F, 1.08F, 0.2F, 0.19F, 0.19F, 0.2F, 
	    0.15F, 0.25F };
	  float[] column9ServProdCollonWidth = { 0.1F, 1.0F, 0.08F, 0.2F, 0.19F, 0.19F, 
	    0.2F, 0.15F, 0.25F };
	  float[] column4ProdCollonWidth = { 1.75F, 0.2F, 0.15F, 0.25F };
	  float[] column5ProdCollonWidth = { 1.75F, 0.15F, 0.2F, 0.15F, 0.25F };
	  
	  float[] column2ProdCollonWidth = { 1.9F, 0.25F };
	  float[] column3ProdCollonWidth = { 1.0F, 0.9F, 0.4F };
	  
	  float[] column13CollonWidth = { 0.1F, 0.4F, 0.2F, 0.15F, 0.15F, 0.3F, 0.3F, 
	    0.15F, 0.25F, 0.4F, 0.4F, 0.4F, 0.3F };
	  







	  Boolean UniversalFlag = Boolean.valueOf(false);
	  





	  Boolean isSingleService = Boolean.valueOf(false);
	  
	  public Document document;
	  
	  Invoice invoiceentity;
	  
	  List<BillingDocumentDetails> billingDoc;
	  
	  ProcessConfiguration processConfig;
	  Customer cust;
	  Company comp;
	  Contract con;
	  List<TaxesAndCharges.ContractCharges> billingTaxesLis;
	  List<CustomerBranchDetails> custbranchlist;
	  List<CustomerBranchDetails> customerbranchlist;
	  List<Branch> branch;
	  SimpleDateFormat sdf;
	  /**
	   * Date 2-5-2018
	   * By jayshree
	   * 
	   */
	  SimpleDateFormat monthformat;
	  SimpleDateFormat yearformat;
	  
	  //end
	  DecimalFormat df = new DecimalFormat("0.00");
	  boolean upcflag = false;
	  boolean onlyForFriendsPestControl = false;
	  boolean authOnLeft = false;
	  boolean isPlaneFormat = false;
	  
	  
	  
	  
	  boolean serviceSchedulelistFlag = false;
	  


	  Boolean multipleCompanyName = Boolean.valueOf(false);
	  Boolean printPremiseDetails = Boolean.valueOf(false);
	  


	  List<PaymentTerms> payTermsLis;
	  

	  List<State> stateList;
	  

	  Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16.0F, 5);
	  
	  Font font16bold = new Font(Font.FontFamily.HELVETICA, 16.0F, 1);
	  Font font14bold = new Font(Font.FontFamily.HELVETICA, 14.0F, 1);
	  Font font14 = new Font(Font.FontFamily.HELVETICA, 14.0F);
	  Font font10 = new Font(Font.FontFamily.HELVETICA, 7.0F);
	  Font font10bold = new Font(Font.FontFamily.HELVETICA, 7.0F, 1);
	  



	  Font font13 = new Font(Font.FontFamily.HELVETICA, 9.0F);
	  Font font13bold = new Font(Font.FontFamily.HELVETICA, 9.0F, 1);
	  
	  Font font12bold = new Font(Font.FontFamily.HELVETICA, 12.0F, 1);
	  Font font8bold = new Font(Font.FontFamily.HELVETICA, 8.0F, 1);
	  Font font8 = new Font(Font.FontFamily.HELVETICA, 8.0F);
	  Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12.0F, 5);
	  
	  Font font12 = new Font(Font.FontFamily.HELVETICA, 12.0F);
	  Font font11 = new Font(Font.FontFamily.HELVETICA, 10.0F);
	  Font font11bold = new Font(Font.FontFamily.HELVETICA, 10.0F, 1);
	  Font font6 = new Font(Font.FontFamily.HELVETICA, 7.0F);
	  Font font6bold = new Font(Font.FontFamily.HELVETICA, 6.0F, 1);
	  Font font9bold = new Font(Font.FontFamily.HELVETICA, 9.0F, 1);
	  
	  Phrase blankCell = new Phrase(" ", font10);
	  



	  double totalAmount;
	  


	  int noOfLines = 10; //old 16 changed on 22-12-2023. Due to qr code size pdf going to next page.
	  


	  int prouductCount = 0;
	  



	  boolean productDescFlag = false;
	  



	  boolean printAttnInPdf = false;
	  boolean consolidatePrice = false;
	  


	  CompanyPayment comppayment;
	  


	  private PdfPCell imageSignCell;
	  

	  boolean checkEmailId = false;
	  








	  boolean checkheaderLeft = false;
	  boolean checkheaderRight = false;
	  
	  /**
	   * @author Anil ,Date : 30-03-2019
	   * added contract duration and service address flag
	   */
	  boolean serviceAddressFlag=false;
	  boolean contractDurationFlag=false;
	  Branch branchDt = null;
      boolean branchAsCompany=false;
	  public OrionServiceInvoicePdf() {}
	  PdfUtility pdfUtility = new PdfUtility();
	  ServerAppUtility serverApp = new ServerAppUtility();
	  SimpleDateFormat  ddMMMyy = new SimpleDateFormat("dd-MMM-yyyy");
	  boolean onlyforOrion=false;
	  boolean isRemarkGiven=false;
	  Service service=null;
	  
	  public void setInvoice(Long count)
	  {
		  logger.log(Level.SEVERE,"Invoice id "+ count);
	    invoiceentity = ((Invoice)ObjectifyService.ofy().load().type(Invoice.class).id(count.longValue()).now());
	    logger.log(Level.SEVERE,"Invoice id "+ invoiceentity.getCount());

	    if (invoiceentity.getCompanyId() == null) {
	      cust = 
	      
	        ((Customer)ObjectifyService.ofy().load().type(Customer.class).filter("count", Integer.valueOf(invoiceentity.getPersonInfo().getCount())).first().now());
	    } else {
	      cust = 
	      

	        ((Customer)ObjectifyService.ofy().load().type(Customer.class).filter("count", Integer.valueOf(invoiceentity.getPersonInfo().getCount())).filter("companyId", invoiceentity.getCompanyId()).first().now());
	    }
	    
	    if (invoiceentity.getCompanyId() == null) {
	      comp = ((Company)ObjectifyService.ofy().load().type(Company.class).first().now());
	    } else {
	      comp = 
	      
	        ((Company)ObjectifyService.ofy().load().type(Company.class).filter("companyId", invoiceentity.getCompanyId()).first().now());
	    }
	    if (invoiceentity.getCompanyId() != null) {
	      con = 
	      

	        ((Contract)ObjectifyService.ofy().load().type(Contract.class).filter("count", invoiceentity.getContractCount()).filter("companyId", invoiceentity.getCompanyId()).first().now());
	    } else {
	      con = 
	      
	        ((Contract)ObjectifyService.ofy().load().type(Contract.class).filter("count", invoiceentity.getContractCount()).first().now());
	    }
	    payTermsLis = con.getPaymentTermsList();
	    
	    if (invoiceentity.getCompanyId() == null) {
	      custbranchlist = ObjectifyService.ofy()
	        .load()
	        .type(CustomerBranchDetails.class)
	        .filter("cinfo.count", 
	        Integer.valueOf(invoiceentity.getPersonInfo().getCount())).list();
	    } else {
	      custbranchlist = ObjectifyService.ofy()
	        .load()
	        .type(CustomerBranchDetails.class)
	        .filter("cinfo.count", 
	        Integer.valueOf(invoiceentity.getPersonInfo().getCount()))
	        .filter("companyId", invoiceentity.getCompanyId()).list();
	    }
	    

	    System.out.println("Branch name======" + 
	      invoiceentity.getCustomerBranch());
	    if (invoiceentity.getCompanyId() == null) {
	      customerbranchlist = ObjectifyService.ofy()
	        .load()
	        .type(CustomerBranchDetails.class)
	        .filter("cinfo.count", 
	        Integer.valueOf(invoiceentity.getPersonInfo().getCount()))
	        .filter("buisnessUnitName", 
	        invoiceentity.getCustomerBranch()).list();
	    } else {
	      customerbranchlist = ObjectifyService.ofy()
	        .load()
	        .type(CustomerBranchDetails.class)
	        .filter("cinfo.count", 
	        Integer.valueOf(invoiceentity.getPersonInfo().getCount()))
	        .filter("buisnessUnitName", 
	        invoiceentity.getCustomerBranch())
	        .filter("companyId", invoiceentity.getCompanyId()).list();
	    }
	    System.out.println("Banch updated=====" + 
	      invoiceentity.getCustomerBranch());
	    
	    if (invoiceentity.getCompanyId() != null) {
	      comppayment = ofy().load().type(CompanyPayment.class).filter("paymentDefault", Boolean.valueOf(true)).filter("companyId", invoiceentity.getCompanyId()).first().now();

	      

//	        ((CompanyPayment)ObjectifyService.ofy().load().type(CompanyPayment.class).filter("paymentDefault", Boolean.valueOf(true)).filter("companyId", invoiceentity.getCompanyId()).first().now());
	    }

	    /**
	     * nidhi
	     * 9-06-2018
	     */
	    if(invoiceentity.getPaymentMode()!=null && invoiceentity.getPaymentMode().trim().length()>0){
			List<String> paymentDt = Arrays.asList(invoiceentity.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", invoiceentity.getCompanyId()).first()
						.now();
				
			}
		}

	    stateList = ObjectifyService.ofy().load().type(State.class)
	      .filter("companyId", invoiceentity.getCompanyId()).list();
	    

	    /**
	     * Date 2-5-2018
	     * By Jayshree
	     * des.to load the branch
	     */
	    
	    if(invoiceentity.getCompanyId()!=null){
	    	branch=ObjectifyService.ofy()
	    	        .load()
	    	        .type(Branch.class).filter("buisnessUnitName",invoiceentity.getBranch())
	    	        .filter("companyId", invoiceentity.getCompanyId()).list();
	    }
	    

	    if (invoiceentity.getCompanyId() != null) {
	      processConfig = 
	      

	        ((ProcessConfiguration)ObjectifyService.ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", Boolean.valueOf(true)).first().now());
	      if (processConfig != null) {
	        for (int k = 0; k < processConfig.getProcessList().size(); k++)
	        {
	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            upcflag = true;
	          }
	          

	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("OnlyForUniversal")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            UniversalFlag = Boolean.valueOf(true);
	          }
	          






	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("PrintMultipleCompanyNamesFromInvoiceGroup")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            multipleCompanyName = Boolean.valueOf(true);
	          }
	          


	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("printProductPremisesInPdf")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            printPremiseDetails = Boolean.valueOf(true);
	          }
	          


	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("PrintProductDescriptionOnPdf")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            productDescFlag = true;
	          }
	          


	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("OnlyForFriendsPestControl")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            onlyForFriendsPestControl = true;
	          }
	          

	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("AuthorityOnLeft")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            authOnLeft = true;
	          }
	          
	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("PrintAttnInPdf")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            printAttnInPdf = true;
	          }
	          
	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("ConsolidatePrice")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            consolidatePrice = true;
	          }
	          



	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("ServiceScheduleList")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            serviceSchedulelistFlag = true;
	          }
	          









	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("ActiveBranchEmailId")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            checkEmailId = true;
	          }
	          









	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("HeaderAtLeft")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            checkheaderLeft = true;
	          }
	          

	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("HeaderAtRight")) && 
	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	            checkheaderRight = true;
	          }
	          
	          
	          
	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("ShowServiceAddress")) && 
	  	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
	  	            serviceAddressFlag = true;
	  	      }
	          
	          if ((((ProcessTypeDetails)processConfig.getProcessList().get(k)).getProcessType().trim().equalsIgnoreCase("ShowContractDuration")) && 
		  	            (((ProcessTypeDetails)processConfig.getProcessList().get(k)).isStatus())) {
		  	       contractDurationFlag = true;
		  	  }
	          
	        }
	      }
	    }
	    /******3-4-2020 by Amol Added branchasCompany Process Configuration Raised by Rahul Tiwari for print the logo****/
if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
	               branchAsCompany=true;
	               
			logger.log(Level.SEVERE,"Process active --");
			if(invoiceentity !=null && invoiceentity.getBranch() != null && invoiceentity.getBranch().trim().length()>0){
				
				 branchDt = ofy().load().type(Branch.class).filter("companyId",invoiceentity.getCompanyId()).filter("buisnessUnitName", invoiceentity.getBranch()).first().now();

				if(branchDt !=null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){

					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", invoiceentity.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
						}
						
					}
					
					
				}
			}
		}
	    
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "ONLYFORORIONPESTSOLUTIONS", invoiceentity.getCompanyId())){
			onlyforOrion=true;
		}
	
		/*
		 * Ashwini Patil Date:25-07-2023 
		 * Orion wants pdf to fill entire page. if remark exist then only it gets displayed and print goes to next page.
		 * If remark does not exist then print properly fits into single page
		 * to manage spacings we need to know if remark exist or not in advance
		 */
		
		String remark = "";
	    if (onlyForFriendsPestControl) {
	    	remark = "1.If you are not satisfied with the treatment within 15 days of treatment, free treatment will be provided. \n2.You will receive two reminders for each of your treatments by call, email and by SMS. Our obligation limits to reminders only. \n3.It is essential to avail the treatment within the due dates to validate the warranty. \n4.Contract period cannot be extended for any reason. \n5.Once the due date is over the treatment cannot be carry forwarded to extend the contract period. \n6.It is mandatory to avail the treatment within a period of fifteen days before or after due date.Otherwise the treatment will be considered as lapsed.\nTHEREFOR PLEASE INSURE THE SCHEDULE MENTION HERE IS STRICTLY FOLLOWED";
	    }
	    else
	    {
	    	remark = invoiceentity.getComment().trim();
	    }
	    
	    if(remark!=null&&!remark.equals(""))
	    	isRemarkGiven=true;
	    else
	    	isRemarkGiven=false;
	    
	    
	    if(isRemarkGiven)
	    	noOfLines=8; //old 14 changed on 22-12-2023. Due to qr code size pdf going to next page.
		  
	    service=ofy().load().type(Service.class)
				.filter("companyId", invoiceentity.getCompanyId())
				.filter("billingCount",invoiceentity.getSalesOrderProducts()
						.get(0).getBiilingId()).first().now();
	  }
	  




	  public void createPdf(String preprintStatus)
	  {
		  
		if(con.isServiceWiseBilling()){
			serviceWiseBilling=true;
		}
		
	    sdf = new SimpleDateFormat("dd-MMM-yyyy");
	    TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	    
	    
	    /**
	     * Date 2-5-2018
	     * By jayshree
	     * Des.add new date format to print only month
	     */
	    String strDateFormat = "M";
	    monthformat = new SimpleDateFormat(strDateFormat);
	    TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	    monthformat.setTimeZone(TimeZone.getTimeZone("IST"));
	    
	    
	    
	     String yerDateFormat = "yy";
	     yearformat = new SimpleDateFormat(yerDateFormat);
	     TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		 monthformat.setTimeZone(TimeZone.getTimeZone("IST"));
	    
	    //End
	    
	    System.out.println("iuhsuvybtyuj" + preprintStatus);
	    



	    double discount = 0;double roundOff = 0;
	    discount = invoiceentity.getDiscountAmt();
	    roundOff = invoiceentity.getDiscount();
	    if (discount != 0) {
	      noOfLines -= 1;
	    }
	    if (roundOff != 0) {
	      noOfLines -= 1;
	    }
	    if (invoiceentity.getOtherCharges().size() > 0) {
	      noOfLines -= 1;
	    }
	    
	    if(onlyforOrion)
	    	createInvoiceDetailsNew(preprintStatus);//Ashwini Patil Date:23-06-2023
	    else
	    	createInvoiceDetails(preprintStatus);
	    
	    if (con.isContractRate()) {
	      createProductDetailsForRate();
	    } else {
	      createProductDetails();
	    }
	    
	    if (con.isContractRate()) {
	      createProductDetailsValForRate();
	    } else if(con.isServiceWiseBilling()){
	      createServiceWiseBilling();
	    }else {
	      createProductDetailsVal();
	    }
	    createFooterAmountPart();
	    if (discount != 0) {
	      createFooterDisCountAfterPart(discount);
	    }
	    if (invoiceentity.getOtherCharges().size() > 0) {
	      createFooterOtherChargesPart2();
	    }
	    createFooterTaxPart();
	    if (roundOff != 0) {
	      createFooterDisCountBeforeNetPayPart(roundOff);
	    }
	    createFooterAmountInWords_NetPayPart();
	    createTermsAndCondition();
	    
	    createFooterLastPart(preprintStatus);
	    CreateOrionFooter();
	    createProductDescription();
	    

	    if (serviceSchedulelistFlag) {
	      createServiceScheduleList();
	    }
	    
	    
	  }
	  





	  private void createServiceWiseBilling() {
		  logger.log(Level.SEVERE,"In createServiceWiseBilling");
		    double rateAmountProd = 0;double amountAmountProd = 0;double discAmountProd = 0;double totalAssAmountProd = 0;
		    HashSet<String> prodIdSet=new HashSet<String>();
		    
		    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++){
				prodIdSet.add(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim());
				
				rateAmountProd = rateAmountProd+ invoiceentity.getSalesOrderProducts().get(i).getPrice();
				amountAmountProd = amountAmountProd+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
						* invoiceentity.getSalesOrderProducts().get(i).getQuantity();
				discAmountProd = discAmountProd+ invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
	
				double taxValue = 0;
				if (invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount() != 0) {
					taxValue = invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
				} else {
					taxValue = invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
				}
				totalAssAmountProd += taxValue;
		    }
		    
		    int firstBreakPoint = 5;
		    float blankLines = 0.0F;
		    
		    
		    
		    PdfPTable productTable ;
		    
		    if(onlyforOrion) {
		    	productTable= new PdfPTable(7);
		    	float[] column7Width = {0.8F,3.0F, 1.2F, 1.0F, 1.0F, 1.0F, 2.0F };
		    	productTable.setWidthPercentage(100.0F);
			    try {
			      productTable.setWidths(column7Width);
			    } catch (DocumentException e) {
			      e.printStackTrace();
			    }
		    }else {
		    	productTable= new PdfPTable(8);
		    	 productTable.setWidthPercentage(100.0F);
		 	    try {
		 	      productTable.setWidths(column8SerProdCollonWidth);
		 	    }
		 	    catch (DocumentException e) {
		 	      e.printStackTrace();
		 	    }
		    }
		    
		    Phrase blankPhrase = new Phrase(" ", font10);
		    PdfPCell blankPhraseCellleft = new PdfPCell(blankPhrase);
//	      blankPhraseCell.setBorder(0);
		    blankPhraseCellleft.setHorizontalAlignment(1);
		    blankPhraseCellleft.setBorderWidthBottom(0);
		    blankPhraseCellleft.setBorderWidthTop(0);
		    blankPhraseCellleft.setBorderWidthRight(0);
//		    blankPhraseCellleft.setBorderWidthLeft(0);
	        PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
//	        blankPhraseCell.setBorder(0);
	        blankPhraseCell.setHorizontalAlignment(1);
	        blankPhraseCell.setBorderWidthBottom(0);
	        blankPhraseCell.setBorderWidthTop(0);
	        blankPhraseCell.setBorderWidthRight(0);
	        
	        PdfPCell blankPhraseRightCell = new PdfPCell(blankPhrase);
//	        blankPhraseRightCell.setBorder(0);
	        blankPhraseRightCell.setHorizontalAlignment(1);
	        blankPhraseRightCell.setBorderWidthBottom(0);
	        blankPhraseRightCell.setBorderWidthTop(0);
//	        blankPhraseRightCell.setBorderWidthRight(0);
	        
		    if(onlyforOrion){	    	
//		        productTable.addCell(blankPhraseCellleft);
//		        productTable.addCell(blankPhraseCell);
//		        productTable.addCell(blankPhraseCell);
//		        productTable.addCell(blankPhraseCell);
//		        productTable.addCell(blankPhraseCell);
//		        productTable.addCell(blankPhraseCell);
//		        productTable.addCell(blankPhraseRightCell);	   
//		        noOfLines -= 1;
		    }
		    
		    int countToBeDeducted = 0;
		    
		    ArrayList<String> hashListSet=new ArrayList<String>(prodIdSet);
		    logger.log(Level.SEVERE,"hashListSet"+hashListSet.size());
		    logger.log(Level.SEVERE,"prodIdSet"+prodIdSet.size());
		    
		    
		    /**
		     * @author Anil,Date : 03-04-2019
		     * product amount was mapped against wrong product
		     * this was happening because in inner loop of invoice line item was called with outer loop indent i
		     * in place of 'j' , 'i' was passed
		     * also in another inner loop of contract instead of 'm' ,'j' is called to retrieve value
		     */
			for (int i = 0; i < hashListSet.size(); i++) {
				
				int noOfService=0;
				double rateValue=0;
				double discValue=0;
				double taxValue = 0;
				PdfPCell serviceNameCell = null,srNoCell=null,hsnCodeCell=null,startDate_endDateCell=null,rate=null,discCell=null;
				if (invoiceentity.getSalesOrderProducts().get(i).getProdName().trim().length() > 42) {
					noOfLines -= 1;
				}
				noOfLines -= 1;
				for (int j = 0; j < invoiceentity.getSalesOrderProducts().size(); j++) {
					if (hashListSet.get(i).trim().equalsIgnoreCase(invoiceentity.getSalesOrderProducts().get(j).getProdCode())) {
//						if (noOfLines == 0) {
//							prouductCount = i;
//							break;
//						}
						countToBeDeducted++;
//						noOfLines -= 1;
	
						int srNoVal = i + 1;
	//					Phrase srNo = new Phrase(srNoVal + "", font6);
	//					if(srNoCell!=null){
	//						srNoCell = new PdfPCell(srNo);
	//						srNoCell.setHorizontalAlignment(1);
	//					}
						System.out.println("getProdName().trim().length()"+ invoiceentity.getSalesOrderProducts().get(j).getProdName().trim().length());
	
						Phrase serviceName = new Phrase(invoiceentity.getSalesOrderProducts().get(j).getProdName().trim(), font6);
						if(serviceNameCell==null){
							serviceNameCell = new PdfPCell(serviceName);
						}
						//No 1
						
	
						/**
						 * No of service
						 */
						noOfService=noOfService+1;
						
						
						Phrase hsnCode = null;
						if (invoiceentity.getSalesOrderProducts().get(j).getHsnCode() != null
								&& !invoiceentity.getSalesOrderProducts().get(j).getHsnCode().equals("")) {
							hsnCode = new Phrase(invoiceentity.getSalesOrderProducts().get(j).getHsnCode().trim(), font6);
						} else {
							ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", comp.getCompanyId())
									.filter("productCode",invoiceentity.getSalesOrderProducts().get(j).getProdCode().trim())
									.first().now();
							if (serviceProduct.getHsnNumber() != null&& serviceProduct.getHsnNumber().equalsIgnoreCase("")) {
								hsnCode = new Phrase(serviceProduct.getHsnNumber(),font6);
							} else {
								hsnCode = new Phrase("", font6);
							}
						}
						
						if(hsnCodeCell==null){
							hsnCodeCell = new PdfPCell(hsnCode);
							hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						}
	
						String startDateStr = "", endDateStr = "";
						for (int m = 0; m < con.getItems().size(); m++) {
	
							SimpleDateFormat simpleDateFmt = new SimpleDateFormat("dd/MM/yyyy");
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
							try {
								if ((invoiceentity.getSalesOrderProducts().get(j).getPrduct().getCount() == con.getItems().get(m).getPrduct().getCount())
										&& (invoiceentity.getSalesOrderProducts().get(j).getOrderDuration() == con.getItems().get(m).getDuration())) {
									if (con.getItems().get(m).getEndDate() != null) {
										startDateStr = simpleDateFmt.format(con.getItems().get(m).getStartDate());
										endDateStr = simpleDateFmt.format(con.getItems().get(m).getEndDate());
									} else {
										Calendar c = Calendar.getInstance();
										c.setTime(con.getItems().get(m).getStartDate());
										c.add(Calendar.DATE, con.getItems().get(m).getDuration());
										Date endDt = c.getTime();
										startDateStr = simpleDateFmt.format(con.getItems().get(m).getStartDate());
										endDateStr = simpleDateFmt.format(endDt);
									}
								}
	
							} catch (Exception e) {
								System.out.println(e);
							}
						}
	
						/**
						 * Date :6/1/2018 By :Manisha Description : When no. of
						 * services is 1 then enddate is increased by 1 day..!!!
						 */
	
						Boolean configurationFlag = false;
	
						processConfig = ofy().load().type(ProcessConfiguration.class)
								.filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice")
								.filter("configStatus", true).first().now();
	
						if (processConfig != null) {
							for (int k = 0; k < processConfig.getProcessList().size(); k++) {
								if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("SingleServiceDuration")
										&& processConfig.getProcessList().get(k).isStatus() == true) {
									configurationFlag = true;
									break;
								}
							}
						}
	
						if (configurationFlag == true) {
							for (int k = 0; k < invoiceentity.getSalesOrderProducts().size(); k++) {
								if (!invoiceentity.getSalesOrderProducts().get(j).getProdName().trim().equalsIgnoreCase("")
										&& invoiceentity.getSalesOrderProducts().get(j).getOrderServices() == 1) {
									isSingleService = true;
								}
							}
						}
	
						if (isSingleService) {
							Calendar c = Calendar.getInstance();
							SimpleDateFormat simpleDateFmt = new SimpleDateFormat("dd/MM/yyyy");
							Date date = null;
							try {
								date = simpleDateFmt.parse(startDateStr);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							c.setTime(date);
							c.add(5, 1);
							Date endDt = c.getTime();
							endDateStr = simpleDateFmt.format(endDt);
							
							Phrase startDate_endDate = new Phrase( " - " , font6);  //Added by Ashwini
							startDate_endDateCell = new PdfPCell(startDate_endDate);
							startDate_endDateCell.setHorizontalAlignment(1);
						} else {
							Phrase startDate_endDate = new Phrase( " - " , font6);  //Added by Ashwini
							startDate_endDateCell = new PdfPCell(startDate_endDate);
							startDate_endDateCell.setHorizontalAlignment(1);
						}
						/**
						 * Rate
						 */
	
						double amountValue = invoiceentity.getSalesOrderProducts().get(j).getPrice()
								* invoiceentity.getSalesOrderProducts().get(j).getQuantity();
						double disPercentTotalAmount = 0;
						double disConTotalAmount = 0;
						if ((invoiceentity.getSalesOrderProducts().get(j).getProdPercDiscount() == null)
								|| (invoiceentity.getSalesOrderProducts().get(j).getProdPercDiscount().doubleValue() == 0)) {
							disPercentTotalAmount = 0;
						} else {
							disPercentTotalAmount = getPercentAmount(invoiceentity.getSalesOrderProducts().get(j), false);
						}
	
						if (invoiceentity.getSalesOrderProducts().get(j).getDiscountAmt() == 0) {
							disConTotalAmount = 0;
						} else {
							disConTotalAmount = invoiceentity.getSalesOrderProducts().get(j).getDiscountAmt();
						}
	
						totalAmount += amountValue;
						
						discValue=discValue+invoiceentity.getSalesOrderProducts().get(j).getFlatDiscount();
						/**
						 * Date 18-09-2018 by Vijay
						 * Des :- Bug - wrong base billed amount   					
						 */
						if (invoiceentity.getSalesOrderProducts().get(j).getBasePaymentAmount() != 0
								&& invoiceentity.getSalesOrderProducts().get(j).getPaymentPercent() != 0) {
							taxValue = taxValue+invoiceentity.getSalesOrderProducts().get(j).getBasePaymentAmount();
							logger.log(Level.SEVERE,"base billing amount == $$ ="+invoiceentity.getSalesOrderProducts().get(j).getBaseBillingAmount());
	
						} else {
							// taxableValue = new Phrase(df.format(invoiceentity
							// .getSalesOrderProducts().get(i).getBaseBillingAmount())
							// + "", font6);
							taxValue =taxValue+ invoiceentity.getSalesOrderProducts().get(j).getBaseBillingAmount();
							logger.log(Level.SEVERE,"taxValue"+taxValue);
							logger.log(Level.SEVERE,"base billing amount =="+invoiceentity.getSalesOrderProducts().get(j).getBaseBillingAmount());
	
						}
						
											
					}
				}
	//			int srNoVal = i + 1;
				Phrase srNo = new Phrase((i + 1) + "", font6);
	//			if(srNoCell!=null){
					srNoCell = new PdfPCell(srNo);
					srNoCell.setHorizontalAlignment(1);
	//			}

//					srNoCell.setBorder(0);
//					srNoCell.setBorderWidthRight(1);
					srNoCell.setBorderWidthBottom(0);
					srNoCell.setBorderWidthTop(0);
					srNoCell.setBorderWidthRight(0);
//					srNoCell.setBorderWidthLeft(0);
					
				productTable.addCell(srNoCell);//1

//				serviceNameCell.setBorder(0);
//				serviceNameCell.setBorderWidthRight(1);
				serviceNameCell.setBorderWidthBottom(0);
				serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.setBorderWidthRight(0);
				productTable.addCell(serviceNameCell);//2
				Phrase phraseNoOfService=new Phrase(noOfService+"",font6);
				PdfPCell noOfServicesCell = new PdfPCell(phraseNoOfService);
				noOfServicesCell.setHorizontalAlignment(1);

//				noOfServicesCell.setBorder(0);
//				noOfServicesCell.setBorderWidthRight(1);
				noOfServicesCell.setBorderWidthBottom(0);
				noOfServicesCell.setBorderWidthTop(0);
				noOfServicesCell.setBorderWidthRight(0);
				productTable.addCell(noOfServicesCell);//3

//				hsnCodeCell.setBorder(0);
//				hsnCodeCell.setBorderWidthRight(1);
				hsnCodeCell.setBorderWidthBottom(0);
				hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.setBorderWidthRight(0);
				productTable.addCell(hsnCodeCell);//4
				
				if(!onlyforOrion) {

//					startDate_endDateCell.setBorder(0);
//					startDate_endDateCell.setBorderWidthRight(1);
					startDate_endDateCell.setBorderWidthBottom(0);
					startDate_endDateCell.setBorderWidthTop(0);
					startDate_endDateCell.setBorderWidthRight(0);
					productTable.addCell(startDate_endDateCell);//5
				}
	
				Phrase blankph = new Phrase("", font8);
				PdfPCell blnkCell = new PdfPCell(blankph);
				blnkCell.setBorderWidthTop(0);
				blnkCell.setBorderWidthRight(0);
//				blnkCell.setBorderWidthLeft(0);
				blnkCell.setBorderWidthBottom(0);
	
				productTable.addCell(blnkCell);//rate should be blank//6
				Phrase discPhrase=new Phrase(df.format(discValue)+"",font6);
				discCell = new PdfPCell(discPhrase);

//				discCell.setBorder(0);
//				discCell.setBorderWidthRight(1);
				discCell.setBorderWidthBottom(0);
				discCell.setBorderWidthTop(0);
				discCell.setBorderWidthRight(0);
				productTable.addCell(discCell);//7
				PdfPCell taxableValueCell = null;/*
				 * = new
				 * PdfPCell(taxableValue);
				 * taxableValueCell
				 * .setHorizontalAlignment
				 * (Element.ALIGN_RIGHT);
				 */
				logger.log(Level.SEVERE,"taxValue at end"+taxValue);
				Phrase taxableValue = new Phrase(df.format(taxValue), font6);
				taxableValueCell = new PdfPCell(taxableValue);
				taxableValueCell.setHorizontalAlignment(2);

//				taxableValueCell.setBorder(0);
				taxableValueCell.setBorderWidthBottom(0);
				taxableValueCell.setBorderWidthTop(0);
//				taxableValueCell.setBorderWidthRight(0);
				productTable.addCell(taxableValueCell);//8
	
				String premisesVal = "";
				for (int k = 0; k < con.getItems().size(); k++) {
					if (hashListSet.get(i).trim().equalsIgnoreCase(con.getItems().get(k)
							.getPrduct().getProductCode())) {
	//					if (invoiceentity.getSalesOrderProducts().get(j)
	//							.getOrderDuration() == con.getItems()
	//							.get(k).getDuration()) {
							premisesVal = con.getItems().get(k)
									.getPremisesDetails();
	//					}
					}
				}
				
				System.out.println("noOfLines in product" + noOfLines);
				if ((premisesVal != null) && (printPremiseDetails)
						&& (!premisesVal.equals(""))) {
					noOfLines -= 1;
					Phrase blankValPhrs = new Phrase(" ", font8);
					PdfPCell premiseCell = new PdfPCell(blankValPhrs);
					premiseCell.setColspan(1);
					premiseCell.setBorderWidthBottom(0);
					productTable.addCell(premiseCell);
	
					Phrase premisesValPhrs = new Phrase(
							"Premise Details : " + premisesVal, font8);
					premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					premiseCell.setBorderWidthBottom(0);
					productTable.addCell(premiseCell);
				}
	
			}
		    int remainingLines = 0;
		    System.out.println("noOfLines outside" + noOfLines);
		    System.out.println("prouductCount" + prouductCount);
		    
		    if (noOfLines != 0) {
		      remainingLines = 16 - (16 - noOfLines);
		    }
		    System.out.println("remainingLines" + remainingLines);
		    
		    if (remainingLines != 0) {
		      for (int i = 0; i < remainingLines; i++) {
		        System.out.println("i::::" + i);
//		        Phrase blankPhrase = new Phrase(" ", font10);
//		        PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
//		        blankPhraseCell.setBorder(0);
//		        blankPhraseCell.setHorizontalAlignment(1);
//		        blankPhraseCell.setBorderWidthRight(1);		        
		        productTable.addCell(blankPhraseCellleft);
		        productTable.addCell(blankPhraseCell);
		        productTable.addCell(blankPhraseCell);
		        productTable.addCell(blankPhraseCell);
		        productTable.addCell(blankPhraseCell);
		        productTable.addCell(blankPhraseCell);
		        productTable.addCell(blankPhraseRightCell);
		      }
		    }
		    PdfPCell productTableCell = null;
		    if (noOfLines == 0) {
		      Phrase my = new Phrase("Please Refer Annexure For More Details", font9bold);
		      productTableCell = new PdfPCell(my);
		    }else {
		      productTableCell = new PdfPCell(blankCell);
		    }
		    productTableCell.setBorder(0);
		    
		    PdfPTable tab = new PdfPTable(1);
		    tab.setWidthPercentage(100.0F);
		    tab.addCell(productTableCell);
		    tab.setSpacingAfter(blankLines);


		    PdfPCell tab1 = new PdfPCell(productTable);
		    tab1.setBorder(0);
		    PdfPCell tab2 = new PdfPCell(tab);

		    PdfPTable mainTable = new PdfPTable(1);
		    mainTable.setWidthPercentage(100.0F);
		    mainTable.addCell(tab1);
//		    mainTable.addCell(tab2);
		    try{
		      document.add(mainTable);
		    }
		    catch (DocumentException e) {
		      e.printStackTrace();
		    }
		    
	}

	private void createServiceScheduleList()
	  {
	    Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
	    try {
	      document.add(nextpage);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    if (upcflag) {
	      Paragraph blank = new Paragraph();
	      blank.add(Chunk.NEWLINE);
	      try {
	        document.add(blank);
	        document.add(blank);
	        document.add(blank);
	        document.add(blank);
	        document.add(blank);
	        document.add(blank);
	      }
	      catch (DocumentException e) {
	        e.printStackTrace();
	      }
	    }
	    
	    String terms = "Service Details :";
	    Phrase term = new Phrase(terms, font12bold);
	    
	    Paragraph para1 = new Paragraph();
	    para1.add(term);
	    para1.setAlignment(1);
	    
	    String decsInfo = "";
	    
	    if (invoiceentity.getComment() != null) {
	      decsInfo = invoiceentity.getComment();
	    }
	    
	    Phrase desphase = new Phrase(decsInfo, font8);
	    Paragraph para2 = new Paragraph();
	    para2.add(desphase);
	    try
	    {
	      document.add(para1);
	      document.add(para2);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    




	    String desc = "";
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {
	      ServiceProduct sup = 
	      




	        (ServiceProduct)ObjectifyService.ofy().load().type(ServiceProduct.class).filter("companyId", invoiceentity.getCompanyId()).filter("productCode", invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim()).first().now();
	      
	      Phrase service = new Phrase("Service Product :" + 
	        sup.getProductName(), font12bold);
	      
	      Paragraph servicePara = new Paragraph();
	      servicePara.add(service);
	      
	      desc = sup.getComment();
	      Phrase prodDesc = new Phrase(desc, font8);
	      Paragraph parades = new Paragraph(prodDesc);
	      
	      PdfPTable serviceTable = new PdfPTable(4);
	      serviceTable.setWidthPercentage(100.0F);
	      
	      Phrase serno = new Phrase("Service No", font6bold);
	      Phrase serDate = new Phrase("Service Date", font6bold);
	      Phrase serStatus = new Phrase("Service Status", font6bold);
	      Phrase custBranch = new Phrase("Customer Branch", font6bold);
	      
	      PdfPCell sernocell = new PdfPCell(serno);
	      PdfPCell serDatecell = new PdfPCell(serDate);
	      PdfPCell serStatuscell = new PdfPCell(serStatus);
	      PdfPCell custBranchcell = new PdfPCell(custBranch);
	      
	      serviceTable.addCell(sernocell);
	      serviceTable.addCell(serDatecell);
	      serviceTable.addCell(serStatuscell);
	      serviceTable.addCell(custBranchcell);
	      serviceTable.setSpacingBefore(10.0F);
	      
	      Phrase chunk = null;
	      
	      System.out.println("service Schedule list size" + 
	        con.getServiceScheduleList().size());
	      
	      for (int k = 0; k < con.getServiceScheduleList().size(); k++)
	      {
	        if (sup.getCount() == ((ServiceSchedule)con.getServiceScheduleList().get(k))
	          .getScheduleProdId()) {
	          chunk = new Phrase(((ServiceSchedule)con.getServiceScheduleList().get(k))
	            .getScheduleServiceNo(),"", 
	            font8);
	          PdfPCell srno = new PdfPCell(chunk);
	          
	          String serviceDt = sdf.format(
	            ((ServiceSchedule)con.getServiceScheduleList().get(k)).getScheduleServiceDate());
	          chunk = new Phrase(serviceDt, font8);
	          PdfPCell srDate = new PdfPCell(chunk);
	          
	          chunk = new Phrase("Scheduled", font8);
	          PdfPCell srStatus = new PdfPCell(chunk);
	          
	          chunk = new Phrase(((ServiceSchedule)con.getServiceScheduleList().get(k))
	            .getScheduleProBranch(), font8);
	          PdfPCell srBranch = new PdfPCell(chunk);
	          
	          serviceTable.addCell(srno);
	          serviceTable.addCell(srDate);
	          serviceTable.addCell(srStatus);
	          serviceTable.addCell(srBranch);
	        }
	      }
	      try
	      {
	        document.add(servicePara);
	        document.add(parades);
	        document.add(serviceTable);
	        document.add(Chunk.NEWLINE);
	        document.add(Chunk.NEWLINE);
	      } catch (DocumentException e) {
	        e.printStackTrace();
	      }
	    }
	  }
	  





	  private void createProductDetailsValForRate()
	  {
		  logger.log(Level.SEVERE,"In createProductDetailsValForRate");
	    double rateAmountProd = 0;double amountAmountProd = 0;double discAmountProd = 0;double totalAssAmountProd = 0;
	    
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {

	      rateAmountProd = rateAmountProd + invoiceentity.getSalesOrderProducts().get(i).getPrice();
	      

	      amountAmountProd = amountAmountProd + invoiceentity.getSalesOrderProducts().get(i).getPrice() * 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getQuantity();
	      

	      discAmountProd = discAmountProd + invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
	      
	      double taxValue = 0;
	      if (invoiceentity.getSalesOrderProducts().get(i)
	        .getBasePaymentAmount() != 0) {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
	      }
	      else {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
	      }
	      totalAssAmountProd += taxValue;
	    }
	    
	    int firstBreakPoint = 5;
	    float blankLines = 0.0F;
	    
	    PdfPTable productTable = new PdfPTable(8);
	    productTable.setWidthPercentage(100.0F);
	    try {
	      productTable.setWidths(column8ProdCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    int countToBeDeducted = 0;
	    
	    Phrase blankPhrase = new Phrase(" ", font10);
        PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
//        blankPhraseCell.setBorder(0);
        blankPhraseCell.setBorderWidthTop(0);
        blankPhraseCell.setBorderWidthBottom(0);
        blankPhraseCell.setBorderWidthRight(0);
        blankPhraseCell.setHorizontalAlignment(1);
        
        PdfPCell blankPhraseRightCell = new PdfPCell(blankPhrase);
//        blankPhraseRightCell.setBorder(0);
        blankPhraseRightCell.setBorderWidthTop(0);
        blankPhraseRightCell.setBorderWidthBottom(0);
        blankPhraseRightCell.setHorizontalAlignment(1);
	    if(onlyforOrion){	    	
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);	 
//	        productTable.addCell(blankPhraseRightCell);	
//	        noOfLines -= 1;
	    }
	    
	    
	    
	    
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {
	    	if(!invoiceentity.isConsolidatePrice()){
				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
						invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
					continue;
				}
			}
	      if (noOfLines == 0) {
	        prouductCount = i;
	        break;
	      }
	      countToBeDeducted++;
	      noOfLines -= 1;
	      
	      int srNoVal = i + 1;
	      Phrase srNo = new Phrase(srNoVal+"", font6);
	      PdfPCell srNoCell = new PdfPCell(srNo);
	      srNoCell.setHorizontalAlignment(1);

//	      srNoCell.setBorder(0);
	      srNoCell.setBorderWidthRight(0);
	      srNoCell.setBorderWidthTop(0);
	      srNoCell.setBorderWidthBottom(0);
	      productTable.addCell(srNoCell);
	      System.out.println("getProdName().trim().length()" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getProdName().trim().length());
	      
	      if (invoiceentity.getSalesOrderProducts().get(i).getProdName().trim().length() > 42) {
	        noOfLines -= 1;
	      }
	      Phrase serviceName = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getProdName().trim(), font6);
	      PdfPCell serviceNameCell = new PdfPCell(serviceName);

//	      serviceNameCell.setBorder(0);
	      serviceNameCell.setBorderWidthRight(0);
	      serviceNameCell.setBorderWidthTop(0);
	      serviceNameCell.setBorderWidthBottom(0);
	      productTable.addCell(serviceNameCell);
	      
	      Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getHsnCode().equals("")) {
				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font6);
			} else {
				ServiceProduct serviceProduct = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("productCode",
								invoiceentity.getSalesOrderProducts().get(i)
								.getProdCode().trim()).first().now();
				if (serviceProduct.getHsnNumber() != null) {
					hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
				} else {
					hsnCode = new Phrase("", font6);
				}
			}

	    
	      PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
	      hsnCodeCell.setHorizontalAlignment(1);

//	      hsnCodeCell.setBorder(0);
	      hsnCodeCell.setBorderWidthRight(0);
	      hsnCodeCell.setBorderWidthTop(0);
	      hsnCodeCell.setBorderWidthBottom(0);
	      productTable.addCell(hsnCodeCell);
	      String startDateStr = "";String endDateStr = "";
	      for (int j = 0; j < con.getItems().size(); j++)
	      {
	        System.out.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()" + 
	          invoiceentity.getSalesOrderProducts().get(i)
	          .getPrduct().getCount());
	        System.out
	          .println("con.getItems().get(j).getPrduct().getCount()" + 
	          con.getItems().get(j).getPrduct().getCount());
	        System.out
	          .println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()" + 
	          invoiceentity.getSalesOrderProducts().get(i)
	          .getOrderDuration());
	        System.out
	          .println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()" + 
	          invoiceentity.getSalesOrderProducts().get(i)
	          .getOrderDuration());
	        System.out.println("con.getItems().get(j).getDuration()" + 
	          con.getItems().get(j).getDuration());
	        
	        SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
	          "dd/MM/yyyy");
	        TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	        simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
	        if (invoiceentity.getSalesOrderProducts().get(i).getPrduct()
	          .getCount() == con.getItems().get(j).getPrduct()
	          .getCount()) {
	          if (invoiceentity.getSalesOrderProducts().get(i)
	            .getOrderDuration() == con.getItems().get(j)
	            .getDuration()) {
	            if (con.getItems().get(j).getEndDate() != null) {
	              startDateStr = simpleDateFmt.format(
	                con.getItems().get(j).getStartDate());
	              endDateStr = simpleDateFmt.format(con.getItems().get(j)
	                .getEndDate());
	            } else {
	              Calendar c = Calendar.getInstance();
	              c.setTime(con.getItems().get(j).getStartDate());
	              c.add(5, con.getItems().get(j)
	                .getDuration());
	              Date endDt = c.getTime();
	              startDateStr = simpleDateFmt.format(
	                con.getItems().get(j).getStartDate());
	              endDateStr = simpleDateFmt.format(endDt);
	            }
	          }
	        }
	      }
	      Phrase startDate_endDate = new Phrase(startDateStr + " - " + 
	        endDateStr, font6);
	      
	      PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
	      startDate_endDateCell.setHorizontalAlignment(1);
	      

//	      Phrase qty = new Phrase(
//	        invoiceentity.getSalesOrderProducts().get(i).getArea(), 
//	        font6);
	      //Ashwini Patil Date:29-12-2023 in case of rate contract invoice quantity should be printed from Quantity column visible on invoice details list
	      String qtyVal=invoiceentity.getSalesOrderProducts().get(i).getArea();
	      if(service!=null) {
	    	  qtyVal=service.getQuantity()+"";
	      }
	      Phrase qty = new Phrase(qtyVal,font6);
	      
	      PdfPCell qtyCell = new PdfPCell(qty);
	      qtyCell.setHorizontalAlignment(1);

//	      qtyCell.setBorder(0);
	      qtyCell.setBorderWidthRight(0);
	      qtyCell.setBorderWidthTop(0);
	      qtyCell.setBorderWidthBottom(0);
	      productTable.addCell(qtyCell);
	      
	      Phrase uom = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim(), font6);
	      PdfPCell uomCell = new PdfPCell(uom);
	      uomCell.setHorizontalAlignment(1);

//	      uomCell.setBorder(0);
	      uomCell.setBorderWidthRight(0);
	      uomCell.setBorderWidthTop(0);
	      uomCell.setBorderWidthBottom(0);
	      productTable.addCell(uomCell);
	      
	      Phrase rate = null;
	      PdfPCell rateCell = null;
	      if (consolidatePrice) {
	        if (i == 0) {
	          rate = new Phrase(df.format(rateAmountProd), font6);
	          
	          rateCell = new PdfPCell(rate);
	          rateCell.setBorderWidthBottom(0.0F);
	          rateCell.setHorizontalAlignment(2);
	        } else {
	          rate = new Phrase(" ", font6);
	          
	          rateCell = new PdfPCell(rate);
	          rateCell.setHorizontalAlignment(2);
	          
	          rateCell.setBorderWidthTop(0.0F);
	        }
	      }
	      else {
	        rate = new Phrase(df.format(
	          invoiceentity.getSalesOrderProducts().get(i).getPrice()), 
	          font6);
	        
	        rateCell = new PdfPCell(rate);
	        
	        rateCell.setHorizontalAlignment(2);
	      }

//	      rateCell.setBorder(0);
	      rateCell.setBorderWidthRight(0);
	      rateCell.setBorderWidthTop(0);
	      rateCell.setBorderWidthBottom(0);
	      productTable.addCell(rateCell);
	      










	      double amountValue = invoiceentity.getSalesOrderProducts().get(i)
	        .getPrice() * 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getQuantity();
	      double disPercentTotalAmount = 0;double disConTotalAmount = 0;
	      if ((invoiceentity.getSalesOrderProducts().get(i)
	        .getProdPercDiscount() == null) || 
	        (invoiceentity.getSalesOrderProducts().get(i)
	        .getProdPercDiscount().doubleValue() == 0)) {
	        disPercentTotalAmount = 0;
	      } else {
	        disPercentTotalAmount = getPercentAmount(
	          invoiceentity.getSalesOrderProducts().get(i), false);
	      }
	      
	      if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
	        disConTotalAmount = 0;
	      } else {
	        disConTotalAmount = 
	          invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt();
	      }
	      
	      totalAmount += amountValue;
	      








	      Phrase disc = null;
	      PdfPCell discCell = null;
	      if (consolidatePrice) {
	        if (i == 0) {
	          disc = new Phrase(df.format(discAmountProd), font6);
	          
	          discCell = new PdfPCell(disc);
	          discCell.setBorderWidthBottom(0.0F);
	          discCell.setHorizontalAlignment(2);
	        } else {
	          disc = new Phrase(" ", font6);
	          
	          discCell = new PdfPCell(disc);
	          discCell.setHorizontalAlignment(2);
	          discCell.setBorderWidthTop(0.0F);
	        }
	      } else {
	        disc = new Phrase(df.format(
	          invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()), 
	          font6);
	        
	        discCell = new PdfPCell(disc);
	        discCell.setHorizontalAlignment(2);
	      }

//	      discCell.setBorder(0);
	      discCell.setBorderWidthRight(0);
	      discCell.setBorderWidthTop(0);
	      discCell.setBorderWidthBottom(0);
	      productTable.addCell(discCell);
	      

	      double taxValue = 0;
	      if (invoiceentity.getSalesOrderProducts().get(i)
	        .getBasePaymentAmount() != 0) {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
	      } else {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
	      }
	      
	      PdfPCell taxableValueCell;
	      if (consolidatePrice) {
	        if (i == 0) {
	          Phrase taxableValue = new Phrase(df.format(totalAssAmountProd), 
	            font6);
	          taxableValueCell = new PdfPCell(taxableValue);
	          taxableValueCell
	            .setHorizontalAlignment(2);
	          taxableValueCell.setBorderWidthBottom(0.0F);
	        } else {
	          Phrase taxableValue = new Phrase(" ", font6);
	         taxableValueCell = new PdfPCell(taxableValue);
	          taxableValueCell
	            .setHorizontalAlignment(2);
	          
	          taxableValueCell.setBorderWidthTop(0.0F);
	        }
	      } else {
	        Phrase taxableValue = new Phrase(df.format(taxValue), font6);
	        taxableValueCell = new PdfPCell(taxableValue);
	        taxableValueCell.setHorizontalAlignment(2);
	      }

//	      taxableValueCell.setBorder(0);
	      taxableValueCell.setBorderWidthTop(0);
	      taxableValueCell.setBorderWidthBottom(0);
	      productTable.addCell(taxableValueCell);
	      




	      logger.log(Level.SEVERE, "VAT TAX ::::Config Name" + 
	        invoiceentity.getSalesOrderProducts().get(i).getVatTax()
	        .getTaxConfigName() + 
	        "VAT TAx:::Tax Name" + 
	        invoiceentity.getSalesOrderProducts().get(i).getVatTax()
	        .getTaxName() + 
	        "Ser TAX ::::Config Name" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getServiceTax().getTaxConfigName() + 
	        "Ser TAx:::Tax Name" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getServiceTax().getTaxName());
	      logger.log(Level.SEVERE, "VAT TAX ::::" + 
	        invoiceentity.getSalesOrderProducts().get(i).getVatTax()
	        .getPercentage() + 
	        "Service Tax::::" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getServiceTax().getPercentage());
	      
	      String premisesVal = "";
	      for (int j = 0; j < con.getItems().size(); j++) {
	        if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == 
	          con.getItems().get(j).getPrduct().getCount()) {
	          premisesVal = con.getItems().get(j).getPremisesDetails();
	        }
	      }
	      
	      System.out.println("noOfLines in product" + noOfLines);
	      if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					noOfLines = noOfLines - 1;
					Phrase blankValPhrs = new Phrase(" ", font8);
					PdfPCell premiseCell = new PdfPCell(blankValPhrs);
					premiseCell.setColspan(1);
					premiseCell.setBorderWidthBottom(0);
					productTable.addCell(premiseCell);

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					premiseCell.setBorderWidthBottom(0);
					productTable.addCell(premiseCell);
				}
			} else {

			}



	  	boolean taxPresent = validateTaxes(invoiceentity
				.getSalesOrderProducts().get(i));
	    }
	    


	    int remainingLines = 0;
	    System.out.println("noOfLines outside" + noOfLines);
	    System.out.println("prouductCount" + prouductCount);
	    
	    if (noOfLines != 0) {
	      remainingLines = 16 - (16 - noOfLines);
	    }
	    System.out.println("remainingLines" + remainingLines);
	    
	    if (remainingLines != 0) {
	      for (int i = 0; i < remainingLines; i++) {
	        System.out.println("i::::" + i);
//	        Phrase blankPhrase = new Phrase(" ", font10);
//	        PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
//	        blankPhraseCell.setBorder(0);
//	        blankPhraseCell.setHorizontalAlignment(1);
//	        blankPhraseCell.setBorderWidthRight(1);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseRightCell);
	      }
	    }
	    PdfPCell productTableCell = null;
	    int remainingLinesCheck = 0;
	    if (noOfLines != 0) {
	      remainingLinesCheck =12 - (12 - noOfLines);
	    }
	    System.out.println("remainingLinesCheck" + remainingLinesCheck);
	    if ((noOfLines == 0) && (remainingLines != 0)) {
	      Phrase my = new Phrase("Please Refer Annexure For More Details", 
	        font9bold);
	      productTableCell = new PdfPCell(my);
	    }
	    else {
	      productTableCell = new PdfPCell(blankCell);
	    }
	    
	    productTableCell.setBorder(0);
	    
	    PdfPTable tab = new PdfPTable(1);
	    tab.setWidthPercentage(100.0F);
	    tab.addCell(productTableCell);
	    tab.setSpacingAfter(blankLines);



	    PdfPCell tab1 = new PdfPCell(productTable);
	    tab1.setBorder(0);

	    PdfPCell tab2 = new PdfPCell(tab);
	    

	    PdfPTable mainTable = new PdfPTable(1);
	    mainTable.setWidthPercentage(100.0F);
	    mainTable.addCell(tab1);
	    mainTable.addCell(tab2);
	    try
	    {
	      document.add(mainTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  


	  private void createProductDetailsForRate()
	  {
		  logger.log(Level.SEVERE,"In createProductDetailsForRate");
	    PdfPTable productTable = new PdfPTable(8);
	    productTable.setWidthPercentage(100.0F);
	    try {
	      productTable.setWidths(column8ProdCollonWidth);
	    } catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    Phrase srNophrase = new Phrase("Sr No", font10bold);
	    PdfPCell srNoCell = new PdfPCell(srNophrase);
	    srNoCell.setHorizontalAlignment(1);
	    srNoCell.setVerticalAlignment(5);
	    srNoCell.setRowspan(2);
//	    srNoCell.setBorder(0);
	    srNoCell.setBorderWidthRight(0);
	    
	    Phrase servicePhrase = new Phrase("Services", font10bold);
	    PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
	    servicePhraseCell.setHorizontalAlignment(1);
	    servicePhraseCell.setVerticalAlignment(5);
	    servicePhraseCell.setRowspan(2);
//	    servicePhraseCell.setBorder(0);
	    servicePhraseCell.setBorderWidthRight(0);
	    
	    Phrase hsnCode = new Phrase("HSN/SAC", font10bold);
	    PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
	    hsnCodeCell.setHorizontalAlignment(1);
	    hsnCodeCell.setVerticalAlignment(5);
	    hsnCodeCell.setRowspan(2);
//	    hsnCodeCell.setBorder(0);
	    hsnCodeCell.setBorderWidthRight(0);
	    
	    Phrase UOMphrase = new Phrase("UOM", font10bold);
	    PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
	    UOMphraseCell.setHorizontalAlignment(1);
	    UOMphraseCell.setVerticalAlignment(5);
	    UOMphraseCell.setRowspan(2);
//	    UOMphraseCell.setBorder(0);
	    UOMphraseCell.setBorderWidthRight(0);
	    
	    Phrase qtyPhrase = new Phrase("Qty", font10bold);
	    PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
	    qtyPhraseCell.setHorizontalAlignment(1);
	    qtyPhraseCell.setVerticalAlignment(5);
	    qtyPhraseCell.setRowspan(2);
//	    qtyPhraseCell.setBorder(0);
	    qtyPhraseCell.setBorderWidthRight(0);
	    
	    Phrase ratePhrase = new Phrase("Rate", font10bold);
	    PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
	    ratePhraseCell.setHorizontalAlignment(1);
	    ratePhraseCell.setVerticalAlignment(5);
	    ratePhraseCell.setRowspan(2);
//	    ratePhraseCell.setBorder(0);
	    ratePhraseCell.setBorderWidthRight(0);
	    
	    Phrase amountPhrase = new Phrase("Amount", font10bold);
	    PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
	    amountPhraseCell.setHorizontalAlignment(1);
	    amountPhraseCell.setVerticalAlignment(5);
	    amountPhraseCell.setRowspan(2);
//	    amountPhraseCell.setBorder(0);
	    amountPhraseCell.setBorderWidthRight(0);
	    
	    Phrase dicphrase = new Phrase("Disc", font10bold);
	    PdfPCell dicphraseCell = new PdfPCell(dicphrase);
	    dicphraseCell.setHorizontalAlignment(1);
	    dicphraseCell.setVerticalAlignment(5);
	    dicphraseCell.setRowspan(2);
//	    dicphraseCell.setBorder(0);
	    dicphraseCell.setBorderWidthRight(0);
	    
	    Phrase taxValPhrase = new Phrase("Amount", font10bold);
	    PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
	    taxValPhraseCell.setHorizontalAlignment(1);
	    taxValPhraseCell.setVerticalAlignment(5);
	    taxValPhraseCell.setRowspan(2);
	    
	    Phrase serviceServDate = new Phrase("Duration", font10bold);
	    PdfPCell serviceServDateCell = new PdfPCell(serviceServDate);
	    serviceServDateCell.setHorizontalAlignment(1);
	    serviceServDateCell.setVerticalAlignment(5);
//	    serviceServDateCell.setBorder(0);
	    serviceServDateCell.setBorderWidthRight(0);
	    
	    productTable.addCell(srNoCell);
	    productTable.addCell(servicePhraseCell);
	    productTable.addCell(hsnCodeCell);
	    

	    productTable.addCell(qtyPhraseCell);
	    productTable.addCell(UOMphraseCell);
	    productTable.addCell(ratePhraseCell);
	    
	    productTable.addCell(dicphraseCell);
	    productTable.addCell(taxValPhraseCell);
	    try
	    {
	      document.add(productTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  


	  private void createFooterOtherChargesPart2()
	  {
	    PdfPTable otherChargesTable = new PdfPTable(2);
	    otherChargesTable.setWidthPercentage(100.0F);
	    try {
	      otherChargesTable.setWidths(columnMoreLeftWidths);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    


	    Phrase amtInWordsValphrase = new Phrase("", font10bold);
	    
	    PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
	    amtInWordsValCell.setHorizontalAlignment(0);
	    otherChargesTable.addCell(amtInWordsValCell);
	    
	    Phrase otherCharges = new Phrase("Other Charges", font10bold);
	    
	    PdfPCell netPayCell = new PdfPCell(otherCharges);
	    netPayCell.setBorder(0);
	    
	    Phrase colon = new Phrase(":", font10bold);
	    
	    PdfPCell colonCell = new PdfPCell(colon);
	    
	    colonCell.setBorder(0);
	    
	    double totalOtherCharges = 0;
	    for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++)
	    {
	      totalOtherCharges = totalOtherCharges + ((OtherCharges)invoiceentity.getOtherCharges().get(i)).getAmount();
	    }
	    Phrase netPayVal = new Phrase(totalOtherCharges+"", font10bold);
	    PdfPCell netPayValCell = new PdfPCell(netPayVal);
	    netPayValCell.setBorderWidthLeft(0.0F);
	    netPayValCell.setBorderWidthTop(0.0F);
	    netPayValCell.setBorderWidthBottom(0.0F);
	    netPayValCell.setHorizontalAlignment(2);
	    
	    PdfPTable innerRightTable = new PdfPTable(3);
	    try {
	      innerRightTable.setWidths(columnCollonGSTWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    innerRightTable.setWidthPercentage(100.0F);
	    innerRightTable.addCell(netPayCell);
	    innerRightTable.addCell(colonCell);
	    innerRightTable.addCell(netPayValCell);
	    
	    PdfPCell netPayableCell = new PdfPCell(innerRightTable);
	    

	    otherChargesTable.addCell(netPayableCell);
	    try {
	      document.add(otherChargesTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createFooterDisCountBeforeNetPayPart(double roundOff)
	  {
	    PdfPTable amountTable = new PdfPTable(2);
	    amountTable.setWidthPercentage(100.0F);
	    try {
	      amountTable.setWidths(columnMoreLeftWidths);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    Phrase amtInWordsValphrase = new Phrase("", font10bold);
	    
	    PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
	    amtInWordsValCell.setHorizontalAlignment(0);
	    amountTable.addCell(amtInWordsValCell);
	    
	    Phrase netPay = new Phrase("Round Off", font10bold);
	    
	    PdfPCell netPayCell = new PdfPCell(netPay);
	    netPayCell.setBorder(0);
	    
	    Phrase colon = new Phrase(":", font10bold);
	    
	    PdfPCell colonCell = new PdfPCell(colon);
	    
	    colonCell.setBorder(0);
	    
	    Phrase netPayVal = new Phrase(df.format(roundOff), font10bold);
	    PdfPCell netPayValCell = new PdfPCell(netPayVal);
	    netPayValCell.setBorderWidthLeft(0.0F);
	    netPayValCell.setBorderWidthTop(0.0F);
	    netPayValCell.setBorderWidthBottom(0.0F);
	    netPayValCell.setHorizontalAlignment(2);
	    
	    PdfPTable innerRightTable = new PdfPTable(3);
	    try {
	      innerRightTable.setWidths(columnCollonGSTWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    innerRightTable.setWidthPercentage(100.0F);
	    innerRightTable.addCell(netPayCell);
	    innerRightTable.addCell(colonCell);
	    innerRightTable.addCell(netPayValCell);
	    
	    PdfPCell netPayableCell = new PdfPCell(innerRightTable);
	    

	    amountTable.addCell(netPayableCell);
	    try {
	      document.add(amountTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createFooterDisCountAfterPart(double discount)
	  {
	    PdfPTable amountTable = new PdfPTable(2);
	    amountTable.setWidthPercentage(100.0F);
	    try {
	      amountTable.setWidths(columnMoreLeftWidths);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    Phrase amtInWordsValphrase = new Phrase("", font10bold);
	    
	    PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
	    amtInWordsValCell.setHorizontalAlignment(0);
	    amountTable.addCell(amtInWordsValCell);
	    
	    Phrase netPay = new Phrase("Discount Amt", font10bold);
	    
	    PdfPCell netPayCell = new PdfPCell(netPay);
	    netPayCell.setBorder(0);
	    
	    Phrase colon = new Phrase(":", font10bold);
	    
	    PdfPCell colonCell = new PdfPCell(colon);
	    
	    colonCell.setBorder(0);
	    
	    Phrase netPayVal = new Phrase(df.format(discount), font10bold);
	    PdfPCell netPayValCell = new PdfPCell(netPayVal);
	    netPayValCell.setBorderWidthLeft(0.0F);
	    netPayValCell.setBorderWidthTop(0.0F);
	    netPayValCell.setBorderWidthBottom(0.0F);
	    netPayValCell.setHorizontalAlignment(2);
	    
	    PdfPTable innerRightTable = new PdfPTable(3);
	    try {
	      innerRightTable.setWidths(columnCollonGSTWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    innerRightTable.setWidthPercentage(100.0F);
	    innerRightTable.addCell(netPayCell);
	    innerRightTable.addCell(colonCell);
	    innerRightTable.addCell(netPayValCell);
	    
	    PdfPCell netPayableCell = new PdfPCell(innerRightTable);
	    

	    amountTable.addCell(netPayableCell);
	    try {
	      document.add(amountTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createFooterOtherChargesPart()
	  {
	    System.out.println("Inside Other Chrages");
	    PdfPTable otherChargesTable = new PdfPTable(2);
	    otherChargesTable.setWidthPercentage(100.0F);
	    try {
	      otherChargesTable.setWidths(columnMoreLeftWidths);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    Phrase blank = new Phrase("", font10bold);
	    PdfPCell blankCell = new PdfPCell(blank);
	    otherChargesTable.addCell(blankCell);
	    
	    PdfPTable otherCharges = new PdfPTable(3);
	    otherCharges.setWidthPercentage(100.0F);
	    try {
	      otherCharges.setWidths(column3ProdCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    




	    Phrase chargeName = new Phrase("Charge Name", font10bold);
	    Phrase taxes = new Phrase("Taxes", font10bold);
	    Phrase assVal = new Phrase("Amt", font10bold);
	    PdfPCell pCell = new PdfPCell(chargeName);
	    pCell.setHorizontalAlignment(5);
	    otherCharges.addCell(pCell);
	    pCell = new PdfPCell(taxes);
	    pCell.setHorizontalAlignment(5);
	    otherCharges.addCell(pCell);
	    pCell = new PdfPCell(assVal);
	    pCell.setHorizontalAlignment(5);
	    otherCharges.addCell(pCell);
	    
	    chargeName = new Phrase("Transport", font10);
	    taxes = new Phrase("SGST@9/CGST@9", 
	    

	      font10);
	    assVal = new Phrase("100", font10);
	    pCell = new PdfPCell(chargeName);
	    otherCharges.addCell(pCell);
	    pCell = new PdfPCell(taxes);
	    otherCharges.addCell(pCell);
	    pCell = new PdfPCell(assVal);
	    otherCharges.addCell(pCell);
	    


	    PdfPCell leftCell = new PdfPCell(otherCharges);
	    otherChargesTable.addCell(leftCell);
	    try {
	      document.add(otherChargesTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  
	  private void createTermsAndCondition()
	  {
	    String friends = "";
	    if (onlyForFriendsPestControl) {
	      friends = "1.If you are not satisfied with the treatment within 15 days of treatment, free treatment will be provided. \n2.You will receive two reminders for each of your treatments by call, email and by SMS. Our obligation limits to reminders only. \n3.It is essential to avail the treatment within the due dates to validate the warranty. \n4.Contract period cannot be extended for any reason. \n5.Once the due date is over the treatment cannot be carry forwarded to extend the contract period. \n6.It is mandatory to avail the treatment within a period of fifteen days before or after due date.Otherwise the treatment will be considered as lapsed.\nTHEREFOR PLEASE INSURE THE SCHEDULE MENTION HERE IS STRICTLY FOLLOWED";


	    }
	    else
	    {


	      friends = invoiceentity.getComment().trim();
	    }
	    
	    int remainingLinesForTerms = 3;
	    
	    if (friends.length() > 690) {
	      friends = friends.substring(0, 690);
	    }
	    Phrase termNcondVal = new Phrase("Remarks: \n" + friends, font11);
	    
	    PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
	    termNcondValCell.setBorderWidthBottom(0.0F);
	    termNcondValCell.setBorderWidthTop(0.0F);
	    PdfPTable pdfTable = new PdfPTable(1);
	    pdfTable.setWidthPercentage(100.0F);
	    pdfTable.addCell(termNcondValCell);
	    
	    Phrase blankPhrase = new Phrase(" ", font10bold);
	    PdfPCell blank = new PdfPCell(blankPhrase);
	    blank.setBorderWidthBottom(0.0F);
	    blank.setBorderWidthTop(0.0F);
	    
	    remainingLinesForTerms = remainingLinesForTerms - friends.length() / 138;
	    System.out.println("remainingLinesForTerms" + remainingLinesForTerms);
	    for (int i = 0; i < remainingLinesForTerms; i++) {
	      pdfTable.addCell(blank);
	    }
	    PdfPCell pdfPcell = new PdfPCell(pdfTable);
	    pdfPcell.setBorder(0);
	    
	    PdfPTable table1 = new PdfPTable(1);
	    table1.setWidthPercentage(100.0F);
	    table1.addCell(pdfPcell);
	    


	    PdfPTable table2 = new PdfPTable(1);
	    table2.setWidthPercentage(100.0F);
	    




	    String Pannoval = null;
	    for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	    {
	      if ((((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("PAN")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticlePrint().equalsIgnoreCase("YES")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getDocumentName().equalsIgnoreCase("Invoice Details")))
	      {
	        Pannoval = (String)comp.getArticleTypeDetails().get(i).getArticleTypeValue();
	      }
	    }
	    


	    PdfPTable Pantable = new PdfPTable(2);
	    Pantable.setWidthPercentage(100);
	    try {
//	      Pantable.setWidths(new float[]{13,78});
	    float[] equalWidth = { 0.5F, 0.5F};
	      Pantable.setWidths(equalWidth);//16,84
	      
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    PdfPCell Pannocell;
	    if(Pannoval!=null&&!Pannoval.equals(""))
	    	Pannocell=pdfUtility.getPdfCell("PAN Number: "+Pannoval, font11bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1, -1, -1, 0, 0, 0, 0, 0);
	    else
	    	Pannocell=pdfUtility.getPdfCell("", font11bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE,0, 0, 0, -1, -1, -1, -1, 0, 0, 0, 0, 0);
		  
	    PdfPTable pannoTable = new PdfPTable(1);
	    pannoTable.setWidthPercentage(100);
	    pannoTable.addCell(Pannocell);
	    
	    String cinstring = "CIN";
	    String cinval = null;
	    
	    for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	    {
	      if ((((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("CIN")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticlePrint().equalsIgnoreCase("YES")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getDocumentName().equalsIgnoreCase("Invoice Details")))
	      {

	        cinval = ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeValue();
	      }
	    }
	    PdfPCell cincell;
	    if(cinval!=null&&!cinval.equals(""))
	    	cincell=pdfUtility.getPdfCell("CIN : "+cinval, font11bold, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1,-1, 0, -1, 0, 0, 0, 0);
	    else
	    	cincell=pdfUtility.getPdfCell("", font11bold, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1,-1, 0, -1, 0, 0, 0, 0);
	   
	    PdfPTable cinTable = new PdfPTable(1);
	    cinTable.setWidthPercentage(100);
	    cinTable.addCell(cincell);
	    
	    PdfPCell panNoTablecell = new PdfPCell(pannoTable);
	    panNoTablecell.setBorder(0);
	    PdfPCell cinTableTablecell = new PdfPCell(cinTable);
	    cinTableTablecell.setBorder(0);
	    Pantable.addCell(panNoTablecell);
	    Pantable.addCell(cinTableTablecell);
	    
//	    Phrase Panno = new Phrase("PAN Number:", font11bold);//Company's PAN :
//	    PdfPCell Pannocell = new PdfPCell(Panno);
//	    Pannocell.setBorder(0);
//	    
//	    PdfPCell Pannovalcell = new PdfPCell(Pannoval);
//	    Pannovalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	    Pannovalcell.setBorder(0);
//	    Pantable.addCell(Pannocell);
//	    Pantable.addCell(Pannovalcell);
	    
	   
	    Paragraph Decla_pg = new Paragraph();
	    Phrase Decph = new Phrase("Declaration:", font11bold);
	    PdfPCell Deccell = new PdfPCell(Decph);
//	    Deccell.setBorder(0);
	    Deccell.setBorderWidthBottom(0);
	    Deccell.setBorderWidthTop(0);
//	    
	    String Decval = "";
//	    Decval = "i) Issue cheque/DD/pay order in favour of 'Orion Pest Solutions Pvt Ltd' \nii) Deduct TDS only @2% u/s 194C(if applicable)\niii)Share your payment advice on accounts@orionpest.com\niv) The MSMED Act, 2006 contains provisions of Delayed Payment to Micro and Small Enterprise (MSEs). (Section 15- 24). As per Micro and Small Enterprise Facilitation Council (MSEFC) for settlement of disputes on getting references/filing on Delayed payments. (Section 20 and 21) UAM: UDYAM-WB-10-0004286\n\n";
	    

	    Declaration declaration=ofy().load().type(Declaration.class)
				.filter("companyId", invoiceentity.getCompanyId())
				.filter("status", true).first().now();
	    if(declaration!=null && !declaration.equals("")){
	    	Decval=declaration.getDeclaratiomMsg();			
	    }else{
			if(invoiceentity.getDeclaration()!=null&&!invoiceentity.getDeclaration().equals("")){
				Decval=invoiceentity.getDeclaration();
			}
		}

	    Phrase Decvalph = new Phrase(Decval, font8);
	    PdfPCell Decvalcell = new PdfPCell(Decvalph);
	    Decvalcell.setBorder(0);
	    
	    
//	    Decla_pg.add(Decph);
//	    Decla_pg.add(Chunk.NEWLINE);
	    Decla_pg.add(Decvalph);
	    PdfPCell declarationcell=new PdfPCell();
	    declarationcell.addElement(Decla_pg);
	    declarationcell.setBorderWidthBottom(0);
	    declarationcell.setBorderWidthTop(0);
	    
//	    PdfPCell pantablecell=new PdfPCell();
//	    pantablecell.addElement(Pantable);
//	    pantablecell.setBorder(0);
	    
//	    table2.addCell(pantablecell);
//	    table2.addCell(Decla_pg);
	    table2.addCell(Deccell);
	    table2.addCell(declarationcell);
	   
	    try
	    {
	    if(friends!=null&&!friends.equals(""))
	    	document.add(table1);
	    	document.add(Pantable);
	    	document.add(table2);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createFooterAmountInWords_NetPayPart()
	  {
	    PdfPTable amountTable = new PdfPTable(2);
	    amountTable.setWidthPercentage(100.0F);
	    try {
	      amountTable.setWidths(columnMoreLeftWidths);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    /**
	     * Date 28-07-2018 By Vijay 
	     * Des :- below tax amount replaced in payment terms table and here added net payable 
	     */
//	    String amtInWordsVal = "Tax Amount in Words : Rupees " + 
//	      SalesInvoicePdf.convert(taxAmountInWords) + 
//	      " Only/-";
	    Phrase amtInWordsValphrase = new Phrase("Total(In Words): Rupees " + SalesInvoicePdf.convert(invoiceentity.getNetPayable()) + " " + "Only/-", font10bold);
	    /**
	     * ends here
	     */
	    
	    PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
	    amtInWordsValCell.setHorizontalAlignment(0);
	    if(!isRemarkGiven)
	    	amtInWordsValCell.setBorderWidthBottom(0.0F);//25-07-2023
	    amtInWordsValCell.setPaddingBottom(5);
	    amountTable.addCell(amtInWordsValCell);
	    
	    Phrase netPay = new Phrase("Net Payable", font10bold);
	    
	    PdfPCell netPayCell = new PdfPCell(netPay);
//	    netPayCell.setBorder(0);
	    netPayCell.setBorderWidthLeft(0);
//	    netPayCell.setBorderWidthTop(0);
	    if(!isRemarkGiven)
	    netPayCell.setBorderWidthBottom(0);
	    netPayCell.setBorderWidthRight(0);
	    
	    Phrase colon = new Phrase(":", font10bold);
	    
	    PdfPCell colonCell = new PdfPCell(colon);
	    
//	    colonCell.setBorder(0);
	    colonCell.setBorderWidthLeft(0);
	    if(!isRemarkGiven)
	    colonCell.setBorderWidthBottom(0);
	    colonCell.setBorderWidthRight(0);
	    
	    Phrase netPayVal = new Phrase(df.format(invoiceentity.getNetPayable()), 
	      font10bold);
	    PdfPCell netPayValCell = new PdfPCell(netPayVal);
	    netPayValCell.setBorderWidthLeft(0.0F);
//	    netPayValCell.setBorderWidthTop(0.0F);
	    if(!isRemarkGiven)
	    netPayValCell.setBorderWidthBottom(0.0F);
	    netPayValCell.setHorizontalAlignment(2);
	    
	    PdfPTable innerRightTable = new PdfPTable(3);
	    try {
	      innerRightTable.setWidths(columnCollonGSTWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    innerRightTable.setWidthPercentage(100.0F);
	    innerRightTable.addCell(netPayCell);
	    innerRightTable.addCell(colonCell);
	    innerRightTable.addCell(netPayValCell);
	    	
	    //22-12-2023 commenting to save one line space
//	    innerRightTable.addCell(pdfUtility.getPdfCell(" ", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	     
	    PdfPCell netPayableCell = new PdfPCell(innerRightTable);
	    netPayableCell.setBorder(0);
	    amountTable.addCell(netPayableCell);
	    try {
	      document.add(amountTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createFooterAmountPart()
	  {
	    double totalAssAmount = 0;
	    double rateAmount = 0;
	    double amountAmount = 0;
	    double discAmount = 0;
	    
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {

	      rateAmount = rateAmount + invoiceentity.getSalesOrderProducts().get(i).getPrice();
	      

	      amountAmount = amountAmount + invoiceentity.getSalesOrderProducts().get(i).getPrice() * 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getQuantity();
	      

	      discAmount = discAmount + invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
	      
	      double taxValue = 0;
	      if (invoiceentity.getSalesOrderProducts().get(i)
	        .getBasePaymentAmount() != 0) {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
	      }
	      else {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
	      }
	      totalAssAmount += taxValue;
	    }
	    PdfPTable productTable = new PdfPTable(5);
	    productTable.setWidthPercentage(100.0F);
	    try {
	    if(onlyforOrion) {
	    	float[] column5 = { 2.00F, 2.00F, 2.00F, 2.00F, 2.00F };
	   	    productTable.setWidths(column5);
	    }else
	    	productTable.setWidths(column5ProdCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    /**
	     * Date 28-07-2018 By Vijay 
	     * Des :- not required this added below net payable
	     */
//	    Phrase totalamount = new Phrase("Total(In Words): Rupees " + SalesInvoicePdf.convert(totalAssAmount) + " " + "Only/-", font10bold);
//	    PdfPCell totalAmountCell = new PdfPCell(totalamount);
//	    totalAmountCell.setHorizontalAlignment(0);
//	    productTable.addCell(totalAmountCell);
	    if(!onlyforOrion)
	    productTable.addCell("");
	    /**
	     * ends here
	     */
	    
	    Phrase totalph = new Phrase("Total ", font10bold);
	    PdfPCell totalCell = new PdfPCell(totalph);
	    totalCell.setHorizontalAlignment(2);
	    if(onlyforOrion){
	    	totalCell.setColspan(4);
//	    	totalCell.setBorderWidthRight(0);
//	    	totalCell.setBorderWidthTop(0);
	    }
	    productTable.addCell(totalCell);
	    

	    Phrase rateValue = new Phrase("", font10);
	    PdfPCell rateValueCell = new PdfPCell(rateValue);
	    rateValueCell.setHorizontalAlignment(2);
	    if(!onlyforOrion)
	    productTable.addCell(rateValueCell);
	    
	    Phrase amountValue = new Phrase(df.format(amountAmount), font10);
	    PdfPCell amountValueCell = new PdfPCell(amountValue);
	    amountValueCell.setHorizontalAlignment(2);
	    

	    Phrase discamount = new Phrase(df.format(discAmount), font10);
	    PdfPCell discamountCell = new PdfPCell(discamount);
	    discamountCell.setHorizontalAlignment(2);
	    if(!onlyforOrion)
	    productTable.addCell(discamountCell);
	    
	    System.out.println("totalAssAmount:::" + totalAssAmount);
	    Phrase totalValamount = new Phrase(df.format(totalAssAmount), font10);
	    PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
	    totalAmountValCell.setHorizontalAlignment(2);
	    totalAmountValCell.setBorderWidthLeft(0);
//	    totalAmountValCell.setBorderWidthTop(0);
//	    totalAmountValCell.setBorderWidthBottom(0);
	    
	    productTable.addCell(totalAmountValCell);
	    try
	    {
	      document.add(productTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createtotalAmount()
	  {
	    PdfPTable productTable = new PdfPTable(2);
	    productTable.setWidthPercentage(100.0F);
	    try {
	      productTable.setWidths(columnMoreLeftWidths);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    Phrase totalamount = new Phrase("Total Ass Amount", font10bold);
	    PdfPCell totalAmountCell = new PdfPCell(totalamount);
	    totalAmountCell.setHorizontalAlignment(0);
	    productTable.addCell(totalAmountCell);
	    
	    Phrase totalValamount = new Phrase(df.format(0), font10);
	    PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
	    totalAmountValCell.setHorizontalAlignment(0);
	    productTable.addCell(totalAmountValCell);
	    try
	    {
	      document.add(productTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  



	  private void createCompanyNameAsHeader(Document doc, Company comp)
	  {
	    DocumentUpload document = comp.getUploadHeader();
	    


	    String environment = 
	      System.getProperty("com.google.appengine.runtime.environment");
	    String hostUrl; 
	    if (environment.equals("Production")) {
	      String applicationId = 
	        System.getProperty("com.google.appengine.application.id");
	      String version = 
	        System.getProperty("com.google.appengine.application.version");
	      hostUrl = "http://" + version + "." + applicationId + 
	        ".appspot.com/";
	    } else {
	      hostUrl = "http://localhost:8888";
	    }
	    try
	    {
	      Image image2 = Image.getInstance(new URL(hostUrl + 
	        document.getUrl()));
	      image2.scalePercent(15.0F);
	      image2.scaleAbsoluteWidth(520.0F);
	      image2.setAbsolutePosition(40.0F, 725.0F);
	      doc.add(image2);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	  }
	  

















	  private void createCompanyNameAsFooter(Document doc, Company comp)
	  {
	    DocumentUpload document = comp.getUploadFooter();
	    


	    String environment = 
	      System.getProperty("com.google.appengine.runtime.environment");
	    String hostUrl; 
	    if (environment.equals("Production")) {
	      String applicationId = 
	        System.getProperty("com.google.appengine.application.id");
	      String version = 
	        System.getProperty("com.google.appengine.application.version");
	      hostUrl = "http://" + version + "." + applicationId + 
	        ".appspot.com/";
	    } else {
	      hostUrl = "http://localhost:8888";
	    }
	    try
	    {
	      Image image2 = Image.getInstance(new URL(hostUrl + 
	        document.getUrl()));
	      image2.scalePercent(15.0F);
	      image2.scaleAbsoluteWidth(520.0F);
	      image2.setAbsolutePosition(40.0F, 40.0F);
	      doc.add(image2);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	  }
	  
















	  private void createBlankforUPC()
	  {
	    Image uncheckedImg = null;
	    try {
	      uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
	    } catch (BadElementException|java.io.IOException e3) {
	      e3.printStackTrace();
	    }
	    uncheckedImg.scalePercent(9.0F);
	    









	    PdfPTable mytbale = new PdfPTable(3);
	    mytbale.setSpacingAfter(5.0F);
	    mytbale.setWidthPercentage(100.0F);
	    





	    Phrase myblank = new Phrase("   ", font10);
	    PdfPCell myblankCell = new PdfPCell(myblank);
	    
	    myblankCell.setHorizontalAlignment(0);
	    
	    Phrase myblankborderZero = new Phrase(" ", font10);
	    PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
	    
	    myblankborderZeroCell.setBorder(0);
	    myblankborderZeroCell.setHorizontalAlignment(0);
	    
	    Phrase stat1Phrase = new Phrase("  Original for Receipient", font10);
	    Paragraph para1 = new Paragraph();
	    para1.setIndentationLeft(10.0F);
	    para1.add(myblank);
	    para1.add(new Chunk(uncheckedImg, 0.0F, 0.0F, true));
	    para1.add(stat1Phrase);
	    para1.setAlignment(5);
	    
	    PdfPCell stat1PhraseCell = new PdfPCell(para1);
	    stat1PhraseCell.setBorder(0);
	    stat1PhraseCell.setHorizontalAlignment(0);
	    
	    Phrase stat2Phrase = new Phrase("  Duplicate for Supplier/Transporter", 
	      font10);
	    Paragraph para2 = new Paragraph();
	    para2.setIndentationLeft(10.0F);
	    para2.add(new Chunk(uncheckedImg, 0.0F, 0.0F, true));
	    para2.add(stat2Phrase);
	    para2.setAlignment(1);
	    
	    PdfPCell stat2PhraseCell = new PdfPCell(para2);
	    stat2PhraseCell.setBorder(0);
	    stat2PhraseCell.setHorizontalAlignment(0);
	    
	    Phrase stat3Phrase = new Phrase("  Triplicate for Supplier", font10);
	    Paragraph para3 = new Paragraph();
	    para3.setIndentationLeft(10.0F);
	    para3.add(new Chunk(uncheckedImg, 0.0F, 0.0F, true));
	    para3.add(stat3Phrase);
	    para3.setAlignment(3);
	    
	    PdfPCell stat3PhraseCell = new PdfPCell(para3);
	    stat3PhraseCell.setBorder(0);
	    stat3PhraseCell.setHorizontalAlignment(0);
	    

	    String titlepdf = "";
	    

	    if (("Proforma Invoice".equals(invoiceentity.getInvoiceType().trim())) || 
	    
	      (invoiceentity.getInvoiceType().trim().equals("Proforma Invoice"))) {
	      titlepdf = "Proforma Invoice";
	    } else {
	      titlepdf = "Tax Invoice";
	    }
	    Phrase titlephrase = new Phrase(titlepdf, font14bold);
	    Paragraph titlepdfpara = new Paragraph();
	    titlepdfpara.add(titlephrase);
	    titlepdfpara.setAlignment(1);
	    
	    PdfPCell titlecell = new PdfPCell();
	    titlecell.addElement(titlepdfpara);
	    titlecell.setBorder(0);
	    
	    Phrase blankphrase = new Phrase("", font8);
	    PdfPCell blankCell = new PdfPCell();
	    blankCell.addElement(blankphrase);
	    blankCell.setBorder(0);
	    
	    PdfPTable titlepdftable = new PdfPTable(3);
	    titlepdftable.setWidthPercentage(100.0F);
	    titlepdftable.setHorizontalAlignment(1);
	    titlepdftable.addCell(blankCell);
	    titlepdftable.addCell(titlecell);
	    titlepdftable.addCell(blankCell);
	    
	    Paragraph blank = new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    
	    PdfPTable parent = new PdfPTable(1);
	    parent.setWidthPercentage(100.0F);
	    parent.setSpacingBefore(10.0F);
	    
	    PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	    parent.addCell(titlePdfCell);
	    try
	    {
	      document.add(blank);
	      document.add(blank);
	      document.add(blank);
	      document.add(blank);
	      document.add(blank);
	      document.add(blank);
	      document.add(mytbale);
	      document.add(parent);
	    } catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  


	  private void createLogo(Document doc, Company comp2)
	  {
	    DocumentUpload document = comp.getLogo();
	    


	    String environment = 
	      System.getProperty("com.google.appengine.runtime.environment");
	    String hostUrl; 
	     if (environment.equals("Production")) {
	      String applicationId = 
	        System.getProperty("com.google.appengine.application.id");
	      String version = 
	        System.getProperty("com.google.appengine.application.version");
	      hostUrl = "http://" + version + "." + applicationId + 
	        ".appspot.com/";
	    } else {
	      hostUrl = "http://localhost:8888";
	    }
	    try {
	      Image image2 = Image.getInstance(new URL(hostUrl + 
	        document.getUrl()));
	      image2.scalePercent(20.0F);
	      image2.setAbsolutePosition(40.0F, 750.0F);
	      doc.add(image2);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	  }
	  

























	  private void createFooterLastPart(String preprintStatus)
	  {
		  boolean isQrcodePresent=false;
		  if(comppayment!=null && comppayment.getQrCodeDocument()!=null && comppayment.getQrCodeDocument().getUrl()!=null && !comppayment.getQrCodeDocument().getUrl().equals("")) {
				isQrcodePresent=true;
		  }
		  logger.log(Level.SEVERE, "isQrcodePresent="+isQrcodePresent);
		  PdfPTable bottomTable;
		if(onlyforOrion) {
			if(isQrcodePresent) {

				bottomTable = new PdfPTable(3);
			    bottomTable.setWidthPercentage(100.0F);
			    float[] columnThreePartWidths = { 1.0F, 1.0F,1.0F};
			    try {
			      bottomTable.setWidths(columnThreePartWidths);
			    }
			    catch (DocumentException e) {
			      e.printStackTrace();
			    }
				
			}else {
				bottomTable = new PdfPTable(2);//3
			    bottomTable.setWidthPercentage(100.0F);
			    float[] columnThreePartWidths = { 1.0F, 1.0F };//, 1.0F
			    try {
			      bottomTable.setWidths(columnThreePartWidths);
			    }
			    catch (DocumentException e) {
			      e.printStackTrace();
			    }
				
			}
	    
		}else {
			bottomTable = new PdfPTable(3);
		    bottomTable.setWidthPercentage(100.0F);
		    float[] columnThreePartWidths = { 1.0F, 1.0F, 1.0F};
		    try {
		      bottomTable.setWidths(columnThreePartWidths);
		    }
		    catch (DocumentException e) {
		      e.printStackTrace();
		    }
		}
	    PdfPTable leftTable = new PdfPTable(1);
	    leftTable.setWidthPercentage(100.0F);
	    

	    if (UniversalFlag)
	    {
	      if ((con.getGroup().equalsIgnoreCase("Universal Pest Control Pvt. Ltd.")) && 
	        (!preprintStatus.equalsIgnoreCase("Plane"))) {
	        for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	        {

	          if (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticlePrint().equalsIgnoreCase("Yes"))
	          {
	            Phrase articalType = new Phrase(
	              ((ArticleType)comp.getArticleTypeDetails().get(i))
	              .getArticleTypeName() + 
	              " : " + 
	              ((ArticleType)comp.getArticleTypeDetails().get(i))
	              .getArticleTypeValue(), font10bold);
	            PdfPCell articalTypeCell = new PdfPCell();
	            articalTypeCell.setBorder(0);
	            articalTypeCell.addElement(articalType);
	            leftTable.addCell(articalTypeCell);
	          }
	          
	        }
	      }
	    }
	    else
	    {
	      ServerAppUtility serverApp = new ServerAppUtility();
	      
	      String gstin = "";String gstinText = "";
	      
	      if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
	        logger.log(Level.SEVERE, "GST Applicable");
	        gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
	          .getBranch().trim());
	        System.out.println("gstin" + gstin);
	      }
	      else {
	        logger.log(Level.SEVERE, "GST Not Applicable");
	        gstinText = comp.getCompanyGSTTypeText().trim();
	        System.out.println("gstinText" + gstinText);
	      }
	      
	      Phrase articalType2 = null;
	      if (!gstin.trim().equals("")) {
	        logger.log(Level.SEVERE, "GST Present");
	        articalType2 = new Phrase("GSTIN : " + gstin, font10bold);
	      } else if (!gstinText.trim().equalsIgnoreCase("")) {
	        logger.log(Level.SEVERE, "GST Not Present");
	        articalType2 = new Phrase(gstinText, font10bold);
	      } else {
	        logger.log(Level.SEVERE, "Nothing Present");
	        articalType2 = new Phrase("", font10bold);
	      }
	      
 
	     Phrase blnph = new Phrase("");
	     PdfPCell blncell=new PdfPCell(blnph);
	     blncell.setBorder(0);
	     
	      if (!gstin.equals("")) {
	        PdfPCell articalType2Cell = new PdfPCell(articalType2);
	        articalType2Cell.setBorder(0);
//	        leftTable.addCell(articalType2Cell);
	        leftTable.addCell(blncell);
	        
	        String stateCodeStr = serverApp.getStateOfCompany(comp, 
	          invoiceentity.getBranch().trim(), stateList);
	        Phrase stateCode = new Phrase("State Code : " + 
	          stateCodeStr, font10bold);
	        


	        PdfPCell stateCodeCell = new PdfPCell(stateCode);
	        stateCodeCell.setBorder(0);
//	        leftTable.addCell(stateCodeCell);
	        leftTable.addCell(blncell);
	      } else {
	        PdfPCell articalType2Cell = new PdfPCell(articalType2);
	        articalType2Cell.setBorder(0);
//	        leftTable.addCell(articalType2Cell);
	        leftTable.addCell(blncell);
	      }
	      
	      PdfPTable articleTab = new PdfPTable(3);
	      articleTab.setWidthPercentage(100.0F);
	      try
	      {
	        articleTab.setWidths(new float[] { 30.0F, 5.0F, 65.0F });
	      }
	      catch (DocumentException e) {
	        e.printStackTrace();
	      }
	    }
	    



	    String cinstring = "CIN";
	    String cinvalphrse = null;
	    
	    for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	    {
	      if ((((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("CIN")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticlePrint().equalsIgnoreCase("YES")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getDocumentName().equalsIgnoreCase("Invoice Details")))
	      {

	        cinvalphrse = ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeValue();
	      }
	    }
	    
	    Phrase cin = new Phrase(cinstring + "   :" + cinvalphrse, font8bold);
	    PdfPCell cinCell = new PdfPCell(cin);
	    leftTable.addCell(cinCell);
	    PdfPCell leftCell = new PdfPCell();
	    leftCell.addElement(leftTable);
	    
	    PdfPTable rightTable = new PdfPTable(1);
	    rightTable.setWidthPercentage(100.0F);
	    
	    PdfPTable innerRightTable = new PdfPTable(3);
	    innerRightTable.setWidthPercentage(100.0F);
	    
	    Phrase colon = new Phrase(" :", font10bold);
	    PdfPCell colonCell = new PdfPCell();
	    colonCell.setBorder(0);
	    colonCell.addElement(colon);
	    
	    Phrase blank = new Phrase(" ", font10bold);
	    PdfPCell leftrightborderblankCell = new PdfPCell(blank);
//	    leftrightborderblankCell.setBorder(0);
	    leftrightborderblankCell.setBorderWidthTop(0);
	    leftrightborderblankCell.setBorderWidthBottom(0);
	    PdfPCell leftborderblankCell = new PdfPCell(blank);
//	    leftborderblankCell.setBorder(0);
	    leftborderblankCell.setBorderWidthTop(0);
	    leftborderblankCell.setBorderWidthBottom(0);
	    leftborderblankCell.setBorderWidthRight(0);
	    PdfPCell rightborderblankCell = new PdfPCell(blank);
//	    righttborderblankCell.setBorder(0);
	    rightborderblankCell.setBorderWidthBottom(0);
	    rightborderblankCell.setBorderWidthTop(0);
	    rightborderblankCell.setBorderWidthLeft(0);
	    
	    PdfPCell blankCell = new PdfPCell(blank);
	    blankCell.setBorderWidthBottom(0);
	    blankCell.setBorderWidthTop(0);
	    blankCell.setBorderWidthLeft(0);
	    blankCell.setBorderWidthRight(0);
	    
	    String companyname = "";
	    if (multipleCompanyName) {
	      if ((con.getGroup() != null) && (!con.getGroup().equals(""))) {
	        companyname = con.getGroup().trim().toUpperCase();
	      } else {
	        companyname = comp.getBusinessUnitName().trim().toUpperCase();
	      }
	    }
	    else if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname= branchDt.getCorrespondenceName();
			logger.log(Level.SEVERE,"correspondanceb name 222 "+companyname);
		}
	    else {
	      companyname = comp.getBusinessUnitName().trim().toUpperCase();
	      logger.log(Level.SEVERE,"company Name "+companyname);
	    }
	    



	    Phrase companyPhrase = new Phrase("For , " + companyname, font10bold);
	    PdfPCell companyParaCell = new PdfPCell(companyPhrase);
//	    companyParaCell.setBorder(0);
	    companyParaCell.setBorderWidthBottom(0);
	    companyParaCell.setBorderWidthLeft(0);
	    if (authOnLeft) {
	      companyParaCell.setHorizontalAlignment(0);
	    } else {
	      companyParaCell.setHorizontalAlignment(1);
	    }
	    if(!isRemarkGiven)
	    	rightTable.addCell(rightborderblankCell);
	    
	    rightTable.addCell(companyParaCell);
	    
	    DocumentUpload digitalDocument = comp.getUploadDigitalSign();
	    
	    String environment = 
	      System.getProperty("com.google.appengine.runtime.environment");
	    String hostUrl;
	    if (environment.equals("Production")) {
	      String applicationId = 
	        System.getProperty("com.google.appengine.application.id");
	      String version = 
	        System.getProperty("com.google.appengine.application.version");
	      hostUrl = "http://" + version + "." + applicationId + 
	        ".appspot.com/";
	    } else {
	      hostUrl = "http://localhost:8888";
	    }
	    imageSignCell = null;
	    Image image2 = null;
	    logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
	    try {
	      image2 = Image.getInstance(new URL(hostUrl + 
	        digitalDocument.getUrl()));
	      image2.scalePercent(15.0F);
	      image2.scaleAbsoluteWidth(100.0F);
	      
	      imageSignCell = new PdfPCell(image2);
//	      imageSignCell.setBorder(0);
	      imageSignCell.setBorderWidthBottom(0);
	      imageSignCell.setBorderWidthLeft(0);
	      imageSignCell.setBorderWidthTop(0);
	      if (authOnLeft) {
	        imageSignCell.setHorizontalAlignment(0);
	      } else {
	        imageSignCell.setHorizontalAlignment(1);
	      }
	    }
	    catch (Exception e) {
//	      e.printStackTrace();
	    	logger.log(Level.SEVERE, "signature not found");
	    }
	    















	    if (imageSignCell != null) {
	      rightTable.addCell(imageSignCell);
	    } else {
	      Phrase blank1 = new Phrase(" ", font10);
	      PdfPCell blank1Cell = new PdfPCell(blank1);
//	      blank1Cell.setBorder(0);
	      blank1Cell.setBorderWidthBottom(0);
	      blank1Cell.setBorderWidthTop(0);
	      blank1Cell.setBorderWidthLeft(0);
	      rightTable.addCell(blank1Cell);
	      rightTable.addCell(blank1Cell);
	      rightTable.addCell(blank1Cell);
	      rightTable.addCell(blank1Cell);
	    }
	    Phrase signAuth;
	 
	    if ((comp.getSignatoryText() != null) && 
	      (comp.getSignatoryText().trim().equalsIgnoreCase(""))) {
	      signAuth = new Phrase("Authorised Signatory", font10bold);
	    } else {
	      signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
	    }
	    


	    PdfPCell signParaCell = new PdfPCell(signAuth);
	    

//	    signParaCell.setBorder(0);
	    signParaCell.setBorderWidthLeft(0);
	    signParaCell.setBorderWidthTop(0);
	    signParaCell.setBorderWidthBottom(0);
	    if (authOnLeft) {
	      signParaCell.setHorizontalAlignment(0);
	    } else {
	      signParaCell.setHorizontalAlignment(1);
	    }
	    rightTable.addCell(signParaCell);
	    
	    PdfPCell lefttableCell = new PdfPCell(leftTable);
	    lefttableCell.setBorder(0);
	    
	    PdfPCell righttableCell = new PdfPCell(rightTable);
	    righttableCell.setBorder(0);





	    PdfPTable middletTable = new PdfPTable(1);
	    middletTable.setWidthPercentage(100.0F);
	    
	    if (comppayment != null)
	    {
	      PdfPTable bankDetailsTable = new PdfPTable(1);
	      bankDetailsTable.setWidthPercentage(100.0F);
	      
	      String favourOf = "";
	      if ((comppayment.getPaymentComName() != null) && 
	        (!comppayment.getPaymentComName().equals(""))) {
	        favourOf = 
	          "Cheque should be in favour of '" + comppayment.getPaymentComName() + "'";
	      }
	      if(!isRemarkGiven){
	    	  bankDetailsTable.addCell(leftborderblankCell);
	      }
	      
	      Phrase favouring = new Phrase(favourOf, font8bold);
	      PdfPCell favouringCell = new PdfPCell(favouring);
//	      favouringCell.setBorder(0);
	      favouringCell.setBorderWidthBottom(0);
	      bankDetailsTable.addCell(favouringCell);
	      
//	      PdfPCell blankCell =pdfUtility.getPdfCell("", font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0);
	      
	      bankDetailsTable.addCell(leftrightborderblankCell);
	      
	      Phrase heading = new Phrase("Bank Details", font8bold);
	      PdfPCell headingCell = new PdfPCell(heading);
//	      headingCell.setBorder(0);
	      headingCell.setBorderWidthBottom(0);
	      headingCell.setBorderWidthTop(0);
	      bankDetailsTable.addCell(headingCell);
	      
	      float[] columnWidths3 = { 1.5F, 0.35F, 4.5F };
	      PdfPTable bankDetails3Table = new PdfPTable(3);
	      bankDetails3Table.setWidthPercentage(100.0F);
	      try {
	        bankDetails3Table.setWidths(columnWidths3);
	      }
	      catch (DocumentException e2) {
	        e2.printStackTrace();
	      }
	      Phrase bankNamePh = new Phrase("Name", font8bold);
	      PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
	      bankNamePhCell.setBorder(0);
	      bankDetails3Table.addCell(bankNamePhCell);
	      
	      Phrase dot = new Phrase(":", font8bold);
	      PdfPCell dotCell = new PdfPCell(dot);
	      dotCell.setBorder(0);
	      bankDetails3Table.addCell(dotCell);
	      
	      String bankName = "";
	      if ((comppayment.getPaymentBankName() != null) && 
	        (!comppayment.getPaymentBankName().equals(""))) {
	        bankName = comppayment.getPaymentBankName();
	      }
	      Phrase headingValue = new Phrase(bankName, font8);
	      PdfPCell headingValueCell = new PdfPCell(headingValue);
	      headingValueCell.setBorder(0);
	      headingValueCell.setHorizontalAlignment(0);
	      bankDetails3Table.addCell(headingValueCell);
	      

	      Phrase bankBranch = new Phrase("Branch", font8bold);
	      PdfPCell bankBranchCell = new PdfPCell(bankBranch);
	      bankBranchCell.setBorder(0);
	      bankDetails3Table.addCell(bankBranchCell);
	      bankDetails3Table.addCell(dotCell);
	      
	      String bankBranchValue = "";
	      if ((comppayment.getPaymentBranch() != null) && 
	        (!comppayment.getPaymentBranch().equals(""))) {
	        bankBranchValue = comppayment.getPaymentBranch();
	      }
	      Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
	      PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
	      bankBranchValuePhCell.setBorder(0);
	      bankBranchValuePhCell
	        .setHorizontalAlignment(0);
	      bankDetails3Table.addCell(bankBranchValuePhCell);
	      
	      Phrase bankAc = new Phrase("A/c No", font8bold);
	      PdfPCell bankAcCell = new PdfPCell(bankAc);
	      bankAcCell.setBorder(0);
	      bankDetails3Table.addCell(bankAcCell);
	      bankDetails3Table.addCell(dotCell);
	      
	      String bankAcNo = "";
	      if ((comppayment.getPaymentAccountNo() != null) && 
	        (!comppayment.getPaymentAccountNo().equals(""))) {
	        bankAcNo = comppayment.getPaymentAccountNo();
	      }
	      Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
	      PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
	      bankAcNoValueCell.setBorder(0);
	      bankAcNoValueCell.setHorizontalAlignment(0);
	      bankDetails3Table.addCell(bankAcNoValueCell);
	      
	      Phrase bankIFSC = new Phrase("IFSC Code", font8bold);
	      PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
	      bankIFSCCell.setBorder(0);
	      bankDetails3Table.addCell(bankIFSCCell);
	      bankDetails3Table.addCell(dotCell);
	      
	      String bankIFSCNo = "";
	      if ((comppayment.getPaymentIFSCcode() != null) && 
	        (!comppayment.getPaymentIFSCcode().equals(""))) {
	        bankIFSCNo = comppayment.getPaymentIFSCcode();
	      }
	      Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
	      PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
	      bankIFSCNoValueCell.setBorder(0);
	      bankIFSCNoValueCell.setHorizontalAlignment(0);
	      bankDetails3Table.addCell(bankIFSCNoValueCell);
	      
	      PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
//	      bankDetails3TableCell.setBorder(0);
	      bankDetails3TableCell.setBorderWidthBottom(0);
	      bankDetails3TableCell.setBorderWidthTop(0);
	      bankDetails3TableCell.setPaddingBottom(5);
	      bankDetailsTable.addCell(bankDetails3TableCell);
	      if(!isRemarkGiven){
	    	  bankDetailsTable.addCell(leftrightborderblankCell);
	    	  }
	      
	      PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
	      bankDetailsTableCell.setBorder(0);
	      middletTable.addCell(bankDetailsTableCell);
	    }
	    

	    PdfPCell middletTableCell = new PdfPCell(middletTable);
	    middletTableCell.setBorder(0);
	    
	    
	    PdfPTable qrTable = new PdfPTable(1);
	    qrTable.setWidthPercentage(100.0F);
	    
	    if(comppayment!=null && comppayment.getQrCodeDocument()!=null && comppayment.getQrCodeDocument().getUrl()!=null && !comppayment.getQrCodeDocument().getUrl().equals("")){

			
			DocumentUpload qrDocument = comppayment.getQrCodeDocument();
			String qrUrl;
			String qrenvironment = System
					.getProperty("com.google.appengine.runtime.environment");
			if (qrenvironment.equals("Production")) {
				String applicationId = System
						.getProperty("com.google.appengine.application.id");
				String version = System
						.getProperty("com.google.appengine.application.version");
				qrUrl = "http://" + version + "." + applicationId
						+ ".appspot.com/";
			} else {
				qrUrl = "http://localhost:8888";
			}
			PdfPCell qrCodeCell = null;
			Image qrImage = null;
			logger.log(Level.SEVERE, "hostUrl::" + qrUrl);
			try {
				logger.log(Level.SEVERE, "comppayment.getQrCodeDocument() "+comppayment.getQrCodeDocument());

				qrImage = Image.getInstance(new URL(qrUrl
						+ qrDocument.getUrl()));
				qrImage.scaleAbsolute(60, 60); 

				qrCodeCell = new PdfPCell(qrImage);
				qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				qrCodeCell.setBorderWidthBottom(0);
				qrCodeCell.setBorderWidthLeft(0);
				qrCodeCell.setBorderWidthTop(0);
				qrCodeCell.setPaddingBottom(4);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			float[] columnWidths = { 2f, 0.60f };
			
			PdfPTable pdfTable = new PdfPTable(1);
			pdfTable.setWidthPercentage(100);
			PdfPCell pdfPqrcodecell = new PdfPCell(qrCodeCell);
			Phrase phQrCodeTitle = new Phrase("Scan to Pay",font8bold);
			PdfPCell qrCodeTitleCell=new PdfPCell(phQrCodeTitle);
			qrCodeTitleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			qrCodeTitleCell.setPaddingBottom(4);
			qrCodeTitleCell.setBorderWidthBottom(0);
			qrCodeTitleCell.setBorderWidthLeft(0);
			
			Phrase phcompName = new Phrase(comppayment.getPaymentComName(),font8);
			PdfPCell phcompNameCell=new PdfPCell(phcompName);
			phcompNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			phcompNameCell.setPaddingBottom(4);
			phcompNameCell.setBorderWidthBottom(0);
			phcompNameCell.setBorderWidthLeft(0);
			phcompNameCell.setBorderWidthTop(0);
			
			if(!isRemarkGiven){
				pdfTable.addCell(blankCell);
		    }
			pdfTable.addCell(qrCodeTitleCell);
			pdfTable.addCell(phcompNameCell);
			pdfTable.addCell(pdfPqrcodecell);

			PdfPCell qrcell = new PdfPCell(pdfTable);
			qrcell.setBorderWidthTop(0);
			qrcell.setBorderWidthBottom(0);
			qrcell.setBorderWidthLeft(0);
			qrcell.setBorderWidthRight(0);
			
			qrTable.addCell(qrcell);
			
			
	    }
			
	    
	    
	    
	    PdfPCell qrTableCell = new PdfPCell(qrTable);
	    qrTableCell.setBorder(0);
	    
	    if(!onlyforOrion) {
	    bottomTable.addCell(lefttableCell);
	    }
	    bottomTable.addCell(middletTableCell);
	    if(isQrcodePresent&&onlyforOrion)
	    	bottomTable.addCell(qrTableCell);
	    
	    bottomTable.addCell(righttableCell);
	    


	    Paragraph para = new Paragraph(
	      "Note : This is computer generated invoice therefore no physical signature is required.", 
	      font8);
	    try
	    {
	      document.add(bottomTable);
	      
	      
	      if (imageSignCell != null&&!onlyforOrion) {
	        document.add(para);
	      }
	      
	      if ((noOfLines == 0) && (prouductCount != 0)) {
	        createAnnexureForRemainingProduct(prouductCount);
	      }
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  

	  private void createAnnexureForRemainingProduct(int count)
	  {
	    Paragraph para = new Paragraph("Annexure 1 :", font10bold);
	    para.setAlignment(0);
	    try
	    {
	      document.add(Chunk.NEWLINE);
	      document.add(Chunk.NEXTPAGE);
	      document.add(para);
	      document.add(Chunk.NEWLINE);
	    }
	    catch (DocumentException e)
	    {
	      e.printStackTrace();
	    }
	    if (con.isContractRate()) {
	      createProductDetailsForRate();
	    } else {
	      createProductDetails();
	    }
	    if (con.isContractRate()) {
	      createProductDetailsMOreThanFiveForRate(prouductCount);
	    } else {
	      createProductDetailsMOreThanFive(prouductCount);
	    }
	  }
	  

	  private void createProductDetailsMOreThanFiveForRate(int count)
	  {
	    for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {
	      PdfPTable productTable = new PdfPTable(8);
	      productTable.setWidthPercentage(100.0F);
	      try {
	        productTable.setWidths(column8ProdCollonWidth);
	      }
	      catch (DocumentException e) {
	        e.printStackTrace();
	      }
	      
	      int srNoVal = i + 1;
	      Phrase srNo = new Phrase(srNoVal+" ", font10);
	      PdfPCell srNoCell = new PdfPCell(srNo);
	      srNoCell.setHorizontalAlignment(1);
	      productTable.addCell(srNoCell);//1
	      
	      Phrase serviceName = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getProdName().trim(), 
	        font10);
	      PdfPCell serviceNameCell = new PdfPCell(serviceName);
	      
	      productTable.addCell(serviceNameCell);//2
	      
	      Phrase hsnCode = null;
	      if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null)
	      {
	        if (!invoiceentity.getSalesOrderProducts().get(i).getHsnCode().trim().equals(""))
	        {
	          hsnCode = new Phrase(
	            invoiceentity.getSalesOrderProducts().get(i).getHsnCode().trim(), font10);
	        
	          }
	        }
	      ServiceProduct serviceProduct = 
	      




	        (ServiceProduct)ObjectifyService.ofy().load().type(ServiceProduct.class).filter("companyId", comp.getCompanyId()).filter("productCode", invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim()).first().now();
	      if (serviceProduct.getHsnNumber() != null) {
	        hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
	      } else {
	        hsnCode = new Phrase("", font6);
	      }
	     
	      PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
	      
	      productTable.addCell(hsnCodeCell);//3
	      
	      String startDateStr = "";String endDateStr = "";
	      for (int j = 0; j < con.getItems().size(); j++)
	      {
	        System.out.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()" + 
	          invoiceentity.getSalesOrderProducts().get(i)
	          .getPrduct().getCount());
	        System.out
	          .println("con.getItems().get(j).getPrduct().getCount()" + 
	          con.getItems().get(j).getPrduct().getCount());
	        System.out
	          .println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()" + 
	          invoiceentity.getSalesOrderProducts().get(i)
	          .getOrderDuration());
	        System.out
	          .println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()" + 
	          invoiceentity.getSalesOrderProducts().get(i)
	          .getOrderDuration());
	        System.out.println("con.getItems().get(j).getDuration()" + 
	          con.getItems().get(j).getDuration());
	        
	        SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
	          "dd/MM/yyyy");
	        TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	        simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
	        if (invoiceentity.getSalesOrderProducts().get(i).getPrduct()
	          .getCount() == con.getItems().get(j).getPrduct()
	          .getCount()) {
	          if (invoiceentity.getSalesOrderProducts().get(i)
	            .getOrderDuration() == con.getItems().get(j)
	            .getDuration())
	            if (con.getItems().get(j).getEndDate() != null) {
	              startDateStr = simpleDateFmt.format(
	                con.getItems().get(j).getStartDate());
	              endDateStr = simpleDateFmt.format(con.getItems().get(j)
	                .getEndDate());
	            } else {
	              Calendar c = Calendar.getInstance();
	              c.setTime(con.getItems().get(j).getStartDate());
	              c.add(5, con.getItems().get(j)
	                .getDuration());
	              Date endDt = c.getTime();
	              startDateStr = simpleDateFmt.format(
	                con.getItems().get(j).getStartDate());
	              endDateStr = simpleDateFmt.format(endDt);
	            }
	        }
	      }
	      Phrase startDate_endDate = new Phrase(startDateStr + " - " + 
	        endDateStr, font6);
	      
	      PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
	      startDate_endDateCell.setHorizontalAlignment(1);
	      


	      Phrase qty = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getArea(), 
	        font10);
	      PdfPCell qtyCell = new PdfPCell(qty);
	      
	      qtyCell.setHorizontalAlignment(1);
	      productTable.addCell(qtyCell);
	      
	      Phrase uom = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim(), font10);
	      PdfPCell uomCell = new PdfPCell(uom);
	      uomCell.setHorizontalAlignment(1);
	      productTable.addCell(uomCell);
	      
	      Phrase rate = new Phrase(df.format(
	        invoiceentity.getSalesOrderProducts().get(i).getPrice()), 
	        font6);
	      PdfPCell rateCell = new PdfPCell(rate);
	      rateCell.setHorizontalAlignment(2);
	      
	      Phrase blank = new Phrase(" ", font6);
	      PdfPCell blankCell = new PdfPCell(blank);
	      blankCell.setHorizontalAlignment(2);
	      if (consolidatePrice) {
	        if (i == count) {
	          blankCell.setBorderWidthBottom(0.0F);
	          productTable.addCell(blankCell);
	        } else {
	          blankCell.setBorderWidthTop(0.0F);
	          productTable.addCell(blankCell);
	        }
	      } else {
	        productTable.addCell(rateCell);
	      }
	      double amountValue = invoiceentity.getSalesOrderProducts().get(i)
	        .getPrice() * 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getQuantity();
	      double disPercentTotalAmount = 0;double disConTotalAmount = 0;
	      if ((invoiceentity.getSalesOrderProducts().get(i)
	        .getProdPercDiscount() == null) || 
	        (invoiceentity.getSalesOrderProducts().get(i)
	        .getProdPercDiscount().doubleValue() == 0)) {
	        disPercentTotalAmount = 0;
	      } else {
	        disPercentTotalAmount = getPercentAmount(
	          invoiceentity.getSalesOrderProducts().get(i), false);
	      }
	      
	      if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
	        disConTotalAmount = 0;
	      } else {
	        disConTotalAmount = 
	          invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt();
	      }
	      
	      totalAmount += amountValue;
	      






	      Phrase disc = new Phrase(df.format(
	        invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()), 
	        font6);
	      PdfPCell discCell = new PdfPCell(disc);
	      discCell.setHorizontalAlignment(2);
	      if (consolidatePrice) {
	        if (i == count) {
	          blankCell.setBorderWidthBottom(0.0F);
	          productTable.addCell(blankCell);
	        } else {
	          blankCell.setBorderWidthTop(0.0F);
	          productTable.addCell(blankCell);
	        }
	      } else {
	        productTable.addCell(discCell);
	      }
	      

	      double taxValue = 0;
	      Phrase taxableValue; 
	      if (invoiceentity.getSalesOrderProducts().get(i)
	        .getBasePaymentAmount() != 0) {
	       taxableValue = new Phrase(df.format(
	          invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()), 
	          font6);
	        taxValue = invoiceentity.getSalesOrderProducts().get(i)
	          .getBasePaymentAmount();
	      } else {
	        taxableValue = new Phrase(df.format(
	          invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount()), 
	          font6);
	        taxValue = invoiceentity.getSalesOrderProducts().get(i)
	          .getBaseBillingAmount();
	      }
	      
	      PdfPCell taxableValueCell = new PdfPCell(taxableValue);
	      taxableValueCell.setHorizontalAlignment(2);
	      if (consolidatePrice) {
	        if (i == count) {
	          blankCell.setBorderWidthBottom(0.0F);
	          productTable.addCell(blankCell);
	        } else {
	          blankCell.setBorderWidthTop(0.0F);
	          productTable.addCell(blankCell);
	        }
	      } else {
	        productTable.addCell(taxableValueCell);
	      }
	      

	      String premisesVal = "";
	      for (int j = 0; j < con.getItems().size(); j++) {
	        if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == 
	          con.getItems().get(j).getPrduct().getCount()) {
	          premisesVal = con.getItems().get(j).getPremisesDetails();
	        }
	      }
	      
	      if ((premisesVal != null) && 
	        (printPremiseDetails) && (!premisesVal.equals(""))) {
	        Phrase blankValPhrs = new Phrase(" ", font8);
	        PdfPCell premiseCellBlank = new PdfPCell(blankValPhrs);
	        premiseCellBlank.setColspan(1);
	        
	        Phrase premisesValPhrs = new Phrase("Premise Details : " + 
	          premisesVal, font10);
	        PdfPCell premiseCell = new PdfPCell(premisesValPhrs);
	        premiseCell.setColspan(7);
	        productTable.addCell(premiseCell);
	      }
	      try
	      {
	        document.add(productTable);
	      }
	      catch (DocumentException e) {
	        e.printStackTrace();
	      }
	    }
	  }
	  




















































































































































































































































































































































































































































	  private void createProductDetailsMOreThanFive(int count)
	  {

			for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++) {

				PdfPTable productTable = new PdfPTable(8);
				productTable.setWidthPercentage(100);
				try {
					productTable.setWidths(column8SerProdCollonWidth);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// 1
				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font10);
				PdfPCell srNoCell = new PdfPCell(srNo);
				srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(srNoCell);
				// 2
				Phrase serviceName = new Phrase(invoiceentity
						.getSalesOrderProducts().get(i).getProdName().trim(),
						font10);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.addElement();
				productTable.addCell(serviceNameCell);

				Phrase noOfServices = new Phrase(invoiceentity
						.getSalesOrderProducts().get(i).getOrderServices()
						+ "", font10);
				PdfPCell noOfServicesCell = new PdfPCell(noOfServices);
				noOfServicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				// serviceNameCell.addElement();
				productTable.addCell(noOfServicesCell);
				// 3
				Phrase hsnCode = null;
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null
						&& !invoiceentity.getSalesOrderProducts().get(i)
								.getHsnCode().trim().equals("")) {

					hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
							.get(i).getHsnCode().trim(), font10);
					// if(hsC)
				} else {
					ServiceProduct serviceProduct = ofy()
							.load()
							.type(ServiceProduct.class)
							.filter("companyId", comp.getCompanyId())
							.filter("productCode",
									invoiceentity.getSalesOrderProducts().get(i)
									.getProdCode().trim()).first().now();
					if (serviceProduct.getHsnNumber() != null) {
						hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
					} else {
						hsnCode = new Phrase("", font6);
					}
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.addElement();
				productTable.addCell(hsnCodeCell);

				String startDateStr = "", endDateStr = "";
				for (int j = 0; j < con.getItems().size(); j++) {
					System.out
							.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()"
									+ invoiceentity.getSalesOrderProducts().get(i)
											.getPrduct().getCount());
					System.out
							.println("con.getItems().get(j).getPrduct().getCount()"
									+ con.getItems().get(j).getPrduct().getCount());
					System.out
							.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
									+ invoiceentity.getSalesOrderProducts().get(i)
											.getOrderDuration());
					System.out
							.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
									+ invoiceentity.getSalesOrderProducts().get(i)
											.getOrderDuration());
					System.out.println("con.getItems().get(j).getDuration()"
							+ con.getItems().get(j).getDuration());

					SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
							"dd/MM/yyyy");
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
					if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
							.getCount() == con.getItems().get(j).getPrduct()
							.getCount())
							&& (invoiceentity.getSalesOrderProducts().get(i)
									.getOrderDuration() == con.getItems().get(j)
									.getDuration())) {
						if (con.getItems().get(j).getEndDate() != null) {
							startDateStr = simpleDateFmt.format(con.getItems()
									.get(j).getStartDate());
							endDateStr = simpleDateFmt.format(con.getItems().get(j)
									.getEndDate());
						} else {
							Calendar c = Calendar.getInstance();
							c.setTime(con.getItems().get(j).getStartDate());
							c.add(Calendar.DATE, con.getItems().get(j)
									.getDuration());
							Date endDt = c.getTime();
							startDateStr = simpleDateFmt.format(con.getItems()
									.get(j).getStartDate());
							endDateStr = simpleDateFmt.format(endDt);
						}
					}
				}
				Phrase startDate_endDate = new Phrase(startDateStr + " - "
						+ endDateStr, font6);

				PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
				startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

				productTable.addCell(startDate_endDateCell);
				// Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
				// .get(i).getUnitOfMeasurement().trim(), font10);
				// PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				// productTable.addCell(uomCell);

				Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getQuantity()
						+ "", font10);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.addElement();
				qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				// productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getPrice())
						+ "", font6);
				PdfPCell rateCell = new PdfPCell(rate);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(rateCell);

				double amountValue = invoiceentity.getSalesOrderProducts().get(i)
						.getPrice()
						* invoiceentity.getSalesOrderProducts().get(i)
								.getQuantity();
				double disPercentTotalAmount = 0, disConTotalAmount = 0;
				if (invoiceentity.getSalesOrderProducts().get(i)
						.getProdPercDiscount() == null
						|| invoiceentity.getSalesOrderProducts().get(i)
								.getProdPercDiscount() == 0) {
					disPercentTotalAmount = 0;
				} else {
					disPercentTotalAmount = getPercentAmount(invoiceentity
							.getSalesOrderProducts().get(i), false);
				}

				if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
					disConTotalAmount = 0;
				} else {
					disConTotalAmount = invoiceentity.getSalesOrderProducts()
							.get(i).getDiscountAmt();
				}

				totalAmount = totalAmount + amountValue;
				// Phrase amount = new Phrase(df.format(amountValue
				// - disPercentTotalAmount - disConTotalAmount)
				// + "", font6);
				// PdfPCell amountCell = new PdfPCell(amount);
				// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(amountCell);

				Phrase disc = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getFlatDiscount())
						+ "", font6);
				PdfPCell discCell = new PdfPCell(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(discCell);

				Phrase taxableValue;
				double taxValue = 0;
				if (invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount() != 0) {
					taxableValue = new Phrase(df.format(invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount())
							+ "", font6);
					taxValue = invoiceentity.getSalesOrderProducts().get(i)
							.getBasePaymentAmount();
				} else {
					taxableValue = new Phrase(df.format(invoiceentity
							.getSalesOrderProducts().get(i).getBaseBillingAmount())
							+ "", font6);
					taxValue = invoiceentity.getSalesOrderProducts().get(i)
							.getBaseBillingAmount();
				}
				// totalAssAmount=totalAssAmount+taxValue;
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				String premisesVal = "";
				for (int j = 0; j < con.getItems().size(); j++) {
					if ((invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
							.getItems().get(j).getPrduct().getCount())
							&& (invoiceentity.getSalesOrderProducts().get(i)
									.getOrderDuration() == con.getItems().get(j)
									.getDuration())) {
						premisesVal = con.getItems().get(j).getPremisesDetails();
					}

				}
				if (premisesVal != null) {
					if (printPremiseDetails && !premisesVal.equals("")) {

						Phrase premisesBlank = new Phrase(" ", font10);
						PdfPCell premiseCellBlankCell = new PdfPCell(premisesBlank);
						premiseCellBlankCell.setColspan(1);
						productTable.addCell(premiseCellBlankCell);

						Phrase premisesValPhrs = new Phrase("Premise Details : "
								+ premisesVal, font10);
						PdfPCell premiseCell = new PdfPCell(premisesValPhrs);
						premiseCell.setColspan(7);
						productTable.addCell(premiseCell);
					}
				}
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage() != 0
				// && invoiceentity.getSalesOrderProducts().get(i)
				// .getServiceTax().getPercentage() != 0) {
				// logger.log(Level.SEVERE, "Inside NON Zero:::::");
				// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
				// .getTaxPrintName().equalsIgnoreCase("IGST")) {
				//
				// double taxAmount = getTaxAmount(invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount(), invoiceentity
				// .getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage());
				// double indivTotalAmount = invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount()
				// + taxAmount;
				// totalAmount = totalAmount + indivTotalAmount;
				//
				// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
				// font10);
				// PdfPCell igstRateValCell = new PdfPCell();
				// // igstRateValCell.setBorder(0);
				// igstRateValCell.addElement(igstRateVal);
				//
				// Phrase igstRate = new Phrase(invoiceentity
				// .getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage()
				// + "", font10);
				// PdfPCell igstRateCell = new PdfPCell();
				// // igstRateCell.setBorder(0);
				// // igstRateCell.setColspan(2);
				// igstRateCell.addElement(igstRate);
				//
				// /* for Cgst */
				//
				// Phrase cgst = new Phrase("-", font10);
				// PdfPCell cell = new PdfPCell(cgst);
				// // cell.addElement(cgst);
				// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				//
				// /* for Sgst */
				// Phrase sgst = new Phrase("-", font10);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				//
				// productTable.addCell(igstRateCell);
				// productTable.addCell(igstRateValCell);
				//
				// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
				// + "", font10);
				// PdfPCell totalCell = new PdfPCell();
				// // totalCell.setColspan(16);
				// totalCell.addElement(totalPhrase);
				// productTable.addCell(totalCell);
				// // totalCell.setBorderWidthBottom(0);
				// // totalCell.setBorderWidthTop(0);
				// String premisesVal = "";
				// for (int j = 0; j < con.getItems().size(); j++) {
				// if (invoiceentity.getSalesOrderProducts().get(i)
				// .getProdId() == con.getItems().get(j)
				// .getPrduct().getCount()) {
				// premisesVal = con.getItems().get(j)
				// .getPremisesDetails();
				// }
				//
				// }
				// if (printPremiseDetails) {
				// Phrase premisesValPhrs = new Phrase(
				// "Premise Details : " + premisesVal, font8);
				// PdfPCell premiseCell = new PdfPCell();
				// premiseCell.setColspan(8);
				// premiseCell.addElement(premisesValPhrs);
				//
				// productTable.addCell(premiseCell);
				// }
				// try {
				// document.add(productTable);
				// } catch (DocumentException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// } else if (invoiceentity.getSalesOrderProducts().get(i)
				// .getServiceTax().getTaxPrintName()
				// .equalsIgnoreCase("IGST")) {
				//
				// double taxAmount = getTaxAmount(invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount(), invoiceentity
				// .getSalesOrderProducts().get(i).getServiceTax()
				// .getPercentage());
				// double indivTotalAmount = invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount()
				// + taxAmount;
				// totalAmount = totalAmount + indivTotalAmount;
				//
				// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
				// font10);
				// PdfPCell igstRateValCell = new PdfPCell();
				// // igstRateValCell.setBorder(0);
				// igstRateValCell.addElement(igstRateVal);
				//
				// Phrase igstRate = new Phrase(invoiceentity
				// .getSalesOrderProducts().get(i).getServiceTax()
				// .getPercentage()
				// + "", font10);
				// PdfPCell igstRateCell = new PdfPCell();
				// // igstRateCell.setBorder(0);
				// // igstRateCell.setColspan(2);
				// igstRateCell.addElement(igstRate);
				//
				// /* for Cgst */
				//
				// Phrase cgst = new Phrase("-", font10);
				// PdfPCell cell = new PdfPCell(cgst);
				// // cell.addElement(cgst);
				// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				//
				// /* for Sgst */
				// Phrase sgst = new Phrase("-", font10);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				//
				// productTable.addCell(igstRateCell);
				// productTable.addCell(igstRateValCell);
				//
				// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
				// + "", font10);
				// PdfPCell totalCell = new PdfPCell();
				// // totalCell.setColspan(16);
				// totalCell.addElement(totalPhrase);
				// productTable.addCell(totalCell);
				// // totalCell.setBorderWidthBottom(0);
				// // totalCell.setBorderWidthTop(0);
				// String premisesVal = "";
				// for (int j = 0; j < con.getItems().size(); j++) {
				// if (invoiceentity.getSalesOrderProducts().get(i)
				// .getProdId() == con.getItems().get(j)
				// .getPrduct().getCount()) {
				// premisesVal = con.getItems().get(j)
				// .getPremisesDetails();
				// }
				//
				// }
				// if (printPremiseDetails) {
				// Phrase premisesValPhrs = new Phrase(
				// "Premise Details : " + premisesVal, font8);
				// PdfPCell premiseCell = new PdfPCell();
				// premiseCell.setColspan(8);
				// premiseCell.addElement(premisesValPhrs);
				//
				// productTable.addCell(premiseCell);
				// }
				// try {
				// document.add(productTable);
				// } catch (DocumentException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// } else {
				//
				// if (invoiceentity.getSalesOrderProducts().get(i)
				// .getVatTax().getTaxPrintName()
				// .equalsIgnoreCase("CGST")) {
				//
				// double ctaxValue = getTaxAmount(invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount(), invoiceentity
				// .getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage());
				//
				// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
				// + "", font10);
				// PdfPCell cgstRateValCell = new PdfPCell();
				// // cgstRateValCell.setBorder(0);
				// // cgstRateValCell.setBorderWidthBottom(0);
				// // cgstRateValCell.setBorderWidthTop(0);
				// // cgstRateValCell.setBorderWidthRight(0);
				// cgstRateValCell.addElement(cgstRateVal);
				//
				// Phrase cgstRate = new Phrase(invoiceentity
				// .getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage()
				// + "", font10);
				// PdfPCell cgstRateCell = new PdfPCell();
				// // cgstRateCell.setBorder(0);
				// // cgstRateCell.setBorderWidthBottom(0);
				// // cgstRateCell.setBorderWidthTop(0);
				// // cgstRateCell.setBorderWidthLeft(0);
				// cgstRateCell.addElement(cgstRate);
				// productTable.addCell(cgstRateCell);
				// productTable.addCell(cgstRateValCell);
				//
				// double staxValue = getTaxAmount(invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount(), invoiceentity
				// .getSalesOrderProducts().get(i).getServiceTax()
				// .getPercentage());
				// Phrase sgstRateVal = new Phrase(df.format(staxValue)
				// + "", font10);
				// PdfPCell sgstRateValCell = new PdfPCell();
				// // sgstRateValCell.setBorder(0);
				// // sgstRateValCell.setBorderWidthBottom(0);
				// // sgstRateValCell.setBorderWidthTop(0);
				// // sgstRateValCell.setBorderWidthRight(0);
				// sgstRateValCell.addElement(sgstRateVal);
				//
				// Phrase sgstRate = new Phrase(invoiceentity
				// .getSalesOrderProducts().get(i).getServiceTax()
				// .getPercentage()
				// + "", font10);
				// PdfPCell sgstRateCell = new PdfPCell();
				// // sgstRateCell.setBorder(0);
				// // sgstRateCell.setBorderWidthBottom(0);
				// // sgstRateCell.setBorderWidthTop(0);
				// // sgstRateCell.setBorderWidthLeft(0);
				// sgstRateCell.addElement(sgstRate);
				// productTable.addCell(sgstRateCell);
				// productTable.addCell(sgstRateValCell);
				//
				// Phrase igstRateVal = new Phrase("-", font10);
				// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
				// // igstRateValCell.setBorder(0);
				// // igstRateValCell.setBorderWidthBottom(0);
				// // igstRateValCell.setBorderWidthTop(0);
				// // igstRateValCell.setBorderWidthRight(0);
				// // igstRateValCell.addElement(igstRateVal);
				// igstRateValCell
				// .setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(igstRateValCell);
				//
				// Phrase igstRate = new Phrase("-", font10);
				// PdfPCell igstRateCell = new PdfPCell(igstRate);
				// // igstRateCell.setBorder(0);
				// // igstRateCell.setBorderWidthBottom(0);
				// // igstRateCell.setBorderWidthTop(0);
				// // igstRateCell.setBorderWidthLeft(0);
				// // igstRateCell.addElement(igstRate);
				// igstRateCell
				// .setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(igstRateCell);
				//
				// double indivTotalAmount = invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount()
				// + ctaxValue + staxValue;
				// totalAmount = totalAmount + indivTotalAmount;
				// Phrase totalPhrase = new Phrase(
				// df.format(indivTotalAmount) + "", font10);
				// PdfPCell totalCell = new PdfPCell();
				// // totalCell.setColspan(16);
				// // totalCell.setBorder(0);
				// // totalCell.setBorderWidthBottom(0);
				// // totalCell.setBorderWidthTop(0);
				// totalCell.addElement(totalPhrase);
				// productTable.addCell(totalCell);
				// // totalCell.setBorderWidthBottom(0);
				// // totalCell.setBorderWidthTop(0);
				// String premisesVal = "";
				// for (int j = 0; j < con.getItems().size(); j++) {
				// if (invoiceentity.getSalesOrderProducts().get(i)
				// .getProdId() == con.getItems().get(j)
				// .getPrduct().getCount()) {
				// premisesVal = con.getItems().get(j)
				// .getPremisesDetails();
				// }
				//
				// }
				// if (printPremiseDetails) {
				// Phrase premisesValPhrs = new Phrase(
				// "Premise Details : " + premisesVal, font8);
				// PdfPCell premiseCell = new PdfPCell();
				// premiseCell.setColspan(8);
				// premiseCell.addElement(premisesValPhrs);
				// productTable.addCell(premiseCell);
				// }
				// try {
				// document.add(productTable);
				// } catch (DocumentException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				//
				// } else if (invoiceentity.getSalesOrderProducts().get(i)
				// .getVatTax().getTaxPrintName()
				// .equalsIgnoreCase("SGST")) {
				//
				// double ctaxValue = getTaxAmount(invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount(), invoiceentity
				// .getSalesOrderProducts().get(i).getServiceTax()
				// .getPercentage());
				//
				// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
				// + "", font10);
				// PdfPCell cgstRateValCell = new PdfPCell();
				// // cgstRateValCell.setBorder(0);
				// // cgstRateValCell.setBorderWidthBottom(0);
				// // cgstRateValCell.setBorderWidthTop(0);
				// // cgstRateValCell.setBorderWidthRight(0);
				// cgstRateValCell.addElement(cgstRateVal);
				//
				// Phrase cgstRate = new Phrase(invoiceentity
				// .getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage()
				// + "", font10);
				// PdfPCell cgstRateCell = new PdfPCell();
				// // cgstRateCell.setBorder(0);
				// // cgstRateCell.setBorderWidthBottom(0);
				// // cgstRateCell.setBorderWidthTop(0);
				// // cgstRateCell.setBorderWidthLeft(0);
				// cgstRateCell.addElement(cgstRate);
				// productTable.addCell(cgstRateCell);
				// productTable.addCell(cgstRateValCell);
				//
				// double staxValue = getTaxAmount(invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount(), invoiceentity
				// .getSalesOrderProducts().get(i).getVatTax()
				// .getPercentage());
				// Phrase sgstRateVal = new Phrase(df.format(staxValue)
				// + "", font10);
				// PdfPCell sgstRateValCell = new PdfPCell();
				// // sgstRateValCell.setBorder(0);
				// // sgstRateValCell.setBorderWidthBottom(0);
				// // sgstRateValCell.setBorderWidthTop(0);
				// // sgstRateValCell.setBorderWidthRight(0);
				// sgstRateValCell.addElement(sgstRateVal);
				//
				// Phrase sgstRate = new Phrase(invoiceentity
				// .getSalesOrderProducts().get(i).getServiceTax()
				// .getPercentage()
				// + "", font10);
				// PdfPCell sgstRateCell = new PdfPCell();
				// // sgstRateCell.setBorder(0);
				// // sgstRateCell.setBorderWidthBottom(0);
				// // sgstRateCell.setBorderWidthTop(0);
				// // sgstRateCell.setBorderWidthLeft(0);
				// sgstRateCell.addElement(sgstRate);
				// productTable.addCell(sgstRateCell);
				// productTable.addCell(sgstRateValCell);
				//
				// Phrase igstRateVal = new Phrase("-", font10);
				// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
				// igstRateValCell
				// .setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(igstRateValCell);
				//
				// Phrase igstRate = new Phrase("-", font10);
				// PdfPCell igstRateCell = new PdfPCell(igstRate);
				// // igstRateCell.setBorder(0);
				// // igstRateCell.setBorderWidthBottom(0);
				// // igstRateCell.setBorderWidthTop(0);
				// // igstRateCell.setBorderWidthLeft(0);
				// // igstRateCell.addElement(igstRate);
				// igstRateCell
				// .setHorizontalAlignment(Element.ALIGN_RIGHT);
				// productTable.addCell(igstRateCell);
				//
				// double indivTotalAmount = invoiceentity
				// .getSalesOrderProducts().get(i)
				// .getBasePaymentAmount()
				// + ctaxValue + staxValue;
				// totalAmount = totalAmount + indivTotalAmount;
				// Phrase totalPhrase = new Phrase(
				// df.format(indivTotalAmount) + "", font10);
				// PdfPCell totalCell = new PdfPCell();
				// // totalCell.setColspan(16);
				// // totalCell.setBorder(0);
				// // totalCell.setBorderWidthBottom(0);
				// // totalCell.setBorderWidthTop(0);
				// totalCell.addElement(totalPhrase);
				//
				// productTable.addCell(totalCell);
				// String premisesVal = "";
				// for (int j = 0; j < con.getItems().size(); j++) {
				// if (invoiceentity.getSalesOrderProducts().get(i)
				// .getProdId() == con.getItems().get(j)
				// .getPrduct().getCount()) {
				// premisesVal = con.getItems().get(j)
				// .getPremisesDetails();
				// }
				//
				// }
				// if (printPremiseDetails) {
				// Phrase premisesValPhrs = new Phrase(
				// "Premise Details : " + premisesVal, font8);
				// PdfPCell premiseCell = new PdfPCell();
				// premiseCell.setColspan(8);
				// premiseCell.addElement(premisesValPhrs);
				// productTable.addCell(premiseCell);
				// }
				//
				// try {
				// document.add(productTable);
				// } catch (DocumentException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// }
				// }
				// } else {
				// logger.log(Level.SEVERE, "Inside Zero else:::::");
				//
				// PdfPCell cell = new PdfPCell(new Phrase("NA", font10));
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				// productTable.addCell(cell);
				// Phrase totalPhrase = new Phrase(df.format(invoiceentity
				// .getSalesOrderProducts().get(i).getTotalAmount())
				// + "", font10);
				// PdfPCell totalCell = new PdfPCell();
				// // totalCell.setColspan(16);
				// // totalCell.setBorder(0);
				// // totalCell.setBorderWidthBottom(0);
				// // totalCell.setBorderWidthTop(0);
				// totalCell.addElement(totalPhrase);
				// productTable.addCell(totalCell);
				//
				//
				// try {
				// document.add(productTable);
				// } catch (DocumentException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// }
			}
		}
	  






	  private void createFooterTaxPart()
	  {
			// TODO Auto-generated method stub
			PdfPTable pdfPTaxTable = new PdfPTable(2);
			pdfPTaxTable.setWidthPercentage(100);
			/** Date 28-07-2018 By Vijay for other payment terms and other charge table not required as per sonu
			 * so i have commneted old code and added tax amount in words 
			 */
//			float[] columnThreePartWidths = { 1f, 1f, 1f };
			float[] columnThreePartWidths = { 2f, 1f };

			try {
				pdfPTaxTable.setWidths(columnThreePartWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}


			/**
			 * Date 28-07-2018 By Vijay
			 * Des :- As per Sonu Payment terms not required to display this so i have commented below code
			 */
			
			
//			/**
//			 * rohan added this code for payment terms for invoice details
//			 */
//
//			float[] column3widths = { 2f, 2f, 6f };
//			PdfPTable leftTable = new PdfPTable(3);
//			leftTable.setWidthPercentage(100);
//			try {
//				leftTable.setWidths(column3widths);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			// heading
//
//			Phrase day = new Phrase("Day", font8bold);
//			PdfPCell dayCell = new PdfPCell(day);
//			dayCell.setBorder(0);
//
//			Phrase percent = new Phrase("Percent", font8bold);
//			PdfPCell percentCell = new PdfPCell(percent);
//			percentCell.setBorder(0);
//
//			Phrase comment = new Phrase("Comment", font8bold);
//			PdfPCell commentCell = new PdfPCell(comment);
//			commentCell.setBorder(0);
//
//			// Phrase terms = new Phrase("Payment Terms", font8bold);
//			// PdfPCell termsCell = new PdfPCell(terms);
//			//
//			// Phrase termsBlank = new Phrase(" ", font10bold);
//			// PdfPCell termsBlankCell = new PdfPCell(termsBlank);
//			// termsBlankCell.setBorder(0);
//			// leftTable.addCell(termsCell);
//			// leftTable.addCell(termsBlankCell);
//			// leftTable.addCell(termsBlankCell);
//			leftTable.addCell(dayCell);
//			leftTable.addCell(percentCell);
//			leftTable.addCell(commentCell);
//
//			// Values
//			for (int i = 0; i < invoiceentity.getArrPayTerms().size(); i++) {
//				Phrase dayValue = new Phrase(invoiceentity.getArrPayTerms().get(i)
//						.getPayTermDays()
//						+ "", font8);
//				PdfPCell dayValueCell = new PdfPCell(dayValue);
//				dayValueCell.setBorder(0);
//				leftTable.addCell(dayValueCell);
//
//				Phrase percentValue = new Phrase(df.format(invoiceentity
//						.getArrPayTerms().get(i).getPayTermPercent()), font8);
//				PdfPCell percentValueCell = new PdfPCell(percentValue);
//				percentValueCell.setBorder(0);
//				leftTable.addCell(percentValueCell);
//
//				Phrase commentValue = new Phrase(invoiceentity.getArrPayTerms()
//						.get(i).getPayTermComment(), font8);
//				PdfPCell commentValueCell = new PdfPCell(commentValue);
//				commentValueCell.setBorder(0);
//				leftTable.addCell(commentValueCell);
//			}

			/**
			 * ends here
			 */
			
			// try {
			// document.add(leftTable);
			// } catch (DocumentException e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }

			PdfPTable rightTable = new PdfPTable(1);
			rightTable.setWidthPercentage(100);

			PdfPTable rightInnerTable = new PdfPTable(3);
			rightInnerTable.setWidthPercentage(100);
			try {
				rightInnerTable.setWidths(columnCollonGSTWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Phrase colon = new Phrase(":", font10bold);
			PdfPCell colonCell = new PdfPCell(colon);
			colonCell.setBorder(0);

			Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax",
					font10bold);
			PdfPCell amtB4TaxCell = new PdfPCell();
			amtB4TaxCell.setBorder(0);
			amtB4TaxCell.addElement(amtB4Taxphrase);

			Phrase amtB4TaxValphrase = new Phrase(totalAmount + "", font10bold);
			PdfPCell amtB4ValTaxCell = new PdfPCell();
			amtB4ValTaxCell.setBorder(0);
			amtB4ValTaxCell.addElement(amtB4TaxValphrase);

			double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
			for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {
				logger.log(Level.SEVERE,"Taxname ="+invoiceentity.getBillingTaxes().get(i).getTaxChargeName());
				if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName()
						.equalsIgnoreCase("IGST")) {
					igstTotalVal = igstTotalVal
							+ invoiceentity.getBillingTaxes().get(i)
									.getPayableAmt();
					Phrase IGSTphrase = new Phrase("IGST @"
							+ invoiceentity.getBillingTaxes().get(i)
									.getTaxChargePercent() + " %", font10bold);
					PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
					IGSTphraseCell.setBorder(0);

					Phrase IGSTValphrase = new Phrase(df.format(invoiceentity
							.getBillingTaxes().get(i).getPayableAmt())
							+ "", font10);
					PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
					IGSTValphraseCell.setBorder(0);
					IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

					rightInnerTable.addCell(IGSTphraseCell);
					rightInnerTable.addCell(colonCell);
					rightInnerTable.addCell(IGSTValphraseCell);

				} else if (invoiceentity.getBillingTaxes().get(i)
						.getTaxChargeName().equalsIgnoreCase("SGST")) {
					sgstTotalVal = sgstTotalVal
							+ invoiceentity.getBillingTaxes().get(i)
									.getPayableAmt();

					Phrase SGSTphrase = new Phrase("SGST @"
							+ invoiceentity.getBillingTaxes().get(i)
									.getTaxChargePercent() + " %", font10bold);
					PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
					SGSTphraseCell.setBorder(0);
					// SGSTphraseCell.addElement(SGSTphrase);

					Phrase SGSTValphrase = new Phrase(df.format(invoiceentity
							.getBillingTaxes().get(i).getPayableAmt())
							+ "", font10);
					PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
					SGSTValphraseCell.setBorder(0);
					SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

					rightInnerTable.addCell(SGSTphraseCell);
					rightInnerTable.addCell(colonCell);
					rightInnerTable.addCell(SGSTValphraseCell);
				} else if (invoiceentity.getBillingTaxes().get(i)
						.getTaxChargeName().equalsIgnoreCase("CGST")) {
					cgstTotalVal = cgstTotalVal
							+ invoiceentity.getBillingTaxes().get(i)
									.getPayableAmt();

					Phrase CGSTphrase = new Phrase("CGST @"
							+ invoiceentity.getBillingTaxes().get(i)
									.getTaxChargePercent() + " %", font10bold);
					PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
					CGSTphraseCell.setBorder(0);
					// CGSTphraseCell.addElement(CGSTphrase);

					Phrase CGSTValphrase = new Phrase(df.format(invoiceentity
							.getBillingTaxes().get(i).getPayableAmt())
							+ "", font10);
					PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
					CGSTValphraseCell.setBorder(0);
					CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					// CGSTValphraseCell.addElement(CGSTValphrasePara);

					rightInnerTable.addCell(CGSTphraseCell);
					rightInnerTable.addCell(colonCell);
					rightInnerTable.addCell(CGSTValphraseCell);
				}
				logger.log(Level.SEVERE,"cgstTotalVal="+cgstTotalVal+" sgstTotalVal"+sgstTotalVal+" igstTotalVal"+igstTotalVal);
			}

			Phrase GSTphrase = new Phrase("Total GST", font10bold);
			PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
			GSTphraseCell.setBorder(0);
			double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
			taxAmountInWords=totalGSTValue;
			Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
					font10bold);
			PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
			GSTValphraseCell.setBorder(0);
			GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			rightInnerTable.addCell(GSTphraseCell);
			rightInnerTable.addCell(colonCell);
			rightInnerTable.addCell(GSTValphraseCell);

			PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
//			innerRightCell.setBorder(0);
//			innerRightCell.setBorderWidthRight(1);
			// innerRightCell.addElement();
			innerRightCell.setBorderWidthTop(0);
			innerRightCell.setBorderWidthBottom(0);
			innerRightCell.setBorderWidthLeft(0);

			rightTable.addCell(innerRightCell);

			PdfPTable middleTable = new PdfPTable(1);
			middleTable.setWidthPercentage(100);

			// TODO Auto-generated method stub
			System.out.println("Inside Other Chrages");
			// PdfPTable otherChargesTable = new PdfPTable(1);
			// otherChargesTable.setWidthPercentage(100);
			// try {
			// otherChargesTable.setWidths(columnMoreLeftWidths);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// Phrase blank=new Phrase("",font10bold);
			// PdfPCell blankCell=new PdfPCell(blank);
			// otherChargesTable.addCell(blankCell);

			PdfPTable otherCharges = new PdfPTable(3);
			otherCharges.setWidthPercentage(100);
			try {
				otherCharges.setWidths(column3ProdCollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**
			 * Date 28-07-2018 By Vijay other charges not required as per sonu
			 */
			
//			for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
//				Phrase chargeName, taxes, assVal;
//				PdfPCell pCell;
//				if (i == 0) {
//					chargeName = new Phrase("Charge Name", font10bold);
//					taxes = new Phrase("Taxes", font10bold);
//					assVal = new Phrase("Amt", font10bold);
//					pCell = new PdfPCell(chargeName);
//					pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//					otherCharges.addCell(pCell);
//					pCell = new PdfPCell(taxes);
//					pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//					otherCharges.addCell(pCell);
//					pCell = new PdfPCell(assVal);
//					pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//					otherCharges.addCell(pCell);
//				}
//
//				chargeName = new Phrase(invoiceentity.getOtherCharges().get(i)
//						.getOtherChargeName(), font10);
//				String taxNames = " ";
//				if (invoiceentity.getOtherCharges().get(i).getTax1()
//						.getPercentage() != 0
//						&& invoiceentity.getOtherCharges().get(i).getTax2()
//								.getPercentage() != 0) {
//					taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
//							.getTaxConfigName()
//							+ "/"
//							+ invoiceentity.getOtherCharges().get(i).getTax2()
//									.getTaxConfigName();
//				} else {
//					if (invoiceentity.getOtherCharges().get(i).getTax1()
//							.getPercentage() != 0) {
//						taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
//								.getTaxConfigName();
//					} else if (invoiceentity.getOtherCharges().get(i).getTax2()
//							.getPercentage() != 0) {
//						taxNames = invoiceentity.getOtherCharges().get(i).getTax2()
//								.getTaxConfigName();
//					} else {
//						taxNames = " ";
//					}
//				}
//				taxes = new Phrase(taxNames /*
//											 * invoiceentity. getOtherCharges()
//											 * .get(i).get()
//											 */, font10);
//				assVal = new Phrase(invoiceentity.getOtherCharges().get(i)
//						.getAmount()
//						+ "", font10);
//				pCell = new PdfPCell(chargeName);
//				otherCharges.addCell(pCell);
//				pCell = new PdfPCell(taxes);
//				otherCharges.addCell(pCell);
//				pCell = new PdfPCell(assVal);
//				otherCharges.addCell(pCell);
//
//			}
			/**
			 * ends here
			 */
			
			/***
			 * Date 28-07-2018 By Vijay
			 * Des :- added tax amount 
			 */
			
			PdfPTable leftTable = new PdfPTable(1);
			leftTable.setWidthPercentage(100);
			
			String amtInWordsVal = "Tax Amount in Words : Rupees " + 
				      SalesInvoicePdf.convert(taxAmountInWords) + 
				      " Only/-";
		    Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font10bold);
		    
		    PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		    amtInWordsValCell.setHorizontalAlignment(0);
//		    amtInWordsValCell.setBorder(0);
//		    amtInWordsValCell.setBorderWidthLeft(1);
//		    amtInWordsValCell.setBorderWidthRight(1);
		    amtInWordsValCell.setBorderWidthTop(0);
		    amtInWordsValCell.setBorderWidthBottom(0);
		    leftTable.addCell(amtInWordsValCell);
		    /**
		     * ends here
		     */
		    
		    
			// PdfPCell left2Cell=new PdfPCell(otherCharges);
			// otherChargesTable.addCell();

			PdfPCell left22Cell = new PdfPCell(otherCharges);
			middleTable.addCell(left22Cell);

			PdfPCell rightCell = new PdfPCell(rightTable);
			rightCell.setBorder(0);
			// rightCell.addElement();
			PdfPCell middleCell = new PdfPCell(middleTable);

			PdfPCell leftCell = new PdfPCell(leftTable);
			leftCell.setBorder(0);
			// leftCell.addElement();

			pdfPTaxTable.addCell(leftCell);
			/**
			 * Date 28-07-2018 By Vijay other charges not required as per sonu
			 */
//			if (invoiceentity.getOtherCharges().size() > 0) {
//				pdfPTaxTable.addCell(middleCell);
//			} else {
//				Phrase blankPhrase = new Phrase(" ", font10);
//				middleCell = new PdfPCell(blankPhrase);
//				pdfPTaxTable.addCell(middleCell);
//			}
			pdfPTaxTable.addCell(rightCell);

			try {
				document.add(pdfPTaxTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	  
	  private void createProductDetailsVal() {
		  logger.log(Level.SEVERE,"In createProductDetailsVal");
	    double rateAmountProd = 0;double amountAmountProd = 0;double discAmountProd = 0;double totalAssAmountProd = 0;
	    
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {



	      rateAmountProd = rateAmountProd + invoiceentity.getSalesOrderProducts().get(i).getPrice();
	      

	      amountAmountProd = amountAmountProd + invoiceentity.getSalesOrderProducts().get(i).getPrice() * 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getQuantity();
	      

	      discAmountProd = discAmountProd + invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
	      
	      double taxValue = 0;
	      if (invoiceentity.getSalesOrderProducts().get(i)
	        .getBasePaymentAmount() != 0) {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
	      }
	      else {
	        taxValue = 
	          invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
	      }
	      totalAssAmountProd += taxValue;
	    }
	    
	    int firstBreakPoint = 5;
	    float blankLines = 0.0F;
	    
	    PdfPTable productTable;
	    if(onlyforOrion) {
	    	productTable= new PdfPTable(7);
	    	float[] column7Width = {0.8F,3.0F, 1.2F, 1.0F, 1.0F, 1.0F, 2.0F };
	    	productTable.setWidthPercentage(100.0F);
		    try {
		      productTable.setWidths(column7Width);
		    } catch (DocumentException e) {
		      e.printStackTrace();
		    }
	    }else {
	    	productTable= new PdfPTable(8);
	    	 productTable.setWidthPercentage(100.0F);
	 	    try {
	 	      productTable.setWidths(column8SerProdCollonWidth);
	 	    }
	 	    catch (DocumentException e) {
	 	      e.printStackTrace();
	 	    }
	    }
	    
	    
	    Phrase blankPhrase = new Phrase(" ", font10);
	    PdfPCell blankPhraseCellleft = new PdfPCell(blankPhrase);
//      blankPhraseCell.setBorder(0);
	    blankPhraseCellleft.setHorizontalAlignment(1);
	    blankPhraseCellleft.setBorderWidthBottom(0);
	    blankPhraseCellleft.setBorderWidthTop(0);
	    blankPhraseCellleft.setBorderWidthRight(0);
//	    blankPhraseCellleft.setBorderWidthLeft(0);
        PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
//        blankPhraseCell.setBorder(0);
        blankPhraseCell.setHorizontalAlignment(1);
        blankPhraseCell.setBorderWidthBottom(0);
        blankPhraseCell.setBorderWidthTop(0);
        blankPhraseCell.setBorderWidthRight(0);
        
        PdfPCell blankPhraseRightCell = new PdfPCell(blankPhrase);
//        blankPhraseRightCell.setBorder(0);
        blankPhraseRightCell.setHorizontalAlignment(1);
        blankPhraseRightCell.setBorderWidthBottom(0);
        blankPhraseRightCell.setBorderWidthTop(0);
//        blankPhraseRightCell.setBorderWidthRight(0);
	    if(onlyforOrion){
//	    	productTable.addCell(blankPhraseCellleft);
//	    	productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseCell);
//	        productTable.addCell(blankPhraseRightCell);     
//	        noOfLines -= 1;
        }
	   
	    int countToBeDeducted = 0;
	    
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {
	    	if(!invoiceentity.isConsolidatePrice()){
				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
						invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
					continue;
				}
			}
	      if (noOfLines == 0)
	      {
	        prouductCount = i;
	        break;
	      }
	      countToBeDeducted++;
	      noOfLines -= 1;
	      
	      int srNoVal = i + 1;
	      Phrase srNo = new Phrase(srNoVal+"", font6);
	      PdfPCell srNoCell = new PdfPCell(srNo);
	      srNoCell.setHorizontalAlignment(1);
//	      srNoCell.setBorder(0);
	      srNoCell.setBorderWidthRight(0);
	      srNoCell.setBorderWidthBottom(0);
	      srNoCell.setBorderWidthTop(0);
//	      srNoCell.setBorderWidthLeft(0);
	      
	     
	        
	      productTable.addCell(srNoCell);
	      System.out.println("getProdName().trim().length()" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getProdName().trim().length());
	      
	      if (invoiceentity.getSalesOrderProducts().get(i).getProdName().trim().length() > 42) {
	        noOfLines -= 1;
	      }
	      Phrase serviceName = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getProdName().trim(), font6);
	      PdfPCell serviceNameCell = new PdfPCell(serviceName);
//	      serviceNameCell.setBorder(0);
	      serviceNameCell.setBorderWidthRight(0);
	      serviceNameCell.setBorderWidthBottom(0);
	      serviceNameCell.setBorderWidthTop(0);
	      productTable.addCell(serviceNameCell);
	      
	      Phrase noOfServices = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getOrderServices()+"", 
	        font6);
	      PdfPCell noOfServicesCell = new PdfPCell(noOfServices);
	      noOfServicesCell.setHorizontalAlignment(1);
//	      noOfServicesCell.setBorder(0);
	      noOfServicesCell.setBorderWidthRight(0);
	      noOfServicesCell.setBorderWidthBottom(0);
	      noOfServicesCell.setBorderWidthTop(0);
	      productTable.addCell(noOfServicesCell);
	      
	      Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getHsnCode().equals("")) {
				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font6);
			} else {
				ServiceProduct serviceProduct = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("productCode",
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdCode().trim()).first().now();
				if (serviceProduct.getHsnNumber() != null
						&& serviceProduct.getHsnNumber().equalsIgnoreCase("")) {
					hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
				} else {
					hsnCode = new Phrase("", font6);
				}
			}

			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			hsnCodeCell.setBorder(0);
			hsnCodeCell.setBorderWidthRight(0);
			hsnCodeCell.setBorderWidthBottom(0);
			hsnCodeCell.setBorderWidthTop(0);
			productTable.addCell(hsnCodeCell);
			String startDateStr = "", endDateStr = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				
				System.out
						.println("con.getItems().get(j).getPrduct().getCount()"
								+ con.getItems().get(j).getPrduct().getCount());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out.println("con.getItems().get(j).getDuration()"
						+ con.getItems().get(j).getDuration());

				SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
						"dd/MM/yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				
				simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
				try{
				if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
						.getCount() == con.getItems().get(j).getPrduct()
						.getCount())
						&& (invoiceentity.getSalesOrderProducts().get(i)
								.getOrderDuration() == con.getItems().get(j)
								.getDuration())) {
					if (con.getItems().get(j).getEndDate() != null) {
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(con.getItems().get(j)
								.getEndDate());
					} else {
						Calendar c = Calendar.getInstance();
						c.setTime(con.getItems().get(j).getStartDate());
						c.add(Calendar.DATE, con.getItems().get(j)
								.getDuration());
						Date endDt = c.getTime();
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(endDt);
					}
				}
				
			}
			catch (Exception e) {
				System.out.println(e);
			}
			}

			/**
			 * Date :6/1/2018 By :Manisha Description : When no. of services is
			 * 1 then enddate is increased by 1 day..!!!
			 */


	  	Boolean configurationFlag = false;
	      
	  	processConfig = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", invoiceentity.getCompanyId())
				.filter("processName", "Invoice")
				.filter("configStatus", true).first().now();	 
	  	
	      if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("SingleServiceDuration")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						configurationFlag = true;
						break;
					}
				}
			}
	      
	      if (configurationFlag == true) {
				for (int k = 0; k < invoiceentity.getSalesOrderProducts()
						.size(); k++) {
					if (!invoiceentity.getSalesOrderProducts().get(i)
							.getProdName().trim().equalsIgnoreCase("")
							&& invoiceentity.getSalesOrderProducts().get(i)
									.getOrderServices() == 1) {
						isSingleService = true;
					}
				}
			}
	      
	      PdfPCell startDate_endDateCell = null;
	      if (isSingleService) {
	        Calendar c = Calendar.getInstance();
	        SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
	          "dd/MM/yyyy");
	        Date date = null;
	        try {
	          date = simpleDateFmt.parse(startDateStr);
	        }
	        catch (ParseException e) {
	          e.printStackTrace();
	        }
	        c.setTime(date);
	        c.add(5, 1);
	        Date endDt = c.getTime();
	        endDateStr = simpleDateFmt.format(endDt);
	        Phrase startDate_endDate = new Phrase(startDateStr + " - " + 
	          endDateStr, font6); //commented by Ashwini
	        
	        
	        startDate_endDateCell = new PdfPCell(startDate_endDate);
	        startDate_endDateCell
	          .setHorizontalAlignment(1);
	      } else {
	        Phrase startDate_endDate = new Phrase(startDateStr + " - " + 
	          endDateStr, font6); //commented by Ashwini
	       
	        startDate_endDateCell = new PdfPCell(startDate_endDate);
	        startDate_endDateCell
	          .setHorizontalAlignment(1);
	      }
	      
	      /**
	       * @author Anil , Date : 30-03-2019
	       * 
	       */
	      if(!contractDurationFlag){
	    	  Phrase startDate_endDate = new Phrase(" - ", font6); 
	    	  startDate_endDateCell = new PdfPCell(startDate_endDate);
	    	  startDate_endDateCell.setHorizontalAlignment(1); 
	      }
	      if(!onlyforOrion) {
	    	  startDate_endDateCell.setBorder(0);
	    	  startDate_endDateCell.setBorderWidthRight(1);
//	    	  startDate_endDateCell.setBorder(0);
	    	  startDate_endDateCell.setBorderWidthRight(0);
	    	  startDate_endDateCell.setBorderWidthBottom(0);
	    	  startDate_endDateCell.setBorderWidthTop(0);
	    	  productTable.addCell(startDate_endDateCell);//Ashwini Patil Date:23-06-2023 Orion requirement
	      
	      }
	      Phrase qty = new Phrase(
	        invoiceentity.getSalesOrderProducts().get(i).getQuantity()+"", 
	        font6);
	      PdfPCell qtyCell = new PdfPCell(qty);
	      qtyCell.setHorizontalAlignment(1);
	      







	      Phrase rate = null;
	      PdfPCell rateCell = null;
	      if (consolidatePrice) {
	        if (i == 0) {
	          rate = new Phrase(df.format(rateAmountProd), font6);
	          
	          rateCell = new PdfPCell(rate);
	          rateCell.setHorizontalAlignment(2);
	        } else {
	          rate = new Phrase(" ", font6);
	          
	          rateCell = new PdfPCell(rate);
	          rateCell.setHorizontalAlignment(2);
	          rateCell.setBorderWidthBottom(0.0F);
	          rateCell.setBorderWidthTop(0.0F);
	        }
	      }
	      else {
	        rate = new Phrase(df.format(
	          invoiceentity.getSalesOrderProducts().get(i).getPrice()), 
	          font6);
	        
	        rateCell = new PdfPCell(rate);
	        rateCell.setHorizontalAlignment(2);
	      }
	      
	      Phrase blankph = new Phrase("", font8);
	      PdfPCell blnkCell = new PdfPCell(blankph);
	      blnkCell.setBorderWidthTop(0.0F);
	      blnkCell.setBorderWidthRight(0);
	      blnkCell.setBorderWidthBottom(0.0F);
	      productTable.addCell(blnkCell);
	      




	      double amountValue = invoiceentity.getSalesOrderProducts().get(i)
	        .getPrice() * 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getQuantity();
	      double disPercentTotalAmount = 0;double disConTotalAmount = 0;
	      if ((invoiceentity.getSalesOrderProducts().get(i)
	        .getProdPercDiscount() == null) || 
	        (invoiceentity.getSalesOrderProducts().get(i)
	        .getProdPercDiscount().doubleValue() == 0)) {
	        disPercentTotalAmount = 0;
	      } else {
	        disPercentTotalAmount = getPercentAmount(
	          invoiceentity.getSalesOrderProducts().get(i), false);
	      }
	      
	      if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
	        disConTotalAmount = 0;
	      } else {
	        disConTotalAmount = 
	          invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt();
	      }
	      
	      totalAmount += amountValue;
	      








	      Phrase disc = null;
	      PdfPCell discCell = null;
	      if (consolidatePrice) {
	        if (i == 0) {
	          disc = new Phrase(df.format(discAmountProd), font6);
	          
	          discCell = new PdfPCell(disc);
	          discCell.setHorizontalAlignment(2);
	        } else {
	          disc = new Phrase(" ", font6);
	          
	          discCell = new PdfPCell(disc);
	          discCell.setHorizontalAlignment(2);
	          
//	          discCell.setBorderWidthBottom(0.0F);
//	          discCell.setBorderWidthTop(0.0F);
	        }
	      } else {
	        disc = new Phrase(df.format(
	          invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()), 
	          font6);
	        
	        discCell = new PdfPCell(disc);
	        discCell.setHorizontalAlignment(2);
	      }
	      


//	      discCell.setBorder(0);
	      discCell.setBorderWidthRight(0);
	      discCell.setBorderWidthBottom(0);
	      discCell.setBorderWidthTop(0);
	      productTable.addCell(discCell);
	      
	      Phrase taxableValue = null;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getPaymentPercent() != 0) {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				// taxableValue = new Phrase(df.format(invoiceentity
				// .getSalesOrderProducts().get(i).getBaseBillingAmount())
				// + "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			// taxableValue =null;/* new Phrase(df.format(taxValue)+ "",
			// font6);*/
			// totalAssAmount=totalAssAmount+taxValue;
			PdfPCell taxableValueCell = null;/*
											 * = new PdfPCell(taxableValue);
											 * taxableValueCell
											 * .setHorizontalAlignment
											 * (Element.ALIGN_RIGHT);
											 */


	      if (consolidatePrice) {
	        if (i == 0) {
	          taxableValue = new Phrase(df.format(totalAssAmountProd), 
	            font6);
	          taxableValueCell = new PdfPCell(taxableValue);
	          taxableValueCell
	            .setHorizontalAlignment(2);
	        } else {
	          taxableValue = new Phrase(" ", font6);
	          taxableValueCell = new PdfPCell(taxableValue);
	          taxableValueCell
	            .setHorizontalAlignment(2);
//	          taxableValueCell.setBorderWidthBottom(0.0F);
//	          taxableValueCell.setBorderWidthTop(0.0F);
	        }
	      } else {
	        taxableValue = new Phrase(df.format(taxValue), font6);
	        taxableValueCell = new PdfPCell(taxableValue);
	        taxableValueCell.setHorizontalAlignment(2);
	      }
//	      taxableValueCell.setBorder(0);
//	      taxableValueCell.setBorderWidthRight(0);
	      taxableValueCell.setBorderWidthBottom(0);
	      taxableValueCell.setBorderWidthTop(0);
	      productTable.addCell(taxableValueCell);
	      

	      logger.log(Level.SEVERE, "VAT TAX ::::Config Name" + 
	        invoiceentity.getSalesOrderProducts().get(i).getVatTax()
	        .getTaxConfigName() + 
	        "VAT TAx:::Tax Name" + 
	        invoiceentity.getSalesOrderProducts().get(i).getVatTax()
	        .getTaxName() + 
	        "Ser TAX ::::Config Name" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getServiceTax().getTaxConfigName() + 
	        "Ser TAx:::Tax Name" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getServiceTax().getTaxName());
	      logger.log(Level.SEVERE, "VAT TAX ::::" + 
	        invoiceentity.getSalesOrderProducts().get(i).getVatTax()
	        .getPercentage() + 
	        "Service Tax::::" + 
	        invoiceentity.getSalesOrderProducts().get(i)
	        .getServiceTax().getPercentage());
	      
	      String premisesVal = "";
	      for (int j = 0; j < con.getItems().size(); j++) {
	        if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == 
	          con.getItems().get(j).getPrduct().getCount()) {
	          if (invoiceentity.getSalesOrderProducts().get(i)
	            .getOrderDuration() == con.getItems().get(j)
	            .getDuration()) {
	            premisesVal = con.getItems().get(j).getPremisesDetails();
	          }
	        }
	      }
	      System.out.println("noOfLines in product" + noOfLines);
	      if ((premisesVal != null) && 
	        (printPremiseDetails) && (!premisesVal.equals(""))) {
	        noOfLines -= 1;
	        Phrase blankValPhrs = new Phrase(" ", font8);
	        PdfPCell premiseCell = new PdfPCell(blankValPhrs);
	        premiseCell.setColspan(1);
	        productTable.addCell(premiseCell);
	        
	        Phrase premisesValPhrs = new Phrase("Premise Details : " + 
	          premisesVal, font8);
	        premiseCell = new PdfPCell(premisesValPhrs);
	        premiseCell.setColspan(7);
	        productTable.addCell(premiseCell);
	      }
	    }
	    




	    int remainingLines = 0;
	    System.out.println("noOfLines outside" + noOfLines);
	    System.out.println("prouductCount" + prouductCount);
	    
	    if (noOfLines != 0) {
	      remainingLines = 16 - (16 - noOfLines);
	    }
	    System.out.println("remainingLines" + remainingLines);
	    
	    if (remainingLines != 0) {
	      for (int i = 0; i < remainingLines; i++) {
	        System.out.println("i::::" + i);
//	        Phrase blankPhrase = new Phrase(" ", font10);
//	        PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
//	        blankPhraseCell.setBorder(0);
//	        blankPhraseCell.setHorizontalAlignment(1);
//	        blankPhraseCell.setBorderWidthRight(1);
//	        PdfPCell blankPhraseRightCell = new PdfPCell(blankPhrase);
//	        blankPhraseRightCell.setBorder(0);
//	        blankPhraseRightCell.setHorizontalAlignment(1);
	        productTable.addCell(blankPhraseCellleft);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseCell);
	        productTable.addCell(blankPhraseRightCell);
	      }
	    }
	    PdfPCell productTableCell = null;
	    if (noOfLines == 0) {
	      Phrase my = new Phrase("Please Refer Annexure For More Details", 
	        font9bold);
	      productTableCell = new PdfPCell(my);
	    }
	    else {
	      productTableCell = new PdfPCell(blankCell);
	    }
	    
	    productTableCell.setBorder(0);
	    
	    PdfPTable tab = new PdfPTable(1);
	    tab.setWidthPercentage(100.0F);
	    tab.addCell(productTableCell);
	    tab.setSpacingAfter(blankLines);
	    



	    PdfPCell tab1 = new PdfPCell(productTable);
	    tab1.setBorder(0);

	    PdfPCell tab2 = new PdfPCell(tab);
	    

	    PdfPTable mainTable = new PdfPTable(1);
	    mainTable.setWidthPercentage(100.0F);
	    mainTable.addCell(tab1);
//	    mainTable.addCell(tab2); //26-06-2023 commented
	    try
	    {
	      document.add(mainTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  


	  private boolean validateTaxes(SalesOrderProductLineItem salesOrderProductLineItem)
	  {
	    if (salesOrderProductLineItem.getVatTax().getPercentage() != 0) {
	      return true;
	    }
	    if (salesOrderProductLineItem.getServiceTax().getPercentage() != 0) {
	      return true;
	    }
	    return false;
	  }
	  

	  private double getTaxAmount(double totalAmount2, double percentage)
	  {
	    double percAmount = totalAmount2 / 100;
	    double taxAmount = percAmount * percentage;
	    return taxAmount;
	  }
	  
//	  private double getTaxAmount(double totalAmount2, double percentage)
//	  {
//	    double percAmount = totalAmount2 / 100;
//	    double taxAmount = percAmount * percentage;
//	    return taxAmount;
//	  }





	private void createProductDetails() {
		  logger.log(Level.SEVERE,"In createProductDetails");
	    PdfPTable productTable;
	    if(onlyforOrion) {
	    	productTable= new PdfPTable(7);
	    	float[] column7Width = {0.8F,3.0F, 1.2F, 1.0F, 1.0F, 1.0F, 2.0F };
	    	productTable.setWidthPercentage(100.0F);
		    try {
		      productTable.setWidths(column7Width);
		    } catch (DocumentException e) {
		      e.printStackTrace();
		    }
	    }
	    else {
	    	productTable= new PdfPTable(8);
	    	productTable.setWidthPercentage(100.0F);
		    try {
		      productTable.setWidths(column8SerProdCollonWidth);
		    } catch (DocumentException e) {
		      e.printStackTrace();
		    }	    
	    }
	    
	    Phrase srNophrase = new Phrase("Sr No", font10bold);
	    PdfPCell srNoCell = new PdfPCell(srNophrase);
	    srNoCell.setHorizontalAlignment(1);
	    srNoCell.setVerticalAlignment(5);
	    srNoCell.setRowspan(2);
//	    srNoCell.setBorder(0);
	    srNoCell.setBorderWidthLeft(0);
	    srNoCell.setBorderWidthRight(0);
	    srNoCell.setBorderWidthTop(0);
//	    srNoCell.setBorderWidthBottom(0);
	    
	    
	    Phrase servicePhrase = new Phrase("Particulars", font10bold);
	    PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
	    servicePhraseCell.setHorizontalAlignment(1);
	    servicePhraseCell.setVerticalAlignment(5);
	    servicePhraseCell.setRowspan(2);
//	    servicePhraseCell.setBorder(0);
	    servicePhraseCell.setBorderWidthRight(0);
	    servicePhraseCell.setBorderWidthTop(0);
//	    servicePhraseCell.setBorderWidthBottom(0);
	    
	    Phrase noOfservicePhrase = new Phrase("Service/Qnty", font10bold);//"No Of Services"
	    PdfPCell noOfservicePhraseCell = new PdfPCell(noOfservicePhrase);
	    noOfservicePhraseCell.setHorizontalAlignment(1);
	    noOfservicePhraseCell.setVerticalAlignment(5);
	    noOfservicePhraseCell.setRowspan(2);
//	    noOfservicePhraseCell.setBorder(0);
	    noOfservicePhraseCell.setBorderWidthRight(0);
	    noOfservicePhraseCell.setBorderWidthTop(0);
//	    noOfservicePhraseCell.setBorderWidthBottom(0);
	    
	    Phrase hsnCode = new Phrase("SAC/HSN", font10bold);
	    PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
	    hsnCodeCell.setHorizontalAlignment(1);
	    hsnCodeCell.setVerticalAlignment(5);
	    hsnCodeCell.setRowspan(2);
//	    hsnCodeCell.setBorder(0);
	    hsnCodeCell.setBorderWidthRight(0);
	    hsnCodeCell.setBorderWidthTop(0);
//	    hsnCodeCell.setBorderWidthBottom(0);
	    
	    Phrase UOMphrase = new Phrase("UOM", font10bold);
	    PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
	    UOMphraseCell.setHorizontalAlignment(1);
	    UOMphraseCell.setVerticalAlignment(5);
	    UOMphraseCell.setRowspan(2);
//	    UOMphraseCell.setBorder(0);
	    UOMphraseCell.setBorderWidthRight(0);
	    UOMphraseCell.setBorderWidthTop(0);
//	    UOMphraseCell.setBorderWidthBottom(0);
	    
	    
	    Phrase qtyPhrase = new Phrase("No ", font10bold);
	    PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
	    qtyPhraseCell.setHorizontalAlignment(1);
	    qtyPhraseCell.setVerticalAlignment(5);
	    qtyPhraseCell.setRowspan(2);
//	    qtyPhraseCell.setBorder(0);
	    qtyPhraseCell.setBorderWidthRight(0);
	    qtyPhraseCell.setBorderWidthTop(0);
//	    qtyPhraseCell.setBorderWidthBottom(0);
	    
	    Phrase ratePhrase = new Phrase("Rate", font10bold);
	    PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
	    ratePhraseCell.setHorizontalAlignment(1);
	    ratePhraseCell.setVerticalAlignment(5);
	    ratePhraseCell.setRowspan(2);
//	    ratePhraseCell.setBorder(0);
	    ratePhraseCell.setBorderWidthRight(0);
	    ratePhraseCell.setBorderWidthTop(0);
//	    ratePhraseCell.setBorderWidthBottom(0);
	    
	    Phrase amountPhrase = new Phrase("Amount", font10bold);
	    PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
	    amountPhraseCell.setHorizontalAlignment(1);
	    amountPhraseCell.setVerticalAlignment(5);
	    amountPhraseCell.setRowspan(2);
//	    amountPhraseCell.setBorder(0);
	    amountPhraseCell.setBorderWidthRight(0);
	    amountPhraseCell.setBorderWidthTop(0);
//	    amountPhraseCell.setBorderWidthBottom(0);
	    
	    Phrase dicphrase = new Phrase("Disc", font10bold);
	    PdfPCell dicphraseCell = new PdfPCell(dicphrase);
	    dicphraseCell.setHorizontalAlignment(1);
	    dicphraseCell.setVerticalAlignment(5);
	    dicphraseCell.setRowspan(2);
//	    dicphraseCell.setBorder(0);
	    dicphraseCell.setBorderWidthRight(0);
	    dicphraseCell.setBorderWidthTop(0);
//	    dicphraseCell.setBorderWidthBottom(0);
	    
	    Phrase taxValPhrase = new Phrase("Billed Amount", font10bold);
	    PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
	    taxValPhraseCell.setHorizontalAlignment(1);
	    taxValPhraseCell.setVerticalAlignment(5);
	    taxValPhraseCell.setRowspan(2);
//	    taxValPhraseCell.setBorder(0);
	    taxValPhraseCell.setBorderWidthRight(0);
	    taxValPhraseCell.setBorderWidthTop(0);
//	    taxValPhraseCell.setBorderWidthBottom(0);
	    
	    
	    if(!onlyforOrion) {
	    	Phrase serviceServDate = new Phrase("Contract Duration", font10bold);
		    PdfPCell serviceServDateCell = new PdfPCell(serviceServDate);
		    serviceServDateCell.setHorizontalAlignment(1);
		    serviceServDateCell.setVerticalAlignment(5);
//		    serviceServDateCell.setBorder(0);
		    serviceServDateCell.setBorderWidthRight(0);
		    serviceServDateCell.setBorderWidthTop(0);
//		    serviceServDateCell.setBorderWidthBottom(0);
	    }
	    
	    productTable.addCell(srNoCell);
	    productTable.addCell(servicePhraseCell);
	    productTable.addCell(noOfservicePhraseCell);
	    
	    productTable.addCell(hsnCodeCell);
//	    productTable.addCell(serviceServDateCell);
	    


	    productTable.addCell(ratePhraseCell);
	    
	    productTable.addCell(dicphraseCell);
	    productTable.addCell(taxValPhraseCell);
	    
	    
	    PdfPCell tab1 = new PdfPCell(productTable);
	    tab1.setBorderWidthBottom(0);
	    PdfPTable mainTable = new PdfPTable(1);
	    mainTable.setWidthPercentage(100.0F);
	    mainTable.addCell(tab1);
	    try
	    {
	      document.add(mainTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  


	  private double getPercentAmount(SalesOrderProductLineItem salesOrderProductLineItem, boolean isAreaPresent)
	  {
	    double percentAmount = 0;
	    if (isAreaPresent) {
	      percentAmount = salesOrderProductLineItem.getPrice() * 
	        Double.parseDouble(salesOrderProductLineItem.getArea()
	        .trim()) * salesOrderProductLineItem
	        .getProdPercDiscount().doubleValue() / 100;
	    } else {
	      percentAmount = salesOrderProductLineItem.getPrice() * salesOrderProductLineItem
	        .getProdPercDiscount().doubleValue() / 100;
	    }
	    return percentAmount;
	  }
	  
	  private void createCustomerDetails()
	  {
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100.0F);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100.0F);

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(0);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100.0F);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font10bold);
		PdfPCell nameCell = new PdfPCell(name);

		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(0);

		String tosir = null;
		String custName = "";

		if ((cust.getCustPrintableName() != null)
				&& (!cust.getCustPrintableName().equals(""))) {
			custName = cust.getCustPrintableName().trim();

		} else if ((cust.isCompany()) && (cust.getCompanyName() != null)) {
			custName = "M/S  " + cust.getCompanyName().trim();
		} else if ((cust.getSalutation() != null)
				&& (!cust.getSalutation().equals(""))) {
			custName = cust.getSalutation() + " " + cust.getFullname().trim();
		} else {
			custName = cust.getFullname().trim();
		}

		String fullname = "";
		if ((cust.getCustPrintableName() != null)
				&& (!cust.getCustPrintableName().equals(""))) {
			fullname = custName;
		} else {
			fullname = custName;
		}

		Phrase nameCellVal = new Phrase(fullname, font10bold);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);

		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(0);

		Phrase email = new Phrase("Email", font10bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(0);

		Phrase emailVal = null;
		if (cust.getEmail() != null) {
			emailVal = new Phrase(cust.getEmail(), font10);
		} else {
			emailVal = new Phrase("", font10);
		}
		PdfPCell emailValCell = new PdfPCell(emailVal);

		emailValCell.setBorder(0);
		emailValCell.setColspan(4);
		emailValCell.setHorizontalAlignment(0);

		Phrase mobNo = new Phrase("Phone", font10bold);
		PdfPCell mobNoCell = new PdfPCell(mobNo);
		mobNoCell.setBorder(0);
		mobNoCell.setHorizontalAlignment(0);

		String phone = "";
		String landline = "";

		if ((cust.getCellNumber1() != null)
				&& (cust.getCellNumber1().longValue() != 0L)) {
			System.out.println("pn11");
			phone = cust.getCellNumber1() + "";
		}
		if ((cust.getCellNumber2() != null)
				&& (cust.getCellNumber2().longValue() != 0L)) {
			if (!phone.trim().isEmpty()) {
				phone = phone + " / " + cust.getCellNumber2();
			} else {
				phone = cust.getCellNumber2() + "";
			}
			System.out.println("pn33" + phone);
		}
		if ((cust.getLandline().longValue() != 0L)
				&& (cust.getLandline() != null)) {
			if (!phone.trim().isEmpty()) {
				phone = phone + " / " + cust.getLandline();
			} else {
				phone = cust.getLandline() + "";
			}
			System.out.println("pn44" + phone);
		}

		Phrase mobVal = new Phrase(phone, font10);
		PdfPCell mobValCell = new PdfPCell(mobVal);

		mobValCell.setBorder(0);
		mobValCell.setColspan(4);
		mobValCell.setHorizontalAlignment(0);

		Phrase address = new Phrase("Address", font10bold);

		PdfPCell addressCell = new PdfPCell(address);

		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(0);

		String adrsValString = "";
		if ((con.getNewcustomerAddress() != null)
				&& (con.getNewcustomerAddress().getAddrLine1() != null)
				&& (!con.getNewcustomerAddress().getAddrLine1().equals(""))
				&& (con.getNewcustomerAddress().getCountry() != null)
				&& (!con.getNewcustomerAddress().getCountry().equals(""))) {

			adrsValString = con.getNewcustomerAddress().getCompleteAddress()
					.trim();
		} else {
			adrsValString = cust.getAdress().getCompleteAddress().trim();
		}

		Phrase addressVal = new Phrase(adrsValString, font8);

		PdfPCell addressValCell = new PdfPCell(addressVal);

		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(0);

		String gstTinStr = "";
		if ((invoiceentity.getGstinNumber() != null)
				&& (!invoiceentity.getGstinNumber().equals(""))) {
			gstTinStr = invoiceentity.getGstinNumber().trim();
		} else {
			gstTinStr = getGSTINOfCustomer(cust,
					invoiceentity.getCustomerBranch());
		}

		Phrase gstTin = new Phrase("GSTIN", font10bold);
		PdfPCell gstTinCell = new PdfPCell(gstTin);

		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(0);

		Phrase gstTinVal = new Phrase(gstTinStr, font10);
		PdfPCell gstTinValCell = new PdfPCell(gstTinVal);

		gstTinValCell.setBorder(0);
		gstTinValCell.setHorizontalAlignment(0);

		Phrase stateCode = new Phrase("State Code", font10bold);

		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(0);

		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (((State) stateList.get(i)).getStateName().trim()
					.equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo = ((State) stateList.get(i)).getStateCode().trim();
				break;
			}
		}

		Phrase stateCodeVal = new Phrase(stCo, font13);

		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(0);

		Phrase attn = new Phrase("Attn", font10bold);
		PdfPCell attnCell = new PdfPCell(attn);

		attnCell.setBorder(0);
		attnCell.setHorizontalAlignment(0);

		Phrase attnVal = new Phrase(invoiceentity.getPersonInfo().getPocName(),
				font10);
		PdfPCell attnValCell = new PdfPCell(attnVal);

		attnValCell.setBorder(0);
		attnValCell.setHorizontalAlignment(0);

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attnCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attnValCell);
		}
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);

		PdfPTable colonTable2 = new PdfPTable(6);
		colonTable2.setWidthPercentage(100.0F);
		try {
			colonTable2.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 0.2F,
					2.0F });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable email_MobNumber = new PdfPTable(6);

		email_MobNumber.setWidthPercentage(100.0F);
		try {
			email_MobNumber.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F,
					0.2F, 2.0F });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase colon2 = new Phrase(":", font10bold);
		PdfPCell colon2Cell = new PdfPCell(colon2);
		colon2Cell.setBorder(0);
		colon2Cell.setHorizontalAlignment(0);

		logger.log(Level.SEVERE, " get emaill " + cust.getEmail());
		if (cust.getEmail() != null && cust.getEmail().trim().length() > 0) {
			email_MobNumber.addCell(emailCell);
			email_MobNumber.addCell(colon2Cell);
			email_MobNumber.addCell(emailValCell);

		}

		email_MobNumber.addCell(mobNoCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(mobValCell);

		colonTable2.addCell(gstTinCell);
		colonTable2.addCell(colon2Cell);
		colonTable2.addCell(gstTinValCell);

		colonTable2.addCell(stateCodeCell);
		colonTable2.addCell(colon2Cell);
		colonTable2.addCell(stateCodeValCell);

		PdfPCell cell1 = new PdfPCell(colonTable);
//		cell1.setBorder(0);//23-02-2024
		cell1.setBorderWidthBottom(0);//23-02-2024

		PdfPCell email_Cell = new PdfPCell(email_MobNumber);
		email_Cell.setBorder(0);

		PdfPCell cell12 = new PdfPCell(colonTable2);
		cell12.setBorder(0);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100.0F);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		colonTable.addCell(colonCell);

		PdfPTable pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100.0F);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(0);

		colonTable.addCell(state4Cell);

		PdfPCell stateCell_stateCode = new PdfPCell(colonTable);
		stateCell_stateCode.setBorder(0);

		part1Table.addCell(cell1);

		part1Table.addCell(email_Cell);
		part1Table.addCell(cell12);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100.0F);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100.0F);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase serviceaddress = new Phrase("Service Address", font8bold);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		serviceCell.setHorizontalAlignment(1);
		part2Table.addCell(serviceCell);

		Phrase name2 = new Phrase("Name", font10bold);
		PdfPCell name2Cell = new PdfPCell(name2);

		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(0);

		Phrase name2CellVal = new Phrase(fullname, font10bold);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);

		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(0);

		Phrase email1 = new Phrase("Email", font10bold);
		PdfPCell email1Cell = new PdfPCell(email1);
		email1Cell.setBorder(0);
		email1Cell.setHorizontalAlignment(0);

		Phrase email1Val = null;
		if (cust.getEmail() != null) {
			email1Val = new Phrase(cust.getEmail(), font10);
		} else {
			email1Val = new Phrase("", font10);
		}
		PdfPCell email1ValCell = new PdfPCell(email1Val);

		email1ValCell.setBorder(0);
		email1ValCell.setColspan(4);
		email1ValCell.setHorizontalAlignment(0);

		Phrase mobNo2 = new Phrase("Mobile", font10bold);
		PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
		mobNo2Cell.setBorder(0);
		mobNo2Cell.setHorizontalAlignment(0);

		String phone2 = "";

		if ((cust.getCellNumber1() != null)
				&& (cust.getCellNumber1().longValue() != 0L)) {
			System.out.println("pn11");
			phone2 = cust.getCellNumber1() + "";
		}
		if ((cust.getCellNumber2() != null)
				&& (cust.getCellNumber2().longValue() != 0L)) {
			if (!phone2.trim().isEmpty()) {
				phone2 = phone2 + " / " + cust.getCellNumber2();
			} else {
				phone2 = cust.getCellNumber2() + "";
			}
			System.out.println("pn33" + phone2);
		}
		if ((cust.getLandline().longValue() != 0L)
				&& (cust.getLandline() != null)) {
			if (!phone2.trim().isEmpty()) {
				phone2 = phone2 + " / " + cust.getLandline();
			} else {
				phone2 = cust.getLandline() + "";
			}
			System.out.println("pn44" + phone2);
		}

		Phrase mobNo2Val = new Phrase(phone2, font10);
		PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);

		mobNo2ValCell.setBorder(0);
		mobNo2ValCell.setHorizontalAlignment(0);

		Phrase landlineph2 = new Phrase("LandLine", font10bold);
		PdfPCell landlineCell2 = new PdfPCell(landlineph2);
		landlineCell2.setBorder(0);
		landlineCell2.setHorizontalAlignment(0);

		String landline2 = "";
		if (cust.getLandline().longValue() != 0L) {
			landline2 = cust.getLandline() + "";
		} else {
			landline2 = "";
		}

		Phrase landlineVal2 = new Phrase(landline2, font10);
		PdfPCell landlinevalCells2 = new PdfPCell(landlineVal2);

		landlinevalCells2.setBorder(0);
		landlinevalCells2.setHorizontalAlignment(0);

		Phrase address2 = new Phrase("Address", font10bold);

		PdfPCell address2Cell = new PdfPCell(address2);

		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(0);

		String adrsValString1 = "";
		String customerPoc = null;

		Address serviceAddress = null;

		if ((invoiceentity.getCustomerBranch() != null)
				&& (!invoiceentity.getCustomerBranch().equals(""))) {
			CustomerBranchDetails branch =

			(CustomerBranchDetails) ObjectifyService
					.ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("companyId", comp.getCompanyId())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch())
					.filter("cinfo.count",
							Integer.valueOf(invoiceentity.getPersonInfo()
									.getCount())).first().now();

			if (branch != null) {
				serviceAddress = branch.getAddress();
				System.out.println("heeeeeeeelllllllooo" + serviceAddress);
				adrsValString1 = branch.getAddress().getCompleteAddress();

			}

		} else if (con.getCustomerServiceAddress() != null) {
			serviceAddress = con.getCustomerServiceAddress();
			adrsValString1 = con.getCustomerServiceAddress()
					.getCompleteAddress();

		} else {
			serviceAddress = cust.getSecondaryAdress();

			adrsValString1 = cust.getSecondaryAdress().getCompleteAddress();

		}

		if ((!cust.getSecondaryAdress().getAddrLine1().equals(""))
				&& (customerbranchlist.size() == 0)) {
			customerPoc = invoiceentity.getPersonInfo().getPocName();
		}

		System.out.println("Inside Customer branch  "
				+ customerbranchlist.size());
		for (int i = 0; i < customerbranchlist.size(); i++) {
			customerPoc = ((CustomerBranchDetails) customerbranchlist.get(i))
					.getPocName();
		}

		Phrase address2Val = new Phrase(adrsValString1, font13);

		PdfPCell address2ValCell = new PdfPCell(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(0);

		String gstTin2Str = "";
		if ((invoiceentity.getGstinNumber() != null)
				&& (!invoiceentity.getGstinNumber().equalsIgnoreCase(""))) {
			gstTin2Str = invoiceentity.getGstinNumber().trim();
		} else {

			gstTin2Str = getGSTINOfCustomer(cust,
					invoiceentity.getCustomerBranch());
		}

		Phrase gstTin2 = new Phrase("GSTIN", font10bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(0);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font10);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(0);

		Phrase attn2 = new Phrase("Attn", font10bold);
		PdfPCell attn2Cell = new PdfPCell(attn2);
		attn2Cell.setBorder(0);
		attn2Cell.setHorizontalAlignment(0);

		Phrase attn2Val = null;
		if (custName != null) {
			attn2Val = new Phrase(customerPoc, font10);
		} else {
			attn2Val = new Phrase("", font10);
		}
		PdfPCell attn2ValCell = new PdfPCell(attn2Val);
		attn2ValCell.setBorder(0);
		attn2ValCell.setHorizontalAlignment(0);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100.0F);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attn2Cell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attn2ValCell);
		}
		colonTable.addCell(address2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);

		PdfPTable colonTable22 = new PdfPTable(3);
		colonTable22.setWidthPercentage(100.0F);
		try {
			colonTable22.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		PdfPTable email_MobNumber2 = new PdfPTable(6);
		email_MobNumber2.setWidthPercentage(100.0F);
		try {
			email_MobNumber2.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F,
					0.2F, 2.0F });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/**
		 * nidhi 27-04-2018 for if email details not available
		 */
		if (cust.getEmail() != null && cust.getEmail().trim().length() > 0) {

			email_MobNumber2.addCell(emailCell);
			email_MobNumber2.addCell(colonCell);
			email_MobNumber2.addCell(emailValCell);
		}
		email_MobNumber2.addCell(mobNoCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(mobValCell);

		PdfPCell cell2 = new PdfPCell(colonTable);
		cell2.setBorder(0);

		PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
		email_MobNumber2Cell.setBorder(0);

		PdfPCell cell22 = new PdfPCell(colonTable22);
		cell22.setBorder(0);

		Phrase state2 = new Phrase("State", font13bold);

		PdfPCell state2Cell = new PdfPCell(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(0);

		String stateName = "";
		if ((!cust.getSecondaryAdress().getAddrLine1().equals(""))
				&& (customerbranchlist.size() == 0)) {
			stateName = cust.getSecondaryAdress().getState().trim();
		} else {
			for (int i = 0; i < customerbranchlist.size(); i++) {
				stateName = ((CustomerBranchDetails) customerbranchlist.get(i))
						.getAddress().getState().trim();
			}
		}

		Phrase state2Val = new Phrase(stateName, font8bold);

		PdfPCell state2ValCell = new PdfPCell(state2Val);

		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(0);

		String st2Co = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (((State) stateList.get(i)).getStateName().trim()
					.equalsIgnoreCase(stateName.trim())) {
				st2Co = ((State) stateList.get(i)).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font8bold);

		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(0);

		Phrase state2CodeVal = new Phrase(st2Co, font8);

		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(0);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100.0F);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);

		PdfPCell stateCell_stateCode2 = new PdfPCell(colonTable);
		stateCell_stateCode2.setBorder(0);

		part2Table.addCell(cell2);

		part2Table.addCell(email_MobNumber2Cell);
		part2Table.addCell(cell22);

		PdfPCell part2TableCell = new PdfPCell(part2Table);

		mainTable.addCell(part1TableCell);

		if (custbranchlist.size() == 0) {
			mainTable.addCell(blankCell);
		} else {
			mainTable.addCell(part2TableCell);
		}

		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	  
	  private void createInvoiceDetails(String preprintStatus)
	  {
		  logger.log(Level.SEVERE,"In createInvoiceDetails");
	    PdfPTable mainTable = new PdfPTable(2);
	    mainTable.setWidthPercentage(100.0F);
	    try {
	      mainTable.setWidths(columnHalfWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    

	    String titlepdf = "";
	    

	    if (("Proforma Invoice".equals(invoiceentity.getInvoiceType().trim())) || 
	    
	      (invoiceentity.getInvoiceType().trim().equals("Proforma Invoice"))) {
	      titlepdf = "Proforma Invoice";
	    } else {
	      titlepdf = "Tax Invoice";
	    }
	    Phrase titlephrase = new Phrase(titlepdf, font14bold);
	    Paragraph titlepdfpara = new Paragraph();
	    titlepdfpara.add(titlephrase);
	    titlepdfpara.setAlignment(1);
	    
	    PdfPCell titlecell = new PdfPCell(titlephrase);
	    
	    titlecell.setHorizontalAlignment(1);
	    titlecell.setVerticalAlignment(5);
	    titlecell.setBorder(0);
	    

	    Phrase blankphrase = new Phrase("", font8);
	    PdfPCell blankCell = new PdfPCell(blankphrase);
	    blankCell.setBorder(0);
	    
	    PdfPTable titlepdftable = new PdfPTable(1);
	    titlepdftable.setWidthPercentage(100.0F);
	    titlepdftable.setHorizontalAlignment(1);
	    
	    titlepdftable.addCell(titlecell);
	    

	    Paragraph blank = new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    
	    PdfPTable parent = new PdfPTable(1);
	    parent.setWidthPercentage(100.0F);
	    parent.setSpacingBefore(10.0F);
	    
	    PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	    parent.addCell(titlePdfCell);
	    
	    PdfPTable parent2 = new PdfPTable(1);
	    parent2.setWidthPercentage(100.0F);
	    

	    Phrase Po_phrase = new Phrase();
	    String po_No = invoiceentity.getRefNumber();
	    if ((po_No != null) && (!po_No.equalsIgnoreCase(""))) {
	      Po_phrase = new Phrase("Po.No: " + po_No, font12bold);
	    } else {
	      Po_phrase = new Phrase("");
	    }
	    
	    PdfPCell title2PdfCell = new PdfPCell(Po_phrase);
	    title2PdfCell.setHorizontalAlignment(1);
	    /**
	     * Commented by Rahul Verma
	     * As Orion Pest didnt want PO No(Ref No) in invoice
	     */
//	    parent2.addCell(title2PdfCell);//umcommented by Ashwini
	    


	    try
	    {
	      document.add(parent);
	      if ((po_No != null) && (!po_No.equalsIgnoreCase(""))) {
	        document.add(parent2);
	      }
	    }
	    catch (DocumentException e)
	    {
	      e.printStackTrace();
	    }
	    

	    PdfPTable mainHeaderTable = new PdfPTable(2);
	    mainHeaderTable.setWidthPercentage(100.0F);
	    try {
	      mainHeaderTable.setWidths(columnHalfWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    


	    Phrase companyName = null;
	    
	    if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
	    	companyName = new Phrase( branchDt.getCorrespondenceName(), font12bold);
			logger.log(Level.SEVERE,"correspondance name 111 "+companyName);
			
		}else{
			if (comp != null) {
			      companyName = new Phrase(comp.getBusinessUnitName().trim(), font12bold);
			      logger.log(Level.SEVERE,"company NAme "+companyName);
			    }	
		}
	    
	    
	    
	    /*
	     * commented by Ashwini
	     */
//	    String comadd = "";
//	    Phrase companyAddr = null;
//	    if (comp != null) {
//	      comadd = comp.getAddress().getAddrLine1().trim() + "," + comp.getAddress().getAddrLine2().trim();
//	      
//	      companyAddr = new Phrase(comadd, font8);
//	    }
//	    PdfPCell companyAddrCell = new PdfPCell(companyAddr);
//	    companyAddrCell.setBorder(0);
//	    companyAddrCell.setHorizontalAlignment(0);
	    /*
	     * end by Ashwini
	     */

	    /*
	     * Date:23-10-2018
	     * Developer:Ashwini
	     * Des:To add branchwise Address
	     */
	    ServerAppUtility serverApp = new ServerAppUtility();
	    String compAdress = "";
	   
	    
	    
	
	    if(branch!=null){
	    	
	    	   
	    	for(Branch branch1 :branch){
	    		
	    		if(invoiceentity.getBranch().equalsIgnoreCase(branch1.getBusinessUnitName())){
	    			
	    			System.out.println("gstn no::"+branch1.getGSTINNumber());
	    			if(branch1.getGSTINNumber().equals(null) || branch1.getGSTINNumber().equals("")){
	    				compAdress=comp.getAddress().getCompleteAddress();
//		    			compAdress = comp.getAddress().getAddrLine1().trim() + "," + comp.getAddress().getAddrLine2().trim();
		    			logger.log(Level.SEVERE, "COMPANY Address");
	    		}else{
	    			compAdress = branch1.getAddress().getCompleteAddress().trim();
	    			System.out.println("Branch Address::"+compAdress);
	    			logger.log(Level.SEVERE, "BRANCH ADDRESS");
	    		}
	    		
	    	}
	    	}
	    
	    }else{
	    	compAdress=comp.getAddress().getCompleteAddress();
//	    	compAdress = comp.getAddress().getAddrLine1().trim() + "," + comp.getAddress().getAddrLine2().trim();
			System.out.println("company Address::"+compAdress);
		}
	    
	    
	    Phrase compAdressCell = new Phrase(compAdress,font8);
	    PdfPCell companyAddrCell = new PdfPCell(compAdressCell);
	   
	    companyAddrCell.setBorder(0);
	    companyAddrCell.setHorizontalAlignment(0);
	    
	    /*
	     * end by Ashwini
	     */
	    
	    PdfPCell companyNameCell = new PdfPCell(companyName);
	    
//	    companyNameCell.setBorder(0);
	    companyNameCell.setHorizontalAlignment(0);
	    companyNameCell.setBorderWidthTop(1);

	    PdfPTable companynameTable = new PdfPTable(1);
	    companynameTable.setWidthPercentage(100.0F);
	    try
	    {
	      companynameTable.setWidths(columnHalfWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    companynameTable.addCell(companyNameCell);
	    companynameTable.addCell(companyAddrCell);
	    
	    PdfPCell companynameTablecell = new PdfPCell(companynameTable);
	    companynameTablecell.setBorderWidthBottom(0.0F);
	    companynameTablecell.setBorderWidthTop(1);
	    mainHeaderTable.addCell(companynameTablecell);
	    
	    
	    /**
	     * @author Anil ,Date : 03-01-2019
	     * Changed the lable from Invoice No. to Reference No.
	     * For Orion By Sonu
	     */
	    Phrase invoicenoph = new Phrase(" ", font8bold);
//	    Phrase invoicenoph = new Phrase("Reference No. ", font8bold);//Deepak Salve comment this code as per sonu sir requirement 30-01-2020
	    PdfPCell invoicenocell = new PdfPCell(invoicenoph);
	    invoicenocell.setBorder(0);
	    
	    /**
	     * Date 2-5-2018
	     * by Jayshree
	     * Des.change the invoice no in orion format
	     */
	    
	    String year=null;
	    String month=null;
	    String invoiceid=null;
	    String state=null;
	    String year2=null;
	    String priviousyer=null;
	    String Upcomingyear=null;
	    String branchcode=null;
	    String subinvoiceid=null;
	    int illenth=0;
	    ServerAppUtility serverapp=new ServerAppUtility();
	    
	    try {
			year=(serverApp.getFinancialYearOfDate(invoiceentity.getInvoiceDate()));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	    
	    String[] parts = year.split("-");
	    String parts1 = parts[0];
	    String parts2 = parts[1]; 
	    
	    priviousyer=parts1.substring(2,4);
	    Upcomingyear=parts2.substring(2,4);
	    
	    invoiceid=invoiceentity.getCount()+"";
	    illenth=invoiceid.length();
	    /** Date 28-07-2018 By Vijay for last 3 digit will disaplay on PDF ***/ 
//	    subinvoiceid=invoiceid.substring(illenth-6,illenth-1);
	    subinvoiceid=invoiceid.substring(6,9);
	    /** ends here ***/

	    month=monthformat.format(invoiceentity.getInvoiceDate());
	    
	    for (int i = 0; i <branch.size(); i++) {
	    	branchcode=branch.get(i).getBranchCode();
		}
	    
	    Phrase invoicenovalph = new Phrase(" ");
//	    Phrase invoicenovalph = new Phrase(branchcode+"/"+priviousyer+"-"+Upcomingyear+"/"+month+"/"+subinvoiceid, font8);//Deepak Salve comment this code as per sonu sir requirement  30-01-2020
	    PdfPCell invoicenovalcell = new PdfPCell(invoicenovalph);
	    invoicenovalcell.setBorder(0);
	    
	    

	    Phrase invoicedateph = new Phrase("Dated", font8bold);
	    PdfPCell invoicedatecell = new PdfPCell(invoicedateph);
	    invoicedatecell.setBorder(0);
	    invoicedatecell.setPaddingRight(20);
	    invoicedatecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    
	    Phrase invoicedatevalph = new Phrase(sdf.format(invoiceentity.getInvoiceDate()), font8);
	    PdfPCell invoicedatevalcell = new PdfPCell(invoicedatevalph);
	    invoicedatevalcell.setBorder(0);
	    invoicedatevalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    
	    /**
	     * Date 06-09-2018 By Vijay
	     * Des :- PO No should be display after invoice Date
	     * @author Anil ,Date : 03-01-2019
	     * changed label from Po.No. to Invoice No.
	     * For Orion By Sonu
	     */
	    Phrase invoicePoNo = new Phrase("Invoice No. ", font8bold);
	    PdfPCell invoicePoNocell = new PdfPCell(invoicePoNo);
	    invoicePoNocell.setBorder(0);
	    
	    Phrase invoiceRef =null;
	    if(invoiceentity.getRefNumber()!=null && !invoiceentity.getRefNumber().equals("")){
	    	  invoiceRef = new Phrase(invoiceentity.getRefNumber(), font8);
	    }
	    else{
	    	 if(con.getRefNo()!=null && !con.getRefNo().equals(""))
	    		 invoiceRef = new Phrase(con.getRefNo(), font8);
	    	 else
	    		 invoiceRef = new Phrase(" ", font8); 
	    }
	    PdfPCell invoiceRefCell = new PdfPCell(invoiceRef);
	    invoiceRefCell.setBorder(0);
	    
	    /**
	     * ends here
	     */
	    
	    PdfPTable tablecell2 = new PdfPTable(2);
	    tablecell2.setWidthPercentage(50.0F);
	    try {
	      tablecell2.setWidths(columnHalfWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
//	    tablecell2.addCell(invoicenocell);
	    tablecell2.addCell(invoicePoNocell);
//	    tablecell2.addCell(invoicenocell);//Deepak Salve added this line  30-01-2020
	    tablecell2.addCell(invoicedatecell);
//	    tablecell2.addCell(invoicenovalcell);
	    tablecell2.addCell(invoiceRefCell);
//	    tablecell2.addCell(invoicenovalcell);//Deepak Salve added this code  30-01-2020
	    tablecell2.addCell(invoicedatevalcell);

	    
	    PdfPCell invoiceinfo = new PdfPCell(tablecell2);
	    

	    mainHeaderTable.addCell(invoiceinfo);
	    



	    Phrase companyGSTTIN = null;
	    String gstinValue = serverApp.getGSTINOfCompany(comp, invoiceentity.getBranch().trim());
	    
//	    for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
//	    {
//	      if (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("GSTIN")) {
//	        gstinValue = 
//	          ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeValue().trim();
//	      }
//	    }
	     

	    
	    if (!gstinValue.equals("")) {
	      companyGSTTIN = new Phrase(gstinValue, font8);
	    }
	    
	    Phrase gstph = new Phrase("GSTIN/UIN", font8bold);
	    PdfPCell gstcell = new PdfPCell(gstph);
	    gstcell.setHorizontalAlignment(0);
	    gstcell.setBorder(0);
	    


	    PdfPCell gstvalcell = new PdfPCell(companyGSTTIN);
	    gstvalcell.setBorder(0);
	    gstvalcell.setHorizontalAlignment(0);
	    

	    Phrase statenameph = new Phrase("State", font8bold);
	    PdfPCell statenamecell = new PdfPCell(statenameph);
	    statenamecell.setBorder(0);
	    
	    
		
//        Phrase statenamevalph = new Phrase(comp.getAddress().getState(), font8);//commented by Ashwini
	    
	    /*
	     * Developer:24/10/2018
	     * Added by Ashwini
	     */
        Phrase statenamevalph = null;
        
        /**
         * @author Anil,Date : 03-01-2019 
         * In this variable we will store ,state name and as per that state name we will print state code
         * For Orion By Sonu
         */
        String stateNm="";
        if(branch!=null){
        for(Branch branch1 :branch){
        	if(branch1.getGSTINNumber().equals(null) || branch1.getGSTINNumber().equals("")){
        		
        	
			if(branch1.getAddress().getState()!=null){
				stateNm=comp.getAddress().getState();
				statenamevalph =  new Phrase(comp.getAddress().getState(), font8);
				
				logger.log(Level.SEVERE ,"STATE OF COMPANY");
			}
				
			}else{
				stateNm=branch1.getAddress().getState();
				statenamevalph =  new Phrase(branch1.getAddress().getState(), font8);
				
				logger.log(Level.SEVERE, "STATE OF BANK");
			}
		}
        }
        else{
        	statenamevalph =  new Phrase(comp.getAddress().getState(), font8);
        }
	    PdfPCell statenamvalcell = new PdfPCell(statenamevalph);
	    statenamvalcell.setBorder(0);
	    

	    Phrase stateCodeStr1 = null;
	    /**
	     * nidhi
	     * 31-08-2018 
	     * for print company address state code
	     */
	    
        String stateCodeStr = null;
        /**
         * @author Anil ,Date : 03-01-2019 
         * Below method pick branch state code,but for orion if GSTIN number is present in Branch master 
         * then only we will pick state code of that branch else state code will be picked from company master
         */
//	    stateCodeStr = serverApp.getStateOfCompany(comp, branch.get(0).getBusinessUnitName(), stateList); 
	     for(State st:stateList){
	    	 if(st.getStateName().equals(stateNm)){
	    		 stateCodeStr=st.getStateCode();
	    		 break;
	    	 }
	     }
   
	   

	    PdfPTable statetable = new PdfPTable(3);
	    statetable.setWidthPercentage(100.0F);
	    try {
	      statetable.setWidths(new float[] { 2.0F, 1.0F, 2.0F });
	    }
	    catch (Exception localException1) {}
	    



	    Phrase statecodeph = new Phrase("Code :", font8bold);
	    PdfPCell statecodecell = new PdfPCell(statecodeph);
	    statecodecell.setBorder(0);
	    
         
	    Phrase statecodevalph = new Phrase(stateCodeStr, font8);
	    PdfPCell statecodevalcell = new PdfPCell(statecodevalph);
	    statecodevalcell.setBorder(0);
	    

	    statetable.addCell(statenamvalcell);
	    statetable.addCell(statecodecell);
	    statetable.addCell(statecodevalcell);
	    
	    Phrase cinph = new Phrase("CIN", font8bold);
	    PdfPCell cincell = new PdfPCell(cinph);
	    cincell.setBorder(0);
	    cincell.setHorizontalAlignment(0);
	    Phrase cinvalphrse = null;
	    for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	    {
	      if ((((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("CIN")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticlePrint().equalsIgnoreCase("YES")) && 
	        (((ArticleType)comp.getArticleTypeDetails().get(i)).getDocumentName().equalsIgnoreCase("Invoice Details")))
	      {
	        cinvalphrse = new Phrase(((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeValue(), font8);
	      }
	    }
	    
	    PdfPCell cinvaluecell = new PdfPCell(cinvalphrse);
	    cinvaluecell.setHorizontalAlignment(0);
	    cinvaluecell.setBorder(0);
	    

	    String branchmail = "";
	    
	    if (checkEmailId) {
	      branchmail = serverApp.getBranchEmail(comp, invoiceentity.getBranch());
	      System.out.println("server method " + branchmail);
	    }
	    else {
	      branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
	      System.out.println("server method 22" + branchmail);
	    }
	    

	    String email = null;
	    Phrase emailph = new Phrase("Email", font8bold);
	    PdfPCell emailcell = new PdfPCell(emailph);
	    emailcell.setBorder(0);
	    
	    if (branchmail != null) {
	      email = branchmail;
	    }
	    else {
	      email = "";
	    }
	    
	    Phrase emailvalph = new Phrase(email, font8);
	    PdfPCell emailvalcell = new PdfPCell(emailvalph);
	    emailvalcell.setBorder(0);
	    emailvalcell.setHorizontalAlignment(0);
	    

	    Phrase colon = new Phrase(":", font10bold);
	    PdfPCell colonCell = new PdfPCell(colon);
	    colonCell.setBorder(0);
	    colonCell.setHorizontalAlignment(0);
	    
	    PdfPTable rowspantable = new PdfPTable(3);
	    rowspantable.setWidthPercentage(100.0F);
	    try {
	      rowspantable.setWidths(columnCollonWidth);
	    }
	    catch (Exception localException2) {}
	    
	    /** 
	     *  email for company
	     */
	    rowspantable.addCell(emailcell);
	    rowspantable.addCell(colonCell);
	    rowspantable.addCell(emailvalcell);
	    
	    PdfPCell statecell = new PdfPCell(statetable);
	    statecell.setBorder(0);
	    rowspantable.addCell(statenamecell);
	    rowspantable.addCell(colonCell);
	    rowspantable.addCell(statecell);
	    
	    rowspantable.addCell(gstcell);
	    rowspantable.addCell(colonCell);
	    rowspantable.addCell(gstvalcell);
	    



	    PdfPCell rowspantablecell = new PdfPCell(rowspantable);
	    rowspantablecell.setBorderWidthTop(0.0F);
	    
	    mainHeaderTable.addCell(rowspantablecell);
	    
	    /**Date 4-3-2020 by Amol added a logo as per BranchAsCompany Flow raised by Rahul Tiwari **/
	    DocumentUpload logodocument=null;
	    logger.log(Level.SEVERE, "Branch as Company Flag "+branchAsCompany);
		if (branchAsCompany) {
			ServerAppUtility obj = new ServerAppUtility();
			
			logodocument = obj.getLogoOfCompany(comp, invoiceentity.getBranch()
					.trim());
			logger.log(Level.SEVERE, "Inside Branch As Company IFFF"+branchAsCompany);
		} else {
			if (comp.getLogo() != null) {
				logodocument = comp.getLogo();
			} else {
				logodocument = null;
			}
			logger.log(Level.SEVERE, "outside Branch As Company ELSE"+branchAsCompany);
		}


	    String environment = 
	      System.getProperty("com.google.appengine.runtime.environment");
	    String hostUrl1; 
	    if (environment.equals("Production")) {
	      String applicationId = 
	        System.getProperty("com.google.appengine.application.id");
	      String version = 
	        System.getProperty("com.google.appengine.application.version");
	      hostUrl1 = "http://" + version + "." + applicationId + 
	        ".appspot.com/";
	    } else {
	      hostUrl1 = "http://localhost:8888";
	      System.out.println("local");
	    }
	    PdfPCell imageSignCell = null;
	    
	    
	    Image image2 = null;
	    try {
	      image2 = 
	        Image.getInstance(new URL(hostUrl1 + logodocument.getUrl()));
	      image2.scalePercent(20.0F);
	      
	      imageSignCell = new PdfPCell(image2);
	      
//	      imageSignCell.setFixedHeight(40F);
	      imageSignCell.setBorderWidthBottom(0.0F);
	      imageSignCell.setBorderWidthLeft(0.0F);
	      imageSignCell.setBorderWidthTop(0.0F);
	      
	      imageSignCell.setHorizontalAlignment(1);
//	      imageSignCell.setVerticalAlignment(5);
	      imageSignCell.setVerticalAlignment(1);
	      imageSignCell.setPaddingBottom(10);
	      imageSignCell.setPaddingTop(10);
	      imageSignCell.setPaddingRight(5);
	      imageSignCell.setPaddingLeft(5);
//	      imageSignCell.setBorder(0);
	      if(!con.isServiceWiseBilling()){
     	    imageSignCell.setRowspan(3);
	      }else{
	    	  imageSignCell.setRowspan(2);
	      }
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    
	    
//	      Image image1=null;
//	 		 try
//	 		 {
//	 		 image1=Image.getInstance("images/logo15.jpg");
//	 		 image1.scalePercent(20f);
//	 		 // image1.setAbsolutePosition(40f,765f);
//	 		 // doc.add(image1);
//	 		
//	 		 imageSignCell = new PdfPCell(image1);
//		      
//		      imageSignCell.setFixedHeight(40);
//		      imageSignCell.setBorderWidthBottom(0);
//		      imageSignCell.setBorderWidthLeft(0);
//		      imageSignCell.setBorderWidthTop(0);
//		      imageSignCell.setHorizontalAlignment(1);
//		      imageSignCell.setVerticalAlignment(5);
//	 		 }
//	 		 catch(Exception e)
//	 		 {
//	 		 e.printStackTrace();
//	 		 }
	    



	    
//	    PdfPTable logoTab = new PdfPTable(1);
//	    logoTab.setWidthPercentage(100.0F);
//	    
//	    if (imageSignCell != null) {
//	      logoTab.addCell(imageSignCell);
//	      // vijay 
//	      mainHeaderTable.addCell(imageSignCell);
//	      System.out.println("hi logo cell");
//	    } else {
//	      Phrase logoblank = new Phrase(" ");
//	      PdfPCell logoblankcell = new PdfPCell(logoblank);
//	      /**  Date 14-apr-2018 By vijay commented **/
////	      logoblankcell.setBorder(0);
////	      logoTab.addCell(logoblankcell);
//	      
//	      // vijay new added
//	      logoblankcell.setFixedHeight(40F);
//	      logoblankcell.setHorizontalAlignment(1);
//	      logoblankcell.setVerticalAlignment(5);
//	      logoblankcell.setRowspan(3);
//	      logoTab.addCell(logoblankcell);
//	      mainHeaderTable.addCell(logoblankcell);
//
//	    }

	    /**
	     * @author Anil,Date : 03-01-2018
	     * As per the requirement if logo is uploaded then also we have to show blank space
	     * Not required to get logo from company master
	     * For Orion By Sonu
	     * commented below code which prints logo
	     */
//	    /**  Date 14-apr-2018 By vijay above old code commented and below new code added **/
//	    if (imageSignCell != null) {
//		      mainHeaderTable.addCell(imageSignCell);
//		      
//		      /*
//			     * Date:17/09/2018
//			     * Developer:Ashwini
//			     * Des:To add space while printing logo
//			     */
//			      Phrase logoblank = new Phrase(" ");
//			      PdfPCell logoblankcell = new PdfPCell(logoblank);
//		    	  mainHeaderTable.addCell(logoblankcell);
//		    	  mainHeaderTable.addCell(logoblankcell);
//		    	  mainHeaderTable.addCell(logoblankcell);
//		    	  mainHeaderTable.addCell(logoblankcell);
//		    	  mainHeaderTable.addCell(logoblankcell);
//		    	  mainHeaderTable.addCell(logoblankcell);
//		    	  /*
//		    	   * End here
//		    	   */
//		    	  
//		    } 
//	    
	    
	    
	    logger.log(Level.SEVERE, "priprint status " + preprintStatus);
		if (preprintStatus.contains("yes")) {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);

			logoblankcell.setFixedHeight(40F);
			logoblankcell.setHorizontalAlignment(1);
			logoblankcell.setVerticalAlignment(5);
			logoblankcell.setRowspan(3);
			mainHeaderTable.addCell(logoblankcell);
			logger.log(Level.SEVERE, "inside priprint status yesss " + preprintStatus);
		} 
		else if (preprintStatus.contains("no")) {
			logger.log(Level.SEVERE, "inside priprint status no " + preprintStatus);
			if (imageSignCell != null) {
				mainHeaderTable.addCell(imageSignCell);
				logger.log(Level.SEVERE, "With Logo " + branchAsCompany);
			} else {
				Phrase logoblank = new Phrase(" ");
				PdfPCell logoblankcell = new PdfPCell(logoblank);

				logoblankcell.setFixedHeight(40F);
				logoblankcell.setHorizontalAlignment(1);
				logoblankcell.setVerticalAlignment(5);
				logoblankcell.setRowspan(3);
				mainHeaderTable.addCell(logoblankcell);
				logger.log(Level.SEVERE, "Withou logo " + branchAsCompany);
				
			}
		}
	    
	    /**
	     * ends here
	     */
	    
//	    mainHeaderTable.addCell(imageSignCell);
	    


	    String gstin = "";String gstinText = "";
	    
	    if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
	      logger.log(Level.SEVERE, "GST Applicable");
	      gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
	        .getBranch().trim());
	      System.out.println("gstin" + gstin);
	    }
	    else {
	      logger.log(Level.SEVERE, "GST Not Applicable");
	      gstinText = comp.getCompanyGSTTypeText().trim();
	      System.out.println("gstinText" + gstinText);
	    }
	    
	    String articalType2 = null;
	    if (!gstin.trim().equals("")) {
	      logger.log(Level.SEVERE, "GST Present");
	      articalType2 = new String("GSTIN : " + gstin);
	    } else if (!gstinText.trim().equalsIgnoreCase("")) {
	      logger.log(Level.SEVERE, "GST Not Present");
	      articalType2 = new String(gstinText);
	    } else {
	      logger.log(Level.SEVERE, "Nothing Present");
	      articalType2 = new String("");
	    }
	    


	    Phrase billingAddress = new Phrase("Billing Address", font8bold);
	    PdfPCell billAdressCell = new PdfPCell(billingAddress);
	    billAdressCell.setHorizontalAlignment(1);
	    
	    /** 13-04-2018 By vijay below code commented not required wrong design **/
//	    mainHeaderTable.addCell(billAdressCell);

	    PdfPTable part1Table = new PdfPTable(1);
	    part1Table.setWidthPercentage(100.0F);
	    
	    /** 13-04-2018 By vijay adding cell into table **/
	    part1Table.addCell(billAdressCell);
	    
	    Phrase colon1 = new Phrase(":", font10bold);
	    PdfPCell colonCell1 = new PdfPCell(colon1);
	    
	    colonCell1.setBorder(0);
	    colonCell1.setHorizontalAlignment(0);
	    
	    PdfPTable colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    Phrase name = new Phrase("Name", font10bold);
	    PdfPCell nameCell = new PdfPCell(name);
	    
	    nameCell.setBorder(0);
	    nameCell.setHorizontalAlignment(0);
	    

	    String tosir = null;
	    String custName = "";
	    

	    if ((cust.getCustPrintableName() != null) && 
	      (!cust.getCustPrintableName().equals(""))) {
	      custName = cust.getCustPrintableName().trim();

	    }
	    else if ((cust.isCompany()) && (cust.getCompanyName() != null)) {
	      custName = "M/S  " + cust.getCompanyName().trim();
	    } else if ((cust.getSalutation() != null) && 
	      (!cust.getSalutation().equals(""))) {
	      custName = 
	        cust.getSalutation() + " " + cust.getFullname().trim();
	    } else {
	      custName = cust.getFullname().trim();
	    }
	    

	    String fullname = "";
	    if ((cust.getCustPrintableName() != null) && 
	      (!cust.getCustPrintableName().equals(""))) {
	      fullname = custName;
	    } else {
	      fullname = custName;
	    }
	    

	    Phrase nameCellVal = new Phrase(fullname, font10bold);
	    PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
	    
	    nameCellValCell.setBorder(0);
	    nameCellValCell.setHorizontalAlignment(0);
	    


	    Phrase email1 = new Phrase("Email", font10bold);
	    PdfPCell emailCell = new PdfPCell(email1);
	    emailCell.setBorder(0);
	    emailCell.setHorizontalAlignment(0);
	    


	    Phrase emailVal = null;
	    if (cust.getEmail() != null) {
	      emailVal = new Phrase(cust.getEmail(), font10);
	    } else {
	      emailVal = new Phrase("", font10);
	    }
	    PdfPCell emailValCell = new PdfPCell(emailVal);
	    
	    emailValCell.setBorder(0);
	    emailValCell.setColspan(4);
	    emailValCell.setHorizontalAlignment(0);
	    
	    Phrase mobNo = new Phrase("Phone", font10bold);
	    PdfPCell mobNoCell = new PdfPCell(mobNo);
	    mobNoCell.setBorder(0);
	    mobNoCell.setHorizontalAlignment(0);
	    

	    String phone = "";
	    String landline = "";
	    
	    if ((cust.getCellNumber1() != null) && (cust.getCellNumber1().longValue() != 0L)) {
	      System.out.println("pn11");
	      phone = cust.getCellNumber1()+"";
	    }
	    if ((cust.getCellNumber2() != null) && (cust.getCellNumber2().longValue() != 0L))
	    {
	      if (!phone.trim().isEmpty())
	      {
	        phone = phone + " / " + cust.getCellNumber2();
	      }
	      else {
	        phone = cust.getCellNumber2()+"";
	      }
	      System.out.println("pn33" + phone);
	    }
	    if ((cust.getLandline().longValue() != 0L) && (cust.getLandline() != null))
	    {
	      if (!phone.trim().isEmpty()) {
	        phone = phone + " / " + cust.getLandline();
	      }
	      else {
	        phone = cust.getLandline()+"";
	      }
	      System.out.println("pn44" + phone);
	    }
	    
	    Phrase mobVal = new Phrase(phone, font10);
	    PdfPCell mobValCell = new PdfPCell(mobVal);
	    
	    mobValCell.setBorder(0);
	    mobValCell.setColspan(4);
	    mobValCell.setHorizontalAlignment(0);
	    


	    Phrase address = new Phrase("Address", font10bold);
	    

	    PdfPCell addressCell = new PdfPCell(address);
	    
	    addressCell.setBorder(0);
	    addressCell.setHorizontalAlignment(0);
	    

	    String adrsValString = "";
	    if ((con.getNewcustomerAddress() != null) && 
	      (con.getNewcustomerAddress().getAddrLine1() != null) && 
	      (!con.getNewcustomerAddress().getAddrLine1().equals("")) && 
	      (con.getNewcustomerAddress().getCountry() != null) && 
	      (!con.getNewcustomerAddress().getCountry().equals("")))
	    {

	      adrsValString = con.getNewcustomerAddress().getCompleteAddress().trim();
	    } else {
	      adrsValString = cust.getAdress().getCompleteAddress().trim();
	    }
	    
	    Phrase addressVal = new Phrase(adrsValString, font8);
	    


	    PdfPCell addressValCell = new PdfPCell(addressVal);
	    
	    addressValCell.setBorder(0);
	    addressValCell.setHorizontalAlignment(0);
	    
	    String gstTinStr = "";
	    if ((invoiceentity.getGstinNumber() != null) && 
	      (!invoiceentity.getGstinNumber().equals(""))) {
	      gstTinStr = invoiceentity.getGstinNumber().trim();
	    }
	    else {
	     
	      gstTinStr = getGSTINOfCustomer(cust, 
	        invoiceentity.getCustomerBranch());
	    }
	    


	    Phrase gstTin = new Phrase("GSTIN", font10bold);
	    PdfPCell gstTinCell = new PdfPCell(gstTin);
	    
	    gstTinCell.setBorder(0);
	    gstTinCell.setHorizontalAlignment(0);
	    
	    Phrase gstTinVal = new Phrase(gstTinStr, font10);
	    PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
	    
	    gstTinValCell.setBorder(0);
	    gstTinValCell.setHorizontalAlignment(0);
	    




	    Phrase stateCode = new Phrase("Code", font8bold);
	    




	    PdfPCell stateCodeCell = new PdfPCell(stateCode);
	    stateCodeCell.setBorder(0);
	    stateCodeCell.setHorizontalAlignment(0);
	    
	    String stCo = "";
	    for (int i = 0; i < stateList.size(); i++)
	    {
	      if (((State)stateList.get(i)).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim())) {
	        stCo = ((State)stateList.get(i)).getStateCode().trim();
	        break;
	      }
	    }
	    
	    Phrase stateCodeVal = new Phrase(stCo, font8);
	    

	    PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
	    stateCodeValCell.setBorder(0);
	    stateCodeValCell.setHorizontalAlignment(0);
	    
	    Phrase attn = new Phrase("Attn", font10bold);
	    PdfPCell attnCell = new PdfPCell(attn);
	    
	    attnCell.setBorder(0);
	    attnCell.setHorizontalAlignment(0);
	    
	    Phrase attnVal = new Phrase(invoiceentity.getPersonInfo().getPocName(), 
	      font10);
	    PdfPCell attnValCell = new PdfPCell(attnVal);
	    
	    attnValCell.setBorder(0);
	    attnValCell.setHorizontalAlignment(0);
	    
	    colonTable.addCell(nameCell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(nameCellValCell);
	    if (printAttnInPdf) {
	      colonTable.addCell(attnCell);
	      colonTable.addCell(colonCell);
	      colonTable.addCell(attnValCell);
	    }
	    colonTable.addCell(addressCell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(addressValCell);
	    

	    PdfPTable colonTable2 = new PdfPTable(6);
	    colonTable2.setWidthPercentage(100.0F);
	    try {
	      colonTable2.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 0.2F, 
	        2.0F });
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    PdfPTable email_MobNumber = new PdfPTable(6);
	    

	    email_MobNumber.setWidthPercentage(100.0F);
	    try {
	      email_MobNumber.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 
	        0.2F, 2.0F });
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    Phrase colon2 = new Phrase(":", font10bold);
	    PdfPCell colon2Cell = new PdfPCell(colon2);
	    colon2Cell.setBorder(0);
	    colon2Cell.setHorizontalAlignment(0);
	    

	    /**
	     * nidhi
	     * 27-04-2018
	     * for if email details not available
	     */
	    logger.log(Level.SEVERE," get email addresss  lenth -- " + cust.getEmail().trim().length());
	    if(cust.getEmail() != null && cust.getEmail().trim().length() >0){

		    email_MobNumber.addCell(emailCell);
		    email_MobNumber.addCell(colon2Cell);
		    email_MobNumber.addCell(emailValCell);
	    }
	    
	    email_MobNumber.addCell(mobNoCell);
	    email_MobNumber.addCell(colon2Cell);
	    email_MobNumber.addCell(mobValCell);
	    




	    colonTable2.addCell(gstTinCell);
	    colonTable2.addCell(colon2Cell);
	    colonTable2.addCell(gstTinValCell);
	    blankCell.setBorder(0);
	    colonTable2.addCell(blankCell);
	    colonTable2.addCell(blankCell);
	    colonTable2.addCell(blankCell);
	    


	    Phrase stateNme = new Phrase("State ", font8bold);
	    PdfPCell stateNmecell = new PdfPCell(stateNme);
	    stateNmecell.setBorder(0);
	    stateNmecell.setHorizontalAlignment(0);
	    

	    Phrase stateval = new Phrase(cust.getAdress().getState().trim(), font8);
	    PdfPCell statevalcell = new PdfPCell(stateval);
	    statevalcell.setBorder(0);
	    statevalcell.setHorizontalAlignment(0);
	    
	    colonTable2.addCell(stateNmecell);
	    colonTable2.addCell(colon2Cell);
	    colonTable2.addCell(statevalcell);
	    
	    colonTable2.addCell(stateCodeCell);
	    colonTable2.addCell(colon2Cell);
	    colonTable2.addCell(stateCodeValCell);
	    
	    PdfPCell cell1 = new PdfPCell(colonTable);
	    cell1.setBorder(0);
	    
	    PdfPCell email_Cell = new PdfPCell(email_MobNumber);
	    email_Cell.setBorder(0);
	    
	    PdfPCell cell12 = new PdfPCell(colonTable2);
	    cell12.setBorder(0);
	    

	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    colonTable.addCell(colonCell);
	    
	    PdfPTable pdfStateTable = new PdfPTable(4);
	    pdfStateTable.setWidthPercentage(100.0F);
	    try {
	      pdfStateTable.setWidths(columnStateCodeCollonWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    
	    PdfPCell state4Cell = new PdfPCell(pdfStateTable);
	    state4Cell.setBorder(0);
	    state4Cell.setHorizontalAlignment(0);
	    
	    colonTable.addCell(state4Cell);
	    
	    PdfPCell stateCell_stateCode = new PdfPCell(colonTable);
	    stateCell_stateCode.setBorder(0);
	    
	    part1Table.addCell(cell1);
	    

	    part1Table.addCell(email_Cell);
	    part1Table.addCell(cell12);
	    
	    /**
	     * Date:17/09/2018
	     * Added by Ashwini
	     * Des:To add blankspace to fit logo in case of servicewise billing
	     * @author Anil , Date : 30-03-2019
	     * As we are not printing service  address unless the process configuration is active
	     * raiesd by rahul tiwari for orion
	     */
	    
		if (!serviceAddressFlag) {
//			if (con.isServiceWiseBilling()) {
				Phrase blank1 = new Phrase(" ");
				PdfPCell blank1Cell = new PdfPCell(blank1);
				blank1Cell.setBorder(0);
				part1Table.addCell(blank1Cell);
				part1Table.addCell(blank1Cell);
				part1Table.addCell(blank1Cell);
				part1Table.addCell(blank1Cell);
				part1Table.addCell(blank1Cell);
				part1Table.addCell(blank1Cell);
//			} else {
//				Phrase blank1 = new Phrase(" ");
//				PdfPCell blank1Cell = new PdfPCell(blank1);
//				blank1Cell.setBorder(0);
//				part1Table.addCell(blank1Cell);
//			}
		}
	    /*
	     * End by Ashwini
	     */
	    
	    PdfPCell part1TableCell = new PdfPCell(part1Table);
	    
	    //vijay
//	    part1TableCell.addElement(part1Table);
	    /** Date 14-04-2018 By vijay above line commented and below line added because billing address was not displaying properly**/
	    mainHeaderTable.addCell(part1TableCell);
	    







	    PdfPTable part2Table = new PdfPTable(1);
	    part2Table.setWidthPercentage(100.0F);
	    
	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    


	    Phrase serviceaddress = new Phrase("Service Address", font8bold);
	    PdfPCell serviceCell = new PdfPCell(serviceaddress);
	    serviceCell.setHorizontalAlignment(1);
	    part2Table.addCell(serviceCell);
	    

	    Phrase name2 = new Phrase("Name", font10bold);
	    PdfPCell name2Cell = new PdfPCell(name2);
	    
	    name2Cell.setBorder(0);
	    name2Cell.setHorizontalAlignment(0);
	    
	    Phrase name2CellVal = new Phrase(fullname, font10bold);
	    PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
	    
	    name2CellValCell.setBorder(0);
	    name2CellValCell.setHorizontalAlignment(0);
	    

	    Phrase email2 = new Phrase("Email", font10bold);
	    PdfPCell email1Cell = new PdfPCell(email2);
	    email1Cell.setBorder(0);
	    email1Cell.setHorizontalAlignment(0);
	    


	    Phrase email1Val = null;
	    if (cust.getEmail() != null) {
	      email1Val = new Phrase(cust.getEmail(), font10);
	    } else {
	      email1Val = new Phrase("", font10);
	    }
	    PdfPCell email1ValCell = new PdfPCell(email1Val);
	    
	    email1ValCell.setBorder(0);
	    email1ValCell.setColspan(4);
	    email1ValCell.setHorizontalAlignment(0);
	    
	    Phrase mobNo2 = new Phrase("Mobile", font10bold);
	    PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
	    mobNo2Cell.setBorder(0);
	    mobNo2Cell.setHorizontalAlignment(0);
	    

	    String phone2 = "";
	    

	    if ((cust.getCellNumber1() != null) && (cust.getCellNumber1().longValue() != 0L)) {
	      System.out.println("pn11");
	      phone2 = cust.getCellNumber1()+"";
	    }
	    if ((cust.getCellNumber2() != null) && (cust.getCellNumber2().longValue() != 0L))
	    {
	      if (!phone2.trim().isEmpty())
	      {
	        phone2 = phone2 + " / " + cust.getCellNumber2();
	      }
	      else {
	        phone2 = cust.getCellNumber2()+"";
	      }
	      System.out.println("pn33" + phone2);
	    }
	    if ((cust.getLandline().longValue() != 0L) && (cust.getLandline() != null))
	    {
	      if (!phone2.trim().isEmpty()) {
	        phone2 = phone2 + " / " + cust.getLandline();
	      }
	      else {
	        phone2 = cust.getLandline()+"";
	      }
	      System.out.println("pn44" + phone2);
	    }
	    

	    Phrase mobNo2Val = new Phrase(phone2, font10);
	    PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);
	    
	    mobNo2ValCell.setBorder(0);
	    mobNo2ValCell.setHorizontalAlignment(0);
	    
	    Phrase landlineph2 = new Phrase("LandLine", font10bold);
	    PdfPCell landlineCell2 = new PdfPCell(landlineph2);
	    landlineCell2.setBorder(0);
	    landlineCell2.setHorizontalAlignment(0);
	    
	    String landline2 = "";
	    if (cust.getLandline().longValue() != 0L) {
	      landline2 = cust.getLandline()+"";
	    } else {
	      landline2 = "";
	    }
	    
	    Phrase landlineVal2 = new Phrase(landline2, font10);
	    PdfPCell landlinevalCells2 = new PdfPCell(landlineVal2);
	    
	    landlinevalCells2.setBorder(0);
	    landlinevalCells2.setHorizontalAlignment(0);
	    


	    Phrase address2 = new Phrase("Address", font10bold);
	    

	    PdfPCell address2Cell = new PdfPCell(address2);
	    
	    address2Cell.setBorder(0);
	    address2Cell.setHorizontalAlignment(0);
	    







	    String adrsValString1 = "";
	    String customerPoc = null;
	    
	    Address serviceAddress = null;
	    
	    if ((invoiceentity.getCustomerBranch() != null) && 
	      (!invoiceentity.getCustomerBranch().equals(""))) {
	      CustomerBranchDetails branch = 
	      
	        (CustomerBranchDetails)ObjectifyService.ofy().load().type(CustomerBranchDetails.class).filter("companyId", comp.getCompanyId()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).filter("cinfo.count", Integer.valueOf(invoiceentity.getPersonInfo().getCount())).first().now();
	      
	      if (branch != null) {
	        serviceAddress = branch.getAddress();
	        System.out.println("heeeeeeeelllllllooo" + serviceAddress);
	        
	        adrsValString1 = branch.getAddress().getCompleteAddress();

	      }
	      

	    }
	    else if (con.getCustomerServiceAddress() != null) {
	      serviceAddress = con.getCustomerServiceAddress();
	      adrsValString1 = con.getCustomerServiceAddress()
	        .getCompleteAddress();
	    }
	    else {
	      serviceAddress = cust.getSecondaryAdress();
	      adrsValString1 = cust.getSecondaryAdress().getCompleteAddress();
	    }
	    


	    if ((!cust.getSecondaryAdress().getAddrLine1().equals("")) && 
	      (customerbranchlist.size() == 0)) {
	      customerPoc = invoiceentity.getPersonInfo().getPocName();
	    }
	    
	    System.out.println("Inside Customer branch  " + 
	      customerbranchlist.size());
	    for (int i = 0; i < customerbranchlist.size(); i++)
	    {
	      customerPoc = ((CustomerBranchDetails)customerbranchlist.get(i)).getPocName();
	    }
	    


	    Phrase address2Val = new Phrase(adrsValString1, font13);
	    



	    PdfPCell address2ValCell = new PdfPCell(address2Val);
	    address2ValCell.setBorder(0);
	    address2ValCell.setHorizontalAlignment(0);
	    
	    String gstTin2Str = "";
	    if ((invoiceentity.getGstinNumber() != null) && 
	      (!invoiceentity.getGstinNumber().equalsIgnoreCase(""))) {
	      gstTin2Str = invoiceentity.getGstinNumber().trim();
	    } else {
	      
	      gstTin2Str = getGSTINOfCustomer(cust, 
	        invoiceentity.getCustomerBranch());
	    }
	    

	    Phrase gstTin2 = new Phrase("GSTIN", font10bold);
	    PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
	    gstTin2Cell.setBorder(0);
	    gstTin2Cell.setHorizontalAlignment(0);
	    
	    Phrase gstTin2Val = new Phrase(gstTin2Str, font10);
	    PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
	    gstTin2ValCell.setBorder(0);
	    gstTin2ValCell.setHorizontalAlignment(0);
	    
	    Phrase attn2 = new Phrase("Attn", font10bold);
	    PdfPCell attn2Cell = new PdfPCell(attn2);
	    attn2Cell.setBorder(0);
	    attn2Cell.setHorizontalAlignment(0);
	    
	    Phrase attn2Val = null;
	    if (custName != null) {
	      attn2Val = new Phrase(customerPoc, font10);
	    } else {
	      attn2Val = new Phrase("", font10);
	    }
	    PdfPCell attn2ValCell = new PdfPCell(attn2Val);
	    attn2ValCell.setBorder(0);
	    attn2ValCell.setHorizontalAlignment(0);
	    
	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    colonTable.addCell(nameCell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(nameCellValCell);
	    if (printAttnInPdf) {
	      colonTable.addCell(attn2Cell);
	      colonTable.addCell(colonCell);
	      colonTable.addCell(attn2ValCell);
	    }
	    colonTable.addCell(address2Cell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(address2ValCell);
	    
	    PdfPTable colonTable22 = new PdfPTable(3);
	    colonTable22.setWidthPercentage(100.0F);
	    try {
	      colonTable22.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    PdfPTable email_MobNumber2 = new PdfPTable(6);
	    email_MobNumber2.setWidthPercentage(100.0F);
	    try {
	      email_MobNumber2.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 
	        0.2F, 2.0F });
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    /**
	     * nidhi
	     */
	    if(cust.getEmail() != null && cust.getEmail().trim().length() >0 ){

		    email_MobNumber2.addCell(emailCell);
		    email_MobNumber2.addCell(colonCell);
		    email_MobNumber2.addCell(emailValCell);
	    }
	    
	    email_MobNumber2.addCell(mobNoCell);
	    email_MobNumber2.addCell(colonCell);
	    email_MobNumber2.addCell(mobValCell);
	    








	    PdfPCell cell2 = new PdfPCell(colonTable);
	    cell2.setBorder(0);
	    
	    PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
	    email_MobNumber2Cell.setBorder(0);
	    
	    PdfPCell cell22 = new PdfPCell(colonTable22);
	    cell22.setBorder(0);
	    
	    Phrase state2 = new Phrase("State", font13bold);
	    

	    PdfPCell state2Cell = new PdfPCell(state2);
	    state2Cell.setBorder(0);
	    state2Cell.setHorizontalAlignment(0);
	    
	    String stateName = "";
	    if ((!cust.getSecondaryAdress().getAddrLine1().equals("")) && 
	      (customerbranchlist.size() == 0)) {
	      stateName = cust.getSecondaryAdress().getState().trim();
	    } else { int i;
	      for (int i1 = 0; i1 < customerbranchlist.size(); i1++)
	      {
	        stateName = 
	          ((CustomerBranchDetails)customerbranchlist.get(i1)).getAddress().getState().trim();
	      }
	    }
	    
	    Phrase state2Val = new Phrase(stateName, font13);
	    


	    PdfPCell state2ValCell = new PdfPCell(state2Val);
	    
	    state2ValCell.setBorder(0);
	    state2ValCell.setHorizontalAlignment(0);
	    
	    String st2Co = "";
	    int j; 
	    for (int i = 0; i < stateList.size(); i++)
	    {
	      if ((stateList.get(i)).getStateName().trim().equalsIgnoreCase(stateName.trim())) {
	        st2Co = (stateList.get(i)).getStateCode().trim();
	        break;
	      }
	    }
	    
	    Phrase state2Code = new Phrase("Code:", font13bold);
	    PdfPCell state2CodeCell = new PdfPCell(state2Code);
	    state2CodeCell.setBorder(0);
	    state2CodeCell.setHorizontalAlignment(0);
	    
	    Phrase state2CodeVal = new Phrase(st2Co, font13);
	    


	    PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
	    state2CodeValCell.setBorder(0);
	    state2CodeValCell.setHorizontalAlignment(0);
	    
	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    

	    PdfPTable statetable1 = new PdfPTable(3);
	    statetable1.setWidthPercentage(100.0F);
	    try {
	      statetable1.setWidths(new float[] { 2.0F, 1.0F, 2.0F });
	    }
	    catch (Exception localException3) {}
	    


	    statetable1.addCell(state2ValCell);
	    statetable1.addCell(state2CodeCell);
	    statetable1.addCell(state2CodeValCell);
	    
	    PdfPCell statetablecell = new PdfPCell(statetable1);
	    statetablecell.setBorder(0);
	    
	    colonTable.addCell(gstTin2Cell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(gstTin2ValCell);

	    	    
	    
	    colonTable.addCell(state2Cell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(statetablecell);
	    
	  

	    PdfPCell stateCell_stateCode2 = new PdfPCell(colonTable);
	    stateCell_stateCode2.setBorder(0);
	    
	    
	    
	    part2Table.addCell(cell2);
	    
	    part2Table.addCell(email_MobNumber2Cell);
	    
	    
	    part2Table.addCell(stateCell_stateCode2);
	    part2Table.addCell(cell22);
	    
	    PdfPCell part2TableCell = new PdfPCell(part2Table);
	    

	    Phrase blankph = new Phrase("", font8);
	    PdfPCell blnkCell = new PdfPCell(blankph);
	    blnkCell.setBorderWidthBottom(0.0F);
	    blnkCell.setBorderWidthLeft(0.0F);
	    blnkCell.setBorderWidthTop(0.0F);
	    
//	    mainHeaderTable.addCell(part2TableCell);
	    
	    /**
	     * @author Anil ,Date : 30-03-2019
	     * checked service address flag
	     * raised by rahul tiwari for orion
	     */
	    if(serviceAddressFlag){
			if (!con.isServiceWiseBilling()) {
				if (custbranchlist.size() == 0) {
					System.out.println("No ACtion");
					/**
					 * Date 14-04-2018 By vijay for if there is no customer branches
					 * then also print address
					 **/
					mainHeaderTable.addCell(part2TableCell);
					mainHeaderTable.addCell(blankCell);
	
				} else {
					mainHeaderTable.addCell(part2TableCell);
					mainHeaderTable.addCell(blankCell);
				}
			}
	    }
	    
	    
	    try
	    {
	      document.add(mainTable);
	      document.add(mainHeaderTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  





	  public void createProductDescription()
	  {
	    if (!productDescFlag) {
	      return;
	    }
	    



	    PdfPTable prodDescriptionTbl = new PdfPTable(2);
	    prodDescriptionTbl.setWidthPercentage(100.0F);
	    try
	    {
	      prodDescriptionTbl.setWidths(new float[] { 40.0F, 60.0F });
	    } catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    Phrase prodDescriptionVal = new Phrase("", font8);
	    String prodDescriptionValue = "";
	    for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++)
	    {
	      Phrase productIdLbl = new Phrase("Product Id : " + 
	        invoiceentity.getSalesOrderProducts().get(i).getProdId(), 
	        font10bold);
	      PdfPCell productIdLblCell = new PdfPCell(productIdLbl);
	      productIdLblCell.setHorizontalAlignment(0);
	      productIdLblCell.setVerticalAlignment(5);
	      productIdLblCell.setBorder(0);
	      prodDescriptionTbl.addCell(productIdLblCell);
	      
	      String prodNameValue = invoiceentity.getSalesOrderProducts().get(i)
	        .getProdName();
	      
	      Phrase prodNameLbl = new Phrase("Product Name : " + prodNameValue, 
	        font10bold);
	      PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
	      prodlblCell.setHorizontalAlignment(0);
	      prodlblCell.setVerticalAlignment(5);
	      prodlblCell.setBorder(0);
	      
	      prodDescriptionTbl.addCell(prodlblCell);
	      
	  	prodDescriptionValue = "";

		if (invoiceentity.getSalesOrderProducts().get(i).getProdDesc1() != null
				&& !invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc1().equals("")
				&& invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc2() != null
				&& !invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc2().equals("")) {
			prodDescriptionValue = invoiceentity.getSalesOrderProducts()
					.get(i).getProdDesc1()
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2();
		} else if (invoiceentity.getSalesOrderProducts().get(i)
				.getProdDesc1() == null
				&& invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc1().equals("")
				&& invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc2() != null
				&& !invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc2().equals("")) {
			prodDescriptionValue = invoiceentity.getSalesOrderProducts()
					.get(i).getProdDesc2();
		} else if (invoiceentity.getSalesOrderProducts().get(i)
				.getProdDesc1() != null
				&& !invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc1().equals("")
				&& invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc2() == null
				&& invoiceentity.getSalesOrderProducts().get(i)
						.getProdDesc2().equals("")) {
			prodDescriptionValue = invoiceentity.getSalesOrderProducts()
					.get(i).getProdDesc1();
		} else {
			prodDescriptionValue = "";

		}

		prodDescriptionVal = new Phrase("" + prodDescriptionValue, font8);
	}
	Paragraph value = new Paragraph(prodDescriptionVal);
	value.setAlignment(Element.ALIGN_LEFT);
	    



	    try
	    {
	      if (!prodDescriptionValue.equals("")) {
	        document.add(Chunk.NEXTPAGE);
	        document.add(prodDescriptionTbl);
	        document.add(value);
	      }
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  









	  private void createHeader()
	  {
	    DocumentUpload logodocument = comp.getLogo();
	    


	    String environment = 
	      System.getProperty("com.google.appengine.runtime.environment");
	    String hostUrl; 
	    if (environment.equals("Production")) {
	      String applicationId = 
	        System.getProperty("com.google.appengine.application.id");
	      String version = 
	        System.getProperty("com.google.appengine.application.version");
	      hostUrl = "http://" + version + "." + applicationId + 
	        ".appspot.com/";
	    } else {
	      hostUrl = "http://localhost:8888";
	    }
	    PdfPCell imageSignCell = null;
	    Image image2 = null;
	    try {
	      image2 = 
	        Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
	      image2.scalePercent(20.0F);
	      


	      imageSignCell = new PdfPCell(image2);
	      imageSignCell.setBorder(0);
	      imageSignCell.setHorizontalAlignment(0);
	      imageSignCell.setFixedHeight(40.0F);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    







	    PdfPTable logoTab = new PdfPTable(1);
	    logoTab.setWidthPercentage(100.0F);
	    
	    if (imageSignCell != null) {
	      logoTab.addCell(imageSignCell);
	    } else {
	      Phrase logoblank = new Phrase(" ");
	      PdfPCell logoblankcell = new PdfPCell(logoblank);
	      logoblankcell.setBorder(0);
	      logoTab.addCell(logoblankcell);
	    }
	    


	    Phrase companyName = null;
	    if (comp != null) {
	      companyName = new Phrase(comp.getBusinessUnitName().trim(), 
	        font16bold);
	    }
	    
	    Paragraph companyNamepara = new Paragraph();
	    companyNamepara.add(companyName);
	    





	    if (checkheaderLeft) {
	      companyNamepara.setAlignment(0);
	    }
	    else if (checkheaderRight) {
	      companyNamepara.setAlignment(2);
	    }
	    else {
	      companyNamepara.setAlignment(1);
	    }
	    


	    PdfPCell companyNameCell = new PdfPCell();
	    companyNameCell.addElement(companyNamepara);
	    companyNameCell.setBorder(0);
	    companyNameCell.setHorizontalAlignment(1);
	    
	    Phrase companyAddr = null;
	    if (comp != null) {
	      companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
	        .trim(), font12);
	    }
	    Paragraph companyAddrpara = new Paragraph();
	    companyAddrpara.add(companyAddr);
	    





	    if (checkheaderLeft) {
	      companyAddrpara.setAlignment(0);
	    }
	    else if (checkheaderRight) {
	      companyAddrpara.setAlignment(2);
	    }
	    else {
	      companyAddrpara.setAlignment(1);
	    }
	    






	    PdfPCell companyAddrCell = new PdfPCell(companyAddrpara);
	    
	    companyAddrCell.setBorder(0);
	    if (checkheaderLeft) {
	      companyAddrCell.setHorizontalAlignment(0);
	    }
	    else if (checkheaderRight) {
	      companyAddrCell.setHorizontalAlignment(2);
	    }
	    else {
	      companyAddrCell.setHorizontalAlignment(1);
	    }
	    





	    Phrase companyGSTTIN = null;
	    String gstinValue = "";
	    if (UniversalFlag)
	    {
	      if (con.getGroup().equalsIgnoreCase("Universal Pest Control Pvt. Ltd."))
	      {
	        for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	        {
	          if (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("GSTIN")) {
	            gstinValue = 
	            

	              ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName() + " : " + ((ArticleType)comp.getArticleTypeDetails().get(i))
	              .getArticleTypeValue().trim();
	            break;
	          }
	        }
	        
	        for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	        {
	          if (!((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("GSTIN")) {
	            gstinValue = 
	            



	              gstinValue + "," + ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName() + " : " + ((ArticleType)comp.getArticleTypeDetails().get(i))
	              .getArticleTypeValue().trim();
	          }
	        }
	      }
	    }
	    else {
	      for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	      {
	        if (((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("GSTIN")) {
	          gstinValue = 
	          

	            ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName() + " : " + ((ArticleType)comp.getArticleTypeDetails().get(i))
	            .getArticleTypeValue().trim();
	          break;
	        }
	      }
	      
	      for (int i = 0; i < comp.getArticleTypeDetails().size(); i++)
	      {
	        if (!((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName().equalsIgnoreCase("GSTIN")) {
	          gstinValue = 
	          



	            gstinValue + "," + ((ArticleType)comp.getArticleTypeDetails().get(i)).getArticleTypeName() + " : " + ((ArticleType)comp.getArticleTypeDetails().get(i))
	            .getArticleTypeValue().trim();
	        }
	      }
	    }
	    
	    if (!gstinValue.equals("")) {
	      companyGSTTIN = new Phrase(gstinValue, font12bold);
	    }
	    
	    Paragraph companyGSTTINpara = new Paragraph();
	    companyGSTTINpara.add(companyGSTTIN);
	    companyGSTTINpara.setAlignment(1);
	    
	    PdfPCell companyGSTTINCell = new PdfPCell();
	    companyGSTTINCell.addElement(companyGSTTINpara);
	    companyGSTTINCell.setBorder(0);
	    companyGSTTINCell.setHorizontalAlignment(1);
	    






	    String branchmail = "";
	    ServerAppUtility serverApp = new ServerAppUtility();
	    
	    if (checkEmailId) {
	      branchmail = serverApp.getBranchEmail(comp, invoiceentity.getBranch());
	      System.out.println("server method " + branchmail);
	    }
	    else {
	      branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
	      System.out.println("server method 22" + branchmail);
	    }
	    

	    String email = null;
	    if (branchmail != null) {
	      email = "Email : " + branchmail;
	    }
	    else {
	      email = "Email : ";
	    }
	    




	    String website = "";

	    if (comp.getWebsite()!=null&&!comp.getWebsite().equals("")) {
	    	
		      website = "Website : " + comp.getWebsite();
		  }
		  else {
		      website = " " ;
		  }


	    String number = "";
	    String landline = "";
	    
	    if ((comp.getCellNumber1() != null) && (comp.getCellNumber1().longValue() != 0L)) {
	      System.out.println("pn11");
	      number = comp.getCellNumber1()+"";
	    }
	    if ((comp.getCellNumber2() != null) && (comp.getCellNumber2().longValue() != 0L))
	    {
	      if (!number.trim().isEmpty())
	      {
	        number = number + " , " + comp.getCellNumber2();
	      }
	      else {
	        number = comp.getCellNumber2()+"";
	      }
	      System.out.println("pn33" + number);
	    }
	    if ((comp.getLandline().longValue() != 0L) && (comp.getLandline() != null))
	    {
	      if (!number.trim().isEmpty()) {
	        number = number + " , " + comp.getLandline();
	      }
	      else {
	        number = comp.getLandline()+"";
	      }
	      System.out.println("pn44" + number);
	    }
	    
	    PdfPCell companyEmailandwebCell = null;
	    if ((number != null) && (!number.trim().isEmpty())) {
	      companyEmailandwebCell = new PdfPCell(new Phrase(email + " " + "Phone " + number, font11));
	    } else {
	      companyEmailandwebCell = new PdfPCell(new Phrase(email, font11));
	    }
	    




	    companyEmailandwebCell.setBorder(0);
	    if (checkheaderLeft) {
	      companyEmailandwebCell.setHorizontalAlignment(0);
	    }
	    else if (checkheaderRight) {
	      companyEmailandwebCell.setHorizontalAlignment(2);
	    }
	    else {
	      companyEmailandwebCell.setHorizontalAlignment(1);
	    }
	    



	    PdfPCell companymob = new PdfPCell(new Phrase(website, 
	      font11));
	    
	    companymob.setBorder(0);
	    




	    if (checkheaderLeft) {
	      companymob.setHorizontalAlignment(0);
	    }
	    else if (checkheaderRight) {
	      companymob.setHorizontalAlignment(2);
	    }
	    else {
	      companymob.setHorizontalAlignment(1);
	    }
	    




	    PdfPTable pdfPTable = new PdfPTable(1);
	    
	    pdfPTable.addCell(companyNameCell);
	    pdfPTable.addCell(companyAddrCell);
	    pdfPTable.addCell(companyEmailandwebCell);
	    pdfPTable.addCell(companymob);
	    




	    PdfPTable mainheader = new PdfPTable(2);
	    mainheader.setWidthPercentage(100.0F);
	    try
	    {
	      mainheader.setWidths(new float[] { 20.0F, 80.0F });
	    }
	    catch (DocumentException e2) {
	      e2.printStackTrace();
	    }
	    
	    if (imageSignCell != null) {
	      PdfPCell leftCell = new PdfPCell(logoTab);
	      leftCell.setBorder(0);
	      mainheader.addCell(leftCell);
	      
	      PdfPCell rightCell = new PdfPCell(pdfPTable);
	      rightCell.setBorder(0);
	      mainheader.addCell(rightCell);
	    } else {
	      PdfPCell rightCell = new PdfPCell(pdfPTable);
	      rightCell.setBorder(0);
	      rightCell.setColspan(2);
	      mainheader.addCell(rightCell);
	    }
	    try
	    {
	      document.add(mainheader);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    

	    float[] myWidth = { 1.0F, 3.0F, 20.0F, 17.0F, 3.0F, 30.0F, 17.0F, 3.0F, 20.0F, 1.0F };
	    PdfPTable mytbale = new PdfPTable(10);
	    mytbale.setWidthPercentage(100.0F);
	    mytbale.setSpacingAfter(5.0F);
	    mytbale.setSpacingBefore(5.0F);
	    try
	    {
	      mytbale.setWidths(myWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    
	    Phrase myblank = new Phrase(" ", font10);
	    PdfPCell myblankCell = new PdfPCell(myblank);
	    
	    myblankCell.setHorizontalAlignment(1);
	    
	    Phrase myblankborderZero = new Phrase(" ", font10);
	    PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
	    
	    myblankborderZeroCell.setBorder(0);
	    myblankborderZeroCell.setHorizontalAlignment(1);
	    
	    Phrase stat1Phrase = new Phrase("Original for Receipient", font10);
	    PdfPCell stat1PhraseCell = new PdfPCell(stat1Phrase);
	    stat1PhraseCell.setBorder(0);
	    stat1PhraseCell.setHorizontalAlignment(0);
	    stat1PhraseCell.setVerticalAlignment(5);
	    
	    Phrase stat2Phrase = new Phrase("Duplicate for Supplier/Transporter", 
	      font10);
	    PdfPCell stat2PhraseCell = new PdfPCell(stat2Phrase);
	    stat2PhraseCell.setBorder(0);
	    stat2PhraseCell.setHorizontalAlignment(0);
	    stat2PhraseCell.setVerticalAlignment(5);
	    
	    Phrase stat3Phrase = new Phrase("Triplicate for Supplier", font10);
	    PdfPCell stat3PhraseCell = new PdfPCell(stat3Phrase);
	    stat3PhraseCell.setBorder(0);
	    stat3PhraseCell.setHorizontalAlignment(0);
	    stat3PhraseCell.setVerticalAlignment(5);
	    











	    PdfPTable tab = new PdfPTable(1);
	    tab.setWidthPercentage(100.0F);
	    
	    PdfPCell cell = new PdfPCell(mytbale);
	    tab.addCell(cell);
	    






	    String titlepdf = "";
	    

	    if (("Proforma Invoice".equals(invoiceentity.getInvoiceType().trim())) || 
	    
	      (invoiceentity.getInvoiceType().trim().equals("Proforma Invoice"))) {
	      titlepdf = "Proforma Invoice";
	    } else {
	      titlepdf = "Tax Invoice";
	    }
	    Phrase titlephrase = new Phrase(titlepdf, font14bold);
	    Paragraph titlepdfpara = new Paragraph();
	    titlepdfpara.add(titlephrase);
	    titlepdfpara.setAlignment(1);
	    
	    PdfPCell titlecell = new PdfPCell();
	    titlecell.addElement(titlepdfpara);
	    titlecell.setBorder(0);
	    
	    Phrase blankphrase = new Phrase("", font8);
	    PdfPCell blankCell = new PdfPCell();
	    blankCell.addElement(blankphrase);
	    blankCell.setBorder(0);
	    
	    PdfPTable titlepdftable = new PdfPTable(3);
	    titlepdftable.setWidthPercentage(100.0F);
	    titlepdftable.setHorizontalAlignment(1);
	    titlepdftable.addCell(blankCell);
	    titlepdftable.addCell(titlecell);
	    titlepdftable.addCell(blankCell);
	    
	    Paragraph blank = new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    
	    PdfPTable parent = new PdfPTable(1);
	    parent.setWidthPercentage(100.0F);
	    
	    PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	    parent.addCell(titlePdfCell);
	  }

	  public void createPdfForEmailGST(Invoice invoiceDetails, Company companyEntity, Customer custEntity, Contract contractEntity, BillingDocument billingEntity, List<CustomerBranchDetails> custbranchlist, ServiceGSTInvoice invpdf, Document document)
	  {
	    Invoice invoiceentity = invoiceDetails;
	    long count = invoiceentity.getId().longValue();
	    logger.log(Level.SEVERE, " Count: " + count);
	    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
	    TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	    sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	    
	    boolean flag = false;
	    try {
	      flag = invoiceentity.getInvoiceDate().after(
	        sdf.parse("30 Jun 2017"));
	    }
	    catch (ParseException e) {
	      e.printStackTrace();
	    }
	    
	    System.out.println("invoice Date " + invoiceentity.getInvoiceDate());
	    
	    System.out.println("SINGLE CONTRACT INVOICE");
	    
	    String preprintStatus = "plane";
	    System.out.println("ppppppppppppppppppooooooooooo" + count);
	    OrionServiceInvoicePdf invpdf1=new OrionServiceInvoicePdf();
	    invpdf1.setInvoice(Long.valueOf(count));
	    invpdf1.createPdf(preprintStatus);
	    document.close();
	  }
	  

	  public void CreateOrionFooter()
	  {
		  PdfPTable footerTable = new PdfPTable(1);
		  footerTable.setWidthPercentage(100.0F);
		  
//		  if(!isRemarkGiven) {
//			  PdfPCell blankCell =pdfUtility.getPdfCell(" ", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0);
//			  footerTable.addCell(blankCell);
//		  }
//	      PdfPCell blankCell =pdfUtility.getPdfCell("", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0);
//	      footerTable.addCell(blankCell);
	      logger.log(Level.SEVERE,"2 blank lines added before note");
//	      if(!isRemarkGiven){
//	    	  PdfPCell noteCell =pdfUtility.getPdfCell("Note : This is computer generated invoice therefore no physical signature is required.", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0);
//	    	  footerTable.addCell(noteCell);
//	      }else{
	    	  PdfPCell noteCell =pdfUtility.getPdfCell("Note : This is computer generated invoice therefore no physical signature is required.", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, -1, -1, 0, 0, 0, 0, 0, 0, 0);
		      footerTable.addCell(noteCell);
//	      }
	      

	      PdfPCell subjectCell = pdfUtility.getPdfCell("SUBJECT TO KOLKATA JURISDICTION ", font9bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0);    		     
	      footerTable.addCell(subjectCell);
	      
	      Font font12boldred=new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	      font12boldred.setColor(BaseColor.RED);
	      PdfPCell brandingCell = pdfUtility.getPdfCell("Largest Indian Owned PAN India Pest Control Operator", font12boldred, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0);    		     
	      footerTable.addCell(brandingCell);
	      
	      PdfPCell brandingCell2 = pdfUtility.getPdfCell("28 States • 8 Union Territories • 15000+ PIN Codes serviced accross India", font11bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0);    		     
	      footerTable.addCell(brandingCell2);
	      
	      

//	      PdfPCell line2Cell = pdfUtility.getPdfCell("This is Computer Generated Invoice", font11bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0);    
//	      footerTable.addCell(line2Cell);
	      
	      
//	      String line3="";
//	      if(comp.getCellNumber1()!=null&&comp.getCellNumber1()>0)
//	    	  line3+="Toll Free No. : "+comp.getCellNumber1();
//	      if(comp.getEmail()!=null&&!comp.getEmail().equals(""))
//	    	  line3+="   *   "+"Email : "+comp.getEmail();	      
//	      if (comp.getWebsite()!=null&&!comp.getWebsite().equals("")) {		    	
//	    	  line3+= "   *   "+"Website :"+comp.getWebsite();
//		  }
	      
	      
	      PdfPTable contactTable= new PdfPTable(8);
	      contactTable.setWidthPercentage(100.0F);
//	      float[] column8Width = { 0.5F, 2F, 0.5F,2F, 0.5F,2F,0.5F, 2F };
	      float[] column8Width = { 1F, 2.3F, 0.2F, 2.3F, 0.2F,2F,0.2F, 1.8F };
//	      
//	      float[] column8Width = { 2.5F, 2.5F, 2.5F,2.5F};
		    try {
		    	contactTable.setWidths(column8Width);
		    } catch (DocumentException e) {
		      e.printStackTrace();
		    }	
		    String tollno="";
		    String email="";
		    String website="";
		    String contactno="";
		    if(comp.getLandline()!=null&&comp.getLandline()>0)
		    	tollno="Toll Free No. "+comp.getLandline();
		    if(comp.getCellNumber1()!=null&&comp.getCellNumber1()>0)
		    	contactno=" "+comp.getCellNumber1();
		    if(comp.getEmail()!=null&&!comp.getEmail().equals(""))
		    	email=" "+comp.getEmail();	      
		    if (comp.getWebsite()!=null&&!comp.getWebsite().equals(""))		    	
		    	website=" "+comp.getWebsite();
			
		    Image tollFreeImg=null;
			try {
				tollFreeImg = Image.getInstance("images/tollfree.png");
			} catch (BadElementException | IOException e3) {
				e3.printStackTrace();
			}
			tollFreeImg.scaleAbsolute(15, 15);
			
			Image whatsappImg=null;
			try {
				whatsappImg = Image.getInstance("images/whatsapp.png");
			} catch (BadElementException | IOException e3) {
				e3.printStackTrace();
			}
			whatsappImg.scaleAbsolute(15, 15);
			
			Image emailImg=null;
			try {
				emailImg = Image.getInstance("images/email.png");
			} catch (BadElementException | IOException e3) {
				e3.printStackTrace();
			}
			emailImg.scaleAbsolute(15, 15);
			
			Image websiteImg=null;
			try {
				websiteImg = Image.getInstance("images/website.png");
			} catch (BadElementException | IOException e3) {
				e3.printStackTrace();
			}
			websiteImg.scaleAbsolute(15, 15);

		    
		    
		    Phrase tollPhrase=new Phrase(tollno,font10);
			Paragraph tollPara=new Paragraph();
			tollPara.setIndentationLeft(10f);
			tollPara.add(new Chunk(tollFreeImg, 0, 0, true));				
//			tollPara.add(tollPhrase);
			tollPara.setAlignment(Element.ALIGN_JUSTIFIED);
			
			PdfPCell tollCell=new PdfPCell(tollPara);
			tollCell.setBorder(0);
			tollCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			Phrase whatsappPhrase=new Phrase(contactno,font10);
			Paragraph whatsappPara=new Paragraph();
			whatsappPara.setIndentationLeft(10f);
			whatsappPara.add(new Chunk(whatsappImg, 0, 0, true));				
//			whatsappPara.add(whatsappPhrase);
			whatsappPara.setAlignment(Element.ALIGN_JUSTIFIED);
			
			PdfPCell whatsappCell=new PdfPCell(whatsappPara);
			whatsappCell.setBorder(0);
			whatsappCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase emailPhrase=new Phrase(email,font10);
			Paragraph emailPara=new Paragraph();
			emailPara.setIndentationLeft(10f);
			emailPara.add(new Chunk(emailImg, 0, 0, true));				
//			emailPara.add(emailPhrase);
			emailPara.setAlignment(Element.ALIGN_TOP);
			
			PdfPCell emailCell=new PdfPCell(emailPara);
			emailCell.setBorder(0);
			emailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase websitePhrase=new Phrase(website,font10);
			Paragraph websitePara=new Paragraph();
			websitePara.setIndentationLeft(20f);
			websitePara.add(new Chunk(websiteImg, 0, 0, true));				
//			websitePara.add(websitePhrase);
			websitePara.setAlignment(Element.ALIGN_JUSTIFIED);
			
			PdfPCell websiteCell=new PdfPCell(websitePara);
			websiteCell.setBorder(0);
			websiteCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      

			
			contactTable.addCell(tollCell);
			PdfPCell tollvalCell =pdfUtility.getPdfCell(tollno, font13, Element.ALIGN_LEFT, Element.ALIGN_TOP, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 2, 0);
			contactTable.addCell(tollvalCell);
	      
			contactTable.addCell(emailCell);
			PdfPCell emailvalCell =pdfUtility.getPdfCell(email, font13, Element.ALIGN_LEFT, Element.ALIGN_TOP, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 5, 0);
			contactTable.addCell(emailvalCell);
			
			contactTable.addCell(websiteCell);
			PdfPCell websitevalCell =pdfUtility.getPdfCell(website, font13, Element.ALIGN_LEFT, Element.ALIGN_TOP, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 5, 0);
			contactTable.addCell(websitevalCell);
			
			contactTable.addCell(whatsappCell);
			PdfPCell whatsappvalCell =pdfUtility.getPdfCell(contactno, font13, Element.ALIGN_LEFT, Element.ALIGN_TOP, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 5, 0);
			contactTable.addCell(whatsappvalCell);
			
			PdfPCell contactTableCell=new PdfPCell(contactTable);
			contactTableCell.setBorder(0);
			contactTableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			footerTable.addCell(contactTableCell);
			
//	      PdfPCell line3Cell = pdfUtility.getPdfCell(line3, font11bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0, -1, 0, 0, 0, 0, 0, 3, 0, 0);//paddingBottom changed from 10 to 3
//	      footerTable.addCell(line3Cell);
	      
	      PdfPCell footertableCell=new PdfPCell(footerTable);
//	      footertableCell.setBorder(0);
	      footertableCell.setBorderWidthTop(0);
	      PdfPTable footerCellTable = new PdfPTable(1);
	      footerCellTable.setWidthPercentage(100.0F);
	      footerCellTable.addCell(footertableCell);
	      
	      
	      
	      try {
			document.add(footerCellTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  
//	    String s1 = "SUBJECT TO KOLKATA JURISDICTION ";
//	    String s2 = "This is Computer Generated Invoice";
//	    Phrase s1ph = new Phrase(s1, font11bold);
//	    Phrase s2ph = new Phrase(s2, font11bold);
//	    Paragraph para1 = new Paragraph();
//	    para1.setAlignment(1);
//	    para1.add(s1ph);
//	    para1.add(Chunk.NEWLINE);
//	    
//	    para1.add(s2ph);
//	    try
//	    {
//	      document.add(para1);
//	    }
//	    catch (DocumentException e)
//	    {
//	      e.printStackTrace();
//	    }
	  }
	
	  private String getGSTINOfCustomer(Customer customer, String customerBranch) {
			
			String gstTinStr = "";
			if (customerBranch != null && !customerBranch.equals("")) {
				CustomerBranchDetails branch = ofy().load()
						.type(CustomerBranchDetails.class)
						.filter("companyId", customer.getCompanyId())
						.filter("buisnessUnitName", customerBranch.trim())
						.first().now();
				if (branch != null) {
					if (branch.getGSTINNumber() != null) {
						gstTinStr = branch.getGSTINNumber();
					}
				}
				if (gstTinStr.equals("")) {
					for (int i = 0; i < customer.getArticleTypeDetails().size(); i++) {
						if (customer.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
							gstTinStr = customer.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
							break;
						}
					}
				}
				return gstTinStr;
			} else {
				for (int i = 0; i < customer.getArticleTypeDetails().size(); i++) {
					if (customer.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstTinStr = customer.getArticleTypeDetails().get(i)
								.getArticleTypeValue().trim();
						break;
					}
				}
				return gstTinStr;
			}
			
			
		}
	  
	//Ashwini Patil Date:27-06-2023
	public String getUINOfCustomer(Customer customer) {
				String uinStr = "";
				if(customer!=null&&customer.getArticleTypeDetails()!=null) {
					for (int i = 0; i < customer.getArticleTypeDetails().size(); i++) {
						
							System.out.println("Inside else");
						if (customer.getArticleTypeDetails().get(i)
								.getArticleTypeName().equalsIgnoreCase("UIN")) {
							System.out.println("inside second else");
							uinStr = customer.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
							break;
						}
					}
					return uinStr;
				}
				return uinStr;
	}


	  private void createInvoiceDetailsNew(String preprintStatus)
	  {		
		  logger.log(Level.SEVERE,"In createInvoiceDetailsNew");
	    PdfPTable mainTable = new PdfPTable(2);
	    mainTable.setWidthPercentage(100.0F);
	    try {
	      mainTable.setWidths(columnHalfWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    

	    String titlepdf = "";
	    
	    if (("Proforma Invoice".equals(invoiceentity.getInvoiceType().trim())) || 
	    
	      (invoiceentity.getInvoiceType().trim().equals("Proforma Invoice"))) {
	      titlepdf = "Proforma Invoice";
	    } else {
	      titlepdf = "Tax Invoice";
	    }
	    Phrase titlephrase = new Phrase(titlepdf, font14bold);
   
	    PdfPCell titlecell = new PdfPCell(titlephrase);
	    
	    titlecell.setHorizontalAlignment(1);
	    titlecell.setVerticalAlignment(5);
	    titlecell.setBorder(0);
    
	    PdfPTable titlepdftable = new PdfPTable(1);
	    titlepdftable.setWidthPercentage(100.0F);
	    titlepdftable.setHorizontalAlignment(1);
	    //17-07-2024 
//	    titlepdftable.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);				
	    titlepdftable.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);						   
	    titlepdftable.addCell(titlecell);
	    if(invoiceentity.getIRN()!=null&&!invoiceentity.getIRN().equals("")&&cust!=null&&cust.geteInvoiceSupplyType()!=null&&(cust.geteInvoiceSupplyType().equals("SEZWP")||cust.geteInvoiceSupplyType().equals("SEZWOP")))
	    	titlepdftable.addCell(pdfUtility.getCell("(SUPPLY MEANT FOR EXPORT/SUPPLY TO SEZ UNIT OR SEZ DEVELOPER FOR AUTHORISED OPERATIONS UNDER BOND OR LETTER OF UNDERTAKING WITHOUT PAYMENT OF IGST)", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);						   
		   
	    	   
	    PdfPTable parent = new PdfPTable(1);
	    parent.setWidthPercentage(100.0F);
	    parent.setSpacingBefore(10.0F);
	    
	    PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	    titlePdfCell.setBorder(0);
	    parent.addCell(titlePdfCell);

	    try
	    {
	      document.add(parent);
	    }
	    catch (DocumentException e)
	    {
	      e.printStackTrace();
	    }
	    
	    CustomerBranchDetails custbranch=null;
	    if (invoiceentity.getCustomerBranch() != null && !invoiceentity.getCustomerBranch().equals("")) {
	  	    	custbranch =  (CustomerBranchDetails)ObjectifyService.ofy().load().type(CustomerBranchDetails.class).filter("companyId", comp.getCompanyId()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).filter("cinfo.count", Integer.valueOf(invoiceentity.getPersonInfo().getCount())).first().now();
	    }
	    createIrnTable(preprintStatus);	    
	    createCompanyAndInvoiceInfoTable(preprintStatus,custbranch);	    
	    
	    Phrase colon = new Phrase(":", font10bold);
	    PdfPCell colonCell = new PdfPCell(colon);
	    colonCell.setBorder(0);
	    colonCell.setHorizontalAlignment(0);

	    PdfPTable mainHeaderTable = new PdfPTable(2);
	    mainHeaderTable.setWidthPercentage(100.0F);
	    float[] columnWidth2 = { 6.0F, 4.0F };
	    try {
	      mainHeaderTable.setWidths(columnWidth2);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }

	    
	    
	   
	    /**
	     * ends here
	     */
	    
//	    mainHeaderTable.addCell(imageSignCell);	    
//	    mainHeaderTable.addCell(pdfUtility.getPdfCell("", font11bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	    

	    String gstin = "";String gstinText = "";
	    
	    if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
	      logger.log(Level.SEVERE, "GST Applicable");
	      gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
	        .getBranch().trim());
	      System.out.println("gstin" + gstin);
	    }
	    else {
	      logger.log(Level.SEVERE, "GST Not Applicable");
	      gstinText = comp.getCompanyGSTTypeText().trim();
	      System.out.println("gstinText" + gstinText);
	    }
	    
	    String articalType2 = null;
	    if (!gstin.trim().equals("")) {
	      logger.log(Level.SEVERE, "GST Present");
	      articalType2 = new String("GSTIN : " + gstin);
	    } else if (!gstinText.trim().equalsIgnoreCase("")) {
	      logger.log(Level.SEVERE, "GST Not Present");
	      articalType2 = new String(gstinText);
	    } else {
	      logger.log(Level.SEVERE, "Nothing Present");
	      articalType2 = new String("");
	    }
	    

	    
	    Phrase billingAddress = new Phrase("Buyer (Bill to)", font8);//"Billing Address"
	    PdfPCell billAdressCell = new PdfPCell(billingAddress);
	    billAdressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	    billAdressCell.setBorderWidthBottom(0);
	    billAdressCell.setBorder(0);
	    
	    /** 13-04-2018 By vijay below code commented not required wrong design **/
//	    mainHeaderTable.addCell(billAdressCell);

	    PdfPTable part1Table = new PdfPTable(1);
	    part1Table.setWidthPercentage(100.0F);
	    
	    /** 13-04-2018 By vijay adding cell into table **/
	    part1Table.addCell(billAdressCell);
	    
	    Phrase colon1 = new Phrase(":", font8bold);
	    PdfPCell colonCell1 = new PdfPCell(colon1);
	    
	    colonCell1.setBorder(0);
	    colonCell1.setHorizontalAlignment(0);
	    
	    PdfPTable colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
//	    Phrase name = new Phrase("Name", font10bold);
//	    PdfPCell nameCell = new PdfPCell(name);
//	    
//	    nameCell.setBorder(0);
//	    nameCell.setHorizontalAlignment(0);
	    

	    String tosir = null;
	    String custName = "";
	    

	    if ((cust.getCustPrintableName() != null) && 
	      (!cust.getCustPrintableName().equals(""))) {
	      custName = cust.getCustPrintableName().trim().toUpperCase();

	    }
	    else if ((cust.isCompany()) && (cust.getCompanyName() != null)) {
	      custName = "M/S  " + cust.getCompanyName().trim().toUpperCase();
	    } else if ((cust.getSalutation() != null) && 
	      (!cust.getSalutation().equals(""))) {
	      custName = 
	        cust.getSalutation() + " " + cust.getFullname().trim().toUpperCase();
	    } else {
	      custName = cust.getFullname().trim().toUpperCase();
	    }
	    

	    String fullname = "";
	    if ((cust.getCustPrintableName() != null) && 
	      (!cust.getCustPrintableName().equals(""))) {
	      fullname = custName.toUpperCase();
	    } else {
	      fullname = custName.toUpperCase();
	    }
	    

	    Phrase nameCellVal = new Phrase(fullname, font10bold);
	    PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
	    nameCellValCell.setColspan(3);
	    nameCellValCell.setBorder(0);
	    nameCellValCell.setHorizontalAlignment(0);
	    


//	    Phrase email1 = new Phrase("Email", font10bold);
//	    PdfPCell emailCell = new PdfPCell(email1);
//	    emailCell.setBorder(0);
//	    emailCell.setHorizontalAlignment(0);
//	    
//
//
//	    Phrase emailVal = null;
//	    if (cust.getEmail() != null) {
//	      emailVal = new Phrase(cust.getEmail(), font10);
//	    } else {
//	      emailVal = new Phrase("", font10);
//	    }
//	    PdfPCell emailValCell = new PdfPCell(emailVal);
//	    
//	    emailValCell.setBorder(0);
//	    emailValCell.setColspan(4);
//	    emailValCell.setHorizontalAlignment(0);
//	    
//	    Phrase mobNo = new Phrase("Phone", font10bold);
//	    PdfPCell mobNoCell = new PdfPCell(mobNo);
//	    mobNoCell.setBorder(0);
//	    mobNoCell.setHorizontalAlignment(0);
//	    
//
//	    String phone = "";
//	    String landline = "";
//	    
//	    if ((cust.getCellNumber1() != null) && (cust.getCellNumber1().longValue() != 0L)) {
//	      System.out.println("pn11");
//	      phone = cust.getCellNumber1()+"";
//	    }
//	    if ((cust.getCellNumber2() != null) && (cust.getCellNumber2().longValue() != 0L))
//	    {
//	      if (!phone.trim().isEmpty())
//	      {
//	        phone = phone + " / " + cust.getCellNumber2();
//	      }
//	      else {
//	        phone = cust.getCellNumber2()+"";
//	      }
//	      System.out.println("pn33" + phone);
//	    }
//	    if ((cust.getLandline().longValue() != 0L) && (cust.getLandline() != null))
//	    {
//	      if (!phone.trim().isEmpty()) {
//	        phone = phone + " / " + cust.getLandline();
//	      }
//	      else {
//	        phone = cust.getLandline()+"";
//	      }
//	      System.out.println("pn44" + phone);
//	    }
//	    
//	    Phrase mobVal = new Phrase(phone, font10);
//	    PdfPCell mobValCell = new PdfPCell(mobVal);
//	    
//	    mobValCell.setBorder(0);
//	    mobValCell.setColspan(4);
//	    mobValCell.setHorizontalAlignment(0);

//	    Phrase address = new Phrase("Address", font10bold);
//	    
//
//	    PdfPCell addressCell = new PdfPCell(address);
//	    
//	    addressCell.setBorder(0);
//	    addressCell.setHorizontalAlignment(0);
	    

	    String adrsValString = "";
	    if ((con.getNewcustomerAddress() != null) && 
	      (con.getNewcustomerAddress().getAddrLine1() != null) && 
	      (!con.getNewcustomerAddress().getAddrLine1().equals("")) && 
	      (con.getNewcustomerAddress().getCountry() != null) && 
	      (!con.getNewcustomerAddress().getCountry().equals("")))
	    {

	      adrsValString = con.getNewcustomerAddress().getCompleteAddress().trim().toUpperCase();;
	    } else {
	      adrsValString = cust.getAdress().getCompleteAddress().trim().toUpperCase();;
	    }
	    
	    Phrase addressVal = new Phrase(adrsValString, font8);
	    
	    Paragraph addressPara=new Paragraph();
	    addressPara.add(new Phrase(cust.getAdress().getAddrLine1(),font8));
	    if(cust.getAdress().getAddrLine2()!=null&&!cust.getAdress().getAddrLine2().equals(""))
	    	addressPara.add(new Phrase("\n"+cust.getAdress().getAddrLine2(),font8));
	    if(cust.getAdress().getCity()!=null&&!cust.getAdress().getCity().equals(""))
		    addressPara.add(new Phrase("\n"+cust.getAdress().getCity(),font8));
	    if(cust.getAdress().getPin()!=0)
		    addressPara.add(new Phrase(", "+cust.getAdress().getPin(),font8));
	    addressPara.setFont(font8);

//	    PdfPCell addressValCell = new PdfPCell(addressVal);
	    PdfPCell addressValCell = new PdfPCell(addressPara);
	    addressValCell.setColspan(3);
	    addressValCell.setBorder(0);
	    addressValCell.setHorizontalAlignment(0);
	    
	    String gstTinStr = "";
	    if ((invoiceentity.getGstinNumber() != null) && 
	      (!invoiceentity.getGstinNumber().equals(""))) {
	      gstTinStr = invoiceentity.getGstinNumber().trim();
	    }
	    else {
	     
	      gstTinStr = getGSTINOfCustomer(cust, 
	        invoiceentity.getCustomerBranch());
	    }
	    
	    if(gstTinStr.equals("")) {
	    	gstTinStr=getUINOfCustomer(cust);
	    }

	    Phrase gstTin = new Phrase("GSTIN/UIN", font8);
	    PdfPCell gstTinCell = new PdfPCell(gstTin);
	    
	    gstTinCell.setBorder(0);
	    gstTinCell.setHorizontalAlignment(0);
	    
	    Phrase gstTinVal = new Phrase(gstTinStr, font8);
	    PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
	    
	    gstTinValCell.setBorder(0);
	    gstTinValCell.setHorizontalAlignment(0);
	    

	    
	    String stCo = "";
	    for (int i = 0; i < stateList.size(); i++)
	    {
	      if (((State)stateList.get(i)).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim())) {
	        stCo = ((State)stateList.get(i)).getStateCode().trim();
	        break;
	      }
	    }
	    
	    Phrase stateCodeVal = new Phrase(stCo, font8);
	    

	    PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
	    stateCodeValCell.setBorder(0);
	    stateCodeValCell.setHorizontalAlignment(0);
	    
//	    Phrase attn = new Phrase("Attn", font10bold);
//	    PdfPCell attnCell = new PdfPCell(attn);
//	    
//	    attnCell.setBorder(0);
//	    attnCell.setHorizontalAlignment(0);
//	    
//	    Phrase attnVal = new Phrase(invoiceentity.getPersonInfo().getPocName(), 
//	      font10);
//	    PdfPCell attnValCell = new PdfPCell(attnVal);
//	    
//	    attnValCell.setBorder(0);
//	    attnValCell.setHorizontalAlignment(0);
	    
//	    colonTable.addCell(nameCell);
//	    colonTable.addCell(colonCell);
	    colonTable.addCell(nameCellValCell);
//	    if (printAttnInPdf) {
//	      colonTable.addCell(attnCell);
//	      colonTable.addCell(colonCell);
//	      colonTable.addCell(attnValCell);
//	    }
//	    colonTable.addCell(addressCell);
//	    colonTable.addCell(colonCell);
	    colonTable.addCell(addressValCell);
	    

//	    PdfPTable colonTable2 = new PdfPTable(6);
//	    colonTable2.setWidthPercentage(100.0F);
//	    try {
//	      colonTable2.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 0.2F, 
//	        2.0F });
//	    }
//	    catch (DocumentException e) {
//	      e.printStackTrace();
//	    }
	    
	    PdfPTable email_MobNumber = new PdfPTable(6);
	    

	    email_MobNumber.setWidthPercentage(100.0F);
	    try {
	      email_MobNumber.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 
	        0.2F, 2.0F });
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    Phrase colon2 = new Phrase(":", font8bold);
	    PdfPCell colon2Cell = new PdfPCell(colon2);
	    colon2Cell.setBorder(0);
	    colon2Cell.setHorizontalAlignment(0);
	    

	    /**
	     * nidhi
	     * 27-04-2018
	     * for if email details not available
	     */
	    logger.log(Level.SEVERE," get email addresss  lenth -- " + cust.getEmail().trim().length());
//	    if(cust.getEmail() != null && cust.getEmail().trim().length() >0){
//
//		    email_MobNumber.addCell(emailCell);
//		    email_MobNumber.addCell(colon2Cell);
//		    email_MobNumber.addCell(emailValCell);
//	    }
//	    
//	    email_MobNumber.addCell(mobNoCell);
//	    email_MobNumber.addCell(colon2Cell);
//	    email_MobNumber.addCell(mobValCell);
	    




	    colonTable.addCell(gstTinCell);
	    colonTable.addCell(colon2Cell);
	    colonTable.addCell(gstTinValCell);
//	    blankCell.setBorder(0);
//	    colonTable2.addCell(blankCell);
//	    colonTable2.addCell(blankCell);
//	    colonTable2.addCell(blankCell);
	    


	    Phrase stateNme = new Phrase("State Name", font8);
	    PdfPCell stateNmecell = new PdfPCell(stateNme);
	    stateNmecell.setBorder(0);
	    stateNmecell.setHorizontalAlignment(0);
	    

	    Phrase stateval = new Phrase(cust.getAdress().getState().trim()+", Code : "+stCo, font8);
	    PdfPCell statevalcell = new PdfPCell(stateval);
	    statevalcell.setBorder(0);
	    statevalcell.setHorizontalAlignment(0);
	    
	    colonTable.addCell(stateNmecell);
	    colonTable.addCell(colon2Cell);
	    colonTable.addCell(statevalcell);
	    
//	    colonTable2.addCell(stateCodeCell);
//	    colonTable2.addCell(colon2Cell);
//	    colonTable2.addCell(stateCodeValCell);
	    
	    PdfPCell cell1 = new PdfPCell(colonTable);
	    cell1.setBorder(0);
	    
//	    PdfPCell email_Cell = new PdfPCell(email_MobNumber);
//	    email_Cell.setBorder(0);
	    
//	    PdfPCell cell12 = new PdfPCell(colonTable2);
//	    cell12.setBorder(0);
	    

	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    colonTable.addCell(colonCell);
	    
	    PdfPTable pdfStateTable = new PdfPTable(4);
	    pdfStateTable.setWidthPercentage(100.0F);
	    try {
	      pdfStateTable.setWidths(columnStateCodeCollonWidth);
	    }
	    catch (DocumentException e1) {
	      e1.printStackTrace();
	    }
	    
	    PdfPCell state4Cell = new PdfPCell(pdfStateTable);
	    state4Cell.setBorder(0);
	    state4Cell.setHorizontalAlignment(0);
	    
	    colonTable.addCell(state4Cell);
	    
//	    PdfPCell stateCell_stateCode = new PdfPCell(colonTable);
//	    stateCell_stateCode.setBorder(0);
	    
	    part1Table.addCell(cell1);
	    

//	    part1Table.addCell(email_Cell);
//	    part1Table.addCell(cell12);
	    
//	    part1Table.addCell(pdfUtility.getPdfCell(" ", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)); //commented on 20-12-2023 as qr code size increased
	    
	    PdfPCell part1TableCell = new PdfPCell(part1Table);
//	    part1TableCell.setBorder(0);
	    part1TableCell.setBorderWidthBottom(0);
	    part1TableCell.setBorderWidthRight(0);
	    mainHeaderTable.addCell(part1TableCell);
	    

	    PdfPTable part2Table = new PdfPTable(1);
	    part2Table.setWidthPercentage(100.0F);
	    
	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    


	    Phrase serviceaddress = new Phrase("Consignee (Ship to)", font8);//"Service Address"
	    PdfPCell serviceCell = new PdfPCell(serviceaddress);
	    serviceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	    serviceCell.setBorderWidthBottom(0);
//	    serviceCell.setBorder(0);
	    serviceCell.setBorderWidthBottom(0);
	    part2Table.addCell(serviceCell);
	    

	    Phrase name2 = new Phrase("Name", font10bold);
	    PdfPCell name2Cell = new PdfPCell(name2);
	    
	    name2Cell.setBorder(0);
	    name2Cell.setHorizontalAlignment(0);
	    
	    Phrase name2CellVal = new Phrase(fullname, font10bold);
	    PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
	    
	    name2CellValCell.setBorder(0);
	    name2CellValCell.setHorizontalAlignment(0);
	    

//	    Phrase email2 = new Phrase("Email", font10bold);
//	    PdfPCell email1Cell = new PdfPCell(email2);
//	    email1Cell.setBorder(0);
//	    email1Cell.setHorizontalAlignment(0);
//	    
//
//
//	    Phrase email1Val = null;
//	    if (cust.getEmail() != null) {
//	      email1Val = new Phrase(cust.getEmail(), font10);
//	    } else {
//	      email1Val = new Phrase("", font10);
//	    }
//	    PdfPCell email1ValCell = new PdfPCell(email1Val);
//	    
//	    email1ValCell.setBorder(0);
//	    email1ValCell.setColspan(4);
//	    email1ValCell.setHorizontalAlignment(0);
//	    
//	    Phrase mobNo2 = new Phrase("Mobile", font10bold);
//	    PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
//	    mobNo2Cell.setBorder(0);
//	    mobNo2Cell.setHorizontalAlignment(0);
//	    
//
//	    String phone2 = "";
//	    
//
//	    if ((cust.getCellNumber1() != null) && (cust.getCellNumber1().longValue() != 0L)) {
//	      System.out.println("pn11");
//	      phone2 = cust.getCellNumber1()+"";
//	    }
//	    if ((cust.getCellNumber2() != null) && (cust.getCellNumber2().longValue() != 0L))
//	    {
//	      if (!phone2.trim().isEmpty())
//	      {
//	        phone2 = phone2 + " / " + cust.getCellNumber2();
//	      }
//	      else {
//	        phone2 = cust.getCellNumber2()+"";
//	      }
//	      System.out.println("pn33" + phone2);
//	    }
//	    if ((cust.getLandline().longValue() != 0L) && (cust.getLandline() != null))
//	    {
//	      if (!phone2.trim().isEmpty()) {
//	        phone2 = phone2 + " / " + cust.getLandline();
//	      }
//	      else {
//	        phone2 = cust.getLandline()+"";
//	      }
//	      System.out.println("pn44" + phone2);
//	    }
//	    
//
//	    Phrase mobNo2Val = new Phrase(phone2, font10);
//	    PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);
//	    
//	    mobNo2ValCell.setBorder(0);
//	    mobNo2ValCell.setHorizontalAlignment(0);
//	    
//	    Phrase landlineph2 = new Phrase("LandLine", font10bold);
//	    PdfPCell landlineCell2 = new PdfPCell(landlineph2);
//	    landlineCell2.setBorder(0);
//	    landlineCell2.setHorizontalAlignment(0);
//	    
//	    String landline2 = "";
//	    if (cust.getLandline().longValue() != 0L) {
//	      landline2 = cust.getLandline()+"";
//	    } else {
//	      landline2 = "";
//	    }
//	    
//	    Phrase landlineVal2 = new Phrase(landline2, font10);
//	    PdfPCell landlinevalCells2 = new PdfPCell(landlineVal2);
//	    
//	    landlinevalCells2.setBorder(0);
//	    landlinevalCells2.setHorizontalAlignment(0);
	    


//	    Phrase address2 = new Phrase("Address", font10bold);
//	    
//
//	    PdfPCell address2Cell = new PdfPCell(address2);
//	    
//	    address2Cell.setBorder(0);
//	    address2Cell.setHorizontalAlignment(0);
	    







	    String adrsValString1 = "";
	    String customerPoc = null;
	    
	    Address serviceAddress = null;
	    String serviceAddressLine1="";
	    String serviceAddressLine2="";
	    String serviceAddressCity="";
	    long serviceAddressPin=0;
	    
	    if ((invoiceentity.getCustomerBranch() != null) && 
	      (!invoiceentity.getCustomerBranch().equals(""))) {
	    	
	      if (custbranch != null) {
	        serviceAddress = custbranch.getAddress();
	        System.out.println("heeeeeeeelllllllooo" + serviceAddress);
	        
	        adrsValString1 = custbranch.getAddress().getCompleteAddress().toUpperCase();;
	        serviceAddressLine1=custbranch.getAddress().getAddrLine1();
		    serviceAddressLine2=custbranch.getAddress().getAddrLine2();
		    serviceAddressCity=custbranch.getAddress().getCity();
		    serviceAddressPin=custbranch.getAddress().getPin();
	      }
	      

	    }
	    else if (con.getCustomerServiceAddress() != null) {
	      serviceAddress = con.getCustomerServiceAddress();
	      adrsValString1 = con.getCustomerServiceAddress()
	        .getCompleteAddress().toUpperCase();
	      serviceAddressLine1=con.getCustomerServiceAddress().getAddrLine1();
		    serviceAddressLine2=con.getCustomerServiceAddress().getAddrLine2();
		    serviceAddressCity=con.getCustomerServiceAddress().getCity();
		    serviceAddressPin=con.getCustomerServiceAddress().getPin();
	    }
	    else {
	      serviceAddress = cust.getSecondaryAdress();
	      adrsValString1 = cust.getSecondaryAdress().getCompleteAddress().toUpperCase();
	      serviceAddressLine1=cust.getSecondaryAdress().getAddrLine1();
		    serviceAddressLine2=cust.getSecondaryAdress().getAddrLine2();
		    serviceAddressCity=cust.getSecondaryAdress().getCity();
		    serviceAddressPin=cust.getSecondaryAdress().getPin();
	    }
	    


	    if ((!cust.getSecondaryAdress().getAddrLine1().equals("")) && 
	      (customerbranchlist.size() == 0)) {
	      customerPoc = invoiceentity.getPersonInfo().getPocName();
	    }
	    
	    System.out.println("Inside Customer branch  " + 
	      customerbranchlist.size());
	    for (int i = 0; i < customerbranchlist.size(); i++)
	    {
	      customerPoc = ((CustomerBranchDetails)customerbranchlist.get(i)).getPocName();
	    }
	    
	    String name2str="";
	    
	    //27-02-2024 orion does not want customer branch name to get printed in service address
//	    if(custbranch!=null){
//			if(custbranch.getServiceAddressName()!=null&&!custbranch.getServiceAddressName().equals(""))
//				name2str=custbranch.getServiceAddressName().toUpperCase();
//			else
//				name2str=custbranch.getBusinessUnitName().toUpperCase();
//		}else 
			
			if(cust.getServiceAddressName()!=null&&!cust.getServiceAddressName().equals("")) {
			name2str=cust.getServiceAddressName().toUpperCase();
	    }else
	    	name2str=fullname;
	    
	    Phrase nameCell2Val = new Phrase(name2str, font10bold);
	    PdfPCell nameCell2ValCell = new PdfPCell(nameCell2Val);
	    nameCell2ValCell.setColspan(3);
	    nameCell2ValCell.setBorder(0);
	    nameCell2ValCell.setHorizontalAlignment(0);

	    Phrase address2Val = new Phrase(adrsValString1, font8);
	    
	    Paragraph address2Para=new Paragraph();
	    serviceAddressLine1=cust.getSecondaryAdress().getAddrLine1();
	    serviceAddressLine2=cust.getSecondaryAdress().getAddrLine2();
	    serviceAddressCity=cust.getSecondaryAdress().getCity();
	    serviceAddressPin=cust.getSecondaryAdress().getPin();
	    address2Para.add(new Phrase(serviceAddressLine1,font8));
	    if(serviceAddressLine2!=null&&!serviceAddressLine2.equals(""))
	    	address2Para.add(new Phrase("\n"+serviceAddressLine2,font8));
	    if(serviceAddressCity!=null&&!serviceAddressCity.equals(""))
	    	address2Para.add(new Phrase("\n"+serviceAddressCity,font8));
	    if(serviceAddressPin!=0)
	    	address2Para.add(new Phrase(", "+serviceAddressPin,font8));
	    

//	    PdfPCell address2ValCell = new PdfPCell(address2Val);
	    PdfPCell address2ValCell = new PdfPCell(address2Para);
	    address2ValCell.setColspan(3);
	    address2ValCell.setBorder(0);
	    address2ValCell.setHorizontalAlignment(0);
	    
	    String gstTin2Str = "";
	    if ((invoiceentity.getGstinNumber() != null) && 
	      (!invoiceentity.getGstinNumber().equalsIgnoreCase(""))) {
	      gstTin2Str = invoiceentity.getGstinNumber().trim();
	    } else {
	      
	      gstTin2Str = getGSTINOfCustomer(cust, 
	        invoiceentity.getCustomerBranch());
	    }
	    if(gstTin2Str.equals(""))
	    	gstTin2Str=getUINOfCustomer(cust);

	    Phrase gstTin2 = new Phrase("GSTIN/UIN", font8);
	    PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
	    gstTin2Cell.setBorder(0);
	    gstTin2Cell.setHorizontalAlignment(0);
	    
	    Phrase gstTin2Val = new Phrase(gstTin2Str, font8);
	    PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
	    gstTin2ValCell.setBorder(0);
	    gstTin2ValCell.setHorizontalAlignment(0);
	    
//	    Phrase attn2 = new Phrase("Attn", font10bold);
//	    PdfPCell attn2Cell = new PdfPCell(attn2);
//	    attn2Cell.setBorder(0);
//	    attn2Cell.setHorizontalAlignment(0);
//	    
//	    Phrase attn2Val = null;
//	    if (custName != null) {
//	      attn2Val = new Phrase(customerPoc, font10);
//	    } else {
//	      attn2Val = new Phrase("", font10);
//	    }
//	    PdfPCell attn2ValCell = new PdfPCell(attn2Val);
//	    attn2ValCell.setBorder(0);
//	    attn2ValCell.setHorizontalAlignment(0);
	    
	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    try {
	      colonTable.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
//	    colonTable.addCell(nameCell);
//	    colonTable.addCell(colonCell);
	    colonTable.addCell(nameCell2ValCell);
//	    if (printAttnInPdf) {
//	      colonTable.addCell(attn2Cell);
//	      colonTable.addCell(colonCell);
//	      colonTable.addCell(attn2ValCell);
//	    }
//	    colonTable.addCell(address2Cell);
//	    colonTable.addCell(colonCell);
	    colonTable.addCell(address2ValCell);
	    
	    PdfPTable colonTable22 = new PdfPTable(3);
	    colonTable22.setWidthPercentage(100.0F);
	    try {
	      colonTable22.setWidths(columnCollonWidth);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    PdfPTable email_MobNumber2 = new PdfPTable(6);
	    email_MobNumber2.setWidthPercentage(100.0F);
	    try {
	      email_MobNumber2.setWidths(new float[] { 1.9F, 0.2F, 3.7F, 2.0F, 
	        0.2F, 2.0F });
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
	    /**
	     * nidhi
	     */
//	    if(cust.getEmail() != null && cust.getEmail().trim().length() >0 ){
//
//		    email_MobNumber2.addCell(emailCell);
//		    email_MobNumber2.addCell(colonCell);
//		    email_MobNumber2.addCell(emailValCell);
//	    }
//	    
//	    email_MobNumber2.addCell(mobNoCell);
//	    email_MobNumber2.addCell(colonCell);
//	    email_MobNumber2.addCell(mobValCell);
	    

	    PdfPCell cell2 = new PdfPCell(colonTable);
	    cell2.setBorder(0);
	    
	    PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
	    email_MobNumber2Cell.setBorder(0);
	    
	    PdfPCell cell22 = new PdfPCell(colonTable22);
	    cell22.setBorder(0);
	    
	    Phrase state2 = new Phrase("State Name", font8);
	    

	    PdfPCell state2Cell = new PdfPCell(state2);
	    state2Cell.setBorder(0);
	    state2Cell.setHorizontalAlignment(0);
	    
	    
	    
	    

	    String servicestateName = "";	   
	    if(custbranch!=null&&custbranch.getAddress()!=null) {
	    	servicestateName = custbranch.getAddress().getState().trim();	      
	    }else if(cust!=null&&cust.getSecondaryAdress()!=null) {
	    	servicestateName = cust.getSecondaryAdress().getState().trim();
	    }
	    	    
	    String st2Co = "";
	    int j; 
	    for (int i = 0; i < stateList.size(); i++)
	    {
	      if ((stateList.get(i)).getStateName().trim().equalsIgnoreCase(servicestateName.trim())) {
	        st2Co = (stateList.get(i)).getStateCode().trim();
	        break;
	      }
	    }
	    
	    Phrase state2Val = new Phrase(servicestateName+", Code : "+st2Co, font8);

	    PdfPCell state2ValCell = new PdfPCell(state2Val);	    
	    state2ValCell.setBorder(0);
	    state2ValCell.setHorizontalAlignment(0);
	    
	    colonTable = new PdfPTable(3);
	    colonTable.setWidthPercentage(100.0F);
	    float[] column3 = { 2.2F, 0.2F, 7.1F };
	    try {
	      colonTable.setWidths(column3);
	      
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	    
 
	    colonTable.addCell(gstTin2Cell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(gstTin2ValCell);

	    	    
	    
	    colonTable.addCell(state2Cell);
	    colonTable.addCell(colonCell);
	    colonTable.addCell(state2ValCell);
	    
	  

	    PdfPCell stateCell_stateCode2 = new PdfPCell(colonTable);
	    stateCell_stateCode2.setBorder(0);
	    
	    
	    
	    part2Table.addCell(cell2);
//	    
//	    part2Table.addCell(email_MobNumber2Cell);
	    
	    
	    part2Table.addCell(stateCell_stateCode2);
	    part2Table.addCell(cell22);
//	    part2Table.addCell(pdfUtility.getPdfCell(" ", font13, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));//commented on 20-12-2023 as qr code size increased
	    
	    
	    PdfPCell part2TableCell = new PdfPCell(part2Table);

	    part2TableCell.setBorderWidthBottom(0);
	    part2TableCell.setBorderWidthTop(0);

	    Phrase blankph = new Phrase("", font8);
	    PdfPCell blnkCell = new PdfPCell(blankph);
	    blnkCell.setBorderWidthBottom(0.0F);
	    blnkCell.setBorderWidthLeft(0.0F);
	    blnkCell.setBorderWidthTop(0.0F);
	    
//	    mainHeaderTable.addCell(part2TableCell);
	    
	    /**
	     * @author Anil ,Date : 30-03-2019
	     * checked service address flag
	     * raised by rahul tiwari for orion
	     */
//	    if(serviceAddressFlag){
//			if (!con.isServiceWiseBilling()) {
				if (custbranchlist.size() == 0) {
					System.out.println("No ACtion");
					/**
					 * Date 14-04-2018 By vijay for if there is no customer branches
					 * then also print address
					 **/
					mainHeaderTable.addCell(part2TableCell);
					mainHeaderTable.addCell(blankCell);	
				} else {
					mainHeaderTable.addCell(part2TableCell);
					mainHeaderTable.addCell(blankCell);
//				}
			}
//	    }
	    
	    
	    try
	    {
	      
	      document.add(mainTable);
	      document.add(mainHeaderTable);
	    }
	    catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }
	  private void createIrnTable(String preprintStatus) {
		  PdfPTable parent = new PdfPTable(1);
		  parent.setWidthPercentage(100.0F);
		  parent.setSpacingBefore(2.0F);
		  
		  if(invoiceentity.getIRN()!=null&&!invoiceentity.getIRN().equals("")){
			    PdfPTable IRNtable = new PdfPTable(3);
				IRNtable.setWidthPercentage(100f);
				float[] tblcol2width = {1.8f,6f,2f};
				float[] tblcol3width = {0.9f,0.1f,7f};
				try {
					IRNtable.setWidths(tblcol2width);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				
				DocumentUpload logodocument=null;
			    logger.log(Level.SEVERE, "Branch as Company Flag "+branchAsCompany);
				if (branchAsCompany) {				
					
					logodocument = serverApp.getLogoOfCompany(comp, invoiceentity.getBranch()
							.trim());
					logger.log(Level.SEVERE, "Inside Branch As Company IFFF"+branchAsCompany);
				} else {
					if (comp.getLogo() != null) {
						logodocument = comp.getLogo();
					} else {
						logodocument = null;
					}
					logger.log(Level.SEVERE, "outside Branch As Company ELSE"+branchAsCompany);
				}


			    String environment = 
			      System.getProperty("com.google.appengine.runtime.environment");
			    String hostUrl1; 
			    if (environment.equals("Production")) {
			      String applicationId = 
			        System.getProperty("com.google.appengine.application.id");
			      String version = 
			        System.getProperty("com.google.appengine.application.version");
			      hostUrl1 = "http://" + version + "." + applicationId + 
			        ".appspot.com/";
			    } else {
			      hostUrl1 = "http://localhost:8888";
			      System.out.println("local");
			    }
			    PdfPCell imageSignCell = null;
			    
			    
			    Image image2 = null;
			    try {
			      image2 = 
			        Image.getInstance(new URL(hostUrl1 + logodocument.getUrl()));
//			      image2.scalePercent(20.0F);
			      image2.scaleAbsolute(90, 90);    
			      imageSignCell = new PdfPCell(image2);
			      
//			      imageSignCell.setFixedHeight(40F);
			      imageSignCell.setBorder(0);
//			      imageSignCell.setBorderWidthBottom(0);
//			      imageSignCell.setBorderWidthLeft(1);
//			      imageSignCell.setBorderWidthTop(1);
//			      imageSignCell.setBorderWidthRight(0);
			      imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      imageSignCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			      imageSignCell.setPaddingBottom(2);
			      imageSignCell.setPaddingTop(2);
			      imageSignCell.setPaddingRight(2);
			      imageSignCell.setPaddingLeft(2);
			      
//		     	  imageSignCell.setRowspan(4);
			      
			    } catch (Exception e) {
			      e.printStackTrace();
			    }
			    

			    logger.log(Level.SEVERE, "priprint status " + preprintStatus);
				if (preprintStatus.contains("yes")) {
					Phrase logoblank = new Phrase(" ");
					PdfPCell logoblankcell = new PdfPCell(logoblank);

//					logoblankcell.setFixedHeight(20F);
					logoblankcell.setHorizontalAlignment(1);
					logoblankcell.setVerticalAlignment(5);
//					logoblankcell.setRowspan(3);
					IRNtable.addCell(logoblankcell);
					logger.log(Level.SEVERE, "inside priprint status yesss " + preprintStatus);
				} 
				else if (preprintStatus.contains("no")) {
					logger.log(Level.SEVERE, "inside priprint status no " + preprintStatus);
					if (imageSignCell != null) {
						IRNtable.addCell(imageSignCell);
						logger.log(Level.SEVERE, "With Logo " + branchAsCompany);
					} else {
						Phrase logoblank = new Phrase(" ");
						PdfPCell logoblankcell = new PdfPCell(logoblank);

//						logoblankcell.setFixedHeight(20F);
						logoblankcell.setHorizontalAlignment(1);
						logoblankcell.setVerticalAlignment(5);
//						logoblankcell.setRowspan(3);
						IRNtable.addCell(logoblankcell);
						logger.log(Level.SEVERE, "Withou logo " + branchAsCompany);
						
					}
				}
			    
				
				PdfPTable IRNLefttable = new PdfPTable(3);
				IRNLefttable.setWidthPercentage(100f);
				try {
					IRNLefttable.setWidths(tblcol3width);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				PdfPTable IRNRighttable = new PdfPTable(1);
				IRNRighttable.setWidthPercentage(100f);				
				
				
				String irn="",ackNo="",ackDate="";
				if(invoiceentity.getIRN()!=null)
					irn=invoiceentity.getIRN();
				if(invoiceentity.getIrnAckNo()!=null)
					ackNo=invoiceentity.getIrnAckNo();
				if(invoiceentity.getIrnAckDate()!=null&&!invoiceentity.getIrnAckDate().equals("")) {
					 String sDate=invoiceentity.getIrnAckDate().substring(0, 10);
					 String year=invoiceentity.getIrnAckDate().substring(0, 4);
					 String month=invoiceentity.getIrnAckDate().substring(5, 7);
					 String day=invoiceentity.getIrnAckDate().substring(8, 10);
					 ackDate=day+"-"+month+"-"+year;
				}
				
				PdfPCell qrCodeCell = null;
				if(invoiceentity.getIrnQrCode()!=null&&!invoiceentity.getIrnQrCode().equals("")) {
					BarcodeQRCode barcodeQRCode = new BarcodeQRCode(invoiceentity.getIrnQrCode(), 1000, 1000, null);
				     
					Image codeQrImage=null;
						try {
							codeQrImage = barcodeQRCode.getImage();
						} catch (BadElementException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
				        codeQrImage.scaleAbsolute(100, 100);  //50,50 old value. Ashwini Patil Date:20-12-2023  	Changed size as few qr code were not scannable in old size and clients were rejecting them		
					
					
					qrCodeCell = new PdfPCell(codeQrImage);
					qrCodeCell.setBorder(0);
					qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					qrCodeCell.setVerticalAlignment(Element.ALIGN_TOP);
//					qrCodeCell.setRowspan(2);
				
				}

				IRNLefttable.addCell(pdfUtility.getCell("IRN ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);		
				IRNLefttable.addCell(pdfUtility.getCell(":", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);		
				IRNLefttable.addCell(pdfUtility.getCell(""+irn, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				IRNRighttable.addCell(pdfUtility.getPdfCell("e-Invoice", font6bold, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
		    	
				
				IRNLefttable.addCell(pdfUtility.getCell("Ack No. ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				IRNLefttable.addCell(pdfUtility.getCell(":", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);				
				IRNLefttable.addCell(pdfUtility.getCell(""+ackNo, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				if(qrCodeCell!=null)
					IRNRighttable.addCell(qrCodeCell);
				else
					IRNRighttable.addCell(pdfUtility.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);				
				
				IRNLefttable.addCell(pdfUtility.getCell("Ack Date ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				IRNLefttable.addCell(pdfUtility.getCell(":", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);			
				IRNLefttable.addCell(pdfUtility.getCell(""+ackDate, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
//				IRNLefttable.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//				IRNLefttable.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);			
//				IRNLefttable.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				
				PdfPCell leftCell = new PdfPCell();
				leftCell.addElement(IRNLefttable);
				leftCell.setVerticalAlignment(5);
				leftCell.setBorder(0);
				IRNtable.addCell(leftCell);
				
				PdfPCell rightCell = new PdfPCell();
				rightCell.addElement(IRNRighttable);
				rightCell.setBorder(0);
				IRNtable.addCell(rightCell);
				
				PdfPCell irnCell = new PdfPCell();
				irnCell.addElement(IRNtable);
				irnCell.setBorderWidthBottom(0);
//				irnCell.setBorder(1);
//				irnCell.setBorderWidthLeft(1);
//				irnCell.setBorderWidthRight(1);
				irnCell.setVerticalAlignment(Element.ALIGN_CENTER);  
//				irnCell.setColspan(3);
			    
				parent.addCell(irnCell);
				
				try {
					document.add(parent);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   }
	  }
	  private void createCompanyAndInvoiceInfoTable(String preprintStatus,CustomerBranchDetails custbranch ) {
		 
		  boolean einvoiceflag=false;
		  if(invoiceentity.getIRN()!=null&&!invoiceentity.getIRN().equals(""))
			  einvoiceflag=true;
		  
		  PdfPTable companyAndInvoiceInfoTable = null;
		  if(!einvoiceflag) {
			 companyAndInvoiceInfoTable = new PdfPTable(3);
			 companyAndInvoiceInfoTable.setWidthPercentage(100.0F);
			 float[] columnWidth = { 1.8f, 4.2f, 4f };
			 try {
			    	companyAndInvoiceInfoTable.setWidths(columnWidth);
			    }
			    catch (DocumentException e) {
			      e.printStackTrace();
			    }
		  }else {
			companyAndInvoiceInfoTable =new PdfPTable(2);
		    companyAndInvoiceInfoTable.setWidthPercentage(100.0F);
		    float[] columnWidth = { 6f, 4f };
		    try {
		    	companyAndInvoiceInfoTable.setWidths(columnWidth);
		    }
		    catch (DocumentException e) {
		      e.printStackTrace();
		    }
		  }
		    
		    
		   if(!einvoiceflag) {
		    /**Date 4-3-2020 by Amol added a logo as per BranchAsCompany Flow raised by Rahul Tiwari **/
		    DocumentUpload logodocument=null;
		    logger.log(Level.SEVERE, "Branch as Company Flag "+branchAsCompany);
			if (branchAsCompany) {				
				
				logodocument = serverApp.getLogoOfCompany(comp, invoiceentity.getBranch()
						.trim());
				logger.log(Level.SEVERE, "Inside Branch As Company IFFF"+branchAsCompany);
			} else {
				if (comp.getLogo() != null) {
					logodocument = comp.getLogo();
				} else {
					logodocument = null;
				}
				logger.log(Level.SEVERE, "outside Branch As Company ELSE"+branchAsCompany);
			}


		    String environment = 
		      System.getProperty("com.google.appengine.runtime.environment");
		    String hostUrl1; 
		    if (environment.equals("Production")) {
		      String applicationId = 
		        System.getProperty("com.google.appengine.application.id");
		      String version = 
		        System.getProperty("com.google.appengine.application.version");
		      hostUrl1 = "http://" + version + "." + applicationId + 
		        ".appspot.com/";
		    } else {
		      hostUrl1 = "http://localhost:8888";
		      System.out.println("local");
		    }
		    PdfPCell imageSignCell = null;
		    
		    
		    Image image2 = null;
		    try {
		      image2 = 
		        Image.getInstance(new URL(hostUrl1 + logodocument.getUrl()));
//		      image2.scalePercent(20.0F);
		      image2.scaleAbsolute(80, 80);    
		      
		      imageSignCell = new PdfPCell(image2);
		      
//		      imageSignCell.setFixedHeight(40F);
//		      imageSignCell.setBorder(0);
		      imageSignCell.setBorderWidthBottom(0);
//		      imageSignCell.setBorderWidthLeft(1);
//		      imageSignCell.setBorderWidthTop(1);
		      imageSignCell.setBorderWidthRight(0);
		      imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      imageSignCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		      imageSignCell.setPaddingBottom(2);
		      imageSignCell.setPaddingTop(2);
		      imageSignCell.setPaddingRight(2);
		      imageSignCell.setPaddingLeft(2);
		      
//	     	  imageSignCell.setRowspan(4);
		      
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		    

		    logger.log(Level.SEVERE, "priprint status " + preprintStatus);
			if (preprintStatus.contains("yes")) {
				Phrase logoblank = new Phrase(" ");
				PdfPCell logoblankcell = new PdfPCell(logoblank);

//				logoblankcell.setFixedHeight(20F);
				logoblankcell.setHorizontalAlignment(1);
				logoblankcell.setVerticalAlignment(5);
//				logoblankcell.setRowspan(3);
				companyAndInvoiceInfoTable.addCell(logoblankcell);
				logger.log(Level.SEVERE, "inside priprint status yesss " + preprintStatus);
			} 
			else if (preprintStatus.contains("no")) {
				logger.log(Level.SEVERE, "inside priprint status no " + preprintStatus);
				if (imageSignCell != null) {
					companyAndInvoiceInfoTable.addCell(imageSignCell);
					logger.log(Level.SEVERE, "With Logo " + branchAsCompany);
				} else {
					Phrase logoblank = new Phrase(" ");
					PdfPCell logoblankcell = new PdfPCell(logoblank);

//					logoblankcell.setFixedHeight(20F);
					logoblankcell.setHorizontalAlignment(1);
					logoblankcell.setVerticalAlignment(5);
//					logoblankcell.setRowspan(3);
					companyAndInvoiceInfoTable.addCell(logoblankcell);
					logger.log(Level.SEVERE, "Withou logo " + branchAsCompany);
					
				}
			}
	  }
		    
		    

		    Phrase companyName = null;
		    
		    if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
		    	companyName = new Phrase( branchDt.getCorrespondenceName(), font12bold);
				logger.log(Level.SEVERE,"correspondance name 111 "+companyName);
				
			}else{
				if (comp != null) {
				      companyName = new Phrase(comp.getBusinessUnitName().trim(), font12bold);
				      logger.log(Level.SEVERE,"company NAme "+companyName);
				    }	
			}
		    

		    String compAdress = "";
	
		    if(branch!=null){
		    	
		    	   
		    	for(Branch branch1 :branch){
		    		
		    		if(invoiceentity.getBranch().equalsIgnoreCase(branch1.getBusinessUnitName())){
		    			
		    			System.out.println("gstn no::"+branch1.getGSTINNumber());
		    			if(branch1.getGSTINNumber().equals(null) || branch1.getGSTINNumber().equals("")){
		    				compAdress=comp.getAddress().getCompleteAddress();
//			    			compAdress = comp.getAddress().getAddrLine1().trim() + "," + comp.getAddress().getAddrLine2().trim();
			    			logger.log(Level.SEVERE, "COMPANY Address");
		    		}else{
		    			compAdress = branch1.getAddress().getCompleteAddress().trim();
		    			System.out.println("Branch Address::"+compAdress);
		    			logger.log(Level.SEVERE, "BRANCH ADDRESS");
		    		}
		    		
		    	}
		    	}
		    
		    }else{
		    	compAdress=comp.getAddress().getCompleteAddress();
//		    	compAdress = comp.getAddress().getAddrLine1().trim() + "," + comp.getAddress().getAddrLine2().trim();
				System.out.println("company Address::"+compAdress);
			}
		    
		    
		    Phrase compAdressCell = new Phrase(compAdress,font8);
		    PdfPCell companyAddrCell = new PdfPCell(compAdressCell);
		   
//		    companyAddrCell.setBorder(0);
		    companyAddrCell.setHorizontalAlignment(0);
		    companyAddrCell.setBorderWidthTop(0);
		    companyAddrCell.setBorderWidthBottom(0);
		    companyAddrCell.setBorderWidthRight(0);
		    /*
		     * end by Ashwini
		     */
		    
		    PdfPCell companyNameCell = new PdfPCell(companyName);
		    
//		    companyNameCell.setBorder(0); 
//		    companyNameCell.setBorderWidthTop(1);
		    companyNameCell.setHorizontalAlignment(0);
		    companyNameCell.setBorderWidthBottom(0);
		    companyNameCell.setBorderWidthRight(0);
		    PdfPTable companynameTable = new PdfPTable(1);
		    companynameTable.setWidthPercentage(100.0F);
		    try
		    {
		      companynameTable.setWidths(columnHalfWidth);
		    }
		    catch (DocumentException e) {
//		      e.printStackTrace();
		    }
		    
		    companynameTable.addCell(companyNameCell);
		    companynameTable.addCell(companyAddrCell);
		    
		    //other company details like email state gstin statecode
		    Phrase companyGSTTIN = null;
		    String gstinValue = serverApp.getGSTINOfCompany(comp, invoiceentity.getBranch().trim());
		    if (gstinValue.equals("")) {
		    	gstinValue=serverApp.getUINOfCompany(comp, invoiceentity.getBranch().trim());
		    }

		    
		    if (!gstinValue.equals("")) {
		      companyGSTTIN = new Phrase(gstinValue, font8);
		    }
		    
		    Phrase gstph = new Phrase("GSTIN/UIN", font8);
		    PdfPCell gstcell = new PdfPCell(gstph);
		    gstcell.setHorizontalAlignment(0);
		    gstcell.setBorder(0);


		    PdfPCell gstvalcell = new PdfPCell(companyGSTTIN);
		    gstvalcell.setBorder(0);
		    gstvalcell.setHorizontalAlignment(0);
		    

		    Phrase statenameph = new Phrase("State Name", font10);
		    PdfPCell statenamecell = new PdfPCell(statenameph);
		    statenamecell.setBorder(0);

		    
	        /**
	         * @author Anil,Date : 03-01-2019 
	         * In this variable we will store ,state name and as per that state name we will print state code
	         * For Orion By Sonu
	         */
	        String stateNm="";
	        if(branch!=null){
	        for(Branch branch1 :branch){
	        	if(branch1.getGSTINNumber().equals(null) || branch1.getGSTINNumber().equals("")){
	        		
	        	
				if(branch1.getAddress().getState()!=null){
					stateNm=comp.getAddress().getState();
					
					logger.log(Level.SEVERE ,"STATE OF COMPANY");
				}
					
				}else{
					stateNm=branch1.getAddress().getState();				
					logger.log(Level.SEVERE, "STATE OF BANK");
				}
			}
	        }
	        else{
	        	stateNm=comp.getAddress().getState();        	
	        }
		    

		    Phrase stateCodeStr1 = null;
		    /**
		     * nidhi
		     * 31-08-2018 
		     * for print company address state code
		     */
		    
	        String stateCodeStr = null;
	        /**
	         * @author Anil ,Date : 03-01-2019 
	         * Below method pick branch state code,but for orion if GSTIN number is present in Branch master 
	         * then only we will pick state code of that branch else state code will be picked from company master
	         */
//		    stateCodeStr = serverApp.getStateOfCompany(comp, branch.get(0).getBusinessUnitName(), stateList); 
		     for(State st:stateList){
		    	 if(st.getStateName().equals(stateNm)){
		    		 stateCodeStr=st.getStateCode();
		    		 break;
		    	 }
		     }
	   
		   

		    PdfPTable statetable = new PdfPTable(3);
		    statetable.setWidthPercentage(100.0F);
		    try {
		      statetable.setWidths(new float[] { 2.0F, 1.0F, 2.0F });
		    }
		    catch (Exception localException1) {}


		    Phrase statecodeph = new Phrase(stateNm+", Code : "+stateCodeStr, font10);
		    PdfPCell statenameAndcodecell = new PdfPCell(statecodeph);
		    statenameAndcodecell.setBorder(0);
		    
	         
	 
		    String branchmail = "";
		    
		    if (checkEmailId) {
		      branchmail = serverApp.getBranchEmail(comp, invoiceentity.getBranch());
		      System.out.println("server method " + branchmail);
		    }
		    else {
		      branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
		      System.out.println("server method 22" + branchmail);
		    }
		    

		    String email = null;
		    Phrase emailph = new Phrase("Email", font8bold);
		    PdfPCell emailcell = new PdfPCell(emailph);
		    emailcell.setBorder(0);
		    
		    if (branchmail != null) {
		      email = branchmail;
		    }
		    else {
		      email = "";
		    }
		    
		    Phrase emailvalph = new Phrase(email, font8);
		    PdfPCell emailvalcell = new PdfPCell(emailvalph);
		    emailvalcell.setBorder(0);
		    emailvalcell.setHorizontalAlignment(0);
		    

		    Phrase colon = new Phrase(":", font10bold);
		    PdfPCell colonCell = new PdfPCell(colon);
		    colonCell.setBorder(0);
		    colonCell.setHorizontalAlignment(0);
		    
		    PdfPTable rowspantable = new PdfPTable(3);
		    rowspantable.setWidthPercentage(100.0F);
		    float[] columnCollon3 = { 2.3F, 0.2F, 7.5F };
		    try {
		      rowspantable.setWidths(columnCollon3);
		    }
		    catch (Exception localException2) {}
		    
		    /** 
		     *  email for company
		     */
//		    rowspantable.addCell(emailcell);
//		    rowspantable.addCell(colonCell);
//		    rowspantable.addCell(emailvalcell);
		    
		    rowspantable.addCell(gstcell);
		    rowspantable.addCell(colonCell);
		    rowspantable.addCell(gstvalcell);
		    
		 
		    rowspantable.addCell(statenamecell);
		    rowspantable.addCell(colonCell);
		    rowspantable.addCell(statenameAndcodecell); 
		    

		    PdfPCell rowspantablecell = new PdfPCell(rowspantable);
//		    rowspantablecell.setBorderWidthTop(0.0F);
//		    rowspantablecell.setBorderWidthLeft(0);
//		    rowspantablecell.setBorderWidthBottom(0);
//		    rowspantablecell.setBorder(0);
		    rowspantablecell.setBorderWidthTop(0);
		    rowspantablecell.setBorderWidthRight(0);
		    rowspantablecell.setBorderWidthBottom(0);
		    companynameTable.addCell(rowspantablecell);
		    
		    PdfPCell companynameTablecell = new PdfPCell(companynameTable);
		    companynameTablecell.setBorderWidthTop(0.0F);
		    companynameTablecell.setBorderWidthBottom(0.0F);
		    companynameTablecell.setBorderWidthLeft(0.0F);
		    companynameTablecell.setBorderWidthRight(0.0F);
		    companyAndInvoiceInfoTable.addCell(companynameTablecell);
		    
		    PdfPTable invoiceNoTable = new PdfPTable(4);
		    invoiceNoTable.setWidthPercentage(100.0F);
		    float[] columnWidth4 = { 2.8F, 3.5F, 1.2F, 2.5F};
		    try {
		    	invoiceNoTable.setWidths(columnWidth4);
		    }
		    catch (DocumentException e) {
		      e.printStackTrace();
		    }
		    String invoiceRef="";
		    if(invoiceentity.getRefNumber()!=null && !invoiceentity.getRefNumber().equals("")){
		    	  invoiceRef = invoiceentity.getRefNumber();
		    }
		    logger.log(Level.SEVERE,"invoiceRef="+invoiceRef);
		    if(con!=null&&con.getRefNo()!=null&&!con.getRefNo().equals("")) {
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell("Invoice No.", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1, 0, -1, 0, 10, 10, 0, 0));
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell(""+invoiceRef, font8bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0,-1,-1, 0, 0, 0, 10, 10, 0, 0));
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell("Dated", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1, 0, 0, 0, 10, 10, 0, 0));
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell(""+ddMMMyy.format(invoiceentity.getInvoiceDate()), font8bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1,0, 0, -1, 10, 10, 0, 0));
		    }
		    if(con!=null&&con.getPoNumber()!=null&&!con.getPoNumber().equals("")&&con.getPoDate()!=null&&!con.getPoDate().equals("")) {
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell("PO No.", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1,-1, 0, -1, 0, 10, 10, 0, 0));
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell(""+con.getPoNumber(), font8bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1, 0, 0, 0, 10, 10, 0, 0));
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell("Dated", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1, 0, 0, 0, 10, 10, 0, 0));
		    	invoiceNoTable.addCell(pdfUtility.getPdfCell(""+ddMMMyy.format(con.getPoDate()), font8bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1,-1,0, 0, -1, 10, 10, 0, 0));
		    }
		    
		    String servicestateName = "";	   
		    if(custbranch!=null&&custbranch.getAddress()!=null) {
		    	servicestateName = custbranch.getAddress().getState().trim();	      
		    }else if(cust!=null&&cust.getSecondaryAdress()!=null) {
		    	servicestateName = cust.getSecondaryAdress().getState().trim();
		    }
		    
		    invoiceNoTable.addCell(pdfUtility.getPdfCell("Place of Supply.", font8, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, -1, -1, 0, -1, 0, 0, 0, 0, 0));
		    invoiceNoTable.addCell(pdfUtility.getPdfCell(servicestateName, font8bold, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 3, 0, -1, -1,0, 0,-1, 0, 0, 0, 0));
		    
		  
		    PdfPCell invoiceinfo = new PdfPCell(invoiceNoTable);
		    invoiceinfo.setBorder(0);
		    companyAndInvoiceInfoTable.addCell(invoiceinfo);
		    
		    try {
				document.add(companyAndInvoiceInfoTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  }
	  
	  
	  
	  

}