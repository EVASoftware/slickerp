package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class CustomerPaySlipVersionOnePdf {
	
	CustomerPayment custpay;
	Customer cust;
	Company comp;
	Branch branchDt = null;
	Vendor vendor;
	Branch branch;
	public  Document document;
	public GenricServiceImpl impl;
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font10bold,font10,font14bold,font9,font11bold;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1= new SimpleDateFormat("MMM-yyyy");
	  Logger logger = Logger.getLogger("NameOfYourLogger");

	  float[] columnWidths2={1.2f,0.2f,1.5f,1.4f,0.2f,1.5f};
	  float[] columnWidths={1.2f,0.4f,1.5f,1.2f,0.4f,1.5f};
	  
	  float[] columnWidths3 = {3f, 0.5f, 2f, 2.5f,0.5f,2f,2.5f,0.5f,3.5f};
	  
	  float[] relativeWidths ={0.8f,9.2f};
	
	DecimalFormat df1 = new DecimalFormat("#");
	DecimalFormat df=new DecimalFormat("0.00");
	ProcessConfiguration processConfig;
	boolean PrintPaySlipOnSamePage=false;
	 boolean paytermsflag=false;
	 boolean upcflag;
	 boolean disclaimerflag=false;
	 
	 CompanyPayment comppayment,selectedCompPayment;
	 private PdfPCell imageSignCell;

	 //Ashwini Patil Date:25-06-2022
	 boolean thaiFontFlag=false; 
	 boolean thaiPdfFlag=false;
	 boolean AmountInWordsHundreadFormatFlag=false;
	 Invoice invoice;
	 CustomerBranchDetails customerBranch=null;

	
	 public CustomerPaySlipVersionOnePdf()
	 {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font9= new Font(Font.FontFamily.HELVETICA,9);
		font11bold=new Font(Font.FontFamily.HELVETICA,11,Font.BOLD);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10bold= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD);
		font10=new Font(Font.FontFamily.HELVETICA,10,Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		impl= new GenricServiceImpl();
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
	}

	public void setCustomerPaySlip(long count)
	{
		custpay=ofy().load().type(CustomerPayment.class).id(count).now();
		
		//Load Customer
		if(custpay.getCompanyId()!=null)
			 cust=ofy().load().type(Customer.class).filter("companyId", custpay.getCompanyId()).filter("count",custpay.getCustomerId()).first().now();
		
		//Load Vendor
		if(custpay.getCompanyId()!=null)
			 vendor=ofy().load().type(Vendor.class).filter("companyId", custpay.getCompanyId()).filter("count",custpay.getCustomerId()).first().now();
		
		//Load Company
		if(custpay.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",custpay.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		
		//Load Branch
		if(custpay.getCompanyId()==null)
			branch=ofy().load().type(Branch.class).filter("companyId",custpay.getCompanyId()).first().now();
		else
			branch=ofy().load().type(Branch.class).filter("companyId",custpay.getCompanyId()).filter("buisnessUnitName",custpay.getBranch().trim()).first().now();

		//Load selected Compnay payment
		String[] paymentmode=null;
		String pid="";
		if(custpay.getPaymentMode()!=null)
			paymentmode=custpay.getPaymentMode().split("/");
		if(paymentmode!=null && paymentmode.length>0)
			pid=paymentmode[0].trim();
		logger.log(Level.SEVERE, "pid="+pid);
		if(!pid.equals("")) {
			logger.log(Level.SEVERE, "fetching comp payment");
			selectedCompPayment = ofy().load().type(CompanyPayment.class)
				.filter("count", Integer.parseInt(pid))
				.filter("companyId", custpay.getCompanyId()).first()
				.now();
			if(selectedCompPayment!=null)
				logger.log(Level.SEVERE, "after fetching comp payment");
			else
				logger.log(Level.SEVERE, "selectedCompPayment not loaded");
		}
		
		if(custpay.getCompanyId()!=null){
			
			System.out.println("inside condition");
			
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "CustomerPayment").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					System.out.println("xxxxxxxxxxxxx"+processConfig.getProcessList().get(k).getProcessType());
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintPaySlipOnSamePage")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						System.out.println("value true condition");
						
						PrintPaySlipOnSamePage=true;
					}
				}
			}
		}
		
		
		logger.log(Level.SEVERE,"get for check configration --");
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(custpay !=null && custpay.getBranch() != null && custpay.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",custpay.getCompanyId()).filter("buisnessUnitName", custpay.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", custpay.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		//Ashwini Patil
		invoice=ofy().load().type(Invoice.class)
				.filter("count", custpay.getInvoiceCount())
				.filter("companyId", custpay.getCompanyId()).first()
				.now();
		if(invoice!=null&&invoice.getCustomerBranch()!=null&&!invoice.getCustomerBranch().equals("")) {
			customerBranch = ofy().load().type(CustomerBranchDetails.class)
					.filter("cinfo.count",invoice.getPersonInfo().getCount())
					.filter("buisnessUnitName",invoice.getCustomerBranch()).first().now();
		}
		
		String companyCountry="";
		if(comp!=null){
			companyCountry=comp.getAddress().getCountry().trim();
		}
		if(companyCountry!=null&&!companyCountry.equals("")){
			if(companyCountry.equalsIgnoreCase("Thailand")||companyCountry.trim().equalsIgnoreCase("ประเทศไทย")){				
				thaiPdfFlag=true;
//				thaiFontFlag=true;
			}
		}
		ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", custpay.getCompanyId())
				.filter("processName", "Company")
				.filter("configStatus", true).first().now();
		
		if(processConfig!=null){
			for(ProcessTypeDetails obj:processConfig.getProcessList()){
				if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
					thaiFontFlag=true;
//					break;
				}
				if(obj.getProcessType().equalsIgnoreCase(AppConstants.PC_AMOUNTINWORDSHUNDREADSTRUCTURE)&&obj.isStatus()==true){
					AmountInWordsHundreadFormatFlag = true;
				}
			}
		}
		
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
																	
				BaseFont tahomaFont=BaseFont.createFont("Tahoma Regular font.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahomaBoldFont=BaseFont.createFont("TAHOMAB0.TTF",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font10 = new Font(tahomaFont, 9);
				font10bold = new Font(tahomaFont, 9,Font.BOLD);
			}
			
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
	}
	
	
	public  void createPdf(String preprintStatus) 
	{
		
		
			if(preprintStatus.equals("plane")){
				
				
				System.out.println("contract inside plane");
//				createLogo(document,comp);
//				createCompanyAddress();
//				Createblank();
				
				createHeader();
				if(thaiPdfFlag) {
					createThaiPaymentReceiptDetails();
				}else
					createCompanyAddressDetails();

			}else{
				
				if(preprintStatus.equals("yes")){
					
					System.out.println("inside prit yes");
					createSpcingForHeading();
					//createCompanyAddress();
					//Createblank();
					if(thaiPdfFlag) {
						createThaiPaymentReceiptDetails();
					}else
						createCompanyAddressDetails();
				}
				
				if(preprintStatus.equals("no")){
				
				System.out.println("inside prit no");
				
				if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
				createSpcingForHeading();
				if(thaiPdfFlag) {
					createThaiPaymentReceiptDetails();
				}else
					createCompanyAddressDetails();
				}
			}
			
			
		
    }
	
	
private void createLogo(Document doc, Company comp) {
		
		//********************logo for server ********************
		
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
private void Createblank() {
	
    Paragraph blank =new Paragraph();
    blank.add(Chunk.NEWLINE);
    try {
		document.add(blank);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
		
}	


private void createCompanyAddressDetails() {
	
	PdfUtility pdfUtility=new PdfUtility();
	String title="";
	title="Payment Receipt";
	
	Date conEnt=null;
	conEnt=custpay.getPaymentDate();

	String countinfo="ID : "+custpay.getCount();
	String creationdateinfo="Date : "+fmt.format(conEnt);

	Phrase titlephrase =new Phrase(title,font11bold);
	Paragraph titlepdfpara=new Paragraph();
	titlepdfpara.add(titlephrase);
	titlepdfpara.setAlignment(Element.ALIGN_CENTER);
	titlepdfpara.add(Chunk.NEWLINE);
	PdfPCell titlepdfcell=new PdfPCell();
	titlepdfcell.addElement(titlepdfpara);
	titlepdfcell.setBorder(0);
	//titlepdfcell.addElement(Chunk.NEWLINE);

	Phrase idphrase=new Phrase(countinfo,font11bold);
	Paragraph idofpdfpara=new Paragraph();
	idofpdfpara.add(idphrase);
	idofpdfpara.setAlignment(Element.ALIGN_LEFT);
	idofpdfpara.add(Chunk.NEWLINE);
	PdfPCell countcell=new PdfPCell();
	countcell.addElement(idofpdfpara);
	countcell.setBorder(0);
	//countcell.addElement(Chunk.NEWLINE);
	
	Phrase dateofpdf=new Phrase(creationdateinfo,font11bold);
	Paragraph creatndatepara=new Paragraph();
	creatndatepara.add(dateofpdf);
	creatndatepara.setAlignment(Element.ALIGN_RIGHT);
	creatndatepara.add(Chunk.NEWLINE);
	PdfPCell creationcell=new PdfPCell();
	creationcell.addElement(creatndatepara);
	creationcell.setBorder(0);
	//creationcell.addElement(Chunk.NEWLINE);
	
	PdfPTable titlepdftable=new PdfPTable(3);
	titlepdftable.setWidthPercentage(100);
	titlepdftable.addCell(countcell);
	titlepdftable.addCell(titlepdfcell);
	titlepdftable.addCell(creationcell);
	
	titlepdftable.setSpacingAfter(10f);
	
	
	
	PdfPTable custtable=new PdfPTable(3);
	custtable.setWidthPercentage(100);
	float[] columns = { 1f,0.1f,2f};
	try {
		custtable.setWidths(columns);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	
	
		String Title="Received with thanks from         :   ";
		
		String custName="";
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
	    Paragraph fullname =new Paragraph();
	    fullname.add(Chunk.NEWLINE);
	    if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
	    	fullname.add(custName);
		}
	    else
	    {
	    	fullname.add(Title+custName);
	    }
	    fullname.setFont(font10);
	    
			PdfPCell custnamecell=new PdfPCell();
			custnamecell.addElement(fullname);
			custnamecell.setBorder(0);
			
			custtable.addCell(pdfUtility.getCell("Received with thanks from", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(custName+"", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			
		
			String custinfo="Customer ID : "+cust.getCount();
			Phrase custidphrase=new Phrase("                                                       "+custinfo,font10);
			                              
			Paragraph pdfpara=new Paragraph();
			pdfpara.add(custidphrase);
			pdfpara.setAlignment(Element.ALIGN_LEFT);
			//idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell custcell=new PdfPCell();
			custcell.addElement(pdfpara);
			custcell.setBorder(0);
			
		
			custtable.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell("Customer ID : "+cust.getCount()+"", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			
//			Phrase customeradress= new Phrase("                                                      "+cust.getAdress().getAddrLine1(),font10);
//			Phrase customeradress2=null;
//			if(cust.getAdress().getAddrLine2()!=null){
//		    customeradress2= new Phrase("                                                      "+cust.getAdress().getAddrLine2(),font10);
//		}
//		
//		PdfPCell custaddress1=new PdfPCell();
//		custaddress1.addElement(customeradress);
//		custaddress1.setBorder(0);
//		
//		PdfPCell custaddress2=new PdfPCell();
//		if(cust.getAdress().getAddrLine2()!=null){
//			custaddress2.addElement(customeradress2);
//			custaddress2.setBorder(0);
//			}
//		
//		
//		String custlandmark2="";
//		Phrase landmar2=null;
//		
//		String  localit2="";
//		Phrase custlocality2= null;
//		
//		if(cust.getAdress().getLandmark()!=null)
//		{
//			custlandmark2 = cust.getAdress().getLandmark();
//			landmar2=new Phrase("                                                      "+custlandmark2,font10);
//		}
//		
//		
//		if(cust.getAdress().getLocality()!=null){
//			localit2=cust.getAdress().getLocality();
//			custlocality2=new Phrase("                                                      "+localit2,font10);
//			}
//		
//		
//		
//		
////		if(comp.getAddress().getPin()!=0){
////			custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
////		}else{
////			custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
////		}
//		
//		String  Pin="";
//		Phrase cityState2= null;
//		
//		if(cust.getAdress().getPin()!=0){
//		Pin=(cust.getAdress().getCity()+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()+" - "+cust.getAdress().getPin());
//		landmar2=new Phrase("                                                      "+cityState2,font10);
//		}else {
//		Pin=(cust.getAdress().getCity()+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry());
//		landmar2=new Phrase("                                                      "+cityState2,font10);
//		}
//		PdfPCell custlandcell2=new PdfPCell();
//		custlandcell2.addElement(landmar2);
//		custlandcell2.setBorder(0);
//		
//		PdfPCell custlocalitycell2=new PdfPCell();
//		custlocalitycell2.addElement(custlocality2);
//		custlocalitycell2.setBorder(0);
//		
//		PdfPCell custcitycell2=new PdfPCell();
//		custcitycell2.addElement(cityState2);
//		custcitycell2.setBorder(0); 
//	//	custcitycell2.addElement(Chunk.NEWLINE);
			
			
			 String custAdd1="";
				String custFullAdd1="";
				/*
				 * Ashwini Patil 
				 * Date:30-04-2024
				 * Ankita pest reported that on Invoice, customer branch address is coming but on payment receipt, customer address is coming 
				 * 
				 */
				if(customerBranch!=null) {
					if(customerBranch.getBillingAddress().getAddrLine2()!=null && !customerBranch.getBillingAddress().getAddrLine2().equals("")){
					
						if(customerBranch.getBillingAddress().getLandmark()!=null && !customerBranch.getBillingAddress().getLandmark().equals("")){
							custAdd1=customerBranch.getBillingAddress().getAddrLine1()+","+customerBranch.getBillingAddress().getAddrLine2()+","+customerBranch.getBillingAddress().getLandmark();
						}else{
							custAdd1=customerBranch.getBillingAddress().getAddrLine1()+","+customerBranch.getBillingAddress().getAddrLine2();
						}
					}else{
						if(customerBranch.getBillingAddress().getLandmark()!=null && !customerBranch.getBillingAddress().getLandmark().equals("") ){
							custAdd1=customerBranch.getBillingAddress().getAddrLine1()+"\n"+customerBranch.getBillingAddress().getLandmark();
						}else{
							custAdd1=customerBranch.getBillingAddress().getAddrLine1();
						}
					}
					
					if(customerBranch.getBillingAddress().getLocality()!=null&& !customerBranch.getBillingAddress().getLocality().equals("")){
						if(customerBranch.getBillingAddress().getPin()!=0){
							custFullAdd1=custAdd1+"\n"+""+customerBranch.getBillingAddress().getLocality()+","+customerBranch.getBillingAddress().getCity()+","+customerBranch.getBillingAddress().getState()+","+customerBranch.getBillingAddress().getCountry()+","+customerBranch.getBillingAddress().getPin();
						}else{
							custFullAdd1=custAdd1+"\n"+""+customerBranch.getBillingAddress().getLocality()+","+customerBranch.getBillingAddress().getCity()+","+customerBranch.getBillingAddress().getState()+","+customerBranch.getBillingAddress().getCountry();
						}
					}else{
						if(customerBranch.getBillingAddress().getPin()!=0){
							custFullAdd1=custAdd1+"\n"+""+customerBranch.getBillingAddress().getCity()+", "+customerBranch.getBillingAddress().getState()+", "+customerBranch.getBillingAddress().getCountry()+","+customerBranch.getBillingAddress().getPin();
						}else{
							custFullAdd1=custAdd1+"\n"+""+customerBranch.getBillingAddress().getCity()+","+customerBranch.getBillingAddress().getState()+","+customerBranch.getBillingAddress().getCountry();
						}
					}
				}else if(cust.getAdress()!=null){
					
					if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
					
						if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
						}else{
							custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
						}
					}else{
						if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("") ){
							custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
						}else{
							custAdd1=cust.getAdress().getAddrLine1();
						}
					}
					
					if(cust.getAdress().getLocality()!=null&& !cust.getAdress().getLocality().equals("")){
						if(cust.getAdress().getPin()!=0){
							custFullAdd1=custAdd1+"\n"+""+cust.getAdress().getLocality()+","+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+","+cust.getAdress().getPin();
						}else{
							custFullAdd1=custAdd1+"\n"+""+cust.getAdress().getLocality()+","+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
						}
					}else{
						if(cust.getAdress().getPin()!=0){
							custFullAdd1=custAdd1+"\n"+""+cust.getAdress().getCity()+", "+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+","+cust.getAdress().getPin();
						}else{
							custFullAdd1=custAdd1+"\n"+""+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
						}
					}
					
				}	
				
//					Phrase addressline=new Phrase("                                                       "+custFullAdd1,font10);
//					Paragraph addresspara=new Paragraph();
//					addresspara.add(addressline);
//					addresspara.setAlignment(Element.ALIGN_LEFT);
//					
//					PdfPCell addresscell=new PdfPCell();
//					addresscell.addElement(addresspara);
//					addresscell.setBorder(0);
					
					
		
					custtable.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					custtable.addCell(pdfUtility.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
					custtable.addCell(pdfUtility.getCell(custFullAdd1+"", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					
					
//					Phrase totalval=new Phrase(Math.round(con.getNetpayable())+"",font9);		// change by Viraj for decimal on Date 25-10-2018
//					PdfPCell totalvalcell=new PdfPCell(totalval);
//					totalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//					table3.addCell(totalvalcell);
					
		//double amount=0;
		//Math.round(con.getNetpayable())
		//amount=Math.round(custpay.getPaymentReceived());
		System.out.println("Math.round(custpay.getPaymentReceived())"+Math.round(custpay.getNetPay()));
		//amount=Math.round(custpay.getPaymentReceived());
		
		Phrase amountpara=null;
		String  amtInFigure="";
		if(custpay.getNetPay()!=0)
		{
		amtInFigure= ServiceInvoicePdf.convert(custpay.getNetPay());
		//String output =amtInFigure.substring(0, 1).toUpperCase()+amtInFigure.substring(1);
		//String  amtInFigure= ServiceInvoicePdf.convert(custpay.getPaymentReceived()).toLowerCase();
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.CONTRACT, AppConstants.PC_NO_ROUNDOFF_INVOICE_CONTRACT, custpay.getCompanyId())){
			amountpara=new Phrase("\n"+"A sum of Rupees                        :  "+" "+custpay.getNetPay()+"/- "+"("+amtInFigure+" Only"+" )",font10);//amount
			amtInFigure=custpay.getNetPay()+"/- "+"("+amtInFigure+" Only"+" )";
		}
		else{
			amountpara=new Phrase("\n"+"A sum of Rupees                        :  "+" "+Math.round(custpay.getNetPay())+"/- "+"("+amtInFigure+" Only"+" )",font10);//amount
			amtInFigure=Math.round(custpay.getNetPay())+"/- "+"("+amtInFigure+" Only"+" )";
		}
		
		}
		else
		{
		 amountpara=new Phrase(" ",font10);
		 amtInFigure="";
		}
		
		PdfPCell amountcell=new PdfPCell(amountpara);
		amountcell.setBorder(0);
		amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//amountcell.setHorizontalAlignment(Element.ALIGN_BOTTOM);
		
		custtable.addCell(pdfUtility.getCell("A sum of Rupees", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		custtable.addCell(pdfUtility.getCell(amtInFigure, font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		//Phrase payMethodPhrasetitle = new Phrase("Payment Method ",font10);
		
//		Phrase paymethod=null;
//		String payMethodValue="";
//		System.out.println("getPaymentMethod()"+custpay.getPaymentMethod());
//		if (custpay.getPaymentMethod() != null) {
//			System.out.println("getPaymentMethod()"+custpay.getPaymentMethod());
//			if (custpay.getPaymentMethod().trim().equalsIgnoreCase("Cash")) {
//				paymethod = new Phrase("By mode                                      :   " + custpay.getPaymentMethod(), font10);
//				payMethodValue=custpay.getPaymentMethod();
//			} else if (custpay.getPaymentMethod().trim().equalsIgnoreCase("Cheque")) {
//				paymethod = new Phrase("By mode                                      :   " + custpay.getPaymentMethod()
//						+ "/" + "Cheque Number : " + custpay.getChequeNo(),font10);
//				payMethodValue=custpay.getPaymentMethod()+"/" + "Cheque Number : " + custpay.getChequeNo();
//				
//			} else if (custpay.getPaymentMethod().trim().equalsIgnoreCase("NEFT")) {
//				paymethod = new Phrase("By mode                                      :   " + custpay.getPaymentMethod()
//						+ "/" + "Digital Reference No : "+ custpay.getReferenceNo(), font10);
//				payMethodValue=custpay.getPaymentMethod()+ "/" + "Digital Reference No : "+ custpay.getReferenceNo();
//			} else {
//				paymethod = new Phrase("By mode                                      :   " + custpay.getPaymentMethod(), font10);
//				payMethodValue=custpay.getPaymentMethod();
//			}
//
//		}

		
		
	
//		if(custpay.getPaymentMethod()!=null){
//			if(custpay.getPaymentMethod().equals("Cash")){
//			 paymethod=new Phrase("By Mode                      : "+custpay.getPaymentMethod(),font10);
//			}else{
//			 paymethod=new Phrase("By Mode                      : "+" ",font10);
//			}
//			
//			if(custpay.getPaymentMethod().equals("Cheque")){
//			paymethod=new Phrase("By Mode                      : "+custpay.getPaymentMethod()+"/"+"Cheque Number :-"+custpay.getChequeNo(),font10);
//		    }else{
//			paymethod=new Phrase("By Mode                      : "+" ",font10);
//			}
//			
//			if(custpay.getPaymentMethod().equals("NEFT")){
//				paymethod=new Phrase("By Mode                      : "+custpay.getPaymentMethod()+"/"+"Digital-Reference No :-"+custpay.getReferenceNo(),font10);
//			 }else{
//			 paymethod=new Phrase("By Mode                      : "+" ",font10);
//			 }
//			
//			
//		
//		}
		
//		PdfPCell paymethodcell1=new PdfPCell();
//		paymethodcell1.addElement(paymethod);
//		paymethodcell1.setBorder(0);
//		paymethodcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		if (custpay.getPaymentMethod() != null) {
			custtable.addCell(pdfUtility.getCell("By mode", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(custpay.getPaymentMethod(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			
			if (custpay.getChequeNo()!=null&&!custpay.getChequeNo().equals("")) {
				custtable.addCell(pdfUtility.getCell("Cheque Number", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
				custtable.addCell(pdfUtility.getCell(custpay.getChequeNo(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
			if (custpay.getReferenceNo()!=null&&!custpay.getReferenceNo().equals("")){
				custtable.addCell(pdfUtility.getCell("Digital Reference No", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
				custtable.addCell(pdfUtility.getCell(custpay.getReferenceNo(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);			
			}
		
		}
		
		
		
		Phrase paymentDate=null;
		String paymentDateValue="";
		
		System.out.println("getPaymentMethod()"+custpay.getPaymentMethod());
		if (custpay.getPaymentMethod() != null) {
//			System.out.println("getPaymentMethod()"+custpay.getPaymentMethod());
//			if (custpay.getPaymentMethod().trim().equalsIgnoreCase("Cash")) {
//				paymentDate = new Phrase("On the date                                :   " +fmt.format(custpay.getPaymentDate())+"", font10); //Ashwini Patil Date: 14-02-2022 Desciption: issue reported: Payment Date not getting reflected 
//			} else if (custpay.getPaymentMethod().trim().equalsIgnoreCase("Cheque")) {
//				paymentDate = new Phrase("On the date                                :   " +fmt.format(custpay.getChequeDate())+"",font10);
//			} else if (custpay.getPaymentMethod().trim().equalsIgnoreCase("NEFT")) {
//				if(custpay.getAmountTransferDate()!=null) {
//				paymentDate = new Phrase("On the date                                :   " +fmt.format(custpay.getAmountTransferDate())+"",font10);
//				} else {
//					paymentDate = new Phrase("On the date                                :   " + " ", font10);
//				}
//			} else {
//				if(custpay.getAmountTransferDate()!=null) {
//					paymentDate = new Phrase("On the date                                :   " +fmt.format(custpay.getAmountTransferDate()), font10);
//				}
//				else{
//					paymentDate = new Phrase("On the date                                :   " + " ", font10);
//				}
//
//			}
			/**
			 * @authod Ashwini Patil
			 * @since 17-03-2022
			 * As per Nitin sir's Instruction If Payment Received date is present then that should get printed otherwise Payment due date should get printed.
			 */
			if (custpay.getPaymentMethod().trim().equalsIgnoreCase("Cheque")&&custpay.getChequeDate()!=null) {
				paymentDate = new Phrase("On the date                                 :   " +fmt.format(custpay.getChequeDate())+"",font10);
				paymentDateValue=fmt.format(custpay.getChequeDate());
			}else if(custpay.getAmountTransferDate()!=null){
				paymentDate = new Phrase("On the date                                 :   " +fmt.format(custpay.getAmountTransferDate())+"", font10);
				paymentDateValue=fmt.format(custpay.getAmountTransferDate());
			}else{
				paymentDate = new Phrase("On the date                                 :   " +fmt.format(custpay.getPaymentDate())+"", font10);
				paymentDateValue=fmt.format(custpay.getPaymentDate());
			}
		}
		
//		if(custpay.getChequeDate()!=null){
//			paymentDate=new Phrase("On the date                               : "+fmt.format(custpay.getChequeDate())+"",font10);
//		}else{
//			paymentDate=new Phrase("On the date                               : "+" ",font10);
//		}
//				
//		if(custpay.getAmountTransferDate()!=null){
//			paymentDate=new Phrase("On the date                               : "+fmt.format(custpay.getAmountTransferDate())+"",font10);
//		}else{
//			paymentDate=new Phrase("On the date                               : "+" ",font10);
//		}
		
		PdfPCell paymethodcell=new PdfPCell();
		paymethodcell.addElement(paymentDate);
		paymethodcell.setBorder(0);
		paymethodcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		if(!paymentDateValue.equals("")) {
		custtable.addCell(pdfUtility.getCell("On the date", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		custtable.addCell(pdfUtility.getCell(paymentDateValue, font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
			
		PdfPCell payBankcell1=null;
		if(custpay.getBankName()!=null&&!custpay.getBankName().equals("")) {
//			Phrase payBank=new Phrase("Bank Name                                 :   "+custpay.getBankName(), font10);
//			payBankcell1=new PdfPCell();
//			payBankcell1.addElement(payBank);
//			payBankcell1.setBorder(0);
//			payBankcell1.setHorizontalAlignment(Element.ALIGN_LEFT);	
			custtable.addCell(pdfUtility.getCell("Bank Name", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(custpay.getBankName(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
		}
		
		PdfPCell payBankBranchcell1=null;
		if(custpay.getBankBranch()!=null&&!custpay.getBankBranch().equals("")) {
//			Phrase payBank=new Phrase("Bank Branch                               :   "+custpay.getBankBranch(), font10);
//			payBankBranchcell1=new PdfPCell();
//			payBankBranchcell1.addElement(payBank);
//			payBankBranchcell1.setBorder(0);
//			payBankBranchcell1.setHorizontalAlignment(Element.ALIGN_LEFT);	
			custtable.addCell(pdfUtility.getCell("Bank Branch", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(custpay.getBankBranch(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
		}
		logger.log(Level.SEVERE, "before printing Payable At");
		PdfPCell payableAtcell1=null;
		if(selectedCompPayment!=null) {
			logger.log(Level.SEVERE, "in selectedCompPayment!=null");
			if(selectedCompPayment.getPaymentPayableAt()!=null&&!selectedCompPayment.getPaymentPayableAt().equals("")) {
//				Phrase payBank=new Phrase("Payable At                                  :   "+selectedCompPayment.getPaymentPayableAt(), font10);
//				payableAtcell1=new PdfPCell();
//				payableAtcell1.addElement(payBank);
//				payableAtcell1.setBorder(0);
//				payableAtcell1.setHorizontalAlignment(Element.ALIGN_LEFT);	
				custtable.addCell(pdfUtility.getCell("Payable At", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
				custtable.addCell(pdfUtility.getCell(selectedCompPayment.getPaymentPayableAt(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);				
			}
		}
						
		
		PdfPCell payBankAccountNocell1=null;
		if(custpay.getBankAccNo()!=null&&!custpay.getBankAccNo().equals("")) {			               
//			Phrase payBank=new Phrase("Bank A/C No.                              :   "+custpay.getBankAccNo(), font10);
//			payBankAccountNocell1=new PdfPCell();
//			payBankAccountNocell1.addElement(payBank);
//			payBankAccountNocell1.setBorder(0);
//			payBankAccountNocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			custtable.addCell(pdfUtility.getCell("Bank A/C No.", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(custpay.getBankAccNo(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);				
		
		}
		
		PdfPCell chkIssuedByCell1=null;
		if(custpay.getChequeIssuedBy()!=null&&!custpay.getChequeIssuedBy().equals("")) {
//			Phrase chkIssuedBy=new Phrase("Cheque Issued By                      :   "+custpay.getChequeIssuedBy(), font10);
//			chkIssuedByCell1=new PdfPCell();
//			chkIssuedByCell1.addElement(chkIssuedBy);
//			chkIssuedByCell1.setBorder(0);
//			chkIssuedByCell1.setHorizontalAlignment(Element.ALIGN_LEFT);	
			custtable.addCell(pdfUtility.getCell("Cheque Issued By", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(custpay.getChequeIssuedBy(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);				
		
		}
		
	
		
		String Title1="For invoice numbers                   :   ";
		String refValue="";

		 Phrase refNovalueValue=null;
		 
		//Ashwini Patil Date:5-10-2023 for orion
		 if(invoice!=null&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "GenerateEinvoiceFromReferenceNumber", custpay.getCompanyId())){
			 if(invoice.getRefNumber()!=null&&!invoice.getRefNumber().equals(""))
				 refValue=invoice.getRefNumber()+" / "+"Date : "+fmt.format(custpay.getInvoiceDate());
			 else 
				 refValue=invoice.getCount()+" / "+"Date : "+fmt.format(custpay.getInvoiceDate());			 
		 }
		 else if(custpay.getInvoiceCount()!=0&&custpay.getInvoiceDate()!=null){
				 refNovalueValue=new Phrase(Title1+custpay.getInvoiceCount()+" / "+"Date : "+fmt.format(custpay.getInvoiceDate()),font10);
				 refValue=custpay.getInvoiceCount()+" / "+"Date : "+fmt.format(custpay.getInvoiceDate());
		}
		 else
		 {
			  refNovalueValue=new Phrase("",font10);
			  refValue="";
		 }
		
		 PdfPCell refNovalueValuecell=new PdfPCell();
		 refNovalueValuecell.addElement(refNovalueValue);
		 refNovalueValuecell.setBorder(0);
		 refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 if(!refValue.equals("")) {
			custtable.addCell(pdfUtility.getCell("For invoice numbers", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(":", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			custtable.addCell(pdfUtility.getCell(refValue, font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);							
		 }
		 
		 Phrase blankphrase=new Phrase("",font10);
			PdfPCell blankCell=new PdfPCell();
			blankCell.addElement(blankphrase);
			blankCell.setBorder(0);
			blankCell.setColspan(3);
		custtable.addCell(blankCell);
		custtable.addCell(blankCell);
		custtable.addCell(blankCell);
		custtable.addCell(blankCell);
		custtable.addCell(blankCell);
		 
		 String Title2="\n"+"\n"+"Cheque(s) is/are subject to realization.";
		 
		 Phrase refNo=new Phrase(Title2,font9);
		 PdfPCell refNocell=new PdfPCell();
		 refNocell.addElement(refNo);
		 refNocell.setBorder(0);
		 refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		custtable.addCell(pdfUtility.getCell(Title2, font10bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);						
		
			
			custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
//			custtable.addCell(custnamecell);//addresscell
//			custtable.addCell(custcell);//custcell
//			custtable.addCell(addresscell);
//			custtable.addCell(custaddress1); 
//			if(!cust.getAdress().getAddrLine2().equals("")){
//				custtable.addCell(custaddress2);
//			}
//			if(!cust.getAdress().getLandmark().equals("")){
//				custtable.addCell(custlandcell2);
//			}
//			if(!cust.getAdress().getLocality().equals("")){
//				
//			custtable.addCell(custlocalitycell2);
//			}
//			custtable.addCell(custcitycell2);
			
//			custtable.addCell(amountcell);//amountcell
//			
//			custtable.addCell(paymethodcell1);//paymethodcell1
//			custtable.addCell(paymethodcell);//paymethodcell			
//			if(payBankcell1!=null)
//				custtable.addCell(payBankcell1);
//			if(payBankBranchcell1!=null)
//				custtable.addCell(payBankBranchcell1);
//			if(payableAtcell1!=null)
//				custtable.addCell(payableAtcell1);
//			if(payBankAccountNocell1!=null)
//				custtable.addCell(payBankAccountNocell1);
//			if(chkIssuedByCell1!=null)
//				custtable.addCell(chkIssuedByCell1);
//			custtable.addCell(refNovalueValuecell);//refNovalueValuecell
//			custtable.addCell(blankCell);//blankCell
//			custtable.addCell(blankCell);
//			custtable.addCell(blankCell);
//			custtable.addCell(blankCell);
//			custtable.addCell(blankCell);
//			custtable.addCell(refNocell);//refNocell
	    
			custtable.setSpacingAfter(10f);
			
			PdfPCell cell = new PdfPCell();
			cell.addElement(custtable);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			
			PdfPTable parenttable = new PdfPTable(1);
			parenttable.addCell(cell);
			parenttable.setWidthPercentage(100);
			
			
			
			String Title3="This is computer generated receipt and does not carry a signature.";
			 
			 Phrase ref=new Phrase(Title3,font10);
			 PdfPCell refcell=new PdfPCell(ref);
			 //refcell.addElement(ref);
			 refcell.setBorder(0);
			 refcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
				//By mo
				PdfPTable main=new PdfPTable(1);
				main.setWidthPercentage(100);
				//main.setHorizontalAlignment(Element.ALIGN_CENTER);
				main.addCell(refcell);


	
	
	try {
		document.add(titlepdftable);
		document.add(parenttable);
		document.add(main);
		} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	
}


private void createSpcingForHeading() {
		
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , cust.getCompanyId())){
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
			
	}
	else{
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
	}

}

private void createCompanyNameAsHeader(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadHeader();

	// patch
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl
				+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 725f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
}

private void createCompanyNameAsFooter(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadFooter();

	// patch
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl
				+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 40f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}	


public  void createCompanyAddress()
{
	    String companyName ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyName=branchDt.getCorrespondenceName();
		}else{
			companyName = comp.getBusinessUnitName().trim().toUpperCase();
		}
	 

	 Paragraph companynamepara = new Paragraph();
	 companynamepara.add(companyName);
	 companynamepara.setFont(font14bold);
	 
		
     PdfPCell companyNameCell=new PdfPCell(companynamepara);
     companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
     companyNameCell.setFixedHeight(0);
   
     companyNameCell.setBorder(0);
     
     
     String custAdd1="";
		String custFullAdd1="";
		
		if(comp.getAddress()!=null){
			
			if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
			
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
				}
			}else{
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
					custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1();
				}
			}
			
			if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}else{
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}
			
		}	
		
			Phrase addressline=new Phrase(custFullAdd1,font12);
			Paragraph addresspara=new Paragraph();
			addresspara.add(addressline);
			addresspara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell addresscell=new PdfPCell();
			addresscell.addElement(addresspara);
			addresscell.setBorder(0);
		
     
	
	
	String mobile="";
	String landline="";
	
	
	if(comp.getCellNumber1()!=0){
		mobile=comp.getCellNumber1()+"";
	}
	else{
		mobile="";
	}
	
	if(comp.getLandline()!=0){
		landline=comp.getLandline()+"";
	}
	else{
		landline="";
	}
	
	String email="";
	if(comp.getEmail()!=null&&comp.getEmail()!="")
	{
		email=comp.getEmail()+"";
	}else{
		email="";
	}
	
	
	
	Phrase contactinfo=new Phrase("Phone : "+mobile+"  "+"Email : "+email,font10);

	Paragraph realmobpara=new Paragraph();
	realmobpara.add(contactinfo);
	realmobpara.setAlignment(Element.ALIGN_CENTER);

	PdfPCell contactcell=new PdfPCell();
	contactcell.addElement(realmobpara);
	contactcell.setBorder(0);
	
	PdfPTable companyDetails=new PdfPTable(1);
	companyDetails.setWidthPercentage(100);
	
	companyDetails.addCell(companyNameCell);
	companyDetails.addCell(addresscell);

	companyDetails.addCell(contactcell);   //contactcell1

	companyDetails.setSpacingAfter(12f);
	

	
	PdfPTable parent=new PdfPTable(1);
	parent.setWidthPercentage(100);
	
	try {
		parent.setWidths(new float[]{100});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	
	PdfPCell comapnyCell=new PdfPCell(companyDetails);
	comapnyCell.setBorderWidthTop(0);
	comapnyCell.setBorderWidthLeft(0);
	comapnyCell.setBorderWidthRight(0);
	comapnyCell.setFixedHeight(0);
	parent.addCell(comapnyCell);

	
	try {
		document.add(parent);

	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

	/*
	 * @author Ashwini Patil
	 * @since 25-06-2022
	 * Innovative require customized payment receipt
	 */
	public  void createThaiPaymentReceiptDetails(){
		PdfUtility pdfUtility=new PdfUtility();
		DecimalFormat df = new DecimalFormat("#,###.00");
		String title="";
		title="Payment Receipt";
		
		Date conEnt=null;
		conEnt=custpay.getPaymentDate();

		String countinfo="ID : "+custpay.getCount();
		String creationdateinfo="Date : "+fmt.format(conEnt);

		Phrase titlephrase =new Phrase(title,font11bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell=new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorder(0);
		//titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase=new Phrase(countinfo,font11bold);
		Paragraph idofpdfpara=new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell=new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorder(0);
		//countcell.addElement(Chunk.NEWLINE);
		
		Phrase dateofpdf=new Phrase(creationdateinfo,font11bold);
		Paragraph creatndatepara=new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell=new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorder(0);
		//creationcell.addElement(Chunk.NEWLINE);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);
		
		titlepdftable.setSpacingAfter(10f);
		
		try {
			document.add(titlepdftable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		PdfPTable mainTable=new PdfPTable(3);
		mainTable.setWidthPercentage(100);
		float[] columns = { 1f,0.1f,2f};
		try {
			mainTable.setWidths(columns);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String custName="";
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
		mainTable.addCell(pdfUtility.getCell("Customer ID", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(cust.getCount()+"", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		mainTable.addCell(pdfUtility.getCell("Customer Name", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(custName +"\n"+cust.getAdress().getCompleteAddress(), font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		int roundAmt = (int)invoice.getFinalTotalAmt();
//		String amtInWordsVal = EnglishNumberToWords.convert(roundAmt)+" baht";
		String amtInWordsVal=pdfUtility.getAmountInWords(invoice.getFinalTotalAmt(),invoice.getCompanyId(), cust);
//		String amtInWordsVal=pdfUtility.getAmountInWords(invoice.getFinalTotalAmt(), invoice.getCompanyId(), cust);		
		mainTable.addCell(pdfUtility.getCell("Invoice Amount", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(df.format(invoice.getFinalTotalAmt())+" ("+amtInWordsVal+")", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		double vat=0;
		for (int i = 0; i < invoice.getBillingTaxes().size(); i++) {
			if (invoice.getBillingTaxes().get(i).getTaxChargeName()
					.equalsIgnoreCase("VAT")) {
				vat = vat+ invoice.getBillingTaxes().get(i).getPayableAmt();
			}
		}
		
		mainTable.addCell(pdfUtility.getCell("VAT Amount", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(df.format(vat)+"", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		roundAmt = (int)invoice.getNetPayable();
//		amtInWordsVal = EnglishNumberToWords.convert(roundAmt)+" baht";
		amtInWordsVal=pdfUtility.getAmountInWords(invoice.getNetPayable(), invoice.getCompanyId(), cust);
		mainTable.addCell(pdfUtility.getCell("Total Invoice Amount", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(df.format(invoice.getNetPayable())+" ("+amtInWordsVal+")", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		roundAmt = (int)custpay.getNetPay();
//		amtInWordsVal = EnglishNumberToWords.convert(roundAmt)+" baht";
		amtInWordsVal=pdfUtility.getAmountInWords(custpay.getNetPay(), invoice.getCompanyId(), cust);
		mainTable.addCell(pdfUtility.getCell("Amount Received", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(df.format(custpay.getNetPay())+" ("+amtInWordsVal+")", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		roundAmt = (int)custpay.getTdsTaxValue();
//		amtInWordsVal = EnglishNumberToWords.convert(roundAmt)+" baht";
		amtInWordsVal=pdfUtility.getAmountInWords(custpay.getTdsTaxValue(), invoice.getCompanyId(), cust);
		mainTable.addCell(pdfUtility.getCell("WHT Amount", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(df.format(custpay.getTdsTaxValue())+" ("+amtInWordsVal+")", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		mainTable.addCell(pdfUtility.getCell("Payment Mode", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(custpay.getPaymentMethod(), font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		mainTable.addCell(pdfUtility.getCell("Payment Date", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		if(custpay.getAmountTransferDate()!=null)
			mainTable.addCell(pdfUtility.getCell(fmt.format(custpay.getAmountTransferDate())+ "", font10, Element.ALIGN_LEFT, 0, 0, 0));
		else
			mainTable.addCell(pdfUtility.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 0));
		
		
		mainTable.addCell(pdfUtility.getCell("Proforma Invoice Numbers", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		if(invoice.getProformaCount()!=null)
			mainTable.addCell(pdfUtility.getCell(invoice.getProformaCount()+"", font10, Element.ALIGN_LEFT, 0, 0, 0));
		else
			mainTable.addCell(pdfUtility.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0));
		
		mainTable.addCell(pdfUtility.getCell("Tax Invoice numbers", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(custpay.getInvoiceCount()+"", font10, Element.ALIGN_LEFT, 0, 0, 0));
	
		mainTable.addCell(pdfUtility.getCell("Remark", font10bold, Element.ALIGN_LEFT, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		mainTable.addCell(pdfUtility.getCell(custpay.getComment(), font10, Element.ALIGN_LEFT, 0, 0, 0));
	
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

private void createHeader() {
	
		
		PdfPTable headerTAB=new PdfPTable(3);
		headerTAB.setWidthPercentage(100);
		try {
			headerTAB.setWidths(new float[]{20,60,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable headertable=new PdfPTable(1);
		headertable.setWidthPercentage(100);
		headertable.setSpacingAfter(10);
		
		//Set Company Name
		String businessunit=null;
		businessunit=comp.getBusinessUnitName();
		
		Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
				font14bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyNameCell);
		
		//Set Address of Company
		String address = "";		
		address=comp.getAddress().getCompleteAddress();		
		
		Phrase companyAddph = new Phrase(address, font9);
		
		PdfPCell companyAddCell = new PdfPCell(companyAddph);
		companyAddCell.setBorder(0);
		companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyAddCell);

		//Email ID
		String emailid="";
		
		emailid = comp.getEmail();
			 
		
		Phrase companyemailph = new Phrase("E-Mail : "+emailid, font9);
		PdfPCell companyemailCell = new PdfPCell(companyemailph);
		companyemailCell.setBorder(0);
		companyemailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyemailCell);
		
		
		//WebSite
		String website="";
		if(comp.getWebsite()==null || comp.getWebsite().equals(""))
		{
			website="";
		}
		else
		{
			
			website="Website : "+comp.getWebsite();
		}
		Phrase companyWebph = new Phrase(website, font9);
		PdfPCell companyWebCell = new PdfPCell(companyWebph);
		companyWebCell.setBorder(0);
		companyWebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyWebCell);
		
		
		//Contact
       String contactinfo="";
		
			if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
				contactinfo = comp.getCountryCode() + comp.getCellNumber1() + "";
			}
			if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
				if (!contactinfo.trim().isEmpty()) {
					contactinfo = contactinfo + " / " + comp.getCountryCode()
							+ comp.getCellNumber2() + "";
				} else {
					contactinfo = comp.getCountryCode() + comp.getCellNumber2()
							+ "";
				}
				System.out.println("pn33" + contactinfo);
			}
			if (comp.getLandline() != 0 && comp.getLandline() != null) {
				if (!contactinfo.trim().isEmpty()) {
					contactinfo = contactinfo + " / " + comp.getStateCode()
							+ comp.getLandline() + "";
				} else {
					contactinfo = comp.getStateCode() + comp.getLandline() + "";
				}
				System.out.println("pn44" + contactinfo);
			}
		
		Phrase companymobph = new Phrase("Phone :  "+contactinfo,font9);
		PdfPCell companymobCell = new PdfPCell(companymobph);
		companymobCell.setBorder(0);
		companymobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companymobCell);
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setSpacingAfter(5);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{8,92});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		DocumentUpload logodocument ;
//		if(checkBranchdata) {
//			logodocument = branchDt.getLogo();
//		} else {
			logodocument =comp.getLogo();
//		}
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		if(logodocument!=null){
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setImage(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if(imageSignCell != null)
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		}
		else
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}

		PdfPTable titletab = new PdfPTable(1);
		titletab.setWidthPercentage(100f);
		titletab.setSpacingAfter(5);
		
		Phrase title =new Phrase("",font10bold);
		PdfPCell titlecell=new PdfPCell(title);
		titlecell.setBorder(0);
		titlecell.setPaddingRight(3);
		titlecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		titletab.addCell(titlecell);
		
		
		PdfPCell logocell=new PdfPCell(logoTable);
		logocell.setBorderWidthRight(0);
		
		PdfPCell headerCell=new PdfPCell(headertable);
		headerCell.setBorderWidthLeft(0);
		headerCell.setBorderWidthRight(0);
		
		PdfPCell titlcell=new PdfPCell(titletab);
		titlcell.setBorderWidthLeft(0);
		
		headerTAB.addCell(logocell);
		headerTAB.addCell(headerCell);
		headerTAB.addCell(titlcell);
		
		
		
		
		
		
		try {
			document.add(headerTAB);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	}


}
