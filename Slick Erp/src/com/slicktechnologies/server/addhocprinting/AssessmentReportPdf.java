package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.assesmentreport.AssesmentReportEmbedForTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class AssessmentReportPdf {

	public Document document;
	AssesmentReport report;
	Company comp;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,
			font14boldul,font16bold,font7bold,font6;

	float[] tblcol1width = { 1.2f, 7.5f };
	float[] tblcol7width = { 1.5f, 1.5f, 2f, 2f, 2.5f, 3.5f, 3.5f };
	
	Logger logger = Logger.getLogger("AssessmentReportPdf.class");
	boolean thaiFontFlag;
	Branch branchDt = null;
	PdfUtility pdfUtility=new PdfUtility();
	Customer cust;
	CustomerBranchDetails customerBranch;
	List<Employee> employeeList;
	int noOfLines=6;
	int index=0;
	boolean onlyForPestOCop=false;
	
	
	public AssessmentReportPdf() {

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font6 = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL);

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	}

	public void setAssessmentDetails(Long count, AssesmentReport assessmentRepObj) {

		if(assessmentRepObj!=null){
			report=assessmentRepObj;
		}else{
			report = ofy().load().type(AssesmentReport.class).id(count).now();
		}

		if (report.getCompanyId() != null) {
			comp = ofy().load().type(Company.class).filter("companyId", report.getCompanyId()).first().now();
		} else {
			comp = ofy().load().type(Company.class).first().now();
		}
		
		if(report!=null&&report.getCinfo()!=null){
			cust=ofy().load().type(Customer.class).filter("companyId", report.getCompanyId()).filter("count", report.getCinfo().getCount()).first().now();
		}
		
		if(cust!=null&&report.getCustomerBranch()!=null&&!report.getCustomerBranch().equals("")){
			customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", report.getCompanyId()).filter("cinfo.count", report.getCinfo().getCount()).filter("buisnessUnitName", report.getCustomerBranch()).first().now();
		}
		
		HashSet<String> empNameHs=new HashSet<String>();
		if(report.getAssessedPeron1()!=null&&!report.getAssessedPeron1().equals("")){
			empNameHs.add(report.getAssessedPeron1());
		}
		if(report.getAssessedPeron2()!=null&&!report.getAssessedPeron2().equals("")){
			empNameHs.add(report.getAssessedPeron2());
		}
		if(report.getAssessedPeron3()!=null&&!report.getAssessedPeron3().equals("")){
			empNameHs.add(report.getAssessedPeron3());
		}
		if(empNameHs.size()!=0){
			List<String> empNameList=new ArrayList<String>(empNameHs);
			logger.log(Level.SEVERE,"Emp Name List size : "+empNameList.size());
			employeeList=ofy().load().type(Employee.class).filter("companyId", report.getCompanyId()).filter("fullname IN", empNameList).list();
			
			if(employeeList!=null){
				logger.log(Level.SEVERE,"employeeList size : "+employeeList.size());
			}
		}

		if (report.getCompanyId() != null) {
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", report.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
						thaiFontFlag=true;
						break;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont;
				BaseFont boldFont;
				regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				
				font16boldul = new Font(boldFont, 17);
				font12bold = new Font(boldFont, 13);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font9 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 13);
				font12 = new Font(regularFont, 13);
				font10bold = new Font(boldFont, 11);
				font14bold = new Font(boldFont, 15);
				font9bold = new Font(boldFont, 8);
				
				font16bold = new Font(boldFont, 16);
				font10 = new Font(regularFont, 10);
				font7bold = new Font(boldFont, 7);
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}	
		
		
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())) {

			logger.log(Level.SEVERE, "Process active --");
			if (report != null && report.getBranch() != null&& report.getBranch().trim().length() > 0) {

				branchDt = ofy().load().type(Branch.class).filter("companyId", report.getCompanyId())
						.filter("buisnessUnitName", report.getBranch()).first().now();

				if (branchDt != null) {
					logger.log(Level.SEVERE,"Process active --" + branchDt.getPaymentMode());
					comp = ServerAppUtility.changeBranchASCompany(branchDt,comp);
				}
			}
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","OnlyForPestOCop", report.getCompanyId())){
			onlyForPestOCop=true;
		}
	}

	public void createPdf(String preprintStatus) {
		
		if(preprintStatus.equals("plane")){
			createCompanyHeadding();
		}
		else{
			if(preprintStatus.equals("no")){
				if (comp.getUploadHeader() != null) {
					pdfUtility.createCompanyNameAsHeader(document , comp);
					createBlankforUPC();
				}
			}else{
				createBlankforUPC();

			}	
		}
		

		createCustomerDetails();
		createAccessedAndAccompaniedByDetails();
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","OnlyForOrion", report.getCompanyId())){
			createProductDetailsNew(0,false);
			if(noOfLines==0&&index!=0){
				
				noOfLines=12;
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				createProductDetailsNew(index,true);
			}
			
		}
		else {
			createProductDetails(0,false);
			if(noOfLines==0&&index!=0){
				
				noOfLines=12;
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				createProductDetails(index,true);
			}
			
		}
		
		createuploadedphotos();
		
		
		
		if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", AppConstants.PC_ENABLEDONOTPRINTOBESERVATIONSUMMARY, report.getCompanyId())){
			createObservationStatusSummary(report);//Ashwini Patil Date:13-08-2022

		}

		if(report.getCustomerSignature()!=null)
			createCustomerSignature();
		
		
		if(preprintStatus.equals("no")){
			if (comp.getUploadFooter() != null) {
				pdfUtility.createCompanyNameAsFooter(document, comp);
			}
		}
		
		

	}

	private void createuploadedphotos() {
		logger.log(Level.SEVERE, "report.getImage1() "+report.getImage1());
		logger.log(Level.SEVERE, "report.getImage2() "+report.getImage2());
		logger.log(Level.SEVERE, "report.getImage3() "+report.getImage3());
		logger.log(Level.SEVERE, "report.getImage4() "+report.getImage4());
		logger.log(Level.SEVERE, "report.getImage5() "+report.getImage5());
		logger.log(Level.SEVERE, "report.getImage6() "+report.getImage6());
		logger.log(Level.SEVERE, "report.getImage7() "+report.getImage7());
		logger.log(Level.SEVERE, "report.getImage8() "+report.getImage8());
		logger.log(Level.SEVERE, "report.getImage9() "+report.getImage9());
		logger.log(Level.SEVERE, "report.getImage10() "+report.getImage10());

		if(report.getImage1()!=null || report.getImage2()!=null || report.getImage3()!=null || report.getImage4()!=null ||
		   report.getImage5()!=null	|| report.getImage6()!=null || report.getImage7()!=null || report.getImage8()!=null || 
		   report.getImage9()!=null || report.getImage10()!=null){
			
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			parent.addCell(pdfUtility.getCell("Assessment Images :-", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			parent.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			parent.setSpacingAfter(5f);
			
			try {
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(100f);
			float[] tblwidth = {2f,2f,2f};
			try {
				table.setWidths(tblwidth);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage1(), 100f, 120f, 0, 0,0)).setBorder(0);				
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage2(), 100f, 120f, 0, 0,0)).setBorder(0);
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage3(), 100f, 120f, 0, 0,0)).setBorder(0);
				if(report.getImage1()!=null&&report.getImage1().getUrl()!=null&&!report.getImage1().getUrl().equals("")&&report.getImage1label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage1label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,0,-1,-1,-1));
				if(report.getImage2()!=null&&report.getImage2().getUrl()!=null&&!report.getImage2().getUrl().equals("")&&report.getImage2label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage2label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				if(report.getImage3()!=null&&report.getImage3().getUrl()!=null&&!report.getImage3().getUrl().equals("")&&report.getImage3label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage3label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				
				table.addCell(pdfUtility.getPdfCell(" ", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0,3, 0,0,0,0,0,-1,-1)).setPaddingTop(10);
				
				
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage4(), 100f, 120f, 0, 0,0)).setBorder(0);
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage5(), 100f, 120f, 0, 0,0)).setBorder(0);
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage6(), 100f, 120f, 0, 0,0)).setBorder(0);
				
				if(report.getImage4()!=null&&report.getImage4().getUrl()!=null&&!report.getImage4().getUrl().equals("")&&report.getImage4label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage4label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				if(report.getImage5()!=null&&report.getImage5().getUrl()!=null&&!report.getImage5().getUrl().equals("")&&report.getImage5label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage5label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				if(report.getImage6()!=null&&report.getImage6().getUrl()!=null&&!report.getImage6().getUrl().equals("")&&report.getImage6label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage6label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				
				table.addCell(pdfUtility.getPdfCell(" ", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0,3, 0,0,0,0,0,-1,-1)).setPaddingTop(10);
				
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage7(), 100f, 120f, 0, 0,0)).setBorder(0);
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage8(), 100f, 120f, 0, 0,0)).setBorder(0);
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage9(), 100f, 120f, 0, 0,0)).setBorder(0);
				
				if(report.getImage7()!=null&&report.getImage7().getUrl()!=null&&!report.getImage7().getUrl().equals("")&&report.getImage7label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage7label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				if(report.getImage8()!=null&&report.getImage8().getUrl()!=null&&!report.getImage8().getUrl().equals("")&&report.getImage8label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage8label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				if(report.getImage9()!=null&&report.getImage9().getUrl()!=null&&!report.getImage9().getUrl().equals("")&&report.getImage9label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage9label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				
				table.addCell(pdfUtility.getPdfCell(" ", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0,3, 0,0,0,0,0,-1,-1)).setPaddingTop(10);
				
				table.addCell(pdfUtility.getAbsolutePhotoCell(report.getImage10(), 100f, 120f, 0, 0,0)).setBorder(0);
				
				if(report.getImage10()!=null&&report.getImage10().getUrl()!=null&&!report.getImage10().getUrl().equals("")&&report.getImage10label()!=null)
					table.addCell(pdfUtility.getPdfCell(report.getImage10label(), font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				else
					table.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
				
			
			try {
				document.add(table);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
	}

	private void createProductDetails(int j,boolean recursiveFlag) {
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100f);
//		float[] tblcol6width = {2f,10f,9f,10f,9f,10f,25f,25f};
		float[] tblcol6width = {12f,9f,10f,9f,10f,25f,25f};
		boolean isDonotprintstatuscreationDate=false;
		boolean isPrintPdfHeaderRepetative=false;
		if(report.isDonotprintstatuscreationDate()||onlyForPestOCop)
			isDonotprintstatuscreationDate=true;
		
		if(report.isPrintPdfHeaderRepetative()||onlyForPestOCop)
			isPrintPdfHeaderRepetative=true;
		
		try {
			table.setWidths(tblcol6width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		int statuscolsSpan = 1;
		if(isDonotprintstatuscreationDate){
			++statuscolsSpan;
		}
		boolean headerColumnRepetativeFlag = false;
		if(isPrintPdfHeaderRepetative){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", "COMPANYASLETTERHEAD", report.getCompanyId())) {
				noOfLines = 5;
			}
			headerColumnRepetativeFlag = true;
			if(isDonotprintstatuscreationDate){
//				++statuscolsSpan;
			}
		}
		else{
			table.addCell(pdfUtility.getCell("Image", font8bold, Element.ALIGN_CENTER, 0, statuscolsSpan, 0));//1-2
			if(isDonotprintstatuscreationDate){
//				++statuscolsSpan;
			}else {
				table.addCell(pdfUtility.getCell("Status / Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0));//3
			}
			table.addCell(pdfUtility.getCell("Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			table.addCell(pdfUtility.getCell("Risk Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			table.addCell(pdfUtility.getCell("Category", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			table.addCell(pdfUtility.getCell("Observation", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			table.addCell(pdfUtility.getCell("Impact", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		
		index=0;
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		
		if(report.getAssessmentDetailsLIst()!=null&&report.getAssessmentDetailsLIst().size()!=0){
			for(int i=j;i<report.getAssessmentDetailsLIst().size();i++){
				
//				int colsSpan = 1;
				if(headerColumnRepetativeFlag){
					
					table.addCell(pdfUtility.getCell("Image", font8bold, Element.ALIGN_CENTER, 0, statuscolsSpan, 0));//1-2
					if(isDonotprintstatuscreationDate){
//						++colsSpan;
					}
					else{
						table.addCell(pdfUtility.getCell("Status / Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0));//3
					}
					table.addCell(pdfUtility.getCell("Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					table.addCell(pdfUtility.getCell("Risk Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					table.addCell(pdfUtility.getCell("Category", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					
					table.addCell(pdfUtility.getCell("Observation", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					table.addCell(pdfUtility.getCell("Impact", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
				}
				AssesmentReportEmbedForTable assesment=report.getAssessmentDetailsLIst().get(i);
				
				if(noOfLines<=0){
					index=i;
					break;
				}
				noOfLines--;
				
				String area="";
				String riskLevel="";
				String category="";
				String location="";
				String impact="";
				String action="Action Plan By "+comp.getBusinessUnitName()+":-";
				String recommendation="Recommendation To Customer:-";
				
				
				Phrase actionPh=new Phrase(action,font8bold);
				Phrase recommendationPh=new Phrase(recommendation,font8bold);
				
				Paragraph actionPara=new Paragraph();
				actionPara.add(actionPh);
				actionPara.setAlignment(Element.ALIGN_LEFT);
				
				Paragraph recommendationPara=new Paragraph();
				recommendationPara.add(recommendationPh);
				recommendationPara.setAlignment(Element.ALIGN_LEFT);
				
				int height =35;
				int height1=35;
				
				if(assesment.getArea()!=null){
					area=assesment.getArea();
				}
				if(assesment.getRiskLevel()!=null){
					riskLevel=assesment.getRiskLevel();
				}
				if(assesment.getCategory()!=null){
					category=assesment.getCategory();
				}
				if(assesment.getLocation()!=null){
					location=assesment.getLocation();
					if(assesment.getLocation().length()>100){
						height1=0;
					}
				}
				if(assesment.getConsequences()!=null){
					impact=assesment.getConsequences();
					if(assesment.getConsequences().length()>100){
						height1=0;
					}
				}
				
				if(assesment.getActionPlanForCompany()!=null){
					Phrase actionPh1=new Phrase("\n"+assesment.getActionPlanForCompany(),font8);
					actionPara.add(actionPh1);
					actionPara.setAlignment(Element.ALIGN_LEFT);
					
					if(assesment.getActionPlanForCompany().length()>=145){
						height=0;
					}
				}
				
				if(assesment.getActionPlanForCustomer()!=null){
					Phrase recommendationPh1=new Phrase("\n"+assesment.getActionPlanForCustomer(),font8);
					recommendationPara.add(recommendationPh1);
					recommendationPara.setAlignment(Element.ALIGN_LEFT);
					
					if(assesment.getActionPlanForCustomer().length()>=145){
						height=0;
					}
				}
				
				PdfPCell actionCell=new PdfPCell(actionPara);
				actionCell.setFixedHeight(height1);
				if(headerColumnRepetativeFlag){
						if(isDonotprintstatuscreationDate){
							actionCell.setColspan(3);
						}
						else{
							actionCell.setColspan(4);
						}

//						actionCell.setColspan(4);
				}
				else{
					actionCell.setColspan(5);
				}

				
				PdfPCell recommentationCell=new PdfPCell(recommendationPara);
				recommentationCell.setFixedHeight(height1);
				recommentationCell.setColspan(2);
				
				String taskStatus="";
				if(assesment.getStatus()!=null){					
					SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					SimpleDateFormat onlyDate = new SimpleDateFormat("dd/MM/yyyy");
					serverTimeSdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					if(assesment.getStatus().equals("Closed")) {
						if(assesment.getClosureDate()!=null)
							taskStatus=assesment.getStatus()+"\n"+onlyDate.format(assesment.getClosureDate())+" "; //Since only date is coming from pedio we are displaying only date
						else
							taskStatus=assesment.getStatus();
					}else {
						if(assesment.getCreationDate()!=null)
							taskStatus=assesment.getStatus()+"\n"+serverTimeSdf.format(assesment.getCreationDate())+" ";
						else
							taskStatus=assesment.getStatus();
					}
				}
				
				if(assesment.getDocUpload()!=null){
					if(!assesment.getDocUpload().getUrl().equals("")){
						if(isPrintPdfHeaderRepetative){
							
							DocumentUpload logodocument = assesment.getDocUpload();
							// patch
							String hostUrl;
							String environment = System.getProperty("com.google.appengine.runtime.environment");
							if (environment.equals("Production")) {
								String applicationId = System.getProperty("com.google.appengine.application.id");
								String version = System.getProperty("com.google.appengine.application.version");
								hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
							} else {
								hostUrl = "http://localhost:8888";
							}
							
							PdfPCell imageSignCell = new PdfPCell();
							Image image2 = null;
							try {
								if(!logodocument.getUrl().contains("https://"))
									image2 = Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
								else
									image2 = Image.getInstance(new URL(logodocument.getUrl()));
//								image2.scaleAbsolute(60f, 65f);
								image2.scaleAbsolute(100f, 65f);
								imageSignCell = new PdfPCell(image2);
								imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							
								imageSignCell.setPaddingTop(1);
								imageSignCell.setPaddingBottom(1);
								imageSignCell.setPaddingLeft(1);
								imageSignCell.setPaddingRight(1);
								
							} catch (Exception e) {
								e.printStackTrace();
							}

							PdfPTable QutationIdDateTable = new PdfPTable(1);
							QutationIdDateTable.setWidthPercentage(100);
							QutationIdDateTable.addCell(imageSignCell);
							
							PdfPCell totalamountCell = new PdfPCell(QutationIdDateTable);
							totalamountCell.setRowspan(2);
							totalamountCell.setColspan(statuscolsSpan);
							table.addCell(totalamountCell);
						}
						else{
							table.addCell(pdfUtility.getPhotoCell(assesment.getDocUpload(), 20f, height1, 0, statuscolsSpan,0));
						}

					}
					else
						table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, height1));
				}else{
					table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, height1));
				}
				if(!isDonotprintstatuscreationDate){
					table.addCell(pdfUtility.getCell(taskStatus, font8, Element.ALIGN_LEFT, 0, 0, height1));
				}
				table.addCell(pdfUtility.getCell(area, font8, Element.ALIGN_LEFT, 0, 0, height1));
				table.addCell(pdfUtility.getCell(riskLevel, font8, Element.ALIGN_LEFT, 0, 0, height1));
				table.addCell(pdfUtility.getCell(category, font8, Element.ALIGN_LEFT, 0, 0, height1));
				table.addCell(pdfUtility.getCell(location, font8, Element.ALIGN_LEFT, 0, 0, height1));
				table.addCell(pdfUtility.getCell(impact, font8, Element.ALIGN_LEFT, 0, 0, height1));
				
				table.addCell(actionCell);
				table.addCell(recommentationCell);

				if(headerColumnRepetativeFlag){
					
					try {
						document.add(table);

						PdfPTable parent=new PdfPTable(1);
//						parent.setWidthPercentage(100);
						parent.addCell(pdfUtility.getCell(" ", font6, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//						parent.setSpacingAfter(2f);
						try {
							document.add(parent);
						} catch (DocumentException e) {
							e.printStackTrace();
						}
						
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					table = new PdfPTable(7);
					table.setWidthPercentage(100f);
					float[] tblcol6width2 = {12f,9f,10f,9f,10f,25f,25f};
					try {
						table.setWidths(tblcol6width2);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				}
				
				
			}
		}

		if(!headerColumnRepetativeFlag){
			try {
				document.add(table);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		
		
		if(recursiveFlag&&noOfLines==0&&index!=0){
			noOfLines=12;
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			createProductDetails(index,true);
		}
		
	}
	

	private void createAccessedAndAccompaniedByDetails() {
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(5f);
		float[] tblcol1width = { 19f,3f,28f,28f,28f };
		try {
			table.setWidths(tblcol1width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		
		
		Paragraph accessPara=new Paragraph();
		Paragraph accompPara=new Paragraph();
		
		/**
		 * @author Vijay Date :- 28-06-2023
		 * Des :- Added for orion
		 */
		
		boolean donotprintAssessedByserialnoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", AppConstants.PC_ENABLEDONOTPRINTASSESSEDBYSERIALNUMBER, report.getCompanyId());
	
		/**
		 * ends here
		 */
		
		int ctr=0;
		if(report.getAssessedPeron1()!=null&&!report.getAssessedPeron1().equals("")){
			ctr++;
			if(donotprintAssessedByserialnoFlag){
				Phrase accessPh2=new Phrase(getEmployeeDetails(report.getAssessedPeron1(),false),font9);
				accessPara.add(accessPh2);
			}
			else{
				
				Phrase accessPh1=new Phrase(ctr+".",font9bold);
				accessPara.add(accessPh1);
				accessPara.setAlignment(Element.ALIGN_LEFT);
				Phrase accessPh2=new Phrase(getEmployeeDetails(report.getAssessedPeron1(),false),font9);
				accessPara.add(accessPh2);
			}
			
			
		}
		if(report.getAssessedPeron2()!=null&&!report.getAssessedPeron2().equals("")){
			ctr++;	
			if(donotprintAssessedByserialnoFlag){
				Phrase accessPh2=new Phrase(getEmployeeDetails(report.getAssessedPeron2(),false),font9);
				accessPara.add(accessPh2);
			}
			else{
				
				Phrase accessPh1=new Phrase(ctr+".",font9bold);
				accessPara.add(accessPh1);
				accessPara.setAlignment(Element.ALIGN_LEFT);
				Phrase accessPh2=new Phrase(getEmployeeDetails(report.getAssessedPeron2(),false),font9);
				accessPara.add(accessPh2);
			}
			
		}
		if(report.getAssessedPeron3()!=null&&!report.getAssessedPeron3().equals("")){
			ctr++;
			
			if(donotprintAssessedByserialnoFlag){
				Phrase accessPh2=new Phrase(getEmployeeDetails(report.getAssessedPeron3(),false),font9);
				accessPara.add(accessPh2);
			}
			else{
				
				Phrase accessPh1=new Phrase(ctr+".",font9bold);
				accessPara.add(accessPh1);
				accessPara.setAlignment(Element.ALIGN_LEFT);
				Phrase accessPh2=new Phrase(getEmployeeDetails(report.getAssessedPeron3(),false),font9);
				accessPara.add(accessPh2);
			}

			
		}
		
		if(ctr>0) {
			table.addCell(pdfUtility.getCell("Assessed By", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(":", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);			
			PdfPCell accessCell=new PdfPCell(accessPara);
			accessCell.setColspan(3);
			accessCell.setBorder(0);
			table.addCell(accessCell);
		}
		
		
		int ctr1=0;
		if(report.getAccompainedByPerson1()!=null&&!report.getAccompainedByPerson1().equals("")){
			ctr1++;
			Phrase accompPh1=new Phrase(ctr1+".",font9bold);
			accompPara.add(accompPh1);
			accompPara.setAlignment(Element.ALIGN_LEFT);
			Phrase accompPh2=new Phrase(getEmployeeDetails(report.getAccompainedByPerson1(),true),font9);
			accompPara.add(accompPh2);
			
		}
		if(report.getAccompainedByPerson2()!=null&&!report.getAccompainedByPerson2().equals("")){
			ctr1++;	
			Phrase accompPh1=new Phrase(ctr1+".",font9bold);
			accompPara.add(accompPh1);
			accompPara.setAlignment(Element.ALIGN_LEFT);
			Phrase accompPh2=new Phrase(getEmployeeDetails(report.getAccompainedByPerson2(),true),font9);
			accompPara.add(accompPh2);
		}
		if(report.getAccompainedByPerson3()!=null&&!report.getAccompainedByPerson3().equals("")){
			ctr1++;
			Phrase accompPh1=new Phrase(ctr1+".",font9bold);
			accompPara.add(accompPh1);
			accompPara.setAlignment(Element.ALIGN_LEFT);
			Phrase accompPh2=new Phrase(getEmployeeDetails(report.getAccompainedByPerson3(),true),font9);
			accompPara.add(accompPh2);
		}
		if(ctr1>0) {
			table.addCell(pdfUtility.getCell("Accompanied By", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(":", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			PdfPCell accompCell=new PdfPCell(accompPara);
			accompCell.setColspan(3);
			accompCell.setBorder(0);
			table.addCell(accompCell);			
		}
		
		
		table.setSpacingAfter(10f);
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private String getEmployeeDetails(String employee, boolean b) {
		logger.log(Level.SEVERE,"getEmployeeDetails "+employee+" Form MAster "+b+" size "+employeeList.size());
		
		boolean donotprintAssessedByCellNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", AppConstants.PC_ENABLEDONOTPRINTASSESSEDBYCELLNUMBER, report.getCompanyId());
//		boolean donotprintAccomplishByCellNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", AppConstants.PC_ENABLEDONOTPRINTACCOMPAINEDBYCELLNUMBER, report.getCompanyId());
		boolean donotprintEmailFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", AppConstants.PC_ENABLEDONOTPRINTEMAIL, report.getCompanyId());

		
		String empName=employee+" ";
		if(b){
			return empName+"  ";	
		}
		
		if(employeeList!=null&&employeeList.size()!=0){
			boolean cellFlag=false;
			boolean emailFlag=false;
			for(Employee emp:employeeList){
				
				logger.log(Level.SEVERE,"Emp Name "+employee+" / "+emp.getFullName()+" / "+emp.getFullname());
				if(emp.getFullname().equals(employee.trim())){
					cellFlag=false;
					emailFlag=false;
					if(!donotprintAssessedByCellNoFlag ){

					if(emp.getCellNumber1()!=null&&emp.getCellNumber1()!=0){
						empName=empName+"("+emp.getCellNumber1();
						cellFlag=true;
					}
					
					}
					if(!donotprintEmailFlag){
						if(emp.getEmail()!=null&&!emp.getEmail().equals("")){
							if(cellFlag){
								empName=empName+" / "+emp.getEmail()+")  ";
							}else{
								empName=empName+"("+emp.getEmail()+")   ";
							}
							emailFlag=true;
						}
					}
					
					
					if(cellFlag&&!emailFlag){
						empName=empName+")   ";
					}
					
					return empName;
				}
			}
			
			if(!cellFlag&&!emailFlag){
				empName=empName+"  ";
			}
		}
		return empName;
	}

	
	private void createCustomerDetails() {
		String attnName = "";
		if (customerBranch != null&&customerBranch.getPocName()!=null&&!customerBranch.getPocName().equals("")) {
			attnName = customerBranch.getPocName();
		} else {
			attnName = cust.getFullname();
		}
		
		if (customerBranch != null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0) {
			attnName = attnName+"("+ customerBranch.getCellNumber1()+")";
		} else {
			attnName = attnName+"("+ cust.getCellNumber1()+")";
		}
		
		if(report.getPrintAttn()!=null && !report.getPrintAttn().equals("")){
			attnName = report.getPrintAttn();
		}
		
		Phrase attnPh = new Phrase("Attn",font10bold);
		PdfPCell attenCell = new PdfPCell(attnPh);
		attenCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		attenCell.setBorder(0);
		
		Phrase custInfo = null ;
		custInfo = new Phrase("Customer", font10bold);
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		String custAddress="";
		if(customerBranch!=null&&customerBranch.getAddress()!=null){
			custAddress=customerBranch.getAddress().getCompleteAddress().trim();
			logger.log(Level.SEVERE,"billing address of customer branch " +custAddress);
		}
		else{
			custAddress = cust.getAdress().getCompleteAddress().trim();
			logger.log(Level.SEVERE,"billing address of customer " +custAddress);
		}
		
		Phrase custAddInfo = new Phrase("Address", font10bold);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		PdfPTable customerTable=new PdfPTable(3);
		customerTable.setWidthPercentage(100);

		float[] colWidth = { 35f,3f,62f };
		try {
			customerTable.setWidths(colWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		customerTable.addCell(custInfoCell);
		customerTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		customerTable.addCell(pdfUtility.getCell(cust.getCompanyName(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		customerTable.addCell(attenCell);
		customerTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		customerTable.addCell(pdfUtility.getCell(attnName, font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(customerBranch!=null){
			customerTable.addCell(pdfUtility.getCell("Service Location", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			customerTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			customerTable.addCell(pdfUtility.getCell(customerBranch.getBusinessUnitName(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		customerTable.addCell(custAddInfoCell);
		customerTable.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		customerTable.addCell(pdfUtility.getCell(custAddress, font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(customerTable);
		customerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerCell.setBorder(0);
		
		PdfPTable rightTbl=new PdfPTable(3);
		rightTbl.setWidthPercentage(100);
		
		float[] colWidth1 = { 40f,3f,57f };
		try {
			rightTbl.setWidths(colWidth1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		rightTbl.addCell(pdfUtility.getCell("Assessment Id", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(report.getCount()+"", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		rightTbl.addCell(pdfUtility.getCell("Assessment Date", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(fmt.format(report.getAssessmentDate()), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		//Ashwini Patil Date:28-11-2024 Pest o cop do not want to print status, creation date and closure date
		if(!onlyForPestOCop) {
			
			/**
			 * @author Anil @since 15-12-2021
			 * Added assessment status below assessment date and removed pest management from heading
			 * raised by Nitin Sir for Prominant
			 */
			rightTbl.addCell(pdfUtility.getCell("Assessment Status", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(pdfUtility.getCell(report.getStatus(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			
			
			if(report.getStatus().equals("Closed")&&report.getSubmissionDate()!=null) {	
				SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
				serverTimeSdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				rightTbl.addCell(pdfUtility.getCell("Closure Date", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightTbl.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightTbl.addCell(pdfUtility.getCell(serverTimeSdf.format(report.getSubmissionDate())+"", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);			
			}
			
			if(report.getAssessmentDetailsLIst()!=null && report.getAssessmentDetailsLIst().size()>0 && report.getAssessmentDetailsLIst().get(0).getCreationDate()!=null){
				rightTbl.addCell(pdfUtility.getCell("Assessment Creation Date", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightTbl.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightTbl.addCell(pdfUtility.getCell(fmt.format(report.getAssessmentDetailsLIst().get(0).getCreationDate())+"", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
		
		
		}
		
		PdfPCell rightCell = new PdfPCell();
		rightCell.addElement(rightTbl);
		rightCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		rightCell.setBorder(0);
		
		PdfPTable headTbl=new PdfPTable(2);
		headTbl.setWidthPercentage(100f);
		
		float[] colWidth2 = { 50f,50f };
		try {
			headTbl.setWidths(colWidth2);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		headTbl.addCell(customerCell);
		headTbl.addCell(rightCell);
		
		PdfPCell parentCell = new PdfPCell();
		parentCell.addElement(headTbl);
		parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		parentCell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(pdfUtility.getCell("Assessment Report", font12bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		parent.addCell(pdfUtility.getCell(" ", font12bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		parent.addCell(parentCell);
		parent.setSpacingAfter(5f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
	}

	private void createBlankforUPC() {
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	

	/**
	 * @author Vijay Chougule Des :- NBHC CCPM to send an email
	 */
	public void createPDFForEmail(long count) {
		setAssessmentDetails(count,null);
		createPdf("no");
	}
	
	public void createCompanyHeadding() {
		
		
		DocumentUpload logodocument = null;
		if(comp.getLogo()!=null){
			logodocument = comp.getLogo();
		}

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);
			 
			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
			
			
			
			 
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		
		String companyname ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname=" "+  branchDt.getCorrespondenceName();
		}else{
			companyname = " "+ comp.getBusinessUnitName().trim().toUpperCase();
		}

		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font12bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);
		

		
		String custAdd1="";
		String custFullAdd1="";
		
		if(comp.getAddress()!=null){
			
			if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
			
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
				}
			}else{
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
					custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1();
				}
			}
			
			if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getPin()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}else{
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getPin()+","+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}
			
		}	
		
			Phrase addressline=new Phrase(custFullAdd1,font10);
	
			Paragraph addresspara=new Paragraph();
			addresspara.add(addressline);
			addresspara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell addresscell=new PdfPCell();
			addresscell.addElement(addresspara);
			addresscell.setBorder(0);
		   
		
		
		String contactinfo="";
		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			contactinfo =  "Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim();
		}
		//Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim(),font10);
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setFont(font10);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(addresscell);
//		table.addCell(localitycell);
		table.addCell(contactcell);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(10f);
		
		PdfPCell cell = new PdfPCell();
		cell.addElement(table);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);
		
	
		PdfPTable mainheader = new PdfPTable(2);
		mainheader.setWidthPercentage(100);

		try {
			mainheader.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		if (imageSignCell != null) {
			PdfPCell leftCell = new PdfPCell(logoTab);
			leftCell.setBorder(0);
			mainheader.addCell(leftCell);

			PdfPCell rightCell = new PdfPCell(parenttable);
			rightCell.setBorder(0);
			mainheader.addCell(rightCell);
		} else {
			PdfPCell rightCell = new PdfPCell(parenttable);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			mainheader.addCell(rightCell);
		}

		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
//	try {
//		document.add(parenttable);
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	
}
	
	private void createObservationStatusSummary(AssesmentReport report) {
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		float[] tblcol3width = {2f,2f,2f};
		try {
			table.setWidths(tblcol3width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		int branches=0;
		int locations=0;
		int areas=0;
		int totalObv=0;
		int openObv=0;
		int closedObv=0;
		
		List<CustomerBranchDetails> custBranches= ofy().load().type(CustomerBranchDetails.class)
				.filter("companyId", report.getCompanyId())
				.filter("cinfo.count", report.getCinfo().getCount()).list();		
		branches=custBranches.size();
		List<Integer> branchIDList=new ArrayList<Integer>();
		
		for(CustomerBranchDetails c:custBranches) {
			branchIDList.add(c.getCount());
		}
		
		//write condition for branch size 0
		List<CustomerBranchServiceLocation> locList=new ArrayList<CustomerBranchServiceLocation>();
		if(branchIDList.size()>0) {
//			logger.log(Level.SEVERE,"branchIDList size="+branchIDList.size());
			
		locList=ofy().load().type(CustomerBranchServiceLocation.class)
				.filter("companyId", custBranches.get(0).getCompanyId())
				.filter("custBranchId IN",branchIDList).list(); 
		}
		if(locList!=null) {
//			logger.log(Level.SEVERE,"locList size="+locList.size());
			
			locations=locList.size();
		
		for(CustomerBranchServiceLocation loc:locList) {
//			logger.log(Level.SEVERE,"loc="+loc.getServiceLocation());
			
			if(loc.getAreaList()!=null) { //condition added on 14-09-2022
				areas+=loc.getAreaList().size();
			}
			if(loc.getAreaList2()!=null) { //condition added on 16-12-2024
				areas+=loc.getAreaList2().size();
			}
		}
		}
		if(report.getAssessmentDetailsLIst()!=null) {			
			totalObv=report.getAssessmentDetailsLIst().size();
			for(AssesmentReportEmbedForTable ar:report.getAssessmentDetailsLIst()) {
				if(ar.getStatus().equals("Open")||ar.getStatus().equals("Created"))
					openObv++;
				if(ar.getStatus().equals("Closed"))
					closedObv++;
			}
		}
		
		
		table.addCell(pdfUtility.getCell("Observation status summary", font10bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(Rectangle.BOX);
		
		table.addCell(pdfUtility.getCell("Customer Branches", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell("Service Locations", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell("Areas", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell(branches+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell(locations+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell(areas+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		
		table.addCell(pdfUtility.getCell("Total Observations", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell("Open Observations", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell("Closed Observations", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell(totalObv+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell(openObv+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		table.addCell(pdfUtility.getCell(closedObv+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		
		

			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void createCustomerSignature(){
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);

		PdfPCell imageCell=pdfUtility.getPhotoCell(report.getCustomerSignature(), 25f, 45f, 0, 0,0);
		imageCell.setBorder(0);
		imageCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		imageCell.setVerticalAlignment(Element.ALIGN_CENTER);
		table.addCell(imageCell);
		PdfPCell labelcell=pdfUtility.getPdfCell("Customer Signature", font8,Element.ALIGN_RIGHT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		labelcell.setPaddingRight(15f);
		table.addCell(labelcell);


		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createProductDetailsNew(int j,boolean recursiveFlag) {
		logger.log(Level.SEVERE,"createProductDetailsNew called index="+j+" recursiveFlag="+recursiveFlag);
		
		int statuscolsSpan = 1;
		if(report.isDonotprintstatuscreationDate()){
			++statuscolsSpan;
		}
		boolean headerColumnRepetativeFlag = false;
		if(report.isPrintPdfHeaderRepetative()){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport", "COMPANYASLETTERHEAD", report.getCompanyId())) {
				noOfLines = 5;
			}
			headerColumnRepetativeFlag = true;
		
		}
//		else{
//			table.addCell(pdfUtility.getCell("Image", font8bold, Element.ALIGN_CENTER, 0, statuscolsSpan, 0));//1-2
//			if(report.isDonotprintstatuscreationDate()){
////				++statuscolsSpan;
//			}else {
//				table.addCell(pdfUtility.getCell("Status / Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0));//3
//			}
//			
//			PdfPTable ARCtable = new PdfPTable(3);
//			ARCtable.setWidthPercentage(100f);
//			float[] tblcol3width = {2f,2f,2f};
//
//			try {
//				ARCtable.setWidths(tblcol3width);
//			} catch (DocumentException e1) {
//				e1.printStackTrace();
//			}
//			
//			ARCtable.addCell(pdfUtility.getCell("Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//			ARCtable.addCell(pdfUtility.getCell("Risk Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//			ARCtable.addCell(pdfUtility.getCell("Category", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//			PdfPCell ARCcell = new PdfPCell(ARCtable);
//			ARCcell.setColspan(2);
//			table.addCell(ARCcell);
//			
////			table.addCell(pdfUtility.getCell("Observation", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
////			table.addCell(pdfUtility.getCell("Impact", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//		}
		
		index=0;
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		
		if(report.getAssessmentDetailsLIst()!=null&&report.getAssessmentDetailsLIst().size()!=0){
			for(int i=j;i<report.getAssessmentDetailsLIst().size();i++){
				
				logger.log(Level.SEVERE,"in for i="+i);
				PdfPTable table = new PdfPTable(4);
				table.setWidthPercentage(100f);
				float[] tblcol6width = {21f,9f,35f,35f};

				try {
					table.setWidths(tblcol6width);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
//				int colsSpan = 1;
				if(headerColumnRepetativeFlag){
					
					table.addCell(pdfUtility.getCell("Image", font8bold, Element.ALIGN_CENTER, 0, statuscolsSpan, 0));//1-2
					if(report.isDonotprintstatuscreationDate()){
//						++colsSpan;
					}
					else{
						table.addCell(pdfUtility.getCell("Status / Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0));//3
					}
//					table.addCell(pdfUtility.getCell("Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//					table.addCell(pdfUtility.getCell("Risk Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//					table.addCell(pdfUtility.getCell("Category", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					PdfPTable ARCtable = new PdfPTable(3);
					ARCtable.setWidthPercentage(100f);
					float[] tblcol3width = {2f,2f,2f};

					try {
						ARCtable.setWidths(tblcol3width);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
					
					ARCtable.addCell(pdfUtility.getCell("Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					ARCtable.addCell(pdfUtility.getCell("Risk Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					ARCtable.addCell(pdfUtility.getCell("Category", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
					PdfPCell ARCcell = new PdfPCell(ARCtable);
					ARCcell.setColspan(2);
					table.addCell(ARCcell);
					
//					table.addCell(pdfUtility.getCell("Observation", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
//					table.addCell(pdfUtility.getCell("Impact", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
				}
				AssesmentReportEmbedForTable assesment=report.getAssessmentDetailsLIst().get(i);
				
				
				if(noOfLines<=0){
					index=i;
					logger.log(Level.SEVERE,"setting index to "+i+" and breaking");
					break;
				}
				noOfLines--;
				
				String area="";
				String riskLevel="";
				String category="";
				String location="";
				String impact="";
				String action="Action Plan By "+comp.getBusinessUnitName()+":-";
				String recommendation="Recommendation To Customer:-";
				
				
				Phrase actionPh=new Phrase(action,font8bold);
				Phrase recommendationPh=new Phrase(recommendation,font8bold);
				
				Paragraph actionPara=new Paragraph();
				actionPara.add(actionPh);
				actionPara.setAlignment(Element.ALIGN_LEFT);
				
				Paragraph recommendationPara=new Paragraph();
				recommendationPara.add(recommendationPh);
				recommendationPara.setAlignment(Element.ALIGN_LEFT);
				
				int height =35;
				int height1=35;
				
				if(assesment.getArea()!=null){
					area=assesment.getArea();
				}
				if(assesment.getRiskLevel()!=null){
					riskLevel=assesment.getRiskLevel();
				}
				if(assesment.getCategory()!=null){
					category=assesment.getCategory();
				}
				if(assesment.getLocation()!=null){
					location=assesment.getLocation();
					if(assesment.getLocation().length()>100){
						height1=0;
					}
				}
				if(assesment.getConsequences()!=null){
					impact=assesment.getConsequences();
					if(assesment.getConsequences().length()>100){
						height1=0;
					}
				}
				
				if(assesment.getActionPlanForCompany()!=null){
					Phrase actionPh1=new Phrase("\n"+assesment.getActionPlanForCompany(),font8);
					actionPara.add(actionPh1);
					actionPara.setAlignment(Element.ALIGN_LEFT);
					
					if(assesment.getActionPlanForCompany().length()>=145){
						height=0;
					}
				}
				
				if(assesment.getActionPlanForCustomer()!=null){
					Phrase recommendationPh1=new Phrase("\n"+assesment.getActionPlanForCustomer(),font8);
					recommendationPara.add(recommendationPh1);
					recommendationPara.setAlignment(Element.ALIGN_LEFT);
					
					if(assesment.getActionPlanForCustomer().length()>=145){
						height=0;
					}
				}
				
				PdfPCell actionCell=new PdfPCell(actionPara);
				actionCell.setFixedHeight(height1);
				if(headerColumnRepetativeFlag){
						if(report.isDonotprintstatuscreationDate()){
//							actionCell.setColspan(3);
						}
						else{
							actionCell.setColspan(2);
						}

//						actionCell.setColspan(4);
				}
				else{
//					actionCell.setColspan(5);
				}

				
				PdfPCell recommentationCell=new PdfPCell(recommendationPara);
				recommentationCell.setFixedHeight(height1);
//				recommentationCell.setColspan(2);
				
				String taskStatus="";
				if(assesment.getStatus()!=null){					
					SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
					SimpleDateFormat onlyDate = new SimpleDateFormat("dd/MM/yyyy");
					serverTimeSdf.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					if(assesment.getStatus().equals("Closed")) {
						if(assesment.getClosureDate()!=null)
							taskStatus=assesment.getStatus()+"\n"+onlyDate.format(assesment.getClosureDate())+" "; //Since only date is coming from pedio we are displaying only date
						else
							taskStatus=assesment.getStatus();
					}else {
						if(assesment.getCreationDate()!=null)
							taskStatus=assesment.getStatus()+"\n"+serverTimeSdf.format(assesment.getCreationDate())+" ";
						else
							taskStatus=assesment.getStatus();
					}
				}
				
				if(assesment.getDocUpload()!=null){
					if(!assesment.getDocUpload().getUrl().equals("")){
						if(report.isPrintPdfHeaderRepetative()){
							
							DocumentUpload logodocument = assesment.getDocUpload();
							// patch
							String hostUrl;
							String environment = System.getProperty("com.google.appengine.runtime.environment");
							if (environment.equals("Production")) {
								String applicationId = System.getProperty("com.google.appengine.application.id");
								String version = System.getProperty("com.google.appengine.application.version");
								hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
							} else {
								hostUrl = "http://localhost:8888";
							}
							
							PdfPCell imageSignCell = new PdfPCell();
							Image image2 = null;
							try {
								//21-01-2024
								if(!logodocument.getUrl().contains("https://"))
									image2 = Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
								else
									image2 = Image.getInstance(new URL(logodocument.getUrl()));
							
//								image2.scaleAbsolute(60f, 65f);
								image2.scaleAbsolute(150f, 160f);
								imageSignCell = new PdfPCell(image2);
								imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							
								imageSignCell.setPaddingTop(1);
								imageSignCell.setPaddingBottom(1);
								imageSignCell.setPaddingLeft(1);
								imageSignCell.setPaddingRight(1);
								
							} catch (Exception e) {
								e.printStackTrace();
							}

							PdfPTable QutationIdDateTable = new PdfPTable(1);
							QutationIdDateTable.setWidthPercentage(100);
							QutationIdDateTable.addCell(imageSignCell);
							
							PdfPCell totalamountCell = new PdfPCell(QutationIdDateTable);
							totalamountCell.setRowspan(5);
							totalamountCell.setColspan(statuscolsSpan);
							table.addCell(totalamountCell);
							logger.log(Level.SEVERE,"image added statuscolsSpan="+statuscolsSpan);
						}
						else{
//							table.addCell(pdfUtility.getPhotoCell(assesment.getDocUpload(), 20f, 40, 5, statuscolsSpan,0));//height1 13-08-2024
						}

					}
					else
						table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 5, 0, height1));
				}else{
					table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT,5, 0, height1));
				}
				if(report.isDonotprintstatuscreationDate()==false){
					table.addCell(pdfUtility.getCell(taskStatus, font8, Element.ALIGN_LEFT, 0, 0, height1));
				}
//				table.addCell(pdfUtility.getCell(area, font8, Element.ALIGN_LEFT, 0, 0, height1));
//				table.addCell(pdfUtility.getCell(riskLevel, font8, Element.ALIGN_LEFT, 0, 0, height1));
//				table.addCell(pdfUtility.getCell(category, font8, Element.ALIGN_LEFT, 0, 0, height1));
				
				PdfPTable ARCtable = new PdfPTable(3);
				ARCtable.setWidthPercentage(100f);
				float[] tblcol3width = {2f,2f,2f};

				try {
					ARCtable.setWidths(tblcol3width);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				ARCtable.addCell(pdfUtility.getCell(area, font8, Element.ALIGN_LEFT, 0, 0, height1));
				ARCtable.addCell(pdfUtility.getCell(riskLevel, font8, Element.ALIGN_LEFT, 0, 0, height1));
				ARCtable.addCell(pdfUtility.getCell(category, font8, Element.ALIGN_LEFT, 0, 0, height1));
				
				PdfPCell ARCcell = new PdfPCell(ARCtable);
				ARCcell.setColspan(2);
				table.addCell(ARCcell);
				logger.log(Level.SEVERE,"arc values added");
				if(report.isDonotprintstatuscreationDate()){
					table.addCell(pdfUtility.getCell("Observation", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
				}else {
					table.addCell(pdfUtility.getCell("Observation", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
					
				}
				table.addCell(pdfUtility.getCell("Impact", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
				
				if(report.isDonotprintstatuscreationDate()){
					table.addCell(pdfUtility.getCell(location, font8, Element.ALIGN_LEFT, 0, 0, height1));
				}else {
					table.addCell(pdfUtility.getCell(location, font8, Element.ALIGN_LEFT, 0, 2, height1));
				}
				table.addCell(pdfUtility.getCell(impact, font8, Element.ALIGN_LEFT, 0, 0, height1));
				
				table.addCell(actionCell);
				table.addCell(recommentationCell);

				if(headerColumnRepetativeFlag){
					
					try {
						document.add(table);

						PdfPTable parent=new PdfPTable(1);
//						parent.setWidthPercentage(100);
						parent.addCell(pdfUtility.getCell(" ", font6, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//						parent.setSpacingAfter(2f);
						try {
							document.add(parent);
						} catch (DocumentException e) {
							e.printStackTrace();
						}
						
					} catch (DocumentException e) {
						e.printStackTrace();
					}
//					table = new PdfPTable(7);
//					table.setWidthPercentage(100f);
//					float[] tblcol6width2 = {12f,9f,10f,9f,10f,25f,25f};
//					try {
//						table.setWidths(tblcol6width2);
//					} catch (DocumentException e1) {
//						e1.printStackTrace();
//					}
				}
				
				
			}
		}
		
		logger.log(Level.SEVERE,"end of createProductDetailsNew noOfLines="+noOfLines+" index="+index);
		
		if(recursiveFlag&&noOfLines<=0&&index!=0){
			noOfLines=12;
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			logger.log(Level.SEVERE,"calling createProductDetailsNew again");
			createProductDetailsNew(index,true);
		}
		
	}
	

}
