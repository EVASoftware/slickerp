package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PestleFiveYearsQuotation {


	public Document document;
	Quotation quot;
	Customer cust;
	Company comp;
	Employee employee;
	Lead lead;
	
	private  Font font10underline,font16boldul,font12bold,font8bold,font8,font9bold,font12boldul,font12,font10bold,font10,font14bold,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
	float[] colwidth = { 1.5f, 0.1f, 6.5f, 1.5f, 0.1f, 1.5f };
	float[] colwidthforsubtable = { 1.0f, 0.1f, 7.0f};
	float[] colwidthforempdetails={ 1.0f, 0.1f, 7.0f};
	float[] colwidthQoutServiceDeatails={ 2.0f, 0.1f, 7.0f};
	float[] colwidthforpayterms={ 1.5f, 0.1f, 6.5f};
	
	public PestleFiveYearsQuotation() {
		// TODO Auto-generated constructor stub

		// TODO Auto-generated constructor stub
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font9= new Font(Font.FontFamily.HELVETICA,9);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10bold= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD);
		font10underline= new Font(Font.FontFamily.HELVETICA,10,Font.UNDERLINE);
		
		font10=new Font(Font.FontFamily.HELVETICA,10,Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font9bold= new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
	
	}
	
	public void getPestlefiveQuotation(Long count) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		
		System.out.println("in side setdirectquotation method");
		//Quoatation Loaded
		quot = ofy().load().type(Quotation.class).id(count).now();
		
		
		if (quot.getCompanyId() != null){
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId())
					.filter("companyId", quot.getCompanyId()).first().now();
			
			lead=ofy().load().type(Lead.class).filter("companyId", quot.getCompanyId()).filter("count", quot.getLeadCount()).first().now();
			
		}
			
		
		if (quot.getCompanyId() == null){
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId()).first().now();
			
			lead=ofy().load().type(Lead.class).filter("count", quot.getLeadCount()).first().now();
		}
			

		if (quot.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", quot.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		
		if (quot.getCompanyId() != null)
			employee = ofy().load().type(Employee.class)
					.filter("companyId", quot.getCompanyId()).filter("fullname", quot.getEmployee()).first().now();
		else
			employee = ofy().load().type(Employee.class).filter("fullname", quot.getEmployee()).first().now();
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	
	
	}

	public void createPdf(String PrintStatus) {
		// TODO Auto-generated method stub
		
		if(PrintStatus.contains("yes"))
		{
			System.out.println("in side yes condition");
			createLogo(document,comp);
			createSpcingForHeading();
		}
		else
		{
			System.out.println("in side no condition");
			createSpcingForHeading();
		    if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
			}
						
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
		}
		createTitleHeading();
		createReferenceHeading();
		createCustDetails();
		createstatic();
		createQoutServiceDeatails();
		createEmployeeDetails();
	}
	
	private void createSpcingForHeading() {
		
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

private void createLogo(Document doc, Company comp) {

	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}




	private void createTitleHeading() {
		// TODO Auto-generated method stub

		
		System.out.println("inside create tilte heading");
		
		String title = "Quotation";
		Phrase quotation = new Phrase(title,font16boldul);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase blk = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blk);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);

		try {
			document.add(qtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
	}
	
	private void createReferenceHeading() {
		// TODO Auto-generated method stub

		
		Phrase quotid= new Phrase("Quotation Id ",font10bold);
		PdfPCell quotidstatic = new PdfPCell(quotid);
		quotidstatic.setBorder(0);
		quotidstatic.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase quotiddynamic = null;
		
		if( quot.getCount()!=0){
			quotiddynamic = new Phrase(quot.getCount()+"",font10bold);	
		}
		
		PdfPCell quotiddynamiccell = new PdfPCell(quotiddynamic);
//		quotiddynamiccell.addElement(quotiddynamic);
		quotiddynamiccell.setBorder(0);
		quotiddynamiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":",font10bold);
		PdfPCell colcell = new PdfPCell(colon);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date",font10bold);
		PdfPCell datecellstatic = new PdfPCell(date);
		datecellstatic.setBorder(0);
		datecellstatic.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase dt = null;
		if(quot.getQuotationDate() !=null){
			dt = new Phrase(fmt.format(quot.getQuotationDate()),font10bold);
		}
		System.out.println("Quotation Date :"+fmt.format(quot.getQuotationDate()));
		
		PdfPCell dtcelldynamic = new PdfPCell(dt);
		dtcelldynamic.setBorder(0);
		dtcelldynamic.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase blank = new Phrase(" ");
//		PdfPCell blankcell = new PdfPCell(blank); 
//		blankcell.setBorder(0);
//		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable rdttable = new PdfPTable(6);
		rdttable.setWidthPercentage(100f);

		try {
			 rdttable.setWidths(colwidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		rdttable.addCell(quotidstatic);
		rdttable.addCell(colcell);
		rdttable.addCell(quotiddynamiccell);
		rdttable.addCell(datecellstatic);
		rdttable.addCell(colcell);
		rdttable.addCell(dtcelldynamic);
//		rdttable.addCell(blankcell);
		
		
		PdfPCell cell3 = new PdfPCell(rdttable); 		
		cell3.setBorder(0);
		
		PdfPTable table2 = new PdfPTable(1);
		table2.addCell(cell3);
		table2.setWidthPercentage(100);
		
		try {
			document.add(table2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}
	
	private void createCustDetails() {
		// TODO Auto-generated method stub
		
		
//		Phrase to=new Phrase("To,",font10bold);
//		PdfPCell tocell=new PdfPCell(to);
//		tocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		tocell.setBorder(0);
		
		String refname="";
		
		String custAdd1="";
		String custFullAdd1="";
		
		boolean company=cust.isCompany();
		System.out.println("COmpany :"+company);
		
		if(cust.isCompany()){
			refname=cust.getCompanyName().trim();
		}else{
			refname=cust.getFullname().trim();
		}
//		Phrase custname=new Phrase(refname,font10bold);
//		PdfPCell custnamecell=new PdfPCell(custname);
//		custnamecell.setBorder(0);
		
		
		if(cust.getSecondaryAdress()!=null){
			
			if(cust.getSecondaryAdress().getAddrLine2()!=null){
				
				if(cust.getSecondaryAdress().getLandmark()!=null){
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2()+", "+cust.getSecondaryAdress().getLandmark();
				}else{
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2();
				}
			}else{
				if(cust.getSecondaryAdress().getLandmark()!=null){
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getLandmark();
				}else{
					custAdd1=cust.getSecondaryAdress().getAddrLine1();
				}
			}
			
			if(cust.getSecondaryAdress().getLocality()!=null){
				custFullAdd1=custAdd1+"\n"+cust.getSecondaryAdress().getCountry()+", "+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCity()+"\n"+cust.getSecondaryAdress().getLocality()+" "+cust.getSecondaryAdress().getPin();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getSecondaryAdress().getCountry()+", "+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCity()+" "+cust.getSecondaryAdress().getPin();
			}
		}
		
		Phrase custAddInfo = new Phrase("To,"+"\n"+refname+"\n"+custFullAdd1, font10bold);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		PdfPTable custTable=new PdfPTable(1);
		custTable.addCell(newline);
//		custTable.addCell(tocell);
//		custTable.addCell(custnamecell);
		custTable.addCell(custAddInfoCell);
//		custTable.setSpacingAfter(10);
		custTable.setWidthPercentage(100);
		
		
		try {
			document.add(custTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase POC=new Phrase("Kind Attn",font10bold);
		PdfPCell POCcell=new PdfPCell(POC);
		POCcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		POCcell.setBorder(0);
		
		String pocdydata=null;
		
		if(!(quot.getDesignation().trim().equals(""))){
			pocdydata=quot.getDesignation();
		}else{
			pocdydata=cust.getFullname();
		}
		Phrase POCDynamic=new Phrase(pocdydata,font10bold);
		PdfPCell POCDynamicCell=new PdfPCell(POCDynamic);
		POCDynamicCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		POCDynamicCell.setBorder(0);
		
		Phrase subject=new Phrase("Subject",font10bold);
		PdfPCell subjectCell=new PdfPCell(subject);
		subjectCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subjectCell.setBorder(0);
		
//		String samplestring=null;
//		
//		for(int i=0;i<quot.getItems().size();i++){
//			
//			if(i==0){
//				samplestring=quot.getItems().get(i).getProductName();
//			}else{
//				
//				if((i+1)!=(quot.getItems().size())){
//					samplestring=samplestring+","+quot.getItems().get(i).getProductName();
//				}else{
//					samplestring=samplestring+" & "+quot.getItems().get(i).getProductName();
//				}
//				
//			}
//			
//			
//		}
		Phrase subjectDynamic=null;
		if(quot.getPremisesDesc()!=null){
			subjectDynamic=new Phrase("Offer for pest management Solution for your "+quot.getPremisesDesc()+" at above Address",font10bold);
		}else{
			subjectDynamic=new Phrase("Offer for pest management Solution for your premises at above Address",font10bold);
			
		}
		
		PdfPCell subjectDynamiccell=new PdfPCell(subjectDynamic);
		subjectDynamiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subjectDynamiccell.setBorder(0);
		
		Phrase colon=new Phrase(":");
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		PdfPTable subtable=new PdfPTable(3);
		subtable.setSpacingBefore(3f);
		
		if(cust.isCompany()){
		subtable.addCell(POCcell);
		subtable.addCell(coloncell);
		subtable.addCell(POCDynamicCell);
		}
		subtable.addCell(subjectCell);
		subtable.addCell(coloncell);
		subtable.addCell(subjectDynamiccell);
		subtable.setWidthPercentage(100);
		try {
			subtable.setWidths(colwidthforsubtable);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(subtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void createstatic() {
		// TODO Auto-generated method stub
		
		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		String response = null;
		
		System.out.println("Lead : "+null);
		if(lead!=null){
			if(lead.getCreationDate()!=null&& quot.getInspectionDate()!=null){
				response="In response to your enquiry dated "+fmt.format(lead.getCreationDate())+" undersigned has carried out inspection of your premises on "+fmt.format(quot.getInspectionDate())+" to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";	
			}else{
				if(quot.getInspectionDate()!=null){
				response="In response to your enquiry, undersigned has carried out inspection of your premises on "+fmt.format(quot.getInspectionDate())+" to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";	
				}
				else
				{
					response="In response to your enquiry, undersigned has carried out inspection of your premises to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";		
				}
				}
		}else{
			if(quot.getInspectionDate()!=null){
				response="In response to your enquiry, undersigned has carried out inspection of your premises on "+fmt.format(quot.getInspectionDate())+" to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";	
				}
				else
				{
					response="In response to your enquiry, undersigned has carried out inspection of your premises to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";		
				}
		}
		
		
		
		Phrase responsephrase=new Phrase(response,font10);
		PdfPCell respnsecell=new PdfPCell(responsephrase);
		respnsecell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		respnsecell.setBorder(0);
		
		PdfPTable responsetable=new PdfPTable(1);
		responsetable.setWidthPercentage(100);
		responsetable.setSpacingAfter(10f);
		responsetable.addCell(respnsecell);
		responsetable.setSpacingAfter(10f);
//		responsetable.addCell(newline);
		try {
			document.add(responsetable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void createQoutServiceDeatails() {
		// TODO Auto-generated method stub
		Phrase colon=new Phrase(":",font10bold);
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		Phrase nameofservice=new Phrase("Name of Service",font10bold);
		PdfPCell nameofservicecell=new PdfPCell(nameofservice);
		nameofservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameofservicecell.setBorder(0);
		
		String productname=null;
		String pestcoverdd=null;
//		BillOfMaterial 
		for (int i = 0; i < quot.getItems().size(); i++) {
			
			if(i==0){	
				productname=quot.getItems().get(i).getProductName();
				if(!quot.getItems().get(i).getComment().equals("")){
				pestcoverdd=quot.getItems().get(i).getComment();
				}
				
			}
			else{
				
			if(((i+1)!= quot.getItems().size())&&(i!=0)){	
				productname=productname+", "+quot.getItems().get(i).getProductName();
				if(!quot.getItems().get(i).getComment().equals("")){
				pestcoverdd=pestcoverdd+", "+quot.getItems().get(i).getComment();
				}
			}else{
				productname=productname+" & "+quot.getItems().get(i).getProductName();
				if(!quot.getItems().get(i).getComment().equals("")){
				pestcoverdd=pestcoverdd+" & "+quot.getItems().get(i).getComment();
				}
			}
			}
		}
		Phrase nameofservicedy=new Phrase(productname,font10);
		PdfPCell nameofservicedycell=new PdfPCell(nameofservicedy);
		nameofservicedycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameofservicedycell.setBorder(0);
		
		Phrase pestcover=new Phrase("Pest Covered",font10bold);
		PdfPCell pestcovercell=new PdfPCell(pestcover);
		pestcovercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pestcovercell.setBorder(0);
		
		Phrase pestcovered = new Phrase(pestcoverdd,font10);
		PdfPCell pestcoveredcell=new PdfPCell(pestcovered);
		pestcoveredcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pestcoveredcell.setBorder(0);
		
		Phrase serviceadd=new Phrase("Site Address",font10bold);
		PdfPCell serviceaddcell=new PdfPCell(serviceadd);
		serviceaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceaddcell.setBorder(0);
		
		Phrase serviceaddress=new Phrase(cust.getSecondaryAdress().getCompleteAddress(),font10);
		PdfPCell serviceaddresscell=new PdfPCell(serviceaddress);
		serviceaddresscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceaddresscell.setBorder(0);
		
		Phrase modeoftreat=new Phrase("Mode of Treatment",font10bold);
		PdfPCell modeoftreatcell=new PdfPCell(modeoftreat);
		modeoftreatcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		modeoftreatcell.setBorder(0);
		
		Phrase modeoftreatdy=new Phrase("As per attached Specifications",font10);
		PdfPCell modeoftreatdycell=new PdfPCell(modeoftreatdy);
		modeoftreatdycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		modeoftreatdycell.setBorder(0);
		
		Phrase chemicaluse=new Phrase("Chemical to be used",font10bold);
		PdfPCell chemicalusecell=new PdfPCell(chemicaluse);
		chemicalusecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		chemicalusecell.setBorder(0);
		
		String samplechemical;
		if(quot.getChemicaltouse()!=null){
			samplechemical=quot.getChemicaltouse();
		}else{
			samplechemical="";
		}
		
		
		Phrase chemicalusedy=new Phrase(samplechemical,font10);
		PdfPCell chemicalusedycell=new PdfPCell(chemicalusedy);
		chemicalusedycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		chemicalusedycell.setBorder(0);
		
//		Phrase chemtoused=new Phrase("Chemical to be used",font10bold);
//		PdfPCell chemtousedcell=new PdfPCell(chemtoused);
//		chemtousedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		chemtousedcell.setBorder(0);
//		
//		Phrase chemtouseddy=new Phrase("Sample Chemical",font10bold);
//		PdfPCell chemtouseddycell=new PdfPCell(chemtouseddy);
//		chemtouseddycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		chemtouseddycell.setBorder(0);
		
		Phrase freqofservice=new Phrase("Frequency of Service",font10bold);
		PdfPCell freqofservicecell=new PdfPCell(freqofservice);
		freqofservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqofservicecell.setBorder(0);
		
		String sampleFreq;
		if(quot.getChemicaltouse()!=null){
			sampleFreq=quot.getFreqService();
		}else{
			sampleFreq="";
		}
		Phrase freqofservicedy=new Phrase(sampleFreq,font10);
		PdfPCell freqofservicedycell=new PdfPCell(freqofservicedy);
		freqofservicedycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqofservicedycell.setBorder(0);
		
		Phrase finalcharges=new Phrase("Our Final Charges",font10bold);
		PdfPCell finalchargescell=new PdfPCell(finalcharges);
		finalchargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		finalchargescell.setBorder(0);
		
		//Final Charges (S.T tax calculation)
		String taxname = null;
		String finalamt;
		for (int i = 0; i < quot.getProductTaxes().size(); i++) {
			taxname=quot.getProductTaxes().get(i).getChargeName()+
			quot.getProductTaxes().get(i).getChargePercent()+"%"+"("+"Rs."+
			quot.getProductTaxes().get(i).getChargePayable()+"/-)";
			
		}
		finalamt="Rs."+quot.getTotalAmount()+"/-"+"+"+taxname+" "+"Total Amount :"+"Rs."+quot.getNetpayable()+"/-";
		System.out.println("finalamt"+finalamt);
		
		Phrase finalchargesdy=new Phrase(finalamt,font10);
		PdfPCell finalchargesdycell=new PdfPCell(finalchargesdy);
		finalchargesdycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		finalchargesdycell.setBorder(0);
		
		Phrase warranty=new Phrase("Warranty Period",font10bold);
		PdfPCell warrantycell=new PdfPCell(warranty);
		warrantycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		warrantycell.setBorder(0);
		

		String sampleWarranty;
		if(quot.getChemicaltouse()!=null){
			sampleWarranty=quot.getWarrantyPeriod();
		}else{
			sampleWarranty="";
		}
		
		Phrase warrantydy=new Phrase(sampleWarranty,font10);
		PdfPCell warrantydycell=new PdfPCell(warrantydy);
		warrantydycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		warrantydycell.setBorder(0);
		

		Phrase valid=new Phrase("Validity of Proposal",font10bold);
		PdfPCell validcell=new PdfPCell(valid);
		validcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		validcell.setBorder(0);
		
		Phrase validdy=new Phrase(fmt.format(quot.getValidUntill()),font10);
		PdfPCell validdycell=new PdfPCell(validdy);
		validdycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		validdycell.setBorder(0);
		
		//End Final Charges (S.T tax calculation)
		
		
		PdfPTable table=new PdfPTable(3);
		table.setWidthPercentage(100);
		try {
			table.setWidths(colwidthQoutServiceDeatails);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		table.addCell(nameofservicecell);
		table.addCell(coloncell);
		table.addCell(nameofservicedycell);
		table.addCell(pestcovercell);
		table.addCell(coloncell);
		table.addCell(pestcoveredcell);
		table.addCell(serviceaddcell);
		table.addCell(coloncell);
		table.addCell(serviceaddresscell);
		table.addCell(modeoftreatcell);
		table.addCell(coloncell);
		table.addCell(modeoftreatdycell);
		table.addCell(chemicalusecell);
		table.addCell(coloncell);
		table.addCell(chemicalusedycell);
		table.addCell(freqofservicecell);
		table.addCell(coloncell);
		table.addCell(freqofservicedycell);
		table.addCell(finalchargescell);
		table.addCell(coloncell);
		table.addCell(finalchargesdycell);
		
		//Payment Terms

				// TODO Auto-generated method stub
				
				
				Phrase blank=new Phrase(" ",font10);
				PdfPCell blankcell=new PdfPCell(blank);
				blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				blankcell.setBorder(0);
				
				
				Phrase payterms=new Phrase("Terms of Payment",font10bold);
				PdfPCell paytermscell=new PdfPCell(payterms);
				paytermscell.setHorizontalAlignment(Element.ALIGN_LEFT);
				paytermscell.setBorder(0);
				
				
				
				table.addCell(paytermscell);
				table.addCell(coloncell);
				
				for(int i=0;i<quot.getPaymentTermsList().size();i++){
					Phrase paypercent=null;
					PdfPCell paypercentcell = null;
					if(quot.getPaymentTermsList().get(i).getPayTermPercent()==100){
						paypercent=new Phrase(quot.getPaymentTermsList().get(i).getPayTermPercent()+"%"+". With work Order",font10);
						paypercentcell=new PdfPCell(paypercent);
						paypercentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						paypercentcell.setBorder(0);
					}else if((i==0 )&&(quot.getPaymentTermsList().get(i).getPayTermPercent()!=100)){
						paypercent=new Phrase(quot.getPaymentTermsList().get(i).getPayTermPercent()+"%"+" "+quot.getPaymentTermsList().get(i).getPayTermComment(),font10);
						paypercentcell=new PdfPCell(paypercent);
						paypercentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						paypercentcell.setBorder(0);
					}
					
					Phrase paypercentage=new Phrase(quot.getPaymentTermsList().get(i).getPayTermPercent()+"%"+" "+quot.getPaymentTermsList().get(i).getPayTermComment(),font10);
					PdfPCell paypercentagecell=new PdfPCell(paypercentage);
					paypercentagecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					paypercentagecell.setBorder(0);
					
					if(i==0){
						table.addCell(paypercentcell);
					}else{
						table.addCell(blankcell);
						table.addCell(blankcell);
						table.addCell(paypercentagecell);
					}
				}
				
				
				//Finished with Payment Terms
				
		table.addCell(warrantycell);
		table.addCell(coloncell);
		table.addCell(warrantydycell);
		table.addCell(validcell);
		table.addCell(coloncell);
		table.addCell(validdycell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}	

	private void createEmployeeDetails() {
		// TODO Auto-generated method stub
		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		Phrase staticpart=new Phrase("We await your acceptance of this quotation. Should you require further information on the above, please call undersigned on mobile No."+employee.getCellNumber1()+" or Tel.No:"+comp.getLandline()+". We would be Pleased to assist you.",font10);
		PdfPCell staticcell=new PdfPCell(staticpart);
		staticcell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		staticcell.setBorder(0);

		Phrase ty=new Phrase("Thanking You",font10);
		PdfPCell tycell=new PdfPCell(ty);
		tycell.setBorder(0);
		tycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase sinpart=new Phrase("Your sincerely,",font10);
		PdfPCell sinpartcell=new PdfPCell(sinpart);
		sinpartcell.setBorder(0);
		sinpartcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase forcomp=new Phrase("For "+comp.getBusinessUnitName(),font10bold);
		PdfPCell forcompcell=new PdfPCell(forcomp);
		forcompcell.setBorder(0);
		forcompcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase space=new Phrase(Chunk.NEWLINE);
		PdfPCell spacecell=new PdfPCell(space);
		spacecell.setBorder(0);
		spacecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table=new PdfPTable(1);
		table.addCell(newline);
		table.addCell(staticcell);
		table.addCell(newline);
		table.addCell(tycell);
		table.addCell(sinpartcell);
		table.addCell(forcompcell);
		table.addCell(spacecell);
		table.addCell(spacecell);
		table.addCell(spacecell);
		table.setWidthPercentage(100);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		///Employee Details
		
		Phrase empfullname=new Phrase(employee.getFullname(),font10bold);
		PdfPCell empfullnamecell=new PdfPCell(empfullname);
		empfullnamecell.setBorder(0);
		empfullnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase empdesignation=new Phrase(employee.getDesignation(),font10bold);
		PdfPCell empdesignationcell=new PdfPCell(empdesignation);
		empdesignationcell.setBorder(0);
		empdesignationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable emptable=new PdfPTable(1);
		
		emptable.addCell(empfullnamecell);
		emptable.addCell(empdesignationcell);
		emptable.setWidthPercentage(100);
		
		try {
			document.add(emptable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Done with employee 
		
		//Contact Details
		Phrase colon=new Phrase(":",font10bold);
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setBorder(0);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emplmob=new Phrase("Mobile",font10bold);
		PdfPCell emplmobcell=new PdfPCell(emplmob);
		emplmobcell.setBorder(0);
		emplmobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emplmobNo=new Phrase(employee.getCellNumber1()+"",font10bold);
		PdfPCell emplmobNocell=new PdfPCell(emplmobNo);
		emplmobNocell.setBorder(0);
		emplmobNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase email=new Phrase("Email",font10bold);
		PdfPCell emailcell=new PdfPCell(email);
		emailcell.setBorder(0);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emailadd=new Phrase(employee.getEmail()+"",font10bold);
		PdfPCell emailaddcell=new PdfPCell(emailadd);
		emailaddcell.setBorder(0);
		emailaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase compland=new Phrase("Office",font10bold);
		PdfPCell complandcell=new PdfPCell(compland);
		complandcell.setBorder(0);
		complandcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase complandno=null;
		if(comp.getLandline()!=null){
			complandno=new Phrase(comp.getLandline()+"",font10bold);
		}else{
			complandno=new Phrase(" ",font10bold);
		}
		
		PdfPCell complandnocell=new PdfPCell(complandno);
		complandnocell.setBorder(0);
		complandnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase web=new Phrase("Website",font10bold);
		PdfPCell webcell=new PdfPCell(web);
		webcell.setBorder(0);
		webcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase website=null;
		if(comp.getLandline()!=null){
			website=new Phrase(comp.getWebsite()+"",font10bold);
		}else{
			website=new Phrase(" ",font10bold);
		}
		
		PdfPCell websitecell=new PdfPCell(website);
		websitecell.setBorder(0);
		websitecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable empltable=new PdfPTable(3);
		empltable.addCell(emplmobcell);
		empltable.addCell(coloncell);
		empltable.addCell(emplmobNocell);
		empltable.addCell(emailcell);
		empltable.addCell(coloncell);
		empltable.addCell(emailaddcell);
		empltable.addCell(complandcell);
		empltable.addCell(coloncell);
		empltable.addCell(complandnocell);
		empltable.addCell(webcell);
		empltable.addCell(coloncell);
		empltable.addCell(websitecell);
		empltable.setWidthPercentage(100);
		try {
			empltable.setWidths(colwidthforempdetails);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			document.add(empltable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
