package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

import static com.googlecode.objectify.ObjectifyService.ofy;



@SuppressWarnings("serial")
public class CreatePdfServlet extends HttpServlet {
	Logger logger = Logger.getLogger("NameOfYourLogger");
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
		
		ProcessConfiguration processConfig;
		//**********quotation version one for first care request **************
		boolean quotationVersionOne=false;
		//**********quotation as letter for pepcop request **************
		boolean quotationAsLetter =false;
		//**********quotation version one for Pecopp request **************
		boolean quotationForPecop=false;
		
		boolean quotationPdf=false;
		boolean gstQuotationPdf=false;
		 
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  
		 
	  try {
		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		  Contract contEntity=ofy().load().type(Contract.class).id(count).now();
		  Quotation quotEntity=ofy().load().type(Quotation.class).id(count).now();
		  
		  if(quotEntity!=null && quotEntity instanceof Quotation){
				processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", quotEntity.getCompanyId()).filter("processName", "Quotation").filter("configStatus", true).first().now();
				if(processConfig!=null){
					
					for(int k=0;k<processConfig.getProcessList().size();k++)
					{
						
						System.out.println("............"+processConfig.getProcessList().get(k).getProcessType().trim());
						System.out.println("111111111111111"+processConfig.getProcessList().get(k).isStatus());
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationAsLetter")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							quotationAsLetter=true;
						}
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationPdf")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							quotationPdf=true;
						}
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("GSTQuotationPdf")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							gstQuotationPdf=true;
						}
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationVersionOne")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							quotationVersionOne=true;
						}
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationForPecop")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							quotationForPecop=true;
						}
						
					}
				}
			}else if(contEntity!=null && contEntity instanceof Contract){

				processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", contEntity.getCompanyId()).filter("processName", "Contract").filter("configStatus", true).first().now();
				if(processConfig!=null){
					
					for(int k=0;k<processConfig.getProcessList().size();k++)
					{
						
						System.out.println("............"+processConfig.getProcessList().get(k).getProcessType().trim());
						System.out.println("111111111111111"+processConfig.getProcessList().get(k).isStatus());
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationAsLetter")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							quotationAsLetter=true;
						}
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationPdf")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							quotationPdf=true;
						}
						
						if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("GSTQuotationPdf")&&processConfig.getProcessList().get(k).isStatus()==true)
						{
							System.out.println("in side condition");
							gstQuotationPdf=true;
						}
					}
				}
			
			}
		  
		  
		  
		  
		  System.out.println("actual flag value "+quotationAsLetter);
		  SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
		  if(quotationAsLetter==true)
		  {
			  System.out.println("inside if actual flag value "+quotationAsLetter);
			  
			  ServiceQuotationPdf servicePsf = new ServiceQuotationPdf();
			  servicePsf.document=new Document();
			  Document document = servicePsf.document;
			 
			  
			  //Ashwini Patil Date:02-05-2023
			  if(quotEntity instanceof Quotation){
				  String filename="Quotation - "+quotEntity.getCount()+" - "+quotEntity.getCinfo().getFullName()+" - "+fmt.format(quotEntity.getQuotationDate())+".pdf";
				  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", quotEntity.getCompanyId()))
					  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
				  logger.log(Level.SEVERE,"filename="+filename);
			  }
			  
			  //Ashwini Patil Date:02-05-2023
			  if(contEntity instanceof Contract){
				  String filename="Contract - "+contEntity.getCount()+" - "+contEntity.getCinfo().getFullName()+" - "+fmt.format(contEntity.getContractDate())+".pdf";
				  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", contEntity.getCompanyId()))
					  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
				  logger.log(Level.SEVERE,"filename="+filename);
			  }
			  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
			  
			  if(quotEntity instanceof Quotation){
				  if(quotEntity.getStatus().equals(Quotation.CANCELLED)){//Date 16/11/2017 By jayshree changes are made to add the cancel watermark
					   
					   writer.setPageEvent(new PdfCancelWatermark());	
				   }else 
				   
				   if(!quotEntity.getStatus().equals("Approved")&&!quotEntity.getStatus().equals("Successful")&&!quotEntity.getStatus().equals("UnSuccessful")){
					   writer.setPageEvent(new PdfWatermark());	
				   }
				  
			   }
			   
			   if(contEntity instanceof Contract){
				   
				   if(contEntity.getStatus().equals(Contract.CANCELLED)){
					   
					   writer.setPageEvent(new PdfCancelWatermark());	
				   }else
				   
				   if(!contEntity.getStatus().equals("Approved")&&!contEntity.getStatus().equals("Expired")){
					   writer.setPageEvent(new PdfWatermark());	
				   } 
			   }
//		   
			   		document.open();
			   		
			   	  String preprintStatus=request.getParameter("preprint");
				   System.out.println("****************"+preprintStatus);
				   if(preprintStatus.contains("yes")){
					   
						servicePsf.setservicequotation(count,preprintStatus);
				   }
				   
				   if(preprintStatus.contains("no")){
					   
						servicePsf.setservicequotation(count,preprintStatus);
				   }
				   
				   if(preprintStatus.contains("plane")){
					   
						servicePsf.setservicequotation(count,preprintStatus);
				   }
				   
				   servicePsf.createPdf(preprintStatus);
				    document.close();

		  }
		  else
		  {
			  
			  System.out.println("inside else actual flag value "+quotationAsLetter);
				if (quotationPdf) {
					QuotationPdf pdf = new QuotationPdf();
					pdf.document = new Document();
					Document document = pdf.document;
					
					//Ashwini Patil Date:02-05-2023
					  if(quotEntity instanceof Quotation){
						  String filename="Quotation - "+quotEntity.getCount()+" - "+quotEntity.getCinfo().getFullName()+" - "+fmt.format(quotEntity.getQuotationDate())+".pdf";
						  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", quotEntity.getCompanyId()))
							  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
						  logger.log(Level.SEVERE,"filename="+filename);
					  }
					  
					  //Ashwini Patil Date:02-05-2023
					  if(contEntity instanceof Contract){
						  String filename="Contract - "+contEntity.getCount()+" - "+contEntity.getCinfo().getFullName()+" - "+fmt.format(contEntity.getContractDate())+".pdf";
						  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", contEntity.getCompanyId()))
							  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
						  logger.log(Level.SEVERE,"filename="+filename);
					  }
					PdfWriter writer = PdfWriter.getInstance(document,response.getOutputStream()); // write the pdf in
															// response

					ServerAppUtility apputility = new ServerAppUtility();
					Company company = null;
					String branch="";
					if (quotEntity instanceof Quotation) {
						company = apputility.loadCompany(quotEntity.getCompanyId());
						branch=quotEntity.getBranch();
						if (quotEntity.getStatus().equals(Quotation.CANCELLED)) {

							writer.setPageEvent(new PdfCancelWatermark());
						} else

						if (!quotEntity.getStatus().equals("Approved")
								&& !quotEntity.getStatus().equals("Successful")
								&& !quotEntity.getStatus().equals(
										"UnSuccessful")) {
							writer.setPageEvent(new PdfWatermark());
						}

					}

					if (contEntity instanceof Contract) {
						company = apputility.loadCompany(contEntity.getCompanyId());
						branch=contEntity.getBranch();
						if (contEntity.getStatus().equals(Contract.CANCELLED)) {

							writer.setPageEvent(new PdfCancelWatermark());
						} else

						if (!contEntity.getStatus().equals("Approved")
								&& !contEntity.getStatus().equals("Expired")) {
							writer.setPageEvent(new PdfWatermark());
						}
					}
					String preprintStatus = request.getParameter("preprint");
//					======================================
					
					
					if(company.getUploadHeader()!=null && company.getUploadFooter()!=null){
				 		   document.setPageSize(PageSize.A4);
					 	   document.setMargins(30, 30, 120, 100);
					 	   document.setMarginMirroring(false);
					 	   
				 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,branch,preprintStatus);
					 	   writer.setPageEvent(event);
				 	   }
//					======================================
					
					
					document.open();

					
					System.out.println("****************" + preprintStatus);
					if (preprintStatus.contains("yes")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					if (preprintStatus.contains("no")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					if (preprintStatus.contains("plane")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					pdf.createPdf(preprintStatus);
					document.close();

				} else if (gstQuotationPdf) {
					logger.log(Level.SEVERE, "GST Quotation calling ");
					GSTQuotationPdf pdf = new GSTQuotationPdf();
					pdf.document = new Document();
					Document document = pdf.document;
					
					//Ashwini Patil Date:02-05-2023
					  if(quotEntity instanceof Quotation){
						  String filename="Quotation - "+quotEntity.getCount()+" - "+quotEntity.getCinfo().getFullName()+" - "+fmt.format(quotEntity.getQuotationDate())+".pdf";
						  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", quotEntity.getCompanyId()))
							  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
						  logger.log(Level.SEVERE,"filename="+filename);
					  }
					  
					  //Ashwini Patil Date:02-05-2023
					  if(contEntity instanceof Contract){
						  String filename="Contract - "+contEntity.getCount()+" - "+contEntity.getCinfo().getFullName()+" - "+fmt.format(contEntity.getContractDate())+".pdf";
						  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", contEntity.getCompanyId()))
							  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
						  logger.log(Level.SEVERE,"filename="+filename);
					  }
					PdfWriter writer = PdfWriter.getInstance(document,
							response.getOutputStream()); // write the pdf in
															// response

					if (quotEntity instanceof Quotation) {
						if (quotEntity.getStatus().equals(Quotation.CANCELLED)) {

							writer.setPageEvent(new PdfCancelWatermark());
						} else

						if (!quotEntity.getStatus().equals("Approved")
								&& !quotEntity.getStatus().equals("Successful")
								&& !quotEntity.getStatus().equals(
										"UnSuccessful")) {
							writer.setPageEvent(new PdfWatermark());
						}

					}

					if (contEntity instanceof Contract) {

						if (contEntity.getStatus().equals(Contract.CANCELLED)) {

							writer.setPageEvent(new PdfCancelWatermark());
						} else

						if (!contEntity.getStatus().equals("Approved")
								&& !contEntity.getStatus().equals("Expired")) {
							writer.setPageEvent(new PdfWatermark());
						}
					}

					document.open();

					String preprintStatus = request.getParameter("preprint");
					System.out.println("****************" + preprintStatus);
					if (preprintStatus.contains("yes")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					if (preprintStatus.contains("no")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					if (preprintStatus.contains("plane")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					pdf.createPdf(preprintStatus);
					document.close();

				}
				else if(quotationVersionOne == true) { 
					

					logger.log(Level.SEVERE, "Quotation version one is calling:: "+ quotationVersionOne);

					FirstCareServiceQuotationPdf fcsq = new FirstCareServiceQuotationPdf();
					fcsq.document = new Document();
					Document document = fcsq.document;

					PdfWriter writer = PdfWriter.getInstance(document,response.getOutputStream());

					if (quotEntity instanceof Quotation) {
						if (quotEntity.getStatus().equals(Quotation.CANCELLED)) {
							writer.setPageEvent(new PdfCancelWatermark());
						} else{
							if (!quotEntity.getStatus().equals("Approved")
									&& !quotEntity.getStatus().equals("Successful")
									&& !quotEntity.getStatus().equals("UnSuccessful")) {
								writer.setPageEvent(new PdfWatermark());
							}
						}
					}

					document.open();

					String preprintStatus = request.getParameter("preprint");
					System.out.println("****************" + preprintStatus);
					if (preprintStatus.contains("yes")) {
						fcsq.setfcservicequotation(count, preprintStatus);
					}

					if (preprintStatus.contains("no")) {
						fcsq.setfcservicequotation(count, preprintStatus);
					}

					if (preprintStatus.contains("plane")) {
						fcsq.setfcservicequotation(count, preprintStatus);
					}

					fcsq.createPdf(preprintStatus);
					document.close();

				
				}
				
				else if(quotationForPecop == true) { 
					

					logger.log(Level.SEVERE, "Quotation version one is calling:: "+ quotationForPecop);

					QuotationVersionOnePdf peq = new QuotationVersionOnePdf();
					peq.document = new Document();
					Document document = peq.document;

					PdfWriter writer = PdfWriter.getInstance(document,response.getOutputStream());

					if (quotEntity instanceof Quotation) {
						if (quotEntity.getStatus().equals(Quotation.CANCELLED)) {
							writer.setPageEvent(new PdfCancelWatermark());
						} else{
							if (!quotEntity.getStatus().equals("Approved")
									&& !quotEntity.getStatus().equals("Successful")
									&& !quotEntity.getStatus().equals("UnSuccessful")) {
								writer.setPageEvent(new PdfWatermark());
							}
						}
					}

					document.open();

					String preprintStatus = request.getParameter("preprint");
					System.out.println("****************" + preprintStatus);
					if (preprintStatus.contains("yes")) {
						peq.setPcservicequotation(count, preprintStatus);
					}

					if (preprintStatus.contains("no")) {
						peq.setPcservicequotation(count, preprintStatus);
					}

					if (preprintStatus.contains("plane")) {
						peq.setPcservicequotation(count, preprintStatus);
					}

					peq.createPdf(preprintStatus);
					document.close();

				
				}
				
				
				else {
					logger.log(Level.SEVERE, "Quotation PDF calling");
					QuotationPdf pdf = new QuotationPdf();
					pdf.document = new Document();
					Document document = pdf.document;
					
					//Ashwini Patil Date:02-05-2023
					  if(quotEntity instanceof Quotation){
						  String filename="Quotation - "+quotEntity.getCount()+" - "+quotEntity.getCinfo().getFullName()+" - "+fmt.format(quotEntity.getQuotationDate())+".pdf";
						  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", quotEntity.getCompanyId()))
							  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
						  logger.log(Level.SEVERE,"filename="+filename);
					  }
					  
					  //Ashwini Patil Date:02-05-2023
					  if(contEntity instanceof Contract){
						  String filename="Contract - "+contEntity.getCount()+" - "+contEntity.getCinfo().getFullName()+" - "+fmt.format(contEntity.getContractDate())+".pdf";
						  if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", contEntity.getCompanyId()))
							  response.setHeader("Content-Disposition", "attachment; filename=" + filename);
						  logger.log(Level.SEVERE,"filename="+filename);
					  }
					PdfWriter writer = PdfWriter.getInstance(document,
							response.getOutputStream()); // write the pdf in
															// response
					ServerAppUtility apputility = new ServerAppUtility();
					Company company = null;
					String branch=null;
					if (quotEntity instanceof Quotation) {
						company = apputility.loadCompany(quotEntity.getCompanyId());
						branch=quotEntity.getBranch();
						if (quotEntity.getStatus().equals(Quotation.CANCELLED)) {

							writer.setPageEvent(new PdfCancelWatermark());
						} else

						if (!quotEntity.getStatus().equals("Approved")
								&& !quotEntity.getStatus().equals("Successful")
								&& !quotEntity.getStatus().equals(
										"UnSuccessful")) {
							writer.setPageEvent(new PdfWatermark());
						}

					}

					if (contEntity instanceof Contract) {
						company = apputility.loadCompany(contEntity.getCompanyId());
						branch=contEntity.getBranch();
						if (contEntity.getStatus().equals(Contract.CANCELLED)) {

							writer.setPageEvent(new PdfCancelWatermark());
						} else

						if (!contEntity.getStatus().equals("Approved")
								&& !contEntity.getStatus().equals("Expired")) {
							writer.setPageEvent(new PdfWatermark());
						}
					}
					String preprintStatus = request.getParameter("preprint");
//					======================================
					
					
//					if(company.getUploadHeader()!=null && company.getUploadFooter()!=null){
//				 		   document.setPageSize(PageSize.A4);
//					 	   document.setMargins(30, 30, 120, 100);
//					 	   document.setMarginMirroring(false);
//					 	   
//					 	  
//				 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,branch,preprintStatus);
//					 	   writer.setPageEvent(event);
//				 	   }
//					======================================
					document.open();

					
					System.out.println("****************" + preprintStatus);
					if (preprintStatus.contains("yes")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					if (preprintStatus.contains("no")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					if (preprintStatus.contains("plane")) {

						String type = request.getParameter("type");
						if (type.contains("q")) {
							pdf.getQuotation(count, preprintStatus);
						}

						if (type.contains("c")) {
							pdf.getContract(count, preprintStatus);
						}

					}

					pdf.createPdf(preprintStatus);
					document.close();

				}
		  }

		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
