package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambQuotationWoodBorerPdf {

	Document document;
	Customer cust;
	Company comp;
	Contract con;
	Quotation quot;
	List<Service> ser;
	Invoice invo;
	List<ArticleType> articletype;
	
	 
	private Font font16boldul, font12bold, font8bold, font8, font9bold,font9boldul,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,
	font14bold,font9,font7,font7bold,font9red,font9boldred,font12boldred;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	DecimalFormat df=new DecimalFormat("0.00");

	
	 public PcambQuotationWoodBorerPdf() {   
		super();
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
		
	}

	
	public void setWdQuotation(Long count) {//Load Quotation
		quot=ofy().load().type(Quotation.class).id(count).now();
		
		// Load Invoice
		invo =ofy().load().type(Invoice.class).id(count).now();
		
		// load Customer
		if(quot.getCompanyId()==null)
		cust = ofy().load().type(Customer.class).filter("count",quot.getCinfo().getCount()).first().now();
		else
	    cust=ofy().load().type(Customer.class).filter("companyId", quot.getCompanyId()).filter("count",quot.getCinfo().getCount()).first().now();

		//Load Company
		if(quot.getCompanyId()==null)
		   comp=ofy().load().type(Company.class).first().now();
		else
		   comp=ofy().load().type(Company.class).filter("companyId",quot.getCompanyId()).first().now();
		//Load Contract
		if(quot.getCompanyId()!=null)
			con=ofy().load().type(Contract.class).filter("companyId",quot.getCompanyId()).filter("count",quot.getContractCount()).first().now();
		else
			con=ofy().load().type(Contract.class).filter("count", quot.getContractCount()).first().now();
		
		       
		//Load Article type Details  
		 articletype = new ArrayList<ArticleType>();
			if(cust.getArticleTypeDetails().size()!=0){
				articletype.addAll(cust.getArticleTypeDetails());
			}
			if(comp.getArticleTypeDetails().size()!=0){
				articletype.addAll(comp.getArticleTypeDetails());
			}
}
	public void createPdf()
	{
	 
		createLogo(document,comp);
		createWoodBorerHeader();
		createWoodBorerTermiteObservationtbl();
		
	}
	
	private void createLogo(Document doc, Company comp) 
	{
	
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private void createWoodBorerHeader() {

PdfPCell headerCell=new PdfPCell();		
        	
Phrase mycomHeader1=new Phrase(comp.getBusinessUnitName().toUpperCase(),font16bold);
Paragraph headerPara1=new Paragraph();
headerPara1.add(mycomHeader1);
headerPara1.add(Chunk.NEWLINE);
headerPara1.setAlignment(Element.ALIGN_CENTER);
headerCell.addElement(headerPara1);


Phrase mycomHeader2=new Phrase("WOOD BORER INSPECTION CHECKLIST",font12bold);
Paragraph headerPara2=new Paragraph();
headerPara2.add(mycomHeader2);
headerPara2.add(Chunk.NEWLINE);
headerPara2.setAlignment(Element.ALIGN_CENTER);
headerCell.addElement(headerPara2);      

	
Phrase mycomHeader3=new Phrase("WOOD BORER MANAGEMENT",font10bold);


String addressline1="";

if(cust.getAdress().getAddrLine2().equals(null)&& cust.getAdress().getAddrLine2().equals("")){
	addressline1=cust.getAdress().getAddrLine1()+"  "+cust.getAdress().getAddrLine2();
}
else{
	addressline1=cust.getAdress().getAddrLine1()+ " , ";
}

String locality = "";
if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
	System.out.println("inside both null condition1");
	locality= (cust.getAdress().getLandmark()+" , "+cust.getAdress().getLocality()+" , "+"\n"+cust.getAdress().getCity()+" , "
		      +cust.getAdress().getPin()+" , "+"\n" +cust.getAdress().getState()+" , "+cust.getAdress().getCountry());
}
else if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
	System.out.println("inside both null condition 2");
	locality= (cust.getAdress().getLandmark()+" , "+"\n"+cust.getAdress().getCity()+" , "
		      +cust.getAdress().getPin()+" , "+"\n"+cust.getAdress().getState()+" , "+cust.getAdress().getCountry()+" . ");
}

else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
	System.out.println("inside both null condition 3");
	locality= (cust.getAdress().getLocality()+" , "+"\n"+cust.getAdress().getCity()+" , "
		      +cust.getAdress().getPin()+"\n"+" , "+cust.getAdress().getState()+" , "+cust.getAdress().getCountry());
}
else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
	System.out.println("inside both null condition 4");
	locality=(cust.getAdress().getCity()+" , "+cust.getAdress().getPin()+" , "+"\n"+cust.getAdress().getState()+" , "+cust.getAdress().getCountry());
}

Phrase mycomHeader4 = new Phrase ("Client Name:  "+     "____________________________________",font9);
Phrase mycomHeader5 = new Phrase ("Client Address: "+  "___________________________________",font9);  
Phrase mycomHeader6 = new Phrase ("                         "+ "___________________________________",font9);
Phrase mycomHeader7 = new Phrase("FLAT /BUNGALOW /OFFICE / SOCIETY AREA _________________ SQ.FT & FLOOR NO:_________________",font10bold);
                                                                  
Paragraph headerPara3=new Paragraph();
headerPara3.add(mycomHeader3);   
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(mycomHeader4);
headerPara3.add(Chunk.NEWLINE);   
headerPara3.add(mycomHeader5);
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(mycomHeader6);
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(mycomHeader7);
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(Chunk.NEWLINE);
headerPara3.setAlignment(Element.ALIGN_LEFT);
headerCell.addElement(headerPara3);

//image logo code added here // 
	try
	{
	Image image1=Image.getInstance("images/PCAMB.png");
	image1.scalePercent(10f);
	image1.scaleAbsoluteWidth(120f);
	image1.setAbsolutePosition(442f,758f);	  
	document.add(image1);
	}
	catch(Exception e)  
	{
		e.printStackTrace();
	}
	// image logo code end here // 
	
	PdfPTable parentTbl=new PdfPTable(1);
	 parentTbl.setWidthPercentage(100);    
	 parentTbl.addCell(headerCell);

try {
	
   document.add(parentTbl);
} catch (Exception e1) {
     e1.printStackTrace();
}

}
	// End method 1
	//start  method 2
	private void createWoodBorerTermiteObservationtbl() {
		

		 PdfPTable termiteObsTbl=new PdfPTable(5);
		 termiteObsTbl.setWidthPercentage(100);  
		
		 // tableheader 5 cols
		 
		    Paragraph p1= new Paragraph();
	 		Phrase srno=new Phrase("SR NO.",font9bold);
	 		p1.add(srno);
	 		p1.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srnoCell =new PdfPCell();    
	 		srnoCell.addElement(p1);
	 		
	 		Paragraph p2= new Paragraph();
	 		Phrase questions=new Phrase("QUESTIONS",font9bold);
	 		p2.add(questions);
	 		p2.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell questionsCell =new PdfPCell();
	 		questionsCell.addElement(p2);
	 		 
	 		Paragraph p3= new Paragraph();
	 		Phrase tmobs=new Phrase(" OBSERVATION      YES  /   NO ",font9bold);
	 		p3.add(tmobs);
	 		p3.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobsCell =new PdfPCell();
	 		tmobsCell.addElement(p3);
	 		
	 		Paragraph p4= new Paragraph();
	 		Phrase suggestion=new Phrase("INFERENCE / SUGGESTION ",font9bold);
	 		p4.add(suggestion);
	 		p4.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell suggestionCell =new PdfPCell();
	 		suggestionCell.addElement(p4);  
	 		 
	 		Paragraph p5= new Paragraph();
	 		Phrase remarks=new Phrase("REMARKS / OTHER DETAILS",font9bold);
	 		p5.add(remarks);
	 		p5.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarksCell =new PdfPCell();   
	 		remarksCell.addElement(p5);
	 		
	 	
	 		//row 1 values table with 6 cols
	 		
	 		PdfPTable termiteObsValueTbl=new PdfPTable(6);
	 		termiteObsValueTbl.setWidthPercentage(100);
	 		
	 		Paragraph p6= new Paragraph();
	 		Phrase srno1=new Phrase(" ",font9);
	 		p6.add(srno1);
	 		p6.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno1Cell =new PdfPCell();    
	 		srno1Cell.addElement(p6);
	 		
	 		Paragraph p7= new Paragraph();
	 		Phrase questions1=new Phrase(" Wood Borer Control",font9boldul);
	 		p7.add(questions1);
	 		p7.add(Chunk.NEWLINE);
	 		p7.add(Chunk.NEWLINE);
	 		p7.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions1Cell =new PdfPCell();
	 		questions1Cell.addElement(p7);
	 		 
	 		Paragraph p81= new Paragraph();
	 		Phrase tmobs1 =new Phrase(" ",font9bold);
	 		p81.add(tmobs1);
	 		p81.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs11Cell =new PdfPCell();
	 		tmobs11Cell.addElement(p81);
	 		
	 		Paragraph p82= new Paragraph();
	 		Phrase tmobs12=new Phrase(" ",font9bold);
	 		p82.add(tmobs12);
	 		p82.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs12Cell =new PdfPCell();
	 		tmobs12Cell.addElement(p82);
	 		
	 		Paragraph p9= new Paragraph();
	 		Phrase suggestion1=new Phrase(" ",font9);
	 		p9.add(suggestion1);
	 		p9.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell suggestion1Cell =new PdfPCell();
	 		suggestion1Cell.addElement(p9);
	 		 
	 		
	 		Paragraph p10= new Paragraph();
	 		Phrase remarks1=new Phrase(" ",font9bold);
	 		p10.add(remarks1);
	 		p10.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks1Cell =new PdfPCell();
	 		remarks1Cell.addElement(p10);
	 		
	//row 2
	 		
	 		Paragraph p11= new Paragraph();
	 		Phrase srno2=new Phrase("1",font9bold);
	 		p11.add(srno2);
	 		p11.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno2Cell =new PdfPCell();    
	 		srno2Cell.addElement(p11);
	 		
	 		Paragraph p12= new Paragraph();
	 		Phrase questions2=new Phrase(" Checking of spots",font10);
	 		p12.add(questions2);
	 		p12.add(Chunk.NEWLINE);
	 		p12.add(Chunk.NEWLINE);
	 		p12.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions2Cell =new PdfPCell();
	 		questions2Cell.addElement(p12);
	 		 
	 		Paragraph p13= new Paragraph();
	 		Phrase tmobs21 =new Phrase(" ",font9bold);
	 		p13.add(tmobs21);
	 		p13.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs21Cell =new PdfPCell();
	 		tmobs21Cell.addElement(p13);        
	 		
	 		Paragraph p14= new Paragraph();
	 		Phrase tmobs22=new Phrase(" ",font9bold);
	 		p14.add(tmobs22);
	 		p14.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs22Cell =new PdfPCell();
	 		tmobs22Cell.addElement(p14);
	 		
	 		Paragraph p15= new Paragraph();
	 		Phrase suggestion2=new Phrase(" Checking of spots",font10);
	 		p15.add(suggestion2);
	 		p15.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion2Cell =new PdfPCell();
	 		suggestion2Cell.addElement(p15);
	 		 
	 		
	 		Paragraph p16= new Paragraph();
	 		Phrase remarks2=new Phrase(" ",font9bold);
	 		p16.add(remarks2);
	 		p16.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks2Cell =new PdfPCell();
	 		remarks2Cell.addElement(p16);
	 		
	 //row3
	 		Paragraph p17= new Paragraph();
	 		Phrase srno3=new Phrase("2",font9bold);
	 		p17.add(srno3);
	 		p17.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno3Cell =new PdfPCell();    
	 		srno3Cell.addElement(p17);  
	 		
	 		Paragraph p18= new Paragraph();
	 		Phrase questions3=new Phrase(" Level of infestation",font10);
	 		p18.add(questions3);
	 		p18.add(Chunk.NEWLINE);
	 		p18.add(Chunk.NEWLINE);
	 		p18.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions3Cell =new PdfPCell();
	 		questions3Cell.addElement(p18);
	 		 
	 		Paragraph p19= new Paragraph();
	 		Phrase tmobs31 =new Phrase(" ",font9);
	 		p19.add(tmobs31);
	 		p19.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs31Cell =new PdfPCell();
	 		tmobs31Cell.addElement(p19);
	 		
	 		Paragraph p20= new Paragraph();
	 		Phrase tmobs32=new Phrase(" if yes, mention areas",font10);
	 		p20.add(tmobs32);
	 		p20.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs32Cell =new PdfPCell();
	 		tmobs32Cell.addElement(p20);
	 		
	 		Paragraph p21= new Paragraph();
	 		Phrase suggestion3=new Phrase(" Level of infestation",font10);
	 		p21.add(suggestion3);
	 		p21.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion3Cell =new PdfPCell();
	 		suggestion3Cell.addElement(p21);
	 		 
	 		
	 		Paragraph p22= new Paragraph();
	 		Phrase remarks3=new Phrase(" ",font9bold);
	 		p22.add(remarks3);
	 		p22.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks3Cell =new PdfPCell();
	 		remarks3Cell.addElement(p22);
	 		
	 		
	 		//row4
	 		Paragraph p23= new Paragraph();
	 		Phrase srno4=new Phrase("3",font9bold);
	 		p23.add(srno4);
	 		p23.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno4Cell =new PdfPCell();    
	 		srno4Cell.addElement(p23);
	 		
	 		Paragraph p24= new Paragraph();
	 		Phrase questions4=new Phrase(" Checking of powder collection",font10);
	 		p24.add(questions4);
	 		p24.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions4Cell =new PdfPCell();
	 		questions4Cell.addElement(p24);
	 		 
	 		Paragraph p25= new Paragraph();
	 		Phrase tmobs41 =new Phrase(" ",font9bold);
	 		p25.add(tmobs41);
	 		p25.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs41Cell =new PdfPCell();
	 		tmobs41Cell.addElement(p25);
	 		
	 		Paragraph p26= new Paragraph();
	 		Phrase tmobs42=new Phrase(" ",font9bold);
	 		p26.add(tmobs42);
	 		p26.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs42Cell =new PdfPCell();
	 		tmobs42Cell.addElement(p26);
	 		
	 		Paragraph p27= new Paragraph();
	 		Phrase suggestion4=new Phrase(" Checking of spots",font10);
	 		p27.add(suggestion4);
	 		p27.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion4Cell =new PdfPCell();
	 		suggestion4Cell.addElement(p27);
	 		 
	 		
	 		Paragraph p28= new Paragraph();
	 		Phrase remarks4=new Phrase(" ",font9bold);
	 		p28.add(remarks4);
	 		p28.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks4Cell =new PdfPCell();
	 		remarks4Cell.addElement(p28);
	 		
	 		
	 		//row5
	 		Paragraph p29= new Paragraph();
	 		Phrase srno5=new Phrase("4",font9bold);
	 		p29.add(srno5);
	 		p29.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno5Cell =new PdfPCell();    
	 		srno5Cell.addElement(p29);
	 		
	 		Paragraph p30= new Paragraph();
	 		Phrase questions5=new Phrase(" Check for tiny holes in wooden furniture or fixtures",font10);
	 		p30.add(questions5);
	 		p30.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions5Cell =new PdfPCell();
	 		questions5Cell.addElement(p30);
	 		 
	 		Paragraph p31= new Paragraph();
	 		Phrase tmobs51 =new Phrase(" ",font9bold);
	 		p31.add(tmobs51);
	 		p31.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs51Cell =new PdfPCell();
	 		tmobs51Cell.addElement(p31);
	 		
	 		Paragraph p32= new Paragraph();
	 		Phrase tmobs52=new Phrase(" ",font9bold);
	 		p32.add(tmobs52);
	 		p32.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs52Cell =new PdfPCell();
	 		tmobs52Cell.addElement(p32);
	 		
	 		Paragraph p33= new Paragraph();
	 		Phrase suggestion5=new Phrase(" Check for tiny holes in wooden furniture or fixtures",font10);
	 		p33.add(suggestion5);
	 		p33.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion5Cell =new PdfPCell();
	 		suggestion5Cell.addElement(p33);
	 		 
	 		
	 		Paragraph p34= new Paragraph();
	 		Phrase remarks5=new Phrase(" ",font9bold);
	 		p34.add(remarks5);
	 		p34.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks5Cell =new PdfPCell();
	 		remarks5Cell.addElement(p34);
	 		
	 		//row6
	 		Paragraph p35= new Paragraph();
	 		Phrase srno6=new Phrase("5",font9bold);
	 		p35.add(srno6);
	 		p35.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno6Cell =new PdfPCell();    
	 		srno6Cell.addElement(p35);
	 		
	 		Paragraph p36= new Paragraph();
	 		Phrase questions6=new Phrase(" Check for eating or biting sounds in the wood",font10);
	 		p36.add(questions6);
	 		p36.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions6Cell =new PdfPCell();
	 		questions6Cell.addElement(p36);
	 		 
	 		Paragraph p37= new Paragraph();
	 		Phrase tmobs61 =new Phrase(" ",font9bold);
	 		p37.add(tmobs61);
	 		p37.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs61Cell =new PdfPCell();
	 		tmobs61Cell.addElement(p37);
	 		
	 		Paragraph p38= new Paragraph();
	 		Phrase tmobs62=new Phrase(" ",font9bold);
	 		p38.add(tmobs62);
	 		p38.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs62Cell =new PdfPCell();
	 		tmobs62Cell.addElement(p38);
	 		
	 		Paragraph p39= new Paragraph();
	 		Phrase suggestion6=new Phrase(" Check for eating or biting sounds in the wood",font10);
	 		p39.add(suggestion6);
	 		p39.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion6Cell =new PdfPCell();
	 		suggestion6Cell.addElement(p39);
	 		 
	 		
	 		Paragraph p40= new Paragraph();
	 		Phrase remarks6=new Phrase(" ",font9bold);
	 		p40.add(remarks6);
	 		p40.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks6Cell =new PdfPCell();
	 		remarks6Cell.addElement(p40);
	 		
	 		
	 		//row7
	 		Paragraph p41= new Paragraph();
	 		Phrase srno7=new Phrase("6",font9bold);
	 		p41.add(srno7);
	 		p41.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno7Cell =new PdfPCell();    
	 		srno7Cell.addElement(p41);
	 		
	 		Paragraph p42= new Paragraph();
	 		Phrase questions7=new Phrase(" Check weather infestation is localized.",font10);
	 		p42.add(questions7);
	 		p42.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions7Cell =new PdfPCell();
	 		questions7Cell.addElement(p42);
	 		 
	 		Paragraph p43= new Paragraph();
	 		Phrase tmobs71 =new Phrase(" ",font9bold);
	 		p43.add(tmobs71);
	 		p43.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs71Cell =new PdfPCell();
	 		tmobs71Cell.addElement(p43);
	 		
	 		Paragraph p44= new Paragraph();
	 		Phrase tmobs72=new Phrase(" ",font9bold);
	 		p44.add(tmobs72);
	 		p44.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs72Cell =new PdfPCell();
	 		tmobs72Cell.addElement(p44);
	 		
	 		Paragraph p45= new Paragraph();
	 		Phrase suggestion7=new Phrase(" Check weather infestation is localized.",font10);
	 		p45.add(suggestion7);
	 		p45.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion7Cell =new PdfPCell();
	 		suggestion7Cell.addElement(p45);
	 		 
	 		
	 		Paragraph p46= new Paragraph();
	 		Phrase remarks7=new Phrase(" ",font9bold);
	 		p46.add(remarks7);
	 		p46.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks7Cell =new PdfPCell();
	 		remarks7Cell.addElement(p46);
	 		
	 		//row8
	 		Paragraph p47= new Paragraph();
	 		Phrase srno8=new Phrase("7",font9bold);
	 		p47.add(srno8);
	 		p47.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno8Cell =new PdfPCell();    
	 		srno8Cell.addElement(p47);
	 		
	 		Paragraph p48= new Paragraph();
	 		Phrase questions8=new Phrase(" Differentiate weather it is powder or granules.",font10);
	 		p48.add(questions8);
	 		p48.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions8Cell =new PdfPCell();
	 		questions8Cell.addElement(p48);
	 		 
	 		Paragraph p49= new Paragraph();
	 		Phrase tmobs81 =new Phrase(" ",font9bold);
	 		p49.add(tmobs81);
	 		p49.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs81Cell =new PdfPCell();
	 		tmobs81Cell.addElement(p49);
	 		
	 		Paragraph p50= new Paragraph();
	 		Phrase tmobs82=new Phrase(" ",font9bold);
	 		p50.add(tmobs82);
	 		p50.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs82Cell =new PdfPCell();
	 		tmobs82Cell.addElement(p50);
	 		
	 		Paragraph p51= new Paragraph();
	 		Phrase suggestion8=new Phrase(" Differentiate weather it is powder or granules.",font9);
	 		p51.add(suggestion8);
	 		p51.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell suggestion8Cell =new PdfPCell();
	 		suggestion8Cell.addElement(p51);
	 		 
	 		
	 		Paragraph p52= new Paragraph();
	 		Phrase remarks8=new Phrase(" ",font9bold);
	 		p52.add(remarks8);
	 		p52.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks8Cell =new PdfPCell();
	 		remarks8Cell.addElement(p52);
	 		
	 		//row9
	 		Paragraph p53= new Paragraph();
	 		Phrase srno9=new Phrase("8",font9bold);
	 		p53.add(srno9);
	 		p53.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell srno9Cell =new PdfPCell();    
	 		srno9Cell.addElement(p53);
	 		
	 		Paragraph p54= new Paragraph();
	 		Phrase questions9=new Phrase(" Society Manager/ Secretary",font9);
	 		p54.add(questions9);
	 		p54.add(Chunk.NEWLINE);
	 		p54.add(Chunk.NEWLINE);
	 		p54.setAlignment(Element.ALIGN_LEFT);
	 		PdfPCell questions9Cell =new PdfPCell();
	 		questions9Cell.addElement(p54);
	 		 
	 		Paragraph p55= new Paragraph();
	 		Phrase tmobs91 =new Phrase(" ",font9bold);
	 		p55.add(tmobs91);
	 		p55.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs91Cell =new PdfPCell();
	 		tmobs91Cell.addElement(p55);
	 		
	 		Paragraph p56= new Paragraph();
	 		Phrase tmobs92=new Phrase(" ",font9bold);
	 		p56.add(tmobs92);
	 		p56.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell tmobs92Cell =new PdfPCell();
	 		tmobs92Cell.addElement(p56);
	 		
	 		Paragraph p57= new Paragraph();
	 		Phrase suggestion9=new Phrase(" ",font9bold);
	 		p57.add(suggestion9);
	 		p57.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell suggestion9Cell =new PdfPCell();
	 		suggestion9Cell.addElement(p57);
	 		 
	 		
	 		Paragraph p58= new Paragraph();
	 		Phrase remarks9=new Phrase(" ",font9bold);
	 		p58.add(remarks9);
	 		p58.setAlignment(Element.ALIGN_CENTER);
	 		PdfPCell remarks9Cell =new PdfPCell();
	 		remarks9Cell.addElement(p58);
	 		
	 		
	 		// adding all Cell
	 	
	 		termiteObsTbl.addCell(srnoCell);
	 		termiteObsTbl.addCell(questionsCell);      
	 		termiteObsTbl.addCell(tmobsCell);
	 		termiteObsTbl.addCell(suggestionCell);
	 		termiteObsTbl.addCell(remarksCell);
	 		
	 		termiteObsValueTbl.addCell(srno1Cell);
	 		termiteObsValueTbl.addCell(questions1Cell);
	 		termiteObsValueTbl.addCell(tmobs11Cell);
	 		termiteObsValueTbl.addCell(tmobs12Cell);
	 		termiteObsValueTbl.addCell(suggestion1Cell);
	 		termiteObsValueTbl.addCell(remarks1Cell);
	 		
	 		termiteObsValueTbl.addCell(srno2Cell);
	 		termiteObsValueTbl.addCell(questions2Cell);      
	 		termiteObsValueTbl.addCell(tmobs21Cell);
	 		termiteObsValueTbl.addCell(tmobs22Cell);
	 		termiteObsValueTbl.addCell(suggestion2Cell);
	 		termiteObsValueTbl.addCell(remarks2Cell);
	 		
	 		termiteObsValueTbl.addCell(srno3Cell);
	 		termiteObsValueTbl.addCell(questions3Cell);      
	 		termiteObsValueTbl.addCell(tmobs31Cell);
	 		termiteObsValueTbl.addCell(tmobs32Cell);
	 		termiteObsValueTbl.addCell(suggestion3Cell);
	 		termiteObsValueTbl.addCell(remarks3Cell);
	 		
	 		termiteObsValueTbl.addCell(srno4Cell);
	 		termiteObsValueTbl.addCell(questions4Cell);      
	 		termiteObsValueTbl.addCell(tmobs41Cell);
	 		termiteObsValueTbl.addCell(tmobs42Cell);
	 		termiteObsValueTbl.addCell(suggestion4Cell);
	 		termiteObsValueTbl.addCell(remarks4Cell);
	 		
	 		termiteObsValueTbl.addCell(srno5Cell);
	 		termiteObsValueTbl.addCell(questions5Cell);      
	 		termiteObsValueTbl.addCell(tmobs51Cell);
	 		termiteObsValueTbl.addCell(tmobs52Cell);
	 		termiteObsValueTbl.addCell(suggestion5Cell);
	 		termiteObsValueTbl.addCell(remarks5Cell);
	 		
	 		termiteObsValueTbl.addCell(srno6Cell);
	 		termiteObsValueTbl.addCell(questions6Cell);      
	 		termiteObsValueTbl.addCell(tmobs61Cell);
	 		termiteObsValueTbl.addCell(tmobs62Cell);
	 		termiteObsValueTbl.addCell(suggestion6Cell);
	 		termiteObsValueTbl.addCell(remarks6Cell);
	 		
	 		termiteObsValueTbl.addCell(srno7Cell);
	 		termiteObsValueTbl.addCell(questions7Cell);      
	 		termiteObsValueTbl.addCell(tmobs71Cell);
	 		termiteObsValueTbl.addCell(tmobs72Cell);
	 		termiteObsValueTbl.addCell(suggestion7Cell);
	 		termiteObsValueTbl.addCell(remarks7Cell);
	 		
	 		termiteObsValueTbl.addCell(srno8Cell);
	 		termiteObsValueTbl.addCell(questions8Cell);      
	 		termiteObsValueTbl.addCell(tmobs81Cell);
	 		termiteObsValueTbl.addCell(tmobs82Cell);
	 		termiteObsValueTbl.addCell(suggestion8Cell);
	 		termiteObsValueTbl.addCell(remarks8Cell);
	 		
	 		
	 		termiteObsValueTbl.addCell(srno9Cell);
	 		termiteObsValueTbl.addCell(questions9Cell);      
	 		termiteObsValueTbl.addCell(tmobs91Cell);
	 		termiteObsValueTbl.addCell(tmobs92Cell);
	 		termiteObsValueTbl.addCell(suggestion9Cell);
	 		termiteObsValueTbl.addCell(remarks9Cell);
	 		
	 		 try {
				 termiteObsTbl.setWidths(new float[] {05,15,16,32,32 });
					document.add(termiteObsTbl);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
	 		 		
	 		 try {
	 		 			termiteObsValueTbl.setWidths(new float[] { 05,15,8,8,32,32 });
	 						document.add(termiteObsValueTbl);
	 						document.add(Chunk.NEXTPAGE);
	 					} catch (Exception e1) {  
	 						e1.printStackTrace();
	 					}
	 		 
	    }
	// end  method 2
}
