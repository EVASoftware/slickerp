package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;


public class CreatePcambQuotationServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5983000961404259213L;

	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//super.doGet(request, response);
		response.setContentType("application/pdf");
	


	
	String stringid = request.getParameter("Id");
	stringid = stringid.trim();
	Long count = Long.parseLong(stringid);
	
	
	String type = request.getParameter("type");
	type =type.trim();
	
	
	
	if(type.equalsIgnoreCase("WoodBorer"))
	{
		 try{
			 PcambQuotationWoodBorerPdf pdf= new  PcambQuotationWoodBorerPdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setWdQuotation(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("BedBugs"))
	{
		 try{
			 PcambQuotationBedBugsPdf pdf= new  PcambQuotationBedBugsPdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setBbQuotation(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
		 
	}
	else if(type.equalsIgnoreCase("FlyMosquito"))
	{
		 try{
			 PcambQuotationFlyMosquitoPdf pdf= new  PcambQuotationFlyMosquitoPdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setInvoice(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("Rodent"))
	{
		 try{
			 PcambQuotationRodentPdf pdf= new  PcambQuotationRodentPdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setQuotationRodent(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("Termite"))
	{
		 try{
			 PcambQuotationTermitePdf pdf= new  PcambQuotationTermitePdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setQuotationTermite(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("Gipc"))
	{
		 try{
			 PcambQuotationGipcPdf pdf= new  PcambQuotationGipcPdf();
				
				pdf.document= new Document(PageSize.A4.rotate());
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 
				  document.open();
				  pdf.setQuotationGipc(count);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	
	}
}
