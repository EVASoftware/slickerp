package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.utility.BahtText;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class ServiceGSTInvoiceWithTaxColumns {


	List<SalesLineItem> products;
	ArrayList<String> dataStringList;
	ArrayList<Integer> alignmentList;
	ArrayList<Chunk> chunkList;
	ArrayList<Font> fontList;
	int firstBreakPoint = 17;
	int nooflineforServicedetail=60;
	int productcountforservicedetail=0;
	float blankLines;
	boolean hideGSTINNo=false;
	float[] columnMoreLeftWidths = { 2f, 1f };
	float[] columnMoreLeftHeaderWidths = { 1.7f, 1.3f };

	float[] columnMoreRightWidths = { 0.8f, 2.2f };
	float[] columnMoreRightCheckBoxWidths = { 0.3f, 2.7f };
	float[] columnHalfWidth = { 1f, 1f };
	float[] columnHalfInnerWidth = { 0.65f, 1f };

	float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
	float[] columnCollon6Width = { 1.8f, 0.2f, 7.5f, 1.8f, 0.2f, 7.5f };

	float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.5f };
	float[] columnStateCodeCollonWidth = { 3.5f, 2f, 0.2f, 1f };
	float[] columnContractPeriodDateCodeCollonWidth = { 3f, 1.5f, 0.2f, 2f };
	float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };

	float[] column6RowCollonWidth = { 0.5f, 0.2f, 1.5f, 0.8f, 0.2f, 0.5f };
	float[] column16CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.3f };
	float[] columnrohanrrCollonWidth = { 3.0f, 0.2f, 7.5f };//

	// float[] column10ProdCollonWidth = { 0.1f, 0.52f, 0.2f, 0.38f, 0.15f,
	// 0.15f,
	// 0.2f, 0.25f, 0.15f, 0.25f };
	// float[] column8ProdCollonWidth = { 0.1f, 0.92f, 0.2f, 0.38f, 0.15f, 0.2f,
	// 0.15f, 0.25f };
	float[] column7ProdCollonWidth = { 0.1f, 1.08f, 0.2f, 0.38f, 0.2f, 0.15f,
			0.25f };

	float[] column8SerProdCollonWidth = { 0.1f, 0.92f, 0.16f, 0.2f, 0.38f,
			0.2f, 0.15f, 0.25f };
	float[] column8ProdCollonWidth = { 0.1f, 1.08f, 0.2f, 0.19f, 0.19f, 0.2f,
			0.15f, 0.25f };
	float[] column9ServProdCollonWidth = { 0.1f, 1f, 0.08f, 0.2f, 0.19f, 0.19f,
			0.2f, 0.15f, 0.25f };
	float[] column4ProdCollonWidth = { 1.75f, 0.2f, 0.15f, 0.25f };

	float[] column2ProdCollonWidth = { 1.9f, 0.25f };
	float[] column3ProdCollonWidth = { 1f, 0.9f, 0.4f };
	Logger logger = Logger.getLogger("Size");
	float[] column13CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.4f, 0.4f, 0.4f, 0.3f };
	// {Sr No,Services,HSN ACS,UOM,Qty,Rate,Amount,Disc,Taxable
	// amt,CGST,SGST,IGST,Total}

	/**
	 * rohan added this flag for universal pest control This is used to print
	 * vat no and other article information
	 */

	Boolean UniversalFlag = false;
	/**
	 * ends here
	 */
	/**
	 * Manisha added this to change the date for single service
	 */
	Boolean isSingleService = false;
	/** End **/

	public Document document;
	/***Date 1-8-2020 by Amol for invoice id prefix raised by Ashwini Bhagwat**/
    boolean invoicePrefix=false;
	Invoice invoiceentity;
	List<BillingDocumentDetails> billingDoc;
	ProcessConfiguration processConfig;
	Customer cust;
	Company comp;
	Contract con;
	/**
	 * Date 25-4-2018
	 * by Jayshree
	 */
	List<Service> servicelist;
	
	List<ContractCharges> billingTaxesLis; // ajinkya added this 03/07/2017
	List<CustomerBranchDetails> custbranchlist;
	List<CustomerBranchDetails> customerbranchlist;
	SimpleDateFormat sdf;
	
	/**
	 * @author Anil
	 * @since 19-01-2022
	 * Amount should be print with comma after each three digit. raised by Nithila and Nitin Sir
	 */
	DecimalFormat df = new DecimalFormat("0.00");//#,###.00
//	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat decimalformat = new DecimalFormat("0.00");
	
	boolean upcflag = false;
	boolean onlyForFriendsPestControl = false;
	boolean authOnLeft = false;
	boolean isPlaneFormat = false;
	boolean printserviceDetailAnnex=false;
	/**DAte 26-6-2020 by Amol Added this to print the address and cell no from customer branch 
	 * if only one customer branch is selected raised by rahul Tiwari For Narmada Pest Control
	 */
	boolean addcellNoFlag = false;
	CustomerBranchDetails customerBranch=null;
	/**
	 * Developer : Jayshree Date : 21 Nov 2017 Description : Added this to check
	 * serviceSchedulingList needed or not
	 */
	boolean serviceSchedulelistFlag = false;
	/**
	 * Rohan added this for Universal pest for printing
	 */
	Boolean multipleCompanyName = false;
	Boolean printPremiseDetails = false;
	
	/**
	 * Date 17-5-2018 By Jayshree
	 */
	Boolean nonbillingInvoice = false;
	
	
	/**
	 * ends here
	 */

	List<PaymentTerms> payTermsLis;

	List<State> stateList;

	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	/**
	 * Date 9/12/2017 Dev.By Jayshree Des.To increses the Font size Changes are
	 * done
	 */
	Font font13 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	// End By jayshree
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
	
	Font nameAddressBoldFont=new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);//Ashwini Patil
	Font nameAddressFont=new Font(Font.FontFamily.HELVETICA, 7);//Ashwini Patil
	Font nameAddressFont6 = new Font(Font.FontFamily.HELVETICA, 7);

	Phrase blankCell = new Phrase(" ", font10);

	/* Total Amount */
	double totalAmount;
	/**
	 * Added By Rahul Verma Max Lines which can be used between products and 50
	 * characters of service Name in product Table represents on Line
	 * */

	int noOfLines = 16;
	/**
	 * This is where lines breaks
	 */
	int prouductCount = 0;
	/**
	 * Date 27-07-2017 By ANIL This flag is used to check whether to print
	 * product description on pdf or not.
	 */
	boolean productDescFlag = false;
	/**
	 * Date 28 Sept 2017 By Rahul This flag is used to check whether to print
	 * product description on pdf or not.
	 */
	boolean printAttnInPdf = false;
	boolean consolidatePrice = false;
	/* Added By Rahul Verma on Date 28 Aug 2017 */
	CompanyPayment comppayment;
	private PdfPCell imageSignCell;

	/**
	 * Date 12/1/2018 Dev By Jayshree; 1)To Check the process congigration for
	 * company email and Branch email 2)To Check the process congigration for
	 * company address and Branch Address
	 */
	boolean checkEmailId = false;
	boolean hoEmail=false;
	/**
	 * Ends For Jayshree
	 */

	/**
	 * Date 15/1/2018 By jayshree dev.To check the process config for company
	 * heading detail
	 */
	boolean checkheaderLeft = false;
	boolean checkheaderRight = false;

	boolean invoiceGroupAsSignatory=false;
	boolean contractTypeAsPremisedetail = false;
	// End By Jayshree

	// PdfPCell premiseTblCell = new PdfPCell();
	/**
	 * nidhi
	 * 9-08-2018
	 * for print serial no & model no
	 */
	boolean printModelSerailNoFlag =false;
	
	/**
	 * Date 22-11-2018 By Vijay 
	 * Des :- if process configuration is active then GST Number will not display
	 * and GST Number will display if GST applicable or not applicable as per nitin sir
	 */
	boolean gstNumberPrintFlag = true;
	
	/**Date 28-8-2019 by Amol make a Billing Address and service Address To uppercase using process config**/
	boolean adresstouppercase=false;
	boolean printGstinFromCustomer=false;
	
	
	/**
	 *@author Anil , Date : 12-09-2019
	 *Rate contract invoice format should be default invoice format for Hvac
	 *raised by Rohan Bhagde
	 */
	boolean hvacFmtFlag=false;
	boolean poDetailsFlag=true;//1-11-2022 made true
	Branch branchDt = null;
	
	/**
	 * @author Abhinav Bihade
	 * @since 20/12/2019
	 * As per Rahul Tiwari's Requirement 
	 * Orkin : Hide Rate and Discount column from contract and Tax Invoice pdf 
	 */
	boolean hideRateAndDiscount=false;
	
	/**
	 * @author Abhinav Bihade
	 * @since 08/02/2020
	 * As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
	 * in case if somebody manages 2 companies under same link of ERP s/w
	 */
	ArrayList <ArticleType> branchWiseFilteredArticleList;
	
	/**
	 * @author Vijay Chougule Date - 28-07-2020
	 * Des :- For PSTPL to calculate Total no of services and print on invoice 
	 */ 
	 boolean complainServiceWithTurnAroundTimeFlag = false;

    /**
	 * @author Vijay Date 20-11-2020 
	 * Des :- As per Rahul and Nitin sir instruction if Quatity exist in invoice product table then
	 * quontity column will display in product table
	 */
	 boolean qtycolumnFlag =false;

	 
	 float[] column9SerProdCollonWidth = { 0.1f, 0.92f, 0.16f, 0.2f, 0.38f,
				0.15f, 0.20f, 0.15f, 0.25f };
	 float[] column12SerProdCollonWidth = { 0.1f, 0.92f, 0.16f, 0.2f, 0.15f,0.15f,
				0.38f, 0.20f, 0.15f, 0.25f, 0.25f, 0.25f };
//	 float[] column11SerProdCollonWidth = { 0.1f, 1.84f, 0.16f, 0.2f, 0.15f,0.15f,
//				0.38f, 0.20f, 0.15f, 0.25f, 0.25f };
	 float[] column11SerProdCollonWidth = { 0.1f, 1.65f, 0.2f, 0.30f, 0.25f,0.25f,
				0.25f, 0.25f, 0.25f, 0.25f, 0.25f };
 // added by Priyanka
    boolean hideContractDate=false;
	boolean hideBillingPeriod=false;
	boolean QtyToDays=false;
		
	boolean HideReverseCharge;
	boolean HideColumn;
	boolean HideNoOfService;
	boolean HideDiscount;
	boolean HideContractDuration;
	
	/**
	 * @author Anil @since 22-07-2021
	 * Thai font flag
	 */
	boolean thaiFontFlag=false; 
	
	boolean custbranchmailFlag=false;
	
	String qtylabel="Qty";
	
	boolean changeTitle=false;
	
	boolean AmountInWordsHundreadFormatFlag=false;
	
	 
	/**
	 * @author Anil @since 01-10-2021
	 * added print bank details flag and default value for this flag will be true
	 * requirement raised by Rahul Tiwar and Nitin Sir
	 */
//	boolean printBankDetailsFlag=true;
	
//	PdfUtility pdfutility=new PdfUtility();
	
	boolean PC_RemoveSalutationFromCustomerOnPdfFlag = false;
	
	/**
	 * @author Anil
	 * @since 19-01-2022
	 * If company country is selected as Thailand then thai pdf will print
	 */
	BahtText bahtText=new BahtText();
	boolean thaiPdfFlag=false;
	boolean pageBreakFlag=false;
	List<CompanyPayment> compPayList=null;
	
	String invoiceTitle="";
	String copyTitle="";
	String companyCountry="";
	
	/**
	 * @author Anil
	 * @since 21-01-2022
	 * Need to add service annexure and some modification for service wise bill invoice 
	 * raised by Nithila and Nitin sir for Hygienic Pest and will be generic for all
	 */
	List<BillingDocument> billingList;
	List<Service> serviceList;
	boolean serviceWiseBillInvoice=false;
	String billingPeriod="";
//	SimpleDateFormat sdf1;
//	SimpleDateFormat sdf2;
	
	Config numberRangeConfig;
	
	PdfUtility pdfUtility = new PdfUtility();
	
	boolean doNotPrintServicesFlag = false;
	
	String billingState="";//1-11-2022	
	
	double totalTaxableValue=0,totalCGST=0,totalSGST=0,grandTotal=0;//14-11-2022
	 
	
	public void setInvoice(Long count) {
		// Load Invoice
		invoiceentity = ofy().load().type(Invoice.class).id(count).now();
		
		if (invoiceentity.getCompanyId() != null) {
			
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
						thaiFontFlag=true;
//						break;
					}
					if(obj.getProcessType().equalsIgnoreCase(AppConstants.PC_AMOUNTINWORDSHUNDREADSTRUCTURE)&&obj.isStatus()==true){
						AmountInWordsHundreadFormatFlag = true;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
													
				font16boldul = new Font(boldFont, 16,Font.UNDERLINE);
				font16bold = new Font(boldFont, 16);
				font14bold = new Font(boldFont, 14);
				font14 = new Font(regularFont, 14);
				font10 = new Font(regularFont, 7);
				font10bold = new Font(boldFont, 7);
				
				font13 = new Font(regularFont, 9);
				font13bold = new Font(boldFont, 9);
				font12bold = new Font(boldFont, 12);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 12,Font.UNDERLINE);
				font12 = new Font(regularFont, 12);
				font11 = new Font(regularFont, 10);
				font11bold = new Font(boldFont, 10);
				font6 = new Font(regularFont, 7);
				font6bold = new Font(boldFont, 6);
				font9bold = new Font(boldFont, 9);
				
				BaseFont tahomaFont=BaseFont.createFont("Tahoma Regular font.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahomaBoldFont=BaseFont.createFont("TAHOMAB0.TTF",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				nameAddressFont=new Font(tahomaFont, 9);
				nameAddressBoldFont=new Font(tahomaFont, 9);
				nameAddressFont6=new Font(tahomaFont, 7);
				
				BaseFont T_regularFont=BaseFont.createFont("angsa.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont T_boldFont=BaseFont.createFont("angsab.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
//				titlefont=new Font(T_boldFont,14);
				
				titlefont=new Font(tahomaBoldFont,12);

			}
			
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
		
		
		
		//
		// billingDoc=invoiceentity.getArrayBillingDocument();
		// invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
		// arrPayTerms=invoiceentity.getArrPayTerms();
		// Load Customer

		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		// Load Company
		if (invoiceentity.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		
		if(comp!=null){
			companyCountry=comp.getAddress().getCountry().trim();
		}

		if (invoiceentity.getCompanyId() != null)
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		else
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount()).first()
					.now();

		payTermsLis = con.getPaymentTermsList();

		if (invoiceentity.getCompanyId() == null)
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount()).list();
		else
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		/****************************** vijay ************************/

		System.out.println("Branch name======"
				+ invoiceentity.getCustomerBranch());
		if (invoiceentity.getCompanyId() == null)
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch()).list();
		else
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		System.out.println("Banch updated====="
				+ invoiceentity.getCustomerBranch());

		if (invoiceentity.getCompanyId() != null) {
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		}
		
		
		if(invoiceentity.getCompanyId()!=null){
			servicelist=ofy().load().type(Service.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("contractCount", invoiceentity.getContractCount()).list();
		}
		
		/****************************** vijay ************************/

		stateList = ofy().load().type(State.class)
				.filter("companyId", invoiceentity.getCompanyId()).list();

		/************************************ Letter Head Flag *******************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAddressAndCellFromCustomerBranch")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						addcellNoFlag = true;

					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}

					if (processConfig.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("OnlyForFriendsPestControl")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						onlyForFriendsPestControl = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("AuthorityOnLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						authOnLeft = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAttnInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printAttnInPdf = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ConsolidatePrice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						consolidatePrice = true;
					}
					// By jayshree Date 15/11/2017
					// changes are made for process configration for service
					// schedule list
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ServiceScheduleList")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						serviceSchedulelistFlag = true;
					}
					// ends for Jayshree

					/**
					 * Date 12/1/2018 Dev.By jayshree 1)Des.To check the branch
					 * and company mail id 2)Des.To check the branch and company
					 * mail Address
					 */

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}

					// End For Jayshree

					/**
					 * Date 15/1/2018 Dev.By jayshree Des.To check the company
					 * heading alingment
					 */

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderLeft = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtRight")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderRight = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("InvoiceGroupAsSignatory")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						invoiceGroupAsSignatory = true;
					}

					// End By Jayshree
					
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("TypeAsPremiseDetail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						contractTypeAsPremisedetail = true;
					}
					
					
					/**
					 * Date 31-3-2018
					 * By jayshree
					 * Des.to print the company emailid
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintHOEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hoEmail = true;
					}
					
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintServiceDetailAnnexture")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printserviceDetailAnnex = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("NonbillingInvoice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						nonbillingInvoice = true;
					}
					
					/*** Date 23-11-2018 By Vijay For GST Number Print or not ****/ 
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("EnableDoNotPrintGSTNumber")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						gstNumberPrintFlag = false;
					}
//					/**
//					 * ends here
//					 */
//					/**Date 26-3-2020 by Amol, in Service address GSTIN number should be directly pick from Customer branch 
//					 * instead of invoice screen ,raised by Vaishnavi Mam for ISPC , Because they need GSTIN Number in Billing address from Customer 
//					 * Master.
//					 */
//					if(processConfig.getProcessList().get(k).getProcessType()
//							.trim()
//							.equalsIgnoreCase("PrintGSTNumberFromCustomer")
//							&& processConfig.getProcessList().get(k).isStatus() == true){
//						printGstinFromCustomer=true;				}
//					
					
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("RateCardInvoiceDefaultFormat")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hvacFmtFlag = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("COMPLAINSERVICEWITHTURNAROUNDTIME")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						complainServiceWithTurnAroundTimeFlag = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_PRINTINVOICENUMBERPREFIX")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						invoicePrefix = true;
					}
					
					/**
					 *   Added By Priyanka
					 */
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_HideReverseCharge")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						HideReverseCharge = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_HideColumn")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						HideColumn = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_HideNoOfService")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						HideNoOfService = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_HideDiscount")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						HideDiscount = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_HideContractDuration")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						HideContractDuration = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideContractDate")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hideContractDate = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideBillingPeriod")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hideBillingPeriod = true;
					}
//					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ChangeQtyToDays")
//							&& processConfig.getProcessList().get(k).isStatus() == true) {
//						QtyToDays = true;
//					}//ChangeQty-
					
//					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ChangeQty-Area")
//					&& processConfig.getProcessList().get(k).isStatus() == true) {
//						QtyToArea = true;
//					}//ChangeQty-
					
//					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ChangeQty-Days")
//							&& processConfig.getProcessList().get(k).isStatus() == true) {
//								QtyToDays = true;
//							}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_CustomerBranchEmail")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						custbranchmailFlag = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_INOICE_DONOT_PRINT_#SERVICES")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						doNotPrintServicesFlag = true;
					}
					
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().contains("ChangeQty-")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						try {
							String label = processConfig.getProcessList().get(k).getProcessType().trim();
							String[] nameArray = label.split("-");
							if (nameArray.length > 0) {
								qtylabel = nameArray[1];
							}
							break;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					
					/**
					 *  End
					 */
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_RenameProformaInvoiceWithInvoice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						changeTitle = true;
					}
				}
			}
		}
		
		/**
		 * @author Anil
		 * @since 21-01-2022
		 * As we are printing annexure details by using process configuration then we should load data also using process configuartion
		 * earlier all services are getting loaded
		 */
		if(invoiceentity.getCompanyId()!=null&&printserviceDetailAnnex){
			servicelist=ofy().load().type(Service.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("contractCount", invoiceentity.getContractCount()).list();
		}

		/**
		 * nidhi
		 * 06-04-2018
		 * for branch as a company process configration
		 * 
		 */ 
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(invoiceentity !=null && invoiceentity.getBranch() != null && invoiceentity.getBranch().trim().length()>0){
				
				 branchDt = ofy().load().type(Branch.class).filter("companyId",invoiceentity.getCompanyId()).filter("buisnessUnitName", invoiceentity.getBranch()).first().now();
				 /**
				  * @author Anil
				  * @since 16-02-2022
				  * Invoice header was not getting updated as per brnach
				  * Raised By Atharva for pest master
				  */
				 if(branchDt != null){
						comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				 }
				 
				if(branchDt !=null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){

					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", invoiceentity.getCompanyId()).first()
								.now();
						
						
//						if(comppayment != null){
//							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
//						}
						
					}
					
					
				}
			}
		}
		
		if(invoiceentity.getPaymentMode()!=null && invoiceentity.getPaymentMode().trim().length()>0){
			List<String> paymentDt = Arrays.asList(invoiceentity.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", invoiceentity.getCompanyId()).first()
						.now();
				
			}
		}
		
		/**
		 * end
		 */
		/**
		 * nidhi
		 * 9-08-2018
		 */
		printModelSerailNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "PrintModelNoAndSerialNo", invoiceentity.getCompanyId());
        
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PrintPoDetailsOnInvoice",invoiceentity.getCompanyId())){
			poDetailsFlag=true;
		}
		
		

		/**
		 * @author Abhinav Bihade
		 * @since 20/12/2019
		 * As per Rahul Tiwari's Requirement 
		 * Orkin : Hide Rate and Discount column from contract and Tax Invoice pdf 
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "HideRateAndDiscountColumn", comp.getCompanyId())){
		hideRateAndDiscount=true;
		logger.log(Level.SEVERE,"Inside ProcessConfig:" +hideRateAndDiscount);
		}
		
		
		
		String dateFormat = ServerAppUtility.getForProcessConfigurartionIsActiveOrNot(AppConstants.PDFDATEFORMAT, comp.getCompanyId());
		logger.log(Level.SEVERE,"dateFormat" +dateFormat);

		if(dateFormat!=null && !dateFormat.equals("")){
			sdf = new SimpleDateFormat(dateFormat);
		}
		else{
			sdf = new SimpleDateFormat("dd/MM/yyyy");
		}
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		/**
		 * @author Abhinav Bihade
		 * @since 08/02/2020
		 * As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
		 * in case if somebody manages 2 companies under same link of ERP s/w
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			branchWiseFilteredArticleList =getArticleBranchList(comp.getArticleTypeDetails(),invoiceentity.getBranch());
			logger.log(Level.SEVERE,"Inside Branch As Company: " +branchWiseFilteredArticleList.size());
		}
		/**
		 * @author Anil @since 2021
		 * Added exception to avoid unexpected termination of program
		 * @author Anil @since 29-10-2021
		 * As Customer branch is stored in invoice entity itself then no need to check it contact 
		 */
//		ArrayList<String> custbranchlist=null;
//		try{
//			custbranchlist=getCustomerBranchList(con.getItems());
//		}catch(Exception e){
//			
//		}
//		if(custbranchlist!=null&&custbranchlist.size()==1&& custbranchlist.contains("Service Address")==false){
		if(invoiceentity.getCustomerBranch()!=null&&!invoiceentity.getCustomerBranch().equals("")){
			logger.log(Level.SEVERE,"In Side AList1:");
			customerBranch= ofy().load().type(CustomerBranchDetails.class)
						.filter("cinfo.count",con.getCinfo().getCount())
						.filter("companyId", con.getCompanyId())
						.filter("buisnessUnitName", invoiceentity.getCustomerBranch()).first().now();
			
			logger.log(Level.SEVERE,"AList1:" +customerBranch);
			logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());
		}
//		}
			
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, comp.getCompanyId())){
			PC_RemoveSalutationFromCustomerOnPdfFlag = true;
		}
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * If country is selected as thai land then we will print thai specific format designed for Innovative
		 * requirement taken by Nitin Sir and Nithila
		 */
		if(companyCountry!=null&&!companyCountry.equals("")){
//			if(companyCountry.equalsIgnoreCase("Thailand")||companyCountry.trim().equalsIgnoreCase("ประเทศไทย")){				
//				System.out.println("In country if. setting flags");
//				thaiPdfFlag=true;
//				thaiFontFlag=true;
//				pageBreakFlag=true;
////				recieptPdf=false;
//			}
			
			if(thaiPdfFlag){
				compPayList=ofy().load().type(CompanyPayment.class).filter("companyId", invoiceentity.getCompanyId()).filter("paymentStatus", true).list();
			}
			
			/**
			 * @author Anil
			 * @since 19-01-2022
			 * Need to print four copy of invoices for thailand client 
			 * two copy for invoice/proforma invoice or reciept each one is termed as original and copy
			 * raised by Nitin Sir and Nithila for Innovative
			 */
//			if(thaiPdfFlag){
//				if(pageBreakFlag){
//					System.out.println("In thaiPdfFlag and pageBreakFlag flag");
//					if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
//						invoiceTitle="Invoice / ใบแจ้งหนี้"; 
//					}else if(AppConstants.CREATETAXINVOICE.equals(invoiceentity.getInvoiceType().trim())){
//						invoiceTitle="Tax Invoice / ใบกำกับภาษี";
//					}
//					
//				}else{
//					invoiceTitle="Receipt / ใบเสร็จรับเงิน";
//				}
//				copyTitle="ต้นฉบับ / ORIGINAL";
//				System.out.println("InvoiceTitle from original="+invoiceTitle);
//			}
		}
		
		
		
		if(invoiceentity!=null&&invoiceentity.getRateContractServiceId()!=0){
			serviceWiseBillInvoice=true;
			/**
			 * @author Anil
			 * @since 02-02-2022
			 * Earlier we are calculating billing period on invoice for display purpose only but as 
			 * per updated requirement it should be stored in invoice and will be controlled from there only
			 * raised by Nitin and Jayesh
			 */
//			serviceList=ofy().load().type(Service.class).filter("companyId", invoiceentity.getCompanyId())
//							.filter("count", invoiceentity.getRateContractServiceId()).list();
			
//			if(serviceList!=null&&serviceList.size()!=0){
//				billingPeriod=sdf1.format(serviceList.get(0).getServiceDate());
//			}
			
			billingPeriod=sdf.format(invoiceentity.getBillingPeroidFromDate())+" - "+sdf.format(invoiceentity.getBillingPeroidToDate());
		}
		
		if(invoiceentity.getNumberRange()!=null && !invoiceentity.getNumberRange().equals("")){
			numberRangeConfig = ofy().load().type(Config.class).filter("companyId", invoiceentity.getCompanyId())
								.filter("name", invoiceentity.getNumberRange()).filter("type", 91).first().now();
		}
		
		
		if(invoiceentity.getIRN()!=null&&!invoiceentity.getIRN().equals("")) //Ashwini Patil Date:28-10-2022
			noOfLines=14;
	}
	
	private ArrayList<String> getCustomerBranchList(List<SalesLineItem> itemList) {
		HashSet<String> branchHs=new HashSet<String>();
		for(SalesLineItem itemObj:itemList){
			if(itemObj.getCustomerBranchSchedulingInfo()!=null){
				ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
				for(BranchWiseScheduling obj:branchSchedulingList){
					if(obj.isCheck()==true){
						branchHs.add(obj.getBranchName());
					}
				}
			}
		}
		
		if(branchHs!=null&&branchHs.size()!=0){
			ArrayList<String> branchList=new ArrayList<String>(branchHs);
			logger.log(Level.SEVERE,"In Side AList3:"+branchList.size());
			return branchList;
		}
		
		return null;
		
	}

	private ArrayList<ArticleType> getArticleBranchList(ArrayList<ArticleType> articleTypeDetails, String branch) {
		ArrayList<ArticleType> artilist =new ArrayList<ArticleType>();
		for(ArticleType at:articleTypeDetails){
			logger.log(Level.SEVERE,"at.getArticleDescription() " +at.getArticleDescription()+" / "+branch);
			/**
			 * @author Vijay Date :- 11-08-2021
			 * Des :- added 2 condition description!=null && description not blank then only it will check branch to manage 2 different companies
			 * otherwise it will work normal flow 
			 */
			if(at.getArticleDescription()!=null && !at.getArticleDescription().equals("") ){
				if(at.getArticleDescription().equals(branch)){
					artilist.add(at);
				}
				
				logger.log(Level.SEVERE,"Inside Branch As Company Method:" +artilist);
			}
			else{
				artilist.add(at);
			}
			
		}
		
		return artilist;
	}
	
	public void createPdf(String preprintStatus) {
		
		
		if (preprintStatus.equals("plane")) {
			createHeader();
		} 
		
		createBlankforUPC();

		
		double discount = 0, roundOff = 0;
		discount = invoiceentity.getDiscountAmt();
		roundOff = invoiceentity.getDiscount();
		createInvoiceDetails();
		createCustomerDetails();
		
		

		createProductDetails();


		createFooterAmountPart();
		if (discount != 0) {
			createFooterDisCountAfterPart(discount);
		}
		if (invoiceentity.getOtherCharges().size() > 0) {
			createFooterOtherChargesPart2();
		}
		createFooterTaxPart();
		if (roundOff != 0) {
			createFooterDisCountBeforeNetPayPart(roundOff);
		}
		createFooterAmountInWords_NetPayPart();
		createTermsAndCondition();
		createFooterLastPart(preprintStatus);
	}

	/**
	 * @author Anil
	 * @since 21-01-2022
	 */
	private void createAnnexureDetailsTblForServiceWiseBill() {
		
		if(serviceList!=null&&serviceList.size()!=0){
			
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			float[] columns = { 5f,10f,9f,9f,10f,15f,9f,9f,10f,15f };
			PdfPTable tbl =new PdfPTable(10);
			tbl.setWidthPercentage(100f);
			
			try {
				tbl.setWidths(columns);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			tbl.addCell(pdfUtility.getCell("Annexure - Details of the services which have been completed", font9bold, Element.ALIGN_CENTER, 0, 10, 0));
			
			tbl.addCell(pdfUtility.getCell("Sr Nr", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Service Date", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Completion", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
			tbl.addCell(pdfUtility.getCell("Service Id", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Product Name", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Signed by", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
			tbl.addCell(pdfUtility.getCell("Status", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("SR Copy Hyperlink", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			
			tbl.addCell(pdfUtility.getCell("Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdfUtility.getCell("Time", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdfUtility.getCell("Name", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdfUtility.getCell("Mobile Number", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			int counter=0;
			for(Service service:serviceList){
				counter++;
				String completionDate="";
				String completionTime="";
				String signedByName="";
				String signedByNum="";
				String srCopyLink="";
				
				if(service.getServiceCompletionDate()!=null){
					completionDate=sdf.format(service.getServiceCompletionDate());
					completionTime=sdf.format(service.getServiceCompletionDate());
				}
				
				if(service.getCustomerSignName()!=null){
					signedByName=service.getCustomerSignName();
				}
				if(service.getCustomerSignNumber()!=null){
					signedByNum=service.getCustomerSignNumber();
				}
				
				font8boldul.setColor(BaseColor.BLUE);
				srCopyLink= "pdfCustserjob"+"?Id="+service.getId()+"&"+"companyId="+service.getCompanyId();
				Phrase phrase = new Phrase();
				Chunk chunk = new Chunk("SR Copy-"+service.getCount(),font8boldul);
				chunk.setAnchor(srCopyLink);
				phrase.add(chunk);
				PdfPCell cell=new PdfPCell(phrase);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				tbl.addCell(pdfUtility.getCell(counter+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(sdf.format(service.getServiceDate()), font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(completionDate, font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(completionTime, font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(service.getCount()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(service.getProductName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(signedByName, font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(signedByNum, font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(service.getStatus(), font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(cell);
				
				
			}
			
			try {
				document.add(tbl);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		
	}

	private void createBankDetailsAndSignatoryTable() {
		
		PdfPTable tbl = new PdfPTable(1);
		tbl.setWidthPercentage(100);
		
		CompanyPayment bankdetail1=null;
		CompanyPayment bankdetail2=null;
		if(compPayList!=null&&compPayList.size()!=0){
			int tblCol=0;
			if(comppayment!=null){
				bankdetail1=comppayment;
			}
			
			if(compPayList.size()==1){
				tblCol=1;
				if(bankdetail1==null){
					bankdetail1=compPayList.get(0);
				}
			}else if(compPayList.size()>1){
				tblCol=2;
				for(CompanyPayment obj:compPayList){
					if(bankdetail1!=null){
						if(obj.getCount()!=bankdetail1.getCount()){
							bankdetail2=obj;
							break;
						}
					}else{
						bankdetail1=obj;
					}
				}
			}
			
			PdfPTable bankTbl = new PdfPTable(tblCol);
			bankTbl.setWidthPercentage(100);
//			bankTbl.addCell(pdfUtility.getCell("รายละเอียดธนาคาร / Bank Details", font10bold, Element.ALIGN_CENTER, 0, tblCol, 0));
			
			tbl.addCell(pdfUtility.getCell("รายละเอียดธนาคาร / Bank Details", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			float[] columnWidths3 = { 1.7f, 0.35f, 4.5f };
			
			PdfPTable leftBankTbl = new PdfPTable(3);
			leftBankTbl.setWidthPercentage(100f);
			try {
				leftBankTbl.setWidths(columnWidths3);
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
			
			leftBankTbl.addCell(pdfUtility.getCell("Name", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(bankdetail1.getPaymentBankName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			leftBankTbl.addCell(pdfUtility.getCell("Branch", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(bankdetail1.getPaymentBranch(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			leftBankTbl.addCell(pdfUtility.getCell("A/c No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(bankdetail1.getPaymentAccountNo(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			bankTbl.addCell(leftBankTbl);
//			tbl.addCell(leftBankTbl);
			
			if(tblCol==2){
				PdfPTable rightBankTbl = new PdfPTable(3);
				rightBankTbl.setWidthPercentage(100f);
				try {
					rightBankTbl.setWidths(columnWidths3);
				} catch (DocumentException e2) {
					e2.printStackTrace();
				}
				
				rightBankTbl.addCell(pdfUtility.getCell("Name", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(bankdetail2.getPaymentBankName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				rightBankTbl.addCell(pdfUtility.getCell("Branch", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(bankdetail2.getPaymentBranch(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				rightBankTbl.addCell(pdfUtility.getCell("A/c No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(bankdetail2.getPaymentAccountNo(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				bankTbl.addCell(rightBankTbl);
//				tbl.addCell(rightBankTbl);
			}
			
			PdfPCell bankCell=new PdfPCell(bankTbl);
			bankCell.setBorder(0);
			tbl.addCell(bankCell);
		}
		
		
		PdfPTable signatoryTbl = new PdfPTable(3);
		signatoryTbl.setWidthPercentage(100);
		
//		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
//		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		
		signatoryTbl.addCell(pdfUtility.getCell("ลูกค้า ผู้รับของ/Receiver", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell("ผู้รับเงิน ผู้ส่งของ/Delivered and Received By", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell("ลายเซ็นผู้มีอำนาจ /Authorized Signature", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		tbl.addCell(signatoryTbl);
		
		try {
			document.add(tbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
	private void createDeclarationTable() {
		System.out.println("in createDeclarationTable");
		PdfPTable tbl = new PdfPTable(1);
		tbl.setWidthPercentage(100);

		/**
		 * @author Ashwini Patil
		 * @since 31-01-2022
		 * Innovative client want hardcoded declaration for Tax Invoice and Receipt	
		 */
//		if(thaiPdfFlag){
			Declaration declaration=ofy().load().type(Declaration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("status", true).first().now();

			String msg=declaration.getDeclaratiomMsg();
			Phrase pdeclaration=new Phrase(msg,font13bold);
			PdfPCell declarationCell=new PdfPCell(pdeclaration);
			declarationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			tbl.addCell(declarationCell);			
//		}else{
//			if(invoiceentity.getDeclaration()!=null&&!invoiceentity.getDeclaration().equals("")){
//				tbl.addCell(pdfUtility.getCell(invoiceentity.getDeclaration(), font10bold, Element.ALIGN_LEFT, 0, 0, 0));
//			}else{
//				tbl.addCell(pdfUtility.getCell(" ", font10bold, Element.ALIGN_LEFT, 0, 0, 50));
//			}
//		}
		
		try {
			document.add(tbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Developer : Jayshree Date: 15 Nov 2017 Description : This method is to
	 * add the service schedule list in invoice on next Page
	 * 
	 */
	private void createServiceScheduleList() {

		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		try {
			document.add(nextpage);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (upcflag == true) {
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		String terms = "Service Details :";
		Phrase term = new Phrase(terms, font12bold);

		Paragraph para1 = new Paragraph();
		para1.add(term);
		para1.setAlignment(Element.ALIGN_CENTER);

		String decsInfo = "";

		if (invoiceentity.getComment() != null) {
			decsInfo = invoiceentity.getComment();
		}

		Phrase desphase = new Phrase(decsInfo, font8);
		Paragraph para2 = new Paragraph();
		para2.add(desphase);

		try {
			document.add(para1);
			document.add(para2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// addServiceDetail();
		// By jayshree Date 15/11/2017
		// changes are made for to load the super product

		String desc = "";
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			ServiceProduct sup = ofy()
					.load()
					.type(ServiceProduct.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("productCode",
							invoiceentity.getSalesOrderProducts().get(i)
									.getProdCode().trim()).first().now();

			Phrase service = new Phrase("Service Product :"
					+ sup.getProductName(), font12bold);

			Paragraph servicePara = new Paragraph();
			servicePara.add(service);

			desc = sup.getComment();
			Phrase prodDesc = new Phrase(desc, font8);
			Paragraph parades = new Paragraph(prodDesc);

			PdfPTable serviceTable = new PdfPTable(4);
			serviceTable.setWidthPercentage(100);

			Phrase serno = new Phrase("Service No", font6bold);
			Phrase serDate = new Phrase("Service Date", font6bold);
			Phrase serStatus = new Phrase("Service Status", font6bold);
			Phrase custBranch = new Phrase("Customer Branch", font6bold);

			PdfPCell sernocell = new PdfPCell(serno);
			PdfPCell serDatecell = new PdfPCell(serDate);
			PdfPCell serStatuscell = new PdfPCell(serStatus);
			PdfPCell custBranchcell = new PdfPCell(custBranch);

			serviceTable.addCell(sernocell);
			serviceTable.addCell(serDatecell);
			serviceTable.addCell(serStatuscell);
			serviceTable.addCell(custBranchcell);
			serviceTable.setSpacingBefore(10f);

			Phrase chunk = null;

			System.out.println("service Schedule list size"
					+ con.getServiceScheduleList().size());

			for (int k = 0; k < con.getServiceScheduleList().size(); k++) {

				if (sup.getCount() == con.getServiceScheduleList().get(k)
						.getScheduleProdId()) {
					chunk = new Phrase(con.getServiceScheduleList().get(k)
							.getScheduleServiceNo()
							+ "", font8);
					PdfPCell srno = new PdfPCell(chunk);

					String serviceDt = sdf.format(con.getServiceScheduleList()
							.get(k).getScheduleServiceDate());
					chunk = new Phrase(serviceDt, font8);
					PdfPCell srDate = new PdfPCell(chunk);

					chunk = new Phrase("Scheduled", font8);
					PdfPCell srStatus = new PdfPCell(chunk);

					chunk = new Phrase(con.getServiceScheduleList().get(k)
							.getScheduleProBranch(), font8);
					PdfPCell srBranch = new PdfPCell(chunk);

					serviceTable.addCell(srno);
					serviceTable.addCell(srDate);
					serviceTable.addCell(srStatus);
					serviceTable.addCell(srBranch);
				}
			}

			try {
				document.add(servicePara);
				document.add(parades);
				document.add(serviceTable);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Ends for Jayshree
	 */


	private void createProductDetailsForRate() {
		
//		createHeaderForRateContracts();
		
		boolean unoOfMeasurementColumnFlag = false;
		boolean discColumnFlag = false;
		
		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			/**
			 * @author Vijay Date :- 24-03-2022
			 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
			 */
			if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
					invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
				continue;
			}
			
			rateAmountProd = rateAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice();

			amountAmountProd = amountAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmountProd = discAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmountProd = totalAssAmountProd + taxValue;
			
			if(invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement()!=null &&
					!invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim().equals("")){
				unoOfMeasurementColumnFlag = true;
				System.out.println("UOM "+invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim());
			}
			
			double quantity=0;
			try {
				if(invoiceentity.getSalesOrderProducts().get(i).getArea()!=null && 
						!invoiceentity.getSalesOrderProducts().get(i).getArea().equals("")){
					quantity = Double.parseDouble(invoiceentity.getSalesOrderProducts().get(i).getArea());
					if(quantity>0){
						qtycolumnFlag = true;
					}
				}
				
			} catch (Exception e) {
				
			}
			
			if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()>0){
				discColumnFlag = true;
			}
			
		}


		PdfPTable productTable = new PdfPTable(8);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column8ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		productTable.addCell(pdfUtility.getPdfCell("Sr No", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		
		if(numberRangeConfig!=null && numberRangeConfig.isGstApplicable()){
			productTable.addCell(pdfUtility.getPdfCell("Services", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
			productTable.addCell(pdfUtility.getPdfCell("SAC", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		}
		else{
			productTable.addCell(pdfUtility.getPdfCell("Services", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 2, 0,-1,0,-1,-1,-1,-1));
		}
		System.out.println("productTable size"+productTable.getHeaderRows());
		int colspan = 3;
		if(unoOfMeasurementColumnFlag){
			System.out.println("unoOfMeasurementColumnFlag "+unoOfMeasurementColumnFlag);
			productTable.addCell(pdfUtility.getPdfCell("UOM", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
			colspan = colspan-1;
		}
		if(qtycolumnFlag){
			productTable.addCell(pdfUtility.getPdfCell(qtylabel, font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, colspan, 0,-1,0,-1,-1,-1,-1));
			colspan = colspan-1;
			System.out.println("qtycolumnFlag "+qtycolumnFlag);
		}
		productTable.addCell(pdfUtility.getPdfCell("Rate", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, colspan, 0,-1,0,-1,-1,-1,-1));
		System.out.println("colspan "+colspan);
		if(discColumnFlag){
			System.out.println("discColumnFlag"+discColumnFlag);
			productTable.addCell(pdfUtility.getPdfCell("Disc", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
			productTable.addCell(pdfUtility.getPdfCell("Amount", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		}
		else{
			productTable.addCell(pdfUtility.getPdfCell("Amount", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 2, 0,-1,0,-1,-1,-1,-1));

		}
//		productTable.setHeaderRows(3);
		
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			/**
			 * @author Vijay Date :- 24-03-2022
			 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
			 */
			if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
					invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
				continue;
			}
			
			int srNoVal = i + 1;
			productTable.addCell(pdfUtility.getPdfCell(srNoVal+"", font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));

			/**
			 * Date 06/06/2018 By vijay
			 * Des :- Service warranty if exist then it will show in pdf
			 * Requirement :- Neatedge Services
			 */
			String productName = "";
			if(invoiceentity.getSalesOrderProducts().get(i).getWarrantyPeriod()!=0){
				productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim()
						+"\n"+" Warranty Period - "+invoiceentity.getSalesOrderProducts().get(i).getWarrantyPeriod();
			}else{
				productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim();
			}
			/**
			 * ends here
			 */
			

			String hsnCode = "";
			String uom = "";

			ServiceProduct serviceProduct = null;
			
			if(numberRangeConfig!=null && numberRangeConfig.isGstApplicable()){
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null && 
						!invoiceentity.getSalesOrderProducts().get(i).getHsnCode().equals("") &&
						invoiceentity.getSalesOrderProducts().get(i).getHsnCode().trim().length() > 0) {
						hsnCode = invoiceentity.getSalesOrderProducts().get(i).getHsnCode().trim();
				}
//				else {
//
//					serviceProduct = ofy()
//							.load()
//							.type(ServiceProduct.class)
//							.filter("companyId", comp.getCompanyId())
//							.filter("productCode",
//									invoiceentity.getSalesOrderProducts().get(i)
//											.getProdCode()).first().now();
//					
//					if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
//						hsnCode = serviceProduct.getHsnNumber();
//					} 
//				}
			}
			

			/**
			 * Date 11-04-2018 By vijay
			 * if UOM is not present in invoice then i will get it from master
			 */
			if(invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement()!=null &&
					!invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim().equals("")){
				 uom = invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim();
			}
//			else{
//				if(serviceProduct==null){
//					serviceProduct = ofy()
//							.load()
//							.type(ServiceProduct.class)
//							.filter("companyId", comp.getCompanyId())
//							.filter("productCode",
//									invoiceentity.getSalesOrderProducts().get(i)
//											.getProdCode().trim()).first().now();
//					if (serviceProduct.getUnitOfMeasurement() != null) {
//						 uom = serviceProduct.getUnitOfMeasurement().trim();
//						 logger.log(Level.SEVERE,"In product master UOM ="+serviceProduct.getUnitOfMeasurement().trim());
//					}
//				}
//				else{
//					if (serviceProduct.getUnitOfMeasurement() != null) {
//						 uom = serviceProduct.getUnitOfMeasurement().trim();
//						 logger.log(Level.SEVERE,"In product master UOM ="+serviceProduct.getUnitOfMeasurement().trim());
//					}
//				}
//				
//			}
			
			/**
			 * ends here
			 */
			
			
			String qty = invoiceentity.getSalesOrderProducts().get(i).getArea();
			if(numberRangeConfig!=null && numberRangeConfig.isGstApplicable()){
				productTable.addCell(pdfUtility.getPdfCell(productName, font6, Element.ALIGN_LEFT,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(hsnCode, font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));

			}
			else{
				productTable.addCell(pdfUtility.getPdfCell(productName, font6, Element.ALIGN_LEFT,null, 0, 2, 0,-1,0,-1,-1,-1,-1));

			}

			if(unoOfMeasurementColumnFlag){
				productTable.addCell(pdfUtility.getPdfCell(uom, font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));

			}
			
			if(qtycolumnFlag){
				productTable.addCell(pdfUtility.getPdfCell(qty, font6, Element.ALIGN_CENTER,null, 0, colspan, 0,-1,0,-1,-1,-1,-1));
			}



			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					int borderWidthBottom = -1;
					if(invoiceentity.getSalesOrderProducts().size() > 1){
						borderWidthBottom = 0;
					}
					productTable.addCell(pdfUtility.getPdfCell(df.format(rateAmountProd), font6, Element.ALIGN_RIGHT,null, 0, colspan, 0,-1,0,-1,borderWidthBottom,-1,-1));

					
				} else {
					if(i == invoiceentity.getSalesOrderProducts().size()-1){
						productTable.addCell(pdfUtility.getPdfCell("", font6, Element.ALIGN_RIGHT,null, 0, colspan, 0,-1,0,0,-1,-1,-1));
					}else{
						productTable.addCell(pdfUtility.getPdfCell("", font6, Element.ALIGN_RIGHT,null, 0, 0, colspan,-1,0,0,0,-1,-1));

					}
				}
			} else {
				String rateAmount = df.format(invoiceentity	.getSalesOrderProducts().get(i).getPrice());
				productTable.addCell(pdfUtility.getPdfCell(rateAmount, font6, Element.ALIGN_RIGHT,null, 0, colspan, 0,-1,0,0,-1,-1,-1));

			}

			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i).getQuantity();

			totalAmount = totalAmount + amountValue;
			String discount = "";
			int discountAmtColspan = 2;
			if(discColumnFlag){
				/** date 06-02-2018 added by komal for consolidate price **/
				if(consolidatePrice || invoiceentity.isConsolidatePrice()){
					if (i == 0) {
						discount = df.format(discAmountProd);
						int borderWidthBottom = -1;
						if(invoiceentity.getSalesOrderProducts().size() > 1){
							borderWidthBottom = 0;
						}
						productTable.addCell(pdfUtility.getPdfCell(discount, font6, Element.ALIGN_RIGHT,null, 0, 0, 0,-1,0,0,borderWidthBottom,-1,-1));

					} else {
						if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
							productTable.addCell(pdfUtility.getPdfCell("", font6, Element.ALIGN_RIGHT,null, 0, 0, 0,-1,0,0,-1,-1,-1));
						}else{
							productTable.addCell(pdfUtility.getPdfCell("", font6, Element.ALIGN_RIGHT,null, 0, 0, 0,-1,0,0,0,-1,-1));

						}
					}
					discountAmtColspan = discountAmtColspan -1;
				} else {
					String discountAmount = "";
					if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()>0) {
						discountAmount = decimalformat.format(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount());
					}
					productTable.addCell(pdfUtility.getPdfCell(discountAmount, font6, Element.ALIGN_RIGHT,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
					discountAmtColspan = discountAmtColspan - 1;
				}
			}

			


			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					int borderWidthBottom = -1;
					if(invoiceentity.getSalesOrderProducts().size() > 1)
						borderWidthBottom = 0;
					
					productTable.addCell(pdfUtility.getPdfCell(df.format(totalAssAmountProd), font6, Element.ALIGN_RIGHT,null, 0, discountAmtColspan, 0,-1,0,-1,borderWidthBottom,-1,-1));

					
				} else {
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						productTable.addCell(pdfUtility.getPdfCell("", font6, Element.ALIGN_RIGHT,null, 0, discountAmtColspan, 0,-1,0,0,-1,-1,-1));

					}else{
						productTable.addCell(pdfUtility.getPdfCell("", font6, Element.ALIGN_RIGHT,null, 0, discountAmtColspan, 0,-1,0,0,0,-1,-1));

					}
				}
			} else {
				productTable.addCell(pdfUtility.getPdfCell(df.format(taxValue), font6, Element.ALIGN_RIGHT,null, 0, discountAmtColspan, 0,-1,0,-1,-1,-1,-1));

			}

			/**
			 * Date 7-4-2018
			 * By jayshree
			 * Des.to add type as premise detail
			 */
			String premisesVal = "";
			String premiseValNew="";
			
			if(contractTypeAsPremisedetail==true){
				for (int j = 0; j < con.getItems().size(); j++) {
					if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
							.getItems().get(j).getPrduct().getCount()) {
						premisesVal = con.getItems().get(j).getPremisesDetails();
					}
					}
				if(premisesVal!=null && !premisesVal.equals("")){
					premiseValNew=premisesVal;
				}
				else if(con.getType()!=null && !con.getType().equals("")){
					premiseValNew=con.getType();
				}
				else{
					premiseValNew="N A";
				}
				
				
			}else{
				System.out.println("contractTypeAsPremisedetail:::11112222");
				for (int j = 0; j < con.getItems().size(); j++) {
					if( invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() != 0 ){
						if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
								.getItems().get(j).getPrduct().getCount()
								&& invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
							premiseValNew = con.getItems().get(j).getPremisesDetails();
						}
					}else{
						if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
								.getItems().get(j).getPrduct().getCount()) {
							premiseValNew = con.getItems().get(j).getPremisesDetails();
						}
					}
				

			}
			}
			if (premiseValNew != null && !premiseValNew.equals("")) {
					productTable.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_LEFT,null, 0, 1, 0,0,0,-1,-1,-1,-1));
					String primicesValue = "Premise Details : "+ premiseValNew;
					productTable.addCell(pdfUtility.getPdfCell(primicesValue, font8, Element.ALIGN_LEFT,null, 0, 7, 0,0,0,-1,-1,-1,-1));
			} 

			/**
			 * nidhi
			 * for print model and serial number
			 */
			int cnnt = 0;
			PdfPCell proModelcell = null ,proSerialNocell = null; 
			String proModelNo = "";
			String proSerialNo = "";
			if(printModelSerailNoFlag){
				if(invoiceentity.getSalesOrderProducts().get(i).getProModelNo()!=null && 
						invoiceentity.getSalesOrderProducts().get(i).getProModelNo().trim().length() >0){
					proModelNo = invoiceentity.getSalesOrderProducts().get(i).getProModelNo();
				}
				
				if(invoiceentity.getSalesOrderProducts().get(i).getProSerialNo()!=null && 
					invoiceentity.getSalesOrderProducts().get(i).getProSerialNo().trim().length() >0){
					proSerialNo = invoiceentity.getSalesOrderProducts().get(i).getProSerialNo();
				}
				
				if(proModelNo.length()>0){
					Phrase modelValPhrs = new Phrase(
							"Model No : " + proModelNo, font8);
					proModelcell = new PdfPCell(modelValPhrs);
					proModelcell.setColspan(3);
					++cnnt;
				}
				if(proSerialNo.length()>0){
					Phrase serialValPhrs = new Phrase(
							"Serial No : " + proSerialNo, font8);
					proSerialNocell = new PdfPCell(serialValPhrs);
					proSerialNocell.setColspan(4);
					++cnnt;
				}
				
				if(cnnt>1 ){
					proSerialNocell.setColspan(5);
				}else if(proModelNo.length()>0){
					proModelcell.setColspan(9);
				}else if(proSerialNocell!=null){
					proSerialNocell.setColspan(9);
				}
				
				if(cnnt>0);
				{
					Phrase blankValPhrs = new Phrase("", font8);
					PdfPCell premiseCell = new PdfPCell(blankValPhrs);
					premiseCell.setColspan(1);

					productTable.addCell(premiseCell);
					if(proModelcell!=null){
						productTable.addCell(proModelcell);
					}
					if(proSerialNocell!=null){
						productTable.addCell(proSerialNocell);
					}
				}
			}
			/**
			 * end
			 */
			
		}
	   

		// manually set the width (as an example to page content width)
		float containerWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
		productTable.setTotalWidth(containerWidth);
		productTable.setLockedWidth(true);

		// get height of table before and after adding it to the document
		System.out.println("Height before adding: " + productTable.getTotalHeight());
		System.out.println("rowhight "+productTable.getRowHeight(1));
		System.out.println("rowhight "+productTable.getRowHeight(2));

		pdfUtility.checkTableSizeAndAddBlanks(productTable, 9,260);
				
		PdfPCell tab1 = new PdfPCell(productTable);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	

	}

	private void createHeaderForRateContracts() {

		PdfPTable headerproductTable = new PdfPTable(8);
		headerproductTable.setWidthPercentage(100);
		try {
			headerproductTable.setWidths(column8ProdCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		headerproductTable.addCell(pdfUtility.getPdfCell("Sr No", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell("Services", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell("SAC", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell("UOM", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell(qtylabel, font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell("Rate", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell("Disc", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
		headerproductTable.addCell(pdfUtility.getPdfCell("Amount", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));

		try {
			document.add(headerproductTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createFooterOtherChargesPart2() {
		// TODO Auto-generated method stub

		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		double totalOtherCharges = 0;
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			totalOtherCharges = totalOtherCharges
					+ invoiceentity.getOtherCharges().get(i).getAmount();
		}

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);

		innerRightTable.addCell(pdfUtility.getPdfCell("Other Charges", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(decimalformat.format(totalOtherCharges), font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,-1,0,0,0,0,-1));

		
		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		otherChargesTable.addCell(netPayableCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountBeforeNetPayPart(double roundOff) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		
		innerRightTable.addCell(pdfUtility.getPdfCell("Other Charges", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(decimalformat.format(roundOff), font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,-1,0,0,0,0,-1));

		
		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountAfterPart(double discount) {
		logger.log(Level.SEVERE,"in createFooterDisCountAfterPart");
		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(pdfUtility.getPdfCell("Discount Amt", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(decimalformat.format(discount), font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,-1,0,0,0,0,-1));


		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		amountTable.addCell(netPayableCell);
		
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterOtherChargesPart() {
		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");
		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase blank = new Phrase("", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		otherChargesTable.addCell(blankCell);

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// for (int i = 0; i < invoiceentity.getBillingOtherCharges().size();
		// i++) {
		Phrase chargeName, taxes, assVal;
		PdfPCell pCell;
		// if(i==0){
		chargeName = new Phrase("Charge Name", font10bold);
		taxes = new Phrase("Taxes", font10bold);
		assVal = new Phrase("Amt", font10bold);
		pCell = new PdfPCell(chargeName);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(taxes);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(assVal);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		// }else{
		chargeName = new Phrase("Transport", font10);
		taxes = new Phrase("SGST@9/CGST@9"/*
										 * invoiceentity.getBillingOtherCharges()
										 * .get(i).get()
										 */, font10);
		assVal = new Phrase("100", font10);
		pCell = new PdfPCell(chargeName);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(taxes);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(assVal);
		otherCharges.addCell(pCell);
		// }

		// }
		PdfPCell leftCell = new PdfPCell(otherCharges);
		otherChargesTable.addCell(leftCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createTermsAndCondition() {

		String friends = "";
		Declaration declaration=ofy().load().type(Declaration.class)
				.filter("companyId", invoiceentity.getCompanyId())
				.filter("status", true).first().now();

		
		String msg="";
		if(declaration!=null&&declaration.getDeclaratiomMsg()!=null)
			msg=declaration.getDeclaratiomMsg();


		int remainingLinesForTerms = 5;

		if (friends.length() > (138 * 5)) {
			friends = friends.substring(0, (138 * 5));
		}
		Phrase termNcondVal=null;

		logger.log(Level.SEVERE, "friends "+friends);
		termNcondVal = new Phrase("" + msg, font10bold);//Remarks: \n
		

		logger.log(Level.SEVERE, "comppayment "+comppayment);

			
			PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
			termNcondValCell.setBorderWidthBottom(0);
			termNcondValCell.setBorderWidthTop(0);
			System.out.println("HEIGHT == "+termNcondValCell.getHeight() );
			PdfPTable pdfTable = new PdfPTable(1);
			pdfTable.setWidthPercentage(100);
			pdfTable.addCell(termNcondValCell);
			
			
			Phrase blankPhrase = new Phrase(" ", font10bold);
			PdfPCell blank = new PdfPCell(blankPhrase);
			blank.setBorderWidthBottom(0);
			blank.setBorderWidthTop(0);
			remainingLinesForTerms = remainingLinesForTerms
					- (friends.length() / (138));
			System.out.println("remainingLinesForTerms" + remainingLinesForTerms);
			for (int i = 0; i < remainingLinesForTerms; i++) {
				pdfTable.addCell(blank);
			}
			PdfPCell pdfPcell = new PdfPCell(pdfTable);
			pdfPcell.setBorder(0);
			pdfPcell.setBorderWidthBottom(1);
			pdfPcell.setFixedHeight(66f);
			System.out.println("Remakrs table height"+pdfPcell.getHeight());

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(pdfPcell);
			table1.setKeepTogether(true);
			
			try {
				document.add(table1);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			


	}

	private void createFooterAmountInWords_NetPayPart() {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 *  Added By Priyanka : 19-08-2021
		 *  Des : For Thai client no need to print rupees word.
		 */
		String amtInWordsVal="";
		
		/**
		 * @author Vijay Date :- 29-09-2021
		 * Des :- if below process config is active then amount will print in hundread structure  
		 */
		if(AmountInWordsHundreadFormatFlag){
			amtInWordsVal = EnglishNumberToWords.convert(invoiceentity.getNetPayable());
		}
		else{
			amtInWordsVal = ServiceInvoicePdf.convert(invoiceentity.getNetPayable());
		}
		/**
		 * ends here
		 */
		
		if(thaiPdfFlag){
			/**
			 * @author Anil
			 * @since 19-01-2022
			 * Amount in words to be in thai language using BahtText. Raised by Nithila and Nitin Sir
			 */
//			amtInWordsVal = "Amount in Words : "+ amtInWordsVal	+ " Only/-";
			amtInWordsVal = bahtText.getBath(df.format(invoiceentity.getNetPayable()));
			System.out.println("amtInWordsVal="+amtInWordsVal+"and invoiceentity.getNetPayable()"+invoiceentity.getNetPayable());
		}else if(thaiFontFlag){
			amtInWordsVal = "Amount in Words : "+ amtInWordsVal	+ " Only/-";
			System.out.println("amtInWordsVal="+amtInWordsVal);
			
		}else if(AmountInWordsHundreadFormatFlag){
			amtInWordsVal = "Amount in Words : "
					+ amtInWordsVal	+ " Only/-";
		}
		else{
			amtInWordsVal = "Amount in Words : Rupees "
					+ amtInWordsVal	+ " Only/-";
		}
		
		/**
		 *  End
		 */

		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		
		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);

		innerRightTable.addCell(pdfUtility.getPdfCell("Net Payable", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		innerRightTable.addCell(pdfUtility.getPdfCell(df.format(invoiceentity.getNetPayable()), font10bold, Element.ALIGN_RIGHT,null, 0, 0, 0,-1,0,0,0,0,-1));

		
		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterAmountPart() {
		logger.log(Level.SEVERE,"in createFooterAmountPart");
		double totalAssAmount = 0;
		double rateAmount = 0;
		double amountAmount = 0;
		double discAmount = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			rateAmount = rateAmount
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice();

			amountAmount = amountAmount
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmount = discAmount
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmount = totalAssAmount + taxValue;
		}
		
	
		PdfPTable productTable = new PdfPTable(11);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column11SerProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		productTable.addCell(pdfUtility.getPdfCell("Total", font10bold, Element.ALIGN_LEFT,null, 0,4, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell("", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell(df.format(totalTaxableValue)+"", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell("", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell(df.format(totalCGST)+"", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell("", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell(df.format(totalSGST)+"", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,0));
		productTable.addCell(pdfUtility.getPdfCell(df.format(grandTotal)+"", font10, Element.ALIGN_CENTER,null, 0, 0, 0,-1,1,-1,-1,1,1));

		
		try {
			document.add(productTable);
			logger.log(Level.SEVERE,"productTable added");
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createtotalAmount() {

		PdfPTable productTable = new PdfPTable(2);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase totalamount = new Phrase("Total Ass Amount", font10bold);
		PdfPCell totalAmountCell = new PdfPCell(totalamount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountCell);

		Phrase totalValamount = new Phrase(df.format(0.00), font10);
		PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
		totalAmountValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountValCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// /////////////////////////////////////////// Ajinkya Code Start Here
	// //////////////////
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,725f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	// ////////////////////////////// Code End Here
	// /////////////////////////////////

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,40f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	// ///////////////////////// Ajinkya Code end Here ///////////////

	private void createBlankforUPC() {
		
		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setSpacingAfter(5f);
		mytbale.setWidthPercentage(100f);
	

		// ends here
		String titlepdf = "";


		if (nonbillingInvoice == true) {
			if (invoiceentity.getBillingTaxes().size() == 0) {
				/**
				 * Date 25-9-2020 by Amol commented this line raised by Rahul
				 * Tiwari.
				 **/
				titlepdf = "Estimate";

				/**
				 * Date 23-12-2020 by Priyanka Bhagwat Des: Om pest control-
				 * replace invoice word to estimate on non billing invoice pdf.
				 */

				// titlepdf = "Invoice";

				/**
				 * @author Anil @since 09-04-2021 If non billing process
				 *         configuration is active and no tax selected then for
				 *         Proforma invoice it should print title as Proforma
				 *         Invoice instead Estimate Raised by Ashwini for Ultra
				 *         Pest Control
				 */
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
				 if(changeTitle){
						titlepdf = "Invoice";
					}else{
						titlepdf = "Proforma Invoice";
					}
					
					
				}

			} else {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
					{
					if(changeTitle){
						titlepdf = "Invoice";
					}else{
						titlepdf = "Proforma Invoice";
					}
			}else{
					titlepdf = "Tax Invoice";
			}
			}
		} else {
			/**
			 * @author Anil @since 12-04-2021
			 * For ultra pest control, if no tax is selected and non billing process configurationj is off then print 
			 * Invoice on PDF else it will be Tax Invoice
			 * Raised by Ashwini 
			 */
//			if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
//				titlepdf = "Proforma Invoice";
//			else
//				titlepdf = "Tax Invoice";
			
			titlepdf = "Invoice";
			if (invoiceentity.getBillingTaxes().size() == 0) {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
					if(changeTitle){
						titlepdf = "Invoice";
					}else{
						titlepdf = "Proforma Invoice";
					}
				}

			} else {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE)){
					if(changeTitle){
						titlepdf = "Invoice";
					}else{
						titlepdf = "Proforma Invoice";
					}
				}else{
					titlepdf = "Tax Invoice";
				}
			}

		}
		
		/**
		 * @author Anil @since 01-10-2021
		 */
		titlepdf=pdfUtility.getInvoiceTitle(invoiceentity, branchDt, titlepdf);
		logger.log(Level.SEVERE,"After getInvoiceTitle Method : "+titlepdf +" | "+pdfUtility.printBankDetailsFlag);
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * if we are printing invoice for thai client then title will be hard coded as provided by client
		 * raised by Nitin And Nithila
		 */
		System.out.println("second thaiPdfFlag="+thaiPdfFlag);
		if(thaiPdfFlag){
			System.out.println("In second thaiPdfFlag invoiceTitle= "+invoiceTitle);
			
			if(!invoiceTitle.equals("")){
				titlepdf=invoiceTitle;
			}
		}
		
		Phrase titlephrase = new Phrase(titlepdf, titlefont); //By Ashwini Patil
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		titlecell.setVerticalAlignment(Element.ALIGN_BOTTOM); //set by ashwini patil 
		
		
		
		//=================================E-Invoice Related Information commented for now===================================
//		PdfPTable IRNtable = new PdfPTable(2);
//		IRNtable.setWidthPercentage(100f);
//		float[] tblcol6width = {8f,2f};
//		try {
//			IRNtable.setWidths(tblcol6width);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//		String irn="",ackNo="",ackDate="";
//		if(invoiceentity.getIRN()!=null)
//			irn=invoiceentity.getIRN();
//		if(invoiceentity.getIrnAckNo()!=null)
//			ackNo=invoiceentity.getIrnAckNo();
//		if(invoiceentity.getIrnAckDate()!=null&&!invoiceentity.getIrnAckDate().equals("")) {
//			 String sDate=invoiceentity.getIrnAckDate().substring(0, 10);
//			 logger.log(Level.SEVERE,"sDate="+sDate);
//			 Date d=null;
//			 try {
//				d=new SimpleDateFormat("YYYY-MM-DD").parse(sDate);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}  
//			 SimpleDateFormat simpleDateFmt = new SimpleDateFormat("dd-MM-yyyy");
//			 TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//			 simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
//			 if(d!=null)
//				 ackDate=simpleDateFmt.format(d);
//		}
//		
//		 BarcodeQRCode barcodeQRCode = new BarcodeQRCode("https://memorynotfound.com", 1000, 1000, null);
//		PdfPCell qrCodeCell = null;
//		if(invoiceentity.getIrnQrCode()!=null&&!invoiceentity.getIrnQrCode().equals("")) {
//			BarcodeQRCode barcodeQRCode = new BarcodeQRCode(invoiceentity.getIrnQrCode(), 1000, 1000, null);
//		     
//			Image codeQrImage=null;
//				try {
//					codeQrImage = barcodeQRCode.getImage();
//				} catch (BadElementException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//		        codeQrImage.scaleAbsolute(30, 30);    			
//			
//			
//			qrCodeCell = new PdfPCell(codeQrImage);
//			qrCodeCell.setBorder(0);
//			qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			qrCodeCell.setVerticalAlignment(Element.ALIGN_TOP);
//			qrCodeCell.setRowspan(2);
//		
//		}						
//		IRNtable.addCell(pdfUtility.getCell("IRN: f6e990fb8ea3856ea317bf5d33bf636f5542a5e7a170a97a060e4dff8f8cc701", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		if(qrCodeCell!=null)
//			IRNtable.addCell(qrCodeCell);
//		else
//			IRNtable.addCell(pdfUtility.getCell("", font8bold, Element.ALIGN_CENTER, 2, 0, 0)).setBorder(0);				
//		IRNtable.addCell(pdfUtility.getCell("Acknowledgement No and Date: "+ackNo+" / "+ackDate, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		
//		PdfPCell irnCell = new PdfPCell();//28-10-2022
//		irnCell.addElement(IRNtable);
//		irnCell.setBorder(0);
//		irnCell.setVerticalAlignment(Element.ALIGN_CENTER);  
//		irnCell.setColspan(3);
//		
		//==================================================================================

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		
		titlepdftable.setWidthPercentage(100);
	
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
//		if(invoiceentity.getIRN()!=null&&!invoiceentity.getIRN().equals(""))
//			titlepdftable.addCell(irnCell);//28-10-2022	
		
		

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(10f);
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * Need to add invoice copy name 
		 * raised by Nithila and Nitin for Innovative
		 */
		if(thaiPdfFlag){
			if(!copyTitle.equals("")){
				parent.addCell(pdfUtility.getCell(copyTitle, font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			}
		}

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		/*
		 * Commented by Ashwini
		 */

		
		/*
		 * Date:30/07/2018
		 * Developer:Ashwini
		 */
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , invoiceentity.getCompanyId())){
//			 Paragraph blank1 =new Paragraph();
//			    blank1.add(Chunk.NEWLINE);
//			    
//			try {
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(mytbale);
//				document.add(parent);
//				
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
//		}else{

		try {
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
			document.add(mytbale);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
//	}
	/*
	 * End by Ashwini
	 */

	}

	private void createLogo(Document doc, Company comp2) {

		// ********************logo for server ********************
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 750f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,785f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

	}

	private void createFooterLastPart(String preprintStatus) {
		// TODO Auto-generated method stub
		PdfPTable bottomTable = new PdfPTable(3);
		bottomTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			bottomTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);


		String gstin = "", gstinText = "";
		// rohan added this code for universal pest
		if (UniversalFlag) {
			if (con.getGroup().equalsIgnoreCase("Universal Pest Control Pvt. Ltd.")) {
				if (!preprintStatus.equalsIgnoreCase("Plane")) {
					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
						if (comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")) {
							Phrase articalType = new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()+ " : "+ comp.getArticleTypeDetails().get(i).getArticleTypeValue(), font10bold);
							PdfPCell articalTypeCell = new PdfPCell();
							articalTypeCell.setBorder(0);
							articalTypeCell.addElement(articalType);
							leftTable.addCell(articalTypeCell);
						}
					}
				}
			}
		}else{
			// if (!preprintStatus.equalsIgnoreCase("Plane")) {
			// leftTable.addCell(articalTypeCell);asa
			ServerAppUtility serverApp = new ServerAppUtility();
            
			
			gstin = serverApp.getGSTINOfCompany(comp);
			
//			//String gstin = "", gstinText = "";
//			if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
//				logger.log(Level.SEVERE, "GST Applicable");
//				gstin = serverApp.getGSTINOfCompany(comp, invoiceentity.getBranch().trim());
//				System.out.println("gstin" + gstin);
//
//			} else {
//				logger.log(Level.SEVERE, "GST Not Applicable");
//				gstinText = comp.getCompanyGSTTypeText().trim();
//				System.out.println("gstinText" + gstinText);
//			}
		}
		
		PdfPTable articleTab = new PdfPTable(3);
		articleTab.setWidthPercentage(100);

		try {
			articleTab.setWidths(new float[] { 30, 5, 65 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ServerAppUtility serverApp = new ServerAppUtility();
		String stateCodeStr = serverApp.getStateOfCompany(comp,invoiceentity.getBranch().trim(), stateList);
			
			if(numberRangeConfig!=null && !numberRangeConfig.isGstApplicable()){
				System.out.println("number range condition");
				articleTab.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				articleTab.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				articleTab.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
			else{
				
			/**
			 * Date : 8-2-2019
			 * Developer : Amol
			 * Description: if no taxes are selected then no gstin no will print
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "HideGSTINNumber", invoiceentity.getCompanyId())){
				hideGSTINNo=true;
			}
		
			if(hideGSTINNo){
				logger.log(Level.SEVERE, "INSIDE hideGSTINNo NUMBER FLAG");
				Phrase articalType2 = null;
//				ServerAppUtility serverApp = new ServerAppUtility();
				System.out.println("billingtaxessize"+invoiceentity.getBillingTaxes().size());
				
				if(invoiceentity.getBillingTaxes().size()==0){
					articalType2 = new Phrase("");
					PdfPCell articalType2Cell = new PdfPCell(articalType2);
					articalType2Cell.setBorder(0);
					leftTable.addCell(articalType2Cell);
	
				}else{
					
					/**
					 * @author Anil @since 07-10-2021
					 * GST and State code alignment issue raised by Nitin Sir and Poonam
					 */
					if(gstin!=null&&!gstin.equals("")){
						articleTab.addCell(pdfUtility.getCell("GSTIN", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						articleTab.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						articleTab.addCell(pdfUtility.getCell(gstin, font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						
						if(stateCodeStr!=null&&!stateCodeStr.equals("")){
							articleTab.addCell(pdfUtility.getCell("State Code", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
							articleTab.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
							articleTab.addCell(pdfUtility.getCell(stateCodeStr, font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						}
					}
				}
				
			}
			else{
				
				/**
				 * @author Anil @since 07-10-2021
				 * GST and State code alignment issue raised by Nitin Sir and Poonam
				 */
				if(gstNumberPrintFlag){
					if(gstin!=null&&!gstin.equals("")){
						articleTab.addCell(pdfUtility.getCell("GSTIN", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						articleTab.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						articleTab.addCell(pdfUtility.getCell(gstin, font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						
						if(stateCodeStr!=null&&!stateCodeStr.equals("")){
							articleTab.addCell(pdfUtility.getCell("State Code", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
							articleTab.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
							articleTab.addCell(pdfUtility.getCell(stateCodeStr, font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
						}
					}
				}
				
			}
			
			logger.log(Level.SEVERE, "gstNumberPrintFlag "+gstNumberPrintFlag);
			logger.log(Level.SEVERE, "hideGSTINNo "+hideGSTINNo);
			
			
			/**
			 * ends here
			 */
			
			/**
			 * @author Abhinav Bihade
			 * @since 08/02/2020
			 * As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
			 * in case if somebody manages 2 companies under same link of ERP s/w
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())&&branchWiseFilteredArticleList!=null && branchWiseFilteredArticleList.size()!=0 ){	
				logger.log(Level.SEVERE,"Inside Branch As Company Printing1:" +branchWiseFilteredArticleList);
				for(ArticleType artict :branchWiseFilteredArticleList){
					Phrase typename;
					Phrase typevalue;
					if (artict.getArticlePrint()
							.equalsIgnoreCase("YES")
							&& (artict
									.getDocumentName().equals("Invoice Details") || artict
									.getDocumentName().equals("ServiceInvoice"))) {
						

						typename = new Phrase(artict.getArticleTypeName(), font10bold);
						typevalue = new Phrase(artict
								.getArticleTypeValue(), font10bold);
						logger.log(Level.SEVERE,"Inside Branch As Company Printing11111:" +typevalue);
						logger.log(Level.SEVERE,"Inside Branch As Company Printing22222:" +typename);

						PdfPCell tymanecell = new PdfPCell();
						tymanecell.addElement(typename);
						tymanecell.setBorder(0);
						tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

						Phrase typeblank = new Phrase(":", font10bold);
						PdfPCell typeCell = new PdfPCell(typeblank);
						typeCell.addElement(typeblank);
						typeCell.setBorder(0);
						typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

						PdfPCell typevalcell = new PdfPCell();
						typevalcell.addElement(typevalue);
						typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						typevalcell.setBorder(0);
						/**
						 * Date 22-11-2018 By Vijay 
						 * Des :- if process configuration is active then GST Number will not display
						 * and GST Number will display if GST applicable or not applicable as per nitin sir
						 */
						
						//if(gstNumberPrintFlag || !artict.getArticleTypeName().equals("GSTIN") ){
							articleTab.addCell(tymanecell);
							articleTab.addCell(typeCell);
							articleTab.addCell(typevalcell);
							logger.log(Level.SEVERE,"Inside Branch As Company Printing3333:" +tymanecell);
							logger.log(Level.SEVERE,"Inside Branch As Company Printing4444:" +typeCell);
							logger.log(Level.SEVERE,"Inside Branch As Company Printing5555:" +typevalcell);
							
							
						//}
					}
				}
				
			}
			
			}
			else{
				
				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					logger.log(Level.SEVERE,"Inside Branch As Company Printing2:");


					/**
					 * Date 13/12/2017 By Jayshree add the this
					 */

					Phrase typename;
					Phrase typevalue;
					if (comp.getArticleTypeDetails().get(i).getArticlePrint()
							.equalsIgnoreCase("YES")
							&& (comp.getArticleTypeDetails().get(i)
									.getDocumentName().equals("Invoice Details") || comp
									.getArticleTypeDetails().get(i)
									.getDocumentName().equals("ServiceInvoice"))) {

						typename = new Phrase(comp.getArticleTypeDetails().get(i)
								.getArticleTypeName(), font10bold);
						typevalue = new Phrase(comp.getArticleTypeDetails().get(i)
								.getArticleTypeValue(), font10bold);

						PdfPCell tymanecell = new PdfPCell();
						tymanecell.addElement(typename);
						tymanecell.setBorder(0);
						tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

						Phrase typeblank = new Phrase(":", font10bold);
						PdfPCell typeCell = new PdfPCell(typeblank);
						typeCell.addElement(typeblank);
						typeCell.setBorder(0);
						typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

						PdfPCell typevalcell = new PdfPCell();
						typevalcell.addElement(typevalue);
						typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						typevalcell.setBorder(0);
						/**
						 * Date 22-11-2018 By Vijay 
						 * Des :- if process configuration is active then GST Number will not display
						 * and GST Number will display if GST applicable or not applicable as per nitin sir
						 */
						
						if(!comp.getArticleTypeDetails().get(i).getArticleTypeName().equals("GSTIN") ){
							articleTab.addCell(tymanecell);
							articleTab.addCell(typeCell);
							articleTab.addCell(typevalcell);
						}
					}
				}

			}

		}
			PdfPCell articleCell = new PdfPCell(articleTab);
			articleCell.setBorder(0);
			leftTable.addCell(articleCell);
		
		
	
		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		
		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font10bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);

		String companyname = "";
		if (multipleCompanyName) {
			if (con.getGroup() != null && !con.getGroup().equals("")) {
				companyname = con.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}
		}
		/**
		 * @author Abhinav Bihade
		 * @since 27/12/2019
		 * For Bitco by Rahul Tiwari, "Correspondence Name" should print	 
		 */
		else if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname= branchDt.getCorrespondenceName();
			logger.log(Level.SEVERE,"3rd:");
		}
		else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		if(invoiceGroupAsSignatory){
			companyname=invoiceentity.getInvoiceGroup();
			logger.log(Level.SEVERE,"invoiceentity.getInvoiceGroup()()"+invoiceentity.getInvoiceGroup());
		}
		// ends here

		// rightTable.addCell(rightUpperCell);
		Phrase companyPhrase = new Phrase("For " + companyname, font10bold);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setBorder(0);
		if (authOnLeft) {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		rightTable.addCell(companyParaCell);

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			if (authOnLeft) {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (imageSignCell != null) {
			rightTable.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);

		}
		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		// signParaCell.addElement();

		signParaCell.setBorder(0);
		if (authOnLeft) {
			signParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell(leftTable);
		// lefttableCell.addElement();
		PdfPCell righttableCell = new PdfPCell(rightTable);

		PdfPTable middletTable = new PdfPTable(1);
		middletTable.setWidthPercentage(100);
		if (pdfUtility.printBankDetailsFlag) {
			if (comppayment != null) {

			    PdfUtility BankDetails = new PdfUtility();
				middletTable.addCell(BankDetails.getBankDetails(comppayment));
			}
		}

		PdfPCell middletTableCell = new PdfPCell(middletTable);

		bottomTable.addCell(lefttableCell);
		if(con.getNumberRange()!=null &&!con.getNumberRange().equals("")){
		if(nonbillingInvoice==true && con.getNumberRange().equalsIgnoreCase("NonBilling")){
			
			Phrase blankphrase=new Phrase(" ");
			PdfPCell blankphrCell=new PdfPCell(blankphrase);
			blankphrCell.setFixedHeight(80);
			bottomTable.addCell(blankphrCell);
			bottomTable.addCell(blankphrCell);
			System.out.println("nonbillingInvoice"+nonbillingInvoice);
		
			
		}
		else{
			bottomTable.addCell(middletTableCell);
			bottomTable.addCell(righttableCell);
		}
		}
		else{
			bottomTable.addCell(middletTableCell);
			bottomTable.addCell(righttableCell);
			
		}
		bottomTable.setKeepTogether(true); //************


		try {
			document.add(bottomTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	private void createFooterTaxPart() {
		// TODO Auto-generated method stub
		PdfPTable pdfPTaxTable = new PdfPTable(3);
		pdfPTaxTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			pdfPTaxTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

		float[] column3widths = { 2f, 2f, 6f };
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		leftTable.addCell(pdfUtility.getPdfCell("Day", font8bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		leftTable.addCell(pdfUtility.getPdfCell("Percent", font8bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		leftTable.addCell(pdfUtility.getPdfCell("Comment", font8bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));

		
		// Values
		for (int i = 0; i < invoiceentity.getArrPayTerms().size(); i++) {
			leftTable.addCell(pdfUtility.getPdfCell(invoiceentity.getArrPayTerms().get(i).getPayTermDays()+"", font8, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
			leftTable.addCell(pdfUtility.getPdfCell(decimalformat.format(invoiceentity.getArrPayTerms().get(i).getPayTermPercent()), font8, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
			leftTable.addCell(pdfUtility.getPdfCell(invoiceentity.getArrPayTerms().get(i).getPayTermComment(), font8, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		}


		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax",
				font10bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);

		Phrase amtB4TaxValphrase = new Phrase(totalAmount + "", font10bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0,otherTaxVal=0;
		for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {
			if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal + invoiceentity.getBillingTaxes().get(i).getPayableAmt();
				String IGSTPercentValue = "IGST @"+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent()+ " %";
				String IGSTValue = df.format(invoiceentity.getBillingTaxes().get(i).getPayableAmt());
				
				rightInnerTable.addCell(pdfUtility.getPdfCell(IGSTPercentValue, font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(IGSTValue, font10, Element.ALIGN_RIGHT,null, 0, 0, 0,0,0,-1,-1,-1,-1));


			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal	+ invoiceentity.getBillingTaxes().get(i).getPayableAmt();

				String SGSTPercent = "SGST @"+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent() + " %";
				String SGSTValue = df.format(invoiceentity.getBillingTaxes().get(i).getPayableAmt());
				
				rightInnerTable.addCell(pdfUtility.getPdfCell(SGSTPercent, font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(SGSTValue, font10, Element.ALIGN_RIGHT,null, 0, 0, 0,0,0,-1,-1,-1,-1));

				
			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal	+ invoiceentity.getBillingTaxes().get(i).getPayableAmt();

				String CGSTPercent = "CGST @"+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent() + " %";
				String CSGTValue = df.format(invoiceentity.getBillingTaxes().get(i).getPayableAmt());
						
				rightInnerTable.addCell(pdfUtility.getPdfCell(CGSTPercent, font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(CSGTValue, font10, Element.ALIGN_RIGHT,null, 0, 0, 0,0,0,-1,-1,-1,-1));

			}else{

				otherTaxVal = otherTaxVal+ invoiceentity.getBillingTaxes().get(i).getPayableAmt();
				
				String taxNamePercent = invoiceentity.getBillingTaxes().get(i).getTaxChargeName()+" @"+ invoiceentity.getBillingTaxes().get(i).getTaxChargePercent() + " %";
				String taxNameValue = df.format(invoiceentity.getBillingTaxes().get(i).getPayableAmt());
				
				rightInnerTable.addCell(pdfUtility.getPdfCell(taxNamePercent, font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
				rightInnerTable.addCell(pdfUtility.getPdfCell(taxNameValue, font10, Element.ALIGN_RIGHT,null, 0, 0, 0,0,0,-1,-1,-1,-1));

			}
		}
		
		String taxNm="GST";
		if(igstTotalVal==0&&cgstTotalVal==0&&sgstTotalVal==0){
			taxNm="Tax";
		}

		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal+otherTaxVal;
		
		//By Ashwini Patil  Innovative client does not want to print Total tax
		if(!thaiPdfFlag){
		rightInnerTable.addCell(pdfUtility.getPdfCell("Total "+taxNm, font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		rightInnerTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
		rightInnerTable.addCell(pdfUtility.getPdfCell(df.format(totalGSTValue), font10bold, Element.ALIGN_RIGHT,null, 0, 0, 0,0,0,-1,-1,-1,-1));

		}

		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
		// innerRightCell.addElement();

		rightTable.addCell(innerRightCell);
		

		PdfPTable middleTable = new PdfPTable(1);
		middleTable.setWidthPercentage(100);

		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			if (i == 0) {
				otherCharges.addCell(pdfUtility.getPdfCell("Charge Name", font10bold, Element.ALIGN_MIDDLE,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				otherCharges.addCell(pdfUtility.getPdfCell("Taxes", font10bold, Element.ALIGN_MIDDLE,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				otherCharges.addCell(pdfUtility.getPdfCell("Amt", font10bold, Element.ALIGN_MIDDLE,null, 0, 0, 0,-1,0,-1,-1,-1,-1));

			}

			String chargeName = invoiceentity.getOtherCharges().get(i).getOtherChargeName();
			String taxNames = " ";
			if (invoiceentity.getOtherCharges().get(i).getTax1()
					.getPercentage() != 0
					&& invoiceentity.getOtherCharges().get(i).getTax2()
							.getPercentage() != 0) {
				taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
						.getTaxConfigName()
						+ "/"
						+ invoiceentity.getOtherCharges().get(i).getTax2()
								.getTaxConfigName();
			} else {
				if (invoiceentity.getOtherCharges().get(i).getTax1()
						.getPercentage() != 0) {
					taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
							.getTaxConfigName();
				} else if (invoiceentity.getOtherCharges().get(i).getTax2()
						.getPercentage() != 0) {
					taxNames = invoiceentity.getOtherCharges().get(i).getTax2()
							.getTaxConfigName();
				} else {
					taxNames = " ";
				}
			}
			double Amount  = invoiceentity.getOtherCharges().get(i).getAmount();
			
			otherCharges.addCell(pdfUtility.getPdfCell(chargeName, font10, Element.ALIGN_MIDDLE,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
			otherCharges.addCell(pdfUtility.getPdfCell(taxNames, font10, Element.ALIGN_MIDDLE,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
			otherCharges.addCell(pdfUtility.getPdfCell(Amount+"", font10, Element.ALIGN_MIDDLE,null, 0, 0, 0,-1,0,-1,-1,-1,-1));

		}

		PdfPCell left22Cell = new PdfPCell(otherCharges);
		middleTable.addCell(left22Cell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);
		// rightCell.addElement();
		PdfPCell middleCell = new PdfPCell(middleTable);
		
		PdfPCell leftCell = new PdfPCell(leftTable);
//		 leftCell.setBorder(0);
		// leftCell.addElement();

		pdfPTaxTable.addCell(leftCell);
		if (invoiceentity.getOtherCharges().size() > 0) {
			pdfPTaxTable.addCell(middleCell);
		} else {
			Phrase blankPhrase = new Phrase(" ", font10);
			middleCell = new PdfPCell(blankPhrase);
			pdfPTaxTable.addCell(middleCell);
		}
		pdfPTaxTable.addCell(rightCell);
		pdfPTaxTable.setKeepTogether(true);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private boolean validateTaxes(
			SalesOrderProductLineItem salesOrderProductLineItem) {
		// TODO Auto-generated method stub
		if (salesOrderProductLineItem.getVatTax().getPercentage() != 0) {
			return true;
		} else {
			if (salesOrderProductLineItem.getServiceTax().getPercentage() != 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}

	private void createProductDetails() {
		
		
		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;
		
		boolean discColumnFlag = false;
		
		
		//======================================this is to consolidate productlines with same product
		HashSet<String> productCode = new HashSet<String>();
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			// contractCount.add(invoiceentity.getSalesOrderProducts().get(i).getOrderId());
			productCode.add(invoiceentity.getSalesOrderProducts().get(i)
					.getProdCode());
		}
		ArrayList<String> productCodeList = new ArrayList<String>(productCode);
		for (int i = 0; i < productCodeList.size(); i++) {
			logger.log(Level.SEVERE,"productCodeList" + productCodeList.get(i));
		}
		//=============================================================================================
		
		
		PdfPTable productTable = new PdfPTable(11);
		productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column11SerProdCollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		

		productTable.addCell(pdfUtility.getPdfCell("Sr No", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));


			productTable.addCell(pdfUtility.getPdfCell("Services", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2,0, -1,0,0,-1,-1,-1,-1));

			productTable.addCell(pdfUtility.getPdfCell("HSN / SAC", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));

			productTable.addCell(pdfUtility.getPdfCell("Frequency / Qty", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));

			productTable.addCell(pdfUtility.getPdfCell("Unit Price in Rs.", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));


			productTable.addCell(pdfUtility.getPdfCell("Taxable Value in Rs.", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));
			
		
		productTable.addCell(pdfUtility.getPdfCell("CGST", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,0, 2, 0,-1,0,-1,-1,-1,-1));
		
		productTable.addCell(pdfUtility.getPdfCell("SGST", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE,0, 2, 0,-1,0,-1,-1,-1,-1));
			
		productTable.addCell(pdfUtility.getPdfCell("Billed Amount", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 2, 0, 0,-1,0,-1,-1,-1,-1));

		productTable.addCell(pdfUtility.getPdfCell("Rate in %", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0,-1,0,-1,-1,-1,-1));
		productTable.addCell(pdfUtility.getPdfCell("Amount in Rs.", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0,-1,0,-1,-1,-1,-1));

		productTable.addCell(pdfUtility.getPdfCell("Rate in %", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0,-1,0,-1,-1,-1,-1));
		productTable.addCell(pdfUtility.getPdfCell("Amount in Rs.", font10bold, Element.ALIGN_CENTER, Element.ALIGN_MIDDLE, 0, 0, 0,-1,0,-1,-1,-1,-1));
		
		productTable.setHeaderRows(2);
		
		
		for (int k = 0; k < productCodeList.size(); k++) {
			boolean foundOnce=false;			
			int srNoVal=0;
			String productName="";
			String SACCode = "";
			String frequency="";
			String frequencyAndQty="";
			double unitPrice=0;
			double taxValue = 0;
			double taxValueIncludingGST=0;
			double sgstAmt=0;
			double cgstAmt=0;
			double igstAmt=0;
			double sgstPercent=0;
			double cgstPercent=0;
			double igstPercent=0;
			double qty=0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			
			if (productCodeList
					.get(k)
					.trim()
					.equalsIgnoreCase(
							invoiceentity.getSalesOrderProducts().get(i)
									.getProdCode())) {
				if(!foundOnce){
					logger.log(Level.SEVERE, "in !foundOnce");
			
			if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
					invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
				continue;
			}
			
			srNoVal = i + 1;
			
			productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim();
			
			
	
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null && 
						invoiceentity.getSalesOrderProducts().get(i).getHsnCode().trim().length() > 0) {
						SACCode = invoiceentity.getSalesOrderProducts().get(i).getHsnCode().trim();
				} 
				else {

					ServiceProduct serviceProduct = ofy()
							.load()
							.type(ServiceProduct.class)
							.filter("companyId", comp.getCompanyId())
							.filter("productCode",
									invoiceentity.getSalesOrderProducts().get(i)
											.getProdCode()).first().now();
					if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
						SACCode = serviceProduct.getHsnNumber();
					} 
				}
			
			
			for (int j = 0; j < con.getItems().size(); j++) {
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con.getItems().get(j).getPrduct().getCount()
						&& invoiceentity.getSalesOrderProducts().get(i).getOrderDuration() == con.getItems().get(j).getDuration() 
						&& invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
						if(invoiceentity.getSalesOrderProducts().get(i)
								.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName().trim())){
							frequency = con.getItems().get(j).getRemark();
					}
				}

			}
			logger.log(Level.SEVERE, "frequency="+frequency);	
			
			SalesOrderProductLineItem item=invoiceentity.getSalesOrderProducts().get(i);
			if(item.getVatTax()!=null){	
				if(item.getVatTax().getTaxName().startsWith("CGST")){
					cgstPercent+=item.getVatTax().getPercentage();
				}else if(item.getVatTax().getTaxName().startsWith("SGST")){
					sgstPercent+=item.getVatTax().getPercentage();
				}
			}
			if(item.getServiceTax()!=null){				
				
				if(item.getServiceTax().getTaxName().startsWith("CGST")){
					cgstPercent+=item.getServiceTax().getPercentage();
				}else if(item.getServiceTax().getTaxName().startsWith("SGST")){
					sgstPercent+=item.getServiceTax().getPercentage();
				}	
			}	
			unitPrice= invoiceentity.getSalesOrderProducts().get(i).getPrice();
			
			}
			foundOnce=true;	
			qty+=1;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getPaymentPercent() != 0) {

				taxValue += invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
				totalTaxableValue+=invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxValue += invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
				totalTaxableValue+=invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}

			
			logger.log(Level.SEVERE, "taxValue="+taxValue+" totalTaxableValue="+totalTaxableValue);
			
			SalesOrderProductLineItem item=invoiceentity.getSalesOrderProducts().get(i);
			if(item.getVatTax()!=null){	
				if(item.getVatTax().getTaxName().startsWith("CGST")){
					cgstAmt+=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalCGST+=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;					
				}else if(item.getVatTax().getTaxName().startsWith("SGST")){
					sgstAmt+=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;
					totalSGST+=item.getBasePaymentAmount()*item.getVatTax().getPercentage()/100;				
				}
			}			
			logger.log(Level.SEVERE, "after item.getVatTax() totalCGST="+totalCGST+" totalSGST="+totalSGST);
			if(item.getServiceTax()!=null){				
				
				if(item.getServiceTax().getTaxName().startsWith("CGST")){
					cgstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalCGST+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;				
				}else if(item.getServiceTax().getTaxName().startsWith("SGST")){
					sgstAmt+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;
					totalSGST+=item.getBasePaymentAmount()*item.getServiceTax().getPercentage()/100;			
				}	
			}	
			
			logger.log(Level.SEVERE, "after item.getVatTax() totalCGST="+totalCGST+" totalSGST="+totalSGST);
			

			}//product code compare if ends
			if (i + 1 == invoiceentity.getSalesOrderProducts().size()&&foundOnce) {
				taxValueIncludingGST=taxValue+cgstAmt+sgstAmt;
				grandTotal+=taxValueIncludingGST;
				if(frequency!=null&&!frequency.equals(""))
					frequencyAndQty=frequency+"/"+qty;
				else
					frequencyAndQty=""+qty;
				
				productTable.addCell(pdfUtility.getPdfCell(srNoVal+"", font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(productName, nameAddressFont6, Element.ALIGN_LEFT,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(SACCode, font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(frequencyAndQty+"", font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));					
				productTable.addCell(pdfUtility.getPdfCell(df.format(unitPrice), font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));//9-11-2022					
				productTable.addCell(pdfUtility.getPdfCell(df.format(taxValue)+"", font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));				
				productTable.addCell(pdfUtility.getPdfCell(cgstPercent+"", font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(df.format(cgstAmt), font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(sgstPercent+"", font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(df.format(sgstAmt), font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));
				productTable.addCell(pdfUtility.getPdfCell(df.format(taxValueIncludingGST), font6, Element.ALIGN_CENTER,null, 0, 0, 0,-1,0,-1,-1,-1,-1));				
				cgstAmt=0;
				sgstAmt=0;
			}
		}		

		}//end of product code for loop
		// manually set the width (as an example to page content width)
		float containerWidth = document.getPageSize().getWidth() - document.leftMargin() - document.rightMargin();
		productTable.setTotalWidth(containerWidth);
		productTable.setLockedWidth(true);

		// get height of table before and after adding it to the document
		System.out.println("Height before adding: " + productTable.getTotalHeight());
		System.out.println("rowhight "+productTable.getRowHeight(1));
		System.out.println("rowhight "+productTable.getRowHeight(2));

		pdfUtility.checkTableSizeAndAddBlanks(productTable, 0,260);

		
		PdfPCell tab1 = new PdfPCell(productTable);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
	
	}
	

	private double getPercentAmount(
			SalesOrderProductLineItem salesOrderProductLineItem,
			boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		if (isAreaPresent) {
			percentAmount = ((salesOrderProductLineItem.getPrice()
					* Double.parseDouble(salesOrderProductLineItem.getArea()
							.trim()) * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		} else {
			percentAmount = ((salesOrderProductLineItem.getPrice() * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		}
		return percentAmount;
	}

	private void createCustomerDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);		
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);


		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","MakeAdressToUppercase", invoiceentity.getCompanyId())){
			adresstouppercase=true;
		}
		
		colonTable = getAddressTable(customerBranch,cust,con,colonTable,pdfUtility,adresstouppercase,
					 addcellNoFlag,comp.getCompanyId(),invoiceentity.getPersonInfo().getPocName(),"Invoice Details",true,false);
		
				
		PdfPCell cell1 = new PdfPCell(colonTable);
		cell1.setBorder(0);

		part1Table.addCell(cell1);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		
		// For Service Address
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

//		colonTable = pdfUtility.getAddressTable(customerBranch,cust,con,colonTable,pdfUtility,adresstouppercase,
//					 addcellNoFlag,comp.getCompanyId(),invoiceentity.getPersonInfo().getPocName(),"Invoice Details",false,true);
				
		
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		PdfPCell cell2 = new PdfPCell(colonTable);
		cell2.setBorder(0);
		
		part2Table.addCell(cell2);
		PdfPCell part2TableCell = new PdfPCell(part2Table);
		part2TableCell.setBorderWidthTop(0);
//		if(invoiceentity.isDonotprintServiceAddress()){
//			part1TableCell.setColspan(2);
//		}

		mainTable.addCell(part1TableCell);
		
//		if(!invoiceentity.isDonotprintServiceAddress()){
			mainTable.addCell(part2TableCell);
//		}

//		mainTable.addCell(blankCell);
//		mainTable.addCell(blankCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	

	private String toCamelCase(String s) {
        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts){
            if(part!=null && part.trim().length()>0)
           camelCaseString = camelCaseString + toProperCase(part);
            else
                camelCaseString=camelCaseString+part+" ";   
        }
        return camelCaseString;
     }

	private String toProperCase(String s) {
        String temp=s.trim();
        String spaces="";
        if(temp.length()!=s.length())
        {
        int startCharIndex=s.charAt(temp.indexOf(0));
        spaces=s.substring(0,startCharIndex);
        }
        temp=temp.substring(0, 1).toUpperCase() +
        spaces+temp.substring(1).toLowerCase()+" ";
        return temp;

    }

	private void createInvoiceDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = { 3.5f, 0.2f, 6.8f };
		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	
	Phrase reverseCharge = new Phrase("Reverse Charge (Y/N) ", font10bold);
	PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
	reverseChargeCell.setBorder(0);
	reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase reverseChargeVal = new Phrase(AppConstants.REVERSECHARGEVALUE, font10);

		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNo = new Phrase("Invoice No", font10bold);
		PdfPCell invoiceNoCell = new PdfPCell(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String invoiceId="";
		String invoicePre="";
		logger.log(Level.SEVERE, "invoicePrefix flag "+invoicePrefix);
		logger.log(Level.SEVERE, "invoicePrefix value "+invoicePrefix);
		
		if(invoicePrefix){
			if(invoiceentity.getInvRefNumber()!=null&&!invoiceentity.getInvRefNumber().equals("")){
				invoiceId=invoiceentity.getInvRefNumber();
				
				/**
				 * For Proforma invoice, system generated id should be printed instead of prefix invoice number
				 * Raised by Ashwini For Ultra Pest Control
				 */
				if(invoiceentity.getInvoiceType().equals(AppConstants.CREATEPROFORMAINVOICE)){
					invoiceId=invoiceentity.getCount()+"";
				}
			}else{
				invoiceId=invoiceentity.getCount()+"";
			}
		}else{
			invoiceId=invoiceentity.getCount()+"";
		}
		
		logger.log(Level.SEVERE, "invoice no "+invoiceId);
		Phrase invoiceNoVal = new Phrase(invoiceId, font10);
		PdfPCell invoiceNoValCell = new PdfPCell(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * printing Ref no/Po/Wo as per the data for each available
		 * Raised by Nithila and Nitin Sir for Innovative
		 */
		String refNoHeading="";
		String refNoValue="";
		
		if(invoiceentity.getRefNumber()!=null&&!invoiceentity.getRefNumber().equals("")){
			refNoHeading=refNoHeading+"Ref No/";
			refNoValue=refNoValue+invoiceentity.getRefNumber()+"/";
		}
		
		if(invoiceentity.getWoNumber()!=null&&!invoiceentity.getWoNumber().equals("")){
			refNoHeading=refNoHeading+"WO No/";
			refNoValue=refNoValue+invoiceentity.getWoNumber()+"/";
		}
		
		if(invoiceentity.getPoNumber()!=null&&!invoiceentity.getPoNumber().equals("")){
			refNoHeading=refNoHeading+"PO No";
			refNoValue=refNoValue+invoiceentity.getPoNumber();
		}
		
		if(!refNoHeading.equals("")){
			if(refNoHeading.substring(refNoHeading.length()-1).contains("/")){
				refNoHeading=refNoHeading.substring(0, refNoHeading.length()-1);
			}
		}
		
		if(!refNoValue.equals("")){
			if(refNoValue.substring(refNoValue.length()-1).contains("/")){
				refNoValue=refNoValue.substring(0, refNoValue.length()-1);
			}
		}
		
		

//		Phrase workOrder_PoNo = new Phrase("Ref No/WO", font10bold);
		Phrase workOrder_PoNo = new Phrase(refNoHeading, font10bold);
		PdfPCell workOrder_PoNoCell = new PdfPCell(workOrder_PoNo);
		workOrder_PoNoCell.setBorder(0);
		workOrder_PoNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase workOrder_PoNoVal = new Phrase(invoiceentity.getRefNumber() + "", font10);
		Phrase workOrder_PoNoVal = new Phrase(refNoValue + "", font10);
		PdfPCell workOrder_PoNoValCell = new PdfPCell(workOrder_PoNoVal);
		workOrder_PoNoValCell.setBorder(0);
		workOrder_PoNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);

		Phrase invoiceDate = new Phrase("Date", font10bold);
		PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceDateVal = new Phrase(sdf.format(invoiceentity
				.getInvoiceDate()), font10);
		PdfPCell invoiceDateValCell = new PdfPCell(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable invcolonTable = new PdfPTable(3);
		invcolonTable.setWidthPercentage(100);

		try {
			invcolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable pdfInvDataTable = new PdfPTable(4);
		pdfInvDataTable.setWidthPercentage(100);
		try {
			pdfInvDataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPTable pdfPODataTable = new PdfPTable(4);
		pdfPODataTable.setWidthPercentage(100);
		try {
			pdfPODataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Phrase poNumber = new Phrase("PO Number", font10bold);
		PdfPCell poNumberCell = new PdfPCell(poNumber);
		poNumberCell.setBorder(0);
		poNumberCell.setHorizontalAlignment(Element.ALIGN_LEFT);
      
		
		String poNo="";
		if(con.getPoNumber()!=null){
			poNo=con.getPoNumber();
		}else{
			poNo="";
		}
		
		Phrase poNumberVale = new Phrase(poNo, font10);
		PdfPCell poNumberValeCell = new PdfPCell(poNumberVale);
		poNumberValeCell.setBorder(0);
		poNumberValeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase poDate = new Phrase("PO Date", font10bold);
		PdfPCell poDateCell = new PdfPCell(poDate);
		poDateCell.setBorder(0);
		poDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String podate="";
		if(con.getPoDate()!=null){
			podate=(sdf.format(con.getPoDate()));
		}else{
			podate="";
		}
		
		
		Phrase poDateValue = new Phrase(podate, font10);
		PdfPCell poDateValueCell = new PdfPCell(poDateValue);
		poDateValueCell.setBorder(0);
		poDateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
//		pdfPODataTable.addCell(poNumberValeCell);
//		pdfPODataTable.addCell(poDateCell);
//		pdfPODataTable.addCell(colonCell);
//		pdfPODataTable.addCell(poDateValueCell);
//		
		
		
		pdfInvDataTable.addCell(invoiceNoValCell);
		pdfInvDataTable.addCell(invoiceDateCell);
		pdfInvDataTable.addCell(colonCell);
		pdfInvDataTable.addCell(invoiceDateValCell);

		PdfPCell pdfPInvoiceDataCell = new PdfPCell(pdfInvDataTable);
		pdfPInvoiceDataCell.setBorder(0);
		pdfPInvoiceDataCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		invcolonTable.addCell(pdfPInvoiceDataCell); // stateTableCell.addElement(statetable);

		PdfPCell invoiceDataCell = new PdfPCell(pdfInvDataTable);
		invoiceDataCell.setBorder(0);

		colonTable.addCell(invoiceDataCell);
//		if (poDetailsFlag) {
//			colonTable.addCell(poNumberCell);
//			colonTable.addCell(colonCell);
//			pdfPODataTable.addCell(poNumberValeCell);
//		} 
//		else {
//			if(!refNoHeading.equals("")){
//				colonTable.addCell(workOrder_PoNoCell);
//				colonTable.addCell(colonCell);
//				colonTable.addCell(workOrder_PoNoValCell);	
//			}
//		}
		
		
		
	
		
		pdfPODataTable.addCell(poDateCell);
		pdfPODataTable.addCell(colonCell);
		pdfPODataTable.addCell(poDateValueCell);
		
		PdfPCell poDataCell = new PdfPCell(pdfPODataTable);
		poDataCell.setBorder(0);
		
		colonTable.addCell(poDataCell);
		
		
		
		colonTable.setSpacingAfter(10f);

		PdfPCell pdfCell = new PdfPCell(colonTable);
		pdfCell.setBorder(0);
		// pdfCell.addElement();

		part1Table.addCell(pdfCell);

		Phrase state = new Phrase("State", font13bold);// Date 9/12/2017 By
														// Jayshree To increse
														// the fontsize by one
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(),
				font13);// Date 9/12/2017 By Jayshree To increse the fontsize by
						// one
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase stateCode = new Phrase("State Code", font13bold);// Date
																// 9/12/2017 By
																// Jayshree To
																// increse the
																// fontsize by
																// one
//		PdfPCell stateCodeCell = new PdfPCell(stateCode);
//		// stateCodeCell.addElement(stateCode);
//		stateCodeCell.setBorder(0);
//		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase stateCodeVal = new Phrase(" ", font13);// Date 9/12/2017 By
//														// Jayshree To increse
//														// the fontsize by one
//		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
//		// stateCodeValCell.addElement();
//		stateCodeValCell.setBorder(0);
//		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);

		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		colonTable.addCell(stateCodeCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateCodeValCell);
		statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);

		PdfPCell stateTableCell = new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

//		Phrase contractId = new Phrase("Contract No", font10bold);
//		PdfPCell contractIdCell = new PdfPCell(contractId);
//		// contractIdCell.addElement(contractId);
//		contractIdCell.setBorder(0);
//		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase contractIdVal = new Phrase(
//				invoiceentity.getContractCount() + "", font10);
//		PdfPCell contractIdValCell = new PdfPCell(contractIdVal);
//		// contractIdValCell.addElement(contractIdVal);
//		contractIdValCell.setBorder(0);
//		contractIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		PdfPTable periodtable = new PdfPTable(2);
//		periodtable.setWidthPercentage(100);

		PdfPTable concolonTable = new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		concolonTable.addCell(contractIdCell);
//		concolonTable.addCell(colonCell);

//		Phrase contractDate = new Phrase("Date", font10bold);
//		PdfPCell contractDateCell = new PdfPCell(contractDate);
//		contractDateCell.setBorder(0);
//		contractDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase contractDateVal = new Phrase(sdf.format(con.getContractDate()),
//				font10);
//		PdfPCell contractDateValCell = new PdfPCell(contractDateVal);
//		contractDateValCell.setBorder(0);
//		contractDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPTable contractColTable = new PdfPTable(3);
//		contractColTable.setWidthPercentage(100);
		
//		Phrase blankphrase = new Phrase("", font10);
//		PdfPCell blankCell = new PdfPCell();
//		blankCell.addElement(blankphrase);
//		blankCell.setBorder(0);

//		try {
//			contractColTable.setWidths(columnrohanrrCollonWidth);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		PdfPTable pdfConDataTable = new PdfPTable(4);
//		pdfConDataTable.setWidthPercentage(100);
//		try {
//			pdfConDataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		pdfConDataTable.addCell(contractIdValCell);
//		if(hideContractDate){
//			pdfConDataTable.addCell(blankCell);
//			pdfConDataTable.addCell(blankCell);
//			pdfConDataTable.addCell(blankCell);
//			}else{
//			pdfConDataTable.addCell(contractDateCell);
//			pdfConDataTable.addCell(colonCell);
//			pdfConDataTable.addCell(contractDateValCell);
//			}

//		PdfPCell pdfPConDataCell = new PdfPCell(pdfConDataTable);
//		pdfPConDataCell.setBorder(0);
//		pdfPConDataCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		concolonTable.addCell(pdfPConDataCell); // stateTableCell.addElement(statetable);
//
//		PdfPCell contractDataCell = new PdfPCell(concolonTable);
//		contractDataCell.setBorder(0);
//
//		concolonTable.addCell(contractDataCell);

//		Phrase startDate = new Phrase("Contract From", font10bold);
//		PdfPCell startDateCell = new PdfPCell(startDate);
//		startDateCell.setBorder(0);
//		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
////		Phrase startDateVal = new Phrase(sdf.format(invoiceentity
////				.getContractStartDate()), font10);
//		Phrase startDateVal = new Phrase(sdf.format(con.getStartDate()), font10);
//		PdfPCell startDateValCell = new PdfPCell(startDateVal);
//		// stateValCell.addElement(stateVal);
//		startDateValCell.setBorder(0);
//		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase endDate = new Phrase("To", font10bold);
//		PdfPCell endDateCell = new PdfPCell(endDate);
//		endDateCell.setBorder(0);
//		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
////		Phrase endDateVal = new Phrase(sdf.format(invoiceentity
////				.getContractEndDate()), font10);
//		Phrase endDateVal = new Phrase(sdf.format(con.getEndDate()), font10);
//		PdfPCell endDateValCell = new PdfPCell(endDateVal);
//		endDateValCell.setBorder(0);
//		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnrohanrrCollonWidth);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		colonTable.addCell(startDateCell);
//		colonTable.addCell(colonCell);
//		PdfPTable pdfContractPeriodTable = new PdfPTable(4);
//		pdfContractPeriodTable.setWidthPercentage(100);
//		try {
//			pdfContractPeriodTable
//					.setWidths(columnContractPeriodDateCodeCollonWidth);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		pdfContractPeriodTable.addCell(startDateValCell);
//		pdfContractPeriodTable.addCell(endDateCell);
//		pdfContractPeriodTable.addCell(colonCell);
//		pdfContractPeriodTable.addCell(endDateValCell);
//
//		PdfPCell state4Cell = new PdfPCell(pdfContractPeriodTable);
		
		colonTable.addCell(poNumberCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(poNumberValeCell);
			
		colonTable.addCell(poDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(poDateValueCell);
		
		PdfPCell state4Cell = new PdfPCell(colonTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		colonTable.addCell(state4Cell); // stateTableCell.addElement(statetable);
//
//		PdfPCell periodTableCell = new PdfPCell(colonTable);
//		periodTableCell.setBorder(0);
		/**
		 * Billing Period
		 */
		/**
		 * @author Anil
		 * @since 21-01-2022
		 * For Hygeinic Pest we will print contract duration at the top and billing duration on product line item
		 * level. raised by Nithila and Nitin sir
		 */
		
//		String headingFrom="Billing From";
//		String headingTo="To"; 
//		String fromValue="";
//		if (invoiceentity.getBillingPeroidFromDate() != null) {
//			fromValue=sdf.format(invoiceentity.getBillingPeroidFromDate());
//		}
//		String toValue="";
//		if (invoiceentity.getBillingPeroidToDate() != null) {
//			toValue=sdf.format(invoiceentity.getBillingPeroidToDate());
//		}
//		
//		if(serviceWiseBillInvoice){
//			headingFrom="Contract From";
//			headingTo="To"; 
//			
//			if (invoiceentity.getContractStartDate()!= null) {
//				fromValue=sdf.format(invoiceentity.getContractStartDate());
//			}
//			if (invoiceentity.getContractEndDate()!= null) {
//				toValue=sdf.format(invoiceentity.getContractEndDate());
//			}
//		}/** @Sheetal : 10-02-2022
//		   * Des : Switching Billing period and Contract duration with below process configuration,
//                   requirment by Pest-O-Shield **/
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PC_INV_Quot_Con_SWAP_BILLING_AND_CONTRACT_PERIOD", invoiceentity.getCompanyId())){
//			headingFrom="Contract From";
//			headingTo="To"; 
//			
//			if (invoiceentity.getContractStartDate()!= null) {
//				fromValue=sdf.format(invoiceentity.getContractStartDate());
//			}
//			if (invoiceentity.getContractEndDate()!= null) {
//				toValue=sdf.format(invoiceentity.getContractEndDate());
//			}
//			System.out.println("sheetal");
//		}
//		/**end**/
////		Phrase periodstartDate = new Phrase("Billing From", font10bold);
//		Phrase periodstartDate = new Phrase(headingFrom, font10bold);
//		PdfPCell periodstartDateCell = new PdfPCell(periodstartDate);
//		// periodstartDateCell.addElement(periodstartDate);
//		periodstartDateCell.setBorder(0);
//		periodstartDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// rohan adde this code
//		Phrase periodstartDateVal = null;
////		if (invoiceentity.getBillingPeroidFromDate() != null) {
////			periodstartDateVal = new Phrase(sdf.format(invoiceentity.getBillingPeroidFromDate()), font10);
////		} else {
////			periodstartDateVal = new Phrase(" ", font10);
////		}
//		periodstartDateVal = new Phrase(fromValue, font10);
//
//		PdfPCell periodstartDateValCell = new PdfPCell(periodstartDateVal);
//		// periodstartDateValCell.addElement(periodstartDateVal);
//		periodstartDateValCell.setBorder(0);
//		periodstartDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
////		Phrase periodendDate = new Phrase("To", font10bold);
//		Phrase periodendDate = new Phrase(headingTo, font10bold);
//		PdfPCell periodendDateCell = new PdfPCell(periodendDate);
//		// periodendDateCell.addElement(periodendDate);
//		periodendDateCell.setBorder(0);
//		periodendDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// rohan adde this code
//		Phrase periodendDateVal = null;
////		if (invoiceentity.getBillingPeroidToDate() != null) {
////			periodendDateVal = new Phrase(sdf.format(invoiceentity.getBillingPeroidToDate()), font10);
////		} else {
////			periodendDateVal = new Phrase(" ", font10);
////		}
//		
//		periodendDateVal = new Phrase(toValue, font10);
//
//		PdfPCell periodendDateValCell = new PdfPCell(periodendDateVal);
//		// periodendDateValCell.addElement(periodendDateVal);
//		periodendDateValCell.setBorder(0);
//		periodendDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
		Phrase blankphrase1 = new Phrase("", font10);
		PdfPCell blankCell1 = new PdfPCell();
		blankCell1.addElement(blankphrase1);
		blankCell1.setBorder(0);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnrohanrrCollonWidth);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if(hideBillingPeriod){
//			//addCell(blankCell1);
//			colonTable.addCell(blankCell1);
//			colonTable.addCell(blankCell1);
//		}else {
//			colonTable.addCell(periodstartDateCell);
//			colonTable.addCell(colonCell);
//		}
//		PdfPTable pdfBillingPeriodTable = new PdfPTable(4);
//		pdfBillingPeriodTable.setWidthPercentage(100);
//		try {
//			pdfBillingPeriodTable
//					.setWidths(columnContractPeriodDateCodeCollonWidth);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		pdfBillingPeriodTable.addCell(periodstartDateValCell);
//		pdfBillingPeriodTable.addCell(periodendDateCell);
//		pdfBillingPeriodTable.addCell(colonCell);
//		pdfBillingPeriodTable.addCell(periodendDateValCell);
		
//		if(hideBillingPeriod){
//			pdfBillingPeriodTable.addCell(blankCell1);
//			pdfBillingPeriodTable.addCell(blankCell1);
//			pdfBillingPeriodTable.addCell(blankCell1);
//			pdfBillingPeriodTable.addCell(blankCell1);	
//		}else{
//			pdfBillingPeriodTable.addCell(periodstartDateValCell);
//			pdfBillingPeriodTable.addCell(periodendDateCell);
//			pdfBillingPeriodTable.addCell(colonCell);
//			pdfBillingPeriodTable.addCell(periodendDateValCell);
//		}
//		
		

//		state4Cell = new PdfPCell(pdfBillingPeriodTable);
//		state4Cell.setBorder(0);
//		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		colonTable.addCell(state4Cell);
		
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * adding proforma invoice number on tax invoice. raised by Nithil and Nitin Sir for innovative
		 */
		if(invoiceentity.getInvoiceType().equals("Tax Invoice")){
			if(invoiceentity.getProformaCount()!=null&&invoiceentity.getProformaCount()!=0){
				Phrase proformaPh = new Phrase("Proforma Invoice No", font10bold);
				PdfPCell proformaCell = new PdfPCell(proformaPh);
				proformaCell.setBorder(0);
				proformaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase proformaPh1 = new Phrase(invoiceentity.getProformaCount()+"", font10);
				PdfPCell proformaCell1 = new PdfPCell(proformaPh1);
				proformaCell1.setBorder(0);
				proformaCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
//				if(hideBillingPeriod){
//					colonTable.addCell(blankCell1);
//					colonTable.addCell(blankCell1);
//				}else {
					colonTable.addCell(proformaCell);
					colonTable.addCell(colonCell);
//				}
				
				PdfPTable pdfBillingPeriodTable1 = new PdfPTable(4);
				pdfBillingPeriodTable1.setWidthPercentage(100);
				try {
					pdfBillingPeriodTable1.setWidths(columnContractPeriodDateCodeCollonWidth);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
//				if(hideBillingPeriod){
//					pdfBillingPeriodTable1.addCell(blankCell1);
//					pdfBillingPeriodTable1.addCell(blankCell1);
//					pdfBillingPeriodTable1.addCell(blankCell1);
//					pdfBillingPeriodTable1.addCell(blankCell1);	
//				}else{
					pdfBillingPeriodTable1.addCell(proformaCell1);
					pdfBillingPeriodTable1.addCell(blankCell1);
					pdfBillingPeriodTable1.addCell(blankCell1);
					pdfBillingPeriodTable1.addCell(blankCell1);
//				}
				
				state4Cell = new PdfPCell(pdfBillingPeriodTable1);
				state4Cell.setBorder(0);
				state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				colonTable.addCell(state4Cell);
			}
		}

		// periodTableCell.addElement();

//		PdfPCell concolonTableCell = new PdfPCell(concolonTable);
//		concolonTableCell.setBorder(0);
//		// concolonTableCell.addElement();

		PdfPCell billperiodtableCell = new PdfPCell(colonTable);
		billperiodtableCell.setBorder(0);
		// billperiodtableCell.addElement();

//		part2Table.addCell(concolonTableCell);
		/** date 25.6.2018 added by komal for complain service invoice print**/
		if (con.isContractRate() || con.getItems().get(0).isComplainService()) {
			//part2Table.addCell(periodTableCell); //Ashwini Patil Date:7-03-2022 Description: On tax Invoice ""Contract from" field repeated @ DoorMojo
		}
		part2Table.addCell(billperiodtableCell);

		PdfPCell part1Cell = new PdfPCell(part1Table);
		part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement();

		mainTable.addCell(part1Cell);

		part1Cell = new PdfPCell(part2Table);
		part1Cell.setRowspan(2);//1-11-2022
		// part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement();
		part1Cell.setBorderWidthBottom(0);
		mainTable.addCell(part1Cell);

		// mainTable.addCell(blankCell);
		// mainTable.addCell(blankCell);
		Phrase billingAddress = new Phrase("Billing Address", font8bold);
		
		// Paragraph billingpara=new Paragraph();
		// billingpara.add(billingAddress);
		// billingpara.setAlignment(Element.ALIGN_CENTER);
		// billingpara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		// billAdressCell.addElement(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//		if(invoiceentity.isDonotprintServiceAddress()){
//			billAdressCell.setColspan(2);
//		}
		mainTable.addCell(billAdressCell);
		
//		if(!invoiceentity.isDonotprintServiceAddress()){
			
		Phrase serviceaddress = new Phrase("", font8bold);
		// Paragraph servicepara=new Paragraph();
		// servicepara.add(serviceaddress);
		// servicepara.setAlignment(Element.ALIGN_CENTER);
		// servicepara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		// serviceCell.addElement(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceCell.setBorderWidthBottom(0);
		serviceCell.setBorderWidthTop(0);
		// serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//		mainTable.addCell(serviceCell);//1-11-2022
//		}
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createProductDescription() {

		/**
		 * Date : 27-07-2017 By ANIL
		 */
		if (!productDescFlag) {
			return;
		}
		/**
		 * End
		 */

		PdfPTable prodDescriptionTbl = new PdfPTable(2);
		prodDescriptionTbl.setWidthPercentage(100);

		try {
			prodDescriptionTbl.setWidths(new float[] { 40, 60 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase prodDescriptionVal = new Phrase("", font8);
		String prodDescriptionValue = "";
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			Phrase productIdLbl = new Phrase("Product Id : "
					+ invoiceentity.getSalesOrderProducts().get(i).getProdId(),
					font10bold);
			PdfPCell productIdLblCell = new PdfPCell(productIdLbl);
			productIdLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			productIdLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productIdLblCell.setBorder(0);
			prodDescriptionTbl.addCell(productIdLblCell);

			String prodNameValue = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();

			Phrase prodNameLbl = new Phrase("Product Name : " + prodNameValue,
					font10bold);
			PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
			prodlblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodlblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodlblCell.setBorder(0);

			prodDescriptionTbl.addCell(prodlblCell);

			prodDescriptionValue = "";

			if (invoiceentity.getSalesOrderProducts().get(i).getProdDesc1() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc1()
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getProdDesc2();
			} else if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdDesc1() == null
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc2();
			} else if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdDesc1() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() == null
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc1();
			} else {
				prodDescriptionValue = "";

			}

			prodDescriptionVal = new Phrase("" + prodDescriptionValue, font8);
		}
		Paragraph value = new Paragraph(prodDescriptionVal);
		value.setAlignment(Element.ALIGN_LEFT);

		try {
			/**
			 * Date :27-07-2017 By ANIL If no product description is found no
			 * table will be printed on pdf
			 */
			if (!prodDescriptionValue.equals("")) {
				document.add(Chunk.NEXTPAGE);
				document.add(prodDescriptionTbl);
				document.add(value);
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Added description code End Here
	 */

	// private void createStaticHeader(String preprintStatus) {
	// // TODO Auto-generated method stub
	// PdfPTable mainTable=new PdfPTable(2);
	// mainTable.setWidthPercentage(100);
	// try {
	// mainTable.setWidths(columnMoreLeftHeaderWidths);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// // rohan added this code for printing INVOICE heading when process config
	// is off
	// Phrase pdfHeading=null;
	// if(preprintStatus.equalsIgnoreCase("Plane")){
	// if(invoiceentity.getInvoiceType().equalsIgnoreCase("Proforma Invoice")){
	// pdfHeading=new Phrase("Proform Invoice",font14bold);
	// }
	// else{
	// pdfHeading=new Phrase("Tax Invoice",font14bold);
	// }
	//
	// }
	// else{
	// pdfHeading=new Phrase(" ",font14bold);
	// }
	//
	// Paragraph invPara=new Paragraph();
	// invPara.add(pdfHeading);
	// invPara.setAlignment(Element.ALIGN_RIGHT);
	// PdfPCell pdfHeadingCell=new PdfPCell();
	// pdfHeadingCell.addElement(invPara);
	// pdfHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// mainTable.addCell(pdfHeadingCell);
	//
	// PdfPTable partialTable=new PdfPTable(2);
	// partialTable.setWidthPercentage(100);
	// try {
	// partialTable.setWidths(columnMoreRightCheckBoxWidths);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// Phrase blankPhrase=new Phrase(" ",font10);
	// PdfPCell blankCell=new PdfPCell(blankPhrase);
	//
	// // blankCell.addElement(blankPhrase);
	// // blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// /*Just to create little spacing between boxes*/
	// // PdfPTable pdfTable=new PdfPTable(1);
	// // pdfTable.addCell(blankCell);
	// // pdfTable.setWidthPercentage(100);
	// //
	// // PdfPCell blank2Cell=new PdfPCell();
	// // blank2Cell.addElement(pdfTable);
	// // blank2Cell.setBorder(0);
	// // blank2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// Phrase stat1Phrase=new Phrase("Original for Receipient",font10);
	// PdfPCell stat1PhraseCell=new PdfPCell(stat1Phrase);
	// // stat1PhraseCell.addElement(stat1Phrase);
	// stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase stat2Phrase=new
	// Phrase("Duplicate for Supplier/Transporter",font10);
	// PdfPCell stat2PhraseCell=new PdfPCell(stat2Phrase);
	// // stat2PhraseCell.addElement(stat2Phrase);
	// stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase stat3Phrase=new Phrase("Triplicate for Supplier",font10);
	// PdfPCell stat3PhraseCell=new PdfPCell(stat3Phrase);
	// // stat3PhraseCell.addElement(stat3Phrase);
	// stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat1PhraseCell);
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat2PhraseCell);
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat3PhraseCell);
	//
	// PdfPCell pdfPCell=new PdfPCell();
	// pdfPCell.addElement(partialTable);
	// // pdfPCell.setBorder(0);
	// pdfPCell.setBorderWidthLeft(0);
	// pdfPCell.setBorderWidthBottom(0);
	// pdfPCell.setBorderWidthTop(0);
	//
	// mainTable.addCell(pdfPCell);
	// try {
	// document.add(mainTable);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	private void createHeader() {

		
		PdfPTable mainheader = new PdfPTable(2);
		mainheader.setWidthPercentage(100);
		
		mainheader = pdfUtility.createHeader(comp, checkheaderLeft, checkheaderRight, checkEmailId, UniversalFlag, con.getGroup(), 
				invoiceentity.getBranch(), hoEmail, con.getNumberRange(), nonbillingInvoice);
		
				
		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		// Date 20/11/2017 By Jayshree
//		// to add the logo in Table
//		DocumentUpload logodocument = comp.getLogo();
//
//		// patch
//		String hostUrl;
//		String environment = System
//				.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//			String applicationId = System
//					.getProperty("com.google.appengine.application.id");
//			String version = System
//					.getProperty("com.google.appengine.application.version");
//			hostUrl = "http://" + version + "." + applicationId
//					+ ".appspot.com/";
//		} else {
//			hostUrl = "http://localhost:8888";
//		}
//		PdfPCell imageSignCell = null;
//		Image image2 = null;
//		try {
//			image2 = Image
//					.getInstance(new URL(hostUrl + logodocument.getUrl()));
//			image2.scalePercent(20f);
//			// image2.setAbsolutePosition(40f,765f);
//			// doc.add(image2);
//
//			imageSignCell = new PdfPCell(image2);
//			imageSignCell.setBorder(0);
//			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			imageSignCell.setFixedHeight(40);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		/**
//		 * Ends For Jayshree
//		 */
//
//		/**
//		 * Developer : Jayshree Date : 21 Nov 2017 Description : Logo position
//		 * adjustment
//		 */
//		PdfPTable logoTab = new PdfPTable(1);
//		logoTab.setWidthPercentage(100);
//
//		if (imageSignCell != null) {
//			logoTab.addCell(imageSignCell);
//		} else {
//			Phrase logoblank = new Phrase(" ");
//			PdfPCell logoblankcell = new PdfPCell(logoblank);
//			logoblankcell.setBorder(0);
//			logoTab.addCell(logoblankcell);
//		}
//		/**
//		 * Ends for Jayshree
//		 */
//		Phrase companyName = null;
//		if (comp != null) {
//			companyName = new Phrase(comp.getBusinessUnitName().trim(),
//					font16bold);
//		}
//
//		Paragraph companyNamepara = new Paragraph();
//		companyNamepara.add(companyName);
//		/**
//		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
//		 * alignment
//		 */
//
//		if (checkheaderLeft == true) {
//			companyNamepara.setAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			companyNamepara.setAlignment(Element.ALIGN_RIGHT);
//		} else {
//			companyNamepara.setAlignment(Element.ALIGN_CENTER);
//		}
//
//		// companyNamepara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell companyNameCell = new PdfPCell();
//		companyNameCell.addElement(companyNamepara);
//		companyNameCell.setBorder(0);
//		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		Phrase companyAddr = null;
//		if (comp != null) {
//			companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
//					.trim(), font12);
//		}
//		Paragraph companyAddrpara = new Paragraph();
//		companyAddrpara.add(companyAddr);
//		/**
//		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
//		 * alignment
//		 */
//
//		if (checkheaderLeft == true) {
//			companyAddrpara.setAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			companyAddrpara.setAlignment(Element.ALIGN_RIGHT);
//		} else {
//			companyAddrpara.setAlignment(Element.ALIGN_CENTER);
//		}
//
//		// companyAddrpara.setAlignment(Element.ALIGN_CENTER);
//		/**
//		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
//		 * alignment
//		 */
//		PdfPCell companyAddrCell = new PdfPCell(companyAddrpara);
//		// companyAddrCell.addElement();
//		companyAddrCell.setBorder(0);
//		if (checkheaderLeft == true) {
//			companyAddrCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			companyAddrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		} else {
//			companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		/**
//		 * Ends
//		 */
//
//		// companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		Phrase companyGSTTIN = null;
//		String gstinValue = "";
//		if (UniversalFlag) {
//			if (con.getGroup().equalsIgnoreCase(
//					"Universal Pest Control Pvt. Ltd.")) {
//
//				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//					if (comp.getArticleTypeDetails().get(i)
//							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
//						gstinValue = comp.getArticleTypeDetails().get(i)
//								.getArticleTypeName()
//								+ " : "
//								+ comp.getArticleTypeDetails().get(i)
//										.getArticleTypeValue().trim();
//						break;
//					}
//				}
//
//				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//					if (!comp.getArticleTypeDetails().get(i)
//							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
//						gstinValue = gstinValue
//								+ ","
//								+ comp.getArticleTypeDetails().get(i)
//										.getArticleTypeName()
//								+ " : "
//								+ comp.getArticleTypeDetails().get(i)
//										.getArticleTypeValue().trim();
//					}
//				}
//			}
//		} else {
//
//			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
//						.equalsIgnoreCase("GSTIN")) {
//					gstinValue = comp.getArticleTypeDetails().get(i)
//							.getArticleTypeName()
//							+ " : "
//							+ comp.getArticleTypeDetails().get(i)
//									.getArticleTypeValue().trim();
//					break;
//				}
//			}
//
//			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
//						.equalsIgnoreCase("GSTIN")) {
//					gstinValue = gstinValue
//							+ ","
//							+ comp.getArticleTypeDetails().get(i)
//									.getArticleTypeName()
//							+ " : "
//							+ comp.getArticleTypeDetails().get(i)
//									.getArticleTypeValue().trim();
//				}
//			}
//		}
//
//		if (!gstinValue.equals("")) {
//			companyGSTTIN = new Phrase(gstinValue, font12bold);
//		}
//
//		Paragraph companyGSTTINpara = new Paragraph();
//		companyGSTTINpara.add(companyGSTTIN);
//		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell companyGSTTINCell = new PdfPCell();
//		companyGSTTINCell.addElement(companyGSTTINpara);
//		companyGSTTINCell.setBorder(0);
//		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		/**
//		 * Date 12/1/2018 Dev.Jayshree Des.To add the company Email And Branch
//		 * Email
//		 */
//		String branchmail = "";
//		ServerAppUtility serverApp = new ServerAppUtility();
//
//		if (checkEmailId == true) {
//			branchmail = serverApp.getBranchEmail(comp,
//					invoiceentity.getBranch());
//			System.out.println("server method " + branchmail);
//
//		} else {
//			branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
//			System.out.println("server method 22" + branchmail);
//		}
//
//		String email = null;
//		if (branchmail != null) {
//			email = "Email : " + branchmail;
//		} else {
//			email = "Email : ";
//		}
//
//		/**
//		 * By Jayshree Date 28/12/2017 to handle null condition
//		 */
//
//		String website = "";
//		if (comp.getWebsite() == null || comp.getWebsite().equals("")) {
//			website = " ";
//		} else {
//
//			website = "Website : " + comp.getWebsite();
//		}
//		/**
//		 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
//		 * add the LandLine no.
//		 */
//
//		/**
//		 * Comment by jayshree the below code
//		 */
//		// if (comp.getCellNumber2() != 0) {
//		// number = "Phone " + comp.getCellNumber1() + ","
//		// + comp.getCellNumber2();
//		// } else if (comp.getCellNumber1() != 0) {
//		// number = "Phone " + comp.getCellNumber1();
//		// } else {
//		// number = "Phone ";
//		// }
//		// String landline = "";
//		// if (comp.getLandline() != 0) {
//		// System.out.println("comp.getLandline()" + comp.getLandline());
//		// landline = "Landline  " + comp.getLandline();
//		// } else {
//		// landline = "";
//		// }
//		/**
//		 * Above code is Commented bY Jayshree
//		 */
//		/**
//		 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
//		 * add the LandLine no.
//		 */
//		String number = "";
//		String landline = "";
//
//		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
//			System.out.println("pn11");
//			number = comp.getCountryCode()+comp.getCellNumber1() + "";
//		}
//		if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
//			if (!number.trim().isEmpty()) {
//				number = number + " , " +comp.getCountryCode()+comp.getCellNumber2() + "";
//			} else {
//				number = comp.getCountryCode()+comp.getCellNumber2() + "";
//			}
//			System.out.println("pn33" + number);
//		}
//		if (comp.getLandline() != 0 && comp.getLandline() != null) {
//			if (!number.trim().isEmpty()) {
//				number = number + " , " + comp.getStateCode()+comp.getLandline() + "";
//			} else {
//				number = comp.getStateCode()+comp.getLandline() + "";
//			}
//			System.out.println("pn44" + number);
//		}
//
////		PdfPCell companyEmailandwebCell = null;
////		if (number != null && !number.trim().isEmpty()) {
////			companyEmailandwebCell = new PdfPCell(new Phrase(email + " "
////					+ "Phone " + number, font11));
////		} else {
////			companyEmailandwebCell = new PdfPCell(new Phrase(email, font11));
////		}
////
////		// PdfPCell companyEmailandwebCell = new PdfPCell(new Phrase(email + " "
////		// + number + " " + landline, font11));
////		/* End By Jayshree */
////		// companyGSTTINCell.addElement(companyGSTTINpara);
////		companyEmailandwebCell.setBorder(0);
////		if (checkheaderLeft == true) {
////			companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_LEFT);
////		} else if (checkheaderRight == true) {
////			companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
////		} else {
////			companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
////		}
////		// companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
////
////		// Date 16/11/2017 By jayshree add the webside in header section
////
////		PdfPCell companymob = new PdfPCell(new Phrase("" + website, font11));
////		// companyGSTTINCell.addElement(companyGSTTINpara);
////		companymob.setBorder(0);
////		/**
////		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
////		 * alignment
////		 */
////		if (checkheaderLeft == true) {
////			companymob.setHorizontalAlignment(Element.ALIGN_LEFT);
////		} else if (checkheaderRight == true) {
////			companymob.setHorizontalAlignment(Element.ALIGN_RIGHT);
////		} else {
////			companymob.setHorizontalAlignment(Element.ALIGN_CENTER);
////		}
////		/**
////		 * Ends
////		 */
////		// companymob.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		
//		
//		
//		
////		PdfPTable pdfPTable = new PdfPTable(1);
////		// pdfPTable.setWidthPercentage(100);
////		pdfPTable.addCell(companyNameCell);
////		pdfPTable.addCell(companyAddrCell);
////		pdfPTable.addCell(companyEmailandwebCell);
////		pdfPTable.addCell(companymob);
//
//		
//		/**
//		 * Date 31-3-2018
//		 * By jayshree
//		 * Des.to print the head off.email
//		 */
//		String hoid=null;
//		if(hoEmail==true){
//			hoid="HO Email : "+comp.getEmail();
//		}
//		
//		Phrase hoemail=new Phrase(hoid,font11);
//		PdfPCell hocell=new PdfPCell(hoemail);
//		hocell.setBorder(0);
//		if (checkheaderLeft == true) {
//			hocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			hocell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		} else {
//			hocell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		
//		
//		Phrase branchmailph=new Phrase(email,font11);
//		PdfPCell branchmailcell=new PdfPCell(branchmailph);
//		branchmailcell.setBorder(0);
//		if (checkheaderLeft == true) {
//			branchmailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			branchmailcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		} else {
//			branchmailcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		
//		
//		
//		PdfPCell companyphoneCell = null;
//		if (number != null && !number.trim().isEmpty()) {
//			companyphoneCell = new PdfPCell(new Phrase( "Phone " + number, font11));
//		} else {
//			companyphoneCell = new PdfPCell(new Phrase("", font11));
//		}
//		companyphoneCell.setBorder(0);
//		if (checkheaderLeft == true) {
//			companyphoneCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			companyphoneCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		} else {
//			companyphoneCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		
//		PdfPCell companyweb = new PdfPCell(new Phrase("" + website, font11));
//		companyweb.setBorder(0);
//		
//		if (checkheaderLeft == true) {
//			companyweb.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else if (checkheaderRight == true) {
//			companyweb.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		} else {
//			companyweb.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		
//
//		PdfPTable pdfPTable = new PdfPTable(1);
//		// pdfPTable.setWidthPercentage(100);
//		pdfPTable.addCell(companyNameCell);
//		pdfPTable.addCell(companyAddrCell);
//		pdfPTable.addCell(branchmailcell);
//		if(hoEmail==true){
//			pdfPTable.addCell(hocell);
//		}
//		pdfPTable.addCell(companyphoneCell);
//		pdfPTable.addCell(companyweb);
//
//		//End By jayshree 31-3-2018
//		
//		/**
//		 * Developer:Jayshree Date 21/11/2017 Description:changes are done to
//		 * add the logo and website at proper position
//		 */
//		PdfPTable mainheader = new PdfPTable(2);
//		mainheader.setWidthPercentage(100);
//
//		try {
//			mainheader.setWidths(new float[] { 20, 80 });
//		} catch (DocumentException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
//
//		
//
//		if(con.getNumberRange()!=null &&!con.getNumberRange().equals("")){
//		if(nonbillingInvoice==true &&con.getNumberRange().equalsIgnoreCase("NonBilling")){
//			Phrase blank =new Phrase(" ");
//			PdfPCell blankcell = new PdfPCell(blank);
//			blankcell.setBorder(0);
//			blankcell.setColspan(2);
//			mainheader.addCell(blankcell);
//		}
//		else{
//			if (imageSignCell != null) {
//			PdfPCell leftCell = new PdfPCell(logoTab);
//			leftCell.setBorder(0);
//			mainheader.addCell(leftCell);
//
//			PdfPCell rightCell = new PdfPCell(pdfPTable);
//			rightCell.setBorder(0);
//			mainheader.addCell(rightCell);
//		} else {
//			PdfPCell rightCell = new PdfPCell(pdfPTable);
//			rightCell.setBorder(0);
//			rightCell.setColspan(2);
//			mainheader.addCell(rightCell);
//		
//				}
//			}
//		}
//		else{
//			if (imageSignCell != null) {
//			PdfPCell leftCell = new PdfPCell(logoTab);
//			leftCell.setBorder(0);
//			mainheader.addCell(leftCell);
//
//			PdfPCell rightCell = new PdfPCell(pdfPTable);
//			rightCell.setBorder(0);
//			mainheader.addCell(rightCell);
//		} else {
//			PdfPCell rightCell = new PdfPCell(pdfPTable);
//			rightCell.setBorder(0);
//			rightCell.setColspan(2);
//			mainheader.addCell(rightCell);
//		
//				}
//	
//	
//		
//		}
//		try {
//			document.add(mainheader);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		// rohan added this code
//		float[] myWidth = { 1, 3, 20, 17, 3, 30, 17, 3, 20, 1 };
//		PdfPTable mytbale = new PdfPTable(10);
//		mytbale.setWidthPercentage(100f);
//		mytbale.setSpacingAfter(5f);
//		mytbale.setSpacingBefore(5f);
//
//		try {
//			mytbale.setWidths(myWidth);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//		Phrase myblank = new Phrase(" ", font10);
//		PdfPCell myblankCell = new PdfPCell(myblank);
//		// stat1PhraseCell.addElement(stat1Phrase);
//		myblankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		Phrase myblankborderZero = new Phrase(" ", font10);
//		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
//		// stat1PhraseCell.addElement(stat1Phrase);
//		myblankborderZeroCell.setBorder(0);
//		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		Phrase stat1Phrase = new Phrase("Original for Receipient", font10);
//		PdfPCell stat1PhraseCell = new PdfPCell(stat1Phrase);
//		stat1PhraseCell.setBorder(0);
//		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		stat1PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//
//		Phrase stat2Phrase = new Phrase("Duplicate for Supplier/Transporter",
//				font10);
//		PdfPCell stat2PhraseCell = new PdfPCell(stat2Phrase);
//		stat2PhraseCell.setBorder(0);
//		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		stat2PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//
//		Phrase stat3Phrase = new Phrase("Triplicate for Supplier", font10);
//		PdfPCell stat3PhraseCell = new PdfPCell(stat3Phrase);
//		stat3PhraseCell.setBorder(0);
//		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		stat3PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//
//		// mytbale.addCell(myblankborderZeroCell);
//		// mytbale.addCell(myblankCell);
//		// mytbale.addCell(stat1PhraseCell);
//		// mytbale.addCell(myblankborderZeroCell);
//		// mytbale.addCell(myblankCell);
//		// mytbale.addCell(stat2PhraseCell);
//		// mytbale.addCell(myblankborderZeroCell);
//		// mytbale.addCell(myblankCell);
//		// mytbale.addCell(stat3PhraseCell);
//		// mytbale.addCell(myblankborderZeroCell);
//
//		PdfPTable tab = new PdfPTable(1);
//		tab.setWidthPercentage(100f);
//
//		PdfPCell cell = new PdfPCell(mytbale);
//		tab.addCell(cell);
//		// try {
//		// // document.add(tab);
//		// } catch (DocumentException e1) {
//		// e1.printStackTrace();
//		// }
//
//		// ends here
//		String titlepdf = "";
//		if(nonbillingInvoice==true){
//			if(invoiceentity.getBillingTaxes().size()==0){
//				/**Date 25-9-2020 by Amol commented this line raised by Rahul Tiwari.**/
//				titlepdf = "Estimate";
//				
//				/**Date 23-12-2020 by Priyanka commented this line raised by Ashwini for Om pest control.**/
//	//			titlepdf = "Invoice";
//				
//				/**
//				 * @author Anil @since 09-04-2021
//				 * If non billing process configuration is active and no tax selected then for Proforma invoice it should print title as
//				 * Proforma Invoice instead Estimate
//				 * Raised by Ashwini for Ultra Pest Control
//				 */
//				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())){
//					//titlepdf = "Proforma Invoice";
//					 if(changeTitle){
//							titlepdf = "Invoice";
//						}else{
//							titlepdf = "Proforma Invoice";
//						}
//				}
//			}
//		else{
//			if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
//					.getInvoiceType().trim())
//					|| invoiceentity.getInvoiceType().trim()
//							.equals(AppConstants.CREATEPROFORMAINVOICE))
//				//titlepdf = "Proforma Invoice";
//				 if(changeTitle){
//						titlepdf = "Invoice";
//					}else{
//						titlepdf = "Proforma Invoice";
//					}
//			else
//				titlepdf = "Tax Invoice";
//			
//			}
//		}
//		else{
//			/**
//			 * @author Anil @since 12-04-2021
//			 * For ultra pest control, if no tax is selected and non billing process configurationj is off then print 
//			 * Invoice on PDF else it will be Tax Invoice
//			 * Raised by Ashwini 
//			 */
////			if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
////				titlepdf = "Proforma Invoice";
////			else
////				titlepdf = "Tax Invoice";
//			
//			titlepdf = "Invoice";
//			if (invoiceentity.getBillingTaxes().size() == 0) {
//				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
//					//titlepdf = "Proforma Invoice";
//					 if(changeTitle){
//							titlepdf = "Invoice";
//						}else{
//							titlepdf = "Proforma Invoice";
//						}
//				}
//
//			} else {
//				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE)){
//					//titlepdf = "Proforma Invoice";
//					 if(changeTitle){
//							titlepdf = "Invoice";
//						}else{
//							titlepdf = "Proforma Invoice";
//						}
//				}else{
//					titlepdf = "Tax Invoice";
//				}
//			}
//			
//			
//		}
//		
//		/**
//		 * @author Anil @since 01-10-2021
//		 */
//		titlepdf=pdfUtility.getInvoiceTitle(invoiceentity, branchDt, titlepdf);
//		logger.log(Level.SEVERE,"After getInvoiceTitle Method : "+titlepdf +" | "+pdfUtility.printBankDetailsFlag);
//		
//		/**
//		 * @author Anil
//		 * @since 19-01-2022
//		 * if we are printing invoice for thai client then title will be hard coded as provided by client
//		 * raised by Nitin And Nithila
//		 */
//		System.out.println("first titlephrase thaiPdfFlag="+thaiPdfFlag);
//		if(thaiPdfFlag){
//			System.out.println("in thaiPdfFlag invoiceTitle= "+invoiceTitle);
//			if(!invoiceTitle.equals("")){
//				titlepdf=invoiceTitle;
//			}
//		}
//		
//		Phrase titlephrase = new Phrase(titlepdf, titlefont);//By Ashwini Patil
//		Paragraph titlepdfpara = new Paragraph();
//		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell titlecell = new PdfPCell();
//		titlecell.addElement(titlepdfpara);
//		titlecell.setBorder(0);
//		
//
//		Phrase blankphrase = new Phrase("", font8);
//		PdfPCell blankCell = new PdfPCell();
//		blankCell.addElement(blankphrase);
//		blankCell.setBorder(0);
//
//		PdfPTable titlepdftable = new PdfPTable(3);
//		titlepdftable.setWidthPercentage(100);
//		float[] columnThreePartWidths = { 1f, 2f, 1f };
//		try {
//			titlepdftable.setWidths(columnThreePartWidths);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
//		titlepdftable.addCell(blankCell);
//		titlepdftable.addCell(titlecell);
//		titlepdftable.addCell(blankCell);
//
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
//
//		PdfPTable parent = new PdfPTable(1);
//		parent.setWidthPercentage(100);
//		
//		/**
//		 * @author Anil
//		 * @since 19-01-2022
//		 * Need to add invoice copy name 
//		 * raised by Nithila and Nitin for Innovative
//		 */
//		if(thaiPdfFlag){
//			if(!copyTitle.equals("")){
//				parent.addCell(pdfUtility.getCell(copyTitle, font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
//			}
//		}
//		
//		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
//		parent.addCell(titlePdfCell);
//
//		try {
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}

	/** 23-10-2017 sagar sore **/
	public void createPdfForEmailGST(Invoice invoiceDetails,
			Company companyEntity, Customer custEntity,
			Contract contractEntity, BillingDocument billingEntity,
			List<CustomerBranchDetails> custbranchlist,
			ServiceGSTInvoice invpdf, Document document)// By Jayshree
	{

		Invoice invoiceentity = invoiceDetails;
		long count = invoiceentity.getId();
		logger.log(Level.SEVERE, " Count: " + count);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		boolean flag = false;
		try {
			flag = invoiceentity.getInvoiceDate().after(
					sdf.parse("30 Jun 2017"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("invoice Date " + invoiceentity.getInvoiceDate());

		System.out.println("SINGLE CONTRACT INVOICE");

		String preprintStatus = "plane";
		System.out.println("ppppppppppppppppppooooooooooo" + count);
		invpdf.setInvoice(count);
		invpdf.createPdf(preprintStatus);// By jayshree
		document.close();

	}

	public PdfPTable getAddressTable(CustomerBranchDetails customerBranch, Customer cust,
			Contract con, PdfPTable colonTable, PdfUtility pdfUtility, boolean adresstouppercase, 
			boolean addcellNoFlag, long companyId, String pocName, String documentName,
			boolean billingAddressFlag, boolean serviceAddressFlag) {
		
		String customerName = pdfUtility.getCustomerCompanyName(customerBranch,cust,serviceAddressFlag);
		String	Atten = pdfUtility.getCustomerAtten(customerBranch,cust);
		String	address = pdfUtility.getAddress(customerBranch,cust,adresstouppercase,con,billingAddressFlag,serviceAddressFlag);
//		String email = pdfUtility.getCustomerEmail(customerBranch,cust);
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
		address=pdfUtility.removePinAsHeadingFromAddress(companyId, "Invoice", address);
		
		colonTable.addCell(pdfUtility.getPdfCell("Name", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
		colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
		colonTable.addCell(pdfUtility.getPdfCell(customerName, nameAddressBoldFont, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
		if(!Atten.equals("")){
			colonTable.addCell(pdfUtility.getPdfCell("Attn", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			colonTable.addCell(pdfUtility.getPdfCell(Atten, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
		}
		colonTable.addCell(pdfUtility.getPdfCell("Address", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
		colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
		colonTable.addCell(pdfUtility.getPdfCell(address, nameAddressFont, Element.ALIGN_LEFT, null, 0, 0, 0,0,5,-1,-1,-1,-1));
//		if(!email.equals("")){
//			colonTable.addCell(pdfUtility.getPdfCell("Email", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
//			colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
//			colonTable.addCell(pdfUtility.getPdfCell(email, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
//		}

//		String countryName=pdfUtility.getBillingAddressCountryName(customerBranch, cust, con,billingAddressFlag,serviceAddressFlag);
//		String phoneNumber = pdfUtility.getPhoneNumber(customerBranch,cust,countryName,companyId);
		
//		if(addcellNoFlag && !phoneNumber.equals("")){
//			phoneNumber += " ("+pocName+ ")";
//		}
//		if(!phoneNumber.equals("")){
//			colonTable.addCell(pdfUtility.getPdfCell("Phone", font10bold, Element.ALIGN_LEFT, null,0, 0, 0,0,0,-1,-1,-1,-1));
//			colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null,0, 0, 0,0,0,-1,-1,-1,-1));
//			colonTable.addCell(pdfUtility.getPdfCell(phoneNumber, font10, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,-1,-1,-1,-1));
//		}
		
		PdfPTable stateTable = new PdfPTable(6);
		stateTable.setWidthPercentage(100);
		float[] columnWidth = {  1.8f, 0.2f, 3.5f, 1.8f, 0.2f, 1.6f};
		try {
			stateTable.setWidths(columnWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String state="";
		String stateCode="";
		if(customerBranch!=null){
			if(customerBranch.getBillingAddress().getState()!=null)
				state=customerBranch.getBillingAddress().getState();
		}
		else if(con!=null&&con.getNewcustomerAddress()!=null){
			if(con.getNewcustomerAddress().getState()!=null)
				state= con.getNewcustomerAddress().getState();
		}else {
			state=cust.getAdress().getState();
		}
		
		if(state!=null&&!state.equals("")){
			stateTable.addCell(pdfUtility.getPdfCell("State", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			stateTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			stateTable.addCell(pdfUtility.getPdfCell(state, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
			
			if(stateList!=null){
				for(State st:stateList){
					if(st.getStateName().equals(state)){
						if(st.getStateCode()!=null)
							stateCode=st.getStateCode();
						break;
					}					
				}
				if(!stateCode.equals("")){
					stateTable.addCell(pdfUtility.getPdfCell("State Code", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
					stateTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
					stateTable.addCell(pdfUtility.getPdfCell(stateCode, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));			
				}else{
					stateTable.addCell(pdfUtility.getPdfCell("", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
					stateTable.addCell(pdfUtility.getPdfCell("", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
					stateTable.addCell(pdfUtility.getPdfCell("", font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));							
				}
			}
//			colonTable.addCell(pdfUtility.getPdfCell(part3, font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));							
			
			PdfPCell stateTableCell = new PdfPCell();
			stateTableCell.addElement(stateTable);
			stateTableCell.setColspan(3);
			stateTableCell.setBorder(0);
			stateTableCell.setPaddingLeft(0);
			stateTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			colonTable.addCell(stateTableCell);
		}
		

		if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
			for(ArticleType type:cust.getArticleTypeDetails()){
				if((type.getDocumentName().equalsIgnoreCase(documentName)&&type.getArticlePrint().equalsIgnoreCase("Yes")) ||
						(type.getDocumentName().equalsIgnoreCase(documentName)&&type.getArticlePrint().equalsIgnoreCase("Yes"))){
					if(type.getArticleTypeValue()!=null && !type.getArticleTypeValue().equals("")){
						
						colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeName(), font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
						colonTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
						colonTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeValue(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));

					}
										
				}
			}
		}
		
		
		return colonTable;
	}
	


}
