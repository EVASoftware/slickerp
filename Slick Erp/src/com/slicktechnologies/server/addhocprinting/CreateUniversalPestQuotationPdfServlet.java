package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;


@SuppressWarnings("serial")
public class CreateUniversalPestQuotationPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4384632791531604631L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		resp.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.

	try {
		String stringid = req.getParameter("Id");
		stringid=stringid.trim();
		Long count =Long.parseLong(stringid);
	    String preprintStatus=req.getParameter("preprint");

	    String type = req.getParameter("type");
	    type = type.trim();
	    
	    if(type.contains("q"))
	    {
	    	UniversalPestQuotationPdf universalQuotationPdf = new UniversalPestQuotationPdf();
	 	    universalQuotationPdf.document = new Document(); 
	 	    Document document = universalQuotationPdf.document;
	 	    PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write the pdf in response
	 	  
	 	    
	 	    document.open();
	 	    System.out.println("****************"+preprintStatus);
	 	    if(preprintStatus.contains("yes")){
	 		   
	 	    	universalQuotationPdf.setservicequotation(count);
	 	    	universalQuotationPdf.createPdf(preprintStatus);
	 	    }
	 	   
	 	    if(preprintStatus.contains("no")){
	 		   
	 	    	universalQuotationPdf.setservicequotation(count);
	 	    	universalQuotationPdf.createPdf(preprintStatus);
	 	    
	 	    }
	 	    
	 	    document.close();
	    }
	    else
	    {
	    	UniversalPestContractPdf universalContactPdf = new UniversalPestContractPdf();
	    	universalContactPdf.document = new Document(); 
		 	    Document document = universalContactPdf.document;
		 	    PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write the pdf in response
		 	  
		 	    
		 	    document.open();
		 	    System.out.println("****************"+preprintStatus);
		 	    if(preprintStatus.contains("yes")){
		 		   
		 	    	universalContactPdf.setservicequotation(count);
		 	    	universalContactPdf.createPdf(preprintStatus);
		 	    }
		 	   
		 	    if(preprintStatus.contains("no")){
		 		   
		 	    	universalContactPdf.setservicequotation(count);
		 	    	universalContactPdf.createPdf(preprintStatus);
		 	    
		 	    }
		 	    
		 	    document.close();
	    }
	   
	}
	catch(DocumentException e )
	{
		
	} 
}
		 
}
