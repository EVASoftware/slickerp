package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;
import java.util.ArrayList;
import java.util.List;


public class EmployeePdf {
	int expdetailtemp=0,edudetailtemp=0,empaddressdetailtemp=0,empfamilydetailtemp=0;

	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);

	float[] columnWidths = {1f, 0.5f, 3f, 1f,0.5f,3f};
	float[] addressemployeewidths={1.5f, 0.5f, 3f, 1.5f,0.5f,3f};
	float[] contactemployeewidths={2f, 0.5f, 3f, 2f,0.5f,3f};
	
	float[] infoemployeewidths={1.55f, 0.15f, 3f, 1.55f,0.15f,3f};
	
	Employee employee;
	EmployeeAdditionalDetails empadditionaldetails;
	Company comp;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	public Document document;
	List<ArticleType> articletype= new ArrayList<ArticleType>();
	
	public void getEmployeeDeatils(Long count) {
		employee = ofy().load().type(Employee.class).id(count).now();
		if (employee != null) {
			articletype = employee.getArticleTypeDetails();
		}
		if (employee.getCompanyId() != null)
			comp = ofy().load().type(Company.class).filter("companyId", employee.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (employee.getCompanyId() != null) {
			empadditionaldetails = ofy().load().type(EmployeeAdditionalDetails.class).filter("empInfo.count", employee.getCount())
					.filter("companyId", employee.getCompanyId()).first().now();
		} else {
			empadditionaldetails = ofy().load().type(EmployeeAdditionalDetails.class).first().now();
		}

	}

	public void createPdf() {
		createLogo(document,comp);
		createCompDetails();
		createphoto(document,comp);
		createEmployeeDetailspdf();
		createEmployeepayrollinfo();
		createEmployeeContactinfo();
		createPassportInfo();
		createSocialInfo();
		
		if(articletype.size()!=0){
			createArticleInfo();  // added by Ajinkya to add Article Info on pdf    Date : 27/04/2017
		}
		if(employee.getPreviousCompanyHistory().size()!=0){
			System.out.println("List Size"+employee.getPreviousCompanyHistory().size());
			expdetailtemp=0;
			createExperienceDetails();
		}
		if(employee.getEducationalInfo().size()!=0){
			edudetailtemp=0;
			createEducationalDetails();	
		}
		if(empadditionaldetails!=null){
			if(empadditionaldetails.getEmpFamDetList().size()!=0){
				empfamilydetailtemp=0;
				employeeFamilyDetails();
			}
			if(empadditionaldetails.getEmpAddDetList().size()!=0){
				empaddressdetailtemp=0;
				employeeAddressDetails();
			}
		}
			
		
	}
	/**
	 * Date : 27/04/2017
	 * Ajinkya added this method  to print Article Info on Pdf 
	 * 
	 * 
	 */
	private void createArticleInfo() {
      
		Phrase title = new Phrase("EMPLOYEE ARTICLE INFORMATION : ", font10bold);

//		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		titlecell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		titlecell.setFixedHeight(20);
		
		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setBorderWidthBottom(0);
		
		
		//Adjustment table
		PdfPTable articleheadertable = new PdfPTable(1);
		articleheadertable.setWidthPercentage(100);
		articleheadertable.addCell(titlecell);
		
		try {
			document.add(articleheadertable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable articleInFoTable = new  PdfPTable(4);
		articleInFoTable.setWidthPercentage(100);
		
		try {
//			articleInFoTable.setWidths(new float[] {14,2,34,16,2,32} );
			articleInFoTable.setWidths(new float[] {1.55f,0.17f,3f,4.72f} );
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
		for (int i = 0; i < this.articletype.size(); i++) {
			System.out.println(articletype.get(i).getArticleTypeName());
			System.out.println(articletype.get(i).getArticleTypeValue());
			System.out.println(articletype.get(i).getArticlePrint());
			System.out.println(articletype.get(i).getDocumentName());
			if (articletype.get(i).getArticleTypeName() != null
					&& articletype.get(i).getArticleTypeValue() != null
					&& !articletype.get(i).getArticleTypeValue().equals("")
//					&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES")
//					&& articletype.get(i).getDocumentName().equalsIgnoreCase("Employee")
					) {

				

				Phrase articleName = new Phrase(articletype.get(i).getArticleTypeName(), font10);
				PdfPCell artNmeCell = new PdfPCell(articleName);
				artNmeCell.setBorder(0);
				artNmeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				artNmeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				articleInFoTable.addCell(artNmeCell);
				// /////////////////////////////////////////
				Phrase colon = new Phrase(":", font10);
				PdfPCell colonCell = new PdfPCell(colon);
				colonCell.setBorder(0);
				colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				colonCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				articleInFoTable.addCell(colonCell);
				// ////////////////////////////////////////
				Phrase articleNameVal = new Phrase(articletype.get(i).getArticleTypeValue(), font10);
				PdfPCell artNmeValCell = new PdfPCell(articleNameVal);
				artNmeValCell.setBorder(0);
				artNmeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				artNmeValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				articleInFoTable.addCell(artNmeValCell);
				
				
				Phrase blankPh = new Phrase("", font10);
				PdfPCell blankCell = new PdfPCell(blankPh);
				blankCell.setBorder(0);
				blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				blankCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				articleInFoTable.addCell(blankCell);
			} 
			
			
//			else if (articletype.get(i).getArticleTypeName() != null
//					&& articletype.get(i).getArticleTypeValue() == null
//					&& articletype.get(i).getArticleTypeValue().equals("")
//					&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES")
//					&& articletype.get(i).getDocumentName().equalsIgnoreCase("Employee")) {
//
//				Phrase articleName = new Phrase(articletype.get(i).getArticleTypeName(), font10);
//				PdfPCell artNmeCell = new PdfPCell(articleName);
//				artNmeCell.setBorder(0);
//				artNmeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				artNmeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				articleInFoTable.addCell(artNmeCell);
//
//				Phrase colon = new Phrase(":", font10);
//				PdfPCell colonCell = new PdfPCell(colon);
//				colonCell.setBorder(0);
//				colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				colonCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				articleInFoTable.addCell(colonCell);
//
//				Phrase articleNameVal = new Phrase("", font10);
//				PdfPCell artNmeValCell = new PdfPCell(articleNameVal);
//				artNmeValCell.setBorder(0);
//				artNmeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				artNmeValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				articleInFoTable.addCell(artNmeValCell);
//
//			} else {
//				Phrase articleNameVal = new Phrase("", font10);
//				PdfPCell artNmeValCell = new PdfPCell(articleNameVal);
//				artNmeValCell.setBorder(0);
//				artNmeValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				artNmeValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//				articleInFoTable.addCell(artNmeValCell);
//
//			}

		}
		PdfPCell artNmeValCell = new PdfPCell(articleInFoTable);
		artNmeValCell.setBorderWidthTop(0);
		
		  PdfPTable borderTbl = new PdfPTable(1);
		  borderTbl.setWidthPercentage(100);
		  borderTbl.addCell(artNmeValCell);
//		  borderTbl.addCell(articleInFoTable);
//		  borderTbl.setBorderWidthTop(0);
		 try {
				document.add(borderTbl);  
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 
		
		
	}

	private void employeeadditionalDetails() {
		// TODO Auto-generated method stub
		
	}



	private void createphoto(Document doc, Company comp2) {
		DocumentUpload document = employee.getPhoto();
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(36f);
			image2.setAbsolutePosition(480f,710f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
			}

		
		

//		 try
//		 {
//		 Image image1=Image.getInstance("images/employeephoto.jpg");
//		 image1.scalePercent(36f);
//		 
//		 image1.setAbsolutePosition(480f,710f);
//		 doc.add(image1);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
	}



	private void createLogo(Document doc, Company comp) {

		// ********************logo for server ********************
		DocumentUpload document = comp.getLogo();
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(37f,780f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
			}

		

//		 try
//		 {
//		 Image image1=Image.getInstance("images/ultrapest.jpg");
//		 image1.scalePercent(20f);
//		 image1.setAbsolutePosition(37f,780f);
//		 doc.add(image1);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
	}
	

	

	private void createCompDetails() {


		Phrase compname = new Phrase(comp.getBusinessUnitName(), font16boldul);
		Paragraph comppara = new Paragraph();
		// comppara.add(Chunk.NEWLINE);
		comppara.add(compname);
		comppara.setAlignment(Element.ALIGN_CENTER);

		// Creating Address Details.

		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),
				font12);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font12);
		}
		Phrase landmark = null;
		Phrase locality = null;

		if (comp.getAddress().getLandmark() != null
				&& comp.getAddress().getLocality().equals("") == false) {
			String landmarks = comp.getAddress().getLandmark() + " , ";

			locality = new Phrase(landmarks + comp.getAddress().getLocality()
					+ " , " + comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		} else {
			locality = new Phrase(comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		}

		Paragraph adressPragraph = new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);

		if (adressline2 != null) {
			adressPragraph.add(adressline2);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(locality);

		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);

		// Phrase for phone,landline ,fax and email

		Phrase titlecell = new Phrase("Mob :", font8bold);
		Phrase titleTele = new Phrase("Tel :", font8bold);
		Phrase titleemail = new Phrase("Email :", font8bold);
		Phrase titlefax = new Phrase("Fax :", font8bold);

		// cell number logic
		String stringcell1 = comp.getContact().get(0).getCellNo1() + "";
		String stringcell2 = null;
		Phrase mob = null;
		if (comp.getContact().get(0).getCellNo2() != -1)
			stringcell2 = comp.getContact().get(0).getCellNo2() + "";
		if (stringcell2 != null)
			mob = new Phrase(stringcell1 + " / " + stringcell2, font8);
		else
			mob = new Phrase(stringcell1, font8);

		// LANDLINE LOGIC
		Phrase landline = null;
		if (comp.getContact().get(0).getLandline() != -1)
			landline = new Phrase(comp.getContact().get(0).getLandline() + "",
					font8);

		// fax logic
		Phrase fax = null;
		if (comp.getContact().get(0).getFaxNo() != null)
			fax = new Phrase(comp.getContact().get(0).getFaxNo() + "", font8);

		// email logic
		Phrase email = new Phrase(comp.getContact().get(0).getEmail(), font8);

		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk(" "));

		if (landline != null) {

			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk(" "));
		}

		if (fax != null) {

			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(titleemail);
		// adressPragraph.add(new Chunk("            "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);

		// Creating Cell.

		PdfPCell compnamecell = new PdfPCell();
		compnamecell.addElement(comppara);
		// compnamecell.setFixedHeight(50f);
		compnamecell.setBorder(0);

		PdfPCell addcell = new PdfPCell();
		addcell.addElement(adressPragraph);
		addcell.setBorder(0);

		// Creating Table

		PdfPTable compdetailtable = new PdfPTable(1);
		compdetailtable.setWidthPercentage(100);

		// Adding Cells in Table.

		compdetailtable.addCell(compnamecell);
		compdetailtable.addCell(addcell);

		PdfPCell adjustcell = new PdfPCell();
		adjustcell.addElement(compdetailtable);

		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);
		adjusttable.addCell(adjustcell);

		try {
			document.add(adjusttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createEmployeeDetailspdf() {
		// TODO Auto-generated method stub

		// Static Phrases
		Phrase employeeIDphrase = new Phrase("Employment ID", font10);
		Phrase baseCountryphrase = new Phrase("Base Country", font10);
		Phrase departmentphrase = new Phrase("Department", font10);
		Phrase employeetypephrase = new Phrase("Employee Type", font10);
		Phrase namephrase = new Phrase("Name", font10);
		Phrase dobphrase = new Phrase("D.O.B", font10);
		Phrase branchphrase = new Phrase("Branch", font10);
		Phrase designationphrase = new Phrase("Designation", font10);
		Phrase rolephrase = new Phrase("Role", font10);
		Phrase joiningdatephrase = new Phrase("Joining Date", font10);
		Phrase reportstophrase = new Phrase("Reports To", font10);
		Phrase accesscardnophrase = new Phrase("Access Card No.", font10);
		Phrase idcardnophrase = new Phrase("ID Card No.", font10);
		Phrase bloodgroupphrase = new Phrase("Blood Group", font10);
		Phrase colonphrase = new Phrase(":", font10);

		// Dynamic Phrases
		Phrase employeeIDvalue = null;
		employeeIDvalue = new Phrase(employee.getCount() + "", font10);

		Phrase baseCountryvalue = null;
		baseCountryvalue = new Phrase(employee.getCountry() + "", font10);
		Phrase departmentvalue = null;
		if (employee.getDepartMent() != null) {
			departmentvalue = new Phrase(employee.getDepartMent() + "", font10);
		} else {
			departmentvalue = new Phrase("", font10);
		}
		Phrase employeetypevalue = null;
		if (employee.getEmployeeType() != null) {
			employeetypevalue = new Phrase(employee.getEmployeeType() + "",font10);
		} else {
			employeetypevalue = new Phrase("", font10);
		}
		Phrase namevalue = null;
		namevalue = new Phrase(employee.getFullname() + "", font10);
		
		Phrase dobvalue = null;
		dobvalue = new Phrase(fmt.format(employee.getDob()) + "", font10);
		
		Phrase branchvalue = null;
		branchvalue = new Phrase(employee.getBranchName() + "", font10);
		
		Phrase designationvalue = null;
		designationvalue = new Phrase(employee.getDesignation() + "", font10);
		
		Phrase rolevalue = null;
		rolevalue = new Phrase(employee.getRoleName() + "", font10);
		
		Phrase joiningdatevalue = null;
		joiningdatevalue = new Phrase(fmt.format(employee.getJoinedAt()),font10);
		
		Phrase reportstovalue = null;
		if (employee.getReportsTo() != null) {
			reportstovalue = new Phrase(employee.getReportsTo() + "", font10);
		} else {
			reportstovalue = new Phrase(" ", font10);
		}

		Phrase accesscardnovalue = null;
		if (employee.getAccessCardNo() != null) {
			accesscardnovalue = new Phrase(employee.getAccessCardNo() + "",font10);
		} else {
			accesscardnovalue = new Phrase(" ", font10);
		}

		Phrase idcardnovalue = null;
		if (employee.getIdCardNo() != null) {
			idcardnovalue = new Phrase(employee.getIdCardNo() + "", font10);
		} else {
			idcardnovalue = new Phrase(" ", font10);
		}
		Phrase bloodgroupvalue = null;
		if (employee.getBloodGroup() != null) {
			bloodgroupvalue = new Phrase(employee.getBloodGroup() + "", font10);
		} else {
			bloodgroupvalue = new Phrase(" ", font10);
		}

		// Creating Static Cell
		Paragraph employeeIDpara = new Paragraph();
		employeeIDpara.add(employeeIDphrase);
		employeeIDpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell employeeIDcell = new PdfPCell(employeeIDphrase);
//		employeeIDcell.addElement(employeeIDpara);
		employeeIDcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeeIDcell.setBorder(0);

		Paragraph baseCountrypara = new Paragraph();
		baseCountrypara.add(baseCountryphrase);
		baseCountrypara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell baseCountrycell = new PdfPCell(baseCountryphrase);
//		baseCountrycell.addElement(baseCountrypara);
		baseCountrycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		baseCountrycell.setBorder(0);

		Paragraph departmentpara = new Paragraph();
		departmentpara.add(departmentphrase);
		departmentpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell departmentcell = new PdfPCell(departmentphrase);
//		departmentcell.addElement(departmentpara);
		departmentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		departmentcell.setBorder(0);

		Paragraph employeetypepara = new Paragraph();
		employeetypepara.add(employeetypephrase);
		employeetypepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell employeetypecell = new PdfPCell(employeetypephrase);
//		employeetypecell.addElement(employeetypepara);
		employeetypecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetypecell.setBorder(0);

		Paragraph namepara = new Paragraph();
		namepara.add(namephrase);
		namepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell namecell = new PdfPCell(namephrase);
//		namecell.addElement(namepara);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		namecell.setBorder(0);

		Paragraph dobpara = new Paragraph();
		dobpara.add(dobphrase);
		dobpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell dobcell = new PdfPCell(dobphrase);
//		dobcell.addElement(dobpara);
		dobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dobcell.setBorder(0);

		Paragraph branchpara = new Paragraph();
		branchpara.add(branchphrase);
		branchpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell branchcell = new PdfPCell(branchphrase);
//		branchcell.addElement(branchpara);
		branchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchcell.setBorder(0);

		Paragraph designationpara = new Paragraph();
		designationpara.add(designationphrase);
		designationpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell designationcell = new PdfPCell(designationphrase);
//		designationcell.addElement(designationpara);
		designationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		designationcell.setBorder(0);

		Paragraph rolepara = new Paragraph();
		rolepara.add(rolephrase);
		rolepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell rolecell = new PdfPCell(rolephrase);
//		rolecell.addElement(rolepara);
		rolecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rolecell.setBorder(0);

		Paragraph joiningdatepara = new Paragraph();
		joiningdatepara.add(joiningdatephrase);
		joiningdatepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell joiningdatecell = new PdfPCell(joiningdatephrase);
//		joiningdatecell.addElement(joiningdatepara);
		joiningdatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		joiningdatecell.setBorder(0);

		Paragraph reportstopara = new Paragraph();
		reportstopara.add(reportstophrase);
		reportstopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell reportstocell = new PdfPCell(reportstophrase);
//		reportstocell.addElement(reportstopara);
		reportstocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		reportstocell.setBorder(0);

		Paragraph accesscardnopara = new Paragraph();
		accesscardnopara.add(accesscardnophrase);
		accesscardnopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell accesscardnocell = new PdfPCell(accesscardnophrase);
//		accesscardnocell.addElement(accesscardnopara);
		accesscardnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		accesscardnocell.setBorder(0);

		Paragraph idcardnopara = new Paragraph();
		idcardnopara.add(idcardnophrase);
		idcardnopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell idcardnocell = new PdfPCell(idcardnophrase);
//		idcardnocell.addElement(idcardnopara);
		idcardnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		idcardnocell.setBorder(0);

		Paragraph bloodgrouppara = new Paragraph();
		bloodgrouppara.add(bloodgroupphrase);
		bloodgrouppara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bloodgroupcell = new PdfPCell(bloodgroupphrase);
//		bloodgroupcell.addElement(bloodgrouppara);
		bloodgroupcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bloodgroupcell.setBorder(0);

		Paragraph colonpara = new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell coloncell = new PdfPCell(colonphrase);
//		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_CENTER);
		coloncell.setBorder(0);

		// Creating dynamic cell

		Paragraph employeeIvaluepara = new Paragraph();
		employeeIvaluepara.add(employeeIDvalue);
		employeeIvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell employeeIvaluecell = new PdfPCell(employeeIDvalue);
//		employeeIvaluecell.addElement(employeeIvaluepara);
		employeeIvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeeIvaluecell.setBorder(0);

		Paragraph baseCountryvaluepara = new Paragraph();
		baseCountryvaluepara.add(baseCountryvalue);
		baseCountryvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell baseCountryvaluecell = new PdfPCell(baseCountryvalue);
//		baseCountryvaluecell.addElement(baseCountryvaluepara);
		baseCountryvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		baseCountryvaluecell.setBorder(0);

		Paragraph departmentvaluepara = new Paragraph();
		departmentvaluepara.add(departmentvalue);
		departmentvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell departmentvaluecell = new PdfPCell(departmentvalue);
//		departmentvaluecell.addElement(departmentvaluepara);
		departmentvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		departmentvaluecell.setBorder(0);

		Paragraph employeetypevaluepara = new Paragraph();
		employeetypevaluepara.add(employeetypevalue);
		employeetypevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell employeetypevaluecell = new PdfPCell(employeetypevalue);
//		employeetypevaluecell.addElement(employeetypevaluepara);
		employeetypevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		employeetypevaluecell.setBorder(0);

		Paragraph namevaluepara = new Paragraph();
		namevaluepara.add(namevalue);
		namevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell namevaluecell = new PdfPCell(namevalue);
//		namevaluecell.addElement(namevaluepara);
		namevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		namevaluecell.setBorder(0);

		Paragraph dobvaluepara = new Paragraph();
		dobvaluepara.add(dobvalue);
		dobvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell dobvaluecell = new PdfPCell(dobvalue);
//		dobvaluecell.addElement(dobvaluepara);
		dobvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dobvaluecell.setBorder(0);

		Paragraph branchvaluepara = new Paragraph();
		branchvaluepara.add(branchvalue);
		branchvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell branchvaluecell = new PdfPCell(branchvalue);
//		branchvaluecell.addElement(branchvaluepara);
		branchvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchvaluecell.setBorder(0);

		Paragraph designationvaluepara = new Paragraph();
		designationvaluepara.add(designationvalue);
		designationvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell designationvaluecell = new PdfPCell(designationvalue);
//		designationvaluecell.addElement(designationvaluepara);
		designationvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		designationvaluecell.setBorder(0);

		Paragraph rolevaluepara = new Paragraph();
		rolevaluepara.add(rolevalue);
		rolevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell rolevaluecell = new PdfPCell(rolevalue);
//		rolevaluecell.addElement(rolevaluepara);
		rolevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rolevaluecell.setBorder(0);

		Paragraph joiningdatevaluepara = new Paragraph();
		joiningdatevaluepara.add(joiningdatevalue);
		joiningdatevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell joiningdatevaluecell = new PdfPCell(joiningdatevalue);
//		joiningdatevaluecell.addElement(joiningdatevaluepara);
		joiningdatevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		joiningdatevaluecell.setBorder(0);

		Paragraph reportstovaluepara = new Paragraph();
		reportstovaluepara.add(reportstovalue);
		reportstovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell reportstovaluecell = new PdfPCell(reportstovalue);
//		reportstovaluecell.addElement(reportstovaluepara);
		reportstovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		reportstovaluecell.setBorder(0);

		Paragraph accesscardnovaluepara = new Paragraph();
		accesscardnovaluepara.add(accesscardnovalue);
		accesscardnovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell accesscardnovaluecell = new PdfPCell(accesscardnovalue);
//		accesscardnovaluecell.addElement(accesscardnovaluepara);
		accesscardnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		accesscardnovaluecell.setBorder(0);

		Paragraph idcardnovaluepara = new Paragraph();
		idcardnovaluepara.add(idcardnovalue);
		idcardnovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell idcardnovaluecell = new PdfPCell(idcardnovalue);
//		idcardnovaluecell.addElement(idcardnovaluepara);
		idcardnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		idcardnovaluecell.setBorder(0);

		Paragraph bloodgroupvaluepara = new Paragraph();
		bloodgroupvaluepara.add(bloodgroupvalue);
		bloodgroupvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bloodgroupvaluecell = new PdfPCell(bloodgroupvalue);
//		bloodgroupvaluecell.addElement(bloodgroupvaluepara);
		bloodgroupvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bloodgroupvaluecell.setBorder(0);

		// Creating Table
		PdfPTable employeeinfotable = new PdfPTable(6);
		employeeinfotable.setWidthPercentage(100);
		try {
			employeeinfotable.setWidths(infoemployeewidths);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		// Adding all cells in table
		employeeinfotable.addCell(employeeIDcell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(employeeIvaluecell);
		employeeinfotable.addCell(baseCountrycell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(baseCountryvaluecell);
		employeeinfotable.addCell(departmentcell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(departmentvaluecell);
		employeeinfotable.addCell(employeetypecell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(employeetypevaluecell);
		employeeinfotable.addCell(namecell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(namevaluecell);
		employeeinfotable.addCell(dobcell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(dobvaluecell);
		employeeinfotable.addCell(branchcell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(branchvaluecell);
		employeeinfotable.addCell(designationcell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(designationvaluecell);
		employeeinfotable.addCell(rolecell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(rolevaluecell);
		employeeinfotable.addCell(joiningdatecell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(joiningdatevaluecell);
		employeeinfotable.addCell(reportstocell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(reportstovaluecell);
		employeeinfotable.addCell(accesscardnocell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(accesscardnovaluecell);
		employeeinfotable.addCell(idcardnocell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(idcardnovaluecell);
		employeeinfotable.addCell(bloodgroupcell);
		employeeinfotable.addCell(coloncell);
		employeeinfotable.addCell(bloodgroupvaluecell);
		try {
			employeeinfotable.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfPCell adjustcell = new PdfPCell();
		adjustcell.addElement(employeeinfotable);
		
		adjustcell.setBorderWidthTop(0f);

		// Heading Part code
		Phrase title = new Phrase("EMPLOYEE INFORMATION : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setBorderWidthBottom(0);
		adjustcell.setBorderWidthTop(0);

		// Blank Cell
		// Phrase blank = new Phrase(" ", font10bold);
		//
		// Paragraph blankpara = new Paragraph();
		// blankpara.add(blank);
		// blankpara.setAlignment(Element.ALIGN_CENTER);

		// PdfPCell blankcell=new PdfPCell();
		// blankcell.addElement(blankpara);
		// blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// blankcell.setBorder(0);
		//
		PdfPTable adjusttable = new PdfPTable(1);
		
		adjusttable.setWidthPercentage(100);

		adjusttable.addCell(titlecell);
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);

		try {
			document.add(adjusttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createEmployeepayrollinfo() {
		// TODO Auto-generated method stub

		// Static Phrases
		Phrase banknamephrase = new Phrase("Bank Name", font10);
		Phrase bankbranchphrase = new Phrase("Bank Branch", font10);
		Phrase ifscphrase = new Phrase("IFSC", font10);
		Phrase bankaccountnophrase = new Phrase("Bank Account No.", font10);
		Phrase esicnophrase = new Phrase("ESIC No.", font10);
		Phrase pannophrase = new Phrase("PAN No.", font10);
		Phrase ppfnophrase = new Phrase("PPF No.", font10);
		Phrase colon = new Phrase(":", font10);

		// Dynamic Phrases
		Phrase banknamevalue = null;
		if (employee.getEmployeeBankName() != null) {
			banknamevalue = new Phrase(employee.getEmployeeBankName(), font10);
		} else {
			banknamevalue = new Phrase(" ", font10);
		}
		Phrase bankbranchvalue = null;

		if (employee.getBankBranch() != null) {
			bankbranchvalue = new Phrase(employee.getBankBranch(), font10);
		} else {
			bankbranchvalue = new Phrase(" ", font10);
		}

		Phrase ifscvalue = null;
		if (employee.getIfscCode() != null) {
			ifscvalue = new Phrase(employee.getIfscCode(), font10);
		} else {
			ifscvalue = new Phrase(" ", font10);
		}
		/**
		 * Date : 26-04-2018 BY ANIL
		 * Earlier it was only account no. and it was not printed on pdf
		 */
		Phrase bankaccountnovalue = null;
		if (employee.getEmployeeBankAccountNo() != null) {
			bankaccountnovalue = new Phrase(employee.getEmployeeBankAccountNo(), font10);
		} else {
			bankaccountnovalue = new Phrase(" ", font10);
		}
		/**
		 * End
		 */
		Phrase esicnovalue = null;
		if (employee.getEmployeeESICcode() != null) {
			esicnovalue = new Phrase(employee.getEmployeeESICcode(), font10);
		} else {
			esicnovalue = new Phrase(" ", font10);
		}
		Phrase pannovalue = null;
		if (employee.getEmployeePanNo() != null) {
			pannovalue = new Phrase(employee.getEmployeePanNo(), font10);
		} else {
			pannovalue = new Phrase(" ", font10);
		}
		Phrase ppfnovalue = null;
		if (employee.getPPFNaumber() != null) {
			ppfnovalue = new Phrase(employee.getPPFNaumber(), font10);
		} else {
			ppfnovalue = new Phrase(" ", font10);
		}

		// Creating Static Cell

		Paragraph banknamepara = new Paragraph();
		banknamepara.add(banknamephrase);
		banknamepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell banknamecell = new PdfPCell(banknamephrase);
//		banknamecell.addElement(banknamepara);
		banknamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		banknamecell.setBorder(0);

		Paragraph bankbranchpara = new Paragraph();
		bankbranchpara.add(bankbranchphrase);
		bankbranchpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bankbranchcell = new PdfPCell(bankbranchphrase);
//		bankbranchcell.addElement(bankbranchpara);
		bankbranchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankbranchcell.setBorder(0);

		Paragraph ifscpara = new Paragraph();
		ifscpara.add(ifscphrase);
		ifscpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell ifsccell = new PdfPCell(ifscphrase);
//		ifsccell.addElement(ifscpara);
		ifsccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		ifsccell.setBorder(0);
		
		Paragraph bankaccountnopara = new Paragraph();
		bankaccountnopara.add(bankaccountnophrase);
		bankaccountnopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bankaccountnocell = new PdfPCell(bankaccountnophrase);
//		bankaccountnocell.addElement(bankaccountnopara);
		bankaccountnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankaccountnocell.setBorder(0);
		
		Paragraph esicnopara = new Paragraph();
		esicnopara.add(esicnophrase);
		esicnopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell esicnocell = new PdfPCell(esicnophrase);
//		esicnocell.addElement(esicnopara);
		esicnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		esicnocell.setBorder(0);
		
		Paragraph pannopara = new Paragraph();
		pannopara.add(pannophrase);
		pannopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell pannocell = new PdfPCell(pannophrase);
//		pannocell.addElement(pannopara);
		pannocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pannocell.setBorder(0);
		
		Paragraph ppfnopara = new Paragraph();
		ppfnopara.add(ppfnophrase);
		ppfnopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell ppfnocell = new PdfPCell(ppfnophrase);
//		ppfnocell.addElement(ppfnopara);
		ppfnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		ppfnocell.setBorder(0);
		
		Paragraph colonpara = new Paragraph();
		colonpara.add(colon);
		colonpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell coloncell = new PdfPCell(colon);
//		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		// Creating Dynamic Cell

		Paragraph banknamevaluepara = new Paragraph();
		banknamevaluepara.add(banknamevalue);
		banknamevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell banknamevaluecell = new PdfPCell(banknamevalue);
//		banknamevaluecell.addElement(banknamevaluepara);
		banknamevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		banknamevaluecell.setBorder(0);
		
		Paragraph bankbranchvaluepara = new Paragraph();
		bankbranchvaluepara.add(bankbranchvalue);
		bankbranchvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bankbranchvaluecell = new PdfPCell(bankbranchvalue);
//		bankbranchvaluecell.addElement(bankbranchvaluepara);
		bankbranchvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankbranchvaluecell.setBorder(0);
		
		Paragraph ifscvaluepara = new Paragraph();
		ifscvaluepara.add(ifscvalue);
		ifscvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell ifscvaluecell = new PdfPCell(ifscvalue);
//		ifscvaluecell.addElement(ifscvaluepara);
		ifscvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		ifscvaluecell.setBorder(0);
		
		Paragraph bankaccountnovaluepara = new Paragraph();
		bankaccountnovaluepara.add(bankaccountnovalue);
		bankaccountnovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell bankaccountnovaluecell = new PdfPCell(bankaccountnovalue);
//		bankaccountnovaluecell.addElement(bankaccountnovaluepara);
		bankaccountnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankaccountnovaluecell.setBorder(0);
		
		Paragraph esicnovaluepara = new Paragraph();
		esicnovaluepara.add(esicnovalue);
		esicnovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell esicnovaluecell = new PdfPCell(esicnovalue);
//		esicnovaluecell.addElement(esicnovaluepara);
		esicnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		esicnovaluecell.setBorder(0);
		
		Paragraph pannovaluepara = new Paragraph();
		pannovaluepara.add(pannovalue);
		pannovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell pannovaluecell = new PdfPCell(pannovalue);
//		pannovaluecell.addElement(pannovaluepara);
		pannovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pannovaluecell.setBorder(0);
		
		Paragraph ppfnovaluepara = new Paragraph();
		ppfnovaluepara.add(ppfnovalue);
		ppfnovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell ppfnovaluecell = new PdfPCell(ppfnovalue);
//		ppfnovaluecell.addElement(ppfnovaluepara);
		ppfnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		ppfnovaluecell.setBorder(0);
		
		// Creating and adding all cells in table
		PdfPTable emppayrolltable = new PdfPTable(6);
		emppayrolltable.setWidthPercentage(100);
		
		try {
			emppayrolltable.setWidths(infoemployeewidths);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}

		emppayrolltable.addCell(banknamecell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(banknamevaluecell);
		emppayrolltable.addCell(bankbranchcell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(bankbranchvaluecell);
		emppayrolltable.addCell(ifsccell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(ifscvaluecell);
		emppayrolltable.addCell(bankaccountnocell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(bankaccountnovaluecell);
		emppayrolltable.addCell(esicnocell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(esicnovaluecell);
		emppayrolltable.addCell(pannocell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(pannovaluecell);
		emppayrolltable.addCell(ppfnocell);
		emppayrolltable.addCell(coloncell);
		emppayrolltable.addCell(ppfnovaluecell);

		//adjsutment table
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(emppayrolltable);
		
		// Heading Part code
		Phrase title = new Phrase("EMPLOYEE PAYROLL INFORMATION : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setBorderWidthBottom(0);
		adjustcell.setBorderWidthTop(0);
		
		//Adjustment table
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);

		adjusttable.addCell(titlecell);
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);

		try {
			document.add(adjusttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void createEmployeeContactinfo() {
		// TODO Auto-generated method stub
		//Static Phrases
		Phrase landlinephnphrase=new Phrase("Landline Phone",font10);
		Phrase cellphn1phrase=new Phrase("Cell Phone No.1",font10);
		Phrase cellphn2phrase=new Phrase("Cell Phone No.2",font10);
		Phrase emailphrase=new Phrase("Email",font10);
		Phrase addressphrase=new Phrase("Address",font10);
		Phrase colonphrase=new Phrase(":",font10);
		
		//Dynamic phrases
		Phrase landlinephnvalue=null;
		if(employee.getLandline()!=null&&employee.getLandline()!=0){
			landlinephnvalue=new Phrase(employee.getLandline()+"",font10);
		}else{
			landlinephnvalue=new Phrase(" ",font10);
		}
		
		Phrase cellphn1value=null;
		cellphn1value=new Phrase(employee.getCellNumber1()+"",font10);
		Phrase cellphn2value=null;
		if(employee.getCellNumber2()!=null&&employee.getCellNumber2()!=0){
			cellphn2value=new Phrase(employee.getCellNumber2()+"",font10);
		}else{
			cellphn2value=new Phrase(" ",font10);
		}
		Phrase emailvalue=null;
		emailvalue=new Phrase(employee.getEmail(),font10);
		
		Phrase addressvalue=null;
		
		//address logic
		String addline1=employee.getAddress().getAddrLine1()+" ";
		String addline2=null;
		String temp=addline1;
		if(employee.getAddress().getAddrLine2()!=null){
			addline2=employee.getAddress().getAddrLine2()+" ";
			temp+=addline2;
		}else{
			addline2=" ";
		}
		String lankmark=null;
		if(employee.getAddress().getLandmark()!=null){
			lankmark=employee.getAddress().getLandmark()+" ";
			temp+=lankmark;
		}else{
			lankmark=" ";
		}
		
		String locality=null;
		if(employee.getAddress().getLocality()!=null){
			locality=employee.getAddress().getLocality()+" ";
			temp+=locality;
		}else{
			locality=" ";
		}
		
		String country=employee.getAddress().getCountry()+" ";
		String state=employee.getAddress().getState()+" ";
		String city=employee.getAddress().getCity()+" ";
		temp+=city;
		String pin="";
		if(employee.getAddress().getPin()!=0){
			pin=""+employee.getAddress().getPin();
			temp+=pin;
		}
		temp+=state;
		temp+=country;
		
//		String totaladdress=addline1+", "+addline2+", "+"\n"+lankmark+", "+country+", "+"\n"+state+","+"\n"+city+", "+locality+", "+"\n"+pin;
		
		String totaladdress=addline1+", "+addline2+", "+"\n"+lankmark+", "+locality+", "+"\n"+city+" "+pin+","+"\n"+state+", "+country;
		
		addressvalue=new Phrase(temp,font10);
		

		// Blank Cell
		 Phrase blank = new Phrase(" ", font10bold);
		
		 Paragraph blankpara = new Paragraph();
		 blankpara.add(blank);
		 blankpara.setAlignment(Element.ALIGN_CENTER);

		 PdfPCell blankcell=new PdfPCell();
		 blankcell.addElement(blankpara);
		 blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 blankcell.setBorder(0);
		
		
		//Creating Static Cell
		Paragraph landlinepara=new Paragraph();
		landlinepara.add(landlinephnphrase);
		landlinepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell landlinecell=new PdfPCell(landlinephnphrase);
//		landlinecell.addElement(landlinepara);
		landlinecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		landlinecell.setBorder(0);
		
		Paragraph cellphnno1para=new Paragraph();
		cellphnno1para.add(cellphn1phrase);
		cellphnno1para.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cellphnno1cell=new PdfPCell(cellphn1phrase);
//		cellphnno1cell.addElement(cellphnno1para);
		cellphnno1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellphnno1cell.setBorder(0);
		
		Paragraph cellphnno2para=new Paragraph();
		cellphnno2para.add(cellphn2phrase);
		cellphnno2para.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cellphnno2cell=new PdfPCell(cellphn2phrase);
//		cellphnno2cell.addElement(cellphnno2para);
		cellphnno2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellphnno2cell.setBorder(0);
		
		Paragraph emailpara=new Paragraph();
		emailpara.add(emailphrase);
		emailpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell emailcell=new PdfPCell(emailphrase);
//		emailcell.addElement(emailpara);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailcell.setBorder(0);
		
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressphrase);
		addresspara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell addresscell=new PdfPCell(addressphrase);
//		addresscell.addElement(addresspara);
		addresscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addresscell.setBorder(0);
		
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell coloncell=new PdfPCell(colonphrase);
//		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_CENTER);
		coloncell.setBorder(0);
		
		//Dynamic cell
		Paragraph landlinevaluepara=new Paragraph();
		landlinevaluepara.add(landlinephnvalue);
		landlinevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell landlinevaluecell=new PdfPCell(landlinephnvalue);
//		landlinevaluecell.addElement(landlinevaluepara);
		landlinevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		landlinevaluecell.setBorder(0);
		
		Paragraph cellphnno1valuepara=new Paragraph();
		cellphnno1valuepara.add(cellphn1value);
		cellphnno1valuepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cellphnno1valuecell=new PdfPCell(cellphn1value);
//		cellphnno1valuecell.addElement(cellphnno1valuepara);
		cellphnno1valuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellphnno1valuecell.setBorder(0);
		
		Paragraph cellphnno2valuepara=new Paragraph();
		cellphnno2valuepara.add(cellphn2value);
		cellphnno2valuepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cellphnno2valuecell=new PdfPCell(cellphn2value);
//		cellphnno2valuecell.addElement(cellphnno2valuepara);
		cellphnno2valuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellphnno2valuecell.setBorder(0);
		
		Paragraph emailvaluepara=new Paragraph();
		emailvaluepara.add(emailvalue);
		emailvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell emailvaluecell=new PdfPCell(emailvalue);
//		emailvaluecell.addElement(emailvaluepara);
		emailvaluecell.setBorder(0);
		emailvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Paragraph addressvaluepara=new Paragraph();
		addressvaluepara.add(addressvalue);
		addressvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell addressvaluecell=new PdfPCell(addressvalue);
//		addressvaluecell.addElement(addressvaluepara);
		addressvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addressvaluecell.setBorder(0);
		addressvaluecell.setColspan(4);
		
		//Creating Table
		PdfPTable contactinfotable=new PdfPTable(6);
		contactinfotable.setWidthPercentage(100);
//		try {
//			contactinfotable.setWidths(contactemployeewidths);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		try {
			contactinfotable.setWidths(infoemployeewidths);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		contactinfotable.addCell(landlinecell);
		contactinfotable.addCell(coloncell);
		contactinfotable.addCell(landlinevaluecell);
		contactinfotable.addCell(cellphnno1cell);
		contactinfotable.addCell(coloncell);
		contactinfotable.addCell(cellphnno1valuecell);
		contactinfotable.addCell(cellphnno2cell);
		contactinfotable.addCell(coloncell);
		contactinfotable.addCell(cellphnno2valuecell);
		contactinfotable.addCell(emailcell);
		contactinfotable.addCell(coloncell);
		contactinfotable.addCell(emailvaluecell);
		contactinfotable.addCell(addresscell);
		contactinfotable.addCell(coloncell);
		contactinfotable.addCell(addressvaluecell);
//		contactinfotable.addCell(blankcell);
//		contactinfotable.addCell(blankcell);
//		contactinfotable.addCell(blankcell);
		
		//adjsutment table
				PdfPCell adjustcell=new PdfPCell();
				adjustcell.addElement(contactinfotable);
				
				// Heading Part code
				Phrase title = new Phrase("EMPLOYEE CONTACT INFORMATION : ", font10bold);

				Paragraph titlepara = new Paragraph();
				titlepara.add(title);
				titlepara.setAlignment(Element.ALIGN_CENTER);

				PdfPCell titlecell = new PdfPCell(title);
//				titlecell.addElement(titlepara);
				titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
				titlecell.setBorderWidthBottom(0);
				adjustcell.setBorderWidthTop(0);
				
				//Adjustment table
				PdfPTable adjusttable = new PdfPTable(1);
				adjusttable.setWidthPercentage(100);

				adjusttable.addCell(titlecell);
				// adjusttable.addCell(blankcell);
				adjusttable.addCell(adjustcell);
		try {
			document.add(adjusttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void createSocialInfo() {
		// TODO Auto-generated method stub
		
		//Creating Static Phrases
		Phrase googleplusphrase=new Phrase("Google Plus ID",font10);
		Phrase facebookphrase=new Phrase("Facebook ID",font10);
		Phrase twitterphrase=new Phrase("Twitter ID",font10);
		Phrase colonphrase=new Phrase(":",font10);
		
		//Creating Dynamic Phrases
		Phrase googleplusvaluephrase=null;
		if(employee.getSocialInfo().getGooglePlusId()!=null){
			googleplusvaluephrase=new Phrase(employee.getSocialInfo().getGooglePlusId()+"",font10);
		}else{
			googleplusvaluephrase=new Phrase(" ",font10);
		}
		
		Phrase facebookvaluephrase=null;
		if(employee.getSocialInfo().getFaceBookId()!=null){
			facebookvaluephrase=new Phrase(employee.getSocialInfo().getFaceBookId()+"",font10);
		}else{
			facebookvaluephrase=new Phrase(" ",font10);
		}
		
		Phrase twittervaluephrase=null;
		if(employee.getSocialInfo().getTwitterId()!=null){
			twittervaluephrase=new Phrase(employee.getSocialInfo().getTwitterId()+"",font10);
		}else{
			twittervaluephrase=new Phrase(" ",font10);
		}
		
		
		//Creating Static Cell
		Paragraph googlepluspara=new Paragraph();
		googlepluspara.add(googleplusphrase);
		googlepluspara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell googlepluscell=new PdfPCell(googleplusphrase);
//		googlepluscell.addElement(googlepluspara);
		googlepluscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		googlepluscell.setBorder(0);
		
		Paragraph facebookpara=new Paragraph();
		facebookpara.add(facebookphrase);
		facebookpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell facebookcell=new PdfPCell(facebookphrase);
//		facebookcell.addElement(facebookpara);
		facebookcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		facebookcell.setBorder(0);
		
		Paragraph twitterpara=new Paragraph();
		twitterpara.add(twitterphrase);
		twitterpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell twittercell=new PdfPCell(twitterphrase);
//		twittercell.addElement(twitterpara);
		twittercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		twittercell.setBorder(0);
		
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell coloncell=new PdfPCell(colonphrase);
//		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		// Blank Cell
		Phrase blank = new Phrase(" ", font10bold);
				
		Paragraph blankpara = new Paragraph();
		blankpara.add(blank);
		blankpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell blankcell=new PdfPCell(blankpara);
//		blankcell.addElement(blankpara);
		blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankcell.setBorder(0);
		
		//Dynamic Cell
		Paragraph googleplusvaluepara=new Paragraph();
		googleplusvaluepara.add(googleplusvaluephrase);
		googleplusvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell googleplusvaluecell=new PdfPCell(googleplusvaluephrase);
//		googleplusvaluecell.addElement(googleplusvaluepara);
		googleplusvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		googleplusvaluecell.setBorder(0);
		
		Paragraph facebookvaluepara=new Paragraph();
		facebookvaluepara.add(facebookvaluephrase);
		facebookvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell facebookvaluecell=new PdfPCell(facebookvaluephrase);
//		facebookvaluecell.addElement(facebookvaluepara);
		facebookvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		facebookvaluecell.setBorder(0);
		
		Paragraph twittervaluepara=new Paragraph();
		twittervaluepara.add(twittervaluephrase);
		twittervaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell twittervaluecell=new PdfPCell(twittervaluephrase);
//		twittervaluecell.addElement(twittervaluepara);
		twittervaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		twittervaluecell.setBorder(0);
		
		//Creating Table
		PdfPTable soocialinfotable=new PdfPTable(6);
		soocialinfotable.setWidthPercentage(100);
		
		try {
			soocialinfotable.setWidths(infoemployeewidths);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		soocialinfotable.addCell(googlepluscell);
		soocialinfotable.addCell(coloncell);
		soocialinfotable.addCell(googleplusvaluecell);
		soocialinfotable.addCell(facebookcell);
		soocialinfotable.addCell(coloncell);
		soocialinfotable.addCell(facebookvaluecell);
		soocialinfotable.addCell(twittercell);
		soocialinfotable.addCell(coloncell);
		soocialinfotable.addCell(twittervaluecell);
		soocialinfotable.addCell(blankcell);
		soocialinfotable.addCell(blankcell);
		soocialinfotable.addCell(blankcell);
		
		//adjsutment table
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(soocialinfotable);
		
		// Heading Part code
		Phrase title = new Phrase("EMPLOYEE SOCIAL INFORMATION : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setBorderWidthBottom(0);
		adjustcell.setBorderWidthTop(0);
		
		//Adjustment table
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);

		adjusttable.addCell(titlecell);
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);
		try {
			document.add(adjusttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	private void createPassportInfo() {
		// TODO Auto-generated method stub
	//Static Phrases
		Phrase passportnophrase=new Phrase("Passport No.",font10);
		Phrase issuedatphrase=new Phrase("Issued At",font10);
		Phrase issuedatephrase=new Phrase("Issue Date",font10);
		Phrase expirydatephrase=new Phrase("Expiry Date",font10);
		Phrase colonphrase=new Phrase(":",font10);
		
		//Dynamic Phrases
		Phrase passportnovalue=null;
		if(employee.getPassPortInformation().getPassportNumber()!=null){
			passportnovalue=new Phrase(employee.getPassPortInformation().getPassportNumber(),font10);
		}else{
			passportnovalue=new Phrase(" ",font10);
		}
		
		Phrase issuedatvalue=null;
		if(employee.getPassPortInformation().getIssuedAt()!=null){
			issuedatvalue=new Phrase(employee.getPassPortInformation().getIssuedAt(),font10);
		}else{
			issuedatvalue=new Phrase(" ",font10);
		}
		
		Phrase issuedatevalue=null;
		if(employee.getPassPortInformation().getIssueDate()!=null){
			issuedatevalue=new Phrase(fmt.format(employee.getPassPortInformation().getIssueDate())+"",font10);
		}else{
			issuedatevalue=new Phrase(" ",font10);
		}
		Phrase expirydatevalue=null;
		if(employee.getPassPortInformation().getExpiryDate()!=null){
			expirydatevalue=new Phrase(fmt.format(employee.getPassPortInformation().getExpiryDate())+"",font10);
		}else{
			expirydatevalue=new Phrase(" ",font10);
		}
		
		//Creating Static Cell
		Paragraph passportnopara=new Paragraph();
		passportnopara.add(passportnophrase);
		passportnopara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell passportnocell=new PdfPCell(passportnophrase);
//		passportnocell.addElement(passportnopara);
		passportnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		passportnocell.setBorder(0);
		
		Paragraph issuedatpara=new Paragraph();
		issuedatpara.add(issuedatphrase);
		issuedatpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell issuedatcell=new PdfPCell(issuedatphrase);
//		issuedatcell.addElement(issuedatpara);
		issuedatcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		issuedatcell.setBorder(0);
		
		Paragraph issuedatepara=new Paragraph();
		issuedatepara.add(issuedatephrase);
		issuedatepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell issuedatecell=new PdfPCell(issuedatephrase);
//		issuedatecell.addElement(issuedatepara);
		issuedatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		issuedatecell.setBorder(0);
		
		Paragraph expirydatepara=new Paragraph();
		expirydatepara.add(expirydatephrase);
		expirydatepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell expirydatecell=new PdfPCell(expirydatephrase);
//		expirydatecell.addElement(expirydatepara);
		expirydatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		expirydatecell.setBorder(0);
		
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell coloncell=new PdfPCell(colonphrase);
//		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		//Creating Dynamic cell
		Paragraph passportnovaluepara=new Paragraph();
		passportnovaluepara.add(passportnovalue);
		passportnovaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell passportnovaluecell=new PdfPCell(passportnovalue);
//		passportnovaluecell.addElement(passportnovaluepara);
		passportnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		passportnovaluecell.setBorder(0);
		
		Paragraph issuedatvaluepara=new Paragraph();
		issuedatvaluepara.add(issuedatvalue);
		issuedatvaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell issuedatvaluecell=new PdfPCell(issuedatvalue);
//		issuedatvaluecell.addElement(issuedatvaluepara);
		issuedatvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		issuedatvaluecell.setBorder(0);
		
		Paragraph issuedatevaluepara=new Paragraph();
		issuedatevaluepara.add(issuedatevalue);
		issuedatevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell issuedatevaluecell=new PdfPCell(issuedatevalue);
//		issuedatevaluecell.addElement(issuedatevaluepara);
		issuedatevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		issuedatevaluecell.setBorder(0);
		
		Paragraph expirydatevaluepara=new Paragraph();
		expirydatevaluepara.add(expirydatevalue);
		expirydatevaluepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell expirydatevaluecell=new PdfPCell(expirydatevalue);
//		expirydatevaluecell.addElement(expirydatevaluepara);
		expirydatevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		expirydatevaluecell.setBorder(0);
		
		//Creating Table 
		PdfPTable passportinfotable=new PdfPTable(6);
		passportinfotable.setWidthPercentage(100);
		
		try {
			passportinfotable.setWidths(infoemployeewidths);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		//Adding all cells now we get
		
		passportinfotable.addCell(passportnocell);
		passportinfotable.addCell(coloncell);
		passportinfotable.addCell(passportnovaluecell);
		passportinfotable.addCell(issuedatcell);
		passportinfotable.addCell(coloncell);
		passportinfotable.addCell(issuedatvaluecell);
		passportinfotable.addCell(issuedatecell);
		passportinfotable.addCell(coloncell);
		passportinfotable.addCell(issuedatevaluecell);
		passportinfotable.addCell(expirydatecell);
		passportinfotable.addCell(coloncell);
		passportinfotable.addCell(expirydatevaluecell);
		

		//adjsutment table
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(passportinfotable);
		
		// Heading Part code
		Phrase title = new Phrase("EMPLOYEE PASSPORT INFORMATION : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setBorderWidthBottom(0);
		adjustcell.setBorderWidthTop(0);
		
		//Adjustment table
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);

		adjusttable.addCell(titlecell);
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);
		try {
			document.add(adjusttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createExperienceDetails() {
		//Creating Table
		PdfPTable expdetailtable=new PdfPTable(5);
		expdetailtable.setWidthPercentage(100);
		
		//Static Phrases
		Phrase companyphrase=new Phrase("Company",font9bold);
		Phrase designationphrase=new Phrase("Designation",font10bold);
		Phrase salaryphrase=new Phrase("Salary",font10bold);
		Phrase fromdatephrase=new Phrase("From Date",font10bold);
		Phrase todatephrase=new Phrase("To Date",font10bold);
		Phrase colonphrase=new Phrase(":",font10bold);
		
		//Creating Static Cell
		Paragraph companypara=new Paragraph();
		companypara.add(companyphrase);
		companypara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell companycell=new PdfPCell();
		companycell.addElement(companypara);
		companycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		companycell.setBorder(0);
		
		Paragraph designationpara=new Paragraph();
		designationpara.add(designationphrase);
		designationpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell designationcell=new PdfPCell();
		designationcell.addElement(designationpara);
		designationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		designationcell.setBorder(0);
		
		Paragraph salarypara=new Paragraph();
		salarypara.add(salaryphrase);
		salarypara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell salarycell=new PdfPCell();
		salarycell.addElement(salarypara);
		salarycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		salarycell.setBorder(0);
		
		Paragraph fromdatepara=new Paragraph();
		fromdatepara.add(fromdatephrase);
		fromdatepara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell fromdatecell=new PdfPCell();
		fromdatecell.addElement(fromdatepara);
		fromdatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		fromdatecell.setBorder(0);
		
		Paragraph todatepara=new Paragraph();
		todatepara.add(todatephrase);
		todatepara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell todatecell=new PdfPCell();
		todatecell.addElement(todatepara);
		todatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		todatecell.setBorder(0);
		
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell coloncell=new PdfPCell();
		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		coloncell.setBorder(0);
		
		Phrase title = new Phrase("EMPLOYEE EXPERIENCE DETAILS : ", font10bold);
		
		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);
	
		PdfPCell titlecell = new PdfPCell(title);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setColspan(5);
		
		
		expdetailtable.addCell(titlecell);
		
		
		expdetailtable.addCell(companycell);
		expdetailtable.addCell(designationcell);
		expdetailtable.addCell(salarycell);
		expdetailtable.addCell(fromdatecell);
		expdetailtable.addCell(todatecell);
		
		for(int i=0;i<employee.getPreviousCompanyHistory().size();i++){
			
			//Dynamic Phrases
			Phrase companyvalue=null;
			if(employee.getPreviousCompanyHistory().get(i).getCompany()!=null){
				companyvalue=new Phrase(employee.getPreviousCompanyHistory().get(i).getCompany(),font10);
			}else{
				companyvalue=new Phrase(" ",font10);
			}
			
			Phrase designationvalue=null;
			if(employee.getPreviousCompanyHistory().get(i).getDesignation()!=null){
				designationvalue=new Phrase(employee.getPreviousCompanyHistory().get(i).getDesignation(),font10);
			}else{
				designationvalue=new Phrase(" ",font10);
			}
			
			Phrase salaryvalue=null;
			if(employee.getPreviousCompanyHistory().get(i).getSalary()!=0){
				salaryvalue=new Phrase(employee.getPreviousCompanyHistory().get(i).getSalary()+"",font10);
			}else{
				salaryvalue=new Phrase("0",font10);
			}
			
			Phrase fromdatevalue=null;
			if(employee.getPreviousCompanyHistory().get(i).getFromDate()!=null){
//				fromdatevalue=new Phrase(fmt.format(employee.getPreviousCompanyHistory().get(i).getFromDate()),font10);
				fromdatevalue=new Phrase(employee.getPreviousCompanyHistory().get(i).getFromDate(),font10);
			}else{
				fromdatevalue=new Phrase(" ",font10);
			}
			Phrase todatevalue=null;
			if(employee.getPreviousCompanyHistory().get(i).getToDate()!=null){
//				todatevalue=new Phrase(fmt.format(employee.getPreviousCompanyHistory().get(i).getToDate()),font10);
				todatevalue=new Phrase(employee.getPreviousCompanyHistory().get(i).getToDate(),font10);
			}else{
				todatevalue=new Phrase(" ",font10);
			}
			
			
			
			//Creating Dynamic Cell
			
			Paragraph companyvaluepara=new Paragraph();
			companyvaluepara.add(companyvalue);
			companyvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell companyvaluecell=new PdfPCell();
			companyvaluecell.addElement(companyvaluepara);
			companyvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			companyvaluecell.setBorder(0);
			
			Paragraph designationvaluepara=new Paragraph();
			designationvaluepara.add(designationvalue);
			designationvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell designationvaluecell=new PdfPCell();
			designationvaluecell.addElement(designationvaluepara);
			designationvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			designationvaluecell.setBorder(0);
			
			Paragraph salaryvaluepara=new Paragraph();
			salaryvaluepara.add(salaryvalue);
			salaryvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell salaryvaluecell=new PdfPCell();
			salaryvaluecell.addElement(salaryvaluepara);
			salaryvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			salaryvaluecell.setBorder(0);
			
			Paragraph fromdatevaluepara=new Paragraph();
			fromdatevaluepara.add(fromdatevalue);
			fromdatevaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell fromdatevaluecell=new PdfPCell();
			fromdatevaluecell.addElement(fromdatevaluepara);
			fromdatevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			fromdatevaluecell.setBorder(0);
			
			Paragraph todatevaluepara=new Paragraph();
			todatevaluepara.add(todatevalue);
			todatevaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell todatevaluecell=new PdfPCell();
			todatevaluecell.addElement(todatevaluepara);
			todatevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			todatevaluecell.setBorder(0);
			
			
			//Creating Blank cell
			Phrase blankphrase=new Phrase(" ");
			Paragraph blankpara=new Paragraph();
			blankpara.add(blankphrase);
			blankpara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell blankcell=new PdfPCell();
			blankcell.addElement(blankpara);
			blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			blankcell.setBorder(0);
			
			
			
			
//			expdetailtable.addCell(companycell);
//			expdetailtable.addCell(coloncell);
			expdetailtable.addCell(companyvaluecell);
//			expdetailtable.addCell(designationcell);
//			expdetailtable.addCell(coloncell);
			expdetailtable.addCell(designationvaluecell);
//			expdetailtable.addCell(salarycell);
//			expdetailtable.addCell(coloncell);
			expdetailtable.addCell(salaryvaluecell);
//			expdetailtable.addCell(fromdatecell);
//			expdetailtable.addCell(coloncell);
			expdetailtable.addCell(fromdatevaluecell);
//			expdetailtable.addCell(todatecell);
//			expdetailtable.addCell(coloncell);
			expdetailtable.addCell(todatevaluecell);
//			expdetailtable.addCell(blankcell);
//			expdetailtable.addCell(blankcell);
//			expdetailtable.addCell(blankcell);
			

			
		}
		
		//adjsutment cell
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(expdetailtable);
		
		//Adjustment table
		
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);
		// Heading Part code
		if(expdetailtemp==0){
				
		//			try {
		//				document.add(Chunk.NEXTPAGE);
		//			} catch (DocumentException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
			
			
//			Phrase title = new Phrase("EMPLOYEE EXPERIENCE DETAILS : ", font10bold);
//		
//			Paragraph titlepara = new Paragraph();
//			titlepara.add(title);
//			titlepara.setAlignment(Element.ALIGN_CENTER);
//		
//			PdfPCell titlecell = new PdfPCell(title);
//		//		titlecell.addElement(titlepara);
//			titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
//			titlecell.setColspan(5);
			
//			titlecell.setBorderWidthBottom(0);
//			adjustcell.setBorderWidthTop(0);
			
			
		
			adjusttable.addCell(titlecell);
			
			expdetailtemp=1;
		}
		adjusttable.addCell(adjustcell);
		try {
			document.add(expdetailtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createEducationalDetails() {
		//Creating Table
		PdfPTable educationaldetailtable=new PdfPTable(4);
		educationaldetailtable.setWidthPercentage(100);
		
//		try {
//			educationaldetailtable.setWidths(infoemployeewidths);
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
		
		//Creating Static Phrases
		Phrase schoolphrase=new Phrase("School/University",font10bold);
		Phrase Universityphrase=new Phrase("University",font10bold);
		Phrase qualificationphrase=new Phrase("Qualification",font10bold);
		Phrase levelphrase=new Phrase("Level",font10bold);
		Phrase yopphrase=new Phrase("Year Of Passing",font10bold);
		Phrase colonphrase=new Phrase(":",font10);
		
		//Creating Static Cell
		Paragraph schoolpara=new Paragraph();
		schoolpara.add(schoolphrase);
		schoolpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell schoolcell=new PdfPCell();
		schoolcell.addElement(schoolpara);
		schoolcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		schoolcell.setBorder(0);
		
		Paragraph universitypara=new Paragraph();
		universitypara.add(Universityphrase);
		universitypara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell universitycell=new PdfPCell();
		universitycell.addElement(universitypara);
		universitycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		universitycell.setBorder(0);
		
		Paragraph qualificationpara=new Paragraph();
		qualificationpara.add(qualificationphrase);
		qualificationpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell qualificationcell=new PdfPCell();
		qualificationcell.addElement(qualificationpara);
		qualificationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		qualificationcell.setBorder(0);
		
		Paragraph levelpara=new Paragraph();
		levelpara.add(levelphrase);
		levelpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell levelcell=new PdfPCell();
		levelcell.addElement(levelpara);
		levelcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		levelcell.setBorder(0);
		
		Paragraph yoppara=new Paragraph();
		yoppara.add(yopphrase);
		yoppara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell yopcell=new PdfPCell();
		yopcell.addElement(yoppara);
		yopcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		yopcell.setBorder(0);
		
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell coloncell=new PdfPCell();
		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		coloncell.setBorder(0);
		
		Phrase title = new Phrase("EMPLOYEE EDUCATIONAL DETAILS : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setColspan(4);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
//		titlecell.setBorderWidthBottom(0);
//		adjustcell.setBorderWidthTop(0);
		educationaldetailtable.addCell(titlecell);
		
		educationaldetailtable.addCell(schoolcell);
//		educationaldetailtable.addCell(universitycell);
		educationaldetailtable.addCell(qualificationcell);
		educationaldetailtable.addCell(levelcell);
		educationaldetailtable.addCell(yopcell);
		
		for(int i=0;i<employee.getEducationalInfo().size();i++){
			//Dynamic Phrases
			Phrase schoolvalue=null;
			if(employee.getEducationalInfo().get(i).getSchoolName()!=null){
				schoolvalue=new Phrase(employee.getEducationalInfo().get(i).getSchoolName(),font10);
			}else{
				schoolvalue=new Phrase(" ",font10);
			}
			Phrase universityvalue=null;
			if(employee.getEducationalInfo().get(i).getUniversity()!=null){
				universityvalue=new Phrase(employee.getEducationalInfo().get(i).getUniversity(),font10);
			}else{
				universityvalue=new Phrase(" ",font10);
			}
			Phrase qualificationvalue=null;
			if(employee.getEducationalInfo().get(i).getDegreeObtained()!=null){
				qualificationvalue=new Phrase(employee.getEducationalInfo().get(i).getDegreeObtained(),font10);
			}else{
				qualificationvalue=new Phrase(" ",font10);
			}
			
			Phrase levelvalue=null;
			if(employee.getEducationalInfo().get(i).getPercentage()!=null){
				levelvalue=new Phrase(employee.getEducationalInfo().get(i).getPercentage(),font10);	
			}else{
				levelvalue=new Phrase(" ",font10);	
			}
			Phrase yopvalue=null;
			if(employee.getEducationalInfo().get(i).getYearOfPassing()!=null)
			{
//				yopvalue=new Phrase(fmt.format(employee.getEducationalInfo().get(i).getYearOfPassing())+"",font10);	
				yopvalue=new Phrase(employee.getEducationalInfo().get(i).getYearOfPassing()+"",font10);
			}else{
				yopvalue=new Phrase(" ",font10);
			}
			
			//Creating Dynamic Cell
			Paragraph schoolvaluepara=new Paragraph();
			schoolvaluepara.add(schoolvalue);
			schoolvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell schoolvaluecell=new PdfPCell();
			schoolvaluecell.addElement(schoolvaluepara);
			schoolvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			schoolvaluecell.setBorder(0);
			
			Paragraph universityvaluepara=new Paragraph();
			universityvaluepara.add(universityvalue);
			universityvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell universityvaluecell=new PdfPCell();
			universityvaluecell.addElement(universityvaluepara);
			universityvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			universityvaluecell.setBorder(0);
			
			Paragraph qualificationvaluepara=new Paragraph();
			qualificationvaluepara.add(qualificationvalue);
			qualificationvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell qualificationvaluecell=new PdfPCell();
			qualificationvaluecell.addElement(qualificationvaluepara);
			qualificationvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			qualificationvaluecell.setBorder(0);
			
			Paragraph levelvaluepara=new Paragraph();
			levelvaluepara.add(levelvalue);
			levelvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell levelvaluecell=new PdfPCell();
			levelvaluecell.addElement(levelvaluepara);
			levelvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			levelvaluecell.setBorder(0);
			
			Paragraph yopvaluepara=new Paragraph();
			yopvaluepara.add(yopvalue);
			yopvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell yopvaluecell=new PdfPCell();
			yopvaluecell.addElement(yopvaluepara);
			yopvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			yopvaluecell.setBorder(0);
			
			//Creating Blank cell
			Phrase blankphrase=new Phrase(" ");
			Paragraph blankpara=new Paragraph();
			blankpara.add(blankphrase);
			blankpara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell blankcell=new PdfPCell();
			blankcell.addElement(blankpara);
			blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			blankcell.setBorder(0);
			
			
			
//			educationaldetailtable.addCell(schoolcell);
//			educationaldetailtable.addCell(coloncell);
			educationaldetailtable.addCell(schoolvaluecell);
			
//			educationaldetailtable.addCell(universitycell);
//			educationaldetailtable.addCell(coloncell);
//			educationaldetailtable.addCell(universityvaluecell);
			
//			educationaldetailtable.addCell(qualificationcell);
//			educationaldetailtable.addCell(coloncell);
			educationaldetailtable.addCell(qualificationvaluecell);
			
//			educationaldetailtable.addCell(levelcell);
//			educationaldetailtable.addCell(coloncell);
			educationaldetailtable.addCell(levelvaluecell);
			
//			educationaldetailtable.addCell(yopcell);
//			educationaldetailtable.addCell(coloncell);
			educationaldetailtable.addCell(yopvaluecell);
			
//			educationaldetailtable.addCell(blankcell);
//			educationaldetailtable.addCell(blankcell);
//			educationaldetailtable.addCell(blankcell);
			
			
			
		}
		
		//Adjsutment cell
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(educationaldetailtable);
		
		
		//Adjustment table
		
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);
		// Heading Part code
		if(edudetailtemp==0){
//		Phrase title = new Phrase("EMPLOYEE EDUCATIONAL DETAILS : ", font10bold);
//
//		Paragraph titlepara = new Paragraph();
//		titlepara.add(title);
//		titlepara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell titlecell = new PdfPCell(title);
////		titlecell.addElement(titlepara);
//		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
//		titlecell.setBorderWidthBottom(0);
//		adjustcell.setBorderWidthTop(0);
		

		adjusttable.addCell(titlecell);
		
		edudetailtemp=1;
		}
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);
		try {
			document.add(educationaldetailtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void employeeAddressDetails() {
		// TODO Auto-generated method stub
		//Creating Static Phrases
		Phrase addresstypephrase=new Phrase("Address Type",font10bold);
		Phrase fulladdressphrase=new Phrase("Full Address",font10bold);
		Phrase colonphrase=new Phrase(":",font10);
		
		//Creating Static Cell
		Paragraph addresstypepara=new Paragraph();
		addresstypepara.add(addresstypephrase);
		addresstypepara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell addresstypecell=new PdfPCell();
		addresstypecell.addElement(addresstypepara);
		addresstypecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addresstypecell.setBorder(0);
		
		Paragraph fulladdresspara=new Paragraph();
		fulladdresspara.add(fulladdressphrase);
		fulladdresspara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell fulladdresscell=new PdfPCell();
		fulladdresscell.addElement(fulladdresspara);
		fulladdresscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		fulladdresscell.setBorder(0);
		
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonphrase);
		colonpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell coloncell=new PdfPCell();
		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		coloncell.setBorder(0);
		
		//Creating Table
		PdfPTable addressDetailsTable=new PdfPTable(2);
		addressDetailsTable.setWidthPercentage(100);
//		try {
//			addressDetailsTable.setWidths(addressemployeewidths);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		try {
//			addressDetailsTable.setWidths(infoemployeewidths);
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
		
		try {
			addressDetailsTable.setWidths(new float[] {25f,75f} );
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase title = new Phrase("EMPLOYEE ADDRESS INFORMATION : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setColspan(2);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		
		addressDetailsTable.addCell(titlecell);
		
		addressDetailsTable.addCell(addresstypecell);
		addressDetailsTable.addCell(fulladdresscell);
		
		for(int i=0;i<empadditionaldetails.getEmpAddDetList().size();i++){

			
			//Creating Dynamic Phrases
			
			
			Phrase 	addresstypevalue=null;		
			if(empadditionaldetails.getEmpAddDetList().get(i).getAddressType()!=null){
				addresstypevalue=new Phrase(empadditionaldetails.getEmpAddDetList().get(i).getAddressType(),font10);
			}else{
				addresstypevalue=new Phrase(" ",font10);
			}
			Phrase fulladdressvalue=null;
			if(empadditionaldetails.getEmpAddDetList().get(i).getFullAddress()!=null){
				fulladdressvalue=new Phrase(empadditionaldetails.getEmpAddDetList().get(i).getFullAddress(),font10);
			}else{
				fulladdressvalue=new Phrase(" ",font10);
			}
			
		
			
			//Creating Dynamic Cell
			Paragraph addresstypevaluepara=new Paragraph();
			addresstypevaluepara.add(addresstypevalue);
			addresstypevaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell addresstypevaluecell=new PdfPCell();
			addresstypevaluecell.addElement(addresstypevaluepara);
			addresstypevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			addresstypevaluecell.setBorder(0);
			
			Paragraph fulladdressvaluepara=new Paragraph();
			fulladdressvaluepara.add(fulladdressvalue);
			fulladdressvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell fulladdressvaluecell=new PdfPCell();
			fulladdressvaluecell.addElement(fulladdressvaluepara);
			fulladdressvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			fulladdressvaluecell.setBorder(0);
			
			
			
//			addressDetailsTable.addCell(addresstypecell);
//			addressDetailsTable.addCell(coloncell);
			addressDetailsTable.addCell(addresstypevaluecell);
//			addressDetailsTable.addCell(fulladdresscell);
//			addressDetailsTable.addCell(coloncell);
			addressDetailsTable.addCell(fulladdressvaluecell);
			

			
			
		} 
		//Adjsutment cell
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(addressDetailsTable);
		
		//Adjustment table
		
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);
		// Heading Part code
		if(empaddressdetailtemp==0){
//
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//		Phrase title = new Phrase("EMPLOYEE ADDRESS INFORMATION : ", font10bold);
//
//		Paragraph titlepara = new Paragraph();
//		titlepara.add(title);
//		titlepara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell titlecell = new PdfPCell(title);
////		titlecell.addElement(titlepara);
//		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
//		titlecell.setBorderWidthBottom(0);
//		adjustcell.setBorderWidthTop(0);
		

//		adjusttable.addCell(titlecell);
		
		empaddressdetailtemp=1;
		}
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);
		try {
			document.add(addressDetailsTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void employeeFamilyDetails() {
		// TODO Auto-generated method stub
		
		//Static Phrases
		Phrase relationphrase=new Phrase("Relation",font10bold);
		Phrase fullnamephrase=new Phrase("Name",font10bold);
		Phrase cellnophrase=new Phrase("Cell No.",font10bold);
		Phrase dobphrase=new Phrase("D.O.B",font10bold);
		Phrase colonvalue=new Phrase(":",font10);
		
		//Creating Static cell
		Paragraph relationpara=new Paragraph();
		relationpara.add(relationphrase);
		relationpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell relationcell=new PdfPCell();
		relationcell.addElement(relationpara);
		relationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		relationcell.setBorder(0);
		
		Paragraph fullnamepara=new Paragraph();
		fullnamepara.add(fullnamephrase);
		fullnamepara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell fullnamecell=new PdfPCell();
		fullnamecell.addElement(fullnamepara);
		fullnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		fullnamecell.setBorder(0);
		
		Paragraph cellnopara=new Paragraph();
		cellnopara.add(cellnophrase);
		cellnopara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell cellnocell=new PdfPCell();
		cellnocell.addElement(cellnopara);
		cellnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		cellnocell.setBorder(0);
		
		Paragraph dobpara=new Paragraph();
		dobpara.add(dobphrase);
		dobpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell dobcell=new PdfPCell();
		dobcell.addElement(dobpara);
		dobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		dobcell.setBorder(0);
	
		Paragraph colonpara=new Paragraph();
		colonpara.add(colonvalue);
		colonpara.setAlignment(Element.ALIGN_LEFT);
		PdfPCell coloncell=new PdfPCell();
		coloncell.addElement(colonpara);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		coloncell.setBorder(0);
		
		//Creating Table
		PdfPTable familydetailstable=new PdfPTable(4);
//		try {
//			familydetailstable.setWidths(columnWidths);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
//		try {
//			familydetailstable.setWidths(infoemployeewidths);
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
		familydetailstable.setWidthPercentage(100);
		
		
		
		Phrase title = new Phrase("EMPLOYEE FAMILY DETAILS : ", font10bold);

		Paragraph titlepara = new Paragraph();
		titlepara.add(title);
		titlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell(title);
//		titlecell.addElement(titlepara);
		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setColspan(4);
		familydetailstable.addCell(titlecell);
		
		familydetailstable.addCell(relationcell);
		familydetailstable.addCell(fullnamecell);
		familydetailstable.addCell(cellnocell);
		familydetailstable.addCell(dobcell);
		
		for(int i=0;i<empadditionaldetails.getEmpFamDetList().size();i++){
			
			
			//Dynamic Phrases
			Phrase relationvaluephrase=null;
			if(empadditionaldetails.getEmpFamDetList().get(i).getFamilyRelation()!=null){
				relationvaluephrase=new Phrase(empadditionaldetails.getEmpFamDetList().get(i).getFamilyRelation(),font10);
			}else{
				relationvaluephrase=new Phrase(" ",font10);
			}
			Phrase fullnamevaluephrase=null;
			
			String firstname=empadditionaldetails.getEmpFamDetList().get(i).getFname();
			
			String middlename=" ";
			if(empadditionaldetails.getEmpFamDetList().get(i).getMname()!=null){
				middlename=empadditionaldetails.getEmpFamDetList().get(i).getMname();
			}else{
				middlename=" ";
			}
			
			String lastname=empadditionaldetails.getEmpFamDetList().get(i).getLname();
			
			String fullname=firstname+" "+middlename+" "+lastname; 
			
			fullnamevaluephrase=new Phrase(fullname,font10);
			
			Phrase cellnovaluephrase=null;
			if(empadditionaldetails.getEmpFamDetList().get(i).getCellNo()!=null){
				cellnovaluephrase=new Phrase(empadditionaldetails.getEmpFamDetList().get(i).getCellNo()+"",font10);
			}else{
				cellnovaluephrase=new Phrase(" ",font10);
			}
			
			Phrase dobvaluephrase=null;
			if(empadditionaldetails.getEmpFamDetList().get(i).getDob()!=null){
				dobvaluephrase=new Phrase(fmt.format(empadditionaldetails.getEmpFamDetList().get(i).getDob()),font10);
			}else{
				dobvaluephrase=new Phrase(" ",font10);
			}
			
			
			
			//Creating Dynamic Cells
			
			Paragraph relationvaluepara=new Paragraph();
			relationvaluepara.add(relationvaluephrase);
			relationvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell relationvaluecell=new PdfPCell();
			relationvaluecell.addElement(relationvaluepara);
			relationvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			relationvaluecell.setBorder(0);
			
			Paragraph fullnamevaluepara=new Paragraph();
			fullnamevaluepara.add(fullnamevaluephrase);
			fullnamevaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell fullnamevaluecell=new PdfPCell();
			fullnamevaluecell.addElement(fullnamevaluepara);
			fullnamevaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			fullnamevaluecell.setBorder(0);
			
			Paragraph cellnovaluepara=new Paragraph();
			cellnovaluepara.add(cellnovaluephrase);
			cellnovaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell cellnovaluecell=new PdfPCell();
			cellnovaluecell.addElement(cellnovaluepara);
			cellnovaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cellnovaluecell.setBorder(0);
			
			Paragraph dobvaluepara=new Paragraph();
			dobvaluepara.add(dobvaluephrase);
			dobvaluepara.setAlignment(Element.ALIGN_LEFT);
			PdfPCell dobvaluecell=new PdfPCell();
			dobvaluecell.addElement(dobvaluepara);
			dobvaluecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			dobvaluecell.setBorder(0);
		
			
			
//			familydetailstable.addCell(relationcell);
//			familydetailstable.addCell(coloncell);
			familydetailstable.addCell(relationvaluecell);
//			familydetailstable.addCell(fullnamecell);
//			familydetailstable.addCell(coloncell);
			familydetailstable.addCell(fullnamevaluecell);
//			familydetailstable.addCell(cellnocell);
//			familydetailstable.addCell(coloncell);
			familydetailstable.addCell(cellnovaluecell);
//			familydetailstable.addCell(dobcell);
//			familydetailstable.addCell(coloncell);
			familydetailstable.addCell(dobvaluecell);
			

			
			
		
			
		}
		
		//Adjsutment cell
		PdfPCell adjustcell=new PdfPCell();
		adjustcell.addElement(familydetailstable);
		
		
		//Adjustment table
		
		PdfPTable adjusttable = new PdfPTable(1);
		adjusttable.setWidthPercentage(100);
		// Heading Part code
		if(empfamilydetailtemp==0){
			
//		Phrase title = new Phrase("EMPLOYEE FAMILY DETAILS : ", font10bold);
//
//		Paragraph titlepara = new Paragraph();
//		titlepara.add(title);
//		titlepara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell titlecell = new PdfPCell(title);
////		titlecell.addElement(titlepara);
//		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
//		titlecell.setBorderWidthBottom(0);
//		adjustcell.setBorderWidthTop(0);
		
		

		adjusttable.addCell(titlecell);
		
		empfamilydetailtemp=1;
		}
		// adjusttable.addCell(blankcell);
		adjusttable.addCell(adjustcell);
		try {
			document.add(familydetailstable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//
	}

}
