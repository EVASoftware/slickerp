package com.slicktechnologies.server.addhocprinting;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
public class PcambrodentCountMonthlyReportpdf {

	public Document document;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,font9boldred,font9,font9red,font7,font7bold,
			font12boldul, font10boldul, font12, font16bold, font10, font10bold,font12boldred,
			font14bold;
	Contract con;
	Company comp;
	Customer cust;
	Invoice invoiceentity;
	
	List<Service> servicelist;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-mm-yyyy");
	
	public PcambrodentCountMonthlyReportpdf() {
		super();
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
		
	}
	
    public void setContract(Long count) {
    	
    	 con = ofy().load().type(Contract.class).id(count).now();

    	 //Load Company
 		if(con.getCompanyId()==null)
 		   comp=ofy().load().type(Company.class).first().now();
 		else
 		   comp=ofy().load().type(Company.class).filter("companyId",con.getCompanyId()).first().now();
    	 
    	
    	servicelist=ofy().load().type(Service.class).filter("contractCount",con.getCount()).list();
    	   fmt.setTimeZone(TimeZone.getTimeZone("IST"));
    } 
    
     public void createPdf() {
    	 
    	 createLogo(document,comp);
	     createHeader();
	     createFooter();    
	   
     }
     
     private void createLogo(Document doc, Company comp) {
 		DocumentUpload document = comp.getLogo();

 		// patch
 		String hostUrl;
 		String environment = System
 				.getProperty("com.google.appengine.runtime.environment");
 		if (environment.equals("Production")) {
 			String applicationId = System.getProperty("com.google.appengine.application.id");
 			String version = System.getProperty("com.google.appengine.application.version");
 			hostUrl = "http://" + version + "." + applicationId + ".appspot.com/";
 		} else {
 			hostUrl = "http://localhost:8888";
 		}
  
 		try {
 			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
 			image2.scalePercent(20f);
 			image2.setAbsolutePosition(40f, 765f);
 			doc.add(image2);
 		} catch (Exception e) {
 			e.printStackTrace();
 		}
 	}   
      
   private void createHeader() {

	Phrase myHeader1=new Phrase("Rodent Trapped & Eliminated at  ",font10boldul);
	Phrase myHeader2=new Phrase("   _____________________________",font10);
	Phrase myHeader3=new Phrase("___________________________________________________________",font10);
	Paragraph headerPara = new Paragraph();
	headerPara.add(myHeader1);
	headerPara.add(myHeader2);                                 
	headerPara.add(Chunk.NEWLINE);
	headerPara.add(myHeader3);
	headerPara.add(Chunk.NEWLINE);
	headerPara.add(Chunk.NEWLINE);
	headerPara.setAlignment(Element.ALIGN_CENTER);
	PdfPCell header =new PdfPCell();
	header.addElement(headerPara);
	
	PdfPTable headertbl = new PdfPTable(1);
	headertbl.setWidthPercentage(100);
	headertbl.addCell(header);
	
	// image logo code added here // 
//	try
//	{
//	Image image1=Image.getInstance("images/PCAMB.png");
//	image1.scalePercent(10f);
//	image1.scaleAbsoluteWidth(120f);
//	image1.setAbsolutePosition(442f,758f);	  
//	document.add(image1);
//	}
//	catch(Exception e)  
//	{
//		e.printStackTrace();
//	}
//	
	// image logo code end here // 
	
	Phrase srNO = new Phrase("SR NO ", font9bold);
	PdfPCell srNOCell = new PdfPCell(srNO);
	srNOCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase date = new Phrase("DATE OF TREATEMENT ", font9bold);
	PdfPCell dateCell = new PdfPCell(date);
	dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

	Phrase trapeliminate = new Phrase("RODENT TRAPPED & ELIMINATED ", font8bold);
	PdfPCell trapeliminateCell = new PdfPCell(trapeliminate);
	trapeliminateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPTable rodentCounttbl = new PdfPTable(3);
	
	rodentCounttbl.setWidthPercentage(100); 
	rodentCounttbl.addCell(srNOCell);
	rodentCounttbl.addCell(dateCell);
	rodentCounttbl.addCell(trapeliminateCell);
	  
	for (int  i = 0; i < servicelist.size() ;i ++) {
		
		Phrase srNoValue = new Phrase(i);
		PdfPCell srNoValueCell = new PdfPCell(srNoValue);
		srNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	}
	System.out.println("outside for loop condition of service details");
for (int  i = 0; i < servicelist.size() ;i ++) 
   {
	
	System.out.println("outside condition of service details");
	
		System.out.println("inside condition of service details");
		
		Phrase srNoValue = new Phrase(i+1+"",font9);
		PdfPCell srNoValueCell = new PdfPCell(srNoValue);
		srNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rodentCounttbl.addCell(srNoValueCell);
		
		Phrase srDate = new Phrase(" ",font9);
		PdfPCell srDateValueCell = new PdfPCell(srDate);
		srDateValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rodentCounttbl.addCell(srDateValueCell);
		
		Phrase srtrapeliminated = new Phrase(" ",font8);
		PdfPCell srtrapeliminatedCell = new PdfPCell(srtrapeliminated);
		srtrapeliminatedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rodentCounttbl.addCell(srtrapeliminatedCell);
	}
	try {
		  
		  rodentCounttbl.setWidths(new float[] { 10,45,45 });
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	try {
		document.add(headertbl);
		document.add(rodentCounttbl);
	}
        catch (DocumentException e)
	{
        	e.printStackTrace();
	   } 
    }

		  private void createFooter() {
			  
			  PdfPTable totaltbl = new PdfPTable (2);
			  totaltbl.setWidthPercentage(100);
			  
			  Phrase totalrodent = new Phrase("TOTAL RODENT COUNT ",font9bold);
			 
			  PdfPCell totalrodentCell = new PdfPCell(totalrodent);
			  totalrodentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  totalrodentCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			  totaltbl.addCell(totalrodentCell); 
			    
			  Phrase totalrodentValue = new Phrase(" ",font9bold);
			  Paragraph p2 = new Paragraph();
			  p2.add(totalrodentValue);
			  PdfPCell totalrodentValueCell = new PdfPCell();
			  totalrodentValueCell.addElement(p2);
			  totaltbl.addCell(totalrodentValueCell);
			 
			  
			  Phrase footer = new Phrase(comp.getBusinessUnitName().toUpperCase()+",",font10bold);
			  
			  String addressline1="";
				
				if(comp.getAddress().getAddrLine2()!=null){
					addressline1=comp.getAddress().getAddrLine1()+"  "+comp.getAddress().getAddrLine2();
				}
				else{
					addressline1=comp.getAddress().getAddrLine1();
				}
			   
				String locality=null;
				if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
					System.out.println("inside both null condition1");
					locality= (comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
						      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry());
				}
				else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
					System.out.println("inside both null condition 2");
					locality= (comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
						      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry());
				}
				
				else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
					System.out.println("inside both null condition 3");
					locality= (comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
						      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry());
				}
				else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
					System.out.println("inside both null condition 4");
					locality=(comp.getAddress().getCity()+" , "
						      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry());
				}
				
				String contactinfo ;
				if(comp.getLandline()!=0){
					contactinfo=("Phone: "+comp.getLandline()+"  Mobile: "+comp.getCellNumber1()+"  FAX : "+comp.getFaxNumber());
				}
				else{
					contactinfo=("  Mobile: "+comp.getCellNumber1()+"  FAX : "+comp.getFaxNumber());
				}
				
			  Phrase footer1 = new Phrase(""+addressline1+locality,font9);
			  
				Phrase footer2 = new Phrase ("TEL.:"+contactinfo,font9);
				Phrase footer3 = new Phrase ("E-Mail :"+comp.getEmail().trim()+ " Website :"+comp.getWebsite(),font8);

			  Paragraph footerPara = new Paragraph();
			  footerPara.add(footer);
			  footerPara.add(Chunk.NEWLINE);
			  footerPara.add(footer1);
			  footerPara.add(footer2);
			  footerPara.add(Chunk.NEWLINE);
			  footerPara.add(footer3);
			  footerPara.add(Chunk.NEWLINE);
			  footerPara.add(Chunk.NEWLINE);
			  footerPara.setAlignment(Element.ALIGN_CENTER);
			  
			  PdfPCell footerCell = new PdfPCell();
			  footerCell.addElement(footerPara);
			  PdfPTable footertbl = new PdfPTable(1);
			  footertbl.setWidthPercentage(100);
			  footertbl.addCell(footerCell);
			  try {
				totaltbl.setWidths(new float[] {55,45});
				document.add(totaltbl);     
				document.add(footertbl);
			} catch (DocumentException e) {
  
				e.printStackTrace();  
			}
				
	  }
}
