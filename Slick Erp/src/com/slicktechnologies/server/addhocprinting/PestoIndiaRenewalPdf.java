package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
public class PestoIndiaRenewalPdf {


	public Document document;
	Contract contract;
	Company comp;
	private Font font10underline, font16boldul, font12bold, font8bold, font8,
	font9bold, font12boldul, font12, font10bold, font10, font14bold,
	font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yy");
	Customer cust;
	float[] colwidthforrenewalLine = { 7f, 3f };
	float[] colwidthcont = { 2f,1f,4f,5f };
	float[] colwidthsub = {1f,1f,11f};
	float[] colwidthprod = { 1.1f,0.5f,5f,3f };
	float[] colwidthforrates = { 2f, 8f };
	DecimalFormat df=new DecimalFormat("0.00");
	
	public PestoIndiaRenewalPdf() {
		// TODO Auto-generated constructor stub

		// TODO Auto-generated constructor stub
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10underline = new Font(Font.FontFamily.HELVETICA, 10,
				Font.UNDERLINE);

		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	}
	
	public void setContractRewnewal(ContractRenewal wo) {
		// TODO Auto-generated method stub
		if(wo.getCompanyId()!=null){
			contract=ofy().load().type(Contract.class).filter("companyId",wo.getCompanyId()).filter("count", wo.getContractId()).first().now();
			cust=ofy().load().type(Customer.class).filter("companyId",wo.getCompanyId()).filter("count",wo.getCustomerId()).first().now();
			comp=ofy().load().type(Company.class).filter("companyId",wo.getCompanyId()).first().now();
		}else{
			contract=ofy().load().type(Contract.class).filter("count", wo.getContractId()).first().now();
			cust=ofy().load().type(Customer.class).filter("count", wo.getCustomerId()).first().now();
		}
		

	}

	public void createPdf() {
		createSpcingForHeading();
		createHeading();
		createaddress();
		createdetailsdata();
	}
	
	private void createSpcingForHeading() {
		
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public PdfPCell createblankcell(String blank,Font font,int leftborder,int rightborder,int topborder,int bottomborder,int alignment){
		Phrase blankp=new Phrase(blank,font); 
		PdfPCell cell =new PdfPCell(blankp);
		cell.setBorderWidthLeft(leftborder);
		cell.setBorderWidthRight(rightborder);
		cell.setBorderWidthBottom(bottomborder);
		cell.setBorderWidthTop(topborder);
		cell.setHorizontalAlignment(alignment);
		return cell;
	}
	
	public PdfPCell createColonCell(String colon,Font font,int leftborder,int rightborder,int topborder,int bottomborder,int alignment){
		Phrase colonp=new Phrase(colon,font); 
		PdfPCell cell =new PdfPCell(colonp);
		cell.setBorderWidthLeft(leftborder);
		cell.setBorderWidthRight(rightborder);
		cell.setBorderWidthBottom(bottomborder);
		cell.setBorderWidthTop(topborder);
		cell.setHorizontalAlignment(alignment);
		return cell;
	}
	
	public PdfPCell createStringCell(String string,Font font,int leftborder,int rightborder,int topborder,int bottomborder,int alignment){
		Phrase colonp=new Phrase(string,font); 
		PdfPCell cell =new PdfPCell(colonp);
		cell.setBorderWidthLeft(leftborder);
		cell.setBorderWidthRight(rightborder);
		cell.setBorderWidthBottom(bottomborder);
		cell.setBorderWidthTop(topborder);
		cell.setHorizontalAlignment(alignment);
		return cell;
	}
	
	private void createHeading() {
		// TODO Auto-generated method stub
		Phrase renewalHeading=new Phrase("RENEWAL LETTER",font12bold);
		PdfPCell renewalHeadingCell=new PdfPCell(renewalHeading);
		renewalHeadingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		renewalHeadingCell.setBorder(0);
		
		
		Phrase date=new Phrase("Date :"+fmt.format(new Date()),font10);
		PdfPCell datecell=new PdfPCell();
		datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datecell.setBorder(0);
		
		PdfPTable reneTable=new PdfPTable(2);
		reneTable.setWidthPercentage(100);
		reneTable.addCell(renewalHeadingCell);
//		reneTable.addCell(createblankcell("", font10, 0,0,0,0, Element.ALIGN_LEFT));
		reneTable.addCell(datecell);
		try {
			reneTable.setWidths(colwidthforrenewalLine);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(reneTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon=new Phrase(":",font10);
		PdfPCell colonCell=new PdfPCell(colon);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		colonCell.setBorder(0);
		
		Phrase contrId=new Phrase("Contract Id",font10bold);
		PdfPCell contrIdCell=new PdfPCell(contrId);
		contrIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contrIdCell.setBorder(0);

		Phrase contractId=new Phrase(contract.getCount()+"",font10);
		PdfPCell contractIdCell=new PdfPCell(contractId);
		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractIdCell.setBorder(0);
		

		Phrase tel_No=new Phrase("Tel No",font10bold);
		PdfPCell tel_NoCell=new PdfPCell(tel_No);
		tel_NoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		tel_NoCell.setBorder(0);

		Phrase pocTel=new Phrase(cust.getCellNumber1()+"",font10);
		PdfPCell pocTelCell=new PdfPCell(pocTel);
		pocTelCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pocTelCell.setBorder(0);
		

		Phrase kindAttn=new Phrase("Kind Attn",font10bold);
		PdfPCell kindAttnCell=new PdfPCell(kindAttn);
		kindAttnCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		kindAttnCell.setBorder(0);

		Phrase kindAttnt=new Phrase(cust.getFullname()+"",font10);
		PdfPCell kindAttntCell=new PdfPCell(kindAttnt);
		kindAttntCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		kindAttntCell.setBorder(0);
		
		PdfPTable table =new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setSpacingAfter(10f);
		table.addCell(contrIdCell);
		table.addCell(createColonCell(":", font10, 0,0,0,0, Element.ALIGN_LEFT));
		table.addCell(contractIdCell);
		table.addCell(createblankcell("", font10, 0,0,0,0, Element.ALIGN_LEFT));
		table.addCell(tel_NoCell);
		table.addCell(createColonCell(":", font10, 0,0,0,0, Element.ALIGN_LEFT));
		table.addCell(pocTelCell);
		table.addCell(createblankcell("", font10, 0,0,0,0, Element.ALIGN_LEFT));
		table.addCell(kindAttnCell);
		table.addCell(createColonCell(":", font10, 0,0,0,0, Element.ALIGN_LEFT));
		table.addCell(kindAttntCell);
		table.addCell(createblankcell("", font10, 0,0,0,0, Element.ALIGN_LEFT));
		try {
			table.setWidths(colwidthcont);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createaddress() {
		// TODO Auto-generated method stub

		String custAdd1 = "";

		String custFullAdd1 = "";

		if (cust.getAdress() != null) {

			if (cust.getAdress().getAddrLine2() != "") {

				if (cust.getAdress().getLandmark() != "") {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2() + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (cust.getAdress().getLandmark() != "") {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}
			System.out.println("custAdd1"+custAdd1);
			System.out.println("cust.getAdress().getAddrLine1()"+cust.getAdress().getAddrLine1());
			if (cust.getAdress().getLocality() != "") {
				custFullAdd1 = custAdd1 + "," + cust.getAdress().getState()
						+ "," + cust.getAdress().getCountry() + "\n"
						+ cust.getAdress().getLocality() + ", "
						+ cust.getAdress().getCity() + "\n"+"Pin : "
						+ cust.getAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "," + cust.getAdress().getState()
						+ "," + cust.getAdress().getCountry() + "\n"
						+ cust.getAdress().getCity() + "\n"+"Pin : "
						+ cust.getAdress().getPin();
			}
		}

		Phrase addrcell = new Phrase(custFullAdd1, font10);
		PdfPCell custname_addrcell = new PdfPCell();
		custname_addrcell.addElement(addrcell);
		custname_addrcell.setBorder(0);
		custname_addrcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table=new PdfPTable(1);
		table.addCell(createStringCell("To,", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		table.addCell(custname_addrcell);
		table.setWidthPercentage(100);
		table.setSpacingAfter(10f);
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void createdetailsdata() {
		// TODO Auto-generated method stub
		PdfPTable atable=new PdfPTable(1);
		atable.addCell(createStringCell("Dear Sir/Madam", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		atable.setWidthPercentage(100);
		try {
			document.add(atable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable btable=new PdfPTable(3);
		btable.setWidthPercentage(100);
		btable.addCell(createStringCell("Sub", font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		btable.addCell(createColonCell(":", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		btable.addCell(createStringCell("Renewal of our contract No."+contract.getCount()+" Dated :"+fmt.format(contract.getStartDate())+" by "+contract.getEmployee(), font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		try {
			btable.setWidths(colwidthsub);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(btable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("contract.getServiceScheduleList().size()"+contract.getServiceScheduleList().size());
		
		for (int i = 0; i < contract.getServiceScheduleList().size(); i++) {
			
		PdfPTable ctable=new PdfPTable(4);
		ctable.setWidthPercentage(100);
		try {
			ctable.setWidths(colwidthprod);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ctable.addCell(createStringCell("Premises", font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		ctable.addCell(createColonCell(":", font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		
		if(contract.getServiceScheduleList().get(i).getPremisesDetails()!=null){
			ctable.addCell(createStringCell(contract.getServiceScheduleList().get(i).getPremisesDetails(), font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		}else{
			ctable.addCell(createStringCell("", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		}
		ctable.addCell(createblankcell("", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		
		ctable.addCell(createStringCell("Treatment", font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		ctable.addCell(createColonCell(":", font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		ctable.addCell(createStringCell(contract.getServiceScheduleList().get(i).getScheduleProdName(), font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		ctable.addCell(createStringCell(contract.getServiceScheduleList().get(i).getScheduleNoOfServices()+" Services", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		try {
			document.add(ctable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		PdfPTable dtable=new PdfPTable(1);
		dtable.setWidthPercentage(100);
		dtable.addCell(createStringCell("We invite your attention to our above contract with you for regular pest control which is due to expire on "+fmt.format(contract.getEndDate()), font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createStringCell("As you are undoubtedly aware of the manifold advantages of continuing our above services at your premises, we are sure you would be interested to renew the contract for a further period.", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createStringCell("Our charges for renewal of the contract would be Rs. "+df.format(contract.getNetpayable())+" /- p.a.", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
	
		String rates ="";
		for (int i = 0; i <contract.getProductTaxes().size(); i++) {
			
			if(i==0){
				rates=rates+"Rs "+contract.getTotalAmount()+" /-";
			}
			
			if(contract.getProductTaxes().get(i).getChargePayable()>0){
				rates=rates+" + "+contract.getProductTaxes().get(i).getChargeName()+" Rs "+df.format(contract.getProductTaxes().get(i).getChargePayable())+" /- p.a.";
		}
	}
		
		dtable.addCell(createStringCell(rates, font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		String amountInWord=ServiceInvoicePdf.convert(contract.getNetpayable());
		dtable.addCell(createStringCell("Rs "+amountInWord+", only", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		
		dtable.addCell(createStringCell("Kindly send us your confirmation at the earliest to enable us to maintain our high standard services", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createblankcell(" ", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createStringCell("Yours faithfully,", font10, 0, 0, 0, 0, Element.ALIGN_LEFT));
		dtable.addCell(createStringCell("For "+comp.getBusinessUnitName(), font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		dtable.addCell(createblankcell(" ", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createblankcell(" ", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createblankcell(" ", font10, 0, 0, 0, 0, Element.ALIGN_JUSTIFIED));
		dtable.addCell(createStringCell("(AUTHORISED SIGNATORY", font10bold, 0, 0, 0, 0, Element.ALIGN_LEFT));
		try {
			document.add(dtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
}
}
