package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class CreateSalesInvoicePdfServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7603445324947220504L;

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
		  String stringid=request.getParameter("Id");
		   stringid=stringid.trim();
		   Long count =Long.parseLong(stringid);
		   Invoice invoiceentity=ofy().load().type(Invoice.class).id(count).now();
		   
		   //Date 31-08-2017 added by vijay for differentiate sales pdf and purchase pdf.  po pdf not printing
		   
		   String typeofOrder = request.getParameter("typeOfOrder");
		  
		   /**
		    * rohan added this condition to implement GST
		    */
		   
		   SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
			boolean flag = false;
			try {
				flag = invoiceentity.getInvoiceDate().after(sdf.parse("30 Jun 2017"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		   System.out.println("invoice Date "+invoiceentity.getInvoiceDate());
		   if(flag && typeofOrder.equals(AppConstants.SALESORDER))
		   {
			   System.out.println("SINGLE CONTRACT INVOICE");
//			   ServiceInvoicePdf invpdf=new ServiceInvoicePdf();
			   SalesGSTInvoice invpdf=new SalesGSTInvoice();
			   invpdf.document=new Document();
			   Document document = invpdf.document;
			   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
			  
			   if(invoiceentity.getStatus().equals("Cancelled")){
					writer.setPageEvent(new PdfCancelWatermark());
				}
			   // ************rohan added this condition for removing draft from proforma invoice 
			   
			   else if(!invoiceentity.getStatus().equals("Approved") && (!invoiceentity.getStatus().equals(BillingDocument.PROFORMAINVOICE)) && (!invoiceentity.getStatus().equals(BillingDocument.BILLINGINVOICED))){	
					 writer.setPageEvent(new PdfWatermark());
				} 
			   
			   document.open();
			   String preprintStatus=request.getParameter("preprint");
			   System.out.println("ppppppppppppppppppooooooooooo"+count);
			   invpdf.setInvoice(count);
			   invpdf.createPdf(preprintStatus);
			   document.close();
		   }
		   else 
		   {
			   SalesInvoicePdf invpdf=new SalesInvoicePdf();
			   invpdf.document=new Document();
			   Document document = invpdf.document;
			   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
			   
			   if(invoiceentity.getStatus().equals("Cancelled"))	{
					 writer.setPageEvent(new PdfCancelWatermark());
				}else 
			   if(!invoiceentity.getStatus().equals("Approved")){	
					 writer.setPageEvent(new PdfWatermark());
				}
			   
			   document.open();
			   
			   invpdf.setInvoice(count);
			   invpdf.createPdf();
			   document.close();
	  		}
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }

}
