package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;

public class GenerateQRPdf extends HttpServlet{

	
	private static final long serialVersionUID = 410547881120889343L;
	Logger logger = Logger.getLogger("GenerateQRPdf");

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("application/pdf"); 
		try {
			logger.log(Level.SEVERE,"in doGet");
			String customerId = request.getParameter("CustomerId");		
			String customerName = request.getParameter("CustomerName");
			String customerBranchName = request.getParameter("CustomerBranch");
			String customerBranchId = request.getParameter("CustomerBranchId");		
			String companyid=request.getParameter("companyId");
			Long companyId=Long.parseLong(companyid);
			Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			CustomerBranchDetails custbranch = ofy().load().type(CustomerBranchDetails.class).filter("count",Integer.parseInt(customerBranchId))
					.filter("companyId", companyId).first().now();
			
			Document document = new Document(); 
	 	    PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); 
	 	    document.setPageSize(PageSize.A4);
	 	  	document.setMargins(20, 20, 120, 100);
	 	  	document.setMarginMirroring(false);
			
		  	HeaderFooterPageEvent event = new HeaderFooterPageEvent(comp,custbranch.getBranch(),"No");
		  	writer.setPageEvent(event);
		  	document.open();
		  	createPdf(comp,custbranch,document);
		  	document.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void createPdf(Company comp,CustomerBranchDetails custbranch,Document document) {
		logger.log(Level.SEVERE,"in createPdf");
		Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		PdfUtility pdfUtility=new PdfUtility();
		
		String url="";
		if(comp.getZohoSurveyUrl()!=null&&!comp.getZohoSurveyUrl().equals(""))
			url=comp.getZohoSurveyUrl();
		
		url+="?CustomerID="+custbranch.getCinfo().getCount()+"&OPSCode="+custbranch.getBusinessUnitName();
		
		logger.log(Level.SEVERE,"url="+url);
		
		BarcodeQRCode barcodeQRCode = new BarcodeQRCode(url, 1000, 1000, null);https://survey.zoho.in/survey/newui#/portal/60002493631/department/DjBF2y/survey/11749000002229005/launch
	     
		table.addCell(pdfUtility.getPdfCell("Name: "+ custbranch.getCinfo().getFullName(), font10, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
		
		table.addCell(pdfUtility.getPdfCell("OPS Code: "+custbranch.getBusinessUnitName(), font10, Element.ALIGN_CENTER, Element.ALIGN_BOTTOM, 0, 0, 0,0,0,-1,-1,-1,-1));
		
		Image codeQrImage=null;
			try {
				codeQrImage = barcodeQRCode.getImage();
			} catch (BadElementException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        codeQrImage.scaleAbsolute(350, 350);  //50,50 old value. Ashwini Patil Date:20-12-2023  	Changed size as few qr code were not scannable in old size and clients were rejecting them		
		
		
	    PdfPCell qrCodeCell = new PdfPCell(codeQrImage);
		qrCodeCell.setBorder(0);
		qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qrCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(qrCodeCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
