package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocprinting.SalesGSTInvoice;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class PurchaseGSTInvoice {

	Logger logger=Logger.getLogger("PurchaseGSTInvoice.class");
	ArrayList<String> dataStringList;
	ArrayList<Integer> alignmentList;
	ArrayList<Chunk> chunkList;
	ArrayList<Font> fontList;
	boolean authOnLeft = false;
	
	float[] columnMoreLeftWidths = {2f,1f};
	float[] columnMoreLeftHeaderWidths = {1.7f,1.3f};
	
	float[] columnMoreRightWidths = {0.8f,2.2f};
	float[] columnMoreRightCheckBoxWidths = {0.3f,2.7f};
	float[] columnHalfWidth = {1f,1f};
	float[] columnHalfInnerWidth = {0.65f,1f};
//	float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
	float[] columnCollonWidth = {1.8f,0.2f,7.5f};
	float[] columnCollonGSTWidth = {0.8f,0.2f,1.3f};
	float[] columnCollonOtherChargesWidth = {0.8f,0.2f,0.8f,1.3f,0.8f};
//	float[] columnStateCodeCollonWidth = {35,5,15,45};
	float[] columnStateCodeCollonWidth = { 3.5f, 2f, 0.2f, 1f };
	float[] columnDateCollonWidth = {1.5f,0.2f,1.2f};
	
	float[] column6RowCollonWidth = {0.5f,0.2f,1.5f,0.8f,0.2f,0.5f};
	float[] column16CollonWidth = {0.1f,0.4f,0.2f,0.15f,0.15f,0.3f,0.3f,0.15f,0.25f,0.15f,0.25f,0.15f,0.25f,0.15f,0.25f,0.3f};
	float[] column15CollonWidth = {0.1f,0.4f,0.2f,0.15f,0.15f,0.3f,0.3f,0.15f,0.25f,0.15f,0.25f,0.15f,0.25f,0.15f,0.3f};
	float[] column12CollonWidth = {0.1f/*1*/,0.4f/*2*/+0.2f/*3*/+0.2f/*4*/+0.15f/*5*/+0.3f/*6*/,0.35f/*7*/,0.15f/*8*/,0.15f/*9*/,0.3f/*10*/,0.2f/*11*/,0.3f/*12*/,0.2f/*13*/,0.2f/*14*/,0.2f/*15*/,0.3f/*16*/};
//								  {Sr No,Services,HSN ACS,UOM,Qty,Rate,Amount,Disc,Taxable amt,CGST,SGST,IGST,Total}
	float[] column8CollonWidth = {0.1f+0.4f+0.2f+0.15f+0.15f+0.3f,0.3f,0.15f,0.25f,0.15f+0.25f,0.15f+0.25f,0.15f+0.25f,0.3f};
	float[] column5CollonWidth = {0.1f/*1*/+0.4f/*2*/+0.2f/*3*/+0.2f/*4*/+0.15f/*5*/+0.3f/*6*/+0.35f/*7*/+0.15f/*8*/+0.15f/*9*/+0.3f/*10*/,0.2f/*11*/,0.3f/*12*/,0.2f/*13*/+0.2f/*14*/+0.2f/*15*/,0.3f/*16*/};
	
	/**
	 * rohan added this flag for universal pest control 
	 * This is used to print vat no and other article information  	
	 */
		
		Boolean UniversalFlag = false;
	/**
	 *   ends here 
	 */
		
	public Document document;
	/**
	 * Added By Rahul Verma Max Lines which can be used between products and 
	 * characters of service Name in product Table represents on Line
	 * */

	int noOfLines = 16;
	int noOfPage=1;
	
	/**
	 * This is where lines breaks
	 */
	int prouductCount = 0;
	
	VendorInvoice invoiceentity;
	List<BillingDocumentDetails> billingDoc; 
	ProcessConfiguration processConfig; 
	Customer cust;
	Company comp;
	PurchaseOrder purchaseOrder;
	GRN grn;
	List<ContractCharges> billingTaxesLis; // ajinkya added this 03/07/2017
	List<CustomerBranchDetails> custbranchlist;
	List<CustomerBranchDetails> customerbranchlist;
	SimpleDateFormat sdf;
	DecimalFormat df=new DecimalFormat("0.00");
	boolean upcflag=false;
	boolean productDescFlag = false;
	/**Date 11-8-2020 by Amol 
	 * this flag is used to print orderno and grn no
	 */
	boolean orderNoAndGrnNO=false;
	double totalAmount = 0;
	double discAmount = 0;
	double assValAmount = 0;
	double cgstTotal = 0;
	double sgstTotal = 0;
	double igstTotal = 0;
	double totalAmountIncludingTax = 0;
	/**
	 * Rohan added this for Universal pest for printing  	
	 */
	Boolean multipleCompanyName = false;
	/* Added By Rahul Verma on Date 28 Aug 2017 */
	CompanyPayment comppayment;
	private PdfPCell imageSignCell;
	/**
	 * ends here 
	 */
	
	List<PaymentTerms> payTermsLis;
	
	List<State> stateList;
	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	/**
	 * Date 9/12/2017
	 * By Jayshree
	 * Des.to increse the font size by one
	 */
	Font font13 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	//end by jayshree
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	
	Phrase blankCell=new Phrase(" ",font10);

	float[] column3ProdCollonWidth = { 1f, 0.9f, 0.4f };
	
	/*Total Amount*/
//	double totalAmount;
	boolean printAttnInPdf = false;
	int vendorID;
	Vendor vendor;
	String gstinValue="";
	/** date 20.12.2018 added by komal for branch as company code **/
	Branch branchDt = null;
	ServerAppUtility serverApp = new ServerAppUtility();
	String companyNameValue = "";
	
	public void setInvoice(Long count) {
		//Load Invoice
		invoiceentity=ofy().load().type(VendorInvoice.class).id(count).now();
//		
//		billingDoc=invoiceentity.getArrayBillingDocument();
//		invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
//		arrPayTerms=invoiceentity.getArrPayTerms();
	//Load Customer
		
		
		
	if(invoiceentity.getCompanyId()==null)
		cust=ofy().load().type(Customer.class).filter("count",invoiceentity.getPersonInfo().getCount()).first().now();
	else
		cust=ofy().load().type(Customer.class).filter("count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).first().now();
		
	//Load Company
	if(invoiceentity.getCompanyId()==null)
	   comp=ofy().load().type(Company.class).first().now();
	else
	   comp=ofy().load().type(Company.class).filter("companyId",invoiceentity.getCompanyId()).first().now();
	
	/** date 20.12.2018 added by komal to store company name as when we apply branchascompany value of businessunit name gets changed **/
	if(comp != null){
		companyNameValue = comp.getBusinessUnitName();
	}
	
	if(invoiceentity.getCompanyId()!=null)
		purchaseOrder=ofy().load().type(PurchaseOrder.class).filter("count",invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
	else
		purchaseOrder=ofy().load().type(PurchaseOrder.class).filter("count", invoiceentity.getContractCount()).first().now();
	
	 payTermsLis=purchaseOrder.getPaymentTermsList();
	 System.out.println("payment list size"+payTermsLis.size());

	 if(invoiceentity.getCompanyId()!=null){
		 int grnId = 0;
		 if(invoiceentity.getRefNumber() != null && !invoiceentity.getRefNumber().equals("")){
			 grnId = Integer.parseInt(invoiceentity.getRefNumber());
			 grn=ofy().load().type(GRN.class).filter("count",grnId).filter("companyId",invoiceentity.getCompanyId()).first().now();
		 }else{
			 grn=ofy().load().type(GRN.class).filter("poNo",purchaseOrder.getCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
		 }
	 	
	 }else{
		grn=ofy().load().type(GRN.class).filter("poNo", purchaseOrder.getCount()).first().now();
	
	 }
	logger.log(Level.SEVERE , "GRN"+grn.getCount());
	 
	 
	if(invoiceentity.getCompanyId()==null)
		custbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).list();
	else
		custbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).list();
	
//	if (invoiceentity.getCompanyId() != null) {
//		comppayment = ofy().load().type(CompanyPayment.class)
//				.filter("paymentDefault", true)
//				.filter("companyId", invoiceentity.getCompanyId()).first()
//				.now();
//	}
	/****************************** vijay ************************/
	
	System.out.println("Branch name======"+invoiceentity.getCustomerBranch());
	if(invoiceentity.getCompanyId()==null)
		customerbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).list();
	else
		customerbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).filter("companyId", invoiceentity.getCompanyId()).list();
	
	System.out.println("Banch updated====="+invoiceentity.getCustomerBranch());
	/****************************** vijay ************************/
	
	
	stateList=ofy().load().type(State.class).filter("companyId", invoiceentity.getCompanyId()).list();
	
	/************************************Letter Head Flag*******************************/
	
	if(invoiceentity.getCompanyId()!=null)
	{
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					upcflag =true;
				}
				
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("OnlyForUniversal")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					UniversalFlag=true;
				}
				
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintMultipleCompanyNamesFromInvoiceGroup")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					multipleCompanyName=true;
				}
				
				if (processConfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("AuthorityOnLeft")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					authOnLeft = true;
				}
				if (processConfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("PrintAttnInPdf")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					printAttnInPdf = true;
				}
				
				if (processConfig.getProcessList().get(k).getProcessType()
						.trim()
						.equalsIgnoreCase("PrintProductDescriptionOnPdf")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					productDescFlag = true;
				}
				if (processConfig.getProcessList().get(k).getProcessType()
						.trim()
						.equalsIgnoreCase("PrintOrderNoAndGrnNo")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					orderNoAndGrnNO = true;
				}
			}
		}
	 }
	
	if (purchaseOrder.getVendorDetails() != null) {
		ArrayList<VendorDetails> vendorlist = purchaseOrder.getVendorDetails();
		for (int i = 0; i < vendorlist.size(); i++) {
			if (vendorlist.get(i).getStatus() == true) {
				vendorID = vendorlist.get(i).getVendorId();
			}
		}
	}
	vendor = (Vendor) ofy().load().type(Vendor.class).filter("companyId", purchaseOrder.getCompanyId()).filter("count", vendorID).first().now();
	
	/** date 20.12.2018 added by komal to implement branch as company in purchase invoice**/
	if (invoiceentity != null && invoiceentity.getBranch() != null
			&& invoiceentity.getBranch().trim().length() > 0) {

	branchDt = ofy().load().type(Branch.class)
				.filter("companyId", invoiceentity.getCompanyId())
				.filter("buisnessUnitName", invoiceentity.getBranch())
				.first().now();
	}

	if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
			"Branch", "BranchAsCompany", comp.getCompanyId())) {

		logger.log(Level.SEVERE, "Process active --");
		
			if (branchDt != null && branchDt.getPaymentMode() != null
					&& !branchDt.getPaymentMode().trim().equals("")) {

				logger.log(Level.SEVERE,
						"Process active --" + branchDt.getPaymentMode());

				List<String> paymentDt = Arrays.asList(branchDt
						.getPaymentMode().trim().split("/"));

				if (paymentDt.get(0).trim().matches("[0-9]+")) {

					int payId = Integer.parseInt(paymentDt.get(0).trim());

					comppayment = ofy().load()
							.type(CompanyPayment.class)
							.filter("count", payId)
							.filter("companyId", invoiceentity.getCompanyId())
							.first().now();

					if (comppayment != null) {
						comp = ServerAppUtility.changeBranchASCompany(
								branchDt, comp);
					}

				}

			}
		
	}else{
		if (invoiceentity.getCompanyId() != null) {
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		}
	}
	/** end komal **/
	
	
	
	ArrayList<SalesOrderProductLineItem> vendorlist=convertOtherChargesToPoitem(invoiceentity.getOtherCharges());
	if(vendorlist.size()!=0){
		invoiceentity.getSalesOrderProducts().addAll(vendorlist);
	}
	}
	
	private ArrayList<SalesOrderProductLineItem> convertOtherChargesToPoitem(
			ArrayList<OtherCharges> otherCharges) {
		ArrayList<SalesOrderProductLineItem> list=new ArrayList<SalesOrderProductLineItem>();
		
		for(OtherCharges othercharges:invoiceentity.getOtherCharges()){
			
			SalesOrderProductLineItem productdetails=new SalesOrderProductLineItem();
			 productdetails.setPrduct(new ItemProduct());
			productdetails.setProdName(othercharges.getOtherChargeName());
			productdetails.setHsnCode(othercharges.getHsnSacCode());		
			productdetails.setUnitOfMeasurement("");		
			productdetails.setQuantity(0d);
			productdetails.setPrice(othercharges.getAmount());
			productdetails.setServiceTax(othercharges.getTax1());	
			productdetails.setVatTax(othercharges.getTax2());
					
					list.add(productdetails);
		}
		
		
		return list;
	}

	public void createPdf(String preprintStatus) {
		
		sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		for(int i=0;i<3;i++)
		{
			noOfLines=16;
			double discount = 0,roundOff=0;
			discount= invoiceentity.getDiscountAmt();
			roundOff=invoiceentity.getDiscount();
			if (discount != 0) {
				noOfLines = noOfLines - 1;
			}
			if(roundOff!=0){
				noOfLines=noOfLines-1;
			}
			if (invoiceentity.getOtherCharges().size() > 0) {
				noOfLines = noOfLines - 1;
			}
		if(upcflag==false && preprintStatus.equals("plane")){
//			createLogo(document,comp);//Date 28/11/2017 Comment By jayshree
			createHeader(i);
//			createCompanyAddress();
		}else{
			
			if(preprintStatus.equals("yes")){
				System.out.println("inside prit yes");
				createBlankforUPC(i);
			}
			if(preprintStatus.equals("no")){
			    if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
				createBlankforUPC(i);
			}
		}
		
		createInvoiceDetails();
		createCustomerDetails();
		createProductDetails();
		createProductDetailsVal();
		createProductTotalValues();
		if (discount != 0) {
			createFooterDisCountAfterPart(discount);
		}
		if (invoiceentity.getOtherCharges().size() > 0) {
			createFooterOtherChargesPart2();
		}
		createFooterTaxPart();
		if(roundOff!=0){
			createRoundOffNetPayPart(roundOff);
		}
		createFooterAmountInWords_NetPayPart();
//		createTermsAndCondition();
//		createFooterLastPart(preprintStatus);
//		if(productDescFlag){
//			createProductDescription();
//		}
		if (noOfLines <= 0 && prouductCount != 0) {
			System.out.println("noOfLines...."+noOfLines);
			createAnnexureForRemainingProduct(prouductCount);
		}
		createFooterTab();
		
		
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	}
	
	
	private void createTermsAndCondition() {

		String friends = "";
		
		int remainingLinesForTerms=5;
		friends=invoiceentity.getComment().trim();
		
		if(friends.length()>(138*5)){
			friends = friends.substring(0,(138*5));
		}else{
			friends = friends;
		}
		Phrase termNcondVal = new Phrase("Remarks: \n" + friends, font10bold);
		
		PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
		termNcondValCell.setBorderWidthBottom(0);
		termNcondValCell.setBorderWidthTop(0);
		PdfPTable pdfTable = new PdfPTable(1);
		pdfTable.setWidthPercentage(100);
		pdfTable.addCell(termNcondValCell);

		Phrase blankPhrase = new Phrase(" ", font10bold);
		PdfPCell blank = new PdfPCell(blankPhrase);
		blank.setBorderWidthBottom(0);
		blank.setBorderWidthTop(0);
		remainingLinesForTerms=remainingLinesForTerms-(friends.length()/(138));
		System.out.println("remainingLinesForTerms"+remainingLinesForTerms);
		for (int i = 0; i < remainingLinesForTerms; i++) {
			pdfTable.addCell(blank);
		}
		PdfPCell pdfPcell = new PdfPCell(pdfTable);
		pdfPcell.setBorder(0);

		PdfPTable table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		table1.addCell(pdfPcell);
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createRoundOffNetPayPart(double roundOff) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Round Off", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(roundOff) + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterOtherChargesPart2() {
		// TODO Auto-generated method stub

		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// String amtInWordsVal = "Amount in Words : Rupees "
		// + SalesInvoicePdf.convert(invoiceentity.getNetPayable())
		// + " Only/-";
		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		otherChargesTable.addCell(amtInWordsValCell);

		Phrase otherCharges = new Phrase("Other Charges", font10bold);

		PdfPCell netPayCell = new PdfPCell(otherCharges);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		double totalOtherCharges = 0;
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			totalOtherCharges = totalOtherCharges
					+ invoiceentity.getOtherCharges().get(i)
							.getAmount();
		}
		Phrase netPayVal = new Phrase(totalOtherCharges + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		otherChargesTable.addCell(netPayableCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountAfterPart(double discount) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Discount Amt", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(discount) + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createProductTotalValues() {
		
		PdfPTable totalTable=new PdfPTable(5);
		totalTable.setWidthPercentage(100);
		try {
			totalTable.setWidths(column5CollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		double totalAmount = 0;
		double discAmount = 0;
		double assValAmount = 0;
		double cgstTotal = 0;
		double sgstTotal = 0;
		double igstTotal = 0;
		double totalAmountIncludingTax = 0;
		double amountValue = 0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			
			
			
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				amountValue = invoiceentity.getSalesOrderProducts().get(i)
						.getPrice();
			}else{
			amountValue = invoiceentity.getSalesOrderProducts().get(i)
						.getPrice()
						* invoiceentity.getSalesOrderProducts().get(i)
								.getQuantity();
			}
			totalAmount=totalAmount+amountValue;
			discAmount=discAmount+invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
			
			
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
			assValAmount=assValAmount+ invoiceentity.getSalesOrderProducts().get(i).getPrice();
			}else{
			assValAmount=assValAmount+invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
			}
			
			double ctaxValue =0;
			
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				ctaxValue = getTaxAmount(invoiceentity
						.getSalesOrderProducts().get(i)
						.getPrice(), invoiceentity
						.getSalesOrderProducts().get(i).getVatTax()
						.getPercentage());
			}else{
				ctaxValue = getTaxAmount(invoiceentity
						.getSalesOrderProducts().get(i)
						.getBasePaymentAmount(), invoiceentity
						.getSalesOrderProducts().get(i).getVatTax()
						.getPercentage());
			}
			
			
			
			double staxValue = 0;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				staxValue = getTaxAmount(invoiceentity
						.getSalesOrderProducts().get(i)
						.getPrice(), invoiceentity
						.getSalesOrderProducts().get(i).getServiceTax()
						.getPercentage());
				
			}else{
				staxValue = getTaxAmount(invoiceentity
						.getSalesOrderProducts().get(i)
						.getBasePaymentAmount(), invoiceentity
						.getSalesOrderProducts().get(i).getServiceTax()
						.getPercentage());
				
			}
			
			
			
			
			double indivTotalAmount =0;
			
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				indivTotalAmount = invoiceentity.getSalesOrderProducts().get(i).getPrice()+ ctaxValue + staxValue;
			}else{
				indivTotalAmount = invoiceentity
						.getSalesOrderProducts().get(i)
						.getBasePaymentAmount()
						+ ctaxValue + staxValue;
			}
			
		
			
			
			
			
			
			totalAmountIncludingTax = totalAmountIncludingTax + indivTotalAmount;
		}
		Phrase totalPhrase=new Phrase("Total",font10bold);
		PdfPCell totalPhraseCell=new PdfPCell(totalPhrase);
//		totalPhraseCell.setBorder(0);
		
		Phrase totalAmountPhrase=new Phrase(df.format(totalAmount),font10bold);
		PdfPCell totalAmountPhraseCell=new PdfPCell(totalAmountPhrase);
//		totalAmountPhraseCell.setBorder(0);
		
		Phrase totalDiscPhrase=new Phrase(df.format(discAmount),font10bold);
		PdfPCell totalDiscPhraseCell=new PdfPCell(totalDiscPhrase);
//		totalDiscPhraseCell.setBorder(0);

		Phrase totalAssAmtPhrase=new Phrase(df.format(assValAmount),font10bold);
		PdfPCell totalAssAmtPhraseCell=new PdfPCell(totalAssAmtPhrase);
//		totalAssAmtPhraseCell.setBorder(0);

		Phrase totalcgstPhrase=new Phrase(df.format(cgstTotal),font10bold);
		PdfPCell totalcgstPhraseCell=new PdfPCell(totalcgstPhrase);
		totalcgstPhraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		totalcgstPhraseCell.setBorder(0);

		Phrase totalsgstPhrase=new Phrase(df.format(sgstTotal),font10bold);
		PdfPCell totalsgstPhraseCell=new PdfPCell(totalsgstPhrase);
		totalsgstPhraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		totalsgstPhraseCell.setBorder(0);

		Phrase totaligstPhrase=new Phrase(df.format(igstTotal),font10bold);
		PdfPCell totaligstPhraseCell=new PdfPCell(totaligstPhrase);
		totaligstPhraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		totaligstPhraseCell.setBorder(0);

		Phrase totalAmountInclTaxPhrase=new Phrase(df.format(totalAmountIncludingTax),font10bold);
		PdfPCell totalAmountInclTaxPhraseCell=new PdfPCell(totalAmountInclTaxPhrase);
//		totalAmountInclTaxPhraseCell.setBorder(0);
		totalTable.addCell(totalPhraseCell);
//		totalTable.addCell(totalAmountPhraseCell);
		totalTable.addCell(totalDiscPhraseCell);
		totalTable.addCell(totalAssAmtPhraseCell);
		totalTable.addCell(blankCell);
//		totalTable.addCell(totalsgstPhraseCell);
//		totalTable.addCell(totaligstPhraseCell);
		totalTable.addCell(totalAmountInclTaxPhraseCell);
		
		try {
			document.add(totalTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

private void createFooterAmountInWords_NetPayPart() {

	PdfPTable amountTable = new PdfPTable(2);
	amountTable.setWidthPercentage(100);
	try {
		amountTable.setWidths(columnMoreLeftWidths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	String amtInWordsVal = "Amount in Words : Rupees "
			+ SalesInvoicePdf.convert(invoiceentity.getNetPayable())
			+ " Only/-";
	Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font10bold);

	PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
	amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	amountTable.addCell(amtInWordsValCell);

	Phrase netPay = new Phrase("Net Payable", font10bold);

	PdfPCell netPayCell = new PdfPCell(netPay);
	netPayCell.setBorder(0);

	Phrase colon = new Phrase(":", font10bold);

	PdfPCell colonCell = new PdfPCell(colon);

	colonCell.setBorder(0);

	Phrase netPayVal = new Phrase(df.format(invoiceentity.getNetPayable())
			+ "", font10bold);
	PdfPCell netPayValCell = new PdfPCell(netPayVal);
	netPayValCell.setBorderWidthLeft(0);
	netPayValCell.setBorderWidthTop(0);
	netPayValCell.setBorderWidthBottom(0);
	netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

	PdfPTable innerRightTable = new PdfPTable(3);
	try {
		innerRightTable.setWidths(columnCollonGSTWidth);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	innerRightTable.setWidthPercentage(100);
	innerRightTable.addCell(netPayCell);
	innerRightTable.addCell(colonCell);
	innerRightTable.addCell(netPayValCell);

	PdfPCell netPayableCell = new PdfPCell(innerRightTable);
	// netPayableCell.setBorder(0);

	amountTable.addCell(netPayableCell);
	try {
		document.add(amountTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

///////////////////////////////////////////// Ajinkya Code Start Here //////////////////
		private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try 
		{
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
		

		private void createCompanyNameAsFooter(Document doc, Company comp) {
			
			
			DocumentUpload document =comp.getUploadFooter();

			//patch
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(520f);
				image2.setAbsolutePosition(40f,40f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
//			try
//			{
//			Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//			image1.scalePercent(15f);
//			image1.scaleAbsoluteWidth(520f);
//			image1.setAbsolutePosition(40f,40f);	
//			doc.add(image1);
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
			}
		/////////////////////////// Ajinkya Code end Here /////////////// 
		
		
	private void createBlankforUPC(int i) 
	{

		Image uncheckedImg=null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(9);

		
		Image checkedImg=null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(9);

		
//		Phrase phrAlp=new Phrase("  ALP",font9);
//		Paragraph paraAlp=new Paragraph();
//		paraAlp.setIndentationLeft(10f);
//		paraAlp.add(new Chunk(uncheckedImg, 0, 0, true));
//		paraAlp.add(phrAlp);
		
		//  rohan added this code 
//		float[] myWidth = {1,3,20,17,3,30,17,3,20,1};
		
		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setSpacingAfter(5f);
		mytbale.setWidthPercentage(100f);
		
//		try {
//			mytbale.setWidths(myWidth);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		Phrase myblank=new Phrase("   ",font10);
		PdfPCell myblankCell=new PdfPCell(myblank);
//		stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero=new Phrase(" ",font10);
		PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
//		stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
		Paragraph para1=new Paragraph();
		para1.setIndentationLeft(10f);
		para1.add(myblank);
		if(i==0){
			para1.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para1.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		
		para1.add(stat1Phrase);
		para1.setAlignment(Element.ALIGN_MIDDLE);
		
		PdfPCell stat1PhraseCell=new PdfPCell(para1);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
		Paragraph para2=new Paragraph();
		para2.setIndentationLeft(10f);
		
		if(i==1){
			para2.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para2.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		
		para2.add(stat2Phrase);
		para2.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell stat2PhraseCell=new PdfPCell(para2);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
		Paragraph para3=new Paragraph();
		para3.setIndentationLeft(10f);
		
		if(i==2){
			para3.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para3.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		para3.add(stat3Phrase);
		para3.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell stat3PhraseCell=new PdfPCell(para3);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		mytbale.addCell(myblankborderZero);
//		mytbale.addCell(myblankborderZero);
		mytbale.addCell(stat1PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
		mytbale.addCell(stat2PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
		mytbale.addCell(stat3PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
		
		//  ends here 
		String titlepdf="";
		String category = "";
		if(purchaseOrder.getCategory() != null && !purchaseOrder.getCategory().equals("")){
			category = " - "+purchaseOrder.getCategory();
		}
			if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
				titlepdf="Proforma Invoice";
			else
				titlepdf="Purchase"+category;

			
			Phrase titlephrase=new Phrase(titlepdf,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell titlecell=new PdfPCell();
			titlecell.addElement(titlepdfpara);
			titlecell.setBorder(0);
			
			Phrase blankphrase=new Phrase("",font8);
			PdfPCell blankCell=new PdfPCell();
			blankCell.addElement(blankphrase);
			blankCell.setBorder(0);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
			titlepdftable.addCell(blankCell);
			titlepdftable.addCell(titlecell);
			titlepdftable.addCell(blankCell);
			
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			parent.setSpacingBefore(10f);
			
			PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
			parent.addCell(titlePdfCell);
			
			
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(mytbale);
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
			
			
		
		
	}

	private void createLogo(Document doc, Company comp2) {
		

//		********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
		}
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	
	
//	try
//	{
//		Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	
	}

	private void createFooterLastPart(String preprintStatus) {
		// TODO Auto-generated method stub
		PdfPTable bottomTable = new PdfPTable(3);
		bottomTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			bottomTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		// rohan added this code for universal pest
//		if (UniversalFlag) {
//			if (con.getGroup().equalsIgnoreCase(
//					"Universal Pest Control Pvt. Ltd.")) {
//				if (!preprintStatus.equalsIgnoreCase("Plane")) {
//					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//
//						if (comp.getArticleTypeDetails().get(i)
//								.getArticlePrint().equalsIgnoreCase("Yes")) {
//
//							Phrase articalType = new Phrase(comp
//									.getArticleTypeDetails().get(i)
//									.getArticleTypeName()
//									+ " : "
//									+ comp.getArticleTypeDetails().get(i)
//											.getArticleTypeValue(), font10bold);
//							PdfPCell articalTypeCell = new PdfPCell();
//							articalTypeCell.setBorder(0);
//							articalTypeCell.addElement(articalType);
//							leftTable.addCell(articalTypeCell);
//						}
//					}
//				}
//			}
//		} else {
			// if (!preprintStatus.equalsIgnoreCase("Plane")) {
			// leftTable.addCell(articalTypeCell);asa
			ServerAppUtility serverApp = new ServerAppUtility();

			String gstin="",gstinText="";
			if (comp.getCompanyGSTType().trim()
					.equalsIgnoreCase("GST Applicable")) {
				logger.log(Level.SEVERE,"GST Applicable");
				gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
							.getBranch().trim());
			} else {
				logger.log(Level.SEVERE,"GST Not Applicable");
				gstinText = comp.getCompanyGSTTypeText().trim();
			}
				Phrase articalType2=null ;
				if(!gstin.trim().equals("")){
					logger.log(Level.SEVERE,"GST Present");
					articalType2= new Phrase("GSTIN" + " : " + gstin,
						font10bold);
				}else if (!gstinText.trim().equalsIgnoreCase("")) {
					logger.log(Level.SEVERE,"GST Not Present");
					articalType2= new Phrase(gstinText,font10bold);
				}else{
					logger.log(Level.SEVERE,"Nothing Present");
					articalType2= new Phrase("",font10bold);
					
				}

				PdfPCell articalType2Cell = new PdfPCell(articalType2);
				articalType2Cell.setBorder(0);
				leftTable.addCell(articalType2Cell);
				
				String stateCodeStr= serverApp.getStateOfCompany(comp, invoiceentity
						.getBranch().trim(),stateList);
				Phrase stateCode= new Phrase("State Code" + " : " + stateCodeStr,
						font10bold);

				PdfPCell stateCodeCell = new PdfPCell(stateCode);
				stateCodeCell.setBorder(0);
				leftTable.addCell(stateCodeCell);
			
				/**
				 * Date 14/12/2017
				 * By Jayshree
				 * To add the artical type info
				 */
				
				PdfPTable articleTab=new PdfPTable(3);
				articleTab.setWidthPercentage(100);
				
				try {
					articleTab.setWidths(new float[]{30,5,65});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

//				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
//						.trim().equalsIgnoreCase("GSTIN")) {
//					Phrase articalType = new Phrase(comp
//							.getArticleTypeDetails().get(i)
//							.getArticleTypeName()
//							+ " : "
//							+ comp.getArticleTypeDetails().get(i)
//									.getArticleTypeValue(), font10bold);
//					PdfPCell articalTypeCell = new PdfPCell(articalType);
//					articalTypeCell.setBorder(0);
//					leftTable.addCell(articalTypeCell);
				
				
				Phrase typename;
				Phrase typevalue;
				if (comp.getArticleTypeDetails().get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& comp.getArticleTypeDetails().get(i).getDocumentName()
								.equals("SalesInvoice")){
					
					typename = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName(), font10bold);
					typevalue = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font10bold);

					PdfPCell tymanecell = new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);
					tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					
					Phrase typeblank=new Phrase(":",font10bold);
					PdfPCell typeCell=new PdfPCell(typeblank);
					typeCell.addElement(typeblank);
					typeCell.setBorder(0);
					typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					PdfPCell typevalcell = new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					typevalcell.setBorder(0);
					
					
					articleTab.addCell(tymanecell);
					articleTab.addCell(typeCell);
					articleTab.addCell(typevalcell);
				}
				
			}
			
			PdfPCell articleCell=new PdfPCell(articleTab);
			articleCell.setBorder(0);
			leftTable.addCell(articleCell);
			
			//End by jayshree	
				
				
			
//		
		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font10bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);

		String companyname = "";
		if (multipleCompanyName) {
			if (invoiceentity.getGroup() != null && !invoiceentity.getGroup().equals("")) {
				companyname = invoiceentity.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}

		} else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		// ends here

		// rightTable.addCell(rightUpperCell);
		Phrase companyPhrase = new Phrase("For , " + companyname, font10bold);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setBorder(0);
		if (authOnLeft) {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		rightTable.addCell(companyParaCell);

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE,"hostUrl::"+hostUrl);
		try {
			image2= Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);
			
			imageSignCell=new PdfPCell(image2);
			imageSignCell.setBorder(0);
			if (authOnLeft) {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

//		 Image image1=Image.getInstance("images/digisign2copy.png");
//		 image1.scalePercent(15f);
//		 image1.scaleAbsoluteWidth(100f);
//		 imageSignCell=new PdfPCell(image1);
//		 imageSignCell.setBorder(0);
//		 if (authOnLeft) {
//			 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 } else {
//			 imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		 }
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
		if(imageSignCell!=null){
			rightTable.addCell(imageSignCell);
		}else{
			Phrase blank1=new Phrase(" ",font10);
			PdfPCell blank1Cell=new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			
		}
		
		Phrase signAuth;
		if(comp.getSignatoryText()!=null&&!comp.getSignatoryText().equals("")){
			signAuth = new Phrase(comp.getSignatoryText(),font10bold);
		}else{
			signAuth = new Phrase("Authorised Signatory", font10bold);
		}
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		// signParaCell.addElement();
		signParaCell.setBorder(0);
		if (authOnLeft) {
			signParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell(leftTable);
		// lefttableCell.addElement();
		PdfPCell righttableCell = new PdfPCell(rightTable);
		// if(authOnLeft){
		// righttableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// }else{
		// righttableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// }

		PdfPTable middletTable = new PdfPTable(1);
		middletTable.setWidthPercentage(100);
		if (true) {
			if (comppayment != null) {

				PdfPTable bankDetailsTable = new PdfPTable(1);
				bankDetailsTable.setWidthPercentage(100f);

				String favourOf = "";
				if (comppayment.getPaymentComName() != null
						&& !comppayment.getPaymentComName().equals("")) {
					favourOf = "Cheque should be in favour of '"
							+ comppayment.getPaymentComName() + "'";
				}
				Phrase favouring = new Phrase(favourOf, font8bold);
				PdfPCell favouringCell = new PdfPCell(favouring);
				favouringCell.setBorder(0);
				bankDetailsTable.addCell(favouringCell);

				Phrase heading = new Phrase("Bank Details", font8bold);
				PdfPCell headingCell = new PdfPCell(heading);
				headingCell.setBorder(0);
				bankDetailsTable.addCell(headingCell);

				float[] columnWidths3 = { 1.5f, 0.35f, 4.5f };
				PdfPTable bankDetails3Table = new PdfPTable(3);
				bankDetails3Table.setWidthPercentage(100f);
				try {
					bankDetails3Table.setWidths(columnWidths3);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				Phrase bankNamePh = new Phrase("Name", font8bold);
				PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
				bankNamePhCell.setBorder(0);
				bankDetails3Table.addCell(bankNamePhCell);

				Phrase dot = new Phrase(":", font8bold);
				PdfPCell dotCell = new PdfPCell(dot);
				dotCell.setBorder(0);
				bankDetails3Table.addCell(dotCell);

				String bankName = "";
				if (comppayment.getPaymentBankName() != null
						&& !comppayment.getPaymentBankName().equals("")) {
					bankName = comppayment.getPaymentBankName();
				}
				Phrase headingValue = new Phrase(bankName, font8);
				PdfPCell headingValueCell = new PdfPCell(headingValue);
				headingValueCell.setBorder(0);
				headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(headingValueCell);

				// this is for branch
				Phrase bankBranch = new Phrase("Branch", font8bold);
				PdfPCell bankBranchCell = new PdfPCell(bankBranch);
				bankBranchCell.setBorder(0);
				bankDetails3Table.addCell(bankBranchCell);
				bankDetails3Table.addCell(dotCell);

				String bankBranchValue = "";
				if (comppayment.getPaymentBranch() != null
						&& !comppayment.getPaymentBranch().equals("")) {
					bankBranchValue = comppayment.getPaymentBranch();
				}
				Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
				PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
				bankBranchValuePhCell.setBorder(0);
				bankBranchValuePhCell
						.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankBranchValuePhCell);

				Phrase bankAc = new Phrase("A/c No", font8bold);
				PdfPCell bankAcCell = new PdfPCell(bankAc);
				bankAcCell.setBorder(0);
				bankDetails3Table.addCell(bankAcCell);
				bankDetails3Table.addCell(dotCell);

				String bankAcNo = "";
				if (comppayment.getPaymentAccountNo() != null
						&& !comppayment.getPaymentAccountNo().equals("")) {
					bankAcNo = comppayment.getPaymentAccountNo();
				}
				Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
				PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
				bankAcNoValueCell.setBorder(0);
				bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankAcNoValueCell);

				Phrase bankIFSC = new Phrase("IFS Code", font8bold);
				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
				bankIFSCCell.setBorder(0);
				bankDetails3Table.addCell(bankIFSCCell);
				bankDetails3Table.addCell(dotCell);

				String bankIFSCNo = "";
				if (comppayment.getPaymentIFSCcode() != null
						&& !comppayment.getPaymentIFSCcode().equals("")) {
					bankIFSCNo = comppayment.getPaymentIFSCcode();
				}
				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
				bankIFSCNoValueCell.setBorder(0);
				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankIFSCNoValueCell);

				PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
				bankDetails3TableCell.setBorder(0);
				bankDetailsTable.addCell(bankDetails3TableCell);

				PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
				bankDetailsTableCell.setBorder(0);
				middletTable.addCell(bankDetailsTableCell);
			}
		}

		PdfPCell middletTableCell = new PdfPCell(middletTable);

		bottomTable.addCell(lefttableCell);
		bottomTable.addCell(middletTableCell);
		bottomTable.addCell(righttableCell);

		//

		Paragraph para = new Paragraph(
				"Note : This is computer generated invoice therefore no physical signature is required.",
				font8);

		try {
			document.add(bottomTable);
			// if(comp.getUploadDigitalSign()!=null){
			if(imageSignCell==null){
				document.add(para);
			}
			// }
//			if (noOfLines == 0 && prouductCount != 0) {
//				createAnnexureForRemainingProduct(prouductCount);
//			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createAnnexureForRemainingProduct(int count) {
		
		noOfPage++;
		Paragraph para  =new Paragraph("Annexure 1 :",font10bold);
		para.setAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(Chunk.NEWLINE);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		createProductDetails();
		createProductDetailsMOreThanFive(count);
	}

	private void createProductDetailsMOreThanFive(int count){
		
		

	       boolean recursiveFlag=false;
	       noOfLines=80;
	       logger.log(Level.SEVERE,"Annexture TABLE : "+noOfLines);


		
		for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			
			/**
			 * Date : 2-01-2019 By amol
			 * updating no of lines
			 */
			if (noOfLines <= 0) {
				prouductCount = i;
				recursiveFlag=true;
				break;
			}
			int lenght=invoiceentity.getSalesOrderProducts().get(i).getProdName().length();
			logger.log(Level.SEVERE,"Length : "+lenght);
			int lines= (int) Math.ceil((lenght/42.0));
			logger.log(Level.SEVERE,"LINES : "+lines);
//			noOfLines = noOfLines - 1;
			noOfLines = noOfLines - lines;
			logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
			/**
			 * End
			 */		
			
			PdfPTable productTable = new PdfPTable(12);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column12CollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell();
			srNoCell.addElement(srNo);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(),
					font10);
			PdfPCell serviceNameCell = new PdfPCell();
			serviceNameCell.addElement(serviceName);
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font10);
			} else {
				hsnCode = new Phrase("", font10);
			}
			PdfPCell hsnCodeCell = new PdfPCell();
			hsnCodeCell.addElement(hsnCode);
			productTable.addCell(hsnCodeCell);
			
			
			
			Phrase uom =null;
           if(invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement() !=null){
        	   uom= new Phrase(invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim(), font10);
           }else{
        	   uom=new Phrase("", font10);
           }
			PdfPCell uomCell = new PdfPCell();
			uomCell.addElement(uom);
			productTable.addCell(uomCell);

			
			
			String quantity = "";
			if(!invoiceentity.getSalesOrderProducts().get(i).getArea().equals("") && !invoiceentity.getSalesOrderProducts().get(i).getArea().equals("NA")){
				quantity = invoiceentity.getSalesOrderProducts().get(i).getArea();
			}else{
				quantity = invoiceentity.getSalesOrderProducts().get(i).getQuantity()+"";
			}
			
			
		     Phrase qty = new Phrase(quantity+"", font10);
			PdfPCell qtyCell = new PdfPCell();
			qtyCell.addElement(qty);
			productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getPrice())
					+ "", font10);
			PdfPCell rateCell = new PdfPCell();
			rateCell.addElement(rate);
			productTable.addCell(rateCell);

			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			totalAmount = totalAmount + amountValue;
			Phrase amount = new Phrase(df.format(amountValue) + "", font10);
			PdfPCell amountCell = new PdfPCell();
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.addElement(amount);
//			productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getFlatDiscount())
					+ "", font10);
			PdfPCell discCell = new PdfPCell();
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.addElement(disc);
			productTable.addCell(discCell);

			double assValBasePaymentAmount=0;
			if(invoiceentity
					.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
							.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
				assValBasePaymentAmount=invoiceentity
				.getSalesOrderProducts().get(i).getBasePaymentAmount();
			}else{
				assValBasePaymentAmount=invoiceentity
						.getSalesOrderProducts().get(i).getBaseBillingAmount();
			}
			Phrase taxableValue = new Phrase(df.format(assValBasePaymentAmount)
					+ "", font10);
			PdfPCell taxableValueCell = new PdfPCell();
			// taxableValueCell.setBorder(0);
			// taxableValueCell.setBorderWidthBottom(0);
			// taxableValueCell.setBorderWidthTop(0);
			taxableValueCell.addElement(taxableValue);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;

			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
					.getPercentage() != 0
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getPercentage() != 0) {
				logger.log(Level.SEVERE,"Inside NON Zero:::::");
				if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					double taxAmount = getTaxAmount(invoiceentity
							.getSalesOrderProducts().get(i)
							.getBasePaymentAmount(), invoiceentity
							.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage());
					
					double assValBasePaymentAmountGST=0;
					if(invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
									.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
						assValBasePaymentAmountGST=invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount();
					}else{
						assValBasePaymentAmountGST=invoiceentity
								.getSalesOrderProducts().get(i).getBaseBillingAmount();
					}
					double indivTotalAmount = assValBasePaymentAmountGST
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font10);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(invoiceentity
							.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage()
							+ "", font10);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font10);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
//					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font10);
					productTable.addCell(cell);
//					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
//					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font10);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setColspan(16);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					
					productTable.addCell(totalCell);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
//					String premisesVal = "";
//					for (int j = 0; j < con.getItems().size(); j++) {
//						if (invoiceentity.getSalesOrderProducts().get(i)
//								.getProdId() == con.getItems().get(j)
//								.getPrduct().getCount()) {
//							premisesVal = con.getItems().get(j)
//									.getPremisesDetails();
//						}
//
//					}
//					if (printPremiseDetails) {
//						Phrase premisesValPhrs = new Phrase(
//								"Premise Details : " + premisesVal, font8);
//						PdfPCell premiseCell = new PdfPCell();
//						premiseCell.setColspan(16);
//						premiseCell.addElement(premisesValPhrs);
//
//						productTable.addCell(premiseCell);
//					}
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if (invoiceentity.getSalesOrderProducts().get(i).getServiceTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					double taxAmount = getTaxAmount(invoiceentity
							.getSalesOrderProducts().get(i)
							.getBasePaymentAmount(), invoiceentity
							.getSalesOrderProducts().get(i).getServiceTax()
							.getPercentage());
					double assValBasePaymentAmountGST=0;
					if(invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
									.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
						assValBasePaymentAmountGST=invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount();
					}else{
						assValBasePaymentAmountGST=invoiceentity
								.getSalesOrderProducts().get(i).getBaseBillingAmount();
					}
					
					double indivTotalAmount = assValBasePaymentAmountGST
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font10);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(invoiceentity
							.getSalesOrderProducts().get(i).getServiceTax()
							.getPercentage()
							+ "", font10);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font10);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
//					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font10);
					productTable.addCell(cell);
//					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
//					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font10);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setColspan(16);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					
					productTable.addCell(totalCell);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
//					String premisesVal = "";
//					for (int j = 0; j < con.getItems().size(); j++) {
//						if (invoiceentity.getSalesOrderProducts().get(i)
//								.getProdId() == con.getItems().get(j)
//								.getPrduct().getCount()) {
//							premisesVal = con.getItems().get(j)
//									.getPremisesDetails();
//						}
//
//					}
//					if (printPremiseDetails) {
//						Phrase premisesValPhrs = new Phrase(
//								"Premise Details : " + premisesVal, font8);
//						PdfPCell premiseCell = new PdfPCell();
//						premiseCell.setColspan(16);
//						premiseCell.addElement(premisesValPhrs);
//
//						productTable.addCell(premiseCell);
//					}
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {

					if (invoiceentity.getSalesOrderProducts().get(i)
							.getVatTax().getTaxPrintName()
							.equalsIgnoreCase("CGST")) {

						double ctaxValue = getTaxAmount(invoiceentity
								.getSalesOrderProducts().get(i)
								.getBasePaymentAmount(), invoiceentity
								.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font10);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage()
								+ "", font10);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
//						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(invoiceentity
								.getSalesOrderProducts().get(i)
								.getBasePaymentAmount(), invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font10);
						PdfPCell sgstRateValCell = new PdfPCell();
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage()
								+ "", font10);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
//						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font10);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font10);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);
						double assValBasePaymentAmountGST=0;
						if(invoiceentity
								.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
										.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
							assValBasePaymentAmountGST=	invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount();
						}else{
							assValBasePaymentAmountGST=invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
						}
						double indivTotalAmount = assValBasePaymentAmountGST
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font10);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						
						// totalCell.setColspan(16);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
//						String premisesVal = "";
//						for (int j = 0; j < con.getItems().size(); j++) {
//							if (invoiceentity.getSalesOrderProducts().get(i)
//									.getProdId() == con.getItems().get(j)
//									.getPrduct().getCount()) {
//								premisesVal = con.getItems().get(j)
//										.getPremisesDetails();
//							}
//
//						}
//						if (printPremiseDetails) {
//							Phrase premisesValPhrs = new Phrase(
//									"Premise Details : " + premisesVal, font8);
//							PdfPCell premiseCell = new PdfPCell();
//							premiseCell.setColspan(16);
//							premiseCell.addElement(premisesValPhrs);
//							productTable.addCell(premiseCell);
//						}
						 try {
						 document.add(productTable);
						 } catch (DocumentException e) {
						 // TODO Auto-generated catch block
						 e.printStackTrace();
						 }

					} else if (invoiceentity.getSalesOrderProducts().get(i)
							.getVatTax().getTaxPrintName()
							.equalsIgnoreCase("SGST")) {

						double ctaxValue = getTaxAmount(invoiceentity
								.getSalesOrderProducts().get(i)
								.getBasePaymentAmount(), invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font10);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage()
								+ "", font10);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
//						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(invoiceentity
								.getSalesOrderProducts().get(i)
								.getBasePaymentAmount(), invoiceentity
								.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font10);
						PdfPCell sgstRateValCell = new PdfPCell();
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage()
								+ "", font10);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
//						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font10);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font10);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);
						double assValBasePaymentAmountGST=0;
						if(invoiceentity
								.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
										.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
							assValBasePaymentAmountGST=	invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount();
						}else{
							assValBasePaymentAmountGST=invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
						}
						double indivTotalAmount = assValBasePaymentAmountGST
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font10);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						
						// totalCell.setColspan(16);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);

						productTable.addCell(totalCell);
//						String premisesVal = "";
//						for (int j = 0; j < con.getItems().size(); j++) {
//							if (invoiceentity.getSalesOrderProducts().get(i)
//									.getProdId() == con.getItems().get(j)
//									.getPrduct().getCount()) {
//								premisesVal = con.getItems().get(j)
//										.getPremisesDetails();
//							}
//
//						}
//						if (printPremiseDetails) {
//							Phrase premisesValPhrs = new Phrase(
//									"Premise Details : " + premisesVal, font8);
//							PdfPCell premiseCell = new PdfPCell();
//							premiseCell.setColspan(16);
//							premiseCell.addElement(premisesValPhrs);
//							productTable.addCell(premiseCell);
//						}
//						

						try {
							document.add(productTable);
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			} else {
				logger.log(Level.SEVERE,"Inside Zero else:::::");

				PdfPCell cell = new PdfPCell(new Phrase("NA", font10));
				productTable.addCell(cell);
//				productTable.addCell(cell);
				productTable.addCell(cell);
//				productTable.addCell(cell);
				productTable.addCell(cell);
//				productTable.addCell(cell);
				Phrase totalPhrase = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getTotalAmount())
						+ "", font10);
				PdfPCell totalCell = new PdfPCell(totalPhrase);
				totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// totalCell.setColspan(16);
				// totalCell.setBorder(0);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				productTable.addCell(totalCell);

//				String premisesVal = "";
//				for (int j = 0; j < con.getItems().size(); j++) {
//					if (invoiceentity.getSalesOrderProducts().get(i)
//							.getProdId() == con.getItems().get(j).getPrduct()
//							.getCount()) {
//						premisesVal = con.getItems().get(j)
//								.getPremisesDetails();
//					}
//
//				}
//				if (printPremiseDetails) {
//					Phrase premisesValPhrs = new Phrase("Premise Details : "
//							+ premisesVal, font8);
//					PdfPCell premiseCell = new PdfPCell();
//					premiseCell.setColspan(16);
//					premiseCell.addElement(premisesValPhrs);
//					productTable.addCell(premiseCell);
//				}
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void createFooterTaxPart() {
		// TODO Auto-generated method stub
		PdfPTable pdfPTaxTable = new PdfPTable(3);
		pdfPTaxTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			pdfPTaxTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

		float[] column3widths = { 2f, 2f, 6f };
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// heading

		Phrase day = new Phrase("Day", font8bold);
		PdfPCell dayCell = new PdfPCell(day);
		dayCell.setBorder(0);

		Phrase percent = new Phrase("Percent", font8bold);
		PdfPCell percentCell = new PdfPCell(percent);
		percentCell.setBorder(0);

		Phrase comment = new Phrase("Comment", font8bold);
		PdfPCell commentCell = new PdfPCell(comment);
		commentCell.setBorder(0);

//		Phrase terms = new Phrase("Payment Terms", font8bold);
//		PdfPCell termsCell = new PdfPCell(terms);
//
//		Phrase termsBlank = new Phrase(" ", font10bold);
//		PdfPCell termsBlankCell = new PdfPCell(termsBlank);
//		termsBlankCell.setBorder(0);
//		leftTable.addCell(termsCell);
//		leftTable.addCell(termsBlankCell);
//		leftTable.addCell(termsBlankCell);
		leftTable.addCell(dayCell);
		leftTable.addCell(percentCell);
		leftTable.addCell(commentCell);

		// Values
		for (int i = 0; i < invoiceentity.getArrPayTerms().size(); i++) {
			Phrase dayValue = new Phrase(invoiceentity.getArrPayTerms().get(i)
					.getPayTermDays()
					+ "", font8);
			PdfPCell dayValueCell = new PdfPCell(dayValue);
			dayValueCell.setBorder(0);
			leftTable.addCell(dayValueCell);

			Phrase percentValue = new Phrase(df.format(invoiceentity.getArrPayTerms().get(i).getPayTermPercent())
					+ "", font8);
			PdfPCell percentValueCell = new PdfPCell(percentValue);
			percentValueCell.setBorder(0);
			leftTable.addCell(percentValueCell);

			Phrase commentValue = new Phrase(invoiceentity.getArrPayTerms()
					.get(i).getPayTermComment(), font8);
			PdfPCell commentValueCell = new PdfPCell(commentValue);
			commentValueCell.setBorder(0);
			leftTable.addCell(commentValueCell);
		}

		// try {
		// document.add(leftTable);
		// } catch (DocumentException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax",
				font10bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);

		Phrase amtB4TaxValphrase = new Phrase(totalAmount + "", font10bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {
			if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();
				Phrase IGSTphrase = new Phrase("IGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorder(0);

				Phrase IGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(IGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(IGSTValphraseCell);

			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase SGSTphrase = new Phrase("SGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorder(0);
				// SGSTphraseCell.addElement(SGSTphrase);

				Phrase SGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(SGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(SGSTValphraseCell);
			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase CGSTphrase = new Phrase("CGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				// CGSTphraseCell.addElement(CGSTphrase);

				Phrase CGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				rightInnerTable.addCell(CGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(CGSTValphraseCell);
			}
		}

		Phrase GSTphrase = new Phrase("Total GST", font10bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
				font10bold);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
		// innerRightCell.addElement();

		rightTable.addCell(innerRightCell);

		PdfPTable middleTable = new PdfPTable(1);
		middleTable.setWidthPercentage(100);

		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");
		// PdfPTable otherChargesTable = new PdfPTable(1);
		// otherChargesTable.setWidthPercentage(100);
		// try {
		// otherChargesTable.setWidths(columnMoreLeftWidths);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// Phrase blank=new Phrase("",font10bold);
		// PdfPCell blankCell=new PdfPCell(blank);
		// otherChargesTable.addCell(blankCell);

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			Phrase chargeName, taxes, assVal;
			PdfPCell pCell;
			if (i == 0) {
				chargeName = new Phrase("Charge Name", font10bold);
				taxes = new Phrase("Taxes", font10bold);
				assVal = new Phrase("Amt", font10bold);
				pCell = new PdfPCell(chargeName);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
				pCell = new PdfPCell(taxes);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
				pCell = new PdfPCell(assVal);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
			}

			chargeName = new Phrase(invoiceentity.getOtherCharges().get(i)
					.getOtherChargeName(), font10);
			String taxNames = " ";
			if (invoiceentity.getOtherCharges().get(i).getTax1()
					.getPercentage() != 0
					&& invoiceentity.getOtherCharges().get(i).getTax2()
							.getPercentage() != 0) {
				taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
						.getTaxConfigName()
						+ "/"
						+ invoiceentity.getOtherCharges().get(i).getTax2()
								.getTaxConfigName();
			} else {
				if (invoiceentity.getOtherCharges().get(i).getTax1()
						.getPercentage() != 0) {
					taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
							.getTaxConfigName();
				} else if (invoiceentity.getOtherCharges().get(i).getTax2()
						.getPercentage() != 0) {
					taxNames = invoiceentity.getOtherCharges().get(i).getTax2()
							.getTaxConfigName();
				} else {
					taxNames = " ";
				}
			}
			taxes = new Phrase(taxNames /*
										 * invoiceentity. getOtherCharges()
										 * .get(i).get()
										 */, font10);
			assVal = new Phrase(invoiceentity.getOtherCharges().get(i)
					.getAmount()
					+ "", font10);
			pCell = new PdfPCell(chargeName);
			otherCharges.addCell(pCell);
			pCell = new PdfPCell(taxes);
			otherCharges.addCell(pCell);
			pCell = new PdfPCell(assVal);
			otherCharges.addCell(pCell);

		}

		// }
		// PdfPCell left2Cell=new PdfPCell(otherCharges);
		// otherChargesTable.addCell();

		PdfPCell left22Cell = new PdfPCell(otherCharges);
		middleTable.addCell(left22Cell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);
		// rightCell.addElement();
		PdfPCell middleCell = new PdfPCell(middleTable);

		PdfPCell leftCell = new PdfPCell(leftTable);
		// leftCell.setBorder(0);
		// leftCell.addElement();

		pdfPTaxTable.addCell(leftCell);
		if (invoiceentity.getOtherCharges().size() > 0) {
			pdfPTaxTable.addCell(middleCell);
		} else {
			Phrase blankPhrase = new Phrase(" ", font10);
			middleCell = new PdfPCell(blankPhrase);
			pdfPTaxTable.addCell(middleCell);
		}
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createProductDetailsVal() {

		int firstBreakPoint = 5;
		float blankLines = 0;

		// productTable.setSpacingAfter(blankLines);

		// PdfPTable premiseTable=new PdfPTable(1);
		PdfPTable productTable = new PdfPTable(12);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column12CollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int countToBeDeducted = 0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

//			if (i == 5) {
//				break;
//			}
			if (noOfLines == 0) {
				prouductCount = i;
				break;
			}
			countToBeDeducted = countToBeDeducted + 1;
			noOfLines = noOfLines - 1;
			
			/**
			 * Date 31-01-2018 By Vijay
			 * for versatile venture zero payable amt product dont show in invoice pdf
			 */
			boolean flag = true;
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "DontShowZeroPayableAmtProduct",invoiceentity.getCompanyId())){
				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()<=0){
					flag = false;
				}
				
			}
			/**
			 * ends here
			 */
			if(flag){
				
			

			int srNoVal = i + 1;
			Phrase srNo =null;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()!=0){
		    srNo = new Phrase(srNoVal + "", font10);
			}else{
			srNo = new Phrase( "", font10);
			}
			
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(),
					font10);
//			Phrase serviceName = new Phrase("qwertyuiopasdfghjklzxcvbnmasacdsaddsadqqqeeQWERTYuiopasdfghjklzxcvbnm",
//					font10);
			if (invoiceentity.getSalesOrderProducts().get(i).getProdName()
					.trim().length() > 47) {
				noOfLines = noOfLines - 1;
			}
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()!=0){
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			}else{
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}
			
			
			
			
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font10);
			} else {
				hsnCode = new Phrase("", font10);
			}
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(hsnCodeCell);
			
			String unit = "";
			if(invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement()!=null){
				unit = invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim();
			}
			
			Phrase uom = new Phrase(unit, font10);
			PdfPCell uomCell = new PdfPCell(uom);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(uomCell);

			/** date 20.12.2018 added by komal to show area from quantity **/
			String quantity = "";
			
			
			
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				quantity="";
			}else{
			if(!invoiceentity.getSalesOrderProducts().get(i).getArea().equals("") && !invoiceentity.getSalesOrderProducts().get(i).getArea().equals("NA")){
				quantity = invoiceentity.getSalesOrderProducts().get(i).getArea();
			}else{
				quantity = invoiceentity.getSalesOrderProducts().get(i).getQuantity()+"";
			}
			}
			
			Phrase qty = new Phrase(quantity+ "", font10);
			PdfPCell qtyCell = new PdfPCell(qty);
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getPrice())
					+ "", font10);
			PdfPCell rateCell = new PdfPCell(rate);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(rateCell);

			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			totalAmount = totalAmount + amountValue;
			Phrase amount = new Phrase(df.format(amountValue) + "", font10);
			PdfPCell amountCell = new PdfPCell(amount);
			amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
//			amountCell.addElement();
//			productTable.addCell(amountCell);
           
			Phrase disc =null;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				disc= new Phrase("", font10);
			}else{
				disc = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getFlatDiscount())
						+ "", font10);
			}
			
			
		
			PdfPCell discCell = new PdfPCell(disc);

			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(discCell);
			discAmount=discAmount+invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
			
			assValAmount=assValAmount+invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
			double assValBasePaymentAmount=0;
			
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
				assValBasePaymentAmount=invoiceentity.getSalesOrderProducts().get(i).getPrice();
			}else{
				if(invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
								.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
					assValBasePaymentAmount=	invoiceentity
					.getSalesOrderProducts().get(i).getBasePaymentAmount();
				}else{
					assValBasePaymentAmount=invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();
				}
			}
			
			
			
			
			
			
				Phrase taxableValue= new Phrase(df.format(assValBasePaymentAmount)
					+ "", font10);
			
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);

			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;

			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());

			logger.log(Level.SEVERE,"VAT TAX ::::Config Name"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxConfigName()+"VAT TAx:::Tax Name"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxName()
					+"Ser TAX ::::Config Name"+invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getTaxConfigName()+"Ser TAx:::Tax Name"+invoiceentity.getSalesOrderProducts().get(i).getServiceTax().getTaxName());
			logger.log(Level.SEVERE,"VAT TAX ::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax()
					.getPercentage()+"Service Tax::::"+invoiceentity.getSalesOrderProducts().get(i).getServiceTax()
					.getPercentage());
//			boolean vatPercentZero=invoiceentity.getSalesOrderProducts().get(i).getVatTax()
//					.getPercentage() != 0;
//			boolean serPercentZero=invoiceentity.getSalesOrderProducts().get(i)
//					.getServiceTax().getPercentage() != 0;
			boolean taxPresent=validateTaxes(invoiceentity.getSalesOrderProducts().get(i));
			if (taxPresent) {
				logger.log(Level.SEVERE,"Inside Tax Applicable");
				
				if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					
					
					double taxAmount = 0;
					
					if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
						taxAmount = getTaxAmount(invoiceentity.getSalesOrderProducts().get(i).getPrice(), invoiceentity.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage());
					}else{
					taxAmount = getTaxAmount(invoiceentity
							.getSalesOrderProducts().get(i)
							.getBasePaymentAmount(), invoiceentity
							.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage());
					
					}
					
					igstTotal=igstTotal+taxAmount;
					double assValBasePaymentAmountIGST=0;
					if(invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
									.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
						assValBasePaymentAmountIGST=invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount();
					}else{
						assValBasePaymentAmountIGST=invoiceentity
								.getSalesOrderProducts().get(i).getBaseBillingAmount();
					}
					
					
					double indivTotalAmount =0;
					if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
					indivTotalAmount=taxAmount+invoiceentity.getSalesOrderProducts().get(i).getPrice();
					}else{
					indivTotalAmount = assValBasePaymentAmountIGST+ taxAmount;
					}
					
					totalAmountIncludingTax = totalAmountIncludingTax + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font10);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					Phrase igstRate = new Phrase(invoiceentity
							.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage()
							+ "", font10);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font10);
					PdfPCell cell = new PdfPCell(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font10);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font10);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(totalCell);
					String premisesVal = "";
//					for (int j = 0; j < con.getItems().size(); j++) {
//						if (invoiceentity.getSalesOrderProducts().get(i)
//								.getProdId() == con.getItems().get(j)/  
					
//								.getPrduct().getCount()) {
//							premisesVal = con.getItems().get(j)
//									.getPremisesDetails();
//						}
//
//					}
//					if (printPremiseDetails) {
//						Phrase premisesValPhrs = new Phrase(
//								"Premise Details : " + premisesVal, font8);
//						PdfPCell premiseCell = new PdfPCell();
//						premiseCell.setColspan(16);
//						premiseCell.addElement(premisesValPhrs);
//
//						productTable.addCell(premiseCell);
//					}

				}else if (invoiceentity.getSalesOrderProducts().get(i).getServiceTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					
					double taxAmount =0;
					if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
						taxAmount = getTaxAmount(invoiceentity
								.getSalesOrderProducts().get(i)
								.getPrice(), invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage());
					}else{
						taxAmount = getTaxAmount(invoiceentity
								.getSalesOrderProducts().get(i)
								.getPrice(), invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage());
					}
					
					
					
					
					
					igstTotal=igstTotal+taxAmount;
					double assValBasePaymentAmountGST=0;
					if(invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
									.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
						assValBasePaymentAmountGST=invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount();
					}else{
						assValBasePaymentAmountGST=invoiceentity
								.getSalesOrderProducts().get(i).getBaseBillingAmount();
					}
					double indivTotalAmount =0;
					
					if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
						indivTotalAmount =  taxAmount+invoiceentity.getSalesOrderProducts().get(i).getPrice();
					}else{
						indivTotalAmount = assValBasePaymentAmountGST+ taxAmount;
					}
					
					
					
					totalAmountIncludingTax = totalAmountIncludingTax + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font10);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					// igstRateValCell.setBorder(0);

					Phrase igstRate = new Phrase(invoiceentity
							.getSalesOrderProducts().get(i).getServiceTax()
							.getPercentage()
							+ "", font10);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font10);
					PdfPCell cell = new PdfPCell(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font10);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font10);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					productTable.addCell(totalCell);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					String premisesVal = "";
//					for (int j = 0; j < con.getItems().size(); j++) {
//						if (invoiceentity.getSalesOrderProducts().get(i)
//								.getProdId() == con.getItems().get(j)
//								.getPrduct().getCount()) {
//							premisesVal = con.getItems().get(j)
//									.getPremisesDetails();
//						}
//
//					}
//					if (printPremiseDetails) {
//						Phrase premisesValPhrs = new Phrase(
//								"Premise Details : " + premisesVal, font8);
//						PdfPCell premiseCell = new PdfPCell();
//						premiseCell.setColspan(16);
//						premiseCell.addElement(premisesValPhrs);
//
//						productTable.addCell(premiseCell);
//					}

				} else {

					if (invoiceentity.getSalesOrderProducts().get(i)
							.getVatTax().getTaxPrintName()
							.equalsIgnoreCase("CGST")) {
						double ctaxValue = 0;
						
						
						
						if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
						ctaxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getPrice(), invoiceentity
									.getSalesOrderProducts().get(i).getVatTax()
									.getPercentage());
						}else{
							ctaxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getBasePaymentAmount(), invoiceentity
									.getSalesOrderProducts().get(i).getVatTax()
									.getPercentage());	
						}
						
						
						
						cgstTotal=cgstTotal+ctaxValue;
						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font10);
						PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);

						Phrase cgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage()
								+ "", font10);
						PdfPCell cgstRateCell = new PdfPCell(cgstRate);
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						productTable.addCell(cgstRateCell);
//						productTable.addCell(cgstRateValCell);

						double staxValue =0;
						
						if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
							staxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getPrice(), invoiceentity
									.getSalesOrderProducts().get(i).getServiceTax()
									.getPercentage());
						}else{
							staxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getBasePaymentAmount(), invoiceentity
									.getSalesOrderProducts().get(i).getServiceTax()
									.getPercentage());
						}
						
						
						
						
						
						
						
						
						sgstTotal=sgstTotal+staxValue;
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font10);
						PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);

						
						
						
						
						
						
						
						
						Phrase sgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage()
								+ "", font10);
						PdfPCell sgstRateCell = new PdfPCell(sgstRate);
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						productTable.addCell(sgstRateCell);
//						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font10);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font10);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						productTable.addCell(igstRateValCell);
						double assValBasePaymentAmountGST=0;
						if(invoiceentity
								.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
										.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
							assValBasePaymentAmountGST=invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount();
						}else{
							assValBasePaymentAmountGST=invoiceentity
									.getSalesOrderProducts().get(i).getBaseBillingAmount();
						}
						
						double indivTotalAmount =0;
						if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
							indivTotalAmount =invoiceentity.getSalesOrderProducts().get(i).getPrice()+ ctaxValue + staxValue;
						}else{
							indivTotalAmount = assValBasePaymentAmountGST
									+ ctaxValue + staxValue;
						}
						
						
						
						
						totalAmountIncludingTax = totalAmountIncludingTax + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font10);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setColspan(16);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
//						String premisesVal = "";
//						for (int j = 0; j < con.getItems().size(); j++) {
//							if (invoiceentity.getSalesOrderProducts().get(i)
//									.getProdId() == con.getItems().get(j)
//									.getPrduct().getCount()) {
//								premisesVal = con.getItems().get(j)
//										.getPremisesDetails();
//							}
//
//						}
//						if (printPremiseDetails) {
//							Phrase premisesValPhrs = new Phrase(
//									"Premise Details : " + premisesVal, font8);
//							PdfPCell premiseCell = new PdfPCell();
//							premiseCell.setColspan(16);
//							premiseCell.addElement(premisesValPhrs);
//							productTable.addCell(premiseCell);
//						}
						// try {
						// document.add(productTable);
						// } catch (DocumentException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }

					} else if (invoiceentity.getSalesOrderProducts().get(i)
							.getVatTax().getTaxPrintName()
							.equalsIgnoreCase("SGST")) {

						
						double ctaxValue =0;
						if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
							ctaxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getPrice(), invoiceentity
									.getSalesOrderProducts().get(i).getServiceTax()
									.getPercentage());
						}else{

						ctaxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getBasePaymentAmount(), invoiceentity
									.getSalesOrderProducts().get(i).getServiceTax()
									.getPercentage());
						}
						
						
						
						
						
						
						cgstTotal=cgstTotal+ctaxValue;
						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font10);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getVatTax()
								.getPercentage()
								+ "", font10);
						PdfPCell cgstRateCell = new PdfPCell(cgstRate);
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						productTable.addCell(cgstRateCell);
//						productTable.addCell(cgstRateValCell);

						double staxValue = 0;
						
						if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
							staxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getPrice(), invoiceentity
									.getSalesOrderProducts().get(i).getVatTax()
									.getPercentage());
						}else{
							staxValue = getTaxAmount(invoiceentity
									.getSalesOrderProducts().get(i)
									.getBasePaymentAmount(), invoiceentity
									.getSalesOrderProducts().get(i).getVatTax()
									.getPercentage());
						}
						
						
						
						
						
						
					
						sgstTotal=sgstTotal+staxValue;
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font10);
						PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);

						Phrase sgstRate = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getServiceTax()
								.getPercentage()
								+ "", font10);
						PdfPCell sgstRateCell = new PdfPCell(sgstRate);
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						productTable.addCell(sgstRateCell);
//						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font10);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						productTable.addCell(igstRateVal/Cell);

						Phrase igstRate = new Phrase("-", font10);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);
						double assValBasePaymentAmountGST=0;
						if(invoiceentity
								.getSalesOrderProducts().get(i).getBasePaymentAmount()!=0&&invoiceentity
										.getSalesOrderProducts().get(i).getPaymentPercent()!=0){
							assValBasePaymentAmountGST=invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount();
						}else{
							assValBasePaymentAmountGST=invoiceentity
									.getSalesOrderProducts().get(i).getBaseBillingAmount();
						}
						
						double indivTotalAmount =0;
						if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
							indivTotalAmount =invoiceentity.getSalesOrderProducts().get(i).getPrice() + ctaxValue + staxValue;
						}else{

							indivTotalAmount = assValBasePaymentAmountGST
									+ ctaxValue + staxValue;
						}
						
						
						
						
						totalAmountIncludingTax = totalAmountIncludingTax + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font10);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setColspan(16);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);

						productTable.addCell(totalCell);
//						String premisesVal = "";
//						for (int j = 0; j < con.getItems().size(); j++) {
//							if (invoiceentity.getSalesOrderProducts().get(i)
//									.getProdId() == con.getItems().get(j)
//									.getPrduct().getCount()) {
//								premisesVal = con.getItems().get(j)
//										.getPremisesDetails();
//							}
//
//						}
//						if (printPremiseDetails) {
//							Phrase premisesValPhrs = new Phrase(
//									"Premise Details : " + premisesVal, font8);
//							PdfPCell premiseCell = new PdfPCell();
//							premiseCell.setColspan(16);
//							premiseCell.addElement(premisesValPhrs);
//							productTable.addCell(premiseCell);
//						}
					}
				}

			} else {
				logger.log(Level.SEVERE,"Inside Tax Not Applicable");

				PdfPCell cell = new PdfPCell(new Phrase("-", font10));
				productTable.addCell(cell);
//				productTable.addCell(cell);
				productTable.addCell(cell);
//				productTable.addCell(cell);
				productTable.addCell(cell);
//				productTable.addCell(cell);
				
				
				Phrase totalPhrase = null;
				if(invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()==0){
					totalPhrase = new Phrase(df.format(invoiceentity.getSalesOrderProducts().get(i).getPrice())
							+ "", font10);
				}else{
					totalPhrase = new Phrase(df.format(invoiceentity
							.getSalesOrderProducts().get(i).getBasePaymentAmount())
							+ "", font10);
				}
				
				
				
				
				assValAmount=assValAmount+invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount();
				
				PdfPCell totalCell = new PdfPCell(totalPhrase);
				
				totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// totalCell.setColspan(16);
				// totalCell.setBorder(0);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				productTable.addCell(totalCell);

//				String premisesVal = "";
//				for (int j = 0; j < con.getItems().size(); j++) {
//					if (invoiceentity.getSalesOrderProducts().get(i)
//							.getProdId() == con.getItems().get(j).getPrduct()
//							.getCount()) {
//						premisesVal = con.getItems().get(j)
//								.getPremisesDetails();
//					}
//
//				}
//				if (printPremiseDetails) {
//					Phrase premisesValPhrs = new Phrase("Premise Details : "
//							+ premisesVal, font8);
//					PdfPCell premiseCell = new PdfPCell();
//					premiseCell.setColspan(16);
//					premiseCell.addElement(premisesValPhrs);
//					productTable.addCell(premiseCell);
//				}
			}
			}
		}
		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines != 0) {
			remainingLines = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);

		if (remainingLines != 0) {
			for (int i = 0; i < remainingLines; i++) {
				System.out.println("i::::" + i);
				Phrase blankPhrase = new Phrase(" ", font10);
				PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
				blankPhraseCell.setBorder(0);
				blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
			}
		}
		PdfPCell productTableCell = null;
		if (noOfLines == 0&&prouductCount!=0) {
			Phrase my = new Phrase("Please Refer Annexure For More Details");
			productTableCell = new PdfPCell(my);

		} else {
			productTableCell = new PdfPCell(blankCell);
		}

		// PdfPCell productTableCell = new PdfPCell(blankCell);
		// productTableCell.setBorderWidthBottom(0);
		// productTableCell.setBorderWidthTop(0);
		productTableCell.setBorder(0);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		tab.addCell(productTableCell);
		// tab.addCell(premiseTblCell);
		tab.setSpacingAfter(blankLines);

		// last code for both table to be added in one table

		PdfPCell tab1;
		tab1 = new PdfPCell(productTable);
		// tab1.setBorder(0);

		PdfPCell tab2 = new PdfPCell(tab);
		// tab2.setBorder(0);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		mainTable.addCell(tab2);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean validateTaxes(
			SalesOrderProductLineItem salesOrderProductLineItem) {
		// TODO Auto-generated method stub
		if(salesOrderProductLineItem.getVatTax()
				.getPercentage() != 0){
			return true;
		}else{
			if(salesOrderProductLineItem.getServiceTax().getPercentage()!=0){
				return true;
			}else{
				return false;
			}
		}
	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount=totalAmount2/100;
		double taxAmount=percAmount*percentage;
		return taxAmount;
	}

	private void createProductDetails() {
		PdfPTable productTable=new PdfPTable(12);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column12CollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase srNophrase=new Phrase("Sr No",font10bold);
		PdfPCell srNoCell=new PdfPCell(srNophrase);
//		srNoCell.addElement(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); //1
		
		Phrase servicePhrase=new Phrase("Description of Goods",font10bold);
		PdfPCell servicePhraseCell=new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);//2
		
		Phrase hsnCode=new Phrase("HSN",font10bold);
		PdfPCell hsnCodeCell=new PdfPCell(hsnCode);
//		hsnCodeCell.addElement(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);//3
		
		Phrase UOMphrase=new Phrase("UOM",font10bold);
		PdfPCell UOMphraseCell=new PdfPCell(UOMphrase);
//		UOMphraseCell.addElement(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);//4
		
		Phrase qtyPhrase=new Phrase("Qty",font10bold);
		PdfPCell qtyPhraseCell=new PdfPCell(qtyPhrase);
//		qtyPhraseCell.addElement(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);//5
		
		Phrase ratePhrase=new Phrase("Rate",font10bold);
		PdfPCell ratePhraseCell=new PdfPCell(ratePhrase);
//		ratePhraseCell.addElement(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);//6
		
		Phrase amountPhrase=new Phrase("Amount",font10bold);
		PdfPCell amountPhraseCell=new PdfPCell(amountPhrase);
//		amountPhraseCell.addElement(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);//7
		
		Phrase dicphrase=new Phrase("Disc",font10bold);
		PdfPCell dicphraseCell=new PdfPCell(dicphrase);
//		dicphraseCell.addElement(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);//8
		
		Phrase taxValPhrase=new Phrase("Ass Val",font10bold);
		PdfPCell taxValPhraseCell=new PdfPCell(taxValPhrase);
//		taxValPhraseCell.addElement(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);//9
		
//		PdfPTable cgstcellTable=new PdfPTable(1);
//		cgstcellTable.setWidthPercentage(100);
		
		Phrase cgstphrase=new Phrase("CGST",font10bold);
		PdfPCell cgstphraseCell=new PdfPCell(cgstphrase);
//		cgstphraseCell.addElement(cgstphrase);
//		cgstphraseCell.setBorder(0);
		cgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		cgstcellTable.addCell(cgstphraseCell);
		cgstphraseCell.setColspan(1);
//		cgstphraseCell.setRowspan(2);
		
		Phrase sgstphrase=new Phrase("SGST",font10bold);
		PdfPCell sgstphraseCell=new PdfPCell(sgstphrase);
//		sgstphraseCell.setBorder(0);
		sgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		sgstphraseCell.addElement(sgstphrase);
//		sgstcellTable.addCell(sgstphraseCell);
		sgstphraseCell.setColspan(1);
//		sgstphraseCell.setRowspan(2);
		
		Phrase igstphrase=new Phrase("IGST",font10bold);
		PdfPCell igstphraseCell=new PdfPCell(igstphrase);
//		igstphraseCell.setBorder(0);
		igstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		igstphraseCell.addElement(igstphrase);
//		igstcellTable.addCell(igstphraseCell);
		igstphraseCell.setColspan(1);
//		igstphraseCell.setRowspan(2);
		
		Phrase totalPhrase=new Phrase("Total",font10bold);
		PdfPCell totalPhraseCell=new PdfPCell(totalPhrase);
//		totalPhraseCell.addElement(totalPhrase);
		totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		totalPhraseCell.setRowspan(2);//2
		
		
		Phrase cgstpercentphrase=new Phrase("%",font6bold);
		PdfPCell cgstpercentphraseCell=new PdfPCell(cgstpercentphrase);
//		cgstpercentphraseCell.setBorderWidthBottom(0);
//		cgstpercentphraseCell.setBorderWidthTop(0);
//		cgstpercentphraseCell.setBorderWidthLeft(0);
//		cgstpercentphraseCell.addElement(cgstpercentphrase);
//		innerCgstTable.addCell(cgstpercentphraseCell);
		
//		Phrase cgstamtphrase=new Phrase("Amt",font6bold);
//		PdfPCell cgstamtphraseCell=new PdfPCell();
//		cgstamtphraseCell.addElement(cgstamtphrase);
////		
		
		
		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(hsnCodeCell);
		productTable.addCell(UOMphraseCell);
		productTable.addCell(qtyPhraseCell);
		productTable.addCell(ratePhraseCell);
//		productTable.addCell(amountPhraseCell);
		productTable.addCell(dicphraseCell);
		productTable.addCell(taxValPhraseCell);
		
		productTable.addCell(cgstphraseCell);
		productTable.addCell(sgstphraseCell);
		productTable.addCell(igstphraseCell);
		
		productTable.addCell(totalPhraseCell);
		
		productTable.addCell(cgstpercentphraseCell);
//		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
//		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
//		productTable.addCell(cgstamtphraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createCustomerDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable=new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*Start Part 1*/
		PdfPTable part1Table=new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		
		Phrase colon=new Phrase(":",font10bold);
		PdfPCell colonCell=new PdfPCell(colon);
//		colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase name=new Phrase("Name",font10bold);
		PdfPCell nameCell=new PdfPCell(name);
//		nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		//  rohan added this code for considering customer printable name as well Date : 04-07-2017
		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
//		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
//		{
//			custName=cust.getCustPrintableName().trim();
//		}
//		else
//		{
//		
//			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//				custName="M/S "+"  "+cust.getCompanyName().trim();
//			}
//			/**
//			 * Date 28/11/2017
//			 * Dev.By Jayshree
//			 * Des.To add the salutation changes are made
//			 */
//			else if(cust.getSalutation()!=null)
//			{
//				custName=cust.getSalutation()+" "+cust.getFullname().trim();
//			}
//			else
//			{
//				custName=cust.getFullname().trim();
//			}
//		}//End By jayshree
//		
		String fullname= "";
//		 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
//		   	{
//		       	fullname = custName;
//		   	}
//		       else
//		       {
//		       	fullname =custName;
//		       }
//		//   ends here 
		if(vendor.getVendorName() != null && !vendor.getVendorName().equals(""))
		{
			fullname  = vendor.getVendorName();
		}
		
		Phrase nameCellVal=new Phrase(fullname,font10);
		PdfPCell nameCellValCell=new PdfPCell(nameCellVal);
//		nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase address=new Phrase("Address",font13bold);//Date 9/12/2017 By Jayshree To increse the size
		PdfPCell addressCell=new PdfPCell(address);
//		addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
	
		String adrsValString = vendor.getPrimaryAddress().getCompleteAddress().trim();
		Phrase addressVal=new Phrase(adrsValString,font13);//Date 9/12/2017 By Jayshree To increse the size
		PdfPCell addressValCell=new PdfPCell(addressVal);
//		addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase attn = new Phrase("Attn", font10bold);
		PdfPCell attnCell = new PdfPCell(attn);
		// gstTinCell.addElement(gstTin);
		attnCell.setBorder(0);
		attnCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attnVal = new Phrase(invoiceentity.getPersonInfo().getPocName(), font10);
		PdfPCell attnValCell = new PdfPCell(attnVal);
		// gstTinValCell.addElement(gstTinVal);
		attnValCell.setBorder(0);
		attnValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if(printAttnInPdf){
			colonTable.addCell(attnCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attnValCell);
		}
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);
		/**
		 * Date 13/12/2017 By Jayshree 
		 * Comment this to remove
		 */
//		Phrase state = new Phrase("State", font10bold);
//		PdfPCell stateCell = new PdfPCell(state);
//		stateCell.setBorder(0);
//		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stateVal = new Phrase(cust.getAdress().getState().trim(), font10);
//		PdfPCell stateValCell = new PdfPCell(stateVal);
//		// stateValCell.addElement(stateVal);
//		stateValCell.setBorder(0);
//		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		String stCo = "";
//		for (int i = 0; i < stateList.size(); i++) {
//			if (stateList.get(i).getStateName().trim()
//					.equalsIgnoreCase(cust.getAdress().getState().trim())) {
//				stCo = stateList.get(i).getStateCode().trim();
//				break;
//			}
//		}
//
//		Phrase stateCode = new Phrase("State Code", font10bold);
//		PdfPCell stateCodeCell = new PdfPCell(stateCode);
//		stateCodeCell.setBorder(0);
//		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stateCodeVal = new Phrase(stCo, font10);
//		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
//		stateCodeValCell.setBorder(0);
//		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable colonTableState = new PdfPTable(3);
		colonTableState.setWidthPercentage(100);
		try {
			colonTableState.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * Date 13/12/2017
		 * Comment by jayshree
		 * to Remove state
		 */
//		colonTableState.addCell(stateCell);
//		colonTableState.addCell(colonCell);
		PdfPTable pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		pdfStateTable.addCell(stateValCell);
//		pdfStateTable.addCell(stateCodeCell);
//		pdfStateTable.addCell(colonCell);
//		pdfStateTable.addCell(stateCodeValCell);

		PdfPCell state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTableState.addCell(state4Cell);
		
		PdfPCell cell1=new PdfPCell(colonTable);
		cell1.setBorder(0);
		
		PdfPCell colonTableValState=new PdfPCell(colonTableState);
		colonTableValState.setBorder(0);
		part1Table.addCell(cell1);
		/**
		 * Date 13/12/2017
		 * By Jayshree
		 * Comment This
		 */
//		part1Table.addCell(colonTableValState);

		//End By Jayshree
		Phrase emailTin=new Phrase("Email",font10bold);
		PdfPCell emailTinCell=new PdfPCell(emailTin);
		emailTinCell.setBorder(0);
		emailTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emailVal;
		if(vendor.getEmail()!=null){
			emailVal=new Phrase(vendor.getEmail().trim(),font10);
		}else{
			emailVal=new Phrase("",font10);
		}
		PdfPCell emailValCell=new PdfPCell(emailVal);
		emailValCell.setBorder(0);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobile=new Phrase("Mobile",font10bold);
		PdfPCell mobileCell=new PdfPCell(mobile);
		mobileCell.setBorder(0);
		mobileCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase mobileVal;
		if(vendor.getCellNumber1()!=0){
			mobileVal=new Phrase(vendor.getCellNumber1()+"",font10);
		}else{
			mobileVal=new Phrase("",font10);
		}
		PdfPCell mobileValCell=new PdfPCell(mobileVal);
		mobileValCell.setBorder(0);
		mobileValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String gstTinStr="";
		for (int j = 0; j < vendor.getArticleTypeDetails().size(); j++) {
			if(vendor.getArticleTypeDetails().get(j).getArticleTypeName().equalsIgnoreCase("GSTIN")){
				gstTinStr=getGSTNOfVendor(vendor,"SalesInvoice");//Date 1/2/2018 by jayshree add parameter
				if(gstTinStr!=null && !gstTinStr.equalsIgnoreCase("")){
				}else{
					gstTinStr=getGSTNOfVendor(vendor, "Invoice Details");//Date 1/2/2018 by jayshree add parameter
				}
				
			}
			
		}//
		if(gstTinStr == null || gstTinStr.equals("")){
			if(invoiceentity.getGstinNumber() != null){
				gstTinStr = invoiceentity.getGstinNumber();
			}
		}
		
		Phrase gstTin=new Phrase("GSTIN",font10bold);
		PdfPCell gstTinCell=new PdfPCell(gstTin);
		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase gstTinVal=new Phrase(gstTinStr,font10);
		PdfPCell gstTinValCell=new PdfPCell(gstTinVal);
		gstTinValCell.setBorder(0);
		gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * Date 13/12/2017
		 * By Jayshree 
		 * added this to add the state and state code 
		 */
		
		PdfPTable gstTable=new PdfPTable(6);
		gstTable.setWidthPercentage(100);
		
		try {
			gstTable.setWidths(new float[]{1.9f, 0.2f,3.7f,2.0f,0.2f,2.0f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(vendor.getPrimaryAddress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}
		
		

		Phrase stateCode = new Phrase("State Code", font10bold);//Date 9/12/2017 By Jayshree To increse the size
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(stCo, font10);//Date 9/12/2017 By Jayshree To increse the size
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable gstTable2=new PdfPTable(3);
		gstTable2.setWidthPercentage(100);
		
		try {
			gstTable2.setWidths(columnCollonWidth);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		gstTable2.addCell(emailTinCell);
		gstTable2.addCell(colonCell);
		gstTable2.addCell(emailValCell);

		gstTable2.addCell(mobileCell);
		gstTable2.addCell(colonCell);
		gstTable2.addCell(mobileValCell);
		
		gstTable.addCell(gstTinCell);
		gstTable.addCell(colonCell);
		gstTable.addCell(gstTinValCell); 
		gstTable.addCell(stateCodeCell);
		gstTable.addCell(colonCell);
		gstTable.addCell(stateCodeValCell);
		
		PdfPCell gstCell=new PdfPCell(gstTable2);
		gstCell.setBorder(0);
		part1Table.addCell(gstCell);
		
		PdfPCell gstCell2=new PdfPCell(gstTable);
		gstCell2.setBorder(0);
		part1Table.addCell(gstCell2);
		//End By Jayshree
		
		PdfPCell part1TableCell=new PdfPCell(part1Table);
		
		
		/*Ends Part 1*/
		
		/*Part 2 Start*/
		PdfPTable part2Table=new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		String warehouseName = "";
//		if(purchaseOrder.getProductDetails().get(0).getWarehouseName()!=null){
//			warehouseName = purchaseOrder.getProductDetails().get(0).getWarehouseName();
//		}
		
		Phrase name2=new Phrase("Name",font10bold);
		PdfPCell name2Cell=new PdfPCell();
		name2Cell.addElement(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase name2CellVal=new Phrase(companyNameValue,font10);//+"       Mob : "+cust.getCellNumber1()
		PdfPCell name2CellValCell=new PdfPCell(name2CellVal);
//		name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase address2=new Phrase("Address",font13bold);//Date 9/12/2017 By Jayshree To increse the size
		PdfPCell address2Cell=new PdfPCell(address2);
//		address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
	
		
		///////////////////////////  Ajinkya added branch code for customer Branch On Date : 15/07/2017  //////////////////////////////
		String adrsValString1 = "";
		String customerPoc=null;
		String shippingAddress = "", city = "";
		if(grn !=null){
			if(grn.getInventoryProductItem().size()>0){
				if(purchaseOrder.isDeliverAdd()){
					shippingAddress = purchaseOrder.getAdress().getCompleteAddress();
				}else{
					shippingAddress = grn.getInventoryProductItem().get(0).getWarehouseAddress();
				}
			}
		}
		////////////////////////////////
		//Date 10/11/2017
		//By Jayshree
		if(customerbranchlist.size()!=0)
		{
			System.out.println("Inside Customer branch  "+customerbranchlist.size());
			for(int i=0; i<customerbranchlist.size();i++)
			{
				
				adrsValString1 = customerbranchlist.get(i).getAddress().getCompleteAddress() ;
				customerPoc=customerbranchlist.get(i).getPocName();
			}
			
		}
		else if(shippingAddress != null){
			adrsValString1 = shippingAddress;
			customerPoc=invoiceentity.getPersonInfo().getPocName();
		}
		else if(!cust.getSecondaryAdress().getAddrLine1().equals("")&&customerbranchlist.size()==0)
		{
	    	 System.out.println("Inside service Addrss");
			adrsValString1 = cust.getSecondaryAdress().getCompleteAddress().trim();
			customerPoc=invoiceentity.getPersonInfo().getPocName();
		}
		
		
		///////////////////////////////
//		
//		/////////////////////////////////////////////// branch Code   ////////////////////////////////////////
		
		
		Phrase address2Val=new Phrase(comp.getAddress().getCompleteAddress(),font13);//Date 9/12/2017 By Jayshree To increse the size
		PdfPCell address2ValCell=new PdfPCell(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		Phrase state2 = new Phrase("State", font10bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2Val = new Phrase(comp.getAddress().getState().trim(), font10);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// stateValCell.addElement(stateVal);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String st2Co = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(comp.getAddress().getState().trim())) {
				st2Co = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font10bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(st2Co, font10);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTableState = new PdfPTable(3);
		colonTableState.setWidthPercentage(100);
		try {
			colonTableState.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * Date 13/12/2017
		 * By Jayshree Comment This to Remove state code
		 */
//		colonTableState.addCell(stateCell);
//		colonTableState.addCell(colonCell);
		//end
		pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//By jayshree comment this date 13/12/2017
//		pdfStateTable.addCell(state2ValCell);
//		pdfStateTable.addCell(state2CodeCell);
		//End
		
		pdfStateTable.addCell(colonCell);
		pdfStateTable.addCell(state2CodeValCell);

		state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTableState.addCell(state4Cell);
		
		PdfPCell cell12=new PdfPCell(colonTable);
		cell12.setBorder(0);
//		
		colonTableValState=new PdfPCell(colonTableState);
		colonTableValState.setBorder(0);
		/**
		 * Date 13/12/2017
		 * By Jayshree Comment This to Remove state code
		 */
//		part2Table.addCell(cell1);
//		part2Table.addCell(colonTableValState);
//		
//		PdfPCell stateTableValCell=new PdfPCell();
		
		
		//End By Jayshree
		
		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase attn2 = new Phrase("Attn", font10bold);
		PdfPCell attn2Cell = new PdfPCell(attn2);
		attn2Cell.setBorder(0);
		attn2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		Phrase attn2Val=null;
		if(custName!=null){
			attn2Val = new Phrase(customerPoc, font10);
		}else{
			attn2Val = new Phrase("", font10);
		}
		PdfPCell attn2ValCell = new PdfPCell(attn2Val);
		attn2ValCell.setBorder(0);
		attn2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
		
		PdfPCell cell2=new PdfPCell();
		cell2.setBorder(0);
		cell2.addElement(colonTable);

		colonTable.addCell(name2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(name2CellValCell);
		if(printAttnInPdf){
			colonTable.addCell(attn2Cell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attn2ValCell);
		}
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);
		/**
		 * Date 13/12/2017
		 * By Jayshree
		 * Des.To remove the state comment this
		 */
//		colonTable.addCell(state2Cell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(state2ValCell);
		Phrase emailVal1;
		if(comp.getEmail()!=null){
			emailVal1=new Phrase(comp.getEmail().trim(),font10);
		}else{
			emailVal1=new Phrase("",font10);
		}
		PdfPCell emailValCell1=new PdfPCell(emailVal1);
		emailValCell1.setBorder(0);
		emailValCell1.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		Phrase mobileVal1;
		if(comp.getCellNumber1()!=0){
			mobileVal1=new Phrase(comp.getCellNumber1()+"",font10);
		}else{
			mobileVal1=new Phrase("",font10);
		}
		PdfPCell mobileValCell1=new PdfPCell(mobileVal1);
		mobileValCell1.setBorder(0);
		mobileValCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String cinval=null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("CIN")) {

				cinval=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
		}
		Phrase cinph = new Phrase("CIN", font10bold);
		PdfPCell cincell = new PdfPCell(cinph);
		cincell.setBorder(0);
		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase cinValuePh ;
		if(!gstinValue.equals("")){
			cinValuePh=new Phrase(cinval, font10 );
		}else{
			cinValuePh = new Phrase("" , font10);
		}
		
		PdfPCell cinValueCell=new PdfPCell(cinValuePh);
		cinValueCell.setBorder(0);
		cinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase gstTin1=new Phrase("GSTIN/UIN",font10bold);
		PdfPCell gstTinCell1=new PdfPCell(gstTin1);
		gstTinCell1.setBorder(0);
		gstTinCell1.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstinVal ;
		if(!gstinValue.equals("")){
			gstinVal=new Phrase(gstinValue, font10 );
		}else{
			gstinVal = new Phrase("" , font10);
		}
		PdfPCell gstinValueCell=new PdfPCell(gstinVal);
		gstinValueCell.setBorder(0);
		gstinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		//End By Jayshree
		colonTable.addCell(emailTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(emailValCell1);
		colonTable.addCell(mobileCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(mobileValCell1);
		colonTable.addCell(gstTinCell1);
		colonTable.addCell(colonCell);
		colonTable.addCell(gstinValueCell);

		colonTable.addCell(cincell);
		colonTable.addCell(colonCell);
		colonTable.addCell(cinValueCell);
		
		
		
		PdfPCell pdfPCellColonTable=new PdfPCell(colonTable);
		part2Table.addCell(pdfPCellColonTable);
		PdfPCell part2TableCell=new PdfPCell(part2Table);
		/*Part 2 Ends*/
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createInvoiceDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable=new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable part1Table=new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = {2f,0.2f,2.8f,2f,0.2f,2.8f};
		PdfPTable colonTable=new PdfPTable(6);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon=new Phrase(":",font10bold);
		PdfPCell colonCell=new PdfPCell(colon);
//		colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase reverseCharge=new Phrase("Reverse Charge(Y/N)",font10bold);
		PdfPCell reverseChargeCell=new PdfPCell(reverseCharge);
//		reverseChargeCell.addElement(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase reverseChargeVal=new Phrase("No ",font10);
		PdfPCell reverseChargeValCell=new PdfPCell(reverseChargeVal);
//		reverseChargeValCell.addElement(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setColspan(4);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceNo=new Phrase("Invoice Id",font10bold);
		PdfPCell invoiceNoCell=new PdfPCell(invoiceNo);
//		invoiceNoCell.addElement(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceNoVal=new Phrase(invoiceentity.getCount()+"",font10);
		PdfPCell invoiceNoValCell=new PdfPCell(invoiceNoVal);
//		invoiceNoValCell.addElement(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceDate=new Phrase("Invoice Date",font10bold);
		PdfPCell invoiceDateCell=new PdfPCell(invoiceDate);
//		invoiceDateCell.addElement(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceDateVal=new Phrase(sdf.format(invoiceentity.getInvoiceDate()),font10);
		PdfPCell invoiceDateValCell=new PdfPCell(invoiceDateVal);
//		invoiceDateValCell.addElement(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * Date 18/1/2018	
		 * By jayshree
		 * To Add the ref no and 	
		 *  
		 */

		Phrase refnoph = new Phrase("Ref/GRN No", font10bold);
		PdfPCell refnoCell = new PdfPCell(refnoph);
		refnoCell.setBorder(0);
		refnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase refnoValph=null;
		if(invoiceentity.getRefNumber()!=null){
		 refnoValph = new Phrase(invoiceentity.getRefNumber() + "", font10);
		}
		else
		{
		refnoValph=new Phrase( "", font10);
		}
		PdfPCell refnoValCell = new PdfPCell(refnoValph);
		refnoValCell.setBorder(0);
		
		if(orderNoAndGrnNO){
			refnoValCell.setColspan(0);
		}else{
			refnoValCell.setColspan(4);	
		}
		refnoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
//		Phrase refDateph = new Phrase("Ref/Po Date", font10bold);
//		PdfPCell refDateCell = new PdfPCell(refDateph);
//		refDateCell.setBorder(0);
//		refDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase refdateValph=null;
//		if(salesoder.getReferenceDate()!=null)
//		{
//		 refdateValph = new Phrase(sdf.format(invoiceentity.get)+ "", font10);
//		}
//		else{
//			refdateValph = new Phrase( "", font10);
//		}
//		
//		PdfPCell refdateValCell = new PdfPCell(refdateValph);
//		refdateValCell.setBorder(0);
//		refdateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase oderId=new Phrase("Order Id",font10bold);
		PdfPCell oderIdCell=new PdfPCell(oderId);
//		invoiceDateCell.addElement(invoiceDate);
		oderIdCell.setBorder(0);
		oderIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase oderIdValue=new Phrase(purchaseOrder.getCount()+"",font10);
		PdfPCell oderIdValueValue=new PdfPCell(oderIdValue);
//		invoiceDateValCell.addElement(invoiceDateVal);
		oderIdValueValue.setBorder(0);
		oderIdValueValue.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		if(orderNoAndGrnNO){
			oderIdValueValue.setColspan(0);
		}else{
			oderIdValueValue.setColspan(4);	
		}
		
		Phrase oderNo=new Phrase("Order No",font10bold);
		PdfPCell oderNoCell=new PdfPCell(oderNo);
//		invoiceDateCell.addElement(invoiceDate);
		oderNoCell.setBorder(0);
		oderNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String orderNo="";
		if(purchaseOrder.getRefOrderNO()!=null){
			orderNo=purchaseOrder.getRefOrderNO();
		}else{
			orderNo="";
		}
		
		Phrase oderNoValue=new Phrase(orderNo,font10);
		PdfPCell oderNoValueValue=new PdfPCell(oderNoValue);
//		invoiceDateValCell.addElement(invoiceDateVal);
		oderNoValueValue.setBorder(0);
		oderNoValueValue.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase receiptNo=new Phrase("Receipt No",font10bold);
		PdfPCell receiptNoCell=new PdfPCell(receiptNo);
//		invoiceDateCell.addElement(invoiceDate);
		receiptNoCell.setBorder(0);
		receiptNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String receiptNumber="";
        if(grn.getRefNo()!=null){
        	receiptNumber=grn.getRefNo();
		}else{
			receiptNumber="";
		}
		Phrase receiptNoValue=new Phrase(receiptNumber,font10);
		PdfPCell receiptNoValueCell=new PdfPCell(receiptNoValue);
//		invoiceDateValCell.addElement(invoiceDateVal);
		receiptNoValueCell.setBorder(0);
		receiptNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
//		colonTable.addCell(reverseChargeCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(reverseChargeValCell);
		
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceNoValCell);
		
		colonTable.addCell(invoiceDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceDateValCell);
		
		//Add By jayshree
//		colonTable.addCell(refnoCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(refnoValCell);
//		colonTable.addCell(refDateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(refdateValCell);

		colonTable.addCell(oderIdCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(oderIdValueValue);
		
		if(orderNoAndGrnNO){
		colonTable.addCell(oderNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(oderNoValueValue);
		}
		
		colonTable.addCell(refnoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(refnoValCell);

		if(orderNoAndGrnNO){
		colonTable.addCell(receiptNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(receiptNoValueCell);
		}
		

		PdfPCell pdfCell=new PdfPCell();
		pdfCell.setBorder(0);
		pdfCell.addElement(colonTable);
		
		part1Table.addCell(pdfCell);
		
//		Phrase state=new Phrase("State",font10bold);
//		PdfPCell stateCell=new PdfPCell(state);
////		stateCell.addElement(state);
//		stateCell.setBorder(0);
//		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase stateVal=new Phrase(comp.getAddress().getState().trim(),font10);
//		PdfPCell stateValCell=new PdfPCell(stateVal);
////		stateValCell.addElement(stateVal);
//		stateValCell.setBorder(0);
//		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase stateCode=new Phrase("State Code",font10bold);
//		PdfPCell stateCodeCell=new PdfPCell(stateCode);
////		stateCodeCell.addElement(stateCode);
//		stateCodeCell.setBorder(0);
//		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase stateCodeVal=new Phrase(" ",font10);
//		PdfPCell stateCodeValCell=new PdfPCell();
//		stateCodeValCell.addElement(stateCodeVal);
//		stateCodeValCell.setBorder(0);
//		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		PdfPTable statetable=new PdfPTable(2);
//		statetable.setWidthPercentage(100);
//		
//		colonTable=new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnCollonWidth);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		colonTable.addCell(stateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateValCell);
//		statetable.addCell(colonTable);
//		
////		float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
//		colonTable=new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnCollonWidth);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		colonTable.addCell(stateCodeCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateCodeValCell);
//		statetable=new PdfPTable(2);
//		statetable.setWidthPercentage(100);
//		statetable.addCell(colonTable);
//		
//		PdfPCell stateTableCell=new PdfPCell();
//		stateTableCell.setBorder(0);
//		stateTableCell.addElement(statetable);
//		part1Table.addCell(stateTableCell);
//		
		PdfPTable part2Table=new PdfPTable(1);
		part2Table.setWidthPercentage(100);
		
		Phrase transportmode=new Phrase("Invoice Number",font10bold);
		PdfPCell transportmodeCell=new PdfPCell(transportmode);
		transportmodeCell.setBorder(0);
		transportmodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String transMode="";
		if(invoiceentity.getSegment()!=null){
			transMode=invoiceentity.getSegment();
		}
		Phrase transModeVal=new Phrase(transMode,font10);
		PdfPCell transModeValCell=new PdfPCell(transModeVal);
		transModeValCell.setBorder(0);
		transModeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase vechicleNo=new Phrase("Vendor Invoice No.",font10bold);//old label "Invoice Ref. No."
		PdfPCell vechicleNoCell=new PdfPCell(vechicleNo);
		vechicleNoCell.setBorder(0);
		vechicleNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String vechicalnumber="";
		if(invoiceentity.getInvRefNumber()!=null){
			vechicalnumber=invoiceentity.getInvRefNumber();
		}
		
		Phrase vechicleNoValue=new Phrase(vechicalnumber,font10);
		PdfPCell vechicleNoValueCell=new PdfPCell(vechicleNoValue);
		vechicleNoValueCell.setBorder(0);
		vechicleNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		//  delivery date
		
		Phrase deliveryDate=new Phrase("GRN Date",font10bold);
		PdfPCell deliveryDateCell=new PdfPCell(deliveryDate);
		deliveryDateCell.setBorder(0);
		deliveryDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		Phrase deliveryDateValue=new Phrase(sdf.format(grn.getCreationDate()),font10);
		PdfPCell deliveryDateValueCell=new PdfPCell(deliveryDateValue);
		deliveryDateValueCell.setBorder(0);
		deliveryDateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		//   place of supply 
		
		Phrase placeOfSupply=new Phrase("Place Of Supply",font10bold);
		PdfPCell placeOfSupplyCell=new PdfPCell(placeOfSupply);
		placeOfSupplyCell.setBorder(0);
		placeOfSupplyCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		
		
		Phrase placeOfSupplyValue=new Phrase("",font10);
		PdfPCell placeOfSupplyValueCell=new PdfPCell(placeOfSupplyValue);
		placeOfSupplyValueCell.setBorder(0);
		placeOfSupplyValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);


		
		PdfPTable periodtable=new PdfPTable(2);
		periodtable.setWidthPercentage(100);
		float[] columnrohanrrCollonWidth = {3.2f,0.2f,7f};
		PdfPTable concolonTable=new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Ashwini Patil Date:8-12-2022
//		concolonTable.addCell(transportmodeCell);
//		concolonTable.addCell(colonCell);
//		concolonTable.addCell(transModeValCell);
		
		concolonTable.addCell(vechicleNoCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(vechicleNoValueCell);
		
		concolonTable.addCell(deliveryDateCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(deliveryDateValueCell);
		
//		concolonTable.addCell(placeOfSupplyCell);
//		concolonTable.addCell(colonCell);
//		concolonTable.addCell(placeOfSupplyValueCell);
//		
		
		
		
		
		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		colonTable.addCell(startDateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(startDateValCell);
		
		PdfPCell startcolonTableCell=new PdfPCell(colonTable);
		startcolonTableCell.setBorder(0);
//		startcolonTableCell.addElement(colonTable);
		periodtable.addCell(startcolonTableCell);
		
		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		colonTable.addCell(endDateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(endDateValCell);
		
		PdfPCell endcolonTableCell=new PdfPCell();
		endcolonTableCell.setBorder(0);
		endcolonTableCell.addElement(colonTable);
		periodtable.addCell(endcolonTableCell);
		
		PdfPCell periodTableCell=new PdfPCell();
		periodTableCell.setBorder(0);
		periodTableCell.addElement(periodtable);
		
		PdfPCell concolonTableCell=new PdfPCell();
		concolonTableCell.setBorder(0);
		concolonTableCell.addElement(concolonTable);
		
		
		
		part2Table.addCell(concolonTableCell);
		part2Table.addCell(periodTableCell);
		
		PdfPCell part1Cell=new PdfPCell();
		part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part1Table);
		
		mainTable.addCell(part1Cell);
		
		part1Cell=new PdfPCell();
//		part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part2Table);
		mainTable.addCell(part1Cell);
		
//		mainTable.addCell(blankCell);
//		mainTable.addCell(blankCell);
		Phrase billingAddress=new Phrase("Bill from Party",font8bold);
//		Paragraph billingpara=new Paragraph();
//		billingpara.add(billingAddress);
//		billingpara.setAlignment(Element.ALIGN_CENTER);
//		billingpara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell billAdressCell=new PdfPCell(billingAddress);
//		billAdressCell.addElement(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		
		
		mainTable.addCell(billAdressCell);
		Phrase serviceaddress=new Phrase("Party",font8bold);
//		Paragraph servicepara=new Paragraph();
//		servicepara.add(serviceaddress);
//		servicepara.setAlignment(Element.ALIGN_CENTER);
//		servicepara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell serviceCell=new PdfPCell(serviceaddress);
//		serviceCell.addElement(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);		
		mainTable.addCell(serviceCell);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createProductDescription()
	{
	 for (int i = 0 ;i <invoiceentity.getSalesOrderProducts().size();i++ )
 {
			PdfPTable prodDescriptionTbl = new PdfPTable(2);

			prodDescriptionTbl.setWidthPercentage(100);

			try {
				prodDescriptionTbl.setWidths(new float[] { 20, 80 });
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Phrase prodNameLbl = new Phrase("Product Name :", font12bold);
			PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
			prodlblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodlblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodlblCell.setBorder(0);

			prodDescriptionTbl.addCell(prodlblCell);

			String prodNameValue = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();
			Phrase prodNameVal = new Phrase("" + prodNameValue, font12);
			PdfPCell prodNameValCell = new PdfPCell(prodNameVal);
			prodNameValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodNameValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodNameValCell.setBorder(0);

			prodDescriptionTbl.addCell(prodNameValCell);

			String prodDescriptionValue = "";

			if (!invoiceentity.getSalesOrderProducts().get(i).getProdDesc1()
					.equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() != null) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc1()
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getProdDesc2();
			} else if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1() == null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() != null) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc2();
			} else if (!invoiceentity.getSalesOrderProducts().get(i)
					.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1() != null
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() == null) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc1();
			} else {
				prodDescriptionValue = "";

			}
			// Phrase prodDescriptionVal = new
			// Phrase(""+prodDescriptionValue,font8);
			// PdfPCell prodDescriptionValueCell = new
			// PdfPCell(prodDescriptionVal);
			// prodDescriptionValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// prodDescriptionValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// prodDescriptionValueCell.setBorder(0);
			// prodDescriptionTbl.addCell(prodDescriptionValueCell);
			//
			// Phrase blnk = new Phrase("",font8);
			// PdfPCell blnkCell = new PdfPCell(blnk);
			// blnkCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// blnkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// blnkCell.setBorder(0);

			// prodDescriptionTbl.addCell(blnkCell);

			Paragraph value = new Paragraph(prodDescriptionValue);
			value.setAlignment(Element.ALIGN_LEFT);

			try {
				document.add(Chunk.NEXTPAGE);
				document.add(prodDescriptionTbl);
				document.add(value);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	
//	private void createStaticHeader(String preprintStatus) {
//		// TODO Auto-generated method stub
//		PdfPTable mainTable=new PdfPTable(2);
//		mainTable.setWidthPercentage(100);
//		try {
//			mainTable.setWidths(columnMoreLeftHeaderWidths);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		//   rohan added this code for printing INVOICE heading when process config is off
//		Phrase pdfHeading=null;
//		if(preprintStatus.equalsIgnoreCase("Plane")){
//			if(invoiceentity.getInvoiceType().equalsIgnoreCase("Proforma Invoice")){
//				pdfHeading=new Phrase("Proform Invoice",font14bold);
//			}
//			else{
//				pdfHeading=new Phrase("Tax Invoice",font14bold);
//			}
//			 
//		}
//		else{
//			 pdfHeading=new Phrase(" ",font14bold);	
//		}
//		
//		Paragraph invPara=new Paragraph();
//		invPara.add(pdfHeading);
//		invPara.setAlignment(Element.ALIGN_RIGHT);
//		PdfPCell pdfHeadingCell=new PdfPCell();
//		pdfHeadingCell.addElement(invPara);
//		pdfHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		mainTable.addCell(pdfHeadingCell);
//		
//		PdfPTable partialTable=new PdfPTable(2);
//		partialTable.setWidthPercentage(100);
//		try {
//			partialTable.setWidths(columnMoreRightCheckBoxWidths);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		Phrase blankPhrase=new Phrase(" ",font10);
//		PdfPCell blankCell=new PdfPCell(blankPhrase);
//		
////		blankCell.addElement(blankPhrase);
////		blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		/*Just to create little spacing between boxes*/
////		PdfPTable pdfTable=new PdfPTable(1);
////		pdfTable.addCell(blankCell);
////		pdfTable.setWidthPercentage(100);
////		
////		PdfPCell blank2Cell=new PdfPCell();
////		blank2Cell.addElement(pdfTable);
////		blank2Cell.setBorder(0);
////		blank2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		Phrase stat1Phrase=new Phrase("Original for Receipient",font10);
//		PdfPCell stat1PhraseCell=new PdfPCell(stat1Phrase);
////		stat1PhraseCell.addElement(stat1Phrase);
//		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase stat2Phrase=new Phrase("Duplicate for Supplier/Transporter",font10);
//		PdfPCell stat2PhraseCell=new PdfPCell(stat2Phrase);
////		stat2PhraseCell.addElement(stat2Phrase);
//		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	
//		Phrase stat3Phrase=new Phrase("Triplicate for Supplier",font10);
//		PdfPCell stat3PhraseCell=new PdfPCell(stat3Phrase);
////		stat3PhraseCell.addElement(stat3Phrase);
//		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		partialTable.addCell(blankCell);
//		partialTable.addCell(stat1PhraseCell);
//		partialTable.addCell(blankCell);
//		partialTable.addCell(stat2PhraseCell);
//		partialTable.addCell(blankCell);
//		partialTable.addCell(stat3PhraseCell);
//		
//		PdfPCell pdfPCell=new PdfPCell();
//		pdfPCell.addElement(partialTable);
////		pdfPCell.setBorder(0);
//		pdfPCell.setBorderWidthLeft(0);
//		pdfPCell.setBorderWidthBottom(0);
//		pdfPCell.setBorderWidthTop(0);
//		
//		mainTable.addCell(pdfPCell);
//		try {
//			document.add(mainTable);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}

	private void createHeader(int value) 
	{
		//Date 28/11/2017 By Jayshree
		//to add the logo in Table
		DocumentUpload logodocument =comp.getLogo();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(30);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		Image image1=null;
//		try
//		{
//		image1=Image.getInstance(" ");//images/ipclogo4.jpg
//		image1.scalePercent(20f);
////		image1.setAbsolutePosition(40f,765f);	
////		doc.add(image1);
//		
//		
//		
//		
//		
//		imageSignCell=new PdfPCell();
//		imageSignCell.addElement(image1);
//		imageSignCell.setFixedHeight(30);
//		imageSignCell.setBorder(0);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		

		PdfPTable logoTab=new PdfPTable(1);
		logoTab.setWidthPercentage(100);
		
		if(imageSignCell!=null)
		{
			logoTab.addCell(imageSignCell);
		}
		else
		{
			Phrase logoblank=new Phrase(" ");
			PdfPCell logoblankcell=new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
//End By jayshree
		
		Phrase companyName = null;
		if(comp!=null){
			companyName=new Phrase(companyNameValue,font16bold);
		}
		
		Paragraph companyNamepara=new Paragraph();
		companyNamepara.add(companyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell companyNameCell=new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase companyAddr = null;
		if(comp!=null){
			companyAddr=new Phrase(comp.getAddress().getCompleteAddress().trim(),font12);
		}
		Paragraph companyAddrpara=new Paragraph();
		companyAddrpara.add(companyAddr);
		companyAddrpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell companyAddrCell=new PdfPCell();
		companyAddrCell.addElement(companyAddrpara);
		companyAddrCell.setBorder(0);
		companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase companyGSTTIN = null;
		//String gstinValue="";
			
//			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//				if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
//					gstinValue=comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
//					break;
//				}
//			}
		/** date 20.12.2018 added by komal to take gstn number from branch **/
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			gstinValue = serverApp.getGSTINOfCompany(comp, invoiceentity.getBranch().trim());
			System.out.println("gstin" + gstinValue);

		} else {
			gstinValue = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinValue);
		}

			/**
			 * Date 14/12/2017
			 * By Jayshree
			 * Des.To remove the extra article info comment this code
			 */
			
//			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//				if(!comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
//					gstinValue=gstinValue+","+comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
//				}
//			}
		
		//End By Jayshree
		if(!gstinValue.equals("")){
			companyGSTTIN=new Phrase(gstinValue,font12bold);
		}

		Paragraph companyGSTTINpara=new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell companyGSTTINCell=new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPTable pdfPTable=new PdfPTable(1);
		pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		pdfPTable.addCell(companyGSTTINCell);
		
		

		/**
		 * Date 28/11/2017
		 * Dev.Jayshree
		 * Des.To set the logo at proper position changes are made
		 */
		
		PdfPTable header=new PdfPTable(2);
		header.setWidthPercentage(100);
		
		try {
			header.setWidths(new float[]{20,80});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		if(imageSignCell!=null){
			System.out.println("Image present");
		PdfPCell left=new PdfPCell(logoTab);
		left.setBorder(0);
		header.addCell(left);
		
		PdfPCell right=new PdfPCell(pdfPTable);
		right.setBorder(0);
		header.addCell(right);
		}
		else
		{
			System.out.println("image not present");
			PdfPCell right=new PdfPCell(pdfPTable);
			right.setBorder(0);
			right.setColspan(2);
			header.addCell(right);
		}
		
		
		try {
			document.add(header);//
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//End By Jayshree
		//  rohan added this code 
		float[] myWidth = {1,3,20,17,3,30,17,3,20,1};
		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setWidthPercentage(100f);
		mytbale.setSpacingAfter(5f);
		mytbale.setSpacingBefore(5f);
		
	
		try {
			mytbale.setWidths(myWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Image uncheckedImg=null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(9);

		
		Image checkedImg=null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(9);

		
//		
//		Phrase myblank=new Phrase(" ",font10);
//		PdfPCell myblankCell=new PdfPCell(myblank);
////		stat1PhraseCell.addElement(stat1Phrase);
//		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase myblankborderZero=new Phrase(" ",font10);
//		PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
////		stat1PhraseCell.addElement(stat1Phrase);
//		myblankborderZeroCell.setBorder(0);
//		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase stat1Phrase=new Phrase("Original for Receipient",font10);
//		PdfPCell stat1PhraseCell=new PdfPCell(stat1Phrase);
//		stat1PhraseCell.setBorder(0);
//		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		stat1PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		
//		Phrase stat2Phrase=new Phrase("Duplicate for Supplier/Transporter",font10);
//		PdfPCell stat2PhraseCell=new PdfPCell(stat2Phrase);
//		stat2PhraseCell.setBorder(0);
//		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		stat2PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		
//		Phrase stat3Phrase=new Phrase("Triplicate for Supplier",font10);
//		PdfPCell stat3PhraseCell=new PdfPCell(stat3Phrase);
//		stat3PhraseCell.setBorder(0);
//		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		stat3PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		
		Phrase myblank=new Phrase("   ",font10);
		PdfPCell myblankCell=new PdfPCell(myblank);
//		stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero=new Phrase(" ",font10);
		PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
//		stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
		Paragraph para1=new Paragraph();
		para1.setIndentationLeft(10f);
		para1.add(myblank);
		if(value==0){
			para1.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para1.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		
		para1.add(stat1Phrase);
		para1.setAlignment(Element.ALIGN_MIDDLE);
		
		PdfPCell stat1PhraseCell=new PdfPCell(para1);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
		Paragraph para2=new Paragraph();
		para2.setIndentationLeft(10f);
		
		if(value==1){
			para2.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para2.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		
		para2.add(stat2Phrase);
		para2.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell stat2PhraseCell=new PdfPCell(para2);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
		Paragraph para3=new Paragraph();
		para3.setIndentationLeft(10f);
		
		if(value==2){
			para3.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para3.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		para3.add(stat3Phrase);
		para3.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell stat3PhraseCell=new PdfPCell(para3);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
		mytbale.addCell(stat1PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
		mytbale.addCell(stat2PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
		mytbale.addCell(stat3PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);  

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(mytbale);
		tab.addCell(cell);
		try {
			document.add(tab);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
	//  ends here 
			String titlepdf="";
			String category = "";
			if(purchaseOrder.getCategory() != null && !purchaseOrder.getCategory().equals("")){
				category = " - "+purchaseOrder.getCategory();
			}
				if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
					titlepdf="Proforma Invoice";
				else
					titlepdf="Purchase"+category;

				
				Phrase titlephrase=new Phrase(titlepdf,font14bold);
				Paragraph titlepdfpara=new Paragraph();
				titlepdfpara.add(titlephrase);
				titlepdfpara.setAlignment(Element.ALIGN_CENTER);
				
				PdfPCell titlecell=new PdfPCell();
				titlecell.addElement(titlepdfpara);
				titlecell.setBorder(0);
				
				Phrase blankphrase=new Phrase("",font8);
				PdfPCell blankCell=new PdfPCell();
				blankCell.addElement(blankphrase);
				blankCell.setBorder(0);
				
				PdfPTable titlepdftable=new PdfPTable(3);
				titlepdftable.setWidthPercentage(100);
				titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
				titlepdftable.addCell(blankCell);
				titlepdftable.addCell(titlecell);
				titlepdftable.addCell(blankCell);
				
				 Paragraph blank =new Paragraph();
				    blank.add(Chunk.NEWLINE);
				
				PdfPTable parent=new PdfPTable(1);
				parent.setWidthPercentage(100);
				
				PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
				parent.addCell(titlePdfCell);
				
				try {
					document.add(parent);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
	}
	
	/** 23-10-2017 sagar sore [to send pdf in email of gst format for sales]**/
	/**23-10-2017 sagar sore**/
	public  void createPdfForEmailGST(VendorInvoice invoiceDetails,Company companyEntity,Customer custEntity,SalesOrder salesOrder,PurchaseOrder poEntity,Vendor vendorEntity,SalesGSTInvoice invpdf,Document document) 
	{
		try {   
			   SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			    long count=invoiceDetails.getId();
			    String preprintStatus="plane";
				boolean flag = false;
				this.invoiceentity=invoiceDetails;
				try {
					flag = invoiceentity.getInvoiceDate().after(sdf.parse("30 Jun 2017"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			   System.out.println("invoice Date "+invoiceentity.getInvoiceDate());
			   
				   System.out.println("SINGLE CONTRACT INVOICE");
			
				 
				   
				   document.open();
				  
				   System.out.println("ppppppppppppppppppooooooooooo"+count);
				   invpdf.setInvoice(count);
				   invpdf.createPdf(preprintStatus);
				   document.close();
			  
				 
	} 
	    catch (Exception e1) {
			e1.printStackTrace();
		}
	}
private void createFooterTab() {
		
		PdfPTable leftFootertab = new PdfPTable(1);
		leftFootertab.setWidthPercentage(100);
//		
//		Phrase amtChargeableph = new Phrase("Amount In Words", font8);
//		PdfPCell amtChargeablecell = new PdfPCell(amtChargeableph);
//		amtChargeablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		amtChargeablecell.setBorder(0);
//		leftFootertab.addCell(amtChargeablecell);
//		
//		String amtInWordsVal = "";
//		amtInWordsVal = SalesInvoicePdf.convert(invoiceentity.getNetPayable())+" Rupees Only";
//		Phrase amtInWordph = new Phrase(amtInWordsVal, font8);
//		PdfPCell amtInWordcell = new PdfPCell(amtInWordph);
//		amtInWordcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		amtInWordcell.setBorder(0);
//		leftFootertab.addCell(amtInWordcell);
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
		leftFootertab.addCell(blankcell);
		
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		
		Phrase remarkph = new Phrase("Remarks : " + invoiceentity.getRemark(), font8);//komal
		PdfPCell remarkcell = new PdfPCell(remarkph);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell.setBorder(0);
		leftFootertab.addCell(remarkcell);
		
		
//		Phrase headOfcph = new Phrase("Head Office", font8);
//		PdfPCell headOfccell = new PdfPCell(headOfcph);
//		headOfccell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		headOfccell.setBorder(0);
//		leftFootertab.addCell(headOfccell);
		
		
		String pan="";
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("PAN"))
			pan=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			
		}
		if(pan == null){
			pan = "";
		}
		Phrase companyPanph = new Phrase("Buyer's PAN    : "+pan , font8);
		PdfPCell companyPancell = new PdfPCell(companyPanph);
		companyPancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		companyPancell.setBorder(0);
		leftFootertab.addCell(companyPancell);
		
		
		PdfPTable rightFootertab = new PdfPTable(3);
		rightFootertab.setWidthPercentage(100);
		try {
			rightFootertab.setWidths(new float[]{33,33,33});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase companyNameph = new Phrase("For "+comp.getBusinessUnitName() , font8);
		PdfPCell companyNamecell = new PdfPCell(companyNameph);
		companyNamecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		companyNamecell.setBorder(0);
		companyNamecell.setColspan(3);
		rightFootertab.addCell(companyNamecell);
		
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		
		Phrase preparedbyph = new Phrase("Prepared by", font8);
		PdfPCell preparedbycell = new PdfPCell(preparedbyph);
		preparedbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		preparedbycell.setBorder(0);
		preparedbycell.setPaddingBottom(5);
		rightFootertab.addCell(preparedbycell);
		
		Phrase verifiedbyph = new Phrase("Verified by", font8);
		PdfPCell verifiedbycell = new PdfPCell(verifiedbyph);
		verifiedbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		verifiedbycell.setBorder(0);
		verifiedbycell.setPaddingBottom(5);
		rightFootertab.addCell(verifiedbycell);
		
		
		Phrase authorisedph = new Phrase("Authorised by", font8);
		PdfPCell authorisedcell = new PdfPCell(authorisedph);
		authorisedcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		authorisedcell.setBorder(0);
		authorisedcell.setPaddingBottom(5);
		rightFootertab.addCell(authorisedcell);
		
		PdfPTable outerFootertab = new PdfPTable(2);
		outerFootertab.setWidthPercentage(100);
		
		PdfPCell uppercell = new PdfPCell(leftFootertab);
		uppercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		uppercell.setBorderWidthBottom(0);
		uppercell.setBorderWidthRight(0);
		outerFootertab.addCell(uppercell);
		
		Phrase footerblank=new Phrase (" ",font8);
		PdfPCell footerblankcell = new PdfPCell(footerblank);
		footerblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerblankcell.setBorderWidthBottom(0);
		footerblankcell.setBorderWidthLeft(0);
		outerFootertab.addCell(footerblankcell);
		
		Phrase footerblank2=new Phrase (" ",font8);
		PdfPCell footerblankcell2 = new PdfPCell(footerblank2);
		footerblankcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerblankcell2.setBorderWidthTop(0);
		footerblankcell2.setBorderWidthRight(0);
		footerblankcell2.setPaddingBottom(5);
		outerFootertab.addCell(footerblankcell2);
		
		
		PdfPCell bottomcell = new PdfPCell(rightFootertab);
		bottomcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		bottomcell.setBorderWidthTop(0);
		bottomcell.setBorderWidthLeft(0);
		outerFootertab.addCell(bottomcell);
		
		
		Phrase declarationph=new Phrase ("This is a Computer Generated Document ",font8);
		PdfPCell declarationcell = new PdfPCell(declarationph);
		declarationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		declarationcell.setBorder(0);
		declarationcell.setColspan(2);
		declarationcell.setPaddingTop(5);
		outerFootertab.addCell(declarationcell);
		
		
		try {
			document.add(outerFootertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	private String getGSTNOfVendor(Vendor ven , String documentType){
		String gstTinStr = "";
		for (int i = 0; i < ven.getArticleTypeDetails().size(); i++) {
			if (ven.getArticleTypeDetails().get(i).getArticlePrint()
					.equalsIgnoreCase("YES")
					&& ven.getArticleTypeDetails().get(i).getDocumentName()
							.equals(documentType.trim())){
				System.out.println("Inside else");
			if (ven.getArticleTypeDetails().get(i)
					.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
				System.out.println("inside second else");
				gstTinStr = ven.getArticleTypeDetails().get(i)
						.getArticleTypeValue().trim();
				break;
			}
			}
		}
		return gstTinStr;
	 }		
	}
