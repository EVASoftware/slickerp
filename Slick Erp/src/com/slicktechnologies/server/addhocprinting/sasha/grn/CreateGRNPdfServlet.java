package com.slicktechnologies.server.addhocprinting.sasha.grn;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.server.addhocprinting.PurchaseOrderPdf;
import com.slicktechnologies.server.addhocprinting.purchaseOrderPdf.GRNPdfUpdate;
import com.slicktechnologies.server.addhocprinting.sasha.PurchaseOrderPdfForSasha;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.inventory.GRN;

public class CreateGRNPdfServlet extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = 4192616077384586232L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			GRN grn = ofy().load().type(GRN.class).id(count).now();
			
			String type  = req.getParameter("type");
			if(type!=null&&type.equals("GRNUpdated")){
				GRNPdfUpdate popdfsasha = new GRNPdfUpdate();
				popdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				Document document = popdfsasha.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				if(grn.getStatus().equals("Cancelled")){
					writer.setPageEvent(new PdfCancelWatermark());
				}else															// pdf
				if(!grn.getStatus().equals("Approved")){	
					 writer.setPageEvent(new PdfWatermark());
				} 

				document.open();
				popdfsasha.setPurchaseOrder(count);
				popdfsasha.createPdf();
				document.close();
			}else{
				GRNPdfForSasha popdfsasha = new GRNPdfForSasha();
				popdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				Document document = popdfsasha.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				if(grn.getStatus().equals("Cancelled")){
					writer.setPageEvent(new PdfCancelWatermark());
				}else															// pdf
				if(!grn.getStatus().equals("Approved")){	
					 writer.setPageEvent(new PdfWatermark());
				} 

				document.open();
				popdfsasha.setPurchaseOrder(count);
				popdfsasha.createPdf();
				document.close();
			}
				
				
			} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	
	
	
	


}
