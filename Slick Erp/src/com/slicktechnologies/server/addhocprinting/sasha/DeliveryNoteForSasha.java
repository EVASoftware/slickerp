package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class DeliveryNoteForSasha {

	public Document document;
	DeliveryNote delnoteEntity;
	Customer cust;
	Company comp;
	Company company;
	SalesOrder so;
//	int noOfLines = 38;
	
	//int noOfLines = 16;
	int noOfLines = 22;

	int noOfPage=1;
	int totalLines;
	int prouductCount=0;
	List<State> stateList;
	private Font font16boldul, font12bold, font8bold,font9bold, font8, font12boldul,font12,font14bold,font10,font10bold,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df2 = new DecimalFormat("0");
	Logger logger=Logger.getLogger("GRN PDF");
	CompanyPayment companyPayment;
	Branch branchDt = null;
	Boolean refOrderNo=false;
	int totalNoOfTaxItem=0;
	/**
	 * nidhi
	 * 9-08-2018
	 * for print serial no & model no
	 */
	boolean printModelSerailNoFlag =false;
	boolean delNoteV1=false;
	CustomerBranchDetails custBranch=null;
	boolean recursiveFlag=false;
	String companyName="";
	public void setDeliveryNote(Long count) {
		delnoteEntity=ofy().load().type(DeliveryNote.class).id(count).now();
		
		if(delnoteEntity.getCompanyId()==null)
			cust=ofy().load().type(Customer.class).filter("count",delnoteEntity.getCinfo().getCount()).first().now();
		else
			cust=ofy().load().type(Customer.class).filter("count",delnoteEntity.getCinfo().getCount()).filter("companyId", delnoteEntity.getCompanyId()).first().now();
			
		
		if(delnoteEntity.getCompanyId()==null)
			   comp=ofy().load().type(Company.class).first().now();
			else
			   comp=ofy().load().type(Company.class).filter("companyId",delnoteEntity.getCompanyId()).first().now();
			
		

		if (delnoteEntity.getCompanyId() != null)
			company = ofy().load().type(Company.class)
					.filter("companyId", delnoteEntity.getCompanyId()).first().now();
		else
			company = ofy().load().type(Company.class).first().now();
		
		
		if (delnoteEntity.getCompanyId() != null)
			so = ofy().load().type(SalesOrder.class)
					.filter("companyId", delnoteEntity.getCompanyId()).filter("count", delnoteEntity.getSalesOrderCount()).first().now();
		else
			so = ofy().load().type(SalesOrder.class).filter("count", delnoteEntity.getSalesOrderCount()).first().now();
		
		
		companyName=company.getBusinessUnitName();
		
		
		
		stateList = ofy().load().type(State.class)
				.filter("companyId", delnoteEntity.getCompanyId()).list();
		/**
		 * nidhi
		 * 9-08-2018
		 */
//		printModelSerailNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "PrintModelNoAndSerialNo", delnoteEntity.getCompanyId());
		
		branchDt = ofy().load().type(Branch.class)
				.filter("companyId", delnoteEntity.getCompanyId())
				.filter("buisnessUnitName", delnoteEntity.getBranch())
				.first().now();
		
		
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
				"Branch", "BranchAsCompany", comp.getCompanyId())) {

			logger.log(Level.SEVERE, "Process active --");
			
				if (branchDt != null && branchDt.getPaymentMode() != null
						&& !branchDt.getPaymentMode().trim().equals("")) {

					logger.log(Level.SEVERE,
							"Process active --" + branchDt.getPaymentMode());

					List<String> paymentDt = Arrays.asList(branchDt
							.getPaymentMode().trim().split("/"));

					if (paymentDt.get(0).trim().matches("[0-9]+")) {

						int payId = Integer.parseInt(paymentDt.get(0).trim());

						companyPayment = ofy().load()
								.type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", delnoteEntity.getCompanyId())
								.first().now();

						if (companyPayment != null) {
							comp = ServerAppUtility.changeBranchASCompany(
									branchDt, comp);
						}

					}

				}
			
		}

		printModelSerailNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "PrintModelNoAndSerialNo", delnoteEntity.getCompanyId());
		
		/**Date 23-9-2020 by Amol added this process configuration raised by Rahul Tiwari**/
		delNoteV1=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "DeliveryNoteV1", delnoteEntity.getCompanyId());
		/**
	 	 * Date : 26-04-2019 BY ANIL
	 	 * if customer branch is selected on sales order then will pick email , 
	 	 * cell and  gstin number from customer branch only
	 	 */
	 	if(delnoteEntity!=null&&delnoteEntity.getCustBranch()!=null&&!delnoteEntity.getCustBranch().equals("")){
	 		custBranch = ofy().load().type(CustomerBranchDetails.class)
	 		        .filter("cinfo.count", Integer.valueOf(delnoteEntity.getCinfo().getCount()))
	 		        .filter("buisnessUnitName", delnoteEntity.getCustBranch()).first().now();
	 	}
	}
	
	public DeliveryNoteForSasha() {
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void createPdf() {
		/**
		  * @author Ashwini Patil
		  * @since 15-04-2022
		  * So as per discussion with Nitin sir old standard pdf format has to be replaced with Sasha format.
		  * Following changes are required in this pdf		  
		  * print 3 copies
		  *	add Eway Bill No. and Sales Order No. after Invoice Id
		  *	add Transport Mode, Vehicle Number,Date of Supply,Place Of Supply in terms and conditions box
		  *	If values are not there dont print headings Remarks, Head Office, Company's PAN
		  */
		for(int i=0;i<3;i++)
		{
			totalLines = getTotalNoOfLines();
			Createletterhead();
			createHeader(i);
			Createtitle();
			createAddressDetailTab();
			createProductTitleTab();
			createProductDetailsTab();
			
			if(totalLines<=22){
				createTotalTab();
				createFooterTab();
				System.out.println("1 st page");
			}
//			else if(totalLines>20&&totalLines<38){
				else if(totalLines>22){
					createFooterTab();
					createAnnexureForRemainingProduct(prouductCount);
					System.out.println("inside annexure");
//				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
//				try {
//					document.add(nextpage);
//				} catch (DocumentException e1) {
//					e1.printStackTrace();
//				}
//				blankTable();
//				createTotalTab();
//				createFooterTab();
					
			}
//				else if(totalLines>38&& prouductCount != 0){
//			createAnnexureForRemainingProduct(prouductCount);
//				
//				}
		
		
//			createFooterlogo();
			
			if (comp.getUploadFooter() != null) {
				createCompanyNameAsFooter(document, comp);
			}
//			if (noOfLines <= 0 && prouductCount != 0) {
//				System.out.println("noOfLines...."+noOfLines);
//				createAnnexureForRemainingProduct(prouductCount);
//			}
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
	
	private void blankTable(){
		PdfPTable productDetailstab = new PdfPTable(10);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,13,50,10,10,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase blankremainig = new Phrase(" ", font8);
		PdfPCell blankremainigcell = new PdfPCell(blankremainig);
		blankremainigcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankremainigcell.setBorderWidthLeft(0);
		blankremainigcell.setBorderWidthRight(0);
		blankremainigcell.setBorderWidthTop(0);
		blankremainigcell.setFixedHeight(20);
		productDetailstab.addCell(blankremainigcell);
		productDetailstab.addCell(blankremainigcell);
		productDetailstab.addCell(blankremainigcell);
		productDetailstab.addCell(blankremainigcell);
		productDetailstab.addCell(blankremainigcell);
		productDetailstab.addCell(blankremainigcell);
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	

	private void createCompanyNameAsFooter(Document doc, Company comp2) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 20f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 try
//		 {
//		 Image
//		 image1=Image.getInstance("images/header.jpg");
//		 image1.scalePercent(13f);
//		 image1.scaleAbsoluteWidth(520f);
//		 image1.setAbsolutePosition(40f,20f);
//		 doc.add(image1);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
	}
	
	
	public int getTotalNoOfLines(){
		int totalLines = 0;
		for(DeliveryLineItems grn1:delnoteEntity.getDeliveryItems()  ){
			int length=grn1.getProdName().length();
			int lines=(int)Math.ceil(length/59.0);
			totalLines+= lines;
			System.out.println("TotalLines"+totalLines);
			}
		return  totalLines ;
	}
	
	
	
	
	
	
	

	private void createTotalTab() {
		
		PdfPTable productDetailstab = new PdfPTable(6);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,13,50,10,10,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productDetailstab.addCell(blankcell);
		
		productDetailstab.addCell(blankcell);
		
		Phrase totalph = new Phrase("Total", font8);
		PdfPCell totalcell = new PdfPCell(totalph);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalcell);
		
		productDetailstab.addCell(blankcell);
		productDetailstab.addCell(blankcell);
		
		double totalQut=0;
		for (int i = 0; i < delnoteEntity.getDeliveryItems().size(); i++) {
			totalQut=totalQut+delnoteEntity.getDeliveryItems().get(i).getQuantity();
		}
		Phrase totalQtph = new Phrase(df2.format(totalQut)+"", font8);
		PdfPCell totalQtcell = new PdfPCell(totalQtph);
		totalQtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalQtcell);
		
		
//		productDetailstab.addCell(blankcell);
	
		
//		Phrase totalAmtph = new Phrase(df.format(po.getNetpayble()), font8);
//		PdfPCell totalAmtcell = new PdfPCell(totalAmtph);
//		totalAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(totalAmtcell);
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createProductDetailsTab() {
		
		PdfPTable productDetailstab = new PdfPTable(6);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,13,50,10,10,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
	
		for (int i = 0; i < delnoteEntity.getDeliveryItems().size(); i++) {
			ItemProduct itemProduct =null;
			try{
				itemProduct = ofy().load().type(ItemProduct.class).filter("companyId", comp.getCompanyId())
						.filter("productCode",delnoteEntity.getDeliveryItems().get(i).getPrduct().getProductCode().trim())
						.first().now();
			}catch(Exception e){
				
			}
			
//			ItemProduct itemProduct = ofy().load().type(ItemProduct.class).filter("companyId", comp.getCompanyId())
//					.filter("productCode",delnoteEntity.getDeliveryItems().get(i).getPrduct().getProductCode().trim())
//					.first().now();
			
			
			
//			if (noOfLines == 0 || noOfLines < 0 ) {
//				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
//				prouductCount = i;
//				break;
//			}
//			
//			noOfLines = noOfLines - 1;
			
			prouductCount = i;
			if (noOfLines <= 0) {
				prouductCount = i;
				break;
			}
			int lenght=delnoteEntity.getDeliveryItems().get(i).getProdCode().length();
			logger.log(Level.SEVERE,"Length : "+lenght);
			int lines= (int) Math.ceil((lenght/59.0));
			logger.log(Level.SEVERE,"LINES : "+lines);
			noOfLines = noOfLines - lines;
			logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
			
			
			
		
			Phrase srnoValph = new Phrase(i+1+"", font8);
			PdfPCell srnoValcell = new PdfPCell(srnoValph);
			srnoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			srnoValcell.setBorderWidthBottom(0);
			srnoValcell.setBorderWidthTop(0);
			productDetailstab.addCell(srnoValcell);
			
			Phrase prodcodevalph = new Phrase(delnoteEntity.getDeliveryItems().get(i).getProdCode(), font8);
			PdfPCell prodcodevalcell = new PdfPCell(prodcodevalph);
			prodcodevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodcodevalcell.setBorderWidthBottom(0);
			prodcodevalcell.setBorderWidthTop(0);
			productDetailstab.addCell(prodcodevalcell);
			
			Phrase descriptionValph = new Phrase(delnoteEntity.getDeliveryItems().get(i).getProdName(), font8);
			PdfPCell descriptionValcell = new PdfPCell(descriptionValph);
			descriptionValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			descriptionValcell.setBorderWidthBottom(0);
			descriptionValcell.setBorderWidthTop(0);
			productDetailstab.addCell(descriptionValcell);
			
			
			if(delnoteEntity.getDeliveryItems().get(i).getPrduct() != null) {
				Phrase hsnSacValph = new Phrase(delnoteEntity.getDeliveryItems().get(i).getPrduct().getHsnNumber(), font8);
				PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
				hsnSacValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				hsnSacValcell.setBorderWidthBottom(0);
				hsnSacValcell.setBorderWidthTop(0);
				productDetailstab.addCell(hsnSacValcell);
			} else {
				Phrase hsnSacValph = new Phrase("", font8);
				PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
				hsnSacValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				hsnSacValcell.setBorderWidthBottom(0);
				hsnSacValcell.setBorderWidthTop(0);
				productDetailstab.addCell(hsnSacValcell);
			}
		
		
//		double cgstper=0;
//		double sgstper=0;
//		double igstper=0;
//		
//		double gstrate=0;
//		double taxamt=0;
//	
//		if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("CGST")||
//				po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("SGST")){
//			cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
//			sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
//		}
//		else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("SGST")||
//				po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("CGST")){
//			sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
//			cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
//		}
//		else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("IGST")||
//				po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("")){
//			
//			igstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
//		}
//		else if(po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("IGST")||
//				po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase(" ")){
//			
//			igstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
//		}
//		
//		if(cgstper!=0&&sgstper!=0){
//			gstrate=cgstper+sgstper;
//			
//		}
//		else if(igstper!=0){
//			gstrate=igstper;
//		}
//		
//		taxamt=(po.getProductDetails().get(i).getProdPrice()*gstrate)/100;
//		
//		
//		Phrase gstRateph = new Phrase(gstrate+"%", font8);
//		PdfPCell gstRatecell = new PdfPCell(gstRateph);
//		gstRatecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(gstRatecell);
		
		Phrase uom=null;
//		ItemProduct itemProduct = ofy()
//				.load()
//				.type(ItemProduct.class)
//				.filter("companyId", comp.getCompanyId())
//				.filter("productCode",delnoteEntity.getDeliveryItems().get(i).getPrduct().getProductCode().trim())
//				.first().now();
		
		if (itemProduct != null&& itemProduct.getUnitOfMeasurement() != null) {
			uom = new Phrase(itemProduct.getUnitOfMeasurement(), font8);
		} else {
			uom = new Phrase(" ", font8);
		}
		
		PdfPCell percell = new PdfPCell(uom);
		percell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		percell.setBorderWidthBottom(0);
		percell.setBorderWidthTop(0);
		productDetailstab.addCell(percell);
		
		
		Phrase quantityph = new Phrase(df2.format(delnoteEntity.getDeliveryItems().get(i).getQuantity())+"", font8);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		quantitycell.setBorderWidthBottom(0);
		quantitycell.setBorderWidthTop(0);
		productDetailstab.addCell(quantitycell);
		
//		Phrase rateph = new Phrase(df.format(po.getProductDetails().get(i).getProdPrice())+"", font8);
//		PdfPCell ratecell = new PdfPCell(rateph);
//		ratecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(ratecell);
		
		
	
		
		
		if(printModelSerailNoFlag){
			if(delnoteEntity.getDeliveryItems().get(i).getProModelNo()!=null 
					&& delnoteEntity.getDeliveryItems().get(i).getProModelNo().trim().length()>0){
				
				Phrase blackCell = new Phrase("", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				
				Phrase modelCell = new Phrase("Model No : " 
							+delnoteEntity.getDeliveryItems().get(i).getProModelNo().trim(), font8);
				PdfPCell modelPCell = new PdfPCell(modelCell);
				modelPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				modelPCell.setBorderWidthBottom(0);
				modelPCell.setBorderWidthTop(0);
				
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(modelPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				noOfLines--;
			}
			
			if(delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails()!=null 
					&& delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().containsKey(0)
				&& delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().get(0) != null &&
						delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().get(0).size()>0	){
				
				Phrase blackCell = new Phrase("", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				
				String str = "";
				for(ProductSerialNoMapping  proSrn : delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().get(0)){
					if(proSrn.getProSerialNo().trim().length()>0){
						str = str + proSrn.getProSerialNo() + ", ";
					}
					
				}
				if(str.trim().length()>0){
					System.out.println(str.substring(0,str.trim().length()-1));
					str = str.substring(0,str.trim().length()-1);
				}
				
				
				Phrase modelCell = new Phrase("Sr No : "+str, font8);
				PdfPCell modelPCell = new PdfPCell(modelCell);
				modelPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				modelPCell.setBorderWidthBottom(0);
				modelPCell.setBorderWidthTop(0);
				
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(modelPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				noOfLines--;
			}
			
		}
		
//		double totalAmount=0;
//		totalAmount=(po.getProductDetails().get(i).getProdPrice()*po.getProductDetails().get(i).getProductQuantity())+taxamt;
//		Phrase amountph = new Phrase(df.format(totalAmount)+"", font8);
//		PdfPCell amountcell = new PdfPCell(amountph);
//		amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(amountcell);
		prouductCount++;
		}
		
		int remainingLines=0;
		/**
		 * Date : 27-12-2018 BY amol
		 * In commented code if line items are less than the defined number of line
		 * then there we were printing blank lines 15
		 */
//		if (noOfLines != 0) {
//			remainingLines = 15 - (15 - noOfLines);
//		}
//		if (noOfLines>0) {
//			remainingLines = 16 - (16-noOfLines);
//		}
		/**
		 * @author Vijay Date :- 01-10-2020
		 * Des :- there is lot of space in 1st page so mapped 33 products in 1st page
		 * requirement raised by Rahul
		 */
		if (noOfLines>0) {
			remainingLines = 22 - (22-noOfLines);
		}
		/**
		 * End
		 */
		

		System.out.println("remainingLines" + remainingLines);
//		for (int j = 0; j < remainingLines; j++) {
//			PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
//			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			pCell.setBorderWidthBottom(0);
//			pCell.setBorderWidthTop(0);
//			productDetailstab.addCell(pCell);
//			productDetailstab.addCell(pCell);
//			productDetailstab.addCell(pCell);
//			productDetailstab.addCell(pCell);
//			productDetailstab.addCell(pCell);
//			productDetailstab.addCell(pCell);
//		}
		
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}

	private void createProductTitleTab() {
		
		PdfPTable productTitletab = new PdfPTable(6);
		productTitletab.setWidthPercentage(100);
		
		try {
			productTitletab.setWidths(new float[]{7,13,50,10,10,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase srnoph = new Phrase("Sr.No", font8);
		PdfPCell srnocell = new PdfPCell(srnoph);
		srnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(srnocell);
		
		Phrase prodcodeph = new Phrase("Prodcuct Code", font8);
		PdfPCell prodcodecell = new PdfPCell(prodcodeph);
		prodcodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(prodcodecell);
		
		Phrase descriptionph = new Phrase("Description of Goods", font8);
		PdfPCell descriptioncell = new PdfPCell(descriptionph);
		descriptioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(descriptioncell);
		
		Phrase hsnSacph = new Phrase("HSN/SAC", font8);
		PdfPCell hsnSaccell = new PdfPCell(hsnSacph);
		hsnSaccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(hsnSaccell);
		
//		Phrase gstRateph = new Phrase("GST Rate", font8);
//		PdfPCell gstRatecell = new PdfPCell(gstRateph);
//		gstRatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		productTitletab.addCell(gstRatecell);
		
		Phrase perph = new Phrase("UOM", font8);
		PdfPCell percell = new PdfPCell(perph);
		percell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(percell);
		
		
		
		
		
		Phrase quantityph = new Phrase("Quantity", font8);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(quantitycell);
		
//		Phrase rateph = new Phrase("Rate", font8);
//		PdfPCell ratecell = new PdfPCell(rateph);
//		ratecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		productTitletab.addCell(ratecell);
		
	
		
//		Phrase amountph = new Phrase("Amount", font8);
//		PdfPCell amountcell = new PdfPCell(amountph);
//		amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		productTitletab.addCell(amountcell);
		
		try {
			document.add(productTitletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createFooterlogo() {

		/**** image ***/
		DocumentUpload footerdocument = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
//		Image image2 = null;
//		try {
//			image2 = Image.getInstance(new URL(hostUrl
//					+ footerdocument.getUrl()));
//			image2.scalePercent(20f);
//			// image2.setAbsolutePosition(40f,765f);
//			// doc.add(image2);
//
//			imageSignCell = new PdfPCell();
//			imageSignCell.setBorder(0);
//			imageSignCell.setImage(image2);
//			// imageSignCell.setPaddingTop(8);
//			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			// imageSignCell.setFixedHeight(40);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		Image image1 = null;
		try {
			image1 = Image.getInstance("images/Capture.JPG");
			image1.scalePercent(20f);
			// image1.setAbsolutePosition(40f,765f);
			// doc.add(image1);

			imageSignCell = new PdfPCell(image1);
			// imageSignCell.addElement();
			imageSignCell.setImage(image1);
			// imageSignCell.setFixedHeight(40);
			imageSignCell.setBorder(0);
			// imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}

		PdfPTable letterheadTab = new PdfPTable(1);
		letterheadTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			letterheadTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			letterheadTab.addCell(logoblankcell);
		}

		try {
			document.add(letterheadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		if (noOfLines == 0 && prouductCount != 0) {
			createAnnexureForRemainingProduct(prouductCount);
		}
	
	}

	private void createAnnexureForRemainingProduct(int prouductCount) {
		
		
		noOfPage++;
		
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Createtitle();
		createProductTitleTab();
		createProductDetailsForAnnextureTab(prouductCount);
		
//	    createTotalTab();
//		createFooterTab();
//		createCompanyNameAsFooter(document, comp);
		
	}

	private void createProductDetailsForAnnextureTab(int count) {
		
		/**
		 * Date : 27-12-2018 BY Amol
		 * here we resetting the no of lines 
		 */
//		boolean recursiveFlag=false;
		 recursiveFlag=false;
		noOfLines=50;
		logger.log(Level.SEVERE,"Annexture TABLE : "+noOfLines);
		
		
		PdfPTable productDetailstab = new PdfPTable(6);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,13,50,10,10,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int i = count; i < delnoteEntity.getDeliveryItems().size(); i++) {
			
//			if (noOfLines == 0) {
//				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
//				prouductCount = i;
//				break;
//			}
//			
//			noOfLines = noOfLines - 1;
			

			/**
			 * Date : 27-12-2018 By amol
			 * updating no of lines
			 */
			prouductCount = i;
			if (noOfLines <= 0) {
				prouductCount = i;
				recursiveFlag=true;
				break;
			}
			int lenght=delnoteEntity.getDeliveryItems().get(i).getProdCode().length();
			logger.log(Level.SEVERE,"Length : "+lenght);
			int lines= (int) Math.ceil((lenght/59.0));
			logger.log(Level.SEVERE,"LINES : "+lines);
//			noOfLines = noOfLines - 1;
			noOfLines = noOfLines - lines;
			logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
			/**
			 * End
			 */
			
		
		Phrase srnoValph = new Phrase(i+1+"", font8);
		PdfPCell srnoValcell = new PdfPCell(srnoValph);
		srnoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		srnoValcell.setBorderWidthBottom(0);
		srnoValcell.setBorderWidthTop(0);
		productDetailstab.addCell(srnoValcell);
		
		Phrase prodcodevalph = new Phrase(delnoteEntity.getDeliveryItems().get(i).getProdCode(), font8);
		PdfPCell prodcodevalcell = new PdfPCell(prodcodevalph);
		prodcodevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		prodcodevalcell.setBorderWidthBottom(0);
		prodcodevalcell.setBorderWidthTop(0);
		productDetailstab.addCell(prodcodevalcell);
		
		Phrase descriptionValph = new Phrase(delnoteEntity.getDeliveryItems().get(i).getProdName(), font8);
		PdfPCell descriptionValcell = new PdfPCell(descriptionValph);
		descriptionValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptionValcell.setBorderWidthBottom(0);
		descriptionValcell.setBorderWidthTop(0);
		productDetailstab.addCell(descriptionValcell);
		
		Phrase hsnSacValph = new Phrase(delnoteEntity.getDeliveryItems().get(i).getPrduct().getHsnNumber(), font8);
		PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
		hsnSacValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		hsnSacValcell.setBorderWidthBottom(0);
		hsnSacValcell.setBorderWidthTop(0);
		productDetailstab.addCell(hsnSacValcell);
		
//		double cgstper=0;
//		double sgstper=0;
//		double igstper=0;
//		
//		double gstrate=0;
//		double taxamt=0;
//	
//		if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("CGST")||
//				po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("SGST")){
//			cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
//			sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
//		}
//		else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("SGST")||
//				po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("CGST")){
//			sgstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
//			cgstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
//		}
//		else if(po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase("IGST")||
//				po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("")){
//			
//			igstper=po.getProductDetails().get(i).getPurchaseTax1().getPercentage();
//		}
//		else if(po.getProductDetails().get(i).getPurchaseTax2().getTaxPrintName().equalsIgnoreCase("IGST")||
//				po.getProductDetails().get(i).getPurchaseTax1().getTaxPrintName().equalsIgnoreCase(" ")){
//			
//			igstper=po.getProductDetails().get(i).getPurchaseTax2().getPercentage();
//		}
//		
//		if(cgstper!=0&&sgstper!=0){
//			gstrate=cgstper+sgstper;
//			
//		}
//		else if(igstper!=0){
//			gstrate=igstper;
//		}
//		
//		taxamt=(po.getProductDetails().get(i).getProdPrice()*gstrate)/100;
//		
//		
//		Phrase gstRateph = new Phrase(gstrate+"%", font8);
//		PdfPCell gstRatecell = new PdfPCell(gstRateph);
//		gstRatecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(gstRatecell);
//		Phrase rateph = new Phrase(df.format(po.getProductDetails().get(i).getProdPrice())+"", font8);
//		PdfPCell ratecell = new PdfPCell(rateph);
//		ratecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(ratecell);
		
		
		Phrase uom=null;
		ItemProduct itemProduct = ofy()
				.load()
				.type(ItemProduct.class)
				.filter("companyId", comp.getCompanyId())
				.filter("productCode",delnoteEntity.getDeliveryItems().get(i).getPrduct().getProductCode().trim())
				.first().now();
		if (itemProduct.getUnitOfMeasurement() != null) {
			uom = new Phrase(itemProduct.getUnitOfMeasurement(), font8);
			}
		else{
			uom = new Phrase(" ", font8);
		}
		
		
		
		PdfPCell percell = new PdfPCell(uom);
		percell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		percell.setBorderWidthBottom(0);
		percell.setBorderWidthTop(0);
		productDetailstab.addCell(percell);
		
		Phrase quantityph = new Phrase(df2.format(delnoteEntity.getDeliveryItems().get(i).getQuantity())+"", font8);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		quantitycell.setBorderWidthBottom(0);
		quantitycell.setBorderWidthTop(0);
		productDetailstab.addCell(quantitycell);
		
		
		
		
		if(printModelSerailNoFlag){
			if(delnoteEntity.getDeliveryItems().get(i).getProModelNo()!=null 
					&& delnoteEntity.getDeliveryItems().get(i).getProModelNo().trim().length()>0){
				
				Phrase blackCell = new Phrase("", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				
				Phrase modelCell = new Phrase("Model No : " 
							+delnoteEntity.getDeliveryItems().get(i).getProModelNo().trim(), font8);
				PdfPCell modelPCell = new PdfPCell(modelCell);
				modelPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				modelPCell.setBorderWidthBottom(0);
				modelPCell.setBorderWidthTop(0);
				
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(modelPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				noOfLines--;
			}
			
			if(delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails()!=null 
					&& delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().containsKey(0)
				&& delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().get(0) != null &&
						delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().get(0).size()>0	){
				
				String str = "";
				for(ProductSerialNoMapping  proSrn : delnoteEntity.getDeliveryItems().get(i).getProSerialNoDetails().get(0)){
					if(proSrn.getProSerialNo().trim().length()>0){
						str = str + proSrn.getProSerialNo() + ", ";
					}
					
				}
				if(str.trim().length()>0){
					System.out.println(str.substring(0,str.trim().length()-1));
					str = str.substring(0,str.trim().length()-1);
				}
				
				
				Phrase blackCell = new Phrase("", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				
				Phrase modelCell = new Phrase("Sr No : "+str, font8);
				PdfPCell modelPCell = new PdfPCell(modelCell);
				modelPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				modelPCell.setBorderWidthBottom(0);
				modelPCell.setBorderWidthTop(0);
				
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(modelPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				noOfLines--;
			}
			
		}
//		double totalAmount=0;
//		totalAmount=(po.getProductDetails().get(i).getProdPrice()*po.getProductDetails().get(i).getProductQuantity())+taxamt;
//		Phrase amountph = new Phrase(df.format(totalAmount)+"", font8);
//		PdfPCell amountcell = new PdfPCell(amountph);
//		amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		productDetailstab.addCell(amountcell);
		prouductCount++;
		}
		
//		Phrase blankremainig = new Phrase(" ", font8);
//		PdfPCell blankremainigcell = new PdfPCell(blankremainig);
//		blankremainigcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		blankremainigcell.setBorderWidthBottom(0);
//		blankremainigcell.setBorderWidthLeft(0);
//		blankremainigcell.setBorderWidthRight(0);
//		blankremainigcell.setColspan(9);
//		productDetailstab.addCell(blankremainigcell);
		
//		int remainingLines=0;
//		if (noOfLines != 0) {
//			remainingLines = 15 - (15 - noOfLines);
//		}
//		System.out.println("remainingLines" + remainingLines);
//		for (int j = 0; j < remainingLines; j++) {
//			PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
//			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			pCell.setColspan(8);
//			pCell.setBorderWidthBottom(0);
//			pCell.setBorderWidthTop(0);
//			productDetailstab.addCell(pCell);
//		}
		
		
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		if(recursiveFlag==false){
			    createTotalTab();
				createFooterTab();
				createCompanyNameAsFooter(document, comp);
			
		}else{
            createFooterTab();
			createCompanyNameAsFooter(document, comp);
		}
		
		if(recursiveFlag){
			createAnnexureForRemainingProduct(prouductCount);
			
		}
	}

	private void createFooterTab() {
		
		PdfPTable leftFootertab = new PdfPTable(3);
		leftFootertab.setWidthPercentage(100);
		
		try {
			leftFootertab.setWidths(new float[]{25,5,70});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Phrase amtChargeableph = new Phrase("Amount In Words", font8);
		PdfPCell amtChargeablecell = new PdfPCell(amtChargeableph);
		amtChargeablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtChargeablecell.setBorder(0);
//		leftFootertab.addCell(amtChargeablecell);
		
		String amtInWordsVal = "";
		amtInWordsVal = SalesInvoicePdf.convert(delnoteEntity.getNetpayable())+" Rupees Only";
		Phrase amtInWordph = new Phrase(amtInWordsVal, font8);
		PdfPCell amtInWordcell = new PdfPCell(amtInWordph);
		amtInWordcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtInWordcell.setBorder(0);
//		leftFootertab.addCell(amtInWordcell);
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
//		leftFootertab.addCell(blankcell);
		
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		
		Phrase colPh = new Phrase(" : ", font8);
		PdfPCell colPhcell = new PdfPCell(colPh);
		colPhcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		colPhcell.setBorder(0);
		
		
		
		
		
		Phrase remarkph = new Phrase("Remarks  ", font8);
		PdfPCell remarkcell = new PdfPCell(remarkph);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell.setBorder(0);
		
		String delDesc2="";
		if(delnoteEntity.getDesriptionTwo()!=null){
			delDesc2=delnoteEntity.getDesriptionTwo();
		}else{
			delDesc2="";
		}
		
		Phrase remarkValueph = new Phrase(delDesc2, font8);
		PdfPCell remarkValuephcell = new PdfPCell(remarkValueph);
		remarkValuephcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkValuephcell.setBorder(0);
		
		//Ashwini Patil added if
		if(!delDesc2.equals("")&&delDesc2!=null){
		leftFootertab.addCell(remarkcell);
		leftFootertab.addCell(colPhcell);
		leftFootertab.addCell(remarkValuephcell);
		}
		
		Phrase headOfcph = new Phrase("Head Office  ", font8);
		PdfPCell headOfccell = new PdfPCell(headOfcph);
		headOfccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		headOfccell.setBorder(0);
		
		//Ashwini Patil Commenting below code as there is no value for Head Office
//		leftFootertab.addCell(headOfccell);
//		leftFootertab.addCell(blankcell);
//		leftFootertab.addCell(blankcell);
		
		String pan="";
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("PAN"))
			pan=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			
		}
		
		Phrase companyPanph = new Phrase("Company's PAN  " , font8);
		PdfPCell companyPancell = new PdfPCell(companyPanph);
		companyPancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		companyPancell.setBorder(0);
		
		
		
		Phrase companyPanValueph = new Phrase(pan , font8);
		PdfPCell companyPanValuephcell = new PdfPCell(companyPanValueph);
		companyPanValuephcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		companyPanValuephcell.setBorder(0);
		
	
		if(!pan.equals("")&&pan!=null){
		leftFootertab.addCell(companyPancell);
		leftFootertab.addCell(colPhcell);
		leftFootertab.addCell(companyPanValuephcell);
		}
		
		
		
		
		
		PdfPTable rightFootertab = new PdfPTable(3);
		rightFootertab.setWidthPercentage(100);
		try {
			rightFootertab.setWidths(new float[]{33,33,33});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Phrase companyNameph = new Phrase("For"+comp.getBusinessUnitName() , font8);
		/**Date 4-6-2019 by Amol to print the company name **/
		Phrase companyNameph = new Phrase("For "+companyName , font8);
//		Phrase companyNameph = new Phrase("For "+company.getBusinessUnitName() , font8);
		PdfPCell companyNamecell = new PdfPCell(companyNameph);
		companyNamecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		companyNamecell.setBorder(0);
		companyNamecell.setColspan(3);
		companyNamecell.setPaddingTop(8);
		rightFootertab.addCell(companyNamecell);
		
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		
		/**
		 * @author Anil @since 24-11-2021
		 */
		String createdBy="";
		if(so!=null&&so.getCreatedBy()!=null){
			createdBy=so.getCreatedBy();
		}
		Phrase preparedbyValueph = new Phrase(createdBy, font8);
		PdfPCell preparedbyValuecell = new PdfPCell(preparedbyValueph);
		preparedbyValuecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		preparedbyValuecell.setBorder(0);
		rightFootertab.addCell(preparedbyValuecell);
		
		
		String approverName="";
		if(delnoteEntity.getStatus().equalsIgnoreCase("Approved")){
			logger.log(Level.SEVERE, "deliverynote status "+delnoteEntity.getStatus());
			approverName=delnoteEntity.getApproverName();
		}else{
			approverName="";
		}
		
		Phrase verifiedbyValueph = new Phrase(approverName, font8);
		PdfPCell verifiedbyValuecell = new PdfPCell(verifiedbyValueph);
		verifiedbyValuecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		verifiedbyValuecell.setBorder(0);
		rightFootertab.addCell(verifiedbyValuecell);
		
		
		rightFootertab.addCell(blankcell);
		
		Phrase preparedbyph = new Phrase("Prepared by", font8);
		PdfPCell preparedbycell = new PdfPCell(preparedbyph);
		preparedbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		preparedbycell.setBorder(0);
		preparedbycell.setPaddingBottom(5);
		rightFootertab.addCell(preparedbycell);
		
		Phrase verifiedbyph = new Phrase("Verified by", font8);
		PdfPCell verifiedbycell = new PdfPCell(verifiedbyph);
		verifiedbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		verifiedbycell.setBorder(0);
		verifiedbycell.setPaddingBottom(5);
		rightFootertab.addCell(verifiedbycell);
		
		
		Phrase authorisedph = new Phrase("Authorised Signatory", font8);
		PdfPCell authorisedcell = new PdfPCell(authorisedph);
		authorisedcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		authorisedcell.setBorder(0);
		authorisedcell.setPaddingBottom(5);
		rightFootertab.addCell(authorisedcell);
		
		PdfPTable outerFootertab = new PdfPTable(2);
		outerFootertab.setWidthPercentage(100);
		
		PdfPCell uppercell = new PdfPCell(leftFootertab);
		uppercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		uppercell.setBorderWidthBottom(0);
		uppercell.setBorderWidthRight(0);
		outerFootertab.addCell(uppercell);
		
		if(prouductCount<delnoteEntity.getDeliveryItems().size()){
			
			Phrase footerblank=new Phrase ("Continued... ",font9bold);
			PdfPCell footerblankcell = new PdfPCell(footerblank);
			footerblankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			footerblankcell.setBorderWidthBottom(0);
			footerblankcell.setBorderWidthLeft(0);
			footerblankcell.setPaddingTop(10);
			footerblankcell.setPaddingRight(10);
			outerFootertab.addCell(footerblankcell);
		}else{
		Phrase footerblank=new Phrase (" ",font8);
		PdfPCell footerblankcell = new PdfPCell(footerblank);
		footerblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerblankcell.setBorderWidthBottom(0);
		footerblankcell.setBorderWidthLeft(0);
		outerFootertab.addCell(footerblankcell);
		}
		
		
		PdfPTable leftBottomFootertab = new PdfPTable(3);
		leftBottomFootertab.setWidthPercentage(100);
		try {
			leftBottomFootertab.setWidths(new float[]{25,5,70});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase receivedPh = new Phrase("Received by  " , font8);
		PdfPCell receivedPhcell = new PdfPCell(receivedPh);
		receivedPhcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		receivedPhcell.setBorder(0);
		receivedPhcell.setPaddingTop(8);
		
		Phrase colPh1 = new Phrase(" : ", font8);
		PdfPCell colPhcell1 = new PdfPCell(colPh1);
		colPhcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		colPhcell1.setBorder(0);
		colPhcell1.setPaddingTop(8);
		
		
		if(delNoteV1){
			leftBottomFootertab.addCell(receivedPhcell);
			leftBottomFootertab.addCell(colPhcell1);
			leftBottomFootertab.addCell(blankcell);
		}
		
		if(delNoteV1){
		leftBottomFootertab.addCell(blankcell);
		leftBottomFootertab.addCell(blankcell);
		leftBottomFootertab.addCell(blankcell);
		
		leftBottomFootertab.addCell(blankcell);
		leftBottomFootertab.addCell(blankcell);
		leftBottomFootertab.addCell(blankcell);
		}
		
		
		Phrase receivedSignPh = new Phrase("Recd. Sign and Stamp  " , font8);
		PdfPCell receivedSignPhcell = new PdfPCell(receivedSignPh);
		receivedSignPhcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		receivedSignPhcell.setBorder(0);
		if(delNoteV1){
			leftBottomFootertab.addCell(receivedSignPhcell);
			leftBottomFootertab.addCell(colPhcell);
			leftBottomFootertab.addCell(blankcell);
		}
		
		
		
		if(delNoteV1){
			PdfPCell footerblankcell2 = new PdfPCell(leftBottomFootertab);
			footerblankcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			footerblankcell2.setBorderWidthTop(0);
			footerblankcell2.setBorderWidthRight(0);
			footerblankcell2.setPaddingBottom(5);
			outerFootertab.addCell(footerblankcell2);
		}else{
			Phrase footerblank2=new Phrase (" ",font8);
			PdfPCell footerblankcell2 = new PdfPCell(footerblank2);
			footerblankcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			footerblankcell2.setBorderWidthTop(0);
			footerblankcell2.setBorderWidthRight(0);
			footerblankcell2.setPaddingBottom(5);
			outerFootertab.addCell(footerblankcell2);
		}
		
		
		PdfPCell bottomcell = new PdfPCell(rightFootertab);
		bottomcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		bottomcell.setBorderWidthTop(0);
		bottomcell.setBorderWidthLeft(0);
		outerFootertab.addCell(bottomcell);
		
		
		Phrase declarationph=new Phrase ("This is a Computer Generated Document ",font8);
		PdfPCell declarationcell = new PdfPCell(declarationph);
		declarationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		declarationcell.setBorder(0);
		declarationcell.setColspan(2);
		declarationcell.setPaddingTop(5);
		outerFootertab.addCell(declarationcell);
		
		
		try {
			document.add(outerFootertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void createAddressDetailTab() {
		//PdfPTable invoiceAddtab = new PdfPTable(3); //old
		PdfPTable invoiceAddtab = new PdfPTable(1); //Ashwini Patil
		invoiceAddtab.setWidthPercentage(100);
		String refNos="";
		Phrase blank = new Phrase("", font8);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
		
//		try {
//			invoiceAddtab.setWidths(new float[]{18,3,79});
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String invoiceNumber="";
		if(delNoteV1){
			invoiceNumber="Delivery Challan No.";
		}else{
			invoiceNumber="Invoice No.";
		}
		
		
		
		Phrase invoiceto = new Phrase(invoiceNumber, font8);
		PdfPCell invoicetocell = new PdfPCell(invoiceto);
		invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicetocell.setBorder(0);
		
		
//		Phrase compname = new Phrase(comp.getBusinessUnitName(), font8bold);
//		PdfPCell compnamecell = new PdfPCell(compname);
//		compnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		compnamecell.setColspan(3);
//		compnamecell.setBorder(0);
//		invoiceAddtab.addCell(compnamecell);
//		
//		
//		Phrase compadd = new Phrase(comp.getAddress().getCompleteAddress(), font8);
//		PdfPCell compaddcell = new PdfPCell(compadd);
//		compaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		compaddcell.setColspan(3);
//		compaddcell.setBorder(0);
//		invoiceAddtab.addCell(compaddcell);
//		
//		Phrase gstinph = new Phrase("GSTIN/UIN", font8);
//		PdfPCell gstincell = new PdfPCell(gstinph);
//		gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		gstincell.setBorder(0);
//		invoiceAddtab.addCell(gstincell);
//		
		Phrase colonph = new Phrase(":", font8);
		PdfPCell coloncell = new PdfPCell(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);

		String delChallanNo="";
		if(delNoteV1){
			if(delnoteEntity.getRefOrderNO()!=null){
				delChallanNo=delnoteEntity.getRefOrderNO();
			}else{
				delChallanNo="";
			}
		}else{
			delChallanNo=delnoteEntity.getInvoiceId()+"";
		}
		

		Phrase invoiceNoValph = new Phrase(": "+delChallanNo+"", font8);
		PdfPCell invoiceNoValcell = new PdfPCell(invoiceNoValph);
		invoiceNoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoiceNoValcell.setBorder(0);
		
		logger.log(Level.SEVERE,"delChallanNo="+delChallanNo);
		if(delChallanNo!=null&&!delChallanNo.equals("")&&!delChallanNo.equals("0")) {
//			invoiceAddtab.addCell(invoicetocell);
//			invoiceAddtab.addCell(coloncell);
//			invoiceAddtab.addCell(invoiceNoValcell);
			refNos=invoiceNumber+" : "+delChallanNo+"  ";
		}
		


		

		
		Phrase sales = new Phrase("Sales Order No.", font8);
		PdfPCell salesOrderNocell = new PdfPCell(sales);
		salesOrderNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		salesOrderNocell.setBorder(0);
		
		
		Phrase salesOrderNoValph = new Phrase(": "+delnoteEntity.getSalesOrderCount()+"", font8);
		
		PdfPCell salesOrderNoValcell = new PdfPCell(salesOrderNoValph);
		salesOrderNoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		salesOrderNoValcell.setBorder(0);
		
		logger.log(Level.SEVERE,"delnoteEntity.getSalesOrderCount()="+delnoteEntity.getSalesOrderCount());
		if(delnoteEntity.getSalesOrderCount()!=null&&delnoteEntity.getSalesOrderCount()!=0) {
			logger.log(Level.SEVERE,"adding sales order");
//			invoiceAddtab.addCell(salesOrderNocell); 	
//			invoiceAddtab.addCell(coloncell);
//			invoiceAddtab.addCell(salesOrderNoValcell);	
			refNos=refNos+"Sales Order No. : "+delnoteEntity.getSalesOrderCount();
		}
		

		Phrase refnosPh = new Phrase(refNos, font8);
		PdfPCell refnoscell = new PdfPCell(refnosPh);
		refnoscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		refnoscell.setBorder(0);
		invoiceAddtab.addCell(refnoscell);
		
		ServerAppUtility serverApp = new ServerAppUtility();

		String gstin = "", gstinText = "";
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			gstin = serverApp.getGSTINOfCompany(comp, delnoteEntity.getBranch().trim());
			System.out.println("gstin" + gstin);

		} else {
			gstinText = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinText);
		}

		Phrase gstinvalph = null;
		if (!gstin.trim().equals("")) {
			gstinvalph = new Phrase( gstin, font8);
		} else if (!gstinText.trim().equalsIgnoreCase("")) {
			gstinvalph = new Phrase(gstinText, font8);
		} else {
			gstinvalph = new Phrase("", font8);

		}
		
		PdfPCell gstinvalcell = new PdfPCell(gstinvalph);
		gstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell.setBorder(0);
//		invoiceAddtab.addCell(gstinvalcell);
		
		
		Phrase statename = new Phrase("State Name", font8);
		PdfPCell statenamecell = new PdfPCell(statename);
		statenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamecell.setBorder(0);
//		invoiceAddtab.addCell(statenamecell);
		
//		invoiceAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statecodetab=new PdfPTable(3);
		statecodetab.setWidthPercentage(100);
		try {
			statecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevalname = new Phrase(comp.getAddress().getState(), font8);
		PdfPCell statenamevalcell = new PdfPCell(statevalname);
		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevalcell.setBorder(0);
//		statecodetab.addCell(statenamevalcell);
		
		Phrase statecode = new Phrase("Code :", font8);
		PdfPCell statecodecell = new PdfPCell(statecode);
		statecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodecell.setBorder(0);
//		statecodetab.addCell(statecodecell);
		
		String stateCodeStr = serverApp.getStateOfCompany(comp,
				delnoteEntity.getBranch().trim(), stateList);
		Phrase stateCode = new Phrase(stateCodeStr, font8);

		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		stateCodeCell.setBorder(0);
//		statecodetab.addCell(stateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell statecodetabcell = new PdfPCell(statecodetab);
		statecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodetabcell.setBorder(0);
//		invoiceAddtab.addCell(statecodetabcell);
		
		
		Phrase cinph = new Phrase("CIN", font8);
		PdfPCell cincell = new PdfPCell(cinph);
		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cincell.setBorder(0);
//		invoiceAddtab.addCell(cincell);
		
//		invoiceAddtab.addCell(coloncell);
		
		String cinval=null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("CIN")) {

				cinval=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			}
			
		Phrase cinvalph = new Phrase(cinval, font8);
		PdfPCell cinvalphCell = new PdfPCell(cinvalph);
		cinvalphCell.setBorder(0);
//		invoiceAddtab.addCell(cinvalphCell);
			
		
		Phrase emailph = new Phrase("E-Mail", font8);
		PdfPCell emailcell = new PdfPCell(emailph);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailcell.setBorder(0);
//		invoiceAddtab.addCell(emailcell);
		
//		invoiceAddtab.addCell(coloncell);
		
		Phrase emailvalph = new Phrase(comp.getEmail(), font8);
		PdfPCell emailvalcell = new PdfPCell(emailvalph);
		emailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailvalcell.setBorder(0);
//		invoiceAddtab.addCell(emailvalcell);
		
		
		/*invoice to tab complet*/
		
		
		
		
		PdfPTable deliveryAddtab = new PdfPTable(3);
		deliveryAddtab.setWidthPercentage(100);
		
		try {
			deliveryAddtab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase despatchto = new Phrase("Despatch To", font8bold);
		PdfPCell despatchtocell = new PdfPCell(despatchto);
		despatchtocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		despatchtocell.setColspan(3);
		despatchtocell.setBorder(0);
		deliveryAddtab.addCell(despatchtocell);
		
		
		/**
		 * @author Anil ,Date : 16-04-2019
		 * picking up poc name from delivery note
		 */
		String pocName="";
		if(delnoteEntity.getCustPocName()!=null&&!delnoteEntity.getCustPocName().equals("")){
			pocName=delnoteEntity.getCustPocName();
		}else{
			pocName=cust.getCustomerName();
		}
//		Phrase compname2 = new Phrase(cust.getCustomerName(), font8);
		Phrase compname2 = new Phrase(pocName, font8);
		PdfPCell compnamecell2 = new PdfPCell(compname2);
		compnamecell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell2.setColspan(3);
		compnamecell2.setBorder(0);
		deliveryAddtab.addCell(compnamecell2);
		
		Phrase compadd2 = new Phrase(delnoteEntity.getShippingAddress().getCompleteAddress(), font8);
		PdfPCell compaddcell2 = new PdfPCell(compadd2);
		compaddcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		compaddcell2.setColspan(3);
		compaddcell2.setBorder(0);
		deliveryAddtab.addCell(compaddcell2);
		
		/**
		 * @author Anil , Date : 26-04-2019
		 */
		String email="";
		if(custBranch!=null){
			if(custBranch.getEmail()!=null&&!custBranch.getEmail().equals("")){
				email=custBranch.getEmail();
			}else{
				email=cust.getEmail();
			}
		}else{
			email=cust.getEmail();
		}
		
		Phrase devemailph = new Phrase("E-Mail", font8);
		PdfPCell devemailcell = new PdfPCell(devemailph);
		devemailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devemailcell.setBorder(0);
		
		
		Phrase devemailvalph = new Phrase(email, font8);
		PdfPCell devemailvalcell = new PdfPCell(devemailvalph);
		devemailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devemailvalcell.setBorder(0);
		
		if(email!=null&&!email.equals("")) {
			deliveryAddtab.addCell(devemailcell);	
			deliveryAddtab.addCell(coloncell);
			deliveryAddtab.addCell(devemailvalcell);
		}
		
		
		
		Phrase devgstinph = new Phrase("GSTIN/UIN", font8);
		PdfPCell devgstincell = new PdfPCell(devgstinph);
		devgstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devgstincell.setBorder(0);
		
		
		Phrase devcolonph = new Phrase(":", font8);
		PdfPCell devcoloncell = new PdfPCell(devcolonph);
		devcoloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devcoloncell.setBorder(0);
		
		
		
		String gstinnval=null;
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {

			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("GSTIN")) {

				gstinnval=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			}
		
		if(custBranch!=null&&custBranch.getGSTINNumber()!=null&&!custBranch.getGSTINNumber().equals("")){
			gstinnval=custBranch.getGSTINNumber();
		}
			
		Phrase gstinval=new Phrase(gstinnval,font8);
		PdfPCell devgstinvalcell = new PdfPCell(gstinval);
		devgstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devgstinvalcell.setBorder(0);
		
		
		if(gstinnval!=null&&!gstinnval.equals("")) {
			deliveryAddtab.addCell(devgstincell);
			deliveryAddtab.addCell(devcoloncell);
			deliveryAddtab.addCell(devgstinvalcell);
		}
		
		
		
		Phrase devstatename = new Phrase("State Name", font8);
		PdfPCell devstatenamecell = new PdfPCell(devstatename);
		devstatenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatenamecell.setBorder(0);
		deliveryAddtab.addCell(devstatenamecell);
		
		deliveryAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable devstatecodetab=new PdfPTable(3);
		devstatecodetab.setWidthPercentage(100);
		try {
			devstatecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase devstatevalname = new Phrase(delnoteEntity.getShippingAddress().getState(), font8);
		PdfPCell devstatenamevalcell = new PdfPCell(devstatevalname);
		devstatenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatenamevalcell.setBorder(0);
		devstatecodetab.addCell(devstatenamevalcell);
		
		Phrase devstatecode = new Phrase("Code :", font8);
		PdfPCell devstatecodecell = new PdfPCell(devstatecode);
		devstatecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatecodecell.setBorder(0);
		
		
		String devstateCodeStr = serverApp.getStateOfCompany(comp,
				delnoteEntity.getBranch().trim(), stateList);
		Phrase devstateCode = new Phrase(devstateCodeStr, font8);

		PdfPCell devstateCodeCell = new PdfPCell(devstateCode);
		devstateCodeCell.setBorder(0);
		
		
		if(devstateCodeStr!=null&&!devstateCodeStr.equals("")) {
			devstatecodetab.addCell(devstatecodecell);
			devstatecodetab.addCell(devstateCodeCell);
		}
		else{
			devstatecodetab.addCell(blankcell);
			devstatecodetab.addCell(blankcell);
		}
		
		/*state code tab complete*/
		
		PdfPCell devstatecodetabcell = new PdfPCell(devstatecodetab);
		devstatecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatecodetabcell.setBorder(0);
		deliveryAddtab.addCell(devstatecodetabcell);
		
		
		/* despatch to*/
		
		
		PdfPTable vendorAddtab = new PdfPTable(3);
		vendorAddtab.setWidthPercentage(100);
		
		try {
			vendorAddtab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase supplier = new Phrase("Supplier", font8);
		PdfPCell suppliercell = new PdfPCell(supplier);
		suppliercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		suppliercell.setColspan(3);
		suppliercell.setBorder(0);
		vendorAddtab.addCell(suppliercell);
		
		String companyname="";
		if(delNoteV1){
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyname= branchDt.getCorrespondenceName();
			}
			else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}
		}else{
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}
		
		
		Phrase compname3 = new Phrase(companyname, font8bold);
		PdfPCell compnamecell3 = new PdfPCell(compname3);
		compnamecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell3.setColspan(3);
		compnamecell3.setBorder(0);
		vendorAddtab.addCell(compnamecell3);
		
		Phrase vendoradd = new Phrase(comp.getAddress().getCompleteAddress(), font8);
		PdfPCell vendoraddcell = new PdfPCell(vendoradd);
		vendoraddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendoraddcell.setColspan(3);
		vendoraddcell.setBorder(0);
		vendorAddtab.addCell(vendoraddcell);
		
		Phrase vendorPhoneph = new Phrase("Phone No", font8);
		PdfPCell vendorPhonecell = new PdfPCell(vendorPhoneph);
		vendorPhonecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorPhonecell.setBorder(0);
		vendorAddtab.addCell(vendorPhonecell);
		
		vendorAddtab.addCell(coloncell);
		
		String cellNo="";
		if(delNoteV1){
			if(branchDt.getCellNumber2()!=null&branchDt.getCellNumber2()!=0){
				cellNo=branchDt.getCellNumber2()+"";	
			}else{
				cellNo="";
			}
			
		}else{
			cellNo=comp.getCellNumber1()+"";
		}
		
		
		Phrase vendorPhonephval = new Phrase(cellNo, font8);
		PdfPCell vendorPhonecellval = new PdfPCell(vendorPhonephval);
		vendorPhonecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorPhonecellval.setBorder(0);
		vendorAddtab.addCell(vendorPhonecellval);
		
		Phrase vendorConph = new Phrase("Contact No", font8);
		PdfPCell vendorConcell = new PdfPCell(vendorConph);
		vendorConcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorConcell.setBorder(0);
		vendorAddtab.addCell(vendorConcell);
		
		vendorAddtab.addCell(coloncell);
		
		String cellNo2="";
		if (delNoteV1) {
			if (branchDt.getLandline() != null&&branchDt.getLandline() != 0) {
				cellNo2 = branchDt.getLandline() + "";
			} else {
				cellNo2 = "";
			}

		}else{
			cellNo2=comp.getCellNumber2()+"";
		}
		
		
		Phrase vendorConphval = new Phrase(cellNo2, font8);
		PdfPCell vendorConcellval = new PdfPCell(vendorConphval);
		vendorConcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorConcellval.setBorder(0);
		vendorAddtab.addCell(vendorConcellval);
		
		Phrase emailVenph = new Phrase("E-Mail", font8);
		PdfPCell emailVencell = new PdfPCell(emailVenph);
		emailVencell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailVencell.setBorder(0);
		
		
		Phrase emailVenvalph = new Phrase(comp.getEmail(), font8);
		PdfPCell emailVenvalcell = new PdfPCell(emailVenvalph);
		emailVenvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailVenvalcell.setBorder(0);
		
		if(comp.getEmail()!=null&&!comp.getEmail().equals("")){
			vendorAddtab.addCell(emailVencell);		
			vendorAddtab.addCell(coloncell);
			vendorAddtab.addCell(emailVenvalcell);
		}
		
		
		Phrase gstinvendorph = new Phrase("GSTIN/UIN", font8);
		PdfPCell gstinvendorcell = new PdfPCell(gstinvendorph);
		gstinvendorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvendorcell.setBorder(0);
		
		
		
//		1String gstinvalue=null;
//		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
//
//			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("GSTIN")) {
//
//				gstinvalue=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
//			}
//			}

		
		ServerAppUtility serverApp2 = new ServerAppUtility();

		String gstin2 = "", gstinText2 = "";
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			gstin2 = serverApp2.getGSTINOfCompany(comp, delnoteEntity.getBranch().trim());
			System.out.println("gstin" + gstin);

		} else {
			gstinText2 = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinText);
		}

		Phrase gstinvalph2 = null;
		if (!gstin2.trim().equals("")) {
			gstinvalph2 = new Phrase( gstin2, font8);
		} else if (!gstinText.trim().equalsIgnoreCase("")) {
			gstinvalph2 = new Phrase(gstinText2, font8);
		} else {
			gstinvalph2 = new Phrase("", font8);

		}
		
		
		PdfPCell gstinvalcell2 = new PdfPCell(gstinvalph2);
		gstinvalcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell2.setBorder(0);
		
		if(gstinvalph2.getContent()!=null&&!gstinvalph2.getContent().equals("")){
			vendorAddtab.addCell(gstinvendorcell);
			vendorAddtab.addCell(coloncell);
			vendorAddtab.addCell(gstinvalcell2);			
		}
		
		
		Phrase statevendorname = new Phrase("State Name", font8);
		PdfPCell statenamevendorcell = new PdfPCell(statevendorname);
		statenamevendorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevendorcell.setBorder(0);
		vendorAddtab.addCell(statenamevendorcell);
		
		vendorAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statevendorcodetab=new PdfPTable(3);
		statevendorcodetab.setWidthPercentage(100);
		try {
			statevendorcodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevendorvalname = new Phrase(comp.getAddress().getState(), font8);
		PdfPCell statenamevendorvalcell = new PdfPCell(statevendorvalname);
		statenamevendorvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevendorvalcell.setBorder(0);
		statevendorcodetab.addCell(statenamevendorvalcell);
		
		
		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(comp.getAddress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase VendorstateCode=new Phrase(stCo,font8);
		PdfPCell venstateCodeCell = new PdfPCell(VendorstateCode);
		venstateCodeCell.setBorder(0);
		
		if(stCo!=null&&!stCo.equals("")){
			statevendorcodetab.addCell(statecodecell);
			statevendorcodetab.addCell(venstateCodeCell);
		}else{
			statevendorcodetab.addCell(blankcell);
			statevendorcodetab.addCell(blankcell);
		}
		
		/*state code tab complete*/
		
		PdfPCell venstatecodecell = new PdfPCell(statevendorcodetab);
		venstatecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		venstatecodecell.setBorder(0);
		vendorAddtab.addCell(venstatecodecell);
		
		
		PdfPTable leftoutertab=new PdfPTable(1);
		leftoutertab.setWidthPercentage(100);
			
		PdfPCell invoicecell=new PdfPCell(invoiceAddtab);
		invoicecell.setPaddingBottom(5);
		PdfPCell delivery=new PdfPCell(deliveryAddtab);
		delivery.setPaddingBottom(5);
		PdfPCell vendorcell=new PdfPCell(vendorAddtab);
		vendorcell.setPaddingBottom(5);
		
		leftoutertab.addCell(invoicecell);
		leftoutertab.addCell(delivery);
		leftoutertab.addCell(vendorcell);
		
		/*Left Tab complete***/
		
		PdfPTable dateAndVoucherTab=new PdfPTable(2);
		dateAndVoucherTab.setWidthPercentage(100);
		try {
			dateAndVoucherTab.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Phrase vouchernoph = new Phrase("Voucher No", font8);
		PdfPCell vouchernocell = new PdfPCell(vouchernoph);
		vouchernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vouchernocell.setBorderWidthBottom(0);
		dateAndVoucherTab.addCell(vouchernocell);
		
		Phrase datedph = new Phrase("Dated", font8);
		PdfPCell datedcell = new PdfPCell(datedph);
		datedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedcell.setBorderWidthBottom(0);
		dateAndVoucherTab.addCell(datedcell);
		
		Phrase vouchernovalph = new Phrase(delnoteEntity.getCount()+"", font8);
		PdfPCell vouchernoValcell = new PdfPCell(vouchernovalph);
		vouchernoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vouchernoValcell.setBorderWidthTop(0);
		dateAndVoucherTab.addCell(vouchernoValcell);
		
		Phrase datedValph = new Phrase(fmt.format(delnoteEntity.getDeliveryDate()), font8);
		PdfPCell datedValcell = new PdfPCell(datedValph);
		datedValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedValcell.setBorderWidthTop(0);
		dateAndVoucherTab.addCell(datedValcell);
		
//		Phrase createddayph = new Phrase("Created Days", font8);
//		PdfPCell createdDaycell = new PdfPCell(createddayph);
//		createdDaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		createdDaycell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(createdDaycell);
//		
//		Phrase modeph = new Phrase("Mode/Terms of Payment", font8);
//		PdfPCell modecell = new PdfPCell(modeph);
//		modecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		modecell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(modecell);
//		
////		int days=0;
////		for (int i = 0; i <po.getPaymentTermsList().size(); i++) {
////			days=po.getPaymentTermsList().get(0).getPayTermDays();
////		}
//		
//		Phrase createddayValph = new Phrase("", font8);
//		PdfPCell createdDayValcell = new PdfPCell(createddayValph);
//		createdDayValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		createdDayValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(createdDayValcell);
//		
//		
//		
////		double percent=0;
////		String comment=null;
////		for (int i = 0; i <po.getPaymentTermsList().size(); i++) {
////			percent=po.getPaymentTermsList().get(0).getPayTermPercent();
////			comment=po.getPaymentTermsList().get(0).getPayTermComment();
////		}
//		
//		Phrase modeValph = new Phrase(" ", font8);
//		PdfPCell modeValcell = new PdfPCell(modeValph);
//		modeValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		modeValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(modeValcell);
		
		/**
		 * @author Anil , Date : 31-07-2019
		 * Showing ewaybill number
		 * raised by Sasha, sonu
		 */
		if(delnoteEntity!=null&&delnoteEntity.getEwayBillNumber()!=null&&!delnoteEntity.getEwayBillNumber().equals("")){
			Phrase supplierph = new Phrase("Eway Bill Number", font8);
			PdfPCell supplicell = new PdfPCell(supplierph);
			supplicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			supplicell.setBorderWidthBottom(0);
			supplicell.setBorderWidthRight(0);
			dateAndVoucherTab.addCell(supplicell); 
			
			supplierph = new Phrase(": "+delnoteEntity.getEwayBillNumber(), font8);
			supplicell = new PdfPCell(supplierph);
			supplicell.setHorizontalAlignment(Element.ALIGN_LEFT);
			supplicell.setBorderWidthBottom(0);
			supplicell.setBorderWidthLeft(0);
			dateAndVoucherTab.addCell(supplicell); 
		}
		
		Phrase supplierph = new Phrase("Supplier's Ref/Order No", font8);
		PdfPCell supplicell = new PdfPCell(supplierph);
		supplicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		supplicell.setBorderWidthBottom(0);
		supplicell.setColspan(2);
//		dateAndVoucherTab.addCell(supplicell);
		
//		Phrase otherph = new Phrase("Other Reference", font8);
//		PdfPCell othercell = new PdfPCell(otherph);
//		othercell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		othercell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(othercell);
		
		/***
		 * Date 2-2-2019 
		 @author AMOL
		 added refernce order no for change the format of refno for sasha**********/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("DeliveryNote", "RefrenceOrderNumber", delnoteEntity.getCompanyId())){
			refOrderNo=true;
		}
		
		Phrase suppliervalph=null;
		if(refOrderNo){
		if(delnoteEntity.getRefOrderNO()!=null){
			 suppliervalph = new Phrase(delnoteEntity.getRefOrderNO(), font8);
		}
		}
		else{
		   if(delnoteEntity.getReferenceNumber()!=null){
				 suppliervalph = new Phrase(delnoteEntity.getReferenceNumber(), font8);
			}
				}
	
	
		PdfPCell supplivalcell = new PdfPCell(suppliervalph);
		supplivalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		supplivalcell.setBorderWidthTop(0);
		supplivalcell.setColspan(2);
//		dateAndVoucherTab.addCell(supplivalcell);
		
//		Phrase otherValph = new Phrase(" ", font8);
//		PdfPCell otherValcell = new PdfPCell(otherValph);
//		otherValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		otherValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(otherValcell);
		
//		Phrase despathph = new Phrase("Despath Through", font8);
//		PdfPCell despathcell = new PdfPCell(despathph);
//		despathcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		despathcell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(despathcell);
//		
//		
//		Phrase destinationph = new Phrase("Destination", font8);
//		PdfPCell destinationcell = new PdfPCell(destinationph);
//		destinationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		destinationcell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(destinationcell);
//		
//		Phrase despathValph=null;
//		if(delnoteEntity.getDriverName()!=null){
//			 despathValph = new Phrase(delnoteEntity.getDriverName(), font8);
//		}
//		else{
//			 despathValph = new Phrase("  ", font8);
//		}
//		
//		PdfPCell despathValcell = new PdfPCell(despathValph);
//		despathValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		despathValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(despathValcell);
//		
//		
//		
////		String warehouse=null;
////		for (int i = 0; i <po.getProductDetails().size(); i++) {
////			warehouse=po.getProductDetails().get(0).getItemProductWarehouseName();
////		}
//		
////		Phrase destinationValph="";
////		if(po.getPoWarehouseName()!=null){
////			
////			 destinationValph = new Phrase(po.getPoWarehouseName(), font8);
////		}
////		else{
////			destinationValph = new Phrase("  ", font8);
////		}
//		Phrase destinationValph = new Phrase("  ", font8);
//		PdfPCell destinationValphcell = new PdfPCell(destinationValph);
//		destinationValphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		destinationValphcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(destinationValphcell);
		

		//Ashwini Patil
		Phrase transportmode=new Phrase("Transport Mode",font8);
		PdfPCell transportmodeCell=new PdfPCell(transportmode);
		transportmodeCell.setBorder(0);
		transportmodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String transMode="";
		if(delnoteEntity.getVehicleName()!=null){
			transMode=delnoteEntity.getVehicleName();
		}
		Phrase transModeVal=new Phrase(": "+transMode,font8);
		PdfPCell transModeValCell=new PdfPCell(transModeVal);
		transModeValCell.setBorder(0);
		transModeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase vechicleNo=new Phrase("Vehicle Number",font8);
		PdfPCell vechicleNoCell=new PdfPCell(vechicleNo);
		vechicleNoCell.setBorder(0);
		vechicleNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String vechicalnumber="";
		if(delnoteEntity.getVehicleNo()!=null){
			vechicalnumber=delnoteEntity.getVehicleNo();
		}
		
		Phrase vechicleNoValue=new Phrase(": "+vechicalnumber,font8);
		PdfPCell vechicleNoValueCell=new PdfPCell(vechicleNoValue);
		vechicleNoValueCell.setBorder(0);
		vechicleNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		//  delivery date
		
		Phrase deliveryDate=new Phrase("Date of Supply",font8);
		PdfPCell deliveryDateCell=new PdfPCell(deliveryDate);
		deliveryDateCell.setBorder(0);
		deliveryDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		Phrase deliveryDateValue=new Phrase(": "+fmt.format(delnoteEntity.getDeliveryDate()),font8);
		PdfPCell deliveryDateValueCell=new PdfPCell(deliveryDateValue);
		deliveryDateValueCell.setBorder(0);
		deliveryDateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		//   place of supply 
		
		Phrase placeOfSupply=new Phrase("Place Of Supply",font8);
		PdfPCell placeOfSupplyCell=new PdfPCell(placeOfSupply);
		placeOfSupplyCell.setBorder(0);
		placeOfSupplyCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		Phrase placeOfSupplyValue=new Phrase(": "+delnoteEntity.getShippingAddress().getCity(),font8);
		PdfPCell placeOfSupplyValueCell=new PdfPCell(placeOfSupplyValue);
		placeOfSupplyValueCell.setBorder(0);
		placeOfSupplyValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		if(transMode!=null&&!transMode.equals("")) {
			dateAndVoucherTab.addCell(transportmodeCell);
			dateAndVoucherTab.addCell(transModeValCell);
		}
		
		if(vechicalnumber!=null&&!vechicalnumber.equals("")) {
			dateAndVoucherTab.addCell(vechicleNoCell);
			dateAndVoucherTab.addCell(vechicleNoValueCell);			
		}
		
		if(delnoteEntity.getDeliveryDate()!=null) {
			dateAndVoucherTab.addCell(deliveryDateCell);
			dateAndVoucherTab.addCell(deliveryDateValueCell);			
		}
		if(delnoteEntity.getShippingAddress().getCity()!=null&&!delnoteEntity.getShippingAddress().getCity().equals("")) {
			dateAndVoucherTab.addCell(placeOfSupplyCell);
			dateAndVoucherTab.addCell(placeOfSupplyValueCell);		
		}		
		//end
		
		Phrase termsOfDelph = new Phrase("Terms Of Delivery", font8);
		PdfPCell termsOfDelcell = new PdfPCell(termsOfDelph);
		termsOfDelcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		termsOfDelcell.setBorderWidthBottom(0);
		termsOfDelcell.setBorder(0);
		termsOfDelcell.setColspan(2);
		dateAndVoucherTab.addCell(termsOfDelcell);
		
		Phrase termsOfDelValph=null;
//		if(po.getTermsOfDelivery()!=null){
//			 termsOfDelValph = new Phrase(po.getTermsOfDelivery(), font8);
//		}
//		else{
//			 termsOfDelValph = new Phrase(po.getTermsOfDelivery(), font8);
//		}
		 termsOfDelValph = new Phrase(delnoteEntity.getDescription(), font8);
		PdfPCell termsOfDelValcell = new PdfPCell(termsOfDelValph);
		termsOfDelValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		termsOfDelValcell.setBorderWidthTop(0);
		termsOfDelValcell.setBorder(0);
		termsOfDelValcell.setColspan(2);
		dateAndVoucherTab.addCell(termsOfDelValcell);
		
		
		PdfPTable mainout=new PdfPTable(2) ;
		mainout.setWidthPercentage(100);
		
		try {
			mainout.setWidths(new float[]{60,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell leftcell=new PdfPCell(leftoutertab);
		PdfPCell rightCell=new PdfPCell(dateAndVoucherTab);
		
		mainout.addCell(leftcell);
		mainout.addCell(rightCell);
		
		
		try {
			document.add(mainout);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void Createtitle() {
		
		PdfPTable titleTab = new PdfPTable(1);
		titleTab.setWidthPercentage(100);
		
		Phrase titlephbl = new Phrase(" ", font10);
		PdfPCell titleCellbl = new PdfPCell(titlephbl);
		titleCellbl.setBorder(1); //changed 0 to 1
//		titleTab.addCell(titleCellbl);//commented
//		titleTab.addCell(titleCellbl);
//		titleTab.addCell(titleCellbl); 
		
		
	/**27-12-2018 added by amol for printing page number with header**/
		
		String pageTitle="";
		if(noOfPage>1){
			pageTitle="DELIVERY NOTE(Page "+noOfPage+")";
		}else{
			pageTitle="DELIVERY NOTE";
		}
		Phrase titleph1 = new Phrase(pageTitle, font12bold);
		
		PdfPCell titleCell1 = new PdfPCell(titleph1);
//		titleCell1.setBorder(1);
		titleCell1.setBorder(Rectangle.BOX);
		titleCell1.setPaddingBottom(10);
		titleCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(titleCell1);
		
		
		
//		Phrase titleph = new Phrase("DELIVERY NOTE", font12bold);
//		PdfPCell titleCell = new PdfPCell(titleph);
//		titleCell.setBorder(0);
//		titleCell.setPaddingBottom(10);
//		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		titleTab.addCell(titleCell);
		
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void Createletterhead() {
		/**** image ***/
		DocumentUpload letterheaddocument = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ letterheaddocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.setBorder(0);
			imageSignCell.setImage(image2);
			// imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		Image image1 = null;
//		try {
//			image1 = Image.getInstance("images/Capture.JPG");
//			image1.scalePercent(20f);
//			// image1.setAbsolutePosition(40f,765f);
//			// doc.add(image1);
//
//			imageSignCell = new PdfPCell(image1);
//			// imageSignCell.addElement();
//			imageSignCell.setImage(image1);
//			// imageSignCell.setFixedHeight(40);
//			imageSignCell.setBorder(0);
//			// imageSignCell.setPaddingTop(8);
//			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		PdfPTable letterheadTab = new PdfPTable(1);
		letterheadTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			letterheadTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			letterheadTab.addCell(logoblankcell);
		}

		try {
			document.add(letterheadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	//Ashwini Patil 
	private void createHeader(int value) 
	{
		
		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setWidthPercentage(100f);
		mytbale.setSpacingAfter(5f);
		mytbale.setSpacingBefore(5f);
		
		Image uncheckedImg=null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(9);

		
		Image checkedImg=null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(9);

		
		Phrase myblank=new Phrase("   ",font10);
		PdfPCell myblankCell=new PdfPCell(myblank);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero=new Phrase(" ",font10);
		PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
		Paragraph para1=new Paragraph();
		para1.setIndentationLeft(10f);
		para1.add(myblank);
		if(value==0){
			para1.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para1.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		
		para1.add(stat1Phrase);
		para1.setAlignment(Element.ALIGN_MIDDLE);
		
		PdfPCell stat1PhraseCell=new PdfPCell(para1);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
		Paragraph para2=new Paragraph();
		para2.setIndentationLeft(10f);
		
		if(value==1){
			para2.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para2.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		
		para2.add(stat2Phrase);
		para2.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell stat2PhraseCell=new PdfPCell(para2);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
		Paragraph para3=new Paragraph();
		para3.setIndentationLeft(10f);
		
		if(value==2){
			para3.add(new Chunk(checkedImg, 0, 0, true));	
		}
		else{
			para3.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		para3.add(stat3Phrase);
		para3.setAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPCell stat3PhraseCell=new PdfPCell(para3);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		mytbale.addCell(stat1PhraseCell);
		mytbale.addCell(stat2PhraseCell);
		mytbale.addCell(stat3PhraseCell);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(mytbale);
		tab.addCell(cell);
		try {
			document.add(tab);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
	}

}
