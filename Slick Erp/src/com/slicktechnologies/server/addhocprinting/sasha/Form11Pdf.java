package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class Form11Pdf {
	public Document document;
	Company company;
	Employee employee;
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD| Font.UNDERLINE);
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	EmploymentCardPdf cell=new EmploymentCardPdf();
	
	EmployeeAdditionalDetails empAdditionalDet=null;

	EmployeeFamilyDeatails employeefamilyDetalis;
	
	public Form11Pdf() {
		// TODO Auto-generated constructor stub
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setForm11Data(long companyId,int empId){
		company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		if(company!=null){
			employee=ofy().load().type(Employee.class).filter("companyId", companyId).filter("count", empId).first().now();
		}
		
		if(employee!=null){
			empAdditionalDet=ofy().load().type(EmployeeAdditionalDetails.class).filter("companyId", companyId).filter("empInfo.empCount", empId).first().now();
		}
	}
	
	public void createPdf(){
		createFormHeading();
		createEmployeePfDetails();
		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		try {
			  document.add(nextpage);
		  } catch (DocumentException e1) {
			  e1.printStackTrace();
		  }
		createUndertakingDetails();
		createDeclarationByPresentEmployer();
		footerTable();
	}

	private void createFormHeading() {
		// TODO Auto-generated method stub
//		float[] columnWidths = { 1.5f, 4.5f, 4f};
		PdfPTable table=new PdfPTable(1);
		table.setWidthPercentage(100f);
//		try {
//			table.setWidths(columnWidths);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		table.addCell(cell.getCell("Composite Declaration Form-11 ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("(To be retained by the employer for future reference) ", font10bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
//		table.addCell(cell.getCell(" ", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("(To be retained by the employer for future reference)", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
//		table.addCell(cell.getCell(" ", font8bold, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
		
		table.addCell(cell.getCell("EMPLOYEES' PROVIDENT FUND ORGANIZATION", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		PdfPCell empCodeCell=cell.getCell("Emp Code: "+employee.getCount(), font9, Element.ALIGN_LEFT, 0, 0, 0);
//		empCodeCell.setBorderWidthBottom(0);
//		empCodeCell.setBorderWidth(1.5f);
//		empCodeCell.setPadding(3);
//		table.addCell(empCodeCell);
		
		
		table.addCell(cell.getCell("Employees' Provident Funds Scheme, 1952(paragraph 34 & 57) & ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		table.addCell(cell.getCell("Employees' pension scheme 1995 (paragraph 24)", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		PdfPCell companyCell=cell.getCell("Company: "+company.getBusinessUnitName(), font9, Element.ALIGN_LEFT, 0, 0, 0);
//		companyCell.setBorderWidthTop(0);
//		companyCell.setBorderWidth(1.5f);
//		companyCell.setPadding(3);
//		table.addCell(companyCell);
		
		table.addCell(cell.getCell("(Declaration by a person taking up employment in any establishment on which EPF Scheme, 1952 and /or EPS, 1995 is applicable)", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createEmployeePfDetails() {
		// TODO Auto-generated method stub
		float[] columnWidths = { 0.5f, 5.5f, 4f};
		PdfPTable table=new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		table.addCell(cell.getCell("1", font8, Element.ALIGN_CENTER, 0, 0, 15));
		table.addCell(cell.getCell("Name of the member", font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell(employee.getFullName(), font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		String father_SpouseName="";
		boolean spouseFlag=false;
		boolean fatherFlag=false;
//		if(empAdditionalDet!=null){
//			for(EmployeeFamilyDeatails fam:empAdditionalDet.getEmpFamDetList()){
//				if(fam.getFamilyRelation().trim().equalsIgnoreCase("Husband")){
//					father_SpouseName=fam.getFname()+" "+fam.getMname()+" "+fam.getLname();
//					spouseFlag=true;
//				}else if(fam.getFamilyRelation().trim().equalsIgnoreCase("Father")){
//					father_SpouseName=fam.getFname()+" "+fam.getMname()+" "+fam.getLname();
//					fatherFlag=true;
//				}
//			}
//		}
		Image blankImg = null;
		try {
			blankImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		blankImg.scalePercent(4);
		blankImg.scaleAbsoluteHeight(4);
		blankImg.scaleAbsoluteWidth(4);
		
		PdfPCell uncheckCell=new PdfPCell(blankImg);
		uncheckCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		uncheckCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		uncheckCell.setBorder(0);
		
//		table.addCell(cell.getCell("2", font8, Element.ALIGN_CENTER, 2, 0, 15));
//		table.addCell(getFatherOrSpouseNameTbl(fatherFlag, spouseFlag)).setBorderWidthBottom(0);
//		table.addCell(cell.getCell("Father's Name( ) Spouse's Name ( )", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidthBottom(0);
//		table.addCell(cell.getCell(father_SpouseName, font8, Element.ALIGN_LEFT, 2, 0, 0));
//		table.addCell(cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidthTop(0);
//		table.addCell(cell.getCell("Father's Name    "+blankImg, font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthBottom(0);
//		
//		table.addCell(cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthBottom(0);
//		table.addCell(cell.getCell("Spouse's Name    "+blankImg, font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
//		
//		table.addCell(cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
		
		float[] columnWidths8 = {30,70};
		PdfPTable fatherTable=new PdfPTable(2);
		fatherTable.setWidthPercentage(100f);
		try {
			fatherTable.setWidths(columnWidths8);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		PdfPCell uncheckCell2=new PdfPCell(blankImg);
		uncheckCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		uncheckCell2.setBorder(0);
		
		fatherTable.addCell(cell.getCell("Father's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		fatherTable.addCell(uncheckCell2).setBorder(0);
		
		fatherTable.addCell(cell.getCell("Spouse's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		fatherTable.addCell(uncheckCell2).setBorder(0);
		
		
		float[] columnWidths15 = {100};
		PdfPTable blankTable=new PdfPTable(1);
		blankTable.setWidthPercentage(100f);
		try {
			blankTable.setWidths(columnWidths15);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
//		String spouseName="";
//		if(empAdditionalDet!=null){
//			for(EmployeeFamilyDeatails familydetails:empAdditionalDet.getEmpFamDetList()){
//				if(familydetails.getFamilyRelation().trim().equalsIgnoreCase("Husband")){
//					spouseName=familydetails.getFname()+" "+familydetails.getMname()+" "+familydetails.getLname();
//				}else{
//					spouseName="";
//				}
//			}
//		}
//		String fatherName="";
//		if(empAdditionalDet!=null){
//			for(EmployeeFamilyDeatails familydetails:empAdditionalDet.getEmpFamDetList()){
//				if(familydetails.getFamilyRelation().trim().equalsIgnoreCase("Father")){
//					fatherName=familydetails.getFname()+" "+familydetails.getMname()+" "+familydetails.getLname();
//				}else{
//					fatherName="";
//				}
//			}
//		}
	
		String fatherName="";
		 String husbandName="";
		 if(empAdditionalDet!=null){
				for(EmployeeFamilyDeatails fam:empAdditionalDet.getEmpFamDetList()){
					if(fam.getFamilyRelation().trim().equalsIgnoreCase("Husband")&&fam.getFamilyRelation().trim().equalsIgnoreCase("HUSBAND")){
						husbandName=fam.getFname()+" "+fam.getMname()+" "+fam.getLname();
						spouseFlag=true;
					}else if(fam.getFamilyRelation().trim().equalsIgnoreCase("Father")&&fam.getFamilyRelation().trim().equalsIgnoreCase("FATHER")){
						fatherName=fam.getFname()+" "+fam.getMname()+" "+fam.getLname();
						fatherFlag=true;
					}
				}
			}
		
		
		
			PdfPCell fatherCell=cell.getCell(fatherName, font8, Element.ALIGN_LEFT, 0, 0, 0);
		    fatherCell.setBorder(0);
		    blankTable.addCell(fatherCell);
		
			PdfPCell husBandCell=cell.getCell(husbandName, font8, Element.ALIGN_LEFT, 0, 0, 0);
		    husBandCell.setBorder(0);
		    blankTable.addCell(husBandCell);
		
		
		
		
		
		
//		table.addCell(cell.getCell("2", font8, Element.ALIGN_CENTER, 0, 0, 15)).setBorderWidthTop(0);
		PdfPCell firstcell=cell.getCell("2", font8, Element.ALIGN_CENTER, 0, 0, 0);
//		firstcell.addElement(fatherTable);
		table.addCell(firstcell);
		
		PdfPCell fatherSpouseCell=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0);
		fatherSpouseCell.addElement(fatherTable);
		table.addCell(fatherSpouseCell);
		
		PdfPCell thirdCell=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0);
		thirdCell.addElement(blankTable);
		
		table.addCell(thirdCell);
		
		
//		table.addCell(cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		table.addCell(cell.getCell("3", font8, Element.ALIGN_CENTER, 0, 0, 0));
		
		float[] columnWidths10 = {20,80};
		PdfPTable dobTable=new PdfPTable(2);
		dobTable.setWidthPercentage(100f);
		try {
			dobTable.setWidths(columnWidths10);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		PdfPCell dobCell=cell.getCell("Date of Birth: ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		dobCell.setBorder(0);
		dobTable.addCell(dobCell);
		PdfPCell dobCell2=cell.getCell("(DD/MM/YYYY)", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		dobCell2.setBorder(0);
		dobTable.addCell(dobCell2);
		
		PdfPCell dateOfBirth=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0);
		dateOfBirth.addElement(dobTable);
//		dateOfBirth.setBorder(0);
		table.addCell(dateOfBirth);
		
//		table.addCell(cell.getCell("Date of Birth: (DD/MM/YYYY)", font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		
		table.addCell(cell.getCell(format.format(employee.getDob()), font8, Element.ALIGN_LEFT, 0, 0, 0));
		
		table.addCell(cell.getCell("4", font8, Element.ALIGN_CENTER, 0, 0, 15));
		table.addCell(cell.getCell("Gender: (Male / Female / Transgender)", font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell(employee.getGender(), font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		table.addCell(cell.getCell("5", font8, Element.ALIGN_CENTER, 0, 0, 15));
		table.addCell(cell.getCell("Marital Status (Married / Unmarried / Widow / Widower/Divorcee)", font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell(employee.getMaritalStatus(), font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		table.addCell(cell.getCell("6", font8, Element.ALIGN_CENTER, 2, 0, 15));
		table.addCell(cell.getCell("(a)Email ID: ", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthBottom(0);
		table.addCell(cell.getCell(employee.getEmail(), font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell("(b)Mobile No.: ", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
		table.addCell(cell.getCell(employee.getCellNumber1()+"", font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		table.addCell(cell.getCell("7", font8, Element.ALIGN_CENTER, 2, 0, 15));
		table.addCell(cell.getCell("Present employment details:", font8bold, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthBottom(0);
//		table.addCell(getCheckBoxYesNoTbl(false,true));
		table.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 15)).setBorderWidthBottom(0);
		table.addCell(cell.getCell("Date of joining in the current establishment (DD/MM/YYYY)", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
		table.addCell(cell.getCell(format.format(employee.getJoinedAt())+"", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
		
		
		table.addCell(cell.getCell("8", font8, Element.ALIGN_CENTER, 5, 0, 15));
		float[] columnWidths11 = {20,80};
		PdfPTable kycTable=new PdfPTable(2);
		kycTable.setWidthPercentage(100f);
		try {
			kycTable.setWidths(columnWidths11);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		PdfPCell kycCell=cell.getCell("KYC Details:", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		kycCell.setBorder(0);
		kycTable.addCell(kycCell);
		PdfPCell kycDetail=cell.getCell("(attach self attested copies of following KYCs)", font8, Element.ALIGN_LEFT, 0, 0, 0);
		kycDetail.setBorder(0);
		kycTable.addCell(kycDetail);
		
		PdfPCell kycDetails=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
//		kycDetails.setBorder(0);
		kycDetails.addElement(kycTable);
		table.addCell(kycDetails);
		table.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0));
		
		
//		table.addCell(cell.getCell("KYC Details:(attach self attested copies of following KYCs)", font8, Element.ALIGN_LEFT, 0, 0, 15));
//		table.addCell(getCheckBoxYesNoTbl(false,true));
		
		table.addCell(cell.getCell("a)Bank Account No.:", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthBottom(0);
		table.addCell(cell.getCell(employee.getEmployeeBankAccountNo()+"", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthBottom(0);
		table.addCell(cell.getCell("b)IFS Code of the branch:", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
		table.addCell(cell.getCell(employee.getIfscCode()+"", font8, Element.ALIGN_LEFT, 0, 0, 15)).setBorderWidthTop(0);
		table.addCell(cell.getCell("c)AADHAR Number", font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell(employee.getAadharNumber()+"", font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell("d)Permanent Account Number(PAN) , if available", font8, Element.ALIGN_LEFT, 0, 0, 15));
		table.addCell(cell.getCell(employee.getEmployeePanNo()+"", font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		
		String exitDt="";
		if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
			exitDt=employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getToDate();
		}
		
		table.addCell(cell.getCell("9", font8, Element.ALIGN_CENTER, 0, 0, 15));
		
	
		
		
		
		table.addCell(cell.getCell("Whether earlier a member of Employees' Provident Fund Scheme,1952", font8, Element.ALIGN_LEFT, 0, 0, 15));
//		table.addCell(cell.getCell(" a) Universal Account Number(UAN) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(employee.getUANno(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidth(1.5f);
		PdfPCell cell9=null;
		if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
	    if(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getPfNumber().equals("")){
		   cell9=cell.getCell("No", font8, Element.ALIGN_LEFT, 0, 0, 15);
		}else{
			 cell9=cell.getCell("Yes", font8, Element.ALIGN_LEFT, 0, 0, 15);
		}}else{
			cell9=cell.getCell("No", font8, Element.ALIGN_LEFT, 0, 0, 15);
		}
			
		
		table.addCell(cell9);
		
//		table.addCell(cell.getCell("b) Previous PF a/c No ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidth(1.5f);
//		table.addCell(getPfAcNoTbl());
//		table.addCell(getBlankPfAcNoTbl());
		
		String schemeCert="";
		String penPayNum="";
		
		if(employee.getSchemeCertificateNum()!=null){
			schemeCert=employee.getSchemeCertificateNum();
		}
		if(employee.getPensionPaymentNum()!=null){
			penPayNum=employee.getPensionPaymentNum();
		}
		
//		table.addCell(cell.getCell(" c) Date of exit from previous employment (DD/MM/YYYY) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(exitDt, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidth(1.5f);
//		table.addCell(cell.getCell(" d) Scheme Certificate No (If Issued) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(schemeCert, font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(" e) Pension Payment Order (PPO)No (If Issued) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(penPayNum, font8, Element.ALIGN_LEFT, 0, 0, 0));
		
		
		boolean nhFlag=true;
		String countryName="";
		if(employee.getCountry().trim().equalsIgnoreCase("India")){
			nhFlag=false;
		}else{
			countryName=employee.getCountry();
		}
		
		String validity="";
		
		if(employee.getPassPortInformation()!=null&&employee.getPassPortInformation().getIssueDate()!=null){
			validity=format.format(employee.getPassPortInformation().getIssueDate());
			if(employee.getPassPortInformation().getExpiryDate()!=null){
				validity=validity+" To "+format.format(employee.getPassPortInformation().getExpiryDate());
			}
		}
		
		
		table.addCell(cell.getCell("10", font8, Element.ALIGN_CENTER, 0, 0, 15));
		table.addCell(cell.getCell("Whether earlier a member of Employees' Pension Scheme ,1995", font8, Element.ALIGN_LEFT, 0, 0, 15));
		
		
		
		
		
		PdfPCell cell10=null;
		if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
		if(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getPfNumber().equals("")){
			cell10=cell.getCell("No", font8, Element.ALIGN_LEFT, 0, 0, 15);
		}else{
			cell10=cell.getCell("Yes", font8, Element.ALIGN_LEFT, 0, 0, 15);
		}}else{
			cell10=cell.getCell("No", font8, Element.ALIGN_LEFT, 0, 0, 15);
		}
		
		table.addCell(cell10);
//		table.addCell(cell.getCell(" a) International Worker: ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(getCheckBoxYesNoTbl(nhFlag,false));
//		
//		table.addCell(cell.getCell(" b) If Yes , State Country Of Origin(India/Name of Other Country) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(countryName, font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(" c) Passport No ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(employee.getPassPortInformation().getPassportNumber(), font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(" d) Validity Of Passport (DD/MM/YYYY) to (DD/MM/YYYY)) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(validity, font8, Element.ALIGN_LEFT, 0, 0, 0));
		
		String bankDet="";
		if(employee.getEmployeeBankAccountNo()!=null&&!employee.getEmployeeBankAccountNo().equals("")){
			bankDet=bankDet+employee.getEmployeeBankAccountNo();
		}
		if(employee.getIfscCode()!=null&&!employee.getIfscCode().equals("")){
			if(!bankDet.equals("")){
				bankDet=bankDet+"/"+employee.getIfscCode();
			}else{
				bankDet=employee.getIfscCode();
			}
		}
		
		String aadharNum="";
		if(employee.getAadharNumber()!=0){
			aadharNum=employee.getAadharNumber()+"";
		}
		String panNum="";
//		if(employee.getArticleTypeDetails()!=null&&employee.getArticleTypeDetails().size()!=0){
//			for(ArticleType type:employee.getArticleTypeDetails()){
//				if(type.getArticleTypeName().contains("Pan")){
//					panNum=type.getArticleTypeValue();
//					break;
//				}
//			}
//		}
		if(employee.getEmployeePanNo()!=null){
			panNum=employee.getEmployeePanNo();
		}
		
//		table.addCell(cell.getCell("11", font8, Element.ALIGN_CENTER, 4, 0, 0));
//		table.addCell(cell.getCell("KYC Details:(attach Self attested copies of following KYCs)**", font8bold, Element.ALIGN_CENTER, 0, 2, 0)).setBorderWidth(1.5f);
//		table.addCell(cell.getCell(" a) Bank Account No. & IFS code ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(bankDet , font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidth(1.5f);
//		table.addCell(cell.getCell(" b) AADHAR Number(12 Digit) ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(aadharNum, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidth(1.5f);
//		table.addCell(cell.getCell(" c) Permanent Account Number(PAN),If available ", font8, Element.ALIGN_LEFT, 0, 0, 0));
//		table.addCell(cell.getCell(panNum, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorderWidth(1.5f);
		
		
		
		
		
		
		
		
		
		
		
		
			PdfPTable unexmptedTable=new PdfPTable(8);
			unexmptedTable.setWidthPercentage(100f);
			try {
				unexmptedTable.setWidths(new float[] {13,13,13,13,13,13,13,13});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			};
		
			
			PdfPCell tableHeader=cell.getCell("Previous employment details : [if Yes to 9 AND/OR 10 above] - Un-exempted", font8bold, Element.ALIGN_LEFT, 0, 8, 0);
			tableHeader.setBorder(0);
			
			
			PdfPCell name=cell.getCell("Establishment Name & Address", font8, Element.ALIGN_CENTER, 0, 0, 0);
			
			PdfPCell accno=cell.getCell("Universal Account Number", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell pfacc=cell.getCell("PF Account Number", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell doj=cell.getCell("Date of joining ", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell doe=cell.getCell("Date of exit", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell schemecirtificate=cell.getCell("Scheme Certificate No.(if issued)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell ppoNo=cell.getCell("PPO Numner(if issued)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell nonContriPeriod=cell.getCell("Non Contributory Period (NCP) Days", font8, Element.ALIGN_CENTER, 0, 0, 0);
			unexmptedTable.addCell(tableHeader);
			unexmptedTable.addCell(name);
			unexmptedTable.addCell(accno);
			unexmptedTable.addCell(pfacc);
			unexmptedTable.addCell(doj);
			unexmptedTable.addCell(doe);
			unexmptedTable.addCell(schemecirtificate);
			unexmptedTable.addCell(ppoNo);
			unexmptedTable.addCell(nonContriPeriod);
			
			
//			String pFNumber="";
//			if(employee.isPreviousPfNumber()){
//				pFNumber=employee.getPPFNaumber();
//			}
			PdfPCell nameValue=null;
			if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
			nameValue=cell.getCell(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getCompany()+"", font8, Element.ALIGN_CENTER, 0, 0,50);
			}else{
				nameValue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0,50);
			}
			nameValue.setPaddingBottom(5);
			
			PdfPCell accnoValue=null;
			if(employee!=null){
				accnoValue=cell.getCell(employee.getUANno()+"", font8, Element.ALIGN_CENTER, 0, 0,50);
			}else{
				accnoValue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0,50);
			}
			
			accnoValue.setPaddingBottom(5);
			
			PdfPCell pfaccValue=null;
			if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
			if(employee!=null&&employee.getPreviousCompanyHistory().size()>0){
				
				pfaccValue=cell.getCell(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getPfNumber()+"", font8, Element.ALIGN_CENTER, 0, 0, 50);
				pfaccValue.setPaddingBottom(5);
				}else{
				pfaccValue=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 50);
				pfaccValue.setPaddingBottom(5);
				}}
			else{
				pfaccValue=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 50);
				pfaccValue.setPaddingBottom(5);
			}
			
			
			PdfPCell dojValue=null;
			if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
				 if(employee!=null&&employee.getPreviousCompanyHistory().size()>0){
			 dojValue=cell.getCell(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getFromDate()+"", font8, Element.ALIGN_CENTER, 0, 0, 50);
			 dojValue.setPaddingBottom(5);
				 }else{
			   dojValue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 50);
			   dojValue.setPaddingBottom(5);
			}}else{
				dojValue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 50);
				dojValue.setPaddingBottom(5);
			}
			
			
			 
			 PdfPCell doevalue=null;
			 if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
				 if(employee!=null&&employee.getPreviousCompanyHistory().size()>0){
				 doevalue=cell.getCell(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getToDate()+"", font8, Element.ALIGN_CENTER, 0, 0, 50);
				 doevalue.setPaddingBottom(5);
				 }else{
				 doevalue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 50); 
				 doevalue.setPaddingBottom(5);
			       }}else{
			    	   doevalue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 50); 
					   doevalue.setPaddingBottom(5);
			       }
			 
			 
			
			  PdfPCell schemecirtificateValue=null;
			  if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
              if(employee!=null&&employee.getPreviousCompanyHistory().size()>0){
            	  schemecirtificateValue=cell.getCell(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getSchemeCirtificateNumber()+"", font8, Element.ALIGN_CENTER, 0, 0, 50);
            	  schemecirtificateValue.setPaddingBottom(5);
				}else{
					schemecirtificateValue=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 50);
					schemecirtificateValue.setPaddingBottom(5);
				}
			  }else{
				  schemecirtificateValue=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 50);
				  schemecirtificateValue.setPaddingBottom(5);
			  }
			
			
              PdfPCell ppoNoValue=null;
              if(employee.getPreviousCompanyHistory()!=null&&employee.getPreviousCompanyHistory().size()!=0){
              if(employee!=null&&employee.getPreviousCompanyHistory().size()>0){
            	  ppoNoValue=cell.getCell(employee.getPreviousCompanyHistory().get(employee.getPreviousCompanyHistory().size()-1).getPensionPaymentOrderNumber()+"", font8, Element.ALIGN_CENTER, 0, 0, 50);
            	  ppoNoValue.setPaddingBottom(5);
				}else{
					ppoNoValue=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 50);
					ppoNoValue.setPaddingBottom(5);
				}
              }else{
            	  ppoNoValue=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 50);
					ppoNoValue.setPaddingBottom(5);
              }
			
			
//			PdfPCell ppoNoValue=cell.getCell(employee.getPensionPaymentNum()+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
//			ppoNoValue.setPaddingBottom(5);
			
			PdfPCell nonContriPeriodValue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 50);
			nonContriPeriodValue.setPaddingBottom(5);
			nonContriPeriodValue.setPaddingRight(5);
			
			unexmptedTable.addCell(nameValue);
			unexmptedTable.addCell(accnoValue);
			unexmptedTable.addCell(pfaccValue);
			unexmptedTable.addCell(dojValue);
			unexmptedTable.addCell(doevalue);
			unexmptedTable.addCell(schemecirtificateValue);
			unexmptedTable.addCell(ppoNoValue);
			unexmptedTable.addCell(nonContriPeriodValue);
			unexmptedTable.setSpacingBefore(5f);
			unexmptedTable.setSpacingAfter(15f);
			PdfPCell empdetail=cell.getCell("11", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell empdetailvalue=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 2, 0);
			empdetailvalue.addElement(unexmptedTable);
			
			table.addCell(empdetail);
			table.addCell(empdetailvalue);
			
			
			
			
		
			PdfPTable exmptedTable=new PdfPTable(7);
			exmptedTable.setWidthPercentage(100f);
			try {
				exmptedTable.setWidths(new float[] {14,14,14,14,14,14,14});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			PdfPCell headerTable=cell.getCell("Previous employment detalis : [if yes to 9 AND/OR 10 above]-For Exempted Trusts", font8bold, Element.ALIGN_LEFT, 0, 7, 0);
			headerTable.setBorder(0);
			
			PdfPCell name1=cell.getCell("Name & Address of the trust", font8, Element.ALIGN_CENTER, 0, 0, 0);
			name1.setPaddingLeft(15);
			PdfPCell uan=cell.getCell("UAN", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell epsmember=cell.getCell("Member EPS A/c Number", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell dojj=cell.getCell("Date of joining", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell doee=cell.getCell("Date of exit", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell schemecirti=cell.getCell("Scheme Certificate No.(if issued)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell noncontri=cell.getCell("Non Contributory Period (NCP) Days", font8, Element.ALIGN_CENTER, 0, 0, 0);
			
			exmptedTable.addCell(headerTable);
			
			exmptedTable.addCell(name1);
			exmptedTable.addCell(uan);
			exmptedTable.addCell(epsmember);
			exmptedTable.addCell(dojj);
			exmptedTable.addCell(doee);
			exmptedTable.addCell(schemecirti);
			exmptedTable.addCell(noncontri);
			
			
			PdfPCell name1value=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			name1value.setPaddingBottom(15);
			PdfPCell uanvaluevalue=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			uanvaluevalue.setPaddingBottom(15);
			PdfPCell epsmembervalue=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			epsmembervalue.setPaddingBottom(15);
			PdfPCell dojjvalue=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			dojjvalue.setPaddingBottom(15);
			PdfPCell doeevalue=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			doeevalue.setPaddingBottom(15);
			PdfPCell schemecirtivalue=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			schemecirtivalue.setPaddingBottom(15);
			PdfPCell noncontrivalue=cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 50);
			noncontrivalue.setPaddingBottom(15);
			
			exmptedTable.addCell(name1value);
			exmptedTable.addCell(uanvaluevalue);
			exmptedTable.addCell(epsmembervalue);
			exmptedTable.addCell(dojjvalue);
			exmptedTable.addCell(doeevalue);
			exmptedTable.addCell(schemecirtivalue);
			exmptedTable.addCell(noncontrivalue);
			
			exmptedTable.setSpacingBefore(5f);
			exmptedTable.setSpacingAfter(15f);
			PdfPCell empdetail2=cell.getCell("12", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell empdetailvalue2=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 2, 0);
			empdetailvalue2.addElement(exmptedTable);
			
			
			table.addCell(empdetail2);
			table.addCell(empdetailvalue2);
			
			
			table.addCell(cell.getCell("13", font8, Element.ALIGN_CENTER, 4, 0, 15));
			table.addCell(cell.getCell("a)International Worker:", font8bold, Element.ALIGN_LEFT, 0, 0, 15));
			PdfPCell cell13=null;
			 if(employee!=null){
						if(employee.getCountry().trim().equalsIgnoreCase("India")&&employee.getCountry().trim().equalsIgnoreCase("INDIA")){
							cell13=cell.getCell("No", font8, Element.ALIGN_LEFT, 0, 0, 0);	
						}else{
							cell13=cell.getCell("Yes", font8, Element.ALIGN_LEFT, 0, 0, 0);
						}
					}
			
			table.addCell(cell13);
//			table.addCell(cell.getCell("Yes / No", font8, Element.ALIGN_LEFT, 0, 0, 15));
//			table.addCell(getCheckBoxYesNoTbl(false,true));
			table.addCell(cell.getCell("b)If yes, state country of origin (India/Name of other country)", font8, Element.ALIGN_LEFT, 0, 0, 15));
			PdfPCell cell14=null;
			 if(employee!=null){
					if(employee.getCountry().trim().equalsIgnoreCase("India")&&employee.getCountry().trim().equalsIgnoreCase("INDIA")){
							cell14=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 15);	
					 }else{
							cell14=cell.getCell(employee.getCountry()+"", font8, Element.ALIGN_LEFT, 0, 0, 15);
						}
					}
			     table.addCell(cell14);
			
//			table.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 15));
			table.addCell(cell.getCell("c)Passport No.", font8, Element.ALIGN_LEFT, 0, 0, 15));
			
			PdfPCell cell15=null;
			 if(employee!=null){
					if(employee.getCountry().trim().equalsIgnoreCase("India")&&employee.getCountry().trim().equalsIgnoreCase("INDIA")){
							cell15=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 15);	
					 }else{
							cell15=cell.getCell(employee.getPassPortInformation().getPassportNumber()+"", font8, Element.ALIGN_LEFT, 0, 0, 15);
						}
					}
			     table.addCell(cell15);
//			table.addCell(cell.getCell(employee.getPassPortInformation().getPassportNumber()+"", font8, Element.ALIGN_CENTER, 0, 0, 15));
			table.addCell(cell.getCell("d)Validity of passport [(DD/MM/YYYY)to(DD/MM/YYYY)]", font8, Element.ALIGN_LEFT, 0, 0, 15));
			
			PdfPCell cell16=null;
			 if(employee!=null){
					if(employee.getCountry().trim().equalsIgnoreCase("India")&&employee.getCountry().trim().equalsIgnoreCase("INDIA")){
						cell16=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 15);	
					 }else{
						 cell16=cell.getCell(format.format(employee.getPassPortInformation().getExpiryDate())+"", font8, Element.ALIGN_LEFT, 0, 0, 15);
						}
					}
			     table.addCell(cell16);
			
			
			
			
//			table.addCell(cell.getCell(employee.getPassPortInformation().getExpiryDate()+"", font8, Element.ALIGN_CENTER, 0, 0, 15));
			
			
			
			
			
		
		
		table.setSpacingBefore(15f);
		try {
			document.add(table);
//			document.add(unexmptedTable);
//			document.add(exmptedTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private PdfPCell getPfAcNoTbl(){
		float[] columnWidths = { 1.8f,1.8f,2.4f,1.8f,2.2f};
		
		PdfPCell leftCell=cell.getCell("b) Previous PF a/c No ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		leftCell.setBorder(0);
		PdfPCell rightCell=new PdfPCell();
		rightCell.setBorder(0);
		
		PdfPTable table=new PdfPTable(5);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		table.addCell(cell.getCell("AP", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(cell.getCell("HYD", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(cell.getCell("EST.CODE", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(cell.getCell("EXTN", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(cell.getCell("PF NO.", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
//		table.addCell(cell.getCell(" ",font8 , Element.ALIGN_CENTER, 0, 5, 0)).setBorder(0);
		
		rightCell.addElement(table);
		
		PdfPCell outerCell=new PdfPCell();
		float[] columnWidths1 = {40.0f,60.0f};
		PdfPTable outerTbl=new PdfPTable(2);
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(columnWidths1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		outerTbl.addCell(leftCell);
		outerTbl.addCell(rightCell);
		
		outerCell.addElement(outerTbl);
		
		
		return outerCell;
	}
	
	private PdfPTable getBlankPfAcNoTbl(){
		float[] columnWidths = { 1.8f,1.8f,2.4f,1.8f,2.2f};
		
		PdfPCell outerCell=new PdfPCell();
		PdfPTable table=new PdfPTable(5);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		table.addCell(cell.getCell(" ", font6bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorderWidth(1.5f);
		table.addCell(cell.getCell(" ", font6bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorderWidth(1.5f);
		table.addCell(cell.getCell(" ", font6bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorderWidth(1.5f);
		table.addCell(cell.getCell(" ", font6bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorderWidth(1.5f);
		table.addCell(cell.getCell(" ", font6bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorderWidth(1.5f);
		
		outerCell.addElement(table);
		outerCell.setBorderWidth(1.5f);
		
		return table;
	}
	
	private PdfPCell getFatherOrSpouseNameTbl(boolean fatherFlag,boolean spouseFlag) {
		PdfPCell outerCell=new PdfPCell();
//		outerCell.setBorder(0);
		
		Image blankImg = null;
		try {
			blankImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		blankImg.scalePercent(4);
		blankImg.scaleAbsoluteHeight(4);
		blankImg.scaleAbsoluteWidth(4);
		
		Image checkedImg = null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(4);
		checkedImg.scaleAbsoluteHeight(4);
		checkedImg.scaleAbsoluteWidth(4);
		
		PdfPCell uncheckCell=new PdfPCell(blankImg);
		uncheckCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		uncheckCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		uncheckCell.setBorder(0);
		
		PdfPCell checkCell=new PdfPCell(checkedImg);
		checkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		checkCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		checkCell.setBorder(0);
		
		PdfPTable table=new PdfPTable(4);
		table.setWidthPercentage(100f);
		
		
		if(!fatherFlag&&!spouseFlag){
			table.addCell(cell.getCell("Father's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(uncheckCell);
			
			table.addCell(cell.getCell("Spouse's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(uncheckCell);	
		}else{
			if(fatherFlag==true){
				table.addCell(cell.getCell("Father's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				table.addCell(checkCell);
				
				table.addCell(cell.getCell("Spouse's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				table.addCell(uncheckCell);
			}else if(spouseFlag==true){
				table.addCell(cell.getCell("Father's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				table.addCell(uncheckCell);
				
				table.addCell(cell.getCell("Spouse's Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				table.addCell(checkCell);
			}
		}
		outerCell.addElement(table);
		return outerCell;
	}

	private PdfPCell getCheckBoxYesNoTbl(boolean yesNoFlag,boolean blank) {
		PdfPCell outerCell=new PdfPCell();
//		outerCell.setBorder(0);
		
		Image blankImg = null;
		try {
			blankImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		blankImg.scalePercent(4);
		blankImg.scaleAbsoluteHeight(4);
		blankImg.scaleAbsoluteWidth(4);
		
		Image checkedImg = null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(4);
		checkedImg.scaleAbsoluteHeight(4);
		checkedImg.scaleAbsoluteWidth(4);
		
		PdfPCell uncheckCell=new PdfPCell(blankImg);
		uncheckCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		uncheckCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		uncheckCell.setBorder(0);
		
		PdfPCell checkCell=new PdfPCell(checkedImg);
		checkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		checkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		checkCell.setBorder(0);
		
		PdfPTable table=new PdfPTable(4);
		table.setWidthPercentage(100f);
		
		
		if(blank==true){
			table.addCell(cell.getCell("Yes", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			table.addCell(uncheckCell);
			
			table.addCell(cell.getCell("No", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			table.addCell(uncheckCell);	
		}else{
			if(yesNoFlag==true){
				table.addCell(cell.getCell("Yes", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				table.addCell(checkCell);
				
				table.addCell(cell.getCell("No", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				table.addCell(uncheckCell);
			}else{
				table.addCell(cell.getCell("Yes", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				table.addCell(uncheckCell);
				
				table.addCell(cell.getCell("No", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				table.addCell(checkCell);
			}
		}
		outerCell.addElement(table);
		return outerCell;
	}

	private void createUndertakingDetails() {
		// TODO Auto-generated method stub
		float[] columnWidths = { 0.5f, 5.5f, 4f};
		PdfPTable table=new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 25)).setBorder(0);
		table.addCell(cell.getCell("UNDERTAKING", font8boldul, Element.ALIGN_CENTER, 0, 2, 25)).setBorder(0);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("1) Certified that the particulars are true to the best of my Knowledge", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("2) I authorize EPFO to use my Aadhar for verification/authentication/ e-KYC purpose for service delivery", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("3) Kindly transfer the funds and service details, if applicable, from the previous PF account as declared above to the present P.F.", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("   The Transefer would be possible only if the identified KYC details approved by previous employer has been verified by present ", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
//		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("   Account as I am an Aadhar verified employee in my previous PF Account.*", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("4) In case of changes in above details, the same will be intimated to employer at the earliest.", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 2, 3, 0)).setBorder(0);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("   Date:", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("   Place:", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("Signature of Member   ", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		table.setSpacingBefore(40f);
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createDeclarationByPresentEmployer() {
		// TODO Auto-generated method stub
		Image blankImg = null;
		try {
			blankImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		blankImg.scalePercent(4);
		blankImg.scaleAbsoluteHeight(4);
		blankImg.scaleAbsoluteWidth(4);
		
		Chunk bullet = new Chunk("\u2022", font8bold);
		    
		float[] columnWidths = {5,21,22,11,18,22};
		PdfPTable table=new PdfPTable(6);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table.setSpacingBefore(30f);
		
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		table.addCell(cell.getCell("DECLARATION BY PRESENT EMPLOYER", font8boldul, Element.ALIGN_CENTER, 0, 5, 20)).setBorder(0);
	
		table.addCell(cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		table.addCell(cell.getCell("A) The member Mr./Ms./Mrs.", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell name1value=cell.getCell(employee.getFullname()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		name1value.setBorderWidthTop(0);
		name1value.setBorderWidthLeft(0);
		name1value.setBorderWidthRight(0);
		
		table.addCell(name1value);
		table.addCell(cell.getCell(" has joined on  ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell name=cell.getCell(format.format(employee.getJoinedAt())+" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		name.setBorderWidthTop(0);
		name.setBorderWidthLeft(0);
		name.setBorderWidthRight(0);
		table.addCell(name);
		table.addCell(cell.getCell(" and has been", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		float[] columnWidths4 = {5,15,26,9,35,10};
		PdfPTable table2=new PdfPTable(6);
		table2.setWidthPercentage(100f);
		try {
			table2.setWidths(columnWidths4);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		table2.addCell(cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		table2.addCell(cell.getCell("     alloted PF No.", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell name1=cell.getCell(employee.getPPFNaumber()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		name1.setBorderWidthTop(0);
		name1.setBorderWidthLeft(0);
		name1.setBorderWidthRight(0);
		table2.addCell(name1);
		
		table2.addCell(cell.getCell(" and UAN ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell name2=cell.getCell(employee.getUANno()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		name2.setBorderWidthTop(0);
		name2.setBorderWidthLeft(0);
		name2.setBorderWidthRight(0);
		table2.addCell(name2);
		
		PdfPCell name3=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
		name3.setBorder(0);
//		name3.setBorderWidthLeft(0);
//		name3.setBorderWidthRight(0);
//		name3.setBorderWidthTop(0);
		table2.addCell(name3);
		
//		PdfPCell blank=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
//		blank.setBorder(0);
//		PdfPCell addTableCell=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 5, 0);
//		addTableCell.addElement(table2);
//		addTableCell.setBorder(0);
////		table.addCell(blank);
//		table.addCell(addTableCell);
//		
		
		
		
		
		
//		
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("B) In case person was earlier not a member of EPF Scheme,1952 and EPS,1995:", font8, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(bullet+" (Post allotment of UAN) The UAN Allotted for the member is "+employee.getUANno(), font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(bullet+" Please tick the Appropriate Option:", font8bold, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("The KYC details of the above member in the UAN database", font8, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		
		
		float[] columnWidths8 = { 5,85,10};
		PdfPTable innerTable=new PdfPTable(3);
		innerTable.setWidthPercentage(100f);
		try {
			innerTable.setWidths(columnWidths8);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		innerTable.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTable.addCell(cell.getCell("B) In case person was earlier not a member of EPF Scheme,1952 and EPS,1995:", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		innerTable.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		innerTable.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTable.addCell(cell.getCell("   "+bullet+"   Please tick the Appropriate Option:", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		innerTable.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		float[] columnWidths1 = { 0.5f, 0.2f, 9.3f};
		PdfPTable innerTbl1=new PdfPTable(3);
		innerTbl1.setWidthPercentage(100f);
		try {
			innerTbl1.setWidths(columnWidths1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPCell uncheckCell=new PdfPCell(blankImg);
		uncheckCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		uncheckCell.setBorder(0);
		
		
		innerTbl1.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl1.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl1.addCell(cell.getCell("The KYC details of the above member in the UAN database", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		innerTbl1.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl1.addCell(uncheckCell);
		innerTbl1.addCell(cell.getCell("Have not been uploaded", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		innerTbl1.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl1.addCell(uncheckCell);
		innerTbl1.addCell(cell.getCell("Have been uploaded but not approved", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		innerTbl1.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl1.addCell(uncheckCell);
		innerTbl1.addCell(cell.getCell("Have been uploaded and approved with DSC/e-sign.", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
//		PdfPCell cell1=new PdfPCell();
//		cell1.addElement(innerTbl1);
//		cell1.setBorder(0);
//		cell1.setColspan(3);
        PdfPCell empdetail3=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0);
         empdetail3.setBorder(0);
		PdfPCell cell6=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 2, 0);
		cell6.addElement(innerTbl1);
		cell6.setBorder(0);
		innerTable.addCell(empdetail3);
		innerTable.addCell(cell6);
		
//		table.addCell(cell1);
		
		innerTable.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTable.addCell(cell.getCell("C) In case the person was earlier a member of EPF Scheme,1952 and EPS,1995:", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(bullet+" The above PF account number / UAN of the member as mentioned in (a) above been tagged with his/her UAN/previous member ID as ", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("   declared by member", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		
		innerTable.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTable.addCell(cell.getCell("   "+bullet+"  Pleass Tick the Appropriate Option:-", font8bold, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		float[] columnWidths2 = { 0.5f, 0.2f, 9.3f};
		PdfPTable innerTbl2=new PdfPTable(3);
		innerTbl2.setWidthPercentage(100f);
		try {
			innerTbl2.setWidths(columnWidths2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		innerTbl2.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl2.addCell(uncheckCell);
		innerTbl2.addCell(cell.getCell("The KYC details of the above member in the UAN database have been approved with E-sign/Digital Signature Certificate and transfer request has been generated on portal.", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		innerTbl2.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl2.addCell(uncheckCell);
//		innerTbl2.addCell(cell.getCell("As the DSC of establishment are not registered wIth EPFO the member has been informed to file physical claim ( Form 13) for transfer of funds from his previous establishment.", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		innerTbl2.addCell(cell.getCell("The previous Account of the member is not Aadhar verified and hence physical transfer form shall be initiated", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		PdfPCell cell2=new PdfPCell();
//		cell2.addElement(innerTbl2);
//		cell2.setBorder(0);
//		cell2.setColspan(5);
		
		PdfPCell empdetail9=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0);
		empdetail9.setBorder(0);
		PdfPCell cell9=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 2, 0);
		cell9.addElement(innerTbl2);
		cell9.setBorder(0);
		innerTable.addCell(empdetail9);
		innerTable.addCell(cell9);
		
		
		
		
		
		
		
		
		
		
		
		
//		 table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 30)).setBorder(0);
//		 table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 30)).setBorder(0);
//		 table.addCell(cell.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 30)).setBorder(0);
//		 table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 30)).setBorder(0);
//		 table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 30)).setBorder(0);
//		 table.addCell(cell.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 30)).setBorder(0);
			
			
		
		
		
//		
//		table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 2, 3, 0)).setBorder(0);
//		
//		table.addCell(cell.getCell("Date", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("Signature of Employer With seal of ", font8, Element.ALIGN_RIGHT, 0, 2, 0)).setBorder(0);
//		
//        table.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		table.addCell(cell.getCell("Establishment", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		
		
		
		
		
		
//		PdfPTable innerTbl3=new PdfPTable(3);
//		float[] columnWidths5 = {5,20,45,30};
//		innerTbl3.setWidthPercentage(100f);
//		try {
//			innerTbl3.setWidths(columnWidths5);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//		innerTbl3.setSpacingBefore(40f);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell("Date ", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell("Signature of Employer With seal of ", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
//		
//		
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell("Establishment", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
//		
//		
//		
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell("Auto transfer of previous PF account would be possible in respect of Aadhar verified employees only.Other employees are requested to ", font8, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		innerTbl3.addCell(cell.getCell("file physical claim (Form-13)for transfer of account from the previous establishment.", font8, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
//		
		
//		table.setSpacingBefore(20f);
		try {
			document.add(table);
			document.add(table2);
			document.add(innerTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	   private void footerTable(){
		   
		PdfPTable innerTbl3=new PdfPTable(4);
		float[] columnWidths5 = {5,20,45,30};
		innerTbl3.setWidthPercentage(100f);
		try {
			innerTbl3.setWidths(columnWidths5);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		innerTbl3.setSpacingBefore(30f);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell("Date ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell("Signature of Employer with seal of ", font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" Establishment", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 40)).setBorder(0);
		
		
		
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell("*Auto transfer of previous PF account would be possible in respect of Aadhar verified employees only.Other employees are requested to ", font8, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		
		innerTbl3.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		innerTbl3.addCell(cell.getCell("file physical claim (Form-13)for transfer of account from the previous establishment.", font8, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		
		
		
		try {
			document.add(innerTbl3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
}
