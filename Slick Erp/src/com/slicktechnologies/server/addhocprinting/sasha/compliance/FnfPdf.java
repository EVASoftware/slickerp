package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.humanresourcelayer.PaySlipServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.EmployeeAssetBean;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.AllocatedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveBalance;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class FnfPdf {
	public Document document;
	Company company;
	Employee employee;
	PaySlip payslip;
	LeaveBalance leaveBalance;
	CTC ctc;
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	float[] columnWidths = { 1.5f, 0.1f, 3.3f,0.2f, 1.5f, 0.1f, 3.3f };
	float[] column9Widths = {1.2f,1.5f,0.3f,1.5f, 0.3f, 0.8f, 0.8f,0.3f,2.0f };
	float[] column8Widths = {0.9f,0.2f,1.3f,1.6f, 1.7f, 0.2f, 1.2f,1.8f };
	
	float[] colWidths9Strs = {0.9f,0.9f,0.9f,0.9f,0.9f,1.0f,0.8f,0.9f,1.8f };
	float[] colWidths9Strs1 = {1.0f,1.2f,1.2f,0.9f,0.8f,0.8f,0.4f,0.9f,1.8f };
	
	float[] colWidths7Str = {1.1f,1.2f,0.4f,1.9f,1.2f,0.5f,1.2f};
	
	float[] column13Widths = {1.0f,0.1f,1.1f,0.1f, 1.0f, 0.1f, 0.9f,0.1f, 1.0f,0.1f, 1.0f,0.1f, 1.0f };
	
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	Logger logger=Logger.getLogger("FnF Pdf Logger");
	
	ArrayList<EmployeeAssetBean> uniformCollectedList;
	ArrayList<EmployeeAssetBean> uniformDeductionList;
	ArrayList<EmployeeAssetBean> shoesCollectedList;
	ArrayList<EmployeeAssetBean> shoesDeductionList;
	ArrayList<EmployeeAssetBean> otherCollectedList;
	ArrayList<EmployeeAssetBean> otherDeductionList;
	
	double totalNetPay=0;
	double totalAddition=0;
	double totalDeduction=0;
	
	double totalDaysInMonth=0;
	double perDaySal=0;
	double plAmount=0;
	double totalPl=0;
	
	public FnfPdf() {
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setFnfPdfData(Long companyId,int empId,String fromDate,String toDate){
//		Form3aPdf
		company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(company!=null){
			employee=ofy().load().type(Employee.class).filter("companyId", companyId).filter("count", empId).first().now();
		}
		leaveBalance=ofy().load().type(LeaveBalance.class).filter("companyId", companyId).filter("empInfo.empCount", empId).first().now();
		String payrollMonth="";
		if(employee.getFnfMonth()!=null){
			payrollMonth=fmt.format(employee.getFnfMonth());
			logger.log(Level.SEVERE,"FnF month :"+payrollMonth);
			payslip=ofy().load().type(PaySlip.class).filter("companyId", companyId).filter("salaryPeriod", payrollMonth).filter("empid", empId).first().now();
		}else{
			logger.log(Level.SEVERE,"FnF month not selected");
		}
		
		if(payslip==null){
			logger.log(Level.SEVERE,"Payroll NULL");
		}
		
		ctc=ofy().load().type(CTC.class).filter("empid",empId).filter("companyId",companyId).filter("status", "Active").first().now();
		
		uniformCollectedList=new ArrayList<EmployeeAssetBean>();
		uniformDeductionList=new ArrayList<EmployeeAssetBean>();
		
		shoesCollectedList=new ArrayList<EmployeeAssetBean>();
		shoesDeductionList=new ArrayList<EmployeeAssetBean>();
		
		otherCollectedList=new ArrayList<EmployeeAssetBean>();
		otherDeductionList=new ArrayList<EmployeeAssetBean>();
		
		if(employee.getCheckList()!=null&&employee.getCheckList().size()!=0){
			for(EmployeeAssetBean asset:employee.getCheckList()){
				if(asset.isRecordSelect()==true){
					if(asset.getAssetCategory().equalsIgnoreCase("Uniform")||asset.getAssetCategory().contains("Uniform")){
						uniformCollectedList.add(asset);
					}else if(asset.getAssetCategory().equalsIgnoreCase("Shoes")||asset.getAssetCategory().contains("Shoes")){
						shoesCollectedList.add(asset);
					}else{
						otherCollectedList.add(asset);
					}
				}else{
					if(asset.getAssetCategory().equalsIgnoreCase("Uniform")||asset.getAssetCategory().contains("Uniform")){
						uniformDeductionList.add(asset);
					}else if(asset.getAssetCategory().equalsIgnoreCase("Shoes")||asset.getAssetCategory().contains("Shoes")){
						shoesDeductionList.add(asset);
					}else{
						otherDeductionList.add(asset);
					}
				}
			}
			logger.log(Level.SEVERE,"Uniform collected : "+uniformCollectedList.size()+"Uniform Deducted : "+uniformDeductionList.size());
			logger.log(Level.SEVERE,"Shoes collected : "+shoesCollectedList.size()+"Shoes Deducted : "+shoesDeductionList.size());
			logger.log(Level.SEVERE,"Other collected : "+otherCollectedList.size()+"Other Deducted : "+otherDeductionList.size());
			
		}else{
			logger.log(Level.SEVERE,"No checklist is added");
		}
		
		PaySlipServiceImpl pay=new PaySlipServiceImpl();
		if(!payrollMonth.equals("")){
			totalDaysInMonth=pay.daysInMonth(payrollMonth,"IST");
		}
		logger.log(Level.SEVERE,"Total days in month : "+totalDaysInMonth);
		if(ctc!=null&&totalDaysInMonth!=0){
			perDaySal=(ctc.getGrossEarning()/12)/totalDaysInMonth;
		}
		logger.log(Level.SEVERE,"Per day Sal : "+perDaySal);
		
		
		if(leaveBalance!=null){
			for(AllocatedLeaves leave:leaveBalance.getAllocatedLeaves()){
				if(leave.getShortName().equalsIgnoreCase("PL")){
					totalPl=leave.getBalance();
				}
			}
		}
		
		plAmount=totalPl*perDaySal;
		logger.log(Level.SEVERE,"PL Sal : "+plAmount);
		
	}
	
	public void createPdf(){
		createHeader();
		createFormHeading();
		createEmployeesDetails();
		createAdditionsTable();
		createDeductionsTable();
		createSignatureTable();
	}
	
	private void createHeader() {
		DocumentUpload logodocument =company.getLogo();
		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setWidthPercentage(100f);
		
		try {
			logoTable.setWidths(new float[]{80,20});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if (imageSignCell != null) {
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		} else {
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}
		
		try {
			document.add(logoTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createSignatureTable() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(13);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(35f);
		
		try {
			outerTbl.setWidths(column13Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell blankCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);
		
		outerTbl.addCell(blankCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(blankCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(blankCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(blankCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(blankCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(blankCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(blankCell);

		
		
		
		
		outerTbl.addCell(cell.getCell("H.O.D Operations Dept", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("H.O.D Compliance Dept", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("H.O.D Payroll Dept", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("H.O.D Uniform Dept", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("H.O.D H. R. Dept", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("H.O.D Accounts Dept", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("RECEIVER'S SIGNATURE", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createDeductionsTable() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(8);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		try {
			outerTbl.setWidths(column8Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfPCell headingCell=cell.getCell("DEDUCTIONS", font12bold, Element.ALIGN_LEFT, 0, 8, 0);
		headingCell.setBorder(0);
		outerTbl.addCell(headingCell);
		
		outerTbl.addCell(cell.getCell("PO/OPERATIONS (if any) Specify", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		PdfPCell poValCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 2, 0);
		poValCell.setBorderWidthTop(0);
		poValCell.setBorderWidthLeft(0);
		poValCell.setBorderWidthRight(0);
		outerTbl.addCell(poValCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Total Operations", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0));
	
		//1
		outerTbl.addCell(cell.getCell("ACCOUNTS", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("1) Loans", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell princpleCell=cell.getCell("Principal", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		princpleCell.setBorderWidthTop(0);
		princpleCell.setBorderWidthLeft(0);
		princpleCell.setBorderWidthRight(0);
		outerTbl.addCell(princpleCell);
		PdfPCell balanceCell=cell.getCell("Balance New", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		balanceCell.setBorderWidthTop(0);
		balanceCell.setBorderWidthLeft(0);
		balanceCell.setBorderWidthRight(0);
		outerTbl.addCell(balanceCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		
		//2
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("2) Adv / IOU", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell princpleCell1=cell.getCell("Principal", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		princpleCell1.setBorderWidthTop(0);
		princpleCell1.setBorderWidthLeft(0);
		princpleCell1.setBorderWidthRight(0);
		outerTbl.addCell(princpleCell1);
		PdfPCell balanceCell1=cell.getCell("Balance New", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		balanceCell1.setBorderWidthTop(0);
		balanceCell1.setBorderWidthLeft(0);
		balanceCell1.setBorderWidthRight(0);
		outerTbl.addCell(balanceCell1);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		
		//3
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("3) Mobile Exp.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell valCell=cell.getCell(" ", font8bold, Element.ALIGN_LEFT, 0, 2, 0);
		valCell.setBorderWidthTop(0);
		valCell.setBorderWidthLeft(0);
		valCell.setBorderWidthRight(0);
		outerTbl.addCell(valCell);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		
		//4
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("4) T.D.S", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell valCell1=cell.getCell(" ", font8bold, Element.ALIGN_LEFT, 0, 2, 0);
		valCell1.setBorderWidthTop(0);
		valCell1.setBorderWidthLeft(0);
		valCell1.setBorderWidthRight(0);
		outerTbl.addCell(valCell1);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		

		//5
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("5) Others(Specify)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell valCell2=cell.getCell(" ", font8bold, Element.ALIGN_LEFT, 0, 2, 0);
		valCell2.setBorderWidthTop(0);
		valCell2.setBorderWidthLeft(0);
		valCell2.setBorderWidthRight(0);
		outerTbl.addCell(valCell2);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Total Accounts", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0));
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable outerTbl1=new PdfPTable(9);
		outerTbl1.setWidthPercentage(100f);
		outerTbl1.setSpacingBefore(10f);
		try {
			outerTbl1.setWidths(colWidths9Strs);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		outerTbl1.addCell(cell.getCell("STORES", font8bold, Element.ALIGN_LEFT, 0, 9, 0)).setBorder(0);
		
		outerTbl1.addCell(cell.getCell("Collected", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl1.addCell(cell.getCell("(1) Uniform Sign.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell unifCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		unifCell.setBorderWidthLeft(0);
		unifCell.setBorderWidthRight(0);
		unifCell.setBorderWidthTop(0);
		outerTbl1.addCell(unifCell);
		outerTbl1.addCell(cell.getCell("(2) Shoes Sign.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell shoesCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		shoesCell.setBorderWidthLeft(0);
		shoesCell.setBorderWidthRight(0);
		shoesCell.setBorderWidthTop(0);
		outerTbl1.addCell(shoesCell);
		outerTbl1.addCell(cell.getCell("(3) Other Specify+Sign", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell otherCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		otherCell.setBorderWidthLeft(0);
		otherCell.setBorderWidthRight(0);
		otherCell.setBorderWidthTop(0);
		outerTbl1.addCell(otherCell);
		outerTbl1.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl1.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		
		double uniformDeduction=0;
		double shoesDeduction=0;
		double otherDeduction=0;
		
		for(EmployeeAssetBean asset:uniformDeductionList){
			uniformDeduction=uniformDeduction+asset.getPrice();
		}
		for(EmployeeAssetBean asset:shoesDeductionList){
			shoesDeduction=shoesDeduction+asset.getPrice();
		}
		for(EmployeeAssetBean asset:otherDeductionList){
			otherDeduction=otherDeduction+asset.getPrice();
		}
		
		totalDeduction=uniformDeduction+shoesDeduction+otherDeduction;
		
		outerTbl1.addCell(cell.getCell("Deductions", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl1.addCell(cell.getCell("(1) Uniform Dedn.(Rs)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell unifDedCell=cell.getCell(uniformDeduction+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		unifDedCell.setBorderWidthLeft(0);
		unifDedCell.setBorderWidthRight(0);
		unifDedCell.setBorderWidthTop(0);
		outerTbl1.addCell(unifDedCell);
		outerTbl1.addCell(cell.getCell("(2) Shoes Dedn.(Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell shoesDedCell=cell.getCell(shoesDeduction+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		shoesDedCell.setBorderWidthLeft(0);
		shoesDedCell.setBorderWidthRight(0);
		shoesDedCell.setBorderWidthTop(0);
		outerTbl1.addCell(shoesDedCell);
		outerTbl1.addCell(cell.getCell("(3) Other Specify(Rs)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell otherSpcCell=cell.getCell(otherDeduction+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		otherSpcCell.setBorderWidthLeft(0);
		otherSpcCell.setBorderWidthRight(0);
		otherSpcCell.setBorderWidthTop(0);
		outerTbl1.addCell(otherSpcCell);
		outerTbl1.addCell(cell.getCell("Total Stores", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl1.addCell(cell.getCell(totalDeduction+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
		
		try {
			document.add(outerTbl1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		PdfPTable outerTbl2=new PdfPTable(9);
		outerTbl2.setWidthPercentage(100f);
		outerTbl2.setSpacingBefore(5f);
		try {
			outerTbl2.setWidths(colWidths9Strs1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell compCell=cell.getCell("COMPLIANCE", font8bold, Element.ALIGN_LEFT, 2, 1, 0);
		compCell.setBorder(0);;
		compCell.setVerticalAlignment(Element.ALIGN_CENTER);
		outerTbl2.addCell(compCell);
//		outerTbl2.addCell(cell.getCell("COMPLIANCE", font8bold, Element.ALIGN_LEFT, 2, 1, 0)).setBorder(0);
		PdfPCell pfFormCell=cell.getCell("PF WITHDRAWAL FORM FILLED", font8bold, Element.ALIGN_LEFT, 1, 2, 0);
		pfFormCell.setBorderWidthLeft(0);
		pfFormCell.setBorderWidthRight(0);
		pfFormCell.setBorderWidthTop(0);
		outerTbl2.addCell(pfFormCell);
		PdfPCell dedSpcCell=cell.getCell("Deductions Specify", font8bold, Element.ALIGN_LEFT, 2, 1, 0);
		dedSpcCell.setBorderWidthLeft(0);
		dedSpcCell.setBorderWidthRight(0);
		dedSpcCell.setBorderWidthTop(0);
		outerTbl2.addCell(dedSpcCell);
		PdfPCell blankCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 2, 3, 0);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);
		outerTbl2.addCell(blankCell);
		outerTbl2.addCell(cell.getCell("Total Compliance", font8bold, Element.ALIGN_LEFT, 2, 1, 0)).setBorder(0);
		outerTbl2.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 2, 1, 0));
		outerTbl2.addCell(cell.getCell("(Y) / (N)", font8bold, Element.ALIGN_LEFT, 1, 2, 0)).setBorder(0);
		
		
		outerTbl2.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 8, 0)).setBorder(0);
		outerTbl2.addCell(cell.getCell(totalDeduction+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
		try {
			document.add(outerTbl2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
		
		
		PdfPTable outerTbl3=new PdfPTable(7);
		outerTbl3.setWidthPercentage(100f);
		outerTbl3.setSpacingBefore(5f);
		try {
			outerTbl3.setWidths(colWidths7Str);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String pfAccountNum="";
		String esicNum="";
		if(employee.getPPFNaumber()!=null){
			pfAccountNum=employee.getPPFNaumber();
		}
		if(employee.getEmployeeESICcode()!=null){
			esicNum=employee.getEmployeeESICcode();
		}
		outerTbl3.addCell(cell.getCell("PF Account No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell blankCell1=cell.getCell(pfAccountNum, font8, Element.ALIGN_LEFT, 0, 0, 0);
		blankCell1.setBorderWidthLeft(0);
		blankCell1.setBorderWidthRight(0);
		blankCell1.setBorderWidthTop(0);
		outerTbl3.addCell(blankCell1);
		outerTbl3.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		outerTbl3.addCell(cell.getCell("ESIC Account No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell blankCell2=cell.getCell(esicNum+"", font8, Element.ALIGN_LEFT, 0, 0, 0);
		blankCell2.setBorderWidthLeft(0);
		blankCell2.setBorderWidthRight(0);
		blankCell2.setBorderWidthTop(0);
		outerTbl3.addCell(blankCell2);
		outerTbl3.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl3.addCell(cell.getCell("(C)Net Pay(A)-(B) Cheque No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell blankCell21=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		blankCell21.setBorderWidthLeft(0);
		blankCell21.setBorderWidthRight(0);
		blankCell21.setBorderWidthTop(0);
		outerTbl3.addCell(blankCell21);
		outerTbl3.addCell(cell.getCell("Rs.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		double netpayable = Math.round(totalAddition-totalDeduction);
		PdfPCell blankCell211=cell.getCell(netpayable+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		blankCell211.setBorderWidthLeft(0);
		blankCell211.setBorderWidthRight(0);
		blankCell211.setBorderWidthTop(0);
		outerTbl3.addCell(blankCell211);
		
		try {
			document.add(outerTbl3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable outerTbl4=new PdfPTable(7);
		outerTbl4.setWidthPercentage(100f);
		outerTbl4.setSpacingBefore(5f);
		try {
			outerTbl4.setWidths(colWidths7Str);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		PdfPCell blankCell2112=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 7, 0);
		blankCell2112.setBorderWidthLeft(0);
		blankCell2112.setBorderWidthRight(0);
		blankCell2112.setBorderWidthTop(0);
		outerTbl4.addCell(blankCell2112);
		
		try {
			document.add(outerTbl4);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
	}

	private void createAdditionsTable() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(9);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		try {
			outerTbl.setWidths(column9Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfPCell headingCell=cell.getCell("ADDITIONS", font12bold, Element.ALIGN_LEFT, 0, 9, 0);
		headingCell.setBorder(0);
		outerTbl.addCell(headingCell);
		
		//Net Salary Pending SI.No.
		outerTbl.addCell(cell.getCell("Net Salary Pending SI. No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Month (Operations)", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Payroll (Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Chq. No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Accounts Final(Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		String payrollMonth="";
		double netEarning=0;
		double conveyance=0;
		if(payslip!=null){
			payrollMonth=payslip.getSalaryPeriod();
			netEarning=payslip.getNetEarningWithoutOT();
			conveyance=payslip.getPaidLeaves()+payslip.getBonus()+payslip.getOvertimeSalary();
			for(EmployeeCTCTemplate ctc:payslip.getEmployeeCTCTemplate()){
				if(ctc.getCtcTemplateName().equalsIgnoreCase("Travelling Allowance")
						||ctc.getCtcTemplateName().contains("Travelling")){
					conveyance=conveyance+ctc.getAmount();
				}
			}
		}
		
		double sumofchecklist = 0;
		if(employee!=null && employee.getCheckList().size()!=0){
			for(EmployeeAssetBean empBean : employee.getCheckList()){
				if(empBean.isRecordSelect()==false){
					sumofchecklist += empBean.getPrice();
				}
			}
		}
		if(sumofchecklist!=0){
			System.out.println("sumofchecklist "+sumofchecklist);
			netEarning = netEarning+sumofchecklist;
		}
		
		outerTbl.addCell(cell.getCell("1", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell1=cell.getCell(payrollMonth+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell1.setBorderWidthLeft(0);
		cell1.setBorderWidthRight(0);
		cell1.setBorderWidthTop(0);
		outerTbl.addCell(cell1);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell12=cell.getCell(netEarning+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell12.setBorderWidthLeft(0);
		cell12.setBorderWidthRight(0);
		cell12.setBorderWidthTop(0);
		outerTbl.addCell(cell12);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell13=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell13.setBorderWidthLeft(0);
		cell13.setBorderWidthRight(0);
		cell13.setBorderWidthTop(0);
		outerTbl.addCell(cell13);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		
		outerTbl.addCell(cell.getCell("2", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell21=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell21.setBorderWidthLeft(0);
		cell21.setBorderWidthRight(0);
		cell21.setBorderWidthTop(0);
		outerTbl.addCell(cell21);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell122=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell122.setBorderWidthLeft(0);
		cell122.setBorderWidthRight(0);
		cell122.setBorderWidthTop(0);
		outerTbl.addCell(cell122);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell23=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell23.setBorderWidthLeft(0);
		cell23.setBorderWidthRight(0);
		cell23.setBorderWidthTop(0);
		outerTbl.addCell(cell23);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		outerTbl.addCell(cell.getCell("3", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell31=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell31.setBorderWidthLeft(0);
		cell31.setBorderWidthRight(0);
		cell31.setBorderWidthTop(0);
		outerTbl.addCell(cell31);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell132=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell132.setBorderWidthLeft(0);
		cell132.setBorderWidthRight(0);
		cell132.setBorderWidthTop(0);
		outerTbl.addCell(cell132);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell133=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell133.setBorderWidthLeft(0);
		cell133.setBorderWidthRight(0);
		cell133.setBorderWidthTop(0);
		outerTbl.addCell(cell133);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		outerTbl.addCell(cell.getCell("4", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell41=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell41.setBorderWidthLeft(0);
		cell41.setBorderWidthRight(0);
		cell41.setBorderWidthTop(0);
		outerTbl.addCell(cell41);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell142=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell142.setBorderWidthLeft(0);
		cell142.setBorderWidthRight(0);
		cell142.setBorderWidthTop(0);
		outerTbl.addCell(cell142);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell143=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell143.setBorderWidthLeft(0);
		cell143.setBorderWidthRight(0);
		cell143.setBorderWidthTop(0);
		outerTbl.addCell(cell143);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(netEarning+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
		
		
		
		outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 2, 9, 0)).setBorder(0);
		
		
		//Conveyance Pendinng SI.No.
		outerTbl.addCell(cell.getCell("Conveyance Pendinng SI.No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Month (Operations)", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Payroll (Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Chq. No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Accounts Final(Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("1", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell11=cell.getCell(payrollMonth+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell11.setBorderWidthLeft(0);
		cell11.setBorderWidthRight(0);
		cell11.setBorderWidthTop(0);
		outerTbl.addCell(cell11);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell121=cell.getCell(conveyance+"", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell121.setBorderWidthLeft(0);
		cell121.setBorderWidthRight(0);
		cell121.setBorderWidthTop(0);
		outerTbl.addCell(cell121);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell131=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell131.setBorderWidthLeft(0);
		cell131.setBorderWidthRight(0);
		cell131.setBorderWidthTop(0);
		outerTbl.addCell(cell131);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		
		outerTbl.addCell(cell.getCell("2", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell212=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell212.setBorderWidthLeft(0);
		cell212.setBorderWidthRight(0);
		cell212.setBorderWidthTop(0);
		outerTbl.addCell(cell212);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell1222=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell1222.setBorderWidthLeft(0);
		cell1222.setBorderWidthRight(0);
		cell1222.setBorderWidthTop(0);
		outerTbl.addCell(cell1222);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell232=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell232.setBorderWidthLeft(0);
		cell232.setBorderWidthRight(0);
		cell232.setBorderWidthTop(0);
		outerTbl.addCell(cell232);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		outerTbl.addCell(cell.getCell("3", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell313=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell313.setBorderWidthLeft(0);
		cell313.setBorderWidthRight(0);
		cell313.setBorderWidthTop(0);
		outerTbl.addCell(cell313);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell1323=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell1323.setBorderWidthLeft(0);
		cell1323.setBorderWidthRight(0);
		cell1323.setBorderWidthTop(0);
		outerTbl.addCell(cell1323);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell1334=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell1334.setBorderWidthLeft(0);
		cell1334.setBorderWidthRight(0);
		cell1334.setBorderWidthTop(0);
		outerTbl.addCell(cell1334);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		outerTbl.addCell(cell.getCell("4", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell411=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell411.setBorderWidthLeft(0);
		cell411.setBorderWidthRight(0);
		cell411.setBorderWidthTop(0);
		outerTbl.addCell(cell411);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell1421=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell1421.setBorderWidthLeft(0);
		cell1421.setBorderWidthRight(0);
		cell1421.setBorderWidthTop(0);
		outerTbl.addCell(cell1421);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell1431=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell1431.setBorderWidthLeft(0);
		cell1431.setBorderWidthRight(0);
		cell1431.setBorderWidthTop(0);
		outerTbl.addCell(cell1431);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(conveyance+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
		
		
		
		
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 9, 0)).setBorder(0);
		
		//Other Additions SL.No.
		outerTbl.addCell(cell.getCell("Other Additions SL.No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Month (Operations)", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("As Per Payroll (Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Chq. No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Accounts Final(Rs.)", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("1", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell111=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell111.setBorderWidthLeft(0);
		cell111.setBorderWidthRight(0);
		cell111.setBorderWidthTop(0);
		outerTbl.addCell(cell111);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell1211=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell1211.setBorderWidthLeft(0);
		cell1211.setBorderWidthRight(0);
		cell1211.setBorderWidthTop(0);
		outerTbl.addCell(cell1211);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell1311=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell1311.setBorderWidthLeft(0);
		cell1311.setBorderWidthRight(0);
		cell1311.setBorderWidthTop(0);
		outerTbl.addCell(cell1311);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		
		outerTbl.addCell(cell.getCell("2", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell2122=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell2122.setBorderWidthLeft(0);
		cell2122.setBorderWidthRight(0);
		cell2122.setBorderWidthTop(0);
		outerTbl.addCell(cell2122);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell12222=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell12222.setBorderWidthLeft(0);
		cell12222.setBorderWidthRight(0);
		cell12222.setBorderWidthTop(0);
		outerTbl.addCell(cell12222);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell2322=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell2322.setBorderWidthLeft(0);
		cell2322.setBorderWidthRight(0);
		cell2322.setBorderWidthTop(0);
		outerTbl.addCell(cell2322);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		outerTbl.addCell(cell.getCell("3", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		PdfPCell cell3133=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell3133.setBorderWidthLeft(0);
		cell3133.setBorderWidthRight(0);
		cell3133.setBorderWidthTop(0);
		outerTbl.addCell(cell3133);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell cell13233=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell13233.setBorderWidthLeft(0);
		cell13233.setBorderWidthRight(0);
		cell13233.setBorderWidthTop(0);
		outerTbl.addCell(cell13233);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell cell13343=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 2, 0);
		cell13343.setBorderWidthLeft(0);
		cell13343.setBorderWidthRight(0);
		cell13343.setBorderWidthTop(0);
		outerTbl.addCell(cell13343);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0));
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable outerTbl1=new PdfPTable(9);
		outerTbl1.setWidthPercentage(100f);
		outerTbl1.setSpacingBefore(5f);
		try {
			outerTbl1.setWidths(column9Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String leaveBalance="Leave Balance: Final as per HR/Payroll-Days   "+totalPl+"     x     Per Day Salary      "+df.format(perDaySal)+"     =     ";
		String calculation="CALCULATIONS: Sal Earned(This FY)= ________ + ________ Days Worked=          = Per Day Salary ";
		
		outerTbl1.addCell(cell.getCell(leaveBalance, font8bold, Element.ALIGN_LEFT, 0, 8, 0)).setBorder(0);
		outerTbl1.addCell(cell.getCell(df.format(plAmount)+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
		
		PdfPTable outerTbl2=new PdfPTable(9);
		outerTbl2.setWidthPercentage(100f);
		outerTbl2.setSpacingBefore(5f);
		try {
			outerTbl2.setWidths(column9Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		outerTbl2.addCell(cell.getCell(calculation, font8bold, Element.ALIGN_LEFT, 0, 8, 0)).setBorder(0);
		outerTbl2.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0));
		
		PdfPTable outerTbl3=new PdfPTable(9);
		outerTbl3.setWidthPercentage(100f);
		outerTbl3.setSpacingBefore(5f);
		try {
			outerTbl3.setWidths(column9Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		totalAddition=netEarning+conveyance+plAmount;
		outerTbl3.addCell(cell.getCell("(A) GRAND TOTAL ADDITION ", font8bold, Element.ALIGN_RIGHT, 0, 8, 0)).setBorder(0);
		outerTbl3.addCell(cell.getCell(df.format(totalAddition)+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
		
		PdfPTable outerTbl4=new PdfPTable(9);
		outerTbl4.setWidthPercentage(100f);
		outerTbl4.setSpacingBefore(5f);
		try {
			outerTbl4.setWidths(column9Widths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfPCell bottomCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 9, 0);
		bottomCell.setBorderWidthLeft(0);
		bottomCell.setBorderWidthRight(0);
		bottomCell.setBorderWidthTop(0);
		outerTbl4.addCell(bottomCell);
		
		try {
			
			document.add(outerTbl1);
			document.add(outerTbl2);
			document.add(outerTbl3);
			document.add(outerTbl4);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createFormHeading() {
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		String fnfDate="";
		if(employee!=null&&employee.getFnfDate()!=null){
			fnfDate=format.format(employee.getFnfDate());
		}
		
		
		PdfPCell headingCell=cell.getCell("FULL & FINAL SETTLEMENT OF EMPLOYEE(Dated : "+fnfDate+ ")(F & F Date : "+fnfDate, font12bold, Element.ALIGN_LEFT, 0, 0, 0);
		headingCell.setBorderWidthLeft(0);
		headingCell.setBorderWidthRight(0);
		headingCell.setBorderWidthTop(0);
		headingCell.setVerticalAlignment(Element.ALIGN_TOP);
		outerTbl.addCell(headingCell);
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createEmployeesDetails() {
		// TODO Auto-generated method stub
		//Form3aPdf
		PdfPTable outerTbl=new PdfPTable(7);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		
		try {
			outerTbl.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PdfPCell idCell=cell.getCell("Employee ID", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		idCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(idCell).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell empIdCell=cell.getCell(employee.getCount()+"", font8, Element.ALIGN_LEFT, 0, 0, 0);
		empIdCell.setBorderWidthLeft(0);
		empIdCell.setBorderWidthRight(0);
		empIdCell.setBorderWidthTop(0);
		outerTbl.addCell(empIdCell);
		
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell dateCell=cell.getCell("Date of Joining", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		dateCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(dateCell).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell dojCell=cell.getCell(format.format(employee.getJoinedAt()), font8, Element.ALIGN_LEFT, 0, 0, 0);
		dojCell.setBorderWidthLeft(0);
		dojCell.setBorderWidthRight(0);
		dojCell.setBorderWidthTop(0);
		outerTbl.addCell(dojCell);
		
		
		PdfPCell empCell=cell.getCell("Emp Name", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		empCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(empCell).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell empNameCell=cell.getCell(employee.getFullname(), font8, Element.ALIGN_LEFT, 0, 0, 0);
		empNameCell.setBorderWidthLeft(0);
		empNameCell.setBorderWidthRight(0);
		empNameCell.setBorderWidthTop(0);
		outerTbl.addCell(empNameCell);
		
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell lastDayCell=cell.getCell("Last Date of Work", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		lastDayCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(lastDayCell).setBorder(0);
		
		String lastDate="";
		if(employee.getLastWorkingDate()!=null){
			lastDate=format.format(employee.getLastWorkingDate());
		}
		
		if(employee.getProjectName()!=null){
			if(lastDate.equals("")){
				lastDate=lastDate+"\t"+" Last Site At: "+employee.getProjectName();
			}else{
				lastDate=lastDate+" Last Site At: "+employee.getProjectName();
			}
		}
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell lastDateCell=cell.getCell(lastDate, font8, Element.ALIGN_LEFT, 0, 0, 0);
		lastDateCell.setBorderWidthLeft(0);
		lastDateCell.setBorderWidthRight(0);
		lastDateCell.setBorderWidthTop(0);
		outerTbl.addCell(lastDateCell);
		
		
		PdfPCell desgCell=cell.getCell("Emp Designation", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		desgCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(desgCell).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell empDesgCell=cell.getCell(employee.getDesignation(), font8, Element.ALIGN_LEFT, 0, 0, 0);
		empDesgCell.setBorderWidthLeft(0);
		empDesgCell.setBorderWidthRight(0);
		empDesgCell.setBorderWidthTop(0);
		outerTbl.addCell(empDesgCell);
		
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell cardCell=cell.getCell("ID Card Handed To", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		cardCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(cardCell).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell idCardCell=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		idCardCell.setBorderWidthLeft(0);
		idCardCell.setBorderWidthRight(0);
		idCardCell.setBorderWidthTop(0);
		outerTbl.addCell(idCardCell);
		
		
		PdfPCell leavingCell=cell.getCell("Reason For Leaving", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		leavingCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(leavingCell).setBorder(0);
		
		String reason="";
		if(employee.getReasonForLeaving()!=null){
			reason=employee.getReasonForLeaving();
		}
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell reasonCell=cell.getCell(reason, font8, Element.ALIGN_LEFT, 0, 0, 0);
		reasonCell.setBorderWidthLeft(0);
		reasonCell.setBorderWidthRight(0);
		reasonCell.setBorderWidthTop(0);
		outerTbl.addCell(reasonCell);
		
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell bankCell=cell.getCell("Bank Account No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		bankCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		outerTbl.addCell(bankCell).setBorder(0);
		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		String accountNumber="";
		if(employee.getEmployeeBankAccountNo()!=null){
			accountNumber=employee.getEmployeeBankAccountNo();
		}
		PdfPCell acNoCell=cell.getCell(accountNumber+"", font8, Element.ALIGN_LEFT, 0, 0, 0);
		acNoCell.setBorderWidthLeft(0);
		acNoCell.setBorderWidthRight(0);
		acNoCell.setBorderWidthTop(0);
		outerTbl.addCell(acNoCell);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
