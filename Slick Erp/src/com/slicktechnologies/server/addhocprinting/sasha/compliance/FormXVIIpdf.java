package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.EmployeeOvertimeHistory;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;

public class FormXVIIpdf {
	public Document document;
	Company comp;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul,font16bold,font7;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	SimpleDateFormat salFmt = new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat salMFmt = new SimpleDateFormat("MMMM-yyyy");
	
	ArrayList<PaySlip> paySlipList=new ArrayList<PaySlip>();
	
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	int pageCounter=0;
	boolean nextPageFlag=false;
	int index=0;
	int perPageLimit=0;
	
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	
	ArrayList<String> earningHeaderList=new ArrayList<String>();
	ArrayList<String> deductionHeaderList=new ArrayList<String>();
	EmploymentCardPdf cell = new EmploymentCardPdf();
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	
	Customer customer;
	HrProject hrproject;
	
	int maxColWidth=16;
	int noOfEarningHeader=0;
	int noOfDeductionHeader=0;
	int noOfEarningRow=1;
	int noOfDeductionRow=1;
	
	HashMap<String,Double> earningSummarryMap=new HashMap<String,Double>();
	HashMap<String,Double> deductionSummarryMap=new HashMap<String,Double>();
	int counter=0;
	
	
	int totalNoOfRecord=0;
	int noOfRowUnderGroupRow=0;
	int noOfLineonFirstPage=0;
	int noOfElementOnFirstPage=0;
	int noOfLineOnRestPages=0;
	int noOfElementOnRestPage=0;
	int totalNoOfPage=0;
	int summaryTableLine=0;
	int elementCounter=0;
	
	/**
	 * @author Anil
	 * @since 02-09-2020
	 * Added site location for sun facility raised by Rahul tiwari
	 */
	boolean siteLocation=false;
	
	public FormXVIIpdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		salFmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		salMFmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setPayslip(long companyId, ArrayList<PaySlip> paySlipList){
		this.paySlipList=paySlipList;
		Date date=null;
		try {
			date=salFmt.parse(this.paySlipList.get(0).getSalaryPeriod());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Comparator<PaySlip> comparator=new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip arg0, PaySlip arg1) {
				Integer count1=arg0.getEmpid();
				Integer count2=arg1.getEmpid();
				return count1.compareTo(count2);
			}
		};
		Collections.sort(this.paySlipList, comparator);
		logger.log(Level.SEVERE,"PAY SLIP SIZE "+this.paySlipList.size());
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
	
		if(paySlipList!=null){
			hrproject=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", paySlipList.get(0).getProjectName()).first().now();
		}
		
		if(hrproject!=null){
			customer=ofy().load().type(Customer.class).filter("count",hrproject.getPersoninfo().getCount()).filter("companyId",companyId).first().now();
		}
		
		/**
		 * @author Anil
		 * @since 02-09-2020
		 * site location
		 */
		HashSet<Integer>custIdHs=new HashSet<Integer>();
		HashSet<String>projectHs=new HashSet<String>();
		
		HashSet<String> earningHs=new HashSet<String>();
		HashSet<String> deductionHs=new HashSet<String>();
		for(PaySlip obj:paySlipList){
			for(CtcComponent comp:obj.getEarningList()){
				if(comp.isArrears()==true){
					earningHs.add(comp.getArrearOf()+" "+comp.getName());
				}else{
					earningHs.add(comp.getName());
				}
			}
			if(obj.isOtIncludedInGross()==true){
				if(obj.getEmpOtHistoryList()!=null&&obj.getEmpOtHistoryList().size()!=0){
					for(EmployeeOvertimeHistory ot:obj.getEmpOtHistoryList()){
						earningHs.add(ot.getCorrespondanceName());
					}
				}
			}
			
			for(CtcComponent comp:obj.getDeductionList()){
				if(comp.getShortName()==null||comp.getShortName().equals("")){
					deductionHs.add(comp.getName());
				}else{
					deductionHs.add(comp.getShortName());
				}
			}
			
			/**
			 * @author Anil
			 * @since 02-09-2020
			 * site location
			 */
			if(obj.getProjectName()!=null){
				projectHs.add(obj.getProjectName());
			}
			if(obj.getClientInfo()!=null){
				custIdHs.add(obj.getClientInfo().getCount());
			}
		}
		
		if(custIdHs.size()>1){
			customer=null;
		}
		if(projectHs.size()>1){
			hrproject=null;
		}
		
		if(earningHs.size()!=0){
			
			ArrayList<String>list=new ArrayList<String>(earningHs);
			Collections.sort(list);
			ArrayList<String> alignList=new ArrayList<String>();
			alignList.add("Basic");
			alignList.add("Da");
			alignList.add("Hra");
			alignList.add("Washing Allowance");
			alignList.add("Basic Arrears");
			alignList.add("Da Arrears");
			alignList.add("Hra Arrears");
			
			for(String obj:alignList){
				for(String obj1:list){
					if(obj.equalsIgnoreCase(obj1)){
						earningHeaderList.add(obj1);
					}
				}
			}
			list.removeAll(earningHeaderList);
			earningHeaderList.addAll(list);
			
//			earningHeaderList.addAll(earningHs);
			noOfEarningHeader=earningHeaderList.size();
			if(noOfEarningHeader>10){
				noOfEarningRow=(int) Math.ceil(noOfEarningHeader/10.0);
			}
			logger.log(Level.SEVERE,"EARNING HEADER LIST SIZE "+earningHeaderList.size()+" / "+noOfEarningHeader+" ROW : "+noOfEarningRow);
		}
		
		if(deductionHs.size()!=0){
			
			ArrayList<String>list=new ArrayList<String>(deductionHs);
			Collections.sort(list);
			ArrayList<String> alignList=new ArrayList<String>();
			alignList.add("PT");
			alignList.add("PF");
			alignList.add("ESIC");
			alignList.add("LWF");
			
			for(String obj:alignList){
				for(String obj1:list){
					if(obj.equalsIgnoreCase(obj1)){
						deductionHeaderList.add(obj1);
					}
				}
			}
			list.removeAll(deductionHeaderList);
			deductionHeaderList.addAll(list);
			
//			deductionHeaderList.addAll(deductionHs);
			noOfDeductionHeader=deductionHeaderList.size();
			if(noOfDeductionHeader>10){
				noOfDeductionRow=(int) Math.ceil(noOfDeductionHeader/10.0);
			}
			logger.log(Level.SEVERE,"DEDUCTION HEADER LIST SIZE "+deductionHeaderList.size()+" / "+noOfDeductionHeader+" ROW : "+noOfDeductionRow);
		}
		pageSetup();
		
		siteLocation=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip", "EnableSiteLocation", companyId);
	}
	
	
	public void pageSetup(){
		totalNoOfRecord=paySlipList.size();
		noOfRowUnderGroupRow=5;
		noOfLineonFirstPage=30;
		noOfElementOnFirstPage=6;
		noOfLineOnRestPages=40;
		noOfElementOnRestPage=8;
		totalNoOfPage=0;
		summaryTableLine=6;
		
		double calculatedNoOfRowUnderGroupRow=(noOfEarningRow*2)+noOfDeductionRow+1;
		
		if(calculatedNoOfRowUnderGroupRow<=noOfRowUnderGroupRow){
			calculatedNoOfRowUnderGroupRow=noOfRowUnderGroupRow;
		}else{
			noOfElementOnFirstPage=(int) Math.round(noOfLineonFirstPage/calculatedNoOfRowUnderGroupRow);
			noOfElementOnRestPage=(int) Math.round(noOfLineOnRestPages/calculatedNoOfRowUnderGroupRow);
		}
		
		if((totalNoOfRecord+summaryTableLine)<noOfElementOnFirstPage){
			totalNoOfPage=1;
		}else{
			totalNoOfPage=1;
			int records=totalNoOfRecord-noOfElementOnFirstPage;
			records=records+summaryTableLine;
			totalNoOfPage=(int) (totalNoOfPage+Math.round((records/(double)noOfElementOnRestPage)));
		}
		
		logger.log(Level.SEVERE,"CALCULATED ROW GRP : "+calculatedNoOfRowUnderGroupRow);
		logger.log(Level.SEVERE,"TOTAL NO OF PAGE : "+totalNoOfPage);
		logger.log(Level.SEVERE,"NO OF ELEMENT ON FIRST PAGE : "+noOfElementOnFirstPage);
		logger.log(Level.SEVERE,"NO OF ELEMENT ON REST PAGE : "+noOfElementOnRestPage);
		
	}
	
	public void createPdf(){
		elementCounter=0;
		counter=0;
		boolean summaryFlag=false;
		
		createCompanyHeaderTable();
		
		if(totalNoOfPage>1){
			for(int i=1;i<=totalNoOfPage;i++){
				createHeaderTable();
				createValueTable(i,elementCounter);
				
				logger.log(Level.SEVERE,"ELEMENT COUNTER : "+elementCounter+" SIZE :: "+paySlipList.size());
				if(elementCounter==paySlipList.size()){
					if(i==1){
						if(elementCounter<noOfElementOnFirstPage){
							summaryFlag=true;
						}
					}else{
						if((elementCounter-noOfElementOnFirstPage+1)<(noOfElementOnRestPage*i)-noOfElementOnFirstPage){
							summaryFlag=true;
						}
					}
					if(summaryFlag){
						createSummarryHeaderTable();
						createSummarryValueTable();
					}else{
						try {
							document.add(Chunk.NEXTPAGE);
							PdfPTable tbl=new PdfPTable(1);
							tbl.addCell(cell.getCell(" ", font9, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
							document.add(tbl);
						} catch (DocumentException e) {
							e.printStackTrace();
						}
					}
					break;
				}else{
					try {
						document.add(Chunk.NEXTPAGE);
						PdfPTable tbl=new PdfPTable(1);
						tbl.addCell(cell.getCell(" ", font9, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
						document.add(tbl);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}
			}
		}else{
			summaryFlag=true;
			createHeaderTable();
			createValueTable(totalNoOfPage,elementCounter);
			createSummarryHeaderTable();
			createSummarryValueTable();
		}
		
		if(summaryFlag==false){
			createSummarryHeaderTable();
			createSummarryValueTable();
		}
		signaturePart();
	}
	
	private void createSummarryValueTable() {
		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		try {
			parentTbl.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
			
		
		PdfPTable leftTbl = new PdfPTable(4);
		leftTbl.setWidthPercentage(96);
		
		try {
			leftTbl.setWidths(new float[]{20,20,40,20});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		// Row 1
		leftTbl.addCell(cell.getCell(counter+" ", font7, Element.ALIGN_CENTER, 3, 3, 0)).setBorder(0);
//		leftTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_LEFT, 3, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		// Row 2
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			
		// Row 3
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		int noOfTblCol=12;
		if(noOfEarningRow==1&&noOfDeductionRow==1){
			if(noOfEarningHeader>noOfDeductionHeader){
				noOfTblCol=noOfEarningHeader+2;
			}else{
				noOfTblCol=noOfDeductionHeader+2;
			}
		}
		
		logger.log(Level.SEVERE,"RIGHT TBL NO. OF COL. "+noOfTblCol);
		
		PdfPTable rightTbl = new PdfPTable(noOfTblCol);
		rightTbl.setWidthPercentage(96);
		
		int col=0;
		for(int i=0;i<earningHeaderList.size();i++){
			col++;
			boolean colsFlag=false;
			String earningHeader=earningHeaderList.get(i);
			int colspan=0;
			if(col==(noOfTblCol-2)){
				colspan=2;
				colsFlag=true;
			}
			if(i==earningHeaderList.size()-1){
				if(earningHeaderList.size()<=((noOfTblCol-2)*noOfEarningRow)){
					colspan=((noOfTblCol-2)*noOfEarningRow)-earningHeaderList.size();
				}
			}
			if(colsFlag){
				col=0;
			}
			rightTbl.addCell(cell.getCell(getSummaryValue(true,earningHeader)+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(colspan!=0){
				while(colspan>0){
					rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					colspan--;
				}
			}
		}
		rightTbl.addCell(cell.getCell(getSummaryValue(true,"Earnings")+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		col=0;
		for(int i=0;i<deductionHeaderList.size();i++){
			col++;
			boolean colsFlag=false;
			String deductionHeader=deductionHeaderList.get(i);
			int colspan=0;
			if(col==(noOfTblCol-2)){
				colspan=2;
				colsFlag=true;
			}
			if(i==deductionHeaderList.size()-1){
				if(deductionHeaderList.size()<=((noOfTblCol-2)*noOfDeductionRow)){
					colspan=((noOfTblCol-2)*noOfDeductionRow)-deductionHeaderList.size();
				}
			}
			if(colsFlag){
				col=0;
			}
			rightTbl.addCell(cell.getCell(getSummaryValue(false,deductionHeader)+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(colspan!=0){
				while(colspan>0){
					rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					colspan--;
				}
			}
		}
		rightTbl.addCell(cell.getCell(getSummaryValue(false,"Deduction")+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		PdfPCell leftCell=new PdfPCell(leftTbl);
		leftCell.setBorderWidthLeft(0f);
		leftCell.setBorderWidthRight(0f);
		PdfPCell rightCell=new PdfPCell(rightTbl);
		rightCell.setBorderWidthLeft(0f);
		rightCell.setBorderWidthRight(0f);
		
		
		
		
		
		
		parentTbl.addCell(leftCell);
		parentTbl.addCell(rightCell);
		try {
			document.add(parentTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private double getSummaryValue(boolean isEarning, String earningHeader) {
		// TODO Auto-generated method stub
		if(isEarning){
			if(earningSummarryMap!=null&&earningSummarryMap.size()!=0){
				if(earningSummarryMap.containsKey(earningHeader)){
					return earningSummarryMap.get(earningHeader);
				}
			}
		}else{
			if(deductionSummarryMap!=null&&deductionSummarryMap.size()!=0){
				if(deductionSummarryMap.containsKey(earningHeader)){
					return deductionSummarryMap.get(earningHeader);
				}
			}
		}
		return 0;
	}

	private void createSummarryHeaderTable() {

		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		try {
			parentTbl.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
			
		
		PdfPTable leftTbl = new PdfPTable(4);
		leftTbl.setWidthPercentage(96);
		try {
			leftTbl.setWidths(new float[]{20,20,40,20});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		// Row 1
		leftTbl.addCell(cell.getCell("No. of Employees", font7bold, Element.ALIGN_CENTER, 3, 3, 0)).setBorder(0);
//		leftTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_LEFT, 3, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		// Row 2
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			
		// Row 3
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		int noOfTblCol=12;
		if(noOfEarningRow==1&&noOfDeductionRow==1){
			if(noOfEarningHeader>noOfDeductionHeader){
				noOfTblCol=noOfEarningHeader+2;
			}else{
				noOfTblCol=noOfDeductionHeader+2;
			}
		}
		
		logger.log(Level.SEVERE,"RIGHT TBL NO. OF COL. "+noOfTblCol);
		
		PdfPTable rightTbl = new PdfPTable(noOfTblCol);
		rightTbl.setWidthPercentage(96);
		
		int col=0;
		for(int i=0;i<earningHeaderList.size();i++){
			col++;
			boolean colsFlag=false;
			String earningHeader=earningHeaderList.get(i);
			int colspan=0;
			if(col==(noOfTblCol-2)){
				colspan=2;
				colsFlag=true;
				logger.log(Level.SEVERE,"BF EARNING COLSPAN "+colspan);
			}
			if(i==earningHeaderList.size()-1){
				if(earningHeaderList.size()<=((noOfTblCol-2)*noOfEarningRow)){
					colspan=((noOfTblCol-2)*noOfEarningRow)-earningHeaderList.size();
				}
			}
			if(colsFlag){
				col=0;
			}
			rightTbl.addCell(cell.getCell(earningHeader, font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(colspan!=0){
				while(colspan>0){
					rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					colspan--;
				}
			}
		}
		rightTbl.addCell(cell.getCell("Earnings", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		col=0;
		for(int i=0;i<deductionHeaderList.size();i++){
			col++;
			boolean colsFlag=false;
			String deductionHeader=deductionHeaderList.get(i);
			int colspan=0;
			if(col==(noOfTblCol-2)){
				colspan=2;
				colsFlag=true;
			}
			if(i==deductionHeaderList.size()-1){
				if(deductionHeaderList.size()<=((noOfTblCol-2)*noOfDeductionRow)){
					colspan=((noOfTblCol-2)*noOfDeductionRow)-deductionHeaderList.size();
				}
			}
			if(colsFlag){
				col=0;
			}
			rightTbl.addCell(cell.getCell(deductionHeader, font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(colspan!=0){
				while(colspan>0){
					rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					colspan--;
				}
			}
		}
		rightTbl.addCell(cell.getCell("Deduction", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		PdfPCell leftCell=new PdfPCell(leftTbl);
		leftCell.setBorderWidthLeft(0f);
		leftCell.setBorderWidthRight(0f);
		PdfPCell rightCell=new PdfPCell(rightTbl);
		rightCell.setBorderWidthLeft(0f);
		rightCell.setBorderWidthRight(0f);
		
		PdfPCell centerCell=new PdfPCell(new Phrase("SUMMARY",font7bold));
		centerCell.setBorderWidthLeft(0f);
		centerCell.setBorderWidthRight(0f);
		centerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centerCell.setColspan(2);
		
		
		parentTbl.addCell(centerCell);
		parentTbl.addCell(leftCell);
		parentTbl.addCell(rightCell);
		try {
			document.add(parentTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	
	}
	
	private void createValueTable(int pageNumber,int startPoint) {

		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		try {
			parentTbl.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		int k=0;
		for(int j=startPoint;j<paySlipList.size();j++){
			
			
			elementCounter=j;
			if(pageNumber==1){
				if(k+1>noOfElementOnFirstPage){
					break;
				}
			}else{
				if(k+1>noOfElementOnRestPage){
					break;
				}
			}
			k++;
			
			PdfPTable leftTbl = new PdfPTable(4);
			leftTbl.setWidthPercentage(96);
			
			try {
				leftTbl.setWidths(new float[]{20,20,40,20});
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
			
			int noOfTblCol=12;
			if(noOfEarningRow==1&&noOfDeductionRow==1){
				if(noOfEarningHeader>noOfDeductionHeader){
					noOfTblCol=noOfEarningHeader+2;
				}else{
					noOfTblCol=noOfDeductionHeader+2;
				}
			}
			
			PdfPTable rightTbl = new PdfPTable(noOfTblCol);
			rightTbl.setWidthPercentage(96);
			
			
			PaySlip ps=paySlipList.get(j);
			// Row 1
			leftTbl.addCell(cell.getCell(counter+1+"", font7, Element.ALIGN_CENTER, 3, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell(ps.getEmpid()+"", font7, Element.ALIGN_LEFT, 3, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell(ps.getEmployeeName(), font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			/**
			 * Date : 01-10-2019 @author Anil
			 * As per the requirement showing paid days - weekdays i.e paid days
			 */
			if(ps.isFixedDayWisePayroll()){
				leftTbl.addCell(cell.getCell((ps.getPaidDays())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			}else{
				leftTbl.addCell(cell.getCell((ps.getPaidDays()-ps.getWeekDays())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			}
			// Row 2
			
			if(siteLocation){
				leftTbl.addCell(cell.getCell(ps.getEmployeedDesignation(), font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}else{
				leftTbl.addCell(cell.getCell(ps.getEmployeedDesignation(), font7, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
			}
//			leftTbl.addCell(cell.getCell(ps.getEmployeedDesignation(), font7, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
			
			leftTbl.addCell(cell.getCell(ps.getWeekDays()+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			
			String site=" ";
			if(ps.getSiteLocation()!=null&&!ps.getSiteLocation().equals("")){
				site=ps.getSiteLocation();
			}
			leftTbl.addCell(cell.getCell(site, font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			if(ps.isFixedDayWisePayroll()){
			leftTbl.addCell(cell.getCell(ps.getWoDayAsExtraDay()+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);	
			}
			// Row 3
			leftTbl.addCell(cell.getCell("", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			
			
			
			// Row 4
			String pfNum="";
			if(comp!=null&&comp.getPfGroupNum()!=null&&!comp.getPfGroupNum().equals("")){
				pfNum=comp.getPfGroupNum();
			}
			if(!pfNum.equals("")&&ps.getPfNumber()!=null&&!ps.getPfNumber().equals("")){
				pfNum=pfNum+"/"+ps.getPfNumber();
			}else{
				pfNum="";
			}
					
			leftTbl.addCell(cell.getCell("", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell("PF No. :", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell(pfNum+"", font7, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
			
			// Row 5
			leftTbl.addCell(cell.getCell("", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell("ESIC No. :", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell(ps.getEsicNumber()+"", font7, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
			
			// Row 6
			leftTbl.addCell(cell.getCell("", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell("UAN No. :", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(cell.getCell(ps.getUanNo()+"", font7, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
						
		
			
			
			int col=0;
			for(int i=0;i<earningHeaderList.size();i++){
				double value=0;
				col++;
				boolean colsFlag=false;
				String earningHeader=earningHeaderList.get(i);
				int colspan=0;
				if(col==(noOfTblCol-2)){
					colspan=2;
					colsFlag=true;
				}
				if(i==earningHeaderList.size()-1){
					if(earningHeaderList.size()<=((noOfTblCol-2)*noOfEarningRow)){
						colspan=((noOfTblCol-2)*noOfEarningRow)-earningHeaderList.size();
					}
				}
				if(colsFlag){
					col=0;
				}
				value=getComponentValue(true,ps,earningHeader,false);
				rightTbl.addCell(cell.getCell(value+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				if(colspan!=0){
					while(colspan>0){
						rightTbl.addCell(cell.getCell("", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
						colspan--;
					}
				}
				
				if(earningSummarryMap.containsKey(earningHeader)){
					earningSummarryMap.put(earningHeader, earningSummarryMap.get(earningHeader)+value);
				}else{
					earningSummarryMap.put(earningHeader, value);
				}
			}
			rightTbl.addCell(cell.getCell(ps.getGrossEarning()+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell("", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			
			if(earningSummarryMap.containsKey("Earnings")){
				earningSummarryMap.put("Earnings", earningSummarryMap.get("Earnings")+ps.getGrossEarning());
			}else{
				earningSummarryMap.put("Earnings", ps.getGrossEarning());
			}
			
			col=0;
			for(int i=0;i<deductionHeaderList.size();i++){
				double value=0;
				col++;
				boolean colsFlag=false;
				String deductionHeader=deductionHeaderList.get(i);
				int colspan=0;
				if(col==(noOfTblCol-2)){
					colspan=2;
					colsFlag=true;
				}
				if(i==deductionHeaderList.size()-1){
					if(deductionHeaderList.size()<=((noOfTblCol-2)*noOfDeductionRow)){
						colspan=((noOfTblCol-2)*noOfDeductionRow)-deductionHeaderList.size();
					}
				}
				if(colsFlag){
					col=0;
				}
				value=getComponentValue(false, ps, deductionHeader, false);
				rightTbl.addCell(cell.getCell(value+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				if(colspan!=0){
					while(colspan>0){
						rightTbl.addCell(cell.getCell("", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
						colspan--;
					}
				}
				if(deductionSummarryMap.containsKey(deductionHeader)){
					deductionSummarryMap.put(deductionHeader, deductionSummarryMap.get(deductionHeader)+value);
				}else{
					deductionSummarryMap.put(deductionHeader, value);
				}
			}
			rightTbl.addCell(cell.getCell(ps.getTotalDeduction()+"", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(deductionSummarryMap.containsKey("Deduction")){
				deductionSummarryMap.put("Deduction", deductionSummarryMap.get("Deduction")+ps.getTotalDeduction());
			}else{
				deductionSummarryMap.put("Deduction", ps.getTotalDeduction());
			}
			
			
			
			col=0;
			for(int i=0;i<earningHeaderList.size();i++){
				col++;
				boolean colsFlag=false;
				String earningHeader=earningHeaderList.get(i);
				int colspan=0;
				if(col==(noOfTblCol-2)){
					colspan=2;
					colsFlag=true;
				}
				if(i==earningHeaderList.size()-1){
					if(earningHeaderList.size()<=((noOfTblCol-2)*noOfEarningRow)){
						colspan=((noOfTblCol-2)*noOfEarningRow)-earningHeaderList.size();
					}
				}
				if(colsFlag){
					col=0;
				}
				String rate="";
				double amt=getComponentValue(true,ps,earningHeader,true);
				if(amt!=0){
					rate=amt+"";
				}
				rightTbl.addCell(cell.getCell(rate, font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
				if(colspan!=0){
					while(colspan>0){
						rightTbl.addCell(cell.getCell("", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
						colspan--;
					}
				}
			}
			rightTbl.addCell(cell.getCell("", font7, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell("", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			
			
			//
			
			int sumOfTotalRowCol=(noOfEarningRow*2)+noOfDeductionRow;
//			logger.log(Level.SEVERE,"TOTAL ROW : "+sumOfTotalRowCol);
			int column=(int) Math.round((noOfTblCol-2)/3.0);
			int var=0;
			if((column*3)==(noOfTblCol-2)){
				
			}else if((column*3)<(noOfTblCol-2)){
				var=(noOfTblCol-2)-(column*3);
			}else{
				var=(noOfTblCol-2)-(column*3);
			}
			if(sumOfTotalRowCol<=4){
				int diff=4-sumOfTotalRowCol;
//				logger.log(Level.SEVERE,"DIFF : "+diff);
				do{
					rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, column+var, 0)).setBorder(0);
					rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, column, 0)).setBorder(0);
					rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_CENTER, 0,column, 0)).setBorder(0);
					rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
					diff--;
				}while(diff>0);
			}
			
			
			String bankName="";
			if(ps.getBankName()!=null){
				bankName=ps.getBankName();
			}
			if(!bankName.equals("")&&ps.getBankBranch()!=null&&!ps.getBankBranch().equals("")){
				bankName=bankName+"/"+ps.getBankBranch();
			}
			rightTbl.addCell(cell.getCell("BANK : "+bankName, font7bold, Element.ALIGN_LEFT, 0, column+var, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell("Account No. : "+ps.getAccountNumber(), font7bold, Element.ALIGN_LEFT, 0, column, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell("Net Payment : ", font7bold, Element.ALIGN_CENTER, 0, column, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell(ps.getNetEarning()+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			//
			
			
			
			PdfPCell leftCell=new PdfPCell(leftTbl);
			leftCell.setBorderWidthLeft(0f);
			leftCell.setBorderWidthRight(0f);
			PdfPCell rightCell=new PdfPCell(rightTbl);
			rightCell.setBorderWidthLeft(0f);
			rightCell.setBorderWidthRight(0f);
			
			parentTbl.addCell(leftCell);
			parentTbl.addCell(rightCell);
			counter++;
			elementCounter=j+1;
		}
		
		
		
		
		
		try {
			document.add(parentTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	
		
	}

	private double getComponentValue(boolean isEarning,PaySlip ps, String compHeader,boolean isRate) {
		// TODO Auto-generated method stub
		if(isEarning){
			for(CtcComponent comp:ps.getEarningList()){
				if(comp.isArrears()==true){
					if(isRate==false&&compHeader.equals(comp.getArrearOf()+" "+comp.getName())){
						return comp.getActualAmount();
					}
				}
				if(comp.getName().equals(compHeader)){
					if(isRate){
						return Math.round(comp.getAmount()/12);
					}
					return comp.getActualAmount();
				}
			}
			if(isRate==false&&ps.isOtIncludedInGross()==true){
				double otAmt=0;
				if(ps.getEmpOtHistoryList()!=null&&ps.getEmpOtHistoryList().size()!=0){
					for(EmployeeOvertimeHistory comp:ps.getEmpOtHistoryList()){
						if(comp.getCorrespondanceName().equals(compHeader)){
							otAmt=otAmt+comp.getOtAmount();
//							return comp.getOtAmount();
						}
					}
				}
				return otAmt;
			}
			
		}else{
			for(CtcComponent comp:ps.getDeductionList()){
				if(comp.getShortName().equals(compHeader)){
					if(isRate){
						return comp.getAmount()/12;
					}
					return comp.getActualAmount();
				}
			}
		}
		return 0;
	}

	private void createHeaderTable() {
		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		try {
			parentTbl.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
			
		
		PdfPTable leftTbl = new PdfPTable(4);
		leftTbl.setWidthPercentage(96);
		
		try {
			leftTbl.setWidths(new float[]{20,20,40,20});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		// Row 1
		leftTbl.addCell(cell.getCell("Sr. No.", font7bold, Element.ALIGN_CENTER, 3, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("Emp Code", font7bold, Element.ALIGN_LEFT, 3, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("Employee Name", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("Duties", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		// Row 2
		if(siteLocation){
			leftTbl.addCell(cell.getCell("Designation", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			leftTbl.addCell(cell.getCell("Designation", font7bold, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
		}
//		leftTbl.addCell(cell.getCell("Designation", font7bold, Element.ALIGN_LEFT, 2, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("WO Days", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		if(siteLocation){
			leftTbl.addCell(cell.getCell("Site Location", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		leftTbl.addCell(cell.getCell("Extra Days", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);	
		// Row 3
		leftTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		
		int noOfTblCol=12;
		if(noOfEarningRow==1&&noOfDeductionRow==1){
			if(noOfEarningHeader>noOfDeductionHeader){
				noOfTblCol=noOfEarningHeader+2;
			}else{
				noOfTblCol=noOfDeductionHeader+2;
			}
		}
		
		logger.log(Level.SEVERE,"RIGHT TBL NO. OF COL. "+noOfTblCol);
		
		PdfPTable rightTbl = new PdfPTable(noOfTblCol);
		rightTbl.setWidthPercentage(96);
		
		int col=0;
		for(int i=0;i<earningHeaderList.size();i++){
			col++;
			boolean colsFlag=false;
			String earningHeader=earningHeaderList.get(i);
			int colspan=0;
			if(col==(noOfTblCol-2)){
				colspan=2;
				colsFlag=true;
				logger.log(Level.SEVERE,"BF EARNING COLSPAN "+colspan);
			}
			if(i==earningHeaderList.size()-1){
				if(earningHeaderList.size()<=((noOfTblCol-2)*noOfEarningRow)){
					colspan=((noOfTblCol-2)*noOfEarningRow)-earningHeaderList.size();
				}
				logger.log(Level.SEVERE,"M EARNING COLSPAN "+colspan);
			}
			if(colsFlag){
				col=0;
			}
//			logger.log(Level.SEVERE,"AF EARNING COLSPAN "+colspan);
			rightTbl.addCell(cell.getCell(earningHeader, font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(colspan!=0){
				while(colspan>0){
					rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					colspan--;
				}
			}
			if(earningSummarryMap.containsKey(earningHeader)==false){
				earningSummarryMap.put(earningHeader, 0.0);
			}
			
		}
		if(earningSummarryMap.containsKey("Earnings")==false){
			earningSummarryMap.put("Earnings", 0.0);
		}
		rightTbl.addCell(cell.getCell("Earnings", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell("Sign", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		col=0;
		for(int i=0;i<deductionHeaderList.size();i++){
			col++;
			boolean colsFlag=false;
			String deductionHeader=deductionHeaderList.get(i);
			int colspan=0;
			if(col==(noOfTblCol-2)){
				colspan=2;
//				col=0;
				colsFlag=true;
				logger.log(Level.SEVERE,"DEDUCTION COLSPAN "+colspan);
			}
			if(i==deductionHeaderList.size()-1){
				if(deductionHeaderList.size()<=((noOfTblCol-2)*noOfDeductionRow)){
					colspan=((noOfTblCol-2)*noOfDeductionRow)-deductionHeaderList.size();
				}
				logger.log(Level.SEVERE,"DEDUCTION COLSPAN "+colspan+" / "+deductionHeader);
			}
			if(colsFlag){
				col=0;
			}
			
			rightTbl.addCell(cell.getCell(deductionHeader, font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			if(colspan!=0){
				while(colspan>0){
					rightTbl.addCell(cell.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
					colspan--;
				}
			}
			if(deductionSummarryMap.containsKey(deductionHeader)==false){
				deductionSummarryMap.put(deductionHeader, 0.0);
			}
		}
		rightTbl.addCell(cell.getCell("Deduction", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		if(deductionSummarryMap.containsKey("Deduction")==false){
			deductionSummarryMap.put("Deduction", 0.0);
		}
		
		PdfPCell leftCell=new PdfPCell(leftTbl);
		leftCell.setBorderWidthLeft(0f);
		leftCell.setBorderWidthRight(0f);
		PdfPCell rightCell=new PdfPCell(rightTbl);
		rightCell.setBorderWidthLeft(0f);
		rightCell.setBorderWidthRight(0f);
		
		parentTbl.addCell(leftCell);
		parentTbl.addCell(rightCell);
		try {
			document.add(parentTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	public  void createCompanyHeaderTable(){
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		
		PdfPCell imageSignCell = null;
		if(comp!=null&&comp.getLogo()!=null){
			DocumentUpload logodocument =comp.getLogo();
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			Image image2=null;
			try {
				image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
				image2.scalePercent(20f);
				imageSignCell = new PdfPCell();
				imageSignCell.addElement(image2);
				imageSignCell.setBorder(0);
				imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				imageSignCell.setFixedHeight(15);
			} catch (Exception e) {
			}
		}
		
		PdfPTable parenTbl = new PdfPTable(3);
		parenTbl.setWidthPercentage(96);
		try {
			parenTbl.setWidths(new float[]{15,70,15});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase companyName= new Phrase(comp.getBusinessUnitName(),font12bold);
	    Paragraph p =new Paragraph();
	    p.add(companyName);
	    p.add(Chunk.NEWLINE);
	    p.setAlignment(Element.ALIGN_CENTER);
	     
		String addressline1="";
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2();
		}else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		if(comp.getAddress().getLandmark()!=null&&!comp.getAddress().getLandmark().equals("")){
			addressline1=addressline1+", "+comp.getAddress().getLandmark()+",";
		}
		
		Phrase locality=null;
		if(comp.getAddress().getLocality().equals("")==false){
			locality= new Phrase(addressline1+""+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+"-"+comp.getAddress().getPin()+".",font8);
		}
		else if(comp.getAddress().getLocality().equals("")==true){
			locality= new Phrase(addressline1+""+comp.getAddress().getCity()+"-"+comp.getAddress().getPin()+".",font8);
		}
		p.add(locality);
		p.add(Chunk.NEWLINE);
		
		PdfPCell companyNameCell=new PdfPCell();
	    companyNameCell.addElement(p);
	    companyNameCell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		companyDetails.addCell(companyNameCell);
		companyDetails.setSpacingAfter(10f);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		comapnyCell.setBorder(0);
		parent.addCell(comapnyCell);
		
		if(imageSignCell!=null){
			parenTbl.addCell(imageSignCell);
		}else{
			parenTbl.addCell(blankCell);
		}
		parenTbl.addCell(comapnyCell);
		parenTbl.addCell(blankCell);
		
		try {
			document.add(parenTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		Date yearWise=null;
		try {
			yearWise=salFmt.parse(paySlipList.get(0).getSalaryPeriod().trim());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		if(yearWise==null){
			yearWise=new Date();
		}
		
		
		String branch="";
		if(paySlipList!=null&&paySlipList.size()!=0){
			branch=paySlipList.get(0).getBranch();
		}
		String customerName=" ";
		if(customer!=null){
			if (customer.getCustPrintableName() != null&&!customer.getCustPrintableName().equals("")) {
				customerName = customer.getCustPrintableName();
			} else {
				if(siteLocation){
					if(customer.isCompany()){
						customerName=customer.getCompanyName();
					}
				}else{
					customerName = customer.getFullname();
					if(customer.isCompany()){
						customerName=customer.getCompanyName();
					}
				}
			}
		}else{
			customerName="";
		}
		
		String project=" ";
		if(hrproject!=null){
			project=hrproject.getProjectName();
		}
		
	    
	    PdfPTable locTbl=new PdfPTable(2);
		locTbl.setWidthPercentage(98);
		
		PdfPTable leftTbl=new PdfPTable(1);
		leftTbl.setWidthPercentage(98);
		leftTbl.addCell(cell.getCell("FORM XVII", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("REGISTER OF WAGES", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("Rule 78 (1) (a) (i)", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("Correspondance Name : "+customerName, font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell("Project Name                 : "+project, font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPTable rightTbl=new PdfPTable(1);
		rightTbl.setWidthPercentage(98);
		rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell("MONTH - YEAR    : "+salMFmt.format(yearWise), font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell("DATE OF PAYMENT : "+"____________", font7bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(cell.getCell(" ", font9bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell projectNmCell=new PdfPCell(leftTbl);
		projectNmCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		projectNmCell.setBorder(0);
		
		PdfPCell branchNmCell=new PdfPCell(rightTbl);
		branchNmCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		branchNmCell.setBorder(0);
		
		locTbl.addCell(projectNmCell);
		locTbl.addCell(branchNmCell);
		
		try {
			document.add(locTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	private void signaturePart(){
		PdfPTable signTable = new PdfPTable(3);
		signTable.setWidthPercentage(100);
		signTable.setSpacingBefore(80);
		
		signTable.addCell(cell.getCell("___________________", font9, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		signTable.addCell(cell.getCell("___________________", font9, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		signTable.addCell(cell.getCell("___________________", font9, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		signTable.addCell(cell.getCell("Accountant Signature", font9bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);;
		signTable.addCell(cell.getCell("Site Supervisor Signature", font9bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);;
		signTable.addCell(cell.getCell("Owner Signature", font9bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);;
		
		try {
			document.add(signTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}
	
	
	
}
