package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.shared.TaxSummaryBean;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PfReportPdf<create> {
	public Document document;
	Employee employee;
	Company comp;
	
	double sumOfGrossSalary=0;
	double sumOfPfWages=0;
	double sumOfEmpPf=0;
	double sumOfVpf=0;
	double sumOfEmprPf=0;
	double sumOfPensionCont=0;
	double sumOfPensionWages=0;
	double sumOfEdliWages=0;
	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private  Font font16boldul,font12bold,font11bold,font8bold,font10bold,font8,font12boldul,font12,font6bold;
	CsvWriter csvWriter=new CsvWriter();
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0");
	ArrayList<PaySlip> pfList = new ArrayList<PaySlip>();
	int noofemployee=0;
	int noOfLines = 0;
	int loopCount = 0;
	int pageno=1;
	
	/**
	 * @author Anil , Date : 01-08-2019
	 * Storing employee wise payslip for statutory reports
	 */
	public HashMap<String,ArrayList<PaySlip>> statoturyReportMap=new HashMap<String,ArrayList<PaySlip>>();
	
	
	public PfReportPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font11bold=new Font(Font.FontFamily.HELVETICA,11,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
		font10bold=new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	
	
	public void loadEmployeedata(Long count) {
		comp = ofy().load().type(Company.class).filter("companyId", count).first().now();
//		employee = ofy().load().type(Employee.class).id(count).now();
		pfList = CsvWriter.statuteryReport;
		System.out.println("list size :" + pfList.size());
		noofemployee=pfList.size();
		
		
		statoturyReportMap=CsvWriter.statoturyReportMap;
	}	
		
			
			
	public void createPFReportPdf() {
		createCompanyHeading();
		
		getPfRecordTable(35, loopCount);
		
		
		while (noOfLines == 0 && loopCount < pfList.size()) {
			pageno++;
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			getPfRecordTable(35, loopCount);

		}
		pfrecords();
		createpfsummaryTable();

	}
		
	private PdfPCell getCell(String value, Font font, int alignment) {
		Phrase phrase = new Phrase(value, font);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(alignment);
		return cell;
	}
	
		
		
	public void createCompanyHeading() {
		PdfPTable maintitle = new PdfPTable(1);
		maintitle.setWidthPercentage(100);
		Phrase titleph1 = new Phrase(comp.getBusinessUnitName(), font16boldul);
		titleph1.add(Chunk.NEWLINE);
		PdfPCell title1Cell = new PdfPCell(titleph1);
		title1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		maintitle.addCell(title1Cell).setBorder(0);

		try {
			document.add(maintitle);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MMM");
		SimpleDateFormat fmt2 = new SimpleDateFormat("MMM-yyyy");
		
		
		String payrollMonth = "";
		if (pfList.size() > 0) {
			try {
				payrollMonth = fmt2.format(fmt1.parse(pfList.get(0).getSalaryPeriod().trim()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		PdfPTable title2 = new PdfPTable(1);
		title2.setWidthPercentage(100);
		
		Phrase titlepf = new Phrase("Monthly Provident Fund Statement for the Month  "+ payrollMonth, font11bold);
		titlepf.add(Chunk.NEWLINE);
		PdfPCell titleCell1 = new PdfPCell(titlepf);
		titleCell1.setPaddingTop(10);

		titleCell1.setPaddingBottom(10);
		titleCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		title2.addCell(titleCell1).setBorder(0);

		try {
			document.add(title2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String cinval = "";

		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.contains("PF Code")) {

				cinval = comp.getArticleTypeDetails().get(i)
						.getArticleTypeValue();
			}

		}
		
		
		
		PdfPTable titleTab = new PdfPTable(2);
		titleTab.setWidthPercentage(100);
		try {
			titleTab.setWidths(new float[] { 50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell cell = getCell("PF Code No. of the Establishment:     "+ cinval, font10bold, Element.ALIGN_LEFT);
		cell.setBorder(0);
        titleTab.addCell(cell);
        
        
        PdfPCell cell1=getCell("Page: "+pageno, font8bold,Element.ALIGN_RIGHT);
		cell1.setBorder(0);
        titleTab.addCell(cell1);
	
		
		
		
		


		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		}
		
		
		
		
		

		
	private void getPfRecordTable(int count, int loopC) {
		HashMap<String,ArrayList<PaySlip>> innerReportMap=new HashMap<String,ArrayList<PaySlip>>();
		for (int j = loopC; j < pfList.size(); j++) {
			String key=pfList.get(j).getEmpid()+pfList.get(j).getSalaryPeriod();
			if(innerReportMap!=null&&innerReportMap.size()!=0){
				if(innerReportMap.containsKey(key)==false){
					innerReportMap.put(key, statoturyReportMap.get(key));
				}
			}else{
				innerReportMap.put(key, statoturyReportMap.get(key));
			}
		}

		noOfLines = count;
		PdfPTable pfRecordTable = new PdfPTable(13);
		pfRecordTable.setWidthPercentage(100);
		try {
			pfRecordTable.setWidths(new float[] { 3, 8, 7, 10, 16, 7, 7, 7, 7,
					7, 7, 7, 7});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		pfRecordTable
				.addCell(getCell("Sr. No.", font8bold, Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("Employee Number", font8bold,
				Element.ALIGN_CENTER));
		pfRecordTable
				.addCell(getCell("PF No.", font8bold, Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("UAN Number", font8bold,
				Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("Employee Name", font8bold,
				Element.ALIGN_LEFT));
		pfRecordTable.addCell(getCell("Gross Salary", font8bold,
				Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("PF Wages", font8bold,
				Element.ALIGN_CENTER));
		pfRecordTable
				.addCell(getCell("EE-PF", font8bold, Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("VPF", font8bold, Element.ALIGN_CENTER));
		pfRecordTable
				.addCell(getCell("ER-PF", font8bold, Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("Pension Cont.", font8bold,
				Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("Pension Wages", font8bold,
				Element.ALIGN_CENTER));
		pfRecordTable.addCell(getCell("EDLI Wages", font8bold,
				Element.ALIGN_CENTER));


		int counter=0;
		for(Map.Entry<String, ArrayList<PaySlip>> entry:innerReportMap.entrySet()){
			String key=entry.getKey();
			ArrayList<PaySlip> paySlipList=entry.getValue();
			
			double grossWage=0;
			double pfWages=0;
			double empWages=0;
			
			for(PaySlip obj:paySlipList){
				pfWages=pfWages+csvWriter.getPFwages(obj,true,false);
				empWages=empWages+csvWriter.getPFwages(obj,false,true);
				grossWage=grossWage+obj.getGrossEarningWithoutOT();
			}

//		for (int j = loopC; j < pfList.size(); j++) {
			PaySlip pay = paySlipList.get(0);
//			PaySlip pay = pfList.get(j);
			if (noOfLines == 0) {
				loopCount = counter;
				break;
			}

			noOfLines--;

			String pfNumber = "";
			if (pay.getPfNumber() != null) {
				pfNumber = pay.getPfNumber();
			}

			String UanNo = "";
			if (pay.getUanNo() != null) {
				UanNo = pay.getUanNo();
			}
			double pfMaxFixedValue = 0;
			double VPF = 0;
			
			/**
			 * @author Anil,Date : 01-07-2019
			 * picking pf wage amount from database 
			 * if not present then doing reverse calculation with the help of pf percent and pf amount
			 */
//			double pfWages = csvWriter.getPFwages(pay.getEarningList());
//			double pfWages = csvWriter.getPFwages(pay,true,false);
//			if (pfMaxFixedValue != 0 && pfWages > pfMaxFixedValue) {
//				pfWages = pfMaxFixedValue;
//			}
//			double empWages=csvWriter.getPFwages(pay,false,true);
//			double empWages = Math.round((pfWages * 12) / 100);
			double emprWages = Math.round((pfWages * 3.67) / 100);
			double pensionCont = Math.round((pfWages * 8.33) / 100);
			sumOfGrossSalary=sumOfGrossSalary+grossWage;
		
			sumOfPfWages=sumOfPfWages+pfWages;
	        sumOfEmpPf=sumOfEmpPf+empWages;
			sumOfEmprPf=sumOfEmprPf+emprWages;
			sumOfPensionCont=sumOfPensionCont+pensionCont;
			sumOfPensionWages=sumOfPensionWages+pfWages;
			sumOfEdliWages=sumOfEdliWages+pfWages;


			pfRecordTable.addCell(getCell((counter + 1) + "", font8,
					Element.ALIGN_CENTER));
			pfRecordTable.addCell(getCell(pay.getEmpid() + "", font8,
					Element.ALIGN_CENTER));
			pfRecordTable
					.addCell(getCell(pfNumber, font8, Element.ALIGN_CENTER));
			pfRecordTable.addCell(getCell(UanNo, font8, Element.ALIGN_CENTER));
			pfRecordTable.addCell(getCell(pay.getEmployeeName() + "", font8,
					Element.ALIGN_LEFT));
			pfRecordTable.addCell(getCell(df.format(grossWage) + "",
					font8, Element.ALIGN_RIGHT));
			pfRecordTable.addCell(getCell(df.format(pfWages) + "", font8,
					Element.ALIGN_RIGHT));
			pfRecordTable.addCell(getCell(df.format(empWages) + "", font8,
					Element.ALIGN_RIGHT));
			pfRecordTable
					.addCell(getCell(df.format(VPF )+ "", font8, Element.ALIGN_RIGHT));
			pfRecordTable.addCell(getCell(df.format(emprWages) + "", font8,
					Element.ALIGN_RIGHT));
			pfRecordTable.addCell(getCell(df.format(pensionCont) + "", font8,
					Element.ALIGN_RIGHT));
			pfRecordTable.addCell(getCell(df.format(pfWages) + "", font8,
					Element.ALIGN_RIGHT));
			pfRecordTable.addCell(getCell(df.format(pfWages) + "", font8,
					Element.ALIGN_RIGHT));
			
			counter++;
//			statoturyReportMap.remove(key);
		
		}
		try {
			document.add(pfRecordTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

        private void pfrecords() {
    	PdfPTable pfRecordtotalTable = new PdfPTable(9);
		pfRecordtotalTable.setWidthPercentage(100);
		try {
			pfRecordtotalTable.setWidths(new float[] { 44, 7, 7, 7, 7, 7, 7,
					7, 7});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pfRecordtotalTable.addCell(getCell("Sub Total ==>>", font8bold,
				Element.ALIGN_CENTER));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfGrossSalary) + "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfPfWages )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfEmpPf) + "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfVpf )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfEmprPf) + "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfPensionCont) + "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfPensionWages) + "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfEdliWages )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell("Grand Total ==>>", font8bold,
				Element.ALIGN_CENTER));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfGrossSalary )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfPfWages )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfEmpPf) + "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfVpf )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfEmprPf )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfPensionCont )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfPensionWages )+ "", font8bold,
				Element.ALIGN_RIGHT));
		pfRecordtotalTable.addCell(getCell(df.format(sumOfEdliWages )+ "", font8bold,
				Element.ALIGN_RIGHT));
		   try {
				
				document.add(pfRecordtotalTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	
    }
	
	
	
	
	
	

		
      
	private void createpfsummaryTable() {

		String payrollMonth = "";
		if (pfList.size() > 0) {
			payrollMonth = pfList.get(0).getSalaryPeriod();
		}
		PdfPTable outerTbl = new PdfPTable(1);
		outerTbl.setSpacingAfter(20);
		outerTbl.addCell(
				getCell("Dues for the month of " + payrollMonth, font12,
						Element.ALIGN_CENTER)).setBorder(0);
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable outertable = new PdfPTable(3);
		outertable.setSpacingAfter(20);
		outertable.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable.addCell(getCell("No.Of Employee", font8bold,
				Element.ALIGN_CENTER));
		outertable.addCell(getCell("Wages", font8bold, Element.ALIGN_CENTER));
		outertable.addCell(getCell("Provident Fund", font8,
				Element.ALIGN_CENTER));
		outertable.addCell(getCell(noofemployee+"", font8, Element.ALIGN_CENTER));
		
		PdfPCell wageCell=getCell(sumOfPfWages + "", font8,
				Element.ALIGN_RIGHT);
		wageCell.setPaddingRight(10);
		outertable.addCell(wageCell);
		outertable
				.addCell(getCell("Pension Fund", font8, Element.ALIGN_CENTER));
		outertable.addCell(getCell(noofemployee+"", font8, Element.ALIGN_CENTER));
		
		
		PdfPCell wageCell1=getCell(sumOfPfWages + "", font8,
				Element.ALIGN_RIGHT);
		
		wageCell1.setPaddingRight(10);
		outertable.addCell(wageCell1);
		outertable.addCell(getCell("E.D.L.I", font8, Element.ALIGN_CENTER));
		outertable.addCell(getCell(noofemployee+"", font8, Element.ALIGN_CENTER));
		
		PdfPCell wageCell2=getCell(sumOfPfWages + "", font8,
				Element.ALIGN_RIGHT);
		wageCell2.setPaddingRight(10);
		outertable.addCell(wageCell2);

		try {
			document.add(outertable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable outertable2 = new PdfPTable(3);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		PdfPCell dues=getCell("Dues", font8bold, Element.ALIGN_RIGHT);
		dues.setPaddingRight(10);
		outertable2.addCell(dues);
	    outertable2.addCell(getCell("Paid", font8bold, Element.ALIGN_RIGHT));
		outertable2.addCell(getCell("A/c. I Employee Share", font8,
				Element.ALIGN_CENTER));
		
		PdfPCell sumofwages=getCell(Math.round((sumOfPfWages * 12) / 100) + "",
				font8, Element.ALIGN_RIGHT);
		sumofwages.setPaddingRight(10);
				
		outertable2.addCell(sumofwages);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("A/c. I Employer Share", font8,
				Element.ALIGN_CENTER));
		PdfPCell sumofwages1=getCell(Math.round((sumOfPfWages * 3.67) / 100)
				+ "", font8, Element.ALIGN_RIGHT);
		sumofwages1.setPaddingRight(10);
	    outertable2.addCell(sumofwages1);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("A/c. II", font8, Element.ALIGN_CENTER));
		PdfPCell sumofwages2=getCell(
				Math.round((sumOfPfWages * 0.5) / 100) + "", font8,
				Element.ALIGN_RIGHT);
		sumofwages2.setPaddingRight(10);
		outertable2.addCell(sumofwages2);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("A/c. X Employee Share", font8,
				Element.ALIGN_CENTER));
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("A/c. X Employer Share", font8,
				Element.ALIGN_CENTER));
		
		PdfPCell sumofwages3=getCell(Math.round((sumOfPfWages * 8.33) / 100)+ "", font8, Element.ALIGN_RIGHT);
		
		sumofwages3.setPaddingRight(10);
		outertable2.addCell(sumofwages3);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("A/c. XXI", font8, Element.ALIGN_CENTER));
		
		PdfPCell sumofwages4=getCell(
				Math.round((sumOfPfWages * 0.5) / 100) + "", font8,Element.ALIGN_RIGHT);
		sumofwages4.setPaddingRight(10);
		outertable2.addCell(sumofwages4);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("A/c. XXII", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));

		try {
			document.add(outertable2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable outertable3 = new PdfPTable(1);
		double sumOfTotalContribution=Math.round((sumOfPfWages*12)/100)+Math.round((sumOfPfWages*3.67)/100)+Math.round((sumOfPfWages*0.5)/100)+Math.round((sumOfPfWages*8.33)/100)+Math.round((sumOfPfWages*0.5)/100);
		outertable3.addCell(getCell("Total ==>>     "+sumOfTotalContribution, font8bold,
				Element.ALIGN_CENTER));
		
		//outertable3.addCell(getCell(sumOfTotalContribution+"", font8bold,
		//		Element.ALIGN_CENTER));
		
		try {
			document.add(outertable3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}    
}
      
	
	    
	    
	    
	    
	    
	    
	    

			
	    
	    
	    
	    
	    
 









	

