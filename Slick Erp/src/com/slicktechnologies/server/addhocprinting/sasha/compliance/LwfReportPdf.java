package com.slicktechnologies.server.addhocprinting.sasha.compliance;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.shared.TaxSummaryBean;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;


public class LwfReportPdf<create> {
	public Document document;
	Employee employee;
	Company comp;
	Branch branch=null;
	double sumOfDays=0;
	double sumOfWages=0;
	double sumOfContribution=0;
	int pageno=1;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private  Font font16boldul,font12bold,font11bold,font10bold,font8bold,font8,font12boldul,font12,font6bold;
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0");
	ArrayList<PaySlip> pfList = new ArrayList<PaySlip>();
	int noOfLines = 0;
	int loopCount = 0;
	
	/**
	 * @author Anil , Date : 06-08-2019
	 * Storing employee wise payslip for statutory reports
	 */
	public HashMap<String,ArrayList<PaySlip>> statoturyReportMap=new HashMap<String,ArrayList<PaySlip>>();

	public LwfReportPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void loadEmployeedata(Long count) {

		comp = ofy().load().type(Company.class).filter("companyId", count)
				.first().now();

		employee = ofy().load().type(Employee.class).id(count).now();
		pfList = CsvWriter.statuteryReport;
		System.out.println("list size :" + pfList.size());
		branch = ofy().load().type(Branch.class).filter("companyId", pfList.get(0).getCompanyId())
				.filter("buisnessUnitName", pfList.get(0).getBranch()).first().now();

		statoturyReportMap=CsvWriter.statoturyReportMap;
	}
	
	
	
	    public void createLwfReportPdf(){
			createCompanyHeddding();
			geLwfRecordTable(20 , loopCount);
			while (noOfLines == 0 && loopCount < pfList.size()) {
				pageno++;
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				geLwfRecordTable(20, loopCount);
	
			}
			lwfrecords();
	    }
	    private  PdfPCell getCell(String value ,Font font ,int alignment) {
			Phrase phrase = new Phrase(value , font);
			PdfPCell cell = new PdfPCell(phrase);
			cell.setHorizontalAlignment(alignment);
			return cell;		
		    }
		
	
	        public void createCompanyHeddding() {
		    PdfPTable maintitle = new PdfPTable(1);
			maintitle.setWidthPercentage(100);
			Phrase titleph1 = new Phrase(comp.getBusinessUnitName(),font16boldul);
			titleph1.add(Chunk.NEWLINE);
			PdfPCell title1Cell = new PdfPCell(titleph1);
			title1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			maintitle.addCell(title1Cell).setBorder(0);
			
				try {
					document.add(maintitle);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MMM");
				SimpleDateFormat fmt2 = new SimpleDateFormat("MMM-yyyy");
				String payrollMonth = "";
				if (pfList.size() > 0) {
					try {
						payrollMonth = fmt2.format(fmt1.parse(pfList.get(0).getSalaryPeriod().trim()));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
				PdfPTable title2 = new PdfPTable(1);
				title2.setWidthPercentage(100);
				Phrase titlepf = new Phrase("Monthly LWF Contribution Statement for the Month  "+payrollMonth, font11bold);
				titlepf.add(Chunk.NEWLINE);
			    PdfPCell titleCell1 = new PdfPCell(titlepf);
			    titleCell1.setPaddingTop(10);
			    
				titleCell1.setPaddingBottom(10);
			    titleCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			    title2.addCell(titleCell1).setBorder(0);
			
				try {
					document.add(title2);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				String lwfCode="";
				if(branch!= null&&branch.getLwfCode()!=null){
					lwfCode=branch.getLwfCode();
					}
				
				
				PdfPTable titleTab = new PdfPTable(2);
				titleTab.setWidthPercentage(100);
				try {
					titleTab.setWidths(new float[] { 50,50});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				PdfPCell cell = getCell("Code No. of the Establishment:     "+ lwfCode, font10bold, Element.ALIGN_LEFT);
				cell.setBorder(0);
		        titleTab.addCell(cell);
		        
		        
		        PdfPCell cell1=getCell("Page: "+pageno, font8bold,Element.ALIGN_RIGHT);
				cell1.setBorder(0);
		        titleTab.addCell(cell1);
			
				
				
				
				


				try {
					document.add(titleTab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
					}
//						   
//	    private  PdfPCell getCell(String value ,Font font ,int alignment) {
//		Phrase phrase = new Phrase(value , font);
//		PdfPCell cell = new PdfPCell(phrase);
//		cell.setHorizontalAlignment(alignment);
//		return cell;		
//	    }
//	
	 private void geLwfRecordTable(int count , int loopC) {
		 	HashMap<String,ArrayList<PaySlip>> innerReportMap=new HashMap<String,ArrayList<PaySlip>>();
			for (int j = loopC; j < pfList.size(); j++) {
				String key=pfList.get(j).getEmpid()+pfList.get(j).getSalaryPeriod();
				if(innerReportMap!=null&&innerReportMap.size()!=0){
					if(innerReportMap.containsKey(key)==false){
						innerReportMap.put(key, statoturyReportMap.get(key));
					}
				}else{
					innerReportMap.put(key, statoturyReportMap.get(key));
				}
			}
	
		    noOfLines = count;
	    	PdfPTable  lwfRecordTable = new PdfPTable(6);
	    	lwfRecordTable.setWidthPercentage(100);
	    	try {
	    	lwfRecordTable.setWidths(new float[]{5,10,55,10,10,10});
	    	}catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
	    	}
	    	lwfRecordTable.addCell(getCell("Sr.NO", font8bold, Element.ALIGN_CENTER));
			lwfRecordTable.addCell(getCell("Employee Number", font8bold, Element.ALIGN_CENTER));
			lwfRecordTable.addCell(getCell("Employee Name", font8bold, Element.ALIGN_LEFT));
			lwfRecordTable.addCell(getCell("Days", font8bold, Element.ALIGN_CENTER));
			lwfRecordTable.addCell(getCell("Wages", font8bold, Element.ALIGN_CENTER));
			lwfRecordTable.addCell(getCell("Contribution", font8bold, Element.ALIGN_CENTER));
			
			int i=0;
			
			for(Map.Entry<String, ArrayList<PaySlip>> entry:innerReportMap.entrySet()){
				String key=entry.getKey();
				ArrayList<PaySlip> paySlipList=entry.getValue();
				
				double employeeLwfContribution=0;
				double paidDays=0;
				double grossWage=0;
				
				for(PaySlip obj:paySlipList){
					for(CtcComponent comp:obj.getDeductionList()){
						if(comp.getShortName().equalsIgnoreCase("LWF")||comp.getShortName().equalsIgnoreCase("MLWF")){
							employeeLwfContribution=employeeLwfContribution+comp.getActualAmount();
						}
					}
					paidDays=paidDays+obj.getPaidDays();
					grossWage=grossWage+obj.getGrossEarningWithoutOT();
				}
				PaySlip pay=paySlipList.get(0);
	        
//	         for(int j = loopC ; j < pfList.size() ; j++)
//	         {
//	        	 PaySlip pay = pfList.get(j);
	        	 if(noOfLines == 0){
	        		loopCount = i; 
	        		 break;
	        	 }
	        	

	        	 noOfLines--; 
	        	 
	        	 
	        	 
//	        	 double lwf = 0;
//	 			for (int k = 0; k < pay.getDeductionList().size(); k++) {
//	 				if (pay.getDeductionList().get(k).getShortName().trim().equalsIgnoreCase("LWF")) {
//	 					lwf = pay.getDeductionList().get(k).getActualAmount();
//	 				}
//	 			}
	 			
	 			
				
	        	    lwfRecordTable.addCell(getCell((i+1)+"", font8, Element.ALIGN_CENTER));
					lwfRecordTable.addCell(getCell(pay.getEmpid()+"", font8, Element.ALIGN_CENTER));
					lwfRecordTable.addCell(getCell(pay.getEmployeeName()+"", font8, Element.ALIGN_LEFT));
					lwfRecordTable.addCell(getCell(df.format(paidDays)+ "", font8, Element.ALIGN_RIGHT));
					lwfRecordTable.addCell(getCell(df.format(Math.round(grossWage)) + "", font8, Element.ALIGN_RIGHT));
					lwfRecordTable.addCell(getCell(df.format(Math.round(employeeLwfContribution)) + "", font8, Element.ALIGN_RIGHT));
			
			
					
					
					
					
					
//			PdfPTable  lwfRecordtotalTable = new PdfPTable(4);
//			lwfRecordtotalTable.setWidthPercentage(100);
//	    	try {
//	    		lwfRecordtotalTable.setWidths(new float[]{70,10,10,10});
//		    	}catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//		    	}
//	    	
			
	    	sumOfDays=sumOfDays+paidDays;
			sumOfWages=sumOfWages+Math.round(grossWage);
			sumOfContribution=sumOfContribution+employeeLwfContribution;
			
			
//	    	lwfRecordtotalTable.addCell(getCell("Sub Total ==>>", font8bold, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell(sumOfDays+"", font8, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell(sumOfWages+"", font8, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell(sumOfContribution+"", font8, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell("Grand Total ==>>", font8bold, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell(sumOfDays+"", font8, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell(sumOfWages+"", font8, Element.ALIGN_CENTER));
//	    	lwfRecordtotalTable.addCell(getCell(sumOfContribution+"", font8, Element.ALIGN_CENTER));
			 
			 i++;
	         }
	      	try {
				document.add(lwfRecordTable);
				
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
//			}
	 }
	 
	 private void lwfrecords(){
		 PdfPTable  lwfRecordtotalTable = new PdfPTable(4);
			lwfRecordtotalTable.setWidthPercentage(100);
	    	try {
	    		lwfRecordtotalTable.setWidths(new float[]{70,10,10,10});
		    	}catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
		    	}
		 
		 
	    	lwfRecordtotalTable.addCell(getCell("Sub Total ==>>", font8bold, Element.ALIGN_CENTER));
	    	lwfRecordtotalTable.addCell(getCell(df.format(sumOfDays)+"", font8, Element.ALIGN_RIGHT));
	    	lwfRecordtotalTable.addCell(getCell(df.format(sumOfWages)+"", font8, Element.ALIGN_RIGHT));
	    	lwfRecordtotalTable.addCell(getCell(df.format(sumOfContribution)+"", font8, Element.ALIGN_RIGHT));
	    	lwfRecordtotalTable.addCell(getCell("Grand Total ==>>", font8bold, Element.ALIGN_CENTER));
	    	lwfRecordtotalTable.addCell(getCell(df.format(sumOfDays)+"", font8, Element.ALIGN_RIGHT));
	    	lwfRecordtotalTable.addCell(getCell(df.format(sumOfWages)+"", font8, Element.ALIGN_RIGHT));
	    	lwfRecordtotalTable.addCell(getCell(df.format(sumOfContribution)+"", font8, Element.ALIGN_RIGHT));

	    	try {
				
				document.add(lwfRecordtotalTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		 
		 
		 
		 
	 }
	 
} 

				
				
		
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
				
				
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


