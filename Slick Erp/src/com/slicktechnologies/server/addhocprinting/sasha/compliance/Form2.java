package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class Form2 {
public Document document;
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat fmt1= new SimpleDateFormat("MMM");
	List<PaySlip> paySlipList;
	Company company;
	Employee employee;
	EmployeeInfo employeeinfo;
	double normalEarnings=0.0;
	double hrapayable=0.0;
	double totalotherearnings=0.0;
	double  advances=0.0;
	HrProject hrProject;
double otherearningamount=0.0;
	double pf1=0.0;
	double esic1=0.0;
	double pt1=0.0;
	double mlwf1=0.0;
	double it1=0.0;
	double otherdeductiononpermissible=0.0;
	double totaldeduction1=0.0;
	double netwagespaid1=0.0;
	int loopCount = 0;
	double othertotaldeductedamount=0.0;
	int noOfempcode=0;
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 6);
	Font font7a = new Font(Font.FontFamily.HELVETICA, 7);
	Font font7abold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	
	
	SimpleDateFormat fmt2 = new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt3 = new SimpleDateFormat("MMM-yyyy");
	
		Logger logger = Logger.getLogger("Form2.class");
	List<Attendance> attendanceList=new ArrayList<Attendance>();
	
	List<Shift> shiftList=new ArrayList<Shift>();
	
	public void setform2data(Long companyId) {
		company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		employeeinfo=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).first().now();
		
//		paySlipList=ofy().load().type(PaySlip.class).filter("companyId",companyId).list();
		//employee = ofy().load().type(Employee.class).id(count).now();
		
		ArrayList<Integer> emplIdList=new ArrayList<Integer>();
		String payrollMonth="";
		if(paySlipList!=null&&paySlipList.size()!=0){
			System.out.println("PAYSLIPLIST SIZE "+paySlipList.size());
			
			payrollMonth=paySlipList.get(0).getSalaryPeriod();
			System.out.println("PAYROLL MONTH VALUE "+payrollMonth);
			
			for(PaySlip obj:paySlipList){
				emplIdList.add(obj.getEmpid());
				System.out.println("Employee ID LIST Size in FOR"+emplIdList.size());
			}
		}
		
		Date payrollDate=null;
		try {
			payrollDate=fmt2.parse(payrollMonth);
			System.out.println("PAYROLL MONTH BEFORE "+payrollMonth);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		payrollMonth=fmt3.format(payrollDate);
		System.out.println("PAYROLL MONTH AFTER "+payrollMonth);
		 
		attendanceList=ofy().load().type(Attendance.class).filter("companyId", companyId).filter("month", payrollMonth).filter("empId IN", emplIdList).list();
	     System.out.println("Attendance list Size"+attendanceList.size());
	     System.out.println("Employee ID LIST Size"+emplIdList.size());
	     hrProject=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName",paySlipList.get(0).getProjectName()).first().now();
	     
	     
	     
	     
	    HashSet<String> shift=new HashSet<String>();
		
		for(Attendance atten:attendanceList){
			shift.add((atten.getShift()));
			
		}
		System.out.println("SHIFTSIZE"+shift.size());
		
      	ArrayList<String> ShiftTimeOfEmployee=new ArrayList<String>(shift);
      	System.out.println("SHIFTSIZE"+ShiftTimeOfEmployee.size());
      	
      	if(ShiftTimeOfEmployee.size()!=0){
      	shiftList=ofy().load().type(Shift.class).filter("companyId", companyId).filter("shiftName IN", ShiftTimeOfEmployee).list();
      	
      	}
	     System.out.println("SHIFT LIST SIZE"+shiftList.size());
	}
	
	public void createPdf() {
		
		createFormHeading();
		createFormsubHheading();
		EmployeeDetails(15,loopCount);
		
       while (noOfempcode == 0 && loopCount < paySlipList.size()) {
    	   logger.log(Level.SEVERE,"noOfempcode "+noOfempcode+" loopCount "+loopCount);
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			createFormHeading();
			createFormsubHheading();
			EmployeeDetails(15, loopCount);
			
		}
		createFooterTable();
		Phrase nextpage2 = new Phrase(Chunk.NEXTPAGE);
		try {
			document.add(nextpage2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		formIIPartTwo();
		
	}
	
	
	public Shift getShift(List<Attendance>attendanceList,int empId,List<Shift>shiftList,String projectName){
		if(attendanceList!=null){
			for(Attendance obj:attendanceList){
				if(empId==obj.getEmpId()&&projectName.equals(obj.getProjectName())){
					for(Shift object:shiftList){
						String shiftName=obj.getShift();
						System.out.println("SHIFT NAME"+obj.getShift());
						System.out.println("SHIFT NAME11"+object.getShiftName());
						if(shiftName.equals(object.getShiftName())){
							
							return object;
						}
					}
				}
			}
		}
		return null;
	}
	
	
	
	
	
	
	
	 
	
	public String getAttendanceDetails(List<Attendance>attendanceList,int empId,int day,String projectName){
		 boolean actionFlag=false;
		    if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "ShowAttendanceLabel",company.getCompanyId())){
		    	actionFlag=true;
		    }
		if(attendanceList!=null){
			for(Attendance obj:attendanceList){
				if(empId==obj.getEmpId()&&projectName.equals(obj.getProjectName())){
					Date date=obj.getAttendanceDate();
					int days=date.getDate();
					if(days==day){
						if(obj.isPresent()){
							if(actionFlag){
								return obj.getActionLabel();
							}else{
								return obj.getTotalWorkedHours()+"";
							}
						}else{
							return obj.getActionLabel();
						}
					}
				}
			}
		}
		return "";
	}
	
	
	
	
	
	
	private void createFormHeading() {
		
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		
		outerTbl.addCell(cell.getCell("Maharashtra Minimum Wages Rules", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("FORM II", font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell("See Rule 27 (1)", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" Muster Roll-cum Wage Register", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		
		try {
			document.add(outerTbl);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
		
		private void createFormsubHheading() {
			
			PdfPTable outerTbl=new PdfPTable(5);
			outerTbl.setWidthPercentage(100f);
			outerTbl.setSpacingBefore(10f);
			try {
				outerTbl.setWidths(new float[] {20,20,35,15,10});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}                                                   
			PaySlip payslip=paySlipList.get(0);
			
			
			outerTbl.addCell(cell.getCell("  ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("Name of Establishment - "+company.getBusinessUnitName(), font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("Location - "+payslip.getBranch(), font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("  ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			
			outerTbl.addCell(cell.getCell(" ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("Name of the Employer - "+payslip.getProjectName(), font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell(" ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("  ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			
			
			outerTbl.addCell(cell.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			try {
				outerTbl.addCell(cell.getCell("For the month of  "+fmt3.format(fmt2.parse(payslip.getSalaryPeriod())), font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			outerTbl.addCell(cell.getCell(" ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(cell.getCell("  ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			
			
			try {
				document.add(outerTbl);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
		
		
		
		
	
	
	private void EmployeeDetails(int j, int loopCount2) {
		
		noOfempcode=j;
		
		PdfPTable outerTbl=new PdfPTable(28);
		outerTbl.setWidthPercentage(100f);
		try {
//			outerTbl.setWidths(new float[] {6,10,4,8,2,2,6,3,4,4,2,2,4,3,3,3,3,3,2,3,4,4,3,2,2,2,6,2});
			outerTbl.setWidths(new float[] {0.33f,0.44f,0.28f,0.34f,0.23f,0.23f,0.34f,0.23f,0.28f,0.28f,0.28f,0.28f,0.28f,0.28f,0.26f,0.26f,0.26f,0.26f,0.26f,0.28f,0.28f,0.28f,0.26f,0.26f,0.26f,0.26f,0.28f,0.28f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		outerTbl.setSpacingBefore(5f);
		
		
		PdfPCell SINo=cell.getCell("SI. No. ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		SINo.setBorderWidthBottom(0);
		
		PdfPCell fullname=cell.getCell("Full Name of the Employee", font7, Element.ALIGN_CENTER, 2, 0, 0);
		fullname.setBorderWidthBottom(0);
		
		PdfPCell agensex=cell.getCell(" Age and Sex", font7, Element.ALIGN_CENTER, 2, 0, 0);
		agensex.setBorderWidthBottom(0);
		
		PdfPCell natureofwork=cell.getCell("Nature of Work and Designation ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		natureofwork.setBorderWidthBottom(0);
		
		PdfPCell workhr=cell.getCell("Working hours From to", font7, Element.ALIGN_CENTER, 2, 0, 0);
		workhr.setBorderWidthBottom(0);
		
		PdfPCell interval=cell.getCell("Interval for rest or From To ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		interval.setBorderWidthBottom(0);
		
		PdfPCell doe=cell.getCell(" Data of Entry into Service", font7, Element.ALIGN_CENTER, 2, 0, 0);
		doe.setBorderWidthBottom(0);
		
		PdfPCell totaldaysworked=cell.getCell(" Total Days Worked", font7, Element.ALIGN_CENTER, 2, 0, 0);
		totaldaysworked.setBorderWidthBottom(0);
		
		PdfPCell normalearning=cell.getCell("Normal Earnings", font7, Element.ALIGN_CENTER, 2, 0, 0);
		normalearning.setBorderWidthBottom(0);
		
		PdfPCell HRApayble=cell.getCell("HRA payable ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		HRApayble.setBorderWidthBottom(0);
		
		PdfPCell ot=cell.getCell("Overtime Earnings ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		ot.setBorderWidthBottom(0);
		
		PdfPCell totalearning=cell.getCell("Total Other Earnings", font7, Element.ALIGN_CENTER, 2, 0, 0);
		totalearning.setBorderWidthBottom(0);
		
		PdfPCell grosswagespayble=cell.getCell(" Gross Wages Payable", font7, Element.ALIGN_CENTER, 2, 0, 0);
		grosswagespayble.setBorderWidthBottom(0);
		
		PdfPCell advance=cell.getCell("Advances ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		advance.setBorderWidthBottom(0);
		
		PdfPCell pf=cell.getCell(" PF", font7, Element.ALIGN_CENTER, 0, 0, 0);
		pf.setBorderWidthBottom(0);
		
		PdfPCell esic=cell.getCell(" ESIC", font7, Element.ALIGN_CENTER, 0, 0, 0);
		esic.setBorderWidthBottom(0);
		
		PdfPCell pt=cell.getCell(" PT", font7, Element.ALIGN_CENTER, 0, 0, 0);
		pt.setBorderWidthBottom(0);
		
		PdfPCell mlwf=cell.getCell("MLWF ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		mlwf.setBorderWidthBottom(0);
		
		PdfPCell it=cell.getCell(" IT", font7, Element.ALIGN_CENTER, 0, 0, 0);
		it.setBorderWidthBottom(0);
		
		PdfPCell deduction=cell.getCell("Deduction ", font7, Element.ALIGN_CENTER, 1, 5, 0);
		deduction.setBorderWidthBottom(0);
		
	    PdfPCell otherdeduction=cell.getCell(" Other Deduction on Permissible", font7, Element.ALIGN_CENTER, 2, 0, 0);
		otherdeduction.setBorderWidthBottom(0);
		
		PdfPCell totaldeduction=cell.getCell(" Total Deduction", font7, Element.ALIGN_CENTER, 2, 0, 0);
		totaldeduction.setBorderWidthBottom(0);
		
		PdfPCell netwagespaid=cell.getCell(" Net Wages paid", font7, Element.ALIGN_CENTER, 2, 0, 0);
		netwagespaid.setBorderWidthBottom(0);
		
		PdfPCell prebalance=cell.getCell(" Previous Balance", font7, Element.ALIGN_CENTER, 0, 0, 0);
		prebalance.setBorderWidthBottom(0);
		
		PdfPCell earnedduringmonth=cell.getCell(" Earned during the Month", font7, Element.ALIGN_CENTER, 0, 0, 0);
		earnedduringmonth.setBorderWidthBottom(0);
		
		PdfPCell availableduringthemonth=cell.getCell("Availed During the Month", font7, Element.ALIGN_CENTER, 0, 0, 0);
		availableduringthemonth.setBorderWidthBottom(0);
		
		PdfPCell balanceatendofmonth=cell.getCell("Balance at the End of the Month", font7, Element.ALIGN_CENTER, 0, 0, 0);
		balanceatendofmonth.setBorderWidthBottom(0);
		
		PdfPCell leavewithwages=cell.getCell("Leave with wages ", font7, Element.ALIGN_CENTER, 1, 4, 0);
		leavewithwages.setBorderWidthBottom(0);
		
		
		
		PdfPCell dateofpayment=cell.getCell("Date of Payment of Wages ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		dateofpayment.setBorderWidthBottom(0);
		
		PdfPCell sign=cell.getCell("Signature or Thumb Impression of the Employee ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		sign.setBorderWidthBottom(0);
		
		outerTbl.addCell(SINo);
		outerTbl.addCell(fullname);
		outerTbl.addCell(agensex);
		outerTbl.addCell(natureofwork);
		outerTbl.addCell(workhr);
		outerTbl.addCell(interval);
		outerTbl.addCell(doe);
		outerTbl.addCell(totaldaysworked);
		outerTbl.addCell(normalearning);
		outerTbl.addCell(HRApayble);
		outerTbl.addCell(ot);
		outerTbl.addCell(totalearning);
		outerTbl.addCell(grosswagespayble);
		outerTbl.addCell(advance);
		outerTbl.addCell(deduction);
		outerTbl.addCell(otherdeduction);
		outerTbl.addCell(totaldeduction);
		outerTbl.addCell(netwagespaid);
//		outerTbl.addCell(prebalance);
//		outerTbl.addCell(earnedduringmonth);
//		outerTbl.addCell(availableduringthemonth);
//		outerTbl.addCell(balanceatendofmonth);
		outerTbl.addCell(leavewithwages);
		outerTbl.addCell(dateofpayment);
		outerTbl.addCell(sign);
		outerTbl.addCell(pf);
		outerTbl.addCell(esic);
		outerTbl.addCell(pt);
		outerTbl.addCell(mlwf);
		outerTbl.addCell(it);
		outerTbl.addCell(prebalance);
		outerTbl.addCell(earnedduringmonth);
		outerTbl.addCell(availableduringthemonth);
		outerTbl.addCell(balanceatendofmonth);
		
		PdfPCell cell1=cell.getCell("1", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell1.setBorderWidthBottom(0);
	
		PdfPCell cell2=cell.getCell("2", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell2.setBorderWidthBottom(0);
		
		PdfPCell cell3=cell.getCell("3", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell3.setBorderWidthBottom(0);
		
		PdfPCell cell4=cell.getCell("4 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell4.setBorderWidthBottom(0);
		
		PdfPCell cell5=cell.getCell("5", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell5.setBorderWidthBottom(0);
		
		PdfPCell cell6=cell.getCell("6 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell6.setBorderWidthBottom(0);
		
		PdfPCell cell7=cell.getCell(" 7", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell7.setBorderWidthBottom(0);
		
		PdfPCell cell8=cell.getCell(" 9", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell8.setBorderWidthBottom(0);
		
		PdfPCell cell9=cell.getCell("10", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell9.setBorderWidthBottom(0);
		
		PdfPCell cell10=cell.getCell("11 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell10.setBorderWidthBottom(0);
		
		PdfPCell cell11=cell.getCell("12 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell11.setBorderWidthBottom(0);
		
		PdfPCell cell12=cell.getCell("13", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell12.setBorderWidthBottom(0);
		
		PdfPCell cell13=cell.getCell(" 14", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell3.setBorderWidthBottom(0);
		
		PdfPCell cell14=cell.getCell("15 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell14.setBorderWidthBottom(0);
		
		PdfPCell cell15=cell.getCell(" 16", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell15.setBorderWidthBottom(0);
		
		PdfPCell cell16=cell.getCell("17", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell16.setBorderWidthBottom(0);
		
		PdfPCell cell17=cell.getCell(" 18", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell17.setBorderWidthBottom(0);
		
		PdfPCell cell18=cell.getCell("19 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell18.setBorderWidthBottom(0);
		
		PdfPCell cell19=cell.getCell(" 20", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell19.setBorderWidthBottom(0);
		
		PdfPCell cell20=cell.getCell(" 21", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell20.setBorderWidthBottom(0);
		
		PdfPCell cell21=cell.getCell(" 22", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell21.setBorderWidthBottom(0);
		
		PdfPCell cell22=cell.getCell(" 23", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell22.setBorderWidthBottom(0);
		
		PdfPCell cell23=cell.getCell(" 24", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell23.setBorderWidthBottom(0);
		
		PdfPCell cell24=cell.getCell(" 25", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell24.setBorderWidthBottom(0);
		
		PdfPCell cell25=cell.getCell("26", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell25.setBorderWidthBottom(0);
		
		PdfPCell cell26=cell.getCell("27", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell26.setBorderWidthBottom(0);
		
		PdfPCell cell27=cell.getCell("28", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell27.setBorderWidthBottom(0);
		
		PdfPCell cell28=cell.getCell("29 ", font7, Element.ALIGN_CENTER, 0, 0, 0);
		cell28.setBorderWidthBottom(0);
		
		outerTbl.addCell(cell1);
		outerTbl.addCell(cell2);
		outerTbl.addCell(cell3);
		outerTbl.addCell(cell4);
		outerTbl.addCell(cell5);
		outerTbl.addCell(cell6);
		outerTbl.addCell(cell7);
		outerTbl.addCell(cell8);
		outerTbl.addCell(cell9);
		outerTbl.addCell(cell10);
		outerTbl.addCell(cell11);
		outerTbl.addCell(cell12);
		outerTbl.addCell(cell13);
		outerTbl.addCell(cell14);
		outerTbl.addCell(cell15);
		outerTbl.addCell(cell16);
		outerTbl.addCell(cell17);
		outerTbl.addCell(cell18);
		outerTbl.addCell(cell19);
		outerTbl.addCell(cell20);
		outerTbl.addCell(cell21);
		outerTbl.addCell(cell22);
		outerTbl.addCell(cell23);
		outerTbl.addCell(cell24);
		outerTbl.addCell(cell25);
		outerTbl.addCell(cell26);
		outerTbl.addCell(cell27);
		outerTbl.addCell(cell28);
		
		/**
		 * @author Anil,Date : 27-03-2019
		 * Variable i was initialized with 0 it should be as per parameter passed
		 */
		for(int i=loopCount2;i<paySlipList.size();i++) {
			
			PaySlip payslip=paySlipList.get(i);
			if (noOfempcode == 0) {
				loopCount = i;
				break;
			}

			noOfempcode--;
			
		Calendar cal = Calendar.getInstance(); 
		
		
		 normalEarnings+=payslip.getPFAmount();
		 hrapayable+= payslip.getHRAAmount();
         pf1+=payslip.getDeductedPFAmount();
		 esic1+=payslip.getDeductedESICAmount();
		 pt1+=payslip.getDeductedPTAmount();
		 mlwf1+=payslip.getDeductedLWFAmount();
		 it1+=payslip.getDeductedITAmount();

		 totaldeduction1+=payslip.getTotalDeduction();
		 netwagespaid1+=payslip.getNetEarning();
		 otherearningamount+=payslip .getOtherEarningAmount();
		othertotaldeductedamount +=payslip.gettotalOtherDeductedAmount();

		
		
		PdfPCell cell31=cell.getCell(payslip.getEmpid()+"", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell1.setBorder(0);
		
		PdfPCell cell32=cell.getCell(payslip.getEmployeeName(), font7, Element.ALIGN_LEFT, 2, 0, 0);
		cell2.setBorder(0);
		
		PdfPCell cell33=cell.getCell(payslip.getGender(), font7, Element.ALIGN_LEFT, 2, 0, 0);
		cell3.setBorder(0);
		
		PdfPCell cell34=cell.getCell(payslip.getEmployeedDesignation(), font7, Element.ALIGN_LEFT, 2, 0, 0);
		cell4.setBorder(0);
		
		/**
		 * @author Anil
		 * @since 01-07-2020
		 * Employee calendar issue while printing form 2
		 */
		String workingHours="";
		if(employeeinfo!=null&&employeeinfo.getLeaveCalendar()!=null){
			workingHours=employeeinfo.getLeaveCalendar().getWorkingHours()+"";
		}
		
		PdfPCell cell35=cell.getCell(workingHours, font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell5.setBorder(0);
		PdfPCell cell36=cell.getCell("1", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell6.setBorder(0);
		
		PdfPCell cell37=cell.getCell(fmt.format(payslip.getDoj())+"", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell7.setBorder(0);
		
		PdfPCell cell38=cell.getCell(payslip.getPaidDays()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell8.setBorder(0);
		
		PdfPCell cell39=cell.getCell(payslip.getPFAmount()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell9.setBorder(0);
		
		PdfPCell cell40=cell.getCell(payslip.getHRAAmount()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell10.setBorder(0);
		
		PdfPCell cell41=cell.getCell(" ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell11.setBorder(0);
		
		PdfPCell cell42=cell.getCell(payslip.getOtherEarningAmount()+"", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell12.setBorder(0);
		
		PdfPCell cell43=cell.getCell(payslip.getGrossEarning()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell3.setBorder(0);
		
		PdfPCell cell44=cell.getCell(" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell14.setBorder(0);
		
		PdfPCell cell45=cell.getCell(payslip.getDeductedPFAmount()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell15.setBorder(0);
		
		PdfPCell cell46=cell.getCell(payslip.getDeductedESICAmount()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell16.setBorder(0);
		
		PdfPCell cell47=cell.getCell(payslip.getDeductedPTAmount()+" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell17.setBorder(0);
		
		PdfPCell cell48=cell.getCell(payslip.getDeductedLWFAmount()+"", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell18.setBorder(0);
		
		PdfPCell cell49=cell.getCell(payslip.getDeductedITAmount()+" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell19.setBorder(0);
		
		PdfPCell cell50=cell.getCell(payslip.gettotalOtherDeductedAmount()+" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell20.setBorder(0);
		
		PdfPCell cell51=cell.getCell(payslip.getTotalDeduction()+" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell21.setBorder(0);
		
		PdfPCell cell52=cell.getCell(payslip.getNetEarning()+" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell22.setBorder(0);
		
		PdfPCell cell53=cell.getCell(" ", font7, Element.ALIGN_RIGHT, 2, 0, 0);
		cell23.setBorder(0);
		
		PdfPCell cell54=cell.getCell(" ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell24.setBorder(0);
		
		PdfPCell cell55=cell.getCell("", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell25.setBorder(0);
		
		PdfPCell cell56=cell.getCell("", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell26.setBorder(0);
		
		PdfPCell cell57=cell.getCell("", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell27.setBorder(0);
		
		PdfPCell cell58=cell.getCell("", font7, Element.ALIGN_CENTER, 2, 0, 0);
		cell28.setBorder(0);
		
		outerTbl.addCell(cell31);
		outerTbl.addCell(cell32);
		outerTbl.addCell(cell33);
		outerTbl.addCell(cell34);
		outerTbl.addCell(cell35);
		outerTbl.addCell(cell36);
		outerTbl.addCell(cell37);
		outerTbl.addCell(cell38);
		outerTbl.addCell(cell39);
		outerTbl.addCell(cell40);
		outerTbl.addCell(cell41);
		outerTbl.addCell(cell42);
		outerTbl.addCell(cell43);
		outerTbl.addCell(cell44);
		outerTbl.addCell(cell45);
		outerTbl.addCell(cell46);
		outerTbl.addCell(cell47);
		outerTbl.addCell(cell48);
		outerTbl.addCell(cell49);
		outerTbl.addCell(cell50);
		outerTbl.addCell(cell51);
		outerTbl.addCell(cell52);
		outerTbl.addCell(cell53);
		outerTbl.addCell(cell54);
		outerTbl.addCell(cell55);
		outerTbl.addCell(cell56);
		outerTbl.addCell(cell57);
		outerTbl.addCell(cell58);
		}
		
		PdfPCell cell61=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		
	
		PdfPCell cell62=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell63=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell64=cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		
		PdfPCell cell65=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell66=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell67=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell68=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell69=cell.getCell(normalEarnings+"", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		
		PdfPCell cell70=cell.getCell(hrapayable+"", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		
		PdfPCell cell71=cell.getCell(" ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		
		PdfPCell cell72=cell.getCell(otherearningamount+"", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell73=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		PdfPCell cell74=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		PdfPCell cell75=cell.getCell(pf1+"", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		PdfPCell cell76=cell.getCell(esic1+"", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		
		PdfPCell cell77=cell.getCell(pt1+" ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		
		PdfPCell cell78=cell.getCell(mlwf1+" ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		
		PdfPCell cell79=cell.getCell(it1+" ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		PdfPCell cell80=cell.getCell(othertotaldeductedamount+" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell81=cell.getCell(totaldeduction1+" ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		PdfPCell cell82=cell.getCell(netwagespaid1+" ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0);
		
		PdfPCell cell83=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell84=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell85=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell86=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell87=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		PdfPCell cell88=cell.getCell(" ", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		outerTbl.addCell(cell61);
		outerTbl.addCell(cell62);
		outerTbl.addCell(cell63);
		outerTbl.addCell(cell64);
		outerTbl.addCell(cell65);
		outerTbl.addCell(cell66);
		outerTbl.addCell(cell67);
		outerTbl.addCell(cell68);
		outerTbl.addCell(cell69);
		outerTbl.addCell(cell70);
		outerTbl.addCell(cell71);
		outerTbl.addCell(cell72);
		outerTbl.addCell(cell73);
		outerTbl.addCell(cell74);
		outerTbl.addCell(cell75);
		outerTbl.addCell(cell76);
		outerTbl.addCell(cell77);
		outerTbl.addCell(cell78);
		outerTbl.addCell(cell79);
		outerTbl.addCell(cell80);
		outerTbl.addCell(cell81);
		outerTbl.addCell(cell82);
		outerTbl.addCell(cell83);
		outerTbl.addCell(cell84);
		outerTbl.addCell(cell85);
		outerTbl.addCell(cell86);
		outerTbl.addCell(cell87);
		outerTbl.addCell(cell88);
	
		
		
		
		
		
		
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	private void createFooterTable() {
		PdfPTable outerTbl=new PdfPTable(3);
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(new float[] {31,35,33});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		outerTbl.addCell(cell.getCell(" The actual number of house worked on each day shall be marked.", font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("The actual number of house worked on each day shall be marked. ", font7a, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Signature of the employer or the person authorised ", font7abold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" mark \"H\" and \"A\" or \"L\" in the employee is absent", font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("  mark \"H\" and \"A\" or \"L\" in the employee is absent", font7a, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font7a, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("by him to authenticates the above entries.", font7abold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
		
		
	}
	private void formIIPartTwo(){
		PdfPTable outerTbl=new PdfPTable(4);
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(new float[] {30,40,10,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PaySlip payslip=paySlipList.get(0);
		outerTbl.setSpacingBefore(40);
		
		
		String payrollMonth="";
		if(paySlipList!=null&&paySlipList.size()!=0){
			System.out.println("PAYSLIPLIST SIZE "+paySlipList.size());
			
			payrollMonth=paySlipList.get(0).getSalaryPeriod();
			System.out.println("PAYROLL MONTH VALUE "+payrollMonth);
			
		}
		Date payrollDate=null;
		try {
			payrollDate=fmt2.parse(payrollMonth);
			System.out.println("PAYROLL MONTH BEFORE "+payrollMonth);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		payrollMonth=fmt3.format(payrollDate);
		
		
		outerTbl.addCell(cell.getCell("Name of the Establishment :"+company.getBusinessUnitName(), font10bold, Element.ALIGN_CENTER, 0, 4, 20)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_RIGHT, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell("Address :"+company.getAddress().getCompleteAddress(), font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_RIGHT, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_RIGHT, 0, 0, 20)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("Name of the Employer : "+payslip.getProjectName(), font7bold, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_RIGHT, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font7a, Element.ALIGN_RIGHT, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell("Month : "+payrollMonth, font7bold, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
		
		outerTbl.setSpacingAfter(10);
		
		
//		PdfPTable outerTable=new PdfPTable(32);
//		outerTable.setWidthPercentage(100f);
//		PdfPCell hoursWorked=cell.getCell("HOURS WORKED ON", font7, Element.ALIGN_CENTER, 0,32, 0);
//		hoursWorked.setBorder(0);
		PdfPCell cell41=cell.getCell("1", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell2=cell.getCell("2", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell3=cell.getCell("3", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell4=cell.getCell("4", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell5=cell.getCell("5", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell6=cell.getCell("6", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell7=cell.getCell("7", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell8=cell.getCell("8", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell9=cell.getCell("9", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell10=cell.getCell("10", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell11=cell.getCell("11", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell12=cell.getCell("12", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell13=cell.getCell("13", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell14=cell.getCell("14", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell15=cell.getCell("15", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell16=cell.getCell("16", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell17=cell.getCell("17", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell18=cell.getCell("18", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell19=cell.getCell("19", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell20=cell.getCell("20", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell21=cell.getCell("21", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell22=cell.getCell("22", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell23=cell.getCell("23", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell24=cell.getCell("24", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell25=cell.getCell("25", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell26=cell.getCell("26", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell27=cell.getCell("27", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell28=cell.getCell("28", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell29=cell.getCell("29", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell30=cell.getCell("30", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell cell31=cell.getCell("31", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell totaldays=cell.getCell("Total Days Worked", font7, Element.ALIGN_CENTER, 1, 0, 0);
		
		
		
		PdfPTable outerTb2=new PdfPTable(43);
		outerTb2.setWidthPercentage(100f);
		
		try {
			outerTb2.setWidths(new float[] {0.7f,2.3f,2.5f,0.5f,2.0f,2.5f,1.2f,1.2f,1.0f,1.0f,2.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.1f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		
		PdfPCell cell1=cell.getCell("MUSTOR ROLL FORM II SEE MAH.M.W.RULE 27(1)& Register of Workmen Form-i (See MAH.W.M.H..R.A Rule 12)", font7abold, Element.ALIGN_CENTER, 0, 43, 0);
		outerTb2.addCell(cell1);
		
	
		
		
		
		PdfPCell srno=cell.getCell("Sr.No ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell empCode=cell.getCell("Emp.Code", font7, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell name=cell.getCell("Full Name Of The Employee ", font7, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell sex=cell.getCell("Sex", font7, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell dob=cell.getCell("Date of birth", font7, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell nature=cell.getCell("Nature Of Work & Designation", font7, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell workhrs=cell.getCell("Working Hours", font7, Element.ALIGN_CENTER, 1,2,0);
		PdfPCell from=cell.getCell(" From", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell to=cell.getCell(" To", font7, Element.ALIGN_CENTER, 1, 0, 0);
//		
		PdfPCell leavewages=cell.getCell("Leave With Wages", font7, Element.ALIGN_CENTER, 1, 2, 0);
		PdfPCell prebal=cell.getCell("Pre.Bal", font7, Element.ALIGN_CENTER, 1, 0, 0);
		PdfPCell enjoyed=cell.getCell("Enjoyed or Refused", font7, Element.ALIGN_CENTER, 1, 0, 0);
//		
//		PdfPCell cell73=cell.getCell("Note: The actual number of hours worked on each day shall be marked.Mark "H" shall be made in the column relating to any day on which a weekly holiday is given and "A"or "L" if the emplolyee is absent or an leave as the case may be.", font7abold, Element.ALIGN_CENTER, 0, 0, 0);
		PdfPCell doe=cell.getCell("Date of Entry", font7, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell hoursWorked=cell.getCell("Hours Worked On", font7, Element.ALIGN_CENTER, 1,33, 0);
		
		PdfPCell Note=cell.getCell("Note: The actual number of hours worked on each day shall be marked.Mark \"H\" shall be made in the column relating to any day on which a weekly holiday is given and \"A\"or \"L\" if the emplolyee is absent or an leave as the case may be.", font7, Element.ALIGN_LEFT, 0,43, 0);
		PdfPCell nameofsupervisor=cell.getCell("Name of the Supervisor : "+hrProject.getSupervisor(), font7, Element.ALIGN_LEFT, 0,7, 0);
		PdfPCell signature=cell.getCell("Signature", font7, Element.ALIGN_LEFT, 0, 6, 0);
		
		PdfPCell signatureofftheemployer=cell.getCell("Signature of the employer or the person authorized by him to authenticate the above entries", font7, Element.ALIGN_LEFT, 0, 30, 0);
//		hrsworkedon.addElement(outerTable);
		
		outerTb2.addCell(srno);
		outerTb2.addCell(empCode);
		outerTb2.addCell(name);
		outerTb2.addCell(sex);
		outerTb2.addCell(dob);
		outerTb2.addCell(nature);
		
		outerTb2.addCell(workhrs);
		
		outerTb2.addCell(leavewages);
//		
		
		outerTb2.addCell(doe);
		outerTb2.addCell(hoursWorked);
		outerTb2.addCell(from);
		outerTb2.addCell(to);
		outerTb2.addCell(prebal);
		outerTb2.addCell(enjoyed);
//		outerTb2.addCell(hrsworkedon);
//		outerTb2.addCell(blank);
//		outerTb2.addCell(from);
//		outerTb2.addCell(to);
//		
		outerTb2.addCell(cell41);
		outerTb2.addCell(cell2);
		outerTb2.addCell(cell3);
		outerTb2.addCell(cell4);
		outerTb2.addCell(cell5);
		outerTb2.addCell(cell6);
		outerTb2.addCell(cell7);
		outerTb2.addCell(cell8);
		outerTb2.addCell(cell9);
		outerTb2.addCell(cell10);
		outerTb2.addCell(cell11);
		outerTb2.addCell(cell12);
		outerTb2.addCell(cell13);
		outerTb2.addCell(cell14);
		outerTb2.addCell(cell15);
		outerTb2.addCell(cell16);
		outerTb2.addCell(cell17);
		outerTb2.addCell(cell18);
		outerTb2.addCell(cell19);
		outerTb2.addCell(cell20);
		outerTb2.addCell(cell21);
		outerTb2.addCell(cell22);
		outerTb2.addCell(cell23);
		outerTb2.addCell(cell24);
		outerTb2.addCell(cell25);
		outerTb2.addCell(cell26);
		outerTb2.addCell(cell27);
		outerTb2.addCell(cell28);
		outerTb2.addCell(cell29);
		outerTb2.addCell(cell30);
		outerTb2.addCell(cell31);
		outerTb2.addCell(totaldays);
//		outerTb2.addCell(Note);
//		
		int counter=0;
		for(PaySlip pay:paySlipList){
//			for(Shift shift:shiftList){
			counter++;
			
			
			String gender=pay.getGender();
			char genderr=gender.charAt(0);
			
			PdfPCell srnovalue=cell.getCell(counter+" ", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell empCodeValue=cell.getCell(pay.getEmpid()+"", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell nameValue=cell.getCell(pay.getEmployeeName()+"", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell sexValue=cell.getCell(genderr+"", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell dobValue=cell.getCell(format.format(pay.getDob())+"", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell natureValue=cell.getCell(pay.getEmployeedDesignation()+"", font7, Element.ALIGN_CENTER, 0, 0, 0);
			
			PdfPCell workhrsvalue=cell.getCell(" ", font7, Element.ALIGN_CENTER, 1, 2, 0);
			
			
			String inTime="";
			String outTime="";
			Shift shift=getShift(attendanceList,pay.getEmpid(), shiftList,pay.getProjectName());
			
			System.out.println("INSIDE LOOP"+attendanceList.size());
			
			if(shift!=null){
				inTime=shift.getFromTime()+"";
				
				System.out.println("INTIME"+shift.getFromTime()+"");
				
				outTime=shift.getToTime()+"";
			}
			
			PdfPCell fromvalue=cell.getCell(inTime, font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell  tovalue=cell.getCell(outTime, font7, Element.ALIGN_CENTER, 0, 0, 0);
			
			PdfPCell leavewagesvalue=cell.getCell(" ", font7, Element.ALIGN_CENTER, 1, 2, 0);
			PdfPCell prebalvalue=cell.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell enjoyedvalue=cell.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0);
			
			PdfPCell doevalue=cell.getCell(format.format(pay.getDoj())+" ", font7, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell hoursworkedvalue=cell.getCell(" ", font7, Element.ALIGN_CENTER, 1, 33, 0);
			
			
//			if(getAttendanceDetails(attendanceList,pay.getEmpid(),1,pay.getProjectName()).equals("HF")){
//				value = new PdfPCell("HD");
//			}
//			System.out.println("Project Name"+pay.getProjectName());
			PdfPCell cell1value=null;
			String Halfday="HD";
			String Paidleave="L";
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),1,pay.getProjectName()).equals("PL")){
				cell1value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),1,pay.getProjectName()).equals("HF")){
				cell1value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
	         else{
				cell1value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),1,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}
			
			
			PdfPCell cell2value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),2,pay.getProjectName()).equals("PL")){
            	cell2value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),2,pay.getProjectName()).equals("HF")){
				cell2value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell2value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),2,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell2value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),2,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
           
            PdfPCell cell3value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),3,pay.getProjectName()).equals("PL")){
            	cell3value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),3,pay.getProjectName()).equals("HF")){
				cell3value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}else{
				cell3value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),3,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
			
			
//			PdfPCell cell3value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),3,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
            
        	PdfPCell cell4value=null;
			 if(getAttendanceDetails(attendanceList,pay.getEmpid(),4,pay.getProjectName()).equals("PL")){
				 cell4value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),4,pay.getProjectName()).equals("HF")){
					cell4value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell4value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),4,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
			
//			PdfPCell cell4value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),4,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			 
			 PdfPCell cell5value=null;
			 if(getAttendanceDetails(attendanceList,pay.getEmpid(),5,pay.getProjectName()).equals("PL")){
				 cell5value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),5,pay.getProjectName()).equals("HF")){
					cell5value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell5value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),5,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
//			PdfPCell cell5value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),5,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
				PdfPCell cell6value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),6,pay.getProjectName()).equals("PL")){
				 cell6value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),6,pay.getProjectName()).equals("HF")){
					cell6value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}else{
					cell6value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),6,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
//			PdfPCell cell6value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),6,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			PdfPCell cell7value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),7,pay.getProjectName()).equals("PL")){
				 cell7value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),7,pay.getProjectName()).equals("HF")){
					cell7value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell7value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),7,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
//			PdfPCell cell7value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),7,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
			PdfPCell cell8value=null;
			 if(getAttendanceDetails(attendanceList,pay.getEmpid(),8,pay.getProjectName()).equals("PL")){
				 cell8value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),8,pay.getProjectName()).equals("HF")){
					cell8value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}else{
					cell8value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),8,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
			
//			PdfPCell cell8value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),8,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
			 PdfPCell cell9value=null;
			 if(getAttendanceDetails(attendanceList,pay.getEmpid(),9,pay.getProjectName()).equals("PL")){
				 cell9value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),9,pay.getProjectName()).equals("HF")){
					cell9value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell9value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),9,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
			
//            PdfPCell cell9value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),9,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
    
			 PdfPCell cell10value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),10,pay.getProjectName()).equals("PL")){
	 cell10value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),10,pay.getProjectName()).equals("HF")){
				cell10value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell10value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),10,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}
//            PdfPCell cell10value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),10,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
 
            PdfPCell cell11value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),11,pay.getProjectName()).equals("PL")){
	        cell11value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),11,pay.getProjectName()).equals("HF")){
				cell11value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell11value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),11,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell11value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),11,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
            
            PdfPCell cell12value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),12,pay.getProjectName()).equals("PL")){
            	cell12value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),12,pay.getProjectName()).equals("HF")){
				cell12value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
				else{
					cell12value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),12,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
				
			}
 
//            PdfPCell cell12value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),12,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
 
            
            PdfPCell cell13value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),13,pay.getProjectName()).equals("PL")){
            	cell13value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),13,pay.getProjectName()).equals("HF")){
				cell13value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell13value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),13,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell13value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),13,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
 
            PdfPCell cell14value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),14,pay.getProjectName()).equals("PL")){
	        cell14value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),14,pay.getProjectName()).equals("HF")){
				cell14value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell14value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),14,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell14value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),14,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);

            PdfPCell cell15value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),15,pay.getProjectName()).equals("PL")){
	        cell15value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),15,pay.getProjectName()).equals("HF")){
				cell15value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell15value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),15,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}
//            PdfPCell cell15value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),15,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
 
            PdfPCell cell16value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),16,pay.getProjectName()).equals("PL")){
	       cell16value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),16,pay.getProjectName()).equals("HF")){
				cell16value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell16value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),16,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell16value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),16,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
     
            
            PdfPCell cell17value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),17,pay.getProjectName()).equals("PL")){
	            cell17value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),17,pay.getProjectName()).equals("HF")){
				cell17value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell17value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),17,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell17value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),17,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
 
            PdfPCell cell18value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),18,pay.getProjectName()).equals("PL")){
            	cell18value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),18,pay.getProjectName()).equals("HF")){
				cell18value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell18value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),18,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell18value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),18,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
 
            PdfPCell cell19value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),19,pay.getProjectName()).equals("PL")){
	
	         cell19value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),19,pay.getProjectName()).equals("HF")){
				cell19value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell19value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),19,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell19value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),19,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
           
            PdfPCell cell20value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),20,pay.getProjectName()).equals("PL")){
            	cell20value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),20,pay.getProjectName()).equals("HF")){
				cell20value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}else{
				cell20value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),20,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell20value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),20,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
           
            PdfPCell cell21value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),21,pay.getProjectName()).equals("PL")){
            	 cell21value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),21,pay.getProjectName()).equals("HF")){
				 cell21value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				 cell21value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),21,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//            PdfPCell cell21value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),21,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
            
            PdfPCell cell22value=null;
            if(getAttendanceDetails(attendanceList,pay.getEmpid(),22,pay.getProjectName()).equals("PL")){
            	cell22value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),22,pay.getProjectName()).equals("HF")){
				cell22value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
			}else{
				cell22value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),22,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
//           PdfPCell cell22value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),22,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
          
            PdfPCell cell23value=null;
           if(getAttendanceDetails(attendanceList,pay.getEmpid(),23,pay.getProjectName()).equals("PL")){
        	   cell23value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),23,pay.getProjectName()).equals("HF")){
				cell23value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
			}else{
				cell23value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),23,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			}
           
//			PdfPCell cell23value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),23,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			 
           PdfPCell cell24value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),24,pay.getProjectName()).equals("PL")){
				 cell24value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),24,pay.getProjectName()).equals("HF")){
					cell24value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}else{
					cell24value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),24,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
			 
//			PdfPCell cell24value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),24,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
			PdfPCell cell25value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),25,pay.getProjectName()).equals("PL")){
				 cell25value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),25,pay.getProjectName()).equals("HF")){
					cell25value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell25value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),25,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
			 
//			PdfPCell cell25value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),25,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
			
			PdfPCell cell26value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),26,pay.getProjectName()).equals("PL")){
				 cell26value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),26,pay.getProjectName()).equals("HF")){
					cell26value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}else{
					cell26value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),26,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
			 
//			PdfPCell cell26value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),26,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			 
			PdfPCell cell27value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),27,pay.getProjectName()).equals("PL")){
				 cell27value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),27,pay.getProjectName()).equals("HF")){
					cell27value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell27value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),27,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
			 
//			PdfPCell cell27value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),27,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
			
			PdfPCell cell28value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),28,pay.getProjectName()).equals("PL")){
				
				 cell28value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),28,pay.getProjectName()).equals("HF")){
					cell28value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell28value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),28,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
			 
//			PdfPCell cell28value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),28,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			PdfPCell cell29value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),29,pay.getProjectName()).equals("PL")){
				 cell29value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),29,pay.getProjectName()).equals("HF")){
					cell29value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell29value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),29,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);	
				}
			 
//			PdfPCell cell29value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),29,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			 
			PdfPCell cell30value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),30,pay.getProjectName()).equals("PL")){
				cell30value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),30,pay.getProjectName()).equals("HF")){
					cell30value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell30value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),30,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
			 
//			PdfPCell cell30value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),30,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			
			
			PdfPCell cell31value=null;
			if(getAttendanceDetails(attendanceList,pay.getEmpid(),31,pay.getProjectName()).equals("PL")){
				    cell31value=cell.getCell(Paidleave, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else if(getAttendanceDetails(attendanceList,pay.getEmpid(),31,pay.getProjectName()).equals("HF")){
					cell31value=cell.getCell(Halfday, font7, Element.ALIGN_CENTER, 1, 0, 0);
				}else{
					cell31value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),31,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
				}
//			 PdfPCell cell31value=cell.getCell(getAttendanceDetails(attendanceList,pay.getEmpid(),31,pay.getProjectName())+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);


			PdfPCell totaldaysworked=cell.getCell(pay.getEligibleDays()+" ", font7, Element.ALIGN_CENTER, 1, 0, 0);
			

			outerTb2.addCell(srnovalue);
			outerTb2.addCell(empCodeValue);
			outerTb2.addCell(nameValue);
			outerTb2.addCell(sexValue);
			outerTb2.addCell(dobValue);
			outerTb2.addCell(natureValue);
			
//			outerTb2.addCell(workhrsvalue);
			
//			outerTb2.addCell(leavewagesvalue);
			
			
//			outerTb2.addCell(hoursworkedvalue);
			outerTb2.addCell(fromvalue);
			outerTb2.addCell(tovalue);
			outerTb2.addCell(prebalvalue);
			outerTb2.addCell(enjoyedvalue);
			outerTb2.addCell(doevalue);
			outerTb2.addCell(cell1value);
			outerTb2.addCell(cell2value);
			outerTb2.addCell(cell3value);
			outerTb2.addCell(cell4value);
			outerTb2.addCell(cell5value);
			outerTb2.addCell(cell6value);
			outerTb2.addCell(cell7value);
			outerTb2.addCell(cell8value);
			outerTb2.addCell(cell9value);
			outerTb2.addCell(cell10value);
			outerTb2.addCell(cell11value);
			outerTb2.addCell(cell12value);
			outerTb2.addCell(cell13value);
			outerTb2.addCell(cell14value);
			outerTb2.addCell(cell15value);
			outerTb2.addCell(cell16value);
			outerTb2.addCell(cell17value);
			outerTb2.addCell(cell18value);
			outerTb2.addCell(cell19value);
			outerTb2.addCell(cell20value);
			outerTb2.addCell(cell21value);
			outerTb2.addCell(cell22value);
			outerTb2.addCell(cell23value);
			outerTb2.addCell(cell24value);
			outerTb2.addCell(cell25value);
			outerTb2.addCell(cell26value);
			outerTb2.addCell(cell27value);
			outerTb2.addCell(cell28value);
			outerTb2.addCell(cell29value);
			outerTb2.addCell(cell30value);
			outerTb2.addCell(cell31value);
			outerTb2.addCell(totaldaysworked);
			
//		}
			}
		 outerTb2.addCell(nameofsupervisor);
	     outerTb2.addCell(signature);
	     outerTb2.addCell(signatureofftheemployer);
		  outerTb2.addCell(Note);
		    
		
		try {
			document.add(outerTbl);
			document.add(outerTb2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	
	

}
