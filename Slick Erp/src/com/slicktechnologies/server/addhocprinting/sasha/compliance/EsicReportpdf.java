package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.shared.TaxSummaryBean;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.BusinessUnit;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CompanyPayrollRecord;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EsicReportpdf<create> {
	public Document document;
	Company comp;
	Branch branch = null;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private Font font16boldul, font12bold, font8bold, font11bold, font8,
			font12boldul, font12, font6bold,font10bold;

	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df = new DecimalFormat("0");

	ArrayList<PaySlip> pfList = new ArrayList<PaySlip>();
	int noofemployee = 0;
	int noOfLines = 0;
	int loopCount = 0;
	double sumOfDays = 0;
	double sumOfWages = 0;
	double sumOfContribution = 0;
	double sumOfEmpContribution = 0;
	String deductionTypeName = "";
	int pageno = 1;
	
	/**
	 * @author Anil , Date : 01-08-2019
	 * Storing employee wise payslip for statutory reports
	 */
	public HashMap<String,ArrayList<PaySlip>> statoturyReportMap=new HashMap<String,ArrayList<PaySlip>>();
	HashMap<String,Double> employerContributionMap=new HashMap<String, Double>();
	String esicCompName="";

	public EsicReportpdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		font10bold= new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void loadEmployeedata(Long count) {

		comp = ofy().load().type(Company.class).filter("companyId", count)
				.first().now();
		pfList = CsvWriter.statuteryReport;
		System.out.println("list size :" + pfList.size());
		branch = ofy().load().type(Branch.class)
				.filter("companyId", pfList.get(0).getCompanyId())
				.filter("buisnessUnitName", pfList.get(0).getBranch()).first()
				.now();
		noofemployee = pfList.size();
		statoturyReportMap=CsvWriter.statoturyReportMap;
		
		
		HashSet<Integer>hsEmp=new HashSet<Integer>();
		for(PaySlip paySlip:pfList){
			hsEmp.add(paySlip.getEmpid());
		}
		ArrayList<Integer> empIdLis=new ArrayList<Integer>(hsEmp);
		List<CompanyPayrollRecord> employerContributionList = new ArrayList<CompanyPayrollRecord>();
		esicCompName="";
		if (empIdLis.size()!=0) {
			employerContributionList = ofy().load().type(CompanyPayrollRecord.class).filter("companyId", pfList.get(0).getCompanyId()).filter("payrollMonth", pfList.get(0).getSalaryPeriod()).filter("empId IN", empIdLis).list();
			for(CompanyPayrollRecord obj:employerContributionList){
				if(obj.getCtcCompShortName().equalsIgnoreCase("ESIC")){
					esicCompName=obj.getCtcCompShortName();
					String key=obj.getEmpId()+obj.getPayrollMonth()+obj.getCtcCompShortName();
					if(employerContributionMap!=null&&employerContributionMap.size()!=0){
						if(employerContributionMap.containsKey(key)){
							double amount=employerContributionMap.get(key);
							employerContributionMap.put(key, amount+obj.getCtcAmount());
						}else{
							employerContributionMap.put(key, obj.getCtcAmount());
						}
					}else{
						employerContributionMap.put(key, obj.getCtcAmount());
					}
				}
			}
		
		}
	}

	public void createEsicReportpdf() {
		createCompanyHeddding();
		geEsicRecordTable(30, loopCount);
		while (noOfLines == 0 && loopCount < pfList.size()) {
			pageno++;
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			geEsicRecordTable(30, loopCount);
		}
		esicrecords();
		createesicsummaryTable();
	}

	private PdfPCell getCell(String value, Font font, int alignment) {
		Phrase phrase = new Phrase(value, font);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(alignment);
		return cell;
	}

	public void createCompanyHeddding() {
		PdfPTable maintitle = new PdfPTable(1);
		maintitle.setWidthPercentage(100);
		Phrase titleph1 = new Phrase(comp.getBusinessUnitName(), font16boldul);
		titleph1.add(Chunk.NEWLINE);
		PdfPCell title1Cell = new PdfPCell(titleph1);
		title1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		maintitle.addCell(title1Cell).setBorder(0);

		try {
			document.add(maintitle);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat fmt1 = new SimpleDateFormat("yyyy-MMM");
		SimpleDateFormat fmt2 = new SimpleDateFormat("MMM-yyyy");
		String payrollMonth = "";
		if (pfList.size() > 0) {
			try {
				payrollMonth = fmt2.format(fmt1.parse(pfList.get(0).getSalaryPeriod().trim()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		System.out.println("payroll month :" + payrollMonth);
		PdfPTable title2 = new PdfPTable(1);
		title2.setWidthPercentage(100);
		Phrase titlepf = new Phrase(
				"Monthly ESIC Contribution Statement for the Month  "
						+ payrollMonth, font11bold);
		titlepf.add(Chunk.NEWLINE);
		PdfPCell titleCell1 = new PdfPCell(titlepf);
		titleCell1.setPaddingTop(10);

		titleCell1.setPaddingBottom(10);
		titleCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		title2.addCell(titleCell1).setBorder(0);

		try {
			document.add(title2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String esicCode = "";
		if (branch != null && branch.getEsicCode() != null) {
			esicCode = branch.getEsicCode();
		}

		PdfPTable titleTab = new PdfPTable(2);
		titleTab.setWidthPercentage(100);
		try {
			titleTab.setWidths(new float[] { 50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell cell = getCell("Code No. of the Establishment:     "+ esicCode, font10bold, Element.ALIGN_LEFT);
		cell.setBorder(0);
        titleTab.addCell(cell);
        
        
        PdfPCell cell1=getCell("Page: "+pageno, font8bold,Element.ALIGN_RIGHT);
		cell1.setBorder(0);
        titleTab.addCell(cell1);
	
		
		
		
		


		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void geEsicRecordTable(int count, int loopC) {
		
		
		CsvWriter csv=new CsvWriter();
		HashMap<String,ArrayList<PaySlip>> innerReportMap=new HashMap<String,ArrayList<PaySlip>>();
		for (int j = loopC; j < pfList.size(); j++) {
			String key=pfList.get(j).getEmpid()+pfList.get(j).getSalaryPeriod();
			if(innerReportMap!=null&&innerReportMap.size()!=0){
				if(innerReportMap.containsKey(key)==false){
					innerReportMap.put(key, statoturyReportMap.get(key));
				}
			}else{
				innerReportMap.put(key, statoturyReportMap.get(key));
			}
		}

		noOfLines = count;
		PdfPTable esicRecordTable = new PdfPTable(7);
		esicRecordTable.setWidthPercentage(100);
		try {
			esicRecordTable
					.setWidths(new float[] { 5, 10, 15, 40, 10, 10, 10 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		esicRecordTable.addCell(getCell("Sr.NO", font8bold,Element.ALIGN_CENTER));
		esicRecordTable.addCell(getCell("Employee Number", font8bold,Element.ALIGN_CENTER));
		esicRecordTable.addCell(getCell("Insurance Number", font8bold,Element.ALIGN_CENTER));
		esicRecordTable.addCell(getCell("Employee Name", font8bold,Element.ALIGN_LEFT));
		esicRecordTable.addCell(getCell("Days", font8bold, Element.ALIGN_CENTER));
		esicRecordTable.addCell(getCell("Wages", font8bold,Element.ALIGN_CENTER));
		esicRecordTable.addCell(getCell("Contribution", font8bold,Element.ALIGN_CENTER));

		int i = 0;
		for(Map.Entry<String, ArrayList<PaySlip>> entry:statoturyReportMap.entrySet()){
			ArrayList<PaySlip> paySlipList=entry.getValue();
			
			double esicWages=0;
			double esicContribution=0;
			double esicEmpContribution=0;
			double paidDays=0;
			
			for(PaySlip obj:paySlipList){
				String key=obj.getEmpid()+obj.getSalaryPeriod()+esicCompName;
				esicWages=esicWages+csv.getEsicWages(obj,true,false);
				esicContribution=esicContribution+csv.getEsicWages(obj,false,true);
				
				if(employerContributionMap.containsKey(key)){
					esicEmpContribution=esicEmpContribution+employerContributionMap.get(key);
				}
				paidDays=paidDays+obj.getPaidDays();
			}
			PaySlip pay=paySlipList.get(0);
//		for (int j = loopC; j < pfList.size(); j++) {
//			PaySlip pay = pfList.get(j);
			if (noOfLines == 0) {
				loopCount = i;
				break;
			}
			
			noOfLines--;

			String EsicNumber = "";
			if (pay.getEsicNumber() != null) {
				EsicNumber = pay.getEsicNumber();
			}
//			String deductionTypeName = "";
//			double washingAllowanceAmt = getWashingAllowanceAmount(pay.getEarningList(), deductionTypeName);
//			double esicWages = pay.getGrossEarningWithoutOT()- washingAllowanceAmt;
//			PaySlip ps = new PaySlip();
//			double esicContribution = ps.getEsicRoundOff((esicWages * 1.75) / 100);
//			double esicEmpContribution = ps.getEsicRoundOff((esicWages * 4.75) / 100);

			esicRecordTable.addCell(getCell((i + 1) + "", font8,Element.ALIGN_CENTER));
			esicRecordTable.addCell(getCell(pay.getEmpid() + "", font8,Element.ALIGN_CENTER));
			esicRecordTable.addCell(getCell(EsicNumber, font8,Element.ALIGN_CENTER));
			esicRecordTable.addCell(getCell(pay.getEmployeeName() + "", font8,Element.ALIGN_LEFT));
			esicRecordTable.addCell(getCell(df.format(pay.getPaidDays()) + "", font8,Element.ALIGN_RIGHT));
			esicRecordTable.addCell(getCell(Math.round(esicWages) + "", font8,Element.ALIGN_RIGHT));
			esicRecordTable.addCell(getCell(df.format(esicContribution )+ "", font8,Element.ALIGN_RIGHT));


			sumOfDays = sumOfDays + pay.getPaidDays();
			sumOfWages = sumOfWages + esicWages;
			sumOfContribution = sumOfContribution + esicContribution;
			sumOfEmpContribution = sumOfEmpContribution + esicEmpContribution;

			
			i++;
		}
		try {
			document.add(esicRecordTable);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private double getWashingAllowanceAmount(
			ArrayList<CtcComponent> earningList, String deductionTypeName) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void esicrecords() {
		PdfPTable esicRecordtotalTable = new PdfPTable(4);
		esicRecordtotalTable.setWidthPercentage(100);
		try {
			esicRecordtotalTable.setWidths(new float[] { 70, 10, 10, 10 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		esicRecordtotalTable.addCell(getCell("Sub Total ==>>", font8bold,
				Element.ALIGN_CENTER));
		esicRecordtotalTable.addCell(getCell(df.format(sumOfDays) + "", font8bold,
				Element.ALIGN_RIGHT));
		esicRecordtotalTable.addCell(getCell(df.format(sumOfWages )+ "", font8bold,
				Element.ALIGN_RIGHT));
		esicRecordtotalTable.addCell(getCell(df.format(sumOfContribution) + "", font8bold,
				Element.ALIGN_RIGHT));
		esicRecordtotalTable.addCell(getCell("Grand Total ==>>", font8bold,
				Element.ALIGN_CENTER));
		esicRecordtotalTable.addCell(getCell(df.format(sumOfDays )+ "", font8bold,
				Element.ALIGN_RIGHT));
		esicRecordtotalTable.addCell(getCell(df.format(sumOfWages) + "", font8bold,
				Element.ALIGN_RIGHT));
		esicRecordtotalTable.addCell(getCell(df.format(sumOfContribution) + "", font8bold,
				Element.ALIGN_RIGHT));

		try {

			document.add(esicRecordtotalTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createesicsummaryTable() {

		String payrollMonth = "";
		if (pfList.size() > 0) {
			payrollMonth = pfList.get(0).getSalaryPeriod();
		}
		PdfPTable outerTbl = new PdfPTable(1);
		outerTbl.setSpacingAfter(20);
		outerTbl.addCell(
				getCell("Dues for the month of " + payrollMonth, font12,
						Element.ALIGN_CENTER)).setBorder(0);
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable outertable = new PdfPTable(3);
		outertable.setSpacingAfter(20);
		outertable.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable.addCell(getCell("No.Of Employee", font8bold,
				Element.ALIGN_CENTER));
		outertable.addCell(getCell("Wages", font8bold, Element.ALIGN_CENTER));
		outertable.addCell(getCell("ESIC", font8, Element.ALIGN_CENTER));
		outertable.addCell(getCell(noofemployee + "", font8,Element.ALIGN_CENTER));
		
		PdfPCell wageCell=getCell(sumOfWages + "", font8, Element.ALIGN_RIGHT);
		wageCell.setPaddingRight(30);
		outertable.addCell(wageCell);
		try {
			document.add(outertable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable outertable2 = new PdfPTable(3);

		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("Dues Contribution", font8bold,
				Element.ALIGN_CENTER));
		outertable2.addCell(getCell("Paid contribution", font8bold,
				Element.ALIGN_CENTER));
		outertable2.addCell(getCell("Employee Contribution", font8,
				Element.ALIGN_CENTER));
		
		PdfPCell contrCell=getCell(sumOfContribution + "", font8,Element.ALIGN_RIGHT);
		contrCell.setPaddingRight(10);
		outertable2.addCell(contrCell);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));
		outertable2.addCell(getCell("Employer Contribution", font8,
				Element.ALIGN_CENTER));
		
		PdfPCell empcontr=getCell(sumOfEmpContribution + "", font8,
				Element.ALIGN_RIGHT);
		empcontr.setPaddingRight(10);
		outertable2.addCell(empcontr);
		outertable2.addCell(getCell("", font8, Element.ALIGN_CENTER));

		try {
			document.add(outertable2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable outertable3 = new PdfPTable(1);
		outertable3.addCell(getCell("Total ==>>    "
				+ (sumOfContribution + sumOfEmpContribution), font8bold,
				Element.ALIGN_CENTER));

		try {
			document.add(outertable3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
