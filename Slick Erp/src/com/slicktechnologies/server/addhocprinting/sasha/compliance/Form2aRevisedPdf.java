package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;
/**
 * @author Viraj
 * Date: 02-02-2019
 * Description: To show data and formating of form 2a revised pdf
 */
public class Form2aRevisedPdf {
	Logger logger=Logger.getLogger("Form2A Revised PDF");
	public Document document;
	
	Employee employee;
	int noofemployee=0;
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1= new SimpleDateFormat("MM/yyyy");
	
	Company company;
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 6);
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	
	ArrayList<EmployeeFamilyDeatails> epfList = new ArrayList<EmployeeFamilyDeatails>();
	ArrayList<EmployeeFamilyDeatails> epsList = new ArrayList<EmployeeFamilyDeatails>();
	ArrayList<EmployeeFamilyDeatails> otherList = new ArrayList<EmployeeFamilyDeatails>();
	
	public void setform2aRevData(Long count, int empId) {
		
		company=ofy().load().type(Company.class).filter("companyId", count).first().now();
		logger.log(Level.SEVERE,"empId: "+empId);
		employee=ofy().load().type(Employee.class).filter("companyId", count).filter("count",empId).first().now();
		if(employee.getNominationList() != null) {
			logger.log(Level.SEVERE,"empNominationList: "+employee.getNominationList().size());
			for(int i=0;i< employee.getNominationList().size();i++) {
				if(employee.getNominationList().get(i).getNominatedFor().equalsIgnoreCase("EPF") && employee.getNominationList().get(i).isNonFamilyMember() == false) {
					epfList.add(employee.getNominationList().get(i));
					logger.log(Level.SEVERE,"epfList: "+epfList);
				}else if(employee.getNominationList().get(i).getNominatedFor().equalsIgnoreCase("EPS") && employee.getNominationList().get(i).isNonFamilyMember() == false) {
					epsList.add(employee.getNominationList().get(i));
					logger.log(Level.SEVERE,"epsList: "+epsList);
				}else{
					otherList.add(employee.getNominationList().get(i));
					logger.log(Level.SEVERE,"OterhList: "+otherList);
				}
			}
		}
	}
	
	public void createPdf(PdfWriter writer) {
		createHeadingDetails();
		createEmployeeDetails();
		createEpfTable();
		createSignature();
		createEpsTable();
		createOtherTable();
		createCertificateByEmployer();
	}

	private void createSignature() {
		PdfPTable signTb1=new PdfPTable(4);
		signTb1.setWidthPercentage(100f);
		
		try {
			signTb1.setWidths(new float[] {5,5,85,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 4, 0)).setBorder(0);
		
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell("1", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell("*Certified that I have no family as defined in para 2(g) of the Employees Provident Fund Scheme 1952 and should I", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell("acquire a family hereafter the above nomination should be deemed as cancelled", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 4, 0)).setBorder(0);
		
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell("2", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell("*Certified that my father/mother is/are dependent upon me.", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		signTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 4, 20)).setBorder(0);
		
		PdfPTable signTb2=new PdfPTable(5);
		signTb2.setWidthPercentage(100f);
		
		try {
			signTb2.setWidths(new float[] {5,30,20,40,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell("Strike out whichever is not applicable", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell("Signature/or thumb impression", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell("of the subscriber", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		signTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		try {
			document.add(signTb1);
			document.add(signTb2);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCertificateByEmployer() {
		PdfPTable cbeTb1=new PdfPTable(5);
		cbeTb1.setWidthPercentage(100f);
		
		try {
			cbeTb1.setWidths(new float[] {5,5,50,35,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell("CERTIFICATE BY EMPLOYER", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell("Certified that the above declaration and nomination has been signed / thumb impression before me by Shri / Smt./", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell("Miss", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell empName = cell.getCell(employee.getFullname(), font7, Element.ALIGN_LEFT, 0, 0, 0);
		empName.setBorderWidthLeft(0);
		empName.setBorderWidthRight(0);
		empName.setBorderWidthTop(0);
		
		cbeTb1.addCell(empName);
		cbeTb1.addCell(cell.getCell("employed in my establishment after he/she has", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell("read the entries/the entries have been read over to him/her by me and got confirmed by him/her.", font8, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		cbeTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPTable cbeTb2=new PdfPTable(5);
		cbeTb2.setWidthPercentage(100f);
		
		try {
			cbeTb2.setWidths(new float[] {5,40,10,40,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell("Date _____________", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell("Signature of the employer or other authorised officer of the", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell("establishment", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell("Place :", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell("Name & address of the Factory/Establishment", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell("Date :", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		cbeTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(cbeTb1);
			document.add(cbeTb2);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createOtherTable() {
		PdfPTable otherTb1=new PdfPTable(5);
		otherTb1.setWidthPercentage(100f);
		
		try {
			otherTb1.setWidths(new float[] {5,40,10,40,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell("Certified that I have no family as defined in para 2 (vii) of employees's Family Pension Scheme 1995 and should I acquire a", font7, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell("family hereafter I shall furnish Particulars there on in the above form.", font7, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell("I hereby nominate the following person for receiving the monthly widow pension (admissible under para 16 2 (a) (i)&(ii) in the)", font7, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell("event of my death without leaving any eligible family member for receiving pension.", font7, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 20)).setBorder(0);
		
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 30)).setBorder(0);
		otherTb1.addCell(cell.getCell("Name and address of the nominee", font7, Element.ALIGN_CENTER, 0, 0, 30));
		otherTb1.addCell(cell.getCell("Date of Birth", font7, Element.ALIGN_CENTER, 0, 0, 30));
		otherTb1.addCell(cell.getCell("Relationship with member", font7, Element.ALIGN_CENTER, 0, 0, 30));
		otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 30)).setBorder(0);
		
		int count = 0;
		int noOfRows = 0;
		
		for(int i=0;i< otherList.size();i++) {
			count += 1;
			
			otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			String nomineeAdd;
			String nomineeName;
			if(otherList.get(i).getFullName() != null) {
				nomineeName = otherList.get(i).getFullName();
			}else {
				nomineeName = "";
			}
			if(otherList.get(i).getAddress() != null) {
				nomineeAdd = nomineeName +"/"+ otherList.get(i).getAddress();
			}else {
				nomineeAdd = nomineeName;
			}
			otherTb1.addCell(cell.getCell(nomineeAdd, font7, Element.ALIGN_CENTER, 0, 0, 0));
			otherTb1.addCell(cell.getCell(format.format(otherList.get(i).getDob()), font7, Element.ALIGN_CENTER, 0, 0, 0));
			otherTb1.addCell(cell.getCell(otherList.get(i).getFamilyRelation(), font7, Element.ALIGN_CENTER, 0, 0, 0));
			otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);	
		}
		
		noOfRows = 7 - count;
		
		if(noOfRows > 0) {
			for(int i=0;i< noOfRows;i++) {
				otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
				otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20));
				otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20));
				otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20));
				otherTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
			}
		}
		
		PdfPTable otherTb2=new PdfPTable(5);
		otherTb2.setWidthPercentage(100f);
		
		try {
			otherTb2.setWidths(new float[] {5,40,25,25,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell("Date _____________", font7, Element.ALIGN_LEFT, 0, 3, 15)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 5, 0)).setBorder(0);
		
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell("Signature or thumb impression", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell("of the subscriber", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		PdfPCell blkCell = cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 3, 0);
		blkCell.setBorderWidthBottom(1);
		blkCell.setBorderWidthLeft(0);
		blkCell.setBorderWidthRight(0);
		blkCell.setBorderWidthTop(0);
		otherTb2.addCell(blkCell);
		otherTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(otherTb1);
			document.add(otherTb2);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createEpsTable() {
		PdfPTable epsTb1=new PdfPTable(6);
		epsTb1.setWidthPercentage(100f);
		
		try {
			epsTb1.setWidths(new float[] {5,10,30,10,30,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell("PART-(EPS)", font8bold, Element.ALIGN_CENTER, 0, 4, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell("Para 18", font7, Element.ALIGN_CENTER, 0, 4, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell("I hereby furnish below particulars of the members of my family who would be eligible to receive Widow/Children Pension in the", font7, Element.ALIGN_LEFT, 0, 4, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell("event of my premature death in service", font7, Element.ALIGN_LEFT, 0, 4, 0)).setBorder(0);
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 6, 0)).setBorder(0);
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 30)).setBorder(0);
		epsTb1.addCell(cell.getCell("Sr. No", font7, Element.ALIGN_CENTER, 0, 0, 30));
		epsTb1.addCell(cell.getCell("Name & Address of the Family Member", font7, Element.ALIGN_CENTER, 0, 0, 30));
		epsTb1.addCell(cell.getCell("Age", font7, Element.ALIGN_CENTER, 0, 0, 30));
		epsTb1.addCell(cell.getCell("Relationship with the member", font7, Element.ALIGN_CENTER, 0, 0, 30));
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 30)).setBorder(0);
		
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 17)).setBorder(0);
		epsTb1.addCell(cell.getCell("(1)", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epsTb1.addCell(cell.getCell("(2)", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epsTb1.addCell(cell.getCell("(3)", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epsTb1.addCell(cell.getCell("(4)", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
		
		int count = 0;
		int noOfRows = 0;
		
		for(int i=0;i< epsList.size();i++) {
			count += 1;
			
			epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			epsTb1.addCell(cell.getCell(count+"", font7, Element.ALIGN_CENTER, 0, 0, 0));
			
			String nomineeAdd;
			String nomineeName;
			if(epsList.get(i).getFullName() != null) {
				nomineeName = epsList.get(i).getFullName();
			}else {
				nomineeName = "";
			}
			if(epsList.get(i).getAddress() != null) {
				nomineeAdd = nomineeName +"/"+ epsList.get(i).getAddress();
			}else {
				nomineeAdd = nomineeName;
			}
			System.out.println("nomineeName: "+ epsList.get(i).getGuardianName() +"nomineeAdd: "+ epsList.get(i).getGuardianAddress());
			System.out.println("nomineeName1: "+ epsList.get(i).getFullName() +"nomineeAdd1: "+ epsList.get(i).getAddress());
			epsTb1.addCell(cell.getCell(nomineeAdd, font7, Element.ALIGN_CENTER, 0, 0, 0));
			int age = 0;
			Date cDate = null;
			try {
				cDate = format.parse(format.format(new Date()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			age = cDate.getYear()-epsList.get(i).getDob().getYear();
			if(age != 0) {
				epsTb1.addCell(cell.getCell(age+"", font7, Element.ALIGN_CENTER, 0, 0, 0));
			} else {
				epsTb1.addCell(cell.getCell("", font7, Element.ALIGN_CENTER, 0, 0, 0));
			}
			epsTb1.addCell(cell.getCell(epsList.get(i).getFamilyRelation(), font7, Element.ALIGN_CENTER, 0, 0, 0));
			epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		noOfRows = 7 - count;
		
		if(noOfRows > 0) {
			for(int i=0;i< noOfRows;i++) {
				epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
				epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epsTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
			}
		}
		
		try {
			document.add(epsTb1);
			document.newPage();
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createEpfTable() {
		PdfPTable epfTb1=new PdfPTable(8);
		epfTb1.setWidthPercentage(100f);
		
		try {
			epfTb1.setWidths(new float[] {5,15,15,15,11,17,17,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell("PART-A(EPF)", font8bold, Element.ALIGN_CENTER, 0, 6, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell("I hereby nominate the person(s)/cancel the nomination made by me previously and nominate the person(s) mentioned below", font7, Element.ALIGN_LEFT, 0, 6, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell("to receive the amount standing to my credit in  the Employees Provident Fund,in the event of my death.", font7, Element.ALIGN_LEFT, 0, 6, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		epfTb1.addCell(cell.getCell("Name of the Nominee(s)", font7, Element.ALIGN_CENTER, 0, 0, 0));
		epfTb1.addCell(cell.getCell("Address", font7, Element.ALIGN_CENTER, 0, 0, 0));
		epfTb1.addCell(cell.getCell("Nominee(s) relationship with the member", font7, Element.ALIGN_CENTER, 0, 0, 0));
		epfTb1.addCell(cell.getCell("Date of Birth", font7, Element.ALIGN_CENTER, 0, 0, 0));
		epfTb1.addCell(cell.getCell("Total amount or share of accumulations in Provident Funds to be paid to each nominee", font7, Element.ALIGN_CENTER, 0, 0, 0));
		epfTb1.addCell(cell.getCell("If the nominee is minor name and address of the guardian who may receive the amount during the minority of the nominee", font7, Element.ALIGN_CENTER, 0, 0, 0));
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
		epfTb1.addCell(cell.getCell("1", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epfTb1.addCell(cell.getCell("2", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epfTb1.addCell(cell.getCell("3", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epfTb1.addCell(cell.getCell("4", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epfTb1.addCell(cell.getCell("5", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epfTb1.addCell(cell.getCell("6", font7, Element.ALIGN_CENTER, 0, 0, 17));
		epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
		
		int count = 0;
		int noOfRows = 0;
		
		for(int i=0;i< epfList.size();i++) {
			count += 1;
			
			epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			epfTb1.addCell(cell.getCell(epfList.get(i).getFullName(), font7, Element.ALIGN_CENTER, 0, 0, 0));
			epfTb1.addCell(cell.getCell(epfList.get(i).getAddress(), font7, Element.ALIGN_CENTER, 0, 0, 0));
			epfTb1.addCell(cell.getCell(epfList.get(i).getFamilyRelation(), font7, Element.ALIGN_CENTER, 0, 0, 0));
			epfTb1.addCell(cell.getCell(format.format(epfList.get(i).getDob()), font7, Element.ALIGN_CENTER, 0, 0, 0));
			epfTb1.addCell(cell.getCell(epfList.get(i).getNomineeShare()+"%", font7, Element.ALIGN_CENTER, 0, 0, 0));
			
			String nomineeAdd;
			String nomineeName;
			if(epfList.get(i).getGuardianName() != null) {
				nomineeName = epfList.get(i).getGuardianName();
			}else {
				nomineeName = "";
			}
			if(epfList.get(i).getGuardianAddress() != null) {
				nomineeAdd = nomineeName +"/"+ epfList.get(i).getGuardianAddress();
			}else {
				nomineeAdd = nomineeName;
			}
			epfTb1.addCell(cell.getCell(nomineeAdd, font7, Element.ALIGN_CENTER, 0, 0, 0));
			epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		noOfRows = 7 - count;
		
		if(noOfRows > 0) {
			for(int i=0;i< noOfRows;i++) {
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17));
				epfTb1.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 17)).setBorder(0);
			}
		}
		
		try {
			document.add(epfTb1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createEmployeeDetails() {
		PdfPTable empTbl=new PdfPTable(4);
		empTbl.setWidthPercentage(100f);
		
		try {
			empTbl.setWidths(new float[] {5,20,70,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		empTbl.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		empTbl.addCell(cell.getCell("1.Name(IN BLOCK LETTERS):", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		PdfPCell empName = cell.getCell(employee.getFullname().toUpperCase(), font7, Element.ALIGN_LEFT, 0, 0, 15);
		empName.setBorderWidthBottom(1);
		empName.setBorderWidthLeft(0);
		empName.setBorderWidthRight(0);
		empName.setBorderWidthTop(0);
		empTbl.addCell(empName);
		
		empTbl.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		PdfPTable empTb2=new PdfPTable(6);
		empTb2.setWidthPercentage(100f);
		
		try {
			empTb2.setWidths(new float[] {5,20,25,20,25,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		empTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		empTb2.addCell(cell.getCell("2.Date of Birth", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		PdfPCell empDOB = cell.getCell(format.format(employee.getDob())+"", font7, Element.ALIGN_LEFT, 0, 0, 15);
		empDOB.setBorderWidthBottom(1);
		empDOB.setBorderWidthLeft(0);
		empDOB.setBorderWidthRight(0);
		empDOB.setBorderWidthTop(0);
		empTb2.addCell(empDOB);
		
		empTb2.addCell(cell.getCell("3.Account No", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		PdfPCell empAccNo = cell.getCell(employee.getAccountNo(), font7, Element.ALIGN_LEFT, 0, 0, 15);
		empAccNo.setBorderWidthBottom(1);
		empAccNo.setBorderWidthLeft(0);
		empAccNo.setBorderWidthRight(0);
		empAccNo.setBorderWidthTop(0);
		empTb2.addCell(empAccNo);
		
		empTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		empTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		empTb2.addCell(cell.getCell("4.*Sex: MALE/FEMALE:", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		PdfPCell empGender = cell.getCell(employee.getGender(), font7, Element.ALIGN_LEFT, 0, 0, 15);
		empGender.setBorderWidthBottom(1);
		empGender.setBorderWidthLeft(0);
		empGender.setBorderWidthRight(0);
		empGender.setBorderWidthTop(0);
		empTb2.addCell(empGender);
		
		empTb2.addCell(cell.getCell("5.Marital Status", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		PdfPCell empMaritalStatus = cell.getCell(employee.getMaritalStatus(), font7, Element.ALIGN_LEFT, 0, 0, 15);
		empMaritalStatus.setBorderWidthBottom(1);
		empMaritalStatus.setBorderWidthLeft(0);
		empMaritalStatus.setBorderWidthRight(0);
		empMaritalStatus.setBorderWidthTop(0);
		empTb2.addCell(empMaritalStatus);
		
		empTb2.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 15)).setBorder(0);
		
		
		PdfPTable empTb3=new PdfPTable(4);
		empTb3.setWidthPercentage(100f);
		
		try {
			empTb3.setWidths(new float[] {5,20,70,5});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		empTb3.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		empTb3.addCell(cell.getCell("6.Address Permanent/Temporary:", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell empAddress = cell.getCell(employee.getAddress().getCompleteAddress(), font7, Element.ALIGN_LEFT, 0, 0, 0);
		empAddress.setBorderWidthLeft(0);
		empAddress.setBorderWidthRight(0);
		empAddress.setBorderWidthTop(0);
		empTb3.addCell(empAddress);
		
		empTb3.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		empTb3.addCell(cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 4, 0)).setBorder(0);
		try {
			document.add(empTbl);
			document.add(empTb2);
			document.add(empTb3);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createHeadingDetails() {
		PdfPTable outerTbl=new PdfPTable(3);
		outerTbl.setWidthPercentage(100f);
		
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(new float[] {5,90,5});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("(FORM 2 REVISED)", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("NOMINATION AND DECLARATION FORM FOR UNEXEMPTED/EXEMPTED ESTABLISHMENTS", font9bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Declaration and Nomination Form under the Employees Provident Funds and Employees Pension Schemes", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("(Paragraph 33 and 61(1) of the Employees Provident Fund Scheme 1952 and Paragraph 18 of the Employees", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Pension Scheme 1995)", font7, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 15)).setBorder(0);
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
}
