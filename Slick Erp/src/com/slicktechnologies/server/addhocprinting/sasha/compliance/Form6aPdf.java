package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class Form6aPdf {
public Document document;
Logger logger=Logger.getLogger("Form6A PDF");
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1= new SimpleDateFormat("MMM");
	CsvWriter csvWriter=new CsvWriter();
	List<PaySlip> paySlipList;
	ArrayList<String> monthList=new ArrayList<String>();
	Company company;
	int totalwages=0;
	int totalamountofworker=0;
	int totalEPFdiff1=0;
	int totalpensionfund=0;
	int totalmonthly_PF=0;
	String month=null;
	int totalpfwages=0;
	int epfcontribution=0;
	int pensionfundcontri=0;
	int noOfLines = 30;
	int loopCount = 0;
	protected BaseFont baseFont;
    private PdfTemplate totalPages;
    private float footerTextSize = 8f;
    private int pageNumberAlignment = Element.ALIGN_CENTER;
 
    EmploymentCardPdf cell=new EmploymentCardPdf();
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11bold=new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	Date fromDate=null;
	Date toDate=null;
	
	
	
	
  public void createPdf(PdfWriter writer){
	  int totalPages=(paySlipList.size()/3)+1;
	  int currentpage=1;
	  createFormHeading();
	  companyDetails();
  	  employeeDetails(3, loopCount);
  	  writeFooterTable(writer,totalPages,currentpage);
  	  while (noOfLines == 0 && loopCount < paySlipList.size()) {
  		  currentpage++;
  		  Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
  		  try {
  			  document.add(nextpage);
  		  } catch (DocumentException e1) {
  			  e1.printStackTrace();
  		  }
  		  employeeDetails(3, loopCount);
  		 
  		  writeFooterTable(writer,totalPages,currentpage);
  	  }
  	  currentpage++;
  	  footerTable();
  	  writeFooterTable(writer,totalPages,currentpage);
}
	
  public void setform6adata(Long companyId,String fromDate,String toDate) {
	Date fromDt=null;
	Date toDt=null;
	try {
		this.fromDate=format.parse(fromDate);
		this.toDate=format.parse(toDate);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	fromDt=DateUtility.getDateWithTimeZone("IST", this.fromDate);
	toDt=DateUtility.getDateWithTimeZone("IST", this.toDate);
//	logger.log(Level.SEVERE,"FORM : "+fmt.format(this.fromDate)+" TO : "+fmt.format(this.toDate)+" comp Id : "+companyId+" Emp Id : "+empId);
	

	
	while(fromDt.before(toDt)||fromDt.equals(toDt)){
		monthList.add(fmt.format(fromDt));
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(fromDt);
		cal.add(Calendar.MONTH, 1);
		fromDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
	}
	logger.log(Level.SEVERE,"MONTH LIST : "+monthList);
	
	if(monthList.size()!=0){
		paySlipList=ofy().load().type(PaySlip.class).filter("companyId",companyId).filter("salaryPeriod IN", monthList).list();
	}
	if(paySlipList!=null){
		System.out.println("PaySlip : "+paySlipList.size());
		logger.log(Level.SEVERE,"PaySlip : "+paySlipList.size());
	}
	System.out.println("DONE");
	company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
	//paySlipList = CsvWriter.statuteryReport;
//	System.out.println("list size :" + paySlipList.size());
}

 private void createFormHeading(){
	
	PdfPTable outerTbl=new PdfPTable(3);
	outerTbl.setWidthPercentage(100f);
	try {
		outerTbl.setWidths(new float[] {15,70,15});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	};
	
	outerTbl.addCell(cell.getCell("", font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
	outerTbl.addCell(cell.getCell("Form - 6A", font12bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	
	outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	outerTbl.addCell(cell.getCell("THE EMPLOYEE'S PROVIDENT FUND SCHEME, 1952", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	
	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0); 
	outerTbl.addCell(cell.getCell("THE EMPLOYEE'S PENSION SCHEME, 1995", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	
	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0); 
	outerTbl.addCell(cell.getCell("Paragraph 20 (4)", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	
//	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0); 
//	outerTbl.addCell(cell.getCell("Annual Statement of Contribution for the Currency period from : "+format.format(fromDate)+" To "+format.format(toDate)+"TO"+format.format(toDate), font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//	outerTbl.addCell(cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//	
	
	PdfPTable outerTb2=new PdfPTable(1);
	outerTb2.setWidthPercentage(100f);
	outerTb2.addCell(cell.getCell("Annual Statement of Contribution for the Currency period from : "+format.format(fromDate)+" To "+format.format(toDate), font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	
	
	
	
	
	try {
		document.add(outerTbl);
		document.add(outerTb2);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
		
}
	

    private void companyDetails() {
	PdfPTable outertable=new PdfPTable(2);
	
	outertable.setWidthPercentage(100f);
	try {
		outertable.setWidths(new float[] {40,60});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	outertable.setSpacingBefore(5f);
	String cinval = "";

	for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {

		if (company.getArticleTypeDetails().get(i).getArticleTypeName()
				.contains("PF Code")) {

			cinval = company.getArticleTypeDetails().get(i)
					.getArticleTypeValue();
		}

	}

	
	outertable.addCell(cell.getCell("Name of the Factory / Establishment", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell(company.getBusinessUnitName().toUpperCase()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	outertable.addCell(cell.getCell("Address", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell(company.getAddress().getCompleteAddress().toUpperCase()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	outertable.addCell(cell.getCell("PF Local Office ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell(company.getPfOfficeAt()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	outertable.addCell(cell.getCell("Code number of the establishment ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell(cinval, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	outertable.addCell(cell.getCell("PF Group No. ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell(company.getPfGroupNum()+" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	outertable.addCell(cell.getCell("Statutory rate of PF contribution", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell("12.00% ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	outertable.addCell(cell.getCell("Number of members voluntarily contributing at higher rate ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	outertable.addCell(cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
	
	try {
		document.add(outertable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

  private void employeeDetails(int i, int loopC) {
	
	PdfPTable outertable=new PdfPTable(10);
	outertable.setWidthPercentage(100f);
	try {
		outertable.setWidths(new float[] {4,17,27,10,7,7,7,7,5,7});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	outertable.setSpacingBefore(5f);
	
	   PdfPCell srNo=cell.getCell("Sr.No", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	   PdfPCell accno=cell.getCell("Account Number ", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	   
	    PdfPCell empname=cell.getCell("Name of the member", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	    
	    PdfPCell wages=cell.getCell("Wages retaining Allowances (if any) and D.A including cash value of food concession paid during the currency", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	   
	    PdfPCell amountofworker=cell.getCell(" Amount of worker's contribution from the wages EPF", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	   
	    PdfPCell empcontri=cell.getCell("Employers Contribution", font9bold, Element.ALIGN_CENTER, 1, 2, 0);
	   
	    PdfPCell EPFdiff=cell.getCell("EPF Difference between 12.00 % & 8.33%", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
	   
	    PdfPCell pensionfund=cell.getCell("Pension Fund 8.33%", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
	    
	    PdfPCell refund=cell.getCell("Refund of Advance ", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	    
	    PdfPCell rate=cell.getCell(" Rate of higher voluntary contribution", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	    
	    PdfPCell remark=cell.getCell("Remark", font9bold, Element.ALIGN_CENTER, 2, 0, 0);
	   
	    outertable.addCell(srNo);
	    outertable.addCell(accno);
	    outertable.addCell(empname);
	    outertable.addCell(wages);
	    outertable.addCell(amountofworker);
	    outertable.addCell(empcontri);
	    outertable.addCell(refund);
	    outertable.addCell(rate);
	    outertable.addCell(remark);
	    outertable.addCell(EPFdiff);
	    outertable.addCell(pensionfund);
	
	    PdfPCell cell1=cell.getCell("(1)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell2=cell.getCell("(2)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell3=cell.getCell("(3)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		
		PdfPCell cell4=cell.getCell("(4)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell5=cell.getCell("(5)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell6=cell.getCell("(6)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell7=cell.getCell("(7)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell8=cell.getCell("(8)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell9=cell.getCell("(9)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		PdfPCell cell10=cell.getCell("(10)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		
		outertable.addCell(cell1);
		outertable.addCell(cell2);
		outertable.addCell(cell3);
		outertable.addCell(cell4);
		outertable.addCell(cell5);
		outertable.addCell(cell6);
		outertable.addCell(cell7);
		outertable.addCell(cell8);
		outertable.addCell(cell9);
		outertable.addCell(cell10);
		
		System.out.println("paysliplist size"+paySlipList.size());
		
		for (int j = loopC; j < paySlipList.size(); j++) {
			PaySlip payslip=paySlipList.get(j);
			System.out.println("paysliplist size"+paySlipList.get(j));
			if (noOfLines == 0) {
				loopCount = j;
				break;
			}

			noOfLines--;
			
			/**
			 * @author Anil,Date : 01-07-2019
			 * picking pf wage amount from database 
			 * if not present then doing reverse calculation with the help of pf percent and pf amount
			 */
//			double pfWages = csvWriter.getPFwages(payslip.getEarningList());
			double pfWages = csvWriter.getPFwages(payslip,true,false);
//			double empWages = Math.round((pfWages * 12) / 100);
			double empWages=csvWriter.getPFwages(payslip,false,true);
			double emprWages = Math.round((pfWages * 3.67) / 100);
			double pensionCont = Math.round((pfWages * 8.33) / 100);
			
			totalwages+=payslip.getPFAmount();
			totalamountofworker+=empWages;
			totalEPFdiff1+=emprWages;
			totalpensionfund+=pensionCont;
			
			
		
		
	
		  PdfPCell srNo1=cell.getCell((j+1)+"", font8, Element.ALIGN_CENTER, 2, 0, 0);
		   PdfPCell accno1=cell.getCell("MH/BAN"+payslip.getPfNumber(), font8, Element.ALIGN_LEFT, 2, 0, 0);
		   
		    PdfPCell empname1=cell.getCell(payslip.getEmployeeName()+"", font8, Element.ALIGN_LEFT, 2, 0, 0);
		    
		    PdfPCell wages1=cell.getCell(df1.format(payslip.getPFAmount())+"", font8, Element.ALIGN_RIGHT, 2, 0, 0);
		   
		    PdfPCell amountofworker1=cell.getCell(df1.format(empWages)+" ", font8, Element.ALIGN_RIGHT, 2, 0, 0);
		   
		    PdfPCell empcontri1=cell.getCell("", font8, Element.ALIGN_RIGHT, 1, 2, 0);
		   
		    PdfPCell EPFdiff1=cell.getCell(df1.format(emprWages)+"", font8, Element.ALIGN_RIGHT, 0, 0, 0);
		   
		    PdfPCell pensionfund1=cell.getCell(df1.format(pensionCont)+"", font8, Element.ALIGN_RIGHT, 0, 0, 0);
		    
		    PdfPCell refund1=cell.getCell(" ", font8, Element.ALIGN_RIGHT, 2, 0, 0);
		    
		    PdfPCell rate1=cell.getCell("", font8, Element.ALIGN_RIGHT, 2, 0, 0);
		    
		    PdfPCell remark1=cell.getCell("", font8, Element.ALIGN_RIGHT, 2, 0, 0);
		   
		    outertable.addCell(srNo1);
		    outertable.addCell(accno1);
		    outertable.addCell(empname1);
		    outertable.addCell(wages1);
		    outertable.addCell(amountofworker1);
		    outertable.addCell(empcontri1);
		    outertable.addCell(refund1);
		    outertable.addCell(rate1);
		    outertable.addCell(remark1);
		    outertable.addCell(EPFdiff1);
		    outertable.addCell(pensionfund1);

		}
		
		PdfPTable outertable2=new PdfPTable(8);
		outertable2.setWidthPercentage(100f);
		try {
			outertable2.setWidths(new float[] {48,10,7,7,7,7,5,7});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		      
			 
			    PdfPCell empname2=cell.getCell("Total", font9, Element.ALIGN_CENTER, 2, 0, 0);
			    
			    PdfPCell wages2=cell.getCell(totalwages+"", font9, Element.ALIGN_RIGHT, 2, 0, 0);
			   
			    PdfPCell amountofworker2=cell.getCell(totalamountofworker+" ", font9, Element.ALIGN_RIGHT, 2, 0, 0);
			   
			    PdfPCell empcontri2=cell.getCell("", font9, Element.ALIGN_CENTER, 1, 2, 0);
			   
			    PdfPCell EPFdiff2=cell.getCell(totalEPFdiff1+"", font9, Element.ALIGN_RIGHT, 0, 0, 0);
			   
			    PdfPCell pensionfund2=cell.getCell(totalpensionfund+"", font9, Element.ALIGN_RIGHT, 0, 0, 0);
			    
			    PdfPCell refund2=cell.getCell(" ", font9, Element.ALIGN_CENTER, 2, 0, 0);
			    
			    PdfPCell rate2=cell.getCell(" ", font9, Element.ALIGN_CENTER, 2, 0, 0);
			    
			    PdfPCell remark2=cell.getCell("", font9, Element.ALIGN_CENTER, 2, 0, 0);
			    
			    outertable2.addCell(empname2);
			    outertable2.addCell(wages2);
			    outertable2.addCell(amountofworker2);
			    outertable2.addCell(empcontri2);
			    outertable2.addCell(refund2);
			    outertable2.addCell(rate2);
			    outertable2.addCell(remark2);
			    outertable2.addCell(EPFdiff2);
			    outertable2.addCell(pensionfund2);
			    
			    try {
					document.add(outertable);
					document.add(outertable2);
					document.newPage();
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			
  }
  
  public double getPfContributionOfMonth(String month, List<PaySlip> paySlipList){
    	
    	double sum=0;
    	for(PaySlip slip :paySlipList){
    		if(slip.getSalaryPeriod().equals(month)){
    			sum=sum+slip.getPFAmount();
    			
    		}
    	}
    	
    	return sum;
    }
	

  private void footerTable(){
	  
	        PdfPTable outertable1=new PdfPTable(8);
			outertable1.setWidthPercentage(100f);
//			try {
//				outertable1.setWidths(new float[] {50,50});
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			outertable1.setSpacingBefore(20f);
			
			 PdfPCell MONTH4=cell.getCell("  ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 MONTH4.setBorder(0);
			 
			 PdfPCell totalpf4=cell.getCell("  ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 totalpf4.setBorder(0);
			 
			 PdfPCell epfcontr4=cell.getCell("  ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 epfcontr4.setBorder(0);
			 
			 PdfPCell pebsionfund4=cell.getCell("   ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 pebsionfund4.setBorder(0);
			 
			 PdfPCell edli4=cell.getCell("  ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 edli4.setBorder(0);
			 
			 PdfPCell admcharge4=cell.getCell("   ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 admcharge4.setBorder(0);
			 
			 PdfPCell edliadm4=cell.getCell("   ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 edliadm4.setBorder(0);
			 
			 PdfPCell dop4=cell.getCell("   ", font9bold, Element.ALIGN_CENTER, 0, 0, 30);
			 dop4.setBorder(0);
	  
			 outertable1.addCell(MONTH4);
			 outertable1.addCell(totalpf4);
			 outertable1.addCell(epfcontr4);
			 outertable1.addCell(pebsionfund4);
			 outertable1.addCell(edli4);
			 outertable1.addCell(admcharge4);
			 outertable1.addCell(edliadm4);
			 outertable1.addCell(dop4);
			
			
			
			
			PdfPCell MONTH=cell.getCell("Month/Year", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 MONTH.setBorderWidthBottom(0);
			 
			 PdfPCell totalpf=cell.getCell("Total PF Wages", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 totalpf.setBorderWidthBottom(0);
			 
			 PdfPCell epfcontr=cell.getCell("EPF Contribution ", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 epfcontr.setBorderWidthBottom(0);
			 
			 PdfPCell pebsionfund=cell.getCell("Pension Fund Contribution ", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 pebsionfund.setBorderWidthBottom(0);
			 
			 PdfPCell edli=cell.getCell("EDLI Contribution ", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 edli.setBorderWidthBottom(0);
			 
			 PdfPCell admcharge=cell.getCell("Adm.Charges ", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 admcharge.setBorderWidthBottom(0);
			 
			 PdfPCell edliadm=cell.getCell("EDLI Adm. Charges 0.01 %", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 edliadm.setBorderWidthBottom(0);
			 
			 PdfPCell dop=cell.getCell("Date of Payment", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 dop.setBorderWidthBottom(0);
	  
			 outertable1.addCell(MONTH);
			 outertable1.addCell(totalpf);
			 outertable1.addCell(epfcontr);
			 outertable1.addCell(pebsionfund);
			 outertable1.addCell(edli);
			 outertable1.addCell(admcharge);
			 outertable1.addCell(edliadm);
			 outertable1.addCell(dop);
			 
			 PdfPCell MONTH2=cell.getCell("", font10, Element.ALIGN_CENTER, 0, 0, 0);
			 MONTH2.setBorderWidthTop(0);
			 
			 PdfPCell totalpf2=cell.getCell("", font10, Element.ALIGN_RIGHT, 0, 0, 0);
			 totalpf2.setBorderWidthTop(0);
			 
			 PdfPCell epfcontr2=cell.getCell("A/c No.1", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 epfcontr2.setBorderWidthTop(0);
			 
			 PdfPCell pebsionfund2=cell.getCell("A/c No.10", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 pebsionfund2.setBorderWidthTop(0);
			 
			 PdfPCell edli2=cell.getCell("A/c No.21", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 edli2.setBorderWidthTop(0);
			 
			 PdfPCell admcharge2=cell.getCell("A/c No.2", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 admcharge2.setBorderWidthTop(0);
			 
			 PdfPCell edliadm2=cell.getCell("", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 edliadm2.setBorderWidthTop(0);
			 
			 PdfPCell dop2=cell.getCell("", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			 dop2.setBorderWidthTop(0);
			 outertable1.addCell(MONTH2);
			 outertable1.addCell(totalpf2);
			 outertable1.addCell(epfcontr2);
			 outertable1.addCell(pebsionfund2);
			 outertable1.addCell(edli2);
			 outertable1.addCell(admcharge2);
			 outertable1.addCell(edliadm2);
			 outertable1.addCell(dop2);
			 
	
			
             for(String month : monthList){
			 
			 double monthly_PF=getPfContributionOfMonth(month,paySlipList);
			 totalmonthly_PF+=monthly_PF;
			 
			 String month1="";
				if (paySlipList.size() > 0) {
					try {
						month1 = fmt1.format(fmt.parse(month));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			
				
				
			 
		     PdfPCell MONTH1=cell.getCell(month1+"", font10, Element.ALIGN_LEFT, 2, 0, 0);
			 PdfPCell totalpf1=cell.getCell(df1.format(monthly_PF)+"", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell epfcontr1=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell pebsionfund1=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell edli1=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell admcharge1=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell edliadm1=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell dop1=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
	  
			 outertable1.addCell(MONTH1);
			 outertable1.addCell(totalpf1);
			 outertable1.addCell(epfcontr1);
			 outertable1.addCell(pebsionfund1);
			 outertable1.addCell(edli1);
			 outertable1.addCell(admcharge1);
			 outertable1.addCell(edliadm1);
			 outertable1.addCell(dop1);
			 
			 }
			 PdfPCell MONTH3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
			 PdfPCell totalpf3=cell.getCell(totalmonthly_PF+"", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell epfcontr3=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell pebsionfund3=cell.getCell("", font10, Element.ALIGN_RIGHT, 2, 0, 0);
			 PdfPCell edli3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
			 PdfPCell admcharge3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
			 PdfPCell edliadm3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
			 PdfPCell dop3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
	  
			 outertable1.addCell(MONTH3);
			 outertable1.addCell(totalpf3);
			 outertable1.addCell(epfcontr3);
			 outertable1.addCell(pebsionfund3);
			 outertable1.addCell(edli3);
			 outertable1.addCell(admcharge3);
			 outertable1.addCell(edliadm3);
			 outertable1.addCell(dop3);
	  
	  
	    PdfPTable outertable=new PdfPTable(2);
		outertable.setWidthPercentage(100f);
		try {
			outertable.setWidths(new float[] {50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		outertable.setSpacingBefore(10f);
		outertable.addCell(cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 30)).setBorder(0);
		outertable.addCell(cell.getCell("For "+company.getBusinessUnitName().toUpperCase(), font8bold, Element.ALIGN_RIGHT, 0, 0, 30)).setBorder(0);
		
		outertable.addCell(cell.getCell("", font8bold, Element.ALIGN_RIGHT, 0, 0, 50)).setBorder(0);
		outertable.addCell(cell.getCell("DIRECTOR", font8bold, Element.ALIGN_RIGHT, 0, 0, 50)).setBorder(0);
		
		
		 try {
			    document.add(outertable1);
				document.add(outertable);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
  }
  
  
       
    private void writeFooterTable(PdfWriter writer,int totalPages,int currentPage) {
    	
    	PdfPTable table=new PdfPTable(1);
    	table.setWidthPercentage(100f);
    	table.addCell(cell.getCell("Page "+currentPage+" of "+totalPages, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
    	
        final int FIRST_ROW = 0;
        final int LAST_ROW = -1;
        //Table must have absolute width set.
        if(table.getTotalWidth()==0)
            table.setTotalWidth((document.right()-document.left())*table.getWidthPercentage()/100f);
        table.writeSelectedRows(FIRST_ROW, LAST_ROW, document.left(), document.bottom()+table.getTotalHeight(),writer.getDirectContent());
        try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
   

}
