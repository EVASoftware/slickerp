package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;
import org.apache.tools.ant.Project;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class FormA {
	public Document document;
	Company company;
	Employee employee;
	Branch branch;
	Project project;
	List<Employee> employeeList;
	int noofemployee = 0;
	int noOfLines = 0;
	int loopCount = 0;
	Logger logger = Logger.getLogger("FormF PDF");
	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1 = new SimpleDateFormat("MMM");
	EmploymentCardPdf cell = new EmploymentCardPdf();
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font7boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD| Font.UNDERLINE);
	Font font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 6);

	public FormA() {
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void createPdf() {
		formHeading();
		EmployeeInfoTable(14, loopCount);

		while (noOfLines == 0 && loopCount < employeeList.size()-1) {

			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			EmployeeInfoTable(14, loopCount);

		}
	}

	public void setFormAdata(Long companyId, int empId, String branch,
			String project) {

		if (empId != -1 && branch != null && project != null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("count", empId).filter("branchName", branch)
					.filter("projectName", project).list();
		} else if (empId != -1 && branch == null && project == null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("count", empId).list();

		} else if (empId != -1 && branch != null && project == null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("count", empId).filter("branchName", branch).list();

		} else if (empId != -1 && branch == null && project != null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("count", empId).filter("projectName", project)
					.list();

		} else if (empId == -1 && branch != null && project != null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("branchName", branch)
					.filter("projectName", project).list();

		} else if (empId == -1 && branch != null && project == null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("branchName", branch).list();

		} else if (empId == -1 && branch == null && project != null) {
			employeeList = ofy().load().type(Employee.class)
					.filter("companyId", companyId).filter("status", true)
					.filter("projectName", project).list();

		}
		// else if(empId==-1&&branch==null&&project==null){
		//
		// }

		// employee =
		// ofy().load().type(Employee.class).filter("companyId",companyId).filter("count",empId).first().now();
		company = ofy().load().type(Company.class)
				.filter("companyId", companyId).first().now();
		// if(employee.getBranchName()!=null){
		//
		// }
		// employeeList=ofy().load().type(Employee.class).filter("companyId",
		// companyId).filter("status",true).filter("branchName",
		// employee.getBranchName())
		// .filter("projectName",employee.getProjectName()).list();
	}

	private void formHeading() {

		PdfPTable outerTb1 = new PdfPTable(3);
		outerTb1.setWidthPercentage(100f);
		// try {
		// outerTb1.setWidths(new float[] {20,60,20});
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		PdfPCell blank = cell.getCell("", font8bold, Element.ALIGN_CENTER, 0,
				0, 0);
		blank.setBorder(0);
		PdfPCell sched = cell.getCell("Schedule", font7bold,
				Element.ALIGN_CENTER, 0, 0, 0);
		sched.setBorder(0);
		PdfPCell cmpnyname = null;
		if (company != null) {
			cmpnyname = cell.getCell(
					"Name of the Establishment : "
							+ company.getBusinessUnitName(), font7,
					Element.ALIGN_LEFT, 0, 0, 0);
		} else {
			cmpnyname = cell.getCell("Name of the Establishment : " + "",
					font7, Element.ALIGN_LEFT, 0, 0, 0);
		}
		cmpnyname.setBorder(0);
		PdfPCell rule = cell.getCell("[See rule 2(1)]", font7bold,
				Element.ALIGN_CENTER, 0, 0, 0);
		rule.setBorder(0);
		PdfPCell forma = cell.getCell("FORM A", font10bold,
				Element.ALIGN_CENTER, 0, 0, 0);
		forma.setBorder(0);
		PdfPCell format = cell.getCell("Format Of Employee Register ",
				font7bold, Element.ALIGN_CENTER, 0, 0, 0);
		format.setBorder(0);
		PdfPCell partA = cell.getCell("[Part-A: For All Establishments]",
				font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		partA.setBorder(0);

		outerTb1.addCell(blank);
		outerTb1.addCell(sched);
		outerTb1.addCell(blank);
		outerTb1.addCell(cmpnyname);
		outerTb1.addCell(rule);
		outerTb1.addCell(blank);
		outerTb1.addCell(blank);
		outerTb1.addCell(forma);
		outerTb1.addCell(blank);
		outerTb1.addCell(blank);
		outerTb1.addCell(format);
		outerTb1.addCell(blank);
		outerTb1.addCell(blank);
		outerTb1.addCell(partA);
		outerTb1.addCell(blank);

		PdfPTable outerTb2 = new PdfPTable(3);
		outerTb2.setWidthPercentage(100f);
		try {
			outerTb2.setWidths(new float[] { 10, 10, 80 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell blank1 = cell.getCell("", font8bold, Element.ALIGN_CENTER, 0,
				0, 0);
		blank1.setBorder(0);
		PdfPCell nameofowner = cell.getCell("Name of Owner", font7,
				Element.ALIGN_LEFT, 0, 0, 0);
		nameofowner.setBorder(0);
		PdfPCell lin = cell.getCell("LIN", font7, Element.ALIGN_LEFT, 0, 0, 0);
		lin.setBorder(0);

		outerTb2.addCell(blank1);
		outerTb2.addCell(nameofowner);
		outerTb2.addCell(lin);
		try {
			document.add(outerTb1);
			document.add(outerTb2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void EmployeeInfoTable(int count, int loopC) {
		noOfLines = count;
		PdfPTable outerTb1 = new PdfPTable(31);
		outerTb1.setWidthPercentage(100f);
		try {
			outerTb1.setWidths(new float[] { 3, 4, 3, 3, 3, 4, 3, 3, 3, 3, 3,
					6, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 5, 3, 3, 3 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell cell1 = cell.getCell("Sr. No. ", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell1.setVerticalAlignment(Element.ALIGN_CENTER);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell2 = cell.getCell("Employee Code ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell2.setVerticalAlignment(Element.ALIGN_CENTER);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cell3 = cell.getCell("Name", font8, Element.ALIGN_LEFT, 0, 0,
				0);
		cell3.setVerticalAlignment(Element.ALIGN_CENTER);
		cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cell4 = cell.getCell("Surname", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell4.setVerticalAlignment(Element.ALIGN_CENTER);
		cell4.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell5 = cell.getCell("Gender ", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell5.setVerticalAlignment(Element.ALIGN_CENTER);
		cell5.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell6 = cell.getCell(" Father's /Spouse Name", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell6.setVerticalAlignment(Element.ALIGN_CENTER);
		cell6.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell7 = cell.getCell(" Date of Birth#", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell7.setVerticalAlignment(Element.ALIGN_CENTER);
		cell7.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell8 = cell.getCell("Nationality ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell8.setVerticalAlignment(Element.ALIGN_CENTER);
		cell8.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell9 = cell.getCell("Education Level", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell9.setVerticalAlignment(Element.ALIGN_CENTER);
		cell9.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell10 = cell.getCell("Date of Joining ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell10.setVerticalAlignment(Element.ALIGN_CENTER);
		cell10.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell11 = cell.getCell("Designation ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell11.setVerticalAlignment(Element.ALIGN_CENTER);
		cell11.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell12 = cell.getCell("Category Addres  *(HS/S/SS/US) ",
				font8, Element.ALIGN_LEFT, 0, 0, 0);
		cell12.setVerticalAlignment(Element.ALIGN_CENTER);
		cell12.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell13 = cell.getCell(" Type Of Employement", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell13.setVerticalAlignment(Element.ALIGN_CENTER);
		cell13.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell14 = cell.getCell("Mobile ", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell14.setVerticalAlignment(Element.ALIGN_CENTER);
		cell14.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell15 = cell.getCell("UAN ", font8, Element.ALIGN_LEFT, 0, 0,
				0);
		cell15.setVerticalAlignment(Element.ALIGN_CENTER);
		cell15.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell16 = cell.getCell(" PAN", font8, Element.ALIGN_LEFT, 0, 0,
				0);
		cell16.setVerticalAlignment(Element.ALIGN_CENTER);
		cell16.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell17 = cell.getCell("ESIC IP ", font8, Element.ALIGN_LEFT,
				0, 0, 0);
		cell17.setVerticalAlignment(Element.ALIGN_CENTER);
		cell17.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell18 = cell.getCell("LWF ", font8, Element.ALIGN_LEFT, 0, 0,
				0);
		cell18.setVerticalAlignment(Element.ALIGN_CENTER);
		cell18.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell19 = cell.getCell("ADHAAR ", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell19.setVerticalAlignment(Element.ALIGN_CENTER);
		cell19.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell20 = cell.getCell("Bank A/C Number ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell20.setVerticalAlignment(Element.ALIGN_CENTER);
		cell20.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell21 = cell.getCell("Bank ", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell21.setVerticalAlignment(Element.ALIGN_CENTER);
		cell21.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell22 = cell.getCell("Branch (IFSC)", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell22.setVerticalAlignment(Element.ALIGN_CENTER);
		cell22.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell23 = cell.getCell(" Present Address", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell23.setVerticalAlignment(Element.ALIGN_CENTER);
		cell23.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell24 = cell.getCell("Permanent ", font8, Element.ALIGN_LEFT,
				0, 0, 0);
		cell24.setVerticalAlignment(Element.ALIGN_CENTER);
		cell24.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell25 = cell.getCell(" Service Book No.", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell25.setVerticalAlignment(Element.ALIGN_CENTER);
		cell25.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell26 = cell.getCell(" Date of Exit", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell26.setVerticalAlignment(Element.ALIGN_CENTER);
		cell26.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell27 = cell.getCell("Reason of Exit   ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell27.setVerticalAlignment(Element.ALIGN_CENTER);
		cell27.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell28 = cell.getCell("Mark Of Identification ", font8,
				Element.ALIGN_LEFT, 0, 0, 0);
		cell28.setVerticalAlignment(Element.ALIGN_CENTER);
		cell28.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell29 = cell.getCell("Photo ", font8, Element.ALIGN_LEFT, 0,
				0, 0);
		cell29.setVerticalAlignment(Element.ALIGN_CENTER);
		cell29.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell30 = cell.getCell(" Specimen Signature /Thumb Impression",
				font8, Element.ALIGN_LEFT, 0, 0, 0);
		cell30.setVerticalAlignment(Element.ALIGN_CENTER);
		cell30.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cell31 = cell.getCell(" Remarks", font8, Element.ALIGN_LEFT,
				0, 0, 0);
		cell31.setVerticalAlignment(Element.ALIGN_CENTER);
		cell31.setHorizontalAlignment(Element.ALIGN_CENTER);

		outerTb1.addCell(cell1);
		outerTb1.addCell(cell2);
		outerTb1.addCell(cell3);
		outerTb1.addCell(cell4);
		outerTb1.addCell(cell5);
		outerTb1.addCell(cell6);
		outerTb1.addCell(cell7);
		outerTb1.addCell(cell8);
		outerTb1.addCell(cell9);
		outerTb1.addCell(cell10);
		outerTb1.addCell(cell11);
		outerTb1.addCell(cell12);
		outerTb1.addCell(cell13);
		outerTb1.addCell(cell14);
		outerTb1.addCell(cell15);
		outerTb1.addCell(cell16);
		outerTb1.addCell(cell17);
		outerTb1.addCell(cell18);
		outerTb1.addCell(cell19);
		outerTb1.addCell(cell20);
		outerTb1.addCell(cell21);
		outerTb1.addCell(cell22);
		outerTb1.addCell(cell23);
		outerTb1.addCell(cell24);
		outerTb1.addCell(cell25);
		outerTb1.addCell(cell26);
		outerTb1.addCell(cell27);
		outerTb1.addCell(cell28);
		outerTb1.addCell(cell29);
		outerTb1.addCell(cell30);
		outerTb1.addCell(cell31);

		PdfPCell cell32 = cell.getCell("1 ", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell33 = cell.getCell("2 ", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell34 = cell.getCell("3", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell35 = cell.getCell("4", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell36 = cell.getCell("5 ", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell37 = cell.getCell("6", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell38 = cell.getCell("7 ", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell39 = cell.getCell("8 ", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell40 = cell.getCell("9", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell41 = cell.getCell("10 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell42 = cell.getCell("11", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell43 = cell.getCell("12 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell44 = cell.getCell("13 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell45 = cell.getCell("14", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell46 = cell.getCell("15 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell47 = cell.getCell("16", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell48 = cell.getCell("17", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell49 = cell.getCell("18 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell50 = cell.getCell("19 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell51 = cell.getCell("20", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell52 = cell.getCell("21", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell53 = cell.getCell("22", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell54 = cell.getCell("23 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell55 = cell.getCell("24", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell56 = cell.getCell("25 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell57 = cell.getCell("26 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell58 = cell.getCell("27 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell59 = cell.getCell("28 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell60 = cell.getCell("29", font8, Element.ALIGN_CENTER, 0, 0,
				0);
		PdfPCell cell61 = cell.getCell("30 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);
		PdfPCell cell62 = cell.getCell("31 ", font8, Element.ALIGN_CENTER, 0,
				0, 0);

		outerTb1.addCell(cell32);
		outerTb1.addCell(cell33);
		outerTb1.addCell(cell34);
		outerTb1.addCell(cell35);
		outerTb1.addCell(cell36);
		outerTb1.addCell(cell37);
		outerTb1.addCell(cell38);
		outerTb1.addCell(cell39);
		outerTb1.addCell(cell40);
		outerTb1.addCell(cell41);
		outerTb1.addCell(cell42);
		outerTb1.addCell(cell43);
		outerTb1.addCell(cell44);
		outerTb1.addCell(cell45);
		outerTb1.addCell(cell46);
		outerTb1.addCell(cell47);
		outerTb1.addCell(cell48);
		outerTb1.addCell(cell49);
		outerTb1.addCell(cell50);
		outerTb1.addCell(cell51);
		outerTb1.addCell(cell52);
		outerTb1.addCell(cell53);
		outerTb1.addCell(cell54);
		outerTb1.addCell(cell55);
		outerTb1.addCell(cell56);
		outerTb1.addCell(cell57);
		outerTb1.addCell(cell58);
		outerTb1.addCell(cell59);
		outerTb1.addCell(cell60);
		outerTb1.addCell(cell61);
		outerTb1.addCell(cell62);

		for (int j = loopC; j < employeeList.size(); j++) {
			Employee employee = employeeList.get(j);
			/**
			 * @author Anil , Date : 08-07-2019
			 * for 14 employee case,looping condition creates deadlock
			 */
			loopCount = j;
			
			if (noOfLines == 0) {
//				loopCount = j;
				break;
			}

			noOfLines--;

			PdfPCell cell63 = cell.getCell((j + 1) + "", font6,
					Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell cell64 = null;
			if (employee != null && employee.getCount() != 0) {
				cell64 = cell.getCell(employee.getCount() + "", font6,
						Element.ALIGN_LEFT, 0, 0, 0);
				cell64.setVerticalAlignment(80);
			} else {
				cell64 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell65 = null;
			if (employee != null && employee.getFirstName() != null) {
				cell65 = cell.getCell(employee.getFirstName(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
				cell65.setVerticalAlignment(80);
			} else {
				cell65 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell66 = null;
			if (employee != null && employee.getLastName() != null) {
				cell66 = cell.getCell(employee.getLastName(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
				cell66.setVerticalAlignment(80);
			} else {
				cell66 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell67 = null;
			if (employee != null && employee.getGender() != null) {
				cell67 = cell.getCell(employee.getGender(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell67 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell68 = null;
			if (employee != null && employee.getHusbandName() != null) {
				cell68 = cell.getCell(employee.getHusbandName(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell68 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell69 = null;
			if (employee != null && employee.getDob() != null) {
				cell69 = cell.getCell(format.format(employee.getDob()) + "",
						font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell69 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell70 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0,
					0);

			PdfPCell cell71 = null;
			if (employee != null && employee.getEducationalInfo().size() > 0) {
				cell71 = cell.getCell(
						employee.getEducationalInfo()
								.get(employee.getEducationalInfo().size() - 1)
								.getPercentage()
								+ "", font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell71 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell72 = null;
			if (employee != null && employee.getJoinedAt() != null) {
				cell72 = cell.getCell(format.format(employee.getJoinedAt())
						+ "", font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell72 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell73 = null;
			if (employee != null && employee.getDesignation() != null) {
				cell73 = cell.getCell(employee.getDesignation(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell73 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell74 = null;
			if (employee != null) {
				cell74 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell74 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell75 = null;
			if (employee != null && employee.getEmployeeType() != null) {
				cell75 = cell.getCell(employee.getEmployeeType(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell75 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell76 = null;
			if (employee != null && employee.getCellNumber1() != 0) {
				cell76 = cell.getCell(employee.getCellNumber1() + "", font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell76 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell77 = null;
			if (employee != null && employee.getUANno() != null) {
				cell77 = cell.getCell(employee.getUANno(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell77 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell78 = null;
			if (employee != null && employee.getEmployeePanNo() != null) {
				cell78 = cell.getCell(employee.getEmployeePanNo(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell78 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell79 = null;
			if (employee != null && employee.getEmployeeESICcode() != null) {
				cell79 = cell.getCell(employee.getEmployeeESICcode(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell79 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell80 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0,
					0);

			PdfPCell cell81 = null;
			if (employee != null && employee.getAadharNumber() != 0) {
				cell81 = cell.getCell(employee.getAadharNumber() + "", font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell81 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell82 = null;
			if (employee != null && employee.getEmployeeBankAccountNo() != null) {
				cell82 = cell.getCell(employee.getEmployeeBankAccountNo(),
						font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell82 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell83 = null;
			if (employee != null && employee.getEmployeeBankName() != null) {
				cell83 = cell.getCell(employee.getEmployeeBankName(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell83 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell84 = null;
			if (employee != null && employee.getIfscCode() != null) {
				cell84 = cell.getCell(employee.getIfscCode(), font6,
						Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell84 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell85 = null;
			if (employee != null
					&& employee.getAddress().getCompleteAddress() != null) {
				cell85 = cell.getCell(employee.getAddress()
						.getCompleteAddress() + "", font6, Element.ALIGN_LEFT,
						0, 0, 0);
			} else {
				cell85 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell86 = null;
			if (employee != null
					&& employee.getSecondaryAddress().getCompleteAddress() != null) {
				cell86 = cell.getCell(employee.getSecondaryAddress()
						.getCompleteAddress() + "", font6, Element.ALIGN_LEFT,
						0, 0, 0);
			} else {
				cell86 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell87 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0,
					0);

			PdfPCell cell88;
			Date lastDate = null;
			if (employee != null && employee.getLastWorkingDate() != null) {
				lastDate = employee.getLastWorkingDate();
			} else if (employee != null && employee.getAbscondDate() != null) {
				lastDate = employee.getAbscondDate();
			} else if (employee != null
					&& employee.getTerminationDate() != null) {
				lastDate = employee.getTerminationDate();
			}
			String lastDateInString = "";
			if (lastDate != null) {
				lastDateInString = format.format(lastDate);
			}
			cell88 = cell.getCell(lastDateInString, font6, Element.ALIGN_LEFT,
					0, 0, 0);

			// if(employee!=null&&employee.getResignationDate()!=null){
			// cell88=cell.getCell(employee.getResignationDate()+"", font6,
			// Element.ALIGN_LEFT, 0, 0, 0);
			// }
			//
			// else{
			// cell88=cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			// }
			PdfPCell cell89 = null;
			if (employee != null) {
				cell89 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell89 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell90 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0,
					0);

			PdfPCell cell91 = null;
			if (employee != null && employee.getPhoto() != null) {
				cell91 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell91 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}
			PdfPCell cell92 = null;
			if (employee != null && employee.getUploadSign() != null) {
				cell92 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				cell92 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0, 0);
			}

			PdfPCell cell93 = cell.getCell("", font6, Element.ALIGN_LEFT, 0, 0,
					0);

			outerTb1.addCell(cell63);
			outerTb1.addCell(cell64);
			outerTb1.addCell(cell65);
			outerTb1.addCell(cell66);
			outerTb1.addCell(cell67);
			outerTb1.addCell(cell68);
			outerTb1.addCell(cell69);
			outerTb1.addCell(cell70);
			outerTb1.addCell(cell71);
			outerTb1.addCell(cell72);
			outerTb1.addCell(cell73);
			outerTb1.addCell(cell74);
			outerTb1.addCell(cell75);
			outerTb1.addCell(cell76);
			outerTb1.addCell(cell77);
			outerTb1.addCell(cell78);
			outerTb1.addCell(cell79);
			outerTb1.addCell(cell80);
			outerTb1.addCell(cell81);
			outerTb1.addCell(cell82);
			outerTb1.addCell(cell83);
			outerTb1.addCell(cell84);
			outerTb1.addCell(cell85);
			outerTb1.addCell(cell86);
			outerTb1.addCell(cell87);
			outerTb1.addCell(cell88);
			outerTb1.addCell(cell89);
			outerTb1.addCell(cell90);
			outerTb1.addCell(cell91);
			outerTb1.addCell(cell92);
			outerTb1.addCell(cell93);

		}

		try {
			document.add(outerTb1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
