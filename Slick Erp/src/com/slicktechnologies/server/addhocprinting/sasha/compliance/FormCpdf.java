package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.google.appengine.api.images.Image;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.TallyInterfaceBean;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class FormCpdf {
public Document document;
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1= new SimpleDateFormat("MM/yyyy");
	List<PaySlip> paySlipList;
	Company company;
	int noofemployee=0;
	int noOfLines = 20;
	int loopCount = 0;
	Date fromDate=null;
	Date toDate=null;
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	double noofdaysworkedintheyear=0.0;
	double totalsalaryrespectofaccyear=0.0;
	double amountofbonuspayableunder10=0.0;
	double netamountpayable=0.0;
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	
	HashMap<Integer, FormC_Data> paySlipMap=new HashMap<Integer, FormC_Data>();
	
	
	public void createPdf() {
		createFormHeading();
		employeedetails(20, loopCount);
		
		
		
      while (noOfLines == 0 && loopCount < paySlipMap.size()) {
			
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    createFormHeading();
			employeedetails(20, loopCount);
		}
		
      
		
	}

	
	public FormCpdf() {
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	
	
	
	public void setFormCdata(Long companyId,int empId,String fromDate,String toDate){
		Date fromDt=null;
		Date toDt=null;
		try {
			this.fromDate=format.parse(fromDate);
			this.toDate=format.parse(toDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fromDt=DateUtility.getDateWithTimeZone("IST", this.fromDate);
		toDt=DateUtility.getDateWithTimeZone("IST", this.toDate);
		System.out.println("FORM : "+fmt1.format(this.fromDate)+" TO : "+fmt1.format(this.toDate)+" comp Id : "+companyId+" Emp Id : "+empId);
		
//		Calendar frmCal = Calendar.getInstance(); 
//		frmCal.setTime(fromDt);
//		frmCal.add(Calendar.MONTH, -1);
//		fromDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
//		
//		Calendar toCal = Calendar.getInstance(); 
//		toCal.setTime(toDt);
//		toCal.add(Calendar.MONTH, -1);
//		toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
//		System.out.println("FRM :: "+format.format(fromDt)+" TO :: "+format.format(toDt));
		
		
		ArrayList<String> monthList=new ArrayList<String>();
		while(fromDt.before(toDt)||fromDt.equals(toDt)){
			monthList.add(fmt.format(fromDt));
			Calendar cal = Calendar.getInstance(); 
			cal.setTime(fromDt);
			cal.add(Calendar.MONTH, 1);
			fromDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
		}
		System.out.println("MONTH LIST : "+monthList);
		
		if(monthList.size()!=0){
			paySlipList=ofy().load().type(PaySlip.class).filter("companyId",companyId).filter("salaryPeriod IN", monthList).list();
		}
		if(paySlipList!=null){
			System.out.println("PaySlip : "+paySlipList.size());
			
			for(PaySlip slip:paySlipList){
				if(slip.getBonus()!=0){
					if(paySlipMap!=null){
						if(paySlipMap.get(slip.getEmpid())!=null){
							FormC_Data formC=paySlipMap.get(slip.getEmpid());
							formC.setNoOfDaysWorked(formC.getNoOfDaysWorked()+slip.getPaidDays());
							formC.setAnnualBonusAmt(formC.getAnnualBonusAmt()+slip.getPFAmount());
							formC.setBonusAmtPaid(formC.getBonusAmtPaid()+slip.getBonus());
							paySlipMap.put(slip.getEmpid(), formC);
						}else{
							FormC_Data formC=new FormC_Data(slip.getEmployeeName(), slip.getEmpid(), slip.getFatherName(), "Yes", slip.getEmployeedDesignation(), slip.getPaidDays(), slip.getPFAmount(), slip.getBonus());
							paySlipMap.put(slip.getEmpid(), formC);
						}
						
					}else{
						FormC_Data formC=new FormC_Data(slip.getEmployeeName(), slip.getEmpid(), slip.getFatherName(), "Yes", slip.getEmployeedDesignation(), slip.getPaidDays(), slip.getPFAmount(), slip.getBonus());
						paySlipMap.put(slip.getEmpid(), formC);
					}
					
					System.out.println("PaySlip Map SIze : "+paySlipMap.size());
				}
			}
			
		}
		System.out.println("DONE");
		
		
	company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
	paySlipList=CsvWriter.statuteryReport;
		
	}
	
	
	
	private void employeedetails(int count,int loopC) {
		// TODO Auto-generated method stub
		
		PdfPTable outerTbl=new PdfPTable(18);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		try {
			outerTbl.setWidths(new float[] {3,12,7,10,6,8,4,6,6,4,4,4,4,4,5,4,4,5});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPCell srno=cell.getCell("Sr.No", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell emp=cell.getCell("Employee", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell empcode=cell.getCell("Empcode", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell fhname=cell.getCell("Father / Husband Name", font7bold, Element.ALIGN_CENTER, 2,0 , 0);
		
		PdfPCell comp15=cell.getCell("Whether he / she has completed 15 year of age at the beginning of the accounting", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell desg=cell.getCell("Designation", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell noof=cell.getCell("No.of days worked in the year", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
			
		
		PdfPCell totalsalary=cell.getCell("Total salary or wage in respect of the accounting year[Earned]", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell amtCell=cell.getCell("Amount of Bonus payable under Section 10 or Section 11,as the case may be", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell pujabonus=cell.getCell("Puja Bonus or other customary bonus paid during the accounting year", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell interenbonus=cell.getCell("Interim bonus of bonus paid in advance", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell incomebonus=cell.getCell("[Amount of Income-Tax deducted] ", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell deduct=cell.getCell("Deduction on account of financial loss,if any,caused by misconduct", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell totalsum=cell.getCell("[Total sum deducted under Columns 9,10,10-A&11]", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell netamt=cell.getCell("Net amount payable (Column 8 minus column 12)", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell amtpaid=cell.getCell("Amount actually paid", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell datonpaid=cell.getCell("Date on which paid", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell signature=cell.getCell("Signature / Thumb impression of the employee", font7bold, Element.ALIGN_CENTER, 2, 0, 0);
		
		outerTbl.addCell(srno);
		outerTbl.addCell(emp);
		outerTbl.addCell(empcode);
	    outerTbl.addCell(fhname);
		outerTbl.addCell(comp15);
		outerTbl.addCell(desg);
		outerTbl.addCell(noof);
		outerTbl.addCell(totalsalary);
		outerTbl.addCell(amtCell);
		outerTbl.addCell(pujabonus);
		outerTbl.addCell(interenbonus);
		outerTbl.addCell(incomebonus);
		outerTbl.addCell(deduct);
		outerTbl.addCell(totalsum);
		outerTbl.addCell(netamt);
		outerTbl.addCell(amtpaid);
		outerTbl.addCell(datonpaid);
		outerTbl.addCell(signature);
		
		
		PdfPCell cell1=cell.getCell("1", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell1.setBorderWidthTop(0);
		
		PdfPCell cell2=cell.getCell("2", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell2.setBorderWidthTop(0);
		
		PdfPCell cell2a=cell.getCell("2a", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell2a.setBorderWidthTop(0);
		
		PdfPCell cell3=cell.getCell("3", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell3.setBorderWidthTop(0);
		
		PdfPCell cell4=cell.getCell("4", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell4.setBorderWidthTop(0);
		
		PdfPCell cell5=cell.getCell("5", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell5.setBorderWidthTop(0);
		
		PdfPCell cell6=cell.getCell("6", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell6.setBorderWidthTop(0);
		
		PdfPCell cell7=cell.getCell("7", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell7.setBorderWidthTop(0);
		
		PdfPCell cell8=cell.getCell("8", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell8.setBorderWidthTop(0);
		
		PdfPCell cell9=cell.getCell("9", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell9.setBorderWidthTop(0);
		
		PdfPCell cell10=cell.getCell("10", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell10.setBorderWidthTop(0);
		
		PdfPCell cell10a=cell.getCell("10a", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell10a.setBorderWidthTop(0);
		
		PdfPCell cell11=cell.getCell("11", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell11.setBorderWidthTop(0);
		
		PdfPCell cell12=cell.getCell("12", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell12.setBorderWidthTop(0);
		

		PdfPCell cell13=cell.getCell("13", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell13.setBorderWidthTop(0);
		
		PdfPCell cell14=cell.getCell("14", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell14.setBorderWidthTop(0);
		
		PdfPCell cell15=cell.getCell("15", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell15.setBorderWidthTop(0);
		
		PdfPCell cell16=cell.getCell("16", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell16.setBorderWidthTop(0);
		
		
		outerTbl.addCell(cell1);
		outerTbl.addCell(cell2);
		outerTbl.addCell(cell2a);
		outerTbl.addCell(cell3);
		outerTbl.addCell(cell4);
		outerTbl.addCell(cell5);
		outerTbl.addCell(cell6);
		outerTbl.addCell(cell7);
		outerTbl.addCell(cell8);
		outerTbl.addCell(cell9);
		outerTbl.addCell(cell10);
		outerTbl.addCell(cell10a);
		outerTbl.addCell(cell11);
		outerTbl.addCell(cell12);
		outerTbl.addCell(cell13);
		outerTbl.addCell(cell14);
		outerTbl.addCell(cell15);
		outerTbl.addCell(cell16);
		
			
            FormC_Data formc_data=null;
			PdfPCell cell21=null;
			PdfPCell cell22=null;
			PdfPCell cell22a=null;
			PdfPCell cell23=null;
			PdfPCell cell24=null;
			PdfPCell cell25=null;
			PdfPCell cell26=null;
			PdfPCell cell27=null;
			PdfPCell cell28=null;
			PdfPCell cell29=null;
			PdfPCell cell30=null;
			PdfPCell cell30a=null;
			PdfPCell cell31=null;
			PdfPCell cell32=null;
			PdfPCell cell33=null;
			PdfPCell cell34=null;
			PdfPCell cell35=null;
			PdfPCell cell36=null;
			int j=0;
			for(Map.Entry<Integer, FormC_Data> entry : paySlipMap.entrySet())
			     {
				
				
				if (noOfLines == 0) {
					loopCount = j;
					break;
				}
	
			noOfLines--;
				
			
			
			
				
				
				System.out.println("pay slip map :" + entry.getKey());
				formc_data = entry.getValue();
				
				
				noofdaysworkedintheyear+=formc_data.getNoOfDaysWorked();	
			    totalsalaryrespectofaccyear+=formc_data.getAnnualBonusAmt();
			    amountofbonuspayableunder10+=formc_data.getBonusAmtPaid();
			    netamountpayable+=formc_data.getBonusAmtPaid();
				
				
				
				
				
				
				 cell21=cell.getCell((j+1)+"", font8, Element.ALIGN_CENTER, 0, 0, 16);
				
				if(formc_data.getEmpName() !=null) {
					 cell22=cell.getCell(formc_data.getEmpName(), font8, Element.ALIGN_LEFT, 0, 0, 16);
				}else{
					cell22=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				if(formc_data.getEmpId() !=0) {
					cell22a=cell.getCell(formc_data.getEmpId()+"", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}else{
					cell22=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				if(formc_data.getFatherName() !=null) {
					 cell23=cell.getCell(formc_data.getFatherName(), font8, Element.ALIGN_LEFT, 0, 0, 16);
				}else{
					cell23=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				
				if(formc_data.getApplicalble() !=null) {
					 cell24=cell.getCell(formc_data.getApplicalble(), font8, Element.ALIGN_CENTER, 0, 0, 16);
				}else{
					cell24=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				if(formc_data.getDesignation() !=null) {
					 cell25=cell.getCell(formc_data.getDesignation(), font8, Element.ALIGN_LEFT, 0, 0, 16);
				}else{
					cell25=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				if(formc_data.getNoOfDaysWorked() !=0) {
					 cell26=cell.getCell(formc_data.getNoOfDaysWorked()+"", font8, Element.ALIGN_CENTER, 0, 0, 16);
				}else{
					cell26=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				
				if(formc_data.getAnnualBonusAmt() !=0) {
					 cell27=cell.getCell(formc_data.getAnnualBonusAmt()+"", font8, Element.ALIGN_CENTER, 0, 0, 16);
				}else{
					cell27=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				if(formc_data.getBonusAmtPaid() !=0) {
					 cell28=cell.getCell(formc_data.getBonusAmtPaid()+"", font8, Element.ALIGN_CENTER, 0, 0, 16);
				}else{
					cell28=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				}
				
				
				 cell29=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 16);
				 cell30=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 16);
				 cell30a=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 16);
				 cell31=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 16);
				 cell32=cell.getCell("", font8, Element.ALIGN_RIGHT, 0, 0, 16);
				 
				 
				 if(formc_data.getBonusAmtPaid() !=0){
					 cell33=cell.getCell(formc_data.getBonusAmtPaid()+"", font8, Element.ALIGN_CENTER, 0, 0, 16);
				 }else{
					 cell33=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 16);
				 }
				
				 cell34=cell.getCell("", font8, Element.ALIGN_RIGHT, 0, 0, 16);
				 cell35=cell.getCell("", font8, Element.ALIGN_RIGHT, 0, 0, 16);
				 cell36=cell.getCell("", font8, Element.ALIGN_RIGHT, 0, 0, 16);
				
	    outerTbl.addCell(cell21);
		outerTbl.addCell(cell22);
		outerTbl.addCell(cell22a);
		outerTbl.addCell(cell23);
		outerTbl.addCell(cell24);
		outerTbl.addCell(cell25);
		outerTbl.addCell(cell26);
		outerTbl.addCell(cell27);
		outerTbl.addCell(cell28);
		outerTbl.addCell(cell29);
		outerTbl.addCell(cell30);
		outerTbl.addCell(cell30a);
		outerTbl.addCell(cell31);
		outerTbl.addCell(cell32);
		outerTbl.addCell(cell33);
		outerTbl.addCell(cell34);
		outerTbl.addCell(cell35);
		outerTbl.addCell(cell36);
	
           j++;
                 
}
			 PdfPCell cell41=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell41.setBorderWidthRight(0);
		       	
		       	
		       	PdfPCell cell42=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell42.setBorderWidthRight(0);
		      	cell42.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell42a=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell42a.setBorderWidthRight(0);
		       	cell42a.setBorderWidthLeft(0);
		       	
		       	
		       	PdfPCell cell43=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell43.setBorderWidthRight(0);
		       	cell43.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell44=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell44.setBorderWidthRight(0);
		       	cell44.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell45=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell45.setBorderWidthRight(0);
		       	cell45.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell46=cell.getCell(  noofdaysworkedintheyear+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell46.setBorderWidthRight(0);
		       	cell46.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell47=cell.getCell(  totalsalaryrespectofaccyear+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell47.setBorderWidthRight(0);
		       	cell47.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell48=cell.getCell(  amountofbonuspayableunder10+ "", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell48.setBorderWidthRight(0);
		       	cell48.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell49=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell49.setBorderWidthRight(0);
		       	cell49.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell50=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell50.setBorderWidthRight(0);
		       	cell50.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell50a=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell50a.setBorderWidthRight(0);
		       	cell50a.setBorderWidthLeft(0);
		       	
		       	
		       	PdfPCell cell51=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell51.setBorderWidthRight(0);
		       	cell51.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell52=cell.getCell("Total", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell52.setBorderWidthRight(0);
		       	cell52.setBorderWidthLeft(0);
		       
		       	PdfPCell cell53=cell.getCell(netamountpayable+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell53.setBorderWidthRight(0);
		       	cell53.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell54=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell54.setBorderWidthRight(0);
		       	cell54.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell55=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell55.setBorderWidthRight(0);
		       	cell55.setBorderWidthLeft(0);
		       	
		       	PdfPCell cell56=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		       	cell56.setBorderWidthLeft(0);
		       	
		       	outerTbl.addCell(cell41);
		       	outerTbl.addCell(cell42);
		       	outerTbl.addCell(cell42a);
		       	outerTbl.addCell(cell43);
		       	outerTbl.addCell(cell44);
		       	outerTbl.addCell(cell45);
		       	outerTbl.addCell(cell46);
		       	outerTbl.addCell(cell47);
		       	outerTbl.addCell(cell48);
		       	outerTbl.addCell(cell49);
		       	outerTbl.addCell(cell50);
		       	outerTbl.addCell(cell50a);
		       	outerTbl.addCell(cell51);
		       	outerTbl.addCell(cell52);
		       	outerTbl.addCell(cell53);
		       	outerTbl.addCell(cell54);
		       	outerTbl.addCell(cell55);
		       	outerTbl.addCell(cell56);
		     
		           
		     
		
	try {
			document.add(outerTbl);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
       
	private void createFormHeading() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		
		outerTbl.addCell(cell.getCell(company.getBusinessUnitName().toUpperCase(), font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getAddress().getCompleteAddress(), font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Bonus Working Sheet - FORM 'C' [See Rule 4(C)]  "+fmt1.format(fromDate)+" To "+fmt1.format(toDate), font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
}
		
	
	class FormC_Data{
		String empName;
		int empId;
		String fatherName;
		String applicalble;
		String designation;
		double noOfDaysWorked;
		double annualBonusAmt;
		double bonusAmtPaid;
		
		public FormC_Data(String empName,int empId,String fatherName,String applicable,String designation,double noOfDaysWorked,double annualBonusAmt,double bonusAmtPaid) {
			// TODO Auto-generated constructor stub
			this.empName=empName;
			this.empId=empId;
			this.fatherName=fatherName;
			this.applicalble=applicable;
			this.designation=designation;
			this.noOfDaysWorked=noOfDaysWorked;
			this.annualBonusAmt=annualBonusAmt;
			this.bonusAmtPaid=bonusAmtPaid;
		}
		public String getEmpName() {
			return empName;
		}
		public void setEmpName(String empName) {
			this.empName = empName;
		}
		public int getEmpId() {
			return empId;
		}
		public void setEmpId(int empId) {
			this.empId = empId;
		}
		public String getFatherName() {
			return fatherName;
		}
		public void setFatherName(String fatherName) {
			this.fatherName = fatherName;
		}
		public String getApplicalble() {
			return applicalble;
		}
		public void setApplicalble(String applicalble) {
			this.applicalble = applicalble;
		}
		public String getDesignation() {
			return designation;
		}
		public void setDesignation(String designation) {
			this.designation = designation;
		}
		public double getNoOfDaysWorked() {
			return noOfDaysWorked;
		}
		public void setNoOfDaysWorked(double noOfDaysWorked) {
			this.noOfDaysWorked = noOfDaysWorked;
		}
		public double getAnnualBonusAmt() {
			return annualBonusAmt;
		}
		public void setAnnualBonusAmt(double annualBonusAmt) {
			this.annualBonusAmt = annualBonusAmt;
		}
		public double getBonusAmtPaid() {
			return bonusAmtPaid;
		}
		public void setBonusAmtPaid(double bonusAmtPaid) {
			this.bonusAmtPaid = bonusAmtPaid;
		}
		
		
		
	}
	
	
    public void addFooter(PdfWriter writer){
    	
    	Double total=Math.ceil(paySlipMap.size()/20.0);
    	int totalNoOfPage=total.intValue();
    	if(totalNoOfPage==0){
    		totalNoOfPage=1;
    	}
    	
        PdfPTable footer = new PdfPTable(1);
        try {
            // set defaults
            footer.setWidthPercentage(100f);
            footer.setTotalWidth(780f);
            footer.setLockedWidth(true);
            footer.getDefaultCell().setFixedHeight(40);
            footer.getDefaultCell().setBorder(Rectangle.TOP);
            footer.getDefaultCell().setBorderColor(BaseColor.LIGHT_GRAY);

            // add copyright
         //   footer.addCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD)));

            // add current page count
            footer.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            footer.addCell(new Phrase(String.format("Page %d of ", writer.getPageNumber())+totalNoOfPage, new Font(Font.FontFamily.HELVETICA, 8)));

            // add placeholder for total page count
//            PdfPCell totalPageCount = new PdfPCell(new Phrase(totalNoOfPage+"", new Font(Font.FontFamily.HELVETICA, 8)));
//            totalPageCount.setBorder(Rectangle.TOP);
//            totalPageCount.setBorderColor(BaseColor.LIGHT_GRAY);
//            footer.addCell(totalPageCount);

            // write page
            PdfContentByte canvas = writer.getDirectContent();
            canvas.beginMarkedContentSequence(PdfName.ARTIFACT);
            footer.writeSelectedRows(0, -1, 34, 50, canvas);
            canvas.endMarkedContentSequence();
        } catch(Exception de) {
            throw new ExceptionConverter(de);
        }
    }
	
	
	
	
	
	
	}
	
	
	
	
	


