package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.humanresourcelayer.PaySlipServiceImpl;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class Form3aPdf {
	public Document document;
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1= new SimpleDateFormat("MMM");
	List<PaySlip> paySlipList;
	Company company;
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	Date fromDate=null;
	Date toDate=null;
	
	double sumOfPfWage=0;
	double sumOfWorkerShare=0;
	double sumOfEpf=0;
	double sumOfPension=0;
	double pfMaxFixedValue=0;
	
	/**
	 * @author Anil , Date : 06-08-2019
	 * Storing employee wise payslip for statutory reports
	 */
	public HashMap<String,ArrayList<PaySlip>> statoturyReportMap=new HashMap<String,ArrayList<PaySlip>>();
	
	
	
	Logger logger = Logger.getLogger("FORM3A");
	
	public Form3aPdf() {
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setForm3aData(Long companyId,int empId,String fromDate,String toDate){
		Date fromDt=null;
		Date toDt=null;
		try {
			this.fromDate=format.parse(fromDate);
			this.toDate=format.parse(toDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fromDt=DateUtility.getDateWithTimeZone("IST", this.fromDate);
		toDt=DateUtility.getDateWithTimeZone("IST", this.toDate);
		logger.log(Level.SEVERE,"FORM : "+format.format(this.fromDate)+" TO : "+format.format(this.toDate)+" comp Id : "+companyId+" Emp Id : "+empId);
		
		Calendar frmCal = Calendar.getInstance(); 
		frmCal.setTime(fromDt);
		frmCal.add(Calendar.MONTH, -1);
		fromDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
		
		Calendar toCal = Calendar.getInstance(); 
		toCal.setTime(toDt);
		toCal.add(Calendar.MONTH, -1);
		toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
		logger.log(Level.SEVERE,"FRM :: "+format.format(fromDt)+" TO :: "+format.format(toDt));
		
		
		ArrayList<String> monthList=new ArrayList<String>();
		while(fromDt.before(toDt)||fromDt.equals(toDt)){
			monthList.add(fmt.format(fromDt));
			Calendar cal = Calendar.getInstance(); 
			cal.setTime(fromDt);
			cal.add(Calendar.MONTH, 1);
			fromDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
		}
		logger.log(Level.SEVERE,"MONTH LIST : "+monthList);
		
		if(monthList.size()!=0){
			paySlipList=ofy().load().type(PaySlip.class).filter("companyId",companyId).filter("empid", empId).filter("salaryPeriod IN", monthList).list();
		}
		if(paySlipList!=null){
			logger.log(Level.SEVERE,"PaySlip : "+paySlipList.size());
		}
		logger.log(Level.SEVERE,"DONE");
		
		company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		ArrayList<String> processNameList=new ArrayList<String>();
		processNameList.add("PfMaxFixedValue");
		
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName IN", processNameList).filter("configStatus", true).list();
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PfMaxFixedValue")&&ptDetails.isStatus()==true){
						pfMaxFixedValue=Double.parseDouble(ptDetails.getProcessType().trim());
					}
				}
			}
		}
		
		/**
		 * @author Anil , Date : 01-08-2019
		 */
		logger.log(Level.SEVERE, " statuteryReport SIZE : "+paySlipList.size());
		statoturyReportMap=new HashMap<String, ArrayList<PaySlip>>();
		for(PaySlip obj:paySlipList){
			String key=obj.getEmpid()+obj.getSalaryPeriod();
			if(statoturyReportMap!=null&&statoturyReportMap.size()!=0){
				if(statoturyReportMap.containsKey(key)){
					statoturyReportMap.get(key).add(obj);
				}else{
					ArrayList<PaySlip> list=new ArrayList<PaySlip>();
					list.add(obj);
					statoturyReportMap.put(key, list);
				}
			}else{
				ArrayList<PaySlip> list=new ArrayList<PaySlip>();
				list.add(obj);
				statoturyReportMap.put(key, list);
			}
		}
		logger.log(Level.SEVERE, " statuteryReport MAP SIZE : "+statoturyReportMap.size());
	
	}
	
	public void createPdf(){
		createFormHeading();
		createEmployeesPfDetails();
		createPfDetailsTbl();
		createFooterTbl();
	}

	private void createFormHeading() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		
		outerTbl.addCell(cell.getCell("Form - 3A(Revised)", font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		outerTbl.addCell(cell.getCell("THE EMPLOYEE'S PROVIDENT SCHEME,1956 AND THE EMPLOYEE'S PENSION SCHEME,1995", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("(Para's 35 and 42) and Para 19", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("CONTRIBUTION CARD FOR CURRENCY PERIOD FROM : "+format.format(fromDate)+" To "+format.format(toDate), font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createEmployeesPfDetails() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(6);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		
		PaySlip payslip=paySlipList.get(0);
		
		double pfRate=0;
		for(CtcComponent ctc:payslip.getDeductionList()){
			if(ctc.getShortName().equalsIgnoreCase("PF")){
				pfRate=ctc.getMaxPerOfCTC();
				break;
			}
		}
		
		outerTbl.addCell(cell.getCell("1. Account No.", font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(payslip.getPfNumber(), font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("PF Group No.", font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getPfGroupNum(), font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("Office At", font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getPfOfficeAt(), font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("2. Name", font8bold, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(payslip.getEmployeeName().toUpperCase(), font8bold, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("3. Father/Husband Name", font8, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(payslip.getFatherName().toUpperCase(), font8, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("4. Name & Address of the Factory / Establishment", font8bold, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getBusinessUnitName().toUpperCase(), font8bold, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		
		outerTbl.addCell(cell.getCell(" ", font8bold, Element.ALIGN_LEFT, 0, 3, 25)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getAddress().getCompleteAddress(), font8bold, Element.ALIGN_LEFT, 0, 3, 25)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("5. Statutory rate of PF contribution", font8, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell(pfRate+"%", font8, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("6. Voluntary higher rate of Employee's contribution if any", font8, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("NIL", font8, Element.ALIGN_LEFT, 0, 3, 18)).setBorder(0);
		
		
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createPfDetailsTbl() {
		// TODO Auto-generated method stub
		CsvWriter csv=new CsvWriter();
		PdfPTable outerTbl=new PdfPTable(8);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		
		PdfPCell monthCell=cell.getCell("Month", font8, Element.ALIGN_CENTER, 2, 0, 0);
		monthCell.setBorderWidthBottom(0);
		
		PdfPCell amtCell=cell.getCell("Amount of Wages", font8, Element.ALIGN_CENTER, 2, 0, 0);
		amtCell.setBorderWidthBottom(0);
		
		PdfPCell wrkShareCell=cell.getCell("Workers Share", font8, Element.ALIGN_CENTER, 2, 0, 0);
		wrkShareCell.setBorderWidthBottom(0);
		
		PdfPCell emplrShareCell=cell.getCell("Employer's share", font8, Element.ALIGN_CENTER, 0, 2, 0);
		
		PdfPCell refundCell=cell.getCell("Refund of Advance", font8, Element.ALIGN_CENTER, 2, 0, 0);
		refundCell.setBorderWidthBottom(0);
		
		PdfPCell daysCell=cell.getCell("No. of Days of non contributing service", font8, Element.ALIGN_CENTER, 2, 0, 0);
		daysCell.setBorderWidthBottom(0);
		
		PdfPCell remarkCell=cell.getCell("Remarks", font8, Element.ALIGN_CENTER, 2, 0, 0);
		remarkCell.setBorderWidthBottom(0);
		
		outerTbl.addCell(monthCell);
		outerTbl.addCell(amtCell);
		outerTbl.addCell(wrkShareCell);
		outerTbl.addCell(emplrShareCell);
		outerTbl.addCell(refundCell);
		outerTbl.addCell(daysCell);
		outerTbl.addCell(remarkCell);
		
		
		PdfPCell cell1=cell.getCell("1", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell1.setBorderWidthTop(0);
		
		PdfPCell cell2=cell.getCell("2", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell2.setBorderWidthTop(0);
		
		PdfPCell cell3=cell.getCell("3", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell3.setBorderWidthTop(0);
		
		PdfPCell cell4a=cell.getCell("EPF Difference between 12% & 8.33%", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell4a.setBorderWidthBottom(0);
		
		PdfPCell cell4b=cell.getCell("Pension Fund Contribution 8.33%", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell4b.setBorderWidthBottom(0);
		
		PdfPCell cell4a1=cell.getCell("4(a)", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell4a1.setBorderWidthTop(0);
		
		PdfPCell cell4b2=cell.getCell("4(b)", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell4b2.setBorderWidthTop(0);
		
		PdfPCell cell5=cell.getCell("5", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell5.setBorderWidthTop(0);
		
		PdfPCell cell6=cell.getCell("6", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell6.setBorderWidthTop(0);
		
		PdfPCell cell7=cell.getCell("7", font8, Element.ALIGN_CENTER, 0, 0, 0);
		cell7.setBorderWidthTop(0);
		
		outerTbl.addCell(cell4a);
		outerTbl.addCell(cell4b);
		
		outerTbl.addCell(cell1);
		outerTbl.addCell(cell2);
		outerTbl.addCell(cell3);
		outerTbl.addCell(cell4a1);
		outerTbl.addCell(cell4b2);
		outerTbl.addCell(cell5);
		outerTbl.addCell(cell6);
		outerTbl.addCell(cell7);
		
		Date fromDt=DateUtility.getDateWithTimeZone("IST", this.fromDate);
		Date toDt=DateUtility.getDateWithTimeZone("IST", this.toDate);
		
		Date frmDt=DateUtility.getDateWithTimeZone("IST", this.fromDate);
		logger.log(Level.SEVERE,"FORM : "+format.format(this.fromDate)+" TO : "+format.format(this.toDate));
		
		Calendar frmCal = Calendar.getInstance(); 
		frmCal.setTime(fromDt);
		frmCal.add(Calendar.MONTH, -1);
		fromDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
		
		Calendar toCal = Calendar.getInstance(); 
		toCal.setTime(toDt);
		toCal.add(Calendar.MONTH, -1);
		toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
		logger.log(Level.SEVERE,"FRM :: "+format.format(fromDt)+" TO :: "+format.format(toDt));
		
		while(fromDt.before(toDt)||fromDt.equals(toDt)){
			String month="";
			month=fmt1.format(frmDt);
			ArrayList<PaySlip> paySlipList=getPaySlipDetails(fmt.format(frmDt));
			if(paySlipList!=null){
				if(fromDt.before(fromDate)){
					month=fmt1.format(fromDt)+" paid in "+fmt1.format(fromDate);
				}else if(fromDt.equals(toDt)){
					month=fmt1.format(fromDt)+" paid in "+fmt1.format(toDate);
				}
				double pfWage=0;
				double pfDeduction=0;
				double unpaidDays=0;
				for(PaySlip obj:paySlipList){
					pfWage=pfWage+csv.getPFwages(obj, true, false);
					pfDeduction=pfDeduction+obj.getDeductedPFAmount();
					unpaidDays=unpaidDays+obj.getUnpaidDay();
				}
				
				/**
				 * @author Anil,Date : 01-07-2019
				 * picking pf wage amount from database 
				 * if not present then doing reverse calculation with the help of pf percent and pf amount
				 */
//				double pfWage=paySlip.getPFAmount()+paySlip.getPFArrearsAmount();
//				double pfWage=csv.getPFwages(paySlip, true, false);
				
//				if(pfMaxFixedValue!=0&&pfWage>pfMaxFixedValue){
//					pfWage=pfMaxFixedValue;
//				}
				
				PdfPCell cell11=cell.getCell(month, font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell11.setBorderWidthTop(0);
				cell11.setBorderWidthBottom(0);
				
				PdfPCell cell12=cell.getCell(df.format(pfWage), font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell12.setBorderWidthTop(0);
				cell12.setBorderWidthBottom(0);
				
				double epfAmt=0;
				double pensionAmt=0;
				if(pfDeduction!=0){
					epfAmt=Math.round((pfWage/100)*3.67);
					pensionAmt=Math.round((pfWage/100)*8.33);
				}
				PdfPCell cell13=cell.getCell(df.format(pfDeduction), font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell13.setBorderWidthTop(0);
				cell13.setBorderWidthBottom(0);
				
				PdfPCell cell14a=cell.getCell(df.format(epfAmt), font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell14a.setBorderWidthTop(0);
				cell14a.setBorderWidthBottom(0);
				
				PdfPCell cell14b=cell.getCell(df.format(pensionAmt), font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell14b.setBorderWidthTop(0);
				cell14b.setBorderWidthBottom(0);
				
				PdfPCell cell15=cell.getCell(" " , font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell15.setBorderWidthTop(0);
				cell15.setBorderWidthBottom(0);
				
				PdfPCell cell16=cell.getCell(df1.format(unpaidDays)+"" , font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell16.setBorderWidthTop(0);
				cell16.setBorderWidthBottom(0);
				
				PdfPCell cell17=cell.getCell(" " , font8, Element.ALIGN_RIGHT, 0, 0, 16);
				cell17.setBorderWidthTop(0);
				cell17.setBorderWidthBottom(0);
				
				outerTbl.addCell(cell11);
				outerTbl.addCell(cell12);
				outerTbl.addCell(cell13);
				outerTbl.addCell(cell14a);
				outerTbl.addCell(cell14b);
				outerTbl.addCell(cell15);
				outerTbl.addCell(cell16);
				outerTbl.addCell(cell17);
			
				sumOfPfWage+=pfWage;
				sumOfWorkerShare+=pfDeduction;
				sumOfEpf+=Math.round(epfAmt);
				sumOfPension+=Math.round(pensionAmt);
			}
			
			Calendar cal = Calendar.getInstance(); 
			cal.setTime(fromDt);
			cal.add(Calendar.MONTH, 1);
			fromDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
			
			Calendar cal1 = Calendar.getInstance(); 
			cal1.setTime(fromDt);
			cal1.add(Calendar.MONTH, 1);
			frmDt=DateUtility.getDateWithTimeZone("IST", cal1.getTime());
		}
		
		outerTbl.addCell(cell.getCell("TOTAL", font9, Element.ALIGN_LEFT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(df.format(sumOfPfWage), font8, Element.ALIGN_RIGHT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(df.format(sumOfWorkerShare), font8, Element.ALIGN_RIGHT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(df.format(sumOfEpf), font8, Element.ALIGN_RIGHT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(df.format(sumOfPension), font8, Element.ALIGN_RIGHT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(" " , font8, Element.ALIGN_RIGHT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(" " , font8, Element.ALIGN_RIGHT, 0, 0, 16));
		outerTbl.addCell(cell.getCell(" " , font8, Element.ALIGN_RIGHT, 0, 0, 16));
		
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private ArrayList<PaySlip> getPaySlipDetails(String salaryPeriod) {
		// TODO Auto-generated method stub
//		if(paySlipList!=null){
//			for(PaySlip pay:paySlipList){
//				if(pay.getSalaryPeriod().equals(salaryPeriod)){
//					return pay;
//				}
//			}
//		}
		String key=paySlipList.get(0).getEmpid()+salaryPeriod;
		if(statoturyReportMap.containsKey(key)){
			return statoturyReportMap.get(key);
		}
		
		return null;
	}

	private void createFooterTbl() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(2);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		
		outerTbl.addCell(cell.getCell("Dated : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("For "+company.getBusinessUnitName().toUpperCase(), font9, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font9, Element.ALIGN_CENTER, 4, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font9, Element.ALIGN_CENTER, 3, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("DIRECTOR", font9, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font9, Element.ALIGN_CENTER, 2, 2, 0)).setBorder(0);
		
		outerTbl.addCell(cell.getCell("Certified that the total amount of contributions(both shares) indicated in this card i.e. Rs. "+ df.format(Math.round(sumOfWorkerShare+sumOfEpf)) +" has already been remitted in full in EPF A/c No. 1 and Pension Fund A/c No. 10 Rs. "+df.format(Math.round(sumOfPension)), font9, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font9, Element.ALIGN_CENTER, 2, 2, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Certified that the difference between the total of the contribution shown under Column 3 and 4(a) and 4(b) of that arrived at on the total wages shown in column 2 at the prescibed rate is solely due to rounding off of to the nearest rupee under the rules.", font9, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(0);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
