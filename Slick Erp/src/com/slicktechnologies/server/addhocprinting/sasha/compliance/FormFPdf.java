package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class FormFPdf {
	public Document document;
	Company company;
	Employee employee;
	EmployeeInfo employeeinfo;
	EmployeeFamilyDeatails employeedetails;
	Logger logger=Logger.getLogger("FormF PDF");
		SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
		SimpleDateFormat fmt1= new SimpleDateFormat("MMM");
	   EmploymentCardPdf cell=new EmploymentCardPdf();
		Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
		Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
		Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
		Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
		Font font11bold=new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
		Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
		Font font7boldul = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD|Font.UNDERLINE);
		Font font8boldul=new Font(Font.FontFamily.HELVETICA, 8,Font.BOLD|Font.UNDERLINE);
		Font font10boldul=new Font(Font.FontFamily.HELVETICA, 10,Font.BOLD|Font.UNDERLINE);
		
		float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
		DecimalFormat df=new DecimalFormat("0.00");
		DecimalFormat df1=new DecimalFormat("0");
		ArrayList<EmployeeFamilyDeatails> nominationList;
		
		public void createPdf(PdfWriter writer){
			formHeading();
			createFormSubHeading();
			nomineeDetails();
			writeFooterTable(writer);
			statement2();
			declarationByWitness();
			certificateByTheEmployer();
			acknowledgementByTheEmployee();
		}
		
		public void setFormFPdf(Long companyId,int empId){
			employee = ofy().load().type(Employee.class).filter("companyId",companyId).filter("count",empId).first().now();
			if(employee!=null){
			company=ofy().load().type(Company.class).filter("companyId", employee.getCompanyId()).first().now();
			}
			
			
		}
		
		
		private void formHeading(){
			
			PdfPTable outertb=new PdfPTable(2);
			outertb.setWidthPercentage(100f);
			try {
				outertb.setWidths(new float[] {40,60});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell celluan=cell.getCell("Emp. UAN No.", font7bold, Element.ALIGN_LEFT, 0, 0, 0);
			celluan.setBorder(0);
//			celluan.setPaddingBottom(0);
//			celluan.setPaddingTop(0);
			celluan.setPaddingLeft(5);
//			celluan.setPaddingRight(0);
			
			
			
			PdfPCell celluanvalue=cell.getCell(employee.getUANno()+"", font7bold, Element.ALIGN_LEFT, 0,0,0);
//			celluanvalue.setPaddingBottom(0);
//			celluanvalue.setPaddingTop(0);
//			celluanvalue.setPaddingLeft(0);
//			celluanvalue.setPaddingRight(0);
			
			outertb.addCell(celluan);
			outertb.addCell(celluanvalue);
			
			
			PdfPTable outerTb1=new PdfPTable(3);
			outerTb1.setWidthPercentage(100f);
			try {
				outerTb1.setWidths(new float[] {20,60,20});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPCell celblank=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			celblank.setBorder(0);
			PdfPCell cmpnameval2=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			cmpnameval2.setBorder(0);
			PdfPCell empcode=cell.getCell("Emp Code : "+employee.getCount(), font7, Element.ALIGN_CENTER, 0, 0, 0);
			empcode.setBorder(0);
			PdfPCell celblank2=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			celblank2.setBorder(0);
			PdfPCell formf=cell.getCell("FORM 'F'", font10bold, Element.ALIGN_CENTER, 0, 0, 0);
			formf.setBorder(0);
			PdfPCell category=cell.getCell("Category : "+employee.getEmployeeType(), font7, Element.ALIGN_RIGHT, 0, 0, 0);
			category.setBorder(0);
			
			PdfPCell celblank1=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			celblank1.addElement(outertb);
			
			PdfPCell act=cell.getCell("THE PAYMENT OF GRATUITY ACT", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			act.setBorder(0);
			PdfPCell telno=cell.getCell("Tel No. : "+employee.getCellNumber1(), font7, Element.ALIGN_CENTER, 0, 0, 0);
			telno.setBorder(0);
			
			PdfPCell celblank3=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			celblank3.setBorder(0);
			PdfPCell cellblank3=cell.getCell("[See Sub-rule (1) of Rule 6]", font8, Element.ALIGN_CENTER, 0, 0, 0);
			cellblank3.setBorder(0);
			PdfPCell empcode2=cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			empcode2.setBorder(0);
			
			
			PdfPCell bnk=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			bnk.setBorder(0);
			PdfPCell nomination=cell.getCell("NOMINATION", font10bold, Element.ALIGN_CENTER, 0, 0, 0);
			nomination.setBorder(0);
			PdfPCell bnl2=cell.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			bnl2.setBorder(0);
			
			outerTb1.addCell(celblank);
			outerTb1.addCell(cmpnameval2);
			outerTb1.addCell(empcode);
			outerTb1.addCell(celblank2);
			outerTb1.addCell(formf);
			outerTb1.addCell(category);
			outerTb1.addCell(celblank1);
			outerTb1.addCell(act);
			outerTb1.addCell(telno);
			outerTb1.addCell(celblank3);
			outerTb1.addCell(cellblank3);
			outerTb1.addCell(empcode2);
			outerTb1.addCell(bnk);
			outerTb1.addCell(nomination);
			outerTb1.addCell(bnl2);
			
			
			
			
			
			PdfPTable outerTb2=new PdfPTable(3);
			outerTb2.setWidthPercentage(100f);
			try {
				outerTb2.setWidths(new float[] {5,25,70});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell blk=cell.getCell("1.", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk.setBorder(0);
			PdfPCell cmpname1=cell.getCell("I.Shri /Shrimati / Kumari ", font9, Element.ALIGN_LEFT, 0, 0, 0);
			cmpname1.setBorder(0);
			
			PdfPCell cmpnameval=null;
			if(employee !=null && employee.getFullname() !=null){
				cmpnameval=cell.getCell(employee.getFullName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		     	cmpnameval.setBorderWidthLeft(0);
		     	cmpnameval.setBorderWidthRight(0);
		     	cmpnameval.setBorderWidthTop(0);
			}else{
				cmpnameval=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		     	cmpnameval.setBorderWidthLeft(0);
		     	cmpnameval.setBorderWidthRight(0);
		     	cmpnameval.setBorderWidthTop(0);
			}
	     
			PdfPCell  name1 =cell.getCell("(Name in Full here)", font9, Element.ALIGN_CENTER, 0, 3, 0);
			name1.setBorder(0);
			outerTb2.addCell(blk);
			outerTb2.addCell(cmpname1);
			outerTb2.addCell(cmpnameval);
			outerTb2.addCell(name1);
			
		
			PdfPTable outerTb3=new PdfPTable(2);
			outerTb3.setWidthPercentage(100f);
			try {
				outerTb3.setWidths(new float[] {5,95});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell to=cell.getCell("To ,   ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			to.setBorder(0);
			
			PdfPCell cmpname=null;
			if(company !=null &&company.getBusinessUnitName()!=null){
				 cmpname=cell.getCell(company.getBusinessUnitName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
				cmpname.setBorderWidthLeft(0);
				cmpname.setBorderWidthRight(0);
				cmpname.setBorderWidthTop(0);
			}else{
			    cmpname=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
				cmpname.setBorderWidthLeft(0);
				cmpname.setBorderWidthRight(0);
				cmpname.setBorderWidthTop(0);
			}
			
		
			outerTb3.addCell(to);
			outerTb3.addCell(cmpname);
			
			PdfPCell to1=cell.getCell("   ", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			to1.setBorder(0);
			
			
			PdfPCell add=null;
			if(company !=null &&company.getAddress().getCompleteAddress()!=null){
				add=cell.getCell(company.getAddress().getCompleteAddress()+"", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
				add.setBorder(0);
			}else{
				 add=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
				add.setBorder(0);
			}
			
			
			outerTb3.addCell(to1);
			outerTb3.addCell(add);
			PdfPCell desc=cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 10);
			desc.setBorder(0);
			PdfPCell bnk2=cell.getCell("(Give here name or description of the establishment with full address)", font7, Element.ALIGN_CENTER, 0, 0, 0);
			bnk2.setBorder(0);
			outerTb3.addCell(desc);
			outerTb3.addCell(bnk2);
			try {
				document.add(outerTb1);
				document.add(outerTb3);
				document.add(outerTb2);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
					
		}
		
		
	
		private void createFormSubHeading(){
			PdfPTable outerTbl=new PdfPTable(2);
			outerTbl.setWidthPercentage(100f);
			try {
				outerTbl.setWidths(new float[] {5,95});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell blk=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk.setBorder(0);
			PdfPCell cell111=cell.getCell("Whose particulars are given in the statement below, hereby nominate the person(s), mentioned", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell111.setBorder(0);
			
		
			
			
			
			
			PdfPCell blk1=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk1.setBorder(0);
			PdfPCell cell112=cell.getCell("below to receive the gratuity payable after my death as also the gratuity standing to my credit in", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell112.setBorder(0);
			
			PdfPCell blk2=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk2.setBorder(0);
			PdfPCell cell113=cell.getCell("the event of my death before that amount has become payable, or having become payable has not ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell113.setBorder(0);
			
			PdfPCell blk3=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk3.setBorder(0);
			PdfPCell cell114=cell.getCell("been paid and direct that said amount of gratuity shall be paid in proportion indicated against the", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell114.setBorder(0);
			
			PdfPCell blk4=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk4.setBorder(0);
			PdfPCell cell115=cell.getCell("name(s) of the nominee(s).", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell115.setBorder(0);
			
			
			PdfPCell blkcell=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell.setBorder(0);
			PdfPCell blkcell1=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell1.setBorder(0);
			
			
			PdfPCell blk5=cell.getCell("2.", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk5.setBorder(0);
			PdfPCell cell116=cell.getCell("I.hereby certify that the person(s) nominated is a/are member(s) of my family within the meaning", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell116.setBorder(0);
			
			PdfPCell blk6=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk6.setBorder(0);
			PdfPCell cell117=cell.getCell("of clause (h) of section 2 of the payment of Gratuity Act, 1972.", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell117.setBorder(0);
			
			PdfPCell blkcell2=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell2.setBorder(0);
			PdfPCell blkcell3=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell3.setBorder(0);
			
			
			
			PdfPCell blk7=cell.getCell("3.", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk7.setBorder(0);
			PdfPCell cell118=cell.getCell("I,hereby declare that I have no family within the meaning of clause (h) of section 2 of the said Act.", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell118.setBorder(0);
			
			PdfPCell blkcell4=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell4.setBorder(0);
			PdfPCell blkcell5=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell5.setBorder(0);
			
			PdfPCell blk8=cell.getCell("4.", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk8.setBorder(0);
			PdfPCell cell119=cell.getCell("(a) My father /mother /parents is /are not dependent on me.", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell119.setBorder(0);
			
			PdfPCell blkcell6=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell6.setBorder(0);
			PdfPCell blkcell7=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell7.setBorder(0);
			
			PdfPCell blk9=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk9.setBorder(0);
			PdfPCell cell120=cell.getCell("(b) My husband's father /mother /parents is / are not dependent on my husband .", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell120.setBorder(0);
			
			PdfPCell blkcell8=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell8.setBorder(0);
			PdfPCell blkcell9=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell9.setBorder(0);
			
			PdfPCell blk10=cell.getCell("5.", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk10.setBorder(0);
			
			PdfPTable outerTb2=new PdfPTable(3);
			outerTb2.setWidthPercentage(100f);
			try {
				outerTb2.setWidths(new float[] {68,27,5});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell1=cell.getCell("I, have excluded my husband from my family by a notice dated the ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell1.setBorder(0);
			outerTb2.addCell(cell1);
			PdfPCell cell2=cell.getCell(" ", font8boldul, Element.ALIGN_LEFT, 0, 0, 0);
			cell2.setBorderWidthLeft(0);
			cell2.setBorderWidthRight(0);
			cell2.setBorderWidthTop(0);
			outerTb2.addCell(cell2);
			
			PdfPCell cell3=cell.getCell("to", font8, Element.ALIGN_LEFT, 0, 0, 0);
			cell3.setBorder(0);
			outerTb2.addCell(cell3);
			
			PdfPCell cell121=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 3, 0);
			cell121.setBorder(0);
			cell121.addElement(outerTb2);
			
			
			
//			PdfPCell cell121=cell.getCell("I, have excluded my husband from my family by a notice dated the                                                 to", font10, Element.ALIGN_LEFT, 0, 0, 0);
//			cell121.setBorder(0);
			
			PdfPCell blk11=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk11.setBorder(0);
			PdfPCell cell122=cell.getCell("the controlling authority in items of the proviso to clause (h) of section 2 of the said Act.", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell122.setBorder(0);
			
			PdfPCell blkcell10=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell10.setBorder(0);
			PdfPCell blkcell11=cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blkcell11.setBorder(0);
			
			PdfPCell blk12=cell.getCell("6.", font8, Element.ALIGN_LEFT, 0, 0, 0);
			blk12.setBorder(0);
			PdfPCell cell123=cell.getCell("Nomination made herein invalidates my previous nomination.", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell123.setBorder(0);
			
			outerTbl.addCell(blk);
			outerTbl.addCell(cell111);
			outerTbl.addCell(blk1);
			outerTbl.addCell(cell112);
			outerTbl.addCell(blk2);
			outerTbl.addCell(cell113);
			outerTbl.addCell(blk3);
			outerTbl.addCell(cell114);
			outerTbl.addCell(blk4);
			outerTbl.addCell(cell115);
			outerTbl.addCell(blkcell);
			outerTbl.addCell(blkcell1);
			outerTbl.addCell(blk5);
			outerTbl.addCell(cell116);
			outerTbl.addCell(blk6);
			outerTbl.addCell(cell117);
			outerTbl.addCell(blkcell2);
			outerTbl.addCell(blkcell3);
			outerTbl.addCell(blk7);
			outerTbl.addCell(cell118);
			outerTbl.addCell(blkcell4);
			outerTbl.addCell(blkcell5);
			outerTbl.addCell(blk8);
			outerTbl.addCell(cell119);
			outerTbl.addCell(blkcell6);
			outerTbl.addCell(blkcell7);
			outerTbl.addCell(blk9);
			outerTbl.addCell(cell120);
			outerTbl.addCell(blkcell8);
			outerTbl.addCell(blkcell9);
			outerTbl.addCell(blk10);
			outerTbl.addCell(cell121);
			outerTbl.addCell(blk11);
			outerTbl.addCell(cell122);
			outerTbl.addCell(blkcell10);
			outerTbl.addCell(blkcell11);
			outerTbl.addCell(blk12);
			outerTbl.addCell(cell123);
			
			
			
			try {
				document.add(outerTbl);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
			
//			
//			
			
		}
		
		private void nomineeDetails(){
			PdfPTable outerTbl=new PdfPTable(4);
			outerTbl.setWidthPercentage(100);
			outerTbl.setSpacingBefore(20);
			try {
				outerTbl.setWidths(new float[] {54,12,12,12});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell nom=cell.getCell("NOMINEE(S)", font10bold, Element.ALIGN_CENTER, 0, 4, 0);
			nom.setBorder(0);
			PdfPCell cell2=cell.getCell("Name in full with full address of Nominee(s)", font9, Element.ALIGN_CENTER, 0, 0, 0);
			cell2.setBorderWidthBottom(0);
			cell2.setBorderWidthLeft(0);
		    PdfPCell cell3=cell.getCell("Relationship with the employee", font9, Element.ALIGN_CENTER, 0, 0, 0);
		    cell3.setBorderWidthBottom(0);
			PdfPCell cell4=cell.getCell("Age of nominee", font9, Element.ALIGN_CENTER, 0, 0, 0);
			cell4.setBorderWidthBottom(0);
			PdfPCell cell5=cell.getCell("Proportion by which the gratuity will be shared", font9, Element.ALIGN_CENTER, 0, 0, 0);
			cell5.setBorderWidthBottom(0);
			cell5.setBorderWidthRight(0);
			outerTbl.addCell(nom);
			outerTbl.addCell(cell2);
			outerTbl.addCell(cell3);
			outerTbl.addCell(cell4);
			outerTbl.addCell(cell5);
			
			
			
			PdfPCell cell8=cell.getCell("(1)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			cell8.setBorderWidthTop(0);
			cell8.setBorderWidthLeft(0);
			PdfPCell cell9=cell.getCell("(2)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			cell9.setBorderWidthTop(0);
			PdfPCell cell10=cell.getCell("(3)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			cell10.setBorderWidthTop(0);
			PdfPCell cell11=cell.getCell("(4)", font8, Element.ALIGN_CENTER, 0, 0, 0);
			cell11.setBorderWidthTop(0);
			cell11.setBorderWidthRight(0);
            outerTbl.addCell(cell8);
			outerTbl.addCell(cell9);
			outerTbl.addCell(cell10);
			outerTbl.addCell(cell11);
			
	if(employee.getNominationList()!=null){
		for (EmployeeFamilyDeatails employeedetails : employee.getNominationList()) {

			System.out.println("nominationlist"+ employee.getNominationList().size());
			PdfPCell nomname = null;
			if (employeedetails != null&& employeedetails.getFullName() != null) {
				nomname = cell.getCell(employeedetails.getFullName(),font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				nomname = cell.getCell("", font8bold, Element.ALIGN_CENTER, 0,0, 0);
			}

			PdfPCell nomrel = null;
			if (employeedetails != null&& employeedetails.getFamilyRelation() != null) {
				nomrel = cell.getCell(employeedetails.getFamilyRelation(),font8bold, Element.ALIGN_CENTER, 0, 0, 0);
				nomrel.setBorderWidthBottom(0);
			} else {
				nomrel = cell.getCell("", font8bold, Element.ALIGN_CENTER, 0,0, 0);
				nomrel.setBorder(0);
			}

			PdfPCell nomage = null;
			if (employeedetails != null && employeedetails.getDob() != null) {
				nomage = cell.getCell(getAgeAsOnDate(employeedetails.getDob())+ "", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
				nomage.setBorderWidthBottom(0);
				System.out.println("ageasondate"+ getAgeAsOnDate(employeedetails.getDob()));
			} else {
				nomage = cell.getCell("", font8bold, Element.ALIGN_CENTER, 0,0, 0);
				nomage.setBorderWidthBottom(0);
			}

			PdfPCell nomproportion = null;
			if (employeedetails != null && employeedetails.getNomineeShare() != 0) {
				nomproportion = cell.getCell(employeedetails.getNomineeShare()+ "", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
				nomproportion.setBorderWidthRight(0);
				nomproportion.setBorderWidthBottom(0);
			} else {
				nomproportion = cell.getCell("", font8bold,Element.ALIGN_CENTER, 0, 0, 0);
				nomproportion.setBorderWidthRight(0);
				nomproportion.setBorderWidthBottom(0);
			}

			PdfPCell nomadd = null;
			if (employeedetails != null && employeedetails.getAddress() != null) {
				nomadd = cell.getCell(employeedetails.getAddress(), font8bold,Element.ALIGN_LEFT, 0, 0, 0);
			} else {
				nomadd = cell.getCell("", font8bold, Element.ALIGN_CENTER, 0,0, 0);
			}
			nomadd.setBorderWidthLeft(0);

			PdfPCell blk1 = cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0,0);
			blk1.setBorderWidthTop(0);

			PdfPCell dob = null;
			if (employeedetails != null && employeedetails.getDob() != null) {
				dob = cell.getCell(format.format(employeedetails.getDob()) + "", font8,Element.ALIGN_CENTER, 0, 0, 0);
			} else {
				dob = cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0);
			}
			dob.setBorderWidthTop(0);

			PdfPCell blk3 = cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0,0);
			blk3.setBorderWidthTop(0);
			blk3.setBorderWidthRight(0);

			nomname.setBorderWidthLeft(0);
			nomname.setBorderWidthBottom(0);
			
			outerTbl.addCell(nomname);
			outerTbl.addCell(nomrel);
			outerTbl.addCell(nomage);
			outerTbl.addCell(nomproportion);

			outerTbl.addCell(nomadd);
			outerTbl.addCell(blk1);
			outerTbl.addCell(dob);
			outerTbl.addCell(blk3);

		}
	}
			
			
			
			
			
			PdfPCell cell14=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			cell14.setBorderWidthBottom(0);
			cell14.setBorderWidthLeft(0);
			cell14.setBorderWidthRight(0);
			PdfPCell cell15=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			cell15.setBorderWidthBottom(0);
			cell15.setBorderWidthLeft(0);
			cell15.setBorderWidthRight(0);
			PdfPCell cell16=cell.getCell("", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			cell16.setBorderWidthBottom(0);
			cell16.setBorderWidthLeft(0);
			cell16.setBorderWidthRight(0);
			PdfPCell cell17=cell.getCell("P.T.O", font8bold, Element.ALIGN_RIGHT, 0, 0, 0);
			cell17.setBorderWidthBottom(0);
			cell17.setBorderWidthLeft(0);
			cell17.setBorderWidthRight(0);
		
			
			outerTbl.addCell(cell14);
			outerTbl.addCell(cell15);
			outerTbl.addCell(cell16);
			outerTbl.addCell(cell17);
			
	
			try {
				document.add(outerTbl);
				document.newPage();
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}
		
		
		private void writeFooterTable(PdfWriter writer) {
	    	
	    	PdfPTable table=new PdfPTable(1);
	    	table.setWidthPercentage(100f);
	    	table.addCell(cell.getCell("P.T.O", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
	    	
	        final int FIRST_ROW = 0;
	        final int LAST_ROW = -1;
	        //Table must have absolute width set.
	        if(table.getTotalWidth()==0)
	            table.setTotalWidth((document.right()-document.left())*table.getWidthPercentage()/100f);
	        table.writeSelectedRows(FIRST_ROW, LAST_ROW, document.left(), document.bottom()+table.getTotalHeight(),writer.getDirectContent());
	        try {
				document.add(table);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }
		
		
		
		
		
		private  int getAgeAsOnDate(Date dob){
	        Date now = new Date();
	        long timeBetween = now.getTime() - dob.getTime();
	        double yearsBetween = timeBetween / 3.15576e+10;
	        int age = (int) Math.floor(yearsBetween);
	        return age;
		}
		


		    private void  statement2(){
			PdfPTable outerTbl=new PdfPTable(3);
			outerTbl.setWidthPercentage(100);
			try {
				outerTbl.setWidths(new float[] {20,60,20});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell blank=cell.getCell("  ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			blank.setBorder(0);
			PdfPCell cell1=cell.getCell("  ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			cell1.setBorder(0);
			PdfPCell cell2=cell.getCell("Emp Code :"+employee.getCount(), font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell2.setBorder(0);
			outerTbl.addCell(blank);
	      	outerTbl.addCell(cell1);
			outerTbl.addCell(cell2);
			
			PdfPCell blank1=cell.getCell("  ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			blank1.setBorder(0);
			PdfPCell cell3=cell.getCell("2", font8bold, Element.ALIGN_CENTER, 0, 0, 10);
			cell3.setBorder(0);
			PdfPCell cell4=cell.getCell("Category :"+employee.getEmployeeType(), font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell4.setBorder(0);
			outerTbl.addCell(blank1);
	      	outerTbl.addCell(cell3);
			outerTbl.addCell(cell4);
			
			
			PdfPCell blank2=cell.getCell("  ", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			blank2.setBorder(0);
			PdfPCell cell5=cell.getCell("STATEMENT", font9bold, Element.ALIGN_CENTER, 0, 0,0);
			cell5.setBorder(0);
			PdfPCell cell6=cell.getCell("Tel No. :"+employee.getCellNumber1(), font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell6.setBorder(0);
			outerTbl.addCell(blank2);
	      	outerTbl.addCell(cell5);
			outerTbl.addCell(cell6);
			
		
			PdfPTable outerTbl2=new PdfPTable(3);
			outerTbl2.setWidthPercentage(100);
			try {
				outerTbl2.setWidths(new float[] {3,40,57});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			    PdfPCell	  cellsrno=cell.getCell("1.", font10, Element.ALIGN_LEFT, 0, 0, 18);
			    cellsrno.setBorderWidthLeft(0);
			    cellsrno.setBorderWidthRight(0);
			    cellsrno.setBorderWidthTop(0);
			PdfPCell cell7=cell.getCell("Name of the employee in full.", font10, Element.ALIGN_LEFT, 0, 0, 18);
			cell7.setBorderWidthLeft(0);
			cell7.setBorderWidthRight(0);
			cell7.setBorderWidthTop(0);
			PdfPCell cell8=null;
			if(employee !=null && employee.getFullName() !=null){
			     cell8=cell.getCell(employee.getFullName(), font9bold, Element.ALIGN_LEFT, 0, 0, 18);
				cell8.setBorderWidthLeft(0);
				cell8.setBorderWidthRight(0);
				cell8.setBorderWidthTop(0);
				
			}else{
				 cell8=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 18);
				cell8.setBorderWidthLeft(0);
				cell8.setBorderWidthRight(0);
				cell8.setBorderWidthTop(0);
				
			}
			
			outerTbl2.addCell(cellsrno);
			outerTbl2.addCell(cell7);
			outerTbl2.addCell(cell8);
			
			
			
			PdfPTable outerTbl3=new PdfPTable(3);
			outerTbl3.setWidthPercentage(100);
			try {
				outerTbl3.setWidths(new float[] {3,15,82});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPCell srno1=cell.getCell("2.", font10, Element.ALIGN_LEFT, 0, 0,18);
			srno1.setBorderWidthLeft(0);
			srno1.setBorderWidthRight(0);
			srno1.setBorderWidthTop(0);
			
			PdfPCell cell9=cell.getCell("Sex ", font10, Element.ALIGN_LEFT, 0, 0,18);
			cell9.setBorderWidthLeft(0);
			cell9.setBorderWidthRight(0);
			cell9.setBorderWidthTop(0);
			
			PdfPCell cell10=null;
			if(employee !=null && employee.getGender() !=null){
				 cell10=cell.getCell(employee.getGender(), font9bold, Element.ALIGN_LEFT, 0, 0,18);
				cell10.setBorderWidthLeft(0);
				cell10.setBorderWidthRight(0);
				cell10.setBorderWidthTop(0);
			}else{
				cell10=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0,18);
				cell10.setBorderWidthLeft(0);
				cell10.setBorderWidthRight(0);
				cell10.setBorderWidthTop(0);
			}
			
			PdfPCell srno2=cell.getCell("3.", font10, Element.ALIGN_LEFT, 0, 0,18);
			srno2.setBorderWidthLeft(0);
			srno2.setBorderWidthRight(0);
			srno2.setBorderWidthTop(0);
			

			PdfPCell cell11=cell.getCell("Religion ", font10, Element.ALIGN_LEFT, 0, 0, 18);
			cell11.setBorderWidthLeft(0);
			cell11.setBorderWidthRight(0);
			cell11.setBorderWidthTop(0);
			
			    PdfPCell cell12=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 18);
				cell12.setBorderWidthLeft(0);
				cell12.setBorderWidthRight(0);
				cell12.setBorderWidthTop(0);
		
			
			
		    outerTbl3.addCell(srno1);
			outerTbl3.addCell(cell9);
			outerTbl3.addCell(cell10);
			outerTbl3.addCell(srno2);
			outerTbl3.addCell(cell11);
			outerTbl3.addCell(cell12);
			
			PdfPTable outerTbl4=new PdfPTable(3);
			outerTbl4.setWidthPercentage(100);
			try {
				outerTbl4.setWidths(new float[] {3,50,47});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPCell srno3=cell.getCell("4.", font10, Element.ALIGN_LEFT, 0, 0,18);
			srno3.setBorderWidthLeft(0);
			srno3.setBorderWidthRight(0);
			srno3.setBorderWidthTop(0);
			
			PdfPCell cell13=cell.getCell("Whether unmarried / married / widow / widower", font10, Element.ALIGN_LEFT, 0, 0, 18);
			cell13.setBorderWidthLeft(0);
			cell13.setBorderWidthRight(0);
			cell13.setBorderWidthTop(0);
			PdfPCell cell14=null;
			if(employee !=null && employee.getMaritalStatus() !=null){
				cell14=cell.getCell(employee.getMaritalStatus(), font9bold, Element.ALIGN_LEFT, 0, 0, 18);
				cell14.setBorderWidthLeft(0);
				cell14.setBorderWidthRight(0);
				cell14.setBorderWidthTop(0);
			}else{
				cell14=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 18);
				cell14.setBorderWidthLeft(0);
				cell14.setBorderWidthRight(0);
				cell14.setBorderWidthTop(0);
			}
			
			
			PdfPCell srno4=cell.getCell("5.", font10, Element.ALIGN_LEFT, 0, 0,18);
			srno4.setBorderWidthLeft(0);
			srno4.setBorderWidthRight(0);
			srno4.setBorderWidthTop(0);
			
			
			PdfPCell cell15=cell.getCell("Department / Branch / Section No.if any ", font10, Element.ALIGN_LEFT, 0, 0, 18);
			cell15.setBorderWidthLeft(0);
			cell15.setBorderWidthRight(0);
			cell15.setBorderWidthTop(0);
			
			PdfPCell cell16=cell.getCell(employee.getDepartMent()+"/"+employee.getBranchName(), font9bold, Element.ALIGN_LEFT, 0, 0, 18);
			cell16.setBorderWidthLeft(0);
			cell16.setBorderWidthRight(0);
			cell16.setBorderWidthTop(0);
			
			PdfPCell srno5=cell.getCell("6.", font10, Element.ALIGN_LEFT, 0, 0,18);
			srno5.setBorderWidthLeft(0);
			srno5.setBorderWidthRight(0);
			srno5.setBorderWidthTop(0);
			
			PdfPCell cell17=cell.getCell("Post held with Ticket No. or Serial No. if any", font10, Element.ALIGN_LEFT, 0, 0, 18);
			cell17.setBorderWidthLeft(0);
			cell17.setBorderWidthRight(0);
			cell17.setBorderWidthTop(0);
			
			PdfPCell cell18=cell.getCell(employee.getCount()+" "+employee.getDesignation(), font9bold, Element.ALIGN_LEFT, 0, 0,18);
			cell18.setBorderWidthLeft(0);
			cell18.setBorderWidthRight(0);
			cell18.setBorderWidthTop(0);
			
			outerTbl4.addCell(srno3);
			outerTbl4.addCell(cell13);
			outerTbl4.addCell(cell14);
			outerTbl4.addCell(srno4);
			outerTbl4.addCell(cell15);
			outerTbl4.addCell(cell16);
			outerTbl4.addCell(srno5);
			outerTbl4.addCell(cell17);
			outerTbl4.addCell(cell18);
			
			
			PdfPTable outerTbl5=new PdfPTable(3);
			outerTbl5.setWidthPercentage(100);
			try {
				outerTbl5.setWidths(new float[] {3,30,67});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPCell srno6=cell.getCell("7.", font10, Element.ALIGN_LEFT, 0, 0,18);
			srno6.setBorderWidthLeft(0);
			srno6.setBorderWidthRight(0);
			srno6.setBorderWidthTop(0);
			
			
			
			PdfPCell cell19=cell.getCell("Date of Appointment", font10, Element.ALIGN_LEFT, 0, 0, 18);
			cell19.setBorderWidthLeft(0);
			cell19.setBorderWidthRight(0);
			cell19.setBorderWidthTop(0);
			
		
				PdfPCell cell20=cell.getCell(format.format(employee.getJoinedAt())+"", font9bold, Element.ALIGN_LEFT, 0, 0, 18);
				cell20.setBorderWidthLeft(0);
				cell20.setBorderWidthRight(0);
				cell20.setBorderWidthTop(0);
			
				PdfPCell srno7=cell.getCell("8.", font10, Element.ALIGN_LEFT, 0, 0,0);
				srno7.setBorderWidthLeft(0);
				srno7.setBorderWidthRight(0);
				srno7.setBorderWidthTop(0);
				
				
			PdfPCell cell21=cell.getCell("Permanent address", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell21.setBorderWidthLeft(0);
			cell21.setBorderWidthRight(0);
			cell21.setBorderWidthTop(0);
			
			PdfPCell cell22=null;
			if(employee !=null && employee.getAddress().getCompleteAddress() !=null){
				cell22=cell.getCell(employee.getAddress().getCompleteAddress() +"", font9bold, Element.ALIGN_LEFT, 0, 0, 0);
				cell22.setBorderWidthLeft(0);
				cell22.setBorderWidthRight(0);
				cell22.setBorderWidthTop(0);

			}else{
				 cell22=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
				cell22.setBorderWidthLeft(0);
				cell22.setBorderWidthRight(0);
				cell22.setBorderWidthTop(0);

			}
			
			outerTbl5.addCell(srno6);		
			outerTbl5.addCell(cell19);
			outerTbl5.addCell(cell20);
			outerTbl5.addCell(srno7);
			outerTbl5.addCell(cell21);
			outerTbl5.addCell(cell22);
			
			PdfPTable outerTbl6=new PdfPTable(7);
			outerTbl6.setWidthPercentage(100);
			try {
				outerTbl6.setWidths(new float[] {2,13,20,13,20,12,20});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell blank11=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			blank11.setBorder(0);
			PdfPCell cell23=cell.getCell("Village ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell23.setBorder(0);
		
			PdfPCell cell24=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell24.setBorderWidthLeft(0);
			cell24.setBorderWidthRight(0);
			cell24.setBorderWidthTop(0);
			PdfPCell cell25=cell.getCell("Thana ", font10, Element.ALIGN_LEFT, 0, 0,0);
			cell25.setBorder(0);
			
			PdfPCell cell26=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0,0);
			cell26.setBorderWidthLeft(0);
			cell26.setBorderWidthRight(0);
			cell26.setBorderWidthTop(0);
			PdfPCell cell27=cell.getCell("Sub-division ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell27.setBorder(0);
			
			PdfPCell cell28=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0,0);
			cell28.setBorderWidthLeft(0);
			cell28.setBorderWidthRight(0);
			cell28.setBorderWidthTop(0);
			
			outerTbl6.addCell(blank11);
			outerTbl6.addCell(cell23);
			outerTbl6.addCell(cell24);
			outerTbl6.addCell(cell25);
			outerTbl6.addCell(cell26);
			outerTbl6.addCell(cell27);
			outerTbl6.addCell(cell28);
			PdfPCell blank12=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			blank12.setBorder(0);
			PdfPCell cell29=cell.getCell("Post Office ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell29.setBorder(0);
			PdfPCell cell30=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell30.setBorderWidthLeft(0);
			cell30.setBorderWidthRight(0);
			cell30.setBorderWidthTop(0);
			PdfPCell cell31=cell.getCell("District ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell31.setBorder(0);
			
			PdfPCell cell32=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell32.setBorderWidthLeft(0);
			cell32.setBorderWidthRight(0);
			cell32.setBorderWidthTop(0);
			PdfPCell cell33=cell.getCell("State ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell33.setBorder(0);
			PdfPCell cell34=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell34.setBorderWidthLeft(0);
			cell34.setBorderWidthRight(0);
			cell34.setBorderWidthTop(0);
			
			outerTbl6.addCell(blank12);
			outerTbl6.addCell(cell29);
			outerTbl6.addCell(cell30);
			outerTbl6.addCell(cell31);
			outerTbl6.addCell(cell32);
			outerTbl6.addCell(cell33);
			outerTbl6.addCell(cell34);
			
			PdfPTable outerTbl7=new PdfPTable(4);
			outerTbl7.setWidthPercentage(100);
			try {
				outerTbl7.setWidths(new float[] {10,10,35,45});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell35=cell.getCell("Place : ", font10, Element.ALIGN_LEFT, 0, 0, 20);
			cell35.setBorder(0);
			PdfPCell cell36=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20);
			cell36.setBorder(0);
			PdfPCell cell37=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 20);
			cell37.setBorder(0);
			PdfPCell cell38=cell.getCell("X", font10, Element.ALIGN_LEFT, 0, 0, 20);
			cell38.setBorder(0);
			
			
			outerTbl7.addCell(cell35);
			outerTbl7.addCell(cell36);
			outerTbl7.addCell(cell37);
			outerTbl7.addCell(cell38);
			
			PdfPCell cell39=cell.getCell("Date : ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell39.setBorderWidthLeft(0);
			cell39.setBorderWidthRight(0);
			cell39.setBorderWidthTop(0);
			PdfPCell cell40=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell40.setBorderWidthLeft(0);
			cell40.setBorderWidthRight(0);
			cell40.setBorderWidthTop(0);
			PdfPCell cell41=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell41.setBorderWidthLeft(0);
			cell41.setBorderWidthRight(0);
			cell41.setBorderWidthTop(0);
			PdfPCell cell42=cell.getCell(" Signature / Thumb-Impression of the Employee", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell42.setBorderWidthLeft(0);
			cell42.setBorderWidthRight(0);
			cell42.setBorderWidthTop(0);
			
			outerTbl7.addCell(cell39);
			outerTbl7.addCell(cell40);
			outerTbl7.addCell(cell41);
			outerTbl7.addCell(cell42);
			
			try {
				document.add(outerTbl);
				document.add(outerTbl2);
				document.add(outerTbl3);
				document.add(outerTbl4);
				document.add(outerTbl5);
				document.add(outerTbl6);
				document.add(outerTbl7);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		private  void certificateByTheEmployer(){
			
			PdfPTable outerTbl1=new PdfPTable(1);
			outerTbl1.setWidthPercentage(100);
			PdfPCell cell43=cell.getCell("CERTIFICATE BY THE EMPLOYER ", font10boldul, Element.ALIGN_CENTER, 0, 0, 30);
			cell43.setBorder(0);
			PdfPCell cell44=cell.getCell("Certified that the particulars of the above nomination have been verified and recorded in this establishment. ", font10, Element.ALIGN_CENTER, 0, 0, 0);
			cell44.setBorder(0);
			outerTbl1.addCell(cell43);
			outerTbl1.addCell(cell44);
			
			PdfPTable outerTbl2=new PdfPTable(3);
			outerTbl2.setWidthPercentage(100);
			try {
				outerTbl2.setWidths(new float[] {49,2,49});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell45=cell.getCell("Employer's Reference No. if any", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell45.setBorderWidthLeft(0);
			cell45.setBorderWidthRight(0);
			cell45.setBorderWidthTop(0);
			PdfPCell cell46=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell46.setBorder(0);
			PdfPCell cell47=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell47.setBorderWidthLeft(0);
			cell47.setBorderWidthRight(0);
			cell47.setBorderWidthTop(0);
			
			PdfPCell cell48=cell.getCell(company.getBusinessUnitName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			cell48.setBorderWidthBottom(0);
			PdfPCell cell49=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell49.setBorder(0);
			PdfPCell cell50=cell.getCell("For "+company.getBusinessUnitName(), font8bold, Element.ALIGN_RIGHT, 0, 0, 20);
			cell50.setBorderWidthBottom(0);
			
			PdfPCell cell51=cell.getCell(company.getAddress().getCompleteAddress(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			cell51.setBorderWidthBottom(0);
			cell51.setBorderWidthTop(0);
			PdfPCell cell52=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell52.setBorder(0);
			PdfPCell cell53=cell.getCell(" PROPRIETOR", font9bold, Element.ALIGN_RIGHT, 0, 0, 20);
			cell53.setBorderWidthBottom(0);
			cell53.setBorderWidthTop(0);
			
			PdfPCell cell54=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell54.setBorderWidthBottom(0);
			cell54.setBorderWidthTop(0);
			PdfPCell cell55=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell55.setBorder(0);
			PdfPCell cell56=cell.getCell("Signature of the Employer / Authorised Officer  ", font7, Element.ALIGN_LEFT, 0, 2, 0);
			cell56.setBorderWidthTop(0);
			cell56.setBorderWidthBottom(0);
			
			PdfPCell cell57=cell.getCell("Name and address of the establishment or rubber stamp thereof. ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell57.setBorderWidthTop(0);
			PdfPCell cell58=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell58.setBorder(0);
			PdfPCell cell59=cell.getCell("Designation: ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell59.setBorderWidthTop(0);
//			PdfPCell cell60=cell.getCell("Date ", font7, Element.ALIGN_LEFT, 0, 0, 0);
//			cell60.setBorderWidthTop(0);
//			
			outerTbl2.addCell(cell45);
			outerTbl2.addCell(cell46);
			outerTbl2.addCell(cell47);
			outerTbl2.addCell(cell48);
			outerTbl2.addCell(cell49);
			outerTbl2.addCell(cell50);
			outerTbl2.addCell(cell51);
			outerTbl2.addCell(cell52);
			outerTbl2.addCell(cell53);
			outerTbl2.addCell(cell54);
			outerTbl2.addCell(cell55);
			outerTbl2.addCell(cell56);
		
			outerTbl2.addCell(cell57);
		    outerTbl2.addCell(cell58);
			outerTbl2.addCell(cell59);
//			outerTbl2.addCell(cell60);
			
			
			try {
				document.add(outerTbl1);
				document.add(outerTbl2);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
		}
		private void acknowledgementByTheEmployee(){
			PdfPTable outerTbl1=new PdfPTable(1);
			outerTbl1.setWidthPercentage(100);
			PdfPCell cell61=cell.getCell("ACKNOWLEDGEMENT BY THE EMPLOYEE", font10boldul, Element.ALIGN_CENTER, 0, 0, 30);
			cell61.setBorder(0);
			PdfPCell cell62=cell.getCell("Received the duplicate copy of nomination in Form 'F' filed by me and duly certified by the employer.", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell62.setBorder(0);
			outerTbl1.addCell(cell61);
			outerTbl1.addCell(cell62);
			
			PdfPTable outerTbl2=new PdfPTable(4);
			outerTbl2.setWidthPercentage(100);
			try {
				outerTbl2.setWidths(new float[] {7,43,10,40});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPCell cell63=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell63.setBorder(0);
			PdfPCell cell64=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell64.setBorder(0);
			PdfPCell cell65=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell65.setBorder(0);
			PdfPCell cell66=cell.getCell("X ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell66.setBorder(0);
			
			PdfPCell cell67=cell.getCell("Date:", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell67.setBorder(0);
			PdfPCell cell68=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell68.setBorder(0);
			PdfPCell cell69=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell69.setBorder(0);
			PdfPCell cell70=cell.getCell("Signature of the employee ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell70.setBorder(0);
			
			PdfPCell cell71=cell.getCell("Note :", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell71.setBorder(0);
			PdfPCell cell72=cell.getCell("Strike out the words / paragraph not applicable. ", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell72.setBorder(0);
			PdfPCell cell73=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell73.setBorder(0);
			PdfPCell cell74=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell74.setBorder(0);
			outerTbl2.addCell(cell63);
			outerTbl2.addCell(cell64);
			outerTbl2.addCell(cell65);
			outerTbl2.addCell(cell66);
			outerTbl2.addCell(cell67);
			outerTbl2.addCell(cell68);
			outerTbl2.addCell(cell69);
			outerTbl2.addCell(cell70);
			outerTbl2.addCell(cell71);
			outerTbl2.addCell(cell72);
			outerTbl2.addCell(cell73);
			outerTbl2.addCell(cell74);
			try {
				document.add(outerTbl1);
				document.add(outerTbl2);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			}
		private void declarationByWitness(){
			PdfPTable outerTbl1=new PdfPTable(1);
			outerTbl1.setWidthPercentage(100);
			PdfPCell cell75=cell.getCell("DECLARATION BY WITNESS", font10boldul, Element.ALIGN_CENTER, 0, 0, 30);
			cell75.setBorder(0);
			PdfPCell cell76=cell.getCell("Nomination signed /thumb-impression before me", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell76.setBorder(0);
			outerTbl1.addCell(cell75);
			outerTbl1.addCell(cell76);
			
			PdfPTable outerTbl2=new PdfPTable(2);
			outerTbl2.setWidthPercentage(100);
			try {
				outerTbl2.setWidths(new float[] {70,30});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell77=cell.getCell("Name in full and full address", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell77.setBorder(0);
			PdfPCell cell78=cell.getCell("Signature of witness", font10, Element.ALIGN_LEFT, 0, 0, 0);
			cell78.setBorder(0);
			outerTbl2.addCell(cell77);
			outerTbl2.addCell(cell78);
			
			PdfPTable outerTbl3=new PdfPTable(3);
			outerTbl3.setWidthPercentage(100);
			try {
				outerTbl3.setWidths(new float[] {5,65,30});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell79=cell.getCell("1.", font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell79.setBorder(0);
			PdfPCell cell80=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell80.setBorderWidthTop(0);
			cell80.setBorderWidthRight(0);
			cell80.setBorderWidthLeft(0);
			PdfPCell cell81=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell81.setBorder(0);
			
			PdfPCell cell82=cell.getCell("", font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell82.setBorder(0);
			PdfPCell cell83=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell83.setBorderWidthTop(0);
			cell83.setBorderWidthRight(0);
			cell83.setBorderWidthLeft(0);
			PdfPCell cell84=cell.getCell("1.", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell84.setBorder(0);
			
			PdfPCell cell85=cell.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell85.setBorderWidthTop(0);
			cell85.setBorderWidthRight(0);
			cell85.setBorderWidthLeft(0);
			PdfPCell cell86=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell86.setBorderWidthTop(0);
			cell86.setBorderWidthRight(0);
			cell86.setBorderWidthLeft(0);
			PdfPCell cell87=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell87.setBorderWidthTop(0);
			cell87.setBorderWidthRight(0);
			cell87.setBorderWidthLeft(0);
			
			
	
			PdfPCell cell88=cell.getCell("2.", font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell88.setBorder(0);
	        PdfPCell cell89=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell89.setBorderWidthTop(0);
			cell89.setBorderWidthRight(0);
			cell89.setBorderWidthLeft(0);
			PdfPCell cell90=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell90.setBorder(0);
		
			
			PdfPCell cell91=cell.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell91.setBorder(0);
			PdfPCell cell92=cell.getCell(" ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell92.setBorderWidthTop(0);
			cell92.setBorderWidthRight(0);
			cell92.setBorderWidthLeft(0);
			PdfPCell cell93=cell.getCell("2.", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell93.setBorder(0);
			
			
			PdfPCell cell94=cell.getCell("   ", font7, Element.ALIGN_RIGHT, 0, 0, 0);
			cell94.setBorderWidthTop(0);
			cell94.setBorderWidthRight(0);
			cell94.setBorderWidthLeft(0);
			PdfPCell cell95=cell.getCell("   ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell95.setBorderWidthTop(0);
			cell95.setBorderWidthRight(0);
			cell95.setBorderWidthLeft(0);
			PdfPCell cell96=cell.getCell("   ", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell96.setBorderWidthTop(0);
			cell96.setBorderWidthRight(0);
			cell96.setBorderWidthLeft(0);
			
			outerTbl3.addCell(cell79);
			outerTbl3.addCell(cell80);
			outerTbl3.addCell(cell81);
			outerTbl3.addCell(cell82);
			outerTbl3.addCell(cell83);
			outerTbl3.addCell(cell84);
			outerTbl3.addCell(cell85);
			outerTbl3.addCell(cell86);
			outerTbl3.addCell(cell87);
			outerTbl3.addCell(cell88);
			outerTbl3.addCell(cell89);
			outerTbl3.addCell(cell90);
			outerTbl3.addCell(cell91);
			outerTbl3.addCell(cell92);
			outerTbl3.addCell(cell93);
			outerTbl3.addCell(cell94);
			outerTbl3.addCell(cell95);
			outerTbl3.addCell(cell96);
			
			
			PdfPTable outerTbl4=new PdfPTable(6);
			outerTbl4.setWidthPercentage(100);
			try {
				outerTbl4.setWidths(new float[] {10,10,30,10,10,30});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPCell cell97=cell.getCell("Place :", font10, Element.ALIGN_RIGHT, 0, 0, 0);
			cell97.setBorderWidthTop(0);
			cell97.setBorderWidthRight(0);
			cell97.setBorderWidthLeft(0);
			PdfPCell cell98=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell98.setBorderWidthTop(0);
			cell98.setBorderWidthRight(0);
			cell98.setBorderWidthLeft(0);
			PdfPCell cell99=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell99.setBorderWidthTop(0);
			cell99.setBorderWidthRight(0);
			cell99.setBorderWidthLeft(0);
			PdfPCell cell100=cell.getCell("Date :", font10, Element.ALIGN_RIGHT, 0, 0, 0);
			cell100.setBorderWidthTop(0);
			cell100.setBorderWidthRight(0);
			cell100.setBorderWidthLeft(0);
			PdfPCell cell101=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell101.setBorderWidthTop(0);
			cell101.setBorderWidthRight(0);
			cell101.setBorderWidthLeft(0);
			PdfPCell cell102=cell.getCell("", font7, Element.ALIGN_LEFT, 0, 0, 0);
			cell102.setBorderWidthTop(0);
			cell102.setBorderWidthRight(0);
			cell102.setBorderWidthLeft(0);
			
			outerTbl4.addCell(cell97);
			outerTbl4.addCell(cell98);
			outerTbl4.addCell(cell99);
			outerTbl4.addCell(cell100);
			outerTbl4.addCell(cell101);
			outerTbl4.addCell(cell102);
			
			
			try {
				document.add(outerTbl1);
				document.add(outerTbl2);
				document.add(outerTbl3);
				document.add(outerTbl4);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
			
			
		}
}
