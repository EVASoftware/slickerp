package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gwt.user.client.ui.ValueBoxBase.TextAlignment;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.server.addhocprinting.sasha.Form11Pdf;
import com.smartgwt.client.types.VerticalAlignment;

public class HrComplianceFormServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1529821264998423562L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.

		String stringid = request.getParameter("Id"); //Company Id
		String pdfType = request.getParameter("type");
		String subtype = request.getParameter("subtype");
		stringid = stringid.trim();
		Long count = Long.parseLong(stringid);
		Document document = new Document();
		PfReportPdf pfPdf = null;
		EsicReportpdf esicPdf = null;
		LwfReportPdf lwfPdf = null;
		Form3aPdf form3A=null;
		FormDpdf formD=null;
		FormCpdf formC=null;
		Form9pdf form9=null;
		Form2 form2=null;
		Form6aPdf form6a=null;
		
		/**
		 * Updated By: Viraj
		 * Date: 02-02-2019
		 * Description: To add Form2A revised
		 */
		Form2aRevisedPdf form2Arev = null;
		/**
		 * @author Anil ,Date : 17-04-2019
		 */
		FnfPdf fnf=null;
		
		
		/**Date 25-3-2019 added by Amol
		 * for FormA employee Register
		 * 
		 */
		FormA formA=null;
		Form11Pdf form11=null;	
		FormFPdf formf=null;
		
		/**
		 * @author Anil , Date : 10-08-2019
		 */
		FormXVIIpdf formXVII=null;
		/**
		 * @author Anil
		 * @since 10-08-2020
		 */
		FormD_MusterRoll formD_MusterRoll=null;
		
		if (pdfType.equalsIgnoreCase("PFreport")) {
			pfPdf = new PfReportPdf();
			pfPdf.document = new Document(PageSize.A4.rotate());
			document = pfPdf.document;
		} else if (pdfType.equalsIgnoreCase("ESICReport")) {
			esicPdf = new EsicReportpdf();
			esicPdf.document = new Document(PageSize.A4.rotate());
			document = esicPdf.document;
		}else if (pdfType.equalsIgnoreCase("LWFReport")) {
			lwfPdf = new LwfReportPdf();
			lwfPdf.document = new Document(PageSize.A4.rotate());
			document = lwfPdf.document;
		}else if (pdfType.equalsIgnoreCase("FORM3A")) {
			form3A = new Form3aPdf();
			form3A.document = new Document();
			document = form3A.document;
		}
		else if (pdfType.equalsIgnoreCase("FormD")) {
			formD = new FormDpdf();
			formD.document = new Document();
			formD.document = new Document(PageSize.A4.rotate());
			document = formD.document;
		}
		else if (pdfType.equalsIgnoreCase("FormC")) {
			formC = new FormCpdf();
			formC.document = new Document();
			formC.document = new Document(PageSize.A4.rotate());
			document = formC.document;
			document.setMargins(10,10,10,10);
		}
		
		else if (pdfType.equalsIgnoreCase("Form9")) {
			form9 = new Form9pdf();
			form9.document = new Document();
			form9.document = new Document(PageSize.A4.rotate());
			document = form9.document;
		}
		else if (pdfType.equalsIgnoreCase("Form2")) {
			form2 = new Form2();
			form2.document = new Document();
			form2.document = new Document(PageSize.A4.rotate());
			document = form2.document;
			document.setMargins(2,2,2,2);
		}
		else if (pdfType.equalsIgnoreCase("Form6A")) {
			form6a = new Form6aPdf();
			form6a.document = new Document();
//			form6a.document = new Document(PageSize.A4.rotate());
			document = form6a.document;
			document.setMargins(10,10,10,10);
		}
		else if(pdfType.equalsIgnoreCase("Form2A(Revised)")) {
			form2Arev = new Form2aRevisedPdf();
			form2Arev.document = new Document();
			document = form2Arev.document;
		}else if(pdfType.equalsIgnoreCase("FnF")) {
			fnf = new FnfPdf();
			fnf.document = new Document();
			document = fnf.document;
		}else if(pdfType.equalsIgnoreCase("FormA")){
			formA=new FormA();
			formA.document=new Document();
			formA.document=new Document(PageSize.A2.rotate());
			document.setMargins(2,2,2,2);
			document=formA.document;
		}
		else if(pdfType.equalsIgnoreCase("FORMF")){
			formf=new FormFPdf();
			formf.document=new Document();
			document=formf.document;
	    }else if(pdfType.equalsIgnoreCase("Form11")) {
			form11 = new Form11Pdf();
			form11.document = new Document();
			document = form11.document;
		}else if(pdfType.equalsIgnoreCase("FORM XVII")) {
			formXVII = new FormXVIIpdf();
			formXVII.document =new Document(PageSize.A4.rotate(),0,0,0,0);
			document = formXVII.document;
			
		}else if(pdfType.equalsIgnoreCase("FormD_MusterRoll")) {
			formD_MusterRoll = new FormD_MusterRoll();
			formD_MusterRoll.document =new Document(PageSize.A4.rotate(),2,2,2,2);
//			formD_MusterRoll.document =new Document(PageSize.A4.rotate());
			document = formD_MusterRoll.document;
		}
		
		
		
		PdfWriter writer=null;
		try {
			writer = PdfWriter.getInstance(document,
					response.getOutputStream());
		} catch (DocumentException e) {
			e.printStackTrace();
		} // write the pdf in

		document.open();
		String preprintStatus = request.getParameter("preprint");
		if (pdfType.equalsIgnoreCase("PFreport")) {
			pfPdf.loadEmployeedata(count);
			pfPdf.createPFReportPdf();
		} else if (pdfType.equalsIgnoreCase("ESICReport")) {
			esicPdf.loadEmployeedata(count);
			esicPdf.createEsicReportpdf();
		} else if (pdfType.equalsIgnoreCase("LWFReport")) {
			lwfPdf.loadEmployeedata(count);
			lwfPdf.createLwfReportPdf();
		} else if (pdfType.equalsIgnoreCase("FORM3A")) {
			int empId=Integer.parseInt(subtype.trim());
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			form3A.setForm3aData(count, empId, fromDate,toDate);
			form3A.createPdf();
		}
		
		else if (pdfType.equalsIgnoreCase("FormD")) {
			int empId=Integer.parseInt(subtype.trim());
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
			formD.setformDpdfdata(count, empId, fromDate,toDate);
			formD.createPdf();
		}
		
        else if (pdfType.equalsIgnoreCase("FormC")) {
        	int empId=Integer.parseInt(subtype.trim());
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
        	formC.setFormCdata(count, empId, fromDate,toDate);
			formC.createPdf();
			formC.addFooter(writer);
					
		}
        else if (pdfType.equalsIgnoreCase("Form9")) {
        	int empId=Integer.parseInt(subtype.trim());
        	String branch=request.getParameter("Branch");
        	String project=request.getParameter("Project");
        	
        	if(branch.equals("null")){
        		branch=null;
        	}
        	if(project.equals("null")){
        		project=null;
        	}
        	form9.setform9pdfdata(count, empId,branch,project);
			form9.createPdf();
		}
        else if (pdfType.equalsIgnoreCase("Form2")) {
        	form2.paySlipList=CsvWriter.paySlipList;
        	form2.setform2data(count);
			form2.createPdf();
		}
        else if (pdfType.equalsIgnoreCase("Form6A")) {
//        	int empId=Integer.parseInt(subtype.trim());
			String fromDate = request.getParameter("fromDate");
			String toDate = request.getParameter("toDate");
        	form6a.setform6adata(count, fromDate,toDate);
			form6a.createPdf(writer);
//			form6a.addFooter(writer);
			
		}
        else if (pdfType.equalsIgnoreCase("Form2A(Revised)")) {
        	int empId=Integer.parseInt(subtype.trim());
        	form2Arev.setform2aRevData(count,empId);
        	form2Arev.createPdf(writer);
        } else if (pdfType.equalsIgnoreCase("FnF")) {
        	int empId=Integer.parseInt(subtype.trim());
        	fnf.setFnfPdfData(count,empId,null,null);
        	fnf.createPdf();
        } else if(pdfType.equalsIgnoreCase("FormA")){
        	int empId=Integer.parseInt(subtype.trim());
        	String branch=request.getParameter("Branch");
        	String project=request.getParameter("Project");
        	
        	if(branch.equals("null")){
        		branch=null;
        	}
        	if(project.equals("null")){
        		project=null;
        	}
        	formA.setFormAdata(count, empId,branch,project);
        	formA.createPdf();
        }
        else if(pdfType.equalsIgnoreCase("FormF")){
			int empId=Integer.parseInt(subtype.trim());
			formf.setFormFPdf(count, empId);
			
			formf.createPdf(writer);
		}else if (pdfType.equalsIgnoreCase("Form11")) {
        	int empId=Integer.parseInt(subtype.trim());
        	form11.setForm11Data(count,empId);
        	form11.createPdf();
        }else if (pdfType.equalsIgnoreCase("FORM XVII")) {
        	formXVII.setPayslip(count, CsvWriter.statuteryReport);
        	formXVII.createPdf();
        }else if(pdfType.equalsIgnoreCase("FormD_MusterRoll")) {
        	formD_MusterRoll.setAttendanceList(count, CsvWriter.attendanceReportList);
        	formD_MusterRoll.createPdf();
		}
		
		

		document.close();

	}

}
