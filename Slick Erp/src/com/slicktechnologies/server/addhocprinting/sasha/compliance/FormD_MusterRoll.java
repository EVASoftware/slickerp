package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.attendance.AttendanceBean;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;

public class FormD_MusterRoll {
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	public Document document;
	Company company;
	HrProject project=null;
	PdfUtility pdf=new PdfUtility();
	
	Font font8bold,font14bold,font7,font6;
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD|Font.UNDERLINE);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font5bold = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD);

	SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	String projectName="";
	String branchName="";
	Date fromDate=null;
	Date toDate=null;
	ArrayList<AttendanceBean> attendanceBeanList=new ArrayList<AttendanceBean>();
	HashMap<String,Double> designationMap=new HashMap<String,Double>();
	List<EmployeeInfo>employeeInfoList=new ArrayList<EmployeeInfo>();
	
	int noOfLines=22;
	int totalNoOfElement;
	int totalFooterLines=2;
	int empNo=0;
	
	List<LeaveType> leaveType=new ArrayList<LeaveType>();
	
	
	public FormD_MusterRoll() {
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font6 = new Font(Font.FontFamily.HELVETICA, 6);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setAttendanceList(long companyId,ArrayList<AttendanceBean> attendanceBeanList){
		
		if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Attendance", "ShowAttendanceLabel", companyId)){
			font7bold = new Font(Font.FontFamily.HELVETICA, 4, Font.BOLD);
		}
		company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(attendanceBeanList!=null&&attendanceBeanList.size()>0){
			this.attendanceBeanList.addAll(attendanceBeanList);
		}
		
		try {
			fromDate=fmt.parse(fmt.format(this.attendanceBeanList.get(0).getFromDate()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			toDate=fmt.parse(fmt.format(this.attendanceBeanList.get(0).getToDate()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		List<Integer> empIdList=new ArrayList<Integer>();
		HashSet<String> projectHs=new HashSet<String>();
		HashSet<String> branchHs=new HashSet<String>();
		HashSet<String> designationHs=new HashSet<String>();
		for(AttendanceBean bean:this.attendanceBeanList){
			projectHs.add(bean.getProjectName());
			branchHs.add(bean.getBranchName());
			empIdList.add(bean.getEmpId());
			designationHs.add(bean.getEmpDesignation());
		}
		
		if(projectHs.size()==1){
			for(String obj:projectHs){
				projectName=obj;
			}
			project=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", projectName).first().now();
		}
		
		if(branchHs.size()==1){
			for(String obj:branchHs){
				branchName=obj;
			}
		}
		
		employeeInfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empIdList).list();
		
		
		totalNoOfElement=this.attendanceBeanList.size();
		if(designationHs.size()!=0){
			totalFooterLines+=(int)(designationHs.size()/2);
		}
		
		leaveType=ofy().load().type(LeaveType.class).filter("companyId", companyId).list();
		
		logger.log(Level.INFO, "totalNoOfElement "+totalNoOfElement);
		logger.log(Level.INFO, "totalFooterLines "+totalFooterLines);
	}

	public void createPdf() {
		createHeadingTable();
		createAttendanceHeadingTable(empNo);
		
		if((totalNoOfElement+totalFooterLines)<=22){
			logger.log(Level.INFO, "first - total "+(totalNoOfElement+totalFooterLines));
			createFooterTable();
		}else{
			logger.log(Level.INFO, "first - noOfLines "+noOfLines+" empNo "+empNo);
			if(noOfLines<=0&&empNo==0){
				Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
				try {
					document.add(nextpage);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
			}
			createFooterTable();
		}
		
	}

	private void createFooterTable() {
		PdfPTable tbl = new PdfPTable(3);
		tbl.setWidthPercentage(100);
		
		PdfPTable tbl1 = new PdfPTable(1);
		tbl1.setWidthPercentage(100);
		tbl1.addCell(pdf.getCell("Supervisor-Signature:", font7boldul, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPTable tbl2 = new PdfPTable(1);
		tbl2.setWidthPercentage(100);
		tbl2.addCell(pdf.getCell("Admin-Signature:", font7boldul, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPTable tbl3 = new PdfPTable(1);
		tbl3.setWidthPercentage(100);
		
		PdfPTable subTbl1 = new PdfPTable(2);
		subTbl1.setWidthPercentage(100);
		subTbl1.addCell(pdf.getCell("Branch Manager Verify:", font7boldul, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		subTbl1.addCell(pdf.getCell("Total Days", font7boldul, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		PdfPTable subTbl2 = new PdfPTable(2);
		subTbl2.setWidthPercentage(100);
		
		if(designationMap.size()>0){
			for (Map.Entry<String, Double> entry : designationMap.entrySet()) {
				subTbl2.addCell(pdf.getCell(entry.getKey(), font7bold, Element.ALIGN_LEFT, 0, 0, 0));
				subTbl2.addCell(pdf.getCell(df.format(entry.getValue()), font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
			}
		}
		
		PdfPCell subTblCell=new PdfPCell();
		subTblCell.addElement(subTbl2);
		subTblCell.setBorder(0);
		
		String branchManagerName="";
		if(project!=null){
			if(project.getBranchManager()!=null&&!project.getBranchManager().equals("")){
				branchManagerName=project.getBranchManager();
			}
		}
		subTbl1.addCell(pdf.getCell("Name : "+branchManagerName, font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		subTbl1.addCell(subTblCell);
		
		PdfPCell cell=new PdfPCell();
		cell.addElement(subTbl1);
		cell.setBorder(0);
		tbl3.addCell(cell);
		
		
		PdfPCell cell1=new PdfPCell();
		cell1.addElement(tbl1);
		cell1.setBorder(0);
		tbl.addCell(cell1);
		
		PdfPCell cell2=new PdfPCell();
		cell2.addElement(tbl2);
		cell2.setBorder(0);
		tbl.addCell(cell2);
		
		PdfPCell cell3=new PdfPCell();
		cell3.addElement(tbl3);
		cell3.setBorder(0);
		tbl.addCell(cell3);
		
		PdfPTable parenTbl = new PdfPTable(1);
		parenTbl.setWidthPercentage(100);
		parenTbl.addCell(tbl);
		
		try {
			document.add(parenTbl);
		}catch(Exception e){
			e.printStackTrace();	
		}
	}

	private void createAttendanceHeadingTable(int empNum) {
		empNo = 0;
		int totalNoOfCol=12+attendanceBeanList.get(0).getDaysArray().length;
		
		PdfPTable tbl = new PdfPTable(totalNoOfCol);
		tbl.setWidthPercentage(100);
		
		float width[]=new float[totalNoOfCol];
		width[0]=2.0f;
		width[1]=5.0f;
		width[2]=11.0f;
		width[3]=5.0f;
		width[4]=2.0f;
		int index=5;
		for(int i=0;i<attendanceBeanList.get(0).getDaysArray().length;i++){
			width[5+i]=(float) (60.0/attendanceBeanList.get(0).getDaysArray().length);
		}
		index=index+attendanceBeanList.get(0).getDaysArray().length;
		width[index]=2.0f;
		width[index+1]=2.0f;
		width[index+2]=2.0f;
		width[index+3]=1.9f;
		width[index+4]=1.9f;
		width[index+5]=1.9f;
		width[index+6]=2.3f;
		try {
			tbl.setWidths(width);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		

		tbl.addCell(pdf.getCell("Sr.", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Emp_ID", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Emp Name", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Grade", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell(" ", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
		for(int i=0;i<attendanceBeanList.get(0).getDaysArray().length;i++){
			tbl.addCell(pdf.getCell(attendanceBeanList.get(0).getDaysArray()[i]+" ", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		tbl.addCell(pdf.getCell("Tot Dts", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("OT in Hrs", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("OT in days", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("WO", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Holiday", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Absent", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Work Done", font5bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		
		for(int i=empNum;i<attendanceBeanList.size();i++){
			
			if (noOfLines <= 0  ) {
				empNo = i;
				break;
			}
			noOfLines = noOfLines - 1;
			
			int presentDays=0;
			int absentDays=0;
			int wo=0;
			int holiday=0;
			
			double otHrs=0;
			double otDays=0;
			double totalWorkDone=0;
					
			AttendanceBean bean=attendanceBeanList.get(i);
			
			tbl.addCell(pdf.getCell((i+1)+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(bean.getEmpId()+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(bean.getEmployeeName(), font6bold, Element.ALIGN_LEFT, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(bean.getEmpDesignation(), font6bold, Element.ALIGN_LEFT, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			tbl.addCell(pdf.getCell("P", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
			List<Attendance> attendanceList=new ArrayList<Attendance>();
			double workingHrs=0;
			EmployeeInfo info=getEmployeeInfo(bean.getEmpId());
			if(info!=null){
				if(info.getLeaveCalendar()!=null){
					workingHrs=info.getLeaveCalendar().getWorkingHours();
				}
			}
			for (Map.Entry<Date, Attendance> entry : bean.getAttendanceObjectMap().entrySet()) {
			    Attendance value = entry.getValue();
			    attendanceList.add(value);
			    
			    //Ashwini Patil Date:6-06-2022 Attendance report download was printing html text in cells so removed that text				   
			    String label=value.getGroup();
			    label=label.replace("<html><font color=\"red\">","");
			    label=label.replace("</font></html>","");
			    label=label.replace("<html><font color=\"orange\">","");
			    
			    tbl.addCell(pdf.getCell(label, font7bold, Element.ALIGN_CENTER, 0, 0, 0));
			    if(value.isOvertime()){
			    	otHrs=otHrs+value.getOvertimeHours();
			    }
			}
			
			presentDays=getNoOfPresentDays(attendanceList);
			absentDays=getActualAbsentDays(attendanceList);
			wo=getNoOfWeeklyOff(attendanceList);
			holiday=getNoOfHolidays(attendanceList);
			if(workingHrs!=0){
				otDays=otHrs/workingHrs;
			}
			totalWorkDone=presentDays+wo+holiday+otDays;
			
			if(designationMap.size()!=0){
				if(designationMap.containsKey(bean.getEmpDesignation())){
					double sum=designationMap.get(bean.getEmpDesignation())+totalWorkDone;
					designationMap.put(bean.getEmpDesignation(),sum);
				}else{
					designationMap.put(bean.getEmpDesignation(), totalWorkDone);
				}
			}else{
				designationMap.put(bean.getEmpDesignation(), totalWorkDone);
			}
			
			tbl.addCell(pdf.getCell(presentDays+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(otHrs+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(df.format(otDays)+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(wo+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(holiday+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(absentDays+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			tbl.addCell(pdf.getCell(df.format(totalWorkDone)+"", font6bold, Element.ALIGN_CENTER, 2, 0, 0)).setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			tbl.addCell(pdf.getCell("OT", font6bold, Element.ALIGN_CENTER, 0, 0, 0));
			for (Map.Entry<Date, Attendance> entry : bean.getAttendanceObjectMap().entrySet()) {
			    Attendance value = entry.getValue();
			    String otVal=0+"";
			    if(value.isOvertime()){
			    	otVal=value.getOvertimeHours()+"";
			    }
			    tbl.addCell(pdf.getCell(otVal, font7bold, Element.ALIGN_CENTER, 0, 0, 0));
			}
		}
		
		try {
			document.add(tbl);
		}catch(Exception e){
			e.printStackTrace();	
		}
		
		if(noOfLines<=0&&empNo!=0){
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			noOfLines=25;
			createAttendanceHeadingTable(empNo);
		}
	}

	private void createHeadingTable() {
		PdfPTable tbl = new PdfPTable(2);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[]{75.00f,25.00f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		
		PdfPCell headingCell=pdf.getCell("Form D - Muster Roll", font14bold, Element.ALIGN_CENTER, 2, 0, 0);
		headingCell.setPaddingLeft(200);
		tbl.addCell(headingCell).setBorder(0);	
		
		
		//Ashwini Patil commented below 2 lines
//		tbl.addCell(pdf.getCell("P-Present, HD-Present(half day), Q-Quarter Duty, O-OT, o-OT (Half day), W- Weekly off, w- WO+OT", font6, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
//		tbl.addCell(pdf.getCell("L-Paid Leave, H-National Holiday and Public Holiday, A-Absent, T-Training, N - NFC, C-CO (Compulsory off), U - Unpaid leave", font6, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		//Ashwini Patil Date:13-06-2022 wrong Leave deatils were getting printed 
		tbl.addCell(pdf.getCell("P-Present, HF-Present(half day), WO- Weekly off, H-National Holiday and Public Holiday, A-Absent", font6, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		String leavePairs="";
		for(LeaveType l:leaveType) {
			leavePairs+=l.getShortName()+"-"+l.getName()+", ";
		}
		 if (leavePairs != null && leavePairs.length() > 0 ) {
			 leavePairs = leavePairs.substring(0, leavePairs.length()-2);
		    }
		
		tbl.addCell(pdf.getCell(leavePairs, font6, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(tbl);
		}catch(Exception e){
			e.printStackTrace();	
		}
		
		
		
		PdfPTable tbl1 = new PdfPTable(2);
		tbl1.setWidthPercentage(100);
		try {
			tbl1.setWidths(new float[]{65.00f,35.00f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		tbl1.addCell(pdf.getCell("Name & Address of Establishment: "+company.getBusinessUnitName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);		
		tbl1.addCell(pdf.getCell("Branch Name: "+branchName, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl1.addCell(pdf.getCell("Unit Name: "+projectName, font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl1.addCell(pdf.getCell("From "+fmt.format(fromDate)+" To "+fmt.format(toDate), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl1.addCell(pdf.getCell(" ", font7, Element.ALIGN_CENTER, 0, 2, 0)).setBorder(0);
		try {
			document.add(tbl1);
		}catch(Exception e){
			e.printStackTrace();	
		}
	}
	
	private int getNoOfPresentDays(List<Attendance> attendanceList) {
		// TODO Auto-generated method stub
		int presentDays=0;
		for(Attendance atten:attendanceList){
			if(atten.getActionLabel().trim().equalsIgnoreCase("P")){
				presentDays++;
			}
		}
		return presentDays;
	}
	
	private int getActualAbsentDays(List<Attendance> attendanceList) {
		int absentCounter=0;
		for(Attendance attendance:attendanceList){
			if(attendance.getActionLabel().equalsIgnoreCase("A")||attendance.getActionLabel().equalsIgnoreCase("L")){
				absentCounter++;
			}
		}
		return absentCounter;
	}
	
	private int getNoOfHolidays(List<Attendance> attendanceList) {
		int hCounter=0;
		for(Attendance attendance:attendanceList){
			if(attendance.isHoliday()==true||attendance.getActionLabel().equalsIgnoreCase("H")){
				hCounter++;
			}
		}
		return hCounter;
	}
	
	private int getNoOfWeeklyOff(List<Attendance> attendanceList) {
		int woCounter=0;
		for(Attendance attendance:attendanceList){
			if(attendance.isWeeklyOff()==true||attendance.getActionLabel().equalsIgnoreCase("WO")){
				woCounter++;
			}
		}
		return woCounter;
	}
	
	private EmployeeInfo getEmployeeInfo(int empId){
		if(employeeInfoList!=null){
			for(EmployeeInfo emp:employeeInfoList){
				if(emp.getEmpCount()==empId){
					return emp;
				}
			}
		}
		return null;
	}
}
