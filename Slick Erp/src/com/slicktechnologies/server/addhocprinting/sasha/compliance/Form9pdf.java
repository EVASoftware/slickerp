package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeFamilyDeatails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class Form9pdf {
	
	public Document document;
	List<PaySlip> paySlipList;
	List<Employee>employeeList;
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt2= new SimpleDateFormat("MM/yyyy");
	Branch branch=null;
	Company company;
	Employee employee;
	int noofemployee=0;
	int noOfLines = 0;
	int loopCount = 0;
	String cinval=null;
	Company comp;
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	
	List<EmployeeAdditionalDetails> empAddDetList=new ArrayList<EmployeeAdditionalDetails>();
	
	public void createPdf() {
		createFormHeading();
		companyDetails();
		employeeDetails();
		form9Heading();
		employeePFDetails(14, loopCount);
		
		
		while (noOfLines == 0 && loopCount < employeeList.size()) {
			
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			form9Heading();
			employeePFDetails(14, loopCount);
			
		}
		
		footerDetails();
		
		}
	
	   public void setform9pdfdata(Long companyId,int empId,String branch,String project){
           fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		   TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		   
		   fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		   TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		   
		   fmt2.setTimeZone(TimeZone.getTimeZone("IST"));
		   TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		   
		   format.setTimeZone(TimeZone.getTimeZone("IST"));
		   TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		employee=ofy().load().type(Employee.class).filter("companyId", companyId).filter("designation","Director").first().now();
		
//		employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("count", empId).filter("branchName", branch) .filter("projectName",project).list();
		 if(empId!=-1&&branch!=null&&project!=null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("count", empId).filter("branchName", branch) .filter("projectName",project).list();
		 }else if(empId!=-1&&branch==null&&project==null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("count", empId).list();
				
		 }else if(empId!=-1&&branch!=null&&project==null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("count", empId).filter("branchName", branch) .list();
				
		 }else if(empId!=-1&&branch==null&&project!=null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("count", empId).filter("projectName",project).list();
				
		 }else if(empId==-1&&branch!=null&&project!=null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("branchName", branch) .filter("projectName",project).list();
				
		 }else if(empId==-1&&branch!=null&&project==null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("branchName", branch).list();
				
		 }else if(empId==-1&&branch==null&&project!=null){
			 employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).filter("projectName",project).list();
				
		 }
		empAddDetList=ofy().load().type(EmployeeAdditionalDetails.class).filter("companyId", companyId).list();
		paySlipList=CsvWriter.statuteryReport;
			
//		noofemployee=paySlipList.size();
		
		
		
	   }
	
	public String getFatherOrSpouseName(int empId){
		String name="";
		if(empAddDetList!=null){
			for(EmployeeAdditionalDetails emp:empAddDetList){
				if(emp.getEmpInfo().getEmpCount()==empId){
					for(EmployeeFamilyDeatails fam:emp.getEmpFamDetList()){
						if(fam.getFamilyRelation().trim().equalsIgnoreCase("Father")){
							name=fam.getFname()+" "+fam.getMname()+" "+fam.getLname();
						}
						
						if(fam.getFamilyRelation().trim().equalsIgnoreCase("Husband")){
							name=fam.getFname()+" "+fam.getMname()+" "+fam.getLname();
						}
					}
				}
			}
		}
		return name;
	}
	
	
	private void createFormHeading() {
		PdfPTable outerTable=new PdfPTable(1);
		outerTable.setWidthPercentage(100f);
		
		
		
		
		outerTable.addCell(cell.getCell("FORM-9(Revised)", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTable.addCell(cell.getCell("THE EMPLOYEES PROVIDENT FUND SCHEME-1952 [PARA 36 (1)]", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTable.addCell(cell.getCell("THE EMPLOYEES PENSION SCHEME-1995 [PARA(20) (1)]", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTable.addCell(cell.getCell("RETURN OF EMPLOYEES WHO ARE ENTITLED AND REQUIRED TO BECOME MEMBER OF ", font10, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		PdfPCell bottomCell=cell.getCell("THE EMPLOYEES' PROVIDENT FUND AND PENSION FUND ", font10, Element.ALIGN_CENTER, 0, 0, 0);
		bottomCell.setBorderWidthLeft(0);
		bottomCell.setBorderWidthRight(0);
		bottomCell.setBorderWidthTop(0);
		outerTable.addCell(bottomCell);
		try {
			document.add(outerTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void companyDetails(){
		
		PdfPTable outertable=new PdfPTable(4);
		outertable.setWidthPercentage(100f);
		try {
			outertable.setWidths(new float[] {30,45,15,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		outertable.setSpacingBefore(5f);
		
		
		String dateofcoverage="";
		if(company.getDateofcoverage()!=null)	{
			dateofcoverage=fmt1.format(company.getDateofcoverage());
		}else 
		{
			
		}
			
		String groupno="";
		if(company.getPfGroupNum()!=null){
			groupno=company.getPfGroupNum();
		}else{
			
		}
		
		
		
		
	
		
		 cinval = "";

		for (int i = 0; i < company.getArticleTypeDetails().size(); i++) {

			if (company.getArticleTypeDetails().get(i).getArticleTypeName()
					.contains("PF Code Number")) {

				cinval = company.getArticleTypeDetails().get(i)
						.getArticleTypeValue();
			}

		}
		
		outertable.addCell(cell.getCell("Name of the Factory / Establishment ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(company.getBusinessUnitName().toUpperCase(), font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("Address ", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outertable.addCell(cell.getCell(company.getAddress().getCompleteAddress(), font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outertable.addCell(cell.getCell("PF Code Number ", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outertable.addCell(cell.getCell(cinval, font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("PF Group No.", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(groupno, font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("Industry in which Factory / Estt.is engaged", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("Date of Coverage", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(dateofcoverage, font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("Registration No. of Factory / Establishment", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("ESIC Code", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("Date from which EPS is applicable", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(dateofcoverage, font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		outertable.addCell(cell.getCell("Name of the designated Medical Officer ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable.addCell(cell.getCell(" ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
		PdfPTable outertable1=new PdfPTable(1);
		outertable1.setWidthPercentage(100f);
		
		outertable1.addCell(cell.getCell("If Factory / Estt. is covered under ESI act,indicate the code alloted under ESI.If not,furnish the details of the medical office of the factory / establishment.  ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("Specimen signature of the Employer or Authorised official. ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		
       
	
	    try {
		document.add(outertable);
		document.add(outertable1);
	    } catch (DocumentException e) {
		e.printStackTrace();
	    }
	
	
	}
	
	    private void employeeDetails() {
		PdfPTable outertable=new PdfPTable(5);
		outertable.setWidthPercentage(100f);
		outertable.setSpacingBefore(10f);
		try {
			outertable.setWidths(new float[] {5,32,15,17,31});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		};
		
		
		PdfPCell srno=cell.getCell("Sr.No.", font10, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell name=cell.getCell("Name", font10, Element.ALIGN_LEFT, 2, 0, 0);
		PdfPCell desg=cell.getCell("Designation", font10, Element.ALIGN_LEFT, 2, 0, 0);
		PdfPCell specsign=cell.getCell("Specimen Signature", font10, Element.ALIGN_LEFT, 2, 0, 0);
		PdfPCell blank=cell.getCell("", font10, Element.ALIGN_LEFT, 2, 0, 0);
		
		
		PdfPCell srno1=cell.getCell("1", font10, Element.ALIGN_CENTER, 2, 0, 0);
		srno1.setBorderWidthBottom(0); 
		
        PdfPCell name1 = null;
		if (employee !=null&&employee.getFullName() != null) {
			name1 = cell.getCell(employee.getFullName(), font8,Element.ALIGN_LEFT, 2, 0, 0);
			name1.setBorderWidthTop(0);
			name1.setBorderWidthBottom(0);
		} else {
			name1 = cell.getCell("", font8,Element.ALIGN_LEFT, 2, 0, 0);
			name1.setBorderWidthTop(0);
			name1.setBorderWidthBottom(0);
		}
		
		PdfPCell desg1 =null;
		if (employee !=null&&employee.getDesignation() != null) {
			desg1 = cell.getCell(employee.getDesignation(), font8,Element.ALIGN_LEFT, 2, 0, 0);
			desg1.setBorderWidthTop(0);
			desg1.setBorderWidthBottom(0);
		} else {
			desg1 = cell.getCell("", font8,Element.ALIGN_LEFT, 2, 0, 0);
			desg1.setBorderWidthTop(0);
			desg1.setBorderWidthBottom(0);
		}
		
		PdfPCell specsign1=cell.getCell("   ", font10, Element.ALIGN_LEFT, 2, 0, 0);
		specsign1.setBorderWidthBottom(0);
		specsign1.setBorderWidthBottom(0);
		
		PdfPCell blank1=cell.getCell("  ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		
		
		
		PdfPCell srno2=cell.getCell("2", font10, Element.ALIGN_CENTER, 2, 0, 0);
		srno2.setBorderWidthTop(0);
		srno2.setBorderWidthBottom(0);
		
		PdfPCell name2=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		name2.setBorderWidthTop(0);
		name2.setBorderWidthBottom(0);
		
	    PdfPCell desg2=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		desg2.setBorderWidthTop(0);
		desg2.setBorderWidthBottom(0);
		
		PdfPCell specsign2=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		specsign2.setBorderWidthTop(0);
		specsign2.setBorderWidthBottom(0);
		
		PdfPCell blank2=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		
	    PdfPCell srno3=cell.getCell("3", font10, Element.ALIGN_CENTER, 2, 0, 0);
		srno3.setBorderWidthTop(0);
		srno3.setBorderWidthBottom(0);
		
	    PdfPCell name3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		name3.setBorderWidthTop(0);
		name3.setBorderWidthBottom(0);
		
		PdfPCell desg3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		desg3.setBorderWidthTop(0);
		desg3.setBorderWidthBottom(0);
		
		PdfPCell specsign3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		specsign3.setBorderWidthTop(0);
		specsign3.setBorderWidthBottom(0);
		
		PdfPCell blank3=cell.getCell("", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		
		PdfPCell srno4=cell.getCell("    ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		srno4.setBorderWidthTop(0);
		
		PdfPCell name4=cell.getCell("    ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		name4.setBorderWidthTop(0);
		
		PdfPCell desg4=cell.getCell("     ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		desg4.setBorderWidthTop(0);
		
		PdfPCell specsign4=cell.getCell("    ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		specsign4.setBorderWidthTop(0);
		
		PdfPCell blank4=cell.getCell("      ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		
		
		
		
		
		outertable.addCell(srno);
		outertable.addCell(name);
		outertable.addCell(desg);
		outertable.addCell(specsign);
		outertable.addCell(blank).setBorder(0);
		
		outertable.addCell(srno1);
		outertable.addCell(name1);
		outertable.addCell(desg1);
		outertable.addCell(specsign1);
		outertable.addCell(blank1).setBorder(0);
		
		
		outertable.addCell(srno2);
		outertable.addCell(name2);
		outertable.addCell(desg2);
		outertable.addCell(specsign2);
		outertable.addCell(blank2).setBorder(0);
		
		outertable.addCell(srno3);
		outertable.addCell(name3);
		outertable.addCell(desg3);
		outertable.addCell(specsign3);
		outertable.addCell(blank3).setBorder(0);
		
		outertable.addCell(srno4);
		outertable.addCell(name4);
		outertable.addCell(desg4);
		outertable.addCell(specsign4);
		outertable.addCell(blank4).setBorder(0);
		
	    PdfPTable outertable1=new PdfPTable(1);
		outertable1.setWidthPercentage(100f);
		outertable1.setSpacingBefore(5f);
		
		outertable1.addCell(cell.getCell("Remarks if any", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("   Note:1.This form should be accompanied by declaration in Form-2 by every employee.", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("          2.Any change in the authorised official / designated medical officer should be intimated to the commissioner.", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("Number of Employees enrolled as members on the date of coverage", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("Signature of the Employer or other Authorised officer", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("Date and Stamp of factory / Establishment", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outertable1.addCell(cell.getCell("(For office only)", font10, Element.ALIGN_RIGHT, 0, 0, 18)).setBorder(0);
		
		
	    try {
		document.add(outertable);
		document.add(outertable1);
		document.newPage();
	    } catch (DocumentException e) {
		e.printStackTrace();
	    }
	
	   }
	
	    private void form9Heading(){
	    	
		PdfPTable outertable2=new PdfPTable(1);
		outertable2.setWidthPercentage(100f);
		
		
		outertable2.addCell(cell.getCell("FORM-9(Revised)", font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		outertable2.addCell(cell.getCell("THE EMPLOYEES PROVIDENT FUND SCHEME-1952 [PARA 36 (1)]", font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		outertable2.addCell(cell.getCell("THE EMPLOYEES PENSION SCHEME-1995 [PARA(20) (1)]", font10bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);

		
		PdfPTable outertable3=new PdfPTable(4);
		outertable3.setWidthPercentage(100f);
		
		outertable3.addCell(cell.getCell("Folio Number", font10bold, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
		outertable3.addCell(cell.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
		outertable3.addCell(cell.getCell("PF Code Number", font10bold, Element.ALIGN_RIGHT, 0, 0, 20)).setBorder(0);
		outertable3.addCell(cell.getCell(cinval, font10bold, Element.ALIGN_LEFT, 0, 0, 20)).setBorder(0);
		
		

	    try {
		document.add(outertable2);
		document.add(outertable3);
		
	    } catch (DocumentException e) {
		e.printStackTrace();
	    }
	}
	
	    
	    
	   private void employeePFDetails(int count,int loopC) {
		// TODO Auto-generated method stub
		noOfLines = count;
		PdfPTable outertable4=new PdfPTable(12);
		outertable4.setWidthPercentage(100f);
		try {
			outertable4.setWidths(new float[] {3,7,17,12,8,4,8,10,8,5,8,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		outertable4.setSpacingBefore(5f);
		
	    PdfPCell srNo=cell.getCell("Sr.No", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    srNo.setBorderWidthBottom(0);
	    
	    PdfPCell accno=cell.getCell("Account Number ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    accno.setBorderWidthBottom(0);
	    PdfPCell empname=cell.getCell("Name of the Employee ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    empname.setBorderWidthBottom(0);
	    PdfPCell fhname=cell.getCell("Father's Name (Or Husband's name incase of married women ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    fhname.setBorderWidthBottom(0);
	    PdfPCell dob=cell.getCell(" Date of Birth", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    dob.setBorderWidthBottom(0);
	    PdfPCell sex=cell.getCell("Sex ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    sex.setBorderWidthBottom(0);
	    PdfPCell doj=cell.getCell("Date of Joining PF ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    doj.setBorderWidthBottom(0);
	    PdfPCell servicejPF=cell.getCell("Total Period of previous service as on joining PF(Excl.period of break)", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    servicejPF.setBorderWidthBottom(0);
	    PdfPCell dojpscheme=cell.getCell("Date of Joining Pension Scheme ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    dojpscheme.setBorderWidthBottom(0);
	    PdfPCell hc=cell.getCell(" Intials pf HC", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    hc.setBorderWidthBottom(0);
	    PdfPCell dol=cell.getCell("Date and Reason of Leaving Service ", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    dol.setBorderWidthBottom(0);
	    PdfPCell remark=cell.getCell("Remarks", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    remark.setBorderWidthBottom(0);
		
	    outertable4.addCell(srNo);
	    outertable4.addCell(accno);
	    outertable4.addCell(empname);
	    outertable4.addCell(fhname);
	    outertable4.addCell(dob);
	    outertable4.addCell(sex);
	    outertable4.addCell(doj);
	    outertable4.addCell(servicejPF);
	    outertable4.addCell(dojpscheme);
	    outertable4.addCell(hc);
	    outertable4.addCell(dol);
	    outertable4.addCell(remark);
	    
	    
	    PdfPCell cell1=cell.getCell("(1)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell1.setBorderWidthTop(0);
		
		PdfPCell cell2=cell.getCell("(2)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell2.setBorderWidthTop(0);
		
		PdfPCell cell3=cell.getCell("(3)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell3.setBorderWidthTop(0);
		
		PdfPCell cell4=cell.getCell("(4)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell4.setBorderWidthTop(0);
		
		PdfPCell cell5=cell.getCell("(5)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell5.setBorderWidthTop(0);
		
		PdfPCell cell6=cell.getCell("(6)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell6.setBorderWidthTop(0);
		
		PdfPCell cell7=cell.getCell("(7)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell7.setBorderWidthTop(0);
		
		PdfPCell cell8=cell.getCell("(8)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell8.setBorderWidthTop(0);
		
		PdfPCell cell9=cell.getCell("(9)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell9.setBorderWidthTop(0);
		
		PdfPCell cell10=cell.getCell("(10)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell10.setBorderWidthTop(0);
		
		PdfPCell cell11=cell.getCell("(11)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell11.setBorderWidthTop(0);
		
		PdfPCell cell12=cell.getCell("(12)", font8, Element.ALIGN_CENTER, 0, 0, 18);
		cell12.setBorderWidthTop(0);
		
		outertable4.addCell(cell1);
		outertable4.addCell(cell2);
		outertable4.addCell(cell3);
		outertable4.addCell(cell4);
		outertable4.addCell(cell5);
		outertable4.addCell(cell6);
		outertable4.addCell(cell7);
		outertable4.addCell(cell8);
		outertable4.addCell(cell9);
		outertable4.addCell(cell10);
		outertable4.addCell(cell11);
		outertable4.addCell(cell12);
		
		for (int j = loopC; j < employeeList.size(); j++) {
			Employee employee = employeeList.get(j);
			if (noOfLines == 0) {
				loopCount = j;
				break;
			}

			noOfLines--;
		
	
		
		
		
		PdfPCell cell21=cell.getCell((j+1)+"", font8, Element.ALIGN_CENTER, 0, 0, 16);
		PdfPCell cell22=cell.getCell(employee.getPpfNo(), font8, Element.ALIGN_CENTER, 0, 0,25);
		PdfPCell cell23=cell.getCell(employee.getFullName(), font8, Element.ALIGN_LEFT, 0, 0, 25);
		PdfPCell cell24=cell.getCell(getFatherOrSpouseName(employee.getCount()), font8, Element.ALIGN_LEFT, 0, 0, 25);
		PdfPCell cell25=null;
		if(employee !=null && employee.getDob() !=null) {
			cell25=cell.getCell(fmt1.format(employee.getDob()), font8, Element.ALIGN_CENTER, 0, 0, 25);
		}else{
			cell25=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 25);
		}
		
		PdfPCell cell26=cell.getCell(employee.getGender(), font8, Element.ALIGN_CENTER, 0, 0, 25);
		
		PdfPCell cell27=null;
		if(employee!=null && employee.getJoinedAt() !=null) {
			 cell27=cell.getCell(fmt1.format(employee.getJoinedAt()), font8, Element.ALIGN_CENTER, 0, 0, 25);
		}
		else{
			cell27=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 25);
		}
		
		
		
		
		PdfPCell cell28=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 25);
		
		PdfPCell cell29=null;
		 
		if(employee !=null && employee.getJoinedAt() !=null) {
		cell29=cell.getCell(fmt1.format(employee.getJoinedAt()), font8, Element.ALIGN_CENTER, 0, 0, 25);
		}
		
		else{
			cell29=cell.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 25);
		}
		
		
		PdfPCell cell30=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 25);
		
		/**
		 * @author Anil , Date : 13-07-2019
		 * raised by RT for Riseon
		 */
		PdfPCell cell31=null;
		Date leavingDate=null;
		if(employee.getLastWorkingDate()!=null){
			leavingDate=employee.getLastWorkingDate();
		}else if(employee.getTerminationDate()!=null){
			leavingDate=employee.getTerminationDate();
		}else if(employee.getAbscondDate()!=null){
			leavingDate=employee.getAbscondDate();
		}
		
		if(leavingDate!=null){
			cell31=cell.getCell(fmt1.format(leavingDate), font8, Element.ALIGN_CENTER, 0, 0, 25);
		}else{
			cell31=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 25);
		}
		
		PdfPCell cell32=cell.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 25);
		
		outertable4.addCell(cell21);
		outertable4.addCell(cell22);
		outertable4.addCell(cell23);
		outertable4.addCell(cell24);
		outertable4.addCell(cell25);
		outertable4.addCell(cell26);
		outertable4.addCell(cell27);
		outertable4.addCell(cell28);
		outertable4.addCell(cell29);
		outertable4.addCell(cell30);
		outertable4.addCell(cell31);
		outertable4.addCell(cell32);
		
		
		
		
		
		
		
		
		}
		
		
		
	    
		  try {
				
				document.add(outertable4);
				
			    } catch (DocumentException e) {
				e.printStackTrace();
			    }
			  
		
}
	
	private void footerDetails() {
		// TODO Auto-generated method stub
		
		PdfPTable outerTbl=new PdfPTable(2);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(20f);
		
		outerTbl.addCell(cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getBusinessUnitName().toUpperCase(), font8, Element.ALIGN_RIGHT, 0, 0, 30)).setBorder(0);
		outerTbl.addCell(cell.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("DIRECTOR", font8, Element.ALIGN_RIGHT, 0, 0, 30)).setBorder(0);

		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
		
		
	}
	
	

}
