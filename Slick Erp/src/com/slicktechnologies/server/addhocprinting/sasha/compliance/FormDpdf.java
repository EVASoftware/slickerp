package com.slicktechnologies.server.addhocprinting.sasha.compliance;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocdownload.CsvWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class FormDpdf {
	Logger logger=Logger.getLogger("FormD PDF");
	public Document document;
	List<PaySlip> paySlipList;
	List<Employee>employeeList;
	int noofemployee=0;
	
	SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat fmt= new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat fmt1= new SimpleDateFormat("MM/yyyy");
	
	Company company;
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
	
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	EmploymentCardPdf cell=new EmploymentCardPdf();
	Date fromDate=null;
	Date toDate=null;
	int noOfEmployeeWhomeBonusIsPaid;
	double annualBonusAmt;
	
	public void createPdf() {
		createFormHeading();
		employeedetails();
		pfdetails();
		footerdetails();
		
	}

	public FormDpdf() { 
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
	}
	
	
	
	public void setformDpdfdata(Long companyId,int empId,String fromDate,String toDate){
		Date fromDt=null;
		Date toDt=null;
		try {
			this.fromDate=format.parse(fromDate);
			this.toDate=format.parse(toDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fromDt=DateUtility.getDateWithTimeZone("IST", this.fromDate);
		toDt=DateUtility.getDateWithTimeZone("IST", this.toDate);
		logger.log(Level.SEVERE,"FORM : "+fmt.format(this.fromDate)+" TO : "+fmt.format(this.toDate)+" comp Id : "+companyId+" Emp Id : "+empId);
		
//		Calendar frmCal = Calendar.getInstance(); 
//		frmCal.setTime(fromDt);
//		frmCal.add(Calendar.MONTH, -1);
//		fromDt=DateUtility.getDateWithTimeZone("IST", frmCal.getTime());
//		
//		Calendar toCal = Calendar.getInstance(); 
//		toCal.setTime(toDt);
//		toCal.add(Calendar.MONTH, -1);
//		toDt=DateUtility.getDateWithTimeZone("IST", toCal.getTime());
//		System.out.println("FRM :: "+format.format(fromDt)+" TO :: "+format.format(toDt));
//		
		
		ArrayList<String> monthList=new ArrayList<String>();
		while(fromDt.before(toDt)||fromDt.equals(toDt)){
			monthList.add(fmt.format(fromDt));
			Calendar cal = Calendar.getInstance(); 
			cal.setTime(fromDt);
			cal.add(Calendar.MONTH, 1);
			fromDt=DateUtility.getDateWithTimeZone("IST", cal.getTime());
		}
		logger.log(Level.SEVERE,"MONTH LIST : "+monthList);
		
		if(monthList.size()!=0){
			paySlipList=ofy().load().type(PaySlip.class).filter("companyId",companyId).filter("salaryPeriod IN", monthList).list();
		}
		if(paySlipList!=null){
			System.out.println("PaySlip : "+paySlipList.size());
			logger.log(Level.SEVERE,"PaySlip : "+paySlipList.size());
		}
		System.out.println("DONE");
		getTotalNoofEmpWhoseBonusispaid();
		
		company=ofy().load().type(Company.class).filter("companyId", companyId).first().now();
//		paySlipList= CsvWriter.statuteryReport;
		
		employeeList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("status",true).list();
		noofemployee=employeeList.size();
		
		logger.log(Level.SEVERE,"totalemployee : "+noofemployee);
		
		
	}
	
	
	
	public void getTotalNoofEmpWhoseBonusispaid() {
		HashSet<Integer> empHs=new HashSet<Integer>();
		if(paySlipList !=null){
			for(PaySlip slip:paySlipList) {
				if(slip.getBonus()!=0){
					empHs.add(slip.getEmpid());
					/**
					 * This method return the sum of basic and da from earning list
					 */
					annualBonusAmt+=slip.getPFAmount();
					logger.log(Level.SEVERE,"totalannaulbonusamt : "+annualBonusAmt);
				}
			}
			
		}
		noOfEmployeeWhomeBonusIsPaid=empHs.size();
		
		logger.log(Level.SEVERE,"totalempwhomebonusispaid: "+noOfEmployeeWhomeBonusIsPaid);
		
		
	}
	
	
	
	
	

	private void createFormHeading() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		
		outerTbl.addCell(cell.getCell("[Form \"D\"]", font11bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("(SEE RULE 5)", font11bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Bonus Working Sheet - FORM 'C' [See Rule 4 (C)]  "+fmt1.format(fromDate)+" To "+fmt1.format(toDate), font11bold, Element.ALIGN_CENTER, 0, 0, 20)).setBorder(0);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		}
	
	private void employeedetails() {
		PdfPTable outerTbl=new PdfPTable(4);
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(new float[] {25,60,8,7});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		outerTbl.setSpacingBefore(5f);
		
		outerTbl.addCell(cell.getCell("Name of the establishment & its ", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getBusinessUnitName().toUpperCase(), font10bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);

		

		outerTbl.addCell(cell.getCell("complete postal address", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(company.getAddress().getCompleteAddress(), font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);		
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);


		outerTbl.addCell(cell.getCell("Nature Of Industry", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);


		outerTbl.addCell(cell.getCell("Name of the Employer", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);


		outerTbl.addCell(cell.getCell("Total No of employees", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(noofemployee+"", font10bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);


		outerTbl.addCell(cell.getCell("No. Of employees Benefied by Bonus Payments", font10, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell(noOfEmployeeWhomeBonusIsPaid+"", font10bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);


//		outerTbl.addCell(cell.getCell("Payments", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
//		outerTbl.addCell(cell.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
//		outerTbl.addCell(cell.getCell("", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);
//		outerTbl.addCell(cell.getCell("   ", font10, Element.ALIGN_LEFT, 0, 0, 18)).setBorder(0);


		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void pfdetails() {
		PdfPTable outerTbl=new PdfPTable(8);
		outerTbl.setWidthPercentage(100f);
		outerTbl.setSpacingBefore(10f);
		try {
			outerTbl.setWidths(new float[] {20,15,10,12,12,13,10,8});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	    PdfPCell totalamt=cell.getCell("Total Amount Payable as Bonus U/s 10 or 11 of the Payment of Bonus Act,1965 as the Case may be.", font10, Element.ALIGN_CENTER, 2, 0, 0);
	   
		
	    PdfPCell settlement=cell.getCell("Settelment if any reached U/s 18(1) or 12(3) of the Industrial Disputes Act,1947 , with Date.", font10, Element.ALIGN_CENTER, 2, 0, 0);
	    
		
	    PdfPCell perofbonus=cell.getCell("Percentage of Bonus Declared to be paid.", font10, Element.ALIGN_CENTER, 2, 0, 0);
	   
		
		PdfPCell totalamtbonus=cell.getCell("Total Amount of Bonus actually paid", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell paymentdate=cell.getCell("Date on which Payment Made", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		
		PdfPCell bonuspaid=cell.getCell("Whether Bonus has been paid to all the employees,if not,reasons for non payments", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		PdfPCell remarks=cell.getCell("Remarks", font10, Element.ALIGN_CENTER, 2, 0, 0);
		PdfPCell blank=cell.getCell("  ", font10, Element.ALIGN_CENTER, 2, 0, 0);
		
		outerTbl.addCell(totalamt);
		outerTbl.addCell(settlement);
		outerTbl.addCell(perofbonus);
		outerTbl.addCell(totalamtbonus);
		outerTbl.addCell(paymentdate);
		outerTbl.addCell(bonuspaid);
		outerTbl.addCell(remarks);
		outerTbl.addCell(blank).setBorder(0);
		
		PdfPCell cell1=cell.getCell("[1]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell1.setBorderWidthTop(0);
		
		PdfPCell cell2=cell.getCell("[2]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell2.setBorderWidthTop(0);
		
		PdfPCell cell3=cell.getCell("[3]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell3.setBorderWidthTop(0);
		
		PdfPCell cell4=cell.getCell("[4]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell4.setBorderWidthTop(0);
		
		PdfPCell cell5=cell.getCell("[5]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell5.setBorderWidthTop(0);
		
		PdfPCell cell6=cell.getCell("[6]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell6.setBorderWidthTop(0);
		
		PdfPCell cell7=cell.getCell("[7]", font8, Element.ALIGN_CENTER, 0, 0, 15);
		cell7.setBorderWidthTop(0);
		
		PdfPCell blank1=cell.getCell("  ", font10, Element.ALIGN_CENTER, 2, 0, 15);
		
		outerTbl.addCell(cell1);
		outerTbl.addCell(cell2);
		outerTbl.addCell(cell3);
		outerTbl.addCell(cell4);
		outerTbl.addCell(cell5);
		outerTbl.addCell(cell6);
		outerTbl.addCell(cell7);
		outerTbl.addCell(blank1).setBorder(0);
		
		
		PdfPCell cell11=cell.getCell(annualBonusAmt+"", font12bold, Element.ALIGN_CENTER, 0, 0, 70);
		PdfPCell cell12=cell.getCell("    ", font12bold, Element.ALIGN_RIGHT, 0, 0, 70);
		PdfPCell cell13=cell.getCell("8.33 ", font12bold, Element.ALIGN_CENTER, 0, 0, 70);
		PdfPCell cell14=cell.getCell(annualBonusAmt+"", font12bold, Element.ALIGN_CENTER, 0, 0, 70);
		PdfPCell cell15=cell.getCell("     ", font12bold, Element.ALIGN_RIGHT, 0, 0, 70);
		PdfPCell cell16=cell.getCell(" YES ", font12bold, Element.ALIGN_CENTER, 0, 0, 70);
		PdfPCell cell17=cell.getCell("     ", font12bold, Element.ALIGN_RIGHT, 0, 0, 70);
		PdfPCell blank2=cell.getCell("     ", font12bold, Element.ALIGN_CENTER, 2, 0, 70);
		

		outerTbl.addCell(cell11);
		outerTbl.addCell(cell12);
		outerTbl.addCell(cell13);
		outerTbl.addCell(cell14);
		outerTbl.addCell(cell15);
		outerTbl.addCell(cell16);
		outerTbl.addCell(cell17);
		outerTbl.addCell(blank2).setBorder(0);
		
		
		
		
		
		
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}
	private void footerdetails() {
		// TODO Auto-generated method stub
		PdfPTable outerTbl=new PdfPTable(3);
		outerTbl.setWidthPercentage(100f);
		try {
			outerTbl.setWidths(new float[] {47,45,8});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		outerTbl.setSpacingBefore(20f);
		
		outerTbl.addCell(cell.getCell("      ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("Signature of the employer or his agent ", font10, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(cell.getCell("      ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	
	

}
