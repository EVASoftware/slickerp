package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.MultipleOrdersInvoicePdf;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.server.addhocprinting.ServiceGSTInvoice;
import com.slicktechnologies.server.addhocprinting.ServiceGSTInvoiceForAyendev;
import com.slicktechnologies.server.addhocprinting.ServiceInvoicePdf;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class CreatePurchaseInvoicePdfServlet extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = -990428922311085444L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.

		try {
			String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			VendorInvoice invoiceentity = ofy().load().type(VendorInvoice.class).id(count).now();
			boolean isMultipleBillingDocflag = loadMultipleBillingDoc(invoiceentity);
//			if (isMultipleBillingDocflag) {

//				System.out.println("Multibilling invoice");
//				// ServiceInvoicePdf invpdf=new ServiceInvoicePdf();
//				PurchaseGSTMultiBillInvoice invpdf = new PurchaseGSTMultiBillInvoice();
//				invpdf.document = new Document();
//				Document document = invpdf.document;
//				PdfWriter writer = PdfWriter.getInstance(document,
//						response.getOutputStream()); // write the pdf in
//														// response
//
//				if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
//					writer.setPageEvent(new PdfCancelWatermark());
//				}
//				// ************rohan added this condition for removing draft
//				// from proforma invoice
//
//				else if (!invoiceentity.getStatus().equals("Approved")
//						&& (!invoiceentity.getStatus().equals(
//								BillingDocument.PROFORMAINVOICE))
//						&& (!invoiceentity.getStatus().equals(
//								BillingDocument.BILLINGINVOICED))) {
//					writer.setPageEvent(new PdfWatermark());
//				}
//
//				document.open();
//				String preprintStatus = request.getParameter("preprint");
//				System.out.println("ppppppppppppppppppooooooooooo" + count);
//				invpdf.setInvoice(count);
//				invpdf.createPdf(preprintStatus);
//				document.close();

//			} else{
						System.out.println("SINGLE BILLING INVOICE");
						// ServiceInvoicePdf invpdf=new ServiceInvoicePdf();
						PurchaseGSTInvoice invpdf = new PurchaseGSTInvoice();
						invpdf.document = new Document();
						Document document = invpdf.document;
						PdfWriter writer = PdfWriter.getInstance(document,
								response.getOutputStream()); // write the pdf in
																// response

						if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
							writer.setPageEvent(new PdfCancelWatermark());
						}
						// ************rohan added this condition for removing
						// draft from proforma invoice

						else if (!invoiceentity.getStatus().equals("Approved")
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.PROFORMAINVOICE))
								&& (!invoiceentity.getStatus().equals(
										BillingDocument.BILLINGINVOICED))) {
							writer.setPageEvent(new PdfWatermark());
						}

						document.open();
						String preprintStatus = request
								.getParameter("preprint");
						System.out.println("ppppppppppppppppppooooooooooo"
								+ count);
						invpdf.setInvoice(count);
						invpdf.createPdf(preprintStatus);
						document.close();
//			}
		}

		catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private boolean loadMultipleBillingDoc(Invoice invoiceentity) {
		// TODO Auto-generated method stub

		HashSet<Integer> billCount = new HashSet<Integer>();
		for (BillingDocumentDetails billingDoc : invoiceentity.getArrayBillingDocument()) {
			billCount.add(billingDoc.getBillId());
		}
		if (billCount.size() > 1) {
			return true;
		}
		return false;
	}


}
