package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;

public class CreateCNCInvoicePdf extends HttpServlet {


	/**
	 * 
	 */
	private static final long serialVersionUID = -990428922311085444L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
													// browser to identify the
													// response type.

		try {
			String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			String pdftype = request.getParameter("type");
			boolean isPortrait  = false;
			Invoice invoiceentity = ofy().load().type(Invoice.class).id(count).now();
			if(invoiceentity != null){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PdfPortraitMode", invoiceentity.getCompanyId())){
				isPortrait = true;
			 }
			}
						System.out.println("CNC INVOICE");
						CNCInvoicePdf invpdf = new CNCInvoicePdf();
						invpdf.document = new Document(PageSize.A4.rotate());
						
						
						 if(isPortrait){
							invpdf.document = new Document();
						}
						
						Document document = invpdf.document;
						if(isPortrait){
							document.setMargins(2, 2, 2, 2);
						}else{
							document.setMargins(10,10, 10, 10);
						}

						PdfWriter writer = PdfWriter.getInstance(document,
								response.getOutputStream()); // write the pdf in
																// response
						if(invoiceentity.getStatus().equals("Cancelled"))	{
							 writer.setPageEvent(new PdfCancelWatermark());
						}else 
					   if(!invoiceentity.getStatus().equals("Approved")){	
							 writer.setPageEvent(new PdfWatermark());
						}
//						if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
//							writer.setPageEvent(new PdfCancelWatermark());
//						}
//						else if (!invoiceentity.getStatus().equals("Approved")
//								&& (!invoiceentity.getStatus().equals(
//										BillingDocument.PROFORMAINVOICE))
//								&& (!invoiceentity.getStatus().equals(
//										BillingDocument.BILLINGINVOICED))) {
//							writer.setPageEvent(new PdfWatermark());
//						}

						document.open();
//						String preprintStatus = request
//								.getParameter("preprint");
						System.out.println("ppppppppppppppppppooooooooooo"
								+ count);
						invpdf.setInvoice(count);
						invpdf.createPdf(pdftype , isPortrait);
						document.close();
		}

		catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}

//package com.slicktechnologies.server.addhocprinting.sasha;
//
 //import static com.googlecode.objectify.ObjectifyService.ofy;
//
//import java.io.IOException;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import com.itextpdf.text.Document;
//import com.itextpdf.text.DocumentException;
//import com.itextpdf.text.PageSize;
//import com.itextpdf.text.Phrase;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
//import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
//import com.slicktechnologies.server.addhocprinting.PdfWatermark;
//import com.slicktechnologies.server.utility.ServerAppUtility;
//import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
//import com.slicktechnologies.shared.common.salesprocess.Invoice;
//import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
//
//public class CreateCNCInvoicePdf extends HttpServlet {
//
//
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = -990428922311085444L;
//
//	@Override
//	protected void doGet(HttpServletRequest request,
//			HttpServletResponse response) throws ServletException, IOException {
//
//		response.setContentType("application/pdf"); // Type of response , helps
//													// browser to identify the
//													// response type.
//
//		try {
//			String stringid = request.getParameter("Id");
//			stringid = stringid.trim();
//			Long count = Long.parseLong(stringid);
//			String pdftype = request.getParameter("type");
//			boolean isPortrait  = false;
//			Invoice invoiceentity = ofy().load().type(Invoice.class).id(count).now();
//			if(invoiceentity != null){
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "PdfPortraitMode", invoiceentity.getCompanyId())){
//				isPortrait = true;
//			 }
//			}
//						System.out.println("CNC INVOICE");
//						CNCInvoicePdf invpdf = new CNCInvoicePdf();
//						invpdf.document = new Document(PageSize.A4.rotate());
//						if(isPortrait){
//							invpdf.document = new Document();
//						}
//						
//						Document document = invpdf.document;
//						if(isPortrait){
//							document.setMargins(2, 2, 2, 2);
//						}
//
//						PdfWriter writer = PdfWriter.getInstance(document,
//								response.getOutputStream()); // write the pdf in
//																// response
//
////						if (invoiceentity.getStatus().equals(Invoice.CANCELLED)) {
////							writer.setPageEvent(new PdfCancelWatermark());
////						}
////						else if (!invoiceentity.getStatus().equals("Approved")
////								&& (!invoiceentity.getStatus().equals(
////										BillingDocument.PROFORMAINVOICE))
////								&& (!invoiceentity.getStatus().equals(
////										BillingDocument.BILLINGINVOICED))) {
////							writer.setPageEvent(new PdfWatermark());
////						}
//
//						document.open();
////						String preprintStatus = request
////								.getParameter("preprint");
//						System.out.println("ppppppppppppppppppooooooooooo"
//								+ count);
//						invpdf.setInvoice(count);
//						invpdf.createPdf(pdftype , isPortrait);
//						document.close();
//		}
//
//		catch (DocumentException e) {
//			e.printStackTrace();
//		}
//	}
//}
