package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;



import com.google.gwt.layout.client.Layout.Alignment;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.CNCBillAnnexure;
import com.slicktechnologies.shared.CNCBillAnnexureBean;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxSummaryBean;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.cnc.StaffingDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class CNCInvoicePdf {
	Logger logger = Logger.getLogger("CNCInvoice.class");
	Document document;	
	Invoice invoice;
	List<BillingDocumentDetails> billingDoc; 
	List<ContractCharges> billingTaxesLis;
	Customer cust;
	List<ArticleType> articletype;
	ProcessConfiguration processConfig;
	Company comp;
	String companyName = "";
	/**
	 * @author Anil,Date : 30-01-2019
	 * Changing noOfLines from 8 to 20
	 * For Sasha By Sonu
	 */
//	int noOfLines = 8;
	int noOfLines = 20;
	
	int prouductCount=0;
	SuperProduct sup;
	double total=0;
	SimpleDateFormat fmt;
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df2 = new DecimalFormat("00");
	List<State> stateList;
	CNC cnc ;
	CompanyPayment companyPayment;
	double totalTax = 0;
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font13 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	//end by jayshree
//	Font font6bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
//	Font font6 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 6);
	Font font5bold = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD);
	Font font5 = new Font(Font.FontFamily.HELVETICA, 5);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	PdfPCell coloncell = null;
	Branch branchDt;
	String invoiceype = "";
	double totalAmount = 0 , totalCGST = 0 , totalSGST = 0 , totalIGST = 0;
	HashMap<String ,TaxSummaryBean> map;
	boolean invoiceRefNo=false;
	
	/**
	 * Date : 22-12-2018 By Anil
	 */
	CustomerBranchDetails customerBranch=null;
	int j = 0;
	
	/**
	 * @author Anil, Date: 31-01-2019
	 * 
	 */
	int totalNoOfLineItems=0;
	
	
	/**
	 * @author Anil, Date : 22-02-2019
	 * This flag is true if same designation is added more than ones in CNC
	 * Issue raised by Megha & Ashish for sasha
	 */
	boolean designationFlag=false;
	HashSet<String> hsDesignation; 
	
	/**
	 * @author Anil ,Date :13-03-2019
	 */
	int minRange=8;
	int maxRange=20;
	
	
	private PdfPCell imageSignCell;

	
	public void setInvoice(Long count) {
		invoice = ofy().load().type(Invoice.class).id(count).now();
		if (invoice.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoice.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", invoice.getPersonInfo().getCount())
					.filter("companyId", invoice.getCompanyId()).first().now();

		// Load Company
		if (invoice.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoice.getCompanyId()).first().now();
		companyName = comp.getBusinessUnitName();

		if (invoice.getCompanyId() != null)
			cnc = ofy().load().type(CNC.class)
					.filter("count", invoice.getContractCount())
					.filter("companyId", invoice.getCompanyId()).first().now();
		else
			cnc = ofy().load().type(CNC.class)
					.filter("count", invoice.getContractCount()).first().now();

		if (invoice != null && invoice.getBranch() != null
				&& invoice.getBranch().trim().length() > 0) {

		branchDt = ofy().load().type(Branch.class)
					.filter("companyId", invoice.getCompanyId())
					.filter("buisnessUnitName", invoice.getBranch())
					.first().now();
		}
		
		if(invoice.getPaymentMode()!=null&&!invoice.getPaymentMode().equals("")){
			logger.log(Level.SEVERE,"INSIDE load Companypayment");
			try{
				String paymentMode="";
				String paymentmodevalue="";
				if(invoice.getPaymentMode().contains("/")){
					String [] payArr=invoice.getPaymentMode().split("/");
					paymentMode=payArr[1];
					
					paymentmodevalue=paymentMode.trim();
					logger.log(Level.SEVERE,"PAYMENT MODE"+paymentmodevalue);
				}
				companyPayment = ofy().load().type(CompanyPayment.class)
						.filter("paymentBankName", paymentmodevalue)
						.filter("companyId", invoice.getCompanyId()).first()
						.now();
				
				logger.log(Level.SEVERE,"INSIDE TRY"+paymentMode);
//				if(companyPayment==null){
//					logger.log(Level.SEVERE,"BANKKKK NAMEEEE"+companyPayment.getPaymentBranch());
//				}else{
//					logger.log(Level.SEVERE,"BANKKKK NAMEEEE11111"+companyPayment.getPaymentBranch());
//				}
				
			}catch(Exception e){
					
			}
		}else{
			logger.log(Level.SEVERE,"INSIDE ELSEEEEE load Companypayment");
			companyPayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", invoice.getCompanyId()).first()
					.now();
		}
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
				"Branch", "BranchAsCompany", comp.getCompanyId())) {

			logger.log(Level.SEVERE, "Process active --");
			
				if (branchDt != null && branchDt.getPaymentMode() != null
						&& !branchDt.getPaymentMode().trim().equals("")) {

					logger.log(Level.SEVERE,
							"Process active --" + branchDt.getPaymentMode());

					List<String> paymentDt = Arrays.asList(branchDt
							.getPaymentMode().trim().split("/"));

					if (paymentDt.get(0).trim().matches("[0-9]+")) {

						int payId = Integer.parseInt(paymentDt.get(0).trim());

						companyPayment = ofy().load()
								.type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", invoice.getCompanyId())
								.first().now();

						if (companyPayment != null) {
							comp = ServerAppUtility.changeBranchASCompany(
									branchDt, comp);
						}

					}

				}
			
		}

		stateList = ofy().load().type(State.class)
				.filter("companyId", invoice.getCompanyId()).list();
		Phrase colonph = new Phrase(":", font6);
		coloncell = new PdfPCell(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		calculateTotalValue();
		
		/**
		 * Date : 22-12-2018 By Anil
		 */
		if(invoice!=null&&invoice.getCustomerBranch()!=null&&!invoice.getCustomerBranch().equals("")){
			customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", invoice.getCompanyId()).filter("buisnessUnitName", invoice.getCustomerBranch()).filter("status", true).first().now();
		}
		/**
		 * @author Anil,Date : 31-01-2019
		 */
	if(invoice!=null&&invoice.getSalesOrderProducts()!=null){
			/**
			 * @author Amol, Date : 04-06-2019
			 * for calculating non zero quantity invoice line item
			 */
//			totalNoOfLineItems=invoice.getSalesOrderProducts().size();
			for(SalesOrderProductLineItem item :invoice.getSalesOrderProducts()){
				if(item.getTotalAmount()!=0){
					totalNoOfLineItems++;
				}
			}
		}
		
		checkForDuplicateDesignation();
		
	}
	
	/**
	 * @author Anil,Date : 22-02-2019
	 * This method check whether duplicate designation is added as invoice line item
	 * this causes error when we fetching ,no of staff and CTC info from CNC staffing list
	 */
	private void checkForDuplicateDesignation() {
		// TODO Auto-generated method stub
		int desgCounter=0;
		if(invoice!=null){
			if(invoice.getSubBillType()!=null&&(invoice.getSubBillType().equals(AppConstants.STAFFING)||invoice.getSubBillType().equals(AppConstants.STAFFINGANDRENTAL))){
				logger.log(Level.SEVERE, "Inside Staffing BILL "+designationFlag);
				hsDesignation=new HashSet<String>();
				for(SalesOrderProductLineItem item:invoice.getSalesOrderProducts()){
					desgCounter=0;
					hsDesignation.add(item.getProdName());
					for(SalesOrderProductLineItem item1:invoice.getSalesOrderProducts()){
						if(item.getProdName().equals(item1.getProdName())){
							desgCounter++;
						}
					}
					if(desgCounter>1){
						designationFlag=true;
					}
				}
				
				logger.log(Level.SEVERE, "Designation Flag value : "+designationFlag);
				logger.log(Level.SEVERE, "Unique Designation size : "+hsDesignation.size()+" TOTAL : "+invoice.getSalesOrderProducts().size());
			}
		}
	}
	
	private StaffingDetails getStaffingDetailsFromCNC(SalesOrderProductLineItem item) {
		// TODO Auto-generated method stub
		Date fromDate=DateUtility.getDateWithTimeZone("IST",invoice.getBillingPeroidFromDate());
		Date toDate=DateUtility.getDateWithTimeZone("IST",invoice.getBillingPeroidToDate());
		long diff= toDate.getTime() - fromDate.getTime();			
		int noOfDays= ((int) (diff / (24 * 60 * 60 * 1000))) + 1;
        logger.log(Level.SEVERE," :::=No of days  = :::  "+noOfDays);
		
		for(StaffingDetails staff : cnc.getSaffingDetailsList()){
			double totalPay = 0, mgntFees = 0, total = 0,totalPrice=0;
			try {
				mgntFees = Double.parseDouble(staff.getManagementFees())/ staff.getNoOfStaff();
			} catch (Exception e) {
				mgntFees = 0;
			}
			try {
				total = Double.parseDouble(staff.getTotalOfPaidAndOverhead());
			} catch (Exception e) {
				total = 0;
			}
			totalPay = total + mgntFees;
			totalPrice=Math.round((totalPay / noOfDays) * 100.0) / 100.0;
			 
			logger.log(Level.SEVERE, "No. Of Staff :"+staff.getNoOfStaff()+" No. of Days : "+noOfDays+" Calculated Price : "+totalPrice+" Stored Price : "+item.getPrice());
			if(totalPrice==item.getPrice()){
				int manDays=0;
				int calculatedNoOfStaff=0;
				if(item.getArea()!=null&&!item.getArea().equals("")){
					try{
						manDays=Integer.parseInt(item.getArea());
						calculatedNoOfStaff=manDays/noOfDays;
						logger.log(Level.SEVERE, "MAN DAYS : "+manDays+" Calculated No Of Staff : "+calculatedNoOfStaff+" Strored No of Staff : "+staff.getNoOfStaff());
						
						/**
						 * @author Anil/Rahul Tiwari
						 * @since 14-09-2020
						 */
						if(item.getManpower()!=null&&!item.getManpower().equals("")){
							try{
								int noOfManPower=Integer.parseInt(item.getManpower());
								if(noOfManPower==staff.getNoOfStaff()){
									return staff;
								}
							}catch(Exception e){
								
							}
							
						}else if(calculatedNoOfStaff==staff.getNoOfStaff()){
							return staff;
						}
					}catch(Exception e){
//						return null;
						return staff;
					}
				}else{
					return staff;
				}
				
				
			}
		}
		return null;
	}
	public void createPdf(String type , boolean isPortrait){
		logger.log(Level.SEVERE, "Total no. of line item : "+totalNoOfLineItems);
		invoiceype = type;
		if(isPortrait){
			font6 = font5;
			font6bold = font5bold;
			minRange=22;
			maxRange=34;
		}else{
			minRange=8;
			maxRange=20;
		}
		fmt = new SimpleDateFormat("dd MMMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		if(isPortrait){
			createletterhead();
		}
		createTitle();
		createCompanyInformationTab(isPortrait);
		createBillingAndShippingInformationTab();
	//	createSubTitle("Services Supplied");
		//createSubTitle(" ");
		switch (invoiceype) {
		case AppConstants.STAFFINGANDRENTAL:
		case AppConstants.STAFFING:	
		case AppConstants.CONSOLIDATEBILL:
		case AppConstants.ARREARSBILL:	
			createSubTitle("Services Supplied");
			createSubTitle(" ");
			getStaffingTable(0);
			createSubTitle(" ");
			break;
		case AppConstants.OVERTIME:
			createSubTitle("Services Supplied");
			createSubTitle(" ");
			getStaffingTable(0);
			createSubTitle(" ");
			break;
		case AppConstants.NATIONALHOLIDAY:
			createSubTitle("Services Supplied");
			createSubTitle(" ");
			getStaffingTable(0);
			createSubTitle(" ");
			break;
		case AppConstants.CONSUMABLES:	
			createSubTitle("Consumables Supplied");
			createSubTitle(" ");
			getConsumablesTable(0);
			createSubTitle(" ");
			break;
		case AppConstants.OTHERSERVICES:
			createSubTitle("Services Supplied");
			createSubTitle(" ");
			getOtherServiceTable(0);
			createSubTitle(" ");
			break;
		case AppConstants.FIXEDCONSUMABLES:	
			createSubTitle("Consumables Supplied");
			createSubTitle(" ");
			getConsumablesTable(0);
			createSubTitle(" ");
			break;
		case AppConstants.PUBLICHOLIDAY:
			createSubTitle("Services Supplied");
			createSubTitle(" ");
			getStaffingTable(0);
			createSubTitle(" ");
			break;
		default:
			createSubTitle(" ");
			getConsumablesTable(0);
			createSubTitle(" ");
			break;
		}

		//createSubTitle(" ");
		if(totalNoOfLineItems<=minRange){
//		if(totalNoOfLineItems<=8){
			getAmountInwords("Total Invoice Value (in words):" , invoice.getNetPayable());
			taxDetailsTab();
			getAmountInwords("Total Tax Amount (in words):" , totalTax);
			createTermsAndConditionTab(isPortrait);
//		}else if(totalNoOfLineItems>8&&totalNoOfLineItems<=20){
//			int blankLine=20-totalNoOfLineItems;
		}else if(totalNoOfLineItems>minRange&&totalNoOfLineItems<=maxRange){
			int blankLine=maxRange-totalNoOfLineItems;
			System.out.println("Blank spaces/lines : "+blankLine);
			if(blankLine!=0){
				getAmountInwords("Total Invoice Value (in words):" , invoice.getNetPayable());
				createBlankLine(blankLine);
			}
			createTermsAndConditionTab(isPortrait);
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			if(blankLine==0){
			getAmountInwords("Total Invoice Value (in words):" , invoice.getNetPayable());
			}
			taxDetailsTab();
			getAmountInwords("Total Tax Amount (in words):" , totalTax);
			createTermsAndConditionTab(isPortrait);
		}
		
		
//		createProductTitleTab();
//		createProductDetailsTab();
//		createTotalTab();
//		createFooterTab();
		if (noOfLines == 0 && prouductCount != 0) {
			System.out.println("noOfLines...."+noOfLines);
			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
			try {
				document.add(nextpage);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			switch (invoiceype) {
			case AppConstants.STAFFINGANDRENTAL:
			case AppConstants.STAFFING:		
			case AppConstants.CONSOLIDATEBILL:
				createSubTitle("Services Supplied");
				createSubTitle(" ");
				getStaffingTable(prouductCount);
				createSubTitle(" ");
				break;
			case AppConstants.OVERTIME:
				createSubTitle("Services Supplied");
				createSubTitle(" ");
				getStaffingTable(prouductCount);
				createSubTitle(" ");
				break;	
			case AppConstants.NATIONALHOLIDAY:
				createSubTitle("Services Supplied");
				createSubTitle(" ");
				getStaffingTable(prouductCount);
				createSubTitle(" ");
				break;
			case AppConstants.CONSUMABLES:
				createSubTitle("Consumables Supplied");
				createSubTitle(" ");
				getConsumablesTable(prouductCount);
				createSubTitle(" ");
				break;
			case AppConstants.OTHERSERVICES:
				createSubTitle("Services Supplied");
				createSubTitle(" ");
				getOtherServiceTable(prouductCount);
				createSubTitle(" ");
				break;
			case AppConstants.FIXEDCONSUMABLES:	
				createSubTitle("Consumables Supplied");
				createSubTitle(" ");
				getConsumablesTable(0);
				createSubTitle(" ");
				break;
			case AppConstants.PUBLICHOLIDAY:
				createSubTitle("Services Supplied");
				createSubTitle(" ");
				getStaffingTable(prouductCount);
				createSubTitle(" ");
				break;	
			default:
				createSubTitle(" ");
				getConsumablesTable(prouductCount);
				createSubTitle(" ");
				break;
			}
			
			getAmountInwords("Total Invoice Value (in words):" , invoice.getNetPayable());
			taxDetailsTab();
			getAmountInwords("Total Tax Amount (in words):" , totalTax);
			createTermsAndConditionTab(isPortrait);

		}
		
		if(invoiceype.equalsIgnoreCase(AppConstants.ARREARSBILL)){
			if(invoice.getCncBillAnnexureId() != null && invoice.getCncBillAnnexureId().size() >0){
				addAnnexure();
			}
		}
		
	}
	private void createBlankLine(int blankLine) {
		// TODO Auto-generated method stub
		System.out.println("Inside create blank line table. "+blankLine);
		PdfPTable blankTbl = new PdfPTable(1);
		blankTbl.setWidthPercentage(100);
		
		Phrase continuePh = new Phrase("continued...     " , font7bold);
		PdfPCell continueCell= new PdfPCell(continuePh);
		continueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		continueCell.setBorderWidthBottom(0);
		blankTbl.addCell(continueCell);
		
		Phrase blankPh = new Phrase(" " , font6bold);
		PdfPCell blankCell=null;
		for(int i=0;i<blankLine;i++){
			blankCell = new PdfPCell(blankPh);
			blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			if(i==0){
//				blankCell.setBorderWidthBottom(0);
//			}else 
			if(i==blankLine-1){
				blankCell.setBorderWidthTop(0);
			}else{
				blankCell.setBorderWidthBottom(0);
				blankCell.setBorderWidthTop(0);
			}
			blankTbl.addCell(blankCell);
		}
		
		try {
			document.add(blankTbl);
			System.out.println("Blank Table Added. "+blankLine);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void createTitle(){
		PdfPTable titleTab = new PdfPTable(1);
		titleTab.setWidthPercentage(100);
		
		Phrase titlephbl = new Phrase(" ", font10);
		PdfPCell titleCellbl = new PdfPCell(titlephbl);
		titleCellbl.setBorder(0);
		titleTab.addCell(titleCellbl); 
		
		/**
		 * @author Vijay  Date :- 29-10-202
		 * Des :- updated code if invoice is Tax invoice then Title will be Tax Invoice
		 * and if invoice is proforma invoice then Title will be Tax Invoice
		 */
//		Phrase titleph = new Phrase("TAX INVOICE", font12bold);
		String titlepdf = "Tax Invoice";
		if (invoice.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE)){
			titlepdf = "PROFORMA INVOICE";
		}
		else{
			titlepdf = "TAX INVOICE";
		}
		Phrase titleph = new Phrase(titlepdf, font12bold);
		PdfPCell titleCell = new PdfPCell(titleph);
		titleCell.setPaddingTop(5);
		titleCell.setPaddingBottom(5);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(titleCell);
		
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	private void createCompanyInformationTab(boolean isPortrait){
		Phrase colonph = new Phrase(":", font6);
		PdfPCell coloncell = new PdfPCell(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		PdfPTable invoiceAddtab = new PdfPTable(3);
		invoiceAddtab.setWidthPercentage(100);
		
		try {
			invoiceAddtab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase invoiceto = new Phrase("Company Name", font6bold);
		PdfPCell invoicetocell = new PdfPCell(invoiceto);
		invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicetocell.setColspan(3);
		invoicetocell.setBorder(0);
		invoiceAddtab.addCell(invoicetocell);
		
		Phrase compname = new Phrase(companyName, font6bold);
		PdfPCell compnamecell = new PdfPCell(compname);
		compnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell.setColspan(3);
		compnamecell.setBorder(0);
		invoiceAddtab.addCell(compnamecell);
		
		String branch ="";
		if(invoice.getBranch().equalsIgnoreCase("Corporate Office")){
			branch = invoice.getBranch()+" :";
		}else{
			branch = "Branch Office :";
		}
		
		Phrase branchName = new Phrase(branch, font6);
		PdfPCell branchNamecell = new PdfPCell(branchName);
		branchNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchNamecell.setColspan(3);
		branchNamecell.setBorder(0);
		invoiceAddtab.addCell(branchNamecell);
	
		Phrase compadd = new Phrase(comp.getAddress().getCompleteAddress(), font6);
		PdfPCell compaddcell = new PdfPCell(compadd);
		compaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compaddcell.setColspan(3);
		compaddcell.setBorder(0);
		invoiceAddtab.addCell(compaddcell);
		
		Phrase statename = new Phrase("State Name", font6);
		PdfPCell statenamecell = new PdfPCell(statename);
		statenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamecell.setBorder(0);
		invoiceAddtab.addCell(statenamecell);
		
		invoiceAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statecodetab=new PdfPTable(3);
		statecodetab.setWidthPercentage(100);
		try {
			statecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevalname = new Phrase(comp.getAddress().getState(), font6);
		PdfPCell statenamevalcell = new PdfPCell(statevalname);
		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevalcell.setBorder(0);
		statecodetab.addCell(statenamevalcell);
		
		Phrase statecode = new Phrase("Code :", font6);
		PdfPCell statecodecell = new PdfPCell(statecode);
		statecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodecell.setBorder(0);
		statecodetab.addCell(statecodecell);
//		
		ServerAppUtility serverApp = new ServerAppUtility();
		String stateCodeStr = serverApp.getStateOfCompany(comp, invoice.getBranch().trim(), stateList);
		Phrase stateCode = new Phrase(stateCodeStr, font6);

		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		stateCodeCell.setBorder(0);
		statecodetab.addCell(stateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell statecodetabcell = new PdfPCell(statecodetab);
		statecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodetabcell.setBorder(0);
		invoiceAddtab.addCell(statecodetabcell);

		
		Phrase gstinph = new Phrase("GSTIN/UIN", font6);
		PdfPCell gstincell = new PdfPCell(gstinph);
		gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstincell.setBorder(0);
		invoiceAddtab.addCell(gstincell);
		
		invoiceAddtab.addCell(coloncell);

		String gstin = "", gstinText = "";
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			gstin = serverApp.getGSTINOfCompany(comp, invoice.getBranch().trim());
			System.out.println("gstin" + gstin);

		} else {
			gstinText = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinText);
		}

		Phrase gstinvalph = null;
		if (!gstin.trim().equals("")) {
			gstinvalph = new Phrase( gstin, font6);
		} else if (!gstinText.trim().equalsIgnoreCase("")) {
			gstinvalph = new Phrase(gstinText, font6);
		} else {
			gstinvalph = new Phrase("", font6);

		}
		
		PdfPCell gstinvalcell = new PdfPCell(gstinvalph);
		gstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell.setBorder(0);
		invoiceAddtab.addCell(gstinvalcell);
		
		
				
		
		Phrase cinph = new Phrase("CIN", font6);
		PdfPCell cincell = new PdfPCell(cinph);
		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cincell.setBorder(0);
		invoiceAddtab.addCell(cincell);
		
		invoiceAddtab.addCell(coloncell);
		
		String cinval="";
		String panNo = "";
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("CIN")) {

				cinval=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("PAN")) {

				panNo=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}

			}
			
		Phrase cinvalph = new Phrase(cinval, font6);
		PdfPCell cinvalphCell = new PdfPCell(cinvalph);
		cinvalphCell.setBorder(0);
		invoiceAddtab.addCell(cinvalphCell);
			
		
		Phrase panph = new Phrase("PAN NO.", font6);
		PdfPCell pancell = new PdfPCell(panph);
		pancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pancell.setBorder(0);
		invoiceAddtab.addCell(pancell);
		
		invoiceAddtab.addCell(coloncell);
		
		Phrase panvalph = new Phrase(panNo, font6);
		PdfPCell panvalcell = new PdfPCell(panvalph);
		panvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		panvalcell.setBorder(0);
		invoiceAddtab.addCell(panvalcell);
		
		
		
		Phrase emailph = new Phrase("Contact Details ", font6);
		PdfPCell emailcell = new PdfPCell(emailph);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailcell.setBorder(0);
		invoiceAddtab.addCell(emailcell);
		
		invoiceAddtab.addCell(coloncell);
		String email = "";
		if(comp.getEmail()!=null){
			email = comp.getEmail();
		}		
		Phrase emailValph = new Phrase(email, font6);
		PdfPCell emailValcell = new PdfPCell(emailValph);
		emailValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailValcell.setBorder(0);
		invoiceAddtab.addCell(emailValcell);
		
		PdfPTable bankDetailsTab = new PdfPTable(3);
		bankDetailsTab.setWidthPercentage(100);
		
		try {
			bankDetailsTab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		Phrase invoiceNoph = new Phrase("Invoice NO.", font6);
//		PdfPCell invoiceNocell = new PdfPCell(invoiceNoph);
//		invoiceNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invoiceNocell.setBorder(0);
//		bankDetailsTab.addCell(invoiceNocell);
//		
//		bankDetailsTab.addCell(coloncell);
//		
//		Phrase invoiceNovalph = new Phrase(invoice.getCount()+"", font6);
//		PdfPCell invoiceNovalcell = new PdfPCell(invoiceNovalph);
//		invoiceNovalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invoiceNovalcell.setBorder(0);
//		bankDetailsTab.addCell(invoiceNovalcell);
//		
		/*****Date 7-2-2019 
		 * @author Amol
		   added invoice ref no *****/
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "RefrenceOrderNumber", invoice.getCompanyId())){
			invoiceRefNo=true;
		}
			if(invoiceRefNo){
		Phrase invoiceRefNo = new Phrase("Invoice No ", font6);
		PdfPCell invoicecell = new PdfPCell(invoiceRefNo);
		invoicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicecell.setBorder(0);
		bankDetailsTab.addCell(invoicecell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase invoiceRefNovalue = new Phrase(invoice.getInvRefNumber(), font6);
		PdfPCell invoicecell1 = new PdfPCell(invoiceRefNovalue);
		invoicecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicecell1.setBorder(0);
		bankDetailsTab.addCell(invoicecell1);
		}else{
			Phrase invoiceRefNo = new Phrase(" ");
			PdfPCell invoicecell = new PdfPCell(invoiceRefNo);
			invoicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicecell.setBorder(0);
			bankDetailsTab.addCell(invoicecell);
			
			Phrase invoiceRefNovalue1 = new Phrase("");
			PdfPCell invoicecell2 = new PdfPCell(invoiceRefNovalue1);
			invoicecell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicecell2.setBorder(0);
			bankDetailsTab.addCell(invoicecell2);
			
			Phrase invoiceRefNovalue = new Phrase("");
			PdfPCell invoicecell1 = new PdfPCell(invoiceRefNovalue);
			invoicecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicecell1.setBorder(0);
			bankDetailsTab.addCell(invoicecell1);
			
		}
		
		
	
		
		Phrase invoiceDateph = new Phrase("Invoice Date", font6);
		PdfPCell invoiceDatecell = new PdfPCell(invoiceDateph);
		invoiceDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoiceDatecell.setBorder(0);
		bankDetailsTab.addCell(invoiceDatecell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase invoiceDatevalph = new Phrase(fmt.format(invoice.getInvoiceDate()), font6);
		PdfPCell invoiceDatevalcell = new PdfPCell(invoiceDatevalph);
		invoiceDatevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoiceDatevalcell.setBorder(0);
		bankDetailsTab.addCell(invoiceDatevalcell);

		String period = "";
		if(invoice.getBillingPeroidFromDate()!=null && !invoice.getBillingPeroidFromDate().equals("")){
			period = "From : "+fmt.format(invoice.getBillingPeroidFromDate());
		}
		if(invoice.getBillingPeroidToDate()!=null && !invoice.getBillingPeroidToDate().equals("")){
			period = period + "       To : " + fmt.format(invoice.getBillingPeroidToDate());
		}
		
		Phrase servicePeriodph = new Phrase("Service Period", font6);
		PdfPCell servicePeriodcell = new PdfPCell(servicePeriodph);
		servicePeriodcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		servicePeriodcell.setBorder(0);
		bankDetailsTab.addCell(servicePeriodcell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase servicePeriodvalph = new Phrase(period, font6);
		PdfPCell servicePeriodvalcell = new PdfPCell(servicePeriodvalph);
		servicePeriodvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		servicePeriodvalcell.setBorder(0);
		bankDetailsTab.addCell(servicePeriodvalcell);

		Phrase bankDetailsph = new Phrase("Banking Details", font6bold);
		PdfPCell bankDetailscell = new PdfPCell(bankDetailsph);
		bankDetailscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankDetailscell.setBorder(0);
		bankDetailscell.setColspan(3);
		bankDetailsTab.addCell(bankDetailscell);
		
		String bankName = "" , bankBranch = "",bankAccNo = "" ,bankIFSC = "",accountName="" ;
		if(companyPayment != null){
			logger.log(Level.SEVERE,"INSIDE Company Payment");
			if(companyPayment.getPaymentBankName()!=null&&!companyPayment.getPaymentBankName().equals("")){
				bankName = companyPayment.getPaymentBankName();
			}
			if(companyPayment.getPaymentBranch() !=null&&!companyPayment.getPaymentBranch().equals("")){
				bankBranch = companyPayment.getPaymentBranch();
			}
			if(companyPayment.getPaymentAccountNo() != null&&!companyPayment.getPaymentAccountNo().equals("")){
				bankAccNo = companyPayment.getPaymentAccountNo();
			}
			if(companyPayment.getPaymentIFSCcode()!=null&&!companyPayment.getPaymentIFSCcode().equals("")){
				bankIFSC = companyPayment.getPaymentIFSCcode();
			}
			
			if(companyPayment.getPaymentFavouring()!=null&&!companyPayment.getPaymentFavouring().equals("")){
				accountName=companyPayment.getPaymentFavouring();
			}
			
		}
			
		/**Date 4-6-2019 by Amol
		 * added Account Name 
		 */
		
		Phrase Accountname = new Phrase("Account Name", font6);
		PdfPCell AccountNamecell = new PdfPCell(Accountname);
		AccountNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		AccountNamecell.setBorder(0);
		bankDetailsTab.addCell(AccountNamecell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase Accountnamevalph = new Phrase(accountName, font6);
		PdfPCell Accountnamevalcell = new PdfPCell(Accountnamevalph);
		Accountnamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		Accountnamevalcell.setBorder(0);
		bankDetailsTab.addCell(Accountnamevalcell);
		
		Phrase bankNameph = new Phrase("Bank Name", font6);
		PdfPCell bankNamecell = new PdfPCell(bankNameph);
		bankNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankNamecell.setBorder(0);
		bankDetailsTab.addCell(bankNamecell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase bankNamevalph = new Phrase(bankName, font6);
		PdfPCell bankNamevalcell = new PdfPCell(bankNamevalph);
		bankNamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankNamevalcell.setBorder(0);
		bankDetailsTab.addCell(bankNamevalcell);
		
		Phrase banckBranchph = new Phrase("Bank Branch", font6);
		PdfPCell bankBranchcell = new PdfPCell(banckBranchph);
		bankBranchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankBranchcell.setBorder(0);
		bankDetailsTab.addCell(bankBranchcell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase bankBranchvalph = new Phrase(bankBranch, font6);
		PdfPCell banckBranchvalcell = new PdfPCell(bankBranchvalph);
		banckBranchvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		banckBranchvalcell.setBorder(0);
		bankDetailsTab.addCell(banckBranchvalcell);


		Phrase bankAccoNoph = new Phrase("Bank A/c NO.", font6);
		PdfPCell bankAccNocell = new PdfPCell(bankAccoNoph);
		bankAccNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankAccNocell.setBorder(0);
		bankDetailsTab.addCell(bankAccNocell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase bankAccNovalph = new Phrase(bankAccNo, font6);
		PdfPCell bankAccNovalcell = new PdfPCell(bankAccNovalph);
		bankAccNovalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankAccNovalcell.setBorder(0);
		bankDetailsTab.addCell(bankAccNovalcell);
		
		Phrase bankIFSCph = new Phrase("IFSC Code", font6);
		PdfPCell bankIFSCcell = new PdfPCell(bankIFSCph);
		bankIFSCcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankIFSCcell.setBorder(0);
		bankDetailsTab.addCell(bankIFSCcell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase bankIFSCvalph = new Phrase(bankIFSC, font6);
		PdfPCell bankIFSCvalcell = new PdfPCell(bankIFSCvalph);
		bankIFSCvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankIFSCvalcell.setBorder(0);
		bankDetailsTab.addCell(bankIFSCvalcell);
		
		
//		/*****Date 7-2-2019 
//		 * @author Amol
//		   added invoice ref no *****/
//		
//		
//		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "RefrenceOrderNumber", invoice.getCompanyId())){
//			invoiceRefNo=true;
//		}
//		
//		
//		if(invoiceRefNo){
//		Phrase invoiceRefNo = new Phrase("Invoice Ref. No ", font6);
//		PdfPCell invoicecell = new PdfPCell(invoiceRefNo);
//		invoicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invoicecell.setBorder(0);
//		bankDetailsTab.addCell(invoicecell);
//		
//		bankDetailsTab.addCell(coloncell);
//		
//		Phrase invoiceRefNovalue = new Phrase(invoice.getInvRefNumber(), font6);
//		PdfPCell invoicecell1 = new PdfPCell(invoiceRefNovalue);
//		invoicecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invoicecell1.setBorder(0);
//		bankDetailsTab.addCell(invoicecell1);
//		}else{
//			Phrase invoiceRefNo = new Phrase(" ");
//			PdfPCell invoicecell = new PdfPCell(invoiceRefNo);
//			invoicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			invoicecell.setBorder(0);
//			bankDetailsTab.addCell(invoicecell);
//			
//			Phrase invoiceRefNovalue1 = new Phrase("");
//			PdfPCell invoicecell2 = new PdfPCell(invoiceRefNovalue1);
//			invoicecell2.setHorizontalAlignment(Element.ALIGN_LEFT);
//			invoicecell2.setBorder(0);
//			bankDetailsTab.addCell(invoicecell2);
//			
//			Phrase invoiceRefNovalue = new Phrase("");
//			PdfPCell invoicecell1 = new PdfPCell(invoiceRefNovalue);
//			invoicecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//			invoicecell1.setBorder(0);
//			bankDetailsTab.addCell(invoicecell1);
//			
//		}
		
		
		Phrase invoiceNoph = new Phrase("Voucher No", font6);
		PdfPCell invoiceNocell = new PdfPCell(invoiceNoph);
		invoiceNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoiceNocell.setBorder(0);
		bankDetailsTab.addCell(invoiceNocell);
		
		bankDetailsTab.addCell(coloncell);
		
		Phrase invoiceNovalph = new Phrase(invoice.getCount()+"", font6);
		PdfPCell invoiceNovalcell = new PdfPCell(invoiceNovalph);
		invoiceNovalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoiceNovalcell.setBorder(0);
		bankDetailsTab.addCell(invoiceNovalcell);
		
		
		

//		Phrase emailph = new Phrase("Contact Details ", font6);
//		PdfPCell emailcell = new PdfPCell(emailph);
//		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		emailcell.setBorder(0);
//		bankDetailsTab.addCell(emailcell);
//		
//		bankDetailsTab.addCell(coloncell);
//		
//		Phrase emailValph = new Phrase(email, font6);
//		PdfPCell emailValcell = new PdfPCell(emailValph);
//		emailValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		emailValcell.setBorder(0);
//		bankDetailsTab.addCell(emailValcell);

		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			imageSignCell = new PdfPCell(image2);
//			imageSignCell.setBorderWidthLeft(0);
//			imageSignCell.setBorderWidthRight(0);
//			imageSignCell.setBorderWidthBottom(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(30);
			imageSignCell.setColspan(4);
			imageSignCell.setBorder(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);
		Phrase logoblank = new Phrase(" ");
		PdfPCell logoblankcell = new PdfPCell(logoblank);
		logoblankcell.setBorder(0);
		if (imageSignCell != null) {
			logoTab.addCell(logoblankcell);
			if(!isPortrait){
			logoTab.addCell(imageSignCell); 
			}
			logoTab.addCell(logoblankcell);
		} else {
			
			logoTab.addCell(logoblankcell);
		}

		PdfPTable companyAndBankDetailsTab = new PdfPTable(3);
		companyAndBankDetailsTab.setWidthPercentage(100);
		
		try {
			companyAndBankDetailsTab.setWidths(new float[]{35 ,30 ,35});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell firstTableCell = new PdfPCell(invoiceAddtab);
		firstTableCell.setBorderWidthRight(0);
		PdfPCell logoCell = new PdfPCell(logoTab);
		logoCell.setBorderWidthLeft(0);
		logoCell.setBorderWidthRight(0);
		PdfPCell secondTableCell = new PdfPCell(bankDetailsTab);
		secondTableCell.setBorderWidthLeft(0);
		companyAndBankDetailsTab.addCell(firstTableCell);
		companyAndBankDetailsTab.addCell(logoCell);
		companyAndBankDetailsTab.addCell(secondTableCell);
		
		try {
			document.add(companyAndBankDetailsTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void createBillingAndShippingInformationTab(){
		/**
		 * @author Anil ,Date :23-03-2019
		 * showing customer branch billing address
		 */
		String billingAddress="";
		String custGstin="";
		String custBillingState="";
		billingAddress=cust.getAdress().getCompleteAddress();
		custBillingState=cust.getAdress().getState();
		if(customerBranch!=null&&customerBranch.getGSTINNumber()!=null&&!customerBranch.getGSTINNumber().equals("")&&customerBranch.getBillingAddress()!=null){
			billingAddress=customerBranch.getBillingAddress().getCompleteAddress();
			custGstin=customerBranch.getGSTINNumber();
			custBillingState=customerBranch.getBillingAddress().getState();
		}
		
		/**
		 * @author Anil ,Date : 12-03-2019
		 * Printing Customer's correspondence name
		 */
		String correspondenceName="";
		if(cust!=null){
			if(cust.getCustPrintableName()!=null&&!cust.getCustPrintableName().equals("")){
				correspondenceName=cust.getCustPrintableName();
			}else{
				correspondenceName=invoice.getPersonInfo().getFullName();
			}
		}else{
			correspondenceName=invoice.getPersonInfo().getFullName();
		}
		
		PdfPTable billingdetailsTab = new PdfPTable(3);
		billingdetailsTab.setWidthPercentage(100);
		
		try {
			billingdetailsTab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase billingAddrph = new Phrase("Billing Address", font6bold);
		PdfPCell billingAddrcell = new PdfPCell(billingAddrph);
		billingAddrcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingAddrcell.setColspan(3);
		billingAddrcell.setBorder(0);
		billingdetailsTab.addCell(billingAddrcell);
		
		
		
		
		Phrase customeNameph = new Phrase(correspondenceName, font6bold);
//		Phrase customeNameph = new Phrase(invoice.getPersonInfo().getFullName(), font6bold);
		PdfPCell customeNamecell = new PdfPCell(customeNameph);
		customeNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customeNamecell.setColspan(3);
		customeNamecell.setBorder(0);
		billingdetailsTab.addCell(customeNamecell);
				
		Phrase billingAddressph = new Phrase(billingAddress, font6);
		PdfPCell billingAddresscell = new PdfPCell(billingAddressph);
		billingAddresscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingAddresscell.setColspan(3);
		billingAddresscell.setBorder(0);
		billingdetailsTab.addCell(billingAddresscell);
		
		Phrase statename = new Phrase("State Name", font6);
		PdfPCell statenamecell = new PdfPCell(statename);
		statenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamecell.setBorder(0);
		billingdetailsTab.addCell(statenamecell);
		
		billingdetailsTab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statecodetab=new PdfPTable(3);
		statecodetab.setWidthPercentage(100);
		try {
			statecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevalname = new Phrase(custBillingState, font6);
		PdfPCell statenamevalcell = new PdfPCell(statevalname);
		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevalcell.setBorder(0);
		statecodetab.addCell(statenamevalcell);
		
		Phrase statecode = new Phrase("Code :", font6);
		PdfPCell statecodecell = new PdfPCell(statecode);
		statecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodecell.setBorder(0);
		statecodetab.addCell(statecodecell);
		
		String stateCodeStr ="";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(custBillingState.trim())) {
				stateCodeStr = stateList.get(i).getStateCode().trim();
				break;
			}
		}
		Phrase stateCode = new Phrase(stateCodeStr, font6);

		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		stateCodeCell.setBorder(0);
		statecodetab.addCell(stateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell statecodetabcell = new PdfPCell(statecodetab);
		statecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodetabcell.setBorder(0);
		billingdetailsTab.addCell(statecodetabcell);

		
		Phrase gstinph = new Phrase("GSTIN/UIN", font6);
		PdfPCell gstincell = new PdfPCell(gstinph);
		gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstincell.setBorder(0);
		billingdetailsTab.addCell(gstincell);
		
		billingdetailsTab.addCell(coloncell);

		String gstinvalue= "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("GSTIN")) {
				gstinvalue=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
				break;
			}
		}
		
		if(!custGstin.equals("")){
			gstinvalue=custGstin;
		}
		
		if(invoice.getGstinNumber()!= null && !invoice.getGstinNumber().equals("")){
			gstinvalue = invoice.getGstinNumber();
		}
		Phrase gstinvalph = null;
		if (!gstinvalue.trim().equalsIgnoreCase("")) {
			gstinvalph = new Phrase(gstinvalue, font6);
		} else {
			gstinvalph = new Phrase("", font6);

		}
		
		PdfPCell gstinvalcell = new PdfPCell(gstinvalph);
		gstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell.setBorder(0);
		billingdetailsTab.addCell(gstinvalcell);
		
		Phrase cinph = new Phrase("CIN", font6);
		PdfPCell cincell = new PdfPCell(cinph);
		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cincell.setBorder(0);
		billingdetailsTab.addCell(cincell);
		
		billingdetailsTab.addCell(coloncell);
		
		String cinval="";
		String panNo = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {

			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("CIN")) {

				cinval=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("PAN")) {

				panNo=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
			}

			}
			
		Phrase cinvalph = new Phrase(cinval, font6);
		PdfPCell cinvalphCell = new PdfPCell(cinvalph);
		cinvalphCell.setBorder(0);
		billingdetailsTab.addCell(cinvalphCell);
			
		
		Phrase panph = new Phrase("PAN NO.", font6);
		PdfPCell pancell = new PdfPCell(panph);
		pancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pancell.setBorder(0);
		billingdetailsTab.addCell(pancell);
		
		billingdetailsTab.addCell(coloncell);
		
		Phrase panvalph = new Phrase(panNo, font6);
		PdfPCell panvalcell = new PdfPCell(panvalph);
		panvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		panvalcell.setBorder(0);
		billingdetailsTab.addCell(panvalcell);
		
		Phrase poph = new Phrase("PO/WO No: ", font6);
		PdfPCell pocell = new PdfPCell(poph);
		pocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pocell.setBorder(0);
		billingdetailsTab.addCell(pocell);
		
		billingdetailsTab.addCell(coloncell);
		String poNumber = "" , woNumber = "" , number = "NA";
		if(invoice.getPoNumber() != null && !invoice.getPoNumber().equals("")){
			poNumber = invoice.getPoNumber();
			number = poNumber;
		}
		if(invoice.getWoNumber() != null && !invoice.getWoNumber().equals("")){
			woNumber = invoice.getWoNumber();
			number = woNumber;
		}
		if(!poNumber.equals("") && !woNumber.equals("")){
			number = poNumber+"/"+woNumber;
		}
		Phrase povalph = new Phrase(number, font6);
		PdfPCell povalcell = new PdfPCell(povalph);
		povalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		povalcell.setBorder(0);
		billingdetailsTab.addCell(povalcell);
		
		Phrase contractph = new Phrase("Contract No", font6);
		PdfPCell contractcell = new PdfPCell(contractph);
		contractcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractcell.setBorder(0);
		//contractcell.setColspan(2);
		billingdetailsTab.addCell(contractcell);
		
		billingdetailsTab.addCell(coloncell);
		String contractNumber = "NA";
		if(invoice.getContractNumber() != null && !invoice.getContractNumber().equals("")){
			contractNumber = invoice.getContractNumber();
		}
		Phrase contractvalph = new Phrase(contractNumber, font6);
		PdfPCell contractvalcell = new PdfPCell(contractvalph);
		contractvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractvalcell.setBorder(0);
		billingdetailsTab.addCell(contractvalcell);
		/** end billing address info **/
		
		/** shipping address info **/
		PdfPTable shippingdetailsTab = new PdfPTable(3);
		shippingdetailsTab.setWidthPercentage(100);
		
		try {
			shippingdetailsTab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase shippingAddrph = new Phrase("Shipping Address", font6bold);
		PdfPCell shippingAddrcell = new PdfPCell(shippingAddrph);
		shippingAddrcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		shippingAddrcell.setColspan(3);
		shippingAddrcell.setBorder(0);
		shippingdetailsTab.addCell(shippingAddrcell);
		
		String Name = correspondenceName;
		/** Date 11-08-2020 by Vijay if Project Name having in the invoice it will display in thr curly braces as per Rahul requirement **/
		if(invoice.getProjectName()!=null && !invoice.getProjectName().equals("")){
			Name += " ( "+invoice.getProjectName()+" )"; 
		}
		
		Phrase customeNameph1 = new Phrase(Name, font6bold);
//		Phrase customeNameph1 = new Phrase(invoice.getPersonInfo().getFullName(), font6bold);
		PdfPCell customeNamecell1 = new PdfPCell(customeNameph1);
		customeNamecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		customeNamecell1.setColspan(3);
		customeNamecell1.setBorder(0);
		shippingdetailsTab.addCell(customeNamecell1);
				
		/**
		 * Date : 22-12-2018 BY ANIL
		 * if customer branch is selected in invoice then print customer branch as service address
		 */
		String shippingAdd="";
		String shippingState="";
		if(customerBranch!=null){
			shippingAdd=customerBranch.getAddress().getCompleteAddress();
			shippingState=customerBranch.getAddress().getState();
		}else{
			shippingAdd=cust.getSecondaryAdress().getCompleteAddress();
			shippingState=cust.getSecondaryAdress().getState();
		}
		/**
		 * ENd
		 */
		Phrase shippingAddressph = new Phrase(shippingAdd, font6);
		PdfPCell shippingAddresscell = new PdfPCell(shippingAddressph);
		shippingAddresscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		shippingAddresscell.setColspan(3);
		shippingAddresscell.setBorder(0);
		shippingdetailsTab.addCell(shippingAddresscell);
		
		shippingdetailsTab.addCell(statenamecell);	
		shippingdetailsTab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statecodetab1=new PdfPTable(3);
		statecodetab1.setWidthPercentage(100);
		try {
			statecodetab1.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevalname1 = new Phrase(shippingState, font6);
		PdfPCell statenamevalcell1 = new PdfPCell(statevalname1);
		statenamevalcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevalcell1.setBorder(0);
		statecodetab1.addCell(statenamevalcell1);

		statecodetab1.addCell(statecodecell);
		
		String stateCodeStr1 ="";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(shippingState)) {
				stateCodeStr1 = stateList.get(i).getStateCode().trim();
				break;
			}
		}
		Phrase stateCode1 = new Phrase(stateCodeStr1, font6);

		PdfPCell stateCodeCell1 = new PdfPCell(stateCode1);
		stateCodeCell1.setBorder(0);
		statecodetab1.addCell(stateCodeCell1);
		
		/*state code tab complete*/
		
		PdfPCell statecodetabcell1 = new PdfPCell(statecodetab1);
		statecodetabcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodetabcell1.setBorder(0);
		shippingdetailsTab.addCell(statecodetabcell1);

		shippingdetailsTab.addCell(gstincell);
		
		shippingdetailsTab.addCell(coloncell);

		shippingdetailsTab.addCell(gstinvalcell);
		
		shippingdetailsTab.addCell(cincell);
		
		shippingdetailsTab.addCell(coloncell);
		
		shippingdetailsTab.addCell(cinvalphCell);
			
		shippingdetailsTab.addCell(pancell);
		
		shippingdetailsTab.addCell(coloncell);
		
		shippingdetailsTab.addCell(panvalcell);
		
		shippingdetailsTab.addCell(pocell);
		
		shippingdetailsTab.addCell(coloncell);
		
		shippingdetailsTab.addCell(povalcell);
		
		shippingdetailsTab.addCell(contractcell);
		
		shippingdetailsTab.addCell(coloncell);
		
		shippingdetailsTab.addCell(contractvalcell);
		
		/**
		 * @author Anil , Date : 31-07-2019
		 * showing ewaybill number
		 */
		if(invoice!=null&&invoice.getEwayBillNumber()!=null&&!invoice.getEwayBillNumber().equals("")){
			Phrase ewayPh = new Phrase("Eway Bill Number", font6);
			PdfPCell ewayCell = new PdfPCell(ewayPh);
			ewayCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			ewayCell.setBorder(0);
			shippingdetailsTab.addCell(ewayCell);
			shippingdetailsTab.addCell(coloncell);
			Phrase ewayValPh = new Phrase(invoice.getEwayBillNumber(), font6);
			PdfPCell ewayValCell = new PdfPCell(ewayValPh);
			ewayValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			ewayValCell.setBorder(0);
			shippingdetailsTab.addCell(ewayValCell);
		}
		
		/** end shipping address info **/
		PdfPTable addressDetailsTab = new PdfPTable(3);
		addressDetailsTab.setWidthPercentage(100);
		try {
			addressDetailsTab.setWidths(new float[] { 35, 30,35});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell firstTableCell = new PdfPCell(billingdetailsTab);
		firstTableCell.setBorderWidthRight(0);
		PdfPCell secondTableCell = new PdfPCell(shippingdetailsTab);
		secondTableCell.setBorderWidthLeft(0);
		PdfPCell blankCell = getCell(" ", font6, Element.ALIGN_CENTER);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		addressDetailsTab.addCell(firstTableCell);
		addressDetailsTab.addCell(blankCell);
		addressDetailsTab.addCell(secondTableCell);

		try {
			document.add(addressDetailsTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	private void createSubTitle(String value){
		PdfPTable titleTab = new PdfPTable(1);
		titleTab.setWidthPercentage(100);
		
		Phrase titleph = new Phrase(value, font6bold);
		PdfPCell titleCell = new PdfPCell(titleph);
		//titleCell.setPaddingTop(5);
		//titleCell.setPaddingBottom(5);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(titleCell);
		
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	private void createTermsAndConditionTab(boolean isPortrait){
		PdfPTable termsTable = new PdfPTable(1);
		termsTable.setWidthPercentage(100);
		
		Phrase termsAnsConditionph = new Phrase("Terms & Conditions :" , font6bold);
		PdfPCell termsAnsConditioncell = new PdfPCell(termsAnsConditionph);
		termsAnsConditioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		termsAnsConditioncell.setBorder(0);
		termsTable.addCell(termsAnsConditioncell);
		
		Phrase conditionph1 = new Phrase("# Any discrepancy in this bill must be notified in writing within 2 days of receipt, otherwise it will be taken as accepted." , font6);
		PdfPCell conditioncell1 = new PdfPCell(conditionph1);
		conditioncell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		conditioncell1.setBorder(0);
		termsTable.addCell(conditioncell1);
		
		ArrayList<PaymentTerms> arrPaymentTerms = invoice.getArrPayTerms();
		int days = 0;
		if(arrPaymentTerms != null && arrPaymentTerms.size() > 0){
			if(arrPaymentTerms.get(0).getPayTermDays() != null){
				days = arrPaymentTerms.get(0).getPayTermDays();
			}
		}
		Phrase conditionph2 = new Phrase("# Please note that, this bill is payable within "+days+" days or as agreed by us in writing by ECS/RTGS/NEFT Only." , font6);
		PdfPCell conditioncell2 = new PdfPCell(conditionph2);
		conditioncell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		conditioncell2.setBorder(0);
		termsTable.addCell(conditioncell2);
		
		Phrase conditionph3 = new Phrase("# Delay of payment of this invoice shall attract an interest on outstanding amount @18% compounding interest p.a." , font6);
		PdfPCell conditioncell3 = new PdfPCell(conditionph3);
		conditioncell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		conditioncell3.setBorder(0);
		termsTable.addCell(conditioncell3);
		if(invoice.isTdsApplicable()){
			String per = 0+"";
			if(invoice.getTdsPercentage() != null){
				per = invoice.getTdsPercentage();
			}
			Phrase conditionph4 = new Phrase("# Kindly note that, TDS will be deductible @"+per+"% on Net Taxable Amount, under section 19:C as per contract norms.", font6);
			PdfPCell conditioncell4 = new PdfPCell(conditionph4);
			conditioncell4.setHorizontalAlignment(Element.ALIGN_LEFT);
			conditioncell4.setBorder(0);
			termsTable.addCell(conditioncell4);
		}else{
			Phrase conditionph4 = new Phrase("# Kindly note that, TDS will not be deductible on this invoice.", font6);
			PdfPCell conditioncell4 = new PdfPCell(conditionph4);
			conditioncell4.setHorizontalAlignment(Element.ALIGN_LEFT);
			conditioncell4.setBorder(0);
			termsTable.addCell(conditioncell4);
		}
		Phrase conditionph5 = new Phrase("# We hereby declare that, no child labour was employed in any of the services." , font6);
		PdfPCell conditioncell5 = new PdfPCell(conditionph5);
		conditioncell5.setHorizontalAlignment(Element.ALIGN_LEFT);
		conditioncell5.setBorder(0);
		termsTable.addCell(conditioncell5);
		
		PdfPTable rightFootertab = new PdfPTable(1);
		rightFootertab.setWidthPercentage(100);
		
		
		/***
		 * @author Vijay Chougule Date :- 27-07-2020
		 * Des :- updated code with Digital Signature
		 */
		if(isPortrait){
			Phrase companyNameph = new Phrase("      For "+companyName, font6);
			PdfPCell companyNamecell = new PdfPCell(companyNameph);
			companyNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			companyNamecell.setBorder(0);
			rightFootertab.addCell(companyNamecell);
		}
		else{
			Phrase companyNameph = new Phrase("                     For "+companyName, font6);
			PdfPCell companyNamecell = new PdfPCell(companyNameph);
			companyNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			companyNamecell.setBorder(0);
			rightFootertab.addCell(companyNamecell);
		}
		
		
		
		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (imageSignCell != null) {
			rightFootertab.addCell(imageSignCell);
		} else {

			Phrase blankph = new Phrase(" ", font6);
			PdfPCell blankcell = new PdfPCell(blankph);
			blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			blankcell.setBorder(0);
			rightFootertab.addCell(blankcell);
			rightFootertab.addCell(blankcell);
			rightFootertab.addCell(blankcell);
			rightFootertab.addCell(blankcell);

		}
		
		/**
		 * ends here
		 */
//		Phrase blankph = new Phrase(" ", font6);
//		PdfPCell blankcell = new PdfPCell(blankph);
//		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		blankcell.setBorder(0);
//		rightFootertab.addCell(blankcell);
//		rightFootertab.addCell(blankcell);
//		rightFootertab.addCell(blankcell);
//		rightFootertab.addCell(blankcell);
		
		if(isPortrait){
			Phrase authorisedph = new Phrase("                 Authorised Signatory", font6);
			PdfPCell authorisedcell = new PdfPCell(authorisedph);
			authorisedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			authorisedcell.setBorder(0);
			rightFootertab.addCell(authorisedcell);
		}
		else{
			Phrase authorisedph = new Phrase("                                Authorised Signatory", font6);
			PdfPCell authorisedcell = new PdfPCell(authorisedph);
			authorisedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			authorisedcell.setBorder(0);
			rightFootertab.addCell(authorisedcell);
		}

		
		
		PdfPTable mainFooterTable = new PdfPTable(2);
		mainFooterTable.setWidthPercentage(100);
		try {
			mainFooterTable.setWidths(new float[]{75 ,25});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell firstCell = new PdfPCell(termsTable);
		firstCell.setBorderWidthRight(0);
		PdfPCell secondCell = new PdfPCell(rightFootertab);
		secondCell.setBorderWidthLeft(0);
		mainFooterTable.addCell(firstCell);
		mainFooterTable.addCell(secondCell);
		
		try {
			document.add(mainFooterTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	private void taxDetailsTab(){
		PdfPTable firstPartTable = getTaxInformationTable();
		PdfPTable secondPartTable = geTaxAnalysisTable();
		PdfPTable thirdParTable = getInvoiceSummaryTable();
//		int table1Size = 0 , table2Size = 0;
//		table1Size = 3 + map.size();	
//		table2Size = 7;
//		if(table2Size > table1Size){
//			PdfPCell cell = getCell(" ", font6, Element.ALIGN_CENTER);
//			cell.setBorder(0);
//			cell.setRowspan(table2Size - table1Size);
//			secondPartTable.addCell(cell);
//		}else{
//			PdfPCell cell = getCell(" ", font6, Element.ALIGN_CENTER);
//			cell.setBorder(0);
//			cell.setRowspan(table1Size - table2Size);
//			thirdParTable.addCell(cell);
//		}
		PdfPTable mainTaxTable = new PdfPTable(4);
		mainTaxTable.setWidthPercentage(100);
		try {
			mainTaxTable.setWidths(new float[] {25,48 ,2, 25 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase emptyph = new Phrase(" " , font6);
		PdfPCell emptycell = new PdfPCell(emptyph);
		emptycell.setBorderWidthBottom(0);
		emptycell.setColspan(4);
		mainTaxTable.addCell(emptycell);
		
		PdfPCell firstCell = new PdfPCell(firstPartTable);
		firstCell.setBorderWidthBottom(0);
		firstCell.setBorderWidthRight(0);
		firstCell.setBorderWidthTop(0);
		PdfPCell secondCell = new PdfPCell(secondPartTable);
	//	secondCell.setBorderWidthLeft(0);
	//	secondCell.setBorderWidthRight(0);
		secondCell.setBorder(0);
		PdfPCell cell1 = new PdfPCell(getCell(" ", font6, Element.ALIGN_CENTER));
		cell1.setBorder(0);
		PdfPCell thirdCell = new PdfPCell(thirdParTable);
		thirdCell.setBorder(0);
	//	thirdCell.setBorderWidthLeft(0);
		
		mainTaxTable.addCell(firstCell);
		mainTaxTable.addCell(secondCell);
		mainTaxTable.addCell(cell1);
		mainTaxTable.addCell(thirdCell);
		
		PdfPCell emptycell1 = new PdfPCell(emptyph);
		emptycell1.setBorderWidthTop(0);
		emptycell1.setColspan(4);
		mainTaxTable.addCell(emptycell1);
		try {
			document.add(mainTaxTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private PdfPTable getTaxInformationTable(){
		PdfPTable taxInfoTable = new PdfPTable(3);
		taxInfoTable.setWidthPercentage(100);
		try {
			taxInfoTable.setWidths(new float[] {40,10,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase reverseChargeph = new Phrase("Reverse Charge" , font6);
		PdfPCell reverseChargecell = new PdfPCell(reverseChargeph);
		reverseChargecell.setBorder(0);
		reverseChargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(reverseChargecell);
		
		taxInfoTable.addCell(coloncell);
		
		Phrase reverseChargevalph = new Phrase("NO" , font6);
		PdfPCell reverseChargevalcell = new PdfPCell(reverseChargevalph);
		reverseChargevalcell.setBorder(0);
		reverseChargevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(reverseChargevalcell);
		
		String pfValue="";
		String esicVal = "";
		String msmeVal = "";
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("PF")) {

				pfValue=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("ESIC")) {

				esicVal=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName().contains("MSME")) {

				msmeVal=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			}

		}

		if(branchDt != null){
			if(branchDt.getEsicCode() !=null && !branchDt.getEsicCode().equals("")){
				esicVal = branchDt.getEsicCode();
			}
		}
			
		Phrase pfph = new Phrase("PF NO." , font6);
		PdfPCell pfcell = new PdfPCell(pfph);
		pfcell.setBorder(0);
		pfcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(pfcell);
		
		taxInfoTable.addCell(coloncell);
		
		Phrase pfValph = new Phrase(pfValue , font6);
		PdfPCell pfValcell = new PdfPCell(pfValph);
		pfValcell.setBorder(0);
		pfValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(pfValcell);
		
		Phrase esicph = new Phrase("ESIC NO." , font6);
		PdfPCell esiccell = new PdfPCell(esicph);
		esiccell.setBorder(0);
		esiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(esiccell);
		
		taxInfoTable.addCell(coloncell);
		
		Phrase esicValph = new Phrase(esicVal , font6);
		PdfPCell esicValcell = new PdfPCell(esicValph);
		esicValcell.setBorder(0);
		esicValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(esicValcell);
		
		
		
		Phrase msmeph = new Phrase("MSME NO." , font6);
		PdfPCell msmecell = new PdfPCell(msmeph);
		msmecell.setBorder(0);
		msmecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(msmecell);
		
		taxInfoTable.addCell(coloncell);
		
		Phrase msmeValph = new Phrase(msmeVal , font6);
		PdfPCell msmeValcell = new PdfPCell(msmeValph);
		msmeValcell.setBorder(0);
		msmeValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(msmeValcell);
		
		Phrase categoryph = new Phrase("Category" , font6);
		PdfPCell categorycell = new PdfPCell(categoryph);
		categorycell.setBorder(0);
		categorycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(categorycell);
		
		taxInfoTable.addCell(coloncell);
		
		Phrase categoryValph = new Phrase("Housekeeping Services" , font6);
		PdfPCell categoryValcell = new PdfPCell(categoryValph);
		categoryValcell.setBorder(0);
		categoryValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxInfoTable.addCell(categoryValcell);

		
		return taxInfoTable;
	}
	private PdfPTable getInvoiceSummaryTable(){
		PdfPTable summaryTable = new PdfPTable(2);
		summaryTable.setWidthPercentage(100);
		try {
			summaryTable.setWidths(new float[] {65 , 35});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase headerph = new Phrase("Invoice Summary" , font6bold);
		PdfPCell headercell = new PdfPCell(headerph);
	//	headercell.setBorder(0);
		headercell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headercell.setColspan(2);
		summaryTable.addCell(headercell);
		
		Phrase taxAmountph = new Phrase("Net Taxable Amount" , font6bold);
		PdfPCell taxAmountcell = new PdfPCell(taxAmountph);
	//	headercell.setBorder(0);
		taxAmountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		summaryTable.addCell(taxAmountcell);
		
		Phrase taxAmountvalph = new Phrase(getRoundOffValue(invoice.getTotalAmtExcludingTax()) , font6bold);
		PdfPCell taxAmountvalcell = new PdfPCell(taxAmountvalph);
	//	headercell.setBorder(0);
		taxAmountvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryTable.addCell(taxAmountvalcell);
		
		double cgst =0 ,sgst = 0,igst = 0;
		for(ContractCharges charges : invoice.getBillingTaxes()){
			double totalCalcAmt=0;
			totalCalcAmt=charges.getTaxChargeAssesVal()*charges.getTaxChargePercent()/100;
			totalCalcAmt=(totalCalcAmt*100.0)/100.0;
			charges.setPayableAmt(totalCalcAmt);
			if(charges.getTaxChargeName().equalsIgnoreCase("CGST")){
				cgst += (Math.round(charges.getPayableAmt()*100.0)/100.0);
			}else if(charges.getTaxChargeName().equalsIgnoreCase("SGST")){
				sgst += (Math.round(charges.getPayableAmt()*100.0)/100.0);
			}else if(charges.getTaxChargeName().equalsIgnoreCase("IGST")){
				igst += (Math.round(charges.getPayableAmt()*100.0)/100.0);
			}
			totalTax += (Math.round(charges.getPayableAmt()*100.0)/100.0);
		}
		
		Phrase cgstph = new Phrase("Add : CGST" , font6);
		PdfPCell cgstcell = new PdfPCell(cgstph);
	//	headercell.setBorder(0);
		cgstcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		summaryTable.addCell(cgstcell);
		
		Phrase cgstvalph = new Phrase(getRoundOffValue(cgst) , font6);
		PdfPCell cgstvalcell = new PdfPCell(cgstvalph);
	//	headercell.setBorder(0);
		cgstvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryTable.addCell(cgstvalcell);
		
		Phrase sgstph = new Phrase("Add : SGST" , font6);
		PdfPCell sgstcell = new PdfPCell(sgstph);
	//	headercell.setBorder(0);
		sgstcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		summaryTable.addCell(sgstcell);
		
		Phrase sgstvalph = new Phrase(getRoundOffValue(sgst) , font6);
		PdfPCell sgstvalcell = new PdfPCell(sgstvalph);
	//	headercell.setBorder(0);
		sgstvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryTable.addCell(sgstvalcell);
		
		Phrase igstph = new Phrase("Add : IGST" , font6);
		PdfPCell igstcell = new PdfPCell(igstph);
	//	headercell.setBorder(0);
		igstcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		summaryTable.addCell(igstcell);
		
		Phrase igstvalph = new Phrase(getRoundOffValue(igst), font6);
		PdfPCell igstvalcell = new PdfPCell(igstvalph);
	//	headercell.setBorder(0);
		igstvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryTable.addCell(igstvalcell);
		
		Phrase totalGSTph = new Phrase("Total GST" , font6bold);
		PdfPCell totalGSTcell = new PdfPCell(totalGSTph);
	//	headercell.setBorder(0);
		totalGSTcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		summaryTable.addCell(totalGSTcell);
		
		Phrase totalGSTvalph = new Phrase(getRoundOffValue(totalTax) , font6bold);
		PdfPCell totalGSTvalcell = new PdfPCell(totalGSTvalph);
	//	headercell.setBorder(0);
		totalGSTvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryTable.addCell(totalGSTvalcell);
		
		Phrase totalInvoieAmtph = new Phrase("Total Payable invoice Amount" , font6bold);
		PdfPCell totalInvoieAmtcell = new PdfPCell(totalInvoieAmtph);
	//	headercell.setBorder(0);
		totalInvoieAmtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		summaryTable.addCell(totalInvoieAmtcell);
		
		Phrase totalInvoieAmtvalph = new Phrase(getRoundOffValue(invoice.getNetPayable()) , font6bold);
		PdfPCell totalInvoieAmtvalCell = new PdfPCell(totalInvoieAmtvalph);
	//	headercell.setBorder(0);
		totalInvoieAmtvalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		summaryTable.addCell(totalInvoieAmtvalCell);	
		
		int table1Size = 0 , table2Size = 0;
		table1Size = 3 + map.size();	
		table2Size = 7;
		if(table1Size > table2Size){
			PdfPCell cell = getCell(" ", font6, Element.ALIGN_CENTER);
			cell.setBorder(0);
			cell.setRowspan(table1Size - table2Size);
			cell.setColspan(2);
			summaryTable.addCell(cell);
		}
//		else{
//			PdfPCell cell = getCell(" ", font6, Element.ALIGN_CENTER);
//			cell.setBorder(0);
//			cell.setRowspan(table1Size - table2Size);
//			thirdParTable.addCell(cell);
//		}
		return summaryTable;
	}
	
	private void getAmountInwords(String value , double amount){
		
		/**
		 * @author Anil,Date : 18-02-2019
		 * Total tax amount in words issue
		 * for sasha raised by sonu and megha
		 */
		
		
		String strAmount = df.format(amount);
		String arr[] = strAmount.split("\\.");
		String decimalAmountInString = "";
		int decimalAmount = 0;
		String decString = "";
		if(arr.length == 2){
			try{
				decimalAmount = Integer.parseInt(arr[1]);
				amount=Integer.parseInt(arr[0]);
			}catch(Exception e){
				decimalAmount = 0;
			}
			if(decimalAmount != 0){
				decimalAmountInString = "and Paise " +SalesInvoicePdf.convert(decimalAmount);
			}
		}
		
		
		String amtInWordsVal = "Rupees "+ SalesInvoicePdf.convert(amount) +" "+decimalAmountInString+ " Only";
		
		PdfPTable titleTab = new PdfPTable(2);
		titleTab.setWidthPercentage(100);
		try {
			titleTab.setWidths(new float[]{25,75});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Phrase titleph = new Phrase(value, font6);
		PdfPCell titleCell = new PdfPCell(titleph);
		titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titleTab.addCell(titleCell);
		
		Phrase titleValph = new Phrase(amtInWordsVal, font6bold);
		PdfPCell titleValCell = new PdfPCell(titleValph);
		titleValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titleTab.addCell(titleValCell);
		
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	private PdfPCell getCell(String value , Font font , int alignment){
		Phrase phrase = new Phrase(value , font);
		PdfPCell cell = new PdfPCell(phrase);
		cell.setHorizontalAlignment(alignment);
		return cell;		
	}
	private PdfPTable geTaxAnalysisTable(){
		PdfPTable taxAnalysisTable = new PdfPTable(9);
		taxAnalysisTable.setWidthPercentage(100);
		try {
			taxAnalysisTable.setWidths(new float[]{ 11,15,10,11,10,11,10,11,11});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPCell headerCell = getCell("Tax Analysis", font6bold, Element.ALIGN_CENTER);
		headerCell.setColspan(9);
		taxAnalysisTable.addCell(headerCell);
		taxAnalysisTable.addCell(getCell("HSN/SAC", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("Taxable Amount", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("CGST Rate", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("CGST Amt", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("SGST Rate", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("SGST Amt", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("IGST Rate", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("IGST Amt", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell("Total Amount", font6bold, Element.ALIGN_CENTER));
		double totCgst = 0 ,totSgst = 0 , totIgst = 0;
		double totalAmt = 0 , totalTaxable = 0;
		TaxSummaryBean bean;
		if(map.size()>0){
		for (Map.Entry<String,TaxSummaryBean> entry : map.entrySet())  {
          logger.log(Level.SEVERE ,"Key = " + entry.getKey() + ", Value = " + entry.getValue());
          bean = entry.getValue();
          logger.log(Level.SEVERE ,"Key = " + bean.getTaxAmount());
          double cgstAmt=0;
          double sgstAmt=0;
          double igstAmt=0;
          totalTaxable += bean.getTaxAmount();
          if(bean.getCgstPer()!=0){
          	cgstAmt = ((bean.getTaxAmount())*bean.getCgstPer())/100;
          	cgstAmt = (Math.round(cgstAmt *100.0))/100.0;
          	totCgst += cgstAmt;
          }
         if(bean.getSgstPer()!=0){
          	sgstAmt = ((bean.getTaxAmount())*bean.getSgstPer())/100;
          	sgstAmt = (Math.round(sgstAmt *100.0))/100.0;
          	totSgst += sgstAmt;
         } 
         if(bean.getIgstPer()!=0){
          	igstAmt = ((bean.getTaxAmount())*bean.getIgstPer())/100;
          	igstAmt = (Math.round(igstAmt *100.0))/100.0;
          	totIgst += igstAmt;
         }
         totalAmt += Math.round((cgstAmt + sgstAmt + igstAmt)*100.0)/100.0;
       //values
 		taxAnalysisTable.addCell(getCell(entry.getKey(), font6, Element.ALIGN_CENTER));
 		taxAnalysisTable.addCell(getCell(getRoundOffValue((Math.round(bean.getTaxAmount()*100.0)/100.0))+"", font6, Element.ALIGN_RIGHT));
 		taxAnalysisTable.addCell(getCell(taxAmount(bean.getCgstPer()), font6, Element.ALIGN_CENTER));
 		taxAnalysisTable.addCell(getCell(getRoundOffValue(cgstAmt)+"", font6, Element.ALIGN_RIGHT));
 		taxAnalysisTable.addCell(getCell(taxAmount(bean.getSgstPer()), font6, Element.ALIGN_CENTER));
 		taxAnalysisTable.addCell(getCell(getRoundOffValue(sgstAmt)+"", font6, Element.ALIGN_RIGHT));
 		taxAnalysisTable.addCell(getCell(taxAmount(bean.getIgstPer()), font6, Element.ALIGN_CENTER));
 		taxAnalysisTable.addCell(getCell(getRoundOffValue(igstAmt)+"", font6, Element.ALIGN_RIGHT));
 		taxAnalysisTable.addCell(getCell(getRoundOffValue((Math.round((cgstAmt + sgstAmt + igstAmt)*100.0)/100.0))+"", font6, Element.ALIGN_RIGHT));
	}
		}

		
		
		//total tab
		taxAnalysisTable.addCell(getCell("Total", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell(getRoundOffValue(totalTaxable), font6bold, Element.ALIGN_RIGHT));
		taxAnalysisTable.addCell(getCell(" ", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell(getRoundOffValue(totCgst), font6bold, Element.ALIGN_RIGHT));
		taxAnalysisTable.addCell(getCell(" ", font6bold, Element.ALIGN_CENTER));
		taxAnalysisTable.addCell(getCell(getRoundOffValue(totSgst), font6bold, Element.ALIGN_RIGHT));
		taxAnalysisTable.addCell(getCell(" ", font6, Element.ALIGN_CENTER));
 		taxAnalysisTable.addCell(getCell(getRoundOffValue(totIgst), font6, Element.ALIGN_RIGHT));
		taxAnalysisTable.addCell(getCell(getRoundOffValue(totalAmt), font6bold, Element.ALIGN_RIGHT));
		int table1Size = 0 , table2Size = 0;
		table1Size = 3 + map.size();	
		table2Size = 7;
		if(table2Size > table1Size){
			PdfPCell cell = getCell(" ", font6, Element.ALIGN_CENTER);
			cell.setBorder(0);
			cell.setRowspan(table2Size - table1Size);
			cell.setColspan(9);
			taxAnalysisTable.addCell(cell);
		}
//		else{
//			PdfPCell cell = getCell(" ", font6, Element.ALIGN_CENTER);
//			cell.setBorder(0);
//			cell.setRowspan(table1Size - table2Size);
//			thirdParTable.addCell(cell);
//		}
		
		return taxAnalysisTable;
	}
	private void getOtherServiceTable(int count){
		PdfPTable otherServiceTable = getOtherServiceTableHeader();
		for(int i =count ; i < invoice.getSalesOrderProducts().size() ; i++){
			if(invoice.getSalesOrderProducts().get(i).getBasePaymentAmount() == 0){
				continue;
			}
			
			if(count == 0){
			if (noOfLines == 0) {
				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
				prouductCount = i;
				break;
			}
			
				noOfLines = noOfLines - 1;
			}
			j++;
			HashMap<String ,String> valueMap = getProductValueMap(invoice.getSalesOrderProducts().get(i),invoice.getSalesOrderProducts().get(i).getPrice());
			
			otherServiceTable.addCell(getCell(j+"", font6, Element.ALIGN_CENTER));
			otherServiceTable.addCell(getCell(valueMap.get("Description"), font6, Element.ALIGN_LEFT));
			otherServiceTable.addCell(getCell(valueMap.get("HSN/SAC"), font6, Element.ALIGN_CENTER));
			otherServiceTable.addCell(getCell(valueMap.get("Amount"), font6, Element.ALIGN_RIGHT));
			otherServiceTable.addCell(getCell(valueMap.get("Net Taxable Amt"), font6, Element.ALIGN_RIGHT));
			otherServiceTable.addCell(getCell(valueMap.get("CGST Rate"), font6, Element.ALIGN_CENTER));
			otherServiceTable.addCell(getCell(valueMap.get("CGST Amt"), font6, Element.ALIGN_RIGHT));
			otherServiceTable.addCell(getCell(valueMap.get("SGST Rate"), font6, Element.ALIGN_CENTER));
			otherServiceTable.addCell(getCell(valueMap.get("SGST Amt"), font6, Element.ALIGN_RIGHT));
			otherServiceTable.addCell(getCell(valueMap.get("IGST Rate"), font6, Element.ALIGN_CENTER));
			otherServiceTable.addCell(getCell(valueMap.get("IGST Amt"), font6, Element.ALIGN_RIGHT));
			otherServiceTable.addCell(getCell(valueMap.get("Payable Invoice Amt"), font6, Element.ALIGN_RIGHT));		
		}
		
		if(totalNoOfLineItems<20||prouductCount==totalNoOfLineItems-1){
		//total tab
		PdfPCell totalAmountCell = getCell("Total Amount", font6bold, Element.ALIGN_CENTER);
		totalAmountCell.setColspan(4);
		
		otherServiceTable.addCell(totalAmountCell);
		otherServiceTable.addCell(getCell(getRoundOffValue(totalAmount), font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(getRoundOffValue(totalCGST), font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(getRoundOffValue(totalSGST), font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(getRoundOffValue(totalIGST), font6bold, Element.ALIGN_RIGHT));
		otherServiceTable.addCell(getCell(getRoundOffValue(invoice.getNetPayable()), font6bold, Element.ALIGN_RIGHT));
		
		}
		try {
			document.add(otherServiceTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private PdfPTable getOtherServiceTableHeader(){
		PdfPTable otherServiceTable = new PdfPTable(12);
		otherServiceTable.setWidthPercentage(100); 
		try {
			otherServiceTable.setWidths(new float[] {5 , 29 ,10,11,11 , 6 , 6 ,6 ,6 ,6,6 ,12});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		otherServiceTable.addCell(getCell("Sr. No.", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("Description", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("HSN/SAC", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("Amount", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("Net Taxable Amt", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("CGST Rate", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("CGST Amt", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("SGST Rate", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("SGST Amt", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("IGST Rate", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("IGST Amt", font6bold, Element.ALIGN_CENTER));
		otherServiceTable.addCell(getCell("Payable Invoice Amt", font6bold, Element.ALIGN_CENTER));
		return otherServiceTable;
	}
	
	private HashMap<String ,String> getProductValueMap(SalesOrderProductLineItem item, double prodprice){
		HashMap<String ,String> valueMap = new HashMap<String ,String>();
		if(item.getProdName() != null && !item.getProdName().equals("")){
			valueMap.put("Description", item.getProdName());
		}else{
			valueMap.put("Description", "");
		}
//		if(invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
//			valueMap.put("HSN/SAC", "Annexure");
//		}else{
		if(item.getHsnCode() != null && !item.getHsnCode().equals("")){
			valueMap.put("HSN/SAC", item.getHsnCode());
		}else{
			valueMap.put("HSN/SAC", "");
		}
//		}
	//	totalAmount += item.getBasePaymentAmount();
		double cgstper=0;
		double sgstper=0;
		double igstper=0;
		
		double cgstAmt=0;
		double sgstAmt=0;
		double igstAmt=0;
	
		if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("CGST")||
				item.getServiceTax().getTaxPrintName().equalsIgnoreCase("SGST")){
			cgstper=item.getVatTax().getPercentage();
			sgstper=item.getServiceTax().getPercentage();
		}
		else if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("SGST")||
				item.getServiceTax().getTaxPrintName().equalsIgnoreCase("CGST")){
			sgstper=item.getVatTax().getPercentage();
			cgstper=item.getServiceTax().getPercentage();
		}
		else if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("IGST")||
				item.getServiceTax().getTaxPrintName().equalsIgnoreCase("")){
			
			igstper=item.getVatTax().getPercentage();
		}
		else if(item.getServiceTax().getTaxPrintName().equalsIgnoreCase("IGST")||
				item.getVatTax().getTaxPrintName().equalsIgnoreCase(" ")){
			
			igstper=item.getServiceTax().getPercentage();
		}
		
		if(cgstper!=0){
			cgstAmt = ((item.getTotalAmount())*cgstper)/100;
			cgstAmt = (Math.round(cgstAmt *100.0))/100.0;
			//totalCGST += cgstAmt;
		}
		if(sgstper!=0){
			sgstAmt = ((item.getTotalAmount())*sgstper)/100;
			sgstAmt = (Math.round(sgstAmt *100.0))/100.0;
			//totalSGST += sgstAmt;
		} 
		if(igstper!=0){
			igstAmt = ((item.getTotalAmount())*igstper)/100;
			igstAmt = (Math.round(igstAmt *100.0))/100.0;
			//totalIGST += igstAmt;
		}
		valueMap.put("CGST Rate" , taxAmount(cgstper));
		valueMap.put("SGST Rate" , taxAmount(sgstper));
		valueMap.put("IGST Rate" , taxAmount(igstper));
		valueMap.put("CGST Amt", getRoundOffValue(cgstAmt)+"");
		valueMap.put("SGST Amt", getRoundOffValue(sgstAmt)+"");
		valueMap.put("IGST Amt", getRoundOffValue(igstAmt)+"");
		double totalAmount = Math.round((item.getTotalAmount() + cgstAmt + sgstAmt + igstAmt)*100.0)/100.0;
		valueMap.put("Payable Invoice Amt", getRoundOffValue(totalAmount)+"");
		
		logger.log(Level.SEVERE,"Total Amount"+item.getTotalAmount());
		
		HashMap<String, StaffingDetails> map = new HashMap<String, StaffingDetails>();
		boolean isMapValue = false;
		if(invoiceype.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL) || invoiceype.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY) || invoiceype.equalsIgnoreCase(AppConstants.OVERTIME)
					|| invoiceype.equalsIgnoreCase(AppConstants.STAFFING) 
					|| invoiceype.equalsIgnoreCase(AppConstants.CONSOLIDATEBILL) || invoiceype.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)
					){
			map = getValuesFromCNC();
			double totalPay = 0 , mgntFees = 0 , total = 0 , noOfStaff = 0;
			if(map.size() > 0){
				if(map.containsKey(item.getProdName())){
					isMapValue = true;
					/**
					 * @author Anil,Date : 22-02-2019
					 * if duplicate designation flag true then will pick staffing detail from below method else from given map only
					 */
//					StaffingDetails details = map.get(item.getProdName());
					StaffingDetails details = null;
					if(designationFlag&&hsDesignation.contains(item.getProdName())){
						details=getStaffingDetailsFromCNC(item);
						if(details==null){
							details = map.get(item.getProdName());
						}
					}else{
						details = map.get(item.getProdName());
					}
					/**
					 * End
					 */
					
					 noOfStaff = details.getNoOfStaff();
					  try{
						  mgntFees = Double.parseDouble(details.getManagementFees()) / noOfStaff;
						 
					  }catch(Exception e){
						  mgntFees = 0;
						
					  }
					  try{
						  total = Double.parseDouble(details.getTotalOfPaidAndOverhead());
					  }catch(Exception e){
						  total = 0;
					  }
					  totalPay = total + mgntFees;
					  
					  logger.log(Level.SEVERE,"NoOfStaff- "+noOfStaff+" MgmtFees- "+mgntFees+" Total- "+total+" Total Pay- "+totalPay);
				}
			}
//			valueMap.put("No. of Staff", getRoundOffValue(noOfStaff));
			/*** Date 14-08-2020 by Vijay Storing no off staff in manpower filed
			 * if no of staff exist in the manpower field then it will display the same or
			 * else part as per the old logic
			 */
			if(item.getManpower()!=null && !item.getManpower().equals("")){
				valueMap.put("No. of Staff", item.getManpower());
			}
			else{
				valueMap.put("No. of Staff", getRoundOffValue(noOfStaff));
			}
		
			int noOfDays =(int) Math.round((totalPay / item.getPrice()));
			
			logger.log(Level.SEVERE,"NoOfDays- "+noOfDays+" Price- "+item.getPrice());
			
			if(invoice.isConsolidatePrice() && isMapValue){
				/**
				 * @author Anil,Rahul Tiwari(Sasha)
				 * @since 09-09-2020
				 * For national holiday bill, In case of consolidated invoice per day ctc and service charge
				 * is getting wrong calculated due to number of days
				 */
				double priceWithoutMgmtFee=total/noOfDays;
				
				logger.log(Level.SEVERE,"priceWithoutMgmtFee- "+priceWithoutMgmtFee);
//				item.setPrice(total/noOfDays);
				double quant = 0;
				if(!(item.getArea().equals("NA"))){
					quant = Double.parseDouble(item.getArea());
				}
				double mgnt = mgntFees / noOfDays;
				
				logger.log(Level.SEVERE,"perDayMgmtFee- "+mgnt);
				
				/**
				 * 
				 */
				int totalWithMgmtFee=(int) Math.round((priceWithoutMgmtFee+mgnt));
				
				logger.log(Level.SEVERE,"totalWithMgmtFee- "+totalWithMgmtFee);
				
				int storedTotalWithMgmtFees=(int) Math.round(item.getPrice());
				logger.log(Level.SEVERE,"storedTotalWithMgmtFees- "+storedTotalWithMgmtFees);
				if(totalWithMgmtFee!=storedTotalWithMgmtFees){
					double noOfDay =totalPay / item.getPrice();
					item.setPrice(total/noOfDay);
					logger.log(Level.SEVERE,"setPrice- "+item.getPrice());
					quant = 0;
					if(!(item.getArea().equals("NA"))){
						quant = Double.parseDouble(item.getArea());
					}
					mgnt = mgntFees / noOfDay;
				}else{
					item.setPrice(total/noOfDays);
				}
				/**
				 * 
				 */
				
				item.setTotalAmount(item.getPrice() * quant);
				
				/**
				 * @author Vijay Chougule Date - 16-09-2020
				 * Des :- CTC  amount value will print from invoice if it ctc amount exist in invoice 
				 * and else block as per the old code for old invoices
				 */
				if(item.getCtcAmount()!=0){
			  		logger.log(Level.SEVERE,"CTC Amount"+item.getCtcAmount());
					valueMap.put("CTC", getRoundOffValue(item.getCtcAmount()));
				}
				else{
					logger.log(Level.SEVERE,"Calculating CTC Amount"+item.getCtcAmount());
					valueMap.put("CTC", getRoundOffValue(Math.round(total*100.0)/100.0));
				}
				
				/**
				 * @author Vijay Chougule Date - 16-09-2020
				 * Des :- Service Charges amount value will print from invoice using invoice ctc amouont amangement using formula
				 * given by Rahul Tiwari
				 * and else block as per the old code for old invoices
				 */
				if(item.getCtcAmount()!=0 && item.getManagementFees()!=0){
			  		logger.log(Level.SEVERE,"item.getManagementFees() "+item.getManagementFees());
			  		logger.log(Level.SEVERE,"item.getCtcAmount() "+item.getCtcAmount());

					double perdayRate = (prodprice/((((item.getManagementFees()*100)/item.getCtcAmount())/100)+1));
			  		logger.log(Level.SEVERE,"perdayRate"+perdayRate);

					double serviceCharge = (((perdayRate*quant)*((item.getManagementFees()*100)/item.getCtcAmount()))/100);
			  		logger.log(Level.SEVERE,"serviceCharge"+serviceCharge);
					valueMap.put("Service Charges", getRoundOffValue(serviceCharge)+"");
					
					/**
					 * @author Anil
					 * @since 25-11-2020
					 * Calculating total amount as per the per day calculation
					 */
					try{
						item.setTotalAmount(Double.parseDouble(getRoundOffValue(perdayRate)) * quant);
					}catch(Exception e){
						
					}
				}
				else{
					valueMap.put("Service Charges", getRoundOffValue((mgnt*quant))+"");
	
				}
				
				
			}else{
				
				/**
				 * @author Vijay Chougule Date - 16-09-2020
				 * Des :- CTC  amount value will print from invoice if it ctc and management fees exist in invoice 
				 * and else block as per the old code for old invoices
				 */
				if(item.getCtcAmount()!=0 && item.getManagementFees()!=0){
					double ctcVal = item.getCtcAmount() + item.getManagementFees();
					valueMap.put("CTC", getRoundOffValue(ctcVal));
				}
				else{
					valueMap.put("CTC", getRoundOffValue(Math.round(totalPay*100.0)/100.0));
				}
			}
		}else if(invoiceype.equalsIgnoreCase(AppConstants.ARREARSBILL)){
			valueMap.put("CTC", getRoundOffValue(Math.round(item.getProdAvailableQuantity()*100.0)/100.0));
			valueMap.put("No. of Staff", item.getWarrantyPeriod()+"");
			
			/**
			 * @author Anil
			 * @since 17-11-2020
			 * Copied vijays code for calculating service charge in case of consolidated bill
			 */
			double quant = 0;
			if(!(item.getArea().equals("NA"))){
				quant = Double.parseDouble(item.getArea());
			}
			if(item.getCtcAmount()!=0 && item.getManagementFees()!=0){
		  		logger.log(Level.SEVERE,"item.getManagementFees() "+item.getManagementFees());
		  		logger.log(Level.SEVERE,"item.getCtcAmount() "+item.getCtcAmount());

				double perdayRate = (prodprice/((((item.getManagementFees()*100)/item.getCtcAmount())/100)+1));
		  		logger.log(Level.SEVERE,"perdayRate"+perdayRate);

				double serviceCharge = (((perdayRate*quant)*((item.getManagementFees()*100)/item.getCtcAmount()))/100);
		  		logger.log(Level.SEVERE,"serviceCharge"+serviceCharge);
				valueMap.put("Service Charges", getRoundOffValue(serviceCharge)+"");
				
				/**
				 * @author Anil
				 * @since 25-11-2020
				 * Calculating total amount as per the per day calculation
				 */
				try{
					item.setTotalAmount(Double.parseDouble(getRoundOffValue(perdayRate)) * quant);
				}catch(Exception e){
					
				}
			}
			
			if(item.getCtcAmount()!=0 && item.getManagementFees()!=0){
				double ctcVal = item.getCtcAmount() + item.getManagementFees();
				valueMap.put("CTC", getRoundOffValue(ctcVal));
				
				if(invoice.isConsolidatePrice()) {
					valueMap.put("CTC", getRoundOffValue(item.getCtcAmount()));
				}
			}
		}
			valueMap.put("Amount", getRoundOffValue(item.getTotalAmount())+"");
//			if(item.getProdName().equalsIgnoreCase("Housekeeping Equipment Charges")){
//				valueMap.put("Per Day Rate", "");
//			}else{
//				
//				valueMap.put("Per Day Rate", getRoundOffValue(item.getPrice())+"");
//			}
			
			/**
			 * @author Vijay Chougule Date - 16-09-2020
			 * Des :- Per Day Rate amount value will print from invoice using invoice ctc amouont amangement using formula
			 * given by Rahul Tiwari
			 * and else block as per the old code for old invoices
			 */
			double perdayRate = 0;
			if(invoice.isConsolidatePrice() && item.getCtcAmount()!=0 && item.getManagementFees()!=0){
				perdayRate = (prodprice/((((item.getManagementFees()*100)/item.getCtcAmount())/100)+1));
			}
			
			if(isMapValue){
				if(perdayRate!=0){
			  		logger.log(Level.SEVERE,"perdayRate == "+perdayRate);
					valueMap.put("Per Day Rate", getRoundOffValue(perdayRate)+"");
				}
				else{
					valueMap.put("Per Day Rate", getRoundOffValue(item.getPrice())+"");
				}
			}else if(invoiceype.equalsIgnoreCase(AppConstants.ARREARSBILL)){
				valueMap.put("Per Day Rate", getRoundOffValue(item.getPrice())+"");
			}else{			
				valueMap.put("Per Day Rate", "");
			}
			if(invoiceype.equalsIgnoreCase(AppConstants.CONSUMABLES)){
				valueMap.put("Man Days",  item.getQuantity()+"");
			}else{
				String area = "";
				if(!(item.getArea().equals("NA"))){
					area = item.getArea();
				}
				if(invoiceype.equalsIgnoreCase(AppConstants.ARREARSBILL)){
					
				}else if(item.getProdName().equalsIgnoreCase("Housekeeping Equipment Charges") || !isMapValue){
					area = "";
				}
				valueMap.put("Man Days",  area);
			}
			
		valueMap.put("Net Taxable Amt", getRoundOffValue(item.getBasePaymentAmount())+"");

	//	valueMap.put("Additional HK Hrs",  0+"");
		return valueMap;
	}
	
	private PdfPTable getStaffingTableHeader(){
		int size = 16;
		float sizeArray[] ;
		if(invoice.isConsolidatePrice()){
			size = 17;
			sizeArray = new float[]{5 , 19 ,8,4 ,6 , 5 , 5,7,7,8 , 5 , 5 ,5 ,5 ,5,5 ,8};
		}else{
			sizeArray = new float[] {5 , 19 ,8,4 ,6 , 5 , 5,10,10 , 5 , 5 ,5 ,5 ,5,5 ,10};
		}
		PdfPTable staffingTableHeader = new PdfPTable(size);
		staffingTableHeader.setWidthPercentage(100); 
		try {
			staffingTableHeader.setWidths(sizeArray);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		staffingTableHeader.addCell(getCell("Sr. No.", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Description", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("HSN/SAC", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("No. of Staff", font6bold, Element.ALIGN_CENTER));
		if(AppConstants.ARREARSBILL.equalsIgnoreCase(invoiceype)){
			staffingTableHeader.addCell(getCell("Arrears CTC", font6bold, Element.ALIGN_CENTER));
		}else{
			staffingTableHeader.addCell(getCell("CTC", font6bold, Element.ALIGN_CENTER));
		}
		staffingTableHeader.addCell(getCell("Per Day Rate", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Man Days", font6bold, Element.ALIGN_CENTER));
		
		staffingTableHeader.addCell(getCell("Total", font6bold, Element.ALIGN_CENTER));
		if(invoice.isConsolidatePrice()){
			staffingTableHeader.addCell(getCell("Service Charges", font6bold, Element.ALIGN_CENTER));
		}
		staffingTableHeader.addCell(getCell("Taxable Amt", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("CGST Rate", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("CGST Amt", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("SGST Rate", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("SGST Amt", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("IGST Rate", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("IGST Amt", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Payable Invoice Amt", font6bold, Element.ALIGN_CENTER));
		return staffingTableHeader;
	}
	
	private void getStaffingTable(int count){
		PdfPTable staffingTable = getStaffingTableHeader();
		for(int i =count ; i < invoice.getSalesOrderProducts().size() ; i++){
			if(invoice.getSalesOrderProducts().get(i).getBasePaymentAmount() == 0){
				continue;
			}
			
			if(count == 0){
			if (noOfLines == 0) {
				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
				prouductCount = i;
				break;
			}			
				noOfLines = noOfLines - 1;
			}
			j++;
			
			logger.log(Level.SEVERE, "invoice product Name "+invoice.getSalesOrderProducts().get(i).getProdName());
			logger.log(Level.SEVERE, "invoice product price "+invoice.getSalesOrderProducts().get(i).getPrice());
			logger.log(Level.SEVERE, "invoice ID "+invoice.getCount());

			HashMap<String ,String> valueMap = getProductValueMap(invoice.getSalesOrderProducts().get(i),invoice.getSalesOrderProducts().get(i).getPrice());
			
			staffingTable.addCell(getCell(j+"", font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("Description"), font6, Element.ALIGN_LEFT));
			staffingTable.addCell(getCell(valueMap.get("HSN/SAC"), font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("No. of Staff"), font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("CTC"), font6, Element.ALIGN_RIGHT));
			staffingTable.addCell(getCell(valueMap.get("Per Day Rate"), font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("Man Days"), font6, Element.ALIGN_CENTER));
			
			staffingTable.addCell(getCell(valueMap.get("Amount"), font6, Element.ALIGN_RIGHT));
			if(invoice.isConsolidatePrice()){
				staffingTable.addCell(getCell(valueMap.get("Service Charges"), font6, Element.ALIGN_RIGHT));
			}
			staffingTable.addCell(getCell(valueMap.get("Net Taxable Amt"), font6, Element.ALIGN_RIGHT));
			staffingTable.addCell(getCell(valueMap.get("CGST Rate"), font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("CGST Amt"), font6, Element.ALIGN_RIGHT));
			staffingTable.addCell(getCell(valueMap.get("SGST Rate"), font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("SGST Amt"), font6, Element.ALIGN_RIGHT));
			staffingTable.addCell(getCell(valueMap.get("IGST Rate"), font6, Element.ALIGN_CENTER));
			staffingTable.addCell(getCell(valueMap.get("IGST Amt"), font6, Element.ALIGN_RIGHT));
			staffingTable.addCell(getCell(valueMap.get("Payable Invoice Amt"), font6, Element.ALIGN_RIGHT));
		
		}
		
		
		if(totalNoOfLineItems<20||prouductCount==totalNoOfLineItems-1){
		
		//total tab
		PdfPCell totalAmountCell = getCell("Total Amount", font6bold, Element.ALIGN_CENTER);
		if(invoice.isConsolidatePrice()){
			totalAmountCell.setColspan(9);
		}else{
			totalAmountCell.setColspan(8);
		}
		staffingTable.addCell(totalAmountCell);
		staffingTable.addCell(getCell(getRoundOffValue(totalAmount), font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(getRoundOffValue(totalCGST), font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(getRoundOffValue(totalSGST), font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(getRoundOffValue(totalIGST), font6bold, Element.ALIGN_RIGHT));
		staffingTable.addCell(getCell(getRoundOffValue(invoice.getNetPayable()), font6bold, Element.ALIGN_RIGHT));
		
		}
		
		try {
			document.add(staffingTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//consumables
	private void getConsumablesTable(int count){
		PdfPTable consumableTable = getConsumablesTableHeader();
		for(int i =count ; i < invoice.getSalesOrderProducts().size() ; i++){
			if(invoice.getSalesOrderProducts().get(i).getBasePaymentAmount() == 0){
				continue;
			}
			
		  if(count == 0){
			if (noOfLines == 0) {
				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
				prouductCount = i;
				break;
			}
			
			noOfLines = noOfLines - 1;
		  }
		  
		  j++;
		  
			HashMap<String ,String> valueMap = getProductValueMap(invoice.getSalesOrderProducts().get(i),invoice.getSalesOrderProducts().get(i).getPrice());
			consumableTable.addCell(getCell(j+"", font6, Element.ALIGN_CENTER));
			consumableTable.addCell(getCell(valueMap.get("Description"), font6, Element.ALIGN_LEFT));
			consumableTable.addCell(getCell(valueMap.get("HSN/SAC"), font6, Element.ALIGN_CENTER));
	//		if(invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
				//consumableTable.addCell(getCell(valueMap.get("Man Days"), font6, Element.ALIGN_CENTER));
			if(invoiceype.equalsIgnoreCase(AppConstants.EQUIPMENTRENTAL) 
					|| invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)
					|| invoiceype.equalsIgnoreCase(AppConstants.CONSUMABLES)){
				consumableTable.addCell(getCell(valueMap.get("Amount"), font6, Element.ALIGN_RIGHT));
			}else{
				consumableTable.addCell(getCell(valueMap.get("Per Day Rate"), font6, Element.ALIGN_RIGHT));
			}
	//		}else{
//				consumableTable.addCell(getCell(valueMap.get("Man Days"), font6, Element.ALIGN_CENTER));
//				consumableTable.addCell(getCell(valueMap.get("Per Day Rate"), font6, Element.ALIGN_RIGHT));
//			}
			consumableTable.addCell(getCell(valueMap.get("Net Taxable Amt"), font6, Element.ALIGN_RIGHT));
			consumableTable.addCell(getCell(valueMap.get("CGST Rate"), font6, Element.ALIGN_CENTER));
			consumableTable.addCell(getCell(valueMap.get("CGST Amt"), font6, Element.ALIGN_RIGHT));
			consumableTable.addCell(getCell(valueMap.get("SGST Rate"), font6, Element.ALIGN_CENTER));
			consumableTable.addCell(getCell(valueMap.get("SGST Amt"), font6, Element.ALIGN_RIGHT));
			consumableTable.addCell(getCell(valueMap.get("IGST Rate"), font6, Element.ALIGN_CENTER));
			consumableTable.addCell(getCell(valueMap.get("IGST Amt"), font6, Element.ALIGN_RIGHT));
			consumableTable.addCell(getCell(valueMap.get("Payable Invoice Amt"), font6, Element.ALIGN_RIGHT));		
		}
		
		if(totalNoOfLineItems<20||prouductCount==totalNoOfLineItems-1){
		//total tab
		PdfPCell totalAmountCell = getCell("Total Amount", font6bold, Element.ALIGN_CENTER);
	//	if(invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
			totalAmountCell.setColspan(4);
//		}else{
//			totalAmountCell.setColspan(5);
//		}
		consumableTable.addCell(totalAmountCell);
		consumableTable.addCell(getCell(getRoundOffValue(totalAmount), font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(getRoundOffValue(totalCGST), font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(getRoundOffValue(totalSGST), font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(" ", font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(getRoundOffValue(totalIGST), font6bold, Element.ALIGN_RIGHT));
		consumableTable.addCell(getCell(getRoundOffValue(invoice.getNetPayable()), font6bold, Element.ALIGN_RIGHT));
		}
		try {
			document.add(consumableTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private PdfPTable getConsumablesTableHeader(){
		PdfPTable consumableHeaderTable;
	//	if(invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
			consumableHeaderTable  = new PdfPTable(12);
	//	}else{
	//		consumableHeaderTable  = new PdfPTable(13);
	//	}
		
		 
		try {
			
		//	if(invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
				consumableHeaderTable.setWidthPercentage(100);
				consumableHeaderTable.setWidths(new float[] {5 , 29 ,10 , 15,10 , 5 , 6 ,5 ,6 ,5,6 ,12});
//			}else{
//				consumableHeaderTable.setWidthPercentage(100);
//				consumableHeaderTable.setWidths(new float[] {5 , 29 ,10 , 5,10,10 , 5 , 6 ,5 ,6 ,5,6 ,12});
//			}
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		consumableHeaderTable.addCell(getCell("Sr. No.", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("Description", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("HSN/SAC", font6bold, Element.ALIGN_CENTER));
		//if(invoiceype.equalsIgnoreCase(AppConstants.FIXEDCONSUMABLES)){
			//consumableHeaderTable.addCell(getCell("Quantity", font6bold, Element.ALIGN_CENTER));
			consumableHeaderTable.addCell(getCell("Amount", font6bold, Element.ALIGN_CENTER));
//		}else{
//		consumableHeaderTable.addCell(getCell("Quantity", font6bold, Element.ALIGN_CENTER));
//		consumableHeaderTable.addCell(getCell("Rate", font6bold, Element.ALIGN_CENTER));
//		}
		consumableHeaderTable.addCell(getCell("Net Taxable Amt", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("CGST Rate", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("CGST Amt", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("SGST Rate", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("SGST Amt", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("IGST Rate", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("IGST Amt", font6bold, Element.ALIGN_CENTER));
		consumableHeaderTable.addCell(getCell("Payable Invoice Amt", font6bold, Element.ALIGN_CENTER));
		return consumableHeaderTable;
	}
	
	private void calculateTotalValue(){
		map = new HashMap<String , TaxSummaryBean>();
		double amount = 0;
		for(SalesOrderProductLineItem item :invoice.getSalesOrderProducts()){
			//if(map.size() >0)
				double cgstper=0;
				double sgstper=0;
				double igstper=0;
				
				if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("CGST")||
						item.getServiceTax().getTaxPrintName().equalsIgnoreCase("SGST")){
					cgstper=item.getVatTax().getPercentage();
					sgstper=item.getServiceTax().getPercentage();
				}
				else if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("SGST")||
						item.getServiceTax().getTaxPrintName().equalsIgnoreCase("CGST")){
					sgstper=item.getVatTax().getPercentage();
					cgstper=item.getServiceTax().getPercentage();
				}
				else if(item.getVatTax().getTaxPrintName().equalsIgnoreCase("IGST")||
						item.getServiceTax().getTaxPrintName().equalsIgnoreCase("")){
					
					igstper=item.getVatTax().getPercentage();
				}
				else if(item.getServiceTax().getTaxPrintName().equalsIgnoreCase("IGST")||
						item.getVatTax().getTaxPrintName().equalsIgnoreCase(" ")){
					
					igstper=item.getServiceTax().getPercentage();
				}
				double cgstAmt = 0 ,sgstAmt = 0 , igstAmt =0 ;
				if(cgstper!=0){
					cgstAmt = ((item.getBasePaymentAmount())*cgstper)/100;
					cgstAmt = (Math.round(cgstAmt *100.0))/100.0;
					totalCGST += cgstAmt;
				}
				if(sgstper!=0){
					sgstAmt = ((item.getBasePaymentAmount())*sgstper)/100;
					sgstAmt = (Math.round(sgstAmt *100.0))/100.0;
					totalSGST += sgstAmt;
				} 
				if(igstper!=0){
					igstAmt = ((item.getBasePaymentAmount())*igstper)/100;
					igstAmt = (Math.round(igstAmt *100.0))/100.0;
					totalIGST += igstAmt;
				}
				totalAmount += ((Math.round(item.getBasePaymentAmount()*100.0))/100.0);
				if(map.containsKey(item.getHsnCode())){
					TaxSummaryBean bean = new TaxSummaryBean();
					bean = map.get(item.getHsnCode());
					amount = bean.getTaxAmount();
					amount+= item.getBasePaymentAmount();
					bean.setHsnNumber(item.getHsnCode());
					bean.setIgstPer(igstper);
					bean.setSgstPer(sgstper);
					bean.setCgstPer(cgstper);
					bean.setTaxAmount(amount);
					map.put(item.getHsnCode(), bean);
				}else{
					TaxSummaryBean bean1 = new TaxSummaryBean();
					bean1.setHsnNumber(item.getHsnCode());
					bean1.setIgstPer(igstper);
					bean1.setSgstPer(sgstper);
					bean1.setCgstPer(cgstper);
					bean1.setTaxAmount(item.getBasePaymentAmount());
					map.put(item.getHsnCode(), bean1);
				}
			}
			
	//	}
	}
private HashMap<String, StaffingDetails> getValuesFromCNC(){
	HashMap<String , StaffingDetails> map = new HashMap<String , StaffingDetails>();
	for(StaffingDetails detail : cnc.getSaffingDetailsList()){
		map.put(detail.getDesignation(), detail);
	}
	return map;
}

	private String getRoundOffValue(double value) {
		if(value != 0){
			DecimalFormat df = new DecimalFormat("##,##,##,##,##0.00");
			return df.format(value);
		}else{
			return "";
		}
	}

	private String taxAmount(double amount) {
		String strAmount = amount + "";
		if (amount != 0) {
			String arr[] = strAmount.split("\\.");
			String taxInString = amount + "%";
			int decimalAmount = 0;
			if (arr.length == 2) {
				try {
					decimalAmount = Integer.parseInt(arr[1]);
				} catch (Exception e) {
					decimalAmount = 0;
				}
				if (decimalAmount != 0) {
					return taxInString;
				} else {
					int tax = (int) amount;
					taxInString = tax + "%";
				}
			}
			return taxInString;
		} else {
			return "";
		}
	}
	private void createletterhead() {
	/**** image ***/
//	DocumentUpload letterheaddocument = comp.getUploadHeader();
//
//	// patch
//	String hostUrl;
//	String environment = System
//			.getProperty("com.google.appengine.runtime.environment");
//	if (environment.equals("Production")) {
//		String applicationId = System
//				.getProperty("com.google.appengine.application.id");
//		String version = System
//				.getProperty("com.google.appengine.application.version");
//		hostUrl = "http://" + version + "." + applicationId
//				+ ".appspot.com/";
//	} else {
//		hostUrl = "http://localhost:8888";
//	}
//	PdfPCell imageSignCell = null;
//	Image image2 = null;
//	try {
//		image2 = Image.getInstance(new URL(hostUrl
//				+ letterheaddocument.getUrl()));
//		image2.scalePercent(20f);
//		// image2.setAbsolutePosition(40f,765f);
//		// doc.add(image2);
//
//		imageSignCell = new PdfPCell();
//		imageSignCell.setBorder(0);
//		imageSignCell.setImage(image2);
//		// imageSignCell.setPaddingTop(8);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		// imageSignCell.setFixedHeight(40);
//	} catch (Exception e) {
//		e.printStackTrace();
//	}

//	Image image1 = null;
//	try {
//		image1 = Image.getInstance("images/Capture.JPG");
//		image1.scalePercent(20f);
//		// image1.setAbsolutePosition(40f,765f);
//		// doc.add(image1);
//
//		imageSignCell = new PdfPCell(image1);
//		// imageSignCell.addElement();
//		imageSignCell.setImage(image1);
//		// imageSignCell.setFixedHeight(40);
//		imageSignCell.setBorder(0);
//		// imageSignCell.setPaddingTop(8);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	} catch (Exception e) {
//		e.printStackTrace();
//	}

	PdfPTable letterheadTab = new PdfPTable(1);
	letterheadTab.setWidthPercentage(100);

//	if (imageSignCell != null) {
//		letterheadTab.addCell(imageSignCell);
//	} else {
		Phrase logoblank = new Phrase(" ");
		PdfPCell logoblankcell = new PdfPCell(logoblank);
		logoblankcell.setBorder(0);
		logoblankcell.setFixedHeight(80);
		letterheadTab.addCell(logoblankcell);
//	}

	try {
		document.add(letterheadTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}	
	
private void addAnnexure(){
	Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
	try {
		document.add(nextpage);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	PdfPTable titleTab = new PdfPTable(2);
	titleTab.setWidthPercentage(100);
	
	try {
		titleTab.setWidths(new float[]{80 , 20});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	Phrase titlephbl = new Phrase(" ", font10);
	PdfPCell titleCellbl = new PdfPCell(titlephbl);
	titleCellbl.setBorder(0);
	titleTab.addCell(titleCellbl); 
	titleTab.addCell(titleCellbl); 
	
	Phrase titleph = new Phrase(companyName, font12bold);
	PdfPCell titleCell = new PdfPCell(titleph);
	titleCell.setPaddingTop(5);
	titleCell.setPaddingBottom(5);
	titleCell.setBorderWidthRight(0);
	titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	titleTab.addCell(titleCell);
	
	
//	PdfPCell titleCellbl1 = new PdfPCell(titlephbl);
//	titleCellbl1.setBorderWidthLeft(0);
//	titleTab.addCell(titleCellbl1);
	DocumentUpload logodocument = comp.getLogo();

	// patch
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}
	PdfPCell imageSignCell = null;
	Image image2 = null;
	try {
		image2 = Image
				.getInstance(new URL(hostUrl + logodocument.getUrl()));
		image2.scalePercent(20f);
		imageSignCell = new PdfPCell(image2);
		imageSignCell.setBorderWidthLeft(0);
//		imageSignCell.setBorderWidthRight(0);
//		imageSignCell.setBorderWidthBottom(0);
		imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		imageSignCell.setFixedHeight(30);
		imageSignCell.setColspan(0);
		//imageSignCell.setBorder(0);
	} catch (Exception e) {
		e.printStackTrace();
		
	}
	if(imageSignCell == null){
		PdfPCell titleCellbl1 = new PdfPCell(titlephbl);
		titleCellbl1.setBorderWidthLeft(0);
		titleTab.addCell(titleCellbl1);
	}else{
		titleTab.addCell(imageSignCell);
	}
	
	try {
		document.add(titleTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	HashMap<String , String> hsnCodeMap = new HashMap<String , String>();
	if(invoice.getSalesOrderProducts() != null && invoice.getSalesOrderProducts().size() > 0){
		for(SalesOrderProductLineItem item : invoice.getSalesOrderProducts()){
			hsnCodeMap.put(item.getProdName(), item.getHsnCode());
		}
	}
	List<Integer> inList = new ArrayList<Integer>();
	inList.addAll(invoice.getCncBillAnnexureId());
	System.out.print("list :" + inList.size());
	List<CNCBillAnnexure> annexureList = ofy().load().type(CNCBillAnnexure.class).filter("companyId", invoice.getCompanyId())
			.filter("count IN", inList).list();
	System.out.print("list 1:" + annexureList);
	for(CNCBillAnnexure annexure : annexureList){
		PdfPTable annexureTable = getAnnexureHeader();
	    int i = 1;
	    double totalAmount = 0 , totalTaxableAmount = 0;
	    System.out.print("list 1:" + annexureList);
		for(CNCBillAnnexureBean bean : annexure.getCNCBillAnnexureList()){
			System.out.print("list 1:" + annexureList);
			if(bean.getArrearsCTC() != 0){
				annexureTable.addCell(getCell(i+"", font6, Element.ALIGN_CENTER));
				annexureTable.addCell(getCell(bean.getProductName(), font6, Element.ALIGN_LEFT));
				annexureTable.addCell(getCell(hsnCodeMap.get(bean.getProductName()), font6, Element.ALIGN_CENTER));
				annexureTable.addCell(getCell(bean.getNoOfStaff()+"", font6, Element.ALIGN_CENTER));
				annexureTable.addCell(getCell(bean.getMonth(), font6, Element.ALIGN_CENTER));
				annexureTable.addCell(getCell(getRoundOffValue(bean.getOldCTC())+"", font6, Element.ALIGN_RIGHT));
				annexureTable.addCell(getCell(getRoundOffValue(bean.getNewCTC())+"", font6, Element.ALIGN_RIGHT));
				annexureTable.addCell(getCell(getRoundOffValue(bean.getArrearsCTC())+"", font6, Element.ALIGN_RIGHT));	
				annexureTable.addCell(getCell(getRoundOffValue(bean.getPerDayRate())+"", font6, Element.ALIGN_CENTER));
				annexureTable.addCell(getCell(bean.getManDays()+"", font6, Element.ALIGN_CENTER));		
				annexureTable.addCell(getCell(getRoundOffValue(bean.getTotal())+"", font6, Element.ALIGN_RIGHT));		
				annexureTable.addCell(getCell(getRoundOffValue(bean.getTaxableAmount())+"", font6, Element.ALIGN_RIGHT));
				i++;
				totalAmount += bean.getTotal();
				totalTaxableAmount += bean.getTaxableAmount();
			}
		}
		for(int j = 1 ;j<= 10 ; j++){
			annexureTable.addCell(getCell(" ", font6bold, Element.ALIGN_CENTER));
		}
		annexureTable.addCell(getCell(getRoundOffValue(totalAmount)+"", font6bold, Element.ALIGN_RIGHT));		
		annexureTable.addCell(getCell(getRoundOffValue(totalTaxableAmount)+"", font6bold, Element.ALIGN_RIGHT));
		//createSubTitle(" ");
		PdfPTable blankTab = new PdfPTable(1);
		blankTab.setWidthPercentage(100);
		Phrase ph = new Phrase(" ", font6bold);
		PdfPCell cell = new PdfPCell(ph);
		cell.setBorder(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankTab.addCell(cell);
		blankTab.addCell(cell);
		try {
			document.add(blankTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		try {
			document.add(annexureTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	

}
	private PdfPTable getAnnexureHeader(){
		int size = 12;
		float sizeArray[] ;
		
		sizeArray = new float[] {5 , 20 ,8,5 ,10 , 10 , 10,10,9 , 5 , 10 ,10};
		
		PdfPTable staffingTableHeader = new PdfPTable(size);
		staffingTableHeader.setWidthPercentage(100); 
		try {
			staffingTableHeader.setWidths(sizeArray);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		staffingTableHeader.addCell(getCell("Sr. No.", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Product Name", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("HSN/SAC", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("No. of Staff", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Month", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("New CTC", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Old CTC", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Arrears CTC", font6bold, Element.ALIGN_CENTER));	
		staffingTableHeader.addCell(getCell("Per Day Rate", font6bold, Element.ALIGN_CENTER));
		staffingTableHeader.addCell(getCell("Man Days", font6bold, Element.ALIGN_CENTER));		
		staffingTableHeader.addCell(getCell("Total", font6bold, Element.ALIGN_CENTER));		
		staffingTableHeader.addCell(getCell("Taxable Amt", font6bold, Element.ALIGN_CENTER));
		return staffingTableHeader;
	}
}

