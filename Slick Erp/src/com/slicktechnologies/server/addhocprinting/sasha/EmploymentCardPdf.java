package com.slicktechnologies.server.addhocprinting.sasha;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EmploymentCardPdf {
	Employee employee;
	Company comp;
	HrProject project;
	Branch branch;
	Customer customer;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	public Document document;
	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	DecimalFormat df=new DecimalFormat("0");
	
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Logger logger = Logger.getLogger("EmploymentCardPdf.class");
	public EmploymentCardPdf() {
		// TODO Auto-generated constructor stub
	}
	
	public void loadEmployeeData(Long count){
		employee = ofy().load().type(Employee.class).id(count).now();
		if (employee!= null){
			comp = ofy().load().type(Company.class).filter("companyId", employee.getCompanyId()).first().now();
		}
		if (employee!= null&&employee.getProjectName()!=null&&!employee.getProjectName().equals("")){
			project = ofy().load().type(HrProject.class).filter("companyId", employee.getCompanyId()).filter("projectName", employee.getProjectName()).first().now();
		}
		if (employee!= null&&employee.getBranchName()!=null&&!employee.getBranchName().equals("")){
			branch = ofy().load().type(Branch.class).filter("companyId", employee.getCompanyId()).filter("buisnessUnitName", employee.getBranchName()).first().now();
		}
		
		if (project!= null&&project.getPersoninfo()!=null&&project.getPersoninfo().getCount()!=0){
			customer = ofy().load().type(Customer.class).filter("companyId", project.getCompanyId()).filter("count", project.getPersoninfo().getCount()).first().now();
		}
	}
	
	public void createPdf() {
//		createEmpomentCardPdf();
		
		// width 5.5 cm : 2.17 inch * 72 =157;; height 8.5 cm:3.34 inch * 72=242
//		createCompanyAddress();
		
		PdfPTable table=new PdfPTable(2);
		table.setWidthPercentage(100f);
		
		PdfPTable rightTbl=createEmploymentCardForBangalore();
		rightTbl.setTotalWidth(157);
		
		PdfPTable leftTbl=createBackSideTbl();
		leftTbl.setTotalWidth(157);
		
		PdfPCell rightCell=new PdfPCell(rightTbl);
		rightCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightCell.setFixedHeight(242);
		rightCell.setBorder(0);
	
		
		PdfPCell leftCell=new PdfPCell(leftTbl);
		leftCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftCell.setFixedHeight(242);
		leftCell.setBorder(0);
		
		table.addCell(rightCell);
		table.addCell(leftCell);
		
		
		table.setTotalWidth(320);
		table.setLockedWidth(true);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private PdfPTable createBackSideTbl() {
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		
		outerTbl.addCell(getCell("Grievance No.: "+"9326900010", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell("D.O.B.:"+fmt.format(employee.getDob()), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
			outerTbl.addCell(getCell("Contact No.:"+employee.getCellNumber1(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getBloodGroup()!=null&&!employee.getBloodGroup().equals(""))
			outerTbl.addCell(getCell("Blood Group:"+employee.getBloodGroup(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getPPFNaumber()!=null&&!employee.getPPFNaumber().equals(""))
			outerTbl.addCell(getCell("PF No.:"+employee.getPPFNaumber(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getUANno()!=null&&!employee.getUANno().equals(""))
			outerTbl.addCell(getCell("UAN :"+employee.getUANno(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getEmployeeESICcode()!=null&&!employee.getEmployeeESICcode().equals(""))
			outerTbl.addCell(getCell("ESIC No.:"+employee.getEmployeeESICcode(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		/***
		 * 
		 */
		
		DocumentUpload docu =comp.getUploadDigitalSign();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		try {
			logger.log(Level.SEVERE,"comp.getUploadDigitalSign() hostUrl="+hostUrl+" doc url="+docu.getUrl());
			Image image2 = Image.getInstance(new URL(hostUrl+docu.getUrl()));
//			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);
			image2.scaleAbsolute(40, 40); 
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			imageSignCell.setFixedHeight(20);
			imageSignCell.setRowspan(3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setRowspan(3);
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{50,50});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		logoTable.addCell(blankCell).setBorder(0);
		if (imageSignCell != null) {
			logoTable.addCell(imageSignCell).setBorder(0);
		} else {
			logoTable.addCell(blankCell).setBorder(0);
		}
		
		PdfPCell logocell=new PdfPCell(logoTable);
		logocell.setBorder(0);
		outerTbl.addCell(logocell);
		
		
		/**
		 * 
		 */
		outerTbl.addCell(getCell("_________________", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell("Authorised Signatory", font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
//		try {
			PdfPCell outercell=new PdfPCell();
			outercell.addElement(outerTbl);
//			outercell.setBorder(0);
			PdfPTable parent = new PdfPTable(1);
			parent.setWidthPercentage(100f);
			parent.addCell(outercell);
			
			parent.setTotalWidth(157);
			parent.setLockedWidth(true);
			parent.setHorizontalAlignment(Element.ALIGN_LEFT);
//			document.add(Chunk.NEXTPAGE);
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
			
		return parent;
		
	}

	private PdfPTable createEmploymentCardForBangalore() {
		
		
		DocumentUpload docu =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		try {
			logger.log(Level.SEVERE,"createEmploymentCardForBangalore company logo hostUrl="+hostUrl+" doc url="+docu.getUrl());
			Image image2 = Image.getInstance(new URL(hostUrl+docu.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setFixedHeight(20);
			imageSignCell.setRowspan(3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setRowspan(2);
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		if (imageSignCell != null) {
			logoTable.addCell(imageSignCell).setBorder(0);
			logoTable.addCell(getCell("Employment Card", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("Form XIV[See Rule 76]", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		} else {
			logoTable.addCell(blankCell).setBorder(0);
			logoTable.addCell(getCell("Employment Card", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("Form XIV[See Rule 76]", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		Phrase companyName= new Phrase(comp.getBusinessUnitName(),font8bold);
	    PdfPCell companyNameCell=new PdfPCell(companyName);
	    companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    companyNameCell.setBorder(0);
	     
		String addressline1="Corp. Off.: ";
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=addressline1+comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2();
		}else{
			addressline1=addressline1+comp.getAddress().getAddrLine1();
		}
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getLandmark()+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getLandmark()+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		
		PdfPCell addresscell=new PdfPCell(locality);
		addresscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		addresscell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
//		companyDetails.setSpacingAfter(5f);
		
		
		
		
		PdfPCell comapnyCell=new PdfPCell(logoTable);
		comapnyCell.setBorder(0);
		PdfPCell titlePdfCell=new PdfPCell(companyDetails);
//		titlePdfCell.setBorderWidthTop(0);
//		titlePdfCell.setBorderWidthLeft(0);
//		titlePdfCell.setBorderWidthRight(0);
		titlePdfCell.setBorder(0);
		
		PdfPTable headTbl=new PdfPTable(1);
		headTbl.setWidthPercentage(100);
		headTbl.addCell(comapnyCell);
		headTbl.addCell(titlePdfCell);
		
//		try {
//			document.add(headTbl);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		
		
		PdfPTable outerTbl=new PdfPTable(1);
		outerTbl.setWidthPercentage(100f);
		
		PdfPCell branchCell=null;
		if(branch!=null&&branch.getAddress()!=null){
			branchCell=getCell("Branch: "+branch.getAddress().getCompleteAddress(), font7bold, Element.ALIGN_CENTER, 0, 0, 0);
			branchCell.setBorder(0);
			outerTbl.addCell(branchCell);
			outerTbl.addCell(getCell(" ", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		}else{
			branchCell=getCell("Branch: ", font7bold, Element.ALIGN_CENTER, 0, 0, 0);
			branchCell.setBorder(0);
			outerTbl.addCell(branchCell);
		}
		
		
		if(employee!=null){
			outerTbl.addCell(getCell("Name: "+employee.getFullName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell("Name: ", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		PdfPTable empTbl=new PdfPTable(2);
		empTbl.setWidthPercentage(100f);
		try {
			empTbl.setWidths(new float[]{28,72});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		String skill="";
		if(employee.isSkilled()==true&&employee.isUnskilled()==false&&employee.isSemiSkilled()==false){
			skill="/Skilled";
		}else if(employee.isSkilled()==false&&employee.isUnskilled()==true&&employee.isSemiSkilled()==false){
			skill="/Unskilled";
		}else if(employee.isSkilled()==false&&employee.isUnskilled()==false&&employee.isSemiSkilled()==true){
			skill="/Semi Skilled";
		}
		
		String designation=employee.getDesignation();
		if(designation.length()>20){
			designation=designation.substring(0, 20);
		}
		
		empTbl.addCell(getEmployeePhotoCell());
		if(employee.getDesignation()!=null&&!employee.getDesignation().equals(""))
			empTbl.addCell(getCell("Dsgn: "+employee.getDesignation(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		else
			empTbl.addCell(getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(skill!=null&&!skill.equals(""))
			empTbl.addCell(getCell("Wage Rate:Min. Wages "+skill, font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		else
			empTbl.addCell(getCell("", font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getSalaryAmt()>0)
			empTbl.addCell(getCell(df.format(employee.getSalaryAmt()), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		else
			empTbl.addCell(getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getJoinedAt()!=null&&!employee.getJoinedAt().equals(""))
			empTbl.addCell(getCell("D.O.J.: "+fmt.format(employee.getJoinedAt()), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		else
			empTbl.addCell(getCell("D.O.J.: ", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		empTbl.addCell(getCell("Emp./Sr. No.: "+employee.getCount(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		PdfPCell emplCell=new PdfPCell();
		emplCell.addElement(empTbl);
		emplCell.setBorder(0);
		outerTbl.addCell(emplCell);
		
		
		
		outerTbl.addCell(getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		String customerName="";
		if(customer!=null){
			if(customer.getCustCorresponence()!=null&&!customer.getCustCorresponence().equals("")){
				customerName=customer.getCustCorresponence();
			}else if(customer.isCompany()==true){
				customerName="M/s "+customer.getCompanyName();
//				outerTbl.addCell(getCell("Principle Employer: "+"M/s "+customer.getCompanyName(), font8, Element.ALIGN_LEFT, 0, 0, 50)).setBorder(0);
			}else{
				customerName=customer.getFullname();
//				outerTbl.addCell(getCell("Principle Employer: "+customer.getFullname(), font8, Element.ALIGN_LEFT, 0, 0, 50)).setBorder(0);
			}
		}
//		else{
//			outerTbl.addCell(getCell("Principle Employer: ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		}
		
		if(customerName.length()>65){
			customerName=customerName.substring(0, 65);
		}
		if(customerName!=null&&!customerName.equals(""))
			outerTbl.addCell(getCell("Principle Employer: "+customerName, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		String projectName="";
		
		if(project!=null&&project.getProjectName()!=null){
			projectName=project.getProjectName();
//			outerTbl.addCell(getCell("Location of Work: "+project.getProjectName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
//		else{
//			outerTbl.addCell(getCell("Location of Work: ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		}
		if(projectName.length()>21){
			projectName=projectName.substring(0, 21);
		}
		
		
		//old label Location of Work
		if(projectName!=null&&!projectName.equals(""))
			outerTbl.addCell(getCell("Project: "+projectName, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		String natureOfWork="";
		if(project!=null&&project.getNatureOfWork()!=null){
			natureOfWork=project.getNatureOfWork();
//			outerTbl.addCell(getCell("Nature of Work: "+project.getNatureOfWork(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
//		else{
//			outerTbl.addCell(getCell("Nature of Work: ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		}
		if(natureOfWork.length()>22){
			natureOfWork=natureOfWork.substring(0, 22);
		}
		if(natureOfWork!=null&&!natureOfWork.equals(""))
		outerTbl.addCell(getCell("Nature of Work: "+natureOfWork, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
//		try {
			PdfPCell headcell=new PdfPCell();
			headcell.addElement(headTbl);
//			headcell.setBorder(0);
			
			PdfPCell outercell=new PdfPCell();
			outercell.addElement(outerTbl);
//			outercell.setBorder(0);
//			outercell.setFixedHeight(600);
			
			PdfPTable parent = new PdfPTable(1);
			parent.setWidthPercentage(100f);
			parent.setTotalWidth(157);
			parent.setLockedWidth(true);
			parent.addCell(headcell);
			parent.addCell(outercell);
			parent.setHorizontalAlignment(Element.ALIGN_LEFT);
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
			
			return parent;
	}

	private void createEmpomentCardPdf() {
		
		PdfPTable outerTbl=new PdfPTable(2);
		outerTbl.setWidthPercentage(100f);
		outerTbl.addCell(getCell("Form XIV", font12bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font12bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell("[See Rule 76]", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell("Employment Card", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell("Name and address of contractor ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(comp!=null){
			if(branch!=null){
				outerTbl.addCell(getCell("M/s "+comp.getBusinessUnitName(), font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				outerTbl.addCell(getCell(branch.getAddress().getCompleteAddress(), font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}else{
				outerTbl.addCell(getCell("M/s "+comp.getBusinessUnitName(), font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("Name and address of Establishment in/under which contract is carried on :", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(project!=null){
			outerTbl.addCell(getCell(project.getProjectName()+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("Nature and location of work :", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(project!=null&&project.getNatureOfWork()!=null){
			outerTbl.addCell(getCell(project.getNatureOfWork()+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("Name and address of Principle Employer :", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(customer!=null){
			String m_s="";
			if(customer.isCompany().equals(true)){
				m_s="M/s ";
			}
			outerTbl.addCell(getCell(m_s+customer.getFullname(), font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			outerTbl.addCell(getCell(customer.getAdress().getCompleteAddress(), font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell("1. Name of the Workman ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(employee!=null&&employee.getFullName()!=null){
			outerTbl.addCell(getCell(employee.getFullName()+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("2. Serial No. in the Register of workman employed ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(employee!=null&&employee.getCount()!=0){
			outerTbl.addCell(getCell(employee.getCount()+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("3. Nature of employment/Designation ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(employee!=null&&employee.getDesignation()!=null){
			outerTbl.addCell(getCell(employee.getDesignation()+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("4. Wage rate (with particulars of unit in case of piece-work) ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(employee!=null&&employee.getSalaryAmt()!=0){
			outerTbl.addCell(getCell(employee.getSalaryAmt()+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("5. Wage period ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell("6. Tenure of employment ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		if(employee!=null&&employee.getJoinedAt()!=null){
			outerTbl.addCell(getCell(fmt.format(employee.getJoinedAt())+" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}else{
			outerTbl.addCell(getCell("   ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		outerTbl.addCell(getCell("7. Remarks ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		outerTbl.addCell(getCell(" ", font9, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		outerTbl.addCell(getCell("Signature of Contractor ", font9, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		
		
		try {
			document.add(outerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Date : 30-10-2018 BY ANIL
	 * @param phName-Cell Value
	 * @param font-font size
	 * @param horizontalAlignment
	 * @param border-default-1,for no border-0
	 * @param rowspan-default-1
	 * @param colspan-default-1
	 * @param fixedHeight-default-0
	 * @return
	 */
	
	public PdfPCell getCell(String phName,Font font,int horizontalAlignment,int rowspan,int colspan,int fixedHeight){
		/**
		 * Updated By: Viraj
		 * Date: 04-02-2019
		 * Description: To check if the value is null and print blank.
		 */
		Phrase phrase;
		if(phName != null) {
			phrase = new Phrase(phName,font);
		}else {
			phrase = new Phrase("",font);
		}
		/** Ends **/
		PdfPCell cell=new PdfPCell(phrase);
		cell.setHorizontalAlignment(horizontalAlignment);
		if(fixedHeight!=0){
			cell.setFixedHeight(fixedHeight);
		}
		if(rowspan!=0){
			cell.setRowspan(rowspan);
		}if(colspan!=0){
			cell.setColspan(colspan);
		}
		return cell;
	}
	
	public PdfPCell getEmployeePhotoCell(){
		DocumentUpload docu =employee.getPhoto();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		try {
			logger.log(Level.SEVERE,"getEmployeePhotoCell employee.getPhoto() hostUrl="+hostUrl+" doc url="+docu.getUrl());
			
			Image image2 = Image.getInstance(new URL(hostUrl+docu.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);
			
			image2.setScaleToFitLineWhenOverflow(true);
			image2.setScaleToFitHeight(true);
//			image2.set
			
			imageSignCell = new PdfPCell(image2,true);
//			imageSignCell.addElement(image2);
//			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
			imageSignCell.setRowspan(5);
		} catch (Exception e) {
			e.printStackTrace();
			imageSignCell = new PdfPCell();
//			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
			imageSignCell.setRowspan(5);
		}
		return imageSignCell;
	}
	
	public  void createCompanyAddress()
	{
		
		DocumentUpload docu =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		try {
			logger.log(Level.SEVERE,"company logo hostUrl="+hostUrl+" doc url="+docu.getUrl());
			Image image2 = Image.getInstance(new URL(hostUrl+docu.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setFixedHeight(20);
			imageSignCell.setRowspan(3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setRowspan(2);
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{30,70});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		if (imageSignCell != null) {
			logoTable.addCell(imageSignCell).setBorder(0);
			logoTable.addCell(getCell("Employment Card", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("Form XIV[See Rule 76]", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		} else {
			logoTable.addCell(blankCell).setBorder(0);
			logoTable.addCell(getCell("Employment Card", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("Form XIV[See Rule 76]", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
			logoTable.addCell(getCell("", font7bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		}
		
		Phrase companyName= new Phrase(comp.getBusinessUnitName(),font8bold);
	    PdfPCell companyNameCell=new PdfPCell(companyName);
	    companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	    companyNameCell.setBorder(0);
	     
		String addressline1="Corp. Off.: ";
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=addressline1+comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2();
		}else{
			addressline1=addressline1+comp.getAddress().getAddrLine1();
		}
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getLandmark()+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getLandmark()+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			locality= new Phrase(addressline1+", "+comp.getAddress().getCity()+", "
				      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry(),font7);
		}
		
		PdfPCell addresscell=new PdfPCell(locality);
		addresscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		addresscell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
//		companyDetails.setSpacingAfter(5f);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(logoTable);
		comapnyCell.setBorder(0);
		PdfPCell titlePdfCell=new PdfPCell(companyDetails);
		titlePdfCell.setBorderWidthTop(0);
		titlePdfCell.setBorderWidthLeft(0);
		titlePdfCell.setBorderWidthRight(0);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
