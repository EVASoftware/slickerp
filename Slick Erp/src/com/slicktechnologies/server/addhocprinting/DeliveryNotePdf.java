package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.deliverynote.DeliveryLineItems;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class DeliveryNotePdf {
	

/**
 * Loggers
 */
	Logger logger=Logger.getLogger("DeliveryNotePdf.class");
	
DeliveryNote delnoteEntity;
List<DeliveryLineItems> delLineItems;
List<ProductOtherCharges> billingTaxesLis;
List<ProductOtherCharges> billingChargesLis;
Customer cust;
SalesOrder salesOrderEntity;
Company comp;
public  Document document;
List<ArticleType> articletype;
Phrase chunk;
boolean upcflag=false;
ProcessConfiguration processConfig;
boolean CompanyNameLogoflag=false;
int firstBreakPoint = 18;
int flag=0;
int count=0;
float blankLines;
int rohan=123;
float[] columnWidths = {0.7f, 3.5f, 0.5f, 0.5f,0.7f,0.7f,0.9f,1.0f};
double total=0;
private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font6bold,font7,font7bold,font12,font16bold,font9bold,font10,font10bold,font14bold,font14,font9;
private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
DecimalFormat df=new DecimalFormat("0.00");

Phrase ph=new Phrase(" ",font10);

List<State> stateList;

float[] columnMoreLeftWidths = {2f,1f};
float[] columnMoreLeftHeaderWidths = {1.7f,1.3f};

float[] columnMoreRightWidths = {0.8f,2.2f};
float[] columnMoreRightCheckBoxWidths = {0.3f,2.7f};
float[] columnHalfWidth = {1f,1f};
float[] columnHalfInnerWidth = {0.65f,1f};

float[] columnCollonWidth = {1.8f,0.2f,7.5f};
float[] columnCollonGSTWidth = {0.8f,0.2f,1.3f};
float[] columnStateCodeCollonWidth = {35,5,15,45};

float[] columnDateCollonWidth = {1.5f,0.2f,1.2f};

float[] column6RowCollonWidth = {0.5f,0.2f,1.5f,0.8f,0.2f,0.5f};
float[] column16CollonWidth = {0.1f,0.4f,0.2f,0.15f,0.15f,0.3f,0.3f,0.15f,0.25f,0.15f,0.25f,0.15f,0.25f,0.15f,0.25f,0.3f};
float[] column13CollonWidth = {0.1f,//Sr No
						       0.4f,//Services
						       0.4f,//HSN ACS
						       0.15f,//UOM
						       0.15f,//Qty
						       0.2f,//Rate
						       0.2f,//Amount
						       0.15f,//Disc
						       0.2f,//Taxable amt
						       0.15f,//CGST
						       0.15f,//SGST
						       0.15f,//IGST
						       0.15f};//Total

/**
 * Rohan added this for Universal pest for printing  	
 */
Boolean multipleCompanyName = false;
/**
 * ends here 
 */
public DeliveryNotePdf() {
	font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
	font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
	new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
	font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
	font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
	font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
	font8= new Font(Font.FontFamily.HELVETICA,8);
	font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
	font12=new Font(Font.FontFamily.HELVETICA,12);
	font10=new Font(Font.FontFamily.HELVETICA,10);
	font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
	font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
	font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
	font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
	font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	font7 = new Font(Font.FontFamily.HELVETICA, 7);
}


public void setDeliveryNote(long count)
{
	//Load Invoice
		delnoteEntity=ofy().load().type(DeliveryNote.class).id(count).now();
		
		
		stateList=ofy().load().type(State.class).filter("companyId", delnoteEntity.getCompanyId()).list();
		System.out.println("stateList size"+stateList.size());
	//Load Customer
	if(delnoteEntity.getCompanyId()==null)
		cust=ofy().load().type(Customer.class).filter("count",delnoteEntity.getCinfo().getCount()).first().now();
	else
		cust=ofy().load().type(Customer.class).filter("count",delnoteEntity.getCinfo().getCount()).filter("companyId", delnoteEntity.getCompanyId()).first().now();
		
	
	if(delnoteEntity.getCompanyId()!=null){
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", delnoteEntity.getCompanyId()).filter("processName", "DeliveryNote").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					CompanyNameLogoflag=true;
				}
			}
		}
	}
	
	if(delnoteEntity.getCompanyId()!=null){
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", delnoteEntity.getCompanyId()).filter("processName", "Quotation").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					upcflag=true;
					
				}
				
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintMultipleCompanyNamesFromInvoiceGroup")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					multipleCompanyName=true;
				}
			}
		}
	}
	
	
	//Load Company
	if(delnoteEntity.getCompanyId()==null)
	   comp=ofy().load().type(Company.class).first().now();
	else
	   comp=ofy().load().type(Company.class).filter("companyId",delnoteEntity.getCompanyId()).first().now();
	
	//Load Sales Order
	if(delnoteEntity.getCompanyId()==null)
		salesOrderEntity=ofy().load().type(SalesOrder.class).filter("count", delnoteEntity.getSalesOrderCount()).first().now();
	else
		salesOrderEntity=ofy().load().type(SalesOrder.class).filter("count", delnoteEntity.getSalesOrderCount()).filter("companyId",delnoteEntity.getCompanyId()).first().now();
	
//	if(invoiceentity.getCompanyId()==null)
//		contractEntity=ofy().load().type(SalesOrder.class).filter("count", invoiceentity.getContractCount()).first().now();
//	else
//		contractEntity=ofy().load().type(SalesOrder.class).filter("count", invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
	
	delLineItems=delnoteEntity.getDeliveryItems();
	billingTaxesLis=delnoteEntity.getProdTaxesLis();
	billingChargesLis=delnoteEntity.getProdChargeLis();
	articletype = new ArrayList<ArticleType>();
	if(cust.getArticleTypeDetails().size()!=0){
		articletype.addAll(cust.getArticleTypeDetails());
	}
	if(comp.getArticleTypeDetails().size()!=0){
		articletype.addAll(comp.getArticleTypeDetails());
	}
	
	System.out.println("RRRRRRRRRRRRRRRRRRRRRRRR "+articletype.size());
	
//	delnoteEntity=getShippingAddress();
	
	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	
}

	public  void createPdf(String preprintStatus) {
////		Createblank();
////		if(CompanyNameLogoflag==true){
////			createLogo(document,comp);
////		}
////		else{
////			createLogo123(document,comp);
////		}
//		
//		if(preprintStatus.equals("plane")){
//			createBlankHeading();
//			createHeadingInfo();
//			}else{
//				
//				if(preprintStatus.equals("yes")){
//					
//					System.out.println("inside prit yes");
//					createBlankforUPC();
//					createTitle();
//				}else if(preprintStatus.equals("no")){
//				System.out.println("inside prit no");
//				
//				if(comp.getUploadHeader()!=null){
//				createCompanyNameAsHeader(document,comp);
//				}
//				
//				if(comp.getUploadFooter()!=null){
//				createCompanyNameAsFooter(document,comp);
//				}
//				createBlankforUPC();
//				createTitle();
//				}
//				
//			}
//		
//		
//		
//		if(upcflag==true){
//			createCompanyAddressDetails();
//			}
//		
////		createHeadingInfo();
//	//	createProductInfo();
//		createProductTable();
//		termsConditionsInfo();
//		footerInfo();
		
		
		
//		sdf = new SimpleDateFormat("dd MMM yyyy");
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		for(int i=0;i<3;i++)
		{
		if(upcflag==false && preprintStatus.equals("plane")){
//			createLogo(document,comp);//Date 23/11/2017 Comment by jayshree to remove logo method
			createHeader(i);
//			createCompanyAddress();
		}else{
			
			if(preprintStatus.equals("yes")){
				System.out.println("inside prit yes");
				createBlankforUPC(i);
			}
			if(preprintStatus.equals("no")){
			    if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
				createBlankforUPC(i);
			}
		}
		
		createDeliveryNoteDetails();
		createCustomerDetails();
		createProductDetails();
		createProductDetailsVal();
		createFooterTaxPart();
		createFooterLastPart(preprintStatus);
		
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	}
	

private void createCompanyAddressDetails() {
		
	//************************changes made by rohan	for M/s **************** 
//	String tosir= null;
//	if(cust.isCompany()==true){
//		
//		
//	 tosir="To, M/S";
//	}
//	else{
//		tosir="To,";
//	}
//	//*******************changes ends here ********************sss*************
//	
//	
//	String custName="";
//	if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//		custName=cust.getCompanyName().trim();
//	}
//	else{
//		custName=cust.getFullname().trim();
//	}
	
	
	String tosir= null;
	String custName="";
	/**
	 * OLD CODE
	 */
//	//   rohan modified this code for printing printable name 
//	
//	if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
//	{
//		custName=cust.getCustPrintableName().trim();
//	}
//	else
//	{
//	
//		if(cust.isCompany()==true){
//			
//			
//		 tosir="To, M/S";
//		}
//		else{
//			tosir="To,";
//		}
//		
//		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//			custName=cust.getCompanyName().trim();
//		}
//		else{
//			custName=cust.getFullname().trim();
//		}
//	}
	//Date 21/11/2017
	//Dev. By jayshree
	//Des. Changes are made to add the salutation in customer name
	if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals("")){
		custName=cust.getCustPrintableName().trim();
	}
	else{
		if(cust.isCompany()==true){
		 tosir="To, M/S  ";
		}else{
			tosir="To,";
		}
		
		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
			custName=cust.getCompanyName().trim();
		}else if(cust.getSalutation()!=null){
			custName=cust.getSalutation()+"  "+cust.getFullname().trim();
		}else{
			custName=cust.getFullname().trim();
		}
	}
	//End By Jayshree
	
    Paragraph fullname =new Paragraph();
    fullname.add(Chunk.NEWLINE);
    if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
   	{
       	fullname.add(custName);
   	}
       else
       {
       	fullname.add(tosir+"   "+custName);
       }
    fullname.setFont(font10bold);
    
		PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);
	
	
	 String addresslin1=" ";
	  if(!cust.getAdress().getAddrLine1().equals("")){
		  addresslin1=cust.getAdress().getAddrLine1().trim()+" , ";
	  }
	  
	  String addressline2=" ";
	  
	  if(!cust.getAdress().getAddrLine2().equals("")){
		  addressline2 =cust.getAdress().getAddrLine2().trim()+" , ";
	  }
	 
	  String landmark=" ";
	  if(!cust.getAdress().getLandmark().equals("")){
		  landmark=cust.getAdress().getLandmark()+" , ";
	  }
	  
	  String locality=" ";
	  if(!cust.getAdress().getLocality().equals("")){
		  locality=cust.getAdress().getLocality()+" , ";
	  }
	  
//	  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality,font9);
//	  PdfPCell fromaddrescell1=new PdfPCell();
//	  fromaddrescell1.addElement(addressline1);
//	  fromaddrescell1.setBorder(0);
	  
	  
	  String city=" ";
	  if(!cust.getAdress().getCity().equals("")){
		  city=cust.getAdress().getCity()+",";
	  }
	  String state=" ";
	  if(!cust.getAdress().getState().equals("")){
		  state=cust.getAdress().getState()+"-";
	  }
	  String pin=" ";
	  if(cust.getAdress().getPin()!=0){
		  pin=cust.getAdress().getPin()+",";
	  }
	  String country=" ";
	  if(cust.getAdress().getCountry()!=null){
		  country=cust.getAdress().getCountry();
	  }
	  
	  
	  
	  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality+city+state+pin+country,font9);
	  PdfPCell fromaddrescell1=new PdfPCell();
	  fromaddrescell1.addElement(addressline1);
	  fromaddrescell1.setBorder(0);
	  
//	  Phrase blank=new Phrase(" ",font9);
//	  PdfPCell blankCell=new PdfPCell();
//	  fromaddrescell1.addElement(addressline1);
//	  fromaddrescell1.setBorder(0);
	  
	  Phrase namePhrase= new Phrase("Customer Details",font10bold); 
	  PdfPCell headcell = new PdfPCell(namePhrase);
	  headcell.setBorder(0);
	  headcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  
	  //*******************rohan added cust details *****************
	  Phrase email= new Phrase("Email :"+cust.getEmail(),font9); 
	  PdfPCell emailCell = new PdfPCell(email);
	  emailCell.setBorder(0);
	  emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  
	  Phrase cellNo= new Phrase("Cell : "+cust.getCellNumber1(),font9); 
	  PdfPCell cellNoCell = new PdfPCell(cellNo);
	  cellNoCell.setBorder(0);
	  cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  
	  //***********************changes ends here *********************
	  
	  PdfPTable addtable = new PdfPTable(1);
	  addtable.setWidthPercentage(100f);
	  addtable.addCell(headcell);
	  addtable.addCell(custnamecell);
	  addtable.addCell(fromaddrescell1);
	  addtable.addCell(emailCell);
	  addtable.addCell(cellNoCell);
	  
	  PdfPCell tablecell = new PdfPCell();
	  tablecell.addElement(addtable);
	  
	  
	  PdfPTable parentaddtable = new PdfPTable(1);
	  parentaddtable.setWidthPercentage(100f);
	  parentaddtable.addCell(tablecell);
	  
	  try {
		document.add(parentaddtable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	}

	

	public  void createBlankHeading() {
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


private void createCompanyNameAsHeader(Document doc, Company comp) {
	
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}
	
	
	public  void createDeliveryNotePdfForEmail(DeliveryNote delNote,Company compEntity,Customer cust) {
		this.delnoteEntity=delNote;
		this.comp=compEntity;
		this.cust=cust;
		
		this.delLineItems=delnoteEntity.getDeliveryItems();
		this.billingTaxesLis=delnoteEntity.getProdTaxesLis();
		this.billingChargesLis=delnoteEntity.getProdChargeLis();
		this.articletype = new ArrayList<ArticleType>();
		if(cust.getArticleTypeDetails().size()!=0){
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if(comp.getArticleTypeDetails().size()!=0){
			articletype.addAll(comp.getArticleTypeDetails());
		}
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(delnoteEntity.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", delnoteEntity.getCompanyId()).filter("processName", "DeliveryNote").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.CompanyNameLogoflag=true;
					}
				}
			}
		}
		
		Createblank();
		if(CompanyNameLogoflag==true){
//			createLogo(document,comp);//Date 23/11/2017 Comment by jayshree to remove logo method
		}
		else{
//			createLogo123(document,comp);//Date 23/11/2017 Comment by jayshree to remove logo method
		}
		createHeadingInfo();
//		createProductInfo();
		createProductTable();
		termsConditionsInfo();
		footerInfo();
	}

private void Createblank() {
	
    Paragraph blank =new Paragraph();
    blank.add(Chunk.NEWLINE);
    try {
		document.add(blank);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	
}


private void createLogo(Document doc, Company comp) {
	
	
	//********************logo for server ********************
	
DocumentUpload document =comp.getLogo();

//patch
String hostUrl; 
String environment = System.getProperty("com.google.appengine.runtime.environment");
if (environment.equals("Production")) {
    String applicationId = System.getProperty("com.google.appengine.application.id");
    String version = System.getProperty("com.google.appengine.application.version");
    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
} else {
    hostUrl = "http://localhost:8888";
}

try {
	Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
	image2.scalePercent(15f);
	image2.scaleAbsoluteWidth(250f);
	image2.setAbsolutePosition(40f,745f);	
	doc.add(image2);
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}



//***********************for local **************************

//try
//{
//Image image1=Image.getInstance("images/logo1.png");
//
//image1.scalePercent(15f);
//image1.scaleAbsoluteWidth(250f);
//image1.setAbsolutePosition(35f,725f);	
//doc.add(image1);
//}
//catch(Exception e)
//{
//	e.printStackTrace();
//}
//
}



private void createLogo123(Document doc, Company comp) {
		

//		********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,745f);	
		doc.add(image2);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,745f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
//	
	}


public void createHeadingInfo()
{	
	//Date 21/11/2017 By Jayshree
		//Dev.By Jayshree
		//Des.to add the logo in Table
		DocumentUpload logodocument =comp.getLogo();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		Image image1=null;
//		try
//		{
//		 image1=Image.getInstance(" images/ipclogo4.jpg");//images/ipclogo4.jpg
//		image1.scalePercent(20f);
////		image1.setAbsolutePosition(40f,765f);	
////		doc.add(image1);
	//	
	//	
	//	
	//	
	//	
//		imageSignCell=new PdfPCell();
//		imageSignCell.addElement(image1);
//		imageSignCell.setFixedHeight(20);
//		imageSignCell.setBorder(0);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}


	PdfPTable logotab =new PdfPTable(1);
	logotab.setWidthPercentage(100);



	if(imageSignCell!=null)
	{
	logotab.addCell(imageSignCell);
	}
	else
	{
	Phrase logoph=new Phrase(" ");
	PdfPCell logoBlank=new PdfPCell(logoph);
	logoBlank.setBorder(0);
	logotab.addCell(logoBlank);
	}
	//complete logotab
	
	Paragraph p =new Paragraph();
	Phrase blankspace=null;
	Phrase companyName=null;
	
	if(CompanyNameLogoflag==true){
		blankspace = new Phrase(" ",font12);
		
		p.add(blankspace);
		}
	else {
		
		companyName = new Phrase("                  "+comp.getBusinessUnitName().toUpperCase(),font12bold);
	}

p.add(Chunk.NEWLINE);
//    p.add(companyName);
    
    PdfPCell companyHeadingCell=new PdfPCell();
    companyHeadingCell.setBorder(0);
    companyHeadingCell.addElement(p);
    if(CompanyNameLogoflag==true){
    	companyHeadingCell.addElement(blankspace);
    	companyHeadingCell.addElement(blankspace);
    	}else {
    		companyHeadingCell.addElement(companyName);
    	}
    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
	Phrase adressline2=null;
	if(comp.getAddress().getAddrLine2()!=null)
	{
		adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
	}
	
	
	Phrase landmark=null;
	Phrase locality=null;
	Phrase city=null;
	if(comp.getAddress().getLandmark()!=null)
	{
		
		 landmark=new Phrase(comp.getAddress().getLandmark(),font10);
	}
	else
	{
		landmark= new Phrase("",font12);
	}
	if(comp.getAddress().getLocality()!=null){
		locality=new Phrase(comp.getAddress().getLocality(),font10);
		
	}else{
		locality=new Phrase("",font10);
	}
	
		city=new Phrase(comp.getAddress().getCity()+" - "+comp.getAddress().getPin(),font10);
	
		PdfPCell citycell= new PdfPCell();
	citycell.addElement(city);
	citycell.setBorder(0);
	PdfPCell addressline1cell=new PdfPCell();
	PdfPCell addressline2cell=new PdfPCell();
	addressline1cell.addElement(adressline1);
	addressline1cell.setBorder(0);
	if(adressline2!=null)
	{
		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);
	}
	
	PdfPCell landmarkcell=new PdfPCell();
	landmarkcell.addElement(landmark);
	landmarkcell.setBorder(0);
	
	PdfPCell localitycell=new PdfPCell();
	localitycell.addElement(locality);
	localitycell.setBorder(0);
	
	String contactinfo="Cell: "+comp.getContact().get(0).getCellNo1();
	if(comp.getContact().get(0).getCellNo2()!=0)
		contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
	if(comp.getContact().get(0).getLandline()!=0){
		contactinfo=contactinfo+"     "+" Tel: "+comp.getContact().get(0).getLandline();
	}
	
	Phrase contactnos=new Phrase(contactinfo,font9);
	Phrase email= new Phrase("Email: "+comp.getContact().get(0).getEmail(),font9);
	
	PdfPCell contactcell=new PdfPCell();
	contactcell.addElement(contactnos);
	contactcell.setBorder(0);
	PdfPCell emailcell=new PdfPCell();
	emailcell.setBorder(0);
	emailcell.addElement(email);
	
	
	PdfPTable companytable=new PdfPTable(1);
	companytable.addCell(companyHeadingCell);
	companytable.addCell(addressline1cell);
	if(!comp.getAddress().getAddrLine2().equals(""))
	{
		companytable.addCell(addressline2cell);
	}
	if(!comp.getAddress().getLandmark().equals("")){
		companytable.addCell(landmarkcell);
	}
	if(!comp.getAddress().getLocality().equals("")){
		companytable.addCell(localitycell);
	}
	
	companytable.addCell(citycell);
	companytable.addCell(contactcell);
	companytable.addCell(emailcell);
	companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
	companytable.setWidthPercentage(100);

	
	//Date 21/11/2017
		//By Jayshree 
		//To add logo tab
		PdfPTable header =new PdfPTable(2);
		header.setWidthPercentage(100);
		
		
		try {
			header.setWidths(new float[]{20,80});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(imageSignCell!=null){
		PdfPCell left=new PdfPCell(logotab);
		left.setBorder(0);
		header.addCell(left);
		
		PdfPCell right=new PdfPCell(companytable);
		right.setBorder(0);
		header.addCell(right);
		}
		else
		{
			PdfPCell right=new PdfPCell(companytable);
			right.setBorder(0);
			right.setColspan(2);
			header.addCell(right);
		}
		try {
			document.add(header);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//End by jayshree
	
	/**
	 * Customer Info
	 */
//	String tosir="To, M/S";
	
	
	//************************changes made by rohan	for M/s **************** 
//		String tosir= null;
//		if(cust.isCompany()==true){
//			
//			
//		 tosir="To, M/S";
//		}
//		else{
//			tosir="To,";
//		}
//		//*******************changes ends here ********************sss*************
//	
//	
//	
////	Phrase tosirphrase= new Phrase(tosir,font12bold);
////	Paragraph tosirpara= new Paragraph();
////	tosirpara.add(tosirphrase);
//	
//	String custName="";
//	
//	if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//		custName=cust.getCompanyName().trim();
//	}
//	else{
//		custName=cust.getFullname().trim();
//	}
	
	
	String tosir= null;
	String custName="";
	/**
	 * Old COde
	 */
//	//   rohan modified this code for printing printable name 
//	
//	if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
//	{
//		custName=cust.getCustPrintableName().trim();
//	}
//	else
//	{
//	
//		if(cust.isCompany()==true){
//			
//			
//		 tosir="To, M/S";
//		}
//		else{
//			tosir="To,";
//		}
//		
//		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//			custName=cust.getCompanyName().trim();
//		}
//		else{
//			custName=cust.getFullname().trim();
//		}
//	}
	
		//Date 21/11/2017
		//Dev.By jayshree 
		//Des.to add the salutation changes are done
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
			if(cust.isCompany()==true){
				
				
			 tosir="To, M/S  ";
			}
			else{
				tosir="To, ";
			}
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else if(cust.getSalutation()!=null)
			{
				custName=cust.getSalutation()+"  "+cust.getFullname().trim();
			}
			else
			{
				custName= cust.getFullname();
			}
		}
	//End by jayshree
	
	/**********tosir**********/
	
//	Phrase tosirphrase= new Phrase(tosir,font12bold);
//	Paragraph tosirpara= new Paragraph();
//	tosirpara.add(tosirphrase);
//	PdfPCell custnamecell1=new PdfPCell();
//	custnamecell1.addElement(tosirpara);
//		custnamecell1.setBorder(0);
	
	
	
//	Phrase customername= new Phrase(custName,font12bold);
    Paragraph fullname =new Paragraph();
    fullname.add(Chunk.NEWLINE);
    fullname.setFont(font12bold);
    if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
   	{
       	fullname.add(custName);
   	}
       else
       {
       	fullname.add(tosir+"   "+custName);
       }
    
		PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);
		 
		Phrase customeradress= new Phrase("                   "+delnoteEntity.getShippingAddress().getAddrLine1(),font10);
	Phrase customeradress2=null;
	if(delnoteEntity.getShippingAddress().getAddrLine2()!=null){
	   customeradress2= new Phrase("                   "+delnoteEntity.getShippingAddress().getAddrLine2(),font10);
	}
	
	PdfPCell custaddress1=new PdfPCell();
	custaddress1.addElement(customeradress);
	custaddress1.setBorder(0);
	PdfPCell custaddress2=new PdfPCell();
	if(delnoteEntity.getShippingAddress().getAddrLine2()!=null){
		custaddress2.addElement(customeradress2);
		custaddress2.setBorder(0);
		}
	
	
//	Phrase custlandmark=null;
	String custlandmark="";
	Phrase custlocality= null;
	String custlocality1="";
	Phrase custlocalityphrase= null;
	if(!delnoteEntity.getShippingAddress().getLandmark().equals(""))
	{
		custlandmark = delnoteEntity.getShippingAddress().getLandmark();
		custlocality=new Phrase("                   "+custlandmark,font10);
	}
	else{
		custlocality=new Phrase("",font10);
	}
	if(!delnoteEntity.getShippingAddress().getLocality().equals("")){
		custlocality1 = delnoteEntity.getShippingAddress().getLocality();
		custlocalityphrase=new Phrase("                   "+custlocality1,font10);
	}
	
		
	
	Phrase cityState=new Phrase("                   "+delnoteEntity.getShippingAddress().getCity()+" - "+delnoteEntity.getShippingAddress().getPin(),font10);
	
	
	
	
	
	PdfPCell custlocalitycell1=new PdfPCell();
	custlocalitycell1.addElement(custlocalityphrase);
	custlocalitycell1.setBorder(0);
	
	PdfPCell custlocalitycell=new PdfPCell();
	custlocalitycell.addElement(custlocality);
	custlocalitycell.setBorder(0);
	PdfPCell custcitycell=new PdfPCell();
	custcitycell.addElement(cityState);
	custcitycell.setBorder(0); 
//	custcitycell.addElement(Chunk.NEWLINE);
		 
	Phrase custcontact=new Phrase("Cell: "+cust.getCellNumber1()+"",font9);
		PdfPCell custcontactcell=new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);
		
		Phrase custemail=new Phrase("Email: "+cust.getEmail(),font9);
		PdfPCell custemailcell=new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);
		
		PdfPTable custtable=new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(custaddress1);
		if(!delnoteEntity.getShippingAddress().getAddrLine2().equals("")){
			custtable.addCell(custaddress2);
		}
		if(!delnoteEntity.getShippingAddress().getLandmark().equals("")){
			custtable.addCell(custlocalitycell);
		}
		if(!delnoteEntity.getShippingAddress().getLocality().equals("")){
			custtable.addCell(custlocalitycell1);
		}
		custtable.addCell(custcitycell);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);
		
		// if company is true then the name of contact person
		
		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
			Phrase realcontact=new Phrase("Contact: "+cust.getFullname(),font10);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell=new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}
		 
		
		PdfPTable headparenttable= new PdfPTable(2);
	headparenttable.setWidthPercentage(100);
	try {
		headparenttable.setWidths(new float[]{50,50});
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	PdfPCell  companyinfocell = new PdfPCell();
	PdfPCell custinfocell = new PdfPCell();
	
	
	companyinfocell.addElement(companytable);
	custinfocell.addElement(custtable);
	headparenttable.addCell(companyinfocell);
	headparenttable.addCell(custinfocell);

//	
//	String title="Delivery Note";
//	
//	String countinfo="ID : "+delnoteEntity.getCount()+"";
//	
//	String creationdateinfo="Date: "+fmt.format(delnoteEntity.getDeliveryDate());
//	
//	Phrase titlephrase =new Phrase(title,font14bold);
//	Paragraph titlepdfpara=new Paragraph();
//	titlepdfpara.add(titlephrase);
//	titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//	titlepdfpara.add(Chunk.NEWLINE);
//	PdfPCell titlepdfcell=new PdfPCell();
//	titlepdfcell.addElement(titlepdfpara);
//	titlepdfcell.setBorderWidthRight(0);
//	titlepdfcell.setBorderWidthLeft(0);
//	titlepdfcell.addElement(Chunk.NEWLINE);
//	
//	Phrase idphrase=new Phrase(countinfo,font14);
//	Paragraph idofpdfpara=new Paragraph();
//	idofpdfpara.add(idphrase);
//	idofpdfpara.setAlignment(Element.ALIGN_LEFT);
//	idofpdfpara.add(Chunk.NEWLINE);
//	PdfPCell countcell=new PdfPCell();
//	countcell.addElement(idofpdfpara);
//	countcell.setBorderWidthRight(0);
//	countcell.addElement(Chunk.NEWLINE);
//	
//	Phrase dateofpdf=new Phrase(creationdateinfo,font14);
//	Paragraph creatndatepara=new Paragraph();
//	creatndatepara.add(dateofpdf);
//	creatndatepara.setAlignment(Element.ALIGN_RIGHT);
//	creatndatepara.add(Chunk.NEWLINE);
//	PdfPCell creationcell=new PdfPCell();
//	creationcell.addElement(creatndatepara);
//	creationcell.setBorderWidthLeft(0);
//	creationcell.addElement(Chunk.NEWLINE);
//	
//	PdfPTable titlepdftable=new PdfPTable(3);
//	titlepdftable.setWidthPercentage(100);
//	titlepdftable.addCell(countcell);
//	titlepdftable.addCell(titlepdfcell);
//	titlepdftable.addCell(creationcell);
	
	try {
		document.add(headparenttable);
//		document.add(titlepdftable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	}
//


	public void createTitle()
	{
		String title="Delivery Note";
		
		String countinfo="ID : "+delnoteEntity.getCount()+"";
		
		String creationdateinfo="Date: "+fmt.format(delnoteEntity.getDeliveryDate());
		
		Phrase titlephrase =new Phrase(title,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell=new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);
		
		Phrase idphrase=new Phrase(countinfo,font14);
		Paragraph idofpdfpara=new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell=new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);
		
		Phrase dateofpdf=new Phrase(creationdateinfo,font14);
		Paragraph creatndatepara=new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell=new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);
		
		PdfPCell titlepdftableCell = new PdfPCell(titlepdftable);
		titlepdftableCell.setBorder(0);
		

		Phrase saleOrderid = new Phrase("Order Id : "+salesOrderEntity.getCount(),font8);
		
		PdfPCell saleOrderidCell =new PdfPCell(saleOrderid);
		saleOrderidCell.setBorderWidthBottom(0);
		saleOrderidCell.setBorderWidthTop(0);
		saleOrderidCell.setBorderWidthLeft(0);
		
		
		saleOrderidCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase blank= new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorderWidthBottom(0);
		blankCell.setBorderWidthTop(0);
		blankCell.setBorderWidthRight(0);
		
		PdfPTable table= new PdfPTable(2);
		table.addCell(blankCell);
		table.addCell(saleOrderidCell);

		PdfPCell tableCell =new  PdfPCell(table);
		tableCell.setBorder(0);
		
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		
		parentTable.addCell(titlepdftableCell);
		parentTable.addCell(tableCell);
		
		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


 public  void createProductTable()
  {
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
		Phrase productdetails= new Phrase("Product Details",font1);
		Paragraph para = new Paragraph();
		para.add(Chunk.NEWLINE);
		para.add(Chunk.NEWLINE);
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		para.add(Chunk.NEWLINE);
			
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100);
		
		Phrase category = new Phrase("SR. NO. ",font1);
//		Phrase productcode = new Phrase("ITEM DETAILS",font1);
        Phrase productname = new Phrase("ITEM DETAILS",font1);
        Phrase qty = new Phrase("QTY",font1);
        Phrase unit = new Phrase("UNIT",font1);
        Phrase rate= new Phrase ("RATE",font1);
        
        Phrase percDisc = new Phrase("DISC %",font1);
        Phrase adddisc = new Phrase("ADDL.DISC %",font1);
        
        Phrase servicetax = new Phrase("TAX",font1);
//        Phrase vat = new Phrase("VAT",font1);
        
        /////
        
        Paragraph tax= new Paragraph();
        //*************chnges mukesh on 24/4/2015******************** 
//         SalesQuotation salesQu=null;
//         if(qp instanceof SalesQuotation){
//         	salesQu=(SalesQuotation)qp;
//         }
         Phrase svat=null;
         Phrase vvat=null;
         Phrase vat=null;
         int cout=0;
         int flag21=0;
         int st=0;
         int st1=0;
//         for(int i=0;i<this.prodTaxes.size();i++){
//         	
//         }
         
         
       
         for(int i=0;i<this.delLineItems.size();i++){
         	 
         	int cstFlag=0;
         	if(delnoteEntity instanceof DeliveryNote){
         		DeliveryNote salesq=(DeliveryNote)delnoteEntity;
         		
         		if(salesq.getCformStatus()!=null){
         		
         		if(salesq.getCformStatus().trim().equals(AppConstants.YES)||salesq.getCformStatus().trim().equals(AppConstants.NO))
         		{
         			cstFlag=1;
         		}
         		}
         	}
         	
         	
//         	Phrase vatphrase=new Phrase("VAT",font1);
//              Phrase Cstphrase=new Phrase("CST",font1);
//              Phrase Stphrase=new Phrase("ST",font1);
         	
         	
              
              if(delLineItems.get(i).getServiceTax().getPercentage()>0&&delLineItems.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
             	 vvat = new Phrase("VAT / ST %",font1);
             	 cout=1;
              }
             else if(delLineItems.get(i).getServiceTax().getPercentage()>0&&delLineItems.get(i).getVatTax().getPercentage()==0&&cstFlag==0){
          	    vat = new Phrase("ST %",font1);
          	  st=1;
             }
             else if(delLineItems.get(i).getServiceTax().getPercentage()==0&&delLineItems.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
             	 vat = new Phrase("VAT %",font1);
             	 st1=1;
             }else if(cstFlag==1 &&delLineItems.get(i).getServiceTax().getPercentage()>0){
             	svat = new Phrase("CST / ST %",font1);
             	flag21=1;
             } 
             else if(cstFlag==1 &&delLineItems.get(i).getServiceTax().getPercentage()==0){
          	    vat = new Phrase("CST %",font1);
         
             }
             else{
                 	 vat = new Phrase("TAX %",font1);
                 }
              
              
         }
         Phrase stvat=null;
         if(cout>0){
         	tax.add(vvat);
         	tax.setAlignment(Element.ALIGN_CENTER);
         }else if(st==1&&st1==1){
         	
         	stvat=new Phrase("VAT / ST %",font1);
         	tax.add(stvat);
         	tax.setAlignment(Element.ALIGN_CENTER);
         }
         
         
         else if(flag21>0){
         	tax.add(svat);
         	tax.setAlignment(Element.ALIGN_CENTER);
         }else{
         tax.add(vat);
         tax.setAlignment(Element.ALIGN_CENTER);
         }
        
        
        /////
        Phrase total = new Phrase("AMOUNT",font1);
		
        
		 PdfPCell cellcategory = new PdfPCell(category);
		 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
         PdfPCell cellproductname = new PdfPCell(productname);
         cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
         PdfPCell cellqty = new PdfPCell(qty);
         cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
         PdfPCell cellunit = new PdfPCell(unit);
         cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
         PdfPCell cellrate = new PdfPCell(rate);
         cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
         PdfPCell percDisccell = new PdfPCell(percDisc);
         percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
         PdfPCell cellservicetax = new PdfPCell(tax);
         cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
         
        
         PdfPCell celltotal= new PdfPCell(total);
         celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
         
         table.addCell(cellcategory);
         table.addCell(cellproductname);
         table.addCell(cellqty);
         table.addCell(cellunit);
         table.addCell(cellrate);
         table.addCell(percDisccell);
         table.addCell(cellservicetax);
         
         table.addCell(celltotal);
         
         try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
//         ///change dummy this.products.size() to eva
//         
//       if( eva<= firstBreakPoint){
//  			int size =  firstBreakPoint - eva;
//  		    blankLines = size*(185/19);
//  		}
//  		else{
//			blankLines = 0;
//		}
//  		table.setSpacingAfter(blankLines);	 
         
         

      	 if( this.delLineItems.size()<= firstBreakPoint){
 			int size =  firstBreakPoint - this.delLineItems.size();
 			      blankLines = size*(195/18);
 			  	System.out.println("blankLines size ="+blankLines);
 			System.out.println("blankLines size ="+blankLines);
 				}
 				else{
 					blankLines = 10f;
 				}
 				table.setSpacingAfter(blankLines);	
         
 				Phrase rephrse = null;
 				
 				 int flagr=0;
         
         for(int i=0;i<this.delLineItems.size();i++)
         {
        	 Phrase chunk=new Phrase((i+1)+"",font8);
        	 PdfPCell pdfcategcell = new PdfPCell(chunk);
        	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	 
        	 
        	 chunk = new Phrase(delLineItems.get(i).getProdName(),font8);
        	 PdfPCell pdfnamecell = new PdfPCell(chunk);
        	 
        	 chunk = new Phrase(delLineItems.get(i).getQuantity()+"",font8);
        	 PdfPCell pdfqtycell = new PdfPCell(chunk);
        	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	 
        	 chunk = new Phrase(delLineItems.get(i).getUnitOfMeasurement(),font8);
        	 PdfPCell pdfunitcell = new PdfPCell(chunk);
        	 pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        	 
        	 chunk = new Phrase(df.format(delLineItems.get(i).getPrice()),font8);
        	 PdfPCell pdfspricecell = new PdfPCell(chunk);
        	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        	 
        	 if(delLineItems.get(i).getServiceTax()!=null)
         	    chunk = new Phrase(delLineItems.get(i).getServiceTax().getPercentage()+"",font8);
         	 else
         		 chunk = new Phrase("N.A"+"",font8);
         	 
         	 PdfPCell pdfservice = new PdfPCell(chunk);
         	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
//         	 if(salesProd.get(i).getVatTax()!=null)
//         	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
//         	 else
//         		 chunk = new Phrase("N.A"+"",font8);
//         	 PdfPCell pdfvattax = new PdfPCell(chunk);
         	 
         	 ////
	         PdfPCell pdfservice1=null;
        	 //******************************
        	 double cstval=0;
        	 int cstFlag=0;
        	 if(delnoteEntity instanceof DeliveryNote){
	         		DeliveryNote salesq=(DeliveryNote)delnoteEntity;
	         		
	         		if(salesq.getCformStatus()!=null){
	         		
	         		if(salesq.getCformStatus().trim().equals(AppConstants.YES)||salesq.getCformStatus().trim().equals(AppConstants.NO))
	         		{
	         			cstFlag=1;
	         			cstval=salesq.getCstPercent();
	         		}
	         		}
	         	}
        	 
        	 
        	 System.out.println("one");
        	 
        	  if((delLineItems.get(i).getVatTax().getPercentage()>0)&& (delLineItems.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
              	   
        		  System.out.println("two");
        		  flagr=1;
        		  if((delLineItems.get(i).getVatTax()!=null)&&(delLineItems.get(i).getServiceTax()!=null)){
        			  if(cout>0){
            			  chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage())+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            			  }
        		  }else{
                		
        			  chunk = new Phrase("0"+"",font8);
        		  }
        			  pdfservice1 = new PdfPCell(chunk);
           		 
           		  
           		  pdfservice1 = new PdfPCell(chunk);
                	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
        		  
        		  
        		  
        	  }
             else if((delLineItems.get(i).getServiceTax().getPercentage()>0) && (delLineItems.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
            	
            	 System.out.println("three");
            	 if(delLineItems.get(i).getServiceTax()!=null){
            		if(flag21==1||cout==1){
            				chunk = new Phrase("0.00"+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		}else if(st==1&&st1==1){
            			chunk = new Phrase("0.00"+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		}else{
            				chunk = new Phrase(df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		}
            		
            	 }else{
             		 chunk = new Phrase("0.00",font8);
             	 }
             	  pdfservice1 = new PdfPCell(chunk);
             	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             	 
             }
             else if((delLineItems.get(i).getServiceTax().getPercentage()>0) &&(delLineItems.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
            	 System.out.println("four");
            	
            	 		
            	 	chunk = new Phrase(df.format(cstval)+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		 
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
                 	 flagr=1;
            	
             }
             else if(cstFlag==1&&(delLineItems.get(i).getServiceTax().getPercentage()==0)
            		 &&(delLineItems.get(i).getVatTax().getPercentage()==0)){
            	 
            	 System.out.println("five");
            	 if(flag21==1||cout==1){
            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
            	 }else{
            		 chunk = new Phrase(df.format(cstval),font8);
            	 }
            		 
            		 
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
             } 
             else if(cstFlag==1&&delLineItems.get(i).getServiceTax().getPercentage()>0){
          	  
            	 System.out.println("six");
            	 flagr=1;
            	 chunk = new Phrase(df.format(cstval)+" / "+
            			 df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		 
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
             } 
             else if((cstFlag==1&&(delLineItems.get(i).getServiceTax().getPercentage()==0)&&(delLineItems.get(i).getVatTax().getPercentage()>0))){
            	 System.out.println("cst1..."+cstval);
            	 System.out.println("seven");
            	 if(flag21==1||cout==1){
            		 System.out.println("cst2..."+cstval);
            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
            	 }else{
            		 System.out.println("cst3..."+cstval);
            		 chunk = new Phrase(df.format(cstval),font8);
            	 }
            	 System.out.println("cst4..."+cstval);
      			  
      	  pdfservice1 = new PdfPCell(chunk);
      	  
       	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
             }else if((delLineItems.get(i).getServiceTax().getPercentage()==0)&&(delLineItems.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
            	 System.out.println("eight");
            	 if(flag21==1||cout==1){
            		 chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
            	 }else if(st==1&&st1==1){
            		 chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
            	 }else{
            		 chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage()),font8);
            	 }
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                 	 
                 	 System.out.println("service tax flagr:::::::::::::   "+flagr);
	  
             }else{
            	 if(flag21==1||cout==1){
            		 chunk= new Phrase("0.00 / 0.00",font8);
            	 }else{
            	 chunk = new Phrase("0.00",font8);
            	 }
            	 pdfservice1 = new PdfPCell(chunk);
             }
	         
	         ///
         	
         	
         	
         	
         	
         	 if(delLineItems.get(i).getProdPercDiscount()!=0)
          	    chunk = new Phrase(delLineItems.get(i).getProdPercDiscount()+"",font8);
          	 else
          		 chunk = new Phrase("0"+"",font8);
          	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
          	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
         	
        	 chunk = new Phrase(df.format(delLineItems.get(i).getPrice()*delLineItems.get(i).getQuantity()),font8);
        	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
        	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
        	 
        	 
        	 table.addCell(pdfcategcell);
        	 table.addCell(pdfnamecell);
        	 table.addCell(pdfqtycell);
        	 table.addCell(pdfunitcell);
        	 table.addCell(pdfspricecell);
        	 table.addCell(pdfperdiscount);
        	 pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
        	 table.addCell(pdfservice1);
        	 
        	 table.addCell(pdftotalproduct);
        	 try {
				table.setWidths(columnWidths);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	 
        	 
        	 count=i;
				
				
	        	if(count==this.delLineItems.size()|| count==firstBreakPoint){
							
	        		
	        		
	        	 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
	        		
	        		
							flag=firstBreakPoint;
							break;
				}
					
				
				
				System.out.println("flag value" + flag);
				if(firstBreakPoint==flag){
					
					termsConditionsInfo();
//					addFooter();
					footerInfo();
					
					}
        	 
//        	 
//        	 count= i;
//        	 
//        	 ///change this.salesProd.size() to eva.
//			 
// 			if(count==eva|| count==firstBreakPoint){
// 						
// 						flag=firstBreakPoint;
// 						break;
// 			}
// 			if(firstBreakPoint==flag){
// 				
// 				termsConditionsInfo();
// 				addFooter();
// 				}
//         	}
        	////
         
//         Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
//      	Phrase category = new Phrase("Category ", font1);
//      	Phrase qty = new Phrase("Quantity", font1);
//      	Phrase servicetax = new Phrase("Service Tax", font1);
//      	Phrase vat = new Phrase("VAT", font1);
//      	Phrase percdisc = new Phrase("% Discount", font1);
// 
//      	PdfPCell cellcategory = new PdfPCell(category);
//      	PdfPCell cellqty = new PdfPCell(qty);
//      	PdfPCell cellperdisc = new PdfPCell(percdisc);
//      	PdfPCell cellvat = new PdfPCell(vat);
//      	PdfPCell cellservicetax = new PdfPCell(servicetax);
// 
//      	table.addCell(cellcategory);
//      	table.addCell(cellqty);
//      	table.addCell(cellperdisc);
//      	table.addCell(cellvat);
//      	table.addCell(cellservicetax);
  		
  		
//      	 if( eva<= firstBreakPoint){
// 			int size =  firstBreakPoint - eva;
// 			      blankLines = size*(185/18);
// 			  	System.out.println("blankLines size ="+blankLines);
// 			System.out.println("blankLines size ="+blankLines);
// 				}
// 				else{
// 					blankLines = 0;
// 				}
// 				table.setSpacingAfter(blankLines);	
//      	
//      	
//  		
//   				
//   				for(int i =0;i<eva;i++){
//   				if(getdata().get(i).getProductCategory()!=null){
//  					Phrase chunk = new Phrase(getdata().get(i).getProductCategory(),font8);
//  					
//  					}else{
//  					}
//  					PdfPCell pdfcategcell = new PdfPCell(chunk);
//  		
//  					
//  					
//  					
//  					if(getdata().get(i).getProductQuantity()!=0){
//  					chunk = new Phrase(getdata().get(i).getProductQuantity() + "", font8);
//  					}
//  					else{
//  						chunk = new Phrase("", font8);
//  					}
//  					PdfPCell pdfqtycell = new PdfPCell(chunk);
//  					
//  					
//  					
//  					if(getdata().get(i).getTax()!=0){
//  						chunk = new Phrase(getdata().get(i).getTax() + "", font8);
//  					}else{
//  						chunk = new Phrase("", font8);
//  					}
//  					PdfPCell pdfservice = new PdfPCell(chunk);
//  		
//  					
//  					
//  					
//  					if(getdata().get(i).getVat()!=0){
//  					
//  						chunk = new Phrase(getdata().get(i).getVat() + "", font8);
//  					}else{
//  						chunk = new Phrase( "", font8);
//  					}
//  					PdfPCell pdfvattax = new PdfPCell(chunk);
//  					
//  					
//  					
//  					
//  					if(getdata().get(i).getDiscount()!=0)
//  					{
//  						chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//  				
//  						}else{
//  							chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//  						}	
//  					
//  					PdfPCell pdfperdiscount = new PdfPCell(chunk);
//  		
//  						table.addCell(pdfcategcell);
//  						table.addCell(pdfqtycell);
//  						table.addCell(pdfperdiscount);
//  						table.addCell(pdfvattax);
//  						table.addCell(pdfservice);
  						
  						
//  						count=i;
// 						
// 						
//  			        	if(count==eva|| count==firstBreakPoint){
//  									
//  									flag=firstBreakPoint;
//  									break;
//  						}
//  							
//  						
//  						
//  						System.out.println("flag value" + flag);
//  						if(firstBreakPoint==flag){
//  							
//  							termsConditionsInfo();
//  							addFooter();
//  							
//  							
//  							}
         
         
         ////
         
    }
   				
   				
   				PdfPTable parentTableProd=new PdfPTable(1);
   	         parentTableProd.setWidthPercentage(100);
   	         
   	         PdfPCell prodtablecell=new PdfPCell();
   	         prodtablecell.addElement(table);
   	      prodtablecell.addElement(rephrse);
   	         parentTableProd.addCell(prodtablecell);
   	      
   	         
   	         try {
   				document.add(parentTableProd);
   			} catch (DocumentException e) {
   				e.printStackTrace();
   			}
  }


//
public void createProductInfo()
{
	Font font1 = new Font(Font.FontFamily.HELVETICA  , 8,Font.BOLD);
	Phrase productdetails= new Phrase("Product Details",font1);
	Paragraph para = new Paragraph();
	para.add(productdetails);
	para.setAlignment(Element.ALIGN_CENTER);
		
	PdfPTable table = new PdfPTable(9);
	
	try {
		table.setWidths(columnWidths);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	table.setWidthPercentage(100);
	
	
	Phrase category = new Phrase("Product ID ",font1);
    Phrase product = new Phrase("Name ",font1);
    Phrase qty = new Phrase("Quantity",font1);
    Phrase uom = new Phrase("UOM",font1);
    Phrase rate= new Phrase ("Rate",font1);
    Phrase servicetax = new Phrase("Serv.Tax",font1);
    Phrase vat = new Phrase("VAT",font1);
    Phrase percdisc = new Phrase("% Disc",font1);
    Phrase total = new Phrase("Total",font1);
	
	 PdfPCell cellcategory = new PdfPCell(category);
     PdfPCell cellproduct = new PdfPCell(product);
     PdfPCell cellqty = new PdfPCell(qty);
     PdfPCell celluom = new PdfPCell(uom);
     PdfPCell cellrate = new PdfPCell(rate);
     PdfPCell cellservicetax = new PdfPCell(servicetax);
     PdfPCell cellvat = new PdfPCell(vat);
     PdfPCell cellperdisc = new PdfPCell(percdisc);
     PdfPCell celltotal= new PdfPCell(total);
     
     table.addCell(cellcategory);
     table.addCell(cellproduct);
     table.addCell(cellqty);
     table.addCell(celluom);
     table.addCell(cellrate);
     table.addCell(cellservicetax);
     table.addCell(cellvat);
     table.addCell(cellperdisc);
     table.addCell(celltotal);
     
 	//*************calculating spaces here*************
     if( this.delLineItems.size()<= firstBreakPoint){
			int size =  firstBreakPoint - this.delLineItems.size();
			      blankLines = size*(185/118);
			  	System.out.println("blankLines size ="+blankLines);
			System.out.println("blankLines size ="+blankLines);
				}
				else{
					blankLines = 0;
				}
				table.setSpacingAfter(blankLines);	
     
     for(int i=0;i<this.delLineItems.size();i++)
     {
    	 Phrase chunk=new Phrase(delLineItems.get(i).getProdId()+"",font8);
    	 PdfPCell pdfcategcell = new PdfPCell(chunk);
    	 
    	 chunk = new Phrase(delLineItems.get(i).getProdName(),font8);
    	 PdfPCell pdfnamecell = new PdfPCell(chunk);
    	
    	 chunk = new Phrase(delLineItems.get(i).getQuantity()+"",font8);
    	 PdfPCell pdfqtycell = new PdfPCell(chunk);
    	 
    	 chunk = new Phrase (delLineItems.get(i).getUnitOfMeasurement(),font8);
    	 PdfPCell pdfuomcell = new PdfPCell(chunk);
    	 
    	 chunk = new Phrase(df.format(delLineItems.get(i).getPrice()),font8);
    	 PdfPCell pdfspricecell = new PdfPCell(chunk);
    	 
    	 if(delLineItems.get(i).getServiceTax()!=null)
    	    chunk = new Phrase(delLineItems.get(i).getServiceTax().getPercentage()+"",font8);
    	 else
    		 chunk = new Phrase("N.A"+"",font8);
    	 
    	 PdfPCell pdfservice = new PdfPCell(chunk);
    	 
    	 if(delLineItems.get(i).getVatTax()!=null)
    	    chunk = new Phrase(delLineItems.get(i).getVatTax().getPercentage()+"",font8);
    	 else
    		 chunk = new Phrase("N.A"+"",font8);
    	 PdfPCell pdfvattax = new PdfPCell(chunk);
    	 
    	 if(delLineItems.get(i).getProdPercDiscount()!=0)
     	    chunk = new Phrase(delLineItems.get(i).getProdPercDiscount()+"",font8);
     	 else
     		 chunk = new Phrase("0"+"",font8);
     	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
    	
     	 double totalVal=delLineItems.get(i).getPrice()*delLineItems.get(i).getQuantity();
     	 if(delLineItems.get(i).getProdPercDiscount()!=0){
     		 totalVal=totalVal-(totalVal*delLineItems.get(i).getProdPercDiscount()/100);
     	 }
    	 chunk = new Phrase(df.format(totalVal),font8);
    	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
    	 
    	 
    	 count=i;
    	 
    	 table.addCell(pdfcategcell);
    	 table.addCell(pdfnamecell);
    	 table.addCell(pdfqtycell);
    	 table.addCell(pdfuomcell);
    	 table.addCell(pdfspricecell);
    	 table.addCell(pdfservice);
    	 table.addCell(pdfvattax);
    	 table.addCell(pdfperdiscount);
    	 table.addCell(pdftotalproduct);
				 
		if(count==this.delLineItems.size()|| count==firstBreakPoint){
					
					flag=firstBreakPoint;
					break;
		}
		if(firstBreakPoint==flag){
			
			termsConditionsInfo();
			footerInfo();
			}
    	}
     
     PdfPTable parentTableProd=new PdfPTable(1);
     parentTableProd.setWidthPercentage(100);
     
     PdfPCell prodtablecell=new PdfPCell();
     prodtablecell.addElement(table);
     parentTableProd.addCell(prodtablecell);
     
     
     
    
     try {
		document.add(parentTableProd);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
	} catch (DocumentException e) {
		e.printStackTrace();
	}}



public void termsConditionsInfo()
{
	Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
	
	PdfPTable termpayTable=new PdfPTable(1);
	  termpayTable.setWidthPercentage(100);
	  termpayTable.setSpacingAfter(30f);
//	  String titleterms="Description :";
//	Phrase termsphrase=null;
//	Phrase descPhrase=null;
//	if(!delnoteEntity.getDescription().equals("")){
//		termsphrase=new Phrase(titleterms,font10bold);
//		termsphrase.add(Chunk.NEWLINE);
//		PdfPCell termstitlecell=new PdfPCell();
//		termstitlecell.addElement(termsphrase);
//		termstitlecell.setBorder(0);
//		termpayTable.addCell(termstitlecell);
//		
//		descPhrase=new Phrase(delnoteEntity.getDescription(),font10);
//	}
//	else{
//		descPhrase=new Phrase("  ",font10);
//	}
//	
//	PdfPCell termstitlecell=new PdfPCell();
//	termstitlecell.addElement(termsphrase);
//	termstitlecell.setBorder(0);
//	
//	PdfPCell desccell=new PdfPCell();
//	
//	desccell.addElement(descPhrase);
//	desccell.setBorder(0);
//	 termpayTable.addCell(desccell);
	
	 
	 String invoice="";
		PdfPCell incell;
		PdfPCell getincell;
		if(delnoteEntity.getCount()!=0)
		{
		invoice="Invoice Id : ";
//		Phrase inphrase=new Phrase(invoice,font10bold);
//		 incell=new PdfPCell();
//		incell.addElement(inphrase);
//		incell.setBorder(0);
//		bankinfotable.addCell(incell);
		System.out.println("invoice ID === "+delnoteEntity.getInvoiceId());
		if(delnoteEntity.getInvoiceId()!=0){
		Phrase getinvoice=new Phrase(invoice+delnoteEntity.getInvoiceId()+"");
		 getincell=new PdfPCell();
		getincell.addElement(getinvoice);
		getincell.setBorder(0);
		
		termpayTable.addCell(getincell);
		}
		}else{
		
		
		Phrase blank=new Phrase("  ",font8);
		incell=new PdfPCell();
		incell.addElement(blank);
		incell.setBorder(0);
		termpayTable.addCell(incell);
		}
	 
	 
		Phrase typename=null;
		Phrase typevalue=null;
		

 		PdfPTable artictable=new PdfPTable(4);
 		artictable.setWidthPercentage(100);
 		
 		Phrase nnn=new Phrase("");
 		PdfPCell blankcekk=new PdfPCell(nnn);
 		blankcekk.setBorder(0);
		
		
		for(int i=0;i<this.articletype.size();i++){
			 
			if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("DeliveryNote")){
				
				
				System.out.println("cust docu name if in == "+articletype.get(i).getDocumentName());
				
				 typename = new Phrase(articletype.get(i).getArticleTypeName(),font8);
				 typevalue=new Phrase(articletype.get(i).getArticleTypeValue(),font8);
				 	PdfPCell tymanecell=new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);
					PdfPCell typevalcell=new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setBorder(0);
					

					
					artictable.addCell(tymanecell);
					artictable.addCell(typevalcell);
					artictable.addCell(blankcekk);
					artictable.addCell(blankcekk);
					
			 }
			
			
		 }
		PdfPCell articcell=new PdfPCell(artictable);
 		articcell.setBorder(0);
 		
 		termpayTable.addCell(articcell);
	 
	 
	 //*****************for amt in words****************************************
	   
   
   PdfPTable chargetaxtable = new PdfPTable(2);
   chargetaxtable.setWidthPercentage(100);
   chargetaxtable.setHorizontalAlignment(Element.ALIGN_RIGHT);
   chargetaxtable.setSpacingAfter(40f);
	Phrase totalAmtPhrase=new Phrase("Total Amount   "+"",font1);
	PdfPCell totalAmtCell=new PdfPCell(totalAmtPhrase);
	totalAmtCell.setBorder(0);
	double totalAmt=0;
	totalAmt=delnoteEntity.getTotalAmount();
	
	Phrase realtotalAmt=new Phrase(df.format(totalAmt),font1);
	PdfPCell realtotalAmtCell=new PdfPCell(realtotalAmt);
	realtotalAmtCell.setBorder(0);
	realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	chargetaxtable.addCell(totalAmtCell);
	chargetaxtable.addCell(realtotalAmtCell);
	
	int first=7;
	int cnt= this.billingTaxesLis.size()+this.billingChargesLis.size();
	
	List<String> myList1 = new ArrayList<>();
	List<String> myList2 = new ArrayList<>();
	List<String> myList3 = new ArrayList<>();
	List<String> myList4 = new ArrayList<>();
	
	if(cnt>first){
		
		 for(int i=0;i<this.billingTaxesLis.size();i++)
	      {
	    	  
	   	  
	   	  if(billingTaxesLis.get(i).getChargePercent()!=0){
	   	  
	   		 if(billingTaxesLis.get(i).getChargeName().equals("VAT")||billingTaxesLis.get(i).getChargeName().equals("CST")){
					
					System.out.println("1st loop"+billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent());
					
					String str = billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent();
					double taxAmt1 = billingTaxesLis.get(i).getChargePercent()
							* billingTaxesLis.get(i).getAssessableAmount() / 100;
					
					 myList1.add(str);
					 myList2.add(df.format(taxAmt1));
					 
					 System.out.println("Size of mylist1 is () === "+myList1.size());
				}
				
				if(billingTaxesLis.get(i).getChargeName().equals("Service Tax")){
					
					System.out.println("2nd loop == "+billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent());
					
					String str = billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent();
					double taxAmt1 = billingTaxesLis.get(i).getChargePercent()
							* billingTaxesLis.get(i).getAssessableAmount() / 100;
					String ddd=" ";
					 myList3.add(str);
					 myList4.add(df.format(taxAmt1));
					 myList4.add(ddd);
					 System.out.println("Size of mylist2 is () === "+myList2.size());
				}
	   	  		}
	     	}
		 
		 
		 PdfPCell pdfservicecell=null;
			
			PdfPTable other1table=new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for(int j=0;j<myList1.size();j++){
				chunk = new Phrase(myList1.get(j),font8);
				 pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);
				
			}
			
			PdfPCell pdfservicecell1=null;
			
			PdfPTable other2table=new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for(int j=0;j<myList2.size();j++){
				chunk = new Phrase(myList2.get(j),font8);
				 pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);
				
			}
			
			PdfPCell chargescell=null;
			for(int j=0;j<myList3.size();j++){
				chunk = new Phrase(myList3.get(j),font8);
				chargescell = new PdfPCell(chunk);
				chargescell.setBorder(0);
				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(chargescell);
				
			}
			
			PdfPCell chargescell1=null;
			for(int j=0;j<myList4.size();j++){
				chunk = new Phrase(myList4.get(j),font8);
				chargescell1 = new PdfPCell(chunk);
				chargescell1.setBorder(0);
				chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(chargescell1);
				
			}
			
			
			PdfPCell othercell = new PdfPCell();
			othercell.addElement(other1table);
			othercell.setBorder(0);
			chargetaxtable.addCell(othercell);
			
			PdfPCell othercell1 = new PdfPCell();
			othercell1.addElement(other2table);
			othercell1.setBorder(0);
			chargetaxtable.addCell(othercell1);
		 
		 
		 
		 
		 
		 
		 PdfPCell chargecell = null;
	      PdfPCell chargeamtcell=null;
	      PdfPCell otherchargecell=null;
		 
		 for(int i=0;i<this.billingChargesLis.size();i++)
	      {
	   	   Phrase chunk = null;
	   	   Phrase chunk1 = new Phrase("",font8);
	   	   double chargeAmt=0;
	   	   PdfPCell pdfchargeamtcell = null;
	   	   PdfPCell pdfchargecell = null;
	   	   if(billingChargesLis.get(i).getChargePercent()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getChargeName()+" @ "+billingChargesLis.get(i).getChargePercent(),font1);
	   		   pdfchargecell=new PdfPCell(chunk);
	   		   chargeAmt=billingChargesLis.get(i).getChargePercent()*billingChargesLis.get(i).getAssessableAmount()/100;
	   		   chunk=new Phrase(df.format(chargeAmt),font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	   pdfchargeamtcell.setBorder(0);
	  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	   	   }else{
	   		pdfchargecell=new PdfPCell(chunk1);
	   		pdfchargeamtcell = new PdfPCell(chunk1);
	   	   }
	   	   
//		   double chargeAmt=0;
		   PdfPCell pdfchargeamtcell1 = null;
	   	   PdfPCell pdfchargecell1 = null;
	   	   if(billingChargesLis.get(i).getChargeAbsValue()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getChargeName()+"",font1);
	   		pdfchargecell1=new PdfPCell(chunk);
	   		chargeAmt=billingChargesLis.get(i).getChargeAbsValue();
	   		   chunk=new Phrase(chargeAmt+"",font8);
	   		pdfchargeamtcell1 = new PdfPCell(chunk);
	   		pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	   	   }else{
	   		pdfchargecell1=new PdfPCell(chunk1);
	   		pdfchargeamtcell1 = new PdfPCell(chunk1);
	   	   }
	 	       pdfchargecell.setBorder(0);
	 	      pdfchargeamtcell.setBorder(0);
	 	     pdfchargecell1.setBorder(0);
		      pdfchargeamtcell1.setBorder(0);
	 	      
//		      chargetaxtable.addCell(pdfchargecell);
//		       chargetaxtable.addCell(pdfchargeamtcell);
//		      chargetaxtable.addCell(pdfchargecell1);
//	 	       chargetaxtable.addCell(pdfchargeamtcell1);
	 	       
		      
		      
		      total = total+chargeAmt; 	
			     
			     if(total!=0){
			   		   chunk = new Phrase("Other charges total",font1);
			   		   chargecell=new PdfPCell(chunk);
			   		   chargecell.setBorder(0);
			   		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			   		  
			   		chunk=new Phrase(df.format(total),font8);
			  	      	   chargeamtcell = new PdfPCell(chunk);
			  	      	chargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  	      	   chargeamtcell.setBorder(0);
			   	  
			     }
		      
		      
	      }
		 Phrase other = new Phrase("Refer other charge details",font1);
	     otherchargecell=new PdfPCell(other);
	     otherchargecell.setBorder(0);
	     otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
	     Phrase other1 = new Phrase(" ",font1);
	    PdfPCell otherchargecell1=new PdfPCell(other1);
	     otherchargecell1.setBorder(0);
	     otherchargecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	     
		
		 	chargetaxtable.addCell(chargecell);
	       chargetaxtable.addCell(chargeamtcell);
	       chargetaxtable.addCell(otherchargecell);
	       chargetaxtable.addCell(otherchargecell1);
		
		
	}else{
		
		for(int i=0;i<this.billingTaxesLis.size();i++)
	      {
	    	  
	   	  
	   	  if(billingTaxesLis.get(i).getChargePercent()!=0){
	   	  
	   		if(billingTaxesLis.get(i).getChargeName().equals("VAT")||billingTaxesLis.get(i).getChargeName().equals("CST")){
				
				System.out.println("1st loop"+billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent());
				
				String str = billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent();
				double taxAmt1 = billingTaxesLis.get(i).getChargePercent() * billingTaxesLis.get(i).getAssessableAmount() / 100;
				
				 myList1.add(str);
				 myList2.add(df.format(taxAmt1));
				 
				 System.out.println("Size of mylist1 is () === "+myList1.size());
			}
			
			if(billingTaxesLis.get(i).getChargeName().equals("Service Tax")){
				
				System.out.println("2nd loop == "+billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent());
				
				String str = billingTaxesLis.get(i).getChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getChargePercent();
				double taxAmt1 = billingTaxesLis.get(i).getChargePercent()* billingTaxesLis.get(i).getAssessableAmount() / 100;
				 myList3.add(str);
				 myList4.add(df.format(taxAmt1));
				 
				 System.out.println("Size of mylist2 is () === "+myList2.size());
			}
	   	  		}
	   	  
	     	}
	      
		 PdfPCell pdfservicecell=null;
			
			PdfPTable other1table=new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for(int j=0;j<myList1.size();j++){
				chunk = new Phrase(myList1.get(j),font8);
				 pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);
				
			}
			
			PdfPCell pdfservicecell1=null;
			
			PdfPTable other2table=new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for(int j=0;j<myList2.size();j++){
				chunk = new Phrase(myList2.get(j),font8);
				 pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);
				
			}
			
			
			PdfPCell chargescell=null;
			for(int j=0;j<myList3.size();j++){
				chunk = new Phrase(myList3.get(j),font8);
				chargescell = new PdfPCell(chunk);
				chargescell.setBorder(0);
				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(chargescell);
				
			}
			
			PdfPCell chargescell1=null;
			for(int j=0;j<myList4.size();j++){
				chunk = new Phrase(myList4.get(j),font8);
				chargescell1 = new PdfPCell(chunk);
				chargescell1.setBorder(0);
				chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(chargescell1);
				
			}
			
			
			
			PdfPCell othercell = new PdfPCell();
			othercell.addElement(other1table);
			othercell.setBorder(0);
			chargetaxtable.addCell(othercell);
			
			PdfPCell othercell1 = new PdfPCell();
			othercell1.addElement(other2table);
			othercell1.setBorder(0);
			chargetaxtable.addCell(othercell1);
	      

			/****************************************************************************************/
	    
	     
	      
	      
	      for(int i=0;i<this.billingChargesLis.size();i++)
	      {
	   	   Phrase chunk = null;
	   	   Phrase chunk1 = new Phrase("",font8);
	   	   double chargeAmt=0;
	   	   PdfPCell pdfchargeamtcell = null;
	   	   PdfPCell pdfchargecell = null;
	   	   if(billingChargesLis.get(i).getChargePercent()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getChargeName()+" @ "+billingChargesLis.get(i).getChargePercent(),font1);
	   		   pdfchargecell=new PdfPCell(chunk);
	   		   chargeAmt=billingChargesLis.get(i).getChargePercent()*billingChargesLis.get(i).getAssessableAmount()/100;
	   		   chunk=new Phrase(df.format(chargeAmt),font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	   pdfchargeamtcell.setBorder(0);
	  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	   	   }else{
	   		pdfchargecell=new PdfPCell(chunk1);
	   		pdfchargeamtcell = new PdfPCell(chunk1);
	   	   }
	   	   
		   double chargeAmt1=0;
		   PdfPCell pdfchargeamtcell1 = null;
	   	   PdfPCell pdfchargecell1 = null;
	   	   if(billingChargesLis.get(i).getChargeAbsValue()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getChargeName()+"",font1);
	   		pdfchargecell1=new PdfPCell(chunk);
	   		chargeAmt1=billingChargesLis.get(i).getChargeAbsValue();
	   		   chunk=new Phrase(chargeAmt1+"",font8);
	   		pdfchargeamtcell1 = new PdfPCell(chunk);
	   		pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	   	   }else{
	   		pdfchargecell1=new PdfPCell(chunk1);
	   		pdfchargeamtcell1 = new PdfPCell(chunk1);
	   	   }
	 	       pdfchargecell.setBorder(0);
	 	      pdfchargeamtcell.setBorder(0);
	 	     pdfchargecell1.setBorder(0);
		      pdfchargeamtcell1.setBorder(0);
	 	      
		      chargetaxtable.addCell(pdfchargecell);
		       chargetaxtable.addCell(pdfchargeamtcell);
		      chargetaxtable.addCell(pdfchargecell1);
	 	       chargetaxtable.addCell(pdfchargeamtcell1);
		      	
	      }
		
		
	}
	
  
  
  
  
  PdfPTable billamtable = new PdfPTable(2);
  billamtable.setWidthPercentage(100);
  billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
  
  
  for(int i=0;i<this.billingChargesLis.size();i++){
	       
	       
      	System.out.println("======I  "+i+"   size"+billingChargesLis.size());
      	if(i==billingChargesLis.size()-1){
      		chunk = new Phrase("Net Payable",font1);
	        PdfPCell pdfnetpaycell = new PdfPCell(chunk);
	        pdfnetpaycell.setBorder(0);
	      	Double netPayAmt=(double) 0;
	    	int netpayble = 0;
	      	
    		netPayAmt=delnoteEntity.getNetpayable();
    		
    		netpayble=(int) netPayAmt.doubleValue();
	      	
    		chunk=new Phrase(netpayble+"",font8);
	        PdfPCell pdfnetpayamt = new PdfPCell(chunk);
	        pdfnetpayamt.setBorder(0);
	        pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        billamtable.addCell(pdfnetpaycell);
	        billamtable.addCell(pdfnetpayamt);
//	        chargetaxtable.addCell(pdfnetpaycell);
//	        chargetaxtable.addCell(pdfnetpayamt);
      	}
 	}
  
  if(this.billingChargesLis.size()==0){
	  Double netPayAmt=(double) 0;
    	int netpayble = 0;
	  Phrase chunknetpaytitle = new Phrase("Net Payable",font1);
	  PdfPCell netpaytitlecell=new PdfPCell(chunknetpaytitle);
	  netpaytitlecell.setBorder(0);
      netPayAmt=delnoteEntity.getNetpayable();
      netpayble=(int) netPayAmt.doubleValue();
      
      Phrase chunknetpay=new Phrase(netpayble+"",font8);
      PdfPCell pdfnetpayamt = new PdfPCell(chunknetpay);
      pdfnetpayamt.setBorder(0);
      pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
      billamtable.addCell(netpaytitlecell);
      billamtable.addCell(pdfnetpayamt);
//      chargetaxtable.addCell(netpaytitlecell);
//      chargetaxtable.addCell(pdfnetpayamt);
  }
 
  
  PdfPTable taxinfotable=new PdfPTable(1);
  taxinfotable.setWidthPercentage(100);
  taxinfotable.addCell(chargetaxtable);
  
  PdfPTable parenttaxtable=new PdfPTable(2);
  parenttaxtable.setWidthPercentage(100);
 
  try {
	parenttaxtable.setWidths(new float[]{70,30});
} catch (DocumentException e1) {
	e1.printStackTrace();
}
  
  PdfPCell termsdatacell = new PdfPCell();
  PdfPCell taxdatacell = new PdfPCell();
	
  termsdatacell.addElement(termpayTable);
//  termsdatacell.setBorder(0);
  taxdatacell.addElement(chargetaxtable);
  parenttaxtable.addCell(termsdatacell);
  parenttaxtable.addCell(taxdatacell);

	try {
		document.add(parenttaxtable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	
	 String amtInWords="Amount In Words : ";
      String  amtInFigure= "Rupees: "+SalesInvoicePdf.convert(delnoteEntity.getNetpayable())+" Only";
	   Phrase amtwords=new Phrase(amtInWords,font8bold);
	   Phrase amtFigure=new Phrase(amtInFigure,font1);
	   PdfPCell amtWordsCell=new PdfPCell();
//	   amtWordsCell.addElement(amtwords);
	   amtWordsCell.addElement(amtFigure);
	   amtWordsCell.setBorder(0);
 
 
 //*********************************************************

  PdfPTable termsTable=new PdfPTable(1);
termsTable.setWidthPercentage(100);
//termsTable.setSpacingAfter(120f);
termsTable.addCell(amtWordsCell);
	
	
//PdfPTable termpayTable=new PdfPTable(1);
//termpayTable.setWidthPercentage(100);
////termpayTable.addCell(headingpayterms);
//termpayTable.addCell(termsTable);
////termpayTable.addCell(amountInWordsTable);

PdfPTable parenttaxtable1=new PdfPTable(2);
 parenttaxtable1.setWidthPercentage(100);
 try {
	parenttaxtable1.setWidths(new float[]{70,30});
} catch (DocumentException e1) {
	e1.printStackTrace();
}
 
  PdfPCell amtWordsTblcell = new PdfPCell();
  PdfPCell taxdatacell1 = new PdfPCell();
	
  amtWordsTblcell.addElement(termsTable);
  taxdatacell1.addElement(billamtable);
  parenttaxtable1.addCell(amtWordsTblcell);
  parenttaxtable1.addCell(taxdatacell1);

	try {
		document.add(parenttaxtable1);
	} catch (DocumentException e) {
		e.printStackTrace();
	}


	
	
	
	}


public void footerInfo()
{
	
	PdfPTable bankinfotable=new PdfPTable(1);
	bankinfotable.setWidthPercentage(100);
//	bankinfotable.setSpacingBefore(35f);
	Phrase blank=new Phrase(" ",font8);
	PdfPCell blankc=new PdfPCell();
	blankc.addElement(blank);
	blankc.setBorder(0);
	

	Phrase confirmation = new Phrase("   I/We hereby confirm ",font8);
	 PdfPCell confirmationcell=new PdfPCell();
	 confirmationcell.addElement(confirmation);
	 confirmationcell.setBorder(0);
	 bankinfotable.addCell(confirmationcell);
	 
	 bankinfotable.addCell(blankc);
	 bankinfotable.addCell(blankc);
	 bankinfotable.addCell(blankc);
	 bankinfotable.addCell(blankc);
	  
	 Phrase dottedline = new Phrase("---------------------------"); 
	 PdfPCell dottedlinecell=new PdfPCell();
	 dottedlinecell.addElement(dottedline);
	 dottedlinecell.setBorder(0);
	 bankinfotable.addCell(dottedlinecell);
	
	 Phrase custsignphrase=new Phrase("Customer Signature");
	 PdfPCell custsigncell=new PdfPCell();
	 custsigncell.addElement(custsignphrase);
	 custsigncell.setBorder(0);
	 bankinfotable.addCell(custsigncell);
	
	
	
	
	
	PdfPTable bankinfotable1=new PdfPTable(1);
	bankinfotable1.setWidthPercentage(100);
	
	 String titleterms="Description :";
		Phrase termsphrase=null;
		Phrase descPhrase=null;
		if(!delnoteEntity.getDescription().equals("")){
			termsphrase=new Phrase(titleterms,font10bold);
			termsphrase.add(Chunk.NEWLINE);
			PdfPCell termstitlecell=new PdfPCell();
			termstitlecell.addElement(termsphrase);
			termstitlecell.setBorder(0);
			bankinfotable1.addCell(termstitlecell);
			
			descPhrase=new Phrase(delnoteEntity.getDescription(),font10);
		}
		else{
			descPhrase=new Phrase("  ",font10);
		}
		
		PdfPCell termstitlecell=new PdfPCell();
		termstitlecell.addElement(termsphrase);
		termstitlecell.setBorder(0);
		
		PdfPCell desccell=new PdfPCell();
		
		desccell.addElement(descPhrase);
		desccell.setBorder(0);
		bankinfotable1.addCell(desccell);
	
	
	
	Phrase recieverSinature = new Phrase("",font12boldul);
	Phrase blankphrase = new Phrase(" ");
	PdfPCell blankcell=new PdfPCell();
	blankcell.addElement(blankphrase);
	blankcell.setBorder(0);
	Phrase blankphrase1 = new Phrase(" ");
	 Phrase blankphrase2 = new Phrase(" ");
	 
	 String companyname=comp.getBusinessUnitName().trim().toUpperCase();
	 Paragraph companynamepara=new Paragraph();
	 companynamepara.add("FOR "+companyname);
	 companynamepara.setFont(font9bold);
	 companynamepara.setAlignment(Element.ALIGN_CENTER);
	 
	 
//	 Phrase companyname= new Phrase(comppayment.getPaymentComName().trim(),font12bold);
//	 Phrase authorizedsignatory=null;
//     authorizedsignatory = new Phrase("AUTHORISED SIGNATORY"+"",font10bold);
     String authsign="AUTHORISED SIGNATORY";
     Paragraph authpara=new Paragraph();
     authpara.add(authsign);
     authpara.setFont(font9bold);
     authpara.setAlignment(Element.ALIGN_CENTER);
     
     PdfPCell companynamecell=new PdfPCell();
     companynamecell.addElement(companynamepara);
     companynamecell.setBorder(0);
     
     PdfPCell authsigncell=new PdfPCell();
     authsigncell.addElement(authpara);
     authsigncell.setBorder(0);

     PdfPTable table = new PdfPTable(1);
	 table.addCell(companynamecell);
	 table.addCell(blankcell);
	 table.addCell(blankcell);
	 table.addCell(blankcell);
	 table.addCell(authsigncell);
	 
	 table.setWidthPercentage(100);
	 
	 
	 String endDucument = "*************************X--X--X*************************";
	 Paragraph endpara=new Paragraph();
	 endpara.add(endDucument);
	 endpara.setAlignment(Element.ALIGN_CENTER);
	 
	 
	  PdfPTable parentbanktable=new PdfPTable(3);
	  parentbanktable.setSpacingAfter(30f);
	  parentbanktable.setWidthPercentage(100);
      try {
    	  parentbanktable.setWidths(new float[]{25,45,30});
	  } 
      catch (DocumentException e1) {
		e1.printStackTrace();
	}
      
      PdfPCell bankdatacell1 = new PdfPCell();
	  PdfPCell bankdatacell = new PdfPCell();
	  PdfPCell authorisedcell = new PdfPCell();
		
	  bankdatacell.addElement(bankinfotable);
	  bankdatacell1.addElement(bankinfotable1);
	  authorisedcell.addElement(table);
	  parentbanktable.addCell(bankdatacell);
	  parentbanktable.addCell(bankdatacell1);
	  parentbanktable.addCell(authorisedcell);
	  
	  
	  Phrase refphrase=new Phrase("Annexure 1 ",font10bold);
	  Paragraph repara=new Paragraph(refphrase);
	  
	  try {
		  
			document.add(parentbanktable);
		if(this.delLineItems.size() > 18){
//		if(rohan>20){	
		document.newPage();
		document.add(repara);
		settingRemainingRowsToPDF(19);
		}if(this.delLineItems.size() > 78){
//		}if(rohan>80){
			document.newPage();
			document.add(repara);
		settingRemainingRowsToPDF(79);
		}if(this.delLineItems.size() > 138){
//		}if(rohan>140){	
			document.newPage();
			document.add(repara);
		settingRemainingRowsToPDF(139);
		}if(this.delLineItems.size() > 198){
//		}if(rohan>200){	
			document.newPage();
			document.add(repara);
		settingRemainingRowsToPDF(199);
		}
		if(this.delLineItems.size()>18){
//			document.add(endpara);
			}
			
		} 
		catch (DocumentException e) {
			e.printStackTrace();
		}
	  
	  
	  
	  int first=7;
		int cnt= this.billingTaxesLis.size()+this.billingChargesLis.size();
		
		if(cnt>first){
			otherTaxes();
		}
		
	  
	  
}



private void otherTaxes() {

	Phrase nextpage=new Phrase(Chunk.NEXTPAGE);
	try {
		document.add(nextpage);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	String title="Delivery Note";
	
	String countinfo="ID : "+delnoteEntity.getCount()+"";
	
	String creationdateinfo="Date: "+fmt.format(delnoteEntity.getDeliveryDate());
	
	Phrase titlephrase =new Phrase(title,font14bold);
	Paragraph titlepdfpara=new Paragraph();
	titlepdfpara.add(titlephrase);
	titlepdfpara.setAlignment(Element.ALIGN_CENTER);
	titlepdfpara.add(Chunk.NEWLINE);
	PdfPCell titlepdfcell=new PdfPCell();
	titlepdfcell.addElement(titlepdfpara);
	titlepdfcell.setBorderWidthRight(0);
	titlepdfcell.setBorderWidthLeft(0);
	titlepdfcell.addElement(Chunk.NEWLINE);
	
	Phrase idphrase=new Phrase(countinfo,font14);
	Paragraph idofpdfpara=new Paragraph();
	idofpdfpara.add(idphrase);
	idofpdfpara.setAlignment(Element.ALIGN_LEFT);
	idofpdfpara.add(Chunk.NEWLINE);
	PdfPCell countcell=new PdfPCell();
	countcell.addElement(idofpdfpara);
	countcell.setBorderWidthRight(0);
	countcell.addElement(Chunk.NEWLINE);
	
	Phrase dateofpdf=new Phrase(creationdateinfo,font14);
	Paragraph creatndatepara=new Paragraph();
	creatndatepara.add(dateofpdf);
	creatndatepara.setAlignment(Element.ALIGN_RIGHT);
	creatndatepara.add(Chunk.NEWLINE);
	PdfPCell creationcell=new PdfPCell();
	creationcell.addElement(creatndatepara);
	creationcell.setBorderWidthLeft(0);
	creationcell.addElement(Chunk.NEWLINE);
	
	PdfPTable titlepdftable=new PdfPTable(3);
	titlepdftable.setWidthPercentage(100);
	titlepdftable.addCell(countcell);
	titlepdftable.addCell(titlepdfcell);
	titlepdftable.addCell(creationcell);

		
	try {
		document.add(titlepdftable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 otherchargestoAnnexur();
}


public void settingRemainingRowsToPDF(int flag){
	
	
	PdfPTable table = new PdfPTable(8);
	try {
		table.setWidths(columnWidths);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	table.setWidthPercentage(100);
	
	
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
	Phrase category = new Phrase("SR. NO. ",font1);
    Phrase productname = new Phrase("ITEM DETAILS",font1);
    Phrase qty = new Phrase("QTY",font1);
    Phrase unit = new Phrase("UNIT",font1);
    Phrase rate= new Phrase ("RATE",font1);
    
    Phrase percDisc = new Phrase("DISC %",font1);
    Phrase adddisc = new Phrase("ADDL.DISC %",font1);
    
    Phrase servicetax = new Phrase("TAX",font1);
    
    Paragraph tax= new Paragraph();
    //*************chnges mukesh on 24/4/2015******************** 
//     SalesQuotation salesQu=null;
//     if(qp instanceof SalesQuotation){
//     	salesQu=(SalesQuotation)qp;
//     }
     Phrase svat=null;
     Phrase vvat=null;
     Phrase vat=null;
     int cout=0;
     int flag1=0;
     int st=0;
     int st1=0;
//     for(int i=0;i<this.prodTaxes.size();i++){
//     	
//     }
     
     
   
     for(int i=0;i<this.delLineItems.size();i++){
     	 
     	int cstFlag=0;
     	if(delnoteEntity instanceof DeliveryNote){
     		DeliveryNote salesq=(DeliveryNote)delnoteEntity;
     		
     		if(salesq.getCformStatus()!=null){
     		
     		if(salesq.getCformStatus().trim().equals(AppConstants.YES)||salesq.getCformStatus().trim().equals(AppConstants.NO))
     		{
     			cstFlag=1;
     		}
     		}
     	}
     	
     	
//     	Phrase vatphrase=new Phrase("VAT",font1);
//          Phrase Cstphrase=new Phrase("CST",font1);
//          Phrase Stphrase=new Phrase("ST",font1);
     	
     	
          
          if(delLineItems.get(i).getServiceTax().getPercentage()>0&&delLineItems.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
         	 vvat = new Phrase("VAT / ST %",font1);
         	 cout=1;
          }
         else if(delLineItems.get(i).getServiceTax().getPercentage()>0&&delLineItems.get(i).getVatTax().getPercentage()==0&&cstFlag==0){
      	    vat = new Phrase("ST %",font1);
      	  st=1;
         }
         else if(delLineItems.get(i).getServiceTax().getPercentage()==0&&delLineItems.get(i).getVatTax().getPercentage()>0&&cstFlag==0){
         	 vat = new Phrase("VAT %",font1);
         	 st1=1;
         }else if(cstFlag==1 &&delLineItems.get(i).getServiceTax().getPercentage()>0){
         	svat = new Phrase("CST / ST %",font1);
          	   flag1=1;
         } 
         else if(cstFlag==1 &&delLineItems.get(i).getServiceTax().getPercentage()==0){
      	    vat = new Phrase("CST %",font1);
     
         }
         else{
             	 vat = new Phrase("TAX %",font1);
             }
          
          
     }
     Phrase stvat=null;
     if(cout>0){
     	tax.add(vvat);
     	tax.setAlignment(Element.ALIGN_CENTER);
     }else if(st==1&&st1==1){
     	
     	stvat=new Phrase("VAT / ST %",font1);
     	tax.add(stvat);
     	tax.setAlignment(Element.ALIGN_CENTER);
     }
     
     
     else if(flag1>0){
     	tax.add(svat);
     	tax.setAlignment(Element.ALIGN_CENTER);
     }else{
     tax.add(vat);
     tax.setAlignment(Element.ALIGN_CENTER);
     }
    
    
    
    
    
    
    
    
    Phrase total = new Phrase("AMOUNT",font1);
	
    
	 PdfPCell cellcategory = new PdfPCell(category);
	 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
     PdfPCell cellproductname = new PdfPCell(productname);
     cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
     PdfPCell cellqty = new PdfPCell(qty);
     cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
     PdfPCell cellunit = new PdfPCell(unit);
     cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
     PdfPCell cellrate = new PdfPCell(rate);
     cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
     PdfPCell percDisccell = new PdfPCell(percDisc);
     percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
     PdfPCell cellservicetax = new PdfPCell(tax);
     cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell celltotal= new PdfPCell(total);
     celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     table.addCell(cellcategory);
     table.addCell(cellproductname);
     table.addCell(cellqty);
     table.addCell(cellunit);
     table.addCell(cellrate);
     table.addCell(percDisccell);
     table.addCell(cellservicetax);
     table.addCell(celltotal);

     
     try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
     
     
     
     
     for(int i=flag;i<this.delLineItems.size();i++)
     {
    	 Phrase chunk=new Phrase((i+1)+"",font8);
    	 PdfPCell pdfcategcell = new PdfPCell(chunk);
    	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	 
    	 
    	 chunk = new Phrase(delLineItems.get(i).getProdName(),font8);
    	 PdfPCell pdfnamecell = new PdfPCell(chunk);
    	 
    	 chunk = new Phrase(delLineItems.get(i).getQuantity()+"",font8);
    	 PdfPCell pdfqtycell = new PdfPCell(chunk);
    	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	 
    	 chunk = new Phrase(delLineItems.get(i).getUnitOfMeasurement(),font8);
    	 PdfPCell pdfunitcell = new PdfPCell(chunk);
    	 pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	 
    	 chunk = new Phrase(df.format(delLineItems.get(i).getPrice()),font8);
    	 PdfPCell pdfspricecell = new PdfPCell(chunk);
    	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	 
//    	 if(delLineItems.get(i).getServiceTax()!=null)
//     	    chunk = new Phrase(delLineItems.get(i).getServiceTax().getPercentage()+"",font8);
//     	 else
//     		 chunk = new Phrase("N.A"+"",font8);
//     	 
//     	 PdfPCell pdfservice = new PdfPCell(chunk);
//     	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	 
    	 
    	  PdfPCell pdfservice1=null;
        	 //******************************
        	 double cstval=0;
        	 int cstFlag=0;
        	 if(delnoteEntity instanceof DeliveryNote){
	         		DeliveryNote salesq=(DeliveryNote)delnoteEntity;
	         		
	         		if(salesq.getCformStatus()!=null){
	         		
	         		if(salesq.getCformStatus().trim().equals(AppConstants.YES)||salesq.getCformStatus().trim().equals(AppConstants.NO))
	         		{
	         			cstFlag=1;
	         			cstval=salesq.getCstPercent();
	         		}
	         		}
	         	}
        	 
        	 
        	 System.out.println("one");
        	 
        	  if((delLineItems.get(i).getVatTax().getPercentage()>0)&& (delLineItems.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
              	   
        		  System.out.println("two");
        		  if((delLineItems.get(i).getVatTax()!=null)&&(delLineItems.get(i).getServiceTax()!=null)){
        			  if(cout>0){
            			  chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage())+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            			  }
        		  }else{
                		
        			  chunk = new Phrase("0"+"",font8);
        		  }
        			  pdfservice1 = new PdfPCell(chunk);
           		 
           		  
           		  pdfservice1 = new PdfPCell(chunk);
                	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
        		  
        		  
        		  
        	  }
             else if((delLineItems.get(i).getServiceTax().getPercentage()>0) && (delLineItems.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
            	
            	 System.out.println("three");
            	 if(delLineItems.get(i).getServiceTax()!=null){
            		if(flag1==1||cout==1){
            				chunk = new Phrase("0.00"+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		}else if(st==1&&st1==1){
            			chunk = new Phrase("0.00"+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		}else{
            				chunk = new Phrase(df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		}
            		
            	 }else{
             		 chunk = new Phrase("0.00",font8);
             	 }
             	  pdfservice1 = new PdfPCell(chunk);
             	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
             	 
             }
             else if((delLineItems.get(i).getServiceTax().getPercentage()>0) &&(delLineItems.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
            	 System.out.println("four");
            	
            	 		
            	 	chunk = new Phrase(df.format(cstval)+" / "+df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		 
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
            	
             }
             else if(cstFlag==1&&(delLineItems.get(i).getServiceTax().getPercentage()==0)
            		 &&(delLineItems.get(i).getVatTax().getPercentage()==0)){
            	 
            	 System.out.println("five");
            	 if(flag1==1||cout==1){
            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
            	 }else{
            		 chunk = new Phrase(df.format(cstval),font8);
            	 }
            		 
            		 
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
             } 
             else if(cstFlag==1&&delLineItems.get(i).getServiceTax().getPercentage()>0){
          	  
            	 System.out.println("six");
            	 chunk = new Phrase(df.format(cstval)+" / "+
            			 df.format(delLineItems.get(i).getServiceTax().getPercentage()),font8);
            		 
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
             } 
             else if((cstFlag==1&&(delLineItems.get(i).getServiceTax().getPercentage()==0)&&(delLineItems.get(i).getVatTax().getPercentage()>0))){
            	 System.out.println("cst1..."+cstval);
            	 System.out.println("seven");
            	 if(flag1==1||cout==1){
            		 System.out.println("cst2..."+cstval);
            		 chunk = new Phrase(df.format(cstval)+" / "+"0.00",font8);
            	 }else{
            		 System.out.println("cst3..."+cstval);
            		 chunk = new Phrase(df.format(cstval),font8);
            	 }
            	 System.out.println("cst4..."+cstval);
      			  
      	  pdfservice1 = new PdfPCell(chunk);
      	  
       	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            	 
             }else if((delLineItems.get(i).getServiceTax().getPercentage()==0)&&(delLineItems.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
            	 System.out.println("eight");
            	 if(flag1==1||cout==1){
            		 chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
            	 }else if(st==1&&st1==1){
            		 chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
            	 }else{
            		 chunk = new Phrase(df.format(delLineItems.get(i).getVatTax().getPercentage()),font8);
            	 }
            		 pdfservice1 = new PdfPCell(chunk);
                 	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                 	 
	  
             }else{
            	 if(flag1==1||cout==1){
            		 chunk= new Phrase("0.00 / 0.00",font8);
            	 }else{
            	 chunk = new Phrase("0.00",font8);
            	 }
            	 pdfservice1 = new PdfPCell(chunk);
             }
    	 
    	 
     	 
     	 if(delLineItems.get(i).getProdPercDiscount()!=0)
      	    chunk = new Phrase(delLineItems.get(i).getProdPercDiscount()+"",font8);
      	 else
      		 chunk = new Phrase("0"+"",font8);
      	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
      	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
     	
    	 chunk = new Phrase(df.format(delLineItems.get(i).getTotalAmount()),font8);
    	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
    	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
    	 
    	 
    	 table.addCell(pdfcategcell);
    	 table.addCell(pdfnamecell);
    	 table.addCell(pdfqtycell);
    	 table.addCell(pdfunitcell);
    	 table.addCell(pdfspricecell);
    	 table.addCell(pdfperdiscount);
    	 pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
    	 table.addCell(pdfservice1);
    	 
    	 table.addCell(pdftotalproduct);
    	 try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		flag=flag+1;
		if(flag==78||flag==138||flag==198){
			break;
		}
	}
	
	PdfPTable parentTableProd=new PdfPTable(1);
    parentTableProd.setWidthPercentage(100);
    parentTableProd.setSpacingBefore(10f);
    PdfPCell prodtablecell=new PdfPCell();
    prodtablecell.addElement(table);
    parentTableProd.addCell(prodtablecell);
   
	
	try {
		document.add(parentTableProd);
		
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
}


private void otherchargestoAnnexur(){
	
	Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
	String charge ="Other charge details";
	Chunk prodchunk = new Chunk(charge,font10bold);
	Paragraph parag= new Paragraph();
	parag.add(prodchunk);
	
	
	PdfPTable chargetable = new PdfPTable(2);
	chargetable.setWidthPercentage(100);
	//********************
	double total=0;
	 for(int i=0;i<this.billingChargesLis.size();i++)
     {
  	   Phrase chunk = null;
  	   double chargeAmt=0;
  	   Phrase blank11=new Phrase("",font1);
  	   PdfPCell blankcell=new PdfPCell(blank11);
  	   PdfPCell pdfchargeamtcell = null;
  	   PdfPCell pdfchargecell = null;
  	   if(billingChargesLis.get(i).getChargePercent()!=0){
  		   chunk = new Phrase(billingChargesLis.get(i).getChargeName()+" @ "+billingChargesLis.get(i).getChargePercent(),font1);
  		   pdfchargecell=new PdfPCell(chunk);
  		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
  		   chargeAmt=billingChargesLis.get(i).getChargePercent()*billingChargesLis.get(i).getAssessableAmount()/100;
  		   chunk=new Phrase(df.format(chargeAmt),font8);
 	      	   pdfchargeamtcell = new PdfPCell(chunk);
 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 	      	   pdfchargeamtcell.setBorder(0);
 	      	   
  	   }
  	   if(billingChargesLis.get(i).getChargeAbsValue()!=0){
  		   chunk = new Phrase(billingChargesLis.get(i).getChargeName()+"",font1);
  		   pdfchargecell=new PdfPCell(chunk);
  		
  		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
  		   chargeAmt=billingChargesLis.get(i).getChargeAbsValue();
  		   chunk=new Phrase(chargeAmt+"",font8);
 	      	   pdfchargeamtcell = new PdfPCell(chunk);
 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 	      	   pdfchargeamtcell.setBorder(0);
  	   }
  	 pdfchargecell.setBorder(0);
  	 
  	total=total+chargeAmt;
  	if(chargeAmt!=0){
  	chargetable.addCell(pdfchargecell);
  	chargetable.addCell(pdfchargeamtcell);
  	}
//  	else{
//  		chargetable.addCell(blankcell);
//	  	chargetable.addCell(blankcell);
//  	}
	      	
     }
	 
	 PdfPTable taotatable= new PdfPTable(2);
	 taotatable.setWidthPercentage(100);
	 
	 Phrase totalchunk = new Phrase("Total",font10bold);
		PdfPCell totalcell= new PdfPCell(totalchunk);
//		totalcell.addElement(totalchunk);
		totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totalcell.setBorder(0);
		
	 Phrase toatlamt = new Phrase(df.format(total),font10bold);
	 PdfPCell totalcell1= new PdfPCell(toatlamt);
//		totalcell1.addElement(toatlamt);
		totalcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalcell1.setBorder(0);
		
		taotatable.addCell(totalcell); 
		taotatable.addCell(totalcell1); 
		
	 PdfPCell chargecell= new PdfPCell();
	 chargecell.addElement(chargetable);
//	 chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 PdfPCell chargecell1= new PdfPCell();
	 chargecell1.addElement(taotatable);
	 
	 
	 PdfPTable parent= new PdfPTable(1);
	 parent.setWidthPercentage(70);
	 parent.setSpacingBefore(10f);
	 parent.addCell(chargecell);
	 parent.addCell(chargecell1);
	 parent.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 try {
		    document.add(Chunk.NEWLINE);
		 	document.add(Chunk.NEWLINE);
			document.add(parag);
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
	
}


//   latest delivery note pdf code by rohan Date : 31-07-2017

private void createHeader(int value) 
{
	//Date 21/11/2017 
	//Dev.By Jayshree
	//Des.to add the logo in Table
	DocumentUpload logodocument =comp.getLogo();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	PdfPCell imageSignCell = null;
	Image image2=null;
	try {
		image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
		image2.scalePercent(20f);
//		image2.setAbsolutePosition(40f,765f);	
//		doc.add(image2);
		
		imageSignCell = new PdfPCell(image2);
		imageSignCell.setBorder(0);
		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		imageSignCell.setFixedHeight(20);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	Image image1=null;
//	try
//	{
//	 image1=Image.getInstance("images/ipclogo4.jpg ");//images/ipclogo4.jpg
//	image1.scalePercent(20f);
////	image1.setAbsolutePosition(40f,765f);	
////	doc.add(image1);
//	
//	
//	
//	
//	
//	imageSignCell=new PdfPCell();
//	imageSignCell.addElement(image1);
//	imageSignCell.setFixedHeight(20);
//	imageSignCell.setBorder(0);
//	imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}


PdfPTable logotab =new PdfPTable(1);
logotab.setWidthPercentage(100);



if(imageSignCell!=null)
{
logotab.addCell(imageSignCell);
}
else
{
Phrase logoph=new Phrase(" ");
PdfPCell logoBlank=new PdfPCell(logoph);
logoBlank.setBorder(0);
logotab.addCell(logoBlank);
}
//complete logotab

	Phrase companyName = null;
	if(comp!=null){
		companyName=new Phrase(comp.getBusinessUnitName().trim(),font16bold);
	}
	
	Paragraph companyNamepara=new Paragraph();
	companyNamepara.add(companyName);
	companyNamepara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell companyNameCell=new PdfPCell();
	companyNameCell.addElement(companyNamepara);
	companyNameCell.setBorder(0);
	companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase companyAddr = null;
	if(comp!=null){
		companyAddr=new Phrase(comp.getAddress().getCompleteAddress().trim(),font12);
	}
	Paragraph companyAddrpara=new Paragraph();
	companyAddrpara.add(companyAddr);
	companyAddrpara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell companyAddrCell=new PdfPCell();
	companyAddrCell.addElement(companyAddrpara);
	companyAddrCell.setBorder(0);
	companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase companyGSTTIN = null;
	String gstinValue="";
		
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
				gstinValue=comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
				break;
			}
		}
		
		
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(!comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
				gstinValue=gstinValue+","+comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
			}
		}
	
	
	if(!gstinValue.equals("")){
		companyGSTTIN=new Phrase(gstinValue,font12bold);
	}

	Paragraph companyGSTTINpara=new Paragraph();
	companyGSTTINpara.add(companyGSTTIN);
	companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell companyGSTTINCell=new PdfPCell();
	companyGSTTINCell.addElement(companyGSTTINpara);
	companyGSTTINCell.setBorder(0);
	companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPTable pdfPTable=new PdfPTable(1);
	pdfPTable.setWidthPercentage(100);
	pdfPTable.addCell(companyNameCell);
	pdfPTable.addCell(companyAddrCell);
	pdfPTable.addCell(companyGSTTINCell);
	
	//Date 21/11/2017
		//Dev.By Jayshree 
		//Des.To add logo tab in table
		PdfPTable header =new PdfPTable(2);
		header.setWidthPercentage(100);
		
		
		try {
			header.setWidths(new float[]{20,80});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(imageSignCell!=null){
		PdfPCell left=new PdfPCell(logotab);
		left.setBorder(0);
		header.addCell(left);
		
		PdfPCell right=new PdfPCell(pdfPTable);
		right.setBorder(0);
		header.addCell(right);
		}
		else
		{
			PdfPCell right=new PdfPCell(pdfPTable);
			right.setBorder(0);
			right.setColspan(2);
			header.addCell(right);
		}
		try {
			document.add(header);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		try {
//		document.add(pdfPTable);
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
		
		//End by jayshree
	

	
	//  rohan added this code 
	PdfPTable mytbale = new PdfPTable(3);
	mytbale.setWidthPercentage(100f);
	mytbale.setSpacingAfter(5f);
	mytbale.setSpacingBefore(5f);
	

//	try {
//		mytbale.setWidths(myWidth);
//	} catch (DocumentException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
	
	Image uncheckedImg=null;
	try {
		uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	uncheckedImg.scalePercent(9);

	
	Image checkedImg=null;
	try {
		checkedImg = Image.getInstance("images/checked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	checkedImg.scalePercent(9);

	
	Phrase myblank=new Phrase("   ",font10);
	PdfPCell myblankCell=new PdfPCell(myblank);
	myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase myblankborderZero=new Phrase(" ",font10);
	PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
	myblankborderZeroCell.setBorder(0);
	myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
	Paragraph para1=new Paragraph();
	para1.setIndentationLeft(10f);
	para1.add(myblank);
	if(value==0){
		para1.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para1.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para1.add(stat1Phrase);
	para1.setAlignment(Element.ALIGN_MIDDLE);
	
	PdfPCell stat1PhraseCell=new PdfPCell(para1);
	stat1PhraseCell.setBorder(0);
	stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
	Paragraph para2=new Paragraph();
	para2.setIndentationLeft(10f);
	
	if(value==1){
		para2.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para2.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para2.add(stat2Phrase);
	para2.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell stat2PhraseCell=new PdfPCell(para2);
	stat2PhraseCell.setBorder(0);
	stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
	Paragraph para3=new Paragraph();
	para3.setIndentationLeft(10f);
	
	if(value==2){
		para3.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para3.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	para3.add(stat3Phrase);
	para3.setAlignment(Element.ALIGN_JUSTIFIED);
	
	PdfPCell stat3PhraseCell=new PdfPCell(para3);
	stat3PhraseCell.setBorder(0);
	stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	mytbale.addCell(stat1PhraseCell);
	mytbale.addCell(stat2PhraseCell);
	mytbale.addCell(stat3PhraseCell);

	PdfPTable tab = new PdfPTable(1);
	tab.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(mytbale);
	tab.addCell(cell);
	try {
		document.add(tab);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
//  ends here 
		String titlepdf="Delivery Note";
			
			Phrase titlephrase=new Phrase(titlepdf,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell titlecell=new PdfPCell();
			titlecell.addElement(titlepdfpara);
			titlecell.setBorder(0);
			
			Phrase blankphrase=new Phrase("",font8);
			PdfPCell blankCell=new PdfPCell();
			blankCell.addElement(blankphrase);
			blankCell.setBorder(0);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
			titlepdftable.addCell(blankCell);
			titlepdftable.addCell(titlecell);
			titlepdftable.addCell(blankCell);
			
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
			parent.addCell(titlePdfCell);
			
			try {
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
}


private void createBlankforUPC(int i) 
{
	Image uncheckedImg=null;
	try {
		uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	uncheckedImg.scalePercent(9);
	
	Image checkedImg=null;
	try {
		checkedImg = Image.getInstance("images/checked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	checkedImg.scalePercent(9);

	PdfPTable mytbale = new PdfPTable(3);
	mytbale.setSpacingAfter(5f);
	mytbale.setWidthPercentage(100f);
	
	Phrase myblank=new Phrase("   ",font10);
	PdfPCell myblankCell=new PdfPCell(myblank);
	myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase myblankborderZero=new Phrase(" ",font10);
	PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
	myblankborderZeroCell.setBorder(0);
	myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
	Paragraph para1=new Paragraph();
	para1.setIndentationLeft(10f);
	para1.add(myblank);
	if(i==0){
		para1.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para1.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para1.add(stat1Phrase);
	para1.setAlignment(Element.ALIGN_MIDDLE);
	
	PdfPCell stat1PhraseCell=new PdfPCell(para1);
	stat1PhraseCell.setBorder(0);
	stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
	Paragraph para2=new Paragraph();
	para2.setIndentationLeft(10f);
	
	if(i==1){
		para2.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para2.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para2.add(stat2Phrase);
	para2.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell stat2PhraseCell=new PdfPCell(para2);
	stat2PhraseCell.setBorder(0);
	stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
	Paragraph para3=new Paragraph();
	para3.setIndentationLeft(10f);
	
	if(i==2){
		para3.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para3.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	para3.add(stat3Phrase);
	para3.setAlignment(Element.ALIGN_JUSTIFIED);
	
	PdfPCell stat3PhraseCell=new PdfPCell(para3);
	stat3PhraseCell.setBorder(0);
	stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	mytbale.addCell(stat1PhraseCell);
	mytbale.addCell(stat2PhraseCell);
	mytbale.addCell(stat3PhraseCell);
	
	//  ends here 
	String titlepdf="Delivery Note ";
		
		Phrase titlephrase=new Phrase(titlepdf,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(10f);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(mytbale);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
}


private void createDeliveryNoteDetails() {
	
	float[] columnHalfWidth = {1f,1f};
	// TODO Auto-generated method stub
	PdfPTable mainTable=new PdfPTable(2);
	mainTable.setWidthPercentage(100);
	try {
		mainTable.setWidths(columnHalfWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	PdfPTable part1Table=new PdfPTable(1);
	part1Table.setWidthPercentage(100);
	float[] columnrohanCollonWidth = {3.5f,0.2f,6.8f};
	PdfPTable colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnrohanCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	Phrase colon=new Phrase(":",font7bold);
	PdfPCell colonCell=new PdfPCell(colon);
//	colonCell.addElement(colon);
	colonCell.setBorder(0);
	colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase reverseCharge=new Phrase("Reverse Charge(Y/N)",font7bold);
	PdfPCell reverseChargeCell=new PdfPCell(reverseCharge);
//	reverseChargeCell.addElement(reverseCharge);
	reverseChargeCell.setBorder(0);
	reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase reverseChargeVal=new Phrase("No ",font7);
	PdfPCell reverseChargeValCell=new PdfPCell(reverseChargeVal);
//	reverseChargeValCell.addElement(reverseChargeVal);
	reverseChargeValCell.setBorder(0);
	reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase invoiceNo=new Phrase("Delivery Note Id",font7bold);
	PdfPCell invoiceNoCell=new PdfPCell(invoiceNo);
//	invoiceNoCell.addElement(invoiceNo);
	invoiceNoCell.setBorder(0);
	invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase invoiceNoVal=new Phrase(delnoteEntity.getCount()+"",font7);
	PdfPCell invoiceNoValCell=new PdfPCell(invoiceNoVal);
//	invoiceNoValCell.addElement(invoiceNoVal);
	invoiceNoValCell.setBorder(0);
	invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	
	Phrase invoiceDate=new Phrase("Delivery Date",font7bold);
	PdfPCell invoiceDateCell=new PdfPCell(invoiceDate);
//	invoiceDateCell.addElement(invoiceDate);
	invoiceDateCell.setBorder(0);
	invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase invoiceDateVal=new Phrase(fmt.format(delnoteEntity.getDeliveryDate()),font7);
	PdfPCell invoiceDateValCell=new PdfPCell(invoiceDateVal);
//	invoiceDateValCell.addElement(invoiceDateVal);
	invoiceDateValCell.setBorder(0);
	invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase oderId=new Phrase("Sales Oder Id",font7bold);
	PdfPCell oderIdCell=new PdfPCell(oderId);
//	invoiceDateCell.addElement(invoiceDate);
	oderIdCell.setBorder(0);
	oderIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase oderIdValue=new Phrase(salesOrderEntity.getCount()+"",font7);
	PdfPCell oderIdValueValue=new PdfPCell(oderIdValue);
//	invoiceDateValCell.addElement(invoiceDateVal);
	oderIdValueValue.setBorder(0);
	oderIdValueValue.setHorizontalAlignment(Element.ALIGN_LEFT);

	colonTable.addCell(reverseChargeCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(reverseChargeValCell);
	colonTable.addCell(invoiceNoCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(invoiceNoValCell);
	colonTable.addCell(invoiceDateCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(invoiceDateValCell);

	colonTable.addCell(oderIdCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(oderIdValueValue);

	PdfPCell pdfCell=new PdfPCell();
	pdfCell.setBorder(0);
	pdfCell.addElement(colonTable);
	
	part1Table.addCell(pdfCell);
	
	Phrase state=new Phrase("State",font7bold);
	PdfPCell stateCell=new PdfPCell(state);
//	stateCell.addElement(state);
	stateCell.setBorder(0);
	stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateVal=new Phrase(comp.getAddress().getState().trim(),font7);
	PdfPCell stateValCell=new PdfPCell(stateVal);
//	stateValCell.addElement(stateVal);
	stateValCell.setBorder(0);
	stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateCode=new Phrase("State Code",font7bold);
	PdfPCell stateCodeCell=new PdfPCell(stateCode);
//	stateCodeCell.addElement(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateCodeVal=new Phrase(" ",font7);
	PdfPCell stateCodeValCell=new PdfPCell();
	stateCodeValCell.addElement(stateCodeVal);
	stateCodeValCell.setBorder(0);
	stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable statetable=new PdfPTable(2);
	statetable.setWidthPercentage(100);
	
	colonTable=new PdfPTable(3);
	float[] columnCollonWidth = {1.8f,0.2f,7.5f};
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(stateCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(stateValCell);
	statetable.addCell(colonTable);
	
//	float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(stateCodeCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(stateCodeValCell);
	statetable=new PdfPTable(2);
	statetable.setWidthPercentage(100);
	statetable.addCell(colonTable);
	
	PdfPCell stateTableCell=new PdfPCell();
	stateTableCell.setBorder(0);
	stateTableCell.addElement(statetable);
	part1Table.addCell(stateTableCell);
	
	PdfPTable part2Table=new PdfPTable(1);
	part2Table.setWidthPercentage(100);
	
	Phrase transportmode=new Phrase("Transport Mode",font7bold);
	PdfPCell transportmodeCell=new PdfPCell(transportmode);
	transportmodeCell.setBorder(0);
	transportmodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	String transMode="";
	if(delnoteEntity.getVehicleName()!=null){
		transMode=delnoteEntity.getVehicleName();
	}
	Phrase transModeVal=new Phrase(transMode,font7);
	PdfPCell transModeValCell=new PdfPCell(transModeVal);
	transModeValCell.setBorder(0);
	transModeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase vechicleNo=new Phrase("Vehicle Number",font7bold);
	PdfPCell vechicleNoCell=new PdfPCell(vechicleNo);
	vechicleNoCell.setBorder(0);
	vechicleNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	String vechicalnumber="";
	if(delnoteEntity.getVehicleNo()!=null){
		vechicalnumber=delnoteEntity.getVehicleNo();
	}
	
	Phrase vechicleNoValue=new Phrase(vechicalnumber,font7);
	PdfPCell vechicleNoValueCell=new PdfPCell(vechicleNoValue);
	vechicleNoValueCell.setBorder(0);
	vechicleNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	
	//  delivery date
	
	Phrase deliveryDate=new Phrase("Date of Supply",font7bold);
	PdfPCell deliveryDateCell=new PdfPCell(deliveryDate);
	deliveryDateCell.setBorder(0);
	deliveryDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	
	Phrase deliveryDateValue=new Phrase(fmt.format(delnoteEntity.getDeliveryDate()),font7);
	PdfPCell deliveryDateValueCell=new PdfPCell(deliveryDateValue);
	deliveryDateValueCell.setBorder(0);
	deliveryDateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	//   place of supply 
	
	Phrase placeOfSupply=new Phrase("Place Of Supply",font7bold);
	PdfPCell placeOfSupplyCell=new PdfPCell(placeOfSupply);
	placeOfSupplyCell.setBorder(0);
	placeOfSupplyCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	
	Phrase placeOfSupplyValue=new Phrase(delnoteEntity.getShippingAddress().getCity(),font7);
	PdfPCell placeOfSupplyValueCell=new PdfPCell(placeOfSupplyValue);
	placeOfSupplyValueCell.setBorder(0);
	placeOfSupplyValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);


	
	PdfPTable periodtable=new PdfPTable(2);
	periodtable.setWidthPercentage(100);
	float[] columnrohanrrCollonWidth = {2.8f,0.2f,7.7f};
	PdfPTable concolonTable=new PdfPTable(3);
	concolonTable.setWidthPercentage(100);
	try {
		concolonTable.setWidths(columnrohanrrCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	concolonTable.addCell(transportmodeCell);
	concolonTable.addCell(colonCell);
	concolonTable.addCell(transModeValCell);
	
	concolonTable.addCell(vechicleNoCell);
	concolonTable.addCell(colonCell);
	concolonTable.addCell(vechicleNoValueCell);
	
	concolonTable.addCell(deliveryDateCell);
	concolonTable.addCell(colonCell);
	concolonTable.addCell(deliveryDateValueCell);
	
	concolonTable.addCell(placeOfSupplyCell);
	concolonTable.addCell(colonCell);
	concolonTable.addCell(placeOfSupplyValueCell);
	
	float[] columnDateCollonWidth = {1.5f,0.2f,1.2f};
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnDateCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//	colonTable.addCell(startDateCell);
//	colonTable.addCell(colonCell);
//	colonTable.addCell(startDateValCell);
	
	PdfPCell startcolonTableCell=new PdfPCell(colonTable);
	startcolonTableCell.setBorder(0);
//	startcolonTableCell.addElement(colonTable);
	periodtable.addCell(startcolonTableCell);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnDateCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
//	colonTable.addCell(endDateCell);
//	colonTable.addCell(colonCell);
//	colonTable.addCell(endDateValCell);
	
	PdfPCell endcolonTableCell=new PdfPCell();
	endcolonTableCell.setBorder(0);
	endcolonTableCell.addElement(colonTable);
	periodtable.addCell(endcolonTableCell);
	
	PdfPCell periodTableCell=new PdfPCell();
	periodTableCell.setBorder(0);
	periodTableCell.addElement(periodtable);
	
	PdfPCell concolonTableCell=new PdfPCell();
	concolonTableCell.setBorder(0);
	concolonTableCell.addElement(concolonTable);
	
	
	
	part2Table.addCell(concolonTableCell);
	part2Table.addCell(periodTableCell);
	
	PdfPCell part1Cell=new PdfPCell();
	part1Cell.setBorderWidthRight(0);
	part1Cell.addElement(part1Table);
	
	mainTable.addCell(part1Cell);
	
	part1Cell=new PdfPCell();
	part1Cell.addElement(part2Table);
	mainTable.addCell(part1Cell);
	
	Phrase billingAddress=new Phrase("Bill to Party",font8bold);
	PdfPCell billAdressCell=new PdfPCell(billingAddress);
	billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	
	mainTable.addCell(billAdressCell);
	Phrase serviceaddress=new Phrase("Ship to Party",font8bold);
	PdfPCell serviceCell=new PdfPCell(serviceaddress);
	serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	mainTable.addCell(serviceCell);
	try {
		document.add(mainTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

private void createCustomerDetails() {
	// TODO Auto-generated method stub
	PdfPTable mainTable=new PdfPTable(2);
	mainTable.setWidthPercentage(100);
	try {
		mainTable.setWidths(columnHalfWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	/*Start Part 1*/
	PdfPTable part1Table=new PdfPTable(1);
	part1Table.setWidthPercentage(100);
	
	Phrase colon=new Phrase(":",font7bold);
	PdfPCell colonCell=new PdfPCell(colon);
//	colonCell.addElement(colon);
	colonCell.setBorder(0);
	colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Phrase name=new Phrase("Name",font7bold);
	PdfPCell nameCell=new PdfPCell(name);
	nameCell.setBorder(0);
	nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	//  rohan added this code for considering customer printable name as well Date : 04-07-2017
	
	String tosir= null;
	String custName="";
	//   rohan modified this code for printing printable name 
	/**
	 * old code
	 */
//	if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
//	{
//		custName=cust.getCustPrintableName().trim();
//	}
//	else
//	{
//	
////		if(cust.isCompany()==true){
////			
////			
////		 tosir="M/S";
////		}
////		else{
////			tosir="";
////		}
//		
//		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//			custName="M/S"+cust.getCompanyName().trim();
//		}
//		else{
//			custName=cust.getFullname().trim();
//		}
//	}
	
	//Date 21/11/2017
		//Dev.By jayshree
		//Des.To add the salutation for customer name
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
//			if(cust.isCompany()==true){
//				
//				
//			 tosir="M/S";
//			}
//			else{
//				tosir="";
//			}
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName="M/S "+" "+cust.getCompanyName().trim();
			}
			else if(cust.getSalutation()!=null)
			{
				custName=cust.getSalutation()+" "+cust.getFullname().trim();
			}
			else
			{
				custName=cust.getFullname().trim();
			}
		}
		//End by jayshree
	
	String fullname= "";
	 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
	   	{
	       	fullname = custName;
	   	}
	       else
	       {
	       	fullname =custName;
	       }
	//   ends here 
	
	Phrase nameCellVal=new Phrase(fullname +"       Mob : "+cust.getCellNumber1(),font7);
	PdfPCell nameCellValCell=new PdfPCell(nameCellVal);
//	nameCellValCell.addElement(nameCellVal);
	nameCellValCell.setBorder(0);
	nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase address=new Phrase("Address",font7bold);
	PdfPCell addressCell=new PdfPCell(address);
	addressCell.setBorder(0);
	addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	System.out.println("Rohan cust "+cust.getName());

	String adrsValString = cust.getAdress().getCompleteAddress().trim();
	
	/**
	 * @author Anil
	 * @since 30-11-2020
	 * if customer billing order is stored in sales order entity then
	 * we pick up and display billing address from there only
	 * Raised by Rahul Tiwari ,PTSPL
	 */
	if (delnoteEntity!=null) {
		if(delnoteEntity.getBillingAddress()!=null){
			adrsValString = delnoteEntity.getBillingAddress().getCompleteAddress().trim();
		}
	}
	
	Phrase addressVal=new Phrase(adrsValString,font7);
	PdfPCell addressValCell=new PdfPCell(addressVal);
	addressValCell.setBorder(0);
	addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String gstTinStr="";
	for (int j = 0; j < cust.getArticleTypeDetails().size(); j++) {
		if(cust.getArticleTypeDetails().get(j).getArticleTypeName().equalsIgnoreCase("GSTIN")){
			gstTinStr=cust.getArticleTypeDetails().get(j).getArticleTypeValue().trim();
		}
	}
	
	
	Phrase gstTin=new Phrase("GSTIN",font7bold);
	PdfPCell gstTinCell=new PdfPCell(gstTin);
//	gstTinCell.addElement(gstTin);
	gstTinCell.setBorder(0);
	gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase gstTinVal=new Phrase(gstTinStr,font7);
	PdfPCell gstTinValCell=new PdfPCell(gstTinVal);
//	gstTinValCell.addElement(gstTinVal);
	gstTinValCell.setBorder(0);
	gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	colonTable.addCell(nameCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(nameCellValCell);
	colonTable.addCell(addressCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(addressValCell);
	colonTable.addCell(gstTinCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(gstTinValCell);
	
	PdfPCell cell1=new PdfPCell();
	cell1.setBorder(0);
	cell1.addElement(colonTable);
	
	Phrase state=new Phrase("State",font7bold);
	PdfPCell stateCell=new PdfPCell(state);
//	stateCell.addElement(state);
	stateCell.setBorder(0);
	stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateVal=new Phrase(":   "+cust.getAdress().getState().trim(),font7);
	PdfPCell stateValCell=new PdfPCell(stateVal);
//	stateValCell.addElement(stateVal);
	stateValCell.setBorder(0);
	stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String stCo="";
	for(int i=0;i<stateList.size();i++){
		if(stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim()))
		{
			stCo=stateList.get(i).getStateCode().trim();
			break;
		}
	}
	
	Phrase stateCode=new Phrase("State Code",font7bold);
	PdfPCell stateCodeCell=new PdfPCell(stateCode);
//	stateCodeCell.addElement(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateCodeVal=new Phrase(stCo,font7);
	PdfPCell stateCodeValCell=new PdfPCell(stateCodeVal);
//	stateCodeValCell.addElement(stateCodeVal);
//	stateCodeValCell.setBorder(0);
	stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable statetable=new PdfPTable(2);
	statetable.setWidthPercentage(100);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(stateCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(stateValCell);
	
	PdfPCell colonTableState=new PdfPCell();
	colonTableState.setBorder(0);
	colonTableState.addElement(colonTable);
	statetable.addCell(colonTableState);
	
	colonTable=new PdfPTable(4);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnStateCodeCollonWidth);
	} catch (DocumentException e)
	{
		e.printStackTrace();
	}
	Phrase blak= new Phrase(" ", font8);
	PdfPCell blakCell = new PdfPCell(blak);
	blakCell.setBorder(0);
	
	colonTable.addCell(stateCodeCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(stateCodeValCell);
	colonTable.addCell(blakCell);
	
	PdfPCell colonTableValState=new PdfPCell();
	colonTableValState.setBorder(0);
	colonTableValState.addElement(colonTable);
	statetable.addCell(colonTableValState);
	
	PdfPCell stateTableCell=new PdfPCell(statetable);
	stateTableCell.setBorder(0);
	stateTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	stateTableCell.addElement(statetable);
	
	part1Table.addCell(cell1);
	part1Table.addCell(stateTableCell);
	
	PdfPCell part1TableCell=new PdfPCell();
	part1TableCell.addElement(part1Table);
	
	/*Ends Part 1*/
	
	/*Part 2 Start*/
	PdfPTable part2Table=new PdfPTable(1);
	part2Table.setWidthPercentage(100);

	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	Phrase name2=new Phrase("Name",font7bold);
	PdfPCell name2Cell=new PdfPCell();
	name2Cell.addElement(name2);
	name2Cell.setBorder(0);
	name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase name2CellVal=new Phrase(fullname +"       Mob : "+cust.getCellNumber1(),font7);
	PdfPCell name2CellValCell=new PdfPCell(name2CellVal);
//	name2CellValCell.addElement(name2CellVal);
	name2CellValCell.setBorder(0);
	name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase address2=new Phrase("Address",font7bold);
	PdfPCell address2Cell=new PdfPCell(address2);
//	address2Cell.addElement(address2);
	address2Cell.setBorder(0);
	address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	///////////////////////////  Ajinkya added branch code for customer Branch On Date : 15/07/2017  //////////////////////////////
	String adrsValString1 = "";
	
	if(!delnoteEntity.getShippingAddress().getAddrLine1().equals("")){
		adrsValString1 = delnoteEntity.getShippingAddress().getCompleteAddress().trim();
	}
	
	Phrase address2Val=new Phrase(adrsValString1,font7);
	PdfPCell address2ValCell=new PdfPCell(address2Val);
	address2ValCell.setBorder(0);
	address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	String gstTin2Str="";
	for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
		if(cust.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
			gstTin2Str=cust.getArticleTypeDetails().get(i).getArticleTypeName().trim();
		}
	}
	
	
	Phrase gstTin2=new Phrase("GSTIN",font7bold);
	PdfPCell gstTin2Cell=new PdfPCell(gstTin2);
	gstTin2Cell.setBorder(0);
	gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase gstTin2Val=new Phrase(gstTin2Str,font7);
	PdfPCell gstTin2ValCell=new PdfPCell(gstTin2Val);
	gstTin2ValCell.setBorder(0);
	gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	colonTable.addCell(nameCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(nameCellValCell);
	colonTable.addCell(addressCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(address2ValCell);
	colonTable.addCell(gstTinCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(gstTinValCell);
	
	PdfPCell cell2=new PdfPCell();
	cell2.setBorder(0);
	cell2.addElement(colonTable);
	
	Phrase state2=new Phrase("State",font7bold);
	PdfPCell state2Cell=new PdfPCell(state2);
//	state2Cell.addElement(state2);
	state2Cell.setBorder(0);
	state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String add="";
	if(!delnoteEntity.getShippingAddress().getState().equals("")){
		add = delnoteEntity.getShippingAddress().getState().trim();
	}
	else{
		add = cust.getSecondaryAdress().getState().trim();
	}
	
	Phrase state2Val=new Phrase(":   "+add,font7);
	PdfPCell state2ValCell=new PdfPCell(state2Val);
//	state2ValCell.addElement(state2Val);
	state2ValCell.setBorder(0);
	state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String stsecCo="";
	for(int i=0;i<stateList.size();i++){
		if(stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getSecondaryAdress().getState().trim()))
		{
			stsecCo=stateList.get(i).getStateCode().trim();
			break;
		}
	}
	
	Phrase state2Code=new Phrase("State Code",font7bold);
	PdfPCell state2CodeCell=new PdfPCell(state2Code);
//	state2CodeCell.addElement(state2Code);
	state2CodeCell.setBorder(0);
	state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase state2CodeVal=new Phrase(stsecCo,font7);
	PdfPCell state2CodeValCell=new PdfPCell(state2CodeVal);
//	state2CodeValCell.addElement(state2CodeVal);
//	state2CodeValCell.setBorder(0);
	state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable state2table=new PdfPTable(2);
	state2table.setWidthPercentage(100);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(state2Cell);
	colonTable.addCell(colonCell);
	colonTable.addCell(state2ValCell);
	PdfPCell state2ValcolonTableCell =new PdfPCell();
	state2ValcolonTableCell.setBorder(0);
	state2ValcolonTableCell.addElement(colonTable);
	state2table.addCell(state2ValcolonTableCell);
	
	colonTable=new PdfPTable(4);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnStateCodeCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(state2CodeCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(state2CodeValCell);
	colonTable.addCell(blakCell);
	
	state2ValcolonTableCell =new PdfPCell();
	state2ValcolonTableCell.setBorder(0);
	state2ValcolonTableCell.addElement(colonTable);
	state2table.addCell(state2ValcolonTableCell);
	
	PdfPCell state2TableCell=new PdfPCell(state2table);
	state2TableCell.setBorder(0);
//	state2TableCell.addElement(state2table);
	
	part2Table.addCell(cell2);
	part2Table.addCell(state2TableCell);
	
	PdfPCell part2TableCell=new PdfPCell(part2Table);
//	part2TableCell.addElement(part2Table);
	/*Part 2 Ends*/
	mainTable.addCell(part1TableCell);
	mainTable.addCell(part2TableCell);
	
	Phrase my = new Phrase("",font8);
	PdfPCell blankCell= new PdfPCell(my);
	mainTable.addCell(blankCell);
	mainTable.addCell(blankCell);
	
	try {
		document.add(mainTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

private void createProductDetails() {
	PdfPTable productTable=new PdfPTable(13);
	productTable.setWidthPercentage(100);
	try {
		productTable.setWidths(column13CollonWidth);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	Phrase srNophrase=new Phrase("Sr No",font7bold);
	PdfPCell srNoCell=new PdfPCell();
	srNoCell.addElement(srNophrase);
	srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	srNoCell.setRowspan(2); //1
	
	Phrase servicePhrase=new Phrase("Services",font7bold);
	PdfPCell servicePhraseCell=new PdfPCell(servicePhrase);
	 servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	servicePhraseCell.setRowspan(2);//2
	
	Phrase hsnCode=new Phrase("HSN",font7bold);
	PdfPCell hsnCodeCell=new PdfPCell(hsnCode);
	hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	hsnCodeCell.setRowspan(2);//3
	
	Phrase UOMphrase=new Phrase("UOM",font7bold);
	PdfPCell UOMphraseCell=new PdfPCell(UOMphrase);
	UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	UOMphraseCell.setRowspan(2);//
	
	Phrase qtyPhrase=new Phrase("Qty",font7bold);
	PdfPCell qtyPhraseCell=new PdfPCell(qtyPhrase);
	qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	qtyPhraseCell.setRowspan(2);//5
	
	Phrase ratePhrase=new Phrase("Rate",font7bold);
	PdfPCell ratePhraseCell=new PdfPCell(ratePhrase);
	ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	ratePhraseCell.setRowspan(2);//6
	
	Phrase amountPhrase=new Phrase("Amount",font7bold);
	PdfPCell amountPhraseCell=new PdfPCell(amountPhrase);
	amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	amountPhraseCell.setRowspan(2);//7
	
	Phrase dicphrase=new Phrase("Disc",font7bold);
	PdfPCell dicphraseCell=new PdfPCell(dicphrase);
	dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	dicphraseCell.setRowspan(2);//8
	
	Phrase taxValPhrase=new Phrase("Ass Val",font7bold);
	PdfPCell taxValPhraseCell=new PdfPCell(taxValPhrase);
	taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	taxValPhraseCell.setRowspan(2);//9
	
//	PdfPTable cgstcellTable=new PdfPTable(1);
//	cgstcellTable.setWidthPercentage(100);
	
	Phrase cgstphrase=new Phrase("CGST",font7bold);
	PdfPCell cgstphraseCell=new PdfPCell(cgstphrase);
//	cgstphraseCell.addElement(cgstphrase);
//	cgstphraseCell.setBorder(0);
	cgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	cgstcellTable.addCell(cgstphraseCell);
	cgstphraseCell.setColspan(1);
//	cgstphraseCell.setRowspan(2);
	
	Phrase sgstphrase=new Phrase("SGST",font7bold);
	PdfPCell sgstphraseCell=new PdfPCell(sgstphrase);
//	sgstphraseCell.setBorder(0);
	sgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	sgstphraseCell.addElement(sgstphrase);
//	sgstcellTable.addCell(sgstphraseCell);
	sgstphraseCell.setColspan(1);
//	sgstphraseCell.setRowspan(2);
	
	Phrase igstphrase=new Phrase("IGST",font7bold);
	PdfPCell igstphraseCell=new PdfPCell(igstphrase);
//	igstphraseCell.setBorder(0);
	igstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	igstphraseCell.addElement(igstphrase);
//	igstcellTable.addCell(igstphraseCell);
	igstphraseCell.setColspan(1);
//	igstphraseCell.setRowspan(2);
	
	Phrase totalPhrase=new Phrase("Total",font7bold);
	PdfPCell totalPhraseCell=new PdfPCell(totalPhrase);
	totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	totalPhraseCell.setRowspan(2);//2
	
	
	Phrase cgstpercentphrase=new Phrase("%",font6bold);
	PdfPCell cgstpercentphraseCell=new PdfPCell(cgstpercentphrase);
//	cgstpercentphraseCell.setBorderWidthBottom(0);
//	cgstpercentphraseCell.setBorderWidthTop(0);
//	cgstpercentphraseCell.setBorderWidthLeft(0);
//	cgstpercentphraseCell.addElement();
//	innerCgstTable.addCell(cgstpercentphraseCell);
	
//	Phrase cgstamtphrase=new Phrase("Amt",font6bold);
//	PdfPCell cgstamtphraseCell=new PdfPCell();
//	cgstamtphraseCell.addElement(cgstamtphrase);
//	
	
	
	productTable.addCell(srNoCell);
	productTable.addCell(servicePhraseCell);
	productTable.addCell(hsnCodeCell);
	productTable.addCell(UOMphraseCell);
	productTable.addCell(qtyPhraseCell);
	productTable.addCell(ratePhraseCell);
	productTable.addCell(amountPhraseCell);
	productTable.addCell(dicphraseCell);
	productTable.addCell(taxValPhraseCell);
	
	productTable.addCell(cgstphraseCell);
	productTable.addCell(sgstphraseCell);
	productTable.addCell(igstphraseCell);
	
	productTable.addCell(totalPhraseCell);
	
	productTable.addCell(cgstpercentphraseCell);
//	productTable.addCell(cgstamtphraseCell);
	productTable.addCell(cgstpercentphraseCell);
//	productTable.addCell(cgstamtphraseCell);
	productTable.addCell(cgstpercentphraseCell);
//	productTable.addCell(cgstamtphraseCell);

	try {
		document.add(productTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		logger.log(Level.SEVERE,"Error::"+e);
	}
}


private void createProductDetailsVal(){

	int firstBreakPoint = 5;
	float blankLines = 0;
	double totalAmount=0;
	if (delnoteEntity.getDeliveryItems().size() <= firstBreakPoint) {
		int size = firstBreakPoint
				- delnoteEntity.getDeliveryItems().size();
		blankLines = size * (100 / 5);
		System.out.println("blankLines size =" + blankLines);
	} else {
		blankLines = 10f;
	}
	
	PdfPTable productTable = new PdfPTable(13);
	productTable.setWidthPercentage(100);
	try {
		productTable.setWidths(column13CollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	for (int i = 0; i < delnoteEntity.getDeliveryItems().size(); i++) {

		if (i == 5) {
			break;
		}

		int srNoVal = i + 1;
		Phrase srNo = new Phrase(srNoVal + "", font7);
		PdfPCell srNoCell = new PdfPCell(srNo);
//		srNoCell.addElement();
		productTable.addCell(srNoCell);

		Phrase serviceName = new Phrase(delnoteEntity.getDeliveryItems().get(i).getProdName().trim(),
				font7);
		PdfPCell serviceNameCell = new PdfPCell();
		serviceNameCell.addElement(serviceName);
		productTable.addCell(serviceNameCell);

		Phrase hsnCode = null;
		if (delnoteEntity.getDeliveryItems().get(i).getPrduct().getHsnNumber() != null) {
			hsnCode = new Phrase(delnoteEntity.getDeliveryItems()
					.get(i).getPrduct().getHsnNumber().trim(), font7);
		} else {
			hsnCode = new Phrase("", font7);
		}
		PdfPCell hsnCodeCell = new PdfPCell();
		hsnCodeCell.addElement(hsnCode);
		productTable.addCell(hsnCodeCell);
		Phrase uom ;
//		if(delnoteEntity.getDeliveryItems()
//				.get(i).getUnitOfMeasurement().trim()!=null){
//			uom = new Phrase(delnoteEntity.getDeliveryItems()
//					.get(i).getUnitOfMeasurement().trim(), font10);
//		}else{
			uom = new Phrase("", font7);
//		}
		
		PdfPCell uomCell = new PdfPCell();
		uomCell.addElement(uom);
		productTable.addCell(uomCell);

		Phrase qty = new Phrase(delnoteEntity.getDeliveryItems()
				.get(i).getQuantity()
				+ "", font7);
		PdfPCell qtyCell = new PdfPCell();
		qtyCell.addElement(qty);
		productTable.addCell(qtyCell);

//		Phrase rate = new Phrase(df.format(delnoteEntity.getDeliveryItems().get(i).getPrice())
//				+ "", font10);
		Phrase rate = new Phrase("", font7);
		PdfPCell rateCell = new PdfPCell();
		rateCell.addElement(rate);
		productTable.addCell(rateCell);

		double amountValue = delnoteEntity.getDeliveryItems().get(i)
				.getPrice()
				* delnoteEntity.getDeliveryItems().get(i)
						.getQuantity();
		totalAmount = totalAmount + amountValue;
//		Phrase amount = new Phrase(df.format(amountValue) + "", font10);
		Phrase amount = new Phrase("", font7);
		PdfPCell amountCell = new PdfPCell();
		// amountCell.setBorder(0);
		// amountCell.setBorderWidthBottom(0);
		// amountCell.setBorderWidthTop(0);
		amountCell.addElement(amount);
		productTable.addCell(amountCell);

//		Phrase disc = new Phrase(df.format(invoiceentity
//				.getSalesOrderProducts().get(i).getFlatDiscount())
//				+ "", font10);
		Phrase disc = new Phrase("", font7);
		PdfPCell discCell = new PdfPCell();
		// discCell.setBorder(0);
		// discCell.setBorderWidthBottom(0);
		// discCell.setBorderWidthTop(0);
		discCell.addElement(disc);
		productTable.addCell(discCell);

//		Phrase taxableValue = new Phrase(df.format(invoiceentity
//				.getSalesOrderProducts().get(i).getBasePaymentAmount())
//				+ "", font10);
		Phrase taxableValue = new Phrase("", font7);
		PdfPCell taxableValueCell = new PdfPCell();
		// taxableValueCell.setBorder(0);
		// taxableValueCell.setBorderWidthBottom(0);
		// taxableValueCell.setBorderWidthTop(0);
		taxableValueCell.addElement(taxableValue);
		productTable.addCell(taxableValueCell);

		PdfPCell cellIGST;

		boolean taxPresent=validateTaxes(delnoteEntity.getDeliveryItems().get(i));
		if (taxPresent) {
			if (delnoteEntity.getDeliveryItems().get(i).getVatTax()
					.getTaxPrintName().equalsIgnoreCase("IGST")) {

//				double taxAmount = getTaxAmount(invoiceentity
//						.getSalesOrderProducts().get(i)
//						.getBasePaymentAmount(), invoiceentity
//						.getSalesOrderProducts().get(i).getVatTax()
//						.getPercentage());
//				double indivTotalAmount = invoiceentity
//						.getSalesOrderProducts().get(i)
//						.getBasePaymentAmount()
//						+ taxAmount;
//				totalAmount = totalAmount + indivTotalAmount;

//				Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
//						font10);

				Phrase igstRateVal = new Phrase("",
						font7);
				PdfPCell igstRateValCell = new PdfPCell();
				// igstRateValCell.setBorder(0);
				igstRateValCell.addElement(igstRateVal);

				Phrase igstRate = new Phrase(delnoteEntity.getDeliveryItems().get(i).getVatTax()
						.getPercentage()
						+ "", font7);
				PdfPCell igstRateCell = new PdfPCell();
				// igstRateCell.setBorder(0);
				// igstRateCell.setColspan(2);
				igstRateCell.addElement(igstRate);

				/* for Cgst */

				Phrase cgst = new Phrase(" ", font7);
				PdfPCell cell = new PdfPCell(cgst);
				cell.addElement(cgst);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(cell);
//				productTable.addCell(cell);

				/* for Sgst */
				Phrase sgst = new Phrase(" ", font7);
				productTable.addCell(cell);
//				productTable.addCell(cell);

				productTable.addCell(igstRateCell);
//				productTable.addCell(igstRateValCell);

//				Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
//						+ "", font10);
				Phrase totalPhrase = new Phrase("", font7);
				
				PdfPCell totalCell = new PdfPCell();
				// totalCell.setColspan(16);
				totalCell.addElement(totalPhrase);
				productTable.addCell(totalCell);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				
			}else if (delnoteEntity.getDeliveryItems().get(i).getServiceTax()
					.getTaxPrintName().equalsIgnoreCase("IGST")) {

//				double taxAmount = getTaxAmount(invoiceentity
//						.getSalesOrderProducts().get(i)
//						.getBasePaymentAmount(), invoiceentity
//						.getSalesOrderProducts().get(i).getServiceTax()
//						.getPercentage());
//				double indivTotalAmount = invoiceentity
//						.getSalesOrderProducts().get(i)
//						.getBasePaymentAmount()
//						+ taxAmount;
//				totalAmount = totalAmount + indivTotalAmount;

//				Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
//						font10);
//				PdfPCell igstRateValCell = new PdfPCell();
//				// igstRateValCell.setBorder(0);
//				igstRateValCell.addElement(igstRateVal);

				Phrase igstRate = new Phrase(delnoteEntity
						.getDeliveryItems().get(i).getServiceTax()
						.getPercentage()
						+ "", font7);
				PdfPCell igstRateCell = new PdfPCell();
				// igstRateCell.setBorder(0);
				// igstRateCell.setColspan(2);
				igstRateCell.addElement(igstRate);

				/* for Cgst */

				Phrase cgst = new Phrase("-", font7);
				PdfPCell cell = new PdfPCell(cgst);
				// cell.addElement(cgst);
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(cell);
//				productTable.addCell(cell);

				/* for Sgst */
				Phrase sgst = new Phrase("-", font7);
				productTable.addCell(cell);
//				productTable.addCell(cell);

				productTable.addCell(igstRateCell);
//				productTable.addCell(igstRateValCell);

//				Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
//						+ "", font10);
				Phrase totalPhrase = new Phrase("", font7);
				PdfPCell totalCell = new PdfPCell();
				// totalCell.setColspan(16);
				totalCell.addElement(totalPhrase);
				productTable.addCell(totalCell);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
//				String premisesVal = "";
//				for (int j = 0; j < con.getItems().size(); j++) {
//					if (delnoteEntity.getDeliveryItems().get(i)
//							.getProdId() == con.getItems().get(j)
//							.getPrduct().getCount()) {
//						premisesVal = con.getItems().get(j)
//								.getPremisesDetails();
//					}
//
//				}
//				if (printPremiseDetails) {
//					Phrase premisesValPhrs = new Phrase(
//							"Premise Details : " + premisesVal, font8);
//					PdfPCell premiseCell = new PdfPCell();
//					premiseCell.setColspan(16);
//					premiseCell.addElement(premisesValPhrs);
//
//					productTable.addCell(premiseCell);
//				}

			} else {

				if (delnoteEntity.getDeliveryItems().get(i)
						.getVatTax().getTaxPrintName()
						.equalsIgnoreCase("CGST")) {

//					double ctaxValue = getTaxAmount(delnoteEntity.getDeliveryItems().get(i)
//							.getBasePaymentAmount(), invoiceentity
//							.getSalesOrderProducts().get(i).getVatTax()
//							.getPercentage());

//					Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
//							+ "", font10);
//					PdfPCell cgstRateValCell = new PdfPCell();
//					// cgstRateValCell.setBorder(0);
//					// cgstRateValCell.setBorderWidthBottom(0);
//					// cgstRateValCell.setBorderWidthTop(0);
//					// cgstRateValCell.setBorderWidthRight(0);
//					cgstRateValCell.addElement(cgstRateVal);

					Phrase cgstRate = new Phrase(delnoteEntity.getDeliveryItems().get(i).getVatTax()
							.getPercentage()
							+ "", font7);
					PdfPCell cgstRateCell = new PdfPCell();
					// cgstRateCell.setBorder(0);
					// cgstRateCell.setBorderWidthBottom(0);
					// cgstRateCell.setBorderWidthTop(0);
					// cgstRateCell.setBorderWidthLeft(0);
					cgstRateCell.addElement(cgstRate);
					productTable.addCell(cgstRateCell);
//					productTable.addCell(cgstRateValCell);

//					double staxValue = getTaxAmount(invoiceentity
//							.getSalesOrderProducts().get(i)
//							.getBasePaymentAmount(), invoiceentity
//							.getSalesOrderProducts().get(i).getServiceTax()
//							.getPercentage());
//					Phrase sgstRateVal = new Phrase(df.format(staxValue)
//							+ "", font10);
//					PdfPCell sgstRateValCell = new PdfPCell();
//					// sgstRateValCell.setBorder(0);
//					// sgstRateValCell.setBorderWidthBottom(0);
//					// sgstRateValCell.setBorderWidthTop(0);
//					// sgstRateValCell.setBorderWidthRight(0);
//					sgstRateValCell.addElement(sgstRateVal);

					Phrase sgstRate = new Phrase(delnoteEntity.getDeliveryItems().get(i).getServiceTax()
							.getPercentage()
							+ "", font7);
					PdfPCell sgstRateCell = new PdfPCell();
					// sgstRateCell.setBorder(0);
					// sgstRateCell.setBorderWidthBottom(0);
					// sgstRateCell.setBorderWidthTop(0);
					// sgstRateCell.setBorderWidthLeft(0);
					sgstRateCell.addElement(sgstRate);
					productTable.addCell(sgstRateCell);
//					productTable.addCell(sgstRateValCell);

//					Phrase igstRateVal = new Phrase("-", font10);
//					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
//					// igstRateValCell.setBorder(0);
//					// igstRateValCell.setBorderWidthBottom(0);
//					// igstRateValCell.setBorderWidthTop(0);
//					// igstRateValCell.setBorderWidthRight(0);
//					// igstRateValCell.addElement(igstRateVal);
//					igstRateValCell
//							.setHorizontalAlignment(Element.ALIGN_RIGHT);
//					productTable.addCell(igstRateValCell);

					Phrase igstRate = new Phrase("-", font7);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					// igstRateCell.setBorder(0);
					// igstRateCell.setBorderWidthBottom(0);
					// igstRateCell.setBorderWidthTop(0);
					// igstRateCell.setBorderWidthLeft(0);
					// igstRateCell.addElement(igstRate);
					igstRateCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(igstRateCell);

//					double indivTotalAmount = invoiceentity
//							.getSalesOrderProducts().get(i)
//							.getBasePaymentAmount()
//							+ ctaxValue + staxValue;
//					totalAmount = totalAmount + indivTotalAmount;
					Phrase totalPhrase = new Phrase("", font7);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
//					String premisesVal = "";
//					for (int j = 0; j < con.getItems().size(); j++) {
//						if (delnoteEntity.getDeliveryItems().get(i)
//								.getProdId() == con.getItems().get(j)
//								.getPrduct().getCount()) {
//							premisesVal = con.getItems().get(j)
//									.getPremisesDetails();
//						}
//
//					}
//					if (printPremiseDetails) {
//						Phrase premisesValPhrs = new Phrase(
//								"Premise Details : " + premisesVal, font8);
//						PdfPCell premiseCell = new PdfPCell();
//						premiseCell.setColspan(16);
//						premiseCell.addElement(premisesValPhrs);
//						productTable.addCell(premiseCell);
//					}
					// try {
					// document.add(productTable);
					// } catch (DocumentException e) {
					// // TODO Auto-generated catch block
					// e.printStackTrace();
					// }

				} else if (delnoteEntity.getDeliveryItems().get(i)
						.getVatTax().getTaxPrintName()
						.equalsIgnoreCase("SGST")) {

//					double ctaxValue = getTaxAmount(invoiceentity
//							.getSalesOrderProducts().get(i)
//							.getBasePaymentAmount(), invoiceentity
//							.getSalesOrderProducts().get(i).getServiceTax()
//							.getPercentage());

//					Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
//							+ "", font10);
//					PdfPCell cgstRateValCell = new PdfPCell();
//					// cgstRateValCell.setBorder(0);
//					// cgstRateValCell.setBorderWidthBottom(0);
//					// cgstRateValCell.setBorderWidthTop(0);
//					// cgstRateValCell.setBorderWidthRight(0);
//					cgstRateValCell.addElement(cgstRateVal);

					Phrase cgstRate = new Phrase(delnoteEntity.getDeliveryItems().get(i).getVatTax()
							.getPercentage()
							+ "", font7);
					PdfPCell cgstRateCell = new PdfPCell();
					// cgstRateCell.setBorder(0);
					// cgstRateCell.setBorderWidthBottom(0);
					// cgstRateCell.setBorderWidthTop(0);
					// cgstRateCell.setBorderWidthLeft(0);
					cgstRateCell.addElement(cgstRate);
					productTable.addCell(cgstRateCell);
//					productTable.addCell(cgstRateValCell);

//					double staxValue = getTaxAmount(invoiceentity
//							.getSalesOrderProducts().get(i)
//							.getBasePaymentAmount(), invoiceentity
//							.getSalesOrderProducts().get(i).getVatTax()
//							.getPercentage());
//					Phrase sgstRateVal = new Phrase(df.format(staxValue)
//							+ "", font10);
//					PdfPCell sgstRateValCell = new PdfPCell();
//					// sgstRateValCell.setBorder(0);
//					// sgstRateValCell.setBorderWidthBottom(0);
//					// sgstRateValCell.setBorderWidthTop(0);
//					// sgstRateValCell.setBorderWidthRight(0);
//					sgstRateValCell.addElement(sgstRateVal);

					Phrase sgstRate = new Phrase(delnoteEntity.getDeliveryItems().get(i).getServiceTax()
							.getPercentage()
							+ "", font7);
					PdfPCell sgstRateCell = new PdfPCell();
					// sgstRateCell.setBorder(0);
					// sgstRateCell.setBorderWidthBottom(0);
					// sgstRateCell.setBorderWidthTop(0);
					// sgstRateCell.setBorderWidthLeft(0);
					sgstRateCell.addElement(sgstRate);
					productTable.addCell(sgstRateCell);
//					productTable.addCell(sgstRateValCell);

					Phrase igstRateVal = new Phrase("-", font7);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
//					productTable.addCell(igstRateValCell);

					Phrase igstRate = new Phrase("-", font7);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					// igstRateCell.setBorder(0);
					// igstRateCell.setBorderWidthBottom(0);
					// igstRateCell.setBorderWidthTop(0);
					// igstRateCell.setBorderWidthLeft(0);
					// igstRateCell.addElement(igstRate);
					igstRateCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
//					productTable.addCell(igstRateCell);

//					double indivTotalAmount = invoiceentity
//							.getSalesOrderProducts().get(i)
//							.getBasePaymentAmount()
//							+ ctaxValue + staxValue;
//					totalAmount = totalAmount + indivTotalAmount;
					Phrase totalPhrase = new Phrase("", font7);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);

					productTable.addCell(totalCell);
					String premisesVal = "";
//					for (int j = 0; j < con.getItems().size(); j++) {
//						if (delnoteEntity.getDeliveryItems().get(i)
//								.getProdId() == con.getItems().get(j)
//								.getPrduct().getCount()) {
//							premisesVal = con.getItems().get(j)
//									.getPremisesDetails();
//						}
//
//					}
//					if (printPremiseDetails) {
//						Phrase premisesValPhrs = new Phrase(
//								"Premise Details : " + premisesVal, font8);
//						PdfPCell premiseCell = new PdfPCell();
//						premiseCell.setColspan(16);
//						premiseCell.addElement(premisesValPhrs);
//						productTable.addCell(premiseCell);
//					}
				}
			}

		} else {
//			logger.log(Level.SEVERE,"Inside Tax Not Applicable");

			PdfPCell cell = new PdfPCell(new Phrase(" ", font7));
			productTable.addCell(cell);
//			productTable.addCell(cell);
			productTable.addCell(cell);
//			productTable.addCell(cell);
			productTable.addCell(cell);
//			productTable.addCell(cell);
			Phrase totalPhrase = new Phrase("", font7);
			PdfPCell totalCell = new PdfPCell();
			// totalCell.setColspan(16);
			// totalCell.setBorder(0);
			// totalCell.setBorderWidthBottom(0);
			// totalCell.setBorderWidthTop(0);
			totalCell.addElement(totalPhrase);
			productTable.addCell(totalCell);

//			String premisesVal = "";
//			for (int j = 0; j < con.getItems().size(); j++) {
//				if (delnoteEntity.getDeliveryItems().get(i)
//						.getProdId() == con.getItems().get(j).getPrduct()
//						.getCount()) {
//					premisesVal = con.getItems().get(j)
//							.getPremisesDetails();
//				}
//
//			}
//			if (printPremiseDetails) {
//				Phrase premisesValPhrs = new Phrase("Premise Details : "
//						+ premisesVal, font8);
//				PdfPCell premiseCell = new PdfPCell();
//				premiseCell.setColspan(16);
//				premiseCell.addElement(premisesValPhrs);
//				productTable.addCell(premiseCell);
//			}
		}
	}
	Phrase blankCell = new Phrase(" ", font7);
	PdfPCell productTableCell = null;
	if (delnoteEntity.getDeliveryItems().size() > firstBreakPoint) {
		Phrase my = new Phrase("Please Refer Annexure For More Details");
		productTableCell = new PdfPCell(my);

	} else {
		productTableCell = new PdfPCell(blankCell);
	}

	// PdfPCell productTableCell = new PdfPCell(blankCell);
	// productTableCell.setBorderWidthBottom(0);
	// productTableCell.setBorderWidthTop(0);
	productTableCell.setBorder(0);

	PdfPTable tab = new PdfPTable(1);
	tab.setWidthPercentage(100f);
	tab.addCell(productTableCell);
	// tab.addCell(premiseTblCell);
	tab.setSpacingAfter(blankLines);

	// last code for both table to be added in one table

	PdfPCell tab1;
	tab1 = new PdfPCell(productTable);
	// tab1.setBorder(0);

	PdfPCell tab2 = new PdfPCell(tab);
	// tab2.setBorder(0);

	PdfPTable mainTable = new PdfPTable(1);
	mainTable.setWidthPercentage(100f);
	mainTable.addCell(tab1);
	mainTable.addCell(tab2);

	try {
		document.add(mainTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

private boolean validateTaxes(DeliveryLineItems deliveryLineItems) {
	if(deliveryLineItems.getVatTax()
			.getPercentage() != 0){
		return true;
	}else{
		if(deliveryLineItems.getServiceTax().getPercentage()!=0){
			return true;
		}else{
			return false;
		}
	}
}


private void createFooterLastPart(String preprintStatus) {
	// TODO Auto-generated method stub
	PdfPTable bottomTable=new PdfPTable(2);
	bottomTable.setWidthPercentage(100);
	try {
		bottomTable.setWidths(columnMoreLeftWidths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	PdfPTable leftTable=new PdfPTable(1);
	leftTable.setWidthPercentage(100);
	
	
//	String amtInWordsVal="Amount in Words : Rupees "+SalesInvoicePdf.convert(invoiceentity.getNetPayable());
	Phrase amtInWordsValphrase=new Phrase("",font10bold);
	
	PdfPCell amtInWordsValCell=new PdfPCell(amtInWordsValphrase);
	amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	amtInWordsValCell.setBorderWidthTop(0);
	amtInWordsValCell.setBorderWidthLeft(0);
	amtInWordsValCell.setBorderWidthRight(0);
	leftTable.addCell(amtInWordsValCell);
	
	
	Phrase termNcond=new Phrase("Terms and Conditions:",font7bold);
	PdfPCell termNcondCell=new PdfPCell();
	termNcondCell.setBorder(0);
	termNcondCell.addElement(termNcond);
	
	Phrase termNcondVal=new Phrase(delnoteEntity.getDescription().trim(),font7bold);
	PdfPCell termNcondValCell=new PdfPCell();
	termNcondValCell.setBorder(0);
	termNcondValCell.addElement(termNcondVal);
	
	leftTable.addCell(termNcondCell);
	leftTable.addCell(termNcondValCell);
	
	//   rohan added this code for universal pest 
		if(!preprintStatus.equalsIgnoreCase("Plane")){
			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				
				if(comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")){
					
					Phrase articalType=new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue(),font10bold);
					PdfPCell articalTypeCell=new PdfPCell();
					articalTypeCell.setBorder(0);
					articalTypeCell.addElement(articalType);
					leftTable.addCell(articalTypeCell);
				}
			}
		}	
	PdfPCell leftCell=new PdfPCell();
	leftCell.addElement(leftTable);
	
	PdfPTable rightTable=new PdfPTable(1);
	rightTable.setWidthPercentage(100);
	
	PdfPTable innerRightTable=new PdfPTable(3);
	innerRightTable.setWidthPercentage(100);
	
	Phrase colon=new Phrase(" :",font7bold);
	PdfPCell colonCell=new PdfPCell();
	colonCell.setBorder(0);
	colonCell.addElement(colon);
	
	Phrase blank=new Phrase(" ",font7bold);
	PdfPCell blankCell=new PdfPCell();
	blankCell.setBorder(0);
	blankCell.addElement(blank);
	
	Phrase netPay=new Phrase("Net Payable",font7bold);
	
	PdfPCell netPayCell=new PdfPCell();
	netPayCell.setBorder(0);
	netPayCell.addElement(netPay);
	
	
	
	
	
	Phrase netPayVal=new Phrase("",font7bold);
//	Paragraph netPayPara=new Paragraph();
//	netPayPara.add(netPayVal);
//	netPayPara.setAlignment(Element.ALIGN_RIGHT);
	PdfPCell netPayValCell=new PdfPCell(netPayVal);
	netPayValCell.setBorder(0);
//	netPayValCell.addElement(netPayPara);
	netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase gstReverseCharge=new Phrase("GST Payable on Reverse Charge",font6bold);
	PdfPCell gstReverseChargeCell=new PdfPCell();
	gstReverseChargeCell.setBorder(0);
	gstReverseChargeCell.addElement(gstReverseCharge);
	
	Phrase gstReverseChargeVal=new Phrase(" ",font7);
	PdfPCell gstReverseChargeValCell=new PdfPCell();
	gstReverseChargeValCell.setBorder(0);
	gstReverseChargeValCell.addElement(gstReverseChargeVal);
	
	innerRightTable.addCell(netPayCell);
	innerRightTable.addCell(colonCell);
	innerRightTable.addCell(netPayValCell);
	innerRightTable.addCell(blankCell);
	innerRightTable.addCell(blankCell);
	innerRightTable.addCell(blankCell);
	innerRightTable.addCell(gstReverseChargeCell);
	innerRightTable.addCell(colonCell);
	innerRightTable.addCell(gstReverseChargeValCell);
	
	PdfPCell rightUpperCell=new PdfPCell();
	rightUpperCell.addElement(innerRightTable);
	rightUpperCell.setBorderWidthLeft(0);
	rightUpperCell.setBorderWidthRight(0);
	rightUpperCell.setBorderWidthTop(0);
	
	//  rohan added this code for universal pest and printing multiple company name  
	
	 String companyname ="";
	  if(multipleCompanyName)
		 {
		  if(salesOrderEntity.getGroup()!=null && !salesOrderEntity.getGroup().equals(""))
		  {
			  companyname =salesOrderEntity.getGroup().trim().toUpperCase();
		  }
		  else
		  {
			  companyname = comp.getBusinessUnitName().trim().toUpperCase();
		  }
		 
		 }
		 else
		 {
			 companyname = comp.getBusinessUnitName().trim().toUpperCase(); 
		 }
	
	//  ends here 
	
	rightTable.addCell(rightUpperCell);
	Phrase companyPhrase=new Phrase("For , "+companyname,font7bold);
	Paragraph companyPara=new Paragraph();
	companyPara.add(companyPhrase);
	companyPara.setAlignment(Element.ALIGN_CENTER);
	PdfPCell companyParaCell=new PdfPCell();
	companyParaCell.addElement(companyPara);
	companyParaCell.setBorder(0);
	
	rightTable.addCell(companyParaCell);
	rightTable.addCell(blankCell);
	rightTable.addCell(blankCell);
	rightTable.addCell(blankCell);
	Phrase signAuth=new Phrase("Authorised Signatory",font7bold);
	Paragraph signPara=new Paragraph();
	signPara.add(signAuth);
	signPara.setAlignment(Element.ALIGN_CENTER);
	PdfPCell signParaCell=new PdfPCell();
	signParaCell.addElement(signPara);
	signParaCell.setBorder(0);
	rightTable.addCell(signParaCell);
	
	PdfPCell lefttableCell=new PdfPCell();
	lefttableCell.addElement(leftTable);
	PdfPCell righttableCell=new PdfPCell();
	righttableCell.addElement(rightTable);
	
	bottomTable.addCell(lefttableCell);
	bottomTable.addCell(righttableCell);
	
	//
	
	Paragraph para = new Paragraph("Note : This is computer generated invoice no signature required.",font8);

	//
	try {
		document.add(bottomTable);
		document.add(para);
		if(delnoteEntity.getDeliveryItems().size() > 5 ){
			createAnnexureForRemainingProduct(5);
		}
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}

private void createAnnexureForRemainingProduct(int count) {

	Paragraph para = new Paragraph("Annexure 1 :", font10bold);
	para.setAlignment(Element.ALIGN_LEFT);

	try {
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEXTPAGE);
		document.add(para);
		document.add(Chunk.NEWLINE);

	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	createProductDetails();
	createProductDetailsMOreThanFive(count);
}


private void createProductDetailsMOreThanFive(int count) {
	
		for (int i = count; i < delnoteEntity.getDeliveryItems().size(); i++) {
			PdfPTable productTable = new PdfPTable(13);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column13CollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell();
			srNoCell.addElement(srNo);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(delnoteEntity.getDeliveryItems()
					.get(i).getProdName().trim(), font10);
			PdfPCell serviceNameCell = new PdfPCell();
			serviceNameCell.addElement(serviceName);
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;
			if (delnoteEntity.getDeliveryItems().get(i).getPrduct()
					.getHsnNumber() != null) {
				hsnCode = new Phrase(delnoteEntity.getDeliveryItems().get(i)
						.getPrduct().getHsnNumber().trim(), font10);
			} else {
				hsnCode = new Phrase("", font10);
			}
			PdfPCell hsnCodeCell = new PdfPCell();
			hsnCodeCell.addElement(hsnCode);
			productTable.addCell(hsnCodeCell);
			Phrase uom;
			// if(delnoteEntity.getDeliveryItems()
			// .get(i).getUnitOfMeasurement().trim()!=null){
			// uom = new Phrase(delnoteEntity.getDeliveryItems()
			// .get(i).getUnitOfMeasurement().trim(), font10);
			// }else{
			uom = new Phrase("", font10);
			// }

			PdfPCell uomCell = new PdfPCell();
			uomCell.addElement(uom);
			productTable.addCell(uomCell);

			Phrase qty = new Phrase(delnoteEntity.getDeliveryItems().get(i)
					.getQuantity()
					+ "", font10);
			PdfPCell qtyCell = new PdfPCell();
			qtyCell.addElement(qty);
			productTable.addCell(qtyCell);

			// Phrase rate = new
			// Phrase(df.format(delnoteEntity.getDeliveryItems().get(i).getPrice())
			// + "", font10);
			Phrase rate = new Phrase("", font10);
			PdfPCell rateCell = new PdfPCell();
			rateCell.addElement(rate);
			productTable.addCell(rateCell);

			double amountValue = delnoteEntity.getDeliveryItems().get(i)
					.getPrice()
					* delnoteEntity.getDeliveryItems().get(i).getQuantity();
//			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue) + "", font10);
			Phrase amount = new Phrase("", font10);
			PdfPCell amountCell = new PdfPCell();
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.addElement(amount);
			productTable.addCell(amountCell);

			// Phrase disc = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getFlatDiscount())
			// + "", font10);
			Phrase disc = new Phrase("", font10);
			PdfPCell discCell = new PdfPCell();
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.addElement(disc);
			productTable.addCell(discCell);

			// Phrase taxableValue = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getBasePaymentAmount())
			// + "", font10);
			Phrase taxableValue = new Phrase("", font10);
			PdfPCell taxableValueCell = new PdfPCell();
			// taxableValueCell.setBorder(0);
			// taxableValueCell.setBorderWidthBottom(0);
			// taxableValueCell.setBorderWidthTop(0);
			taxableValueCell.addElement(taxableValue);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;

			boolean taxPresent = validateTaxes(delnoteEntity.getDeliveryItems()
					.get(i));
			if (taxPresent) {
				if (delnoteEntity.getDeliveryItems().get(i).getVatTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					// double taxAmount = getTaxAmount(invoiceentity
					// .getSalesOrderProducts().get(i)
					// .getBasePaymentAmount(), invoiceentity
					// .getSalesOrderProducts().get(i).getVatTax()
					// .getPercentage());
					// double indivTotalAmount = invoiceentity
					// .getSalesOrderProducts().get(i)
					// .getBasePaymentAmount()
					// + taxAmount;
					// totalAmount = totalAmount + indivTotalAmount;

					// Phrase igstRateVal = new Phrase(df.format(taxAmount) +
					// "",
					// font10);

					Phrase igstRateVal = new Phrase("", font10);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(delnoteEntity
							.getDeliveryItems().get(i).getVatTax()
							.getPercentage()
							+ "", font10);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase(" ", font10);
					PdfPCell cell = new PdfPCell(cgst);
					cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					// productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase(" ", font10);
					productTable.addCell(cell);
					// productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					// productTable.addCell(igstRateValCell);

					// Phrase totalPhrase = new
					// Phrase(df.format(indivTotalAmount)
					// + "", font10);
					Phrase totalPhrase = new Phrase("", font10);

					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else if (delnoteEntity.getDeliveryItems().get(i)
						.getServiceTax().getTaxPrintName()
						.equalsIgnoreCase("IGST")) {

					// double taxAmount = getTaxAmount(invoiceentity
					// .getSalesOrderProducts().get(i)
					// .getBasePaymentAmount(), invoiceentity
					// .getSalesOrderProducts().get(i).getServiceTax()
					// .getPercentage());
					// double indivTotalAmount = invoiceentity
					// .getSalesOrderProducts().get(i)
					// .getBasePaymentAmount()
					// + taxAmount;
					// totalAmount = totalAmount + indivTotalAmount;

					// Phrase igstRateVal = new Phrase(df.format(taxAmount) +
					// "",
					// font10);
					// PdfPCell igstRateValCell = new PdfPCell();
					// // igstRateValCell.setBorder(0);
					// igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(delnoteEntity
							.getDeliveryItems().get(i).getServiceTax()
							.getPercentage()
							+ "", font10);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font10);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					// productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font10);
					productTable.addCell(cell);
					// productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					// productTable.addCell(igstRateValCell);

					// Phrase totalPhrase = new
					// Phrase(df.format(indivTotalAmount)
					// + "", font10);
					Phrase totalPhrase = new Phrase("", font10);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					// String premisesVal = "";
					// for (int j = 0; j < con.getItems().size(); j++) {
					// if (delnoteEntity.getDeliveryItems().get(i)
					// .getProdId() == con.getItems().get(j)
					// .getPrduct().getCount()) {
					// premisesVal = con.getItems().get(j)
					// .getPremisesDetails();
					// }
					//
					// }
					// if (printPremiseDetails) {
					// Phrase premisesValPhrs = new Phrase(
					// "Premise Details : " + premisesVal, font8);
					// PdfPCell premiseCell = new PdfPCell();
					// premiseCell.setColspan(16);
					// premiseCell.addElement(premisesValPhrs);
					//
					// productTable.addCell(premiseCell);
					// }
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {

					if (delnoteEntity.getDeliveryItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {

						// double ctaxValue =
						// getTaxAmount(delnoteEntity.getDeliveryItems().get(i)
						// .getBasePaymentAmount(), invoiceentity
						// .getSalesOrderProducts().get(i).getVatTax()
						// .getPercentage());

						// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
						// + "", font10);
						// PdfPCell cgstRateValCell = new PdfPCell();
						// // cgstRateValCell.setBorder(0);
						// // cgstRateValCell.setBorderWidthBottom(0);
						// // cgstRateValCell.setBorderWidthTop(0);
						// // cgstRateValCell.setBorderWidthRight(0);
						// cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(delnoteEntity
								.getDeliveryItems().get(i).getVatTax()
								.getPercentage()
								+ "", font10);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						// productTable.addCell(cgstRateValCell);

						// double staxValue = getTaxAmount(invoiceentity
						// .getSalesOrderProducts().get(i)
						// .getBasePaymentAmount(), invoiceentity
						// .getSalesOrderProducts().get(i).getServiceTax()
						// .getPercentage());
						// Phrase sgstRateVal = new Phrase(df.format(staxValue)
						// + "", font10);
						// PdfPCell sgstRateValCell = new PdfPCell();
						// // sgstRateValCell.setBorder(0);
						// // sgstRateValCell.setBorderWidthBottom(0);
						// // sgstRateValCell.setBorderWidthTop(0);
						// // sgstRateValCell.setBorderWidthRight(0);
						// sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(delnoteEntity
								.getDeliveryItems().get(i).getServiceTax()
								.getPercentage()
								+ "", font10);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						// productTable.addCell(sgstRateValCell);

						// Phrase igstRateVal = new Phrase("-", font10);
						// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// // igstRateValCell.setBorder(0);
						// // igstRateValCell.setBorderWidthBottom(0);
						// // igstRateValCell.setBorderWidthTop(0);
						// // igstRateValCell.setBorderWidthRight(0);
						// // igstRateValCell.addElement(igstRateVal);
						// igstRateValCell
						// .setHorizontalAlignment(Element.ALIGN_RIGHT);
						// productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font10);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						// double indivTotalAmount = invoiceentity
						// .getSalesOrderProducts().get(i)
						// .getBasePaymentAmount()
						// + ctaxValue + staxValue;
						// totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase("", font10);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setColspan(16);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);
						productTable.addCell(totalCell);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						// String premisesVal = "";
						// for (int j = 0; j < con.getItems().size(); j++) {
						// if (delnoteEntity.getDeliveryItems().get(i)
						// .getProdId() == con.getItems().get(j)
						// .getPrduct().getCount()) {
						// premisesVal = con.getItems().get(j)
						// .getPremisesDetails();
						// }
						//
						// }
						// if (printPremiseDetails) {
						// Phrase premisesValPhrs = new Phrase(
						// "Premise Details : " + premisesVal, font8);
						// PdfPCell premiseCell = new PdfPCell();
						// premiseCell.setColspan(16);
						// premiseCell.addElement(premisesValPhrs);
						// productTable.addCell(premiseCell);
						// }
						// try {
						// document.add(productTable);
						// } catch (DocumentException e) {
						// // TODO Auto-generated catch block
						// e.printStackTrace();
						// }
						try {
							document.add(productTable);
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else if (delnoteEntity.getDeliveryItems().get(i)
							.getVatTax().getTaxPrintName()
							.equalsIgnoreCase("SGST")) {

						// double ctaxValue = getTaxAmount(invoiceentity
						// .getSalesOrderProducts().get(i)
						// .getBasePaymentAmount(), invoiceentity
						// .getSalesOrderProducts().get(i).getServiceTax()
						// .getPercentage());

						// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
						// + "", font10);
						// PdfPCell cgstRateValCell = new PdfPCell();
						// // cgstRateValCell.setBorder(0);
						// // cgstRateValCell.setBorderWidthBottom(0);
						// // cgstRateValCell.setBorderWidthTop(0);
						// // cgstRateValCell.setBorderWidthRight(0);
						// cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(delnoteEntity
								.getDeliveryItems().get(i).getVatTax()
								.getPercentage()
								+ "", font10);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						// productTable.addCell(cgstRateValCell);

						// double staxValue = getTaxAmount(invoiceentity
						// .getSalesOrderProducts().get(i)
						// .getBasePaymentAmount(), invoiceentity
						// .getSalesOrderProducts().get(i).getVatTax()
						// .getPercentage());
						// Phrase sgstRateVal = new Phrase(df.format(staxValue)
						// + "", font10);
						// PdfPCell sgstRateValCell = new PdfPCell();
						// // sgstRateValCell.setBorder(0);
						// // sgstRateValCell.setBorderWidthBottom(0);
						// // sgstRateValCell.setBorderWidthTop(0);
						// // sgstRateValCell.setBorderWidthRight(0);
						// sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(delnoteEntity
								.getDeliveryItems().get(i).getServiceTax()
								.getPercentage()
								+ "", font10);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						// productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font10);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font10);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// productTable.addCell(igstRateCell);

						// double indivTotalAmount = invoiceentity
						// .getSalesOrderProducts().get(i)
						// .getBasePaymentAmount()
						// + ctaxValue + staxValue;
						// totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase("", font10);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setColspan(16);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);
						String premisesVal = "";
						// for (int j = 0; j < con.getItems().size(); j++) {
						// if (delnoteEntity.getDeliveryItems().get(i)
						// .getProdId() == con.getItems().get(j)
						// .getPrduct().getCount()) {
						// premisesVal = con.getItems().get(j)
						// .getPremisesDetails();
						// }
						//
						// }
						// if (printPremiseDetails) {
						// Phrase premisesValPhrs = new Phrase(
						// "Premise Details : " + premisesVal, font8);
						// PdfPCell premiseCell = new PdfPCell();
						// premiseCell.setColspan(16);
						// premiseCell.addElement(premisesValPhrs);
						// productTable.addCell(premiseCell);
						// }
						try {
							document.add(productTable);
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

			} else {
				// logger.log(Level.SEVERE,"Inside Tax Not Applicable");

				PdfPCell cell = new PdfPCell(new Phrase(" ", font10));
				productTable.addCell(cell);
				// productTable.addCell(cell);
				productTable.addCell(cell);
				// productTable.addCell(cell);
				productTable.addCell(cell);
				// productTable.addCell(cell);
				Phrase totalPhrase = new Phrase("", font10);
				PdfPCell totalCell = new PdfPCell();
				// totalCell.setColspan(16);
				// totalCell.setBorder(0);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				totalCell.addElement(totalPhrase);
				productTable.addCell(totalCell);

				// String premisesVal = "";
				// for (int j = 0; j < con.getItems().size(); j++) {
				// if (delnoteEntity.getDeliveryItems().get(i)
				// .getProdId() == con.getItems().get(j).getPrduct()
				// .getCount()) {
				// premisesVal = con.getItems().get(j)
				// .getPremisesDetails();
				// }
				//
				// }
				// if (printPremiseDetails) {
				// Phrase premisesValPhrs = new Phrase("Premise Details : "
				// + premisesVal, font8);
				// PdfPCell premiseCell = new PdfPCell();
				// premiseCell.setColspan(16);
				// premiseCell.addElement(premisesValPhrs);
				// productTable.addCell(premiseCell);
				// }
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	
}


private void createFooterTaxPart(){

	// TODO Auto-generated method stub
	PdfPTable pdfPTaxTable=new PdfPTable(2);
	pdfPTaxTable.setWidthPercentage(100);
	try {
		pdfPTaxTable.setWidths(columnMoreLeftWidths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	/**
	 * rohan added this code for payment terms for invoice details
	 */
	
	float[] column3widths = {2f,2f,6f};
	PdfPTable leftTable=new PdfPTable(3);
	leftTable.setWidthPercentage(100);
	try {
		leftTable.setWidths(column3widths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	PdfPTable rightTable=new PdfPTable(1);
	rightTable.setWidthPercentage(100);
	
	PdfPTable rightInnerTable=new PdfPTable(3);
	rightInnerTable.setWidthPercentage(100);
	try {
		rightInnerTable.setWidths(columnCollonGSTWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Phrase colon=new Phrase(":",font7bold);
	PdfPCell colonCell=new PdfPCell();
	colonCell.addElement(colon);
	colonCell.setBorder(0);
	
	Phrase amtB4Taxphrase=new Phrase("Total Amount Before Tax",font7bold);
	PdfPCell amtB4TaxCell=new PdfPCell();
	amtB4TaxCell.setBorder(0);
	amtB4TaxCell.addElement(amtB4Taxphrase);
	
	Phrase amtB4TaxValphrase=new Phrase(" ",font7bold);
	PdfPCell amtB4ValTaxCell=new PdfPCell();
	amtB4ValTaxCell.setBorder(0);
	amtB4ValTaxCell.addElement(amtB4TaxValphrase);
	
	double cgstTotalVal=0,sgstTotalVal=0,igstTotalVal=0;
	

	Phrase CGSTphrase=new Phrase("CGST",font7bold);
	PdfPCell CGSTphraseCell=new PdfPCell(CGSTphrase);
	CGSTphraseCell.setBorder(0);
//	CGSTphraseCell.addElement(CGSTphrase);
	
	Phrase CGSTValphrase=new Phrase(df.format(cgstTotalVal)+"",font7);
//	Paragraph CGSTValphrasePara=new Paragraph();
//	CGSTValphrasePara.add(CGSTValphrase);
//	CGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
	PdfPCell CGSTValphraseCell=new PdfPCell(CGSTValphrase);
	CGSTValphraseCell.setBorder(0);
	CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	CGSTValphraseCell.addElement(CGSTValphrasePara);

	Phrase SGSTphrase=new Phrase("SGST",font7bold);
	PdfPCell SGSTphraseCell=new PdfPCell(SGSTphrase);
	SGSTphraseCell.setBorder(0);
//	SGSTphraseCell.addElement(SGSTphrase);
	
	Phrase SGSTValphrase=new Phrase(df.format(sgstTotalVal)+"",font7);
//	Paragraph SGSTValphrasePara=new Paragraph();
//	SGSTValphrasePara.add(SGSTValphrase);
//	SGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
	PdfPCell SGSTValphraseCell=new PdfPCell(SGSTValphrase);
	SGSTValphraseCell.setBorder(0);
//	SGSTValphraseCell.addElement(SGSTValphrasePara);
	SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

	Phrase IGSTphrase=new Phrase("IGST",font7bold);
	PdfPCell IGSTphraseCell=new PdfPCell(IGSTphrase);
	IGSTphraseCell.setBorder(0);
//	IGSTphraseCell.addElement(IGSTphrase);
	
	Phrase IGSTValphrase=new Phrase(df.format(igstTotalVal)+"",font7);
//	Paragraph IGSTValphrasePara=new Paragraph();
//	IGSTValphrasePara.add(IGSTValphrase);
//	IGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
	PdfPCell IGSTValphraseCell=new PdfPCell(IGSTValphrase);
	IGSTValphraseCell.setBorder(0);
	IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	IGSTValphraseCell.addElement(IGSTValphrasePara);

	Phrase GSTphrase=new Phrase("Total GST",font7bold);
	PdfPCell GSTphraseCell=new PdfPCell(GSTphrase);
	GSTphraseCell.setBorder(0);
//	GSTphraseCell.addElement(GSTphrase);
	
	double totalGSTValue=igstTotalVal+cgstTotalVal+cgstTotalVal;
	Phrase GSTValphrase=new Phrase(df.format(totalGSTValue)+"",font7bold);
//	Paragraph GSTValphrasePara=new Paragraph();
//	GSTValphrasePara.add(GSTValphrase);
//	GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
	PdfPCell GSTValphraseCell=new PdfPCell(GSTValphrase);
	GSTValphraseCell.setBorder(0);
	GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	GSTValphraseCell.addElement(GSTValphrasePara);
	
	rightInnerTable.addCell(CGSTphraseCell);
	rightInnerTable.addCell(colonCell);
	rightInnerTable.addCell(CGSTValphraseCell);
	rightInnerTable.addCell(SGSTphraseCell);
	rightInnerTable.addCell(colonCell);
	rightInnerTable.addCell(SGSTValphraseCell);
	rightInnerTable.addCell(IGSTphraseCell);
	rightInnerTable.addCell(colonCell);
	rightInnerTable.addCell(IGSTValphraseCell);
	rightInnerTable.addCell(GSTphraseCell);
	rightInnerTable.addCell(colonCell);
	rightInnerTable.addCell(GSTValphraseCell);
	
	PdfPCell innerRightCell=new PdfPCell();
	innerRightCell.setBorder(0);
	innerRightCell.addElement(rightInnerTable);
	
	rightTable.addCell(innerRightCell);
	
	PdfPCell rightCell=new PdfPCell();
//	rightCell.setBorder(0);
	rightCell.addElement(rightTable);
	
	PdfPCell leftCell=new PdfPCell();
//	leftCell.setBorder(0);
	leftCell.addElement(leftTable);
	
	pdfPTaxTable.addCell(leftCell);
	pdfPTaxTable.addCell(rightCell);
	
	try {
		document.add(pdfPTaxTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}


}
