package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambServiceCardPdf 

{

	public Document document;
	Contract con;
	Company comp;
	Customer cust;
	Service ser;
	Invoice invoiceentity;
	List<ArticleType> articletype;
	public int j ;

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font9boldred, font9, font9red, font7, font7bold, font12boldul,
			font10boldul, font12, font16bold, font10, font10bold,
			font12boldred, font14bold, font11bold;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd MMM yyyy");
	
//	private SimpleDateFormat fmt1 = new SimpleDateFormat("hh-mm-ss"); 
	
	
	int bedbug = 0, termite = 0, woodborer = 0, flycontrol = 0, rodent = 0,gel=0,
			gipc = 0, other = 0 , dt =0 ;

	public PcambServiceCardPdf() {
		super();

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.RED);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,
				BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,
				BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

	}

	public void setContract(Long count) {	// load service
		
		con = ofy().load().type(Contract.class).id(count).now();

	// load Service
		
		if (con.getCompanyId() == null)
			
			ser = ofy().load().type(Service.class).first().now();
		else
			
		    ser = ofy().load().type(Service.class).filter("companyId", con.getCompanyId()).filter("contractCount", con.getCount()).first().now();
		
		// load Company
		
		if (con.getCompanyId() == null)

			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();


		// load Customer
		if(con.getCompanyId()==null)
			cust = ofy().load().type(Customer.class).filter("count", con.getCustomerId()).first().now();
			else
		    cust=ofy().load().type(Customer.class).filter("companyId", con.getCompanyId()).filter("count", con.getCustomerId()).first().now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
}
   
	public void createPdf() {
		
        for (int i=0 ; i< con.getItems().size(); i++)
			
		 {
        	createServiceCaredDetails(con.getItems().get(i).getProductCode());
        	try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
		 }
		
	}

	private void createServiceCaredDetails(String productCode) {  //  
		
        System.out.println("String productCode"+ productCode);
        
		Phrase myHeader1 = new Phrase(""+ comp.getBusinessUnitName().toUpperCase(), font11bold);
		Phrase myHeader2 = new Phrase("SERVICE CARD", font11bold);
		
		PdfPCell header1Cell = new PdfPCell(myHeader1);
		header1Cell.setBorder(0);
		header1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		header1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		PdfPCell header2Cell = new PdfPCell(myHeader2);
		header2Cell.setBorder(0);
		header2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		header2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		PdfPTable headertbl = new PdfPTable(1);
		headertbl.setWidthPercentage(100);
		headertbl.addCell(header1Cell);
		headertbl.addCell(header2Cell);

		// ///////////////////////// sevice details with customer info table added ////////////////////

		PdfPTable serviceDetailstbl = new PdfPTable(2);
		serviceDetailstbl.setWidthPercentage(100);
		try {
			serviceDetailstbl.setWidths(new float[] { 45, 55 });
		} catch (DocumentException e1) {

			e1.printStackTrace();
		}

		Phrase cardNo = new Phrase("CARD NO:-", font9bold);
		PdfPCell cardNoCell = new PdfPCell();
		cardNoCell.addElement(cardNo);
		cardNoCell.setBorder(0);
		serviceDetailstbl.addCell(cardNoCell);
		
		 ////////////////////////////// 4 digit card No logic added by Ajinkya on Date: 03/06/2017  /////////////////////////
		 
		 String strfrdigitCardNo = ""+ con.getCount()%10000 ; 
		 
		 if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==2){
			 strfrdigitCardNo = "00"+strfrdigitCardNo;
		 }
		 else if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==3){
			 strfrdigitCardNo = "0"+strfrdigitCardNo; 
		 }
		 System.out.println("4 digit " +strfrdigitCardNo);
		 
	//////////////////////////////4 digit card No logic end Here //////////////////////////
		

		 		String	 code = productCode;
				String crdNo = code +" / "+strfrdigitCardNo ;
				Phrase cardNoValue = new Phrase(crdNo, font9);
				
				PdfPCell cardNoValueCell = new PdfPCell();	
				cardNoValueCell.addElement(cardNoValue);
			    cardNoValueCell.setBorder(0);
				serviceDetailstbl.addCell(cardNoValueCell);
		
		Phrase contractNo = new Phrase("CONTRACT NO:-", font9bold);
		PdfPCell contractNoCell = new PdfPCell();
		contractNoCell.addElement(contractNo);
		contractNoCell.setBorder(0);
		serviceDetailstbl.addCell(contractNoCell);

		Phrase contractNoValue = new Phrase("" + con.getCount(), font9); // con.getCount(); check     
		PdfPCell contractNoValueCell = new PdfPCell();
		contractNoValueCell.addElement(contractNoValue);
		contractNoValueCell.setBorder(0);
		serviceDetailstbl.addCell(contractNoValueCell);     

		// 3
		Phrase occupant = new Phrase("OCCUPANT:- ", font9bold);   
		PdfPCell occupantCell = new PdfPCell();
		occupantCell.addElement(occupant);
		occupantCell.setBorder(0);
		serviceDetailstbl.addCell(occupantCell);
		
		PdfPCell occupantValueCell = new PdfPCell();
		
		  ///////////////////////////
			
		    String salutation1 = "" ;
		    
			if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
				salutation1 = cust.getSalutation();
			}
			else{  
				 salutation1 = "" ;
			}
			
			 String occupantValue1 = " " ;
			 //////////////////////////////////////////////
			
		if(ser!=null)
		{
		         if ( ser.getCustomerName()!=null && !ser.getCustomerName().equals("") && !ser.getCustomerName().equals(comp.getBusinessUnitName()))  
		           {
				  
			         occupantValue1 = salutation1+" "+ cust.getName() +"\n";
		           }  
		  
		         else if (ser.getCustomerName()!= null && !ser.getCustomerName().equals("") && !ser.getCustomerName().equals(cust.getName())){
		
		         occupantValue1 = salutation1 + ser.getCustomerName()+"\n";
                 }
		         else
		         {
        	      occupantValue1 = " "+ser.getCustomerName() +"\n";
                 }
		}
		else
		{
			  if ( con.getCustomerFullName()!=null && !con.getCustomerFullName().equals("") && !con.getCustomerFullName().equals(comp.getBusinessUnitName()))  
	           {
			  
		         occupantValue1 = salutation1+" "+ cust.getName() +"\n";
	           }  
	  
	           else if (con.getCustomerFullName()!= null && !con.getCustomerFullName().equals("") && !con.getCustomerFullName().equals(cust.getName())){
	
	              occupantValue1 = salutation1 + con.getCustomerFullName()+"\n";
               }
	          else
	          {
   	            occupantValue1 = " "+con.getCustomerFullName() +"\n";
              }
			
		}
		  
		  
			/*
			 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
			 * Used to take customer name from Uppercase to Only first letter Uppercase
			 */
			String nameInFirstLetterUpperCase=getFirstLetterUpperCase(occupantValue1.trim());
	                           ////////////////////// end here ///////////////// 
		
			 String addressline = "" ;
			  String localityline = "" ;
	   /////////////////////////////////
		   if(ser!=null){
		  
		  if(ser.getAddress().getAddrLine2() != null && !ser.getAddress().getAddrLine2().equals(""))   
			{
				addressline= " "+ser.getAddress().getAddrLine1()+", "+ser.getAddress().getAddrLine2()+", "+"\n";
			}
			else
			{
				addressline= ""+ser.getAddress().getAddrLine1()+", "+"\n";
			}
			
			if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
				System.out.println("inside both null condition1");
				localityline= ""+ (ser.getAddress().getLandmark()+", "+ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
					      +ser.getAddress().getPin()+". ");
			}
			else if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
				System.out.println("inside both null condition 2");
				localityline= ""+ (ser.getAddress().getLandmark()+", "+"\n"+ ser.getAddress().getCity()+" - "
					           +ser.getAddress().getPin()+". ");
			}
			
			else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
				System.out.println("inside both null condition 3");
				localityline= ""+(ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
					      +ser.getAddress().getPin()+". ");
			}
			else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
				System.out.println("inside both null condition 4");
				localityline= ""+(ser.getAddress().getCity()+" - "+ser.getAddress().getPin()+". ");
			}
			 Phrase occupantVal = new Phrase(nameInFirstLetterUpperCase+"\n"+ addressline + localityline , font9); //
			 occupantValueCell.addElement(occupantVal);
		     occupantValueCell.setBorder(0);
			 serviceDetailstbl.addCell(occupantValueCell);
	}
	else
	{

		////////////////////// rohan suggested that take this address from customer ////////////////////// 
	
//		take service addrsss from customer or contract
		if(cust.getSecondaryAdress().getAddrLine2() != null && !cust.getSecondaryAdress().getAddrLine2().equals(""))   
		{
			addressline=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2()+", "+"\n";
		}
		else
		{
			addressline= cust.getSecondaryAdress().getAddrLine1()+", "+"\n";
		}
		
		if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			localityline= (cust.getSecondaryAdress().getLandmark()+", "+cust.getSecondaryAdress().getLocality()+", "+"\n"+cust.getSecondaryAdress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n" +cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		else if((!cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			localityline= (cust.getSecondaryAdress().getLandmark()+", "+"\n"+ cust.getSecondaryAdress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		
		else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			localityline= (cust.getSecondaryAdress().getLocality()+", "+"\n"+cust.getSecondaryAdress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		else if((cust.getSecondaryAdress().getLandmark().equals(""))&&(cust.getSecondaryAdress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			localityline=(cust.getSecondaryAdress().getCity()+" - "+cust.getSecondaryAdress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		
		 Phrase occupantVal = new Phrase(nameInFirstLetterUpperCase + addressline + localityline , font9); //
		 occupantValueCell.addElement(occupantVal);
	     occupantValueCell.setBorder(0);
		 serviceDetailstbl.addCell(occupantValueCell);
		
	}
		  /////////////////////////////
// 4
		Phrase tel = new Phrase("TEL:-", font9bold);
		PdfPCell telCell = new PdfPCell();
		telCell.addElement(tel);
		telCell.setBorder(0);
		serviceDetailstbl.addCell(telCell);
            
		PdfPCell telValueCell = new PdfPCell();
		Phrase telValue = new Phrase("",font9);
		if( cust.getLandline()!= null && cust.getLandline()!=0)  
		{
			
			System.out.println("inside tel Value");
			telValue = new Phrase( cust.getCellNumber1() + " / " + cust.getLandline(), font9);
			
		}
		
		else {
			
			System.out.println("inside cell Value");
			 telValue = new Phrase( cust.getCellNumber1()+"", font9);
			
		}
		
		
		telValueCell.addElement(telValue); 
		telValueCell.setBorder(0);   
		serviceDetailstbl.addCell(telValueCell);
		
		// 5
		Phrase fax = new Phrase("FAX:-", font9bold);
		PdfPCell faxCell = new PdfPCell();
		faxCell.addElement(fax);
		faxCell.setBorder(0);
		serviceDetailstbl.addCell(faxCell); 
		
		PdfPCell faxValueCell = new PdfPCell();
		Phrase faxValue = new Phrase("",font9);
		
		if( cust.getFaxNumber()!= null && cust.getFaxNumber()!=0)  
		{
        	 faxValue = new Phrase("" + cust.getFaxNumber(), font9);
		}
		else {
			 faxValue = new Phrase(" ", font9);
        	
		}
		faxValueCell.addElement(faxValue);
		faxValueCell.setBorder(0);
		serviceDetailstbl.addCell(faxValueCell);

// 6
		Phrase email = new Phrase("E-MAIL :-", font9bold);
		PdfPCell emailCell = new PdfPCell();
		emailCell.addElement(email);
		emailCell.setBorder(0);
		serviceDetailstbl.addCell(emailCell);
		
      if(cust.getEmail()!= null && !cust.getEmail().equals("")&&!cust.getEmail().equalsIgnoreCase("abc@gmail.com")&&!cust.getEmail().equalsIgnoreCase("xyz@gmail.com"))
      {
    	  System.out.println("true mail ");
    	  Phrase emailValue = new Phrase("" + cust.getEmail(), font9);
  		PdfPCell emailValueCell = new PdfPCell();
  		emailValueCell.addElement(emailValue);
  		emailValueCell.setBorder(0);  
  		serviceDetailstbl.addCell(emailValueCell);
    	  
      }
      else if(cust.getEmail()!= null && !cust.getEmail().equals("")&&cust.getEmail().equalsIgnoreCase("abc@gmail.com")&& cust.getEmail().equalsIgnoreCase("xyz@gmail.com"))
      {
    	  System.out.println("dummy mail so not printed ");
    	  
    	  Phrase emailValue = new Phrase(" ", font9);
  		PdfPCell emailValueCell = new PdfPCell();
  		emailValueCell.addElement(emailValue);
  		emailValueCell.setBorder(0);  
  		serviceDetailstbl.addCell(emailValueCell);
    	  
      }
      else
      {
    	  System.out.println("else null email printed");  
    	  
    	    Phrase emailValue = new Phrase(" ", font9);
    		PdfPCell emailValueCell = new PdfPCell();
    		emailValueCell.addElement(emailValue);
    		emailValueCell.setBorder(0);  
    		serviceDetailstbl.addCell(emailValueCell);
    	  
      }
// 7
		Phrase contractExp = new Phrase("CONTRACT EXPIRES:-", font9bold);  
		PdfPCell contractExpCell = new PdfPCell();
		contractExpCell.addElement(contractExp);
		contractExpCell.setBorder(0);
		serviceDetailstbl.addCell(contractExpCell);
    System.out.println("contrac texpires: without format "+ con.getEndDate());
    System.out.println("contrac texpires: with format "+fmt.format(con.getEndDate()) );

    Phrase contractExpValue = new Phrase(""+ fmt.format(con.getEndDate()), font9);   
		PdfPCell contractExpValueCell = new PdfPCell();
		contractExpValueCell.addElement(contractExpValue);
		contractExpValueCell.setBorder(0);  
		serviceDetailstbl.addCell(contractExpValueCell);

// 8
		Phrase freq = new Phrase("FREQUENCY:-", font9bold);
		PdfPCell freqCell = new PdfPCell(freq);
		freqCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		freqCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqCell.setBorder(0);
		serviceDetailstbl.addCell(freqCell);  
		     
        ////////////////////////////////
        
  	  String ferqVal= null ;
  	  
//  	   if(){
//	    for(int i=0;i<con.getItems().size();i++)
//	    {
	    	
	    if(productCode.equalsIgnoreCase("WB")||productCode.equalsIgnoreCase("BT")
	    	||productCode.equalsIgnoreCase("GIPC")||productCode.equalsIgnoreCase("GT")
	    	||productCode.equalsIgnoreCase("FLY")||productCode.equalsIgnoreCase("MO")
	    	||productCode.equalsIgnoreCase("GST")||productCode.equalsIgnoreCase("WA")
	    	||productCode.equalsIgnoreCase("DT")||productCode.equalsIgnoreCase("FT")
	    	||productCode.equalsIgnoreCase("BMT")||productCode.equalsIgnoreCase("SP")
	    	||productCode.equalsIgnoreCase("RC")
	    	)
	    {
	    	 ferqVal= " Treatment " ;
	    }
	    
	   if( productCode.equalsIgnoreCase("WB00")||productCode.equalsIgnoreCase("BT00")
		||productCode.equalsIgnoreCase("GIPC00")||productCode.equalsIgnoreCase("GT00")
		||productCode.equalsIgnoreCase("FLY00")||productCode.equalsIgnoreCase("MO00")
		||productCode.equalsIgnoreCase("GST00")||productCode.equalsIgnoreCase("WA00")
		||productCode.equalsIgnoreCase("DT00")||productCode.equalsIgnoreCase("FT00")
		||productCode.equalsIgnoreCase("BMT00")||productCode.equalsIgnoreCase("SP00")
		||productCode.equalsIgnoreCase("RC00")
		)
	{   
		   ferqVal= " As & When " ;
		
	}
	   
	   if( productCode.equalsIgnoreCase("WB365")||productCode.equalsIgnoreCase("BT")
				||productCode.equalsIgnoreCase("GIPC365")||productCode.equalsIgnoreCase("GT365")
				||productCode.equalsIgnoreCase("FLY365")||productCode.equalsIgnoreCase("MO365")
				||productCode.equalsIgnoreCase("GST365")||productCode.equalsIgnoreCase("WA365")
				||productCode.equalsIgnoreCase("DT365")||productCode.equalsIgnoreCase("FT365")
				||productCode.equalsIgnoreCase("BMT365")||productCode.equalsIgnoreCase("SP365")
				||productCode.equalsIgnoreCase("RC365")
				)
			{
		      ferqVal= " Daily" ;
			}
	   
	   if(productCode.equalsIgnoreCase("WB52")|productCode.equalsIgnoreCase("BT52")
				||productCode.equalsIgnoreCase("GIPC52")||productCode.equalsIgnoreCase("GT52")
				||productCode.equalsIgnoreCase("FLY52")||productCode.equalsIgnoreCase("MO52")
				||productCode.equalsIgnoreCase("GST52")||productCode.equalsIgnoreCase("WA52")
				||productCode.equalsIgnoreCase("DT52")||productCode.equalsIgnoreCase("FT52")
				||productCode.equalsIgnoreCase("BMT52")||productCode.equalsIgnoreCase("SP52")
				||productCode.equalsIgnoreCase("RC52")
				)
			{
		         ferqVal= " Weeekly " ;	
			}
	   
	   if( productCode.equalsIgnoreCase("WB36")||productCode.equalsIgnoreCase("BT36")
				||productCode.equalsIgnoreCase("GIPC36")||productCode.equalsIgnoreCase("GT36")
				||productCode.equalsIgnoreCase("FLY36")||productCode.equalsIgnoreCase("MO36")
				||productCode.equalsIgnoreCase("GST36")||productCode.equalsIgnoreCase("WA36")
				||productCode.equalsIgnoreCase("DT36")||productCode.equalsIgnoreCase("FT36")
				||productCode.equalsIgnoreCase("BMT36")||productCode.equalsIgnoreCase("SP36")
				||productCode.equalsIgnoreCase("RC36")
				)
			{
		        ferqVal= " Twice the Month " ;		
			}
	   
	   if( productCode.equalsIgnoreCase("WB24")||productCode.equalsIgnoreCase("BT24")
				||productCode.equalsIgnoreCase("GIPC24")||productCode.equalsIgnoreCase("GT24")
				||productCode.equalsIgnoreCase("FLY24")||productCode.equalsIgnoreCase("MO24")
				||productCode.equalsIgnoreCase("GST24")||productCode.equalsIgnoreCase("WA24")
				||productCode.equalsIgnoreCase("DT24")||productCode.equalsIgnoreCase("FT24")
				||productCode.equalsIgnoreCase("BMT24")||productCode.equalsIgnoreCase("SP24")
				||productCode.equalsIgnoreCase("RC24")
				)
			{
		        ferqVal= " Four Nightly " ;   
			}
	   
	   
	   if( productCode.equalsIgnoreCase("WB12")||productCode.equalsIgnoreCase("BT12")
				||productCode.equalsIgnoreCase("GIPC12")||productCode.equalsIgnoreCase("GT12")
				||productCode.equalsIgnoreCase("FLY12")||productCode.equalsIgnoreCase("MO12")
				||productCode.equalsIgnoreCase("GST12")||productCode.equalsIgnoreCase("WA12")
				||productCode.equalsIgnoreCase("DT12")||productCode.equalsIgnoreCase("FT12")
				||productCode.equalsIgnoreCase("BMT12")||productCode.equalsIgnoreCase("SP12")
				||productCode.equalsIgnoreCase("RC12")
				)
			{
		      ferqVal= " Monthly" ;
			}
	   
	   if( productCode.equalsIgnoreCase("WB06")||productCode.equalsIgnoreCase("BT06")
				||productCode.equalsIgnoreCase("GIPC06")||productCode.equalsIgnoreCase("GT06")
				||productCode.equalsIgnoreCase("FLY06")||productCode.equalsIgnoreCase("MO06")
				||productCode.equalsIgnoreCase("GST06")||productCode.equalsIgnoreCase("WA06")
				||productCode.equalsIgnoreCase("DT06")||productCode.equalsIgnoreCase("FT06")
				||productCode.equalsIgnoreCase("BMT06")||productCode.equalsIgnoreCase("SP06")
				||productCode.equalsIgnoreCase("RC06")
				)
			{
		      ferqVal= " Alternate Month" ;
			}
	   
	   if(productCode.equalsIgnoreCase("WB04")||productCode.equalsIgnoreCase("BT04")
				||productCode.equalsIgnoreCase("GIPC04")||productCode.equalsIgnoreCase("GT04")
				||productCode.equalsIgnoreCase("FLY04")||productCode.equalsIgnoreCase("MO04")
				||productCode.equalsIgnoreCase("GST04")||productCode.equalsIgnoreCase("WA04")
				||productCode.equalsIgnoreCase("DT04")||productCode.equalsIgnoreCase("FT04")
				||productCode.equalsIgnoreCase("BMT04")||productCode.equalsIgnoreCase("SP04")
				||productCode.equalsIgnoreCase("RC04")
				)
			{
		         ferqVal= " Quarterly " ;
			}
	   
	   if( productCode.equalsIgnoreCase("WB03")||productCode.equalsIgnoreCase("BT03")
				||productCode.equalsIgnoreCase("GIPC03")|productCode.equalsIgnoreCase("GT03")
				||productCode.equalsIgnoreCase("FLY03")||productCode.equalsIgnoreCase("MO03")
				||productCode.equalsIgnoreCase("GST03")||productCode.equalsIgnoreCase("WA03")
				||productCode.equalsIgnoreCase("DT03")||productCode.equalsIgnoreCase("FT03")
				||productCode.equalsIgnoreCase("BMT03")||productCode.equalsIgnoreCase("SP03")
				||productCode.equalsIgnoreCase("RC03")
				)
			{
		          ferqVal= " Four Monthtly " ;	
			}
	   
	   if( productCode.equalsIgnoreCase("WB02")||productCode.equalsIgnoreCase("BT02")
				||productCode.equalsIgnoreCase("GIPC02")||productCode.equalsIgnoreCase("GT02")
				||productCode.equalsIgnoreCase("FLY02")||productCode.equalsIgnoreCase("MO02")
				||productCode.equalsIgnoreCase("GST02")||productCode.equalsIgnoreCase("WA02")
				||productCode.equalsIgnoreCase("DT02")||productCode.equalsIgnoreCase("FT02")
				||productCode.equalsIgnoreCase("BMT02")||productCode.equalsIgnoreCase("SP02")
				||productCode.equalsIgnoreCase("RC02")
				)
			{
		        ferqVal= " Six Monthly " ;	
			}
//	    }
//	}
	    
  //////////////////////////////////////////////frequency value end here   //////////////////////////////////// 
        
		  /////////////////////////////
	    
	    Phrase ferqValphrase = new Phrase (ferqVal,font9);
		PdfPCell freqValueCell = new PdfPCell(ferqValphrase);
		freqValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		freqValueCell.setBorder(0);
		serviceDetailstbl.addCell(freqValueCell);

// 9
		Phrase daytime = new Phrase("DAY & TIME :-", font9bold);
		PdfPCell daytimeCell = new PdfPCell(daytime);
		daytimeCell.setBorder(0);
		serviceDetailstbl.addCell(daytimeCell);
		
		////////////////////////////  week code  strat //////////////////////////
			
		// rohan added this code for week in service schedule  on date : 25-04-2017
		String week = getWeekFromService();
	
		Phrase daytimeValue = new Phrase(" " + ser.getServiceDateDay()+" " + week +" WEEK "+ ser.getServiceTime(), font9);
		
		System.out.println("day " + ser.getServiceDateDay() +  "time "+ ser.getServiceTime() );
		
		PdfPCell daytimeValueCell = new PdfPCell(daytimeValue);
		daytimeValueCell.setBorder(0);
		serviceDetailstbl.addCell(daytimeValueCell);
		
		// 10
		PdfPTable contactPresonTbl = new PdfPTable(2);
		contactPresonTbl.setWidthPercentage(100);

		Phrase contactPerson = new Phrase("CONTACT PERSON :-", font9bold);  
		PdfPCell contactPersonCell = new PdfPCell();
		contactPersonCell.addElement(contactPerson);
		contactPersonCell.setBorder(0);
		contactPresonTbl.addCell(contactPersonCell);
		
		////////////////
		   String salutation = "" ;
			if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
				salutation = cust.getSalutation();
			}  
			else{
				 salutation = "" ;
			}
		
		///////////////
			String serCustName = "";	
		if(ser!=null)
		{	
		             if (ser.getCustomerName() == comp.getBusinessUnitName())
		                {
			              serCustName =  ser.getCustomerName() ;
			              System.out.println("Service Cust name "+ser.getCustomerName());
		                }
		             else 
		                {
			              serCustName =  salutation  +" "+ ser.getPersonInfo().getPocName() ;
			              System.out.println("Service Cust POC "+ser.getPersonInfo().getPocName());
		                }
		}
		else 
		{
			   if (con.getCustomerFullName() == comp.getBusinessUnitName())
               {
	              serCustName =  con.getCustomerFullName() ;
	              System.out.println("Service Cust name "+con.getCustomerFullName());
               }
            else 
               {
	              serCustName =  salutation  +" "+ con.getCinfo().getPocName() ;
	              System.out.println("Service Cust POC "+con.getCinfo().getPocName());
               }
			
		}
        
		String nameInFirstLetterUpperCase1=getFirstLetterUpperCase(serCustName.trim());
		Phrase contactPersonValue = new Phrase( nameInFirstLetterUpperCase1, font9);  
		PdfPCell contactPersonValueCell = new PdfPCell();
		contactPersonValueCell.addElement(contactPersonValue);
		contactPersonValueCell.setBorder(0);
		contactPresonTbl.addCell(contactPersonValueCell);
		
		
		try 
		{
			contactPresonTbl.setWidths(new float[] { 50, 50 });

		}
		catch (DocumentException e3)
		{
			e3.printStackTrace();
		}


		PdfPCell contactPresonTblCell = new PdfPCell();
		contactPresonTblCell.addElement(contactPresonTbl);
		contactPresonTblCell.setBorder(0);
		serviceDetailstbl.addCell(contactPresonTblCell);

		PdfPTable contactPresonMobNoTbl = new PdfPTable(2);
		contactPresonMobNoTbl.setWidthPercentage(100);

		Phrase contactMobNo = new Phrase("Mobile No:", font9bold);
		PdfPCell contactMobNoCell = new PdfPCell();
		contactMobNoCell.addElement(contactMobNo);
		contactMobNoCell.setBorder(0);
		contactPresonMobNoTbl.addCell(contactMobNoCell);

		if (ser.getCustomerName() == comp.getBusinessUnitName()) {

			Phrase contactMobNoVal = new Phrase("" + ser.getCellNumber(), font9);
			PdfPCell contactMobNoValCell = new PdfPCell();
			contactMobNoValCell.addElement(contactMobNoVal);
			contactMobNoValCell.setBorder(0);
			contactPresonMobNoTbl.addCell(contactMobNoValCell);
		}
		else 
		   {
			Phrase contactMobNoVal = new Phrase("" + comp.getPocCell(), font9);
			PdfPCell contactMobNoValCell = new PdfPCell();
			contactMobNoValCell.addElement(contactMobNoVal);
			contactMobNoValCell.setBorder(0);
			contactPresonMobNoTbl.addCell(contactMobNoValCell);
		   }

		try {
			contactPresonMobNoTbl.setWidths(new float[] { 50, 50 });

     		} catch (DocumentException e3) {
			e3.printStackTrace();
		    }

		PdfPCell contactPresonMobNoTblCell = new PdfPCell();
		contactPresonMobNoTblCell.addElement(contactPresonMobNoTbl);
		contactPresonMobNoTblCell.setBorder(0);
		serviceDetailstbl.addCell(contactPresonMobNoTblCell);

		// 11
		Phrase premise = new Phrase("PREMISE:-", font9bold);
		PdfPCell premiseCell = new PdfPCell();
		premiseCell.addElement(premise);
		premiseCell.setBorder(0);
		serviceDetailstbl.addCell(premiseCell);
		
		PdfPCell premiseValueCell = new PdfPCell();
		String premises = "";
		////////////////////////////// Premises value ////////////////////
		Phrase premiseValue = new Phrase("  " , font9);
		
		for (int i = 0; i < con.getItems().size();i++)
		{
         if( con.getItems().get(i).getProductCode()== productCode)
            {
	          premises =  con.getItems().get(i).getPremisesDetails();
	          
	        if(!premises.equals(null) && !premises.equals(""))   
               { 
		        premiseValue = new Phrase(premises, font9);
               }
	        else  
	           {
	    	    premiseValue = new Phrase(" ", font9);
	           }
            }
}
        ////////////////////////////// End Here //////////////////////////		
		
		premiseValueCell.addElement(premiseValue);
		premiseValueCell.setBorder(0);    
		serviceDetailstbl.addCell(premiseValueCell);
//		
		// 12
		
		Phrase remarks = new Phrase("REMARKS :-", font9bold);
		PdfPCell remarksCell = new PdfPCell();
		remarksCell.addElement(remarks);
		remarksCell.setBorder(0);
		serviceDetailstbl.addCell(remarksCell);
		
		
		PdfPCell remarksValueCell = new PdfPCell();
		Phrase remarksValue = new Phrase(" ", font9);
      
    	   for (int i = 0; i < con.getItems().size();i++)
    			{
    	   if( con.getItems().get(i).getProductCode()== productCode)
    	   {
    		   String remark =  con.getItems().get(i).getRemark(); 
    		   if(!remark.equals(null) && !remark.equals(""))   
    	        { 
    			   remarksValue = new Phrase(remark, font9);
    	        }
               else  
                   {
        	        remarksValue = new Phrase(" ", font9);
                   }
    	   }
	  }
    	remarksValueCell.addElement(remarksValue);   
		remarksValueCell.setBorder(0);
		serviceDetailstbl.addCell(remarksValueCell);
		
		PdfPTable pdTbl = new PdfPTable(1);
		pdTbl.setWidthPercentage(100);

		PdfPCell headerTblCell = new PdfPCell();
		headerTblCell.addElement(headertbl);
		headerTblCell.setBorder(0);

		PdfPCell serviceDetailstblCell = new PdfPCell();
		serviceDetailstblCell.addElement(serviceDetailstbl);
		serviceDetailstblCell.setBorder(0);

		pdTbl.addCell(headerTblCell);
		pdTbl.addCell(serviceDetailstblCell);

		PdfPCell pdTblCell = new PdfPCell();
		pdTblCell.addElement(pdTbl);
		pdTblCell.setBorder(0);


		PdfPTable emptyTbl = new PdfPTable(4);
		emptyTbl.setWidthPercentage(100);

		try {
			emptyTbl.setWidths(new float[] { 25, 25, 25, 25 });
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}

		for (int i = 0; i < 16; i++) {
			Phrase emptyphrse = new Phrase("   ", font9);
			PdfPCell empty1Cell = new PdfPCell();
			empty1Cell.addElement(emptyphrse);
			empty1Cell.setBorderWidthLeft(0);
			emptyTbl.addCell(empty1Cell);

			PdfPCell empty2Cell = new PdfPCell();
			empty2Cell.addElement(emptyphrse);
			emptyTbl.addCell(empty2Cell);

			PdfPCell empty3Cell = new PdfPCell();
			empty3Cell.addElement(emptyphrse);
			emptyTbl.addCell(empty3Cell);

			PdfPCell empty4Cell = new PdfPCell();
			empty4Cell.addElement(emptyphrse);
			empty4Cell.setBorderWidthRight(0);
			emptyTbl.addCell(empty4Cell);

		}

		PdfPCell srviceCardTblCell = new PdfPCell();

//		srviceCardTblCell.addElement(titleTbl);
//		srviceCardTblCell.addElement(emptyTbl);
		srviceCardTblCell.setBorder(0);

		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		parentTbl.addCell(srviceCardTblCell);
		parentTbl.addCell(pdTblCell);

		try {
			parentTbl.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {

			e1.printStackTrace();
		}

		try {

			document.add(parentTbl);
			// document.add(Chunk.NEXTPAGE);

		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	// //////////////////////////////////////2nd Page Added by Ajinkya  29/12/2016 ////////////////////////////////////////////////////////

	  //Ajinkya added this code to convert customer name in CamelCase    
	//  Date : 12/4/2017

private String getFirstLetterUpperCase(String customerFullName)

{
	System.out.println("customerFullName "+customerFullName);
	String customerName="";
	String[] customerNameSpaceSpilt=customerFullName.split(" ");
	System.out.println("customerNameSpaceSpilt"+ customerNameSpaceSpilt.length);
	int count=0;
	for (String name : customerNameSpaceSpilt) 
	{
		String nameLowerCase=name.toLowerCase();
		System.out.println("nameLowerCase"+ nameLowerCase);   
		if(count==0)
		{
			customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			System.out.println("customerName"+customerName);
		}
		else
		{
			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			System.out.println("customerName"+customerName);
		}
		count=count+1;  
	}
	return customerName;    
}

	private void createServiceCaredRemarksForTMTbl() 
	{
		PdfPTable blnkTable = new  PdfPTable(1);
		blnkTable.setWidthPercentage(100);
		
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.setBorder(0);
		blnkTable.addCell(blnkCell);
		
		
		try {
			document.add(blnkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
//	{
//
//		Phrase srdate = new Phrase("DATE", font9);
//		Paragraph srdatePara = new Paragraph();
//		srdatePara.add(srdate);
//		srdatePara.add(Chunk.NEWLINE);
//		srdatePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell srDateCell = new PdfPCell();
//		srDateCell.addElement(srdatePara);
//		srDateCell.setBorderWidthLeft(0);
//
//		PdfPTable detailstbl = new PdfPTable(1);
//		detailstbl.setWidthPercentage(100);
//
//		Phrase title = new Phrase(" ", font9);
//		Paragraph titlePara = new Paragraph();
//		titlePara.add(title);
//		titlePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell titleCell = new PdfPCell();
//		titleCell.addElement(titlePara);
//		titleCell.setBorderWidthRight(0);
//		titleCell.setBorderWidthTop(0);
//		titleCell.setBorderWidthLeft(0);
//
//		Phrase no1 = new Phrase("7 NO.", font9);
//		Paragraph no1Para = new Paragraph();
//		no1Para.add(no1);
//		no1Para.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell no1Cell = new PdfPCell();
//		no1Cell.addElement(no1Para);
//		no1Cell.setBorderWidthBottom(0);
//		no1Cell.setBorderWidthTop(0);
//		no1Cell.setBorderWidthLeft(0);
//
//		Phrase no2 = new Phrase("18 NO.", font9);
//		Paragraph no2Para = new Paragraph();
//		no2Para.add(no2);
//		no2Para.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell no2Cell = new PdfPCell();
//		no2Cell.addElement(no2Para);
//		no2Cell.setBorderWidthBottom(0);
//		no2Cell.setBorderWidthTop(0);
//		// no2Cell.setBorderWidthRight(0);
//
//		Phrase nphrase = new Phrase("N", font9);
//		Paragraph nphrasePara = new Paragraph();
//		nphrasePara.add(nphrase);
//		nphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nphraseCell = new PdfPCell();
//		nphraseCell.addElement(nphrasePara);
//		nphraseCell.setBorderWidthBottom(0);
//		nphraseCell.setBorderWidthTop(0);
//		// nphraseCell.setBorderWidthRight(0);
//
//		Phrase hphrase = new Phrase("H", font9);
//		Paragraph hphrasePara = new Paragraph();
//		hphrasePara.add(hphrase);
//		hphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell hphraseCell = new PdfPCell();
//		hphraseCell.addElement(hphrasePara);
//		hphraseCell.setBorderWidthBottom(0);
//		hphraseCell.setBorderWidthTop(0);
//		hphraseCell.setBorderWidthRight(0);
//		// outTimeCell.setBorder(0);
//
//		PdfPTable NoTitleCellTbl = new PdfPTable(4);
//		NoTitleCellTbl.setWidthPercentage(100);
//
//		try {
//			NoTitleCellTbl.setWidths(new float[] { 25, 25, 25, 25 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		NoTitleCellTbl.addCell(no1Cell);
//		NoTitleCellTbl.addCell(no2Cell);
//		NoTitleCellTbl.addCell(nphraseCell);
//		NoTitleCellTbl.addCell(hphraseCell);
//
//		PdfPCell noValueCell = new PdfPCell();
//
//		noValueCell.addElement(NoTitleCellTbl);
//		noValueCell.setBorder(0);
//
//		detailstbl.addCell(titleCell);
//		detailstbl.addCell(noValueCell);
//
//		PdfPCell noCell = new PdfPCell();
//		noCell.addElement(detailstbl);
//
//		Phrase nameofworker = new Phrase("NAME OF WORKER", font9);
//		Paragraph nameofworkerPara = new Paragraph();
//		nameofworkerPara.add(nameofworker);
//		nameofworkerPara.add(Chunk.NEWLINE);
//		nameofworkerPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nameofworkerCell = new PdfPCell();
//		nameofworkerCell.addElement(nameofworkerPara);
//
//		Phrase remarks = new Phrase("REMARKS", font9);
//		Paragraph remarksPara = new Paragraph();
//		remarksPara.add(remarks);
//		remarksPara.add(Chunk.NEWLINE);
//		remarksPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell remarksCell = new PdfPCell();
//		remarksCell.addElement(remarksPara);
//		remarksCell.setBorderWidthRight(0);
//
//		PdfPTable titleTbl = new PdfPTable(4);
//		titleTbl.setWidthPercentage(100);
//
//		try {
//			titleTbl.setWidths(new float[] { 18, 32, 20, 30 });
//
//		} catch (DocumentException e1) {
//
//			e1.printStackTrace();
//		}
//
//		titleTbl.addCell(srDateCell);
//		titleTbl.addCell(noCell);
//		titleTbl.addCell(nameofworkerCell);
//		titleTbl.addCell(remarksCell);
//
//		PdfPTable emptyTbl = new PdfPTable(7);
//		emptyTbl.setWidthPercentage(100);
//
//		try {
//			emptyTbl.setWidths(new float[] { 18, 8, 8, 8, 8, 20, 30 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		for (int i = 0; i < 16; i++) {
//			Phrase emptyphrse = new Phrase("   ", font9);
//			PdfPCell empty1Cell = new PdfPCell();
//			empty1Cell.addElement(emptyphrse);
//			empty1Cell.setBorderWidthLeft(0);
//			emptyTbl.addCell(empty1Cell);
//
//			PdfPCell empty2Cell = new PdfPCell();
//			empty2Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty2Cell);
//
//			PdfPCell empty3Cell = new PdfPCell();
//			empty3Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty3Cell);
//
//			PdfPCell empty4Cell = new PdfPCell();
//			empty4Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty4Cell);
//
//			PdfPCell empty5Cell = new PdfPCell();
//			empty5Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty5Cell);
//
//			PdfPCell empty6Cell = new PdfPCell();
//			empty6Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty6Cell);
//
//			PdfPCell empty7Cell = new PdfPCell();
//			empty7Cell.addElement(emptyphrse);
//			empty7Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty7Cell);
//
//		}
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//			document.add(titleTbl);
//			document.add(emptyTbl);
//			document.add(Chunk.NEXTPAGE);
//
//		} catch (DocumentException e) {
//
//			e.printStackTrace();
//		}
//
//	}
//}
	private void createServiceCaredRemarksForRCTbl()
	{
		PdfPTable blnkTable = new  PdfPTable(1);
		blnkTable.setWidthPercentage(100);
		
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.setBorder(0);
		blnkTable.addCell(blnkCell);
		
		try {
			document.add(blnkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	{
//		
//		
//
//		Phrase srdate = new Phrase("DATE", font9);
//		Paragraph srdatePara = new Paragraph();
//		srdatePara.add(srdate);
//		srdatePara.add(Chunk.NEWLINE);
//		srdatePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell srDateCell = new PdfPCell();
//		srDateCell.addElement(srdatePara);
//		srDateCell.setBorderWidthLeft(0);
//
//		PdfPTable detailstbl = new PdfPTable(1);
//		detailstbl.setWidthPercentage(100);
//
//		Phrase title = new Phrase(" ", font9);
//		Paragraph titlePara = new Paragraph();
//		titlePara.add(title);
//		titlePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell titleCell = new PdfPCell();
//		titleCell.addElement(titlePara);
//		titleCell.setBorderWidthRight(0);
//		titleCell.setBorderWidthTop(0);
//		titleCell.setBorderWidthLeft(0);
//
//		Phrase traps = new Phrase("Traps", font8);
//		PdfPCell trapsCell = new PdfPCell(traps);
//		trapsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		trapsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		trapsCell.setBorderWidthBottom(0);
//		trapsCell.setBorderWidthTop(0);
////		trapsCell.setBorderWidthLeft(0);
//
//		Phrase baits = new Phrase("Baits", font8);
//		
//		PdfPCell baitsCell = new PdfPCell(baits);
//		baitsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		baitsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		baitsCell.setBorderWidthBottom(0);
//		baitsCell.setBorderWidthTop(0);
//		// .setBorderWidthRight(0);
//
//		Phrase cakes = new Phrase("Cakes", font8);
//		PdfPCell cakesCell = new PdfPCell(cakes);
//		cakesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cakesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		cakesCell.setBorderWidthBottom(0);
//		cakesCell.setBorderWidthTop(0);
//		// .setBorderWidthRight(0);
//
//		Phrase glueBrd = new Phrase("Glue Board", font8);
//		
//		PdfPCell glueBrdCell = new PdfPCell();
//		glueBrdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cakesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		glueBrdCell.setBorderWidthBottom(0);
//		glueBrdCell.setBorderWidthTop(0);
////		glueBrdCell.setBorderWidthRight(0);
//		// .setBorder(0);
//		
//		Phrase blnkphrase = new Phrase("  ", font8);
//		PdfPCell blnkphraseCell = new PdfPCell(blnkphrase);
//		blnkphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		blnkphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		blnkphraseCell.setBorderWidthBottom(0);
//		blnkphraseCell.setBorderWidthTop(0);
//		blnkphraseCell.setBorderWidthRight(0);
//		// .setBorder(0);
//
//		PdfPTable NoTitleCellTbl = new PdfPTable(5);
//		NoTitleCellTbl.setWidthPercentage(100);
//
//		try
//		{
//			NoTitleCellTbl.setWidths(new float[] { 20, 20, 20, 20 ,20});
//		}
//		catch (DocumentException e2)
//		{
//			e2.printStackTrace();
//		}
//
//		NoTitleCellTbl.addCell(trapsCell);
//		NoTitleCellTbl.addCell(baitsCell);
//		NoTitleCellTbl.addCell(cakesCell);
//		NoTitleCellTbl.addCell(glueBrdCell);
//		NoTitleCellTbl.addCell(blnkphraseCell);
//
//		PdfPCell noValueCell = new PdfPCell();
//		noValueCell.addElement(NoTitleCellTbl);
//		noValueCell.setBorder(0);
//
//		detailstbl.addCell(titleCell);
//		detailstbl.addCell(noValueCell);
//
//		PdfPCell noCell = new PdfPCell();
//		noCell.addElement(detailstbl);
//
//		Phrase nameofworker = new Phrase("NAME OF WORKER", font9);
//		Paragraph nameofworkerPara = new Paragraph();
//		nameofworkerPara.add(nameofworker);
//		nameofworkerPara.add(Chunk.NEWLINE);
//		nameofworkerPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nameofworkerCell = new PdfPCell();
//		nameofworkerCell.addElement(nameofworkerPara);
//
//		Phrase remarks = new Phrase("REMARKS", font9);
//		Paragraph remarksPara = new Paragraph();
//		remarksPara.add(remarks);
//		remarksPara.add(Chunk.NEWLINE);
//		remarksPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell remarksCell = new PdfPCell();
//		remarksCell.addElement(remarksPara);
//		remarksCell.setBorderWidthRight(0);
//
//		PdfPTable titleTbl = new PdfPTable(4);
//		titleTbl.setWidthPercentage(100);
//
//		try {
//			titleTbl.setWidths(new float[] { 15, 35, 20, 30 });
//
//		} catch (DocumentException e1) {
//
//			e1.printStackTrace();
//		}
//                                                  
//		titleTbl.addCell(srDateCell);
//		titleTbl.addCell(noCell);
//		titleTbl.addCell(nameofworkerCell);
//		titleTbl.addCell(remarksCell);
//
//		PdfPTable emptyTbl = new PdfPTable(8);
//		emptyTbl.setWidthPercentage(100);
//
//		try {
//			emptyTbl.setWidths(new float[] { 15, 7, 7, 7, 7,7, 20, 30 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		for (int i = 0; i < 16; i++) {
//			Phrase emptyphrse = new Phrase("   ", font9);
//			PdfPCell empty1Cell = new PdfPCell();
//			empty1Cell.addElement(emptyphrse);
//			empty1Cell.setBorderWidthLeft(0);
//			emptyTbl.addCell(empty1Cell);
//
//			PdfPCell empty2Cell = new PdfPCell();
//			empty2Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty2Cell);
//
//			PdfPCell empty3Cell = new PdfPCell();
//			empty3Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty3Cell);
//
//			PdfPCell empty4Cell = new PdfPCell();
//			empty4Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty4Cell);
//
//			PdfPCell empty5Cell = new PdfPCell();
//			empty5Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty5Cell);
//
//			PdfPCell empty6Cell = new PdfPCell();
//			empty6Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty6Cell);
//
//			PdfPCell empty7Cell = new PdfPCell();
//			empty7Cell.addElement(emptyphrse);
//			empty7Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty7Cell);
//			
//			PdfPCell empty8Cell = new PdfPCell();
//			empty8Cell.addElement(emptyphrse);
//			empty8Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty8Cell);
//
//
//		}
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//			document.add(titleTbl);
//			document.add(emptyTbl);
//			document.add(Chunk.NEXTPAGE);
//
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	
//		
//	}
//}
	private void createServiceCaredRemarksForGIPCAndGelTbl()
	{
		PdfPTable blnkTable = new  PdfPTable(1);
		blnkTable.setWidthPercentage(100);
		
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.setBorder(0);
		blnkTable.addCell(blnkCell);
		
		try {
			document.add(blnkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	{
//
//		
//		Phrase srdate = new Phrase("DATE", font9);
//		Paragraph srdatePara = new Paragraph();
//		srdatePara.add(srdate);
//		srdatePara.add(Chunk.NEWLINE);
//		srdatePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell srDateCell = new PdfPCell();
//		srDateCell.addElement(srdatePara);
//		srDateCell.setBorderWidthLeft(0);
//
//		PdfPTable detailstbl = new PdfPTable(1);
//		detailstbl.setWidthPercentage(100);
//
//		Phrase title = new Phrase(" ", font9);
//		Paragraph titlePara = new Paragraph();
//		titlePara.add(title);
//		titlePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell titleCell = new PdfPCell();
//		titleCell.addElement(titlePara);
//		titleCell.setBorderWidthRight(0);
//		titleCell.setBorderWidthTop(0);
//		titleCell.setBorderWidthLeft(0);
//
//		Phrase two = new Phrase("2", font9);    
//		Paragraph twoPara = new Paragraph();
//		twoPara.add(two);
//		twoPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell twoCell = new PdfPCell();
//		twoCell.addElement(twoPara);
//		twoCell.setBorderWidthBottom(0);
//		twoCell.setBorderWidthTop(0);
//		twoCell.setBorderWidthLeft(0);
//
//		Phrase eight = new Phrase("8", font9);
//		Paragraph eightPara = new Paragraph();
//		eightPara.add(eight);
//		eightPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell eightCell = new PdfPCell();
//		eightCell.addElement(eightPara);
//		eightCell.setBorderWidthBottom(0);
//		eightCell.setBorderWidthTop(0);
//		// no2Cell.setBorderWidthRight(0);
//
//		Phrase nphrase = new Phrase("N", font9);
//		Paragraph nphrasePara = new Paragraph();
//		nphrasePara.add(nphrase);   
//		nphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nphraseCell = new PdfPCell();
//		nphraseCell.addElement(nphrasePara);
//		nphraseCell.setBorderWidthBottom(0);
//		nphraseCell.setBorderWidthTop(0);
//		// nphraseCell.setBorderWidthRight(0);
//
//		Phrase kmphrase = new Phrase("K/M", font9);
//		Paragraph kmphrasePara = new Paragraph();
//		kmphrasePara.add(kmphrase);
//		kmphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell kmphraseCell = new PdfPCell();
//		kmphraseCell.addElement(kmphrasePara);
//		kmphraseCell.setBorderWidthBottom(0);
//		kmphraseCell.setBorderWidthTop(0);
//		kmphraseCell.setBorderWidthRight(0);
//		// outTimeCell.setBorder(0);
//		
//		Phrase paste = new Phrase("Paste", font9);
//		Paragraph pastePara = new Paragraph();
//		pastePara.add(paste);
//		pastePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell pasteCell = new PdfPCell();
//		pasteCell.addElement(pastePara);
//		pasteCell.setBorderWidthBottom(0);
//		pasteCell.setBorderWidthTop(0);
//		pasteCell.setBorderWidthRight(0);
//		// outTimeCell.setBorder(0);
//
//		PdfPTable NoTitleCellTbl = new PdfPTable(5);
//		NoTitleCellTbl.setWidthPercentage(100);
//
//		try {
//			NoTitleCellTbl.setWidths(new float[] { 20, 20, 20, 20, 20 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		NoTitleCellTbl.addCell(twoCell);
//		NoTitleCellTbl.addCell(eightCell);
//		NoTitleCellTbl.addCell(nphraseCell);
//		NoTitleCellTbl.addCell(kmphraseCell);
//		NoTitleCellTbl.addCell(pasteCell);
//
//		PdfPCell noValueCell = new PdfPCell();
//
//		noValueCell.addElement(NoTitleCellTbl);
//		noValueCell.setBorder(0);
//
//		detailstbl.addCell(titleCell);
//		detailstbl.addCell(noValueCell);
//
//		PdfPCell noCell = new PdfPCell();
//		noCell.addElement(detailstbl);
//
//		Phrase nameofworker = new Phrase("NAME OF WORKER", font9);
//		Paragraph nameofworkerPara = new Paragraph();
//		nameofworkerPara.add(nameofworker);
//		nameofworkerPara.add(Chunk.NEWLINE);
//		nameofworkerPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nameofworkerCell = new PdfPCell();
//		nameofworkerCell.addElement(nameofworkerPara);
//
//		Phrase remarks = new Phrase("REMARKS", font9);
//		Paragraph remarksPara = new Paragraph();
//		remarksPara.add(remarks);
//		remarksPara.add(Chunk.NEWLINE);
//		remarksPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell remarksCell = new PdfPCell();
//		remarksCell.addElement(remarksPara);
//		remarksCell.setBorderWidthRight(0);
//
//		PdfPTable titleTbl = new PdfPTable(4);
//		titleTbl.setWidthPercentage(100);
//
//		try {
//			titleTbl.setWidths(new float[] { 15, 35, 20, 30 });
//
//		} catch (DocumentException e1) {
//
//			e1.printStackTrace();
//		}
//		                                          
//		titleTbl.addCell(srDateCell);
//		titleTbl.addCell(noCell);
//		titleTbl.addCell(nameofworkerCell);
//		titleTbl.addCell(remarksCell);
//
//		PdfPTable emptyTbl = new PdfPTable(8);
//		emptyTbl.setWidthPercentage(100);
//
//		try {
//			emptyTbl.setWidths(new float[] { 15, 7, 7, 7, 7,7, 20, 30 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		for (int i = 0; i < 16; i++) {
//			Phrase emptyphrse = new Phrase("   ", font9);
//			PdfPCell empty1Cell = new PdfPCell();
//			empty1Cell.addElement(emptyphrse);
//			empty1Cell.setBorderWidthLeft(0);
//			emptyTbl.addCell(empty1Cell);
//
//			PdfPCell empty2Cell = new PdfPCell();
//			empty2Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty2Cell);
//
//			PdfPCell empty3Cell = new PdfPCell();
//			empty3Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty3Cell);
//
//			PdfPCell empty4Cell = new PdfPCell();
//			empty4Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty4Cell);
//
//			PdfPCell empty5Cell = new PdfPCell();
//			empty5Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty5Cell);
//
//			PdfPCell empty6Cell = new PdfPCell();
//			empty6Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty6Cell);
//
//			PdfPCell empty7Cell = new PdfPCell();
//			empty7Cell.addElement(emptyphrse);
//			empty7Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty7Cell);
//			
//			PdfPCell empty8Cell = new PdfPCell();
//			empty8Cell.addElement(emptyphrse);
//			empty8Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty8Cell);
//
//		}
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//			document.add(titleTbl);
//			document.add(emptyTbl);
//			document.add(Chunk.NEXTPAGE);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//}
	
	private String getWeekFromService() 
	{
		
		String week ="";
		
		if(ser.getServiceWeekNo()==1){
			week=ser.getServiceWeekNo()+" ST";
		}else if(ser.getServiceWeekNo()==2){
			week=ser.getServiceWeekNo()+" ND";
		}else if(ser.getServiceWeekNo()==3){
			week=ser.getServiceWeekNo()+" RD";
		}else{
			week=ser.getServiceWeekNo()+" TH";
		}
		
		return week;
	}
	
}
