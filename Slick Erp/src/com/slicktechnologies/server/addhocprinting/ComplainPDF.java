package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.servlet.http.HttpServlet;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class ComplainPDF extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1176017772694696046L;

	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 11);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
	Font font12Ul = new Font(Font.FontFamily.HELVETICA, 12, Font.UNDERLINE);


	public Document document;
	Complain complain;
	Company comp;
	Customer cust;
	Branch branchEntity;
	Employee employee;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat decimalformat = new DecimalFormat("0.00");
	
	PdfPCell cellBlank;
	
//	float[] column8SerProdCollonWidth = { 0.67f, 0.68f, 0.35f, 0.28f,
//			0.50f, 0.50f };
	
	float[] column8SerProdCollonWidth = { 0.72f, 0.73f, 0.35f, 0.38f,
			0.80f};
	boolean branchAsCompanyFlag = false;
	
	PdfUtility pdfUtility = new PdfUtility();
	
	
	public void setComplain(Long count) {

		complain=ofy().load().type(Complain.class).id(count).now();
		
		if (complain.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", complain.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		
		if (complain.getCompanyId() != null)
		{
			cust = ofy().load().type(Customer.class).filter("count", complain.getPersoninfo().getCount()).filter("companyId", complain.getCompanyId()).first().now();
		}
		
		
		branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", complain.getBranch()).filter("companyId", complain.getCompanyId()).first().now();
//		employee = ofy().load().type(Employee.class).filter("fullname", quotation.getEmployee()).filter("companyId", quotation.getCompanyId()).first().now();
				
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Phrase phblank = new Phrase(" ",font6);
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
		cellBlank=new PdfPCell(phblank);
		cellBlank.setBorder(0);
		
		branchAsCompanyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId());
		
		try {
			BaseFont verdanafont = BaseFont.createFont("verdana.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
			font12 = new Font(verdanafont, 11);
			font12bold = new Font(verdanafont, 11, Font.BOLD);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void createPdf(String preprintStatus) {
		
		
		createTicketInfo();
		
	}


	private void createTicketInfo() {


		PdfPTable ticketTable = new PdfPTable(9);
		ticketTable.setWidthPercentage(100);
		ticketTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		float[] ticketTableColumnWidth = { 0.5f, 1f, 2f, 0.5f,1f };
//		float[] ticketTableColumnWidth = { 0.5f, 0.5f, 0.5f, 0.5f,0.5f,0.5f,0.5f,0.5f,0.5f};
		float[] ticketTableColumnWidth = { 0.6f, 0.7f, 0.7f, 0.4f,0.3f,0.3f,0.3f,0.3f,0.7f};

		try {
			ticketTable.setWidths(ticketTableColumnWidth);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Service serviceEntity = ofy().load().type(Service.class).filter("companyId", complain.getCompanyId()).filter("ticketNumber", complain.getCount()).first().now();
		if(serviceEntity!=null){
			
		}

		ticketTable.addCell(pdfUtility.getPdfCell("Ticket ID", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,1,1,1,1,3,3,0,0));
		ticketTable.addCell(pdfUtility.getPdfCell(complain.getCount()+"", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,1,1,-1,1,3,3,0,0));
		ticketTable.addCell(pdfUtility.getPdfCell(fmt.format(complain.getDate()), font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,1,1,-1,1,3,3,0,0));
		
		ticketTable.addCell(pdfUtility.getPdfCell("Status", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,1,1,-1,1,3,3,0,0));
		ticketTable.addCell(pdfUtility.getPdfCell(complain.getCompStatus(), font11, Element.ALIGN_CENTER,null, 0, 2, 0,0,1,1,-1,1,3,3,0,0));
		
		if(serviceEntity!=null && serviceEntity.getCompletedDate()!=null){
			ticketTable.addCell(pdfUtility.getPdfCell("Closure Date", font11, Element.ALIGN_LEFT,null, 0, 2, 0,0,1,1,-1,1,1,2,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(fmt.format(complain.getNewServiceDate()), font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,1,1,-1,1,1,1,0,0));
		}
		else{
			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_LEFT,null, 0, 3, 0,0,1,1,-1,1,1,2,0,0));
		}
		

		String category = "";
		if(complain.getCategory()!=null){
			category = complain.getCategory();
		}
		if(!category.equals("")){
			
			ticketTable.addCell(pdfUtility.getPdfCell("Category", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,1,1,1,1,4,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(category, font11, Element.ALIGN_CENTER,null, 0, 2, 0,0,0,1,-1,1,1,1,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));

			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell("", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));

		}
		String contractId = "";
		
		if(complain.getExistingContractId()!=null && complain.getExistingContractId()!=0){
			contractId = complain.getExistingContractId()+"";
		}
		if(!contractId.equals("")){
			ticketTable.addCell(pdfUtility.getPdfCell("Contract Id", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,1,1,1,1,4,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(complain.getExistingContractId()+"", font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,0,0));

			ticketTable.addCell(pdfUtility.getPdfCell("Service Name", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,1,-1,1,1,4,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(complain.getPic().getProductName(), font11, Element.ALIGN_LEFT,null, 0, 3, 0,0,0,1,-1,1,1,3,0,0));
			
		}
		else{
			ticketTable.addCell(pdfUtility.getPdfCell("Service Name", font11, Element.ALIGN_LEFT,null, 0, 0, 0,0,0,1,1,1,1,4,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(complain.getPic().getProductName(), font11, Element.ALIGN_LEFT,null, 0, 5, 0,0,0,1,-1,1,1,3,4,0));
		}
		
		ticketTable.addCell(pdfUtility.getPdfCell("Service Date", font11, Element.ALIGN_LEFT,null, 0, 2, 0,0,0,1,-1,1,1,4,0,0));
		ticketTable.addCell(pdfUtility.getPdfCell(fmt.format(complain.getNewServiceDate()), font11, Element.ALIGN_CENTER,null, 0, 0, 0,0,0,1,-1,1,1,1,4,0));

		String callerEmailMobile = "";
		if(complain.getCallerName()!=null && !complain.getCallerName().equals("")){
			callerEmailMobile = complain.getCallerName()+" / "; 
		}
		if(complain.getEmail()!=null && !complain.getEmail().equals("")){
			callerEmailMobile += complain.getEmail()+" / ";
		}
		if(complain.getCallerCellNo()!=0 ){
			callerEmailMobile += complain.getCallerCellNo();
		}
		if(!callerEmailMobile.equals("")){
			ticketTable.addCell(pdfUtility.getPdfCell("Caller / email / Mobile ", font11, Element.ALIGN_LEFT,null, 0, 2, 0,0,0,1,1,1,1,4,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(callerEmailMobile, font11, Element.ALIGN_CENTER,null, 0, 7, 0,0,0,1,-1,1,1,1,0,0));
		}
		
		String brandModelSerialNumber = "";
		if(complain.getBrandName()!=null && !complain.getBrandName().equals("")){
			brandModelSerialNumber = complain.getBrandName()+" / ";
		}
		if(complain.getModelNumber()!=null && !complain.getModelNumber().equals("")){
			brandModelSerialNumber +=complain.getModelNumber();
		}
		if(complain.getProSerialNo()!=null && !complain.getProSerialNo().equals("")){
			brandModelSerialNumber += " / "+complain.getProSerialNo();
		}
		
		if(!brandModelSerialNumber.equals("")){
			ticketTable.addCell(pdfUtility.getPdfCell("Brand / Model  / Serial Number", font11, Element.ALIGN_LEFT,null, 0, 2, 0,0,0,1,1,1,1,4,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(brandModelSerialNumber, font11, Element.ALIGN_CENTER,null, 0, 7, 0,0,0,1,-1,1,1,1,0,0));
		
		}
		
		if(complain.getRemark()!=null && !complain.getRemark().equals("")){
			ticketTable.addCell(pdfUtility.getPdfCell("Remark", font11, Element.ALIGN_CENTER,null, 0, 2, 100,0,0,1,1,1,15,1,0,0));
			ticketTable.addCell(pdfUtility.getPdfCell(complain.getRemark(), font11, Element.ALIGN_LEFT,null, 0, 7, 0,0,0,1,-1,1,5,1,0,0));
		}

		try {
			document.add(ticketTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
