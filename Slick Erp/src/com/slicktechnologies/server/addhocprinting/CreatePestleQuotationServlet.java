package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
@SuppressWarnings("serial")
public class CreatePestleQuotationServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6865794037587742946L;

	

	
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
		 
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.

	  try {
		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		  
		  String preprintStatus=request.getParameter("preprint");
	  	   System.out.println("****************"+preprintStatus);
		  
		  String quotgroup=request.getParameter("group");
			 System.out.println("quotgroup"+quotgroup);
			 
			  if(quotgroup.contains("Normal Quotation")){
				  
				  PestleQuotationPdf pdf=new PestleQuotationPdf();
				  pdf.document=new Document();
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
			  
			   document.open();
			   
			   pdf.getPestleQuotation(count);
			   if(preprintStatus.contains("yes")){
					 
					   pdf.createPdf(preprintStatus);
				 }
			   else
			   {
				   pdf.createPdf(preprintStatus);
			   }
			   document.close();
			  }
			  
			  else if(quotgroup.contains("Five Years Quotation")){
				  
				  
				  /**************2nd PDF****************/
				  		  
				  		  PestleFiveYearsQuotation pdffive=new PestleFiveYearsQuotation();
				  		  pdffive.document=new Document();
				  		  Document document = pdffive.document;
				  		  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
				  	  
				  	   
				  	   document.open();
				  	 
				  	   pdffive.getPestlefiveQuotation(count);
				  	 if(preprintStatus.contains("yes")){
						 
				  		pdffive.createPdf(preprintStatus);
					 }
					   else
					   {
						   pdffive.createPdf(preprintStatus);
					   }
				  	   document.close();
				  	   
				  	
				  			  
			 	  
		
	}
		  
		  
		  
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	}


	
}
