package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.servicerelated.CompanyAsset;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class ServiceJobCardPdf {

	public Document document;
	ProcessConfiguration processConfig;
	Logger logger = Logger.getLogger("Name of logger");

	Company comp;
	Customer cust;
	Contract con;
	Service service;
	Branch branch;
	CustomerBranchDetails customerbranch;

	ServiceProject serproject;
	ProductGroupList proglist;
	SuperProduct sup;

	List<Service> servicelist;

	float[] colWidths = { 3f, 3f, 3f };
	float[] colWidths1 = { 1.16f, 2.2f, 0.8f, 1.5f };
	float[] colWidths2 = { 6f, 2f };
	float[] colWidths3 = { 0.2f, 0.1f, 0.5f };
	float[] colWidths4 = { 0.3f, 0.1f, 0.6f };

	// ****************for 2nd page code***********************
	boolean showPreviousRecordFlag = false;
	boolean serviceSignature=false;//Add By jayshree
	ProductGroupDetails prodGrpEntity = null;
	List<ProductGroupList> billMaterialLis;
	// ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();

	List<Service> serviceList = new ArrayList<Service>();
	ArrayList<SuperProduct> stringlis = new ArrayList<SuperProduct>();
	List<ProductGroupList> prodList = new ArrayList<ProductGroupList>();
	List<CompanyAsset> tooltable;
	List<EmployeeInfo> technicians = new ArrayList<EmployeeInfo>();
	List<Employee> emplist = new ArrayList<Employee>();

	float[] columnWidths = { 0.6f, 0.8f, 2.7f, 0.8f, 1f, 1f };
	float[] columnWidths1 = { 1.16f, 2.2f, 0.8f, 1.5f };
	float[] columnWidths3 = { 1.5f, 3f, 1f, 1f, 1f };
	float[] columnWidths4 = { 6f, 2f };
	float[] columnWidths5 = { 10f, 10f, 10f, 30f, 19f, 9f, 12f };
	/**************************************************************************/

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	private Font font16boldul, font12bold, font8bold, font8, font12boldul,
			font12, font16bold, font10, font10bold, font14bold, font14, font9;

	public ServiceJobCardPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL);
		font9 = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);
	}

	public void setpdfserjob(Long count) {

		// loading service
		service = ofy().load().type(Service.class).id(count).now();

		// loading customer
		if (service.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.filter("companyId", service.getCompanyId()).first().now();

		// loading company
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", service.getCompanyId()).first().now();

		// loading contract
		if (service.getCompanyId() == null)
			con = ofy().load().type(Contract.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", service.getContractCount()).first().now();

		// loading branch
		if (service.getCompanyId() == null)
			branch = ofy().load().type(Branch.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			branch = ofy().load().type(Branch.class)
					.filter("companyId", service.getCompanyId())
					.filter("buisnessUnitName", service.getBranch().trim()).first()
					.now();

		// loading customer branch
		if (service.getCompanyId() == null)
			customerbranch = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			customerbranch = ofy().load().type(CustomerBranchDetails.class)
					.filter("companyId", service.getCompanyId())
					.filter("buisnessUnitName", service.getServiceBranch())
					.first().now();

		// loading service project
		if (service.getCompanyId() == null)
			serproject = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId()).first().now();
		else
			serproject = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId())
					.filter("serviceId", service.getCount())
					.filter("serviceSrNo", service.getServiceSerialNo())
					.first().now();

		
		
		//**Loading employee**//
		List<Employee> list1 = ofy().load().type(Employee.class)
				.filter("companyId", service.getCompanyId()).list();
	
	      if(list1.size()!=0){
	        	emplist.addAll(list1);
	     }
		// *******loading service for getting completed dates*********
		servicelist = new ArrayList<Service>();

		List<Service> list = ofy().load().type(Service.class)
				.filter("companyId", service.getCompanyId())
				.filter("contractCount", service.getContractCount()).list();

		if (list.size() != 0) {
			servicelist.addAll(list);
		}
		System.out.println("Size of servicelist:::::::;" + servicelist.size());

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		// ***********for 2nd page code****************

		// All services
		List<Service> serviceLst = ofy().load().type(Service.class)
				.filter("companyId", service.getCompanyId())
				.filter("contractCount", service.getContractCount()).list();

		System.out.println("Service List :: " + serviceLst.size());
		for (Service ser : serviceLst) {
			if (ser.getServiceSerialNo() < service.getServiceSerialNo()) {
				serviceList.add(ser);
			}
		}
		System.out.println("Service List SIZE :: " + serviceList.size());

		// ////////////////
		if (service.getCompanyId() != null) {
			sup = ofy().load().type(SuperProduct.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", service.getProduct().getCount()).first()
					.now();

			SuperProduct superprod = new SuperProduct();

			superprod.setCount(sup.getCount());
			superprod.setProductName(sup.getProductName());
			superprod.setComment(sup.getComment() + " " + sup.getCommentdesc());

			stringlis.add(superprod);
		} else {
			sup = ofy().load().type(SuperProduct.class)
					.filter("count", service.getProduct().getCount()).first()
					.now();
		}

		
		
		// /////////////
		if (serproject != null) {
			technicians = serproject.getTechnicians();
		}

		// //////////////
		int prodGrpId = 0;
		if (serproject != null) {
			if (serproject.getBillMaterialLis().size() != 0) {
				prodGrpId = serproject.getBillMaterialLis().get(0)
						.getProdGroupId();
			}
		}

		// //////////////
		if (prodGrpId != 0) {
			prodGrpEntity = ofy().load().type(ProductGroupDetails.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", prodGrpId).first().now();
		}

		// //////////////////
		if (serproject != null) {
			if (serproject.getProdDetailsList().size() != 0) {
				prodList = serproject.getProdDetailsList();
			}
		}

		// //////
		if (serproject != null) {
			tooltable = serproject.getTooltable();
		}
		if (prodGrpEntity != null) {
			billMaterialLis = prodGrpEntity.getPitems();
		}
		
		
		/**
		 * date 31-3-2018
		 * By Jayshree
		 * Des.to add the cust sign
		 */
		if (service.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Service")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ServiceSignature")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						serviceSignature = true;
					}
				}
			}
		}
		
		//End By jayshree

	}

	public void createPdf() {

		if (customerbranch != null) {
			createCustandCompInfo();
		} else {
			logger.log(Level.SEVERE,
					"inside the Customer and Company Info1 method");
			createCustandCompInfo1();
		}

		createServiceHeadingTable();

		// if (service.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)) {
		createServiceInfoTable();
		// }

		// if (service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
		createServiceInfoTable1();
		// }

		createProductInfo();
		createTechnicianinfo();
		createcustomerFeedback();
		createCustomerandTechnicianSign();

		// **********for 2nd page code***********************
		/**
		 * Date 24/1/2018
		 * By jayshree
		 * To add the contnt in next page
		 */
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		createLogo(document, comp);//comment by jayshree

		
		//End By Jayshree
		
		if (customerbranch != null) {
			createHeadingInfo();
		} else {
			createHeadingInfo1();
		}

		if (showPreviousRecordFlag) {
			if (serviceList.size() != 0) {
				createPreviousServiceDetails();
			}
		}
		createServiceHeadingTable1();

		createCustomerInfo();
		createMaterialReq();
		createTechnicianinfo1();
		createTools();
		customerFeedback();
		customerAndTechSign();

	}

	private void createCustandCompInfo() {

//		Phrase companyName = new Phrase("                  "
//				+ comp.getBusinessUnitName().toUpperCase(), font12bold);
//
//		Paragraph p = new Paragraph();
//		p.add(Chunk.NEWLINE);
//		p.add(companyName);

		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}


		PdfPTable logoTab = new PdfPTable(2);
		logoTab.setWidthPercentage(100);

		try {
			logoTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// logo tab Complete

		Phrase companyName = new Phrase( comp.getBusinessUnitName().toUpperCase(), font12bold);
		// 18 spaces added for setting logo
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);

		PdfPCell companyHeadingCell1 = new PdfPCell();
		companyHeadingCell1.setBorder(0);
		companyHeadingCell1.addElement(p);
		
		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase blank = new Phrase(" ");
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setBorder(0);
			logoTab.addCell(blankCell);
		}
		logoTab.addCell(companyHeadingCell1);
		
		
		
		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(logoTab);

		String branch1 = "Branch Name";
		Phrase branchphrase = new Phrase(branch1 + " - "
				+ branch.getBusinessUnitName(), font10);
		PdfPCell companybranchCell = new PdfPCell();
		companybranchCell.setBorder(0);
		companybranchCell.addElement(branchphrase);

		Phrase adressline1 = new Phrase(branch.getAddress().getAddrLine1(),
				font10);
		Phrase adressline2 = null;
		if (branch.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(branch.getAddress().getAddrLine2(), font10);
		} else {
			adressline2 = new Phrase("", font10);
		}

		Phrase landmark = null;
		if (branch.getAddress().getLandmark() != null) {
			landmark = new Phrase(branch.getAddress().getLandmark(), font10);
		} else {
			landmark = new Phrase("", font12);
		}

		Phrase locality = null;
		if (branch.getAddress().getLocality() != null) {
			locality = new Phrase(branch.getAddress().getLocality(), font10);
		} else {
			locality = new Phrase("", font12);
		}
		Phrase city = new Phrase(branch.getAddress().getCity() + " - "
				+ branch.getAddress().getPin(), font10);

		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);

		PdfPCell addressline1cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);

		PdfPCell addressline2cell = new PdfPCell();
		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);

		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);

		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		
		/**Date 22/1/2018
		 * By jayshree 
		 * Des.to change the title of cell to Phone and remove the Tel title
		 */

//		String contactinfo = "Cell: " + branch.getContact().get(0).getCellNo1();
//		if (branch.getContact().get(0).getCellNo2() != -1)
//			contactinfo = contactinfo + ","
//					+ branch.getContact().get(0).getCellNo2();
//		if (branch.getContact().get(0).getLandline() != -1) {
//			contactinfo = contactinfo + "     " + " Tel: "
//					+ branch.getContact().get(0).getLandline();
//		}
		
		
		
		String contactinfo="";
		
			if(branch.getCellNumber1()!=null && branch.getCellNumber1()!=0){
				System.out.println("pn11");
				contactinfo=branch.getCellNumber1()+"";
			}
			if(branch.getCellNumber2()!=null && branch.getCellNumber2()!=0)
			{
				if(!contactinfo.trim().isEmpty())
					{
					contactinfo=contactinfo+" / "+branch.getCellNumber2()+"";
					}
				else{
					contactinfo=branch.getCellNumber2()+"";
					}
			System.out.println("pn33"+contactinfo);
			}
			if(branch.getLandline()!=0 && branch.getLandline()!=null)
			{
				if(!contactinfo.trim().isEmpty()){
					contactinfo=contactinfo+" / "+branch.getLandline()+"";
				}
				else{
					contactinfo=branch.getLandline()+"";
				}
			System.out.println("pn44"+contactinfo);
			}

		Phrase contactnos = new Phrase("Phone : "+contactinfo, font9);
		
		//End for jayshree
		
		
		Phrase email = new Phrase("Email: "
				+ branch.getContact().get(0).getEmail(), font9);

		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);

		
		/**
		 * Date 22/1/2018
		 * By Jayshree
		 * Des.to add the contract id 
		 */
		Phrase contract = new Phrase("Contract Id : "
				+ serproject.getContractId() + "", font9);
		PdfPCell contractcell = new PdfPCell();
		contractcell.addElement(contract);
		contractcell.setBorder(0);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String contactinfo1 = "Start Date : "
				+ fmt.format(serproject.getContractStartDate());
		contactinfo1 = contactinfo1 + "       " + "End Date : "
				+ fmt.format(serproject.getContractEndDate()) + "";
		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(cstduphase);
		datecell.setBorder(0);
	
	
	/*************************************** Service Day Added ***********************************/
	/******************************* Scheduling Change 27 June ***********************************/

	PdfPCell servicecell = null;
	if(con!=null){
	if (!con.getScheduleServiceDay().equals("Not Select")) {

		String serviceday = "Week Day : " + con.getScheduleServiceDay();
		Phrase servicephrase = new Phrase(serviceday, this.font10);
		servicecell = new PdfPCell();
		servicecell.addElement(servicephrase);
		servicecell.setBorder(0);

	}
	}
	/******************************************************************************************/
		//End By Jayshree
		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(companybranchCell);
		companytable.addCell(addressline1cell);

		if (!branch.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!branch.getAddress().getLandmark().equals("")) {
			companytable.addCell(landmarkcell);
		}
		if (!branch.getAddress().getLocality().equals("")) {
			companytable.addCell(localitycell);
		}
		companytable.addCell(citycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		
		/**
		 * Date 22/1/2018
		 * By Jayshree
		 * Des.to add the contract id 
		 */
		companytable.addCell(contractcell);
		companytable.addCell(datecell);
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {
			companytable.addCell(servicecell);
		}
		}
		//End By Jayshree
		
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);

		
		
		/**
		 * Customer Info
		 */

		// ************************changes made by rohan for M/s
		// ****************
//		String tosir = null;
//		if (cust.isCompany() == true) {
//			tosir = "To, M/S";
//		} else {
//			tosir = "To,";
//		}
//		// *******************changes ends here
//		// ********************sss*************
//
//		String custName = "";
//
//		if (cust.isCompany() == true && cust.getCompanyName() != null) {
//			custName = cust.getCompanyName().trim();
//		} else {
//			custName = cust.getFullname().trim();
//		}

		/********** tosir **********/
//		Phrase tosirphrase = new Phrase(tosir, font12bold);
		// Paragraph tosirpara= new Paragraph();
		// tosirpara.add(tosirphrase);
		// PdfPCell custnamecell1=new PdfPCell();
		// custnamecell1.addElement(tosirpara);
		// custnamecell1.setBorder(0);

//		Phrase customername = new Phrase(custName, font12bold);
		
		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
			
			/**
			 * Date 22/1/2018
			 * By jayshree
			 * des.Comment this code
			 */
		
//			if(cust.isCompany()==true){
//				
//				
//			 tosir="To, M/S ";
//			}
//			else{
//				tosir="To,";
//			}
			//End By jayshree
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
		
		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		fullname.setFont(font12bold);
		 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
		    	fullname.add(custName);
			}
		 
		 //Comment By jayshree
//		    else
//		    {
////		    	fullname.add(tosir+"   "+custName);
//		    }

		 //End
		 
		String branch = "Branch Name";
		Phrase branchphrase1 = new Phrase("                   " + branch
				+ " - " + customerbranch.getBusinessUnitName(), font10);
		PdfPCell companybranchCell1 = new PdfPCell();
		companybranchCell1.setBorder(0);
		companybranchCell1.addElement(branchphrase1);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		Phrase customeradress = null;
		if (customerbranch.getAddress().getAddrLine1() != null) {
			customeradress = new Phrase(customerbranch.getAddress().getAddrLine1(), font10);//Remove blank space
		}

		Phrase customeradress2 = null;
		if (customerbranch.getAddress().getAddrLine2() != null) {
			customeradress2 = new Phrase(customerbranch.getAddress().getAddrLine2(), font10);//Remove blank space
		}

		PdfPCell custaddress1 = new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);
		PdfPCell custaddress2 = new PdfPCell();
		if (customerbranch.getAddress().getAddrLine2() != null) {
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
		}

		String custlandmark = "";
		Phrase custlocality = null;
		String custlocality1 = "";
		Phrase custlocalityphrase = null;
		if (!customerbranch.getAddress().getLandmark().equals("")) {
			custlandmark = customerbranch.getAddress().getLandmark();
			custlocality = new Phrase(custlandmark,font10);//Remove Blank space
		} else {
			custlocality = new Phrase("", font10);
		}
		if (!customerbranch.getAddress().getLocality().equals("")) {
			custlocality1 = customerbranch.getAddress().getLocality();
			custlocalityphrase = new Phrase("                   "
					+ custlocality1, font10);
		}

		Phrase cityState = new Phrase(customerbranch.getAddress().getCity() + " - "
				+ customerbranch.getAddress().getPin(), font10);

		PdfPCell custlocalitycell1 = new PdfPCell();
		custlocalitycell1.addElement(custlocalityphrase);
		custlocalitycell1.setBorder(0);

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(custlocality);
		custlocalitycell.setBorder(0);
		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);
		// custcitycell.addElement(Chunk.NEWLINE);

		/**
		 * Date 22/1/2018
		 * By jayshree
		 * Des.to change the title cell to phone 
		 */
		
		String custcontactinfo="";
		
		if(customerbranch.getCellNumber1()!=null && customerbranch.getCellNumber1()!=0){
			System.out.println("pn11");
			custcontactinfo=customerbranch.getCellNumber1()+"";
		}
		if(customerbranch.getCellNumber2()!=null && customerbranch.getCellNumber2()!=0)
		{
			if(!custcontactinfo.trim().isEmpty())
				{
				custcontactinfo=custcontactinfo+" / "+customerbranch.getCellNumber2()+"";
				}
			else{
				custcontactinfo=customerbranch.getCellNumber2()+"";
				}
		System.out.println("pn33"+custcontactinfo);
		}
		if(customerbranch.getLandline()!=0 && customerbranch.getLandline()!=null)
		{
			if(!custcontactinfo.trim().isEmpty()){
				custcontactinfo=custcontactinfo+" / "+customerbranch.getLandline()+"";
			}
			else{
				custcontactinfo=customerbranch.getLandline()+"";
			}
		System.out.println("pn44"+custcontactinfo);
		}
		
		Phrase custcontact = new Phrase("Phone : "
		+ custcontactinfo + "", font9);
		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);	
		
//		Phrase custcontact = new Phrase("Cell: "
//				+ customerbranch.getCellNumber1() + "", font9);
//		PdfPCell custcontactcell = new PdfPCell();
//		custcontactcell.addElement(custcontact);
//		custcontactcell.setBorder(0);

		//End By jayshree
		
		Phrase custemail = new Phrase("Email: " + customerbranch.getEmail(),
				font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);
		
		/**
		 * Date 22/1/2018
		 * By Jayshree
		 * Des.comment this code
		 */

//		Phrase contract = new Phrase("Contract Id : "
//				+ serproject.getContractId() + "", font9);
//		PdfPCell contractcell = new PdfPCell();
//		contractcell.addElement(contract);
//		contractcell.setBorder(0);
//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		String contactinfo1 = "Start Date : "
//				+ fmt.format(serproject.getContractStartDate());
//		contactinfo1 = contactinfo1 + "       " + "End Date : "
//				+ fmt.format(serproject.getContractEndDate()) + "";
//		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
//		PdfPCell datecell = new PdfPCell();
//		datecell.addElement(cstduphase);
//		datecell.setBorder(0);
		
		//End By Jayshree
		// ****************rohan bhagde changes on 25/5/15*******************
		String refInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refInfo = "Ref Date : " + fmt.format(con.getRefDate());
		} else{
			refInfo = "";
		}
		}else {
			refInfo = "";
		}

		String refnoInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refnoInfo = "Ref No : " + con.getRefNo();
		} else{
			refnoInfo = "";
		}
		}else {
			refnoInfo = "";
		}
		Phrase refnoInfophase = new Phrase(refInfo + "          " + refnoInfo,
				this.font10);
		PdfPCell nocell = new PdfPCell();
		nocell.addElement(refnoInfophase);
		nocell.setBorder(0);

		// *****************changes ends**********************************

		/**
		 * Date 23/1/2018
		 * Comment By jayshree
		 * 
		 */
//		/*************************************** Service Day Added ***********************************/
//		/******************************* Scheduling Change 27 June ***********************************/
//
//		PdfPCell servicecell = null;
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//
//			String serviceday = "Week Day : " + con.getScheduleServiceDay();
//			Phrase servicephrase = new Phrase(serviceday, this.font10);
//			servicecell = new PdfPCell();
//			servicecell.addElement(servicephrase);
//			servicecell.setBorder(0);
//
//		}
//		/******************************************************************************************/
//End By Jayshree
		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(companybranchCell1);
		custtable.addCell(custaddress1);
		if (!serproject.getAddr().getAddrLine2().equals("")) {
			custtable.addCell(custaddress2);
		}
		if (!serproject.getAddr().getLandmark().equals("")) {
			custtable.addCell(custlocalitycell);
		}
		if (!serproject.getAddr().getLocality().equals("")) {
			custtable.addCell(custlocalitycell1);
		}
		custtable.addCell(custcitycell);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);
		
		/**Comment By jayshree**/
//		custtable.addCell(contractcell);
//		custtable.addCell(datecell);

		// ***************rohan changes here********************
		custtable.addCell(nocell);

		/**Comment By jayshree**/
		
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//			custtable.addCell(servicecell);
//		}

		//End by jayshree
		
		
		// if company is true then the name of contact person

		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Contact: " + cust.getFullname(),
					font10);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		// //////////////////////////////////////////////////////

		try {
			document.add(headparenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCustandCompInfo1() {

		logger.log(Level.SEVERE, "inside the createCustandCompInfo1 method");
		System.out
				.println("Inside the createCustandCompInfo1() method:::::::::::::::");

//		Phrase bl = new Phrase(" ");
//		PdfPCell blcell = new PdfPCell(bl);
//		blcell.setBorder(0);
//
//		Phrase companyName = new Phrase("                  "
//				+ comp.getBusinessUnitName().toUpperCase(), font12bold);
//		// 18 spaces added for setting logo
//		Paragraph p = new Paragraph();
//		p.add(Chunk.NEWLINE);
//		p.add(companyName);
		

		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}


		PdfPTable logoTab = new PdfPTable(2);
		logoTab.setWidthPercentage(100);

		try {
			logoTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// logo tab Complete

		Phrase companyName = new Phrase( comp.getBusinessUnitName().toUpperCase(), font12bold);
		// 18 spaces added for setting logo
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);

		PdfPCell companyHeadingCell1 = new PdfPCell();
		companyHeadingCell1.setBorder(0);
		companyHeadingCell1.addElement(p);
		
		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase blank = new Phrase(" ");
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setBorder(0);
			logoTab.addCell(blankCell);
		}
		logoTab.addCell(companyHeadingCell1);
		

		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(logoTab);

		String branch1 = "Branch Name";
		Phrase branchphrase = new Phrase(branch1 + " - "
				+ branch.getBusinessUnitName(), font10);
		PdfPCell companybranchCell = new PdfPCell();
		companybranchCell.setBorder(0);
		companybranchCell.addElement(branchphrase);

		Phrase adressline1 = new Phrase(branch.getAddress().getAddrLine1(),
				font10);
		Phrase adressline2 = null;
		if (branch.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(branch.getAddress().getAddrLine2(), font10);
		} else {
			adressline2 = new Phrase("", font10);
		}

		Phrase landmark = null;
		if (branch.getAddress().getLandmark() != null) {
			landmark = new Phrase(branch.getAddress().getLandmark(), font10);
		} else {
			landmark = new Phrase("", font12);
		}

		Phrase locality = null;
		if (branch.getAddress().getLocality() != null) {
			locality = new Phrase(branch.getAddress().getLocality(), font10);
		} else {
			locality = new Phrase("", font12);
		}
		Phrase city = new Phrase(branch.getAddress().getCity() + " - "
				+ branch.getAddress().getPin(), font10);

		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);

		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);

		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);

		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);

		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);

		/**
		 * Date 23/1/2018
		 * By Jayshree
		 * Des.to change the title of cell to phone
		 */
		
//		String contactinfo = "Cell: " + branch.getContact().get(0).getCellNo1();
//		if (branch.getContact().get(0).getCellNo2() != -1)
//			contactinfo = contactinfo + ","
//					+ branch.getContact().get(0).getCellNo2();
//		if (branch.getContact().get(0).getLandline() != -1) {
//			contactinfo = contactinfo + "     " + " Tel: "
//					+ branch.getContact().get(0).getLandline();
//		}

		String contactinfo="";
		
		if(branch.getCellNumber1()!=null && branch.getCellNumber1()!=0){
			System.out.println("pn11");
			contactinfo=branch.getCellNumber1()+"";
		}
		if(branch.getCellNumber2()!=null && branch.getCellNumber2()!=0)
		{
			if(!contactinfo.trim().isEmpty())
				{
				contactinfo=contactinfo+" / "+branch.getCellNumber2()+"";
				}
			else{
				contactinfo=branch.getCellNumber2()+"";
				}
		System.out.println("pn33"+contactinfo);
		}
		if(branch.getLandline()!=0 && branch.getLandline()!=null)
		{
			if(!contactinfo.trim().isEmpty()){
				contactinfo=contactinfo+" / "+branch.getLandline()+"";
			}
			else{
				contactinfo=branch.getLandline()+"";
			}
		System.out.println("pn44"+contactinfo);
		}
		
		Phrase contactnos = new Phrase(contactinfo, font9);

//ENd By jayshree

		Phrase email = new Phrase("Email: "
				+ branch.getContact().get(0).getEmail(), font9);

		Phrase contract = new Phrase("Contract Id : "
				+ serproject.getContractId() + "", font9);
		PdfPCell contractcell = new PdfPCell();
		contractcell.addElement(contract);
		contractcell.setBorder(0);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));

		/**
		 * Date 23/1/2018
		 * By Jayshree
		 * Des.to change to add start date 
		 */
		
		String contactinfo1 = "Start Date : "
				+ fmt.format(serproject.getContractStartDate());

		contactinfo1 = contactinfo1 + "       " + "End Date : "
				+ fmt.format(serproject.getContractEndDate()) + "";
		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(cstduphase);
		datecell.setBorder(0);
		
		/**
		 * Change for add service day on 13/6/15 by mukesh
		 */
		PdfPCell servicecell = null;
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {

			String serviceday = "Week Day : " + con.getScheduleServiceDay();
			Phrase servicephrase = new Phrase(serviceday, this.font10);
			servicecell = new PdfPCell();
			servicecell.addElement(servicephrase);
			servicecell.setBorder(0);

		}
		}
		//End By Jayshree
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);

		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);

		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(companybranchCell);
		companytable.addCell(addressline1cell);

		if (!branch.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!branch.getAddress().getLandmark().equals("")) {
			companytable.addCell(landmarkcell);
		}
		if (!branch.getAddress().getLocality().equals("")) {
			companytable.addCell(localitycell);
		}
		companytable.addCell(citycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		
		companytable.addCell(contractcell);//add by  By jayshree
		companytable.addCell(datecell);//add by By jayshree

		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {
			companytable.addCell(servicecell);
		}
		}
		//Add By jayshree
		
		/**
		 * Customer Info
		 */
//		String tosir = "To, M/S";
//
//		String custName = "";
//
//		if (cust.isCompany() == true && cust.getCompanyName() != null) {
//			custName = cust.getCompanyName().trim();
//		} else {
//			custName = cust.getFullname().trim();
//		}

		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
			/**
			 * Date 23/1/2018
			 * By jayshree
			 * Comment this
			 */
			
//			if(cust.isCompany()==true){
//				
//				
//			 tosir="To, M/S";
//			}
//			else{
//				tosir="To,";
//			}
			//End
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
		/********** tosir **********/
//		Phrase tosirphrase = new Phrase(tosir, font12bold);
//
//		Phrase customername = new Phrase(custName, font12bold);
		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		fullname.setFont(font12bold);
		 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
		    	fullname.add(custName);
			}
		    else
		    {
//		    	fullname.add(tosir+"   "+custName);//comment by jayshree
		    	fullname.add(custName);
		    }

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		Phrase customeradress = null;
		System.out.println("Rohan service project id" + serproject.getCount());
		System.out.println("Address in the Service Project::::::::::::"
				+ serproject.getAddr().getAddrLine1());

		customeradress = new Phrase(serproject.getAddr().getAddrLine1().trim(), font10);//remove space by jayshree

		Phrase customeradress2 = null;

		if (serproject.getAddr().getAddrLine2() != null) {
			customeradress2 = new Phrase( serproject.getAddr().getAddrLine2().trim(), font10);//remove space by jayshree
		}

		PdfPCell custaddress1 = new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);
		PdfPCell custaddress2 = new PdfPCell();
		if (serproject.getAddr().getAddrLine2() != null) {
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
		}

		// Phrase custlandmark=null;
		String custlandmark = "";
		Phrase custlocality = null;
		String custlocality1 = "";
		Phrase custlocalityphrase = null;
		if (!serproject.getAddr().getLandmark().equals("")) {
			custlandmark = serproject.getAddr().getLandmark().trim();
			custlocality = new Phrase(custlandmark,font10);//Remove space By jayshree
		} else {
			custlocality = new Phrase("", font10);
		}
		if (!serproject.getAddr().getLocality().equals("")) {
			custlocality1 = serproject.getAddr().getLocality().trim();
			custlocalityphrase = new Phrase( custlocality1, font10);//Remove space By jayshree
		}

		Phrase cityState = new Phrase( serproject.getAddr().getCity() + " - "
				+ serproject.getAddr().getPin(), font10);//Remove space By jayshree

		PdfPCell custlocalitycell1 = new PdfPCell();
		custlocalitycell1.addElement(custlocalityphrase);
		custlocalitycell1.setBorder(0);

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(custlocality);
		custlocalitycell.setBorder(0);
		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);
		// custcitycell.addElement(Chunk.NEWLINE);

		/**
		 * Date 23/1/2018
		 * By jayshree 
		 * Des.To change the title cell to phone
		 */
		
		String custcontactinfo="";
		
		if(cust.getCellNumber1()!=null && cust.getCellNumber1()!=0){
			System.out.println("pn11");
			custcontactinfo=cust.getCellNumber1()+"";
		}
		if(cust.getCellNumber2()!=null && cust.getCellNumber2()!=0)
		{
			if(!custcontactinfo.trim().isEmpty())
				{
				custcontactinfo=custcontactinfo+" / "+cust.getCellNumber2()+"";
				}
			else{
				custcontactinfo=cust.getCellNumber2()+"";
				}
		System.out.println("pn33"+custcontactinfo);
		}
		if(cust.getLandline()!=0 && cust.getLandline()!=null)
		{
			if(!custcontactinfo.trim().isEmpty()){
				custcontactinfo=custcontactinfo+" / "+cust.getLandline()+"";
			}
			else{
				custcontactinfo=cust.getLandline()+"";
			}
		System.out.println("pn44"+custcontactinfo);
		}
		
		Phrase custcontact = new Phrase("Phone : " +custcontactinfo + "",
//		1Phrase custcontact = new Phrase("Cell: " + cust.getCellNumber1() + "",
				font9);
		
		//End By jayshree
		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);

		Phrase custemail = new Phrase("Email: " + cust.getEmail(), font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);
/**
 * Date 22/1/2018
 * By jayshree
 * Cooment this
 */
//		Phrase contract = new Phrase("Contract Id : "
//				+ serproject.getContractId() + "", font9);
//		PdfPCell contractcell = new PdfPCell();
//		contractcell.addElement(contract);
//		contractcell.setBorder(0);
//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//
//		String contactinfo1 = "Start Date : "
//				+ fmt.format(serproject.getContractStartDate());
//
//		contactinfo1 = contactinfo1 + "       " + "End Date : "
//				+ fmt.format(serproject.getContractEndDate()) + "";
//		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
//		PdfPCell datecell = new PdfPCell();
//		datecell.addElement(cstduphase);
//		datecell.setBorder(0);

		/**
		 * End By jayshree
		 */
		// ****************rohan bhagde changes on 25/5/15*******************
		String refInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refInfo = "Ref Date : " + fmt.format(con.getRefDate());
		} else{
			refInfo = "";
		}
		}else {
			refInfo = "";
		}

		String refnoInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refnoInfo = "Ref No : " + con.getRefNo();
		} else{
			refnoInfo = "";
		}
		}else {
			refnoInfo = "";
		}
		Phrase refnoInfophase = new Phrase(refInfo + "          " + refnoInfo,
				this.font10);
		PdfPCell nocell = new PdfPCell();
		nocell.addElement(refnoInfophase);
		nocell.setBorder(0);

		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Comment by jayshree
		 */
		
		// *****************changes ends**********************************
//		/**
//		 * Change for add service day on 13/6/15 by mukesh
//		 */
//		PdfPCell servicecell = null;
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//
//			String serviceday = "Week Day : " + con.getScheduleServiceDay();
//			Phrase servicephrase = new Phrase(serviceday, this.font10);
//			servicecell = new PdfPCell();
//			servicecell.addElement(servicephrase);
//			servicecell.setBorder(0);
//
//		}
		//End By Jayshree

		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(custaddress1);
		if (!serproject.getAddr().getAddrLine2().equals("")) {
			custtable.addCell(custaddress2);
		}
		if (!serproject.getAddr().getLandmark().equals("")) {
			custtable.addCell(custlocalitycell);
		}
		if (!serproject.getAddr().getLocality().equals("")) {
			custtable.addCell(custlocalitycell1);
		}
		custtable.addCell(custcitycell);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);
		
		
//		custtable.addCell(contractcell);//comment By jayshree
//		custtable.addCell(datecell);//Comment By jayshree

		// ***************rohan changes here********************
		if(con!=null){
		if (con.getRefDate() != null) {
			custtable.addCell(nocell);
		}
		}
/**
 * Date 23/1/2018
 * By jayshree
 * Comment this
 */
		// ***************mukesh changes here********************
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//			custtable.addCell(servicecell);
//		}
		
		//End By Jayshree
		
		// if company is true then the name of contact person

		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Contact: " + cust.getFullname(),
					font10);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		headparenttable.setWidthPercentage(100f);
		headparenttable.setSpacingAfter(3f);

		// PdfPCell cell = new PdfPCell(headparenttable);
		// cell.setBorder(0);
		//
		//
		// PdfPTable parenttable = new PdfPTable(2);
		// parenttable.addCell(cell);
		// parenttable.setWidthPercentage(100);

		// ///////////////////////////////////////////////////////////////////////////////////////////

		try {
			document.add(headparenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createServiceHeadingTable() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// ************center table************
		PdfPTable centerTable = new PdfPTable(1);
		centerTable.setWidthPercentage(100f);

		Phrase title = new Phrase("Service", font12bold);
		PdfPCell titlecell = new PdfPCell(title);
		titlecell.setBorder(0);
		titlecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		centerTable.addCell(titlecell);
		centerTable.addCell(blcell);

		PdfPCell centercell = new PdfPCell(centerTable);
		centercell.setBorder(0);

		// **********left table**************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		Phrase countinfo = null;
		if (service.getCount() != 0) {
			countinfo = new Phrase("ID : " + service.getCount() + "", font12);
		}
		PdfPCell countcell = new PdfPCell(countinfo);
		countcell.setBorder(0);
		countcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase srNo = null;
		if (service.getServiceSerialNo() != 0) {
			srNo = new Phrase("Service No. : " + service.getServiceSerialNo(),
					font12);
		}
		PdfPCell srNocell = new PdfPCell(srNo);
		srNocell.setBorder(0);
		srNocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(countcell);
		leftTable.addCell(srNocell);

		PdfPCell leftcell = new PdfPCell(leftTable);
		leftcell.setBorder(0);

		// **********right table**************
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		Phrase creationDateinfo = null;
		if (service.getServiceDate() != null) {
			creationDateinfo = new Phrase("Date : "
					+ fmt.format(service.getServiceDate()), font12);
		}
		PdfPCell datecell = new PdfPCell(creationDateinfo);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase creationTimeinfo = null;
		if (!service.getServiceTime().equals("Flexible")) {
			creationTimeinfo = new Phrase(" " + service.getServiceTime(),
					font12);
		} else {
			creationTimeinfo = new Phrase(" ", font12);
		}
		PdfPCell timecell = new PdfPCell(creationTimeinfo);
		timecell.setBorder(0);
		timecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		rightTable.addCell(datecell);
		rightTable.addCell(timecell);

		PdfPCell rightcell = new PdfPCell(rightTable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);

		try {
			table.setWidths(colWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);
		// cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.addCell(cell);
		// parentTable.addCell(blcell);
		parentTable.setWidthPercentage(100);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createServiceInfoTable() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase col = new Phrase(":", font10bold);
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// **********left table**************
		PdfPTable leftTable = new PdfPTable(3);

		try {
			leftTable.setWidths(colWidths4);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		leftTable.setWidthPercentage(100f);

		Phrase scheduleService = new Phrase("Next Schedule Services",
				font10bold);
		PdfPCell scheduleServicecell = new PdfPCell(scheduleService);
		scheduleServicecell.setBorder(0);
		scheduleServicecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(scheduleServicecell);
		leftTable.addCell(colcell);

		String abcDate = "";

		System.out
				.println("Service Scheduled Date and Service status::::::::::::::::"
						+ service.getServiceDate()
						+ service.SERVICESTATUSSCHEDULE);
		for (int i = 0; i < servicelist.size(); i++) {

			if (service.getCount() != servicelist.get(i).getCount()) {
				if (servicelist.get(i).getStatus().trim()
						.equals(service.SERVICESTATUSSCHEDULE)) {
					abcDate = abcDate
							+ (fmt1.format(servicelist.get(i).getServiceDate()))
							+ ",";
				}
				System.out.println("Service Scheduled Date::::::::::::::::"
						+ service.getServiceDate());
			}
		}
		Phrase serv = new Phrase(abcDate, font10);
		PdfPCell servcell = new PdfPCell(serv);
		servcell.setBorder(0);
		servcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(servcell);

		/************************** for scheduled dates *********************************/
		// for (int i = 0; i < servicelist.size(); i++) {
		//
		// if (service.getCount() != servicelist.get(i).getCount()) {
		//
		// if (service.getStatus().equals(service.SERVICESTATUSSCHEDULE)) {
		// serv = new Phrase(" "
		// + fmt.format(servicelist.get(i)
		// .get), font10);
		// }
		// }
		// PdfPCell servcell = new PdfPCell(serv);
		// servcell.setBorder(0);
		// servcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// leftTable.addCell(servcell);
		// }
		/***********************************************************************/

		PdfPCell leftcell = new PdfPCell(leftTable);
		leftcell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(leftcell);
		// table.addCell(rightcell);

		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);
		// cell.setBorder(0);
		cell.setBorderWidthBottom(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		// parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createServiceInfoTable1() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase col = new Phrase(":", font10bold);
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// **********left table**************
		PdfPTable leftTable = new PdfPTable(3);

		try {
			leftTable.setWidths(colWidths4);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		leftTable.setWidthPercentage(100f);

		Phrase completeService = new Phrase("Completed Services", font10bold);
		PdfPCell completeServicecell = new PdfPCell(completeService);
		completeServicecell.setBorder(0);
		completeServicecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(completeServicecell);
		leftTable.addCell(colcell);

		String serCompDt = "";

		for (int i = 0; i < servicelist.size(); i++) {

			if (service.getCount() != servicelist.get(i).getCount()) {
				if (servicelist.get(i).getStatus().trim().equals(Service.SERVICESTATUSCOMPLETED)) {
					/**handle exception here
					 * Date : 14-11-2017**/
					/**by MAnisha**/
					
					if(servicelist.get(i).getServiceCompletionDate()!=null){
						serCompDt = serCompDt+ (fmt.format(servicelist.get(i).getServiceCompletionDate())) + ",";
					}
					else{
						serCompDt=" ";
					}
					/**end**/
				}
			}
		}
		Phrase serv1 = new Phrase(serCompDt, font10);
		PdfPCell serv1cell = new PdfPCell(serv1);
		serv1cell.setBorder(0);
		serv1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(serv1cell);

		PdfPCell leftcell = new PdfPCell(leftTable);
		leftcell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(leftcell);

		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);
		// cell.setBorder(0);
		cell.setBorderWidthTop(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createProductInfo() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase col = new Phrase(":", font10bold);
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// **********left table**************
		PdfPTable leftTable = new PdfPTable(3);

		try {
			leftTable.setWidths(colWidths3);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		leftTable.setWidthPercentage(100f);

		Phrase prod = new Phrase("Service", font10bold);
		PdfPCell prodcell = new PdfPCell(prod);
		prodcell.setBorder(0);
		prodcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase product = null;
		if (service.getProductName() != null) {
			product = new Phrase("" + service.getProductName(), font10);
		}
		PdfPCell productcell = new PdfPCell(product);
		productcell.setBorder(0);
		productcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(prodcell);
		leftTable.addCell(colcell);
		leftTable.addCell(productcell);

		
		Phrase prodModelNo = new Phrase("Product Model No ", font10bold);
		PdfPCell prodModelNocell = new PdfPCell(prodModelNo);
		prodModelNocell.setBorder(0);
		prodModelNocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase productModel = null;
		if (service.getProModelNo() != null && !service.getProModelNo().equals("")) {
			productModel = new Phrase("" + service.getProModelNo(), font10);
		}
		PdfPCell prodModelNoDtcell = new PdfPCell(productModel);
		prodModelNoDtcell.setBorder(0);
		prodModelNoDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		if(productModel!= null){
			leftTable.addCell(prodModelNocell);
			leftTable.addCell(colcell);
			leftTable.addCell(prodModelNoDtcell);
		}
		
		
		Phrase prodSerialNo = new Phrase("Product Serial No ", font10bold);
		PdfPCell prodSerialNocell = new PdfPCell(prodSerialNo);
		prodSerialNocell.setBorder(0);
		prodSerialNocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase productSerial = null;
		if (service.getProSerialNo() != null && !service.getProSerialNo().equals("")) {
			productSerial = new Phrase("" + service.getProSerialNo(), font10);
		}
		PdfPCell prodSerialNoDtcell = new PdfPCell(productSerial);
		prodSerialNoDtcell.setBorder(0);
		prodSerialNoDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		if(productSerial!= null){
			leftTable.addCell(prodSerialNocell);
			leftTable.addCell(colcell);
			leftTable.addCell(prodSerialNoDtcell);
		}
		
		
		PdfPCell leftcell = new PdfPCell(leftTable);
		leftcell.setBorder(0);

		// **********right table**************
		PdfPTable rightTable = new PdfPTable(3);

		try {
			rightTable.setWidths(colWidths3);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		rightTable.setWidthPercentage(100f);

		rightTable.addCell(blcell);
		rightTable.addCell(blcell);
		rightTable.addCell(blcell);

		// prodTable.addCell(blcell);
		// prodTable.addCell(blcell);
		// prodTable.addCell(blcell);

		PdfPCell rightcell = new PdfPCell(rightTable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);
		try {
			table.setWidths(new float[] { 40, 60 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createTechnicianinfo() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		Phrase productdetails = new Phrase("TECHNICIAN INFORMATION", font10bold);
		PdfPCell productdetailcell = new PdfPCell(productdetails);
		productdetailcell.setBorder(0);
		productdetailcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100);

		Phrase tectid = new Phrase("EMPLOYEE ID", font1);
		Phrase tectname = new Phrase("TECHNICIAN NAME", font1);
		Phrase tectcell = new Phrase("CONTACT NO.", font1);
		Phrase branchcell = new Phrase("BRANCH", font1);

		PdfPCell celltectid = new PdfPCell(tectid);
		celltectid.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell celltectname = new PdfPCell(tectname);
		celltectname.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell celltectcell = new PdfPCell(tectcell);
		celltectcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell celltectbranch = new PdfPCell(branchcell);
		celltectbranch.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(celltectid);
		table.addCell(celltectname);
		table.addCell(celltectcell);
		table.addCell(celltectbranch);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		try {
			table.setTotalWidth(colWidths1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

//		for (int i = 0; i < this.technicians.size(); i++) {

         for(int i=0;i<emplist.size();i++){
            	
			String name = emplist.get(i).getFullname();
			String nm = emplist.get(i).getFullName();
	  	   if((service.getEmployee().trim()).equalsIgnoreCase(nm.trim()))
		 {
			Phrase chunk = null;
//			if (technicians.get(i).getCount() + "" != null) {
			 if (emplist.get(i).getCount() != 0) {
//				chunk = new Phrase(technicians.get(i).getEmpCount() + "", font8);
				 chunk = new Phrase(emplist.get(i).getCount() + "", font8);
			}
			PdfPCell pdfdayscell = new PdfPCell(chunk);

//			if (technicians.get(i).getFullName() != null) {
			if (service.getEmployee() != null) {

				chunk = new Phrase(service.getEmployee() + "", font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfpercentcell = new PdfPCell(chunk);

//			if (technicians.get(i).getCellNumber() != null) {
			if (emplist.get(i).getCellNumber1()!= null) {
				chunk = new Phrase(emplist.get(i).getCellNumber1()+ "",
						font8);
//				chunk = new Phrase(technicians.get(i).getCellNumber() + "",
//						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);

	
			/*******************************************************/
			
//			if (!technicians.get(i).getBranch().equals("")) {
//
//				chunk = new Phrase(technicians.get(i).getBranch().trim(), font8);
			if (!emplist.get(i).getBranchName().equals("")) {

				chunk = new Phrase(emplist.get(i).getBranchName(), font8);
			} else {
				chunk = new Phrase("", font8);
//				System.out.println("in side else ");                                                                                         
//				Employee emp = ofy().load().type(Employee.class).filter("companyId", service.getCompanyId())
//						.filter("count", technicians.get(i).getEmpCount()).first()
//						.now();
//
//				if (emp != null) {
//					chunk = new Phrase(emp.getBranchName(), font8);
//				} else {
//					chunk = new Phrase("", font8);
//				}
			
			}
			PdfPCell pdfbranchcell = new PdfPCell(chunk);
			/*****************************************************/

			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);
			table.addCell(pdfbranchcell);

			try {
				table.setTotalWidth(colWidths1);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		 }
	}

		// PdfPCell prodtablecell = new PdfPCell();
		// prodtablecell.addElement(table);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(productdetailcell);
		parentTable.addCell(blcell);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createcustomerFeedback() {

		if (service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {

			System.out.println("Completed status............");
			Font font1 = new Font(Font.FontFamily.HELVETICA, 9);
			Font font9 = new Font(Font.FontFamily.HELVETICA, 9 | Font.BOLD);

			Phrase blank = new Phrase(" ", font1);
			PdfPCell blankcell = new PdfPCell(blank);
			blankcell.setBorder(0);

			Phrase custFback = new Phrase("CUSTOMER FEEDBACK : ", font10bold);
			Paragraph para = new Paragraph();
			para.add(custFback);
			para.setAlignment(Element.ALIGN_LEFT);

			PdfPTable feedtable = new PdfPTable(6);
			feedtable.setWidthPercentage(100);

			Phrase poor = null;
			if (service.getCustomerFeedback() != null) {
				poor = new Phrase("RATE :  " + service.getCustomerFeedback(),
						font9);
			} else {
				poor = new Phrase("RATE :", font9);
			}

			PdfPCell cellpoor = new PdfPCell(poor);
			cellpoor.setBorder(0);
			cellpoor.setHorizontalAlignment(Element.ALIGN_LEFT);

			feedtable.addCell(cellpoor);
			feedtable.addCell(blankcell);
			feedtable.addCell(blankcell);
			feedtable.addCell(blankcell);
			feedtable.addCell(blankcell);
			feedtable.addCell(blankcell);

			PdfPCell feedtablecell = new PdfPCell(feedtable);
			feedtablecell.setBorder(0);

			PdfPTable parentTable = new PdfPTable(1);
			parentTable.setWidthPercentage(100);
			parentTable.addCell(feedtablecell);

			PdfPCell prodtablecell = new PdfPCell();
			prodtablecell.addElement(Chunk.NEWLINE);
			// prodtablecell.addElement(Chunk.NEWLINE);
			// prodtablecell.addElement(Chunk.NEWLINE);
			prodtablecell.setBorder(0);

			try {
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				document.add(para);
				document.add(parentTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

		} else {

			Font font1 = new Font(Font.FontFamily.HELVETICA, 9);
			Font font9 = new Font(Font.FontFamily.HELVETICA, 9 | Font.BOLD);

			Phrase custFback = new Phrase("CUSTOMER FEEDBACK : ", font12bold);
			Paragraph para = new Paragraph();
			para.add(custFback);
			para.setAlignment(Element.ALIGN_LEFT);

			PdfPTable feedtable = new PdfPTable(6);
			feedtable.setWidthPercentage(100);

			Phrase rate = new Phrase("RATE :", font9);
			Phrase poor = new Phrase("[   ] POOR ", font1);
			Phrase avg = new Phrase("[   ] AVERAGE ", font1);
			Phrase good = new Phrase("[   ] GOOD ", font1);
			Phrase verygood = new Phrase("[   ] VERY GOOD ", font1);
			Phrase execellent = new Phrase("[   ]  EXCELLENT ", font1);

			PdfPCell cellrate = new PdfPCell(rate);
			cellrate.setBorder(0);
			cellrate.setHorizontalAlignment(Element.ALIGN_LEFT);

			PdfPCell cellpoor = new PdfPCell(poor);
			cellpoor.setBorder(0);
			cellpoor.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell cellavg = new PdfPCell(avg);
			cellavg.setBorder(0);
			cellavg.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell cellgood = new PdfPCell(good);
			cellgood.setBorder(0);
			cellgood.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell cellverygood = new PdfPCell(verygood);
			cellverygood.setBorder(0);
			cellverygood.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell cellexecellent = new PdfPCell(execellent);
			cellexecellent.setBorder(0);
			cellexecellent.setHorizontalAlignment(Element.ALIGN_CENTER);

			feedtable.addCell(cellrate);
			feedtable.addCell(cellpoor);
			feedtable.addCell(cellavg);
			feedtable.addCell(cellgood);
			feedtable.addCell(cellverygood);
			feedtable.addCell(cellexecellent);

			PdfPCell feedtablecell = new PdfPCell(feedtable);
			feedtablecell.setBorder(0);

			PdfPCell prodtablecell = new PdfPCell();
			prodtablecell.addElement(Chunk.NEWLINE);
			// prodtablecell.addElement(Chunk.NEWLINE);
			// prodtablecell.addElement(Chunk.NEWLINE);
			prodtablecell.setBorder(0);

			PdfPTable parentTable = new PdfPTable(1);
			parentTable.setWidthPercentage(100);
			parentTable.addCell(prodtablecell);
			parentTable.addCell(feedtablecell);

			try {
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				document.add(para);
				document.add(parentTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
	}

	private void createCustomerandTechnicianSign() {

		/**
		 * Date 23/1/2018
		 * By Jayshree
		 * Des.comment this code
		 */
		
//		PdfPTable signtable = new PdfPTable(2);
//		signtable.setWidthPercentage(100);
//		try {
//			signtable.setWidths(colWidths2);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//		
//		
//		Phrase dotedline = new Phrase("------------------------------",
//				font10bold);
//		Phrase custsign = new Phrase("Customer Signature", font10bold);
//		Phrase techsign = new Phrase("Technician Signature", font10bold);
//
//		PdfPCell dotedcell = new PdfPCell();
//		dotedcell.addElement(dotedline);
//		dotedcell.setBorder(0);
//		dotedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPCell custcell = new PdfPCell();
//		custcell.addElement(custsign);
//		custcell.setBorder(0);
//		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPCell dotedcell1 = new PdfPCell();
//		dotedcell1.addElement(dotedline);
//		dotedcell1.setBorder(0);
//		dotedcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
//		PdfPCell techcell = new PdfPCell();
//		techcell.addElement(techsign);
//		techcell.setBorder(0);
//		techcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
//		signtable.addCell(dotedcell);
//
//		signtable.addCell(dotedcell1);
//
//		signtable.addCell(custcell);
//
//		signtable.addCell(techcell);
//
//		signtable.setSpacingBefore(50f);
//
//		try {
//			document.add(signtable);
//			document.newPage();
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		//End By Jayshree
	
		/**
		 * Date 23/1/208
		 * Dev.Jayshree
		 * Des.To add the digital signature changes are made
		 */
		
		PdfPTable custsignTab =new PdfPTable(1);
		custsignTab.setWidthPercentage(100);
		
		
					DocumentUpload digitalsign = service.getCustomerSignature();

					// patch
					String hostUrl;
					String environment = System
							.getProperty("com.google.appengine.runtime.environment");
					if (environment.equals("Production")) {
						String applicationId = System
								.getProperty("com.google.appengine.application.id");
						String version = System
								.getProperty("com.google.appengine.application.version");
						hostUrl = "http://" + version + "." + applicationId
								+ ".appspot.com/";
					} else {
						hostUrl = "http://localhost:8888";
					}
					PdfPCell imageSignCell = null;
					Image image2 = null;
					try {
						image2 = Image
								.getInstance(new URL(hostUrl + digitalsign.getUrl()));
						logger.log(Level.SEVERE, image2+"");
						 /** date 16.7.2018 added by komal for customer signature (uploaded from android)**/
							if(image2 == null){
							image2 = Image
									.getInstance(new URL(digitalsign.getUrl())); 
							}
							/**
							 * end komal
							 */
						
						
						image2.scalePercent(13f);
						image2.scaleAbsoluteWidth(100f);//By Jayshree add this 7/12/2017
						// image2.setAbsolutePosition(40f,765f);
						// doc.add(image2);

						imageSignCell = new PdfPCell(image2);
						imageSignCell.setBorder(0);
						imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//						imageSignCell.setFixedHeight(20);
					} catch (Exception e) {
						e.printStackTrace();
					}

//					 Image image1=null;
//					 try
//					 {
//					 image1=Image.getInstance("images/ipclogo4.jpg");
//					 image1.scalePercent(13f);
//					image1.scaleAbsoluteWidth(100f);
////					  image1.setAbsolutePosition(40f,765f);
//					 // doc.add(image1);
//					
//					
//					
//					
//					
//					 imageSignCell=new PdfPCell(image1);
////					 imageSignCell.addElement(image1);
////					 imageSignCell.setFixedHeight(40);
//					 imageSignCell.setBorder(0);
//					 imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					 }
//					 catch(Exception e)
//					 {
//					 e.printStackTrace();
//					 }
					
					
		
		
		if(imageSignCell!=null)	
		{
		Phrase blankPh=new Phrase (" ",font10);
		PdfPCell blankCell=new PdfPCell(blankPh);
		blankCell.setBorder(0);
		custsignTab.addCell(blankCell);
		custsignTab.addCell(blankCell);
		custsignTab.addCell(imageSignCell);
		}else{		
					
		Phrase blankPh2=new Phrase (" ",font10);
		PdfPCell blankCell2=new PdfPCell(blankPh2);
		blankCell2.setBorder(0);
		custsignTab.addCell(blankCell2);
		
		custsignTab.addCell(blankCell2);
		
		custsignTab.addCell(blankCell2);
		custsignTab.addCell(blankCell2);
		custsignTab.addCell(blankCell2);
		}			
		Phrase dotedline = new Phrase("------------------------------",font10bold);
		PdfPCell dotedcell = new PdfPCell();
		dotedcell.addElement(dotedline);
		dotedcell.setBorder(0);
		dotedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custsignTab.addCell(dotedcell);
		
		Phrase custsign = new Phrase("Customer Signature", font10bold);
		PdfPCell custcell = new PdfPCell();
		custcell.addElement(custsign);
		custcell.setBorder(0);
		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custsignTab.addCell(custcell);
		
		PdfPTable techsignTab =new PdfPTable(1);
		techsignTab.setWidthPercentage(100);
		
		Phrase blankPh2=new Phrase (" ",font10);
		PdfPCell blankCell2=new PdfPCell(blankPh2);
		blankCell2.setBorder(0);
		techsignTab.addCell(blankCell2);
		
		techsignTab.addCell(blankCell2);
		techsignTab.addCell(blankCell2);
		techsignTab.addCell(blankCell2);
		techsignTab.addCell(blankCell2);
		Phrase dotedline2 = new Phrase("------------------------------",font10bold);
		PdfPCell dotedcell1 = new PdfPCell();
		dotedcell1.addElement(dotedline2);
		dotedcell1.setBorder(0);
		dotedcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		techsignTab.addCell(dotedcell1);
		
		Phrase techsign = new Phrase("Technician Signature", font10bold);
		PdfPCell techcell = new PdfPCell();
		techcell.addElement(techsign);
		techcell.setBorder(0);
		techcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		techsignTab.addCell(techcell);
		
		PdfPTable footerTab=new PdfPTable(3);
		footerTab.setWidthPercentage(100);
//		footerTab.setSpacingBefore(50f);
		try {
			footerTab.setWidths(new float[]{20,55,25});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell custsignCell=new PdfPCell(custsignTab);
		custsignCell.setBorder(0);
		footerTab.addCell(custsignCell);
		
		Phrase footerBlank=new Phrase("",font10);
		PdfPCell footerbl=new PdfPCell(footerBlank);
		footerbl.setBorder(0);
		footerTab.addCell(footerbl);
		
		PdfPCell techsignCell=new PdfPCell(techsignTab);
		techsignCell.setBorder(0);
		footerTab.addCell(techsignCell);
		
		try {
			document.add(footerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//End By Jayshree
		
	}

	/********************** for the 2nd page code **********************************************************/

	private void createPreviousServiceDetails() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columnWidths5);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Paragraph para = null;

		Phrase productdetails = new Phrase("Service Record", font10bold);
		para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		Phrase servNo = new Phrase("SERVICE NO.", font1);
		Phrase servId = new Phrase("SERVICE ID", font1);
		Phrase servDate = new Phrase("SERVICE DATE", font1);
		Phrase prodName = new Phrase("ITEM NAME", font1);
		Phrase serEngg = new Phrase("TECHNICIAN", font1);
		Phrase status = new Phrase("STATUS", font1);
		Phrase feedback = new Phrase("FEEDBACK", font1);

		PdfPCell servIdCell = new PdfPCell(servId);
		servIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell servDateCell = new PdfPCell(servDate);
		servDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell servNoCell = new PdfPCell(servNo);
		servNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell prodNameCell = new PdfPCell(prodName);
		prodNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell servEnggCell = new PdfPCell(serEngg);
		servEnggCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell statusCell = new PdfPCell(status);
		statusCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell feedbackCell = new PdfPCell(feedback);
		feedbackCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(servNoCell);
		table.addCell(servIdCell);
		table.addCell(servDateCell);
		table.addCell(prodNameCell);
		table.addCell(servEnggCell);
		table.addCell(statusCell);
		table.addCell(feedbackCell);

		Comparator<Service> sortingServices = new Comparator<Service>() {
			@Override
			public int compare(Service o1, Service o2) {
				Integer c1 = o1.getCount();
				Integer c2 = o2.getCount();
				return c1.compareTo(c2);
			}
		};
		Collections.sort(serviceList, sortingServices);

		for (int i = 0; i < this.serviceList.size(); i++) {

			Phrase chunk = null;
			chunk = new Phrase(serviceList.get(i).getCount() + "", font8);
			PdfPCell pdfServIdCell = new PdfPCell(chunk);
			pdfServIdCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(serviceList.get(i).getServiceSerialNo() + "",
					font8);
			PdfPCell pdfServNoCell = new PdfPCell(chunk);
			pdfServNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(fmt.format(serviceList.get(i).getServiceDate())
					+ "", font8);
			PdfPCell pdfServDateCell = new PdfPCell(chunk);
			pdfServDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(serviceList.get(i).getProductName() + "", font8);
			PdfPCell pdfProdNameCell = new PdfPCell(chunk);
			pdfProdNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(serviceList.get(i).getEmployee() + "", font8);
			PdfPCell pdfServEnggCell = new PdfPCell(chunk);
			pdfServEnggCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(serviceList.get(i).getStatus() + "", font8);
			PdfPCell pdfStatusCell = new PdfPCell(chunk);
			pdfStatusCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			if (serviceList.get(i).getCustomerFeedback() != null) {
				chunk = new Phrase(serviceList.get(i).getCustomerFeedback()
						+ "", font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfCustFeedbackCell = new PdfPCell(chunk);
			pdfCustFeedbackCell.setHorizontalAlignment(Element.ALIGN_LEFT);

			table.addCell(pdfServNoCell);
			table.addCell(pdfServIdCell);
			table.addCell(pdfServDateCell);
			table.addCell(pdfProdNameCell);
			table.addCell(pdfServEnggCell);
			table.addCell(pdfStatusCell);
			table.addCell(pdfCustFeedbackCell);

		}

		PdfPTable parentTableTect = new PdfPTable(1);
		parentTableTect.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(para);
		prodtablecell.addElement(table);

		parentTableTect.addCell(prodtablecell);

		try {
			document.add(parentTableTect);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createHeadingInfo() {
		/**
		 * Date 24/1/2018
		 * By jayshree
		 * Des.add logo
		 */
		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 Image image1=null;
//		 try
//		 {
//		 image1=Image.getInstance("images/ipclogo4.jpg");
//		 image1.scalePercent(20f);
//		 // image1.setAbsolutePosition(40f,765f);
//		 // doc.add(image1);
//		
//		
//		
//		
//		
//		 imageSignCell=new PdfPCell();
//		 imageSignCell.addElement(image1);
//		 imageSignCell.setFixedHeight(20);
//		 imageSignCell.setBorder(0);
//		 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }

		PdfPTable logoTab = new PdfPTable(2);
		logoTab.setWidthPercentage(100);

		try {
			logoTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// logo tab Complete

		Phrase companyName = new Phrase( comp.getBusinessUnitName().toUpperCase(), font12bold);
		// 18 spaces added for setting logo
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);

		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(p);
		
		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase blank = new Phrase(" ");
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setBorder(0);
			logoTab.addCell(blankCell);
		}
		logoTab.addCell(companyHeadingCell);

		PdfPCell logocell=new PdfPCell(logoTab);
		logocell.setBorder(0);
		
		// End by Jayshree

		

		String branch1 = "Branch Name";
		Phrase branchphrase = new Phrase(branch1 + " - "
				+ branch.getBusinessUnitName(), font10);
		PdfPCell companybranchCell = new PdfPCell();
		companybranchCell.setBorder(0);
		companybranchCell.addElement(branchphrase);

		Phrase adressline1 = new Phrase(branch.getAddress().getAddrLine1(),
				font10);
		Phrase adressline2 = null;
		if (branch.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(branch.getAddress().getAddrLine2(), font10);
		} else {
			adressline2 = new Phrase("", font10);
		}

		Phrase landmark = null;
		if (branch.getAddress().getLandmark() != null) {
			landmark = new Phrase(branch.getAddress().getLandmark(), font10);
		} else {
			landmark = new Phrase("", font12);
		}

		Phrase locality = null;
		if (branch.getAddress().getLocality() != null) {
			locality = new Phrase(branch.getAddress().getLocality(), font10);
		} else {
			locality = new Phrase("", font12);
		}
		Phrase city = new Phrase(branch.getAddress().getCity() + " - "
				+ branch.getAddress().getPin(), font10);

		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);
		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);

		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);

		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);

		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
/**
 * Date 23/1/2018
 * By Jayshree
 * Des.To change the title and value of cell
 */
//		String contactinfo = "Cell: " + branch.getContact().get(0).getCellNo1();
//		if (branch.getContact().get(0).getCellNo2() != -1)
//			contactinfo = contactinfo + ","
//					+ branch.getContact().get(0).getCellNo2();
//		if (branch.getContact().get(0).getLandline() != -1) {
//			contactinfo = contactinfo + "     " + " Tel: "
//					+ branch.getContact().get(0).getLandline();
//		}

		String contactinfo="";
		
		if(branch.getCellNumber1()!=null && branch.getCellNumber1()!=0){
			System.out.println("pn11");
			contactinfo=branch.getCellNumber1()+"";
		}
		if(branch.getCellNumber2()!=null && branch.getCellNumber2()!=0)
		{
			if(!contactinfo.trim().isEmpty())
				{
				contactinfo=contactinfo+" / "+branch.getCellNumber2()+"";
				}
			else{
				contactinfo=branch.getCellNumber2()+"";
				}
		System.out.println("pn33"+contactinfo);
		}
		if(branch.getLandline()!=0 && branch.getLandline()!=null)
		{
			if(!contactinfo.trim().isEmpty()){
				contactinfo=contactinfo+" / "+branch.getLandline()+"";
			}
			else{
				contactinfo=branch.getLandline()+"";
			}
		System.out.println("pn44"+contactinfo);
		}
		
		Phrase contactnos = new Phrase("Phone : "+contactinfo, font9);
		//end By jayshree
		Phrase email = new Phrase("Email: "
				+ branch.getContact().get(0).getEmail(), font9);

		Phrase contract = new Phrase("Contract Id : "
				+ serproject.getContractId() + "", font9);
		PdfPCell contractcell = new PdfPCell();
		contractcell.addElement(contract);
		contractcell.setBorder(0);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String contactinfo1 = "Start Date : "
				+ fmt.format(serproject.getContractStartDate());
		contactinfo1 = contactinfo1 + "       " + "End Date : "
				+ fmt.format(serproject.getContractEndDate()) + "";
		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(cstduphase);
		datecell.setBorder(0);
		
		/*************************************** Service Day Added ***********************************/
		/******************************* Scheduling Change 27 June ***********************************/

		PdfPCell servicecell = null;
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {

			String serviceday = "Week Day : " + con.getScheduleServiceDay();
			Phrase servicephrase = new Phrase(serviceday, this.font10);
			servicecell = new PdfPCell();
			servicecell.addElement(servicephrase);
			servicecell.setBorder(0);

		}
		}
		//End By jayshree
		
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);

		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(logocell);// Date 24/1/2018 Add By jayshree
		companytable.addCell(companybranchCell);
		companytable.addCell(addressline1cell);
		if (!branch.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!branch.getAddress().getLandmark().equals("")) {
			companytable.addCell(landmarkcell);
		}
		if (!branch.getAddress().getLocality().equals("")) {
			companytable.addCell(localitycell);
		}
		companytable.addCell(citycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		/**
		 *Date 22/1/2018  add Cell  by jayshree
		 */
		companytable.addCell(contractcell);
		companytable.addCell(datecell);
		//End
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);
		//Add By Jayshree
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {
			companytable.addCell(servicecell);
		}
		}
		//end
		
		/**
		 * Customer Info
		 */
		// String tosir="To, M/S";

		// ************************changes made by rohan for M/s
		// ****************
//		String tosir = null;
//		if (cust.isCompany() == true) {
//
//			tosir = "To, M/S";
//		} else {
//			tosir = "To,";
//		}
//		// *******************changes ends here
//		// ********************sss*************
//
//		// Phrase tosirphrase= new Phrase(tosir,font12bold);
//		// Paragraph tosirpara= new Paragraph();
//		// tosirpara.add(tosirphrase);
//
//		String custName = "";
//
//		if (cust.isCompany() == true && cust.getCompanyName() != null) {
//			custName = cust.getCompanyName().trim();
//		} else {
//			custName = cust.getFullname().trim();
//		}
//
//		/********** tosir **********/
//
//		Phrase tosirphrase = new Phrase(tosir, font12bold);
		// Paragraph tosirpara= new Paragraph();
		// tosirpara.add(tosirphrase);
		// PdfPCell custnamecell1=new PdfPCell();
		// custnamecell1.addElement(tosirpara);
		// custnamecell1.setBorder(0);

		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
			/**
			 * Comment By Jayshree
			 */
			
//			if(cust.isCompany()==true){
//				
//				
//			 tosir="To, M/S";
//			}
//			else{
//				tosir="To,";
//			}
			//End
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
		
		
//		Phrase customername = new Phrase(custName, font12bold);
		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		fullname.setFont(font12bold);
		  if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
		    	fullname.add(custName);
			}
		    else
		    {
//		    	fullname.add(tosir+"   "+custName);//Date 23/1/2018 comment by jayshree
		    	fullname.add(custName);
		    }

		String branch = "Branch Name";
		Phrase branchphrase1 = new Phrase("                   " + branch
				+ " - " + customerbranch.getBusinessUnitName(), font10);
		PdfPCell companybranchCell1 = new PdfPCell();
		companybranchCell1.setBorder(0);
		companybranchCell1.addElement(branchphrase1);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		Phrase customeradress = null;
		if (customerbranch.getAddress().getAddrLine1() != null) {
			customeradress = new Phrase(customerbranch.getAddress().getAddrLine1(), font10);//By jayshree remove blank
		}

		Phrase customeradress2 = null;
		if (customerbranch.getAddress().getAddrLine2() != null) {
			customeradress2 = new Phrase(customerbranch.getAddress().getAddrLine2(), font10);//By jayshree remove blank
		}

		PdfPCell custaddress1 = new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);
		PdfPCell custaddress2 = new PdfPCell();
		if (customerbranch.getAddress().getAddrLine2() != null) {
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
		}

		// Phrase custlandmark=null;
		String custlandmark = "";
		Phrase custlocality = null;
		String custlocality1 = "";
		Phrase custlocalityphrase = null;
		if (!customerbranch.getAddress().getLandmark().equals("")) {
			custlandmark = customerbranch.getAddress().getLandmark();
			custlocality = new Phrase( custlandmark,font10);//By jayshree remove blank
		} else {
			custlocality = new Phrase("", font10);
		}
		if (!customerbranch.getAddress().getLocality().equals("")) {
			custlocality1 = customerbranch.getAddress().getLocality();
			custlocalityphrase = new Phrase("                   "
					+ custlocality1, font10);
		}

		Phrase cityState = new Phrase(customerbranch.getAddress().getCity() + " - "
				+ customerbranch.getAddress().getPin(), font10);

		PdfPCell custlocalitycell1 = new PdfPCell();
		custlocalitycell1.addElement(custlocalityphrase);
		custlocalitycell1.setBorder(0);

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(custlocality);
		custlocalitycell.setBorder(0);
		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);
		// custcitycell.addElement(Chunk.NEWLINE);
/**
 * Date 23/1/2018
 * By Jayshree
 * Des.to change the title and value of cell
 */
		
		String custcontactinfo="";
		
		if(customerbranch.getCellNumber1()!=null && customerbranch.getCellNumber1()!=0){
			System.out.println("pn11");
			custcontactinfo=customerbranch.getCellNumber1()+"";
		}
		if(customerbranch.getCellNumber2()!=null && customerbranch.getCellNumber2()!=0)
		{
			if(!custcontactinfo.trim().isEmpty())
				{
				custcontactinfo=custcontactinfo+" / "+customerbranch.getCellNumber2()+"";
				}
			else{
				custcontactinfo=customerbranch.getCellNumber2()+"";
				}
		System.out.println("pn33"+custcontactinfo);
		}
		if(customerbranch.getLandline()!=0 && customerbranch.getLandline()!=null)
		{
			if(!custcontactinfo.trim().isEmpty()){
				custcontactinfo=custcontactinfo+" / "+customerbranch.getLandline()+"";
			}
			else{
				custcontactinfo=customerbranch.getLandline()+"";
			}
		System.out.println("pn44"+custcontactinfo);
		}
		
//		Phrase custcontact = new Phrase("Cell: "
//				+ customerbranch.getCellNumber1() + "", font9);
		
		Phrase custcontact = new Phrase("Phone : "+custcontactinfo+ "", font9);
		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);

		//End By jayshree
		
		Phrase custemail = new Phrase("Email: " + customerbranch.getEmail(),
				font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);
/**
 * Date 22/1/2018
 * By jayshree
 * Comment this
 */
		
//		Phrase contract = new Phrase("Contract Id : "
//				+ serproject.getContractId() + "", font9);
//		PdfPCell contractcell = new PdfPCell();
//		contractcell.addElement(contract);
//		contractcell.setBorder(0);
//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		String contactinfo1 = "Start Date : "
//				+ fmt.format(serproject.getContractStartDate());
//		contactinfo1 = contactinfo1 + "       " + "End Date : "
//				+ fmt.format(serproject.getContractEndDate()) + "";
//		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
//		PdfPCell datecell = new PdfPCell();
//		datecell.addElement(cstduphase);
//		datecell.setBorder(0);
		
		//End By jayshree
		// ****************rohan bhagde changes on 25/5/15*******************
		String refInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refInfo = "Ref Date : " + fmt.format(con.getRefDate());
		}else{
			refInfo = "";
		}
		}else {
			refInfo = "";
		}

		String refnoInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refnoInfo = "Ref No : " + con.getRefNo();
		}else{
			refnoInfo = "";
		}
		}else {
			refnoInfo = "";
		}
		Phrase refnoInfophase = new Phrase(refInfo + "          " + refnoInfo,
				this.font10);
		PdfPCell nocell = new PdfPCell();
		nocell.addElement(refnoInfophase);
		nocell.setBorder(0);

		// *****************changes ends**********************************

		/**
		 * Date 24/1/2018
		 * By Jayshree
		 * Comment this code
		 */
		
//		/*************************************** Service Day Added ***********************************/
//		/******************************* Scheduling Change 27 June ***********************************/
//
//		PdfPCell servicecell = null;
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//
//			String serviceday = "Week Day : " + con.getScheduleServiceDay();
//			Phrase servicephrase = new Phrase(serviceday, this.font10);
//			servicecell = new PdfPCell();
//			servicecell.addElement(servicephrase);
//			servicecell.setBorder(0);
//
//		}
		
		//End By jayshree
		/******************************************************************************************/

		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(companybranchCell1);
		custtable.addCell(custaddress1);
		if (!serproject.getAddr().getAddrLine2().equals("")) {
			custtable.addCell(custaddress2);
		}
		if (!serproject.getAddr().getLandmark().equals("")) {
			custtable.addCell(custlocalitycell);
		}
		if (!serproject.getAddr().getLocality().equals("")) {
			custtable.addCell(custlocalitycell1);
		}
		custtable.addCell(custcitycell);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);
		/**
		 * Date 24/1/2018
		 * By Jayshree
		 * Comment this code
		 */
//		custtable.addCell(contractcell);//Comment by jayshree
//		custtable.addCell(datecell);//Comment by jayshree
//End By Jayshree
		// ***************rohan changes here********************
		custtable.addCell(nocell);
		/**
		 * Date 24/1/2018
		 * By Jayshree
		 * Comment this code
		 */
		
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//			custtable.addCell(servicecell);
//		}
//ENd By jayshree
		// if company is true then the name of contact person

		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Contact: " + cust.getFullname(),
					font10);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		// //////////////////////////////////////////////////////

		String title = "Service";

		String countinfo = "ID : " + service.getCount() + "";
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String creationdateinfo = "";
		if (!service.getServiceTime().equals("Flexible")) {
			creationdateinfo = "Date : " + fmt.format(service.getServiceDate())
					+ " " + service.getServiceTime();
		} else {
			creationdateinfo = "Date: " + fmt.format(service.getServiceDate());
		}

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {
			document.add(headparenttable);
			// document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHeadingInfo1() {
		/**
		 * Date 24/1/2018
		 * By jayshree
		 * Des.add logo
		 */
		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 Image image1=null;
//		 try
//		 {
//		 image1=Image.getInstance("images/ipclogo4.jpg");
//		 image1.scalePercent(20f);
//		 // image1.setAbsolutePosition(40f,765f);
//		 // doc.add(image1);
//		
//		
//		
//		
//		
//		 imageSignCell=new PdfPCell();
//		 imageSignCell.addElement(image1);
//		 imageSignCell.setFixedHeight(20);
//		 imageSignCell.setBorder(0);
//		 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }

		PdfPTable logoTab = new PdfPTable(2);
		logoTab.setWidthPercentage(100);

		try {
			logoTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// logo tab Complete

		Phrase companyName = new Phrase( comp.getBusinessUnitName().toUpperCase(), font12bold);
		// 18 spaces added for setting logo
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);

		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(p);
		
		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase blank = new Phrase(" ");
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setBorder(0);
			logoTab.addCell(blankCell);
		}
		logoTab.addCell(companyHeadingCell);

		PdfPCell logocell=new PdfPCell(logoTab);
		logocell.setBorder(0);
		
		// End by Jayshree


		String branch1 = "Branch Name";
		Phrase branchphrase = new Phrase(branch1 + " - "
				+ branch.getBusinessUnitName(), font10);
		PdfPCell companybranchCell = new PdfPCell();
		companybranchCell.setBorder(0);
		companybranchCell.addElement(branchphrase);

		Phrase adressline1 = new Phrase(branch.getAddress().getAddrLine1(),
				font10);
		Phrase adressline2 = null;
		if (branch.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(branch.getAddress().getAddrLine2(), font10);
		} else {
			adressline2 = new Phrase("", font10);
		}

		Phrase landmark = null;
		if (branch.getAddress().getLandmark() != null) {
			landmark = new Phrase(branch.getAddress().getLandmark(), font10);
		} else {
			landmark = new Phrase("", font12);
		}

		Phrase locality = null;
		if (branch.getAddress().getLocality() != null) {
			locality = new Phrase(branch.getAddress().getLocality(), font10);
		} else {
			locality = new Phrase("", font12);
		}
		Phrase city = new Phrase(branch.getAddress().getCity() + " - "
				+ branch.getAddress().getPin(), font10);

		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);
		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);

		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);

		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);

		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		/**
		 * Date 23/1/2018
		 * By Jayshree
		 * Des.to change the title cell 
		 */
//		String contactinfo = "Cell: " + branch.getContact().get(0).getCellNo1();
//		if (branch.getContact().get(0).getCellNo2() != -1)
//			contactinfo = contactinfo + ","
//					+ branch.getContact().get(0).getCellNo2();
//		if (branch.getContact().get(0).getLandline() != -1) {
//			contactinfo = contactinfo + "     " + " Tel: "
//					+ branch.getContact().get(0).getLandline();
//		}
		
		String contactinfo="";
		
		if(branch.getCellNumber1()!=null && branch.getCellNumber1()!=0){
			System.out.println("pn11");
			contactinfo=branch.getCellNumber1()+"";
		}
		if(branch.getCellNumber2()!=null && branch.getCellNumber2()!=0)
		{
			if(!contactinfo.trim().isEmpty())
				{
				contactinfo=contactinfo+" / "+branch.getCellNumber2()+"";
				}
			else{
				contactinfo=branch.getCellNumber2()+"";
				}
		System.out.println("pn33"+contactinfo);
		}
		if(branch.getLandline()!=0 && branch.getLandline()!=null)
		{
			if(!contactinfo.trim().isEmpty()){
				contactinfo=contactinfo+" / "+branch.getLandline()+"";
			}
			else{
				contactinfo=branch.getLandline()+"";
			}
		System.out.println("pn44"+contactinfo);
		}

		Phrase contactnos = new Phrase("Phone : " +contactinfo, font9);
		
		//End By Jayshree
		
		Phrase email = new Phrase("Email: "
				+ branch.getContact().get(0).getEmail(), font9);

		/**
		 * Date 23/1/2018
		 * By Jayshree
		 * Des.Add this code
		 */
		
		Phrase contract = new Phrase("Contract Id : "
				+ serproject.getContractId() + "", font9);
		PdfPCell contractcell = new PdfPCell();
		contractcell.addElement(contract);
		contractcell.setBorder(0);
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String contactinfo1 = "Start Date : "
				+ fmt.format(serproject.getContractStartDate());
		contactinfo1 = contactinfo1 + "       " + "End Date : "
				+ fmt.format(serproject.getContractEndDate()) + "";
		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(cstduphase);
		datecell.setBorder(0);
		
		/**
		 * Change for add service day on 13/6/15 by mukesh
		 */
		PdfPCell servicecell = null;
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {

			String serviceday = "Week Day : " + con.getScheduleServiceDay();
			Phrase servicephrase = new Phrase(serviceday, this.font10);
			servicecell = new PdfPCell();
			servicecell.addElement(servicephrase);
			servicecell.setBorder(0);

		}
		}
		//End By jayshree
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);

		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(logocell);
		companytable.addCell(companybranchCell);
		companytable.addCell(addressline1cell);
		if (!branch.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!branch.getAddress().getLandmark().equals("")) {
			companytable.addCell(landmarkcell);
		}
		if (!branch.getAddress().getLocality().equals("")) {
			companytable.addCell(localitycell);
		}
		companytable.addCell(citycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Des.add cell in tab 
		 */
		companytable.addCell(contractcell);
		companytable.addCell(datecell);
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);
		if(con!=null){
		if (!con.getScheduleServiceDay().equals("Not Select")) {
			companytable.addCell(servicecell);
	}
		}
		//End By jayshree
		/**
		 * Customer Info
		 */
//		String tosir = "To, M/S";
		// Phrase tosirphrase= new Phrase(tosir,font12bold);
		// Paragraph tosirpara= new Paragraph();
		// tosirpara.add(tosirphrase);

//		String custName = "";
//
//		if (cust.isCompany() == true && cust.getCompanyName() != null) {
//			custName = cust.getCompanyName().trim();
//		} else {
//			custName = cust.getFullname().trim();
//		}

		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		/**
		 * Comment By Jayshree this code
		 */
//			if(cust.isCompany()==true){
//				
//				
//			 tosir="To, M/S";
//			}
//			else{
//				tosir="To,";
//			}
			
			//End By jayshree
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
		/********** tosir **********/

//		Phrase tosirphrase = new Phrase(tosir, font12bold);
		// Paragraph tosirpara= new Paragraph();
		// tosirpara.add(tosirphrase);
		// PdfPCell custnamecell1=new PdfPCell();
		// custnamecell1.addElement(tosirpara);
		// custnamecell1.setBorder(0);

//		Phrase customername = new Phrase(custName, font12bold);
		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		fullname.setFont(font12bold);
		 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
		    	fullname.add(custName);
			}
		    else
		    {
//		    	fullname.add(tosir+"   "+custName);//Comment by jayshree
		    	fullname.add(custName);//Add by Jayshree
		    }

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		Phrase customeradress = null;

		customeradress = new Phrase(serproject.getAddr().getAddrLine1().trim(), font10);//By jayshree remove space

		Phrase customeradress2 = null;
		if (serproject.getAddr().getAddrLine2() != null) {
			customeradress2 = new Phrase(serproject.getAddr().getAddrLine2().trim(), font10);//By jayshree Remove space
		}

		PdfPCell custaddress1 = new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);
		PdfPCell custaddress2 = new PdfPCell();
		if (serproject.getAddr().getAddrLine2() != null) {
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
		}

		// Phrase custlandmark=null;
		String custlandmark = "";
		Phrase custlocality = null;
		String custlocality1 = "";
		Phrase custlocalityphrase = null;
		if (!serproject.getAddr().getLandmark().equals("")) {
			custlandmark = serproject.getAddr().getLandmark().trim();
			custlocality = new Phrase(custlandmark,font10);//By jayshree remove space
		} else {
			custlocality = new Phrase("", font10);
		}
		if (!serproject.getAddr().getLocality().equals("")) {
			custlocality1 = serproject.getAddr().getLocality().trim();
			custlocalityphrase = new Phrase(custlocality1, font10);//By jayshree remove space
		}

		Phrase cityState = new Phrase(serproject.getAddr().getCity() + " - "
				+ serproject.getAddr().getPin(), font10);

		PdfPCell custlocalitycell1 = new PdfPCell();
		custlocalitycell1.addElement(custlocalityphrase);
		custlocalitycell1.setBorder(0);

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(custlocality);
		custlocalitycell.setBorder(0);
		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);
		// custcitycell.addElement(Chunk.NEWLINE);
		
		
		/**
		 * Date 23/1/208
		 * By jayshree
		 * Des.change the title cell to phone
		 */
		String custcontactinfo="";
		
		if(cust.getCellNumber1()!=null && cust.getCellNumber1()!=0){
			System.out.println("pn11");
			custcontactinfo=cust.getCellNumber1()+"";
		}
		if(cust.getCellNumber2()!=null && cust.getCellNumber2()!=0)
		{
			if(!custcontactinfo.trim().isEmpty())
				{
				custcontactinfo=custcontactinfo+" / "+cust.getCellNumber2()+"";
				}
			else{
				custcontactinfo=cust.getCellNumber2()+"";
				}
		System.out.println("pn33"+custcontactinfo);
		}
		if(cust.getLandline()!=0 && cust.getLandline()!=null)
		{
			if(!custcontactinfo.trim().isEmpty()){
				custcontactinfo=custcontactinfo+" / "+cust.getLandline()+"";
			}
			else{
				custcontactinfo=cust.getLandline()+"";
			}
		System.out.println("pn44"+custcontactinfo);
		}
		
		Phrase custcontact = new Phrase("Phone : " + custcontactinfo + "",
				font9);
		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);

		//End By Jayshree
		Phrase custemail = new Phrase("Email: " + cust.getEmail(), font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);
/**
 * Comment By Jayshree
 */
//		Phrase contract = new Phrase("Contract Id : "
//				+ serproject.getContractId() + "", font9);
//		PdfPCell contractcell = new PdfPCell();
//		contractcell.addElement(contract);
//		contractcell.setBorder(0);
//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		String contactinfo1 = "Start Date : "
//				+ fmt.format(serproject.getContractStartDate());
//		contactinfo1 = contactinfo1 + "       " + "End Date : "
//				+ fmt.format(serproject.getContractEndDate()) + "";
//		Phrase cstduphase = new Phrase(contactinfo1, this.font10);
//		PdfPCell datecell = new PdfPCell();
//		datecell.addElement(cstduphase);
//		datecell.setBorder(0);
		
		//End By Jayshree
		// ****************rohan bhagde changes on 25/5/15*******************
		String refInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refInfo = "Ref Date : " + fmt.format(con.getRefDate());
		} else{
			refInfo = "";
		}
		}else {
			refInfo = "";
		}

		String refnoInfo = null;
		if(con!=null){
		if (con.getRefDate() != null) {
			refnoInfo = "Ref No : " + con.getRefNo();
		}else{
			refnoInfo = "";
		}
		}else {
			refnoInfo = "";
		}
		Phrase refnoInfophase = new Phrase(refInfo + "          " + refnoInfo,
				this.font10);
		PdfPCell nocell = new PdfPCell();
		nocell.addElement(refnoInfophase);
		nocell.setBorder(0);
/**
 * Date 23/1/2018
 * By jayshree
 * Comment this
 */
		// *****************changes ends**********************************
//		/**
//		 * Change for add service day on 13/6/15 by mukesh
//		 */
//	1	PdfPCell servicecell = null;
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//
//			String serviceday = "Week Day : " + con.getScheduleServiceDay();
//			Phrase servicephrase = new Phrase(serviceday, this.font10);
//			servicecell = new PdfPCell();
//			servicecell.addElement(servicephrase);
//			servicecell.setBorder(0);
//
//		}

		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(custaddress1);
		if (!serproject.getAddr().getAddrLine2().equals("")) {
			custtable.addCell(custaddress2);
		}
		if (!serproject.getAddr().getLandmark().equals("")) {
			custtable.addCell(custlocalitycell);
		}
		if (!serproject.getAddr().getLocality().equals("")) {
			custtable.addCell(custlocalitycell1);
		}
		custtable.addCell(custcitycell);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);
		/**
		 * comment by jayshree
		 */
//		custtable.addCell(contractcell);
//		custtable.addCell(datecell);

		// ***************rohan changes here********************
		if(con!=null){
		if (con.getRefDate() != null) {
			custtable.addCell(nocell);
		}
		}
		// ***************mukesh changes here********************
		/**
		 * Comment by jayshree
		 */
//		if (!con.getScheduleServiceDay().equals("Not Select")) {
//			custtable.addCell(servicecell);
//		}
		// if company is true then the name of contact person
//End
		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Contact: " + cust.getFullname(),
					font10);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		// ///////////////////////////////////////////////////////////////////////////////////////////

		String title = "Service";

		String countinfo = "ID : " + service.getCount() + "";
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));

		/**
		 * for service time on pdf
		 */
		String creationdateinfo = "";
		if (!service.getServiceTime().equals("Flexible")) {
			creationdateinfo = "Date : " + fmt.format(service.getServiceDate())
					+ " " + service.getServiceTime();
		} else {
			creationdateinfo = "Date: " + fmt.format(service.getServiceDate());
		}

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {
			document.add(headparenttable);
			// document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createServiceHeadingTable1() {

		String title = "Service";

		String countinfo = "ID : " + service.getCount() + "";
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));

		/**
		 * for service time on pdf
		 */
		String creationdateinfo = "";
		if (!service.getServiceTime().equals("Flexible")) {
			creationdateinfo = "Date : " + fmt.format(service.getServiceDate())
					+ " " + service.getServiceTime();
		} else {
			creationdateinfo = "Date: " + fmt.format(service.getServiceDate());
		}

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerInfo() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		Phrase custInfo = new Phrase("Customer Information", font10bold);
		Paragraph para = new Paragraph();
		para.add(custInfo);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);
		Phrase category = new Phrase("CATEGORY", font1);
		Phrase type = new Phrase("TYPE", font1);
		Phrase group = new Phrase("GROUP", font1);
		Phrase level = new Phrase("LEVEL", font1);
		Phrase priority = new Phrase("PRIORITY", font1);

		PdfPCell cellcategory = new PdfPCell(category);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltype = new PdfPCell(type);
		celltype.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellgroup = new PdfPCell(group);
		cellgroup.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celllevel = new PdfPCell(level);
		celllevel.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellpriority = new PdfPCell(priority);
		cellpriority.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase categoryvalue = new Phrase(cust.getCategory(), font1);
		Phrase typevalue = new Phrase(cust.getType(), font1);
		Phrase groupvalue = new Phrase(cust.getGroup(), font1);
		Phrase levelvalue = new Phrase(cust.getCustomerLevel(), font1);
		Phrase priorityvalue = new Phrase(cust.getCustomerPriority(), font1);

		PdfPCell cellcategoryvalue = new PdfPCell(categoryvalue);
		cellcategoryvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell celltypevalue = new PdfPCell(typevalue);
		celltypevalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell cellgroupvalue = new PdfPCell(groupvalue);
		cellgroupvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell celllevelvalue = new PdfPCell(levelvalue);
		celllevelvalue.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell cellpriorityvalue = new PdfPCell(priorityvalue);
		cellpriorityvalue.setHorizontalAlignment(Element.ALIGN_LEFT);

		table.addCell(cellcategory);
		table.addCell(celltype);
		table.addCell(cellgroup);
		table.addCell(celllevel);
		table.addCell(cellpriority);

		table.addCell(cellcategoryvalue);
		table.addCell(celltypevalue);
		table.addCell(cellgroupvalue);
		table.addCell(celllevelvalue);
		table.addCell(cellpriorityvalue);

		try {
			table.setTotalWidth(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PdfPTable parentTableTect = new PdfPTable(1);
		parentTableTect.setWidthPercentage(100);

		/**
		 * Date 24/1/2018
		 * By jayshree
		 * Des.remove add element
		 */
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(para);
		prodtablecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell custCell=new PdfPCell(table);
//		prodtablecell.addElement(para);
//		prodtablecell.addElement(table);

		parentTableTect.addCell(prodtablecell);
		parentTableTect.addCell(custCell);
		try {
			document.add(parentTableTect);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		//End By jayshree
	}

	private void createMaterialReq() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		Phrase productphrase = new Phrase("Product : "
				+ service.getProductName(), font10bold);

		Paragraph productpara = new Paragraph(productphrase);

		/** date 16.7.2018 added by komal for service premises and service description**/		
		Phrase premisesphrase = null;
		if(service.getPremises()!=null && !service.getPremises().equals("")){
			premisesphrase = new Phrase("Service Premises : "
				+ service.getPremises(), font8);
		}else{
			premisesphrase = new Phrase("", font8);
		}
		
		/** date 16.7.2018 added by komal for service premises and service description**/		
		Phrase descriptionphrase = null;
		if(service.getDescription()!=null && !service.getDescription().equals("")){
			descriptionphrase = new Phrase("Service Description : "
				+ service.getDescription(), font8);
		}else{
			descriptionphrase = new Phrase("", font8);
		}
		Paragraph premisespara = new Paragraph(premisesphrase);
		Paragraph descriptionpara = new Paragraph(descriptionphrase);
		/**
		 * end komal
		 */

		
		
		Phrase productdesc = null;
		System.out.println("Product....size  === " + this.stringlis.size());

		for (int i = 0; i < this.stringlis.size(); i++) {

			if (!stringlis.get(i).getComment().equals("")) {
				productdesc = new Phrase(stringlis.get(i).getComment(), font8);
				System.out.println("pro coment+++   "
						+ stringlis.get(i).getComment());
			} else {

				productdesc = new Phrase("", font8);
			}

		}

		Paragraph prodesc = new Paragraph(productdesc);

		PdfPTable table = new PdfPTable(5);//by jayshree remove one column
		table.setWidthPercentage(100);
		Paragraph para = null;

		Phrase productdetails = new Phrase("Material Required", font10bold);
		para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		Phrase servicephase = new Phrase("SR. NO.", font1);
		Phrase productidphrase = new Phrase("CODE", font1);
		Phrase namephrase = new Phrase("ITEM NAME", font1);
		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Des.Change the titleQTY to REq/Used
		 */
		Phrase codephrase = new Phrase("REQ/USED", font1);
		Phrase titlephrase = new Phrase("UNIT", font1);
		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Des.comment this code
		 */
//		Phrase pgidphrase = new Phrase("RECEIVED", font1);

		PdfPCell cellservice = new PdfPCell(servicephase);
		cellservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproductid = new PdfPCell(productidphrase);
		cellproductid.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellname = new PdfPCell(namephrase);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcode = new PdfPCell(codephrase);
		cellcode.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltitle = new PdfPCell(titlephrase);
		celltitle.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Des.comment this code
		 */
//		PdfPCell cellpgid = new PdfPCell(pgidphrase);
//		cellpgid.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellservice);
		table.addCell(cellproductid);
		table.addCell(cellname);
		table.addCell(cellcode);
		table.addCell(celltitle);
//		table.addCell(cellpgid);//Comment By jayshree

		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// if(prodGrpEntity!=null){

		for (int i = 0; i < this.prodList.size(); i++) {

			Phrase chunk = null;
			chunk = new Phrase((i + 1) + "", font8);

			PdfPCell pdfservicenocell = new PdfPCell(chunk);
			pdfservicenocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			System.out.println("Title iiss === " + prodList.get(i).getTitle());
			if (prodList.get(i).getCode() != null) {
				chunk = new Phrase(prodList.get(i).getCode(), font8);
				System.out.println("Title iiss === "
						+ prodList.get(i).getCode());
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfprodidcell = new PdfPCell(chunk);
			if (prodList.get(i).getName() != null) {
				chunk = new Phrase(prodList.get(i).getName(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfprodnamecell = new PdfPCell(chunk);

			if (prodList.get(i).getQuantity() != 0) {
				chunk = new Phrase(prodList.get(i).getQuantity() + "", font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcodecell = new PdfPCell(chunk);
			pdfcodecell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (prodList.get(i).getUnit() != null) {
				chunk = new Phrase(prodList.get(i).getUnit(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdftitlecell = new PdfPCell(chunk);
			pdftitlecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			Phrase recphrase = new Phrase("Yes / No", font8);

			PdfPCell recevivedcellCell = new PdfPCell(recphrase);
			recevivedcellCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfservicenocell);
			table.addCell(pdfprodidcell);
			table.addCell(pdfprodnamecell);
			table.addCell(pdfcodecell);
			table.addCell(pdftitlecell);
//			table.addCell(recevivedcellCell);comment this by jayshree

			try {
				table.setWidths(columnWidths);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}

		// }

		PdfPTable parentTableTect = new PdfPTable(1);
		parentTableTect.setWidthPercentage(100);

		/**
		 * Date 24/1/2018
		 * By jayshree
		 * Des.to remove add element
		 */
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(productpara);
		prodtablecell.setBorderWidthTop(0);
		prodtablecell.setBorderWidthBottom(0);
		
		/** date 16.7.2018 added by komal for service premises and service description**/		
		PdfPCell premisestablecell = new PdfPCell();
		prodtablecell.addElement(premisespara);
		prodtablecell.setBorderWidthTop(0);
		prodtablecell.setBorderWidthBottom(0);
		
		PdfPCell descriptiontablecell = new PdfPCell();
		prodtablecell.addElement(descriptionpara);
		prodtablecell.setBorderWidthTop(0);
		prodtablecell.setBorderWidthBottom(0);
		
		/**
		 * end komal
		 */
		
		PdfPCell prodescCell=new PdfPCell(); 
		prodescCell.addElement(prodesc);
		prodescCell.setBorderWidthTop(0);
		prodescCell.setBorderWidthBottom(0);
		
		PdfPCell paracell=new PdfPCell();
		paracell.addElement(para);
		paracell.setBorderWidthBottom(0);
		paracell.setBorderWidthTop(0);
		
		PdfPCell tabCell=new PdfPCell(table);
//		prodtablecell.addElement(table);

		parentTableTect.addCell(prodtablecell);
		/** date 16.7.2018 added by komal for service premises and service description**/
		parentTableTect.addCell(premisestablecell);
		parentTableTect.addCell(descriptiontablecell);
		/**
		 * end komal
		 */
		parentTableTect.addCell(prodescCell);
		parentTableTect.addCell(paracell);
		parentTableTect.addCell(tabCell);
		//End By Jayshree
		try {
			document.add(parentTableTect);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createTechnicianinfo1() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Technician Information", font10bold);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		PdfPTable table = new PdfPTable(5);//By jayshree Add new Column
		table.setWidthPercentage(100);

		Phrase tectid = new Phrase("EMPLOYEE ID", font1);
		Phrase tectname = new Phrase("NAME", font1);
		Phrase tectcell = new Phrase("CELL", font1);
		Phrase branchcell = new Phrase("BRANCH", font1);
		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Des.add the new column 
		 */
		Phrase starrating=new Phrase("Star Rating",font1);
		//End By Jayshree
		
		PdfPCell celltectid = new PdfPCell(tectid);
		celltectid.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltectname = new PdfPCell(tectname);
		celltectname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltectcell = new PdfPCell(tectcell);
		celltectcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltectbranch = new PdfPCell(branchcell);
		celltectbranch.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**
		 * Date 23/1/2018
		 * By jayshree
		 * Des.add the new column 
		 */
		
		PdfPCell starCell=new PdfPCell(starrating);
		starCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//End By Jayshree
		table.addCell(celltectid);
		table.addCell(celltectname);
		table.addCell(celltectcell);
		table.addCell(celltectbranch);
		table.addCell(starCell);//Add new Cell In table By jayshree
		try {
			table.setTotalWidth(columnWidths1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

//		for (int i = 0; i < this.technicians.size(); i++) {
		for (int i = 0; i < emplist.size(); i++) 
		{
			String nm = emplist.get(i).getFullName();
			Phrase chunk = null;
		   if((service.getEmployee().trim()).equalsIgnoreCase(nm.trim()))
			{
//			if (technicians.get(i).getCount() + "" != null) {
			   if (emplist.get(i).getCount() + "" != null) {
				chunk = new Phrase(emplist.get(i).getCount() + "", font8);
			}
			PdfPCell pdfdayscell = new PdfPCell(chunk);
			// pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if (emplist.get(i).getFullName() != null) {
				chunk = new Phrase(service.getEmployee() + "", font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			// pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (emplist.get(i).getCellNumber1() != null) {
				chunk = new Phrase(emplist.get(i).getCellNumber1() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			// pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			/****************************************************************/
//			if (technicians.get(i).getBranch() != null) {
//				chunk = new Phrase(technicians.get(i).getBranch(), font8);
//			} else {
//				chunk = new Phrase(" ", font8);
//			}
//			PdfPCell pdfbranchcell = new PdfPCell(chunk);
//			// pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			/*****************************************************************/
		
//			if (!technicians.get(i).getBranch().equals("")) {
//
//				chunk = new Phrase(technicians.get(i).getBranch().trim(), font8);
//			} else {
			if (!emplist.get(i).getBranchName().equals("")) {

				chunk = new Phrase(emplist.get(i).getBranchName().trim(), font8);
			} else {

//				System.out.println("in side else ");
//				Employee emp = ofy().load().type(Employee.class).filter("companyId", service.getCompanyId())
//						.filter("count", technicians.get(i).getEmpCount()).first()
//						.now();
//
//				if (emp != null) {
//					chunk = new Phrase(emp.getBranchName(), font8);
//				} else {
					chunk = new Phrase("", font8);
//				}
			}
			PdfPCell pdfbranchcell = new PdfPCell(chunk);

			/**
			 * Date 23/1/2018
			 * By jayshree
			 * Des.add the new column 
			 */
			
			System.out.println("feed"+service.getCustomerFeedback());
			chunk = new Phrase(service.getCustomerFeedback(), font8);
			PdfPCell starRating=new PdfPCell(chunk);
			
			//End By Jayshree
			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);
			table.addCell(pdfbranchcell);
			table.addCell(starRating);//Add by jayshree new cell in Column
			try {
				table.setTotalWidth(columnWidths1);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			}
		}

		PdfPTable parentTableTect = new PdfPTable(1);
		parentTableTect.setWidthPercentage(100);

		/**
		 * Date 24/1/2018
		 * By Jayshree
		 * Des.remove add element
		 */
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(para);
		
		PdfPCell tabCell=new PdfPCell(table);
//		prodtablecell.addElement(table);
		parentTableTect.addCell(prodtablecell);
		parentTableTect.addCell(tabCell);
		try {
			document.add(parentTableTect);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		//End By jayshree
	}

	private void createTools() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		Phrase productdetails = new Phrase("Tools", font10bold);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(4);//By jayshree remove one column
		table.setWidthPercentage(100);

		Phrase namephase = new Phrase("NAME", font1);
		Phrase brandphrase = new Phrase("BRAND", font1);
		Phrase modelnophrase = new Phrase("MODEL NO.", font1);
		Phrase srnophrase = new Phrase("SR. NO.", font1);
		Phrase pgidphrase = new Phrase("RECEIVED", font1);

		// Phrase installdatephrase = new Phrase("INSTALLATION DATE",font1);

		PdfPCell cellservice = new PdfPCell(namephase);
		cellservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproductid = new PdfPCell(brandphrase);
		cellproductid.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellname = new PdfPCell(modelnophrase);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcode = new PdfPCell(srnophrase);
		cellcode.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltitle = new PdfPCell(pgidphrase);
		celltitle.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell cellpgid = new PdfPCell(installdatephrase);
		// cellpgid.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellservice);
		table.addCell(cellproductid);
		table.addCell(cellname);
		table.addCell(cellcode);
//		table.addCell(celltitle);// Date 24/1/2018 Comment By jayshree
		// table.addCell(cellpgid);//

		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (int i = 0; i < this.tooltable.size(); i++) {

			Phrase chunk = null;
			if (tooltable.get(i).getName() != null) {
				chunk = new Phrase(tooltable.get(i).getName(), font8);
			}
			PdfPCell pdfservicenocell = new PdfPCell(chunk);

			if (tooltable.get(i).getBrand() != null) {
				chunk = new Phrase(tooltable.get(i).getBrand(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfprodidcell = new PdfPCell(chunk);
			if (tooltable.get(i).getModelNo() != null) {
				chunk = new Phrase(tooltable.get(i).getModelNo(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfprodnamecell = new PdfPCell(chunk);

			if (tooltable.get(i).getSrNo() != null) {
				chunk = new Phrase(tooltable.get(i).getSrNo(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcodecell = new PdfPCell(chunk);

			Phrase recphrase = new Phrase("Yes / No", font8);

			PdfPCell recevivedcellCell = new PdfPCell(recphrase);
			recevivedcellCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// pdfcodecell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// if(tooltable.get(i).getDateOfManufacture()!=null){
			// chunk = new
			// Phrase(fmt1.format(tooltable.get(i).getDateOfManufacture()),font8);
			// }else{
			// chunk = new Phrase(" ",font8);
			// }
			// PdfPCell pdftitlecell = new PdfPCell(chunk);
			// pdftitlecell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// if(tooltable.get(i).getDateOfInstallation()!=null){
			// chunk = new
			// Phrase(fmt1.format(tooltable.get(i).getDateOfInstallation()),font8);
			// }else{
			// chunk = new Phrase(" ",font8);
			// }
			// PdfPCell pdfpgidcell = new PdfPCell(chunk);
			// pdfpgidcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfservicenocell);
			table.addCell(pdfprodidcell);
			table.addCell(pdfprodnamecell);
			table.addCell(pdfcodecell);
//			table.addCell(recevivedcellCell);//Comment By jayshree to remove column
			// table.addCell(pdfpgidcell);

			try {
				table.setWidths(columnWidths3);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		PdfPTable parentTableTect = new PdfPTable(1);
		parentTableTect.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(para);
		
		/**
		 * Date 24/1/2018
		 * By Jayshree
		 * Des.to remove add element
		 */
		PdfPCell tabCell=new PdfPCell(table);
//		prodtablecell.addElement(table);
		parentTableTect.addCell(prodtablecell);
		parentTableTect.addCell(tabCell);
		
		//End By Jayshree
		try {
			document.add(parentTableTect);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void customerFeedback() {

		if (service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)) {
//
//			System.out.println("Completed status............");
			Font font1 = new Font(Font.FontFamily.HELVETICA, 8);
			Font font8 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
//
			/**
			 * Date 24/1/2018
			 * By jayshree Comment this
			 */
//			Phrase custFback = new Phrase("Customer Feedback : ", font10bold);
//			Paragraph para = new Paragraph();
//			para.add(custFback);
//			para.setAlignment(Element.ALIGN_LEFT);
//
//			PdfPTable feedtable = new PdfPTable(6);
//			feedtable.setWidthPercentage(100);
//
//			// Phrase rate = new Phrase("RATE :",font8);
//
//			Phrase poor = null;
//			if (service.getCustomerFeedback() != null) {
//				poor = new Phrase("RATE :  " + service.getCustomerFeedback(),
//						font8);
//			} else {
//				poor = new Phrase("RATE :", font8);
//			}
//
//			Phrase blank = new Phrase("", font1);
//			PdfPCell blankcell = new PdfPCell(blank);
//			blankcell.setBorder(0);
//
//			// PdfPCell cellrate = new PdfPCell(rate);
//			// cellrate.setBorder(0);
//			// cellrate.setHorizontalAlignment(Element.ALIGN_LEFT);
//			PdfPCell cellpoor = new PdfPCell(poor);
//			cellpoor.setBorder(0);
//			cellpoor.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//			feedtable.addCell(cellpoor);
//			feedtable.addCell(blankcell);
//			feedtable.addCell(blankcell);
//			feedtable.addCell(blankcell);
//			feedtable.addCell(blankcell);
//			feedtable.addCell(blankcell);
//
//			PdfPCell feedtablecell = new PdfPCell(feedtable);
//			feedtablecell.setBorder(0);
//
//			PdfPTable parentTableTect = new PdfPTable(1);
//			parentTableTect.setWidthPercentage(100);
//
//			PdfPCell prodtablecell = new PdfPCell();
//			prodtablecell.addElement(Chunk.NEWLINE);
//			prodtablecell.addElement(Chunk.NEWLINE);
//			prodtablecell.addElement(Chunk.NEWLINE);
//			prodtablecell.setBorder(0);
//
//			// parentTableTect.addCell(prodtablecell);
//			parentTableTect.addCell(feedtablecell);
//
//			try {
//				document.add(Chunk.NEWLINE);
//				document.add(Chunk.NEWLINE);
//				document.add(para);
//				document.add(parentTableTect);
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
//
			
			/**
			 * Date 24/1/2018
			 * By jayshree add this table
			 */
			PdfPTable parentTableTect = new PdfPTable(1);
			parentTableTect.setWidthPercentage(100);
			
			Phrase blank = new Phrase("", font1);
			PdfPCell blankcell = new PdfPCell(blank);
			blankcell.setBorder(0);
			parentTableTect.addCell(blankcell);
			
			//end by jayshree
			try {
				document.add(parentTableTect);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {

			Font font1 = new Font(Font.FontFamily.HELVETICA, 8);
			Font font8 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

			Phrase custFback = new Phrase("Customer Feedback : ", font10bold);
			Paragraph para = new Paragraph();
			para.add(custFback);
			para.setAlignment(Element.ALIGN_LEFT);

			PdfPTable feedtable = new PdfPTable(6);
			feedtable.setWidthPercentage(100);

			Phrase rate = new Phrase("RATE :", font8);
			Phrase poor = new Phrase("[   ] POOR ", font1);
			Phrase avg = new Phrase("[   ] AVERAGE ", font1);
			Phrase good = new Phrase("[   ] GOOD ", font1);
			Phrase verygood = new Phrase("[   ] VERY GOOD ", font1);
			Phrase execellent = new Phrase("[   ]  EXCELLENT ", font1);

			PdfPCell cellrate = new PdfPCell(rate);
			cellrate.setBorder(0);
			cellrate.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell cellpoor = new PdfPCell(poor);
			cellpoor.setBorder(0);
			cellpoor.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell cellavg = new PdfPCell(avg);
			cellavg.setBorder(0);
			cellavg.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell cellgood = new PdfPCell(good);
			cellgood.setBorder(0);
			cellgood.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell cellverygood = new PdfPCell(verygood);
			cellverygood.setBorder(0);
			cellverygood.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell cellexecellent = new PdfPCell(execellent);
			cellexecellent.setBorder(0);
			cellexecellent.setHorizontalAlignment(Element.ALIGN_CENTER);

			feedtable.addCell(cellrate);
			feedtable.addCell(cellpoor);
			feedtable.addCell(cellavg);
			feedtable.addCell(cellgood);
			feedtable.addCell(cellverygood);
			feedtable.addCell(cellexecellent);

			PdfPCell feedtablecell = new PdfPCell(feedtable);
			feedtablecell.setBorder(0);

			PdfPTable parentTableTect = new PdfPTable(1);
			parentTableTect.setWidthPercentage(100);

			PdfPCell prodtablecell = new PdfPCell();
			prodtablecell.addElement(Chunk.NEWLINE);
			prodtablecell.addElement(Chunk.NEWLINE);
			prodtablecell.addElement(Chunk.NEWLINE);
			prodtablecell.setBorder(0);

			parentTableTect.addCell(prodtablecell);
			parentTableTect.addCell(feedtablecell);

			try {
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				document.add(para);
				document.add(parentTableTect);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}

	}

	private void customerAndTechSign() {

		/**
		 * Date 24/1/2018
		 * By Jayshree
		 */
//		PdfPTable signtable = new PdfPTable(2);
//		signtable.setWidthPercentage(100);
//		try {
//			signtable.setWidths(columnWidths4);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		Phrase dotedline = new Phrase("------------------------------",
//				font10bold);
//		Phrase custsign = new Phrase("Customer Signature", font10bold);
//		Phrase techsign = new Phrase("Technician Signature", font10bold);
//
//		PdfPCell dotedcell = new PdfPCell();
//		dotedcell.addElement(dotedline);
//		dotedcell.setBorder(0);
//		dotedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPCell custcell = new PdfPCell();
//		custcell.addElement(custsign);
//		custcell.setBorder(0);
//		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPCell dotedcell1 = new PdfPCell();
//		dotedcell1.addElement(dotedline);
//		dotedcell1.setBorder(0);
//		dotedcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
//		PdfPCell techcell = new PdfPCell();
//		techcell.addElement(techsign);
//		techcell.setBorder(0);
//		techcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//
////		signtable.addCell(dotedcell);//comment By jayshree
//
//		signtable.addCell(dotedcell1);
//
////		signtable.addCell(custcell);//Comment by jayshree
//
//		signtable.addCell(techcell);
//
//		signtable.setSpacingBefore(50f);
//
//		try {
//			document.add(signtable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}

		//end by jayshree
		/**
		 * Date 23/1/2018
		 * By Jayshree
		 * Des.to add the time and date column
		 */
		/*****customer sign***/
//		PdfPTable customersign=new PdfPTable(1) ;
//		customersign.setWidthPercentage(100);
//		customersign.setSpacingBefore(50);
//		Phrase dotedline = new Phrase(" ",
//				font10bold);
//		Phrase custsign = new Phrase(" ", font10bold);
//		
//		PdfPCell dotedcell = new PdfPCell();
//		dotedcell.addElement(dotedline);
//		dotedcell.setBorder(0);
//		dotedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		PdfPCell custcell = new PdfPCell();
//		custcell.addElement(custsign);
//		custcell.setBorder(0);
//		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase custblank=new Phrase ("",font10);
//		PdfPCell custBlankCell=new PdfPCell(custblank);
//		custBlankCell.setBorder(0);
//		customersign.addCell(custBlankCell);
//		
////		customersign.addCell(custBlankCell);
////		customersign.addCell(custBlankCell);
////		customersign.addCell(custBlankCell);
////		customersign.addCell(custBlankCell);
//		customersign.addCell(dotedcell);
//		customersign.addCell(custcell);
		
		
		/**
		 * Date 31-3-2018
		 * By jayshree
		 * Des.to add cust sign 
		 */
		PdfPTable customersign=new PdfPTable(1) ;
		customersign.setWidthPercentage(100);
		customersign.setSpacingBefore(50);
		
		PdfPTable customersignpro=new PdfPTable(1) ;
		customersignpro.setWidthPercentage(100);
		customersignpro.setSpacingBefore(50);
		if(serviceSignature==true){
			
			Phrase dotedline2 = new Phrase("------------------------------",
					font10bold);
			Phrase custsign2 = new Phrase("Customer Signature ", font10bold);
			
			PdfPCell dotedcell2 = new PdfPCell();
			dotedcell2.addElement(dotedline2);
			dotedcell2.setBorder(0);
			dotedcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			PdfPCell custcell2 = new PdfPCell();
			custcell2.addElement(custsign2);
			custcell2.setBorder(0);
			custcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase custblank2=new Phrase (" ",font10);
			PdfPCell custBlankCell2=new PdfPCell(custblank2);
			custBlankCell2.setBorder(0);
		
//			customersignpro.addCell(custBlankCell2);
//			customersignpro.addCell(custBlankCell2);
//			customersignpro.addCell(custBlankCell2);
//			customersignpro.addCell(custBlankCell2);
//			customersignpro.addCell(custBlankCell2);
			customersignpro.addCell(dotedcell2);
			customersignpro.addCell(custcell2);
		}
		
		else{
		Phrase dotedline = new Phrase(" ",
				font10bold);
		Phrase custsign = new Phrase(" ", font10bold);
		
		PdfPCell dotedcell = new PdfPCell();
		dotedcell.addElement(dotedline);
		dotedcell.setBorder(0);
		dotedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell custcell = new PdfPCell();
		custcell.addElement(custsign);
		custcell.setBorder(0);
		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase custblank=new Phrase ("",font10);
		PdfPCell custBlankCell=new PdfPCell(custblank);
		custBlankCell.setBorder(0);
		customersign.addCell(custBlankCell);
		
		customersign.addCell(custBlankCell);
//		customersign.addCell(custBlankCell);
//		customersign.addCell(custBlankCell);
//		customersign.addCell(custBlankCell);
		customersign.addCell(dotedcell);
		customersign.addCell(custcell);
		
		//End By jayshree
		}
		/**** Time date***/
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8);
		Font font8 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		PdfPTable timeDatetab=new PdfPTable(2); 
		timeDatetab.setWidthPercentage(100);
		timeDatetab.setSpacingAfter(50);
		try {
			timeDatetab.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statusph=new Phrase ("STATUS",font8);
		PdfPCell statusCell=new PdfPCell(statusph);
		statusCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		timeDatetab.addCell(statusCell);
		
		Phrase dateph=new Phrase ("DATE TIME",font8);
		PdfPCell dateCell=new PdfPCell(dateph);
		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		timeDatetab.addCell(dateCell);
		
//		Phrase timeph=new Phrase ("TIME",font8);
//		PdfPCell timeCell=new PdfPCell(timeph);
//		timeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		timeDatetab.addCell(timeCell);
		
		System.out.println("hiiiiii");
		System.out.println("size"+service.getTrackServiceTabledetails().size());
		for (int i = 0; i < service.getTrackServiceTabledetails().size(); i++) {
			
		Phrase startedserph=new Phrase (service.getTrackServiceTabledetails().get(i).getStatus(),font1);
		PdfPCell startedserCell=new PdfPCell(startedserph);
		startedserCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		timeDatetab.addCell(startedserCell);
		
		Phrase startedserdateph=new Phrase (service.getTrackServiceTabledetails().get(i).getDate_time(),font1);
		PdfPCell startedserdateCell=new PdfPCell(startedserdateph);
		startedserdateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		timeDatetab.addCell(startedserdateCell);
		
		
		
		
//		Phrase startedsertimeph=new Phrase (" ",font1);
//		PdfPCell startedsertimeCell=new PdfPCell(startedsertimeph);
//		startedsertimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(startedsertimeCell);
		
//		Phrase reportededserph=new Phrase ("Reported Service",font1);
//		PdfPCell reportededserCell=new PdfPCell(reportededserph);
//		reportededserCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(reportededserCell);
//		
//		Phrase reportedDateph=new Phrase (" ",font1);
//		PdfPCell reportedDateCell=new PdfPCell(reportedDateph);
//		reportedDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(reportedDateCell);
//		
//		Phrase reportedTimeph=new Phrase (" ",font1);
//		PdfPCell reportedTimeCell=new PdfPCell(reportedTimeph);
//		reportedTimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(reportedTimeCell);
//		
//		Phrase completeph=new Phrase ("Completed By Technician",font1);
//		PdfPCell completeCell=new PdfPCell(completeph);
//		completeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(completeCell);
//		
//		Phrase completeDateph=new Phrase (" ",font1);
//		PdfPCell completeDateCell=new PdfPCell(completeDateph);
//		completeDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(completeDateCell);
//		
//		Phrase completeTimeph=new Phrase (" ",font1);
//		PdfPCell completeTimeCell=new PdfPCell(completeTimeph);
//		completeTimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		timeDatetab.addCell(completeTimeCell);
		
		}
		PdfPTable techniciansign=new PdfPTable(1) ;
		techniciansign.setWidthPercentage(100);
		techniciansign.setSpacingBefore(50);
		
		
		
		Phrase dotedline2 = new Phrase("------------------------------",
				font10bold);
		Phrase techsign = new Phrase("Technician Signature", font10bold);
		
		PdfPCell dotedcell2 = new PdfPCell(dotedline2);
//		dotedcell2.addElement(dotedline2);
		dotedcell2.setBorder(0);
		dotedcell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell techsigncell = new PdfPCell(techsign);
//		techsigncell.addElement(techsign);
		techsigncell.setBorder(0);
		techsigncell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase blanktec=new Phrase(" ",font10);
		PdfPCell blankCelltec=new PdfPCell(blanktec);
		blankCelltec.setBorder(0);
		techniciansign.addCell(blankCelltec);
		
//		techniciansign.addCell(blankCelltec);
//		techniciansign.addCell(blankCelltec);
//		techniciansign.addCell(blankCelltec);
//		techniciansign.addCell(blankCelltec);
		
		techniciansign.addCell(dotedcell2);
		techniciansign.addCell(techsigncell);
		
		PdfPTable mainfooter=new PdfPTable(3);
		mainfooter.setWidthPercentage(100);
		
		try {
			mainfooter.setWidths(new float[]{30,40,30});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
		{
		
//		Phrase blank=new Phrase(" ",font10);
//		PdfPCell blankCell=new PdfPCell(blank);
//		blankCell.setBorder(0);
//		mainfooter.addCell(blankCell);
			
		Phrase servicecomph=new Phrase("Service Completion Date : "+fmt.format(service.getServiceCompletionDate()),font1);
		PdfPCell servicecompCell=new PdfPCell(servicecomph);
		servicecompCell.setBorder(0);
		servicecompCell.setPaddingTop(25);
		servicecompCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		mainfooter.addCell(servicecompCell);
		
		
			
		}
		else
		{
//		PdfPCell custSign=new PdfPCell(customersign);
//		custSign.setHorizontalAlignment(Element.ALIGN_LEFT);
//		custSign.setBorder(0);
//		mainfooter.addCell(custSign);
			

			/**
			 * Date 31-3-2018
			 * By jayshree
			 * Add cust sign
			 */
			if(serviceSignature==true){
				System.out.println("serviceSignature==true");
				PdfPCell custSignpro=new PdfPCell(customersignpro);
				custSignpro.setHorizontalAlignment(Element.ALIGN_LEFT);
				custSignpro.setBorder(0);
				mainfooter.addCell(custSignpro);
			}
			else{
				System.out.println("serviceSignature==false");
				PdfPCell custSign=new PdfPCell(customersign);
				custSign.setHorizontalAlignment(Element.ALIGN_LEFT);
				custSign.setBorder(0);
				mainfooter.addCell(custSign);
			}
			
			//End By Jayshree
		
		}
		if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED))
		{
			if(service.getTrackServiceTabledetails().size()!=0)
			{
		
				PdfPCell dateTimeSign=new PdfPCell(timeDatetab);
				dateTimeSign.setHorizontalAlignment(Element.ALIGN_CENTER);
				dateTimeSign.setBorder(0);
				mainfooter.addCell(dateTimeSign);
			}
			else
			{
				Phrase blank=new Phrase(" ",font10);
				PdfPCell blankCell=new PdfPCell(blank);
				blankCell.setBorder(0);
				mainfooter.addCell(blankCell);
			
			}
		}
		else
		{
		Phrase blank=new Phrase(" ",font10);
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		mainfooter.addCell(blankCell);
		}
		PdfPCell technSign=new PdfPCell(techniciansign);
		technSign.setHorizontalAlignment(Element.ALIGN_RIGHT);
		technSign.setBorder(0);
		mainfooter.addCell(technSign);
		
		try {
			document.add(mainfooter);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//end by jsyshree
}
