package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.Contract;
public class ContractPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4196399220156880572L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			
			 String preprintStatus=req.getParameter("preprint");
			 
			ContractPdf conpdf = new ContractPdf();
			
			conpdf.document = new Document();
			Document document = conpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			 document.open();
			 conpdf.setContract(count);
			  if(preprintStatus.contains("yes")){
				  
					System.out.println("yes ");
					conpdf.createPdf(preprintStatus);
			  }
			  else
			  {
					System.out.println("no ");
					conpdf.createPdf(preprintStatus);
			  }
			 
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
