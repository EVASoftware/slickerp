package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
public class reddysPestControlInvoicePdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3026655252319124574L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			
			reddysPestControlInvoice rinvpdf = new reddysPestControlInvoice();
			
			rinvpdf.document = new Document();
			Document document = rinvpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream());
			
			 
			 document.open();
			 rinvpdf.setpdfrinvoice(count);
			 rinvpdf.createPdf();
			 
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
