package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.text.ParseException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.TermsAndConditions;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class PdfUpdated {
	Sales qp;
	ArrayList<Sales> sales;
	List<SalesLineItem> products;
	List<PaymentTerms> payTermsLis;
	List<ProductOtherCharges> prodCharges;
	List<ProductOtherCharges> prodTaxes;
	List<ArticleType> articletype;
	Customer cust;
	Company comp;
	SuperProduct sup;
	SalesOrder soEntity;
	SalesQuotation salesquotEntity;
	public Document document;
	public GenricServiceImpl impl;
	private Font font10boldul, font16boldul, font12bold, font8bold, font8,
			font12boldul, font12, font10bold, font10, font16bold, font14bold,
			font14, font9, font9bold, font7, font6bold,font6,font7bold;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	ProcessConfiguration processConfig;
	boolean CompanyNameLogoflag = false;
	CompanyPayment comppayment, comppayment1;
	float[] columnWidths = { 0.5f, 3.5f, 0.7f, 0.5f, 1f, 0.5f, 0.9f, 1f };
	float[] column16CollonWidth = { 0.1f, 0.4f, 0.2f, 0.18f, 0.15f, 0.26f,
			0.28f, 0.21f, 0.28f, 0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.3f };
	float[] column16CollonWidthnew = { 0.2f, 0.94f, 0.2f, 0.18f, 0.15f, 0.26f,
			 0.21f, 0.28f, 0.19f,  0.19f,  0.19f, 0.3f };
	float[] columnWidths2 = { 3f, 7f };
	int BreakPoint = 6;
	float blankLines;
	float blanklineother;
	int firstBreakPoint = 14;
	int flag = 0;
	int flag1 = 0;
	int count = 0;
	int eva = 123;
	Phrase chunk;
	int cut = 0;
	double total = 0;
	ArrayList<SuperProduct> stringlis = new ArrayList<SuperProduct>();
	Logger logger = Logger.getLogger("Salesorder");

	/**
	 * Rohan added this for Universal pest for printing
	 */
	Boolean multipleCompanyName = false;
	List<State> stateList;
	/**
	 * ends here
	 */

	/** Total Amount */
	double totalAmount;
	Phrase blankCell = new Phrase(" ", font8);
	boolean productDetails=false;
	/*
	 *  **************** NOTE **************************** when you make any
	 * changes in the pdf please make corresponding changes in
	 * createPdfForEmail() Method as well so that it will show same pdf in email
	 * as printed from print button
	 */

	// ******************rohan taken variables for removing Disc column from
	// pdfs ************8
	int both = 0;
	int discAmt = 0;
	int discPer = 0;
	int nothing = 0;
	
	/**
	 * Date 31-3-2018
	 * By jayshre
	 * des.to add the branch mail and ho mail
	 */
	boolean checkEmailId = false;
	boolean hoEmail=false;
	/**
	 * nidhi
	 * 18-06-2018
	 */
	boolean printPremiseDetails =false;
	
	/**
	 * Updated By: Viraj
	 * Date: 20-12-2018
	 * Description: To display product description
	 */
	boolean printProductDesc = false;
	
	/**
	 * Date 22-11-2018 By Vijay 
	 * Des :- if process configuration is active then GST Number will not display
	 * and GST Number will display if GST applicable or not applicable as per nitin sir
	 */
	boolean gstNumberPrintFlag = true;
	/**
	 *  Date : 11-03-2021 by Priyanka
	 *  Des : - BranchAsCompany Process configuration activation.
	 */
	Branch branchDt = null;
	
	boolean PC_DoNotPrintQtyFlag= false;
	boolean PC_DoNotPrintUOMFlag = false;
	boolean PC_DoNotPrintRateFlag = false;
	boolean PC_DoNotPrintDiscFlag = false;
	boolean PC_DoNotPrintTaxFlag = false;
	
	public PdfUpdated() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 7);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font14 = new Font(Font.FontFamily.HELVETICA, 14);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

		// rohan added this fornts for gst formating
		font7 = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font6bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font6 = new Font(Font.FontFamily.HELVETICA, 8);//Add by jayshree
		impl = new GenricServiceImpl();
	}

	public void getSalesOrder(long count) {
		// Load Contract

		qp = ofy().load().type(SalesOrder.class).id(count).now();
		int contractCount = qp.getCount();
		// Load Customer
		if (qp.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId())
					.filter("companyId", qp.getCompanyId()).first().now();
		if (qp.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();

		if (qp.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "SalesOrder")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintCompanyNameAsLogo")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						CompanyNameLogoflag = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDetails = true;
					}
					
					/**
					 * date 1-3-2018
					 * by jayshree
					 * Des.to print the branch email and ho 
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintHOEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hoEmail = true;
					}
					/**
					 * nidhi
					 * 18-06-2018
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}
					/**
					 * end
					 */
					//End By Jayshree
					
					/*** Date 23-11-2018 By Vijay For GST Number Print or not ****/ 
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("EnableDoNotPrintGSTNumber")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						gstNumberPrintFlag = false;
					}
					/**
					 * ends here
					 */
					
				}
			}
			
			
		}

		// load company payment

		
		if (qp.getCompanyId() == null) {
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();
		}

		if (qp.getCompanyId() != null) {
			comppayment1 = ofy().load().type(CompanyPayment.class)
					.filter("companyId", qp.getCompanyId())
					.filter("paymentDefault", false)
					.filter("paymentPrint", true).first().now();
		}

		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (qp.getCompanyId() != null)
			soEntity = ofy().load().type(SalesOrder.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count", qp.getCount()).first().now();
		else
			soEntity = ofy().load().type(SalesOrder.class)
					.filter("count", qp.getCount()).first().now();
		
		
		//Ashwini Patil Date:9-05-2023
		if(soEntity!=null&&soEntity.getPaymentMode()!=null&&!soEntity.getPaymentMode().equals("")){
			List<String> paymentDt = Arrays.asList(soEntity.getPaymentMode().trim().split("/"));
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", soEntity.getCompanyId()).first()
						.now();
			}
		}else if (qp.getCompanyId() != null) {
			
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", qp.getCompanyId()).first().now();
		}

			

		stringlis = new ArrayList<SuperProduct>();
		for (int i = 0; i < soEntity.getItems().size(); i++) {
			
			boolean detailsPd = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "SalesOrderProductDetailsFromTable",  qp.getCompanyId());
			if(detailsPd){
			
				stringlis.add(soEntity.getItems().get(i).getPrduct());
			}else{
					if (qp.getCompanyId() != null) {
						sup = ofy()
								.load()
								.type(SuperProduct.class)
								.filter("companyId", qp.getCompanyId())
								.filter("count",
										soEntity.getItems().get(i).getPrduct()
												.getCount()).first().now();
		
		//				SuperProduct superprod = new SuperProduct();
		////
		//				superprod.setCount(sup.getCount());
		//				superprod.setProductName(sup.getProductName());
		////				superprod.setComment(sup.getComment() + ""
		////						+ superprod.getCommentdesc());
		//				superprod.setProductImage(sup.getProductImage());
		//				superprod.setProductImage1(sup.getProductImage1());
		//				superprod.setProductImage2(sup.getProductImage2());
		//				superprod.setProductImage3(sup.getProductImage3());
		//				sup.getComment();
						/**
						 * nidhi
						 * 
						 */
		//				ItemProduct itm = new ItemProduct();
		//				stringlis.add(itm);
						stringlis.add(sup);
						
						System.out.println("superprod.setComment"+sup.getComment());
						System.out.println("superprod.setComment"+sup.getCommentdesc());
		
					}

					else {
						sup = ofy()
								.load()
								.type(SuperProduct.class)
								.filter("count",
										soEntity.getItems().get(i).getPrduct()
												.getCount()).first().now();
					}
			}
			}

		products = qp.getItems();
		prodCharges = qp.getProductCharges();
		prodTaxes = qp.getProductTaxes();
		payTermsLis = qp.getPaymentTermsList();

		/************************************ discount Flag *******************************/

		if (soEntity.getCompanyId() != null) {
			for (int i = 0; i < products.size(); i++) {
				if ((products.get(i).getDiscountAmt() > 0)
						&& (products.get(i).getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (products.get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (products.get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}

			}

			System.out.println("both" + both + " discAmt" + discAmt + " disper"
					+ discPer + " nothing" + nothing);
		}

		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());
		}

		stateList = ofy().load().type(State.class)
				.filter("companyId", soEntity.getCompanyId()).list();
		
		/**
		 *  added by Priyanka.
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(soEntity !=null && soEntity.getBranch() != null && soEntity.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",soEntity.getCompanyId()).filter("buisnessUnitName", soEntity.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", soEntity.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
							}
						
						}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		
		
	}

	public void getSalesQuotation(long count) {
		// Load Contract

		qp = ofy().load().type(SalesQuotation.class).id(count).now();
		int contractCount = qp.getCount();
		List<Service> services = null;

		// Load Customer

		if (qp.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId())
					.filter("companyId", qp.getCompanyId()).first().now();
		if (qp.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();

		if (qp.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "SalesQuotation")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintCompanyNameAsLogo")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						CompanyNameLogoflag = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDetails = true;
					}
					
					/**
					 * date 1-3-2018
					 * by jayshree
					 * Des.to print the branch email and ho 
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintHOEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hoEmail = true;
					}
					/**
					 * nidhi
					 * 18-06-2018
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}
					/**
					 * end
					 */
					//End By Jayshree
					
					/*** Date 23-11-2018 By Vijay For GST Number Print or not ****/ 
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("EnableDoNotPrintGSTNumber")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						gstNumberPrintFlag = false;
					}
					/**
					 * ends here
					 */
					/**
					 * Updated By: Viraj
					 * Date: 20-12-2018
					 * Description: To display product description
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductDescInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("Inside prod desc process configuration");
						printProductDesc = true;
					}
					/** Ends **/
					
				}
			}
		}

		// load company payment

		if (qp.getCompanyId() != null) {
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", qp.getCompanyId()).first().now();
		}

		if (qp.getCompanyId() == null) {
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();
		}

		if (qp.getCompanyId() != null) {
			comppayment1 = ofy().load().type(CompanyPayment.class)
					.filter("companyId", qp.getCompanyId())
					.filter("paymentDefault", false)
					.filter("paymentPrint", true).first().now();
		}

		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (qp.getCompanyId() != null)
			salesquotEntity = ofy().load().type(SalesQuotation.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count", qp.getCount()).first().now();
		else
			salesquotEntity = ofy().load().type(SalesQuotation.class)
					.filter("count", qp.getCount()).first().now();

		stringlis = new ArrayList<SuperProduct>();
		for (int i = 0; i < salesquotEntity.getItems().size(); i++) {

//			int companyId = (qp.getCustomerId() != null) ? qp.getCustomerId() : ;
			boolean detailsPd = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", "SalesQuotationProductDetailsFromTable",  qp.getCompanyId());
			if(detailsPd){
				
				stringlis.add(salesquotEntity.getItems().get(i).getPrduct());
			}else{
				if (qp.getCompanyId() != null) {
					{
						sup = ofy()
								.load()
								.type(SuperProduct.class)
								.filter("companyId", qp.getCompanyId())
								.filter("count",
										salesquotEntity.getItems().get(i).getPrduct()
												.getCount()).first().now();
	
	//					SuperProduct superprod = new SuperProduct();
		//
	//					superprod.setCount(sup.getCount());
	//					superprod.setProductName(sup.getProductName());
	//					superprod.setComment(sup.getComment() + "\n"
	//							+ sup.getCommentdesc()+"");
	//					superprod.setProductImage(sup.getProductImage());
	//					superprod.setProductImage1(sup.getProductImage1());
	//					superprod.setProductImage2(sup.getProductImage2());
	//					superprod.setProductImage3(sup.getProductImage3());
						stringlis.add(sup);
						
					}
					
				}
			}
		}

		products = qp.getItems();
		prodCharges = qp.getProductCharges();
		prodTaxes = qp.getProductTaxes();
		payTermsLis = qp.getPaymentTermsList();

		/************************************ discount Flag *******************************/

		if (qp.getCompanyId() != null) {
			for (int i = 0; i < products.size(); i++) {
				if ((products.get(i).getDiscountAmt() > 0)
						&& (products.get(i).getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (products.get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (products.get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}

			}

			System.out.println("both" + both + " discAmt" + discAmt + " disper"
					+ discPer + " nothing" + nothing);
		}

		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());
		}

		stateList = ofy().load().type(State.class)
				.filter("companyId", salesquotEntity.getCompanyId()).list();
		
		
		if(salesquotEntity!=null && qp instanceof SalesQuotation) {
			if(salesquotEntity.getPaymentModeName()!=null && !salesquotEntity.getPaymentModeName().equals("")) {
			List<String> paymentDt = Arrays.asList(salesquotEntity.getPaymentModeName().trim().split("/"));
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", salesquotEntity.getCompanyId()).first()
						.now();
			
				logger.log(Level.SEVERE,"Quotation level payment mode" +comppayment);

			}
			}
		}
		
		
		/**
		 *   added by Priyanka
		 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(salesquotEntity !=null && salesquotEntity.getBranch() != null && salesquotEntity.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",salesquotEntity.getCompanyId()).filter("buisnessUnitName", salesquotEntity.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", salesquotEntity.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
							}
						
						}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		PC_DoNotPrintUOMFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.PC_DONOTPRINTUOM, salesquotEntity.getCompanyId());
		PC_DoNotPrintQtyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.PC_DONOTPRINTQUANTITY, salesquotEntity.getCompanyId());
		PC_DoNotPrintRateFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.PC_DONOTPRINTRATE, salesquotEntity.getCompanyId());
		PC_DoNotPrintDiscFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.PC_DONOTPRINTDISCOUNT, salesquotEntity.getCompanyId());
		PC_DoNotPrintTaxFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.PC_DONOTPRINTTAX, salesquotEntity.getCompanyId());

		
	}

	public void createPdf(String preprintStatus) {
		// rohan added this code for new gst formats

		Date saleOderOrQuotationDate = null;

		if (qp instanceof SalesOrder) {
			SalesOrder salesOderEntity = (SalesOrder) qp;
			this.soEntity = salesOderEntity;
			saleOderOrQuotationDate = salesOderEntity.getSalesOrderDate();
		} else {
			SalesQuotation quotation = (SalesQuotation) qp;
			this.salesquotEntity = quotation;
			saleOderOrQuotationDate = quotation.getQuotationDate();
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		boolean flag = false;
		try {
			flag = saleOderOrQuotationDate.after(sdf.parse("30 Jun 2017"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// ends here

		if (!flag) {
			System.out.println("print createPdf");
			if (preprintStatus.equals("plane")) {

				System.out.println("contract inside plane");

				createblankline();
				if (CompanyNameLogoflag == true) {
//					createLogo(document, comp);//Date 23/11/2017 comment by Jayshree to remove logo tab
				} else {
//					createLogo123(document, comp);//Date 23/11/2017 comment by Jayshree to remove logo tab
				}

				createHeadingInfo();
			} else {

				
				
				/**
				 * @author Vijay Date :- 30-03-2023
				 * Des :- Bitco pest company introduction will print on 1st page
				 */
				boolean companyIntroductionFlag = false;
				if(qp instanceof SalesQuotation && salesquotEntity!=null){
					logger.log(Level.SEVERE, "companyIntroduction"+comp.getCompanyIntroduction());
					Company companyEntity = ofy().load().type(Company.class).filter("companyId", salesquotEntity.getCompanyId()).first().now();

					PdfUtility pdfutility = new PdfUtility();
					ArrayList<Paragraph> paraList=pdfutility.addCompanyIntroduction(companyEntity.getCompanyIntroduction(),font10bold,font10);
					
//					Paragraph para = pdfutility.addCompanyIntroduction(companyEntity.getCompanyIntroduction(),font10bold,font10);
					
					try {
						if(paraList!=null){

							for(Paragraph para:paraList){
								
								Paragraph blank = new Paragraph();
								blank.add(Chunk.NEWLINE);
								
								
									if (preprintStatus.equals("yes")) {
										try {
											document.add(blank);
											document.add(blank);
											document.add(blank);
											document.add(blank);
											document.add(blank);
											document.add(blank);
										} catch (Exception e) {
										}
											
									}
									if (preprintStatus.equals("no")) {
										if (comp.getUploadHeader() != null) {
											createCompanyNameAsHeader(document, companyEntity);
										}
										if (comp.getUploadFooter() != null) {
											createCompanyNameAsFooter(document, companyEntity);
										}
										
										if(comp.getUploadHeader()!=null && comp.getUploadFooter() != null){
											try {
												document.add(blank);
												document.add(blank);
												document.add(blank);
												document.add(blank);
												document.add(blank);
												document.add(blank);
											} catch (Exception e) {
											}
										}
									}
								
								document.add(para);
//								companyIntroductionFlag = true;
								Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
								try {
									document.add(nextpage);
								} catch (DocumentException e1) {
									e1.printStackTrace();
								}

							}//end of for
							
						}//end of if
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}
				
				if(companyIntroductionFlag){
					Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
					try {
						document.add(nextpage);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				}
				
				/**
				 * ends here
				 */
				
				
				if (preprintStatus.equals("yes")) {

					System.out.println("inside prit yes");
					createSpcingForHeading();
					createCompanyAddressDetails();
				}

				if (preprintStatus.equals("no")) {

					System.out.println("inside prit no");

					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}

					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					createSpcingForHeading();
					createCompanyAddressDetails();
				}
			}

			if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
				createProductDetailWithDiscount();
			} else if (nothing > 0) {
				createProductDetailWithOutDisc();
			} else {
				createProductInfo();
			}

			termsConditionsInfo();
			footerInfo();

		} else {
			/** date 9/11/2017 added by komal to get only one copy of sales quotation  **/			
					System.out.println("Rohan inside else conditions ");
					
					
					/**
					 * @author Vijay Date :- 04-04-2023
					 * Des :- Bitco pest company introduction will print on 1st page
					 */
					boolean companyIntroductionFlag = false;
					if(qp instanceof SalesQuotation && salesquotEntity!=null){
						logger.log(Level.SEVERE, "companyIntroduction"+comp.getCompanyIntroduction());
						logger.log(Level.SEVERE, "companyIntroduction"+comp.getBusinessUnitName());
						Company companyEntity = ofy().load().type(Company.class).filter("companyId", salesquotEntity.getCompanyId()).first().now();

						PdfUtility pdfutility = new PdfUtility();
						
						ArrayList<Paragraph> paraList = pdfutility.addCompanyIntroduction(companyEntity.getCompanyIntroduction(),font10bold,font10);

						try {
							if(paraList!=null){


								for(Paragraph para:paraList){
								
								Paragraph blank = new Paragraph();
								blank.add(Chunk.NEWLINE);
								
								if (preprintStatus.equals("plane")) {
//									try {
//										document.add(blank);
//										document.add(blank);
//										document.add(blank);
//										document.add(blank);
//										document.add(blank);
//										document.add(blank);
//									} catch (Exception e) {
//									}
									
									createHeader(0);

									
								} else {
									if (preprintStatus.equals("yes")) {
										try {
											document.add(blank);
											document.add(blank);
											document.add(blank);
											document.add(blank);
											document.add(blank);
											document.add(blank);
										} catch (Exception e) {
										}
											
									}
									if (preprintStatus.equals("no")) {
										if (companyEntity.getUploadHeader() != null) {
											createCompanyNameAsHeader(document, comp);
										}
										if (companyEntity.getUploadFooter() != null) {
											createCompanyNameAsFooter(document, comp);
										}
										
										if(comp.getUploadHeader()!=null && comp.getUploadFooter() != null){
											try {
												document.add(blank);
												document.add(blank);
												document.add(blank);
												document.add(blank);
												document.add(blank);
												document.add(blank);
											} catch (Exception e) {
											}
										}
									}
								}
								
								document.add(para);
//								companyIntroductionFlag = true;
								Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
								try {
									document.add(nextpage);
								} catch (DocumentException e1) {
									e1.printStackTrace();
								}
							}
							
							}
						} catch (DocumentException e) {
							e.printStackTrace();
						}
					}
					
					if(companyIntroductionFlag){
						Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
						try {
							document.add(nextpage);
						} catch (DocumentException e1) {
							e1.printStackTrace();
						}
					}
					
					/**
					 * ends here
					 */
					
					// upcflag==false &&
					if (preprintStatus.equals("plane")) {
//						createLogo(document, comp);//Date 23/11/2017 comment by Jayshree to remove logo tab
						createHeader(0);
						// createCompanyAddress();
					} else {

						if (preprintStatus.equals("yes")) {
							System.out.println("inside prit yes");
//							createBlankforUPC(0);//Comment By jayshree
							createBlankforUPC2(0);
						}
						if (preprintStatus.equals("no")) {
							if (comp.getUploadHeader() != null) {
								createCompanyNameAsHeader(document, comp);
							}

							if (comp.getUploadFooter() != null) {
								createCompanyNameAsFooter(document, comp);
							}
//							createBlankforUPC(0);//Comment by jayshree
							createBlankforUPC2(0);
						}
					}
//					createInvoiceDetails();//Comment by jayshree
					createInvoiceDetails2();
//					createCustomerDetails();//Comment By Jayshree
					createCustomerDetails2();
//					createProductDetails();//Comment by jayshree
					createProductDetails2();
//					createProductDetailsVal();//Comment by jayshree
					createProductDetailsVal2();
					createotherCharges();//add by jayshree
					
					if(qp instanceof SalesOrder){
						
						System.out.println("SALES ORDER TAX");
						SalesOrder salesOrder=(SalesOrder) qp;
//						createFooterTaxPartSalesOrder(salesOrder);//comment by jayshree
						createFooterTaxPartSalesOrder2(salesOrder);
						
						
					}else{
						SalesQuotation salesQuotation=(SalesQuotation) qp;
//						createFooterTaxPart(salesQuotation);//comment by jayshree
						createFooterTaxPart2(salesQuotation);
					}
					
					createtermsAndCondition();
//					createFooterLastPart(preprintStatus);//comment by jayshree
					
					createFooterLastPart2(preprintStatus);
					if(productDetails){
						try {
							createServices();
						} catch (DocumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
//					try {
//						document.add(Chunk.NEXTPAGE);
//					} catch (DocumentException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
					
					ServerAppUtility serverAppUtility = new ServerAppUtility();
					if(qp instanceof SalesQuotation && salesquotEntity!=null){
						if(salesquotEntity.isPrintAllTermsConditions()){
							serverAppUtility.createTermAndConditionsPage(document, "SalesQuotation",qp.getCompanyId());
						}
						else{
							System.out.println("salesquotEntity.getTermsAndConditionlist() "+salesquotEntity.getTermsAndConditionlist().size());
							if(salesquotEntity.getTermsAndConditionlist()!=null && salesquotEntity.getTermsAndConditionlist().size()>0){
								createTermsAndConditionPage(document,salesquotEntity.getTermsAndConditionlist(),"SalesQuotation",salesquotEntity.getCompanyId(),preprintStatus);
							}
						}
					}
//					else{
//						serverAppUtility.createTermAndConditionsPage(document, "SalesOrder",qp.getCompanyId());
//					}


		
		}
		
		
	}

	

	

	

	private void createTermsAndConditionPage(Document document,ArrayList<TermsAndConditions> termsAndConditionlist, String documentName, long companyId,String preprintStatus) {
		

		logger.log(Level.SEVERE,"in createTermAndConditionsPage preprintStatus="+preprintStatus);
		PdfUtility pdfUtility=new PdfUtility();
		Font font10bold,font8;
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font10bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		// TODO Auto-generated method stub
		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		try {
			document.add(nextpage);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		List<TermsAndConditions> terms = ofy().load().type(TermsAndConditions.class)
				.filter("companyId",companyId)
				.filter("document", documentName)
				.filter("status", true).list();
		
		if(terms!=null){
			logger.log(Level.SEVERE,"terms size=" +terms.size());
//			
			Comparator<TermsAndConditions> sequenceComparator=new Comparator<TermsAndConditions>() {
				@Override
				public int compare(TermsAndConditions arg0, TermsAndConditions arg1) {
					return arg0.getSequenceNumber().compareTo(arg1.getSequenceNumber());
				}
			};
			Collections.sort(terms, sequenceComparator);
			
			for(TermsAndConditions termsCondition : termsAndConditionlist){
				
				for(TermsAndConditions term:terms){
					System.out.println("termsCondition.getTitle().trim() "+termsCondition.getTitle().trim());
					System.out.println("term.getTitle().trim()"+term.getTitle().trim());
					System.out.println("termsCondition.getTitle().trim().equals(term.getTitle().trim()) "+termsCondition.getTitle().trim().equals(term.getTitle().trim()));
					if(termsCondition.getTitle().trim().equals(term.getTitle().trim())){
						table.addCell(pdfUtility.getCell(term.getTitle(), font10bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);;
						table.addCell(pdfUtility.getCell(term.getMsg(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);	
						table.addCell(pdfUtility.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);	
					}
									
				}
			}
			
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
			
			
			if (preprintStatus.equals("yes")) {
				try {
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
				} catch (Exception e) {
				}
					
			}
			
			
			if (preprintStatus.equals("no")) {
				logger.log(Level.SEVERE,"in no");
				if (comp.getUploadHeader() != null) {
					logger.log(Level.SEVERE,"in header");
					createCompanyNameAsHeader(document, comp);
				}
				if (comp.getUploadFooter() != null) {
					logger.log(Level.SEVERE,"in footer");
					createCompanyNameAsFooter(document, comp);
				}
				
				if(comp.getUploadHeader()!=null && comp.getUploadFooter() != null){
					try {
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
						document.add(blank);
					} catch (Exception e) {
					}
				}
			}
			
			
			try {
				document.add(table);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else
			return;
		
		
	
		
	}

	private void createFooterTaxPartSalesOrder(SalesOrder salesOrder) {
		SalesOrder qp=salesOrder;
		float[] columnMoreLeftWidths = { 2f, 1f };
		PdfPTable pdfPTaxTable = new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

//		float[] column3widths = { 2f, 2f, 6f };
//		PdfPTable leftTable = new PdfPTable(3);
//		leftTable.setWidthPercentage(100);
//		try {
//			leftTable.setWidths(column3widths);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// heading
//
//		Phrase day = new Phrase("Day", font8bold);
//		PdfPCell dayCell = new PdfPCell(day);
//		dayCell.setBorder(0);
//
//		Phrase percent = new Phrase("Percent", font8bold);
//		PdfPCell percentCell = new PdfPCell(percent);
//		percentCell.setBorder(0);
//
//		Phrase comment = new Phrase("Comment", font8bold);
//		PdfPCell commentCell = new PdfPCell(comment);
//		commentCell.setBorder(0);
//
//		leftTable.addCell(dayCell);
//		leftTable.addCell(percentCell);
//		leftTable.addCell(commentCell);
//
//		// Values
//		for (int i = 0; i < qp.getPaymentTermsList().size(); i++) {
//			Phrase dayValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermDays()
//					+ "", font8);
//			PdfPCell dayValueCell = new PdfPCell(dayValue);
//			dayValueCell.setBorder(0);
//			leftTable.addCell(dayValueCell);
//
//			Phrase percentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermPercent()
//					+ "", font8);
//			PdfPCell percentValueCell = new PdfPCell(percentValue);
//			percentValueCell.setBorder(0);
//			leftTable.addCell(percentValueCell);
//
//			Phrase commentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermComment(), font8);
//			PdfPCell commentValueCell = new PdfPCell(commentValue);
//			commentValueCell.setBorder(0);
//			leftTable.addCell(commentValueCell);
//		}
//
//		// try {
//		// document.add(leftTable);
//		// } catch (DocumentException e1) {
//		// e1.printStackTrace();
//		// }

		/** date 10/11/2017 added by komal for vercetile changes **/
				Phrase blankval = new Phrase(" ", font8);
				Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
				Phrase blank = new Phrase("", font8);
				PdfPCell bcell = new PdfPCell(blank);

				Phrase payphrase = new Phrase("Payment Terms", font8bold);
				payphrase.add(Chunk.NEWLINE);
				PdfPTable table = new PdfPTable(3);
				table.setWidthPercentage(100);

				PdfPCell tablecell1 = new PdfPCell();
				System.out.println("b4 table 11" + this.payTermsLis.size());
				if (this.payTermsLis.size() > 6) {
					System.out.println("af table 11" + this.payTermsLis.size());
					tablecell1 = new PdfPCell(table);
					// tablecell1.setBorder(0);
				} else {
					System.out.println(" table 11" + this.payTermsLis.size());
					tablecell1 = new PdfPCell(table);
					tablecell1.setBorder(0);
				}

				Phrase paytermdays = new Phrase("Days", font6bold);
				Phrase paytermpercent = new Phrase("Percent", font6bold);
				Phrase paytermcomment = new Phrase("Comment", font6bold);

				PdfPCell headingpayterms = new PdfPCell(payphrase);
				headingpayterms.setBorder(0);
				headingpayterms.setColspan(3);
				
				PdfPCell celldays = new PdfPCell(paytermdays);
				celldays.setBorder(0);
				celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
				PdfPCell cellpercent = new PdfPCell(paytermpercent);
				cellpercent.setBorder(0);
				cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
				PdfPCell cellcomment = new PdfPCell(paytermcomment);
				cellcomment.setBorder(0);
				cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);

				table.addCell(celldays);
				table.addCell(cellpercent);
				table.addCell(cellcomment);

				if (this.payTermsLis.size() <= BreakPoint) {
					int size = BreakPoint - this.payTermsLis.size();
					blankLines = size * (60 / 6);
					System.out.println("blankLines size =" + blankLines);
					System.out.println("blankLines size =" + blankLines);
				} else {
					blankLines = 0;
				}
				table.setSpacingAfter(blankLines);

				// PdfPTable termsTable=new PdfPTable(1);
				// termsTable.setWidthPercentage(100);
				// termsTable.setSpacingAfter(80f);
				// termsTable.addCell(termstitlecell);
				// if(qp instanceof SalesQuotation){
				// termsTable.addCell(validitydatecell);
				// }
				//
				// termsTable.addCell(desccell);
				// termsTable.addCell(headingpayterms);
				// termsTable.addCell(pdfdayscell);

				for (int i = 0; i < this.payTermsLis.size(); i++) {

					Phrase chunk = null;
					// Phrase chunk=new
					// Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
					// PdfPCell pdfdayscell = new PdfPCell(chunk);
					// pdfdayscell.setBorder(0);
					if (payTermsLis.get(i).getPayTermDays() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfdayscell = new PdfPCell(chunk);
					pdfdayscell.setBorder(0);
					pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (payTermsLis.get(i).getPayTermPercent() != 0) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfpercentcell = new PdfPCell(chunk);
					pdfpercentcell.setBorder(0);
					pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					if (payTermsLis.get(i).getPayTermComment() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
								.trim(), font6);
					} else {
						chunk = new Phrase(" ", font6);
					}
					PdfPCell pdfcommentcell = new PdfPCell(chunk);
					pdfcommentcell.setBorder(0);
					pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table.addCell(pdfdayscell);
					table.addCell(pdfpercentcell);
					table.addCell(pdfcommentcell);

					count = i;
					// /
					if (count == 5 || count == BreakPoint) {

						flag1 = BreakPoint;
						break;
					}
				}

				// /2nd table for pay terms start
				System.out.println(this.payTermsLis.size() + "  out");

				// if(this.payTermsLis.size()>4){

				PdfPTable table1 = new PdfPTable(3);
				table1.setWidthPercentage(100);

				PdfPCell table1cell = new PdfPCell();

				if (this.payTermsLis.size() > 6) {
					table1cell = new PdfPCell(table1);
					// table1cell.setBorder(0);
				} else {
					table1cell = new PdfPCell(blankval);
					table1cell.setBorder(0);
				}

				Phrase paytermdays1 = new Phrase("Days", font6bold);
				Phrase paytermpercent1 = new Phrase("Percent", font6bold);
				Phrase paytermcomment1 = new Phrase("Comment", font6bold);

				PdfPCell celldays1;
				PdfPCell cellpercent1;
				PdfPCell cellcomment1;

				System.out.println(this.payTermsLis.size() + " ...........b4 if");
				if (this.payTermsLis.size() > 5) {
					System.out.println(this.payTermsLis.size() + " ...........af if");
					celldays1 = new PdfPCell(paytermdays1);
					celldays1.setBorder(0);
					celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					celldays1 = new PdfPCell(blankval);
					celldays1.setBorder(0);

				}

				if (this.payTermsLis.size() > 5) {

					cellpercent1 = new PdfPCell(paytermpercent1);
					cellpercent1.setBorder(0);
					cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					cellpercent1 = new PdfPCell(blankval);
					cellpercent1.setBorder(0);

				}

				if (this.payTermsLis.size() > 5) {
					cellcomment1 = new PdfPCell(paytermcomment1);
					cellcomment1.setBorder(0);
					cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					cellcomment1 = new PdfPCell(blankval);
					cellcomment1.setBorder(0);
				}
				table1.addCell(celldays1);
				table1.addCell(cellpercent1);
				table1.addCell(cellcomment1);

				for (int i = 6; i < this.payTermsLis.size(); i++) {
					System.out.println(this.payTermsLis.size() + "  in for");

					Phrase chunk = null;
					if (payTermsLis.get(i).getPayTermDays() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfdayscell = new PdfPCell(chunk);
					pdfdayscell.setBorder(0);
					pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (payTermsLis.get(i).getPayTermPercent() != 0) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfpercentcell = new PdfPCell(chunk);
					pdfpercentcell.setBorder(0);
					pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					if (payTermsLis.get(i).getPayTermComment() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
								.trim(), font6);
					} else {
						chunk = new Phrase(" ", font6);
					}
					PdfPCell pdfcommentcell = new PdfPCell(chunk);
					pdfcommentcell.setBorder(0);
					pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table1.addCell(pdfdayscell);
					table1.addCell(pdfpercentcell);
					table1.addCell(pdfcommentcell);

					if (i == 11) {

						break;
					}

				}

				// /2nd table for pay terms end

				PdfPTable termstable1 = new PdfPTable(2);
				termstable1.setWidthPercentage(100);
				termstable1.addCell(headingpayterms);
				termstable1.addCell(tablecell1);

				termstable1.addCell(table1cell);
				
		
				
				
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		/**
		 * Date 23/3/2018
		 * By jayshree
		 * Des.add the other charges
		 */
		createOtherCharges();
		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(new float[]{0.9f, 1f, 0.4f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				System.out.println("inside sale order");
				Phrase chargename=new Phrase("Charge Name",font6bold);
				PdfPCell chargenameCell=new PdfPCell(chargename);
				chargenameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(chargenameCell);
				
				Phrase taxname=new Phrase("Tax",font6bold);
				PdfPCell taxnameCell=new PdfPCell(taxname);
				taxnameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(taxnameCell);
				
				Phrase amtph=new Phrase("Amt",font6bold);
				PdfPCell amtCell=new PdfPCell(amtph);
				amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(amtCell);
				
				for (int i = 0; i < qp.getOtherCharges().size(); i++) {
				
					System.out.println("inside so for....");
					Phrase chargenameval=new Phrase(qp.getOtherCharges().get(i).getOtherChargeName(),font6);
					PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
					chargenamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(chargenamevalCell);
					
					String taxNames = " ";
					if (qp.getOtherCharges().get(i).getTax1()
							.getPercentage() != 0
							&& qp.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
						taxNames = qp.getOtherCharges().get(i).getTax1()
								.getTaxConfigName()
								+ "/"
								+ qp.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
					} else {
						if (qp.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax1()
									.getTaxConfigName();
						} else if (qp.getOtherCharges().get(i).getTax2()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax2()
									.getTaxConfigName();
						} else {
							taxNames = " ";
						}
					}
					
					Phrase taxnameval=new Phrase(taxNames,font6);
					PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
					taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(taxnamevalCell);
					
					Phrase amtvalph=new Phrase(qp.getOtherCharges().get(i).getAmount()+"",font6);
					PdfPCell amtvalCell=new PdfPCell(amtvalph);
					amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(amtvalCell);
					
					}
				
			
		PdfPCell upperRightCell = new PdfPCell(otherCharges);
		upperRightCell.setBorder(0);
//		upperRightCell.addElement(otherCharges);
		
//		if(qp.getOtherCharges().size()!=0){
//		System.out.println("soEntity.getOtherCharges().size()"+qp.getOtherCharges().size());
//		rightTable.addCell(upperRightCell);
//		}
			
		//End By Jayshree
		
		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.3f };
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.addElement(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax", font6bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		// totalAmount
		Phrase amtB4TaxValphrase = new Phrase("", font6bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < qp.getProductTaxes().size(); i++) {
			if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			}
		}
		System.out.println("Check Point 1");
		Phrase CGSTphrase = new Phrase("CGST", font6bold);
		PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
		CGSTphraseCell.setBorder(0);
		// CGSTphraseCell.addElement(CGSTphrase);

		Phrase CGSTValphrase = new Phrase(df.format(cgstTotalVal) + "", font7);
		// Paragraph CGSTValphrasePara=new Paragraph();
		// CGSTValphrasePara.add(CGSTValphrase);
		// CGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
		CGSTValphraseCell.setBorder(0);
		CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// CGSTValphraseCell.addElement(CGSTValphrasePara);

		Phrase SGSTphrase = new Phrase("SGST", font6bold);
		PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
		SGSTphraseCell.setBorder(0);
		// SGSTphraseCell.addElement(SGSTphrase);

		Phrase SGSTValphrase = new Phrase(df.format(sgstTotalVal) + "", font7);
		// Paragraph SGSTValphrasePara=new Paragraph();
		// SGSTValphrasePara.add(SGSTValphrase);
		// SGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
		SGSTValphraseCell.setBorder(0);
		// SGSTValphraseCell.addElement(SGSTValphrasePara);
		SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase IGSTphrase = new Phrase("IGST", font6bold);
		PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
		IGSTphraseCell.setBorder(0);
		// IGSTphraseCell.addElement(IGSTphrase);

		Phrase IGSTValphrase = new Phrase(df.format(igstTotalVal) + "", font7);
		// Paragraph IGSTValphrasePara=new Paragraph();
		// IGSTValphrasePara.add(IGSTValphrase);
		// IGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
		IGSTValphraseCell.setBorder(0);
		IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// IGSTValphraseCell.addElement(IGSTValphrasePara);

		Phrase GSTphrase = new Phrase("Total GST", font6bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		// GSTphraseCell.addElement(GSTphrase);

		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
				font6bold);
		// Paragraph GSTValphrasePara=new Paragraph();
		// GSTValphrasePara.add(GSTValphrase);
		// GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// GSTValphraseCell.addElement(GSTValphrasePara);

		rightInnerTable.addCell(CGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(CGSTValphraseCell);
		rightInnerTable.addCell(SGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(SGSTValphraseCell);
		rightInnerTable.addCell(IGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(IGSTValphraseCell);
		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
//		innerRightCell.addElement(rightInnerTable);

		
		rightTable.addCell(innerRightCell);

		PdfPCell rightCell = new PdfPCell(rightTable);//date 23/3/2018 comment  by jayshree 
		// rightCell.setBorder(0);
//		rightCell.addElement(rightTable);

		PdfPCell leftCell = new PdfPCell(termstable1);
		// leftCell.setBorder(0);
//		leftCell.addElement(termstable1);

		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createOtherCharges() {
		// TODO Auto-generated method stub
		
	}

	private void createCompanyAddressDetails() {

		// ************************changes made by rohan for M/s
		// ****************
		// String tosir= null;
		// if(cust.isCompany()==true){
		//
		//
		// tosir="To, M/S";
		// }
		// else{
		// tosir="To,";
		// }
		// //*******************changes ends here
		// ********************sss*************
		//
		//
		// String custName="";
		// if(cust.isCompany()==true&&cust.getCompanyName()!=null){
		// custName=cust.getCompanyName().trim();
		// }
		// else{
		// custName=cust.getFullname().trim();
		// }

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name
		/**
		 * Old COde
		 */

//		if (cust.getCustPrintableName() != null
//				&& !cust.getCustPrintableName().equals("")) {
//			custName = cust.getCustPrintableName().trim();
//		} else {
//
//			if (cust.isCompany() == true) {
//
//				tosir = "To, M/S";
//			} else {
//				tosir = "To,";
//			}
//
//			if (cust.isCompany() == true && cust.getCompanyName() != null) {
//				custName = cust.getCompanyName().trim();
//			} else {
//				custName = cust.getFullname().trim();
//			}
//		}
		
		/**
		 * //Dev.By jayshree 
		 * Date 20/11/2017 
		 * Des.add the salutation to customer name
		 */
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true) {

				tosir = "To, M/S  ";
			} else {
				tosir = "To,";
			}

			
			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			} else if(cust.getSalutation()!=null) 
			{
				custName = cust.getSalutation()+" "+cust.getFullname().trim();
			}
			else
			{
				custName = cust.getFullname().trim();
			}
		}
		//end By Jayshree

		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname.add(custName);
		} else {
			fullname.add(tosir + "   " + custName);
		}
		fullname.setFont(font10bold);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		String addresslin1 = " ";
		if (!cust.getAdress().getAddrLine1().equals("")) {
			addresslin1 = cust.getAdress().getAddrLine1().trim() + " , ";
		}

		String addressline2 = " ";

		if (!cust.getAdress().getAddrLine2().equals("")) {
			addressline2 = cust.getAdress().getAddrLine2().trim() + " , ";
		}

		String landmark = " ";
		if (!cust.getAdress().getLandmark().equals("")) {
			landmark = cust.getAdress().getLandmark() + " , ";
		}

		String locality = " ";
		if (!cust.getAdress().getLocality().equals("")) {
			locality = cust.getAdress().getLocality() + " , ";
		}

		// Phrase addressline1=new
		// Phrase(addresslin1+addressline2+landmark+locality,font9);
		// PdfPCell fromaddrescell1=new PdfPCell();
		// fromaddrescell1.addElement(addressline1);
		// fromaddrescell1.setBorder(0);

		String city = " ";
		if (!cust.getAdress().getCity().equals("")) {
			city = cust.getAdress().getCity() + ",";
		}
		String state = " ";
		if (!cust.getAdress().getState().equals("")) {
			state = cust.getAdress().getState() + "-";
		}
		String pin = " ";
		if (cust.getAdress().getPin() != 0) {
			pin = cust.getAdress().getPin() + ",";
		}
		String country = " ";
		if (cust.getAdress().getCountry() != null) {
			country = cust.getAdress().getCountry();
		}

		Phrase addressline1 = new Phrase(addresslin1 + addressline2 + landmark
				+ locality + city + state + pin + country, font9);
		PdfPCell fromaddrescell1 = new PdfPCell();
		fromaddrescell1.addElement(addressline1);
		fromaddrescell1.setBorder(0);

		// Phrase blank=new Phrase(" ",font9);
		// PdfPCell blankCell=new PdfPCell();
		// fromaddrescell1.addElement(addressline1);
		// fromaddrescell1.setBorder(0);

		Phrase namePhrase = new Phrase("Customer Details", font10bold);
		PdfPCell headcell = new PdfPCell(namePhrase);
		headcell.setBorder(0);
		headcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// *******************rohan added cust details *****************
		Phrase email = new Phrase("Email :" + cust.getEmail(), font9);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cellNo = new Phrase("Cell : " + cust.getCellNumber1(), font9);
		PdfPCell cellNoCell = new PdfPCell(cellNo);
		cellNoCell.setBorder(0);
		cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********************changes ends here *********************

		PdfPTable addtable = new PdfPTable(1);
		addtable.setWidthPercentage(100f);
		addtable.addCell(headcell);
		addtable.addCell(custnamecell);
		addtable.addCell(fromaddrescell1);
		addtable.addCell(emailCell);
		addtable.addCell(cellNoCell);

		PdfPCell tablecell = new PdfPCell();
		tablecell.addElement(addtable);

		PdfPTable parentaddtable = new PdfPTable(1);
		parentaddtable.setWidthPercentage(100f);
		parentaddtable.addCell(tablecell);

		try {
			document.add(parentaddtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createSpcingForHeading() {

		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);

		try {
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createblankline() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createPdfForEmail(Company comp, Customer cust, Sales sq,
			CompanyPayment payMode1, CompanyPayment payMode2) {
		this.comp = comp;
		this.qp = sq;
		this.cust = cust;
		this.products = qp.getItems();
		this.payTermsLis = qp.getPaymentTermsList();
		this.prodTaxes = qp.getProductTaxes();
		this.prodCharges = qp.getProductCharges();
		this.comppayment = payMode1;
		this.comppayment1 = payMode2;
		this.articletype = cust.getArticleTypeDetails();

		if (qp instanceof SalesQuotation) {
			if (qp.getCompanyId() != null) {
				processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", qp.getCompanyId())
						.filter("processName", "SalesQuotation")
						.filter("configStatus", true).first().now();
				if (processConfig != null) {
					for (int k = 0; k < processConfig.getProcessList().size(); k++) {
						if (processConfig.getProcessList().get(k)
								.getProcessType().trim()
								.equalsIgnoreCase("PrintCompanyNameAsLogo")
								&& processConfig.getProcessList().get(k)
										.isStatus() == true) {
							this.CompanyNameLogoflag = true;
						}
					}
				}
			}
		} else {
			if (qp.getCompanyId() != null) {
				processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", qp.getCompanyId())
						.filter("processName", "SalesOrder")
						.filter("configStatus", true).first().now();
				if (processConfig != null) {
					for (int k = 0; k < processConfig.getProcessList().size(); k++) {
						if (processConfig.getProcessList().get(k)
								.getProcessType().trim()
								.equalsIgnoreCase("PrintCompanyNameAsLogo")
								&& processConfig.getProcessList().get(k)
										.isStatus() == true) {
							this.CompanyNameLogoflag = true;
						}
					}
				}
			}
		}

		/************************************ discount Flag *******************************/

		if (qp.getCompanyId() != null) {
			for (int i = 0; i < products.size(); i++) {
				if ((products.get(i).getDiscountAmt() > 0)
						&& (products.get(i).getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (products.get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (products.get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}

			}

		}

		createblankline();
		if (CompanyNameLogoflag == true) {
//			createLogo(document, comp);//Date 23/11/2017 comment by Jayshree to remove logo tab
		} else {
//			createLogo123(document, comp);//Date 23/11/2017 comment by Jayshree to remove logo tab
		}
		createHeadingInfo();

		if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
			createProductDetailWithDiscount();
		} else if (nothing > 0) {
			createProductDetailWithOutDisc();
		} else {
			createProductInfo();
		}

		termsConditionsInfo();
		footerInfo();
	}

	private void createLogo123(Document doc, Company comp) {

		// ********************logo for server ********************
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 745f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,745f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
		//
	}

	private void createLogo(Document doc, Company comp) {

		// ********************logo for server ********************

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(250f);
			image2.setAbsolutePosition(40f, 745f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ***********************for local **************************

		// try
		// {
		// Image image1=Image.getInstance("images/logo1.png");
		//
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(250f);
		// image1.setAbsolutePosition(35f,725f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
		//
	}

	public void createHeadingInfo() {

//		Paragraph p = new Paragraph();
//		Phrase blankspace = null;
//		//Phrase companyName = null;
//		if (CompanyNameLogoflag == true) {
//			blankspace = new Phrase(" ", font12);
//
//			p.add(blankspace);
//		}
//		else{
//			
////             
////			//companyName = new Phrase(""+ comp.getBusinessUnitName().toUpperCase(), font12bold);
//			
//			
//			
//		}
//
//		p.add(Chunk.NEWLINE);
//
//		//p.add(companyName);
//
//		PdfPCell companyHeadingCell = new PdfPCell();
//		companyHeadingCell.setBorder(0);
//		companyHeadingCell.addElement(p);
//		if (CompanyNameLogoflag == true) {
//			companyHeadingCell.addElement(blankspace);
//			companyHeadingCell.addElement(blankspace);
//		} else {
//			companyHeadingCell.addElement(companyName);
//		}
		
		Phrase companyName = null;
	    
	    if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
	    	companyName = new Phrase( branchDt.getCorrespondenceName(), font12bold);
			logger.log(Level.SEVERE,"correspondance name "+companyName);
			
		}else{
			if (comp != null) {
			      companyName = new Phrase(comp.getBusinessUnitName().trim(), font12bold);
			      logger.log(Level.SEVERE,"company NAme "+companyName);
			    }	
		}

	    PdfPCell companynamecell = new PdfPCell(companyName);
	    
	    companynamecell.setBorder(0);
	    companynamecell.setHorizontalAlignment(0);
	    
		
//		String companyname ="";
//		if(branchDt!=null&&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
//			companyname=branchDt.getCorrespondenceName();
//		}else{
//		 companyname = comp.getBusinessUnitName().trim().toUpperCase();
//		}
//
//		Paragraph companynamepara = new Paragraph();
//		companynamepara.add(companyname);
//		companynamepara.setFont(font12bold);
//		companynamepara.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell companynamecell = new PdfPCell();
//		companynamecell.addElement(companynamepara);
//		companynamecell.setBorder(0);


		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),
				font10);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font10);
		} else {
			adressline2 = new Phrase("", font10);
		}

		Phrase landmark = null;
		Phrase locality = null;
		Phrase ccity = null;
		if (comp.getAddress().getLandmark() != null) {
			landmark = new Phrase(comp.getAddress().getLandmark(), font10);
		} else {
			landmark = new Phrase("", font10);
		}

		if (comp.getAddress().getLocality() != null) {
			locality = new Phrase(comp.getAddress().getLocality(), font10);
		} else {
			locality = new Phrase("", font10);
		}

		ccity = new Phrase(comp.getAddress().getCity() + " - "
				+ comp.getAddress().getPin(), font10);

		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);

		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);

		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);

		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		PdfPCell ciatycell = new PdfPCell();
		ciatycell.addElement(ccity);
		ciatycell.setBorder(0);

		// localitycell.addElement(Chunk.NEWLINE);

		String contactinfo = "Cell: " + comp.getContact().get(0).getCellNo1();
		if (comp.getContact().get(0).getCellNo2() != 0)
			contactinfo = contactinfo + ","
					+ comp.getContact().get(0).getCellNo2();
		if (comp.getContact().get(0).getLandline() != 0) {
			contactinfo = contactinfo + "     " + " Tel: "
					+ comp.getContact().get(0).getLandline();
		}

		Phrase contactnos = new Phrase(contactinfo, font9);
		
		String email="";
		//if(branchDt!=null){
			if(branchDt!=null && branchDt.getEmail()!=null&&!branchDt.getEmail().equals("")){
				email=branchDt.getEmail();
			}else{
				email=comp.getContact().get(0).getEmail();
			}
		
			Phrase emailPhrase = new Phrase("Email : " +email  ,font9);
			//PdfPCell termscell = new PdfPCell(emailPhrase);
			
			
		
		//Phrase email = new Phrase("Email : "+ comp.getContact().get(0).getEmail(), font9);
		PdfPCell websitecell = null;
		if (!comp.getWebsite().equals("")) {
			Phrase website = new Phrase("Website: " + comp.getWebsite(), font9);
			websitecell = new PdfPCell();
			websitecell.setBorder(0);
			websitecell.addElement(website);
		}

		Phrase email1 = null;
		if (!comp.getEmail().equals("")) {
			if (!comp.getContact().get(0).getEmail()
					.equalsIgnoreCase(comp.getPocEmail())) {
				email1 = new Phrase("Email : " + comp.getPocEmail(), font9);
			}
		}

		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);

		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(emailPhrase);

		PdfPCell emailcell1 = null;
		if (!comp.getContact().get(0).getEmail()
				.equalsIgnoreCase(comp.getPocEmail())) {
			emailcell1 = new PdfPCell();
			emailcell1.setBorder(0);
			emailcell1.addElement(email1);
		}

		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companynamecell);
		companytable.addCell(addressline1cell);
		if (!comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!comp.getAddress().getLandmark().equals("")) {
			companytable.addCell(landmarkcell);
		}
		if (!comp.getAddress().getLocality().equals("")) {
			companytable.addCell(localitycell);
		}

		companytable.addCell(ciatycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);

		if (!comp.getEmail().equals("")) {
			if (!comp.getContact().get(0).getEmail()
					.equalsIgnoreCase(comp.getPocEmail())) {
				companytable.addCell(emailcell1);
			}
		}

		if (!comp.getWebsite().equals("")) {
			companytable.addCell(websitecell);
		}
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);

		/**
		 * Customer Info
		 */

		Phrase blank = null;
		// ************************changes made by rohan for M/s
		// ****************
		// String tosir= null;
		// if(cust.isCompany()==true){
		//
		// tosir="To, M/S";
		// }
		// else{
		// tosir="To,";
		// }
		// //*******************changes ends here
		// ********************sss*************
		// String custName="";
		//
		// if(cust.isCompany()==true&&cust.getCompanyName()!=null){
		// custName=cust.getCompanyName().trim();
		// }
		// else{
		// custName=cust.getFullname().trim();
		// }

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true) {

				tosir = "To, M/S";
			} else {
				tosir = "To,";
			}
			
			/**
			 * Dev.By jayshree 
			 * Date 20/11/2017 
			 * Des.add the salutation to customer name
			 */
			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			} else if(cust.getSalutation()!=null)
			{
				custName =cust.getSalutation()+" "+ cust.getFullname().trim();
			}
			else
			{
				custName = cust.getFullname().trim();
			}
			
//			if (cust.isCompany() == true && cust.getCompanyName() != null) {
//			custName = cust.getCompanyName().trim();
//			} else {
//			custName = cust.getFullname().trim();
//			}
		}
//End by Jayshree



		/********** tosir **********/

		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		fullname.setFont(font12bold);
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname.add(custName);
		} else {
			fullname.add(tosir + "   " + custName);
		}

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		Phrase customeradress = new Phrase("                   "
				+ cust.getAdress().getAddrLine1(), font10);
		Phrase customeradress2 = null;
		PdfPCell custaddress2 = new PdfPCell();
		if (cust.getAdress().getAddrLine2() != null) {
			customeradress2 = new Phrase("                   "
					+ cust.getAdress().getAddrLine2(), font10);
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
		} else {
			custaddress2.addElement(blank);
			custaddress2.setBorder(0);
		}

		PdfPCell custaddress1 = new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);

		String custlandmark = "";
		Phrase custlocality = null;

		String custlocality1 = "";
		Phrase custloclity = null;
		PdfPCell custlocalitycell = new PdfPCell();
		if (cust.getAdress().getLandmark() != null) {
			custlandmark = cust.getAdress().getLandmark();
			custlocality = new Phrase("                   " + custlandmark,
					font10);
			custlocalitycell.addElement(custlocality);
			custlocalitycell.setBorder(0);
		} else {
			custlocalitycell.addElement(blank);
			custlocalitycell.setBorder(0);
		}

		PdfPCell custlocalitycell1 = new PdfPCell();
		if (cust.getAdress().getLocality() != null) {
			custlocality1 = cust.getAdress().getLocality();
			custloclity = new Phrase("                   " + custlocality1,
					font10);
			custlocalitycell1.addElement(custloclity);
			custlocalitycell1.setBorder(0);
		} else {
			custlocalitycell1.addElement(blank);
			custlocalitycell1.setBorder(0);
		}

		Phrase cityState = new Phrase("                   "
				+ cust.getAdress().getCity() + " - "
				+ cust.getAdress().getPin(), font10);

		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);

		Phrase custcontact = new Phrase("Cell: " + cust.getCellNumber1() + "",
				font9);
		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);

		Phrase custemail = new Phrase("Email: " + cust.getEmail(), font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);

		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(custaddress1);
		System.out.println(cust.getAdress().getAddrLine2() + "out if ad2");
		if (!cust.getAdress().getAddrLine2().equals("")) {
			System.out.println(cust.getAdress().getAddrLine2() + "in if ad2");
			custtable.addCell(custaddress2);
		}
		if (!cust.getAdress().getLandmark().equals("")) {
			custtable.addCell(custlocalitycell);
		}
		if (!cust.getAdress().getLocality().equals("")) {
			custtable.addCell(custlocalitycell1);
		}

		custtable.addCell(custcitycell);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);

		// if company is true then the name of contact person

		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Contact: " + cust.getFullname(),
					font9);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}

		Phrase typename = null;
		Phrase typevalue = null;

		PdfPTable artictable = new PdfPTable(2);
		artictable.setWidthPercentage(100);
		try {
			artictable.setWidths(columnWidths2);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		Phrase nnn = new Phrase("");
		PdfPCell blankcekk = new PdfPCell(nnn);
		blankcekk.setBorder(0);
		System.out.println("b4   for   ===== articletype=="
				+ this.articletype.size());
		for (int i = 0; i < this.articletype.size(); i++) {

			System.out.println("in    for   ===== articletype=="
					+ this.articletype.size());
			if (qp instanceof SalesOrder) {

				if (articletype.get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& articletype.get(i).getDocumentName()
								.equals("Sales Order")) {

					System.out.println("cust docu name if in == "
							+ articletype.get(i).getDocumentName());

					typename = new Phrase(articletype.get(i)
							.getArticleTypeName(), font8);
					typevalue = new Phrase(articletype.get(i)
							.getArticleTypeValue(), font8);

					PdfPCell tymanecell = new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);

					PdfPCell typevalcell = new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setBorder(0);

					artictable.addCell(tymanecell);
					artictable.addCell(typevalcell);

					artictable.addCell(blankcekk);
					artictable.addCell(blankcekk);
				}
			} else {
				if (articletype.get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& articletype.get(i).getDocumentName()
								.equals("Sales Quotation")) {

					System.out.println("cust docu name if in == "
							+ articletype.get(i).getDocumentName());

					typename = new Phrase(articletype.get(i)
							.getArticleTypeName(), font8);
					typevalue = new Phrase(articletype.get(i)
							.getArticleTypeValue(), font8);

					PdfPCell tymanecell = new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);

					PdfPCell typevalcell = new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setBorder(0);

					artictable.addCell(tymanecell);
					artictable.addCell(typevalcell);
					artictable.addCell(blankcekk);
					artictable.addCell(blankcekk);
				}
			}
		}

		PdfPCell articcell = new PdfPCell(artictable);
		articcell.setBorder(0);

		custtable.addCell(articcell);

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		String title = "";
		if (qp instanceof SalesOrder)
			title = "Sales Order";
		else
			title = "Quotation";

		String countinfo = "";
		if (qp instanceof SalesOrder) {
			SalesOrder salesordentity = (SalesOrder) qp;
			countinfo = " ID: " + salesordentity.getCount() + "";
		} else {
			SalesQuotation salesquotentity = (SalesQuotation) qp;
			countinfo = " ID: " + salesquotentity.getCount() + "";
		}

		String creationdateinfo = "";

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));

		if (qp instanceof SalesOrder) {
			SalesOrder salesordentity = (SalesOrder) qp;
			creationdateinfo = "Date: "
					+ fmt.format(salesordentity.getSalesOrderDate());
		} else {
			SalesQuotation salesquotentity = (SalesQuotation) qp;
			creationdateinfo = "Date: "
					+ fmt.format(salesquotentity.getCreationDate());
		}

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);

		Phrase idphrase = new Phrase(countinfo, font14);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {
			document.add(headparenttable);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createProductInfo() {
		logger.log(Level.SEVERE, "in product methos============1");

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100);
		table.setSpacingAfter(185f);
		logger.log(Level.SEVERE, "in product methos============12");
		Phrase category = new Phrase("SR. NO.", font1);
		Paragraph srno = new Paragraph();
		srno.add(category);

		Phrase product = new Phrase("ITEM DETAILS ", font1);
		Paragraph itemdetail = new Paragraph();
		itemdetail.add(product);

		Phrase qty = new Phrase("QTY.", font1);
		Paragraph quty = new Paragraph();
		quty.add(qty);

		Phrase rate = new Phrase("UNIT", font1);
		Paragraph unit = new Paragraph();
		unit.add(rate);

		Phrase servicetax = new Phrase("RATE", font1);
		Paragraph rate1 = new Paragraph();
		rate1.add(servicetax);

		Phrase percDisc = null;

		if (discPer > 0) {
			percDisc = new Phrase("DISC %", font1);
		} else if (discAmt > 0) {
			percDisc = new Phrase("DISC", font1);
		}

		Paragraph tax = new Paragraph();
		// *************chnges rohan on 22/4/2015********************
		Phrase svat = null;
		Phrase vvat = null;
		Phrase vat = null;
		int cout = 0;
		int flag21 = 0;
		int st = 0;
		int st1 = 0;

		logger.log(Level.SEVERE, "in product methos============13");

		for (int i = 0; i < this.products.size(); i++) {

			int cstFlag = 0;
			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {

					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vvat = new Phrase("VAT / ST %", font1);
				cout = 1;
			} else if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() == 0
					&& cstFlag == 0) {
				vat = new Phrase("ST %", font1);
				st = 1;
			} else if (products.get(i).getServiceTax().getPercentage() == 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vat = new Phrase("VAT %", font1);
				st1 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {
				svat = new Phrase("CST / ST %", font1);
				flag21 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() == 0) {
				vat = new Phrase("CST %", font1);

			} else {
				vat = new Phrase("TAX %", font1);
			}

		}

		Phrase stvat = null;

		logger.log(Level.SEVERE, "in product methos============14");

		if (cout > 0) {
			tax.add(vvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else if (st == 1 && st1 == 1) {

			stvat = new Phrase("VAT / ST %", font1);
			tax.add(stvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		else if (flag21 > 0) {
			tax.add(svat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else {
			tax.add(vat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		// **************************************changes
		// ends....................

		Phrase total = new Phrase("AMOUNT", font1);
		Paragraph amount = new Paragraph();
		amount.add(total);
		amount.setAlignment(Element.ALIGN_CENTER);

		logger.log(Level.SEVERE, "in product methos============15");
		PdfPCell cellcategory = new PdfPCell(srno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproduct = new PdfPCell(itemdetail);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellqty = new PdfPCell(quty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(unit);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rate1);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellvat = new PdfPCell(percDisc);
		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(tax);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(amount);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellcategory);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellrate);
		table.addCell(cellservicetax);
		table.addCell(cellvat);
		table.addCell(cellperdisc);
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		if (this.products.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size();
			blankLines = size * (140 / 14);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 5f;
		}
		table.setSpacingAfter(blankLines);

		Phrase rephrse = null;

		int flagr = 0;
		int flagr1 = 0;
		for (int i = 0; i < this.products.size(); i++) {
			logger.log(Level.SEVERE, "in product methos============16");
			Phrase chunk = new Phrase((i + 1) + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase chunk1 = new Phrase(products.get(i).getUnitOfMeasurement(),
					font8);
			PdfPCell pdfmofcell = new PdfPCell(chunk1);
			pdfmofcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedRate = products.get(i).getPrice() - taxVal;
			chunk = new Phrase(df.format(calculatedRate), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			if (products.get(i).getPercentageDiscount() != 0)
				chunk = new Phrase(
						products.get(i).getPercentageDiscount() + "", font8);
			else
				chunk = new Phrase("0" + "", font8);
			PdfPCell pdfperdiscount = new PdfPCell(chunk);
			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			logger.log(Level.SEVERE, "in product methos============17");

			PdfPCell pdfservice1 = null;
			// ******************************
			double cstval = 0;
			int cstFlag = 0;

			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {
					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesq.getCstpercent();
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesorder.getCstpercent();
					}
				}
			}

			System.out.println("one");

			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)
					&& (cstFlag == 0)) {

				System.out.println("two");
				flagr = 1;

				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null)) {

					if (cout > 0) {
						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {

						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					}

				} else {

					chunk = new Phrase("00.00" + "", font8);
				}
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				logger.log(Level.SEVERE, "in product methos============18");
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0 && cstFlag == 0)) {

				System.out.println("three");
				if (products.get(i).getServiceTax() != null) {
					if (flag21 == 1 || cout == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else {
						chunk = new Phrase(df.format(products.get(i)
								.getServiceTax().getPercentage()), font8);
					}

				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& cstFlag == 1) {
				System.out.println("four");

				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				flagr = 1;

			} else if (cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {

				System.out.println("five");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {

				System.out.println("six");
				flagr = 1;
				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0) && (products
					.get(i).getVatTax().getPercentage() > 0))) {

				System.out.println("seven");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				logger.log(Level.SEVERE, "in product methos============19");
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& (cstFlag == 0)) {
				System.out.println("eight");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				} else if (st == 1 && st1 == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				}

				else {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage()), font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				System.out.println("service tax flagr:::::::::::::   " + flagr);

			} else {
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase("0.00 / 0.00", font8);
				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
			}

			// **************************************

			double totalVal = (products.get(i).getPrice() - taxVal)
					* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}
			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table.addCell(pdfcategcell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfmofcell);
			table.addCell(pdfspricecell);
			table.addCell(pdfperdiscount);

			pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfservice1);
			logger.log(Level.SEVERE, "in product methos============10");
			table.addCell(pdftotalproduct);
			try {
				table.setWidths(columnWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			count = i;
			// /
			if (count == this.products.size() || count == firstBreakPoint) {
				rephrse = new Phrase(
						"Refer Annexure 1 for additional products ", font8);
				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();

			}
		}
		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.addElement(rephrse);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void termsConditionsInfo() {
		Phrase blankval = new Phrase(" ", font8);

		String titleterms = "";
		Phrase validDate = new Phrase("", font8);
		PdfPCell validitydatecell = new PdfPCell();
		System.out.println("salequotaion out");
		if (qp instanceof SalesQuotation) {
			System.out.println("salequotaion in");

			validDate = new Phrase("", font8);

			SalesQuotation salesquot = (SalesQuotation) qp;
			if (salesquot.getValidUntill() != null) {
				titleterms = "Terms And Conditions";
				Phrase validityDate = new Phrase("Valid Until: "
						+ fmt.format(salesquot.getValidUntill()) + "",
						font10bold);
				validitydatecell.addElement(validityDate);
				validitydatecell.setBorder(0);
			} else {
				validitydatecell.addElement(validDate);
				validitydatecell.setBorder(0);
			}

		}

		// //
		Phrase termsphrase = new Phrase(titleterms, font10boldul);
		// termsphrase.add(Chunk.NEWLINE);
		// termsphrase.add(Chunk.NEWLINE);
		PdfPCell termstitlecell = new PdfPCell();
		termstitlecell.addElement(termsphrase);
		termstitlecell.setBorder(0);

		// //

		// String descHeading="Description :";
		// String descinfo="";
		// Phrase descHead = null;
		// if(qp instanceof SalesQuotation){
		// SalesQuotation sq=(SalesQuotation)qp;
		// if(!sq.getDescription().equals("")){
		// descinfo=sq.getDescription();
		// descHead = new Phrase(descHeading,font8bold);
		// }
		// }
		// if(qp instanceof SalesOrder){
		// SalesOrder so=(SalesOrder)qp;
		// if(!so.getDescription().equals("")){
		// descinfo=so.getDescription();
		// descHead = new Phrase(descHeading,font8bold);
		// }
		// }
		//
		// Phrase descphrase=new Phrase(descinfo,font8);
		// PdfPCell desccell=new PdfPCell();
		// // desccell.addElement(descHead);
		// desccell.addElement(descphrase);
		// desccell.setBorder(0);
		// // desccell.addElement(Chunk.NEWLINE);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase blank = new Phrase("", font8);
		PdfPCell bcell = new PdfPCell(blank);

		Phrase payphrase = new Phrase("Payment Terms", font10bold);
		payphrase.add(Chunk.NEWLINE);
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);

		PdfPCell tablecell1 = new PdfPCell();
		System.out.println("b4 table 11" + this.payTermsLis.size());
		if (this.payTermsLis.size() > 6) {
			System.out.println("af table 11" + this.payTermsLis.size());
			tablecell1 = new PdfPCell(table);
			// tablecell1.setBorder(0);
		} else {
			System.out.println(" table 11" + this.payTermsLis.size());
			tablecell1 = new PdfPCell(table);
			tablecell1.setBorder(0);
		}

		Phrase paytermdays = new Phrase("Days", font1);
		Phrase paytermpercent = new Phrase("Percent", font1);
		Phrase paytermcomment = new Phrase("Comment", font1);

		PdfPCell headingpayterms = new PdfPCell(payphrase);
		headingpayterms.setBorder(0);
		headingpayterms.setColspan(3);
		PdfPCell celldays = new PdfPCell(paytermdays);
		celldays.setBorder(0);
		celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellpercent = new PdfPCell(paytermpercent);
		cellpercent.setBorder(0);
		cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcomment = new PdfPCell(paytermcomment);
		cellcomment.setBorder(0);
		cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(celldays);
		table.addCell(cellpercent);
		table.addCell(cellcomment);

		if (this.payTermsLis.size() <= BreakPoint) {
			int size = BreakPoint - this.payTermsLis.size();
			blankLines = size * (60 / 6);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 0;
		}
		table.setSpacingAfter(blankLines);

		// PdfPTable termsTable=new PdfPTable(1);
		// termsTable.setWidthPercentage(100);
		// termsTable.setSpacingAfter(80f);
		// termsTable.addCell(termstitlecell);
		// if(qp instanceof SalesQuotation){
		// termsTable.addCell(validitydatecell);
		// }
		//
		// termsTable.addCell(desccell);
		// termsTable.addCell(headingpayterms);
		// termsTable.addCell(pdfdayscell);

		for (int i = 0; i < this.payTermsLis.size(); i++) {

			Phrase chunk = null;
			// Phrase chunk=new
			// Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
			// PdfPCell pdfdayscell = new PdfPCell(chunk);
			// pdfdayscell.setBorder(0);
			if (payTermsLis.get(i).getPayTermDays() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (payTermsLis.get(i).getPayTermPercent() != 0) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (payTermsLis.get(i).getPayTermComment() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
						.trim(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);

			count = i;
			// /
			if (count == 5 || count == BreakPoint) {

				flag1 = BreakPoint;
				break;
			}
		}

		// /2nd table for pay terms start
		System.out.println(this.payTermsLis.size() + "  out");

		// if(this.payTermsLis.size()>4){

		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);

		PdfPCell table1cell = new PdfPCell();

		if (this.payTermsLis.size() > 6) {
			table1cell = new PdfPCell(table1);
			// table1cell.setBorder(0);
		} else {
			table1cell = new PdfPCell(blankval);
			table1cell.setBorder(0);
		}

		Phrase paytermdays1 = new Phrase("Days", font1);
		Phrase paytermpercent1 = new Phrase("Percent", font1);
		Phrase paytermcomment1 = new Phrase("Comment", font1);

		PdfPCell celldays1;
		PdfPCell cellpercent1;
		PdfPCell cellcomment1;

		System.out.println(this.payTermsLis.size() + " ...........b4 if");
		if (this.payTermsLis.size() > 5) {
			System.out.println(this.payTermsLis.size() + " ...........af if");
			celldays1 = new PdfPCell(paytermdays1);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			celldays1 = new PdfPCell(blankval);
			celldays1.setBorder(0);

		}

		if (this.payTermsLis.size() > 5) {

			cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellpercent1 = new PdfPCell(blankval);
			cellpercent1.setBorder(0);

		}

		if (this.payTermsLis.size() > 5) {
			cellcomment1 = new PdfPCell(paytermcomment1);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellcomment1 = new PdfPCell(blankval);
			cellcomment1.setBorder(0);
		}
		table1.addCell(celldays1);
		table1.addCell(cellpercent1);
		table1.addCell(cellcomment1);

		for (int i = 6; i < this.payTermsLis.size(); i++) {
			System.out.println(this.payTermsLis.size() + "  in for");

			Phrase chunk = null;
			if (payTermsLis.get(i).getPayTermDays() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (payTermsLis.get(i).getPayTermPercent() != 0) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (payTermsLis.get(i).getPayTermComment() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
						.trim(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table1.addCell(pdfdayscell);
			table1.addCell(pdfpercentcell);
			table1.addCell(pdfcommentcell);

			if (i == 11) {

				break;
			}

		}

		// /2nd table for pay terms end

		PdfPTable termstable1 = new PdfPTable(2);
		termstable1.setWidthPercentage(100);
		termstable1.addCell(headingpayterms);
		termstable1.addCell(tablecell1);

		termstable1.addCell(table1cell);
		// }

		PdfPCell termcell = new PdfPCell();
		termcell.addElement(termstable1);
		termcell.setBorder(0);

		PdfPTable termsTable = new PdfPTable(1);
		termsTable.setWidthPercentage(100);
		// termsTable.setSpacingAfter(55f);

		// termsTable.addCell(table);
		// termsTable.addCell(table1);
		// SalesQuotation salesquot=(SalesQuotation) qp;
		//
		// if(qp instanceof SalesOrder){
		// SalesOrder so=(SalesOrder)qp;
		// if(!so.getDescription().equals("")){
		// termsTable.addCell(termstitlecell);
		// }
		// }else
		// if((salesquot.getValidUntill()!=null)||(!salesquot.getDescription().equals(""))){
		// termsTable.addCell(termstitlecell);
		// }

		if (qp instanceof SalesQuotation) {
			termsTable.addCell(validitydatecell);
		} else {
			termsTable.addCell(bcell);
		}

		termsTable.addCell(termcell);
		// termsTable.addCell(desccell);

		PdfPCell tablecell = new PdfPCell();
		tablecell.addElement(termsTable);
		tablecell.setBorder(0);

		PdfPTable maintable = new PdfPTable(1);
		maintable.setWidthPercentage(100);
		maintable.addCell(tablecell);
		// maintable.addCell(amonttable);

		PdfPTable chargetaxtable = new PdfPTable(1);
		chargetaxtable.setWidthPercentage(100);
		// chargetaxtable.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// chargetaxtable.setSpacingAfter(100f);

		PdfPTable chargetaxtable1 = new PdfPTable(2);
		chargetaxtable1.setWidthPercentage(100);
		// chargetaxtable1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// chargetaxtable1.setSpacingAfter(100f);

		Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
		PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
		totalAmtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totalAmtCell.setBorder(0);
		double totalAmt = 0;
		if (qp instanceof SalesQuotation) {
			totalAmt = ((SalesQuotation) qp).getTotalAmount();
		}
		if (qp instanceof SalesOrder) {
			totalAmt = ((SalesOrder) qp).getTotalAmount();
		}

		Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
		PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
		realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		realtotalAmtCell.setBorder(0);
		chargetaxtable1.addCell(totalAmtCell);
		chargetaxtable1.addCell(realtotalAmtCell);

		int first = 7;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();

		List<String> myList1 = new ArrayList<>();
		List<String> myList2 = new ArrayList<>();
		List<String> myList3 = new ArrayList<>();
		List<String> myList4 = new ArrayList<>();

		if (cnt > first) {

			System.out.println("counttttttttttttin==" + cnt);

			for (int i = 0; i < this.prodTaxes.size(); i++) {

				if (prodTaxes.get(i).getChargePercent() != 0) {

					if (prodTaxes.get(i).getChargeName().equals("VAT")
							|| prodTaxes.get(i).getChargeName().equals("CST")) {

						System.out.println("1st loop"
								+ prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent());

						String str = prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent();
						double taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;

						myList1.add(str);
						myList2.add(df.format(taxAmt1));

						System.out.println("Size of mylist1 is () === "
								+ myList1.size());
					}

					if (prodTaxes.get(i).getChargeName().equals("Service Tax")) {

						System.out.println("2nd loop == "
								+ prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent());

						String str = prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent();
						double taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						String ss = " ";
						myList3.add(str);
						myList4.add(df.format(taxAmt1));
						myList4.add(ss);
						System.out.println("Size of mylist2 is () === "
								+ myList2.size());
					}
				}
			}

			PdfPCell pdfservicecell = null;

			PdfPTable other1table = new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for (int j = 0; j < myList1.size(); j++) {
				chunk = new Phrase(myList1.get(j), font8);
				pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);

			}

			PdfPCell pdfservicecell1 = null;

			PdfPTable other2table = new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for (int j = 0; j < myList2.size(); j++) {
				chunk = new Phrase(myList2.get(j), font8);
				pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);

			}

			PdfPCell chargescell = null;
			for (int j = 0; j < myList3.size(); j++) {
				chunk = new Phrase(myList3.get(j), font8);
				chargescell = new PdfPCell(chunk);
				chargescell.setBorder(0);
				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(chargescell);

			}

			PdfPCell chargescell1 = null;
			for (int j = 0; j < myList4.size(); j++) {
				chunk = new Phrase(myList4.get(j), font8);
				chargescell1 = new PdfPCell(chunk);
				chargescell1.setBorder(0);
				chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(chargescell1);

			}

			PdfPCell othercell = new PdfPCell();
			othercell.addElement(other1table);
			othercell.setBorder(0);
			chargetaxtable1.addCell(othercell);

			PdfPCell othercell1 = new PdfPCell();
			othercell1.addElement(other2table);
			othercell1.setBorder(0);
			chargetaxtable1.addCell(othercell1);

			PdfPCell chargecell = null;
			PdfPCell chargeamtcell = null;
			PdfPCell otherchargecell = null;

			for (int i = 0; i < this.prodCharges.size(); i++) {
				Phrase chunk = null;
				Phrase chunk1 = new Phrase("", font8);
				double chargeAmt = 0;
				PdfPCell pdfchargeamtcell = null;
				PdfPCell pdfchargecell = null;
				if (prodCharges.get(i).getChargePercent() != 0) {
					System.out.println(prodCharges.get(i).getChargeName()
							+ ".......charge name" + "@"
							+ prodCharges.get(i).getChargePercent());
					chunk = new Phrase(prodCharges.get(i).getChargeName()
							+ " @ " + prodCharges.get(i).getChargePercent(),
							font1);
					pdfchargecell = new PdfPCell(chunk);
					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargePercent()
							* prodCharges.get(i).getAssessableAmount() / 100;
					chunk = new Phrase(df.format(chargeAmt), font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
				} else {
					pdfchargecell = new PdfPCell(chunk1);
					pdfchargeamtcell = new PdfPCell(chunk1);
				}

				// /start

				PdfPCell pdfchargeamtcell1 = null;
				PdfPCell pdfchargecell1 = null;

				if (prodCharges.get(i).getChargeAbsValue() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
							font1);
					pdfchargecell1 = new PdfPCell(chunk);
					chargeAmt = prodCharges.get(i).getChargeAbsValue();
					chunk = new Phrase(chargeAmt + "", font8);
					pdfchargeamtcell1 = new PdfPCell(chunk);
					pdfchargeamtcell1
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell1.setBorder(0);
				} else {
					pdfchargecell1 = new PdfPCell(chunk1);
					pdfchargeamtcell1 = new PdfPCell(chunk1);
				}
				pdfchargecell.setBorder(0);
				pdfchargeamtcell.setBorder(0);
				pdfchargecell1.setBorder(0);
				pdfchargeamtcell1.setBorder(0);

				// chargetaxtable1.addCell(pdfchargecell1);
				// chargetaxtable1.addCell(pdfchargeamtcell1);
				// chargetaxtable1.addCell(pdfchargecell);
				// chargetaxtable1.addCell(pdfchargeamtcell);

				// /end

				total = total + chargeAmt;

				if (total != 0) {
					chunk = new Phrase("Other charges total", font1);
					chargecell = new PdfPCell(chunk);
					chargecell.setBorder(0);
					chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

					chunk = new Phrase(df.format(total), font8);
					chargeamtcell = new PdfPCell(chunk);
					chargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					chargeamtcell.setBorder(0);

				}

			}

			Phrase other = new Phrase("Refer other charge details", font1);
			otherchargecell = new PdfPCell(other);
			otherchargecell.setBorder(0);
			otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase other1 = new Phrase(" ", font1);
			PdfPCell otherchargecell1 = new PdfPCell(other1);
			otherchargecell1.setBorder(0);
			otherchargecell1.setHorizontalAlignment(Element.ALIGN_LEFT);

			chargetaxtable1.addCell(chargecell);
			chargetaxtable1.addCell(chargeamtcell);
			chargetaxtable1.addCell(otherchargecell);
			chargetaxtable1.addCell(otherchargecell1);

		} else if (cnt <= first) {

			for (int i = 0; i < this.prodTaxes.size(); i++) {

				Phrase chunk = null;
				double taxAmt = 0;
				PdfPCell pdfservicecell = null;
				PdfPCell pdftaxamtcell = null;

				if (prodTaxes.get(i).getChargePercent() != 0) {

					if (prodTaxes.get(i).getChargeName().equals("VAT")
							|| prodTaxes.get(i).getChargeName().equals("CST")) {

						System.out.println("1st loop"
								+ prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent());

						String str = prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent();
						double taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;

						myList1.add(str);
						myList2.add(df.format(taxAmt1));

						System.out.println("Size of mylist1 is () === "
								+ myList1.size());
					}

					if (prodTaxes.get(i).getChargeName().equals("Service Tax")) {

						System.out.println("2nd loop == "
								+ prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent());

						String str = prodTaxes.get(i).getChargeName().trim()
								+ " @ " + prodTaxes.get(i).getChargePercent();
						double taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;

						String sss = " ";
						myList3.add(str);
						myList4.add(df.format(taxAmt1));
						myList4.add(sss);
						System.out.println("Size of mylist2 is () === "
								+ myList2.size());
					}
				}
			}

			PdfPCell pdfservicecell = null;

			PdfPTable other1table = new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for (int j = 0; j < myList1.size(); j++) {
				chunk = new Phrase(myList1.get(j), font8);
				pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);

			}

			PdfPCell pdfservicecell1 = null;

			PdfPTable other2table = new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for (int j = 0; j < myList2.size(); j++) {
				chunk = new Phrase(myList2.get(j), font8);
				pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);

			}

			PdfPCell chargescell = null;
			for (int j = 0; j < myList3.size(); j++) {
				chunk = new Phrase(myList3.get(j), font8);
				chargescell = new PdfPCell(chunk);
				chargescell.setBorder(0);
				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(chargescell);

			}

			PdfPCell chargescell1 = null;
			for (int j = 0; j < myList4.size(); j++) {
				chunk = new Phrase(myList4.get(j), font8);
				chargescell1 = new PdfPCell(chunk);
				chargescell1.setBorder(0);
				chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(chargescell1);

			}

			PdfPCell othercell = new PdfPCell();
			othercell.addElement(other1table);
			othercell.setBorder(0);
			chargetaxtable1.addCell(othercell);

			PdfPCell othercell1 = new PdfPCell();
			othercell1.addElement(other2table);
			othercell1.setBorder(0);
			chargetaxtable1.addCell(othercell1);

			for (int i = 0; i < this.prodCharges.size(); i++) {
				Phrase chunk = null;
				Phrase chunk1 = new Phrase("", font8);
				double chargeAmt = 0;
				PdfPCell pdfchargeamtcell = null;
				PdfPCell pdfchargecell = null;
				if (prodCharges.get(i).getChargePercent() != 0) {
					System.out.println(prodCharges.get(i).getChargeName()
							+ ".......charge name" + "@"
							+ prodCharges.get(i).getChargePercent());
					chunk = new Phrase(prodCharges.get(i).getChargeName()
							+ " @ " + prodCharges.get(i).getChargePercent(),
							font1);
					pdfchargecell = new PdfPCell(chunk);
					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargePercent()
							* prodCharges.get(i).getAssessableAmount() / 100;
					chunk = new Phrase(df.format(chargeAmt), font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
				} else {
					pdfchargecell = new PdfPCell(chunk1);
					pdfchargeamtcell = new PdfPCell(chunk1);
				}
				double chargeAmt1 = 0;
				PdfPCell pdfchargeamtcell1 = null;
				PdfPCell pdfchargecell1 = null;

				if (prodCharges.get(i).getChargeAbsValue() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
							font1);
					pdfchargecell1 = new PdfPCell(chunk);
					chargeAmt1 = prodCharges.get(i).getChargeAbsValue();
					chunk = new Phrase(chargeAmt1 + "", font8);
					pdfchargeamtcell1 = new PdfPCell(chunk);
					pdfchargeamtcell1
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell1.setBorder(0);
				} else {
					pdfchargecell1 = new PdfPCell(chunk1);
					pdfchargeamtcell1 = new PdfPCell(chunk1);
				}
				pdfchargecell.setBorder(0);
				pdfchargeamtcell.setBorder(0);
				pdfchargecell1.setBorder(0);
				pdfchargeamtcell1.setBorder(0);

				chargetaxtable1.addCell(pdfchargecell1);
				chargetaxtable1.addCell(pdfchargeamtcell1);
				chargetaxtable1.addCell(pdfchargecell);
				chargetaxtable1.addCell(pdfchargeamtcell);

			}

		}

		/****************************************************************************************/

		PdfPTable netpaytable = new PdfPTable(2);
		netpaytable.setWidthPercentage(100);
		netpaytable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
		// netpaytable.setSpacingBefore(10f);
		// netpaytable.setSpacingAfter(10f);

		// for(int i=0;i<this.prodCharges.size();i++)
		// {
		// Phrase chunk = null;
		// Phrase chunk1 = new Phrase("",font8);
		// double chargeAmt=0;
		// PdfPCell pdfchargeamtcell = null;
		// PdfPCell pdfchargecell = null;
		// if(prodCharges.get(i).getChargePercent()!=0){
		// System.out.println(prodCharges.get(i).getChargeName()+".......charge name"+"@"+prodCharges.get(i).getChargePercent());
		// chunk = new
		// Phrase(prodCharges.get(i).getChargeName()+" @ "+prodCharges.get(i).getChargePercent(),font1);
		// pdfchargecell=new PdfPCell(chunk);
		// pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// chargeAmt=prodCharges.get(i).getChargePercent()*prodCharges.get(i).getAssessableAmount()/100;
		// chunk=new Phrase(df.format(chargeAmt),font8);
		// pdfchargeamtcell = new PdfPCell(chunk);
		// pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// pdfchargeamtcell.setBorder(0);
		// }else{
		// pdfchargecell=new PdfPCell(chunk1);
		// pdfchargeamtcell = new PdfPCell(chunk1);
		// }
		// double chargeAmt1=0;
		// PdfPCell pdfchargeamtcell1 = null;
		// PdfPCell pdfchargecell1 = null;
		//
		// if(prodCharges.get(i).getChargeAbsValue()!=0){
		// chunk = new Phrase(prodCharges.get(i).getChargeName()+"",font1);
		// pdfchargecell1=new PdfPCell(chunk);
		// chargeAmt1=prodCharges.get(i).getChargeAbsValue();
		// chunk=new Phrase(chargeAmt1+"",font8);
		// pdfchargeamtcell1 = new PdfPCell(chunk);
		// pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// pdfchargeamtcell1.setBorder(0);
		// }else{
		// pdfchargecell1=new PdfPCell(chunk1);
		// pdfchargeamtcell1 = new PdfPCell(chunk1);
		// }
		// pdfchargecell.setBorder(0);
		// pdfchargeamtcell.setBorder(0);
		// pdfchargecell1.setBorder(0);
		// pdfchargeamtcell1.setBorder(0);
		//
		// chargetaxtable1.addCell(pdfchargecell1);
		// chargetaxtable1.addCell(pdfchargeamtcell1);
		// chargetaxtable1.addCell(pdfchargecell);
		// chargetaxtable1.addCell(pdfchargeamtcell);
		//
		//
		// // System.out.println("======I  "+i+"   size"+prodCharges.size());
		// // if(i==prodCharges.size()-1){
		// // chunk = new Phrase("Net Payable",font1);
		// // PdfPCell pdfnetpaycell = new PdfPCell(chunk);
		// // pdfnetpaycell.setBorder(0);
		// // Double netPayAmt=(double) 0;
		// // int netpayble = 0;
		// // if(qp instanceof SalesQuotation){
		// // netPayAmt=(Double) ((SalesQuotation) qp).getNetpayable();
		// //
		// // netpayble=(int) netPayAmt.doubleValue();
		// // }
		// // if(qp instanceof SalesOrder){
		// // netPayAmt=(Double) ((SalesOrder) qp).getNetpayable();
		// // netpayble=(int) netPayAmt.doubleValue();
		// //
		// // }
		// // chunk=new Phrase(netpayble+"",font8);
		// // PdfPCell pdfnetpayamt = new PdfPCell(chunk);
		// // pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// // pdfnetpayamt.setBorder(0);
		// //
		// //// chargetaxtable1.addCell(pdfnetpaycell);
		// //// chargetaxtable1.addCell(pdfnetpayamt);
		// //
		// // netpaytable.addCell(pdfnetpaycell);
		// // netpaytable.addCell(pdfnetpayamt);
		// //
		// //// PdfPCell netamoutcell = new PdfPCell();
		// ////
		// //// netamoutcell.addElement(netpaytable);
		// //
		// //
		// //// chargetaxtable1.addCell(netpaytable);
		// // }
		// cut=i;
		//
		// System.out.println("cuttttt"+cut);
		// System.out.println("prod size   "+this.prodTaxes.size());
		// if(cut==5){
		// break;
		// }
		//
		// }

		// /

		for (int i = 0; i < this.prodCharges.size(); i++) {

			System.out
					.println("======I  " + i + "   size" + prodCharges.size());
			if (i == prodCharges.size() - 1) {
				chunk = new Phrase("Net Payable", font1);
				PdfPCell pdfnetpaycell = new PdfPCell(chunk);
				pdfnetpaycell.setBorder(0);
				Double netPayAmt = (double) 0;
				int netpayble = 0;
				if (qp instanceof SalesQuotation) {
					netPayAmt = (Double) ((SalesQuotation) qp).getNetpayable();

					netpayble = (int) netPayAmt.doubleValue();
				}
				if (qp instanceof SalesOrder) {
					netPayAmt = (Double) ((SalesOrder) qp).getNetpayable();
					netpayble = (int) netPayAmt.doubleValue();

				}
				chunk = new Phrase(netpayble + "", font8);
				PdfPCell pdfnetpayamt = new PdfPCell(chunk);
				pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfnetpayamt.setBorder(0);

				netpaytable.addCell(pdfnetpaycell);
				netpaytable.addCell(pdfnetpayamt);

			}

		}

		// /

		if (this.prodCharges.size() == 0) {
			Phrase chunknetpaytitle = new Phrase("Net Payable", font1);
			PdfPCell netpaytitlecell = new PdfPCell(chunknetpaytitle);
			netpaytitlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			netpaytitlecell.setBorder(0);

			Double netPayAmt = (double) 0;
			int netpayble = 0;

			if (qp instanceof SalesQuotation) {
				netPayAmt = (Double) ((SalesQuotation) qp).getNetpayable();

				netpayble = (int) netPayAmt.doubleValue();
			}
			if (qp instanceof SalesOrder) {
				netPayAmt = (Double) ((SalesOrder) qp).getNetpayable();

				netpayble = (int) netPayAmt.doubleValue();
			}
			Phrase chunknetpay = new Phrase(netpayble + "", font8);
			PdfPCell pdfnetpayamt = new PdfPCell(chunknetpay);
			pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			pdfnetpayamt.setBorder(0);

			// chargetaxtable1.addCell(netpaytitlecell);
			// chargetaxtable1.addCell(pdfnetpayamt);

			netpaytable.addCell(netpaytitlecell);
			netpaytable.addCell(pdfnetpayamt);

			// PdfPCell netamoutcell = new PdfPCell();
			//
			// netamoutcell.addElement(netpaytable);

			// chargetaxtable1.addCell(netpaytable);
		}

		PdfPCell termsdatacell1 = new PdfPCell();
		PdfPCell taxdatacell1 = new PdfPCell();

		termsdatacell1.addElement(chargetaxtable1);
		termsdatacell1.setBorder(0);
		// taxdatacell1.addElement(netpaytable);
		chargetaxtable.addCell(termsdatacell1);
		// chargetaxtable.addCell(taxdatacell1);

		// PdfPTable taxinfotable=new PdfPTable(1);
		// taxinfotable.setWidthPercentage(100);
		// taxinfotable.addCell(chargetaxtable);

		PdfPTable parenttaxtable = new PdfPTable(2);
		parenttaxtable.setWidthPercentage(100);
		try {
			parenttaxtable.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell termsdatacell = new PdfPCell();
		PdfPCell taxdatacell = new PdfPCell();

		termsdatacell.addElement(maintable);
		// termsdatacell.setBorder(0);

		taxdatacell.addElement(chargetaxtable);
		parenttaxtable.addCell(termsdatacell);
		parenttaxtable.addCell(taxdatacell);

		try {
			document.add(parenttaxtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// *****************for amt in
		// words****************************************

		double netPayInFigures = 0;

		String amtInWords = "";

		if (qp instanceof SalesQuotation) {
			netPayInFigures = ((SalesQuotation) qp).getNetpayable();
		}
		if (qp instanceof SalesOrder) {
			netPayInFigures = ((SalesOrder) qp).getNetpayable();
		}

		System.out.println("amt" + netPayInFigures);

		Phrase amtwords = new Phrase(amtInWords, font1);
		Phrase netPay1 = new Phrase("Rupees: " + convert(netPayInFigures)
				+ " Only", font8);

		PdfPCell amtWordsCell = new PdfPCell();
		amtWordsCell.addElement(amtwords);
		amtWordsCell.addElement(netPay1);
		amtWordsCell.setBorder(0);

		PdfPTable amonttable = new PdfPTable(1);
		amonttable.setWidthPercentage(100);
		// amonttable.setSpacingBefore(10f);
		amonttable.addCell(amtWordsCell);

		PdfPTable parenttaxtable1 = new PdfPTable(2);
		parenttaxtable1.setWidthPercentage(100);
		try {
			parenttaxtable1.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell termsdatacell2 = new PdfPCell();
		PdfPCell taxdatacell2 = new PdfPCell();

		termsdatacell2.addElement(amonttable);
		taxdatacell2.addElement(netpaytable);
		parenttaxtable1.addCell(termsdatacell2);
		parenttaxtable1.addCell(taxdatacell2);

		try {
			document.add(parenttaxtable1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// //

		// //
	}

	public void othercharges() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		try {
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String title = "Remaining Charges";
		Paragraph titlepara = new Paragraph(title);
		titlepara.setFont(font12boldul);
		titlepara.setAlignment(Element.ALIGN_LEFT);

		PdfPTable chargetaxtable2 = new PdfPTable(2);
		chargetaxtable2.setWidthPercentage(40);
		chargetaxtable2.setHorizontalAlignment(Element.ALIGN_LEFT);
		System.out.println(this.prodCharges.size()
				+ "sizzzzzzzzzzzzzzzzzzeeeeeeeeeeeeeee");
		for (int i = 6; i < this.prodCharges.size(); i++) {
			Phrase chunk = null;
			Phrase chunk1 = new Phrase("", font8);
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			if (prodCharges.get(i).getChargePercent() != 0) {
				System.out.println(prodCharges.get(i).getChargeName()
						+ ".......charge name" + "@"
						+ prodCharges.get(i).getChargePercent());
				chunk = new Phrase(prodCharges.get(i).getChargeName() + " @ "
						+ prodCharges.get(i).getChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargePercent()
						* prodCharges.get(i).getAssessableAmount() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else {
				pdfchargecell = new PdfPCell(chunk1);
				pdfchargeamtcell = new PdfPCell(chunk1);
			}
			double chargeAmt1 = 0;
			PdfPCell pdfchargeamtcell1 = null;
			PdfPCell pdfchargecell1 = null;

			if (prodCharges.get(i).getChargeAbsValue() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
						font1);
				pdfchargecell1 = new PdfPCell(chunk);
				chargeAmt1 = prodCharges.get(i).getChargeAbsValue();
				chunk = new Phrase(chargeAmt1 + "", font8);
				pdfchargeamtcell1 = new PdfPCell(chunk);
				pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else {
				pdfchargecell1 = new PdfPCell(chunk1);
				pdfchargeamtcell1 = new PdfPCell(chunk1);
			}
			pdfchargecell.setBorder(0);
			pdfchargeamtcell.setBorder(0);
			pdfchargecell1.setBorder(0);
			pdfchargeamtcell1.setBorder(0);

			chargetaxtable2.addCell(pdfchargecell1);
			chargetaxtable2.addCell(pdfchargeamtcell1);
			chargetaxtable2.addCell(pdfchargecell);
			chargetaxtable2.addCell(pdfchargeamtcell);

		}
		// PdfPCell chargecell=new PdfPCell();
		// chargecell.addElement(chargetaxtable2);
		// chargecell.setBorder(0);

		// PdfPTable blanktable=new PdfPTable(1);
		// blanktable.setWidthPercentage(100);
		//
		// PdfPTable parentable=new PdfPTable(1);
		// parentable.setWidthPercentage(100);
		// try {
		// parentable.setWidths(new float[]{70,30});
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
		//
		// PdfPCell taxdatacell2= new PdfPCell();
		//
		//
		//
		//
		// System.out.println("prod charg"+this.prodCharges.size());
		// if(this.prodCharges.size()>=5){
		// taxdatacell2.addElement(chargetaxtable2);
		// parentable.addCell(taxdatacell2);
		// }

		// try {
		// document.add(parentable);
		// } catch (DocumentException e) {
		// e.printStackTrace();
		// }

		try {
			if (this.prodCharges.size() > 5) {
				document.add(titlepara);
				document.add(Chunk.NEWLINE);
			}

			document.add(chargetaxtable2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void footerInfo1() {
		Phrase banktitle = new Phrase("Bank Details For Making Payment", font10);
		Paragraph banktitlepara = new Paragraph();
		banktitlepara.add(banktitle);
		banktitlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell banktitlecell = new PdfPCell();
		banktitlecell.addElement(banktitlepara);
		banktitlecell.setBorder(0);
		banktitlecell.setColspan(3);

		Phrase banknametitle = new Phrase("Bank", font10bold);
		Phrase branchtitle = new Phrase("Branch", font10bold);
		Phrase acnotitle = new Phrase("A/C No", font10bold);
		Phrase ifscodetitle = new Phrase("IFSC Code", font10bold);
		Phrase accountholdertitle = new Phrase("A/C Holder", font10bold);

		PdfPCell banknametitlecell = new PdfPCell();
		banknametitlecell.addElement(banknametitle);
		banknametitlecell.setBorder(0);
		PdfPCell branchtitlecell = new PdfPCell();
		branchtitlecell.addElement(branchtitle);
		branchtitlecell.setBorder(0);
		PdfPCell accnotitlecell = new PdfPCell();
		accnotitlecell.addElement(acnotitle);
		accnotitlecell.setBorder(0);
		PdfPCell ifscodetitlecell = new PdfPCell();
		ifscodetitlecell.addElement(ifscodetitle);
		ifscodetitlecell.setBorder(0);
		PdfPCell accholdertitlecell = new PdfPCell();
		accholdertitlecell.addElement(accountholdertitle);
		accholdertitlecell.setBorder(0);

		Phrase realbankname = null;
		if (comppayment != null) {
			realbankname = new Phrase(comppayment.getPaymentBankName().trim(),
					font10);
		} else {
			realbankname = new Phrase("  ", font10);
		}

		Phrase realbranch = null;
		if (comppayment != null) {
			realbranch = new Phrase(comppayment.getPaymentBranch().trim(),
					font10);
		} else {
			realbranch = new Phrase("  ", font10);
		}

		Phrase realaccno = null;
		if (comppayment != null) {
			realaccno = new Phrase(comppayment.getPaymentAccountNo().trim(),
					font10);
		} else {
			realaccno = new Phrase("  ", font10);
		}

		Phrase realifscode = null;
		if (comppayment != null) {
			realifscode = new Phrase(comppayment.getPaymentIFSCcode().trim(),
					font10);
		} else {
			realifscode = new Phrase("  ", font10);
		}

		Phrase realaccholder = null;
		if (comppayment != null) {
			realaccholder = new Phrase(comppayment.getPaymentComName().trim(),
					font10);
		} else {
			realaccholder = new Phrase("  ", font10);
		}

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell coloncell = new PdfPCell();
		coloncell.setBorder(0);
		coloncell.addElement(colon);

		PdfPCell realbanknamecell = new PdfPCell();
		realbanknamecell.addElement(realbankname);
		realbanknamecell.setBorder(0);
		PdfPCell realbranchcell = new PdfPCell();
		realbranchcell.addElement(realbranch);
		realbranchcell.setBorder(0);
		PdfPCell realaccnocell = new PdfPCell();
		realaccnocell.addElement(realaccno);
		realaccnocell.setBorder(0);
		PdfPCell realifscodecell = new PdfPCell();
		realifscodecell.addElement(realifscode);
		realifscodecell.setBorder(0);
		PdfPCell realaccholdercell = new PdfPCell();
		realaccholdercell.addElement(realaccholder);
		realaccholdercell.setBorder(0);

		PdfPTable bankinfotable = new PdfPTable(3);
		bankinfotable.setWidthPercentage(100);
		bankinfotable.addCell(banktitlecell);
		bankinfotable.addCell(banknametitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realbanknamecell);
		bankinfotable.addCell(branchtitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realbranchcell);
		bankinfotable.addCell(accnotitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realaccnocell);
		bankinfotable.addCell(ifscodetitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realifscodecell);
		bankinfotable.addCell(accholdertitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realaccholdercell);

		/**** for 2nd bank *******/

		Phrase banktitle1 = new Phrase("Bank Details For Making Payment",
				font10);
		Paragraph banktitlepara1 = new Paragraph();
		banktitlepara1.add(banktitle1);
		banktitlepara1.setAlignment(Element.ALIGN_CENTER);

		PdfPCell banktitlecell1 = new PdfPCell();
		banktitlecell1.addElement(banktitlepara1);
		banktitlecell1.setBorder(0);
		banktitlecell1.setColspan(3);

		Phrase banknametitle1 = new Phrase("Bank", font10bold);
		Phrase branchtitle1 = new Phrase("Branch", font10bold);
		Phrase acnotitle1 = new Phrase("A/C No", font10bold);
		Phrase ifscodetitle1 = new Phrase("IFSC Code", font10bold);
		Phrase accountholdertitle1 = new Phrase("A/C Holder", font10bold);

		PdfPCell banknametitlecell1 = new PdfPCell();
		banknametitlecell1.addElement(banknametitle1);
		banknametitlecell1.setBorder(0);
		PdfPCell branchtitlecell1 = new PdfPCell();
		branchtitlecell1.addElement(branchtitle1);
		branchtitlecell1.setBorder(0);
		PdfPCell accnotitlecell1 = new PdfPCell();
		accnotitlecell1.addElement(acnotitle1);
		accnotitlecell1.setBorder(0);
		PdfPCell ifscodetitlecell1 = new PdfPCell();
		ifscodetitlecell1.addElement(ifscodetitle1);
		ifscodetitlecell1.setBorder(0);
		PdfPCell accholdertitlecell1 = new PdfPCell();
		accholdertitlecell1.addElement(accountholdertitle1);
		accholdertitlecell1.setBorder(0);

		Phrase realbankname1 = null;
		if (comppayment != null) {
			realbankname1 = new Phrase(comppayment.getPaymentBankName().trim(),
					font10);
		} else {
			realbankname1 = new Phrase("  ", font10);
		}

		Phrase realbranch1 = null;
		if (comppayment != null) {
			realbranch1 = new Phrase(comppayment.getPaymentBranch().trim(),
					font10);
		} else {
			realbranch1 = new Phrase("  ", font10);
		}

		Phrase realaccno1 = null;
		if (comppayment != null) {
			realaccno1 = new Phrase(comppayment.getPaymentAccountNo().trim(),
					font10);
		} else {
			realaccno1 = new Phrase("  ", font10);
		}

		Phrase realifscode1 = null;
		if (comppayment != null) {
			realifscode1 = new Phrase(comppayment.getPaymentIFSCcode().trim(),
					font10);
		} else {
			realifscode1 = new Phrase("  ", font10);
		}

		Phrase realaccholder1 = null;
		if (comppayment != null) {
			realaccholder1 = new Phrase(comppayment.getPaymentComName().trim(),
					font10);
		} else {
			realaccholder1 = new Phrase("  ", font10);
		}

		Phrase colon1 = new Phrase(":", font10bold);
		PdfPCell coloncell1 = new PdfPCell();
		coloncell1.setBorder(0);
		coloncell1.addElement(colon1);

		PdfPCell realbanknamecell1 = new PdfPCell();
		realbanknamecell1.addElement(realbankname1);
		realbanknamecell1.setBorder(0);
		PdfPCell realbranchcell1 = new PdfPCell();
		realbranchcell1.addElement(realbranch1);
		realbranchcell1.setBorder(0);
		PdfPCell realaccnocell1 = new PdfPCell();
		realaccnocell1.addElement(realaccno1);
		realaccnocell1.setBorder(0);
		PdfPCell realifscodecell1 = new PdfPCell();
		realifscodecell1.addElement(realifscode1);
		realifscodecell1.setBorder(0);
		PdfPCell realaccholdercell1 = new PdfPCell();
		realaccholdercell1.addElement(realaccholder1);
		realaccholdercell1.setBorder(0);

		PdfPTable bankinfotable1 = new PdfPTable(3);
		bankinfotable1.setWidthPercentage(100);
		bankinfotable1.addCell(banktitlecell1);
		bankinfotable1.addCell(banknametitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realbanknamecell1);
		bankinfotable1.addCell(branchtitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realbranchcell1);
		bankinfotable1.addCell(accnotitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realaccnocell1);
		bankinfotable1.addCell(ifscodetitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realifscodecell1);
		bankinfotable1.addCell(accholdertitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realaccholdercell1);

		/**** for 2nd bank End *******/

		PdfPTable banktable = new PdfPTable(2);
		banktable.setWidthPercentage(100);
		// banktable.setSpacingAfter(10f);

		banktable.addCell(bankinfotable);
		banktable.addCell(bankinfotable1);

		Phrase recieverSinature = new Phrase("", font12boldul);
		Phrase blankphrase = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);
		Phrase blankphrase1 = new Phrase(" ");
		Phrase blankphrase2 = new Phrase(" ");

		String companyname = "FOR "
				+ comp.getBusinessUnitName().trim().toUpperCase();
		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font9bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		// Phrase companyname= new
		// Phrase(comppayment.getPaymentComName().trim(),font12bold);
		// Phrase authorizedsignatory=null;
		// authorizedsignatory = new
		// Phrase("AUTHORISED SIGNATORY"+"",font10bold);
		String authsign = "AUTHORISED SIGNATORY";
		Paragraph authpara = new Paragraph();
		authpara.add(authsign);
		authpara.setFont(font9bold);
		authpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);

		PdfPCell authsigncell = new PdfPCell();
		authsigncell.addElement(authpara);
		authsigncell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(authsigncell);

		table.setWidthPercentage(100);

		PdfPTable parentbanktable = new PdfPTable(2);
		parentbanktable.setWidthPercentage(100);
		try {
			parentbanktable.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell bankdatacell = new PdfPCell();
		PdfPCell authorisedcell = new PdfPCell();

		bankdatacell.addElement(banktable);
		authorisedcell.addElement(table);
		parentbanktable.addCell(bankdatacell);
		parentbanktable.addCell(authorisedcell);

		try {
			document.add(parentbanktable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	// /**********************

	public void footerInfo() {
		Phrase blankval = new Phrase(" ", font8);
		Phrase banktitle = new Phrase("Bank Details For Making Payment",
				font10boldul);
		Paragraph banktitlepara = new Paragraph();
		banktitlepara.add(banktitle);
		banktitlepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell banktitlecell = null;
		PdfPCell banktitlecell1 = null;

		if (comppayment != null) {
			banktitlecell = new PdfPCell();
			banktitlecell.addElement(banktitlepara);
			banktitlecell.setBorder(0);
			banktitlecell.setColspan(3);
		} else {
			banktitlecell = new PdfPCell();
			banktitlecell.addElement(blankval);
			banktitlecell.setBorder(0);
			banktitlecell.setColspan(3);
		}

		if (comppayment1 != null) {
			System.out.println("One=======");
			banktitlecell1 = new PdfPCell();
			banktitlecell1.addElement(banktitlepara);
			banktitlecell1.setBorder(0);
			banktitlecell1.setColspan(3);
		} else {
			banktitlecell1 = new PdfPCell();
			banktitlecell1.addElement(blankval);
			banktitlecell1.setBorder(0);
			banktitlecell1.setColspan(3);
		}

		Phrase banknametitle = new Phrase("Bank", font10bold);
		Phrase branchtitle = new Phrase("Branch", font10bold);
		Phrase acnotitle = new Phrase("A/C No", font10bold);
		Phrase ifscodetitle = new Phrase("IFSC Code", font10bold);
		Phrase accountholdertitle = new Phrase("A/C Holder", font10bold);

		PdfPCell banknametitlecell = null;
		PdfPCell branchtitlecell = null;
		PdfPCell accnotitlecell = null;
		PdfPCell ifscodetitlecell = null;
		PdfPCell accholdertitlecell = null;

		/******************************** For Default Mode *********************************/

		if (comppayment != null) {
			banknametitlecell = new PdfPCell();
			banknametitlecell.addElement(banknametitle);
			banknametitlecell.setBorder(0);
			branchtitlecell = new PdfPCell();
			branchtitlecell.addElement(branchtitle);
			branchtitlecell.setBorder(0);
			accnotitlecell = new PdfPCell();
			accnotitlecell.addElement(acnotitle);
			accnotitlecell.setBorder(0);
			ifscodetitlecell = new PdfPCell();
			ifscodetitlecell.addElement(ifscodetitle);
			ifscodetitlecell.setBorder(0);
			accholdertitlecell = new PdfPCell();
			accholdertitlecell.addElement(accountholdertitle);
			accholdertitlecell.setBorder(0);
		} else {
			banknametitlecell = new PdfPCell();
			banknametitlecell.addElement(blankval);
			banknametitlecell.setBorder(0);
			branchtitlecell = new PdfPCell();
			branchtitlecell.addElement(blankval);
			branchtitlecell.setBorder(0);
			accnotitlecell = new PdfPCell();
			accnotitlecell.addElement(blankval);
			accnotitlecell.setBorder(0);
			ifscodetitlecell = new PdfPCell();
			ifscodetitlecell.addElement(blankval);
			ifscodetitlecell.setBorder(0);
			accholdertitlecell = new PdfPCell();
			accholdertitlecell.addElement(blankval);
			accholdertitlecell.setBorder(0);
		}

		/********************************* For Other Mode *************************************/

		PdfPCell banknametitlecell1 = null;
		PdfPCell branchtitlecell1 = null;
		PdfPCell accnotitlecell1 = null;
		PdfPCell ifscodetitlecell1 = null;
		PdfPCell accholdertitlecell1 = null;

		if (comppayment1 != null) {
			System.out.println("Two=======");
			banknametitlecell1 = new PdfPCell();
			banknametitlecell1.addElement(banknametitle);
			banknametitlecell1.setBorder(0);
			branchtitlecell1 = new PdfPCell();
			branchtitlecell1.addElement(branchtitle);
			branchtitlecell1.setBorder(0);
			accnotitlecell1 = new PdfPCell();
			accnotitlecell1.addElement(acnotitle);
			accnotitlecell1.setBorder(0);
			ifscodetitlecell1 = new PdfPCell();
			ifscodetitlecell1.addElement(ifscodetitle);
			ifscodetitlecell1.setBorder(0);
			accholdertitlecell1 = new PdfPCell();
			accholdertitlecell1.addElement(accountholdertitle);
			accholdertitlecell1.setBorder(0);
		} else {
			banknametitlecell1 = new PdfPCell();
			banknametitlecell1.addElement(blankval);
			banknametitlecell1.setBorder(0);
			branchtitlecell1 = new PdfPCell();
			branchtitlecell1.addElement(blankval);
			branchtitlecell1.setBorder(0);
			accnotitlecell1 = new PdfPCell();
			accnotitlecell1.addElement(blankval);
			accnotitlecell1.setBorder(0);
			ifscodetitlecell1 = new PdfPCell();
			ifscodetitlecell1.addElement(blankval);
			ifscodetitlecell1.setBorder(0);
			accholdertitlecell1 = new PdfPCell();
			accholdertitlecell1.addElement(blankval);
			accholdertitlecell1.setBorder(0);
		}

		/********************************* For Default CompanyMode **************************************/

		Phrase realbankname = null;
		Phrase realbranch = null;
		Phrase realaccno = null;
		Phrase realifscode = null;
		Phrase realaccholder = null;
		if (comppayment != null) {
			if (comppayment.getPaymentBankName() != null) {
				realbankname = new Phrase(comppayment.getPaymentBankName()
						.trim(), font10);
			} else {
				realbankname = new Phrase("  ", font10);
			}

			if (comppayment.getPaymentBranch() != null) {
				realbranch = new Phrase(comppayment.getPaymentBranch().trim(),
						font10);
			} else {
				realbranch = new Phrase("  ", font10);
			}

			if (comppayment.getPaymentAccountNo() != null) {
				realaccno = new Phrase(
						comppayment.getPaymentAccountNo().trim(), font10);
			} else {
				realaccno = new Phrase("  ", font10);
			}

			if (comppayment.getPaymentIFSCcode() != null) {
				realifscode = new Phrase(comppayment.getPaymentIFSCcode()
						.trim(), font10);
			} else {
				realifscode = new Phrase("  ", font10);
			}

			if (comppayment.getPaymentComName() != null) {
				realaccholder = new Phrase(comppayment.getPaymentComName()
						.trim(), font10);
			} else {
				realaccholder = new Phrase("  ", font10);
			}

		}

		/**************************** For Other Mode ******************************/

		Phrase realbankname1 = null;
		Phrase realbranch1 = null;
		Phrase realaccno1 = null;
		Phrase realifscode1 = null;
		Phrase realaccholder1 = null;

		if (comppayment1 != null) {

			System.out.println("Three=======");

			if (comppayment1.getPaymentBankName() != null) {
				realbankname1 = new Phrase(comppayment1.getPaymentBankName()
						.trim(), font10);
			} else {
				realbankname1 = new Phrase("  ", font10);
			}

			if (comppayment1.getPaymentBranch() != null) {
				realbranch1 = new Phrase(
						comppayment1.getPaymentBranch().trim(), font10);
			} else {
				realbranch1 = new Phrase("  ", font10);
			}

			if (comppayment1.getPaymentAccountNo() != null) {
				realaccno1 = new Phrase(comppayment1.getPaymentAccountNo()
						.trim(), font10);
			} else {
				realaccno1 = new Phrase("  ", font10);
			}

			if (comppayment1.getPaymentIFSCcode() != null) {
				realifscode1 = new Phrase(comppayment1.getPaymentIFSCcode()
						.trim(), font10);
			} else {
				realifscode1 = new Phrase("  ", font10);
			}

			if (comppayment1.getPaymentComName() != null) {
				realaccholder1 = new Phrase(comppayment1.getPaymentComName()
						.trim(), font10);
			} else {
				realaccholder1 = new Phrase("  ", font10);
			}

		}

		Phrase colon = null;
		Phrase colon1 = null;
		PdfPCell coloncell = null;
		PdfPCell coloncell1 = null;

		if (comppayment != null) {
			colon = new Phrase(":", font10bold);
		} else {
			colon = new Phrase("", font10bold);
		}

		if (comppayment1 != null) {
			colon1 = new Phrase(":", font10bold);
		} else {
			colon1 = new Phrase("", font10bold);
		}

		coloncell = new PdfPCell();
		coloncell.setBorder(0);
		coloncell.addElement(colon);

		coloncell1 = new PdfPCell();
		coloncell1.setBorder(0);
		coloncell1.addElement(colon1);

		/********************* For Default Mode ***********************/

		PdfPCell realbanknamecell = new PdfPCell();
		realbanknamecell.addElement(realbankname);
		realbanknamecell.setBorder(0);
		PdfPCell realbranchcell = new PdfPCell();
		realbranchcell.addElement(realbranch);
		realbranchcell.setBorder(0);
		PdfPCell realaccnocell = new PdfPCell();
		realaccnocell.addElement(realaccno);
		realaccnocell.setBorder(0);
		PdfPCell realifscodecell = new PdfPCell();
		realifscodecell.addElement(realifscode);
		realifscodecell.setBorder(0);
		PdfPCell realaccholdercell = new PdfPCell();
		realaccholdercell.addElement(realaccholder);
		realaccholdercell.setBorder(0);

		/******************** For Other Mode ************************/

		PdfPCell realbanknamecell1 = new PdfPCell();
		realbanknamecell1.addElement(realbankname1);
		realbanknamecell1.setBorder(0);
		PdfPCell realbranchcell1 = new PdfPCell();
		realbranchcell1.addElement(realbranch1);
		realbranchcell1.setBorder(0);
		PdfPCell realaccnocell1 = new PdfPCell();
		realaccnocell1.addElement(realaccno1);
		realaccnocell1.setBorder(0);
		PdfPCell realifscodecell1 = new PdfPCell();
		realifscodecell1.addElement(realifscode1);
		realifscodecell1.setBorder(0);
		PdfPCell realaccholdercell1 = new PdfPCell();
		realaccholdercell1.addElement(realaccholder1);
		realaccholdercell1.setBorder(0);

		/****************************** For Default Mode **********************************/

		PdfPTable bankinfotable = new PdfPTable(3);
		float[] columnWidths = { 1.5f, 0.3f, 5f };
		try {
			bankinfotable.setWidths(columnWidths);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		bankinfotable.setSpacingAfter(30f);
		bankinfotable.setWidthPercentage(100);
		bankinfotable.addCell(banktitlecell);
		bankinfotable.addCell(banknametitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realbanknamecell);
		bankinfotable.addCell(branchtitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realbranchcell);
		bankinfotable.addCell(accnotitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realaccnocell);
		bankinfotable.addCell(ifscodetitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realifscodecell);
		bankinfotable.addCell(accholdertitlecell);
		bankinfotable.addCell(coloncell);
		bankinfotable.addCell(realaccholdercell);

		/******************************* For Other Mode **********************************/

		PdfPTable bankinfotable1 = new PdfPTable(3);
		float[] columnWidths1 = { 1.5f, 0.3f, 5f };
		try {
			bankinfotable1.setWidths(columnWidths1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		bankinfotable1.setSpacingAfter(30f);
		bankinfotable1.setWidthPercentage(100);
		bankinfotable1.addCell(banktitlecell1);
		bankinfotable1.addCell(banknametitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realbanknamecell1);
		bankinfotable1.addCell(branchtitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realbranchcell1);
		bankinfotable1.addCell(accnotitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realaccnocell1);
		bankinfotable1.addCell(ifscodetitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realifscodecell1);
		bankinfotable1.addCell(accholdertitlecell1);
		bankinfotable1.addCell(coloncell1);
		bankinfotable1.addCell(realaccholdercell1);

		PdfPTable parentmodetable = new PdfPTable(2);
		parentmodetable.setWidthPercentage(100);
		try {
			parentmodetable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPTable blanktable = new PdfPTable(1);
		blanktable.setWidthPercentage(100);

		PdfPCell bankdefault = new PdfPCell();
		PdfPCell bankother = new PdfPCell();

		// dipak
		// bankdefault.addElement(bankinfotable);
		// bankother.addElement(bankinfotable1);
		// parentmodetable.addCell(bankdefault);
		// parentmodetable.addCell(bankother);

		// change for defalut false....

		bankdefault.addElement(bankinfotable);
		bankother.addElement(bankinfotable1);

		if (comppayment != null) {
			parentmodetable.addCell(bankdefault);
			parentmodetable.addCell(bankother);
		} else {
			parentmodetable.addCell(bankother);
			parentmodetable.addCell(blanktable);
		}

		Phrase recieverSinature = new Phrase("", font12boldul);
		Phrase blankphrase = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);
		Phrase blankphrase1 = new Phrase(" ");
		Phrase blankphrase2 = new Phrase(" ");

		String companyname = comp.getBusinessUnitName().trim().toUpperCase();
		Paragraph companynamepara = new Paragraph();
		companynamepara.add("FOR " + companyname);
		companynamepara.setFont(font9bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		// Phrase companyname= new
		// Phrase(comppayment.getPaymentComName().trim(),font12bold);
		// Phrase authorizedsignatory=null;
		// authorizedsignatory = new
		// Phrase("AUTHORISED SIGNATORY"+"",font10bold);
		String authsign = "AUTHORISED SIGNATORY";
		Paragraph authpara = new Paragraph();
		authpara.add(authsign);
		authpara.setFont(font9bold);
		authpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);

		PdfPCell authsigncell = new PdfPCell();
		authsigncell.addElement(authpara);
		authsigncell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(authsigncell);

		table.setWidthPercentage(100);

		// ******************end of document***********************
		String endDucument = "*************************X--X--X*************************";
		Paragraph endpara = new Paragraph();
		endpara.add(endDucument);
		endpara.setAlignment(Element.ALIGN_CENTER);
		// ***************************************************

		PdfPTable parentbanktable = new PdfPTable(2);
		parentbanktable.setWidthPercentage(100);
		try {
			parentbanktable.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell bankdatacell = new PdfPCell();
		PdfPCell authorisedcell = new PdfPCell();

		if (comppayment != null && comppayment1 != null) {

			System.out.println("Four=======");
			bankdatacell.addElement(parentmodetable);
			authorisedcell.addElement(table);
			parentbanktable.addCell(bankdatacell);
			parentbanktable.addCell(authorisedcell);
		}
		if (comppayment1 != null && comppayment == null) {
			System.out.println("Five=======");
			bankdatacell.addElement(parentmodetable);
			authorisedcell.addElement(table);
			parentbanktable.addCell(bankdatacell);
			parentbanktable.addCell(authorisedcell);
		}
		if (comppayment != null && comppayment1 == null) {
			System.out.println("Six=======");
			bankdatacell.addElement(parentmodetable);
			authorisedcell.addElement(table);
			parentbanktable.addCell(bankdatacell);
			parentbanktable.addCell(authorisedcell);
		}
		if (comppayment == null && comppayment1 == null) {
			System.out.println("Seven=======");
			bankdatacell.addElement(blanktable);
			authorisedcell.addElement(table);
			parentbanktable.addCell(bankdatacell);
			parentbanktable.addCell(authorisedcell);
		}

		Phrase refphrase = new Phrase("Annexure 1 ", font10bold);
		Paragraph repara = new Paragraph(refphrase);

		try {
			document.add(parentbanktable);

			if (this.products.size() > 14) {
				// if(eva>20){
				document.newPage();
				document.add(repara);

				settingRemainingRowsToPDF(15);
			}
			if (this.products.size() > 74) {
				// }if(eva>80){
				document.newPage();
				document.add(repara);
				settingRemainingRowsToPDF(75);
			}
			if (this.products.size() > 134) {
				// }if(eva>140){
				document.newPage();
				settingRemainingRowsToPDF(135);
			}
			if (this.products.size() > 194) {
				// }if(eva>200){
				document.newPage();
				document.add(repara);
				settingRemainingRowsToPDF(195);
			}
			// if(this.products.size()>16){
			// document.add(endpara);
			// }
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// //
		int descint = 0;
		if (qp instanceof SalesQuotation) {
			SalesQuotation sq = (SalesQuotation) qp;
			if (!sq.getDescription().equals("")) {
				descint = 1;
				System.out.println("description1111--" + descint);
			}
		}
		if (qp instanceof SalesOrder) {
			SalesOrder so = (SalesOrder) qp;
			if (!so.getDescription().equals("")) {
				descint = 1;
				System.out.println("description222--" + descint);
			}
		}

		int proddes = 0;
		for (int i = 0; i < this.stringlis.size(); i++) {

			if (stringlis.get(i).getComment() != null
					&& !stringlis.get(i).getComment().equals("")) {
				proddes = 1;
				System.out.println("pro coment+++   "
						+ stringlis.get(i). getComment());
			}

		}
		int first = 7;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();
		System.out.println("ocunt is " + cnt);

		if ((descint == 1) || (proddes == 1) || (cnt > first)) {
			try {
				createServices();

			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}

	}

	// /

	// List<ProductDetailsPO> productcount =new ArrayList<ProductDetailsPO>();
	// //
	// public List<ProductDetailsPO> getdata(){
	//
	// ProductDetailsPO po=new ProductDetailsPO();
	// po.setProductCategory("category");
	// // po.setPrduct("Product");
	// po.setProductQuantity(20);
	// po.setTax(65);
	// po.setVat(55);
	// po.setDiscount(22);
	// // po.setTotal(50);
	// productcount.add(po);
	// return productcount;
	// }

	// //

	private void createServices() throws DocumentException {

		// Add new line

		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		document.add(nextpage);

		// Sort the service on product names
		// java.util.Collections.sort(service, new ServiceComparator());

		// ///start

		String title = "";
		if (qp instanceof SalesOrder)
			title = "Sales Order";
		else
			title = "Quotation";

		String countinfo = "";
		if (qp instanceof SalesOrder) {
			SalesOrder salesordentity = (SalesOrder) qp;
			countinfo = " ID: " + salesordentity.getCount() + "";
		} else {
			SalesQuotation salesquotentity = (SalesQuotation) qp;
			countinfo = " ID: " + salesquotentity.getCount() + "";
		}

		String creationdateinfo = "";
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		if (qp instanceof SalesOrder) {
			SalesOrder salesordentity = (SalesOrder) qp;
			creationdateinfo = "Date: "
					+ fmt.format(salesordentity.getSalesOrderDate());
		} else {
			SalesQuotation salesquotentity = (SalesQuotation) qp;
			creationdateinfo = "Date: "
					+ fmt.format(salesquotentity.getCreationDate());
		}

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);

		Phrase idphrase = new Phrase(countinfo, font14);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		document.add(titlepdftable);

		// ///end

		String descHeading = "";
		String descinfo = "";
		// int flag=0;
		if (qp instanceof SalesQuotation) {
			SalesQuotation sq = (SalesQuotation) qp;
			if (!sq.getDescription().equals("")) {
				descinfo = sq.getDescription();
			}
		}
		if (qp instanceof SalesOrder) {
			SalesOrder so = (SalesOrder) qp;
			if (!so.getDescription().equals("")) {
				descinfo = so.getDescription();
			}
		}

		Phrase descphrase = new Phrase(descinfo, font8);

		Paragraph despara = new Paragraph();
		despara.add(descphrase);
		document.add(Chunk.NEWLINE);
		document.add(despara);
		document.add(Chunk.NEWLINE);

		String prodetails = "";
		for (int i = 0; i < this.products.size(); i++) {
			if (!stringlis.get(i).getComment().equals("")) {
				prodetails = "Products Details";
			}

		}

		Phrase prophrase = new Phrase(prodetails, font12boldul);
		Paragraph propara = new Paragraph();
		propara.add(prophrase);
		document.add(propara);
		document.add(Chunk.NEWLINE);

		Phrase term = new Phrase("", font10);

		String descri = "";
		String remark="";
		Phrase descri1 = new Phrase("", font10);
		Paragraph para1 = new Paragraph("");
		System.out.println("pro size() ==== " + this.products.size());

		for (int i = 0; i < this.products.size(); i++) {
			if (!stringlis.get(i).getComment().equals("")) {
				term = new Phrase("Product Id : " + stringlis.get(i).getCount()
						+ "" + "        Product Name : "
						+ stringlis.get(i).getProductName(), font10bold);

				descri1 = new Phrase(descri + stringlis.get(i). getComment()+"\n"+stringlis.get(i). getCommentdesc(),
						font10);
				remark=stringlis.get(i). getCommentdesc();
				System.out.println("des----11"+remark);
				System.out.println("des----"+descri);
				System.out.println("des2222---"+stringlis.get(i).getComment());
				System.out.println("des2222---"+stringlis.get(i).getCommentdesc());
				System.out.println("des2222---"+stringlis.get(i).getCommentdesc1());
				System.out.println("des2222---"+stringlis.get(i).getCommentdesc2());
			} else {
				term = new Phrase("", font10);
				descri1 = new Phrase("", font10);
			}

			/*************************************** Changes ********************************************/
			para1 = new Paragraph();
			para1.add(term);
			para1.setAlignment(Element.ALIGN_LEFT);
			Paragraph para2 = new Paragraph();
			para2.add(descri1);

			document.add(para1);

			PdfPCell immagcell = null;
			// if(!stringlis.get(i).getComment().equals("")){
			DocumentUpload document12 = stringlis.get(i).getProductImage();
			if (document12 != null && !document12.getUrl().equals("")) {
				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}

				try {
					Image image2 = Image.getInstance(new URL(hostUrl
							+ document12.getUrl()));
					image2.scalePercent(20f);
					// image2.setAbsolutePosition(40f,745f);

					immagcell = new PdfPCell(image2);
					immagcell.setBorder(0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// }

			PdfPCell immagcell11 = null;
			DocumentUpload document11 = stringlis.get(i).getProductImage1();
			if (document11 != null && !document11.getUrl().equals("")) {
				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}

				try {
					Image image11 = Image.getInstance(new URL(hostUrl
							+ document11.getUrl()));
					image11.scalePercent(20f);
					// image2.setAbsolutePosition(40f,745f);

					immagcell11 = new PdfPCell(image11);
					immagcell11.setBorder(0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			PdfPCell immagcell22 = null;
			DocumentUpload document22 = stringlis.get(i).getProductImage2();
			if (document22 != null && !document22.getUrl().equals("")) {
				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}

				try {
					Image image22 = Image.getInstance(new URL(hostUrl
							+ document22.getUrl()));
					image22.scalePercent(20f);
					// image2.setAbsolutePosition(40f,745f);

					immagcell22 = new PdfPCell(image22);
					immagcell22.setBorder(0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			PdfPCell immagcell33 = null;
			DocumentUpload document33 = stringlis.get(i).getProductImage3();
			if (document33 != null && !document33.getUrl().equals("")) {
				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}

				try {
					Image image33 = Image.getInstance(new URL(hostUrl
							+ document33.getUrl()));
					image33.scalePercent(20f);
					// image2.setAbsolutePosition(40f,745f);

					immagcell33 = new PdfPCell(image33);
					immagcell33.setBorder(0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			/*****************************************************************************************/
			document.add(para2);
			document.add(Chunk.NEWLINE);
			Phrase blankk = new Phrase(" ", font10);
			PdfPCell blankcell = new PdfPCell(blankk);
			blankcell.setBorder(0);

			PdfPTable table = new PdfPTable(4);
			if (document12 != null && !document12.getUrl().equals("")) {
				table.addCell(immagcell);
			} else {
				table.addCell(blankcell);
			}
			if (document11 != null && !document11.getUrl().equals("")) {
				table.addCell(immagcell11);
			} else {
				table.addCell(blankcell);
			}
			if (document22 != null && !document22.getUrl().equals("")) {
				table.addCell(immagcell22);
			} else {
				table.addCell(blankcell);
			}
			if (document33 != null && !document33.getUrl().equals("")) {
				table.addCell(immagcell33);
			} else {
				table.addCell(blankcell);
			}

			document.add(table);

			term = new Phrase("", font10);
			descri1 = new Phrase("", font10);

			// document.add(Chunk.NEWLINE);
		}

		// int desc=0;
		// if(qp instanceof SalesQuotation){
		// SalesQuotation sq=(SalesQuotation)qp;
		// if(!sq.getDescription().equals("")){
		//
		// desc=1;
		// }
		// }
		// if(qp instanceof SalesOrder){
		// SalesOrder so=(SalesOrder)qp;
		// if(!so.getDescription().equals("")){
		//
		// desc=1;
		// }gg
		// }
		//
		// if(this.prodCharges.size()>=5||desc==1){
		//
		// // othercharges();
		// otherchargestoAnnexur();
		// }
		int first = 7;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();
		if (cnt > first) {
			otherchargestoAnnexur();
		}

	}

	public void settingRemainingRowsToPDF(int flag) {

		PdfPTable table = new PdfPTable(8);

		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase category = new Phrase("SR NO. ", font1);
		Phrase product = new Phrase("ITEM DETAILS ", font1);
		Phrase qty = new Phrase("QTY", font1);
		Phrase uom = new Phrase("UNIT", font1);
		Phrase rate = new Phrase("RATE", font1);
		Phrase percdisc = new Phrase("DISC %", font1);
		Phrase servicetax = new Phrase("TAX", font1);

		Paragraph tax = new Paragraph();
		// *************chnges rohan on 22/4/2015********************
		Phrase svat = null;
		Phrase vvat = null;
		Phrase vat = null;
		int cout = 0;
		int flag1 = 0;
		int st = 0;
		int st1 = 0;

		for (int i = 0; i < this.products.size(); i++) {

			int cstFlag = 0;
			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {

					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vvat = new Phrase("VAT / ST %", font1);
				cout = 1;
			} else if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() == 0
					&& cstFlag == 0) {
				vat = new Phrase("ST %", font1);
				st = 1;
			} else if (products.get(i).getServiceTax().getPercentage() == 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vat = new Phrase("VAT %", font1);
				st1 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {
				svat = new Phrase("CST / ST %", font1);
				flag1 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() == 0) {
				vat = new Phrase("CST %", font1);

			} else {
				vat = new Phrase("TAX %", font1);
			}

		}

		Phrase stvat = null;

		if (cout > 0) {
			tax.add(vvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else if (st == 1 && st1 == 1) {

			stvat = new Phrase("VAT / ST %", font1);
			tax.add(stvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		else if (flag1 > 0) {
			tax.add(svat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else {
			tax.add(vat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		Phrase total = new Phrase("AMOUNT", font1);
		// Phrase total = new Phrase("Total",font1);

		PdfPCell cellcategory = new PdfPCell(category);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celluom = new PdfPCell(uom);
		celluom.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(percdisc);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell cellvat = new PdfPCell(vat);
		PdfPCell cellservicetax = new PdfPCell(tax);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellcategory);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(celluom);
		table.addCell(cellrate);
		table.addCell(cellperdisc);
		// table.addCell(cellvat);
		table.addCell(cellservicetax);
		table.addCell(celltotal);

		// //

		// Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		// Phrase category = new Phrase("Category ", font1);
		// // Phrase product = new Phrase("Product ", font1);
		// Phrase qty = new Phrase("Quantity", font1);
		// // Phrase rate = new Phrase("Rate", font1);
		// Phrase servicetax = new Phrase("Service Tax", font1);
		// Phrase vat = new Phrase("VAT", font1);
		// Phrase percdisc = new Phrase("% Discount", font1);
		// // Phrase total = new Phrase("Total", font1);
		//
		// PdfPCell cellcategory = new PdfPCell(category);
		// // PdfPCell cellproduct = new PdfPCell(product);
		// PdfPCell cellqty = new PdfPCell(qty);
		// // PdfPCell cellrate = new PdfPCell(rate);
		// PdfPCell cellperdisc = new PdfPCell(percdisc);
		// PdfPCell cellvat = new PdfPCell(vat);
		// PdfPCell cellservicetax = new PdfPCell(servicetax);
		// // PdfPCell celltotal = new PdfPCell(total);
		//
		// table.addCell(cellcategory);
		// // table.addCell(cellproduct);
		// table.addCell(cellqty);
		// // table.addCell(cellrate);
		// table.addCell(cellperdisc);
		// table.addCell(cellvat);
		// table.addCell(cellservicetax);

		// //

		for (int i = flag; i < this.products.size(); i++) {

			Phrase chunk = new Phrase((i + 1) + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			chunk = new Phrase(products.get(i).getUnitOfMeasurement(), font8);
			PdfPCell pdfuomcell = new PdfPCell(chunk);
			pdfuomcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedRate = products.get(i).getPrice() - taxVal;
			chunk = new Phrase(df.format(calculatedRate), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			// if(products.get(i).getServiceTax()!=null)
			// chunk = new
			// Phrase(products.get(i).getServiceTax().getPercentage()+"",font8);
			// else
			// chunk = new Phrase("N.A"+"",font8);

			PdfPCell pdfservice1 = null;
			// ******************************
			double cstval = 0;
			int cstFlag = 0;

			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {
					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesq.getCstpercent();
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesorder.getCstpercent();
					}
				}
			}

			System.out.println("one1");

			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)
					&& (cstFlag == 0)) {

				System.out.println("two1");

				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null)) {

					if (cout > 0) {
						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {

						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					}

				} else {

					chunk = new Phrase("00.00" + "", font8);
				}
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0 && cstFlag == 0)) {

				System.out.println("three1");
				if (products.get(i).getServiceTax() != null) {
					if (flag1 == 1 || cout == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else {
						chunk = new Phrase(df.format(products.get(i)
								.getServiceTax().getPercentage()), font8);
					}

				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& cstFlag == 1) {
				System.out.println("four1");

				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else if (cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {

				System.out.println("five1");
				if (flag1 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {

				System.out.println("six1");
				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else if ((cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0) && (products
					.get(i).getVatTax().getPercentage() > 0))) {

				System.out.println("seven1");
				if (flag1 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				pdfservice1 = new PdfPCell(chunk);

				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& (cstFlag == 0)) {
				System.out.println("eight1");
				if (flag1 == 1 || cout == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				} else if (st == 1 && st1 == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				}

				else {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage()), font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else {
				chunk = new Phrase("0.00 / 0.00", font8);
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			// **************************************

			// PdfPCell pdfservice = new PdfPCell(chunk);
			// pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);

			if (products.get(i).getVatTax() != null)
				chunk = new Phrase(products.get(i).getVatTax().getPercentage()
						+ "", font8);
			else
				chunk = new Phrase("N.A" + "", font8);
			PdfPCell pdfvattax = new PdfPCell(chunk);

			if (products.get(i).getPercentageDiscount() != 0)
				chunk = new Phrase(
						products.get(i).getPercentageDiscount() + "", font8);
			else
				chunk = new Phrase("0" + "", font8);
			PdfPCell pdfperdiscount = new PdfPCell(chunk);
			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			double totalVal = (products.get(i).getPrice() - taxVal)
					* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}
			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table.addCell(pdfcategcell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfuomcell);
			table.addCell(pdfspricecell);
			table.addCell(pdfperdiscount);
			// table.addCell(pdfvattax);
			table.addCell(pdfservice1);
			table.addCell(pdftotalproduct);

			// //

			// for(int i =0;i<eva;i++){
			// if(getdata().get(i).getProductCategory()!=null){
			// Phrase chunk = new
			// Phrase(getdata().get(i).getProductCategory(),font8);
			//
			// }else{
			// }
			// PdfPCell pdfcategcell = new PdfPCell(chunk);
			//
			//
			//
			//
			// if(getdata().get(i).getProductQuantity()!=0){
			// chunk = new Phrase(getdata().get(i).getProductQuantity() + "",
			// font8);
			// }
			// else{
			// chunk = new Phrase("", font8);
			// }
			// PdfPCell pdfqtycell = new PdfPCell(chunk);
			//
			//
			//
			// if(getdata().get(i).getTax()!=0){
			// chunk = new Phrase(getdata().get(i).getTax() + "", font8);
			// }else{
			// chunk = new Phrase("", font8);
			// }
			// PdfPCell pdfservice = new PdfPCell(chunk);
			//
			//
			//
			//
			// if(getdata().get(i).getVat()!=0){
			//
			// chunk = new Phrase(getdata().get(i).getVat() + "", font8);
			// }else{
			// chunk = new Phrase( "", font8);
			// }
			// PdfPCell pdfvattax = new PdfPCell(chunk);
			//
			//
			//
			//
			// if(getdata().get(i).getDiscount()!=0)
			// {
			// chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
			//
			// }else{
			// chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
			// }
			//
			// PdfPCell pdfperdiscount = new PdfPCell(chunk);
			//
			// table.addCell(pdfcategcell);
			// table.addCell(pdfqtycell);
			// table.addCell(pdfperdiscount);
			// table.addCell(pdfvattax);
			// table.addCell(pdfservice);

			// //
			flag = flag + 1;
			if (flag == 76 || flag == 136 || flag == 196) {
				break;
			}
		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		parentTableProd.setSpacingBefore(10f);
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	// /************************

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			// Here if both are inclusive then first remove service tax and then
			// on that amount
			// calculate vat.
			double removeServiceTax = (entity.getPrice() / (1 + service / 100));

			// double taxPerc=service+vat;
			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
			// below
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPrice() - retrServ;
		}
		tax = retrVat + retrServ;
		return tax;
	}

	/****************** amount in words ***************************************/

	private static final String[] tensNames = { "", " Ten", " Twenty",
			" Thirty", " Forty", " Fifty", " Sixty", " Seventy", " Eighty",
			" Ninety" };

	private static final String[] numNames = { "", " One", " Two", " Three",
			" Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten",
			" Eleven", " Twelve", " Thirteen", " Fourteen", " Fifteen",
			" Sixteen", " Seventeen", " Eighteen", " Nineteen" };

	// public static String convert(int number){

	private static String convertLessThanOneThousand(int number) {
		String soFar;
		if (number % 100 < 20) {
			soFar = numNames[number % 100];
			number /= 100;
		} else {
			soFar = numNames[number % 10];
			number /= 10;
			soFar = tensNames[number % 10] + soFar;
			number /= 10;
		}
		if (number == 0)
			return soFar;
		return numNames[number] + " Hundred" + soFar;
	}

	public static String convert(double number) {
		// 0 to 999 999 999 999
		if (number == 0) {
			return "Zero";
		}
		String snumber = Double.toString(number);
		// pad with "0"
		String mask = "000000000000";
		DecimalFormat df = new DecimalFormat(mask);
		snumber = df.format(number);
		int hyndredCrore = Integer.parseInt(snumber.substring(3, 5));
		int hundredLakh = Integer.parseInt(snumber.substring(5, 7));
		int hundredThousands = Integer.parseInt(snumber.substring(7, 9));
		int thousands = Integer.parseInt(snumber.substring(9, 12));
		String tradBillions;
		switch (hyndredCrore) {
		case 0:
			tradBillions = "";
			break;
		case 1:
			tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore ";
			break;
		default:
			tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore ";
		}

		String result = tradBillions;
		String tradMillions;
		switch (hundredLakh) {
		case 0:
			tradMillions = "";
			break;
		case 1:
			tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh ";
			break;
		default:
			tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh ";
		}
		result = result + tradMillions;
		String tradHundredThousands;

		switch (hundredThousands) {
		case 0:
			tradHundredThousands = "";
			break;
		case 1:
			tradHundredThousands = "One Thousand ";
			break;
		default:
			tradHundredThousands = convertLessThanOneThousand(hundredThousands)
					+ " Thousand ";
		}
		result = result + tradHundredThousands;

		String tradThousand;
		tradThousand = convertLessThanOneThousand(thousands);
		result = result + tradThousand;
		return result.replaceAll("^\\s+", "").replaceAll(
				"file://b//s%7B2,%7D//b", " ");
	}

	private void otherchargestoAnnexur() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		String charge = "Other charge details";
		Chunk prodchunk = new Chunk(charge, font10bold);
		Paragraph parag = new Paragraph();
		parag.add(prodchunk);

		PdfPTable chargetable = new PdfPTable(2);
		chargetable.setWidthPercentage(100);
		// ********************
		double total = 0;
		for (int i = 0; i < this.prodCharges.size(); i++) {
			Phrase chunk = null;
			double chargeAmt = 0;
			Phrase blank11 = new Phrase("", font1);
			PdfPCell blankcell = new PdfPCell(blank11);
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			if (prodCharges.get(i).getChargePercent() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + " @ "
						+ prodCharges.get(i).getChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargePercent()
						* prodCharges.get(i).getAssessableAmount() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);

			}
			if (prodCharges.get(i).getChargeAbsValue() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
						font1);
				pdfchargecell = new PdfPCell(chunk);

				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargeAbsValue();
				chunk = new Phrase(chargeAmt + "", font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
			}
			pdfchargecell.setBorder(0);

			total = total + chargeAmt;
			if (chargeAmt != 0) {
				chargetable.addCell(pdfchargecell);
				chargetable.addCell(pdfchargeamtcell);
			}
			// else{
			// chargetable.addCell(blankcell);
			// chargetable.addCell(blankcell);
			// }

		}

		PdfPTable taotatable = new PdfPTable(2);
		taotatable.setWidthPercentage(100);

		Phrase totalchunk = new Phrase("Total", font10bold);
		PdfPCell totalcell = new PdfPCell(totalchunk);
		// totalcell.addElement(totalchunk);
		totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totalcell.setBorder(0);

		Phrase toatlamt = new Phrase(df.format(total), font10bold);
		PdfPCell totalcell1 = new PdfPCell(toatlamt);
		// totalcell1.addElement(toatlamt);
		totalcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalcell1.setBorder(0);

		taotatable.addCell(totalcell);
		taotatable.addCell(totalcell1);

		PdfPCell chargecell = new PdfPCell();
		chargecell.addElement(chargetable);
		// chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell chargecell1 = new PdfPCell();
		chargecell1.addElement(taotatable);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(70);
		parent.setSpacingBefore(10f);
		parent.addCell(chargecell);
		parent.addCell(chargecell1);
		parent.setHorizontalAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(parag);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void commentDetailsForEmail() {

		if (qp instanceof SalesOrder) {
			SalesOrder salesOrderEntity = (SalesOrder) qp;
			for (int i = 0; i < salesOrderEntity.getItems().size(); i++) {

				if (qp.getCompanyId() != null) {
					sup = ofy()
							.load()
							.type(SuperProduct.class)
							.filter("companyId", qp.getCompanyId())
							.filter("count",
									salesOrderEntity.getItems().get(i)
											.getPrduct().getCount()).first()
							.now();

					SuperProduct superprod = new SuperProduct();

					superprod.setCount(sup.getCount());
					superprod.setProductName(sup.getProductName());
					superprod.setComment(sup.getComment() + " "
							+ sup.getCommentdesc());

					stringlis.add(superprod);
				} else {
					sup = ofy()
							.load()
							.type(SuperProduct.class)
							.filter("count",
									salesOrderEntity.getItems().get(i)
											.getPrduct().getCount()).first()
							.now();
				}
			}
		}

		if (qp instanceof SalesQuotation) {
			SalesQuotation quotEntity = (SalesQuotation) qp;
			for (int i = 0; i < quotEntity.getItems().size(); i++) {

				if (qp.getCompanyId() != null) {
					sup = ofy()
							.load()
							.type(SuperProduct.class)
							.filter("companyId", qp.getCompanyId())
							.filter("count",
									quotEntity.getItems().get(i).getPrduct()
											.getCount()).first().now();

					SuperProduct superprod = new SuperProduct();

					superprod.setCount(sup.getCount());
					superprod.setProductName(sup.getProductName());
					superprod.setComment(sup.getComment() + " "
							+ sup.getCommentdesc());

					stringlis.add(superprod);
				} else {
					sup = ofy()
							.load()
							.type(SuperProduct.class)
							.filter("count",
									quotEntity.getItems().get(i).getPrduct()
											.getCount()).first().now();
				}
			}
		}
	}

	private void createProductDetailWithDiscount() {

		logger.log(Level.SEVERE, "in product methos============1");

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		float[] columnWidths = { 0.8f, 3.5f, 0.7f, 1.1f, 0.9f, 0.8f, 1.2f,
				1.1f, 1.0f };

		PdfPTable table = new PdfPTable(9);
		table.setWidthPercentage(100);
		table.setSpacingAfter(185f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		logger.log(Level.SEVERE, "in product methos============12");
		Phrase category = new Phrase("SR. NO.", font1);
		Paragraph srno = new Paragraph();
		srno.add(category);

		Phrase product = new Phrase("ITEM DETAILS ", font1);
		Paragraph itemdetail = new Paragraph();
		itemdetail.add(product);

		Phrase qty = new Phrase("QTY.", font1);
		Paragraph quty = new Paragraph();
		quty.add(qty);

		Phrase rate = new Phrase("UNIT", font1);
		Paragraph unit = new Paragraph();
		unit.add(rate);

		Phrase servicetax = new Phrase("RATE", font1);
		Paragraph rate1 = new Paragraph();
		rate1.add(servicetax);

		Phrase percdisc = new Phrase("DISC %", font1);
		Paragraph disct = new Paragraph();
		disct.add(percdisc);

		Phrase disc = new Phrase("DISC", font1);
		Paragraph discCell = new Paragraph();
		discCell.add(disc);

		Paragraph tax = new Paragraph();
		// *************chnges rohan on 22/4/2015********************
		Phrase svat = null;
		Phrase vvat = null;
		Phrase vat = null;
		int cout = 0;
		int flag21 = 0;
		int st = 0;
		int st1 = 0;

		logger.log(Level.SEVERE, "in product methos============13");

		for (int i = 0; i < this.products.size(); i++) {

			int cstFlag = 0;
			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {

					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vvat = new Phrase("VAT / ST %", font1);
				cout = 1;
			} else if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() == 0
					&& cstFlag == 0) {
				vat = new Phrase("ST %", font1);
				st = 1;
			} else if (products.get(i).getServiceTax().getPercentage() == 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vat = new Phrase("VAT %", font1);
				st1 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {
				svat = new Phrase("CST / ST %", font1);
				flag21 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() == 0) {
				vat = new Phrase("CST %", font1);

			} else {
				vat = new Phrase("TAX %", font1);
			}
		}

		Phrase stvat = null;

		logger.log(Level.SEVERE, "in product methos============14");

		if (cout > 0) {
			tax.add(vvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else if (st == 1 && st1 == 1) {

			stvat = new Phrase("VAT / ST %", font1);
			tax.add(stvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		else if (flag21 > 0) {
			tax.add(svat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else {
			tax.add(vat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		// **************************************changes
		// ends....................

		Phrase total = new Phrase("AMOUNT", font1);
		Paragraph amount = new Paragraph();
		amount.add(total);
		amount.setAlignment(Element.ALIGN_CENTER);

		logger.log(Level.SEVERE, "in product methos============15");
		PdfPCell cellcategory = new PdfPCell(srno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproduct = new PdfPCell(itemdetail);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellqty = new PdfPCell(quty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(unit);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rate1);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellvat = new PdfPCell(disct);
		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell disAmtCell = new PdfPCell(discCell);
		disAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellperdisc = new PdfPCell(tax);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(amount);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellcategory);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellrate);
		table.addCell(cellservicetax);
		table.addCell(cellvat);
		table.addCell(disAmtCell);
		table.addCell(cellperdisc);
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		if (this.products.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size();
			blankLines = size * (140 / 14);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 5f;
		}
		table.setSpacingAfter(blankLines);

		Phrase rephrse = null;

		int flagr = 0;
		int flagr1 = 0;
		for (int i = 0; i < this.products.size(); i++) {
			logger.log(Level.SEVERE, "in product methos============16");
			Phrase chunk = new Phrase((i + 1) + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase chunk1 = new Phrase(products.get(i).getUnitOfMeasurement(),
					font8);
			PdfPCell pdfmofcell = new PdfPCell(chunk1);
			pdfmofcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedRate = products.get(i).getPrice() - taxVal;
			chunk = new Phrase(df.format(calculatedRate), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			if (products.get(i).getPercentageDiscount() != 0)
				chunk = new Phrase(
						products.get(i).getPercentageDiscount() + "", font8);
			else
				chunk = new Phrase("0" + "", font8);
			PdfPCell pdfperdiscount = new PdfPCell(chunk);
			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			if (products.get(i).getDiscountAmt() != 0)
				chunk = new Phrase(products.get(i).getDiscountAmt() + "", font8);
			else
				chunk = new Phrase("0" + "", font8);
			PdfPCell discAmtCell = new PdfPCell(chunk);
			discAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			logger.log(Level.SEVERE, "in product methos============17");

			PdfPCell pdfservice1 = null;
			// ******************************
			double cstval = 0;
			int cstFlag = 0;

			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {
					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesq.getCstpercent();
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesorder.getCstpercent();
					}
				}
			}

			System.out.println("one");

			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)
					&& (cstFlag == 0)) {

				System.out.println("two");
				flagr = 1;

				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null)) {

					if (cout > 0) {
						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {

						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					}

				} else {

					chunk = new Phrase("00.00" + "", font8);
				}
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				logger.log(Level.SEVERE, "in product methos============18");
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0 && cstFlag == 0)) {

				System.out.println("three");
				if (products.get(i).getServiceTax() != null) {
					if (flag21 == 1 || cout == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else {
						chunk = new Phrase(df.format(products.get(i)
								.getServiceTax().getPercentage()), font8);
					}

				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& cstFlag == 1) {
				System.out.println("four");

				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				flagr = 1;

			} else if (cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {

				System.out.println("five");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {

				System.out.println("six");
				flagr = 1;
				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0) && (products
					.get(i).getVatTax().getPercentage() > 0))) {

				System.out.println("seven");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				logger.log(Level.SEVERE, "in product methos============19");
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& (cstFlag == 0)) {
				System.out.println("eight");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				} else if (st == 1 && st1 == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				}

				else {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage()), font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				System.out.println("service tax flagr:::::::::::::   " + flagr);

			} else {
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase("0.00 / 0.00", font8);
				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
			}

			// **************************************

			double totalVal = (products.get(i).getPrice() - taxVal)
					* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}
			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table.addCell(pdfcategcell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfmofcell);
			table.addCell(pdfspricecell);
			table.addCell(pdfperdiscount);
			table.addCell(discAmtCell);
			pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfservice1);
			logger.log(Level.SEVERE, "in product methos============10");
			table.addCell(pdftotalproduct);
			try {
				table.setWidths(columnWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			count = i;
			// /
			if (count == this.products.size() || count == firstBreakPoint) {
				rephrse = new Phrase(
						"Refer Annexure 1 for additional products ", font8);
				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();

			}
		}
		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.addElement(rephrse);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createProductDetailWithOutDisc() {

		logger.log(Level.SEVERE, "in product methos============1");

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		float[] columnWidthswith7 = { 0.7f, 3.5f, 0.7f, 0.9f, 0.7f, 0.7f, 0.9f };
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		table.setSpacingAfter(185f);

		try {
			table.setWidths(columnWidthswith7);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		logger.log(Level.SEVERE, "in product methos============12");
		Phrase category = new Phrase("SR. NO.", font1);
		Paragraph srno = new Paragraph();
		srno.add(category);

		Phrase product = new Phrase("ITEM DETAILS ", font1);
		Paragraph itemdetail = new Paragraph();
		itemdetail.add(product);

		Phrase qty = new Phrase("QTY.", font1);
		Paragraph quty = new Paragraph();
		quty.add(qty);

		Phrase rate = new Phrase("UNIT", font1);
		Paragraph unit = new Paragraph();
		unit.add(rate);

		Phrase servicetax = new Phrase("RATE", font1);
		Paragraph rate1 = new Paragraph();
		rate1.add(servicetax);

		// Phrase percdisc = new Phrase("DISC %",font1);
		// Paragraph disct= new Paragraph();
		// disct.add(percdisc);
		//
		// Phrase disc = new Phrase("DISC",font1);
		// Paragraph discCell= new Paragraph();
		// discCell.add(disc);

		Paragraph tax = new Paragraph();
		// *************chnges rohan on 22/4/2015********************
		Phrase svat = null;
		Phrase vvat = null;
		Phrase vat = null;
		int cout = 0;
		int flag21 = 0;
		int st = 0;
		int st1 = 0;

		logger.log(Level.SEVERE, "in product methos============13");

		for (int i = 0; i < this.products.size(); i++) {

			int cstFlag = 0;
			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {

					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
					}
				}
			}

			if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vvat = new Phrase("VAT / ST %", font1);
				cout = 1;
			} else if (products.get(i).getServiceTax().getPercentage() > 0
					&& products.get(i).getVatTax().getPercentage() == 0
					&& cstFlag == 0) {
				vat = new Phrase("ST %", font1);
				st = 1;
			} else if (products.get(i).getServiceTax().getPercentage() == 0
					&& products.get(i).getVatTax().getPercentage() > 0
					&& cstFlag == 0) {
				vat = new Phrase("VAT %", font1);
				st1 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {
				svat = new Phrase("CST / ST %", font1);
				flag21 = 1;
			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() == 0) {
				vat = new Phrase("CST %", font1);

			} else {
				vat = new Phrase("TAX %", font1);
			}
		}

		Phrase stvat = null;

		logger.log(Level.SEVERE, "in product methos============14");

		if (cout > 0) {
			tax.add(vvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else if (st == 1 && st1 == 1) {

			stvat = new Phrase("VAT / ST %", font1);
			tax.add(stvat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		else if (flag21 > 0) {
			tax.add(svat);
			tax.setAlignment(Element.ALIGN_CENTER);
		} else {
			tax.add(vat);
			tax.setAlignment(Element.ALIGN_CENTER);
		}

		// **************************************changes
		// ends....................

		Phrase total = new Phrase("AMOUNT", font1);
		Paragraph amount = new Paragraph();
		amount.add(total);
		amount.setAlignment(Element.ALIGN_CENTER);

		logger.log(Level.SEVERE, "in product methos============15");
		PdfPCell cellcategory = new PdfPCell(srno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproduct = new PdfPCell(itemdetail);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellqty = new PdfPCell(quty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(unit);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rate1);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell cellvat = new PdfPCell(disct);
		// cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
		// PdfPCell disAmtCell = new PdfPCell(discCell);
		// disAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellperdisc = new PdfPCell(tax);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(amount);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellcategory);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellrate);
		table.addCell(cellservicetax);
		// table.addCell(cellvat);
		// table.addCell(disAmtCell);
		table.addCell(cellperdisc);
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		if (this.products.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size();
			blankLines = size * (140 / 14);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 5f;
		}
		table.setSpacingAfter(blankLines);

		Phrase rephrse = null;

		int flagr = 0;
		int flagr1 = 0;
		for (int i = 0; i < this.products.size(); i++) {
			logger.log(Level.SEVERE, "in product methos============16");
			Phrase chunk = new Phrase((i + 1) + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase chunk1 = new Phrase(products.get(i).getUnitOfMeasurement(),
					font8);
			PdfPCell pdfmofcell = new PdfPCell(chunk1);
			pdfmofcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedRate = products.get(i).getPrice() - taxVal;
			chunk = new Phrase(df.format(calculatedRate), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			// if(products.get(i).getPercentageDiscount()!=0)
			// chunk = new
			// Phrase(products.get(i).getPercentageDiscount()+"",font8);
			// else
			// chunk = new Phrase("0"+"",font8);
			// PdfPCell pdfperdiscount = new PdfPCell(chunk);
			// pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			//
			// if(products.get(i).getDiscountAmt()!=0)
			// chunk = new Phrase(products.get(i).getDiscountAmt()+"",font8);
			// else
			// chunk = new Phrase("0"+"",font8);
			// PdfPCell discAmtCell = new PdfPCell(chunk);
			// discAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			logger.log(Level.SEVERE, "in product methos============17");

			PdfPCell pdfservice1 = null;
			// ******************************
			double cstval = 0;
			int cstFlag = 0;

			if (qp instanceof SalesQuotation) {
				SalesQuotation salesq = (SalesQuotation) qp;
				if (salesq.getCformstatus() != null) {
					if (salesq.getCformstatus().trim().equals(AppConstants.YES)
							|| salesq.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesq.getCstpercent();
					}
				}
			}

			if (qp instanceof SalesOrder) {
				SalesOrder salesorder = (SalesOrder) qp;
				if (salesorder.getCformstatus() != null) {
					if (salesorder.getCformstatus().trim()
							.equals(AppConstants.YES)
							|| salesorder.getCformstatus().trim()
									.equals(AppConstants.NO)) {
						cstFlag = 1;
						cstval = salesorder.getCstpercent();
					}
				}
			}

			System.out.println("one");

			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)
					&& (cstFlag == 0)) {

				System.out.println("two");
				flagr = 1;

				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null)) {

					if (cout > 0) {
						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {

						chunk = new Phrase(df.format(products.get(i)
								.getVatTax().getPercentage())
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					}

				} else {

					chunk = new Phrase("00.00" + "", font8);
				}
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				logger.log(Level.SEVERE, "in product methos============18");
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0 && cstFlag == 0)) {

				System.out.println("three");
				if (products.get(i).getServiceTax() != null) {
					if (flag21 == 1 || cout == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {
						chunk = new Phrase("0.00"
								+ " / "
								+ df.format(products.get(i).getServiceTax()
										.getPercentage()), font8);
					} else {
						chunk = new Phrase(df.format(products.get(i)
								.getServiceTax().getPercentage()), font8);
					}

				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& cstFlag == 1) {
				System.out.println("four");

				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				flagr = 1;

			} else if (cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {

				System.out.println("five");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if (cstFlag == 1
					&& products.get(i).getServiceTax().getPercentage() > 0) {

				System.out.println("six");
				flagr = 1;
				chunk = new Phrase(df.format(cstval)
						+ " / "
						+ df.format(products.get(i).getServiceTax()
								.getPercentage()), font8);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((cstFlag == 1
					&& (products.get(i).getServiceTax().getPercentage() == 0) && (products
					.get(i).getVatTax().getPercentage() > 0))) {

				System.out.println("seven");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(cstval) + " / " + "0.00",
							font8);
				} else {
					chunk = new Phrase(df.format(cstval), font8);
				}

				logger.log(Level.SEVERE, "in product methos============19");
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)
					&& (cstFlag == 0)) {
				System.out.println("eight");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				} else if (st == 1 && st1 == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage())
							+ " / " + "0.00", font8);
				}

				else {
					chunk = new Phrase(df.format(products.get(i).getVatTax()
							.getPercentage()), font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				System.out.println("service tax flagr:::::::::::::   " + flagr);

			} else {
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase("0.00 / 0.00", font8);
				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
			}

			// **************************************

			double totalVal = (products.get(i).getPrice() - taxVal)
					* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}
			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			table.addCell(pdfcategcell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfmofcell);
			table.addCell(pdfspricecell);
			// table.addCell(pdfperdiscount);
			// table.addCell(discAmtCell);
			pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfservice1);
			logger.log(Level.SEVERE, "in product methos============10");
			table.addCell(pdftotalproduct);
			try {
				table.setWidths(columnWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			count = i;
			// /
			if (count == this.products.size() || count == firstBreakPoint) {
				rephrse = new Phrase(
						"Refer Annexure 1 for additional products ", font8);
				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();

			}
		}
		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.addElement(rephrse);
		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/**
	 * rohan added this code for GSTsales oder and sales quotation On date :
	 * 19-07-2017
	 * 
	 * @param alue
	 * 
	 */

	// rohan added this code for GST PDF of contract and quotation

	private void createHeader(int value) {
		//Dev. By Jayshree
		//Date 20/11/2017
		//Des.to add the logo in Table
		DocumentUpload logodocument =comp.getLogo();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
//			image2.scalePercent(20f);
			image2.scalePercent(18f);

			
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(30);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		Image image1=null;
//		try
//		{
//		image1=Image.getInstance("images/ipclogo4.jpg ");//images/ipclogo4.jpg
//		image1.scalePercent(20f);
////		image1.setAbsolutePosition(40f,765f);	
////		doc.add(image1);
//		
//		
//		
//		
//		
//		imageSignCell=new PdfPCell();
//		imageSignCell.addElement(image1);
//		imageSignCell.setFixedHeight(30);
//		imageSignCell.setBorder(0);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}

		
		PdfPTable logoTab=new PdfPTable(1);
		logoTab.setWidthPercentage(100);
		
		if(imageSignCell!=null)
		{
			logoTab.addCell(imageSignCell);
		}
		else
		{
			Phrase logoblank=new Phrase(" ");
			PdfPCell logoblankcell=new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		//End By jayshree
		
		Phrase companyName = null;
		if (comp != null) {
			companyName = new Phrase(comp.getBusinessUnitName().trim(),
					font16boldul);
		}

		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(companyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyAddr = null;
		if (comp != null) {
			companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
					.trim(), font12);
		}
		Paragraph companyAddrpara = new Paragraph();
		companyAddrpara.add(companyAddr);
		companyAddrpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyAddrCell = new PdfPCell();
		companyAddrCell.addElement(companyAddrpara);
		companyAddrCell.setBorder(0);
		companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		

		/**
		 * Date By 16/3/2018
		 * By jayshree
		 * Des.to call the comp gstin from service gstin 
		 */
		
		Phrase companyGSTTIN = null;
		String gstinValue = "";
		
//		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
//					.equalsIgnoreCase("GSTIN")) {
//				gstinValue = comp.getArticleTypeDetails().get(i)
//						.getArticleTypeName()
//						+ " : "
//						+ comp.getArticleTypeDetails().get(i)
//								.getArticleTypeValue().trim();
//				break;
//			}
//		}

		ServerAppUtility serverApp = new ServerAppUtility();
		/*** GST Number will print if GST tax applicable or not applicable ***/
		if(gstNumberPrintFlag){
			
		String gstin="",gstinText="";
		if(qp instanceof SalesOrder){
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			logger.log(Level.SEVERE,"GST Applicable");
			SalesOrder salesorder=(SalesOrder) qp;
			gstin = serverApp.getGSTINOfCompany(comp,salesorder
						.getBranch().trim());
		} else {
			logger.log(Level.SEVERE,"GST Not Applicable");
			gstinText = comp.getCompanyGSTTypeText().trim();
		}
		}else{

			if (comp.getCompanyGSTType().trim()
					.equalsIgnoreCase("GST Applicable")) {
				logger.log(Level.SEVERE,"GST Applicable");
				SalesQuotation salesQuotation=(SalesQuotation) qp;
				gstin = serverApp.getGSTINOfCompany(comp, salesQuotation
							.getBranch().trim());
			} else {
				logger.log(Level.SEVERE,"GST Not Applicable");
				gstinText = comp.getCompanyGSTTypeText().trim();
			}
			
		}
		
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstinValue = gstinValue
						
						+ comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
						+ " : "
						+ comp.getArticleTypeDetails().get(i)
								.getArticleTypeValue().trim();
			}
		}

//		/**
//		 * Date 16/3/2018 
//		 * By Jayshree
//		 * Des.remove the gstin if tax are not present
//		 */
//		
//		if(qp instanceof SalesQuotation){
//			if(qp.getProductTaxes().size()==0){
//				gstin="";
//			}
//		}
//		else{
//			if(qp.getProductTaxes().size()==0)
//			{
//				gstin="";
//			}
//		}
		
		if(!gstin.trim().equals("")){
			logger.log(Level.SEVERE,"GST Present");
			companyGSTTIN= new Phrase("GSTIN" + " : " + gstin+""+gstinValue,
				font10bold);
		}else if (!gstinText.trim().equalsIgnoreCase("")) {
			logger.log(Level.SEVERE,"GST Not Present");
			companyGSTTIN= new Phrase(gstinText+""+gstinValue,font10bold);
		}else{
			logger.log(Level.SEVERE,"Nothing Present");
			companyGSTTIN= new Phrase(gstinValue,font10bold);
			
		}
		
//		End By Jayshree
		
	}
	/**
	 * ends here
	 */

//		if (!gstinValue.equals("")) {
//			companyGSTTIN = new Phrase(gstinValue, font12bold);
//		}

		/**
		 * Date 31-3-2018
		 * By jayshre
		 * Des.to print The branch mail 
		 */
		String branchmail = "";
		ServerAppUtility serverAppmail = new ServerAppUtility();

		if(qp instanceof SalesOrder){
		if (checkEmailId == true) {
			branchmail = serverAppmail.getBranchEmail(comp,
					soEntity.getBranch());
			System.out.println("server method " + branchmail);

		} else {
			branchmail = serverAppmail.getCompanyEmail(comp.getCompanyId());
			System.out.println("server method 22" + branchmail);
		}
		}
		else{

			if (checkEmailId == true) {
				branchmail = serverAppmail.getBranchEmail(comp,
						salesquotEntity.getBranch());
				System.out.println("server method " + branchmail);

			} else {
				branchmail = serverAppmail.getCompanyEmail(comp.getCompanyId());
				System.out.println("server method 22" + branchmail);
			}
			
			
		}
		String email = null;
		if (branchmail != null) {
			email = "Email : " + branchmail;
		} else {
			email = "Email : ";
		}

		
		String number = "";
		String landline = "";

		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			System.out.println("pn11");
			number = comp.getCountryCode()+comp.getCellNumber1() + "";
		}
		if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
			if (!number.trim().isEmpty()) {
				number = number + " , " + comp.getCountryCode()+comp.getCellNumber2() + "";
			} else {
				number = comp.getCountryCode()+comp.getCellNumber2() + "";
			}
			System.out.println("pn33" + number);
		}
		if (comp.getLandline() != 0 && comp.getLandline() != null) {
			if (!number.trim().isEmpty()) {
				number = number + " , " + comp.getStateCode()+comp.getLandline() + "";
			} else {
				number = comp.getStateCode()+comp.getLandline() + "";
			}
			System.out.println("pn44" + number);
		}

		/**
		 * Date 31-3-2018
		 * By jayshree
		 * Des.to print the head off.email
		 */
		String hoid=null;
		if(hoEmail==true){
			hoid="HO Email : "+comp.getEmail();
		}
		
		
		
		

		String website = "";
		if (comp.getWebsite() == null || comp.getWebsite().equals("")) {
			website = " ";
		} else {

			website = "Website : " + comp.getWebsite();
		}
		
		Phrase branchemail=new Phrase (email,font12);
		PdfPCell branchmailCell=new PdfPCell(branchemail);
		branchmailCell.setBorder(0);
		branchmailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase hoemail=new Phrase (hoid,font12);
		PdfPCell homailCell=new PdfPCell(hoemail);
		homailCell.setBorder(0);
		homailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase phoneph=new Phrase ("Phone :"+number,font12);
		PdfPCell phoneCell=new PdfPCell(phoneph);
		phoneCell.setBorder(0);
		phoneCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase webph=new Phrase (website,font12);
		PdfPCell webCell=new PdfPCell(webph);
		webCell.setBorder(0);
		webCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		
		Paragraph companyGSTTINpara = new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyGSTTINCell = new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pdfPTable = new PdfPTable(1);
		pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		/**
		 * Date 31-3-2018
		 * By jayshree
		 * Des.add cell in tab
		 */
		pdfPTable.addCell(branchmailCell);
		if(hoEmail==true){
		pdfPTable.addCell(homailCell);
		}
		pdfPTable.addCell(phoneCell);
		pdfPTable.addCell(webCell);
//		pdfPTable.addCell(companyGSTTINCell);

		
		
//		try {
//			document.add(pdfPTable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		/** Date 22/11/2017
		 * Dev.By jayshree
		 * Des.To add the logotab in pdf
		 */
		PdfPTable header=new PdfPTable(2);
		header.setWidthPercentage(100);
		
		try {
//			header.setWidths(new float[]{20,80});
			header.setWidths(new float[]{26,74});

		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		

		if(imageSignCell!=null){
		PdfPCell left=new PdfPCell(logoTab);
		left.setBorder(0);
		header.addCell(left);
		
		PdfPCell right=new PdfPCell(pdfPTable);
		right.setBorder(0);
		header.addCell(right);
		}
		else
		{
			PdfPCell right=new PdfPCell(pdfPTable);
			right.setBorder(0);
			right.setColspan(2);
			header.addCell(right);
		}
		try {
			document.add(header);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		//End By Jayshree
		
		/** date 09/11/2017 added by komal to remove 3 copies of quotation **/

//		// rohan added this code
//	float[] myWidth = { 3, 4, 3 };
//	PdfPTable mytbale = new PdfPTable(3);
//	mytbale.setWidthPercentage(100f);
//	mytbale.setSpacingAfter(5f);
//	mytbale.setSpacingBefore(5f);
//
//	try {
//		mytbale.setWidths(myWidth);
//	} catch (DocumentException e2) {
//		// TODO Auto-generated catch block
//		e2.printStackTrace();
//	}
//
//	Image uncheckedImg = null;
//	try {
//		uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
//	} catch (BadElementException | IOException e3) {
//		e3.printStackTrace();
//	}
//	uncheckedImg.scalePercent(9);
//
//	Image checkedImg = null;
//	try {
//		checkedImg = Image.getInstance("images/checked_checkbox.png");
//	} catch (BadElementException | IOException e3) {
//		e3.printStackTrace();
//	}
//	checkedImg.scalePercent(9);
//
//	Phrase myblank = new Phrase("   ", font10);
//	PdfPCell myblankCell = new PdfPCell(myblank);
//	// stat1PhraseCell.addElement(stat1Phrase);
//	myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//	Phrase myblankborderZero = new Phrase(" ", font10);
//	PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
//	// stat1PhraseCell.addElement(stat1Phrase);
//	myblankborderZeroCell.setBorder(0);
//	myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//	Phrase stat1Phrase = new Phrase("  Original for Receipient", font10);
//	Paragraph para1 = new Paragraph();
//	para1.setIndentationLeft(10f);
//	para1.add(myblank);
//	if (value == 0) {
//		para1.add(new Chunk(checkedImg, 0, 0, true));
//	} else {
//		para1.add(new Chunk(uncheckedImg, 0, 0, true));
//	}
//
//	para1.add(stat1Phrase);
//	para1.setAlignment(Element.ALIGN_MIDDLE);
//
//	PdfPCell stat1PhraseCell = new PdfPCell(para1);
//	stat1PhraseCell.setBorder(0);
//	stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//	Phrase stat2Phrase = new Phrase("  Duplicate for Supplier/Transporter",
//			font10);
//	Paragraph para2 = new Paragraph();
//	para2.setIndentationLeft(10f);
//
//	if (value == 1) {
//		para2.add(new Chunk(checkedImg, 0, 0, true));
//	} else {
//		para2.add(new Chunk(uncheckedImg, 0, 0, true));
//	}
//
//	para2.add(stat2Phrase);
//	para2.setAlignment(Element.ALIGN_CENTER);
//
//	PdfPCell stat2PhraseCell = new PdfPCell(para2);
//	stat2PhraseCell.setBorder(0);
//	stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//	Phrase stat3Phrase = new Phrase("  Triplicate for Supplier", font10);
//	Paragraph para3 = new Paragraph();
//	para3.setIndentationLeft(10f);
//
//	if (value == 2) {
//		para3.add(new Chunk(checkedImg, 0, 0, true));
//	} else {
//		para3.add(new Chunk(uncheckedImg, 0, 0, true));
//	}
//	para3.add(stat3Phrase);
//	para3.setAlignment(Element.ALIGN_JUSTIFIED);
//
//	PdfPCell stat3PhraseCell = new PdfPCell(para3);
//	stat3PhraseCell.setBorder(0);
//	stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//	// mytbale.addCell(myblankborderZeroCell);
//	// mytbale.addCell(myblankCell);
//	mytbale.addCell(stat1PhraseCell);
//	// mytbale.addCell(myblankborderZeroCell);
//	// mytbale.addCell(myblankCell);
//	mytbale.addCell(stat2PhraseCell);
//	// mytbale.addCell(myblankborderZeroCell);
//	// mytbale.addCell(myblankCell);
//	mytbale.addCell(stat3PhraseCell);
//	// mytbale.addCell(myblankborderZeroCell);
//
//	PdfPTable tab = new PdfPTable(1);
//	tab.setWidthPercentage(100f);
//
//	PdfPCell cell = new PdfPCell(mytbale);
//	tab.addCell(cell);
//	try {
//		document.add(tab);
//	} catch (DocumentException e1) {
//		e1.printStackTrace();
//	}
//		

		// ends here
		String titlepdf = "";

		if (qp instanceof SalesOrder) {
			titlepdf = "Sales Order";
		} else {
			titlepdf = "Quotation";
		}

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createFooterTaxPart(SalesQuotation salesQuotation) {
		SalesQuotation qp=salesQuotation;
		float[] columnMoreLeftWidths = { 2f, 1f };
		PdfPTable pdfPTaxTable = new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

//		/**
//		 * rohan added this code for payment terms for invoice details
//		 */
//		
//		
//		float[] column3widths = { 2f, 2f, 6f };
//		PdfPTable leftTable = new PdfPTable(3);
//		leftTable.setWidthPercentage(100);
//		try {
//			leftTable.setWidths(column3widths);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		
//		/** date 9/11/2017 added by komal from payment terms table header **/
//		//Table name 
//		Phrase paymentTerms = new Phrase("Payment Terms", font10bold);
//		PdfPCell paymentTermsCell = new PdfPCell(paymentTerms);
//		paymentTermsCell.setColspan(3);
//		paymentTermsCell.setBorder(0);
//		
//		leftTable.addCell(paymentTermsCell);
//		// heading
//
//		Phrase day = new Phrase("Day", font8bold);
//		PdfPCell dayCell = new PdfPCell(day);
//		dayCell.setBorder(0);
//
//		Phrase percent = new Phrase("Percent", font8bold);
//		PdfPCell percentCell = new PdfPCell(percent);
//		percentCell.setBorder(0);
//
//		Phrase comment = new Phrase("Comment", font8bold);
//		PdfPCell commentCell = new PdfPCell(comment);
//		commentCell.setBorder(0);
//
//		leftTable.addCell(dayCell);
//		leftTable.addCell(percentCell);
//		leftTable.addCell(commentCell);
//
//		// Values
//		/** date 9/11/2017 added by komal to add minimum 12 entries **/
//		int size = 12 - qp.getPaymentTermsList().size();
//		for (int i = 0; i < qp.getPaymentTermsList().size(); i++) {
//			Phrase dayValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermDays()
//					+ "", font8);
//			PdfPCell dayValueCell = new PdfPCell(dayValue);
//			dayValueCell.setBorder(0);
//			leftTable.addCell(dayValueCell);
//
//			Phrase percentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermPercent()
//					+ "", font8);
//			PdfPCell percentValueCell = new PdfPCell(percentValue);
//			percentValueCell.setBorder(0);
//			leftTable.addCell(percentValueCell);
//
//			Phrase commentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermComment(), font8);
//			PdfPCell commentValueCell = new PdfPCell(commentValue);
//			commentValueCell.setBorder(0);
//			leftTable.addCell(commentValueCell);
//		}
		
		// try {
		// document.add(leftTable);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
//komal
		Phrase blankval = new Phrase(" ", font8);
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase blank = new Phrase("", font8);
		PdfPCell bcell = new PdfPCell(blank);

		Phrase payphrase = new Phrase("Payment Terms", font10bold);
		payphrase.add(Chunk.NEWLINE);
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);

		PdfPCell tablecell1 = new PdfPCell();
		System.out.println("b4 table 11" + this.payTermsLis.size());
		if (this.payTermsLis.size() > 6) {
			System.out.println("af table 11" + this.payTermsLis.size());
			tablecell1 = new PdfPCell(table);
			// tablecell1.setBorder(0);
		} else {
			System.out.println(" table 11" + this.payTermsLis.size());
			tablecell1 = new PdfPCell(table);
			tablecell1.setBorder(0);
		}

		Phrase paytermdays = new Phrase("Days", font1);
		Phrase paytermpercent = new Phrase("Percent", font1);
		Phrase paytermcomment = new Phrase("Comment", font1);

		PdfPCell headingpayterms = new PdfPCell(payphrase);
		headingpayterms.setBorder(0);
		headingpayterms.setColspan(3);
		PdfPCell celldays = new PdfPCell(paytermdays);
		celldays.setBorder(0);
		celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellpercent = new PdfPCell(paytermpercent);
		cellpercent.setBorder(0);
		cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcomment = new PdfPCell(paytermcomment);
		cellcomment.setBorder(0);
		cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(celldays);
		table.addCell(cellpercent);
		table.addCell(cellcomment);

		if (this.payTermsLis.size() <= BreakPoint) {
			int size = BreakPoint - this.payTermsLis.size();
			blankLines = size * (60 / 6);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 0;
		}
		table.setSpacingAfter(blankLines);

		for (int i = 0; i < this.payTermsLis.size(); i++) {

			Phrase chunk = null;
		
			if (payTermsLis.get(i).getPayTermDays() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (payTermsLis.get(i).getPayTermPercent() != 0) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (payTermsLis.get(i).getPayTermComment() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
						.trim(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);

			count = i;
			if (count == 5 || count == BreakPoint) {

				flag1 = BreakPoint;
				break;
			}
		}

		// /2nd table for pay terms start
		System.out.println(this.payTermsLis.size() + "  out");
		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);

		PdfPCell table1cell = new PdfPCell();

		if (this.payTermsLis.size() > 6) {
			table1cell = new PdfPCell(table1);
			// table1cell.setBorder(0);
		} else {
			table1cell = new PdfPCell(blankval);
			table1cell.setBorder(0);
		}

		Phrase paytermdays1 = new Phrase("Days", font1);
		Phrase paytermpercent1 = new Phrase("Percent", font1);
		Phrase paytermcomment1 = new Phrase("Comment", font1);

		PdfPCell celldays1;
		PdfPCell cellpercent1;
		PdfPCell cellcomment1;

		System.out.println(this.payTermsLis.size() + " ...........b4 if");
		if (this.payTermsLis.size() > 5) {
			System.out.println(this.payTermsLis.size() + " ...........af if");
			celldays1 = new PdfPCell(paytermdays1);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			celldays1 = new PdfPCell(blankval);
			celldays1.setBorder(0);

		}

		if (this.payTermsLis.size() > 5) {

			cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellpercent1 = new PdfPCell(blankval);
			cellpercent1.setBorder(0);

		}

		if (this.payTermsLis.size() > 5) {
			cellcomment1 = new PdfPCell(paytermcomment1);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellcomment1 = new PdfPCell(blankval);
			cellcomment1.setBorder(0);
		}
		table1.addCell(celldays1);
		table1.addCell(cellpercent1);
		table1.addCell(cellcomment1);

		for (int i = 6; i < this.payTermsLis.size(); i++) {
			System.out.println(this.payTermsLis.size() + "  in for");

			Phrase chunk = null;
			if (payTermsLis.get(i).getPayTermDays() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (payTermsLis.get(i).getPayTermPercent() != 0) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
						font8);
			} else {
				chunk = new Phrase(" ", font8);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (payTermsLis.get(i).getPayTermComment() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
						.trim(), font8);
			} else {
				chunk = new Phrase(" ", font8);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table1.addCell(pdfdayscell);
			table1.addCell(pdfpercentcell);
			table1.addCell(pdfcommentcell);

			if (i == 11) {

				break;
			}

		}

		// /2nd table for pay terms end

		PdfPTable termstable1 = new PdfPTable(2);
		termstable1.setWidthPercentage(100);
		termstable1.addCell(headingpayterms);
		termstable1.addCell(tablecell1);

		termstable1.addCell(table1cell);


		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		/**
		 * Date 23/3/2018
		 * By jayshree
		 * Des.add the other charges
		 */
		createOtherCharges();
		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(new float[]{0.9f, 1f, 0.4f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				System.out.println("inside sale quotation");
				Phrase chargename=new Phrase("Charge Name",font6bold);
				PdfPCell chargenameCell=new PdfPCell(chargename);
				chargenameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(chargenameCell);
				
				Phrase taxname=new Phrase("Tax",font6bold);
				PdfPCell taxnameCell=new PdfPCell(taxname);
				taxnameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(taxnameCell);
				
				Phrase amtph=new Phrase("Amt",font6bold);
				PdfPCell amtCell=new PdfPCell(amtph);
				amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(amtCell);
				
				for (int i = 0; i < qp.getOtherCharges().size(); i++) {
				
					System.out.println("inside so for....");
					Phrase chargenameval=new Phrase(qp.getOtherCharges().get(i).getOtherChargeName(),font6);
					PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
					chargenamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(chargenamevalCell);
					
					String taxNames = " ";
					if (qp.getOtherCharges().get(i).getTax1()
							.getPercentage() != 0
							&& qp.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
						taxNames = qp.getOtherCharges().get(i).getTax1()
								.getTaxConfigName()
								+ "/"
								+ qp.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
					} else {
						if (qp.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax1()
									.getTaxConfigName();
						} else if (qp.getOtherCharges().get(i).getTax2()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax2()
									.getTaxConfigName();
						} else {
							taxNames = " ";
						}
					}
					
					Phrase taxnameval=new Phrase(taxNames,font6);
					PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
					taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(taxnamevalCell);
					
					Phrase amtvalph=new Phrase(qp.getOtherCharges().get(i).getAmount()+"",font6);
					PdfPCell amtvalCell=new PdfPCell(amtvalph);
					amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(amtvalCell);
					
					}
				
			
		PdfPCell upperRightCell = new PdfPCell(otherCharges);
		upperRightCell.setBorder(0);
		
//		if(qp.getOtherCharges().size()!=0){
//		System.out.println("soEntity.getOtherCharges().size()"+qp.getOtherCharges().size());
////		rightTable.addCell(upperRightCell);
//		}
//			
		//End By Jayshree
		
		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.3f };
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.addElement(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax", font6bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		// totalAmount
		Phrase amtB4TaxValphrase = new Phrase("", font6bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < qp.getProductTaxes().size(); i++) {
			if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			}
		}

		Phrase CGSTphrase = new Phrase("CGST", font6bold);
		PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
		CGSTphraseCell.setBorder(0);
		// CGSTphraseCell.addElement(CGSTphrase);

		Phrase CGSTValphrase = new Phrase(df.format(cgstTotalVal) + "", font7);
		// Paragraph CGSTValphrasePara=new Paragraph();
		// CGSTValphrasePara.add(CGSTValphrase);
		// CGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
		CGSTValphraseCell.setBorder(0);
		CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// CGSTValphraseCell.addElement(CGSTValphrasePara);

		Phrase SGSTphrase = new Phrase("SGST", font6bold);
		PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
		SGSTphraseCell.setBorder(0);
		// SGSTphraseCell.addElement(SGSTphrase);

		Phrase SGSTValphrase = new Phrase(df.format(sgstTotalVal) + "", font7);
		// Paragraph SGSTValphrasePara=new Paragraph();
		// SGSTValphrasePara.add(SGSTValphrase);
		// SGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
		SGSTValphraseCell.setBorder(0);
		// SGSTValphraseCell.addElement(SGSTValphrasePara);
		SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase IGSTphrase = new Phrase("IGST", font6bold);
		PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
		IGSTphraseCell.setBorder(0);
		// IGSTphraseCell.addElement(IGSTphrase);

		Phrase IGSTValphrase = new Phrase(df.format(igstTotalVal) + "", font7);
		// Paragraph IGSTValphrasePara=new Paragraph();
		// IGSTValphrasePara.add(IGSTValphrase);
		// IGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
		IGSTValphraseCell.setBorder(0);
		IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// IGSTValphraseCell.addElement(IGSTValphrasePara);

		Phrase GSTphrase = new Phrase("Total GST", font6bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		// GSTphraseCell.addElement(GSTphrase);

		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
				font6bold);
		// Paragraph GSTValphrasePara=new Paragraph();
		// GSTValphrasePara.add(GSTValphrase);
		// GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// GSTValphraseCell.addElement(GSTValphrasePara);

		rightInnerTable.addCell(CGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(CGSTValphraseCell);
		rightInnerTable.addCell(SGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(SGSTValphraseCell);
		rightInnerTable.addCell(IGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(IGSTValphraseCell);
		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
//		innerRightCell.addElement(rightInnerTable);

		rightTable.addCell(innerRightCell);
		
		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
       

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);
//		rightCell.addElement(rightTable);

		PdfPCell leftCell = new PdfPCell(termstable1);
		// leftCell.setBorder(0);
//		leftCell.addElement(termstable1);

		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createFooterLastPart(String preprintStatus) {
		PdfPTable bottomTable = new PdfPTable(2);
		bottomTable.setWidthPercentage(100);
		float[] columnMoreLeftWidths = { 2f, 1f };
		try {
			bottomTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		String amtInWordsVal = "";
		String comment = "";
		double netpayable = 0;
		if (qp instanceof SalesOrder) {
			SalesOrder soEntity = (SalesOrder) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(soEntity.getNetpayable());
			netpayable = soEntity.getNetpayable();
			if (soEntity.getDescription() != null) {
				comment = soEntity.getDescription();
			}
		} else {
			SalesQuotation quotation = (SalesQuotation) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(quotation.getNetpayable());
			netpayable = quotation.getNetpayable();
			if (quotation.getDescription() != null) {
				comment = quotation.getDescription();
			}
		}

		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font6bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtInWordsValCell.setBorderWidthTop(0);
		amtInWordsValCell.setBorderWidthLeft(0);
		amtInWordsValCell.setBorderWidthRight(0);
		leftTable.addCell(amtInWordsValCell);

		// rohan added this code for valid until

		String titleterms = "";
		Phrase validDate = new Phrase("", font8);
		PdfPCell validitydatecell = new PdfPCell();
		System.out.println("salequotaion out");
		if (qp instanceof SalesQuotation) {
			System.out.println("salequotaion in");

			validDate = new Phrase("", font8);

			SalesQuotation salesquot = (SalesQuotation) qp;
			if (salesquot.getValidUntill() != null) {
				titleterms = "Terms And Conditions";
				Phrase validityDate = new Phrase("Valid Until: "
						+ fmt.format(salesquot.getValidUntill()) + "",
						font10bold);
				validitydatecell.addElement(validityDate);
				validitydatecell.setBorder(0);
			} else {
				validitydatecell.addElement(validDate);
				validitydatecell.setBorder(0);
			}

		}
		leftTable.addCell(validitydatecell);

		// ends here

		Phrase termNcond = new Phrase("Terms and Conditions:1", font6bold);
		PdfPCell termNcondCell = new PdfPCell();
		termNcondCell.setBorder(0);
		termNcondCell.addElement(termNcond);

		Phrase termNcondVal = new Phrase(comment, font6bold);
		PdfPCell termNcondValCell = new PdfPCell();
		termNcondValCell.setBorder(0);
		termNcondValCell.addElement(termNcondVal);

		leftTable.addCell(termNcondCell);
		leftTable.addCell(termNcondValCell);

		// rohan added this code for universal pest

		if (!preprintStatus.equalsIgnoreCase("Plane")) {
			
			/**
			 * Date 16/3/2018
			 * By jayshree
			 * Des.to add the comp gstin from serverapp utility changes are done
			 */
			
			ServerAppUtility serverApp = new ServerAppUtility();

			String gstin="",gstinText="";
			if(qp instanceof SalesOrder){
			if (comp.getCompanyGSTType().trim()
					.equalsIgnoreCase("GST Applicable")) {
				logger.log(Level.SEVERE,"GST Applicable");
				gstin = serverApp.getGSTINOfCompany(comp, soEntity
							.getBranch().trim());
			} else {
				logger.log(Level.SEVERE,"GST Not Applicable");
				gstinText = comp.getCompanyGSTTypeText().trim();
			}
			}
			else{

				if (comp.getCompanyGSTType().trim()
						.equalsIgnoreCase("GST Applicable")) {
					logger.log(Level.SEVERE,"GST Applicable");
					gstin = serverApp.getGSTINOfCompany(comp, salesquotEntity
								.getBranch().trim());
				} else {
					logger.log(Level.SEVERE,"GST Not Applicable");
					gstinText = comp.getCompanyGSTTypeText().trim();
				}
				
				
			}
			
			/**
			 * Date 16/3/2018
			 * By Jayshree
			 * Des.to remove gstin if tax are not present
			 */
			
				if(qp.getProductTaxes().size()==0){
					gstin="";
				}
				//End
				Phrase gstinph=null ;
				if(!gstin.trim().equals("")){
					logger.log(Level.SEVERE,"GST Present");
					gstinph= new Phrase("GSTIN" + " : " + gstin,
						font6bold);
				}else if (!gstinText.trim().equalsIgnoreCase("")) {
					logger.log(Level.SEVERE,"GST Not Present");
					gstinph= new Phrase(gstinText,font6bold);
				}else{
					logger.log(Level.SEVERE,"Nothing Present");
					gstinph= new Phrase("",font6bold);
					
				}
				PdfPCell gstinCell = new PdfPCell();
				gstinCell.setBorder(0);
				gstinCell.addElement(gstinph);
				leftTable.addCell(gstinCell);
				//End By jayshree
			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

				if (comp.getArticleTypeDetails().get(i).getArticlePrint()
						.equalsIgnoreCase("Yes")) {

					Phrase articalType = new Phrase(comp
							.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue(), font6bold);
					PdfPCell articalTypeCell = new PdfPCell();
					articalTypeCell.setBorder(0);
					articalTypeCell.addElement(articalType);
					leftTable.addCell(articalTypeCell);
				}
			}
		}
		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font6bold);
		PdfPCell blankCell = new PdfPCell();
		blankCell.setBorder(0);
		blankCell.addElement(blank);

		Phrase netPay = new Phrase("Net Payable", font6bold);

		PdfPCell netPayCell = new PdfPCell();
		netPayCell.setBorder(0);
		netPayCell.addElement(netPay);

		Phrase netPayVal = new Phrase(netpayable + "", font6bold);
		// Paragraph netPayPara=new Paragraph();
		// netPayPara.add(netPayVal);
		// netPayPara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorder(0);
		// netPayValCell.addElement(netPayPara);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase gstReverseCharge = new Phrase("GST Payable on Reverse Charge",
				font6bold);
		PdfPCell gstReverseChargeCell = new PdfPCell();
		gstReverseChargeCell.setBorder(0);
		gstReverseChargeCell.addElement(gstReverseCharge);

		Phrase gstReverseChargeVal = new Phrase(" ", font7);
		PdfPCell gstReverseChargeValCell = new PdfPCell();
		gstReverseChargeValCell.setBorder(0);
		gstReverseChargeValCell.addElement(gstReverseChargeVal);

		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);
		innerRightTable.addCell(blankCell);
		innerRightTable.addCell(blankCell);
		innerRightTable.addCell(blankCell);
		innerRightTable.addCell(gstReverseChargeCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(gstReverseChargeValCell);

		PdfPCell rightUpperCell = new PdfPCell();
		rightUpperCell.addElement(innerRightTable);
		rightUpperCell.setBorderWidthLeft(0);
		rightUpperCell.setBorderWidthRight(0);
		rightUpperCell.setBorderWidthTop(0);

		// rohan added this code for universal pest and printing multiple
		// company name

//		String companyname = "";
//		if (multipleCompanyName) {
//			if (qp.getGroup() != null && !qp.getGroup().equals("")) {
//				companyname = qp.getGroup().trim().toUpperCase();
//			} else {
//				companyname = comp.getBusinessUnitName().trim().toUpperCase();
//			}
//
//		} else { 
//			
//			//companyname = comp.getBusinessUnitName().trim().toUpperCase();
//			
//			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
//				companyname="For "
//						+  branchDt.getCorrespondenceName();
//			}else{
//			 companyname = "For "
//					+ comp.getBusinessUnitName().trim().toUpperCase();
//			}
//			
//			
//			
//		}
		
		String companyname = "";
	    if (multipleCompanyName) {
	      if ((qp.getGroup() != null) && (!qp.getGroup().equals(""))) {
	        companyname = qp.getGroup().trim().toUpperCase();
	      } else {
	        companyname = comp.getBusinessUnitName().trim().toUpperCase();
	      }
	    }
	    else if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname="For "+branchDt.getCorrespondenceName();
			logger.log(Level.SEVERE,"correspondanceb name 222 "+companyname);
		}
	    else {
	      companyname ="For "+comp.getBusinessUnitName().trim().toUpperCase();
	      logger.log(Level.SEVERE,"company Name "+companyname);
	    }
	    
	    

		// ends here

		rightTable.addCell(rightUpperCell);
		//Phrase companyPhrase = new Phrase("For , " + companyname, font6bold);
		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font6bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companyParaCell = new PdfPCell();
		companyParaCell.addElement(companynamepara);
		companyParaCell.setBorder(0);

		rightTable.addCell(companyParaCell);
		rightTable.addCell(blankCell);
		rightTable.addCell(blankCell);
		rightTable.addCell(blankCell);
		Phrase signAuth = new Phrase("Authorised Signatory", font6bold);
		Paragraph signPara = new Paragraph();
		signPara.add(signAuth);
		signPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell signParaCell = new PdfPCell();
		signParaCell.addElement(signPara);
		signParaCell.setBorder(0);
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell();
		lefttableCell.addElement(leftTable);
		PdfPCell righttableCell = new PdfPCell();
		righttableCell.addElement(rightTable);

		bottomTable.addCell(lefttableCell);
		bottomTable.addCell(righttableCell);

		//

		Paragraph para = new Paragraph(
				"Note : This is computer generated hence no signature required.",
				font8);

		//
		try {
			document.add(bottomTable);
			document.add(para);
			
//			Next Page for rest product continously
					} catch (DocumentException e) {
			e.printStackTrace();
		}
//		if (soEntity.getItems().size() > 5) {
			
			if (qp instanceof SalesOrder) {
				SalesOrder soEntity = (SalesOrder) qp;
				if (soEntity.getItems().size() > 5) {
					Paragraph para1 = new Paragraph("Annexure 1 :", font10bold);
					para1.setAlignment(Element.ALIGN_LEFT);

					try {
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEXTPAGE);
						document.add(para1);
						document.add(Chunk.NEWLINE);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					
//					createProductDetails();//comment by jayshree
					createProductDetails2();
					createAnnexureForRemainingSalesOrderProduct(5);
				}
			} else {
				SalesQuotation quotation = (SalesQuotation) qp;
				if (quotation.getItems().size() > noOfLines) {
					Paragraph para1 = new Paragraph("Annexure 1 :", font10bold);
					para1.setAlignment(Element.ALIGN_LEFT);

					try {
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEXTPAGE);
						document.add(para1);
						document.add(Chunk.NEWLINE);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
//					createProductDetails();//Comment by jayshree
					createProductDetails2();
					createAnnexureForRemainingQuoatationProduct(5);
				}
			}
			
//		}
	}

	private void createAnnexureForRemainingSalesOrderProduct(int count) {
		// TODO Auto-generated method stub
		SalesOrder soEntity = (SalesOrder) qp;
		for (int i = productCount; i < soEntity.getItems().size(); i++) {
			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font8);
			PdfPCell srNoCell = new PdfPCell();
			// srNoCell.setBorderWidthBottom(0);
			// srNoCell.setBorderWidthTop(0);
			srNoCell.addElement(srNo);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(soEntity.getItems().get(i)
					.getProductName().trim(), font8);
			PdfPCell serviceNameCell = new PdfPCell();
			// serviceNameCell.setBorder(0);
			// serviceNameCell.setBorderWidthBottom(0);
			// serviceNameCell.setBorderWidthTop(0);
			serviceNameCell.addElement(serviceName);
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;

			if (soEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
				hsnCode = new Phrase(soEntity.getItems().get(i).getPrduct()
						.getHsnNumber().trim(), font8);
			} else {
				hsnCode = new Phrase("", font8);
			}
			PdfPCell hsnCodeCell = new PdfPCell();
			// hsnCodeCell.setBorder(0);
			// hsnCodeCell.setBorderWidthBottom(0);
			// hsnCodeCell.setBorderWidthTop(0);
			hsnCodeCell.addElement(hsnCode);
			productTable.addCell(hsnCodeCell);

			Phrase uom = new Phrase(soEntity.getItems().get(i)
					.getUnitOfMeasurement().trim(), font8);
			PdfPCell uomCell = new PdfPCell();
			// uomCell.setBorder(0);
			// uomCell.setBorderWidthBottom(0);
			// uomCell.setBorderWidthTop(0);
			uomCell.addElement(uom);
			productTable.addCell(uomCell);

			Phrase qty = new Phrase(soEntity.getItems().get(i).getQty() + "",
					font8);
			PdfPCell qtyCell = new PdfPCell();
			// qtyCell.setBorder(0);
			// qtyCell.setBorderWidthBottom(0);
			// qtyCell.setBorderWidthTop(0);
			qtyCell.addElement(qty);
			productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(soEntity.getItems().get(i)
					.getPrice())
					+ "", font8);
			PdfPCell rateCell = new PdfPCell();
			// rateCell.setBorder(0);
			// rateCell.setBorderWidthBottom(0);
			// rateCell.setBorderWidthTop(0);
			rateCell.addElement(rate);
			productTable.addCell(rateCell);

			double amountValue = soEntity.getItems().get(i).getPrice()
					* soEntity.getItems().get(i).getQty();

			totalAmount = totalAmount + amountValue;
			Phrase amount = new Phrase(df.format(amountValue) + "", font8);
			PdfPCell amountCell = new PdfPCell();
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.addElement(amount);
			productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(soEntity.getItems().get(i)
					.getDiscountAmt())
					+ "", font8);
			PdfPCell discCell = new PdfPCell();
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.addElement(disc);
			productTable.addCell(discCell);

			double asstotalAmount1 = getAssessTotalAmount(soEntity
					.getItems().get(i));
			System.out.println("asstotalAmount1:::::"+asstotalAmount1);
			Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
			PdfPCell taxableValueCell = new PdfPCell();
			// taxableValueCell.setBorder(0);
			// taxableValueCell.setBorderWidthBottom(0);
			// taxableValueCell.setBorderWidthTop(0);
			taxableValueCell.addElement(taxableValue);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;
			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			boolean taxPresent = validateTaxes(soEntity.getItems().get(i));
			if (taxPresent) {
				if (soEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("IGST")) {
					double asstotalAmount = getAssessTotalAmount(soEntity
							.getItems().get(i));

					double taxAmount = getTaxAmount(asstotalAmount, soEntity
							.getItems().get(i).getVatTax().getPercentage());
					double indivTotalAmount = asstotalAmount + taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(soEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setBorder(0);
					totalCell.addElement(totalPhrase);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

					try {
						document.add(productTable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}

				} else if (soEntity.getItems().get(i).getServiceTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {
					double asstotalAmount = getAssessTotalAmount(soEntity
							.getItems().get(i));

					double taxAmount = getTaxAmount(asstotalAmount, soEntity
							.getItems().get(i).getServiceTax().getPercentage());
					double indivTotalAmount = asstotalAmount + taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(soEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setBorder(0);
					totalCell.addElement(totalPhrase);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

					try {
						document.add(productTable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}

				} else {

					if (soEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {
						double asstotalAmount = getAssessTotalAmount(soEntity
								.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount,
								soEntity.getItems().get(i).getVatTax()
										.getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(soEntity.getItems().get(i)
								.getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount,
								soEntity.getItems().get(i).getServiceTax()
										.getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
//						sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(soEntity.getItems().get(i)
								.getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount + ctaxValue
								+ staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

						try {
							document.add(productTable);
						} catch (DocumentException e) {
							e.printStackTrace();
						}
					} else if (soEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("SGST")) {
						double asstotalAmount = getAssessTotalAmount(soEntity
								.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount,
								soEntity.getItems().get(i).getServiceTax()
										.getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(soEntity.getItems().get(i)
								.getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount,
								soEntity.getItems().get(i).getVatTax()
										.getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(soEntity.getItems().get(i)
								.getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount + ctaxValue
								+ staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

						try {
							document.add(productTable);
						} catch (DocumentException e) {
							e.printStackTrace();
						}
					}
				}
			} else {

				logger.log(Level.SEVERE, "Inside Tax Not Applicable");

				PdfPCell cell = new PdfPCell(new Phrase("-", font8));
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				Phrase totalPhrase = new Phrase(
						df.format(getAssessTotalAmount(soEntity.getItems().get(
								i)))
								+ "", font8);
				PdfPCell totalCell = new PdfPCell();
				// totalCell.setColspan(16);
				// totalCell.setBorder(0);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				totalCell.addElement(totalPhrase);
				productTable.addCell(totalCell);

				String premisesVal = "";
				if (soEntity.getItems().get(i).getPremisesDetails() != null) {
					premisesVal = soEntity.getItems().get(i)
							.getPremisesDetails();
				}

				// if (printPremiseDetails) {
				// Phrase premisesValPhrs = new Phrase("Premise Details : "
				// + premisesVal, font8);
				// PdfPCell premiseCell = new PdfPCell();
				// premiseCell.setColspan(16);
				// premiseCell.addElement(premisesValPhrs);
				// productTable.addCell(premiseCell);
				// }
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					e.printStackTrace();
				}

			}

		}
	}

	private void createAnnexureForRemainingQuoatationProduct(int count) {
		// TODO Auto-generated method stub
		SalesQuotation quotationEntity = (SalesQuotation) qp;
		for (int i = productCount; i < quotationEntity.getItems().size(); i++) {
			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font8);
			PdfPCell srNoCell = new PdfPCell();
			// srNoCell.setBorderWidthBottom(0);
			// srNoCell.setBorderWidthTop(0);
			srNoCell.addElement(srNo);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(quotationEntity.getItems().get(i)
					.getProductName().trim(), font8);
			PdfPCell serviceNameCell = new PdfPCell();
			// serviceNameCell.setBorder(0);
			// serviceNameCell.setBorderWidthBottom(0);
			// serviceNameCell.setBorderWidthTop(0);
			serviceNameCell.addElement(serviceName);
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;

			if (quotationEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
				hsnCode = new Phrase(quotationEntity.getItems().get(i)
						.getPrduct().getHsnNumber().trim(), font8);
			} else {
				hsnCode = new Phrase("", font8);
			}
			PdfPCell hsnCodeCell = new PdfPCell();
			// hsnCodeCell.setBorder(0);
			// hsnCodeCell.setBorderWidthBottom(0);
			// hsnCodeCell.setBorderWidthTop(0);
			hsnCodeCell.addElement(hsnCode);
			productTable.addCell(hsnCodeCell);

			Phrase uom = new Phrase(quotationEntity.getItems().get(i)
					.getUnitOfMeasurement().trim(), font8);
			PdfPCell uomCell = new PdfPCell();
			// uomCell.setBorder(0);
			// uomCell.setBorderWidthBottom(0);
			// uomCell.setBorderWidthTop(0);
			uomCell.addElement(uom);
			productTable.addCell(uomCell);

			Phrase qty = new Phrase(quotationEntity.getItems().get(i).getQty()
					+ "", font8);
			PdfPCell qtyCell = new PdfPCell();
			// qtyCell.setBorder(0);
			// qtyCell.setBorderWidthBottom(0);
			// qtyCell.setBorderWidthTop(0);
			qtyCell.addElement(qty);
			productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(quotationEntity.getItems()
					.get(i).getPrice())
					+ "", font8);
			PdfPCell rateCell = new PdfPCell();
			// rateCell.setBorder(0);
			// rateCell.setBorderWidthBottom(0);
			// rateCell.setBorderWidthTop(0);
			rateCell.addElement(rate);
			productTable.addCell(rateCell);

			double amountValue = quotationEntity.getItems().get(i).getPrice()
					* quotationEntity.getItems().get(i).getQty();

			totalAmount = totalAmount + amountValue;
			Phrase amount = new Phrase(df.format(amountValue) + "", font8);
			PdfPCell amountCell = new PdfPCell();
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.addElement(amount);
			productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(quotationEntity.getItems()
					.get(i).getDiscountAmt())
					+ "", font8);
			PdfPCell discCell = new PdfPCell();
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.addElement(disc);
			productTable.addCell(discCell);

			Phrase taxableValue = new Phrase(df.format(quotationEntity
					.getItems().get(i).getPrice())
					+ "", font8);
			PdfPCell taxableValueCell = new PdfPCell();
			// taxableValueCell.setBorder(0);
			// taxableValueCell.setBorderWidthBottom(0);
			// taxableValueCell.setBorderWidthTop(0);
			taxableValueCell.addElement(taxableValue);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;
			boolean taxPresent = validateTaxes(quotationEntity.getItems()
					.get(i));
			if (taxPresent) {
				if (quotationEntity.getItems().get(i).getVatTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					double taxAmount = getTaxAmount(quotationEntity.getItems()
							.get(i).getPrice(),
							quotationEntity.getItems().get(i).getVatTax()
									.getPercentage());
					double indivTotalAmount = quotationEntity.getItems().get(i)
							.getPrice()
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(quotationEntity.getItems()
							.get(i).getVatTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setBorder(0);
					totalCell.addElement(totalPhrase);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}

				} else if (quotationEntity.getItems().get(i).getServiceTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")) {

					double asstotalAmount = getAssessTotalAmount(quotationEntity
							.getItems().get(i));
					double taxAmount = getTaxAmount(asstotalAmount,
							quotationEntity.getItems().get(i).getServiceTax()
									.getPercentage());
					double indivTotalAmount = asstotalAmount + taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell();
					// igstRateValCell.setBorder(0);
					igstRateValCell.addElement(igstRateVal);

					Phrase igstRate = new Phrase(quotationEntity.getItems()
							.get(i).getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell();
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.addElement(igstRate);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setBorder(0);
					totalCell.addElement(totalPhrase);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}

				} else {

					if (quotationEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {
						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i).getVatTax()
										.getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(quotationEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(quotationEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount + ctaxValue
								+ staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);

						try {
							document.add(productTable);
						} catch (DocumentException e) {
							e.printStackTrace();
						}

					} else if (quotationEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("SGST")) {
						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i)
										.getServiceTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell();
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell.addElement(cgstRateVal);

						Phrase cgstRate = new Phrase(quotationEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell();
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.addElement(cgstRate);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i).getVatTax()
										.getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell();
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell.addElement(sgstRateVal);

						Phrase sgstRate = new Phrase(quotationEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell();
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.addElement(sgstRate);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount + ctaxValue
								+ staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();

						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.addElement(totalPhrase);

						productTable.addCell(totalCell);
						try {
							document.add(productTable);
						} catch (DocumentException e) {
							e.printStackTrace();
						}

					}
				}
			} else {
				logger.log(Level.SEVERE, "Inside Tax Not Applicable");

				PdfPCell cell = new PdfPCell(new Phrase("-", font8));
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(cell);
				Phrase totalPhrase = new Phrase(
						df.format(getAssessTotalAmount(quotationEntity
								.getItems().get(i))) + "", font8);
				PdfPCell totalCell = new PdfPCell();
				// totalCell.setColspan(16);
				// totalCell.setBorder(0);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				totalCell.addElement(totalPhrase);
				productTable.addCell(totalCell);

				String premisesVal = "";
				if (quotationEntity.getItems().get(i).getPremisesDetails() != null) {
					premisesVal = quotationEntity.getItems().get(i)
							.getPremisesDetails();
				}
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}

		}
	}

	private void createCustomerDetails() {
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		float[] columnHalfWidth = { 1f, 1f };
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font6bold);
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			// if(cust.isCompany()==true){
			//
			//
			// tosir="M/S";
			// }
			// else{
			// tosir="";
			// }

			/***
			 * Dev.By Jayshree 
			 * Date 20/11/2017 
			 * Des.add salutation to customer name
			 */
			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S "+" " + cust.getCompanyName().trim();
				System.out.println("hi vijay ==="+custName);
			} else if(cust.getSalutation()!=null)
			{
				custName = cust.getSalutation()+" "+cust.getFullname().trim();
			}
			else
			{
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here
		System.out.println("fullname =="+fullname);
		Phrase nameCellVal = new Phrase(fullname + "       Mob : "
				+ cust.getCellNumber1(), font7);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase address = new Phrase("Address", font6bold);
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase addressVal = new Phrase(cust.getAdress().getCompleteAddress()
				.trim(), font7);
		PdfPCell addressValCell = new PdfPCell(addressVal);
		// addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String gstTinStr = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstTinStr = cust.getArticleTypeDetails().get(i)
						.getArticleTypeValue().trim();
			}
		}

			/**
			 * Date 16/3/2018 
			 * By Jayshree
			 * Des.remove the gstin if tax are not present
			 */
			
			if(qp instanceof SalesQuotation){
				if(qp.getProductTaxes().size()==0){
					gstTinStr="";
				}
			}
			else{
				if(qp.getProductTaxes().size()==0)
				{
					gstTinStr="";
				}
			}
			//End By Jayshree
		Phrase gstTin = new Phrase("GSTIN", font6bold);
		PdfPCell gstTinCell = new PdfPCell(gstTin);
		// gstTinCell.addElement(gstTin);
		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTinVal = new Phrase(gstTinStr, font7);
		PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
		// gstTinValCell.addElement(gstTinVal);
		gstTinValCell.setBorder(0);
		gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);
		colonTable.addCell(gstTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(gstTinValCell);

		PdfPCell cell1 = new PdfPCell();
		cell1.setBorder(0);
		cell1.addElement(colonTable);

		Phrase state = new Phrase("State", font6bold);
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(cust.getAdress().getState().trim(), font7);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase stateCode = new Phrase("State Code", font6bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(stCo, font7);
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		// stateCodeValCell.addElement(stateCodeVal);
		// stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);

		PdfPCell colonTableState = new PdfPCell();
		colonTableState.setBorder(0);
		colonTableState.addElement(colonTable);
		statetable.addCell(colonTableState);

		colonTable = new PdfPTable(4);
		colonTable.setWidthPercentage(100);
		float[] columnStateCodeCollonWidth = { 35, 5, 15, 45 };
		try {
			colonTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase blak = new Phrase(" ", font8);
		PdfPCell blakCell = new PdfPCell(blak);
		blakCell.setBorder(0);

		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		colonTable.addCell(blakCell);

		PdfPCell colonTableValState = new PdfPCell();
		colonTableValState.setBorder(0);
		colonTableValState.addElement(colonTable);
		statetable.addCell(colonTableValState);

		PdfPCell stateTableCell = new PdfPCell(statetable);
		stateTableCell.setBorder(0);
		stateTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// stateTableCell.addElement(statetable);

		part1Table.addCell(cell1);
		part1Table.addCell(stateTableCell);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		/* Ends Part 1 */

		/* Part 2 Start */
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font6bold);
		PdfPCell name2Cell = new PdfPCell();
		name2Cell.addElement(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name2CellVal = new Phrase(fullname + "       Mob : "
				+ cust.getCellNumber1(), font7);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase address2 = new Phrase("Address", font6bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase address2Val = new Phrase(cust.getSecondaryAdress()
				.getCompleteAddress().trim(), font7);
		PdfPCell address2ValCell = new PdfPCell(address2Val);
		// address2ValCell.addElement(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String gstTin2Str = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstTin2Str = cust.getArticleTypeDetails().get(i)
						.getArticleTypeName().trim();
			}
		}

	
		/**
		 * Date 16/3/2018 
		 * By Jayshree
		 * Des.remove the gstin if tax are not present
		 */
		
		if(qp instanceof SalesQuotation){
			if(qp.getProductTaxes().size()==0){
				gstTin2Str="";
			}
		}
		else{
			if(qp.getProductTaxes().size()==0)
			{
				gstTin2Str="";
			}
		}
		//End By Jayshree
		
		Phrase gstTin2 = new Phrase("GSTIN", font6bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		// gstTin2Cell.addElement(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font7);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		// gstTin2ValCell.addElement(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);
		colonTable.addCell(gstTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(gstTinValCell);

		PdfPCell cell2 = new PdfPCell();
		cell2.setBorder(0);
		cell2.addElement(colonTable);

		Phrase state2 = new Phrase("State", font6bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		// state2Cell.addElement(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2Val = new Phrase(cust.getSecondaryAdress().getState()
				.trim(), font7);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// state2ValCell.addElement(state2Val);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stsecCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList
					.get(i)
					.getStateName()
					.trim()
					.equalsIgnoreCase(
							cust.getSecondaryAdress().getState().trim())) {
				stsecCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font6bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		// state2CodeCell.addElement(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(stsecCo, font7);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		// state2CodeValCell.addElement(state2CodeVal);
		// state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable state2table = new PdfPTable(2);
		state2table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);
		PdfPCell state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		colonTable = new PdfPTable(4);
		colonTable.setWidthPercentage(100);
		// float[] columnStateCodeCollonWidth = {35,5,15,45};
		try {
			colonTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2CodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2CodeValCell);
		colonTable.addCell(blakCell);

		state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		PdfPCell state2TableCell = new PdfPCell(state2table);
		state2TableCell.setBorder(0);
		// state2TableCell.addElement(state2table);

		part2Table.addCell(cell2);
		part2Table.addCell(state2TableCell);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);
		/* Part 2 Ends */

		Phrase blankCell = new Phrase(" ", font7);
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createProductDetails() {
		PdfPTable productTable = new PdfPTable(16);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column16CollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase srNophrase = new Phrase("Sr No", font8bold);
		PdfPCell srNoCell = new PdfPCell();
		srNoCell.addElement(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

		Phrase servicePhrase = new Phrase("Particulars", font8bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2

		Phrase hsnCode = new Phrase("HSN", font8bold);
		PdfPCell hsnCodeCell = new PdfPCell();
		hsnCodeCell.addElement(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 3

		Phrase UOMphrase = new Phrase("UOM", font8bold);
		PdfPCell UOMphraseCell = new PdfPCell();
		UOMphraseCell.addElement(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 4

		Phrase qtyPhrase = new Phrase("Qty", font8bold);
		PdfPCell qtyPhraseCell = new PdfPCell();
		qtyPhraseCell.addElement(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 5

		Phrase ratePhrase = new Phrase("Rate", font8bold);
		PdfPCell ratePhraseCell = new PdfPCell();
		ratePhraseCell.addElement(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 6

		Phrase amountPhrase = new Phrase("Amount", font8bold);
		PdfPCell amountPhraseCell = new PdfPCell();
		amountPhraseCell.addElement(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 7

		Phrase dicphrase = new Phrase("Disc", font8bold);
		PdfPCell dicphraseCell = new PdfPCell();
		dicphraseCell.addElement(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 8

		Phrase taxValPhrase = new Phrase("Ass Val", font8bold);
		PdfPCell taxValPhraseCell = new PdfPCell();
		taxValPhraseCell.addElement(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 9

		// PdfPTable cgstcellTable=new PdfPTable(1);
		// cgstcellTable.setWidthPercentage(100);

		Phrase cgstphrase = new Phrase("CGST", font8bold);
		PdfPCell cgstphraseCell = new PdfPCell(cgstphrase);
		// cgstphraseCell.addElement(cgstphrase);
		// cgstphraseCell.setBorder(0);
		cgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// cgstcellTable.addCell(cgstphraseCell);
		cgstphraseCell.setColspan(2);
		// cgstphraseCell.setRowspan(2);

		Phrase sgstphrase = new Phrase("SGST", font8bold);
		PdfPCell sgstphraseCell = new PdfPCell(sgstphrase);
		// sgstphraseCell.setBorder(0);
		sgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// sgstphraseCell.addElement(sgstphrase);
		// sgstcellTable.addCell(sgstphraseCell);
		sgstphraseCell.setColspan(2);
		// sgstphraseCell.setRowspan(2);

		Phrase igstphrase = new Phrase("IGST", font8bold);
		PdfPCell igstphraseCell = new PdfPCell(igstphrase);
		// igstphraseCell.setBorder(0);
		igstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// igstphraseCell.addElement(igstphrase);
		// igstcellTable.addCell(igstphraseCell);
		igstphraseCell.setColspan(2);
		// igstphraseCell.setRowspan(2);

		Phrase totalPhrase = new Phrase("Total", font8bold);
		PdfPCell totalPhraseCell = new PdfPCell();
		totalPhraseCell.addElement(totalPhrase);
		totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		totalPhraseCell.setRowspan(2);// 2

		Phrase cgstpercentphrase = new Phrase("%", font6bold);
		PdfPCell cgstpercentphraseCell = new PdfPCell();
		// cgstpercentphraseCell.setBorderWidthBottom(0);
		// cgstpercentphraseCell.setBorderWidthTop(0);
		// cgstpercentphraseCell.setBorderWidthLeft(0);
		cgstpercentphraseCell.addElement(cgstpercentphrase);
		// innerCgstTable.addCell(cgstpercentphraseCell);

		Phrase cgstamtphrase = new Phrase("Amt", font6bold);
		PdfPCell cgstamtphraseCell = new PdfPCell();
		cgstamtphraseCell.addElement(cgstamtphrase);
		//

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(hsnCodeCell);
		productTable.addCell(UOMphraseCell);
		productTable.addCell(qtyPhraseCell);
		productTable.addCell(ratePhraseCell);
		productTable.addCell(amountPhraseCell);
		productTable.addCell(dicphraseCell);
		productTable.addCell(taxValPhraseCell);

		productTable.addCell(cgstphraseCell);
		productTable.addCell(sgstphraseCell);
		productTable.addCell(igstphraseCell);

		productTable.addCell(totalPhraseCell);

		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createInvoiceDetails() {

		float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		float[] columnHalfWidth = { 1f, 1f };
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = { 3.5f, 0.2f, 6.8f };
		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseCharge = new Phrase("Reverse Charge", font6bold);
		PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
		// reverseChargeCell.addElement(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseChargeVal = new Phrase(" NO ", font7);
		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		// reverseChargeValCell.addElement(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String oderid = "";
		String oderDate = "";
		Date soDt = null;
		if (qp instanceof SalesOrder) {
			SalesOrder soEntity = (SalesOrder) qp;
			oderid = "Oder Id";
			oderDate = "Oder Date";
			soDt = soEntity.getSalesOrderDate();
		} else {
			SalesQuotation quotation = (SalesQuotation) qp;
			oderid = "Quotation Id";
			oderDate = "Quotation Date";
			soDt = quotation.getQuotationDate();
		}

		Phrase invoiceNo = new Phrase(oderid, font6bold);
		PdfPCell invoiceNoCell = new PdfPCell(invoiceNo);
		// invoiceNoCell.addElement(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNoVal = new Phrase(qp.getCount() + "", font7);
		PdfPCell invoiceNoValCell = new PdfPCell(invoiceNoVal);
		// invoiceNoValCell.addElement(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceDate = new Phrase(oderDate, font6bold);
		PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		// invoiceDateCell.addElement(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// sdf.format(invoiceentity.getInvoiceDate())

		Phrase invoiceDateVal = new Phrase(fmt.format(soDt), font7);
		PdfPCell invoiceDateValCell = new PdfPCell(invoiceDateVal);
		// invoiceDateValCell.addElement(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceNoValCell);
		colonTable.addCell(invoiceDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceDateValCell);

		PdfPCell pdfCell = new PdfPCell();
		pdfCell.setBorder(0);
		pdfCell.addElement(colonTable);

		part1Table.addCell(pdfCell);

		Phrase state = new Phrase("State", font6bold);
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(), font7);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCode = new Phrase("State Code", font6bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(" ", font7);
		PdfPCell stateCodeValCell = new PdfPCell();
		stateCodeValCell.addElement(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);

		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);

		PdfPCell stateTableCell = new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		Phrase contractId = new Phrase("", font6bold);
		PdfPCell contractIdCell = new PdfPCell(contractId);
		// contractIdCell.addElement(contractId);
		contractIdCell.setBorder(0);
		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// " "+invoiceentity.getContractCount()+
		Phrase contractIdVal = new Phrase("", font7);
		PdfPCell contractIdValCell = new PdfPCell(contractIdVal);
		// contractIdValCell.addElement(contractIdVal);
		contractIdValCell.setBorder(0);
		contractIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase startDate = new Phrase("", font6bold);
		PdfPCell startDateCell = new PdfPCell(startDate);
		// startDateCell.addElement(startDate);
		startDateCell.setBorder(0);
		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// sdf.format(q.getContractStartDate())
		Phrase startDateVal = new Phrase("", font7);
		PdfPCell startDateValCell = new PdfPCell(startDateVal);
		// startDateValCell.addElement(startDateVal);
		startDateValCell.setBorder(0);
		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDate = new Phrase("", font6bold);
		PdfPCell endDateCell = new PdfPCell(endDate);
		// endDateCell.addElement(endDate);
		endDateCell.setBorder(0);
		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// sdf.format(invoiceentity.getContractEndDate())
		Phrase endDateVal = new Phrase("", font7);
		PdfPCell endDateValCell = new PdfPCell(endDateVal);
		// endDateValCell.addElement(endDateVal);
		endDateValCell.setBorder(0);
		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
	 * 
	 */

		// Phrase periodstartDate=new Phrase("Billing From",font10bold);
		// PdfPCell periodstartDateCell=new PdfPCell(periodstartDate);
		// periodstartDateCell.addElement(periodstartDate);
		// periodstartDateCell.setBorder(0);
		// periodstartDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// // rohan adde this code
		// Phrase periodstartDateVal=null;
		// if(invoiceentity.getBillingPeroidFromDate()!= null){
		// periodstartDateVal=new
		// Phrase(sdf.format(invoiceentity.getBillingPeroidFromDate()),font10);
		// }
		// else
		// {
		// periodstartDateVal=new Phrase(" ",font10);
		// }

		// PdfPCell periodstartDateValCell=new PdfPCell(periodstartDateVal);
		// // periodstartDateValCell.addElement(periodstartDateVal);
		// periodstartDateValCell.setBorder(0);
		// periodstartDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase periodendDate=new Phrase("To",font10bold);
		// PdfPCell periodendDateCell=new PdfPCell(periodendDate);
		// // periodendDateCell.addElement(periodendDate);
		// periodendDateCell.setBorder(0);
		// periodendDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// // rohan adde this code
		// Phrase periodendDateVal=null;
		// if(invoiceentity.getBillingPeroidToDate()!= null){
		// periodendDateVal=new
		// Phrase(sdf.format(invoiceentity.getBillingPeroidToDate()),font10);
		// }
		// else
		// {
		// periodendDateVal=new Phrase(" ",font10);
		// }
		//
		//
		// PdfPCell periodendDateValCell=new PdfPCell(periodendDateVal);
		// // periodendDateValCell.addElement(periodendDateVal);
		// periodendDateValCell.setBorder(0);
		// periodendDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable billperiodtable = new PdfPTable(2);
		billperiodtable.setWidthPercentage(100);

		PdfPTable billFromcolonTable = new PdfPTable(3);
		billFromcolonTable.setWidthPercentage(100);
		try {
			billFromcolonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// billFromcolonTable.addCell(periodstartDateCell);
		// billFromcolonTable.addCell(colonCell);
		// billFromcolonTable.addCell(periodstartDateValCell);

		PdfPTable billTocolonTable = new PdfPTable(3);
		billTocolonTable.setWidthPercentage(100);
		try {
			billTocolonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// billTocolonTable.addCell(periodendDateCell);
		// billTocolonTable.addCell(colonCell);
		// billTocolonTable.addCell(periodendDateValCell);

		PdfPCell fromDate = new PdfPCell(billFromcolonTable);
		fromDate.setBorder(0);
		// fromDate.addElement(billFromcolonTable);

		billperiodtable.addCell(fromDate);
		fromDate = new PdfPCell();
		fromDate.setBorder(0);
		fromDate.addElement(billTocolonTable);
		billperiodtable.addCell(fromDate);
		/**
	 * 
	 */
		PdfPTable periodtable = new PdfPTable(2);
		periodtable.setWidthPercentage(100);
		float[] columnrohanrrCollonWidth = { 2.8f, 0.2f, 7.7f };
		PdfPTable concolonTable = new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// concolonTable.addCell(contractIdCell);
		// concolonTable.addCell(colonCell);
		// concolonTable.addCell(contractIdValCell);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// colonTable.addCell(startDateCell);
		// colonTable.addCell(colonCell);
		// colonTable.addCell(startDateValCell);

		PdfPCell startcolonTableCell = new PdfPCell(colonTable);
		startcolonTableCell.setBorder(0);
		// startcolonTableCell.addElement(colonTable);
		periodtable.addCell(startcolonTableCell);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// colonTable.addCell(endDateCell);
		// colonTable.addCell(colonCell);
		// colonTable.addCell(endDateValCell);

		PdfPCell endcolonTableCell = new PdfPCell();
		endcolonTableCell.setBorder(0);
		endcolonTableCell.addElement(colonTable);
		periodtable.addCell(endcolonTableCell);

		PdfPCell periodTableCell = new PdfPCell();
		periodTableCell.setBorder(0);
		periodTableCell.addElement(periodtable);

		PdfPCell concolonTableCell = new PdfPCell();
		concolonTableCell.setBorder(0);
		concolonTableCell.addElement(concolonTable);

		PdfPCell billperiodtableCell = new PdfPCell();
		billperiodtableCell.setBorder(0);
		billperiodtableCell.addElement(billperiodtable);

		part2Table.addCell(concolonTableCell);
		part2Table.addCell(periodTableCell);
		part2Table.addCell(billperiodtableCell);

		PdfPCell part1Cell = new PdfPCell();
		part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part1Table);

		mainTable.addCell(part1Cell);

		part1Cell = new PdfPCell();
		// part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part2Table);
		mainTable.addCell(part1Cell);

		// mainTable.addCell(blankCell);
		// mainTable.addCell(blankCell);
		Phrase billingAddress = new Phrase("Bill to Party", font8bold);
		// Paragraph billingpara=new Paragraph();
		// billingpara.add(billingAddress);
		// billingpara.setAlignment(Element.ALIGN_CENTER);
		// billingpara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		// billAdressCell.addElement(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);

		mainTable.addCell(billAdressCell);
		Phrase serviceaddress = new Phrase("Ship to Party", font8bold);
		// Paragraph servicepara=new Paragraph();
		// servicepara.add(serviceaddress);
		// servicepara.setAlignment(Element.ALIGN_CENTER);
		// servicepara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		// serviceCell.addElement(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		mainTable.addCell(serviceCell);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createBlankforUPC(int i) {

		Image uncheckedImg = null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(9);

		Image checkedImg = null;
		try {
			checkedImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		checkedImg.scalePercent(9);

		// Phrase phrAlp=new Phrase("  ALP",font9);
		// Paragraph paraAlp=new Paragraph();
		// paraAlp.setIndentationLeft(10f);
		// paraAlp.add(new Chunk(uncheckedImg, 0, 0, true));
		// paraAlp.add(phrAlp);

		// rohan added this code
		float[] myWidth = { 3, 4, 3 };

		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setSpacingAfter(5f);
		mytbale.setWidthPercentage(100f);
		try {
			mytbale.setWidths(myWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase myblank = new Phrase("   ", font10);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero = new Phrase(" ", font10);
		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat1Phrase = new Phrase("  Original for Receipient", font10);
		Paragraph para1 = new Paragraph();
		para1.setIndentationLeft(10f);
		para1.add(myblank);
		if (i == 0) {
			para1.add(new Chunk(checkedImg, 0, 0, true));
		} else {
			para1.add(new Chunk(uncheckedImg, 0, 0, true));
		}

		para1.add(stat1Phrase);
		para1.setAlignment(Element.ALIGN_MIDDLE);

		PdfPCell stat1PhraseCell = new PdfPCell(para1);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat2Phrase = new Phrase("  Duplicate for Supplier/Transporter",
				font10);
		Paragraph para2 = new Paragraph();
		para2.setIndentationLeft(10f);

		if (i == 1) {
			para2.add(new Chunk(checkedImg, 0, 0, true));
		} else {
			para2.add(new Chunk(uncheckedImg, 0, 0, true));
		}

		para2.add(stat2Phrase);
		para2.setAlignment(Element.ALIGN_CENTER);

		PdfPCell stat2PhraseCell = new PdfPCell(para2);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat3Phrase = new Phrase("  Triplicate for Supplier", font10);
		Paragraph para3 = new Paragraph();
		para3.setIndentationLeft(10f);

		if (i == 2) {
			para3.add(new Chunk(checkedImg, 0, 0, true));
		} else {
			para3.add(new Chunk(uncheckedImg, 0, 0, true));
		}
		para3.add(stat3Phrase);
		para3.setAlignment(Element.ALIGN_JUSTIFIED);

		PdfPCell stat3PhraseCell = new PdfPCell(para3);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// mytbale.addCell(myblankborderZero);
		// mytbale.addCell(myblankborderZero);
		mytbale.addCell(stat1PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		mytbale.addCell(stat2PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		mytbale.addCell(stat3PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);

		String title = "";
		if (qp instanceof SalesOrder)
			title = "Sales Order";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof SalesOrder) {
			SalesOrder conEntity = (SalesOrder) qp;
			conEnt = conEntity.getSalesOrderDate();
		} else {
			SalesQuotation quotEnt = (SalesQuotation) qp;
			conEnt = quotEnt.getQuotationDate();
		}

		String countinfo = "ID : " + qp.getCount() + "";

		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.setBorderWidthBottom(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14bold);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.setBorderWidthBottom(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14bold);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.setBorderWidthBottom(0);
		creationcell.addElement(Chunk.NEWLINE);

		// *********rohan make changes here for contract start and end date
		// ***********8
		PdfPCell conStartAndEndDateCell = null;
		PdfPCell blankCell = null;
		PdfPCell blankCell1 = null;

		// **********************************ends here
		// *******************************
		float[] columnWidthsForSTandEndDate = { 4f, 3f, 4f };
		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);

		try {
			titlepdftable.setWidths(columnWidthsForSTandEndDate);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		// *********rohan make changes here for contract start and end date
		// ***********8

		// **********************************ends here
		// *******************************
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		//
		// PdfPTable parent=new PdfPTable(1);
		// parent.setWidthPercentage(100);
		//
		// PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		// parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			/** date 09/11/2017 commented by komal to remove 3 checkboxxes **/
			//document.add(mytbale);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/** date 10/11/2017 added by komal for noOfLines count **/
//	int noOfLines = 10;
//	int productCount = 0;
	private void createProductDetailsVal() {
		/** date 10/11/2017 added by komal for noOfLines count **/
		int productNameLength = 14;
		if (qp instanceof SalesOrder) {

			SalesOrder salesOderEntity = (SalesOrder) qp;
			System.out.println("Inside  Condn of Instance con and quot ");
			int firstBreakPoint = 5;
			float blankLines = 0;
			/** date 10/11/2017 commented by komal for noOfLines count **/
//			if (salesOderEntity.getItems().size() <= firstBreakPoint) {
//
//				int size = firstBreakPoint - salesOderEntity.getItems().size();
//				blankLines = size * (100 / 5);
//				System.out.println("blankLines size =" + blankLines);
//			} else {
//				blankLines = 10f;
//			}
			/** date 10/11/2017 added by komal for vercetile changes**/
			if(salesOderEntity.getItems().size() <= noOfLines){
				blankLines = noOfLines - salesOderEntity.getItems().size() + 1f;
			}else{
				blankLines = 1f;
			}
			
			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < salesOderEntity.getItems().size(); i++) {

//				if (i == 5) {
//					break;
//				}
				
				
				/** date 10/11/2017 added by komal for vercetile changes**/
					if(noOfLines == 0){
						if(i+1 == salesOderEntity.getItems().size()){
							productCount = i;
							break;
						}
					}
					

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(salesOderEntity.getItems()
						.get(i).getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell();
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.addElement(serviceName);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (salesOderEntity.getItems().get(i).getPrduct()
						.getHsnNumber() != null) {
					hsnCode = new Phrase(salesOderEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell();
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.addElement(hsnCode);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(salesOderEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell();
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
				uomCell.addElement(uom);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(salesOderEntity.getItems().get(i)
						.getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell();
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
				qtyCell.addElement(qty);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(salesOderEntity.getItems()
						.get(i).getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell();
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
				rateCell.addElement(rate);
				productTable.addCell(rateCell);

				double amountValue = salesOderEntity.getItems().get(i)
						.getPrice()
						* salesOderEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
				productTable.addCell(amountCell);

				Phrase disc = new Phrase(df.format(salesOderEntity.getItems()
						.get(i).getDiscountAmt())
						+ "", font8);
				PdfPCell discCell = new PdfPCell();
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
				discCell.addElement(disc);
				productTable.addCell(discCell);
				double asstotalAmount1 = getAssessTotalAmount(soEntity
						.getItems().get(i));
				System.out.println("asstotalAmount1:::::"+asstotalAmount1);
				Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
				PdfPCell taxableValueCell = new PdfPCell();
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
				taxableValueCell.addElement(taxableValue);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				boolean taxPresent = validateTaxes(salesOderEntity.getItems()
						.get(i));
				if (taxPresent) {
					if (salesOderEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("IGST")) {
						double asstotalAmount = getAssessTotalAmount(salesOderEntity
								.getItems().get(i));

						double taxAmount = getTaxAmount(asstotalAmount,
								salesOderEntity.getItems().get(i).getVatTax()
										.getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell();
						// igstRateValCell.setBorder(0);
						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(salesOderEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell();
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);
						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);
						productTable.addCell(cell);

						productTable.addCell(igstRateCell);
						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						totalCell.addElement(totalPhrase);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else if (salesOderEntity.getItems().get(i)
							.getServiceTax().getTaxPrintName()
							.equalsIgnoreCase("IGST")) {
						double asstotalAmount = getAssessTotalAmount(salesOderEntity
								.getItems().get(i));

						double taxAmount = getTaxAmount(asstotalAmount,
								salesOderEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell();
						// igstRateValCell.setBorder(0);
						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(salesOderEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell();
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);
						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);
						productTable.addCell(cell);

						productTable.addCell(igstRateCell);
						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						totalCell.addElement(totalPhrase);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else {

						if (salesOderEntity.getItems().get(i).getVatTax()
								.getTaxPrintName().equalsIgnoreCase("CGST")) {
							double asstotalAmount = getAssessTotalAmount(salesOderEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getVatTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell();
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell();
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);
							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getServiceTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell();
//							sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell();
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);
							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell();
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
							totalCell.addElement(totalPhrase);

							productTable.addCell(totalCell);

						} else if (salesOderEntity.getItems().get(i)
								.getVatTax().getTaxPrintName()
								.equalsIgnoreCase("SGST")) {
							double asstotalAmount = getAssessTotalAmount(salesOderEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getServiceTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell();
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell();
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);
							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getVatTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell();
							// sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell();
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);
							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell();
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
							totalCell.addElement(totalPhrase);

							productTable.addCell(totalCell);

						}
					}
				} else {

					logger.log(Level.SEVERE, "Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(
							df.format(getAssessTotalAmount(salesOderEntity
									.getItems().get(i))) + "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if (salesOderEntity.getItems().get(i).getPremisesDetails() != null) {
						premisesVal = salesOderEntity.getItems().get(i)
								.getPremisesDetails();
					}

					// if (printPremiseDetails) {
					// Phrase premisesValPhrs = new Phrase("Premise Details : "
					// + premisesVal, font8);
					// PdfPCell premiseCell = new PdfPCell();
					// premiseCell.setColspan(16);
					// premiseCell.addElement(premisesValPhrs);
					// productTable.addCell(premiseCell);
					// }

				}
				/** date 10/11/2017 added by komal for vercetile chnages **/
				  noOfLines -= 1;
					 if(salesOderEntity.getItems().get(i).getProductName().length() > productNameLength){
						 noOfLines -= 1;
					 }
			}

			PdfPCell productTableCell = null;
			if (salesOderEntity.getItems().size() > productCount && productCount !=0 ) {
				Phrase my = new Phrase("Please Refer Annexure For More Details");
				productTableCell = new PdfPCell(my);

			} else {
				Phrase ph = new Phrase(" ", font8);
				productTableCell = new PdfPCell(ph);
			}

			// PdfPCell productTableCell = new PdfPCell(blankCell);
			// productTableCell.setBorderWidthBottom(0);
			// productTableCell.setBorderWidthTop(0);
			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			tab.setSpacingBefore(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// //////// End Here

		} else {

			SalesQuotation quotationEntity = (SalesQuotation) qp;

			System.out.println("Else Condn of Instance con and quot ");
			// Ajinkya Start Here
			int firstBreakPoint = 10;
			float blankLines = 0;
			/** date 10/11/2017 commented by komal for vercetile changes**/
//			if (quotationEntity.getItems().size() <= firstBreakPoint) {
//				int size = firstBreakPoint - quotationEntity.getItems().size();
//				
//				//blankLines = size * (100 / 5);
//				System.out.println("blankLines size =" + blankLines);
//			} else {
//				//blankLines = 10f;
//			}
			/** date 10/11/2017 added by komal for vercetile changes**/
			if(quotationEntity.getItems().size() <= noOfLines){
				blankLines = noOfLines - quotationEntity.getItems().size() + 1f;
			}else{
				blankLines = 1f;
			}
			
			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			/** date 10/11/2017 added by komal for vercetile changes**/
			for (int i = 0; i < quotationEntity.getItems().size(); i++) {
				if(noOfLines == 0){
					if(i+1 == quotationEntity.getItems().size()){
						productCount = i;
						break;
					}
				}

				/** date 10/11/2017 commented by komal for vercetile changes**/     
//				if (i == 10) {
//					break;
//				}

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(quotationEntity.getItems()
						.get(i).getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell();
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.addElement(serviceName);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (quotationEntity.getItems().get(i).getPrduct()
						.getHsnNumber() != null) {
					hsnCode = new Phrase(quotationEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell();
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.addElement(hsnCode);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(quotationEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell();
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
				uomCell.addElement(uom);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(quotationEntity.getItems().get(i)
						.getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell();
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
				qtyCell.addElement(qty);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(quotationEntity.getItems()
						.get(i).getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell();
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
				rateCell.addElement(rate);
				productTable.addCell(rateCell);

				double amountValue = quotationEntity.getItems().get(i)
						.getPrice()
						* quotationEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
				productTable.addCell(amountCell);

				/**
				 * Date 8/3/2018 
				 * By jayshree
				 * Des.to print the discount
				 */
				
				boolean isAreaPresent=false;
				double areval=0;
				double totaldis=0;
				double desper=quotationEntity.getItems().get(i).getPercentageDiscount();
				try{
				 areval=Double.parseDouble(quotationEntity.getItems().get(i).getArea());
				 isAreaPresent=true;
				}
				catch(Exception e){
					isAreaPresent=false;
				}
				
				if(isAreaPresent){
					totaldis=(quotationEntity.getItems().get(i).getQty()*quotationEntity.getItems().get(i).getPrice()*areval*desper)/100
							+quotationEntity.getItems().get(i).getDiscountAmt();
				}
				else{
					totaldis=(quotationEntity.getItems().get(i).getQty()*quotationEntity.getItems().get(i).getPrice()*desper)/100+
							quotationEntity.getItems().get(i).getDiscountAmt();
				}
				
				Phrase disc = new Phrase(df.format(totaldis)
						+ "", font8);
				
				//End By jayshree
				PdfPCell discCell = new PdfPCell();
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
				discCell.addElement(disc);
				productTable.addCell(discCell);


				double asstotalAmount1 = getAssessTotalAmount(quotationEntity
						.getItems().get(i));
				System.out.println("asstotalAmount1:::::"+asstotalAmount1);
				Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
				PdfPCell taxableValueCell = new PdfPCell();
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
				taxableValueCell.addElement(taxableValue);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				boolean taxPresent = validateTaxes(quotationEntity.getItems()
						.get(i));
				if (taxPresent) {
					if (quotationEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("IGST")) {

						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						System.out.println("asstotalAmount:::::"+asstotalAmount);
						
						double taxAmount = getTaxAmount(asstotalAmount, quotationEntity
								.getItems().get(i).getVatTax().getPercentage());
						double indivTotalAmount = asstotalAmount
								+ taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell();
						// igstRateValCell.setBorder(0);
						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(quotationEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell();
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);
						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);
						productTable.addCell(cell);

						productTable.addCell(igstRateCell);
						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						totalCell.addElement(totalPhrase);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else if (quotationEntity.getItems().get(i)
							.getServiceTax().getTaxPrintName()
							.equalsIgnoreCase("IGST")) {

						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						double taxAmount = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell();
						// igstRateValCell.setBorder(0);
						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(quotationEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell();
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);
						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);
						productTable.addCell(cell);

						productTable.addCell(igstRateCell);
						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell();
						// totalCell.setBorder(0);
						totalCell.addElement(totalPhrase);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else {

						if (quotationEntity.getItems().get(i).getVatTax()
								.getTaxPrintName().equalsIgnoreCase("CGST")) {
							double asstotalAmount = getAssessTotalAmount(quotationEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getVatTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell();
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(quotationEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell();
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);
							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getServiceTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell();
							sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(quotationEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell();
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);
							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell();
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
							totalCell.addElement(totalPhrase);

							productTable.addCell(totalCell);

							// try {
							// document.add(productTable);
							// } catch (DocumentException e) {
							// e.printStackTrace();
							// }

						} else if (quotationEntity.getItems().get(i)
								.getVatTax().getTaxPrintName()
								.equalsIgnoreCase("SGST")) {
							double asstotalAmount = getAssessTotalAmount(quotationEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getServiceTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell();
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(quotationEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell();
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);
							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getVatTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell();
							// sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(quotationEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell();
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);
							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell();

							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
							totalCell.addElement(totalPhrase);

							productTable.addCell(totalCell);

						}
					}
				} else {
					logger.log(Level.SEVERE, "Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(
							df.format(getAssessTotalAmount(quotationEntity
									.getItems().get(i))) + "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if (quotationEntity.getItems().get(i).getPremisesDetails() != null) {
						premisesVal = quotationEntity.getItems().get(i)
								.getPremisesDetails();
					}
				}
				/** date 10/11/2017 added by komal for vercetile chnages **/
				  noOfLines -= 1;
					 if(quotationEntity.getItems().get(i).getProductName().length() > productNameLength){
						 noOfLines -= 1;
					 }
			}
		
			System.out.println("quotationEntity.getItems()"
					+ quotationEntity.getItems().size());
			PdfPCell productTableCell = null;
			/** date 10/11/2017 added by komal for vercetile changes **/
			if (quotationEntity.getItems().size() > productCount && productCount !=0) {
				Phrase my = new Phrase("Please Refer Annexure For More Details");
				productTableCell = new PdfPCell(my);

			} else {
				Phrase ph = new Phrase(" ", font8);
				productTableCell = new PdfPCell(ph);
			}

			// PdfPCell productTableCell = new PdfPCell(blankCell);
			// productTableCell.setBorderWidthBottom(0);
			// productTableCell.setBorderWidthTop(0);
			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			tab.setSpacingBefore(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// ENd Here

		}
	}

	private double getAssessTotalAmount(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		double totalAmount = 0;

		System.out.println("Inside getAssessTotalAmount");
		
		if (salesLineItem.getArea().trim().equalsIgnoreCase("NA")
				|| salesLineItem.getArea().equalsIgnoreCase("")) {
			System.out.println("Area Not Present");
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				System.out.println("Discount Zero");
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = salesLineItem.getPrice()*salesLineItem.getQty();
				} else {
					totalAmount = (salesLineItem.getPrice()*salesLineItem.getQty())
							- salesLineItem.getDiscountAmt();
				}
			} else {
				System.out.println("Discount Not Zero");
				double disPercentAmount = getPercentAmount(salesLineItem, false);
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = (salesLineItem.getPrice()*salesLineItem.getQty()) - disPercentAmount;
				} else {
					totalAmount = (salesLineItem.getPrice()*salesLineItem.getQty()) - disPercentAmount
							- salesLineItem.getDiscountAmt();
				}
			}
		} else {

			System.out.println("Area Present");
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				System.out.println(
						"ZERO Area Present --PercentageDiscount");
				if (salesLineItem.getDiscountAmt() == 0) {
					System.out.println(
							"ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount = salesLineItem.getPrice()
							* Double.parseDouble(salesLineItem.getArea().trim());
				} else {
					System.out.println(
							"NON ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- salesLineItem.getDiscountAmt();
				}
			} else {
				System.out.println(
						"NON ZERO Area Present --PercentageDiscount");
				double disPercentAmount = getPercentAmount(salesLineItem, true);
				if (salesLineItem.getDiscountAmt() == 0) {
					System.out.println(
							"ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount;
				} else {
					System.out.println(
							"NON ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount - salesLineItem.getDiscountAmt();
				}
			}

		}
		System.out.println( "Assesable Value::::" + totalAmount);
		return totalAmount;
	}

	private double getPercentAmount(SalesLineItem salesLineItem,
			boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		if (isAreaPresent) {
			percentAmount = ((salesLineItem.getPrice()
					* Double.parseDouble(salesLineItem.getArea().trim()) * salesLineItem
					.getPercentageDiscount()) / 100);
		} else {
			percentAmount = ((salesLineItem.getQty()*salesLineItem.getPrice() * salesLineItem //date 9/3/2018 by jayshree add qty
					.getPercentageDiscount()) / 100);
		}
		return percentAmount;
	}

	private boolean validateTaxes(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		if (salesLineItem.getVatTax().getPercentage() != 0) {
			return true;
		} else {
			if (salesLineItem.getServiceTax().getPercentage() != 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}
	
	/** 12-10-2017 sagar sore [ for sales order attachment in email with GST] **/
	public void createPdfForEmailGST(Company comp, Customer cust, Sales sq,
			CompanyPayment payMode1, CompanyPayment payMode2) {

		String preprintStatus = "plane";
		Date saleOderOrQuotationDate = null;
		this.comp = comp;
		this.qp = sq;
		this.cust = cust;
		this.products = qp.getItems();
		this.payTermsLis = qp.getPaymentTermsList();
		this.prodTaxes = qp.getProductTaxes();
		this.prodCharges = qp.getProductCharges();
		this.comppayment = payMode1;
		this.comppayment1 = payMode2;
		this.articletype = cust.getArticleTypeDetails();
		if (sq instanceof SalesOrder) {
			SalesOrder salesOderEntity = (SalesOrder) qp;
			saleOderOrQuotationDate = salesOderEntity.getSalesOrderDate();
			stateList=ofy().load().type(State.class)
					.filter("companyId", salesOderEntity.getCompanyId()).list();
				soEntity = salesOderEntity;			
		} else {
			SalesQuotation quotation = (SalesQuotation) qp;
			this.salesquotEntity = quotation;
			saleOderOrQuotationDate = quotation.getQuotationDate();
			stateList=ofy().load().type(State.class)
					.filter("companyId", quotation.getCompanyId()).list();
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		boolean flag = false;
		try {
			flag = saleOderOrQuotationDate.after(sdf.parse("30 Jun 2017"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// ends here

		if (!flag) {
			System.out.println("print createPdf");
			if (preprintStatus.equals("plane")) {

				System.out.println("contract inside plane");

				createblankline();
				if (CompanyNameLogoflag == true) {
//					createLogo(document, comp);//Date 23/11/2017  comment by Jayshree to remove logo method
				} else {
//					createLogo123(document, comp);//Date 23/11/2017  comment by Jayshree to remove logo method
				}

				createHeadingInfo();
			} else {

				if (preprintStatus.equals("yes")) {

					System.out.println("inside prit yes");
					createSpcingForHeading();
					createCompanyAddressDetails();
				}

				if (preprintStatus.equals("no")) {

					System.out.println("inside prit no");

					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}

					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					createSpcingForHeading();
					createCompanyAddressDetails();
				}
			}

			if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
				createProductDetailWithDiscount();
			} else if (nothing > 0) {
				createProductDetailWithOutDisc();
			} else {
				createProductInfo();
			}

			termsConditionsInfo();
			footerInfo();

		} else {
			/** date 9/11/2017 added by komal to get only one copy of sales quotation  **/			
			System.out.println("Rohan inside else conditions ");
			// upcflag==false &&
			if (preprintStatus.equals("plane")) {
//				createLogo(document, comp);//Date 23/11/2017  comment by Jayshree to remove logo method
				createHeader(0);
				// createCompanyAddress();
			} else {

				if (preprintStatus.equals("yes")) {
					System.out.println("inside prit yes");
//					createBlankforUPC(0);//comment By Jayshree
					createBlankforUPC2(0);
				}
				if (preprintStatus.equals("no")) {
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}

					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
//					createBlankforUPC(0);//Comment By Jayshree
					createBlankforUPC2(0);
				}
			}
//			createInvoiceDetails();//Comment by jayshree
			createInvoiceDetails2();
//			createCustomerDetails();//comment by jayshree
			createCustomerDetails2();
//			createProductDetails();//comment by jayshree
			createProductDetails2();
//			createProductDetailsVal();//comment by jayshree
			createProductDetailsVal2();
			createotherCharges();
			if(qp instanceof SalesQuotation){
				SalesQuotation salesQuotation=(SalesQuotation) qp;
//				createFooterTaxPart(salesQuotation);//Comment by jayshree
				createFooterTaxPart2(salesQuotation);
			}else{
				SalesOrder salesOrder=(SalesOrder) qp;
//				createFooterTaxPartSalesOrder(salesOrder);//comment by jayshree
				createFooterTaxPartSalesOrder2(salesOrder);
			}
//			createFooterLastPart(preprintStatus);
			
			createFooterLastPart2(preprintStatus);
			if(productDetails){
				try {
					createServices();
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

}

	}

	
	
	

	

	

	

	

	

	

	/**
	 * Date 24/3/2018
	 * By Jayshree
	 * Add new duplicate methods for pdf
	 */
	
	private void createBlankforUPC2(int i) {

//		Image uncheckedImg = null;
//		try {
//			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
//		} catch (BadElementException | IOException e3) {
//			e3.printStackTrace();
//		}
//		uncheckedImg.scalePercent(9);
//
//		Image checkedImg = null;
//		try {
//			checkedImg = Image.getInstance("images/checked_checkbox.png");
//		} catch (BadElementException | IOException e3) {
//			e3.printStackTrace();
//		}
//		checkedImg.scalePercent(9);
//
//		// Phrase phrAlp=new Phrase("  ALP",font9);
//		// Paragraph paraAlp=new Paragraph();
//		// paraAlp.setIndentationLeft(10f);
//		// paraAlp.add(new Chunk(uncheckedImg, 0, 0, true));
//		// paraAlp.add(phrAlp);
//
//		// rohan added this code
//		float[] myWidth = { 3, 4, 3 };
//
//		PdfPTable mytbale = new PdfPTable(3);
//		mytbale.setSpacingAfter(5f);
//		mytbale.setWidthPercentage(100f);
//		try {
//			mytbale.setWidths(myWidth);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//
//		Phrase myblank = new Phrase("   ", font10);
//		PdfPCell myblankCell = new PdfPCell(myblank);
//		// stat1PhraseCell.addElement(stat1Phrase);
//		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase myblankborderZero = new Phrase(" ", font10);
//		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
//		// stat1PhraseCell.addElement(stat1Phrase);
//		myblankborderZeroCell.setBorder(0);
//		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stat1Phrase = new Phrase("  Original for Receipient", font10);
//		Paragraph para1 = new Paragraph();
//		para1.setIndentationLeft(10f);
//		para1.add(myblank);
//		if (i == 0) {
//			para1.add(new Chunk(checkedImg, 0, 0, true));
//		} else {
//			para1.add(new Chunk(uncheckedImg, 0, 0, true));
//		}
//
//		para1.add(stat1Phrase);
//		para1.setAlignment(Element.ALIGN_MIDDLE);
//
//		PdfPCell stat1PhraseCell = new PdfPCell(para1);
//		stat1PhraseCell.setBorder(0);
//		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stat2Phrase = new Phrase("  Duplicate for Supplier/Transporter",
//				font10);
//		Paragraph para2 = new Paragraph();
//		para2.setIndentationLeft(10f);
//
//		if (i == 1) {
//			para2.add(new Chunk(checkedImg, 0, 0, true));
//		} else {
//			para2.add(new Chunk(uncheckedImg, 0, 0, true));
//		}
//
//		para2.add(stat2Phrase);
//		para2.setAlignment(Element.ALIGN_CENTER);
//
//		PdfPCell stat2PhraseCell = new PdfPCell(para2);
//		stat2PhraseCell.setBorder(0);
//		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stat3Phrase = new Phrase("  Triplicate for Supplier", font10);
//		Paragraph para3 = new Paragraph();
//		para3.setIndentationLeft(10f);
//
//		if (i == 2) {
//			para3.add(new Chunk(checkedImg, 0, 0, true));
//		} else {
//			para3.add(new Chunk(uncheckedImg, 0, 0, true));
//		}
//		para3.add(stat3Phrase);
//		para3.setAlignment(Element.ALIGN_JUSTIFIED);
//
//		PdfPCell stat3PhraseCell = new PdfPCell(para3);
//		stat3PhraseCell.setBorder(0);
//		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// mytbale.addCell(myblankborderZero);
//		// mytbale.addCell(myblankborderZero);
//		mytbale.addCell(stat1PhraseCell);
//		// mytbale.addCell(myblankborderZeroCell);
//		// mytbale.addCell(myblankCell);
//		mytbale.addCell(stat2PhraseCell);
//		// mytbale.addCell(myblankborderZeroCell);
//		// mytbale.addCell(myblankCell);
//		mytbale.addCell(stat3PhraseCell);
//		// mytbale.addCell(myblankborderZeroCell);

		String title = "";
		if (qp instanceof SalesOrder)
			title = "Sales Order";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof SalesOrder) {
			SalesOrder conEntity = (SalesOrder) qp;
			conEnt = conEntity.getSalesOrderDate();
		} else {
			SalesQuotation quotEnt = (SalesQuotation) qp;
			conEnt = quotEnt.getQuotationDate();
		}

		String countinfo = "ID : " + qp.getCount() + "";
		System.out.println("idddd"+qp.getCount());
		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.setBorderWidthBottom(0);
//		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14bold);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
//		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.setBorderWidthBottom(0);
//		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14bold);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
//		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.setBorderWidthBottom(0);
//		creationcell.addElement(Chunk.NEWLINE);

		// *********rohan make changes here for contract start and end date
		// ***********8
		PdfPCell conStartAndEndDateCell = null;
		PdfPCell blankCell = null;
		PdfPCell blankCell1 = null;

		// **********************************ends here
		// *******************************
		float[] columnWidthsForSTandEndDate = { 4f, 3f, 4f };
		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);

		try {
			titlepdftable.setWidths(columnWidthsForSTandEndDate);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		// *********rohan make changes here for contract start and end date
		// ***********8

		// **********************************ends here
		// *******************************
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		//
		// PdfPTable parent=new PdfPTable(1);
		// parent.setWidthPercentage(100);
		//
		// PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		// parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			/** date 09/11/2017 commented by komal to remove 3 checkboxxes **/
			//document.add(mytbale);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	private void createInvoiceDetails2() {


		float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		float[] columnHalfWidth = { 1f, 1f };
		
		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100);
//		try {
//			mainTable.setWidths(1);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}

//		PdfPTable part1Table = new PdfPTable(1);
//		part1Table.setWidthPercentage(100);
//		float[] columnrohanCollonWidth = { 3.5f, 0.2f, 6.8f };
		PdfPTable colonTable = new PdfPTable(9);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(new float[]{4f, 1f, 6f,4f, 1f, 6f,4f, 1f, 6f});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseCharge = new Phrase("Reverse Charge", font6bold);
		PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
		// reverseChargeCell.addElement(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseChargeVal = new Phrase(" NO ", font7);
		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		// reverseChargeValCell.addElement(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String oderid = "";
		String oderDate = "";
		Date soDt = null;
		if (qp instanceof SalesOrder) {
			SalesOrder soEntity = (SalesOrder) qp;
			oderid = "Oder Id";
			oderDate = "Oder Date";
			soDt = soEntity.getSalesOrderDate();
		} else {
			SalesQuotation quotation = (SalesQuotation) qp;
			oderid = "Quotation Id";
			oderDate = "Quotation Date";
			soDt = quotation.getQuotationDate();
		}

		Phrase invoiceNo = new Phrase(oderid, font6bold);
		PdfPCell invoiceNoCell = new PdfPCell(invoiceNo);
		// invoiceNoCell.addElement(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNoVal = new Phrase(qp.getCount() + "", font7);
		PdfPCell invoiceNoValCell = new PdfPCell(invoiceNoVal);
		// invoiceNoValCell.addElement(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceDate = new Phrase(oderDate, font6bold);
		PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		// invoiceDateCell.addElement(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// sdf.format(invoiceentity.getInvoiceDate())

		Phrase invoiceDateVal = new Phrase(fmt.format(soDt), font7);
		PdfPCell invoiceDateValCell = new PdfPCell(invoiceDateVal);
		// invoiceDateValCell.addElement(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceNoValCell);
		colonTable.addCell(invoiceDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceDateValCell);

		PdfPCell pdfCell = new PdfPCell();
//		pdfCell.setBorder(0);
		pdfCell.addElement(colonTable);
		
		if (qp instanceof SalesOrder) { //Ashwini Patil Date:24-04-2023 not required for sales quotation
			mainTable.addCell(pdfCell);
		}
		
		//Date 24/3/2018 By Jayshree cooment this

//		Phrase state = new Phrase("State", font6bold);
//		PdfPCell stateCell = new PdfPCell(state);
//		// stateCell.addElement(state);
//		stateCell.setBorder(0);
//		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(), font7);
//		PdfPCell stateValCell = new PdfPCell(stateVal);
//		// stateValCell.addElement(stateVal);
//		stateValCell.setBorder(0);
//		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stateCode = new Phrase("State Code", font6bold);
//		PdfPCell stateCodeCell = new PdfPCell(stateCode);
//		// stateCodeCell.addElement(stateCode);
//		stateCodeCell.setBorder(0);
//		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stateCodeVal = new Phrase(" ", font7);
//		PdfPCell stateCodeValCell = new PdfPCell();
//		stateCodeValCell.addElement(stateCodeVal);
//		stateCodeValCell.setBorder(0);
//		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPTable statetable = new PdfPTable(2);
//		statetable.setWidthPercentage(100);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		colonTable.addCell(stateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateValCell);
//		statetable.addCell(colonTable);
//
//		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		colonTable.addCell(stateCodeCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateCodeValCell);
//		statetable = new PdfPTable(2);
//		statetable.setWidthPercentage(100);
//		statetable.addCell(colonTable);
//
//		PdfPCell stateTableCell = new PdfPCell();
//		stateTableCell.setBorder(0);
//		stateTableCell.addElement(statetable);
//		part1Table.addCell(stateTableCell);
//
//		PdfPTable part2Table = new PdfPTable(1);
//		part2Table.setWidthPercentage(100);
//
//		Phrase contractId = new Phrase("", font6bold);
//		PdfPCell contractIdCell = new PdfPCell(contractId);
//		// contractIdCell.addElement(contractId);
//		contractIdCell.setBorder(0);
//		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		// " "+invoiceentity.getContractCount()+
//		Phrase contractIdVal = new Phrase("", font7);
//		PdfPCell contractIdValCell = new PdfPCell(contractIdVal);
//		// contractIdValCell.addElement(contractIdVal);
//		contractIdValCell.setBorder(0);
//		contractIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase startDate = new Phrase("", font6bold);
//		PdfPCell startDateCell = new PdfPCell(startDate);
//		// startDateCell.addElement(startDate);
//		startDateCell.setBorder(0);
//		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		// sdf.format(q.getContractStartDate())
//		Phrase startDateVal = new Phrase("", font7);
//		PdfPCell startDateValCell = new PdfPCell(startDateVal);
//		// startDateValCell.addElement(startDateVal);
//		startDateValCell.setBorder(0);
//		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase endDate = new Phrase("", font6bold);
//		PdfPCell endDateCell = new PdfPCell(endDate);
//		// endDateCell.addElement(endDate);
//		endDateCell.setBorder(0);
//		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		// sdf.format(invoiceentity.getContractEndDate())
//		Phrase endDateVal = new Phrase("", font7);
//		PdfPCell endDateValCell = new PdfPCell(endDateVal);
//		// endDateValCell.addElement(endDateVal);
//		endDateValCell.setBorder(0);
//		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		/**
//	 * 
//	 */
//
//		// Phrase periodstartDate=new Phrase("Billing From",font10bold);
//		// PdfPCell periodstartDateCell=new PdfPCell(periodstartDate);
//		// periodstartDateCell.addElement(periodstartDate);
//		// periodstartDateCell.setBorder(0);
//		// periodstartDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		//
//		// // rohan adde this code
//		// Phrase periodstartDateVal=null;
//		// if(invoiceentity.getBillingPeroidFromDate()!= null){
//		// periodstartDateVal=new
//		// Phrase(sdf.format(invoiceentity.getBillingPeroidFromDate()),font10);
//		// }
//		// else
//		// {
//		// periodstartDateVal=new Phrase(" ",font10);
//		// }
//
//		// PdfPCell periodstartDateValCell=new PdfPCell(periodstartDateVal);
//		// // periodstartDateValCell.addElement(periodstartDateVal);
//		// periodstartDateValCell.setBorder(0);
//		// periodstartDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		// Phrase periodendDate=new Phrase("To",font10bold);
//		// PdfPCell periodendDateCell=new PdfPCell(periodendDate);
//		// // periodendDateCell.addElement(periodendDate);
//		// periodendDateCell.setBorder(0);
//		// periodendDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		//
//		// // rohan adde this code
//		// Phrase periodendDateVal=null;
//		// if(invoiceentity.getBillingPeroidToDate()!= null){
//		// periodendDateVal=new
//		// Phrase(sdf.format(invoiceentity.getBillingPeroidToDate()),font10);
//		// }
//		// else
//		// {
//		// periodendDateVal=new Phrase(" ",font10);
//		// }
//		//
//		//
//		// PdfPCell periodendDateValCell=new PdfPCell(periodendDateVal);
//		// // periodendDateValCell.addElement(periodendDateVal);
//		// periodendDateValCell.setBorder(0);
//		// periodendDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		PdfPTable billperiodtable = new PdfPTable(2);
//		billperiodtable.setWidthPercentage(100);
//
//		PdfPTable billFromcolonTable = new PdfPTable(3);
//		billFromcolonTable.setWidthPercentage(100);
//		try {
//			billFromcolonTable.setWidths(columnDateCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// billFromcolonTable.addCell(periodstartDateCell);
//		// billFromcolonTable.addCell(colonCell);
//		// billFromcolonTable.addCell(periodstartDateValCell);
//
//		PdfPTable billTocolonTable = new PdfPTable(3);
//		billTocolonTable.setWidthPercentage(100);
//		try {
//			billTocolonTable.setWidths(columnDateCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// billTocolonTable.addCell(periodendDateCell);
//		// billTocolonTable.addCell(colonCell);
//		// billTocolonTable.addCell(periodendDateValCell);
//
//		PdfPCell fromDate = new PdfPCell(billFromcolonTable);
//		fromDate.setBorder(0);
//		// fromDate.addElement(billFromcolonTable);
//
//		billperiodtable.addCell(fromDate);
//		fromDate = new PdfPCell();
//		fromDate.setBorder(0);
//		fromDate.addElement(billTocolonTable);
//		billperiodtable.addCell(fromDate);
//		/**
//	 * 
//	 */
//		PdfPTable periodtable = new PdfPTable(2);
//		periodtable.setWidthPercentage(100);
//		float[] columnrohanrrCollonWidth = { 2.8f, 0.2f, 7.7f };
//		PdfPTable concolonTable = new PdfPTable(3);
//		concolonTable.setWidthPercentage(100);
//		try {
//			concolonTable.setWidths(columnrohanrrCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// concolonTable.addCell(contractIdCell);
//		// concolonTable.addCell(colonCell);
//		// concolonTable.addCell(contractIdValCell);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnDateCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// colonTable.addCell(startDateCell);
//		// colonTable.addCell(colonCell);
//		// colonTable.addCell(startDateValCell);
//
//		PdfPCell startcolonTableCell = new PdfPCell(colonTable);
//		startcolonTableCell.setBorder(0);
//		// startcolonTableCell.addElement(colonTable);
//		periodtable.addCell(startcolonTableCell);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnDateCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// colonTable.addCell(endDateCell);
//		// colonTable.addCell(colonCell);
//		// colonTable.addCell(endDateValCell);
//
//		PdfPCell endcolonTableCell = new PdfPCell();
//		endcolonTableCell.setBorder(0);
//		endcolonTableCell.addElement(colonTable);
//		periodtable.addCell(endcolonTableCell);
//
//		PdfPCell periodTableCell = new PdfPCell();
//		periodTableCell.setBorder(0);
//		periodTableCell.addElement(periodtable);
//
//		PdfPCell concolonTableCell = new PdfPCell();
//		concolonTableCell.setBorder(0);
//		concolonTableCell.addElement(concolonTable);
//
//		PdfPCell billperiodtableCell = new PdfPCell();
//		billperiodtableCell.setBorder(0);
//		billperiodtableCell.addElement(billperiodtable);
//
//		part2Table.addCell(concolonTableCell);
//		part2Table.addCell(periodTableCell);
//		part2Table.addCell(billperiodtableCell);
//
//		PdfPCell part1Cell = new PdfPCell();
//		part1Cell.setBorderWidthRight(0);
//		part1Cell.addElement(part1Table);
//
//		mainTable.addCell(part1Cell);
//
//		part1Cell = new PdfPCell();
//		// part1Cell.setBorderWidthRight(0);
//		part1Cell.addElement(part2Table);
//		mainTable.addCell(part1Cell);
//
//		// mainTable.addCell(blankCell);
//		// mainTable.addCell(blankCell);
//		Phrase billingAddress = new Phrase("Bill to Party", font8bold);
//		// Paragraph billingpara=new Paragraph();
//		// billingpara.add(billingAddress);
//		// billingpara.setAlignment(Element.ALIGN_CENTER);
//		// billingpara.setAlignment(Element.ALIGN_MIDDLE);
//		PdfPCell billAdressCell = new PdfPCell(billingAddress);
//		// billAdressCell.addElement(billingAddress);
//		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		// billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//
//		mainTable.addCell(billAdressCell);
//		Phrase serviceaddress = new Phrase("Ship to Party", font8bold);
//		// Paragraph servicepara=new Paragraph();
//		// servicepara.add(serviceaddress);
//		// servicepara.setAlignment(Element.ALIGN_CENTER);
//		// servicepara.setAlignment(Element.ALIGN_MIDDLE);
//		PdfPCell serviceCell = new PdfPCell(serviceaddress);
//		// serviceCell.addElement(serviceaddress);
//		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		// serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
//		mainTable.addCell(serviceCell);
		
		
		
	PdfPTable headTab=new PdfPTable (2) ;	
	headTab.setWidthPercentage(100);
	try {
		headTab.setWidths(new float[]{50,50});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	Phrase billingAddress = new Phrase("Bill to Party", font8bold);
	PdfPCell billAdressCell = new PdfPCell(billingAddress);
	billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headTab.addCell(billAdressCell);
	
	Phrase serviceaddress = new Phrase("Ship to Party", font8bold);
	PdfPCell serviceCell = new PdfPCell(serviceaddress);
	serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headTab.addCell(serviceCell);
	
	PdfPCell headCell=new PdfPCell(headTab);
	headCell.setBorder(0);
	mainTable.addCell(headCell);
	
	try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
		
	}
	
	
	private void createCustomerDetails2() {
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		float[] columnHalfWidth = { 1f, 1f };
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font6bold);
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			// if(cust.isCompany()==true){
			//
			//
			// tosir="M/S";
			// }
			// else{
			// tosir="";
			// }

			/***
			 * Dev.By Jayshree 
			 * Date 20/11/2017 
			 * Des.add salutation to customer name
			 */
			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S "+" " + cust.getCompanyName().trim();
				System.out.println("hi vijay ==="+custName);
			} else if(cust.getSalutation()!=null)
			{
				custName = cust.getSalutation()+" "+cust.getFullname().trim();
			}
			else
			{
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here
		System.out.println("fullname =="+fullname);
		Phrase nameCellVal = new Phrase(fullname + "       Mob : "
				+ cust.getCellNumber1(), font7);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase address = new Phrase("Address", font6bold);
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		String billAddress="";

		if(cust.getAdress().getCompleteAddress()!=null){
			billAddress=cust.getAdress().getCompleteAddress();
			
			/**
			 * @author Vijay Date 21-01-2021
			 * Des :- if billing address exist in quotation it will print or else it will print from customer
			 */
			if (qp instanceof SalesQuotation) {
				SalesQuotation salesQuatation = (SalesQuotation) qp;
				if(salesQuatation.getCustBillingAddress()!=null && salesQuatation.getCustBillingAddress().getAddrLine1() !=null &&
						!salesQuatation.getCustBillingAddress().getAddrLine1().equals("")){
					billAddress=salesQuatation.getCustBillingAddress().getCompleteAddress();
					logger.log(Level.SEVERE," length == "+salesQuatation.getCustBillingAddress().getCompleteAddress().length());
				}
			}
			
		}else{
			billAddress="";
		}
		/**
		 * @author Anil
		 * @since 30-11-2020
		 * if customer billing order is stored in sales order entity then
		 * we pick up and display billing address from there only
		 * Raised by Rahul Tiwari ,PTSPL
		 */
		if (qp instanceof SalesOrder) {
			SalesOrder salesordentity = (SalesOrder) qp;
			if(salesordentity.getNewcustomerAddress()!=null){
				billAddress = salesordentity.getNewcustomerAddress().getCompleteAddress().trim();
			}
		}
		
//		Phrase addressVal = new Phrase(cust.getAdress().getCompleteAddress().trim(), font7);
		Phrase addressVal =new Phrase(billAddress, font7);
		PdfPCell addressValCell = new PdfPCell(addressVal);
		// addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase gstTin = new Phrase("GSTIN", font6bold);
		PdfPCell gstTinCell = new PdfPCell(gstTin);
		// gstTinCell.addElement(gstTin);
		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		PdfPTable statetable = new PdfPTable(5);
		statetable.setWidthPercentage(100);
		
		try {
			statetable.setWidths(new float[]{40,10,30,15,5});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String gstTinStr = "";
		
		if (qp instanceof SalesOrder) {
			logger.log(Level.SEVERE,"GSTIN value for salesorder ");
			for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")
						&&cust.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")
						&&(cust.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("Sales Order")||cust.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("SalesOrder"))){ 
					gstTinStr = cust.getArticleTypeDetails().get(i)
							.getArticleTypeValue().trim();
					logger.log(Level.SEVERE,"gstin value "+gstTinStr);
				}
			}
			
		}
		else if (qp instanceof SalesQuotation) {
			logger.log(Level.SEVERE,"GSTIN value ");
			for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")&&cust.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("SalesQuotation")) {
					gstTinStr = cust.getArticleTypeDetails().get(i)
							.getArticleTypeValue().trim();
					logger.log(Level.SEVERE,"gstin value "+gstTinStr);
				}
			}
			
		}else{
			for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstTinStr = cust.getArticleTypeDetails().get(i)
							.getArticleTypeValue().trim();
				}
			}
			
		}
		
		
		

		/** Date 23-11-2018 By Vijay For GST Number Print if GST applicable or not applicable 
		 *  so below code commented not required
		 */ 

//			/**
//			 * Date 16/3/2018 
//			 * By Jayshree
//			 * Des.remove the gstin if tax are not present
//			 */
//			
//			if(qp instanceof SalesQuotation){
//				if(qp.getProductTaxes().size()==0){
//					gstTinStr="";
//				}
//			}
//			else{
//				if(qp.getProductTaxes().size()==0)
//				{
//					gstTinStr="";
//				}
//			}
//			//End By Jayshree
	
	Phrase gstTinVal = new Phrase(gstTinStr, font7);
	PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
	// gstTinValCell.addElement(gstTinVal);
	gstTinValCell.setBorder(0);
	gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	statetable.addCell(gstTinValCell);
	
	Phrase blank = new Phrase(" ", font6);
	PdfPCell blankcell = new PdfPCell(blank);
	// stateCell.addElement(state);
	blankcell.setBorder(0);
	blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	statetable.addCell(blankcell);
		
	String stCo = "";
	for (int i = 0; i < stateList.size(); i++) {
		if (stateList.get(i).getStateName().trim()
				.equalsIgnoreCase(cust.getAdress().getState().trim())) {
			stCo = stateList.get(i).getStateCode().trim();
			break;
		}
	}

	Phrase stateCode = new Phrase("State Code", font6bold);
	PdfPCell stateCodeCell = new PdfPCell(stateCode);
	// stateCodeCell.addElement(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	statetable.addCell(stateCodeCell);
	
	
	Phrase stateCodeVal = new Phrase(stCo, font7);
	PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
	// stateCodeValCell.addElement(stateCodeVal);
	// stateCodeValCell.setBorder(0);
	stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	statetable.addCell(stateCodeValCell);
	
	statetable.addCell(blankcell);
	
	PdfPCell stateout=new PdfPCell(statetable);
	stateout.setBorder(0);
	
	
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);
		/**
		 * Vijay for GST Number not print with process Config
		 */
		if(gstNumberPrintFlag){
		colonTable.addCell(gstTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateout);
		}

		
		//Ashwini Patil Date;14-02-2023
		if (qp instanceof SalesOrder) {
			if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
				System.out.println("Article type size"+cust.getArticleTypeDetails().size());
				for(ArticleType type:cust.getArticleTypeDetails()){
					System.out.println("type.getDocumentName()==="+type.getDocumentName());//ServiceInvoice
					if(type.getDocumentName().equalsIgnoreCase("Sales Order")&&type.getArticlePrint().equalsIgnoreCase("Yes")||type.getDocumentName().equalsIgnoreCase("SalesOrder")&&type.getArticlePrint().equalsIgnoreCase("Yes")){
						if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")){
							continue;
						}
						System.out.println("type.getArticleTypeName()==="+type.getArticleTypeName());
						Phrase articlname1 = new Phrase(type.getArticleTypeName(), font6bold);
						PdfPCell articlnameCell1 = new PdfPCell(articlname1);
						articlnameCell1.setBorder(0);
						articlnameCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						
						System.out.println("type.getArticleTypeValue()==="+type.getArticleTypeValue());
						Phrase articlnameCellVal1 = new Phrase(type.getArticleTypeValue(), font7);
						PdfPCell articlnameCellValCell1 = new PdfPCell(articlnameCellVal1);
						articlnameCellValCell1.setBorder(0);
						articlnameCellValCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
						
						colonTable.addCell(articlnameCell1);
						colonTable.addCell(colonCell);
						colonTable.addCell(articlnameCellValCell1);
				}
				}
			}
		}
		
		
		PdfPCell cell1 = new PdfPCell();
		cell1.setBorder(0);
		cell1.addElement(colonTable);
		
		
		

//		Phrase state = new Phrase("State", font6bold);
//		PdfPCell stateCell = new PdfPCell(state);
//		// stateCell.addElement(state);
//		stateCell.setBorder(0);
//		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stateVal = new Phrase(cust.getAdress().getState().trim(), font7);
//		PdfPCell stateValCell = new PdfPCell(stateVal);
//		// stateValCell.addElement(stateVal);
//		stateValCell.setBorder(0);
//		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		

//		PdfPTable statetable = new PdfPTable(2);
//		statetable.setWidthPercentage(100);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		colonTable.addCell(stateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateValCell);
//
//		PdfPCell colonTableState = new PdfPCell();
//		colonTableState.setBorder(0);
//		colonTableState.addElement(colonTable);
//		statetable.addCell(colonTableState);
//
//		colonTable = new PdfPTable(4);
//		colonTable.setWidthPercentage(100);
//		float[] columnStateCodeCollonWidth = { 35, 5, 15, 45 };
//		try {
//			colonTable.setWidths(columnStateCodeCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		Phrase blak = new Phrase(" ", font8);
//		PdfPCell blakCell = new PdfPCell(blak);
//		blakCell.setBorder(0);
//
//		colonTable.addCell(stateCodeCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(stateCodeValCell);
//		colonTable.addCell(blakCell);
//
//		PdfPCell colonTableValState = new PdfPCell();
//		colonTableValState.setBorder(0);
//		colonTableValState.addElement(colonTable);
//		statetable.addCell(colonTableValState);
//
//		PdfPCell stateTableCell = new PdfPCell(statetable);
//		stateTableCell.setBorder(0);
//		stateTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		// stateTableCell.addElement(statetable);

		part1Table.addCell(cell1);
//		part1Table.addCell(stateTableCell);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		/* Ends Part 1 */

		/* Part 2 Start */
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font6bold);
		PdfPCell name2Cell = new PdfPCell();
		name2Cell.addElement(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name2CellVal = new Phrase(fullname + "       Mob : "
				+ cust.getCellNumber1(), font7);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase address2 = new Phrase("Address", font6bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
         
		/**Amol**/
//		Phrase address2Val = new Phrase(cust.getSecondaryAdress().getCompleteAddress().trim(), font7);
		/**
		 * Date 29-08-2020 
		 * @author Vijay Chougule
		 * Des :- Bug :- Updated code to handle Sales Quotation and Sales Order  Shipping Address 
		 */
		String shippingAddress = "";
		if (qp instanceof SalesOrder) {
			SalesOrder salesordentity = (SalesOrder) qp;
			shippingAddress = salesordentity.getShippingAddress().getCompleteAddress().trim();
		} else {
			SalesQuotation salesquotentity = (SalesQuotation) qp;
			shippingAddress = salesquotentity.getShippingAddress().getCompleteAddress().trim();
		}
		
		
		
		//Ashwini Patil Date:19-06-2023
		if(cust!=null&&cust.getServiceAddressName()!=null&&!cust.getServiceAddressName().equals("")){
					fullname=cust.getServiceAddressName();
					System.out.println("in createCustomerDetails2 fullname after="+fullname);
		}
		Phrase nameCellVal2 = new Phrase(fullname + "       Mob : "
						+ cust.getCellNumber1(), font7);
		PdfPCell nameCellValCell2 = new PdfPCell(nameCellVal2);
		nameCellValCell2.setBorder(0);
		nameCellValCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
//		Phrase address2Val =new Phrase(salesquotEntity.getShippingAddress().getCompleteAddress().trim(), font7);
		Phrase address2Val =new Phrase(shippingAddress, font7);

		PdfPCell address2ValCell = new PdfPCell(address2Val);
		// address2ValCell.addElement(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase gstTin2 = new Phrase("GSTIN", font6bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		// gstTin2Cell.addElement(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		

		PdfPTable statetable2 = new PdfPTable(5);
		statetable2.setWidthPercentage(100);
		
		try {
			statetable2.setWidths(new float[]{40,10,30,15,5});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/**
		 * Date 16/3/2018 
		 * By Jayshree
		 * Des.remove the gstin if tax are not present
		 */
		
		String gstTin2Str = "";
		if(qp instanceof SalesQuotation){
			for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")&&cust.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("SalesQuotation")) {
					gstTin2Str = cust.getArticleTypeDetails().get(i)
							.getArticleTypeName().trim();
				}
			}
			
		}else{
			for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstTin2Str = cust.getArticleTypeDetails().get(i)
							.getArticleTypeName().trim();
				}
			}
		}
		
		
		
		
		
		if(qp instanceof SalesQuotation){
			if(qp.getProductTaxes().size()==0){
				gstTin2Str="";
			}
		}
		else{
			if(qp.getProductTaxes().size()==0)
			{
				gstTin2Str="";
			}
		}
		//End By Jayshree
		
		
		Phrase gstTin2Val = new Phrase(gstTinStr, font7);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		// gstTin2ValCell.addElement(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statetable2.addCell(gstTin2ValCell);
		
		
		Phrase blank2 = new Phrase(" ", font6);
		PdfPCell blankcell2 = new PdfPCell(blank2);
		// stateCell.addElement(state);
		blankcell2.setBorder(0);
		blankcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		statetable2.addCell(blankcell2);
			
		String stCo2 = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo2 = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase stateCode2 = new Phrase("State Code", font6bold);
		PdfPCell stateCodeCell2 = new PdfPCell(stateCode2);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell2.setBorder(0);
		stateCodeCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		statetable2.addCell(stateCodeCell2);
		
		
		Phrase stateCodeVal2 = new Phrase(stCo, font7);
		PdfPCell stateCodeValCell2 = new PdfPCell(stateCodeVal2);
		// stateCodeValCell.addElement(stateCodeVal);
		// stateCodeValCell.setBorder(0);
		stateCodeValCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		statetable2.addCell(stateCodeValCell2);
		
		statetable2.addCell(blankcell);
		
		PdfPCell stateout2=new PdfPCell(statetable2);
		stateout2.setBorder(0);
		
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell2);//19-06-2023
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);
		/**
		 * Vijay for GST Number not print with process Config
		 */
		if(gstNumberPrintFlag){
		colonTable.addCell(gstTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateout2);
		}

		PdfPCell cell2 = new PdfPCell();
		cell2.setBorder(0);
		cell2.addElement(colonTable);

//		Phrase state2 = new Phrase("State", font6bold);
//		PdfPCell state2Cell = new PdfPCell(state2);
//		// state2Cell.addElement(state2);
//		state2Cell.setBorder(0);
//		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase state2Val = new Phrase(cust.getSecondaryAdress().getState()
//				.trim(), font7);
//		PdfPCell state2ValCell = new PdfPCell(state2Val);
//		// state2ValCell.addElement(state2Val);
//		state2ValCell.setBorder(0);
//		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		String stsecCo = "";
//		for (int i = 0; i < stateList.size(); i++) {
//			if (stateList
//					.get(i)
//					.getStateName()
//					.trim()
//					.equalsIgnoreCase(
//							cust.getSecondaryAdress().getState().trim())) {
//				stsecCo = stateList.get(i).getStateCode().trim();
//				break;
//			}
//		}
//
//		Phrase state2Code = new Phrase("State Code", font6bold);
//		PdfPCell state2CodeCell = new PdfPCell(state2Code);
//		// state2CodeCell.addElement(state2Code);
//		state2CodeCell.setBorder(0);
//		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase state2CodeVal = new Phrase(stsecCo, font7);
//		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
//		// state2CodeValCell.addElement(state2CodeVal);
//		// state2CodeValCell.setBorder(0);
//		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		PdfPTable state2table = new PdfPTable(2);
//		state2table.setWidthPercentage(100);
//
//		colonTable = new PdfPTable(3);
//		colonTable.setWidthPercentage(100);
//		try {
//			colonTable.setWidths(columnCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		colonTable.addCell(state2Cell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(state2ValCell);
//		PdfPCell state2ValcolonTableCell = new PdfPCell();
//		state2ValcolonTableCell.setBorder(0);
//		state2ValcolonTableCell.addElement(colonTable);
//		state2table.addCell(state2ValcolonTableCell);
//
//		colonTable = new PdfPTable(4);
//		colonTable.setWidthPercentage(100);
//		// float[] columnStateCodeCollonWidth = {35,5,15,45};
//		try {
//			colonTable.setWidths(columnStateCodeCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		colonTable.addCell(state2CodeCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(state2CodeValCell);
//		colonTable.addCell(blakCell);
//
//		state2ValcolonTableCell = new PdfPCell();
//		state2ValcolonTableCell.setBorder(0);
//		state2ValcolonTableCell.addElement(colonTable);
//		state2table.addCell(state2ValcolonTableCell);

		PdfPCell state2TableCell = new PdfPCell(statetable2);
		state2TableCell.setBorder(0);
		// state2TableCell.addElement(state2table);

		part2Table.addCell(cell2);
//		part2Table.addCell(state2TableCell);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);
		/* Part 2 Ends */

		Phrase blankCell = new Phrase(" ", font7);
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createProductDetails2() {
		PdfPTable productTable = new PdfPTable(12);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column16CollonWidthnew);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		
		int colsspan =1;
		if(PC_DoNotPrintUOMFlag){
			++colsspan;
		}
		if(PC_DoNotPrintQtyFlag){
			++colsspan;
		}
		if(PC_DoNotPrintRateFlag){
			++colsspan;
		}
		if(PC_DoNotPrintDiscFlag){
			++colsspan;
		}

		System.out.println("colsspan "+colsspan);
		
		Phrase srNophrase = new Phrase("Sr No", font8bold);
		PdfPCell srNoCell = new PdfPCell(srNophrase);
//		srNoCell.addElement(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		srNoCell.setRowspan(2); // 1

		Phrase servicePhrase = new Phrase("Particulars", font8bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setColspan(colsspan);

//		servicePhraseCell.setRowspan(2);// 2

		Phrase hsnCode = new Phrase("HSN", font8bold);
		PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
//		hsnCodeCell.addElement(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		hsnCodeCell.setRowspan(2);// 3

		Phrase UOMphrase = new Phrase("UOM", font8bold);
		PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
//		UOMphraseCell.addElement(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		UOMphraseCell.setRowspan(2);// 4

		Phrase qtyPhrase = new Phrase("Qty", font8bold);
		PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
//		qtyPhraseCell.addElement(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		qtyPhraseCell.setRowspan(2);// 5

		Phrase ratePhrase = new Phrase("Rate", font8bold);
		PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
//		ratePhraseCell.addElement(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		ratePhraseCell.setRowspan(2);// 6

		Phrase amountPhrase = new Phrase("Amount", font8bold);
		PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
//		amountPhraseCell.addElement(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		amountPhraseCell.setRowspan(2);// 7

		Phrase dicphrase = new Phrase("Disc", font8bold);
		PdfPCell dicphraseCell = new PdfPCell(dicphrase);
//		dicphraseCell.addElement(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		dicphraseCell.setRowspan(2);// 8

		String AssVal = "Ass Val";
		if(salesquotEntity!=null && qp instanceof SalesQuotation && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesQuotation", AppConstants.PC_RENAMEASSALVALASAMOUNT, salesquotEntity.getCompanyId())){
			AssVal = "Amount";
		}
		
		int taxCColumnolsspan =1; 
		if(PC_DoNotPrintTaxFlag){
			taxCColumnolsspan +=4 ;
		}
		System.out.println("taxCColumnolsspan "+taxCColumnolsspan);
		
		
//		Phrase taxValPhrase = new Phrase("Ass Val", font8bold);
		Phrase taxValPhrase = new Phrase(AssVal, font8bold);
		PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
//		taxValPhraseCell.addElement(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		taxValPhraseCell.setRowspan(2);// 9
		taxValPhraseCell.setColspan(taxCColumnolsspan);
		
		// PdfPTable cgstcellTable=new PdfPTable(1);
		// cgstcellTable.setWidthPercentage(100);

		Phrase cgstphrase = new Phrase("CGST%", font8bold);
		PdfPCell cgstphraseCell = new PdfPCell(cgstphrase);
		// cgstphraseCell.addElement(cgstphrase);
		// cgstphraseCell.setBorder(0);
		cgstphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// cgstcellTable.addCell(cgstphraseCell);
//		cgstphraseCell.setColspan(2);
		// cgstphraseCell.setRowspan(2);

		Phrase sgstphrase = new Phrase("SGST%", font8bold);
		PdfPCell sgstphraseCell = new PdfPCell(sgstphrase);
		// sgstphraseCell.setBorder(0);
		sgstphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// sgstphraseCell.addElement(sgstphrase);
		// sgstcellTable.addCell(sgstphraseCell);
//		sgstphraseCell.setColspan(2);
		// sgstphraseCell.setRowspan(2);

		Phrase igstphrase = new Phrase("IGST%", font8bold);
		PdfPCell igstphraseCell = new PdfPCell(igstphrase);
		// igstphraseCell.setBorder(0);
		igstphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// igstphraseCell.addElement(igstphrase);
		// igstcellTable.addCell(igstphraseCell);
//		igstphraseCell.setColspan(2);
		// igstphraseCell.setRowspan(2);

		Phrase totalPhrase = new Phrase("Total", font8bold);
		PdfPCell totalPhraseCell = new PdfPCell(totalPhrase);
//		totalPhraseCell.addElement(totalPhrase);
		totalPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		totalPhraseCell.setRowspan(2);// 2

		Phrase cgstpercentphrase = new Phrase("%", font6bold);
		PdfPCell cgstpercentphraseCell = new PdfPCell();
		// cgstpercentphraseCell.setBorderWidthBottom(0);
		// cgstpercentphraseCell.setBorderWidthTop(0);
		// cgstpercentphraseCell.setBorderWidthLeft(0);
		cgstpercentphraseCell.addElement(cgstpercentphrase);
		// innerCgstTable.addCell(cgstpercentphraseCell);

		Phrase cgstamtphrase = new Phrase("Amt", font6bold);
		PdfPCell cgstamtphraseCell = new PdfPCell();
		cgstamtphraseCell.addElement(cgstamtphrase);
		//

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(hsnCodeCell);
		if(!PC_DoNotPrintUOMFlag){
			productTable.addCell(UOMphraseCell);
		}
		if(!PC_DoNotPrintQtyFlag){
			productTable.addCell(qtyPhraseCell);
		}
		if(!PC_DoNotPrintRateFlag){
			productTable.addCell(ratePhraseCell);
		}
//		productTable.addCell(amountPhraseCell);
		if(!PC_DoNotPrintDiscFlag){
			productTable.addCell(dicphraseCell);
		}
		
		productTable.addCell(taxValPhraseCell);
		if(!PC_DoNotPrintTaxFlag){
			productTable.addCell(cgstphraseCell);
			productTable.addCell(sgstphraseCell);
			productTable.addCell(igstphraseCell);
			productTable.addCell(totalPhraseCell);
		}
		

//		productTable.addCell(cgstpercentphraseCell);
//		productTable.addCell(cgstamtphraseCell);
//		productTable.addCell(cgstpercentphraseCell);
//		productTable.addCell(cgstamtphraseCell);
//		productTable.addCell(cgstpercentphraseCell);
//		productTable.addCell(cgstamtphraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	// no of lines updated by viraj Date:20-12-2018
	int noOfLines = 15;//Ashwini Patil Date:24-04-2023
	int productCount = 0;
	
	private void createProductDetailsVal2() {
		/** date 10/11/2017 added by komal for noOfLines count **/
		int productNameLength = 14;
		if (qp instanceof SalesOrder) {

			SalesOrder salesOderEntity = (SalesOrder) qp;
//			System.out.println("Inside  Condn of Instance con and quot ");
//			int firstBreakPoint = 5;
//			float blankLines = 0;
			/** date 10/11/2017 commented by komal for noOfLines count **/
//			if (salesOderEntity.getItems().size() <= firstBreakPoint) {
//
//				int size = firstBreakPoint - salesOderEntity.getItems().size();
//				blankLines = size * (100 / 5);
//				System.out.println("blankLines size =" + blankLines);
//			} else {
//				blankLines = 10f;
//			}
			/** date 10/11/2017 added by komal for vercetile changes**/
//			if(salesOderEntity.getItems().size() <= noOfLines){
//				blankLines = noOfLines - salesOderEntity.getItems().size() + 1f;
//			}else{
//				blankLines = 1f;
//			}
			
			PdfPTable productTable = new PdfPTable(12);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidthnew);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < salesOderEntity.getItems().size(); i++) {

//				if (i == 5) {
//					break;
//				}
				
				
				/** date 10/11/2017 added by komal for vercetile changes**/
//					if(noOfLines == 0){
//						if(i+1 == salesOderEntity.getItems().size()){
//							productCount = i;
//							break;
//						}
//					}
					
				
				if(noOfLines==0){
					productCount = i;
					break;
				}
				noOfLines = noOfLines - 1;

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(salesOderEntity.getItems()
						.get(i).getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
//				serviceNameCell.addElement(serviceName);
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (salesOderEntity.getItems().get(i).getPrduct()
						.getHsnNumber() != null) {
					hsnCode = new Phrase(salesOderEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
//				hsnCodeCell.addElement(hsnCode);
				hsnCodeCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(salesOderEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
//				uomCell.addElement(uom);
				uomCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(salesOderEntity.getItems().get(i)
						.getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
//				qtyCell.addElement(qty);
				qtyCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(salesOderEntity.getItems()
						.get(i).getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell(rate);
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
//				rateCell.addElement(rate);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(rateCell);

				double amountValue = salesOderEntity.getItems().get(i)
						.getPrice()
						* salesOderEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
//				productTable.addCell(amountCell);

				
				/**
				 * Date 8/3/2018 
				 * By jayshree
				 * Des.to print the discount
				 */
				
				boolean isAreaPresent=false;
				double areval=0;
				double totaldis=0;
				double desper=soEntity.getItems().get(i).getPercentageDiscount();
				try{
				 areval=Double.parseDouble(soEntity.getItems().get(i).getArea());
				 isAreaPresent=true;
				}
				catch(Exception e){
					isAreaPresent=false;
				}
				
				if(isAreaPresent){
					totaldis=(soEntity.getItems().get(i).getQty()*soEntity.getItems().get(i).getPrice()*areval*desper)/100
							+soEntity.getItems().get(i).getDiscountAmt();
				}
				else{
					totaldis=(soEntity.getItems().get(i).getQty()*soEntity.getItems().get(i).getPrice()*desper)/100+
							soEntity.getItems().get(i).getDiscountAmt();
				}
				
				Phrase disc = new Phrase(df.format(totaldis)
						+ "", font8);
				
				//End By jayshree
				
				
				
				
				PdfPCell discCell = new PdfPCell(disc);
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
//				discCell.addElement(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(discCell);
				
//				double asstotalAmount1 = getAssessTotalAmount(soEntity
//						.getItems().get(i));
				
				double asstotalAmount1 = getAssessTotalAmount2(soEntity
						.getItems().get(i));
				
				System.out.println("asstotalAmount1:::::"+asstotalAmount1);
				Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
//				taxableValueCell.addElement(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				boolean taxPresent = validateTaxes(salesOderEntity.getItems()
						.get(i));
				if (taxPresent) {
					if (salesOderEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("IGST")) {
						double asstotalAmount = getAssessTotalAmount(salesOderEntity
								.getItems().get(i));

						double taxAmount = getTaxAmount(asstotalAmount,
								salesOderEntity.getItems().get(i).getVatTax()
										.getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);
						Phrase igstRate = new Phrase(salesOderEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);
						igstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else if (salesOderEntity.getItems().get(i)
							.getServiceTax().getTaxPrintName()
							.equalsIgnoreCase("IGST")) {
						double asstotalAmount = getAssessTotalAmount(salesOderEntity
								.getItems().get(i));

						double taxAmount = getTaxAmount(asstotalAmount,
								salesOderEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);
						igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						
						Phrase igstRate = new Phrase(salesOderEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);
						igstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else {

						if (salesOderEntity.getItems().get(i).getVatTax()
								.getTaxPrintName().equalsIgnoreCase("CGST")) {
							double asstotalAmount = getAssessTotalAmount(salesOderEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getVatTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);
							cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Phrase cgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							cgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getServiceTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
//							sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);
							sgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Phrase sgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);//2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

						} else if (salesOderEntity.getItems().get(i)
								.getVatTax().getTaxPrintName()
								.equalsIgnoreCase("SGST")) {
							double asstotalAmount = getAssessTotalAmount(salesOderEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getServiceTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);
							cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Phrase cgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							cgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getVatTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
							// sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							sgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(sgstRateCell);//2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

						}
					}
				} else {

					logger.log(Level.SEVERE, "Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(
							df.format(getAssessTotalAmount(salesOderEntity
									.getItems().get(i))) + "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
//					totalCell.addElement(totalPhrase);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if (salesOderEntity.getItems().get(i).getPremisesDetails() != null) {
						premisesVal = salesOderEntity.getItems().get(i)
								.getPremisesDetails();
					}

				
					
					// if (printPremiseDetails) {
					// Phrase premisesValPhrs = new Phrase("Premise Details : "
					// + premisesVal, font8);
					// PdfPCell premiseCell = new PdfPCell();
					// premiseCell.setColspan(16);
					// premiseCell.addElement(premisesValPhrs);
					// productTable.addCell(premiseCell);
					// }

				}
				/**
				nidhi
				18-06-2018
			 */
			chunk = new Phrase(" ", font7);
			PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
			Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			Pdfsrnocell2.setBorderWidthRight(0);
			
			String premises = "";
			{
				if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premise For " + products.get(i).getProductName()
						+ " : " + products.get(i).getPremisesDetails();
				System.out.println("products.get(i).getPremisesDetails()"+premises);
			} else {
				premises = "Premise For " + products.get(i).getProductName()+":"+"NA";
				System.out.println("products.get(i).getPremisesDetails()bbbb");
			}
			}
			if(printPremiseDetails)
			{
				Phrase premisesPh = new Phrase(premises, font7);
				PdfPCell premisesCell = new PdfPCell(premisesPh);
				// premisesCell.setBorderWidthRight(0);
				premisesCell.setColspan(11);
				
				noOfLines = noOfLines - 1;
				productTable.addCell(Pdfsrnocell2);
				productTable.addCell(premisesCell);
			}
				/** date 10/11/2017 added by komal for vercetile chnages **/
//				  noOfLines -= 1;
//					 if(salesOderEntity.getItems().get(i).getProductName().length() > productNameLength){
//						 noOfLines -= 1;
//					 }
			}

			
			int remainingLines = 0;
			System.out.println("noOfLines outside" + noOfLines);
			System.out.println("prouductCount" + productCount);

			if (noOfLines != 0) {
				System.out.println("noOfLines"+noOfLines);
				remainingLines = 10 - (10 - noOfLines);
			}
			System.out.println("remainingLines" + remainingLines);

			if (remainingLines != 0) {
				System.out.println("remainingLines"+remainingLines);
				for (int i = 0; i < remainingLines; i++) {
					System.out.println("remainingLines....."+remainingLines);
					System.out.println("i::::" + i);
					Phrase blankPhrase = new Phrase(" ", font10);
					PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
					blankPhraseCell.setBorder(0);
					blankPhraseCell.setColspan(12);
					blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					
					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
				}
			}
			PdfPCell productTableCell = null;
//			int remainingLinesCheck = 0;
//			if (noOfLines != 0) {
//				remainingLinesCheck = 10 - (10 - noOfLines);
//			}
//			System.out.println("remainingLinesCheck" + remainingLinesCheck);
			if (noOfLines == 0 && remainingLines == 0) {
				Phrase my = new Phrase("Please Refer Annexure For More Details",
						font9bold);
				productTableCell = new PdfPCell(my);

			} else {
				Phrase my2 = new Phrase(" ",font9);
				productTableCell = new PdfPCell(my2);
			}

			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			
			
//			1PdfPCell productTableCell = null;
//			if (salesOderEntity.getItems().size() > productCount && productCount !=0 ) {
//				Phrase my = new Phrase("Please Refer Annexure For More Details");
//				productTableCell = new PdfPCell(my);
//
//			} else {
//				Phrase ph = new Phrase(" ", font8);
//				productTableCell = new PdfPCell(ph);
//			}
//
//			// PdfPCell productTableCell = new PdfPCell(blankCell);
//			// productTableCell.setBorderWidthBottom(0);
//			// productTableCell.setBorderWidthTop(0);
//			productTableCell.setBorder(0);
//
//			PdfPTable tab = new PdfPTable(1);
//			tab.setWidthPercentage(100f);
//			tab.addCell(productTableCell);
//			tab.setSpacingBefore(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// //////// End Here

		} else {

			SalesQuotation quotationEntity = (SalesQuotation) qp;

			System.out.println("Else Condn of Instance con and quot ");
			// Ajinkya Start Here
//			int firstBreakPoint = 10;
//			float blankLines = 0;
			/** date 10/11/2017 commented by komal for vercetile changes**/
//			if (quotationEntity.getItems().size() <= firstBreakPoint) {
//				int size = firstBreakPoint - quotationEntity.getItems().size();
//				
//				//blankLines = size * (100 / 5);
//				System.out.println("blankLines size =" + blankLines);
//			} else {
//				//blankLines = 10f;
//			}
			/** date 10/11/2017 added by komal for vercetile changes**/
//			if(quotationEntity.getItems().size() <= noOfLines){
//				blankLines = noOfLines - quotationEntity.getItems().size() + 1f;
//			}else{
//				blankLines = 1f;
//			}
			
			PdfPTable productTable = new PdfPTable(12);
			productTable.setWidthPercentage(100);
			// productTable.setSpacingAfter(blankLines);
			try {
				productTable.setWidths(column16CollonWidthnew);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			int colsspan =1;
			if(PC_DoNotPrintUOMFlag){
				++colsspan;
			}
			if(PC_DoNotPrintQtyFlag){
				++colsspan;
			}
			if(PC_DoNotPrintRateFlag){
				++colsspan;
			}
			if(PC_DoNotPrintDiscFlag){
				++colsspan;
			}
			System.out.println("colsspan "+colsspan);
			

			int taxCColumnolsspan =1;
			if(PC_DoNotPrintTaxFlag){
				taxCColumnolsspan +=4 ;
			}
			System.out.println("taxCColumnolsspan "+taxCColumnolsspan);

			/** date 10/11/2017 added by komal for vercetile changes**/
			for (int i = 0; i < quotationEntity.getItems().size(); i++) {
//				if(noOfLines == 0){
//					if(i+1 == quotationEntity.getItems().size()){
//						productCount = i;
//						break;
//					}
//				}
				/**
				 * Updated By: Viraj
				 * Date: 20-12-2018
				 * Description: to break and go to another page(changed condition)
				 */
				System.out.println("No of Lines at the begining: "+ noOfLines);
				if(noOfLines <= 0){
					productCount = i;
					break;
				}
				noOfLines = noOfLines - 1;
				
				/** date 10/11/2017 commented by komal for vercetile changes**/     
//				if (i == 10) {
//					break;
//				}

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);//c1

				Phrase serviceName = new Phrase(quotationEntity.getItems()
						.get(i).getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
//				serviceNameCell.addElement(serviceName);
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceNameCell.setColspan(colsspan);
				productTable.addCell(serviceNameCell);//c2

				Phrase hsnCode = null;

				if (quotationEntity.getItems().get(i).getPrduct()
						.getHsnNumber() != null) {
					hsnCode = new Phrase(quotationEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
//				hsnCodeCell.addElement(hsnCode);
				hsnCodeCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(hsnCodeCell);//c3

				Phrase uom = new Phrase(quotationEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
//				uomCell.addElement(uom);
				uomCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				if(!PC_DoNotPrintUOMFlag){
					productTable.addCell(uomCell);//c4
				}

				Phrase qty = new Phrase(quotationEntity.getItems().get(i)
						.getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
//				qtyCell.addElement(qty);
				qtyCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintQtyFlag){
					productTable.addCell(qtyCell);//c5
				}

				Phrase rate = new Phrase(df.format(quotationEntity.getItems()
						.get(i).getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell(rate);
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
//				rateCell.addElement(rate);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintRateFlag){
					productTable.addCell(rateCell);//c6
				}

				double amountValue = quotationEntity.getItems().get(i)
						.getPrice()
						* quotationEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
//				productTable.addCell(amountCell);

				/**
				 * Date 8/3/2018 
				 * By jayshree
				 * Des.to print the discount
				 */
				
				boolean isAreaPresent=false;
				double areval=0;
				double totaldis=0;
				double desper=quotationEntity.getItems().get(i).getPercentageDiscount();
				try{
				 areval=Double.parseDouble(quotationEntity.getItems().get(i).getArea());
				 isAreaPresent=true;
				}
				catch(Exception e){
					isAreaPresent=false;
				}
				
				if(isAreaPresent){
					totaldis=(quotationEntity.getItems().get(i).getQty()*quotationEntity.getItems().get(i).getPrice()*areval*desper)/100
							+quotationEntity.getItems().get(i).getDiscountAmt();
				}
				else{
					totaldis=(quotationEntity.getItems().get(i).getQty()*quotationEntity.getItems().get(i).getPrice()*desper)/100+
							quotationEntity.getItems().get(i).getDiscountAmt();
				}
				
				Phrase disc = new Phrase(df.format(totaldis)
						+ "", font8);
				
				//End By jayshree
				PdfPCell discCell = new PdfPCell(disc);
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
//				discCell.addElement(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintDiscFlag){
					productTable.addCell(discCell);//c7
				}


				double asstotalAmount1 = getAssessTotalAmount(quotationEntity
						.getItems().get(i));
				System.out.println("asstotalAmount1:::::"+asstotalAmount1);
				Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
//				taxableValueCell.addElement(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				taxableValueCell.setColspan(taxCColumnolsspan);
				productTable.addCell(taxableValueCell);//c8
				
				if(!PC_DoNotPrintTaxFlag){
					
				PdfPCell cellIGST;
				boolean taxPresent = validateTaxes(quotationEntity.getItems()
						.get(i));
				if (taxPresent) {
					if (quotationEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("IGST")) {

						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						System.out.println("asstotalAmount:::::"+asstotalAmount);
						
						double taxAmount = getTaxAmount(asstotalAmount, quotationEntity
								.getItems().get(i).getVatTax().getPercentage());
						double indivTotalAmount = asstotalAmount
								+ taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(quotationEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//c9
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//c10
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//c11
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						
						totalCell.setColspan(taxCColumnolsspan);
						productTable.addCell(totalCell);//12

					} else if (quotationEntity.getItems().get(i)
							.getServiceTax().getTaxPrintName()
							.equalsIgnoreCase("IGST")) {

						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						double taxAmount = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(quotationEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						igstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.setColspan(taxCColumnolsspan);
						productTable.addCell(totalCell);

					} else {

						if (quotationEntity.getItems().get(i).getVatTax()
								.getTaxPrintName().equalsIgnoreCase("CGST")) {
							double asstotalAmount = getAssessTotalAmount(quotationEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getVatTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(quotationEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							cgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getServiceTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
							sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(quotationEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							sgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);//2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							totalCell.setColspan(taxCColumnolsspan);
							productTable.addCell(totalCell);

							// try {
							// document.add(productTable);
							// } catch (DocumentException e) {
							// e.printStackTrace();
							// }

						} else if (quotationEntity.getItems().get(i)
								.getVatTax().getTaxPrintName()
								.equalsIgnoreCase("SGST")) {
							double asstotalAmount = getAssessTotalAmount(quotationEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getServiceTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(quotationEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							cgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getVatTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
							// sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(quotationEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							sgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);///2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);

							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

						}
					}
				} else {
					logger.log(Level.SEVERE, "Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(
							df.format(getAssessTotalAmount(quotationEntity
									.getItems().get(i))) + "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
//					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if (quotationEntity.getItems().get(i).getPremisesDetails() != null) {
						premisesVal = quotationEntity.getItems().get(i)
								.getPremisesDetails();
					}
					
					
				}
				
				}
				
				/**
				 * Date 18-06-2018
				 * By Nidhi
				 * Des.changes as per process configration 
				 */
				chunk = new Phrase(" ", font7);
				PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
				Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				Pdfsrnocell2.setBorderWidthRight(0);
				
				String premises = "";
				{
					if (products.get(i).getPremisesDetails() != null
						&& !products.get(i).getPremisesDetails().equals("")) {
					premises = "Premise For " + products.get(i).getProductName()
							+ " : " + products.get(i).getPremisesDetails();
					System.out.println("products.get(i).getPremisesDetails()"+premises);
				} else {
					premises = "Premise For " + products.get(i).getProductName()+":"+"NA";
					System.out.println("products.get(i).getPremisesDetails()bbbb");
				}
				}
				if(printPremiseDetails)
				{
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
					// premisesCell.setBorderWidthRight(0);
					premisesCell.setColspan(11);
					
					noOfLines = noOfLines - 1;
					productTable.addCell(Pdfsrnocell2);
					productTable.addCell(premisesCell);
				}
				
				/**
				 * Updated By: Viraj 
				 * Date: 19-12-2018
				 * Description: To add Product description
				 */
				String desc="";
				String desc1="";
				System.out.println("desc: "+products.get(i).getPrduct().getComment());
				System.out.println("desc1: "+products.get(i).getPrduct().getCommentdesc());
				if(products.get(i).getPrduct().getComment() != null 
						&& !products.get(i).getPrduct().getComment().equals("")) {
					desc = products.get(i).getPrduct().getComment();
				}
				if(products.get(i).getPrduct().getCommentdesc() != null 
						&& !products.get(i).getPrduct().getCommentdesc().equals("")) {
					desc1 = products.get(i).getPrduct().getCommentdesc();
				}
				/**
				 * Updated By: Viraj
				 * Date: 09-01-2019
				 * Description: To add both the descriptions in single phrase
				 */
				String description = desc + " " + desc1 ;
				if(printProductDesc) {
					System.out.println("No of lines :"+ noOfLines);
					System.out.println("description: "+description);
//					if(desc != null) {
//						Phrase prodDesc = new Phrase(desc, font8);
//						PdfPCell prodDescCell = new PdfPCell(prodDesc);
//						prodDescCell.setColspan(12);
//						noOfLines = noOfLines - 1;
//						productTable.addCell(prodDescCell);
//					}	
//					System.out.println("description1: "+desc1);
//					if(desc1 != null) {
//						Phrase prodDesc1 = new Phrase(desc1, font8);
//						PdfPCell prodDesc1Cell = new PdfPCell(prodDesc1);
//						prodDesc1Cell.setColspan(12);
//						noOfLines = noOfLines - 1;
//						productTable.addCell(prodDesc1Cell);
//					}
					if(description != null) {
						Phrase prodDesc1 = new Phrase(description, font8);
						PdfPCell prodDesc1Cell = new PdfPCell(prodDesc1);
						prodDesc1Cell.setColspan(12);
						noOfLines = noOfLines - 1;
						productTable.addCell(prodDesc1Cell);
					}
					/** Ends **/
				}
				/** Ends **/
				
				/** date 10/11/2017 added by komal for vercetile chnages **/
//				  noOfLines -= 1;
//					 if(quotationEntity.getItems().get(i).getProductName().length() > productNameLength){
//						 noOfLines -= 1;
//					 }
			}
		
//			System.out.println("quotationEntity.getItems()"
//					+ quotationEntity.getItems().size());
//			PdfPCell productTableCell = null;
//			/** date 10/11/2017 added by komal for vercetile changes **/
//			if (quotationEntity.getItems().size() > productCount && productCount !=0) {
//				Phrase my = new Phrase("Please Refer Annexure For More Details");
//				productTableCell = new PdfPCell(my);
//
//			} else {
//				Phrase ph = new Phrase(" ", font8);
//				productTableCell = new PdfPCell(ph);
//			}
//
//			// PdfPCell productTableCell = new PdfPCell(blankCell);
//			// productTableCell.setBorderWidthBottom(0);
//			// productTableCell.setBorderWidthTop(0);
//			productTableCell.setBorder(0);
//
//			PdfPTable tab = new PdfPTable(1);
//			tab.setWidthPercentage(100f);
//			tab.addCell(productTableCell);
//			tab.setSpacingBefore(blankLines);

			// last code for both table to be added in one table

			
			int remainingLines = 0;
			System.out.println("noOfLines outside" + noOfLines);
			System.out.println("prouductCount" + productCount);

			if (noOfLines != 0) {
				remainingLines = 10 - (10 - noOfLines);
			}
			System.out.println("remainingLines" + remainingLines);

//			if (remainingLines != 0) {
				for (int i = 0; i < remainingLines; i++) {
					System.out.println("i----" + i);
					Phrase blankPhrase = new Phrase(" ", font10);
					PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
					blankPhraseCell.setBorder(0);
					blankPhraseCell.setColspan(12);
					blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
//					productTable.addCell(blankPhraseCell);
				}
			
			PdfPCell productTableCell = null;
//			int remainingLinesCheck = 0;
//			if (noOfLines != 0) {
//				remainingLinesCheck = 10 - (10 - noOfLines);
//			}
//			System.out.println("remainingLinesCheck" + remainingLinesCheck);
			if (noOfLines == 0 && remainingLines == 0) {
				Phrase my = new Phrase("Please Refer Annexure For More Details",
						font9bold);
				productTableCell = new PdfPCell(my);
			}
			else {
				
				Phrase my2 = new Phrase(" ",font9);
				productTableCell = new PdfPCell(my2);
			}

			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			
			
			
			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// ENd Here

		}
	
		
	}
	/**
	 * @author Vijay Date 26-08-2023
	 * Des - added by vijay to resolve isinclusive issue
	 */
	private double getAssessTotalAmount2(SalesLineItem salesLineItem) {


		// TODO Auto-generated method stub
		double totalAmount = 0;

		System.out.println("Inside getAssessTotalAmount");
		
		if(salesLineItem.isInclusive()){
			double taxVal = removeAllTaxes2(salesLineItem.getPrduct());
			double price = salesLineItem.getPrice() - taxVal;
			
			System.out.println("Price"+price);
			salesLineItem.setPrice(price);
			
		}
		
		if (salesLineItem.getArea().trim().equalsIgnoreCase("NA")
				|| salesLineItem.getArea().equalsIgnoreCase("")) {
			System.out.println("Area Not Present");
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				System.out.println("Discount Zero");
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = salesLineItem.getPrice()*salesLineItem.getQty();
				} else {
					totalAmount = (salesLineItem.getPrice()*salesLineItem.getQty())
							- salesLineItem.getDiscountAmt();
				}
			} else {
				System.out.println("Discount Not Zero");
				double disPercentAmount = getPercentAmount(salesLineItem, false);
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount = (salesLineItem.getPrice()*salesLineItem.getQty()) - disPercentAmount;
				} else {
					totalAmount = (salesLineItem.getPrice()*salesLineItem.getQty()) - disPercentAmount
							- salesLineItem.getDiscountAmt();
				}
			}
		} else {

			System.out.println("Area Present");
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				System.out.println(
						"ZERO Area Present --PercentageDiscount");
				if (salesLineItem.getDiscountAmt() == 0) {
					System.out.println(
							"ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount = salesLineItem.getPrice()
							* Double.parseDouble(salesLineItem.getArea().trim());
				} else {
					System.out.println(
							"NON ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- salesLineItem.getDiscountAmt();
				}
			} else {
				System.out.println(
						"NON ZERO Area Present --PercentageDiscount");
				double disPercentAmount = getPercentAmount(salesLineItem, true);
				if (salesLineItem.getDiscountAmt() == 0) {
					System.out.println(
							"ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount;
				} else {
					System.out.println(
							"NON ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount = (salesLineItem.getPrice() * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount - salesLineItem.getDiscountAmt();
				}
			}

		}
		System.out.println( "Assesable Value::::" + totalAmount);
		return totalAmount;
	
	}

	private void createotherCharges() {
		
		if(qp instanceof SalesOrder)
		{
			PdfPTable mainother=new PdfPTable(2);
			mainother.setWidthPercentage(100);
			try {
				mainother.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			Phrase titleph = new Phrase("Other Charges", font8bold);
			PdfPCell titlecell=new PdfPCell(titleph);
//			titlecell.setBorder(0);
			titlecell.setColspan(2);
			mainother.addCell(titlecell);
			
			PdfPTable otherChargestab1 = new PdfPTable(3);
			otherChargestab1.setWidthPercentage(100);
			try {
				otherChargestab1.setWidths(new float[]{0.9f, 1f, 0.4f});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
	
					
					
					System.out.println("inside sale quotation");
					Phrase chargename=new Phrase("Charge Name",font6bold);
					PdfPCell chargenameCell=new PdfPCell(chargename);
					chargenameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab1.addCell(chargenameCell);
					
					Phrase taxname=new Phrase("Tax",font6bold);
					PdfPCell taxnameCell=new PdfPCell(taxname);
					taxnameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab1.addCell(taxnameCell);
					
					Phrase amtph=new Phrase("Amt",font6bold);
					PdfPCell amtCell=new PdfPCell(amtph);
					amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab1.addCell(amtCell);
					
					
					
					for (int i = 0; i < soEntity.getOtherCharges().size(); i++) {
						
						if(i>2){
						break;	
						}
						System.out.println("inside so for....");
						Phrase chargenameval=new Phrase(soEntity.getOtherCharges().get(i).getOtherChargeName(),font6);
						PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
						chargenamevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						otherChargestab1.addCell(chargenamevalCell);
						
						String taxNames = " ";
						if (soEntity.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0
								&& soEntity.getOtherCharges().get(i).getTax2()
										.getPercentage() != 0) {
							taxNames = soEntity.getOtherCharges().get(i).getTax1()
									.getTaxConfigName()
									+ " / "
									+ soEntity.getOtherCharges().get(i).getTax2()
											.getTaxConfigName();
						} else {
							if (soEntity.getOtherCharges().get(i).getTax1()
									.getPercentage() != 0) {
								taxNames = soEntity.getOtherCharges().get(i).getTax1()
										.getTaxConfigName();
							} else if (soEntity.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
								taxNames = soEntity.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
							} else {
								taxNames = " ";
							}
						}
						
						Phrase taxnameval=new Phrase(taxNames,font6);
						PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
						taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab1.addCell(taxnamevalCell);
						
						Phrase amtvalph=new Phrase(soEntity.getOtherCharges().get(i).getAmount()+"",font6);
						PdfPCell amtvalCell=new PdfPCell(amtvalph);
						amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab1.addCell(amtvalCell);
						
						
						}
					
					PdfPTable otherChargestab2 = new PdfPTable(3);
					otherChargestab2.setWidthPercentage(100);
					try {
						otherChargestab2.setWidths(new float[]{0.9f, 1f, 0.4f});
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if(soEntity.getOtherCharges().size()>3){
					

					System.out.println("inside sale quotation");
					Phrase chargename2=new Phrase("Charge Name",font6bold);
					PdfPCell chargenameCell2=new PdfPCell(chargename2);
					chargenameCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab2.addCell(chargenameCell2);
					
					Phrase taxname2=new Phrase("Tax",font6bold);
					PdfPCell taxnameCell2=new PdfPCell(taxname2);
					taxnameCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab2.addCell(taxnameCell2);
					
					Phrase amtph2=new Phrase("Amt",font6bold);
					PdfPCell amtCell2=new PdfPCell(amtph2);
					amtCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab2.addCell(amtCell2);
					
					
					
					for (int i = 3; i < soEntity.getOtherCharges().size(); i++) {
						
						
						System.out.println("inside so for....");
						Phrase chargenameval=new Phrase(soEntity.getOtherCharges().get(i).getOtherChargeName(),font6);
						PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
						chargenamevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						otherChargestab2.addCell(chargenamevalCell);
						
						String taxNames = " ";
						if (soEntity.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0
								&& soEntity.getOtherCharges().get(i).getTax2()
										.getPercentage() != 0) {
							taxNames = soEntity.getOtherCharges().get(i).getTax1()
									.getTaxConfigName()
									+ "/"
									+ soEntity.getOtherCharges().get(i).getTax2()
											.getTaxConfigName();
						} else {
							if (soEntity.getOtherCharges().get(i).getTax1()
									.getPercentage() != 0) {
								taxNames = soEntity.getOtherCharges().get(i).getTax1()
										.getTaxConfigName();
							} else if (soEntity.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
								taxNames = soEntity.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
							} else {
								taxNames = " ";
							}
						}
						
						Phrase taxnameval=new Phrase(taxNames,font6);
						PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
						taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab2.addCell(taxnamevalCell);
						
						Phrase amtvalph=new Phrase(soEntity.getOtherCharges().get(i).getAmount()+"",font6);
						PdfPCell amtvalCell=new PdfPCell(amtvalph);
						amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						amtvalCell.setPaddingBottom(5);
						otherChargestab2.addCell(amtvalCell);
						
						
						}
					
				}
				
				
		PdfPCell tab1cell=new PdfPCell(otherChargestab1);
		PdfPCell tab2cell=new PdfPCell(otherChargestab2);
		
		
		if(soEntity.getOtherCharges().size()>3){
			mainother.addCell(tab1cell);
			mainother.addCell(tab2cell);
		}
		else{
			mainother.addCell(tab1cell);
			Phrase otherblank=new Phrase(" ",font6);
			PdfPCell otherCell=new PdfPCell(otherblank);
			otherCell.setBorderWidthBottom(0);
			otherCell.setBorderWidthLeft(0);
			otherCell.setBorderWidthTop(0);
			mainother.addCell(otherCell);
		}
		
		Phrase totalAndOther=new Phrase("Total OtherCharges ",font6);
		PdfPCell totalAndOtherCell=new PdfPCell(totalAndOther);
		totalAndOtherCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		totalAndOtherCell.setColspan(2);
		mainother.addCell(totalAndOtherCell);
		
		double othercharge=0;
		double totalothercharge=0;
		for (int j = 0; j < soEntity.getOtherCharges().size(); j++) {
			
		
		 othercharge=soEntity.getOtherCharges().get(j).getAmount();
		 totalothercharge=totalothercharge+othercharge;
		}
		
		
		System.out.println("totalothercharge"+soEntity.getTotalAmount());
		double amtwdothercharge=totalothercharge+soEntity.getTotalAmount();
		
		System.out.println("amtwdothercharge"+totalAmount);
		
		Phrase totalAndOtherval=new Phrase(df.format(totalothercharge),font6);
		PdfPCell totalAndOthevalrCell=new PdfPCell(totalAndOtherval);
		totalAndOthevalrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		totalAndOthevalrCell.setColspan(2);
		mainother.addCell(totalAndOthevalrCell);
		
		if(soEntity.getOtherCharges().size()!=0){
		try {
			document.add(mainother);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}		
		}
		else{

			PdfPTable mainother=new PdfPTable(2);
			mainother.setWidthPercentage(100);
			try {
				mainother.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			Phrase titleph = new Phrase("Other Charges", font8bold);
			PdfPCell titlecell=new PdfPCell(titleph);
//			titlecell.setBorder(0);
			titlecell.setColspan(2);
			mainother.addCell(titlecell);
			
			PdfPTable otherChargestab1 = new PdfPTable(3);
			otherChargestab1.setWidthPercentage(100);
			try {
				otherChargestab1.setWidths(new float[]{0.9f, 1f, 0.4f});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
					
	
					
					
					System.out.println("inside sale quotation");
					Phrase chargename=new Phrase("Charge Name",font6bold);
					PdfPCell chargenameCell=new PdfPCell(chargename);
					chargenameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab1.addCell(chargenameCell);
					
					Phrase taxname=new Phrase("Tax",font6bold);
					PdfPCell taxnameCell=new PdfPCell(taxname);
					taxnameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab1.addCell(taxnameCell);
					
					Phrase amtph=new Phrase("Amt",font6bold);
					PdfPCell amtCell=new PdfPCell(amtph);
					amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab1.addCell(amtCell);
					
					
					for (int i = 0; i < salesquotEntity.getOtherCharges().size(); i++) {
						
						if(i>2){
							break;	
							}
						System.out.println("inside so for....");
						Phrase chargenameval=new Phrase(salesquotEntity.getOtherCharges().get(i).getOtherChargeName(),font6);
						PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
						chargenamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab1.addCell(chargenamevalCell);
						
						String taxNames = " ";
						if (salesquotEntity.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0
								&& salesquotEntity.getOtherCharges().get(i).getTax2()
										.getPercentage() != 0) {
							taxNames = salesquotEntity.getOtherCharges().get(i).getTax1()
									.getTaxConfigName()
									+ "/"
									+ salesquotEntity.getOtherCharges().get(i).getTax2()
											.getTaxConfigName();
						} else {
							if (salesquotEntity.getOtherCharges().get(i).getTax1()
									.getPercentage() != 0) {
								taxNames = salesquotEntity.getOtherCharges().get(i).getTax1()
										.getTaxConfigName();
							} else if (salesquotEntity.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
								taxNames = salesquotEntity.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
							} else {
								taxNames = " ";
							}
						}
						
						Phrase taxnameval=new Phrase(taxNames,font6);
						PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
						taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab1.addCell(taxnamevalCell);
						
						Phrase amtvalph=new Phrase(salesquotEntity.getOtherCharges().get(i).getAmount()+"",font6);
						PdfPCell amtvalCell=new PdfPCell(amtvalph);
						amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab1.addCell(amtvalCell);
						}
						
						
					
					PdfPTable otherChargestab2 = new PdfPTable(3);
					otherChargestab2.setWidthPercentage(100);
					try {
						otherChargestab2.setWidths(new float[]{0.9f, 1f, 0.4f});
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				if(salesquotEntity.getOtherCharges().size()>3){
					

					System.out.println("inside sale quotation");
					Phrase chargename2=new Phrase("Charge Name",font6bold);
					PdfPCell chargenameCell2=new PdfPCell(chargename2);
					chargenameCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab2.addCell(chargenameCell2);
					
					Phrase taxname2=new Phrase("Tax",font6bold);
					PdfPCell taxnameCell2=new PdfPCell(taxname2);
					taxnameCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab2.addCell(taxnameCell2);
					
					Phrase amtph2=new Phrase("Amt",font6bold);
					PdfPCell amtCell2=new PdfPCell(amtph2);
					amtCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherChargestab2.addCell(amtCell2);
					
					
					
					for (int i = 4; i < salesquotEntity.getOtherCharges().size(); i++) {
						
						
						System.out.println("inside so for....");
						Phrase chargenameval=new Phrase(salesquotEntity.getOtherCharges().get(i).getOtherChargeName(),font6);
						PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
						chargenamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab2.addCell(chargenamevalCell);
						
						String taxNames = " ";
						if (salesquotEntity.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0
								&& salesquotEntity.getOtherCharges().get(i).getTax2()
										.getPercentage() != 0) {
							taxNames = salesquotEntity.getOtherCharges().get(i).getTax1()
									.getTaxConfigName()
									+ "/"
									+ salesquotEntity.getOtherCharges().get(i).getTax2()
											.getTaxConfigName();
						} else {
							if (salesquotEntity.getOtherCharges().get(i).getTax1()
									.getPercentage() != 0) {
								taxNames = salesquotEntity.getOtherCharges().get(i).getTax1()
										.getTaxConfigName();
							} else if (salesquotEntity.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
								taxNames = salesquotEntity.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
							} else {
								taxNames = " ";
							}
						}
						
						Phrase taxnameval=new Phrase(taxNames,font6);
						PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
						taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab2.addCell(taxnamevalCell);
						
						Phrase amtvalph=new Phrase(salesquotEntity.getOtherCharges().get(i).getAmount()+"",font6);
						PdfPCell amtvalCell=new PdfPCell(amtvalph);
						amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						otherChargestab2.addCell(amtvalCell);
						
						
						}
					
				}
				
				
		PdfPCell tab1cell=new PdfPCell(otherChargestab1);
		PdfPCell tab2cell=new PdfPCell(otherChargestab2);
		
		if(salesquotEntity.getOtherCharges().size()>3){
			mainother.addCell(tab1cell);
			mainother.addCell(tab2cell);
		}
		else{
			mainother.addCell(tab1cell);
			Phrase otherblank=new Phrase(" ",font6);
			PdfPCell otherCell=new PdfPCell(otherblank);
			otherCell.setBorder(0);
			mainother.addCell(otherCell);
		}
		
		Phrase totalAndOther=new Phrase("Total OtherCharges ",font6);
		PdfPCell totalAndOtherCell=new PdfPCell(totalAndOther);
		totalAndOtherCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		totalAndOtherCell.setColspan(2);
		mainother.addCell(totalAndOtherCell);
		
		double othercharge=0;
		double totalothercharge=0;
		for (int j = 0; j < salesquotEntity.getOtherCharges().size(); j++) {
			
		
		 othercharge=salesquotEntity.getOtherCharges().get(j).getAmount();
		 totalothercharge=totalothercharge+othercharge;
		}
		
		
		System.out.println("totalothercharge"+salesquotEntity.getTotalAmount());
		double amtwdothercharge=totalothercharge+salesquotEntity.getTotalAmount();
		
		System.out.println("amtwdothercharge"+totalAmount);
		
		Phrase totalAndOtherval=new Phrase(df.format(totalothercharge),font6);
		PdfPCell totalAndOthevalrCell=new PdfPCell(totalAndOtherval);
		totalAndOthevalrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		totalAndOthevalrCell.setColspan(2);
		mainother.addCell(totalAndOthevalrCell);
		
		if(salesquotEntity.getOtherCharges().size()!=0){
		
		try {
			document.add(mainother);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
		
	}		
		
		
	}
	
	private void createFooterTaxPart2(SalesQuotation salesQuotation) {

		SalesQuotation qp=salesQuotation;
		float[] columnMoreLeftWidths = { 2f, 1f };
		PdfPTable pdfPTaxTable = new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

//		/**
//		 * rohan added this code for payment terms for invoice details
//		 */
//		
//		
//		float[] column3widths = { 2f, 2f, 6f };
//		PdfPTable leftTable = new PdfPTable(3);
//		leftTable.setWidthPercentage(100);
//		try {
//			leftTable.setWidths(column3widths);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		
//		/** date 9/11/2017 added by komal from payment terms table header **/
//		//Table name 
//		Phrase paymentTerms = new Phrase("Payment Terms", font10bold);
//		PdfPCell paymentTermsCell = new PdfPCell(paymentTerms);
//		paymentTermsCell.setColspan(3);
//		paymentTermsCell.setBorder(0);
//		
//		leftTable.addCell(paymentTermsCell);
//		// heading
//
//		Phrase day = new Phrase("Day", font8bold);
//		PdfPCell dayCell = new PdfPCell(day);
//		dayCell.setBorder(0);
//
//		Phrase percent = new Phrase("Percent", font8bold);
//		PdfPCell percentCell = new PdfPCell(percent);
//		percentCell.setBorder(0);
//
//		Phrase comment = new Phrase("Comment", font8bold);
//		PdfPCell commentCell = new PdfPCell(comment);
//		commentCell.setBorder(0);
//
//		leftTable.addCell(dayCell);
//		leftTable.addCell(percentCell);
//		leftTable.addCell(commentCell);
//
//		// Values
//		/** date 9/11/2017 added by komal to add minimum 12 entries **/
//		int size = 12 - qp.getPaymentTermsList().size();
//		for (int i = 0; i < qp.getPaymentTermsList().size(); i++) {
//			Phrase dayValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermDays()
//					+ "", font8);
//			PdfPCell dayValueCell = new PdfPCell(dayValue);
//			dayValueCell.setBorder(0);
//			leftTable.addCell(dayValueCell);
//
//			Phrase percentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermPercent()
//					+ "", font8);
//			PdfPCell percentValueCell = new PdfPCell(percentValue);
//			percentValueCell.setBorder(0);
//			leftTable.addCell(percentValueCell);
//
//			Phrase commentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermComment(), font8);
//			PdfPCell commentValueCell = new PdfPCell(commentValue);
//			commentValueCell.setBorder(0);
//			leftTable.addCell(commentValueCell);
//		}
		
		// try {
		// document.add(leftTable);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
//komal
		Phrase blankval = new Phrase(" ", font8);
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase blank = new Phrase("", font8);
		PdfPCell bcell = new PdfPCell(blank);

		Phrase payphrase = new Phrase("Payment Terms", font8bold);
		payphrase.add(Chunk.NEWLINE);
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);

		PdfPCell tablecell1 = new PdfPCell();
		System.out.println("b4 table 11" + this.payTermsLis.size());
		if (this.payTermsLis.size() > 6) {
			System.out.println("af table 11" + this.payTermsLis.size());
			tablecell1 = new PdfPCell(table);
			// tablecell1.setBorder(0);
		} else {
			System.out.println(" table 11" + this.payTermsLis.size());
			tablecell1 = new PdfPCell(table);
			tablecell1.setBorder(0);
		}

		Phrase paytermdays = new Phrase("Days", font6bold);
		Phrase paytermpercent = new Phrase("Percent", font6bold);
		Phrase paytermcomment = new Phrase("Comment", font6bold);

		PdfPCell headingpayterms = new PdfPCell(payphrase);
		headingpayterms.setBorder(0);
		headingpayterms.setColspan(3);
		PdfPCell celldays = new PdfPCell(paytermdays);
		celldays.setBorder(0);
		celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellpercent = new PdfPCell(paytermpercent);
		cellpercent.setBorder(0);
		cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcomment = new PdfPCell(paytermcomment);
		cellcomment.setBorder(0);
		cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(celldays);
		table.addCell(cellpercent);
		table.addCell(cellcomment);

		if (this.payTermsLis.size() <= BreakPoint) {
			int size = BreakPoint - this.payTermsLis.size();
			blankLines = size * (60 / 6);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 0;
		}
		table.setSpacingAfter(blankLines);

		for (int i = 0; i < this.payTermsLis.size(); i++) {

			Phrase chunk = null;
		
			if (payTermsLis.get(i).getPayTermDays() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
						font6);
			} else {
				chunk = new Phrase(" ", font6);
			}

			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (payTermsLis.get(i).getPayTermPercent() != 0) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
						font6);
			} else {
				chunk = new Phrase(" ", font6);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (payTermsLis.get(i).getPayTermComment() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
						.trim(), font6);
			} else {
				chunk = new Phrase(" ", font6);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);

			count = i;
			if (count == 5 || count == BreakPoint) {

				flag1 = BreakPoint;
				break;
			}
		}

		// /2nd table for pay terms start
		System.out.println(this.payTermsLis.size() + "  out");
		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);

		PdfPCell table1cell = new PdfPCell();

		if (this.payTermsLis.size() > 6) {
			table1cell = new PdfPCell(table1);
			// table1cell.setBorder(0);
		} else {
			table1cell = new PdfPCell(blankval);
			table1cell.setBorder(0);
		}

		Phrase paytermdays1 = new Phrase("Days", font6bold);
		Phrase paytermpercent1 = new Phrase("Percent", font6bold);
		Phrase paytermcomment1 = new Phrase("Comment", font6bold);

		PdfPCell celldays1;
		PdfPCell cellpercent1;
		PdfPCell cellcomment1;

		System.out.println(this.payTermsLis.size() + " ...........b4 if");
		if (this.payTermsLis.size() > 5) {
			System.out.println(this.payTermsLis.size() + " ...........af if");
			celldays1 = new PdfPCell(paytermdays1);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			celldays1 = new PdfPCell(blankval);
			celldays1.setBorder(0);

		}

		if (this.payTermsLis.size() > 5) {

			cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellpercent1 = new PdfPCell(blankval);
			cellpercent1.setBorder(0);

		}

		if (this.payTermsLis.size() > 5) {
			cellcomment1 = new PdfPCell(paytermcomment1);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellcomment1 = new PdfPCell(blankval);
			cellcomment1.setBorder(0);
		}
		table1.addCell(celldays1);
		table1.addCell(cellpercent1);
		table1.addCell(cellcomment1);

		for (int i = 6; i < this.payTermsLis.size(); i++) {
			System.out.println(this.payTermsLis.size() + "  in for");

			Phrase chunk = null;
			if (payTermsLis.get(i).getPayTermDays() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
						font6);
			} else {
				chunk = new Phrase(" ", font6);
			}

			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (payTermsLis.get(i).getPayTermPercent() != 0) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
						font6);
			} else {
				chunk = new Phrase(" ", font6);
			}

			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if (payTermsLis.get(i).getPayTermComment() != null) {
				chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
						.trim(), font6);
			} else {
				chunk = new Phrase(" ", font6);
			}
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			table1.addCell(pdfdayscell);
			table1.addCell(pdfpercentcell);
			table1.addCell(pdfcommentcell);

			if (i == 11) {

				break;
			}

		}

		// /2nd table for pay terms end

		PdfPTable termstable1 = new PdfPTable(2);
		termstable1.setWidthPercentage(100);
		termstable1.addCell(headingpayterms);
		termstable1.addCell(tablecell1);

		termstable1.addCell(table1cell);


		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		/**
		 * Date 23/3/2018
		 * By jayshree
		 * Des.add the other charges
		 */
		createOtherCharges();
		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(new float[]{0.9f, 1f, 0.4f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				System.out.println("inside sale quotation");
				Phrase chargename=new Phrase("Charge Name",font6bold);
				PdfPCell chargenameCell=new PdfPCell(chargename);
				chargenameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(chargenameCell);
				
				Phrase taxname=new Phrase("Tax",font6bold);
				PdfPCell taxnameCell=new PdfPCell(taxname);
				taxnameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(taxnameCell);
				
				Phrase amtph=new Phrase("Amt",font6bold);
				PdfPCell amtCell=new PdfPCell(amtph);
				amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(amtCell);
				
				for (int i = 0; i < qp.getOtherCharges().size(); i++) {
				
					System.out.println("inside so for....");
					Phrase chargenameval=new Phrase(qp.getOtherCharges().get(i).getOtherChargeName(),font6);
					PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
					chargenamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(chargenamevalCell);
					
					String taxNames = " ";
					if (qp.getOtherCharges().get(i).getTax1()
							.getPercentage() != 0
							&& qp.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
						taxNames = qp.getOtherCharges().get(i).getTax1()
								.getTaxConfigName()
								+ "/"
								+ qp.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
					} else {
						if (qp.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax1()
									.getTaxConfigName();
						} else if (qp.getOtherCharges().get(i).getTax2()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax2()
									.getTaxConfigName();
						} else {
							taxNames = " ";
						}
					}
					
					Phrase taxnameval=new Phrase(taxNames,font6);
					PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
					taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(taxnamevalCell);
					
					Phrase amtvalph=new Phrase(qp.getOtherCharges().get(i).getAmount()+"",font6);
					PdfPCell amtvalCell=new PdfPCell(amtvalph);
					amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(amtvalCell);
					
					}
				
			
		PdfPCell upperRightCell = new PdfPCell(otherCharges);
		upperRightCell.setBorder(0);
		
//		if(qp.getOtherCharges().size()!=0){
//		System.out.println("soEntity.getOtherCharges().size()"+qp.getOtherCharges().size());
////		rightTable.addCell(upperRightCell);
//		}
//			
		//End By Jayshree
		
		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.3f };
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
//		colonCell.addElement(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax", font6bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		// totalAmount
		Phrase amtB4TaxValphrase = new Phrase("", font6bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < qp.getProductTaxes().size(); i++) {
			if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ qp.getProductTaxes().get(i)
								.getChargePayable();
				Phrase IGSTphrase = new Phrase("IGST @"
						+ qp.getProductTaxes().get(i)
								.getChargePercent() + " %", font6bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorder(0);

				Phrase IGSTValphrase = new Phrase(df.format(qp.getProductTaxes().get(i).getChargePayable())
						+ "", font6);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(IGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(IGSTValphraseCell);

			} else if (qp.getProductTaxes().get(i)
					.getChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ qp.getProductTaxes().get(i)
								.getChargePayable();

				Phrase SGSTphrase = new Phrase("SGST @"
						+ qp.getProductTaxes().get(i)
								.getChargePercent() + " %", font6bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorder(0);
				// SGSTphraseCell.addElement(SGSTphrase);

				Phrase SGSTValphrase = new Phrase(df.format(qp.getProductTaxes().get(i).getChargePayable())
						+ "", font6);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(SGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(SGSTValphraseCell);
			} else if (qp.getProductTaxes().get(i)
					.getChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ qp.getProductTaxes().get(i)
								.getChargePayable();

				Phrase CGSTphrase = new Phrase("CGST @"
						+ qp.getProductTaxes().get(i)
								.getChargePercent() + " %", font6bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				// CGSTphraseCell.addElement(CGSTphrase);

				Phrase CGSTValphrase = new Phrase(df.format(qp.getProductTaxes().get(i).getChargePayable())
						+ "", font6);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				rightInnerTable.addCell(CGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(CGSTValphraseCell);
			}
		}

		

		Phrase GSTphrase = new Phrase("Total GST", font6bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		// GSTphraseCell.addElement(GSTphrase);

		double totalGSTValue = igstTotalVal + sgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
				font6bold);
		// Paragraph GSTValphrasePara=new Paragraph();
		// GSTValphrasePara.add(GSTValphrase);
		// GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// GSTValphraseCell.addElement(GSTValphrasePara);

		
		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		
		/**
		 * Date 09-06-2018
		 * Developer :- Vijay
		 * Des :- For round off amt display if exist
		 */
		
		double roundOffAmt = 0;
		if(qp.getRoundOffAmount()!=0){
			roundOffAmt = qp.getRoundOffAmount();
		}
		boolean rountoff = false;
		if (roundOffAmt != 0) {
			rountoff = true;
		}
		if (rountoff == true) {
			Phrase rountoffPhrase = new Phrase("Round Off   " + "", font1);
			PdfPCell rountoffCell = new PdfPCell(rountoffPhrase);
			rountoffCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			rountoffCell.setBorder(0);
			rightInnerTable.addCell(rountoffCell);
			
			rightInnerTable.addCell(colonCell);

			Phrase roundoffValPhrase = new Phrase(df.format(roundOffAmt) + "", font1);
			PdfPCell roundoffValCell = new PdfPCell(roundoffValPhrase);
			roundoffValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			roundoffValCell.setBorder(0);
			rightInnerTable.addCell(roundoffValCell);

		}
		/**
		 * end shere
		 */
		
		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
//		innerRightCell.addElement(rightInnerTable);

		rightTable.addCell(innerRightCell);
		
		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
       

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);
//		rightCell.addElement(rightTable);

		PdfPCell leftCell = new PdfPCell(termstable1);
		// leftCell.setBorder(0);
//		leftCell.addElement(termstable1);

		Phrase total=new Phrase("Total",font6bold);
		PdfPCell totalcell=new PdfPCell(total);
		totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pdfPTaxTable.addCell(totalcell);
		
		
		/**
		 * Date 13/06/2018 by vijay
		 * Des below total showing net payable which is wrong so i have updated the code 
		 */
//		Phrase totalval=new Phrase(df.format(salesQuotation.getNetpayable()),font6bold);

		Phrase totalval=new Phrase(df.format(salesQuotation.getTotalAmount()),font6bold);
		
		/**
		 * ends here
		 */
		
		PdfPCell totalcellval=new PdfPCell(totalval);
		totalcellval.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pdfPTaxTable.addCell(totalcellval);
		
		
		
		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
		
		
	}
	
	private void createFooterTaxPartSalesOrder2(SalesOrder salesOrder) {

		SalesOrder qp=salesOrder;
		float[] columnMoreLeftWidths = { 2f, 1f };
		
		PdfPTable pdfPTaxTable = new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

//		float[] column3widths = { 2f, 2f, 6f };
//		PdfPTable leftTable = new PdfPTable(3);
//		leftTable.setWidthPercentage(100);
//		try {
//			leftTable.setWidths(column3widths);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//		// heading
//
//		Phrase day = new Phrase("Day", font8bold);
//		PdfPCell dayCell = new PdfPCell(day);
//		dayCell.setBorder(0);
//
//		Phrase percent = new Phrase("Percent", font8bold);
//		PdfPCell percentCell = new PdfPCell(percent);
//		percentCell.setBorder(0);
//
//		Phrase comment = new Phrase("Comment", font8bold);
//		PdfPCell commentCell = new PdfPCell(comment);
//		commentCell.setBorder(0);
//
//		leftTable.addCell(dayCell);
//		leftTable.addCell(percentCell);
//		leftTable.addCell(commentCell);
//
//		// Values
//		for (int i = 0; i < qp.getPaymentTermsList().size(); i++) {
//			Phrase dayValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermDays()
//					+ "", font8);
//			PdfPCell dayValueCell = new PdfPCell(dayValue);
//			dayValueCell.setBorder(0);
//			leftTable.addCell(dayValueCell);
//
//			Phrase percentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermPercent()
//					+ "", font8);
//			PdfPCell percentValueCell = new PdfPCell(percentValue);
//			percentValueCell.setBorder(0);
//			leftTable.addCell(percentValueCell);
//
//			Phrase commentValue = new Phrase(qp.getPaymentTermsList().get(i)
//					.getPayTermComment(), font8);
//			PdfPCell commentValueCell = new PdfPCell(commentValue);
//			commentValueCell.setBorder(0);
//			leftTable.addCell(commentValueCell);
//		}
//
//		// try {
//		// document.add(leftTable);
//		// } catch (DocumentException e1) {
//		// e1.printStackTrace();
//		// }

		/** date 10/11/2017 added by komal for vercetile changes **/
				Phrase blankval = new Phrase(" ", font8);
				Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
				Phrase blank = new Phrase("", font8);
				PdfPCell bcell = new PdfPCell(blank);

				Phrase payphrase = new Phrase("Payment Terms", font8bold);
				payphrase.add(Chunk.NEWLINE);
				PdfPTable table = new PdfPTable(3);
				table.setWidthPercentage(100);

				PdfPCell tablecell1 = new PdfPCell();
				System.out.println("b4 table 11" + this.payTermsLis.size());
				if (this.payTermsLis.size() > 6) {
					System.out.println("af table 11" + this.payTermsLis.size());
					tablecell1 = new PdfPCell(table);
					// tablecell1.setBorder(0);
				} else {
					System.out.println(" table 11" + this.payTermsLis.size());
					tablecell1 = new PdfPCell(table);
					tablecell1.setBorder(0);
				}

				Phrase paytermdays = new Phrase("Days", font6bold);
				Phrase paytermpercent = new Phrase("Percent", font6bold);
				Phrase paytermcomment = new Phrase("Comment", font6bold);

				PdfPCell headingpayterms = new PdfPCell(payphrase);
				headingpayterms.setBorder(0);
				headingpayterms.setColspan(3);
				
				PdfPCell celldays = new PdfPCell(paytermdays);
				celldays.setBorder(0);
				celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
				PdfPCell cellpercent = new PdfPCell(paytermpercent);
				cellpercent.setBorder(0);
				cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
				PdfPCell cellcomment = new PdfPCell(paytermcomment);
				cellcomment.setBorder(0);
				cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);

				table.addCell(celldays);
				table.addCell(cellpercent);
				table.addCell(cellcomment);

				if (this.payTermsLis.size() <= BreakPoint) {
					int size = BreakPoint - this.payTermsLis.size();
					blankLines = size * (60 / 6);
					System.out.println("blankLines size =" + blankLines);
					System.out.println("blankLines size =" + blankLines);
				} else {
					blankLines = 0;
				}
				table.setSpacingAfter(blankLines);

				// PdfPTable termsTable=new PdfPTable(1);
				// termsTable.setWidthPercentage(100);
				// termsTable.setSpacingAfter(80f);
				// termsTable.addCell(termstitlecell);
				// if(qp instanceof SalesQuotation){
				// termsTable.addCell(validitydatecell);
				// }
				//
				// termsTable.addCell(desccell);
				// termsTable.addCell(headingpayterms);
				// termsTable.addCell(pdfdayscell);

				for (int i = 0; i < this.payTermsLis.size(); i++) {

					Phrase chunk = null;
					// Phrase chunk=new
					// Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
					// PdfPCell pdfdayscell = new PdfPCell(chunk);
					// pdfdayscell.setBorder(0);
					if (payTermsLis.get(i).getPayTermDays() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfdayscell = new PdfPCell(chunk);
					pdfdayscell.setBorder(0);
					pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (payTermsLis.get(i).getPayTermPercent() != 0) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfpercentcell = new PdfPCell(chunk);
					pdfpercentcell.setBorder(0);
					pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					if (payTermsLis.get(i).getPayTermComment() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
								.trim(), font6);
					} else {
						chunk = new Phrase(" ", font6);
					}
					PdfPCell pdfcommentcell = new PdfPCell(chunk);
					pdfcommentcell.setBorder(0);
					pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table.addCell(pdfdayscell);
					table.addCell(pdfpercentcell);
					table.addCell(pdfcommentcell);

					count = i;
					// /
					if (count == 5 || count == BreakPoint) {

						flag1 = BreakPoint;
						break;
					}
				}

				// /2nd table for pay terms start
				System.out.println(this.payTermsLis.size() + "  out");

				// if(this.payTermsLis.size()>4){

				PdfPTable table1 = new PdfPTable(3);
				table1.setWidthPercentage(100);

				PdfPCell table1cell = new PdfPCell();

				if (this.payTermsLis.size() > 6) {
					table1cell = new PdfPCell(table1);
					// table1cell.setBorder(0);
				} else {
					table1cell = new PdfPCell(blankval);
					table1cell.setBorder(0);
				}

				Phrase paytermdays1 = new Phrase("Days", font6bold);
				Phrase paytermpercent1 = new Phrase("Percent", font6bold);
				Phrase paytermcomment1 = new Phrase("Comment", font6bold);

				PdfPCell celldays1;
				PdfPCell cellpercent1;
				PdfPCell cellcomment1;

				System.out.println(this.payTermsLis.size() + " ...........b4 if");
				if (this.payTermsLis.size() > 5) {
					System.out.println(this.payTermsLis.size() + " ...........af if");
					celldays1 = new PdfPCell(paytermdays1);
					celldays1.setBorder(0);
					celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					celldays1 = new PdfPCell(blankval);
					celldays1.setBorder(0);

				}

				if (this.payTermsLis.size() > 5) {

					cellpercent1 = new PdfPCell(paytermpercent1);
					cellpercent1.setBorder(0);
					cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					cellpercent1 = new PdfPCell(blankval);
					cellpercent1.setBorder(0);

				}

				if (this.payTermsLis.size() > 5) {
					cellcomment1 = new PdfPCell(paytermcomment1);
					cellcomment1.setBorder(0);
					cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					cellcomment1 = new PdfPCell(blankval);
					cellcomment1.setBorder(0);
				}
				table1.addCell(celldays1);
				table1.addCell(cellpercent1);
				table1.addCell(cellcomment1);

				for (int i = 6; i < this.payTermsLis.size(); i++) {
					System.out.println(this.payTermsLis.size() + "  in for");

					Phrase chunk = null;
					if (payTermsLis.get(i).getPayTermDays() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfdayscell = new PdfPCell(chunk);
					pdfdayscell.setBorder(0);
					pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

					if (payTermsLis.get(i).getPayTermPercent() != 0) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
								font6);
					} else {
						chunk = new Phrase(" ", font6);
					}

					PdfPCell pdfpercentcell = new PdfPCell(chunk);
					pdfpercentcell.setBorder(0);
					pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					if (payTermsLis.get(i).getPayTermComment() != null) {
						chunk = new Phrase(payTermsLis.get(i).getPayTermComment()
								.trim(), font6);
					} else {
						chunk = new Phrase(" ", font6);
					}
					PdfPCell pdfcommentcell = new PdfPCell(chunk);
					pdfcommentcell.setBorder(0);
					pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

					table1.addCell(pdfdayscell);
					table1.addCell(pdfpercentcell);
					table1.addCell(pdfcommentcell);

					if (i == 11) {

						break;
					}

				}

				// /2nd table for pay terms end

				PdfPTable termstable1 = new PdfPTable(2);
				termstable1.setWidthPercentage(100);
				termstable1.addCell(headingpayterms);
				termstable1.addCell(tablecell1);

				termstable1.addCell(table1cell);
				
		
				
				
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		/**
		 * Date 23/3/2018
		 * By jayshree
		 * Des.add the other charges
		 */
//		createOtherCharges();
		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(new float[]{0.9f, 1f, 0.4f});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				System.out.println("inside sale order");
				Phrase chargename=new Phrase("Charge Name",font6bold);
				PdfPCell chargenameCell=new PdfPCell(chargename);
				chargenameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(chargenameCell);
				
				Phrase taxname=new Phrase("Tax",font6bold);
				PdfPCell taxnameCell=new PdfPCell(taxname);
				taxnameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(taxnameCell);
				
				Phrase amtph=new Phrase("Amt",font6bold);
				PdfPCell amtCell=new PdfPCell(amtph);
				amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				otherCharges.addCell(amtCell);
				
				for (int i = 0; i < qp.getOtherCharges().size(); i++) {
				
					System.out.println("inside so for....");
					Phrase chargenameval=new Phrase(qp.getOtherCharges().get(i).getOtherChargeName(),font6);
					PdfPCell chargenamevalCell=new PdfPCell(chargenameval);
					chargenamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(chargenamevalCell);
					
					String taxNames = " ";
					if (qp.getOtherCharges().get(i).getTax1()
							.getPercentage() != 0
							&& qp.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
						taxNames = qp.getOtherCharges().get(i).getTax1()
								.getTaxConfigName()
								+ "/"
								+ qp.getOtherCharges().get(i).getTax2()
										.getTaxConfigName();
					} else {
						if (qp.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax1()
									.getTaxConfigName();
						} else if (qp.getOtherCharges().get(i).getTax2()
								.getPercentage() != 0) {
							taxNames = qp.getOtherCharges().get(i).getTax2()
									.getTaxConfigName();
						} else {
							taxNames = " ";
						}
					}
					
					Phrase taxnameval=new Phrase(taxNames,font6);
					PdfPCell taxnamevalCell=new PdfPCell(taxnameval);
					taxnamevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(taxnamevalCell);
					
					Phrase amtvalph=new Phrase(qp.getOtherCharges().get(i).getAmount()+"",font6);
					PdfPCell amtvalCell=new PdfPCell(amtvalph);
					amtvalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					otherCharges.addCell(amtvalCell);
					
					}
				
			
		PdfPCell upperRightCell = new PdfPCell(otherCharges);
		upperRightCell.setBorder(0);
//		upperRightCell.addElement(otherCharges);
		
//		if(qp.getOtherCharges().size()!=0){
//		System.out.println("soEntity.getOtherCharges().size()"+qp.getOtherCharges().size());
//		rightTable.addCell(upperRightCell);
//		}
			
		//End By Jayshree
		
		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.3f };
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
//		colonCell.addElement(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax", font6bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		// totalAmount
		Phrase amtB4TaxValphrase = new Phrase("", font6bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < qp.getProductTaxes().size(); i++) {
			System.out.println("tax table"+qp.getProductTaxes().size());
			if (qp.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal+ qp.getProductTaxes().get(i).getChargePayable();
				Phrase IGSTphrase = new Phrase("IGST @"+ qp.getProductTaxes().get(i).getChargePercent() + " %", font6bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorder(0);

				Phrase IGSTValphrase = new Phrase(df.format(qp.getProductTaxes().get(i).getChargePayable())+ "", font6);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(IGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(IGSTValphraseCell);

			} else if (qp.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("SGST")) {
				System.out.println("inside sgst "+qp.getProductTaxes().get(i).getChargeName());
				sgstTotalVal = sgstTotalVal+ qp.getProductTaxes().get(i).getChargePayable();

				Phrase SGSTphrase = new Phrase("SGST @"+ qp.getProductTaxes().get(i).getChargePercent() + " %", font6bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorder(0);
				// SGSTphraseCell.addElement(SGSTphrase);

				Phrase SGSTValphrase = new Phrase(df.format(qp.getProductTaxes().get(i).getChargePayable())	+ "", font6);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(SGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(SGSTValphraseCell);
			} else if (qp.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal+ qp.getProductTaxes().get(i).getChargePayable();

				Phrase CGSTphrase = new Phrase("CGST @ "+ qp.getProductTaxes().get(i).getChargePercent() + " %", font6bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				// CGSTphraseCell.addElement(CGSTphrase);

				Phrase CGSTValphrase = new Phrase(df.format(qp.getProductTaxes().get(i).getChargePayable())+ "", font6);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				rightInnerTable.addCell(CGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(CGSTValphraseCell);
			}
		}

		
		Phrase GSTphrase = new Phrase("Total GST", font6bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		// GSTphraseCell.addElement(GSTphrase);

		double totalGSTValue = igstTotalVal + sgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",font6bold);
		// Paragraph GSTValphrasePara=new Paragraph();
		// GSTValphrasePara.add(GSTValphrase);
		// GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// GSTValphraseCell.addElement(GSTValphrasePara);

		
		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		/**
		 * Date 09-06-2018
		 * Developer :- Vijay
		 * Des :- For round off amt display if exist
		 */
		
		double roundOffAmt = 0;
		if(qp.getRoundOffAmount()!=0){
			roundOffAmt = qp.getRoundOffAmount();
		}
		boolean rountoff = false;
		if (roundOffAmt != 0) {
			rountoff = true;
		}
		if (rountoff == true) {
			Phrase rountoffPhrase = new Phrase("Round Off   " + "", font1);
			PdfPCell rountoffCell = new PdfPCell(rountoffPhrase);
			rountoffCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			rountoffCell.setBorder(0);
			rightInnerTable.addCell(rountoffCell);
			
			rightInnerTable.addCell(colonCell);

			Phrase roundoffValPhrase = new Phrase(df.format(roundOffAmt) + "", font1);
			PdfPCell roundoffValCell = new PdfPCell(roundoffValPhrase);
			roundoffValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			roundoffValCell.setBorder(0);
			rightInnerTable.addCell(roundoffValCell);

		}
		/**
		 * end shere
		 */
		
		
		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
//		innerRightCell.addElement(rightInnerTable);

		
		rightTable.addCell(innerRightCell);

		PdfPCell rightCell = new PdfPCell(rightTable);//date 23/3/2018 comment  by jayshree 
		// rightCell.setBorder(0);
//		rightCell.addElement(rightTable);

		PdfPCell leftCell = new PdfPCell(termstable1);
		// leftCell.setBorder(0);
//		leftCell.addElement(termstable1);

		
		
		Phrase total=new Phrase("Total",font6bold);
		PdfPCell totalcell=new PdfPCell(total);
		totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pdfPTaxTable.addCell(totalcell);
		
		/**
		 * Date 13/06/2018 by vijay
		 * Des below total showing net payable which is wrong so i have updated the code 
		 */
//		Phrase totalval=new Phrase(df.format(soEntity.getNetpayable()),font6bold);

		Phrase totalval=new Phrase(df.format(soEntity.getTotalAmount()),font6bold);
		
		/**
		 * ends here
		 */
		
		PdfPCell totalcellval=new PdfPCell(totalval);
		totalcellval.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pdfPTaxTable.addCell(totalcellval);
		
		
		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
		
	}
	private void createtermsAndCondition() {

		PdfPTable bottomTable = new PdfPTable(2);
		bottomTable.setWidthPercentage(100);
		float[] columnMoreLeftWidths = { 2f, 1f };
		try {
			bottomTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		String amtInWordsVal = "";
		String comment = "";
		double netpayable = 0;
		if (qp instanceof SalesOrder) {
			SalesOrder soEntity = (SalesOrder) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(soEntity.getNetpayable());
			netpayable = soEntity.getNetpayable();
			if (soEntity.getDescription() != null) {
				comment = soEntity.getDescription();
			}
		} else {
			SalesQuotation quotation = (SalesQuotation) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(quotation.getNetpayable());
			netpayable = quotation.getNetpayable();
			if (quotation.getDescription() != null) {
				comment = quotation.getDescription();
			}
		}

		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font6bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtInWordsValCell.setBorderWidthTop(0);
		amtInWordsValCell.setBorderWidthLeft(0);
		amtInWordsValCell.setBorderWidthRight(0);
		leftTable.addCell(amtInWordsValCell);

		// rohan added this code for valid until

		
		PdfPTable termtab=new PdfPTable(1);
		termtab.setWidthPercentage(100);
		
		String titleterms = "";
		Phrase validDate = new Phrase("", font8);
//		PdfPCell validitydatecell = new PdfPCell();
		System.out.println("salequotaion out");
		if (qp instanceof SalesQuotation) {
			System.out.println("salequotaion in");

			validDate = new Phrase("", font8);

			SalesQuotation salesquot = (SalesQuotation) qp;
			//As per nitin sir's instruction taking valid until at footer. above gst info
//			Phrase validityDate=null;
//			if (salesquot.getValidUntill() != null) {
//				titleterms = "Terms And Conditions";
//				 validityDate = new Phrase("Valid Until: "
//						+ fmt.format(salesquot.getValidUntill()) + "",
//						font8bold);
////				
//			} else {
//				 validityDate = new Phrase("",
//						font8bold);
//				
//			}
//
//		
//		PdfPCell validitydatecell = new PdfPCell(validityDate);
//		validitydatecell.setBorder(0);
//		termtab.addCell(validitydatecell);
		}
		// ends here

		Phrase termNcond;
		PdfPCell termNcondCell;
		if(comment!=null&&!comment.equals("")){
			termNcond = new Phrase("Remark:", font6bold);//old label Terms and Condition By: Ashwini Patil Date:24-04-2023 
			termNcondCell = new PdfPCell(termNcond);
			termNcondCell.setBorder(0);
//			termNcondCell.addElement(termNcond);		
		}else{
			termNcond = new Phrase("", font6bold);//old label Terms and Condition By: Ashwini Patil Date:24-04-2023 
			termNcondCell = new PdfPCell(termNcond);
			termNcondCell.setBorder(0);
//			termNcondCell.addElement(termNcond);
		}
			
		
		Phrase termNcondVal = null;
		if (comment.length() > 500) {
			System.out.println("to check the lenth more 500"
					+ comment.substring(0, 500));
			termNcondVal = new Phrase(comment.substring(0,
					500), font6);
		} else if(comment!=null&&!comment.equals("")){
			System.out.println("to check the lenth less 500"
					+ termNcondVal);
			termNcondVal = new Phrase(comment, font6);
		}else{
			termNcondVal = new Phrase("", font6);
		}
		

//		Phrase termNcondVal = new Phrase(comment, font6bold);
		PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
		termNcondValCell.setBorder(0);
//		termNcondValCell.addElement(termNcondVal);

		termtab.addCell(termNcondCell);
		termtab.addCell(termNcondValCell);

		// rohan added this code for universal pest

		
		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
//		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font6bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
//		blankCell.addElement(blank);

		Phrase netPay = new Phrase("Net Payable", font6bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
//		netPayCell.setBorderWidthTop(0);
//		netPayCell.setBorderWidthLeft(0);
		netPayCell.setBorderWidthRight(0);
//		netPayCell.addElement(netPay);

		Phrase netPaybl = new Phrase(":", font6bold);

		PdfPCell netPayblCell = new PdfPCell(netPaybl);
//		netPayblCell.setBorderWidthTop(0);;
		netPayblCell.setBorderWidthLeft(0);
		netPayblCell.setBorderWidthRight(0);
//		netPayCell.addElement(netPay);
		
		Phrase netPayVal = new Phrase(netpayable + "", font6bold);
		 Paragraph netPayPara=new Paragraph();
		 netPayPara.add(netPayVal);
		 netPayPara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
//		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthLeft(0);
//		netPayValCell.setBorderWidthRight(0);
		// netPayValCell.addElement(netPayPara);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase gstReverseCharge = new Phrase("GST Payable on Reverse Charge",
				font6bold);
		PdfPCell gstReverseChargeCell = new PdfPCell(gstReverseCharge);
		gstReverseChargeCell.setBorder(0);
//		gstReverseChargeCell.addElement(gstReverseCharge);

		Phrase gstReverseChargeVal = new Phrase(" ", font7);
		PdfPCell gstReverseChargeValCell = new PdfPCell(gstReverseChargeVal);
		gstReverseChargeValCell.setBorder(0);
//		gstReverseChargeValCell.addElement(gstReverseChargeVal);

		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(netPayblCell);
		innerRightTable.addCell(netPayValCell);

//		innerRightTable.addCell(gstReverseChargeCell);
//		innerRightTable.addCell(colonCell);
//		innerRightTable.addCell(gstReverseChargeValCell);

		PdfPCell rightUpperCell = new PdfPCell(innerRightTable);
//		rightUpperCell.addElement(innerRightTable);
		rightUpperCell.setBorder(0);
		

		

		rightTable.addCell(rightUpperCell);


		PdfPCell lefttableCell = new PdfPCell(leftTable);
//		lefttableCell.addElement(leftTable);
		PdfPCell righttableCell = new PdfPCell(rightTable);
//		righttableCell.addElement(rightTable);

		bottomTable.addCell(lefttableCell);
		bottomTable.addCell(righttableCell);
		
		if(comment!=null&&!comment.equals("")){
			PdfPCell termtabcell=new PdfPCell(termtab);
			termtabcell.setFixedHeight(70);
			termtabcell.setColspan(2);
			bottomTable.addCell(termtabcell);
		}
		
		
		
		
		


		try {
			document.add(bottomTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createFooterLastPart2(String preprintStatus) {
		System.out.println("FOOTER TABLE");
		
		
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		
		
		//Ashwini Patil Date:24-04-2023
		if (qp instanceof SalesQuotation) {
			System.out.println("salequotaion in");
			SalesQuotation salesquot = (SalesQuotation) qp;
			Phrase validityDate=null;
			if (salesquot.getValidUntill() != null) {
				 validityDate = new Phrase("Valid Until: "
						+ fmt.format(salesquot.getValidUntill()) + "",
						font6bold);
//				
			} else {
				 validityDate = new Phrase("",
						 font6bold);
				
			}

		
		PdfPCell validitydatecell = new PdfPCell(validityDate);
		validitydatecell.setBorder(0);
		leftTable.addCell(validitydatecell);
		}
		
		
		
		
			/**
			 * Date 16/3/2018
			 * By jayshree
			 * Des.to add the comp gstin from serverapp utility changes are done
			 */
			
			ServerAppUtility serverApp = new ServerAppUtility();

			String gstin="",gstinText="";
			if(qp instanceof SalesOrder){
			if (comp.getCompanyGSTType().trim()
					.equalsIgnoreCase("GST Applicable")) {
				logger.log(Level.SEVERE,"GST Applicable");
				gstin = serverApp.getGSTINOfCompany(comp, soEntity
							.getBranch().trim());
			} else {
				logger.log(Level.SEVERE,"GST Not Applicable");
				gstinText = comp.getCompanyGSTTypeText().trim();
			}
			}
			else{

				if (comp.getCompanyGSTType().trim()
						.equalsIgnoreCase("GST Applicable")) {
					logger.log(Level.SEVERE,"GST Applicable");
					gstin = serverApp.getGSTINOfCompany(comp, salesquotEntity
								.getBranch().trim());
				} else {
					logger.log(Level.SEVERE,"GST Not Applicable");
					gstinText = comp.getCompanyGSTTypeText().trim();
				}
				
				
			}
			
			/*** Date 23-11-2018 By Vijay For GST Number will print even if GST Tax not applicable so
			 * below code commented
			 */ 
//			/**
//			 * Date 16/3/2018
//			 * By Jayshree
//			 * Des.to remove gstin if tax are not present
//			 */
//			
//				if(qp.getProductTaxes().size()==0){
//					gstin="";
//				}
//				//End
				Phrase gstinph=null ;
				if(!gstin.trim().equals("")){
					logger.log(Level.SEVERE,"GST Present");
					gstinph= new Phrase("GSTIN" + " : " + gstin,font6bold);
				}else if (!gstinText.trim().equalsIgnoreCase("")) {
					logger.log(Level.SEVERE,"GST Not Present");
					gstinph= new Phrase(gstinText,font6bold);
				}else{
					logger.log(Level.SEVERE,"Nothing Present");
					gstinph= new Phrase("",font6bold);
				}
				
				/*** GST Number will print if GST tax applicable or not applicable ***/
				if(gstNumberPrintFlag){
				System.out.println("Check point 1a");
				PdfPCell gstinCell = new PdfPCell();
				gstinCell.setBorder(0);
				gstinCell.addElement(gstinph);
				leftTable.addCell(gstinCell);
				}
				//End By jayshree
			
		if(qp instanceof SalesOrder){
			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

				if (comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")&&comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("Sales Order")) {

					Phrase articalType = new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()
							+ " : "+ comp.getArticleTypeDetails().get(i).getArticleTypeValue(), font6bold);
					PdfPCell articalTypeCell = new PdfPCell();
					articalTypeCell.setBorder(0);
					articalTypeCell.addElement(articalType);
					/*** GST Number will print if GST tax applicable or not applicable ***/
					if(gstNumberPrintFlag || !comp.getArticleTypeDetails().get(i).getArticleTypeName().equals("GSTIN")){			
						leftTable.addCell(articalTypeCell);
					}
				}
			}
		}else{
			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

				if (comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")&&comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("SalesQuotation")) {

					Phrase articalType = new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()
							+ " : "+ comp.getArticleTypeDetails().get(i).getArticleTypeValue(), font6bold);
					PdfPCell articalTypeCell = new PdfPCell();
					articalTypeCell.setBorder(0);
					articalTypeCell.addElement(articalType);
					/*** GST Number will print if GST tax applicable or not applicable ***/
					if(gstNumberPrintFlag || !comp.getArticleTypeDetails().get(i).getArticleTypeName().equals("GSTIN")){			
						leftTable.addCell(articalTypeCell);
					}
				}
			}	
		}
		

		
		
		PdfPTable middletTable = new PdfPTable(1);
		middletTable.setWidthPercentage(100);
		
		System.out.println("check point 1b");
			if (comppayment != null) {
				System.out.println("check point 1c");
				
				PdfPTable bankDetailsTable = new PdfPTable(1);
				bankDetailsTable.setWidthPercentage(100f);

				String favourOf = "";
				if (comppayment.getPaymentComName() != null&& !comppayment.getPaymentComName().equals("")) {
					favourOf = "Cheque should be in favour of '"+ comppayment.getPaymentComName() + "'";
				}
				Phrase favouring = new Phrase(favourOf, font8bold);
				PdfPCell favouringCell = new PdfPCell(favouring);
				favouringCell.setBorder(0);
				bankDetailsTable.addCell(favouringCell);

				
				Phrase heading = new Phrase("Bank Details", font8bold);
				PdfPCell headingCell = new PdfPCell(heading);
				headingCell.setBorder(0);
				bankDetailsTable.addCell(headingCell);

				float[] columnWidths3 = { 1.5f, 0.35f, 4.5f };
				PdfPTable bankDetails3Table = new PdfPTable(3);
				bankDetails3Table.setWidthPercentage(100f);
				try {
					bankDetails3Table.setWidths(columnWidths3);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				//First row
				Phrase bankNamePh = new Phrase("Name", font8bold);
				PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
				bankNamePhCell.setBorder(0);
				bankDetails3Table.addCell(bankNamePhCell);

				Phrase dot = new Phrase(":", font8bold);
				PdfPCell dotCell = new PdfPCell(dot);
				dotCell.setBorder(0);
				bankDetails3Table.addCell(dotCell);

				String bankName = "";
				if (comppayment.getPaymentBankName() != null
						&& !comppayment.getPaymentBankName().equals("")) {
					bankName = comppayment.getPaymentBankName();
				}
				Phrase headingValue = new Phrase(bankName, font8);
				PdfPCell headingValueCell = new PdfPCell(headingValue);
				headingValueCell.setBorder(0);
				headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(headingValueCell);
				

				//2nd row
				// this is for branch
				Phrase bankBranch = new Phrase("Branch", font8bold);
				PdfPCell bankBranchCell = new PdfPCell(bankBranch);
				bankBranchCell.setBorder(0);
				bankDetails3Table.addCell(bankBranchCell);
				bankDetails3Table.addCell(dotCell);

				String bankBranchValue = "";
				if (comppayment.getPaymentBranch() != null
						&& !comppayment.getPaymentBranch().equals("")) {
					bankBranchValue = comppayment.getPaymentBranch();
				}
				Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
				PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
				bankBranchValuePhCell.setBorder(0);
				bankBranchValuePhCell
						.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankBranchValuePhCell);

				//3rd row
				Phrase bankAc = new Phrase("A/c No", font8bold);
				PdfPCell bankAcCell = new PdfPCell(bankAc);
				bankAcCell.setBorder(0);
				bankDetails3Table.addCell(bankAcCell);
				bankDetails3Table.addCell(dotCell);

				String bankAcNo = "";
				if (comppayment.getPaymentAccountNo() != null
						&& !comppayment.getPaymentAccountNo().equals("")) {
					bankAcNo = comppayment.getPaymentAccountNo();
				}
				Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
				PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
				bankAcNoValueCell.setBorder(0);
				bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankAcNoValueCell);

				//4th row
				Phrase bankIFSC = new Phrase("IFS Code", font8bold);
				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
				bankIFSCCell.setBorder(0);
				bankDetails3Table.addCell(bankIFSCCell);
				bankDetails3Table.addCell(dotCell);

				String bankIFSCNo = "";
				if (comppayment.getPaymentIFSCcode() != null
						&& !comppayment.getPaymentIFSCcode().equals("")) {
					bankIFSCNo = comppayment.getPaymentIFSCcode();
				}
				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
				bankIFSCNoValueCell.setBorder(0);
				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankIFSCNoValueCell);

				
				PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
				bankDetails3TableCell.setBorder(0);
				bankDetailsTable.addCell(bankDetails3TableCell);

				PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
				bankDetailsTableCell.setBorder(0);
				middletTable.addCell(bankDetailsTableCell);
			}
		

		
		
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);
		System.out.println("check point 1d");
		String companyname = "";
		if (multipleCompanyName) {
			if (qp.getGroup() != null && !qp.getGroup().equals("")) {
				companyname = qp.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}
		} else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		// ends here

		Phrase companyPhrase = new Phrase("For , " + companyname, font6bold);
//		Paragraph companyPara = new Paragraph();
//		companyPara.add(companyPhrase);
//		companyPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		companyParaCell.addElement(companyPara);
		companyParaCell.setBorder(0);

		rightTable.addCell(companyParaCell);
		
		Phrase signblank=new Phrase(" ");
		PdfPCell signBlankCel=new PdfPCell(signblank);
		signBlankCel.setBorder(0);
		/**
		 * Date 28-5-2018 by jayshree 
		 * des.add digital signature 
		 */
		
		
		DocumentUpload digitalsign = comp.getUploadDigitalSign();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + digitalsign.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);// By Jayshree add this 7/12/2017
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 Image image1=null;
//		 try
//		 {
//		 image1=Image.getInstance("images/ipclogo4.jpg");
//		 image1.scalePercent(15f);
//		 image1.scaleAbsoluteWidth(100f);
//		 // image1.setAbsolutePosition(40f,765f);
//		 // doc.add(image1);
//		
//		
//		
//		
//		
//		 imageSignCell=new PdfPCell(image1);
//		 // imageSignCell.addElement(image1);
//		 // imageSignCell.setFixedHeight(40);
//		 imageSignCell.setBorder(0);
//		 imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }

		// End By Jayshree
		if(imageSignCell!=null){
			rightTable.addCell(imageSignCell);
		}
		else{
		rightTable.addCell(signBlankCel);
		rightTable.addCell(signBlankCel);
		rightTable.addCell(signBlankCel);
		}
		Phrase signAuth = new Phrase("Authorised Signatory", font6bold);
//		Paragraph signPara = new Paragraph();
//		signPara.add(signAuth);
//		signPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell signParaCell = new PdfPCell(signAuth);
//		signParaCell.addElement(signPara);
		signParaCell.setBorder(0);
		signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTable.addCell(signParaCell);
		
		
		PdfPCell lefttableCell = new PdfPCell();
		lefttableCell.addElement(leftTable);
		
		PdfPCell middletableCell = new PdfPCell();
		middletableCell.addElement(middletTable);
		
		PdfPCell righttableCell = new PdfPCell();
		righttableCell.addElement(rightTable);
		
//		try{
//		System.out.println("LT "+leftTable.getRows());
//		System.out.println("MT "+middletTable.getRows());
//		System.out.println("RT "+rightTable.getRows());
//		
//		System.out.println("LTC "+lefttableCell);
//		System.out.println("MTC "+middletableCell);
//		System.out.println("RTC "+righttableCell);
//		}catch(Exception e){
//			e.printStackTrace();
//		}

		PdfPTable mainTbl = new PdfPTable(3);
		mainTbl.setWidthPercentage(100);
		float[] columnThreePartWidths = { 33,33,33};
		try {
			mainTbl.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		mainTbl.addCell(lefttableCell);
		mainTbl.addCell(middletableCell);
		mainTbl.addCell(righttableCell);

		//

		Paragraph para = new Paragraph("Note : This is computer generated hence no signature required.",font8);

		//
		try {
			document.add(mainTbl);
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE," get.. "+e.getMessage());
		}
		
//		if (qp instanceof SalesOrder) {
//			SalesOrder soEntity = (SalesOrder) qp;
//			if (soEntity.getItems().size() > 5) {
//				Paragraph para1 = new Paragraph("Annexure 1 :", font10bold);
//				para1.setAlignment(Element.ALIGN_LEFT);
//
//				try {
//					document.add(Chunk.NEWLINE);
//					document.add(Chunk.NEXTPAGE);
//					document.add(para1);
//					document.add(Chunk.NEWLINE);
//				} catch (DocumentException e) {
//					e.printStackTrace();
//				}
//				
////				createProductDetails();//comment by jayshree
//				createProductDetails2();
//				createAnnexureForRemainingSalesOrderProduct(5);
//			}
//		} else {
//			SalesQuotation quotation = (SalesQuotation) qp;
//			if (quotation.getItems().size() > noOfLines) {
//				Paragraph para1 = new Paragraph("Annexure 1 :", font10bold);
//				para1.setAlignment(Element.ALIGN_LEFT);
//
//				try {
//					document.add(Chunk.NEWLINE);
//					document.add(Chunk.NEXTPAGE);
//					document.add(para1);
//					document.add(Chunk.NEWLINE);
//				} catch (DocumentException e) {
//					e.printStackTrace();
//				}
////				createProductDetails();//Comment by jayshree
//				createProductDetails2();
//				1createAnnexureForRemainingQuoatationProduct(5);
//			}
//		}
		
		if (noOfLines == 0 && productCount != 0) {
			createAnnexureForRemainingProduct(productCount);
		}
		
		
		
		System.out.println("inside last method...............");
	}

	private void createAnnexureForRemainingProduct(int productCount) {
		
		Paragraph para = new Paragraph("Annexure 1 :", font10bold);
		para.setAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(Chunk.NEWLINE);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		createProductDetails2();
		createAnnextureproductDetailvalue(productCount);
		
		
	}

	private void createAnnextureproductDetailvalue(int productCount) {
		
		PdfPTable productTable = new PdfPTable(12);
		productTable.setWidthPercentage(100);
		// productTable.setSpacingAfter(blankLines);
		try {
			productTable.setWidths(column16CollonWidthnew);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		

		
		if (qp instanceof SalesOrder) {

			SalesOrder salesOderEntity = (SalesOrder) qp;
			

			for (int i = productCount; i < salesOderEntity.getItems().size(); i++) {

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(salesOderEntity.getItems()
						.get(i).getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
//				serviceNameCell.addElement(serviceName);
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (salesOderEntity.getItems().get(i).getPrduct()
						.getHsnNumber() != null) {
					hsnCode = new Phrase(salesOderEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
//				hsnCodeCell.addElement(hsnCode);
				hsnCodeCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(salesOderEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
//				uomCell.addElement(uom);
				uomCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(salesOderEntity.getItems().get(i)
						.getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
//				qtyCell.addElement(qty);
				qtyCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(salesOderEntity.getItems()
						.get(i).getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell(rate);
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
//				rateCell.addElement(rate);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(rateCell);

				double amountValue = salesOderEntity.getItems().get(i)
						.getPrice()
						* salesOderEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
//				productTable.addCell(amountCell);

				
				/**
				 * Date 8/3/2018 
				 * By jayshree
				 * Des.to print the discount
				 */
				
				boolean isAreaPresent=false;
				double areval=0;
				double totaldis=0;
				double desper=soEntity.getItems().get(i).getPercentageDiscount();
				try{
				 areval=Double.parseDouble(soEntity.getItems().get(i).getArea());
				 isAreaPresent=true;
				}
				catch(Exception e){
					isAreaPresent=false;
				}
				
				if(isAreaPresent){
					totaldis=(soEntity.getItems().get(i).getQty()*soEntity.getItems().get(i).getPrice()*areval*desper)/100
							+soEntity.getItems().get(i).getDiscountAmt();
				}
				else{
					totaldis=(soEntity.getItems().get(i).getQty()*soEntity.getItems().get(i).getPrice()*desper)/100+
							soEntity.getItems().get(i).getDiscountAmt();
				}
				
				Phrase disc = new Phrase(df.format(totaldis)
						+ "", font8);
				
				//End By jayshree
				
				
				
				
				PdfPCell discCell = new PdfPCell(disc);
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
//				discCell.addElement(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(discCell);
				double asstotalAmount1 = getAssessTotalAmount(soEntity
						.getItems().get(i));
				System.out.println("asstotalAmount1:::::"+asstotalAmount1);
				Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
//				taxableValueCell.addElement(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				boolean taxPresent = validateTaxes(salesOderEntity.getItems()
						.get(i));
				if (taxPresent) {
					if (salesOderEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("IGST")) {
						double asstotalAmount = getAssessTotalAmount(salesOderEntity
								.getItems().get(i));

						double taxAmount = getTaxAmount(asstotalAmount,
								salesOderEntity.getItems().get(i).getVatTax()
										.getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);
						Phrase igstRate = new Phrase(salesOderEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);
						igstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else if (salesOderEntity.getItems().get(i)
							.getServiceTax().getTaxPrintName()
							.equalsIgnoreCase("IGST")) {
						double asstotalAmount = getAssessTotalAmount(salesOderEntity
								.getItems().get(i));

						double taxAmount = getTaxAmount(asstotalAmount,
								salesOderEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);
						igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						
						Phrase igstRate = new Phrase(salesOderEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);
						igstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else {

						if (salesOderEntity.getItems().get(i).getVatTax()
								.getTaxPrintName().equalsIgnoreCase("CGST")) {
							double asstotalAmount = getAssessTotalAmount(salesOderEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getVatTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);
							cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Phrase cgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							cgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getServiceTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
//							sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);
							sgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Phrase sgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);//2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

						} else if (salesOderEntity.getItems().get(i)
								.getVatTax().getTaxPrintName()
								.equalsIgnoreCase("SGST")) {
							double asstotalAmount = getAssessTotalAmount(salesOderEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getServiceTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);
							cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							Phrase cgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							cgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									salesOderEntity.getItems().get(i)
											.getVatTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
							// sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(salesOderEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							sgstRateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(sgstRateCell);//2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

						}
					}
				} else {

					logger.log(Level.SEVERE, "Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(
							df.format(getAssessTotalAmount(salesOderEntity
									.getItems().get(i))) + "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if (salesOderEntity.getItems().get(i).getPremisesDetails() != null) {
						premisesVal = salesOderEntity.getItems().get(i)
								.getPremisesDetails();
					}

					
					
					
					// if (printPremiseDetails) {
					// Phrase premisesValPhrs = new Phrase("Premise Details : "
					// + premisesVal, font8);
					// PdfPCell premiseCell = new PdfPCell();
					// premiseCell.setColspan(16);
					// premiseCell.addElement(premisesValPhrs);
					// productTable.addCell(premiseCell);
					// }

				}
				/**
				 * Date 18-06-2018
				 * By Nidhi
				 * Des.changes as per process configration 
				 */
				chunk = new Phrase(" ", font7);
				PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
				Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				Pdfsrnocell2.setBorderWidthRight(0);
				
				String premises = "";
				{
					if (products.get(i).getPremisesDetails() != null
						&& !products.get(i).getPremisesDetails().equals("")) {
					premises = "Premise For " + products.get(i).getProductName()
							+ " : " + products.get(i).getPremisesDetails();
					System.out.println("products.get(i).getPremisesDetails()"+premises);
				} else {
					premises = "Premise For " + products.get(i).getProductName()+":"+"NA";
					System.out.println("products.get(i).getPremisesDetails()bbbb");
				}
				}
				if(printPremiseDetails)
				{
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
					// premisesCell.setBorderWidthRight(0);
					premisesCell.setColspan(11);
					
					noOfLines = noOfLines - 1;
					productTable.addCell(Pdfsrnocell2);
					productTable.addCell(premisesCell);
				}
				
			}

			
			

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			
			

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			// //////// End Here

		} else {

			SalesQuotation quotationEntity = (SalesQuotation) qp;

			int colsspan =1;
			if(PC_DoNotPrintUOMFlag){
				++colsspan;
			}
			if(PC_DoNotPrintQtyFlag){
				++colsspan;
			}
			if(PC_DoNotPrintRateFlag){
				++colsspan;
			}
			if(PC_DoNotPrintDiscFlag){
				++colsspan;
			}
			System.out.println("colsspan "+colsspan);
			

			int taxCColumnolsspan =1;
			if(PC_DoNotPrintTaxFlag){
				taxCColumnolsspan +=4 ;
			}
			System.out.println("taxCColumnolsspan "+taxCColumnolsspan);
			
			for (int i = productCount; i < quotationEntity.getItems().size(); i++) {


				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(quotationEntity.getItems()
						.get(i).getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
//				serviceNameCell.addElement(serviceName);
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceNameCell.setColspan(colsspan);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (quotationEntity.getItems().get(i).getPrduct()
						.getHsnNumber() != null) {
					hsnCode = new Phrase(quotationEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
//				hsnCodeCell.addElement(hsnCode);
				hsnCodeCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(quotationEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
//				uomCell.addElement(uom);
				uomCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintUOMFlag){
					productTable.addCell(uomCell);
				}

				Phrase qty = new Phrase(quotationEntity.getItems().get(i)
						.getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
//				qtyCell.addElement(qty);
				qtyCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintQtyFlag){
					productTable.addCell(qtyCell);
				}

				Phrase rate = new Phrase(df.format(quotationEntity.getItems()
						.get(i).getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell(rate);
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
//				rateCell.addElement(rate);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintRateFlag){
					productTable.addCell(rateCell);
				}

				double amountValue = quotationEntity.getItems().get(i)
						.getPrice()
						* quotationEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell();
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.addElement(amount);
//				productTable.addCell(amountCell);

				/**
				 * Date 8/3/2018 
				 * By jayshree
				 * Des.to print the discount
				 */
				
				boolean isAreaPresent=false;
				double areval=0;
				double totaldis=0;
				double desper=quotationEntity.getItems().get(i).getPercentageDiscount();
				try{
				 areval=Double.parseDouble(quotationEntity.getItems().get(i).getArea());
				 isAreaPresent=true;
				}
				catch(Exception e){
					isAreaPresent=false;
				}
				
				if(isAreaPresent){
					totaldis=(quotationEntity.getItems().get(i).getQty()*quotationEntity.getItems().get(i).getPrice()*areval*desper)/100
							+quotationEntity.getItems().get(i).getDiscountAmt();
				}
				else{
					totaldis=(quotationEntity.getItems().get(i).getQty()*quotationEntity.getItems().get(i).getPrice()*desper)/100+
							quotationEntity.getItems().get(i).getDiscountAmt();
				}
				
				Phrase disc = new Phrase(df.format(totaldis)
						+ "", font8);
				
				//End By jayshree
				PdfPCell discCell = new PdfPCell(disc);
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
//				discCell.addElement(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				if(!PC_DoNotPrintDiscFlag){
					productTable.addCell(discCell);
				}


				double asstotalAmount1 = getAssessTotalAmount(quotationEntity
						.getItems().get(i));
				System.out.println("asstotalAmount1:::::"+asstotalAmount1);
				Phrase taxableValue = new Phrase(df.format(asstotalAmount1)+"", font8);
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
//				taxableValueCell.addElement(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				taxableValueCell.setColspan(taxCColumnolsspan);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				boolean taxPresent = validateTaxes(quotationEntity.getItems()
						.get(i));
				if(!PC_DoNotPrintTaxFlag){
					
				if (taxPresent) {
					if (quotationEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("IGST")) {

						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						System.out.println("asstotalAmount:::::"+asstotalAmount);
						
						double taxAmount = getTaxAmount(asstotalAmount, quotationEntity
								.getItems().get(i).getVatTax().getPercentage());
						double indivTotalAmount = asstotalAmount
								+ taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(quotationEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else if (quotationEntity.getItems().get(i)
							.getServiceTax().getTaxPrintName()
							.equalsIgnoreCase("IGST")) {

						double asstotalAmount = getAssessTotalAmount(quotationEntity
								.getItems().get(i));
						double taxAmount = getTaxAmount(asstotalAmount,
								quotationEntity.getItems().get(i)
										.getServiceTax().getPercentage());
						double indivTotalAmount = asstotalAmount + taxAmount;
						totalAmount = totalAmount + indivTotalAmount;

						Phrase igstRateVal = new Phrase(df.format(taxAmount)
								+ "", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
//						igstRateValCell.addElement(igstRateVal);

						Phrase igstRate = new Phrase(quotationEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setColspan(2);
//						igstRateCell.addElement(igstRate);

						/* for Cgst */

						Phrase cgst = new Phrase("-", font8);
						PdfPCell cell = new PdfPCell(cgst);
						// cell.addElement(cgst);
						cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						productTable.addCell(cell);//1
//						productTable.addCell(cell);

						/* for Sgst */
						Phrase sgst = new Phrase("-", font8);
						productTable.addCell(cell);//2
//						productTable.addCell(cell);

						productTable.addCell(igstRateCell);//3
//						productTable.addCell(igstRateValCell);

						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
//						totalCell.addElement(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						productTable.addCell(totalCell);

					} else {

						if (quotationEntity.getItems().get(i).getVatTax()
								.getTaxPrintName().equalsIgnoreCase("CGST")) {
							double asstotalAmount = getAssessTotalAmount(quotationEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getVatTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(quotationEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getServiceTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
							sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(quotationEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);//2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);
							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

							// try {
							// document.add(productTable);
							// } catch (DocumentException e) {
							// e.printStackTrace();
							// }

						} else if (quotationEntity.getItems().get(i)
								.getVatTax().getTaxPrintName()
								.equalsIgnoreCase("SGST")) {
							double asstotalAmount = getAssessTotalAmount(quotationEntity
									.getItems().get(i));
							double ctaxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getServiceTax().getPercentage());

							Phrase cgstRateVal = new Phrase(
									df.format(ctaxValue) + "", font8);
							PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
							// cgstRateValCell.setBorder(0);
							// cgstRateValCell.setBorderWidthBottom(0);
							// cgstRateValCell.setBorderWidthTop(0);
							// cgstRateValCell.setBorderWidthRight(0);
//							cgstRateValCell.addElement(cgstRateVal);

							Phrase cgstRate = new Phrase(quotationEntity
									.getItems().get(i).getVatTax()
									.getPercentage()
									+ "", font8);
							PdfPCell cgstRateCell = new PdfPCell(cgstRate);
							// cgstRateCell.setBorder(0);
							// cgstRateCell.setBorderWidthBottom(0);
							// cgstRateCell.setBorderWidthTop(0);
							// cgstRateCell.setBorderWidthLeft(0);
//							cgstRateCell.addElement(cgstRate);
							productTable.addCell(cgstRateCell);//1
//							productTable.addCell(cgstRateValCell);

							double staxValue = getTaxAmount(asstotalAmount,
									quotationEntity.getItems().get(i)
											.getVatTax().getPercentage());
							Phrase sgstRateVal = new Phrase(
									df.format(staxValue) + "", font8);
							PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
							// sgstRateValCell.setBorder(0);
							// sgstRateValCell.setBorderWidthBottom(0);
							// sgstRateValCell.setBorderWidthTop(0);
							// sgstRateValCell.setBorderWidthRight(0);
//							sgstRateValCell.addElement(sgstRateVal);

							Phrase sgstRate = new Phrase(quotationEntity
									.getItems().get(i).getServiceTax()
									.getPercentage()
									+ "", font8);
							PdfPCell sgstRateCell = new PdfPCell(sgstRate);
							// sgstRateCell.setBorder(0);
							// sgstRateCell.setBorderWidthBottom(0);
							// sgstRateCell.setBorderWidthTop(0);
							// sgstRateCell.setBorderWidthLeft(0);
//							sgstRateCell.addElement(sgstRate);
							productTable.addCell(sgstRateCell);///2
//							productTable.addCell(sgstRateValCell);

							Phrase igstRateVal = new Phrase("-", font8);
							PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
							// igstRateValCell.setBorder(0);
							// igstRateValCell.setBorderWidthBottom(0);
							// igstRateValCell.setBorderWidthTop(0);
							// igstRateValCell.setBorderWidthRight(0);
							// igstRateValCell.addElement(igstRateVal);
							igstRateValCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
//							productTable.addCell(igstRateValCell);

							Phrase igstRate = new Phrase("-", font8);
							PdfPCell igstRateCell = new PdfPCell(igstRate);
							// igstRateCell.setBorder(0);
							// igstRateCell.setBorderWidthBottom(0);
							// igstRateCell.setBorderWidthTop(0);
							// igstRateCell.setBorderWidthLeft(0);
							// igstRateCell.addElement(igstRate);
							igstRateCell
									.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(igstRateCell);//3

							double indivTotalAmount = asstotalAmount
									+ ctaxValue + staxValue;
							totalAmount = totalAmount + indivTotalAmount;
							Phrase totalPhrase = new Phrase(
									df.format(indivTotalAmount) + "", font8);
							PdfPCell totalCell = new PdfPCell(totalPhrase);

							// totalCell.setBorder(0);
							// totalCell.setBorderWidthBottom(0);
							// totalCell.setBorderWidthTop(0);
//							totalCell.addElement(totalPhrase);
							totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							productTable.addCell(totalCell);

						}
					}
				} else {
					logger.log(Level.SEVERE, "Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
//					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(
							df.format(getAssessTotalAmount(quotationEntity
									.getItems().get(i))) + "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if (quotationEntity.getItems().get(i).getPremisesDetails() != null) {
						premisesVal = quotationEntity.getItems().get(i)
								.getPremisesDetails();
					}
					
					
					
				}
				
				
			}
				/**
				 * Date 18-06-2018
				 * By Nidhi
				 * Des.changes as per process configration 
				 */
				chunk = new Phrase(" ", font7);
				PdfPCell Pdfsrnocell2 = new PdfPCell(chunk);
				Pdfsrnocell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				Pdfsrnocell2.setBorderWidthRight(0);
				
				String premises = "";
				{
					if (products.get(i).getPremisesDetails() != null
						&& !products.get(i).getPremisesDetails().equals("")) {
					premises = "Premise For " + products.get(i).getProductName()
							+ " : " + products.get(i).getPremisesDetails();
					System.out.println("products.get(i).getPremisesDetails()"+premises);
				} else {
					premises = "Premise For " + products.get(i).getProductName()+":"+"NA";
					System.out.println("products.get(i).getPremisesDetails()bbbb");
				}
				}
				if(printPremiseDetails)
				{
					Phrase premisesPh = new Phrase(premises, font7);
					PdfPCell premisesCell = new PdfPCell(premisesPh);
					// premisesCell.setBorderWidthRight(0);
					premisesCell.setColspan(11);
					
					noOfLines = noOfLines - 1;
					productTable.addCell(Pdfsrnocell2);
					productTable.addCell(premisesCell);
				}
				
				/**
				 * Updated By: Viraj 
				 * Date: 19-12-2018
				 * Description: To add Product description
				 */
				String desc="";
				String desc1="";
				System.out.println("desc: "+products.get(i).getPrduct().getComment());
				System.out.println("desc1: "+products.get(i).getPrduct().getCommentdesc());
				if(products.get(i).getPrduct().getComment() != null 
						&& !products.get(i).getPrduct().getComment().equals("")) {
					desc = products.get(i).getPrduct().getComment();
				}
				if(products.get(i).getPrduct().getCommentdesc() != null 
						&& !products.get(i).getPrduct().getCommentdesc().equals("")) {
					desc1 = products.get(i).getPrduct().getCommentdesc();
				}
				/**
				 * Updated By: Viraj
				 * Date: 09-01-2019
				 * Description: To add both the descriptions in single phrase
				 */
				String description = desc + " " + desc1 ;
				if(printProductDesc) {
					System.out.println("No of lines :"+ noOfLines);
					System.out.println("description: "+desc);
//					if(desc != null) {
//						Phrase prodDesc = new Phrase(desc, font8);
//						PdfPCell prodDescCell = new PdfPCell(prodDesc);
//						prodDescCell.setColspan(12);
//						noOfLines = noOfLines - 1;
//						productTable.addCell(prodDescCell);
//					}	
//					System.out.println("description1: "+desc1);
//					if(desc1 != null) {
//						Phrase prodDesc1 = new Phrase(desc1, font8);
//						PdfPCell prodDesc1Cell = new PdfPCell(prodDesc1);
//						prodDesc1Cell.setColspan(12);
//						noOfLines = noOfLines - 1;
//						productTable.addCell(prodDesc1Cell);
//					}
					if(description != null) {
						Phrase prodDesc1 = new Phrase(description, font8);
						PdfPCell prodDesc1Cell = new PdfPCell(prodDesc1);
						prodDesc1Cell.setColspan(12);
						noOfLines = noOfLines - 1;
						productTable.addCell(prodDesc1Cell);
					}
					/** Ends **/
				}
				/** Ends **/
				
			}
		
			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);
			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}


		}
	
		
	
		
		
	}
	
	
	public double removeAllTaxes2(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
//			// Here if both are inclusive then first remove service tax and then on that amount
//			// calculate vat.
//			double removeServiceTax=(entity.getPrice()/(1+service/100));
//						
//			//double taxPerc=service+vat;
//			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//			retrServ=(removeServiceTax/(1+vat/100));
//			retrServ=entity.getPrice()-retrServ;
			
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	
}
