package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipPrintHelper;

@SuppressWarnings("serial")
public class CreatePayRollPDFServlet extends HttpServlet {

	public static ArrayList<PaySlipPrintHelper> list = new ArrayList<PaySlipPrintHelper>();

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf"); // Type of response , helps
		Logger logger=Logger.getLogger("Pay Pdf Logger");											// browser to identify the
													// response type.
//		PayrollPdf pdf = new PayrollPdf();
//		pdf.document = new Document();
//		Document document = pdf.document;
//		logger.log(Level.SEVERE," LIST SIZE :::  "+list.size());
//		
//		String stringid = request.getParameter("Id");
//		
//		
//		if (stringid != null) {
//			list.clear();
//			logger.log(Level.SEVERE," ::::: ID :::  "+stringid);
//			
//			stringid = stringid.trim();
//			Long count = Long.parseLong(stringid);
//			PaySlip insp = ofy().load().type(PaySlip.class).id(count).now();
//			
//			logger.log(Level.SEVERE," ::::: PAYSLIP  :::  "+insp.getEmployeeName());
//			
//			list.add(insp.getPaySlipPrintHelper());
//			
//			logger.log(Level.SEVERE," list size :::  "+list.size());
//		}
//
//		try {
//			PdfWriter.getInstance(document, response.getOutputStream());
//			document.open();
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//		// write the pdf in response
//		for (PaySlipPrintHelper payslip : list) {
//			pdf.setPaySlip(payslip.getId(),payslip);
//			pdf.createPdf();
//			try {
//				document.add(Chunk.NEXTPAGE);
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
//		}
//		document.close();
//
//		list = new ArrayList<PaySlipPrintHelper>();
		
		
		PayrollPdfUpdated pdf = new PayrollPdfUpdated();
		pdf.document = new Document();
		Document document = pdf.document;
		logger.log(Level.SEVERE," LIST SIZE :::  "+list.size());
		
		String stringid = request.getParameter("Id");
		/**
		 * Updated By:Viraj
		 * Date: 20-06-2019
		 * Description: Added status for printing 2 salary slips on one
		 */
		String status = "";
		status = request.getParameter("status");
		logger.log(Level.SEVERE," status :::  "+status);
		/** Ends **/
		
		if (stringid != null) {
			list.clear();
			logger.log(Level.SEVERE," ::::: ID :::  "+stringid);
			
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			PaySlip insp = ofy().load().type(PaySlip.class).id(count).now();
			
			logger.log(Level.SEVERE," ::::: PAYSLIP  :::  "+insp.getEmployeeName());
			
			list.add(insp.getPaySlipPrintHelper());
			
			logger.log(Level.SEVERE," list size :::  "+list.size());
		}

		try {
			PdfWriter.getInstance(document, response.getOutputStream());
			document.open();
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		// write the pdf in response	
		/**
		 * Updated By:Viraj
		 * Date: 26-06-2019
		 * Description: Added status for printing 2 salary slips on one
		 */
		int count = 0;
		/** Ends **/
		for (PaySlipPrintHelper payslip : list) {
			pdf.setPaySlip(payslip.getId(),payslip);
			pdf.createPdf();
			/**
			 * Updated By:Viraj
			 * Date: 20-06-2019
			 * Description: Added status for printing 2 salary slips on one
			 */
			if(status.equalsIgnoreCase("false") || status.equalsIgnoreCase("")) {
				try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			
			//Updated on 26:06-2019 
			if(status.equalsIgnoreCase("true")) {
				count++;
				logger.log(Level.SEVERE," COUNT :::  "+count);
				if((count%2) == 0) {
					try {
						document.add(Chunk.NEXTPAGE);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}
			}
			/** Ends **/
		}
		document.close();

		list = new ArrayList<PaySlipPrintHelper>();

	}

}
