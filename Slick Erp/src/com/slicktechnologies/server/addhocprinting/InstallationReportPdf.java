package com.slicktechnologies.server.addhocprinting;

import com.itextpdf.text.Document;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.util.logging.Level;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class InstallationReportPdf {
	ProcessConfiguration processConfig;
	boolean forInstallationReportFlag = true;
	Logger logger = Logger.getLogger("Name of logger");

	public Document document;
	Company comp;
	Customer cust;
	Phrase blank;
	Phrase chunk;
	Phrase col;
	WorkOrder wo;
	SalesOrder so;

	// int cctv = 0;
	// int vdp = 0;
	// Phrase cable;
	// Phrase equipment;
	// boolean equipmentCCTVFlag = false;
	// boolean equipmentVDPFlag = false;

	List<ItemProduct> itemslist;

	PdfPCell pdfname, pdfdesc, pdftype, pdfprodName;

	float columnWidth[] = { 5f, 0.5f, 8f };
	float colWidth[] = { 40f, 30f, 30f };

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,
			font9boldul, font14boldul;

	public InstallationReportPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}
	

	public void setpdfrep(Long count) {

		wo = ofy().load().type(WorkOrder.class).id(count).now();

		so = ofy().load().type(SalesOrder.class)
				.filter("companyId", wo.getCompanyId())
				.filter("count", wo.getOrderId()).first().now();

		if (wo.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", wo.getCompanyId()).first().now();

		if (wo.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", wo.getOrderId()).first().now();

		else
			cust = ofy().load().type(Customer.class)
					.filter("companyId", so.getCompanyId())
					.filter("count", so.getCinfo().getCount()).first().now();

		/**********************************************/
		// System.out.println("wo.getWoTable().size()"+wo.getWoTable().size());
		// itemslist=new ArrayList<ItemProduct>();
		// for(int i=0;i<wo.getWoTable().size();i++)
		// {
		//
		// System.out.println("comp id "+wo.getCompanyId());
		// System.out.println("prod id "+wo.getWoTable().get(i).getProductId());
		//
		// ItemProduct items =
		// ofy().load().type(ItemProduct.class).filter("companyId",
		// wo.getCompanyId()).filter("count",wo.getWoTable().get(i).getProductId()).first().now();
		// if(items!=null){
		// itemslist.add(items);
		// }else{
		// System.out.println("NULLLLLL");
		// }
		// }

		// System.out.println("bom table size "+wo.getBomTable().size());
		// for(int i=0;i < wo.getBomTable().size();i++)
		// {
		// if(wo.getBomTable().get(i).getPrduct().getProductName().equals("CCTV"))
		// {
		// System.out.println("in side cctv ");
		// cctv=cctv+1;
		// }
		// else
		// if(wo.getBomTable().get(i).getPrduct().getProductName().equals("VDP"))
		// {
		// System.out.println("in side vdp ");
		// vdp=vdp+1;
		// }
		// }
		//
		// if(cctv > 0)
		// {
		// equipmentCCTVFlag = true;
		// }
		//
		// if(vdp > 0)
		// {
		// equipmentVDPFlag = true;
		// }
		/**************************************************/

		System.out.println("size of WoTable:::::::::::::"
				+ wo.getWoTable().size());

		itemslist = new ArrayList<ItemProduct>();

		for (int i = 0; i < wo.getWoTable().size(); i++) {
			List<ItemProduct> list = ofy().load().type(ItemProduct.class)
					.filter("companyId", wo.getCompanyId())
					.filter("count", wo.getWoTable().get(i).getProductId())
					.list();
			if (list.size() != 0) {
				itemslist.addAll(list);
			}

		}
		System.out.println("size of itemslist:::::::::::::" + itemslist.size());

		// ************process cofig code**********************
		if (wo.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", wo.getCompanyId())
					.filter("processName", "WorkOrder")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("InstallationPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						forInstallationReportFlag = true;
					}
				}
			}
		}

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));

	}

	public void createPdf() {

		createBlankHeading();
		createTitle();
		createCustomerDetail1();
		createCustomerDetail2();
		createCustomerDetail3();
		createEquipmentDetail();
		createProductTable();
		createInstallationDetail();
		createCheckBox();
		createRemarkHeading();
		createLine();
		createBottom();
		abcd();
	}


	private void abcd() {

		for (int i = 0; i < itemslist.size(); i++) {
			System.out.println("Desc" + itemslist.get(i).getComment());
		}

	}

	public void createBlankHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createTitle() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase cctv = new Phrase("INSTALLATION & COMMISSIONING REPORT",
				font14boldul);
		PdfPCell cctvcell = new PdfPCell(cctv);
		cctvcell.setBorder(0);
		cctvcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable cctvtable = new PdfPTable(1);
		cctvtable.addCell(cctvcell);
		cctvtable.addCell(blcell);
		cctvtable.setWidthPercentage(100);

		try {
			document.add(cctvtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCustomerDetail1() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// *************left table**************
		PdfPTable lefttable = new PdfPTable(3);

		try {
			lefttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		Phrase custname = new Phrase("CUSTOMER NAME ", font9bold);
		PdfPCell custnamecell = new PdfPCell(custname);
		custnamecell.setBorder(0);
		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase company = null;
		System.out
				.println("company===================" + cust.getCompanyName());

		if (cust.isCompany() == true) {
			company = new Phrase(" " + cust.getCompanyName(), font9);
		} else {
			company = new Phrase(" " + cust.getFullname(), font9);
		}
		PdfPCell companycell = new PdfPCell(company);
		companycell.setBorder(0);
		companycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		lefttable.addCell(custnamecell);
		lefttable.addCell(colcell);
		lefttable.addCell(companycell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ************right table**************
		PdfPTable righttable = new PdfPTable(3);

		try {
			righttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		Phrase code = new Phrase("AREA CODE", font9bold);
		PdfPCell codecell = new PdfPCell(code);
		codecell.setBorder(0);
		codecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase acode = null;
		if (cust.getAdress().getLocality() != null) {
			acode = new Phrase(" " + cust.getAdress().getLocality(), font9);
		}

		PdfPCell acodecell = new PdfPCell(acode);
		acodecell.setBorder(0);
		acodecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		righttable.addCell(codecell);
		righttable.addCell(colcell);
		righttable.addCell(acodecell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerDetail2() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********left table**************
		PdfPTable lefttable = new PdfPTable(3);

		try {
			lefttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		Phrase add = new Phrase("INST. ADDRESS ", font9bold);
		PdfPCell addcell = new PdfPCell(add);
		addcell.setBorder(0);
		addcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null) {
			logger.log(Level.SEVERE, "111111111111111111");
			if (!cust.getAdress().getAddrLine2().equals("")) {
				logger.log(Level.SEVERE, "22222222222222222");
				logger.log(Level.SEVERE, "hiii"
						+ cust.getAdress().getAddrLine2());
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE, "333333333333333");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getAddrLine2() + "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE, "4444444444444");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				logger.log(Level.SEVERE, "5555555555555");
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE, "66666666666666666");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE, "77777777777777777");
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null) {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + "\n"
						// + cust.getAdress().getLocality() + " "+
						// "\n" +
						+ "Pin : " + cust.getAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}
		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		lefttable.addCell(addcell);
		lefttable.addCell(colcell);
		lefttable.addCell(custAddInfoCell);

		lefttable.addCell(blcell);
		lefttable.addCell(blcell);
		lefttable.addCell(blcell);

		lefttable.addCell(blcell);
		lefttable.addCell(blcell);
		lefttable.addCell(blcell);

		// lefttable.addCell(blcell);
		// lefttable.addCell(blcell);
		// lefttable.addCell(blcell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ***********right table**************
		PdfPTable righttable = new PdfPTable(3);

		try {
			righttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		Phrase cel1 = new Phrase("TEL. NO.", font9bold);
		PdfPCell cel1cell = new PdfPCell(cel1);
		cel1cell.setBorder(0);
		cel1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cell1 = null;
		if (cust.getCellNumber1() != null) {
			cell1 = new Phrase(" " + cust.getCellNumber1(), font9);
		}
		PdfPCell cell1cell = new PdfPCell(cell1);
		cell1cell.setBorder(0);
		cell1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cel2 = new Phrase("TEL. NO.", font9bold);
		PdfPCell cel2cell = new PdfPCell(cel2);
		cel2cell.setBorder(0);
		cel2cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cell2 = null;
		if (cust.getCellNumber2() != null) {
			cell2 = new Phrase(" " + cust.getCellNumber2(), font9);
		}
		PdfPCell cell2cell = new PdfPCell(cell2);
		cell2cell.setBorder(0);
		cell2cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase land1 = new Phrase("TEL. NO.", font9bold);
		PdfPCell land1cell = new PdfPCell(land1);
		land1cell.setBorder(0);
		land1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase landline = null;
		if (cust.getLandline() != null) {
			landline = new Phrase(" " + cust.getLandline(), font9);
		}
		PdfPCell landlinecell = new PdfPCell(landline);
		landlinecell.setBorder(0);
		landlinecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		righttable.addCell(cel1cell);
		righttable.addCell(colcell);
		righttable.addCell(cell1cell);

		righttable.addCell(cel2cell);
		righttable.addCell(colcell);
		righttable.addCell(cell2cell);

		righttable.addCell(land1cell);
		righttable.addCell(colcell);
		righttable.addCell(landlinecell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerDetail3() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// **********left table*******************
		PdfPTable lefttable = new PdfPTable(3);

		try {
			lefttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		Phrase poc = new Phrase("CONTACT PERSON", font9bold);
		PdfPCell poccell = new PdfPCell(poc);
		poccell.setBorder(0);
		poccell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase pocname = null;
		if (cust.getFullname() != null) {
			pocname = new Phrase(" " + cust.getFullname(), font9);
		}
		PdfPCell pocnamecell = new PdfPCell(pocname);
		pocnamecell.setBorder(0);
		pocnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// ////////////

		Phrase user = new Phrase("USER TRAINING       1 IMPARTED TO ",
				font9bold);
		PdfPCell usercell = new PdfPCell(user);
		usercell.setBorder(0);
		usercell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase ut1 = null;
		if (wo.getUserTrainingOne() != null) {
			ut1 = new Phrase(" " + wo.getUserTrainingOne(), font9);
		}
		PdfPCell ut1cell = new PdfPCell(ut1);
		ut1cell.setBorderWidth(0);
		ut1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// //////////////

		lefttable.addCell(poccell);
		lefttable.addCell(colcell);
		lefttable.addCell(pocnamecell);

		lefttable.addCell(blcell);
		lefttable.addCell(blcell);
		lefttable.addCell(blcell);

		PdfPCell hrscell = null;
		PdfPCell hrs1cell = null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equals("BUSINESS HOURS")) {
				Phrase hrs = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
					hrs = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName(), font9bold);
				}
				hrscell = new PdfPCell(hrs);
				hrscell.setBorder(0);
				hrscell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase hrs1 = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
					hrs1 = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font9);
				}
				hrs1cell = new PdfPCell(hrs1);
				hrs1cell.setBorder(0);
				hrs1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				lefttable.addCell(hrscell);
				lefttable.addCell(colcell);
				lefttable.addCell(hrs1cell);
			}
		}

		lefttable.addCell(usercell);
		lefttable.addCell(colcell);
		lefttable.addCell(ut1cell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		// ***********right table**************
		PdfPTable righttable = new PdfPTable(3);

		try {
			righttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		Phrase mob = new Phrase("MOB. NO.", font9bold);
		PdfPCell mobcell = new PdfPCell(mob);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mob1 = null;
		if (cust.getCellNumber1() != null) {
			mob1 = new Phrase(" " + cust.getCellNumber1(), font9);
		}
		PdfPCell mob1cell = new PdfPCell(mob1);
		mob1cell.setBorder(0);
		mob1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// ///////////

		Phrase two = new Phrase("2 ", font9bold);
		PdfPCell twocell = new PdfPCell(two);
		twocell.setBorder(0);
		twocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase ut2 = null;
		if (wo.getUserTrainingOne() != null) {
			ut2 = new Phrase(" " + wo.getUserTrainingTwo(), font9);
		}
		PdfPCell ut2cell = new PdfPCell(ut2);
		ut2cell.setBorderWidth(0);
		ut2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// ////////////

		righttable.addCell(mobcell);
		righttable.addCell(colcell);
		righttable.addCell(mob1cell);

		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);

		PdfPCell weekcell = null;
		PdfPCell week1cell = null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equals("WEEKLY OFF")) {
				Phrase week = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
					week = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName(), font9bold);
				}
				weekcell = new PdfPCell(week);
				weekcell.setBorder(0);
				weekcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase week1 = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
					week1 = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font9);
				}
				week1cell = new PdfPCell(week1);
				week1cell.setBorder(0);
				week1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				righttable.addCell(weekcell);
				righttable.addCell(colcell);
				righttable.addCell(week1cell);

			}
		}

		righttable.addCell(twocell);
		righttable.addCell(colcell);
		righttable.addCell(ut2cell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createRemarkHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase remark = new Phrase("REMARK :", font9bold);
		PdfPCell remcell = new PdfPCell(remark);
		remcell.setBorder(0);
		remcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl1 = new Phrase(wo.getRemark(), font9);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setBorderWidthRight(0);

		PdfPTable remtable = new PdfPTable(2);
		remtable.addCell(remcell);
		remtable.addCell(bl1cell);

		try {
			remtable.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		remtable.setWidthPercentage(100f);

		PdfPCell rem1cell = new PdfPCell(remtable);
		rem1cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(rem1cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createLine() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase line = new Phrase(
				"IT IS CERTIFIED THAT THE ABOVE MENTIONED EQUIPMENT HAS BEEN INSTALLED AT OUR PREMISES AT ABOVE ADDRESS TO OUR SATISFACTION & AS PER OUR ORDER ",
				font9);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setBorder(0);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable linetable = new PdfPTable(1);
		linetable.addCell(linecell);
		linetable.addCell(blcell);
		linetable.setWidthPercentage(100);

		try {
			document.add(linetable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createBottom() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(bl2);
		bl2cell.setBorderWidthLeft(0);
		bl2cell.setBorderWidthTop(0);
		bl2cell.setBorderWidthRight(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase serv = new Phrase("SERVICE ENGR. NAME / SIGN", font9bold);
		PdfPCell servcell = new PdfPCell(serv);
		servcell.setBorder(0);
		servcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cust = new Phrase("CUSTOMER SIGN / STAMP", font9bold);
		PdfPCell custcell = new PdfPCell(cust);
		custcell.setBorder(0);
		custcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase date = new Phrase("DATE", font9);
		PdfPCell dtcell = new PdfPCell(date);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable bottomtable = new PdfPTable(3);
		bottomtable.addCell(bl1cell);
		bottomtable.addCell(blankcell);
		bottomtable.addCell(bl2cell);

		bottomtable.addCell(servcell);
		bottomtable.addCell(blankcell);
		bottomtable.addCell(custcell);

		bottomtable.addCell(dtcell);
		bottomtable.addCell(blankcell);
		bottomtable.addCell(dtcell);

		try {
			bottomtable.setWidths(new float[] { 33.33f, 33.33f, 33.33f });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		bottomtable.setWidthPercentage(100);

		try {
			document.add(bottomtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createInstallationDetail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		// blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase install = new Phrase("INSTALLED ON", font9bold);
		PdfPCell installcell = new PdfPCell(install);
		installcell.setBorder(0);
		installcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase date = null;
		if (wo.getCompletionDateone() != null) {
			date = new Phrase(" " + fmt.format(wo.getCompletionDateone()),
					font9);
		}

		PdfPCell dtcell = new PdfPCell(date);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(3);

		lefttable.addCell(installcell);
		lefttable.addCell(colcell);
		lefttable.addCell(dtcell);

		lefttable.addCell(blankcell);
		lefttable.addCell(blankcell);
		lefttable.addCell(blankcell);

		try {
			lefttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		lefttable.setWidthPercentage(100f);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);
		/*********************************************** left table ends here ********************************************/

		/************************************ Right table starts here **************************************/

		Phrase warranty = new Phrase("WARR. ENDS ON", font9bold);
		PdfPCell warcell = new PdfPCell(warranty);
		warcell.setBorder(0);
		warcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase warr = null;
		if (so.getItems().get(0).getWarrantyUntil() != null) {
			warr = new Phrase(" "
					+ fmt.format(so.getItems().get(0).getWarrantyUntil()),
					font9);
		}

		PdfPCell warrcell = new PdfPCell(warr);
		warrcell.setBorder(0);
		warrcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable righttable = new PdfPTable(3);

		righttable.addCell(warcell);
		righttable.addCell(colcell);
		righttable.addCell(warrcell);

		righttable.addCell(blankcell);
		righttable.addCell(blankcell);
		righttable.addCell(blankcell);

		try {
			righttable.setWidths(columnWidth);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);
		/************************************ Right table ends here **************************************/

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createCheckBox() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Phrase cable = new Phrase("CABLING DONE BY GENESIS  :", font9bold);
		PdfPCell cablecell = new PdfPCell(cable);
		cablecell.setBorder(0);
		cablecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cross = null;
		PdfPCell crosscell = null;
		/***************************************** changes done ****************************************************/
		// Phrase cross1 = null;
		// PdfPCell cross1cell = null;

		// ////////////////////////////////////////
		System.out.println(" AAAAAAAAAAAAAAAAA    " + wo.isCablingByGenesis());
		if (wo.isCablingByGenesis() == true) {
			cross = new Phrase("YES[ X ]", font9bold);
		} else {
			cross = new Phrase("YES[   ]", font9bold);
		}

		// if(cableFlag == true){
		// cross = new Phrase("YES[ X ]",font10bold);
		// }
		// else{
		// cross = new Phrase("YES[   ]",font10bold);
		//
		// if(cableFlag == false){
		// cross1 = new Phrase("NO[ X ]",font10bold);
		// }
		// else{
		// cross1 = new Phrase("NO[   ]",font10bold);
		// }
		// }

		crosscell = new PdfPCell(cross);
		crosscell.setBorder(0);
		crosscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/************************************************************/
		// Phrase yes = new Phrase("YES[   ]",font10bold);
		// PdfPCell yescell = new PdfPCell(yes);
		// yescell.setBorder(0);
		// yescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**************************************************************/

		Phrase cross1 = null;
		PdfPCell cross1cell = null;

		if (wo.isCablingByGenesis() == false) {
			cross1 = new Phrase("NO[ X ]", font9bold);
		}

		else {
			cross1 = new Phrase("NO[   ]", font9bold);
		}

		// //////////////////////////////////////////////////////////

		cross1cell = new PdfPCell(cross1);
		cross1cell.setBorder(0);
		cross1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/******************************************************/
		// Phrase no = new Phrase("NO[   ]",font10bold);
		// PdfPCell nocell = new PdfPCell(no);
		// nocell.setBorder(0);
		// nocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**********************************************************/

		PdfPTable cabletable = new PdfPTable(3);
		cabletable.addCell(cablecell);
		cabletable.addCell(crosscell);
		cabletable.addCell(cross1cell);

		cabletable.addCell(blankcell);
		cabletable.addCell(blankcell);
		cabletable.addCell(blankcell);

		try {
			cabletable.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		cabletable.setWidthPercentage(100);

		try {
			document.add(cabletable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/******************************************************************************************************************/
	public void createProductTable() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(3);

		Phrase name = new Phrase("ITEM NAME", font1);
		PdfPCell namecell = new PdfPCell(name);
		namecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase desc = new Phrase("DESCRIPTION", font1);
		PdfPCell desccell = new PdfPCell(desc);
		desccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase type = new Phrase("TYPE", font1);
		PdfPCell typecell = new PdfPCell(type);
		typecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(namecell);
		table.addCell(desccell);
		table.addCell(typecell);

		System.out.println("size of list:::::::" + itemslist.size());
		for (int i = 0; i < itemslist.size(); i++) {

			chunk = new Phrase(itemslist.get(i).getProductName() + "", font8);
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(itemslist.get(i).getComment() + "", font8);
			pdfdesc = new PdfPCell(chunk);
			pdfdesc.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(itemslist.get(i).getProductType() + "", font8);
			pdftype = new PdfPCell(chunk);
			pdftype.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfname);
			table.addCell(pdfdesc);
			table.addCell(pdftype);

		}

		PdfPCell prodcell = new PdfPCell(table);
		prodcell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(prodcell);
		parenttable.addCell(blankcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/************************************************************************************/

	// public void createEquipmentDetail(){
	//
	// Paragraph blank = new Paragraph();
	// blank.add(Chunk.NEWLINE);
	// blank.add(Chunk.NEWLINE);
	//
	// PdfPCell blankcell = new PdfPCell(blank);
	// blankcell.setBorder(0);
	//
	// Phrase equipment = new Phrase("EQUIPMENT DETAILS  :",font10bold);
	// PdfPCell equipcell = new PdfPCell(equipment);
	// equipcell.setBorder(0);
	// equipcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase CCTV = null;
	// PdfPCell tvcell = null;
	// if(equipmentCCTVFlag ==true){
	// CCTV =new Phrase("CCTV",font10bold);
	// }
	// else{
	// CCTV =new Phrase("CCTV",font10);
	// }
	//
	// tvcell = new PdfPCell(CCTV);
	// tvcell.setBorder(0);
	// tvcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase VDP = null;
	// PdfPCell vdpcell = null;
	//
	// if(equipmentVDPFlag == true){
	// VDP = new Phrase("VDP",font10bold);
	// }
	// else{
	// VDP = new Phrase("VDP",font10);
	// }
	//
	// vdpcell = new PdfPCell(VDP);
	// vdpcell.setBorder(0);
	// vdpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	//
	// PdfPTable equiptable = new PdfPTable(3);
	// equiptable.addCell(equipcell);
	// equiptable.addCell(tvcell);
	// equiptable.addCell(vdpcell);
	//
	// equiptable.addCell(blankcell);
	// equiptable.addCell(blankcell);
	// equiptable.addCell(blankcell);
	//
	// try {
	// equiptable.setWidths(colWidth);
	// } catch (DocumentException e1) {
	// e1.printStackTrace();
	// }
	// equiptable.setWidthPercentage(100);
	//
	// try {
	// document.add(equiptable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }

	public void createEquipmentDetail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		PdfPTable equiptable = new PdfPTable(2);

		Phrase equipment = new Phrase("EQUIPMENT DETAILS  :", font9bold);
		PdfPCell equipcell = new PdfPCell(equipment);
		equipcell.setBorder(0);
		equipcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**************************************/
		// Phrase CCTV = null;
		// PdfPCell tvcell = null;
		// if(equipmentFlag ==true){
		// CCTV =new Phrase("CCTV",font10bold);
		// }
		// else{
		// CCTV =new Phrase("CCTV",font10);
		// }
		//
		// tvcell = new PdfPCell(CCTV);
		// tvcell.setBorder(0);
		// tvcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// Phrase VDP = null;
		// PdfPCell vdpcell = null;
		//
		// if(equipmentFlag == false){
		// VDP = new Phrase("VDP",font10bold);
		// }
		// else{
		// VDP = new Phrase("VDP",font10);
		// }
		//
		// vdpcell = new PdfPCell(VDP);
		// vdpcell.setBorder(0);
		// vdpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/********************************************************/

		equiptable.addCell(equipcell);

		for (int i = 0; i < wo.getBomTable().size(); i++) {

			chunk = new Phrase(wo.getBomTable().get(i).getProductName() + "",
					font9bold);
			pdfprodName = new PdfPCell(chunk);
			pdfprodName.setBorderWidthLeft(0);
			pdfprodName.setBorderWidthTop(0);
			pdfprodName.setBorderWidthRight(0);
			pdfprodName.setHorizontalAlignment(Element.ALIGN_LEFT);

			equiptable.addCell(pdfprodName);

		}

		try {
			equiptable.setWidths(new float[] { 25, 75 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		equiptable.setWidthPercentage(100);

		PdfPCell cell = new PdfPCell(equiptable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blankcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
