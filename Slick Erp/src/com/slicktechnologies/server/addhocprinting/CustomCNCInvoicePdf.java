package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.googlecode.objectify.Key;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.salesorder.SalesOrderLineItemTable;
import com.slicktechnologies.server.CncBillServiceImpl;
import com.slicktechnologies.server.humanresourcelayer.PaySlipServiceImpl;
import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.TaxSummaryBean;
import com.slicktechnologies.shared.common.EmployeeProjectAllocation;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.attendance.Attendance;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.calendar.Holiday;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.EmployeeLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.AvailedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.timereport.EmployeeOvertime;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.AnnexureType;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesAnnexureType;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class CustomCNCInvoicePdf {

	Logger logger = Logger.getLogger("CustomCNCInvoice.class");

	Document document;	
	Invoice invoice;
	List<BillingDocumentDetails> billingDoc; 
	List<ContractCharges> billingTaxesLis;
	Customer cust;
	List<ArticleType> articletype;
	ProcessConfiguration processConfig;
	Company comp;
	String companyName = "";
	int noOfLines = 20;
	
	int prouductCount=0;
	SuperProduct sup;
	double total=0;
	SimpleDateFormat fmt,fmt2,fmt3;
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df2 = new DecimalFormat("00");
	List<State> stateList;
	CNC cnc ;
	CompanyPayment companyPayment;
	double totalTax = 0;
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 6);
	Font font5bold = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD);
	Font font5 = new Font(Font.FontFamily.HELVETICA, 5);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	PdfPCell coloncell = null;
	Branch branchDt;
	String invoiceype = "";
	double totalAmount = 0 , totalCGST = 0 , totalSGST = 0 , totalIGST = 0;
	HashMap<String ,TaxSummaryBean> map;
	boolean invoiceRefNo=false;
	
	/**
	 * Date : 22-12-2018 By Anil
	 */
	CustomerBranchDetails customerBranch=null;
	int j = 0;
	
	/**
	 * @author Anil, Date: 31-01-2019
	 * 
	 */
	int totalNoOfLineItems=0;
	
	
	/**
	 * @author Anil, Date : 22-02-2019
	 * This flag is true if same designation is added more than ones in CNC
	 * Issue raised by Megha & Ashish for sasha
	 */
	boolean designationFlag=false;
	HashSet<String> hsDesignation; 
	
	/**
	 * @author Anil ,Date :13-03-2019
	 */
	int minRange=8;
	int maxRange=20;
	
	
	private PdfPCell imageSignCell;
	
	float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
	float[] columnCollonWidth2 = { 6.5f,3.5f};
	float[] columnCollonWidth3 = { 1f,9f};
	
	/**
	 * @author Anil
	 * @since 23-10-2020
	 */
	boolean normalDayOtFlag=false;
	boolean publicHolidayOtFlag=false;
	boolean nationalHolidayOtFlag=false;
	
	float[] columnWidth13= { 5.69f,9.38f,9.38f,7.0f,7.69f,7.0f,7.69f,7.69f,7.69f,7.69f,7.69f,7.69f,7.69f};
	float[] columnWidth14= { 5.14f,8.28f,8.28f,7.0f,7.14f,7.0f,7.14f,7.14f,7.14f,7.14f,7.14f,7.14f,7.14f,7.14f};
	float[] columnWidth15= { 4.3f,8.20f,8.22f,5.3f,6.80f,5.7f,6.80f,6.80f,6.80f,6.80f,6.80f,6.80f,6.80f,6.80f,6.80f};
		
	/**
	 * @author Anil
	 * @since 01-12-2020
	 * staffing column should also be dynamic
	 */
	boolean staffingFlag=false;
	float[] columnWidth12= { 6.00f,9.66f,9.66f,8.0f,8.33f,8.33f,8.33f,8.33f,8.33f,8.33f,8.33f,8.33f};
	int noOfStaff=0;
	
	/**
	 * @author Anil @since 15-11-2021
	 *
	 */
	PdfUtility pdfUtility=new PdfUtility();
	
	float[] columnWidth18= { 3.0f,5.0f,8.70f,5.55f,5.55f,5.55f,5.55f,4.0f,7.10f,5.55f,5.55f,5.55f,5.55f,5.55f,5.55f,5.55f,5.55f,5.55f};
	
	public void setInvoice(Long count) {
		invoice = ofy().load().type(Invoice.class).id(count).now();
		if (invoice.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoice.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", invoice.getPersonInfo().getCount())
					.filter("companyId", invoice.getCompanyId()).first().now();

		// Load Company
		if (invoice.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoice.getCompanyId()).first().now();
		companyName = comp.getBusinessUnitName();

		if (invoice.getCompanyId() != null)
			cnc = ofy().load().type(CNC.class)
					.filter("count", invoice.getContractCount())
					.filter("companyId", invoice.getCompanyId()).first().now();
		else
			cnc = ofy().load().type(CNC.class)
					.filter("count", invoice.getContractCount()).first().now();
		
		/**
		 * @author Anil @since 17-11-2021
		 * If type of order is sales in that case we are not storing cnc id in contractcount so we will load CNC using refernce number
		 */
		try{
			if(invoice.getTypeOfOrder().equals(AppConstants.SALESORDER)&&(invoice.getSubBillType().equals(AppConstants.CONSUMABLES)||invoice.getSubBillType().equals(AppConstants.FIXEDCONSUMABLES))){
				if(invoice.getContractCount()!=Integer.parseInt(invoice.getRefNumber())){
					cnc = ofy().load().type(CNC.class).filter("companyId", invoice.getCompanyId()).filter("count", Integer.parseInt(invoice.getRefNumber())).first().now();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		

		if (invoice != null && invoice.getBranch() != null
				&& invoice.getBranch().trim().length() > 0) {

		branchDt = ofy().load().type(Branch.class)
					.filter("companyId", invoice.getCompanyId())
					.filter("buisnessUnitName", invoice.getBranch())
					.first().now();
		}
		
		if(invoice.getPaymentMode()!=null&&!invoice.getPaymentMode().equals("")){
			logger.log(Level.SEVERE,"INSIDE load Companypayment");
			try{
				String paymentMode="";
				String paymentmodevalue="";
				if(invoice.getPaymentMode().contains("/")){
					String [] payArr=invoice.getPaymentMode().split("/");
					paymentMode=payArr[1];
					
					paymentmodevalue=paymentMode.trim();
					logger.log(Level.SEVERE,"PAYMENT MODE"+paymentmodevalue);
				}
				companyPayment = ofy().load().type(CompanyPayment.class)
						.filter("paymentBankName", paymentmodevalue)
						.filter("companyId", invoice.getCompanyId()).first()
						.now();
				
				logger.log(Level.SEVERE,"INSIDE TRY"+paymentMode);
//				if(companyPayment==null){
//					logger.log(Level.SEVERE,"BANKKKK NAMEEEE"+companyPayment.getPaymentBranch());
//				}else{
//					logger.log(Level.SEVERE,"BANKKKK NAMEEEE11111"+companyPayment.getPaymentBranch());
//				}
				
			}catch(Exception e){
					
			}
		}else{
			logger.log(Level.SEVERE,"INSIDE ELSEEEEE load Companypayment");
			companyPayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", invoice.getCompanyId()).first()
					.now();
		}
//		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(
//				"Branch", "BranchAsCompany", comp.getCompanyId())) {
//
//			logger.log(Level.SEVERE, "Process active --");
//			
//				if (branchDt != null && branchDt.getPaymentMode() != null
//						&& !branchDt.getPaymentMode().trim().equals("")) {
//
//					logger.log(Level.SEVERE,
//							"Process active --" + branchDt.getPaymentMode());
//
//					List<String> paymentDt = Arrays.asList(branchDt
//							.getPaymentMode().trim().split("/"));
//
//					if (paymentDt.get(0).trim().matches("[0-9]+")) {
//
//						int payId = Integer.parseInt(paymentDt.get(0).trim());
//
//						companyPayment = ofy().load()
//								.type(CompanyPayment.class)
//								.filter("count", payId)
//								.filter("companyId", invoice.getCompanyId())
//								.first().now();
//
//						if (companyPayment != null) {
//							comp = ServerAppUtility.changeBranchASCompany(
//									branchDt, comp);
//						}
//
//					}
//
//				}
//			
//		}

		stateList = ofy().load().type(State.class)
				.filter("companyId", invoice.getCompanyId()).list();
		Phrase colonph = new Phrase(":", font6);
		coloncell = new PdfPCell(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		/**
		 * Date : 22-12-2018 By Anil
		 */
		if(invoice!=null&&invoice.getCustomerBranch()!=null&&!invoice.getCustomerBranch().equals("")){
			customerBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId", invoice.getCompanyId()).filter("buisnessUnitName", invoice.getCustomerBranch()).filter("status", true).first().now();
		}
		/**
		 * @author Anil,Date : 31-01-2019
		 */
	if(invoice!=null&&invoice.getSalesOrderProducts()!=null){
			/**
			 * @author Amol, Date : 04-06-2019
			 * for calculating non zero quantity invoice line item
			 */
//			totalNoOfLineItems=invoice.getSalesOrderProducts().size();
			for(SalesOrderProductLineItem item :invoice.getSalesOrderProducts()){
				if(item.getTotalAmount()!=0){
					totalNoOfLineItems++;
				}
			}
		}
		
	}
	
	public void createPdf(String preprintStatus) {
		
		fmt = new SimpleDateFormat("dd MMM yyyy");
		fmt2 = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt2.setTimeZone(TimeZone.getTimeZone("IST"));
		
		fmt3 = new SimpleDateFormat("MMM-yy");
		fmt3.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(preprintStatus.equals("plane")){
			createLogo();
		}
		else{
			if(preprintStatus.equals("yes")){
				createBlankforUPC();
			}
			if(preprintStatus.equals("no")){
//				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
//				}
			}
		}
		
		createInvoiceNoAndInvoiceDateDetails();
		createCustomerAndCompanyDetails();
		createBillingPeriodDetails();
		createDescriptionDetails();
		createProductDetails();
		
		createEOEAndDigitalSignature();
		createAmountInWords();
		createfooterPart();
		
		ArrayList<String> otTypeList=getOtTypeList();
		if(otTypeList.size()!=0){
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			createAnnextureTable(otTypeList);
		}
		try{
			createSalesOrderSummaryTable();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	private void createEOEAndDigitalSignature() {
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		
		
		Paragraph blank = new Paragraph();
        blank.add(Chunk.NEWLINE);
        PdfPCell cellBlank = new PdfPCell(blank);
        cellBlank.setBorder(0);
        cellBlank.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        Phrase EandOE = new Phrase("E.& O.E. ",font10bold);
		
		PdfPCell cellEandOE = new PdfPCell(EandOE);
		cellEandOE.setBorder(0);
		cellEandOE.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String commentval = "";
		if(invoice.getComment1()!=null){
			commentval = invoice.getComment1();
		}
		
		Phrase comment = new Phrase(commentval,font9);
		
		PdfPCell cellcomment = new PdfPCell(comment);
		cellcomment.setBorder(0);
		cellcomment.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(cellBlank);
		table.addCell(cellEandOE);
		table.addCell(cellcomment);
		
		PdfPTable tableDigitalSign = new PdfPTable(1);
		tableDigitalSign.setWidthPercentage(100);
		
		tableDigitalSign.addCell(cellBlank);
		
		String companyName = "For ";
		if(branchDt.getCorrespondenceName()!=null && !branchDt.getCorrespondenceName().equals("")){
			companyName +=branchDt.getCorrespondenceName();
		}
		else{
			companyName += comp.getBusinessUnitName();
		}
		Phrase companyPhrase = new Phrase(companyName, font10bold);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setBorder(0);
		companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		tableDigitalSign.addCell(companyParaCell);
		
		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		
		if(branchDt.getUploadDigitalSign()!=null&&branchDt.getUploadDigitalSign().getUrl()!=null&&!branchDt.getUploadDigitalSign().getUrl().equals("")){
			digitalDocument=branchDt.getUploadDigitalSign();
		}
		
		
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (imageSignCell != null) {
			tableDigitalSign.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			tableDigitalSign.addCell(blank1Cell);
			tableDigitalSign.addCell(blank1Cell);
			tableDigitalSign.addCell(blank1Cell);
			tableDigitalSign.addCell(blank1Cell);

		}
		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		PdfPCell signParaCell = new PdfPCell(signAuth);
		signParaCell.setBorder(0);
		signParaCell.setPaddingLeft(30f);
		signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		tableDigitalSign.addCell(signParaCell);
		
		PdfPTable parent = new PdfPTable(2);
		parent.setWidthPercentage(100);
		
		parent.addCell(table);
		parent.addCell(tableDigitalSign);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createProductDetails() {


		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columnCollonWidth3);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase srNo = new Phrase("1",font10);
		PdfPCell cellsrNo = new PdfPCell(srNo);
		cellsrNo.setBorder(0);
		cellsrNo.setBorderWidthRight(1);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		String commnet = "";
		if(invoice.getComment()!=null){
			commnet = invoice.getComment();
		}
		
		Phrase phdescriptionval = new Phrase(commnet,font10);
		PdfPCell celldescriptionval = new PdfPCell(phdescriptionval);
		celldescriptionval.setBorder(0);
		celldescriptionval.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Paragraph blank = new Paragraph();
        blank.add(Chunk.NEWLINE);
        PdfPCell cellBlank = new PdfPCell(blank);
        cellBlank.setBorder(0);
        cellBlank.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        Paragraph borderblank = new Paragraph();
        borderblank.add(Chunk.NEWLINE);
        PdfPCell cellborderblank = new PdfPCell(blank);
        cellborderblank.setBorder(0);
        cellborderblank.setBorderWidthRight(1);
        cellborderblank.setHorizontalAlignment(Element.ALIGN_LEFT);
	        
		table.addCell(cellsrNo);
		table.addCell(celldescriptionval);
		
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		table.addCell(cellborderblank);
		table.addCell(cellBlank);
		
		PdfPCell tablepcell = new PdfPCell(table);
		
		PdfPTable table2 = new PdfPTable(2);
		table2.setWidthPercentage(100);
		try {
			table2.setWidths(columnCollonWidth2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase annexture = new Phrase("Annexure-I",font10);
		PdfPCell cellannexture = new PdfPCell(annexture);
		cellannexture.setBorder(0);
		cellannexture.setBorderWidthRight(1);
		cellannexture.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase invoiceAmt = new Phrase(invoice.getTotalAmtExcludingTax()+"",font10);
		PdfPCell cellinvoiceAmt = new PdfPCell(invoiceAmt);
		cellinvoiceAmt.setBorder(0);
		cellinvoiceAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase total = new Phrase("Total"+"",font10bold);
		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setBorderWidthBottom(0);
		celltotal.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase totalVal = new Phrase(invoice.getTotalAmtExcludingTax()+"",font10bold);
		PdfPCell celltotalVal = new PdfPCell(totalVal);
		celltotalVal.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Paragraph blankwithrowspan = new Paragraph();
		blankwithrowspan.add(Chunk.NEWLINE);
        PdfPCell cellblankwithrowspan = new PdfPCell(blankwithrowspan);
        cellblankwithrowspan.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellblankwithrowspan.setColspan(2);
        cellblankwithrowspan.setBorderWidthRight(0);
        
        Phrase grandTotal = new Phrase("Grand Total"+"",font9bold);
		PdfPCell cellgrandTotal = new PdfPCell(grandTotal);
		cellgrandTotal.setBorder(0);
		cellgrandTotal.setBorderWidthRight(1);
		cellgrandTotal.setBorderWidthTop(1);
		cellgrandTotal.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase grandTotalval = new Phrase(invoice.getNetPayable()+"",font10);
		PdfPCell cellgrandTotalval = new PdfPCell(grandTotalval);
		cellgrandTotalval.setBorder(0);
		cellgrandTotalval.setBorderWidthTop(1);
		cellgrandTotalval.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		
		
        table2.addCell(cellannexture);
        table2.addCell(cellinvoiceAmt);
        
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        table2.addCell(cellborderblank);
        table2.addCell(cellBlank);
        
        
        
        table2.addCell(celltotal);
        table2.addCell(celltotalVal);
        table2.addCell(cellblankwithrowspan);
        
        double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0,utgstTotalVal=0;
		for (int i = 0; i < invoice.getBillingTaxes().size(); i++) {
			if (invoice.getBillingTaxes().get(i).getTaxChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ invoice.getBillingTaxes().get(i)
								.getPayableAmt();
				Phrase IGSTphrase = new Phrase("IGST @"
						+ invoice.getBillingTaxes().get(i)
								.getTaxChargePercent() + " % of Total", font9bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorder(0);
				IGSTphraseCell.setBorderWidthRight(1);
				IGSTphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);


				Phrase IGSTValphrase = new Phrase(df.format(invoice
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font9);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				table2.addCell(IGSTphraseCell);
				table2.addCell(IGSTValphraseCell);

			} else if (invoice.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ invoice.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase SGSTphrase = new Phrase("SGST @"
						+ invoice.getBillingTaxes().get(i)
								.getTaxChargePercent() + " % of Total", font9bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorder(0);
				SGSTphraseCell.setBorderWidthRight(1);
				SGSTphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);


				Phrase SGSTValphrase = new Phrase(df.format(invoice
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font9);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				table2.addCell(SGSTphraseCell);
				table2.addCell(SGSTValphraseCell);
			} else if (invoice.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ invoice.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase CGSTphrase = new Phrase("CGST @"
						+ invoice.getBillingTaxes().get(i)
								.getTaxChargePercent() + " % of Total", font9bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				CGSTphraseCell.setBorderWidthRight(1);
				CGSTphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase CGSTValphrase = new Phrase(df.format(invoice
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font9);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				table2.addCell(CGSTphraseCell);
				table2.addCell(CGSTValphraseCell);
			}
			/**
			 * @author Anil @since 
			 */
			else if (invoice.getBillingTaxes().get(i).getTaxChargeName().equalsIgnoreCase("UTGST")) {
				utgstTotalVal = utgstTotalVal+ invoice.getBillingTaxes().get(i).getPayableAmt();

				Phrase CGSTphrase = new Phrase("UTGST @"+ invoice.getBillingTaxes().get(i).getTaxChargePercent() + " % of Total", font9bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				CGSTphraseCell.setBorderWidthRight(1);
				CGSTphraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase CGSTValphrase = new Phrase(df.format(invoice.getBillingTaxes().get(i).getPayableAmt())+ "", font9);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				table2.addCell(CGSTphraseCell);
				table2.addCell(CGSTValphraseCell);
			}
		}

        
        table2.addCell(cellgrandTotal);
        table2.addCell(cellgrandTotalval);

        
        PdfPCell table2cell = new PdfPCell(table2);
        
        PdfPTable parent = new PdfPTable(2);
        parent.setWidthPercentage(100);
        
        parent.addCell(tablepcell);
        parent.addCell(table2cell);
        
        try {
     			document.add(parent);
     			
     		} catch (DocumentException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
		
	
	}

	private void createAmountInWords() {
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		
		String amtinwords = "Amount in words : "+SalesInvoicePdf.convert(invoice.getNetPayable());
		
		Phrase phAmtInWords = new Phrase(amtinwords,font10bold);

		PdfPCell cellphAmtInWords= new PdfPCell(phAmtInWords);
//		cellphAmtInWords.setBorder(0);
		cellphAmtInWords.setPaddingLeft(40f);
		cellphAmtInWords.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(cellphAmtInWords);
		
		
		try {
   			document.add(table);
   			
   		} catch (DocumentException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
		
	}

	private void createfooterPart() {
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		
		String companyName = "";
		String companyAddress = "";
		String corporateIdentifyNumber = "Corporate Identify Number : ";
		
		if(branchDt.getCorrespondenceName()!=null && !branchDt.getCorrespondenceName().equals("")){
			companyName = branchDt.getCorrespondenceName();
			companyAddress = branchDt.getAddress().getCompleteAddress();
		}
		else{
			companyName = comp.getBusinessUnitName();
			companyAddress = comp.getAddress().getCompleteAddress();
		}
//		corporateIdentifyNumber += getValueFromCompanyArticalType("ServiceInvoice", "CIN");
		String valcorporateIdentifyNumber = getValueFromCompanyArticalType("ServiceInvoice", "CIN");
		if(!valcorporateIdentifyNumber.equals("")){
			corporateIdentifyNumber +=valcorporateIdentifyNumber;
		}
		Phrase phcompanyName = new Phrase(companyName,font10bold);
		Phrase phcompanyAddress = new Phrase(companyAddress,font9);
		Phrase phCorporateIdntyNumber = new Phrase(corporateIdentifyNumber,font9);
		
		PdfPCell cellcompanyName = new PdfPCell(phcompanyName);
		cellcompanyName.setBorder(0);
		cellcompanyName.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcompanyAddress = new PdfPCell(phcompanyAddress);
		cellcompanyAddress.setBorder(0);
		cellcompanyAddress.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcorporateIdentifyNumber = new PdfPCell(phCorporateIdntyNumber);
		cellcorporateIdentifyNumber.setBorder(0);
		cellcorporateIdentifyNumber.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellcompanyName);
		table.addCell(cellcompanyAddress);
		if(!valcorporateIdentifyNumber.equals("")){
			table.addCell(cellcorporateIdentifyNumber);
		}
		
		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(table);
		
		try {
   			document.add(parent);
   			
   		} catch (DocumentException e) {
   			// TODO Auto-generated catch block
   			e.printStackTrace();
   		}
		
	}

	private void createDescriptionDetails() {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columnCollonWidth3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase sNo = new Phrase("SR. NO.",font9bold);
		Phrase des = new Phrase("DESCRIPTION",font9bold);
		
	 	PdfPCell cellsNo = new PdfPCell(sNo);
	 	cellsNo.setBorderWidthLeft(0);
	 	cellsNo.setBorderWidthBottom(0);
	 	cellsNo.setBorderWidthTop(0);
	 	cellsNo.setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell celldes = new PdfPCell(des);
        celldes.setBorder(0);
        celldes.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
		table.addCell(cellsNo);
		table.addCell(celldes);
		
		PdfPCell leftCell = new PdfPCell(table);
		
		PdfPTable table2 = new PdfPTable(3);
		table2.setWidthPercentage(100);
		
		Phrase rate = new Phrase("RATE",font9bold);
		Phrase days = new Phrase("DAYS",font9bold);
		Phrase amt = new Phrase("AMOUNT(Rs.)",font9bold);
		
	 	PdfPCell cellrate = new PdfPCell(rate);
	 	cellrate.setBorder(0);
	 	cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell celldays = new PdfPCell(days);
        celldays.setBorder(0);
        celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell cellamt = new PdfPCell(amt);
        cellamt.setBorder(0);
        cellamt.setHorizontalAlignment(Element.ALIGN_CENTER);
	        
        table2.addCell(cellrate);
        table2.addCell(celldays);
        table2.addCell(cellamt);
        
        PdfPTable parent = new PdfPTable(2);
        parent.setWidthPercentage(100);
        
        parent.addCell(leftCell);
        parent.addCell(table2);
        
        try {
     			document.add(parent);
     			
     		} catch (DocumentException e) {
     			// TODO Auto-generated catch block
     			e.printStackTrace();
     		}
		
	}

	private void createBillingPeriodDetails() {

		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		
		String branchName = invoice.getBranch();
		
		if(cnc.getState()!=null){
			branchName=cnc.getState();
		}
		
		String strBillingPeriodFromDate = "Billing Period : ";
		if(invoice.getBillingPeroidFromDate()!=null && invoice.getBillingPeroidToDate()!=null){
			strBillingPeriodFromDate += fmt2.format(invoice.getBillingPeroidFromDate());
			strBillingPeriodFromDate +="  to  ";
			strBillingPeriodFromDate += fmt2.format(invoice.getBillingPeroidToDate());

		}
		
		Phrase phbranchName = new Phrase(branchName,font10bold);
        Phrase billingPeriod = new Phrase(strBillingPeriodFromDate,font10bold);

        PdfPCell cellbranchName = new PdfPCell(phbranchName);
        cellbranchName.setHorizontalAlignment(Element.ALIGN_CENTER);
        PdfPCell cellbillingPeriod = new PdfPCell(billingPeriod);
        cellbillingPeriod.setHorizontalAlignment(Element.ALIGN_RIGHT);
        
        table.addCell(cellbranchName);
        table.addCell(cellbillingPeriod);
        
        try {
			document.add(table);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createCustomerAndCompanyDetails() {
		
    	ServerAppUtility serverAppUtility = new ServerAppUtility();

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		Phrase to = new Phrase("To",font10bold);
        Phrase customerName = new Phrase(StringUtils.capitalize(invoice.getPersonInfo().getFullName()),font10bold);
        
        Phrase address = new Phrase(cnc.getBillingAddress().getCompleteAddress(),font9bold);
        
//        Paragraph Topara = new Paragraph();
//        Topara.setTabSettings(new TabSettings(40f));
//        Topara.add(Chunk.TABBING);
//        Topara.add(new Chunk("To",font12bold));

        
        PdfPCell cellTo = new PdfPCell(to);
        cellTo.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellTo.setPaddingLeft(20f);
        cellTo.setBorder(0);
        
        PdfPCell cellCustomerName = new PdfPCell(customerName);
        cellCustomerName.setPaddingLeft(40f);
        cellCustomerName.setBorder(0);
        cellCustomerName.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        PdfPCell celladdress = new PdfPCell(address);
        celladdress.setPaddingLeft(40f);
        celladdress.setBorder(0);
        celladdress.setHorizontalAlignment(Element.ALIGN_LEFT);

        Paragraph blank = new Paragraph();
        blank.add(Chunk.NEWLINE);
        PdfPCell cellBlank = new PdfPCell(blank);
        cellBlank.setBorder(0);
        cellBlank.setHorizontalAlignment(Element.ALIGN_LEFT);
        
        String gstNo = "";
        /**
         * @author Anil @since 30-03-2021
         * picking gst number from cnc first then customer master
         */
//        if(cnc.getRefNum()!=null && !cnc.getRefNum().equals("")){
//        	gstNo = cnc.getRefNum();
//        }
        if(cnc.getCustomerGstNumber()!=null && !cnc.getCustomerGstNumber().equals("")){
        	gstNo = cnc.getCustomerGstNumber();
        }
        else{
            gstNo = serverAppUtility.getGSTINOfCustomer(cust,null, "ServiceInvoice");
        }
        
        PdfPCell cellGSTNo= null;
        
        if(!gstNo.equals("")){
        	String strgstno = "GSTN No :- "+gstNo;
        	Phrase GSTNo = new Phrase(strgstno,font10bold);
        	cellGSTNo = new PdfPCell(GSTNo);
        	cellGSTNo.setPaddingLeft(40f);
        	cellGSTNo.setBorder(0);
        	cellGSTNo.setHorizontalAlignment(Element.ALIGN_LEFT);
        }
        
        String stateCode = "";
        if(cnc!=null && cnc.getBillingAddress().getState()!=null){
	        for(State state : stateList){
	        	if(state.getStateName().equals(cnc.getBillingAddress().getState())){
	        		stateCode = state.getStateCode().trim();
	        	}
	        }
        }
        
        PdfPCell cellstateCode= null;
        
        if(!stateCode.equals("")){
        	String strstateCode = "State Code :- "+stateCode;
        	Phrase phStateCode = new Phrase(strstateCode,font9bold);
        	cellstateCode = new PdfPCell(phStateCode);
        	cellstateCode.setPaddingLeft(40f);
        	cellstateCode.setBorder(0);
        	cellstateCode.setHorizontalAlignment(Element.ALIGN_LEFT);
        }
        
        table.addCell(cellTo);
        table.addCell(cellCustomerName);
        table.addCell(celladdress);
        table.addCell(cellBlank);
        if(cellGSTNo!=null){
        	 table.addCell(cellGSTNo);
        }
        if(cellstateCode!=null){
        	 table.addCell(cellstateCode);
        }
        
        
        
        
        
        PdfPTable companyInfoTable = new PdfPTable(3);
        companyInfoTable.setWidthPercentage(100);
		try {
			companyInfoTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase panNo = new Phrase("PAN NO",font10bold);
        Phrase SerTaxNo = new Phrase("Ser Tax No",font10bold);
        Phrase esiNo = new Phrase("ESI No",font10bold);
        Phrase pfNo = new Phrase("P.F No",font10bold);
        Phrase gstn = new Phrase("GSTN No",font10bold);
        Phrase statecode = new Phrase("State Code",font10bold);
        Phrase sacCode = new Phrase("SAC Code",font10bold);
        Phrase serviceCate = new Phrase("Service Category",font10bold);
        
        PdfPCell cellPanNo = new PdfPCell(panNo);
        cellPanNo.setBorder(0);
        cellPanNo.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellSerTaxNo = new PdfPCell(SerTaxNo);
        cellSerTaxNo.setBorder(0);
        cellSerTaxNo.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellesiNo = new PdfPCell(esiNo);
        cellesiNo.setBorder(0);
        cellesiNo.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellpfNo = new PdfPCell(pfNo);
        cellpfNo.setBorder(0);
        cellpfNo.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellgstn = new PdfPCell(gstn);
        cellgstn.setBorder(0);
        cellgstn.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellstatecode = new PdfPCell(statecode);
        cellstatecode.setBorder(0);
        cellstatecode.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellsacCode = new PdfPCell(sacCode);
        cellsacCode.setBorder(0);
        cellsacCode.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellserviceCate = new PdfPCell(serviceCate);
        cellserviceCate.setBorder(0);
        cellserviceCate.setHorizontalAlignment(Element.ALIGN_LEFT);

        Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		
		 String valPANNO = getValueFromCompanyArticalType("ServiceInvoice","PAN No.");
		 String valSerTaxNo = getValueFromCompanyArticalType("ServiceInvoice","Ser Tax No.");
		 String valpfNo = "";
		 if(comp.getPfGroupNum()!=null){
			 valpfNo = comp.getPfGroupNum();
		 }
		 String valEsicNo = "";
		 if(branchDt.getEsicCode()!=null){
			 valEsicNo = branchDt.getEsicCode();
		 }
		 String valComGSTNo = serverAppUtility.getGSTINOfCompany(comp, invoice.getBranch());
		 String valStateCode = "";
		 for(State state : stateList){
	        	if(state.getStateName().equals(branchDt.getAddress().getState())){
	        		valStateCode = state.getStateCode().trim();
	        	}
	       }
		 String valSACCode = getValueFromCompanyArticalType("ServiceInvoice", "SAC Code");
		 String valserviceCategory = ""; 
		 if(comp.getCompanyType()!=null){
			 valserviceCategory = comp.getCompanyType();
		 }
		 
		 Phrase panNoVal = new Phrase(valPANNO,font10);
		 Phrase serTaxNoVal = new Phrase(valSerTaxNo,font10);
		 Phrase PFno = new Phrase(valpfNo,font10);
		 Phrase ESINo = new Phrase(valEsicNo,font10);
		 Phrase compGSTNo = new Phrase(valComGSTNo,font10);
		 Phrase phstateCode = new Phrase(valStateCode,font10);
		 Phrase compSACCode = new Phrase(valSACCode,font10);
		 Phrase serviceCat = new Phrase(valserviceCategory,font10);

		 
		 PdfPCell cellPanNoval = new PdfPCell(panNoVal);
		 cellPanNoval.setBorder(0);
		 cellPanNoval.setPaddingLeft(15f);
		 cellPanNoval.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 PdfPCell cellserTaxNoVal = new PdfPCell(serTaxNoVal);
		 cellserTaxNoVal.setBorder(0);
		 cellserTaxNoVal.setPaddingLeft(15f);
		 cellserTaxNoVal.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 PdfPCell cellPFno= new PdfPCell(PFno);
		 cellPFno.setBorder(0);
		 cellPFno.setPaddingLeft(15f);
		 cellPFno.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 PdfPCell cellESINo= new PdfPCell(ESINo);
		 cellESINo.setBorder(0);
		 cellESINo.setPaddingLeft(15f);
		 cellESINo.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		 PdfPCell cellcompGSTNo= new PdfPCell(compGSTNo);
		 cellcompGSTNo.setBorder(0);
		 cellcompGSTNo.setPaddingLeft(15f);
		 cellcompGSTNo.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 PdfPCell cellphstateCode= new PdfPCell(phstateCode);
		 cellphstateCode.setBorder(0);
		 cellphstateCode.setPaddingLeft(15f);
		 cellphstateCode.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 PdfPCell cellcompSACCode= new PdfPCell(compSACCode);
		 cellcompSACCode.setBorder(0);
		 cellcompSACCode.setPaddingLeft(15f);
		 cellcompSACCode.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 PdfPCell cellserviceCat= new PdfPCell(serviceCat);
		 cellserviceCat.setBorder(0);
		 cellserviceCat.setPaddingLeft(15f);
		 cellserviceCat.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		 if(!valPANNO.equals("")){
			 companyInfoTable.addCell(cellPanNo);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellPanNoval);
		 }
		 if(!valSerTaxNo.equals("")){
			 companyInfoTable.addCell(cellSerTaxNo);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellserTaxNoVal);
		 }
		 if(!valpfNo.equals("")){
			 companyInfoTable.addCell(cellesiNo);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellESINo);
		 }
		 if(!valEsicNo.equals("")){
			 companyInfoTable.addCell(cellpfNo);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellPFno);
		 }
		 if(!valComGSTNo.equals("")){
			 companyInfoTable.addCell(cellgstn);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellcompGSTNo);
		 }
		 if(!valStateCode.equals("")){
			 companyInfoTable.addCell(cellstatecode);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellphstateCode);
		 }
		 if(!valSACCode.equals("")){
			 companyInfoTable.addCell(cellsacCode);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellcompSACCode);
		 }
		 if(!valserviceCategory.equals("")){
			 companyInfoTable.addCell(cellserviceCate);
			 companyInfoTable.addCell(colonCell);
			 companyInfoTable.addCell(cellserviceCat);
		 }
		
		 
		PdfPTable parent = new PdfPTable(2);
        parent.setWidthPercentage(100);
        
        parent.addCell(table);
        parent.addCell(companyInfoTable);

		try {
			document.add(parent);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}

	private String getValueFromCompanyArticalType(String documentType, String articalType) {
		
		if(comp!=null){
			for(ArticleType artical : comp.getArticleTypeDetails()){
				if(artical.getDocumentName().trim().equals(documentType)&& artical.getArticleTypeName().equals(articalType)){
					return artical.getArticleTypeValue();
				}
			}
		}
		return "";
	}

	private void createInvoiceNoAndInvoiceDateDetails() {
		
		
		Phrase titlephrase = new Phrase("Tax Invoice", font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font12);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(10f);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		
		/**
		 * @author Anil @since 31-03-2021
		 * changing the invoice no. to invoice ref. no
		 * raised by Rahul Tiwari for Sunrise
		 */
//		String invoiceNo = "No : "+invoice.getCount();
		String invoiceNo = "No : ";
		if(invoice.getInvRefNumber()!=null&&!invoice.getInvRefNumber().equals("")){
			invoiceNo=invoiceNo+invoice.getInvRefNumber();
		}
		String invoiceDate = "Dated : "+fmt.format(invoice.getInvoiceDate());
		
		Phrase No = new Phrase(invoiceNo,font10bold);
        Phrase InvoiceDated = new Phrase(invoiceDate,font10bold);

        PdfPCell cellNoValue = new PdfPCell(No);
        cellNoValue.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell cellinvoiceDateValue = new PdfPCell(InvoiceDated);
        cellinvoiceDateValue.setHorizontalAlignment(Element.ALIGN_RIGHT);
        
        table.addCell(cellNoValue);
        table.addCell(cellinvoiceDateValue);
        
        try {
			document.add(table);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createCompanyNameAsHeader(Document doc, Company comp2) {

		DocumentUpload headerdocument = null;
		if(branchDt.getUploadHeader()!=null&&branchDt.getUploadHeader().getUrl()!=null&&!branchDt.getUploadHeader().getUrl().equals("")){
			headerdocument = branchDt.getUploadHeader();
		}
		else{
			headerdocument = comp.getUploadHeader();
		}
		
		if(headerdocument==null||headerdocument.getUrl()==null||headerdocument.getUrl().equals("")){
			return;
		}

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ headerdocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
			createBlankforUPC();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createBlankforUPC() {
		
		Phrase blankphrase=new Phrase("  ",font10);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
//			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}



	
	}

	private void createLogo() {
		DocumentUpload logodocument = null;
		if(branchDt.getLogo()!=null){
			logodocument =branchDt.getLogo();
		}else{
			logodocument =comp.getLogo();
		}

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;

		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			document2.add(image2);
			
			Image image2 = Image.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		

		String companyName ="";
		if(branchDt.getCorrespondenceName()!=null && !branchDt.getCorrespondenceName().equals("")){
			companyName +=branchDt.getCorrespondenceName();
		}else{
			companyName += comp.getBusinessUnitName();
		}
		Phrase phcompanyName = new Phrase(companyName,font16bold);
		
		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(phcompanyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPTable companyTab = new PdfPTable(1);
		companyTab.setWidthPercentage(100);
		companyTab.addCell(companyNameCell);
		companyTab.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		PdfPTable mainheader =null;
		if(logodocument!=null&&logodocument.getUrl()!=null&&!logodocument.getUrl().equals("")){
			mainheader = new PdfPTable(2);
			mainheader.setWidthPercentage(100);
			try {
				mainheader.setWidths(new float[] { 20, 80 });
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
		}else{
			mainheader = new PdfPTable(1);
			mainheader.setWidthPercentage(100);
		}
		

		
		
		PdfPCell leftCell = new PdfPCell(logoTab);
		leftCell.setBorder(0);
		
		if(logodocument!=null&&logodocument.getUrl()!=null&&!logodocument.getUrl().equals("")){
			mainheader.addCell(leftCell);
		}

		PdfPCell rightCell = new PdfPCell(companyTab);
		rightCell.setBorder(0);
		rightCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainheader.addCell(rightCell);
		
		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getOtTypeList(){
		
		ArrayList<String> otTypeList=new ArrayList<String>();
		if(invoice!=null){
			if(invoice.getSubBillType()!=null&&!invoice.getSubBillType().equals("")){
				if(invoice.getSubBillType().equals("Public Holiday")
						||invoice.getSubBillType().equals("National Holiday")
						||invoice.getSubBillType().equals("Overtime")
						||invoice.getSubBillType().equals("Holiday")
						||invoice.getSubBillType().equals("Staffing")
						||invoice.getSubBillType().equals("Staffing And Rental")
						||invoice.getSubBillType().equals("Labour")){
					
					otTypeList.add(invoice.getSubBillType());
					
					if(invoice.getSubBillType().equals("Staffing")
							||invoice.getSubBillType().equals("Staffing And Rental")
							||invoice.getSubBillType().equals("Labour")){
						staffingFlag=true;
					}
					if(invoice.getSubBillType().equals("Overtime")){
						normalDayOtFlag=true;
					}
					if(invoice.getSubBillType().equals("National Holiday")||invoice.getSubBillType().equals("Holiday")){
						nationalHolidayOtFlag=true;
					}
					if(invoice.getSubBillType().equals("Public Holiday")){
						publicHolidayOtFlag=true;
					}
				}else if(invoice.getSubBillType().equals("Consolidate Bill")){
										
					for(SalesOrderProductLineItem product : invoice.getSalesOrderProducts()){
						logger.log(Level.SEVERE, "product.getBillType() "+product.getBillType());
						if(product.getBillType()!=null && !product.getBillType().equals("")){
							if(product.getBillType().equals("Overtime")
									||product.getBillType().equals("Holiday")
									||product.getBillType().equals("National Holiday")
									||product.getBillType().equals("Public Holiday")
									||product.getBillType().equals("Staffing")
									||product.getBillType().equals("Staffing And Rental")
									||product.getBillType().equals("Labour")){
								
								otTypeList.add(product.getBillType());
								
								if(product.getBillType().equals("Staffing")
										||product.getBillType().equals("Staffing And Rental")
										||product.getBillType().equals("Labour")){
									staffingFlag=true;
								}
								
								if(product.getBillType().equals("Overtime")){
									normalDayOtFlag=true;
								}
								if(product.getBillType().equals("National Holiday")||product.getBillType().equals("Holiday")){
									nationalHolidayOtFlag=true;
								}
								
								if(product.getBillType().equals("Public Holiday")){
									publicHolidayOtFlag=true;
								}
								
							}
						}
					}
				}
			}
		}
		return otTypeList;
	}
	
	private void createAnnextureTable(ArrayList<String> otTypeList) {
		CncBillServiceImpl serviceImpl = new CncBillServiceImpl();
		PdfUtility pdfUtility=new PdfUtility();
		
		int coloums=11;
		
		if(staffingFlag){
			coloums++;
		}
		if(nationalHolidayOtFlag){
			coloums++;
		}
		if(normalDayOtFlag){
			coloums++;
		}
		if(publicHolidayOtFlag){
			coloums++;
		}
		
		PdfPTable tbl = new PdfPTable(coloums);
		tbl.setWidthPercentage(100);
		try {
			if(coloums==12){
				tbl.setWidths(columnWidth12);
			}else if(coloums==13){
				tbl.setWidths(columnWidth13);
			}else if(coloums==14){
				tbl.setWidths(columnWidth14);
			}else if(coloums==15){
				tbl.setWidths(columnWidth15);
			}
			
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		tbl.addCell(pdfUtility.getCell("Sr. No.", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Location Name", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Designation", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("No. Of Staff", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Rate per boy", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		if(staffingFlag){
			tbl.addCell(pdfUtility.getCell("No.of days present", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		
		if(normalDayOtFlag){
			tbl.addCell(pdfUtility.getCell("Normal Day OT", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		if(publicHolidayOtFlag){
			tbl.addCell(pdfUtility.getCell("Public Holiday OT", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		if(nationalHolidayOtFlag){
			tbl.addCell(pdfUtility.getCell("National Holiday OT", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		tbl.addCell(pdfUtility.getCell("Overtime Amount", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Total Amount", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("CGST  (9%)", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("SGST  (9%)", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("IGST  (18%)", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Total Amount", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
			
		int i=0;
		int sumOfNoOfStaff = 0;
		double sumOfRate = 0;
		double sumOfNormalOt = 0;
		double sumOfNormalNHOT = 0;
		double sumOfNormalPHOT = 0;
		double sumOfNormalTotalOtAmt = 0;
		double sumOfNormalRateAndOtAMt = 0;
		
		double sumSgst = 0;
		double sumCgst = 0;
		double sumIgst = 0;
		
		double sumNetTotalAmt = 0;
		double sumOfNumPresent=0;
		
//		HashMap<String,ArrayList<AnnexureType>> annexureMap=new HashMap<String, ArrayList<AnnexureType>>();
		HashMap<String, ArrayList<com.slicktechnologies.shared.common.salesprocess.AnnexureType>> annexureMap=new HashMap<String, ArrayList<com.slicktechnologies.shared.common.salesprocess.AnnexureType>>();
		
		if(invoice.getAnnexureMap()!=null&&invoice.getAnnexureMap().size()!=0){
			annexureMap=invoice.getAnnexureMap();
		}else{
			try{
				annexureMap=serviceImpl.getAnnexureDetailsForCNCBill(invoice.getCompanyId(), invoice);
			}catch(Exception e){
				logger.log(Level.SEVERE,"Inside exception "+e.getMessage());
				e.printStackTrace();
			}
		}
		
		
		ArrayList<com.slicktechnologies.shared.common.salesprocess.AnnexureType> annexureList=new ArrayList<com.slicktechnologies.shared.common.salesprocess.AnnexureType>();
		for (Map.Entry<String, ArrayList<com.slicktechnologies.shared.common.salesprocess.AnnexureType>> entry : annexureMap.entrySet()){
			annexureList.addAll(entry.getValue());
		}
		try{
			Comparator<com.slicktechnologies.shared.common.salesprocess.AnnexureType> annexureComp =new Comparator<com.slicktechnologies.shared.common.salesprocess.AnnexureType>() {
				@Override
				public int compare(com.slicktechnologies.shared.common.salesprocess.AnnexureType o1, com.slicktechnologies.shared.common.salesprocess.AnnexureType o2) {
					return o1.getLocationName().compareTo(o2.getLocationName());
				}
			};
			Collections.sort(annexureList , annexureComp);
		}catch(Exception e){
			
		}
		/**
		 * @author Anil @since 25-03-2021
		 * Commented this
		 */
//		for(SalesOrderProductLineItem product : invoice.getSalesOrderProducts()){
//			if(product.getBillType()!=null && !product.getBillType().equals("")){
//				if(product.getBillType().equals("Overtime")
//						||product.getBillType().equals("Holiday")
//						||product.getBillType().equals("National Holiday")
//						||product.getBillType().equals("Public Holiday")
//						||product.getBillType().equals("Staffing")
//						||product.getBillType().equals("Staffing And Rental")
//						||product.getBillType().equals("Labour")){
//					
////					
//					
//					String siteLoc=" ";
//					double noOfPresentDay=0;
//					ArrayList<Attendance> attendanceList=new ArrayList<Attendance>();
//					if(attendanceMap!=null&&attendanceMap.size()!=0){
//						if(attendanceMap.containsKey(product.getProdName())){
//							attendanceList=attendanceMap.get(product.getProdName());
//						}
//					}
//					
//					logger.log(Level.SEVERE ,"$$$$$$$$$$$$$$$$$$"+product.getBillType()+ " attendanceList " + attendanceList.size()+" DESG -- "+product.getProdName());
//					
//					HashMap<String,ArrayList<Attendance>> siteAndDesgWiseAttMap=new HashMap<String, ArrayList<Attendance>>();
//					
//					for(Attendance attendance:attendanceList){
//						String key=attendance.getEmpDesignation()+attendance.getSiteLocation();
//						if(siteAndDesgWiseAttMap.size()==0){
//							ArrayList<Attendance>list=new ArrayList<Attendance>();
//							list.add(attendance);
//							siteAndDesgWiseAttMap.put(key, list);
//						}else{
//							if(siteAndDesgWiseAttMap.containsKey(key)){
//								siteAndDesgWiseAttMap.get(key).add(attendance);
//							}else{
//								ArrayList<Attendance>list=new ArrayList<Attendance>();
//								list.add(attendance);
//								siteAndDesgWiseAttMap.put(key, list);
//							}
//						}
//					}
//					
//					logger.log(Level.SEVERE ,product.getBillType()+ "################ siteAndDesgWiseAttMap ########### " + siteAndDesgWiseAttMap.size());
//					
//					for (Map.Entry<String,ArrayList<Attendance>> entry : siteAndDesgWiseAttMap.entrySet()){
//						logger.log(Level.SEVERE ,product.getBillType()+ "=============== DESG/LOC ATTEN ============= "+entry.getValue().size());
//						
//						ArrayList<Attendance>attenLIst=entry.getValue();
//						siteLoc=attenLIst.get(0).getSiteLocation();
//						logger.log(Level.SEVERE ,product.getBillType()+ " siteLoc:: " + siteLoc+"  ||  "+" desg:: "+product.getProdName());
//						noOfPresentDay=getNoOfPresentDaysOnSite(attenLIst,fromDate,toDate,product.getBillType(),siteLoc);
//						
//						logger.log(Level.SEVERE ,product.getBillType()+ " noOfPresentDay " + noOfPresentDay);
//						
//						if(siteLoc==null){
//							siteLoc=" ";
//						}
//						
//						
//						String key=siteLoc+product.getProdName()+pdfUtility.getRoundOffValue(product.getCtcAmount());
//						
//						AnnexureType type=new AnnexureType();
//						type.setLocationName(siteLoc);
//						type.setDesignation(product.getProdName());
////						type.setNoOfStaff(product.getManpower());
//						type.setNoOfStaff(noOfStaff+"");
//						type.setRatePerBoy((product.getCtcAmount()+product.getManagementFees())+"");
//						
//						
//						
//						
//						if(staffingFlag){
//							if(product.getBillType().equals("Staffing")
//									||product.getBillType().equals("Staffing And Rental")
//									||product.getBillType().equals("Labour")){
//								try{
//	//								sumOfNumPresent=(int) (sumOfNumPresent+Double.parseDouble(product.getArea()));
//									sumOfNumPresent=sumOfNumPresent+noOfPresentDay;
//									type.setNoOfDaysPresent(noOfPresentDay);
//								}catch(Exception e){
//								}
//							}
//						}
//						
//						
//						if(normalDayOtFlag){
//							if(product.getBillType().equals("Overtime")){
//								try{
////									sumOfNormalOt=sumOfNormalOt+Double.parseDouble(product.getArea());
////									type.setNormalDayOt(Double.parseDouble(product.getArea()));
//									
//									sumOfNormalOt=sumOfNormalOt+noOfPresentDay;
//									type.setNormalDayOt(noOfPresentDay);
//									
//								}catch(Exception e){
//								}
//							}
//						}
//						
//						if(publicHolidayOtFlag){
//							if((product.getBillType().equals("Public Holiday"))){
//								try{
////									sumOfNormalPHOT=sumOfNormalPHOT+Double.parseDouble(product.getArea());
////									type.setPhOt(Double.parseDouble(product.getArea()));
//									
//									sumOfNormalPHOT=sumOfNormalPHOT+noOfPresentDay;
//									type.setPhOt(noOfPresentDay);
//									
//								}catch(Exception e){
//								}
//							}
//						}
//						
//						if(nationalHolidayOtFlag){
//							if(product.getBillType().equals("National Holiday")||product.getBillType().equals("Holiday")){
//								try{
////									sumOfNormalNHOT=sumOfNormalNHOT+Double.parseDouble(product.getArea());
////									type.setNhOt(Double.parseDouble(product.getArea()));
//									
//									sumOfNormalNHOT=sumOfNormalNHOT+noOfPresentDay;
//									type.setNhOt(noOfPresentDay);
//									
//								}catch(Exception e){
//								}
//							}
//						}
//						
//						double totalAmount=product.getPrice()*noOfPresentDay;
//						
//						logger.log(Level.SEVERE ,"Total AMOUNT : "+product.getPrice()+" :: "+totalAmount);
//						
//						
//						if(product.getBillType().equals("Overtime")
//								||product.getBillType().equals("Holiday")
//								||product.getBillType().equals("National Holiday")
//								||product.getBillType().equals("Public Holiday")){
//							type.setOtAmount(totalAmount);
//						}
//						
////						type.setOtAmount(product.getPrice());
////						type.setTotalAmount(product.getTotalAmount());
//						type.setTotalAmount(totalAmount);
//						
//						double sgstAmount=0;
//						double cgstAmount=0;
//						double igstAmount=0;
//						double taxPercent=0;
////						double baseAmount=product.getTotalAmount();
//						double baseAmount=totalAmount;
//						double totalNetAmt=0;
//						try{
//							if(product.getVatTax()!=null){
//								if(product.getVatTax().getTaxConfigName().contains("cgst")||product.getVatTax().getTaxConfigName().contains("CGST")
//										||product.getVatTax().getTaxName().contains("cgst")||product.getVatTax().getTaxName().contains("CGST")
//										||product.getVatTax().getTaxPrintName().contains("cgst")||product.getVatTax().getTaxPrintName().contains("CGST")){
//									taxPercent=product.getVatTax().getPercentage();
//									
//									cgstAmount=(baseAmount*taxPercent)/100;
//								}
//								
//								if(product.getVatTax().getTaxConfigName().contains("sgst")||product.getVatTax().getTaxConfigName().contains("SGST")
//										||product.getVatTax().getTaxName().contains("sgst")||product.getVatTax().getTaxName().contains("SGST")
//										||product.getVatTax().getTaxPrintName().contains("sgst")||product.getVatTax().getTaxPrintName().contains("SGST")){
//									taxPercent=product.getVatTax().getPercentage();
//									
//									sgstAmount=(baseAmount*taxPercent)/100;
//								}
//								
//								if(product.getVatTax().getTaxConfigName().contains("igst")||product.getVatTax().getTaxConfigName().contains("IGST")
//										||product.getVatTax().getTaxName().contains("igst")||product.getVatTax().getTaxName().contains("IGST")
//										||product.getVatTax().getTaxPrintName().contains("igst")||product.getVatTax().getTaxPrintName().contains("IGST")){
//									taxPercent=product.getVatTax().getPercentage();
//									
//									igstAmount=(baseAmount*taxPercent)/100;
//								}
//							}
//							
//							
//							if(product.getServiceTax()!=null){
//		
//								if(product.getServiceTax().getTaxConfigName().contains("cgst")||product.getServiceTax().getTaxConfigName().contains("CGST")
//										||product.getServiceTax().getTaxName().contains("cgst")||product.getServiceTax().getTaxName().contains("CGST")
//										||product.getServiceTax().getTaxPrintName().contains("cgst")||product.getServiceTax().getTaxPrintName().contains("CGST")){
//									taxPercent=product.getServiceTax().getPercentage();
//									
//									cgstAmount=(baseAmount*taxPercent)/100;
//								}
//								
//								if(product.getServiceTax().getTaxConfigName().contains("sgst")||product.getServiceTax().getTaxConfigName().contains("SGST")
//										||product.getServiceTax().getTaxName().contains("sgst")||product.getServiceTax().getTaxName().contains("SGST")
//										||product.getServiceTax().getTaxPrintName().contains("sgst")||product.getServiceTax().getTaxPrintName().contains("SGST")){
//									taxPercent=product.getServiceTax().getPercentage();
//									
//									sgstAmount=(baseAmount*taxPercent)/100;
//								}
//								
//								if(product.getServiceTax().getTaxConfigName().contains("igst")||product.getServiceTax().getTaxConfigName().contains("IGST")
//										||product.getServiceTax().getTaxName().contains("igst")||product.getServiceTax().getTaxName().contains("IGST")
//										||product.getServiceTax().getTaxPrintName().contains("igst")||product.getServiceTax().getTaxPrintName().contains("IGST")){
//									taxPercent=product.getServiceTax().getPercentage();
//									
//									igstAmount=(baseAmount*taxPercent)/100;
//								}
//							
//							}
//						}catch(Exception e){
//							e.printStackTrace();
//						}
//						
//						sumCgst+=cgstAmount;
//						sumSgst+=sgstAmount;
//						sumIgst+=igstAmount;
//						totalNetAmt=baseAmount+cgstAmount+sgstAmount+igstAmount;
//						sumNetTotalAmt+=totalNetAmt;
//						
//						
//						type.setCgstAmt(cgstAmount);
//						type.setSgstAmt(sgstAmount);
//						type.setIgstAmt(igstAmount);
//						type.setNetTotalAmt(totalNetAmt);
//						
//						
//						
////						sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+product.getPrice();
////						sumOfNormalRateAndOtAMt=sumOfNormalRateAndOtAMt+(product.getTotalAmount());
//						
//						if(product.getBillType().equals("Overtime")
//								||product.getBillType().equals("Holiday")
//								||product.getBillType().equals("National Holiday")
//								||product.getBillType().equals("Public Holiday")){
//							sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+totalAmount;
//						}
////						sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+product.getPrice();
//						sumOfNormalRateAndOtAMt=sumOfNormalRateAndOtAMt+totalAmount;
//						
//						
//						
//						
//						if(annexureMap.size()==0){
//							ArrayList<AnnexureType> value=new ArrayList<CustomCNCInvoicePdf.AnnexureType>();
//							value.add(type);
//							annexureMap.put(key, value);
//						}else{
//							if(annexureMap.containsKey(key)){
//								ArrayList<AnnexureType> list=annexureMap.get(key);
//								type.setNoOfDaysPresent(type.getNoOfDaysPresent()+list.get(0).getNoOfDaysPresent());
//								type.setNormalDayOt(type.getNormalDayOt()+list.get(0).getNormalDayOt());
//								type.setPhOt(type.getPhOt()+list.get(0).getPhOt());
//								type.setNhOt(type.getNhOt()+list.get(0).getNhOt());
//								type.setOtAmount(type.getOtAmount()+list.get(0).getOtAmount());
//								type.setTotalAmount(type.getTotalAmount()+list.get(0).getTotalAmount());
//								type.setCgstAmt(type.getCgstAmt()+list.get(0).getCgstAmt());
//								type.setSgstAmt(type.getSgstAmt()+list.get(0).getSgstAmt());
//								type.setIgstAmt(type.getIgstAmt()+list.get(0).getIgstAmt());
//								type.setNetTotalAmt(type.getNetTotalAmt()+list.get(0).getNetTotalAmt());
//								
//								ArrayList<AnnexureType> value=new ArrayList<CustomCNCInvoicePdf.AnnexureType>();
//								value.add(type);
//								annexureMap.put(key, value);
//								
//							}else{
//								ArrayList<AnnexureType> value=new ArrayList<CustomCNCInvoicePdf.AnnexureType>();
//								value.add(type);
//								annexureMap.put(key, value);
//							}
//						}
//					}
//					
//				}
//			}
//		}
		
		if(annexureMap!=null){
//		for (Map.Entry<String,ArrayList<com.slicktechnologies.shared.common.salesprocess.AnnexureType>> entry : annexureMap.entrySet()){ 
//		for (Map.Entry<String,ArrayList<AnnexureType>> entry : annexureMap.entrySet()){ 
			
//            System.out.println("Key = " + entry.getKey() +  ", Value = " + entry.getValue());
//            for(com.slicktechnologies.shared.common.salesprocess.AnnexureType type:entry.getValue()){
			 for(com.slicktechnologies.shared.common.salesprocess.AnnexureType type:annexureList){
//            for(AnnexureType type:entry.getValue()){
				/**
				 * @author Anil @since 31-03-2021
				 * If all annexure parameters are zero then skip that row for printing
				 * raised by Rahul Tiwari for Sunrise
				 */
				if(type.getNoOfDaysPresent()==0&&type.getNormalDayOt()==0&&type.getNhOt()==0&&type.getPhOt()==0){
					continue;
				}
            	i++;
				tbl.addCell(pdfUtility.getCell(i+" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
				
				tbl.addCell(pdfUtility.getCell(type.getLocationName(), font7, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(type.getDesignation(), font7, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(type.getNoOfStaff()+" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(Double.parseDouble(type.getRatePerBoy()))+" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				
				try{
					sumOfNoOfStaff=sumOfNoOfStaff+Integer.parseInt(type.getNoOfStaff());
				}catch(Exception e){
					e.printStackTrace();
				}
				
				try{
					sumOfRate=sumOfRate+Double.parseDouble(type.getRatePerBoy());
	            }catch(Exception e){
					e.printStackTrace();
				}
				
				if(staffingFlag){
					if(type.getNoOfDaysPresent()!=0){
						tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getNoOfDaysPresent())+" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
						sumOfNumPresent=sumOfNumPresent+type.getNoOfDaysPresent();
					}else{
						tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
					}
				}
				            
				if(normalDayOtFlag){
					if(type.getNormalDayOt()!=0){
						try{
							tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getNormalDayOt())+" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
							sumOfNormalOt=sumOfNormalOt+type.getNormalDayOt();
						}catch(Exception e){
							tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));	
						}
					}else{
						tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));	
					}
				}
				
				if(publicHolidayOtFlag){
					if(type.getPhOt()!=0){
						try{
							tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getPhOt())+" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
							sumOfNormalPHOT=sumOfNormalPHOT+type.getPhOt();
						}catch(Exception e){
							tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));	
						}
					}else{
						tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));	
					}
				}
				
				if(nationalHolidayOtFlag){
					if(type.getNhOt()!=0){
						try{
							tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getNhOt())+" ", font7, Element.ALIGN_CENTER, 0, 0, 0));
							sumOfNormalNHOT=sumOfNormalNHOT+type.getNhOt();
						}catch(Exception e){
							tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));	
						}
					}else{
						tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_CENTER, 0, 0, 0));	
					}
				}
				
				tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getOtAmount())+" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getTotalAmount())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				
				sumOfNormalTotalOtAmt=sumOfNormalTotalOtAmt+type.getOtAmount();
				sumOfNormalRateAndOtAMt=sumOfNormalRateAndOtAMt+type.getTotalAmount();
				
				if(type.getCgstAmt()!=0){
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(Math.round(type.getCgstAmt()))+" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					sumCgst+=type.getCgstAmt();
				}else{
					tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				}
				if(type.getSgstAmt()!=0){
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(Math.round(type.getSgstAmt()))+" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					sumSgst+=type.getSgstAmt();
				}else{
					tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				}
				if(type.getIgstAmt()!=0){
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(Math.round(type.getIgstAmt()))+" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					sumIgst+=type.getIgstAmt();
				}else{
					tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				}
				
				if(type.getNetTotalAmt()!=0){
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getNetTotalAmt())+" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					sumNetTotalAmt+=type.getNetTotalAmt();
				}else{
					tbl.addCell(pdfUtility.getCell(" ", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				}
            }
//		}
	}
		
		
		
		
		
		tbl.addCell(pdfUtility.getCell("Total ", font7bold, Element.ALIGN_CENTER, 0, 3, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNoOfStaff)+"", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfRate)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
//		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNumPresent)+"", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		if(staffingFlag){
			tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNumPresent)+"", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		if(normalDayOtFlag){
			tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNormalOt)+"", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		if(publicHolidayOtFlag){
			tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNormalPHOT)+"", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		if(nationalHolidayOtFlag){
			tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNormalNHOT)+"", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNormalTotalOtAmt)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfNormalRateAndOtAMt)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumCgst)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumSgst)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumIgst)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumNetTotalAmt)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		
		
		try {
			document.add(tbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
//	}
	
	private double getNoOfPresentDaysOnSite(ArrayList<Attendance> list,Date fromDate, Date toDate,String parameter, String siteLoc) {
		
		
		
		HashMap<String,ArrayList<Attendance>>empAttenMap=new HashMap<String, ArrayList<Attendance>>();
		
		String key="";
        for(Attendance att : list){
        	
        	if(att.getSiteLocation().equals(siteLoc)) {
	        	key=att.getEmpId()+"/"+att.getSiteLocation()+"/"+att.getEmpDesignation();
	        	if(empAttenMap.size()==0){
	        		ArrayList<Attendance> list1=new ArrayList<Attendance>();
	        		list1.add(att);
	        		empAttenMap.put(key, list1);
	        	}else{
	        		if(empAttenMap.containsKey(key)){
	        			empAttenMap.get(key).add(att);
	        		}else{
	        			ArrayList<Attendance> list1=new ArrayList<Attendance>();
	            		list1.add(att);
	            		empAttenMap.put(key, list1);
	        		}
	        	}
        	}
        }
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.BILLINGDOCUMENT, AppConstants.PC_ENABLESITELOCATION, list.get(0).getCompanyId())){
			HashMap<String,ArrayList<Attendance>>newempAttenMap=new HashMap<String, ArrayList<Attendance>>();
			newempAttenMap.putAll(empAttenMap);
			noOfStaff=getnoOfStaff(empAttenMap,newempAttenMap,list.get(0).getCompanyId());
		}
		else{
	        noOfStaff=empAttenMap.size();
		}
        
//        logger.log(Level.SEVERE , "To date :" + toDate);
		long diff= toDate.getTime() - fromDate.getTime();			
        int noofdays= ((int) (diff / (24 * 60 * 60 * 1000))) + 1;
//        logger.log(Level.SEVERE," :::=No of days  = :::  "+noofdays);
        
        List<String> processNameList=new ArrayList<String>();
		processNameList.add("CNC");
		processNameList.add("PaySlip");
		
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", list.get(0).getCompanyId()).filter("processName IN", processNameList).filter("configStatus", true).list();
		boolean nhOtFlag = false, otRateFlag = false;
		
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(processConf.getProcessName().equalsIgnoreCase("PaySlip")&&processConf.isConfigStatus()==true
							&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("NH_OT_Include_In_Earning")){
						nhOtFlag=true;
					}
					if(processConf.getProcessName().equalsIgnoreCase("CNC")&&processConf.isConfigStatus()==true
							&&ptDetails.isStatus()==true&&ptDetails.getProcessType().trim().equalsIgnoreCase("EnableSiteLocation")){
						otRateFlag=true;
					}
				}
			}
		}
		
//		logger.log(Level.SEVERE ,"otRateFlag "+otRateFlag);
		PaySlipServiceImpl paySlipService=new PaySlipServiceImpl();
        double totalDays=0;
        for (Map.Entry<String,ArrayList<Attendance>> entry : empAttenMap.entrySet()){ 
        	EmployeeInfo info=ofy().load().type(EmployeeInfo.class).filter("companyId", entry.getValue().get(0).getCompanyId()).filter("empCount", entry.getValue().get(0).getEmpId()).first().now();
        	double days =0;
        	if(parameter.equalsIgnoreCase(AppConstants.STAFFING)||parameter.equalsIgnoreCase("Labour")||parameter.equalsIgnoreCase(AppConstants.STAFFINGANDRENTAL)){
	        	double unpaidLeaveHrs=getUnpaidLeaveHrs(fromDate, toDate, entry.getValue().get(0));
	        	logger.log(Level.SEVERE,"Unpaid Leave HRS : "+unpaidLeaveHrs);
		    	days = calculatePaidDays(entry.getValue() ,info , noofdays , nhOtFlag , unpaidLeaveHrs);
		    	totalDays=totalDays+days;
        	}else if(parameter.equalsIgnoreCase(AppConstants.NATIONALHOLIDAY)||parameter.equalsIgnoreCase("Holiday")){
    			days=paySlipService.getNationalHolidayDetails(entry.getValue(),info);
    			if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Direct")){
    				totalDays += (days/2);
    				logger.log(Level.SEVERE,"DIRECT : "+totalDays);
				}else if(info.getNumberRangeAsEmpTyp().trim().equalsIgnoreCase("Indirect")){
					totalDays += days;
					logger.log(Level.SEVERE,"INDIRECT : "+totalDays);
				}
    		}else if(parameter.equalsIgnoreCase(AppConstants.OVERTIME)){
    			days = getOverTimeHrs(info, fromDate, toDate, nhOtFlag,otRateFlag,entry.getValue().get(0)) /info.getLeaveCalendar().getWorkingHours();
    			totalDays += days;
    		}else if(parameter.equalsIgnoreCase(AppConstants.PUBLICHOLIDAY)){
//    			days=paySlipService.getPublicHolidayDetails(entry.getValue(),info);
    			/**
    			 * @author Vijay Date :- 13-02-2021
    			 * Des :- getting issue at sunrise raised by Rahul
    			 * So updated the code with new method 
    			 */
    			days=getPublicHolidayDetails(entry.getValue(),info);
    			logger.log(Level.SEVERE ,parameter+" PH "+days);
				totalDays += days;
    		}
    		logger.log(Level.SEVERE ,parameter+" = " + days+" EMP ID- "+entry.getKey()+" Attendance- "+entry.getValue().size());
        }
		return totalDays;
	}
	
	

	private int getnoOfStaff(HashMap<String, ArrayList<Attendance>> empAttenMap,HashMap<String, ArrayList<Attendance>> newempAttenMap, Long companyId) {
		
		for (Map.Entry<String,ArrayList<Attendance>> entry : empAttenMap.entrySet()){ 
				ArrayList<Attendance> list = entry.getValue();

	        	Employee employee=ofy().load().type(Employee.class).filter("companyId", list.get(0).getCompanyId()).filter("count", list.get(0).getEmpId()).first().now();
	        	if(employee!=null && employee.isReliever()){
	        		newempAttenMap.remove(entry.getKey());
			        logger.log(Level.SEVERE, "isReliever "+newempAttenMap.size());

	        	}
		 } 
		return newempAttenMap.size();
	}

	public double getUnpaidLeaveHrs(Date startDate, Date endDate,Attendance attendance) {
		ArrayList<AvailedLeaves> availedLeaves = new ArrayList<AvailedLeaves>();
		double leaveHrs = 0d;
		
		List<EmployeeLeave> leaves = ofy().load().type(EmployeeLeave.class)
				.filter("companyId", attendance.getCompanyId())
				.filter("emplId", attendance.getEmpId())
				.filter("leaveDate >=", startDate).list();
		
		logger.log(Level.SEVERE ,"EMP ID: "+attendance.getEmpId()+" START DT: "+startDate+" LEAVE LIST: "+leaves.size());
		List<EmployeeLeave> validleaves = new ArrayList<EmployeeLeave>();
		for (EmployeeLeave temp : leaves) {
			if (temp.getLeaveDate().equals(endDate)|| temp.getLeaveDate().before(endDate)) {
				
				if(temp.getSiteLocation()==null){
					validleaves.add(temp);
				}else{
					if(attendance.getSiteLocation()!=null&&temp.getSiteLocation().equals(attendance.getSiteLocation())){
						validleaves.add(temp);
					}
				}
			}
		}
		
		for (EmployeeLeave temp : validleaves) {
			boolean flag = false;
			if (temp.getUnpaidLeave() == false) {
				if (availedLeaves.size() == 0) {
					AvailedLeaves avLeav = new AvailedLeaves();
					avLeav.setLeaveName(temp.getLeaveTypeName());
					avLeav.setLeavehour(temp.getLeaveHrs());
					avLeav.setUnPaid(true);
					availedLeaves.add(avLeav);
				} else {
					for (int i = 0; i < availedLeaves.size(); i++) {
						if (availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())) {
							flag = true;
							double hrs = 0;
							hrs = availedLeaves.get(i).getLeavehour();
							hrs = hrs + temp.getLeaveHrs();
							availedLeaves.get(i).setLeavehour(hrs);
							availedLeaves.get(i).setUnPaid(true);
						}
					}
					if (flag == false) {
						AvailedLeaves avLeav = new AvailedLeaves();
						avLeav.setLeaveName(temp.getLeaveTypeName());
						avLeav.setLeavehour(temp.getLeaveHrs());
						avLeav.setUnPaid(true);
						availedLeaves.add(avLeav);
					}
				}

				leaveHrs = leaveHrs + temp.getLeaveHrs();
			} else {
				if (availedLeaves.size() == 0) {
					AvailedLeaves avLeav = new AvailedLeaves();
					avLeav.setLeaveName(temp.getLeaveTypeName());
					avLeav.setLeavehour(temp.getLeaveHrs());
					avLeav.setUnPaid(false);
					availedLeaves.add(avLeav);
				} else {

					for (int i = 0; i < availedLeaves.size(); i++) {
						if (availedLeaves.get(i).getLeaveName().equals(temp.getLeaveTypeName())) {
							flag = true;
							double hrs = availedLeaves.get(i).getLeavehour();
							hrs = hrs + temp.getLeaveHrs();
							availedLeaves.get(i).setLeavehour(hrs);
							availedLeaves.get(i).setUnPaid(false);
						}
					}
					if (flag == false) {
						AvailedLeaves avLeav = new AvailedLeaves();
						avLeav.setLeaveName(temp.getLeaveTypeName());
						avLeav.setLeavehour(temp.getLeaveHrs());
						avLeav.setUnPaid(false);
						availedLeaves.add(avLeav);
					}
				}
			}
		}
		return leaveHrs;
	}
	
	private double calculatePaidDays(ArrayList<Attendance> attendanceList,EmployeeInfo info,int totalPaidDays,boolean nhOtFlag,double unpaidLeaveHrs){
		PaySlipServiceImpl paySlipService=new PaySlipServiceImpl();
		double paidDays = 0;
		int extraDay = 0;
		int absentDays=0;
		int listSize = attendanceList.size();
		if(listSize == 0){
			listSize = totalPaidDays;
		}
		logger.log(Level.SEVERE,"listSize : "+listSize+" paid days "+totalPaidDays);
		if(nhOtFlag){
			if(info != null && info.getLeaveCalendar()!=null){
				extraDay=paySlipService.getNationalHolidayDetails(attendanceList,info);
			}
		}
		absentDays=paySlipService.getTotalAbsentDays(attendanceList);	
		
		logger.log(Level.SEVERE,"Extra day : "+extraDay+" Absent Day: "+absentDays+" wh "+info.getLeaveCalendar().getWorkingHours());
		if(info != null && info.getLeaveCalendar() != null){
			paidDays=(listSize+extraDay)-(unpaidLeaveHrs/info.getLeaveCalendar().getWorkingHours())-absentDays;	
		}
		return paidDays;
	}
	
	public double getOverTimeHrs(EmployeeInfo info, Date startDate,Date endDate, boolean nhOtFlag, boolean otRateFlag,Attendance attendance) {
		List<EmployeeOvertime> otList = new ArrayList<EmployeeOvertime>();
		double otHrs = 0d;
		try {
			DateUtility.getDateWithTimeZone("IST", startDate);
			DateUtility.getDateWithTimeZone("IST", endDate);

			otList = ofy().load().type(EmployeeOvertime.class)
					.filter("companyId", info.getCompanyId())
					.filter("emplId", info.getEmpCount())
					.filter("otDate >=", startDate).list();

			List<EmployeeOvertime> validotList = new ArrayList<EmployeeOvertime>();

			for (EmployeeOvertime temp : otList) {
				if (temp.getOtDate().equals(endDate)|| temp.getOtDate().before(endDate)) {
					if(temp.getSiteLocation()==null){
						validotList.add(temp);
					}else{
						if(attendance.getSiteLocation()!=null&&temp.getSiteLocation().equals(attendance.getSiteLocation())){
							validotList.add(temp);
						}
					}
				}
			}

			for (EmployeeOvertime temp : validotList) {
				boolean isHoliday = info.getLeaveCalendar().isHolidayDate(temp.getOtDate());
				if (isHoliday) {
					for (Holiday holiday : info.getLeaveCalendar().getHoliday()) {
						if (holiday.getHolidaydate().equals(temp.getOtDate())
								&& holiday.getHolidayType().equalsIgnoreCase("National Holiday")) {
							temp.setOtHrs(0);
						}
						if (otRateFlag&& holiday.getHolidaydate().equals(temp.getOtDate())
								&& holiday.getHolidayType().equalsIgnoreCase("Public Holiday")) {
							temp.setOtHrs(0);
						}
					}
				}
				if (otRateFlag && temp.getHolidayLabel() != null&& temp.getHolidayLabel().equals("PH")) {
					temp.setOtHrs(0);
				}
				otHrs = otHrs + temp.getOtHrs();
			}
		} catch (Exception e) {
		}

		return otHrs;

	}

	class AnnexureType{
		String locationName;
		String designation;
		
		String noOfStaff;
		String ratePerBoy;
		double noOfDaysPresent;
		
		double normalDayOt;
		double phOt;
		double nhOt;
		
		double otAmount;
		double totalAmount;
		
		double sgstAmt;
		double cgstAmt;
		double igstAmt;
		
		double netTotalAmt;

		public String getLocationName() {
			return locationName;
		}

		public void setLocationName(String locationName) {
			this.locationName = locationName;
		}

		public String getDesignation() {
			return designation;
		}

		public void setDesignation(String designation) {
			this.designation = designation;
		}

		public String getNoOfStaff() {
			return noOfStaff;
		}

		public void setNoOfStaff(String noOfStaff) {
			this.noOfStaff = noOfStaff;
		}

		public String getRatePerBoy() {
			return ratePerBoy;
		}

		public void setRatePerBoy(String ratePerBoy) {
			this.ratePerBoy = ratePerBoy;
		}

		public double getNoOfDaysPresent() {
			return noOfDaysPresent;
		}

		public void setNoOfDaysPresent(double noOfDaysPresent) {
			this.noOfDaysPresent = noOfDaysPresent;
		}

		public double getNormalDayOt() {
			return normalDayOt;
		}

		public void setNormalDayOt(double normalDayOt) {
			this.normalDayOt = normalDayOt;
		}

		public double getPhOt() {
			return phOt;
		}

		public void setPhOt(double phOt) {
			this.phOt = phOt;
		}

		public double getNhOt() {
			return nhOt;
		}

		public void setNhOt(double nhOt) {
			this.nhOt = nhOt;
		}

		public double getOtAmount() {
			return otAmount;
		}

		public void setOtAmount(double otAmount) {
			this.otAmount = otAmount;
		}

		public double getTotalAmount() {
			return totalAmount;
		}

		public void setTotalAmount(double totalAmount) {
			this.totalAmount = totalAmount;
		}

		public double getSgstAmt() {
			return sgstAmt;
		}

		public void setSgstAmt(double sgstAmt) {
			this.sgstAmt = sgstAmt;
		}

		public double getCgstAmt() {
			return cgstAmt;
		}

		public void setCgstAmt(double cgstAmt) {
			this.cgstAmt = cgstAmt;
		}

		public double getIgstAmt() {
			return igstAmt;
		}

		public void setIgstAmt(double igstAmt) {
			this.igstAmt = igstAmt;
		}

		public double getNetTotalAmt() {
			return netTotalAmt;
		}

		public void setNetTotalAmt(double netTotalAmt) {
			this.netTotalAmt = netTotalAmt;
		}
		
		
		
	}
	
	/**
	 * @author Vijay Date :- 13-02-2021
	 * Des :- getting issue at sunrise raised by Rahul
	 * So updated the code with new method if holiday label is PH and Totalworked hours is >0 then it will consider of PH OT
	 */
	private double getPublicHolidayDetails(ArrayList<Attendance> attendanceList, EmployeeInfo info) {

		int extraDays=0;
		for(Holiday holiday:info.getLeaveCalendar().getHoliday()){
			if(holiday.getHolidayType().trim().equalsIgnoreCase("Public Holiday")){
				for(Attendance attendance:attendanceList){
					if(attendance.getAttendanceDate().equals(holiday.getHolidaydate())&&attendance.getWorkedHours()!=0){
						logger.log(Level.SEVERE ," getAttendanceDate "+attendance.getAttendanceDate()+" getHolidaydate "+holiday.getHolidaydate());
							extraDays=extraDays+1;
							attendance.setHolidayLabel(null);
					}
				}
			}
		}
		
		logger.log(Level.SEVERE ," PH "+extraDays);
		
		for(Attendance attendance:attendanceList){
			if(attendance.getHolidayLabel()!=null&&attendance.getHolidayLabel().equals("PH") && attendance.getTotalWorkedHours()>0){
				extraDays=extraDays+1;
			}
		}
		logger.log(Level.SEVERE ," PH "+extraDays);
		return extraDays;
	
	}
	
	public void createSalesOrderSummaryTable(){
		
		Font font7bold = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD);
		Font font7 = new Font(Font.FontFamily.HELVETICA, 5);
		
		boolean printFlag=false;
		String companyName = "";
		if(branchDt!=null){
			if(branchDt.getCorrespondenceName()!=null && !branchDt.getCorrespondenceName().equals("")){
				companyName =branchDt.getCorrespondenceName();
			}else{
				companyName += comp.getBusinessUnitName();
			}
		}else{
			companyName += comp.getBusinessUnitName();
		}
		
		double sumOfBudget=0;
		double sumOfSoAmount=0;
		double sumOfPayableAmount=0;
		
		double sumOfIgstAmount=0;
		double sumOfUtgstAmount=0;
		double sumOfCgstAmount=0;
		double sumOfSgstAmount=0;
		double sumOfGrandTotal=0;
		
		PdfPTable tbl = new PdfPTable(18);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(columnWidth18);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		tbl.addCell(pdfUtility.getCell("Format of Soft Copy - House Keeping Agency's Bills", this.font7bold, Element.ALIGN_LEFT, 0, 18, 0));
		tbl.addCell(pdfUtility.getCell("Name of House Keeping Agency :- "+companyName, this.font7bold, Element.ALIGN_LEFT, 0, 18, 0));
//		tbl.addCell(pdfUtility.getCell(" ", font7bold, Element.ALIGN_CENTER, 0, 13, 0));
		
		tbl.addCell(pdfUtility.getCell("Sr. No.", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Bill of the month", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Name of the Agency", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Invoice No", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Invoice Date", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		tbl.addCell(pdfUtility.getCell("Location / Branch Name", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("HSN / SAC CODE", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Cost Code", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Type", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Branch Carpet Area", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Max Material Amount", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		tbl.addCell(pdfUtility.getCell("Material Delivery Amount", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Amount Payable", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("IGST 18%", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("UTGST 18%", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("CGST 9%", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("SGST 9%", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell("Grand Total", font7bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		
		
		
		int counter=0;
		for(int i=0;i<invoice.getSalesOrderProducts().size();i++){
			SalesOrderProductLineItem item=invoice.getSalesOrderProducts().get(i);
			if(item.getSalesAnnexureMap()!=null&&item.getSalesAnnexureMap().size()!=0){
				ArrayList<SalesAnnexureType> typeList=item.getSalesAnnexureMap().get(item.getBillProductName());
				for(SalesAnnexureType type:typeList){
					counter++;
					printFlag=true;
					String invoiceGroup="";
					if(invoice.getInvoiceGroup()!=null){
						invoiceGroup=invoice.getInvoiceGroup();
					}
					tbl.addCell(pdfUtility.getCell(counter+"", font7, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(fmt3.format(invoice.getInvoiceDate()), font7, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(companyName, font7, Element.ALIGN_LEFT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(invoice.getInvRefNumber(), font7, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(fmt2.format(invoice.getInvoiceDate()), font7, Element.ALIGN_CENTER, 0, 0, 0));
					
					tbl.addCell(pdfUtility.getCell(type.getSiteLocation(), font7, Element.ALIGN_LEFT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(item.getHsnCode(), font7, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(type.getCostCode(), font7, Element.ALIGN_CENTER, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(invoiceGroup, font7, Element.ALIGN_LEFT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(type.getCarpetArea(), font7, Element.ALIGN_LEFT, 0, 0, 0));
					
					sumOfBudget+=type.getBudget();
					sumOfSoAmount+=type.getSoAmount();
					sumOfPayableAmount+=type.getPayableAmount();
					
					sumOfIgstAmount+=type.getIgstAmt();
					sumOfUtgstAmount+=type.getUtgstAmt();
					sumOfCgstAmount+=type.getCgstAmt();
					sumOfSgstAmount+=type.getSgstAmt();
					sumOfGrandTotal+=type.getGrandTotal();
					
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getBudget())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getSoAmount())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getPayableAmount())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getIgstAmt())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getUtgstAmt())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getCgstAmt())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getSgstAmt())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(type.getGrandTotal())+"", font7, Element.ALIGN_RIGHT, 0, 0, 0));
				}
			}
		}
		
		tbl.addCell(pdfUtility.getCell("", font7bold, Element.ALIGN_RIGHT, 0, 10, 0));
		
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfBudget)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfSoAmount)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfPayableAmount)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfIgstAmount)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfUtgstAmount)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfCgstAmount)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfSgstAmount)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		tbl.addCell(pdfUtility.getCell(pdfUtility.getRoundOffValue(sumOfGrandTotal)+"", font7bold, Element.ALIGN_RIGHT, 0, 0, 0));
		
		if(printFlag){
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			try {
//				document.setPageSize(PageSize.A4.rotate());
				document.add(tbl);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		
	}
	
}
