package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
public class HygeiaPaymentReceiptPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1551608496691382320L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("Vaishnavi +++++++++++++++" + stringid);

			HygeiaPaymentReceiptPdf payreceiptpdf = new HygeiaPaymentReceiptPdf();
			 String preprintStatus=req.getParameter("preprint");
			payreceiptpdf.document = new Document();
			Document document = payreceiptpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream()); // write

			document.open();

			payreceiptpdf.setpdfreceipt(count);
			payreceiptpdf.createPdf(preprintStatus);

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
