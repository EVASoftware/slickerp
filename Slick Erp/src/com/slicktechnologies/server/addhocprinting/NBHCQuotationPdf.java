package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class NBHCQuotationPdf {

	Quotation quotation;
	Company comp;
	Customer cust;
	public Document document;
	Employee employee;
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	

	float[] tblcol1width = { 1.5f, 1.5f, 7.0f };
	float[] tblcol1width1 = { 2.0f, 8.0f };
	
	public NBHCQuotationPdf(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}
	
	
	public void setNBHCQuotation(Long count) {
		
		quotation=ofy().load().type(Quotation.class).id(count).now();
		
		if (quotation.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		
		if (quotation.getCompanyId() != null)
		{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).first().now();
		}
		
		if(quotation.getCompanyId()!=null)
		{
			employee = ofy().load().type(Employee.class).filter("fullname", quotation.getEmployee()).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else
		{
			employee = ofy().load().type(Employee.class).filter("fullname", quotation.getEmployee()).first().now();
		}
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preprintStatus) {
		
		if(preprintStatus.equals("yes")){
			
			System.out.println("inside prit yes");
			createBlankforUPC();
		}
		
		if(preprintStatus.equals("no")){
		System.out.println("inside prit no");
		
		if(comp.getUploadHeader()!=null){
		createCompanyNameAsHeader(document,comp);
		}
		
		if(comp.getUploadFooter()!=null){
		createCompanyNameAsFooter(document,comp);
		}
		createBlankforUPC();
		}
		
		createCustomerDetails();
		premisesdetailsTable();
		productDetails();
		paymentTermsDetails();
		bottomDetails();
	}
	


	private void bottomDetails() {
		
		Branch branch =null;
		if(employee!= null)
		{
			branch = ofy().load().type(Branch.class).filter("buisnessUnitName", employee.getBranchName()).filter("companyId", quotation.getCompanyId()).first().now();
		}
		
		Phrase para = new Phrase(

		"In acceptance of above, please issue us a signed order for initiating our services."+"\n"+

		"We thank you for opportunity given to serve you and look forward to providing you our pest management services.",font8);		
	
		
		PdfPTable table =new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		Phrase yours = new Phrase("Sincerely  Yours,",font8bold);
		PdfPCell yoursCell = new PdfPCell(yours);
		yoursCell.setBorder(0);
		table.addCell(yoursCell);
		
		Phrase blank = new Phrase(" ",font8bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase salesPerson = new Phrase(quotation.getEmployee(),font8);
		PdfPCell salesPersonCell = new PdfPCell(salesPerson);
		salesPersonCell.setBorder(0);
		table.addCell(salesPersonCell);
		
		if(employee.getDesignation()!=null && !employee.getDesignation().equals(""))
		{
		Phrase designation = new Phrase(employee.getDesignation(),font8);
		PdfPCell designationCell = new PdfPCell(designation);
		designationCell.setBorder(0);
		table.addCell(designationCell);
		}

		Phrase compName = new Phrase(comp.getBusinessUnitName(),font8);
		PdfPCell compNameCell = new PdfPCell(compName);
		compNameCell.setBorder(0);
		table.addCell(compNameCell);
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(branch.getAddress()!=null){
			
			if(branch.getAddress().getAddrLine2()!= null && !branch.getAddress().getAddrLine2().equals("")){
				if(branch.getAddress().getLandmark()!= null && !branch.getAddress().getLandmark().equals("")){
					custAdd1=branch.getAddress().getAddrLine1()+","+branch.getAddress().getAddrLine2()+"\n"+branch.getAddress().getLandmark();
				}else{
					custAdd1=branch.getAddress().getAddrLine1()+"\n"+branch.getAddress().getAddrLine2();
				}
			}else{
				if(branch.getAddress().getLandmark()!= null && !branch.getAddress().getLandmark().equals("")){
					custAdd1=branch.getAddress().getAddrLine1()+"\n"+branch.getAddress().getLandmark();
				}else{
					custAdd1=branch.getAddress().getAddrLine1();
				}
			}
			
			if(branch.getAddress().getLocality()!=null && !branch.getAddress().getLocality().equals("")){
				custFullAdd1=custAdd1+"\n"+branch.getAddress().getCity()+" - "+branch.getAddress().getPin()+","+branch.getAddress().getLocality()+","+branch.getAddress().getState()+","+branch.getAddress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+branch.getAddress().getCity()+" - "+branch.getAddress().getPin()+","+branch.getAddress().getState()+","+branch.getAddress().getCountry();
			}
		}
		
		Phrase employeeAddress = new Phrase(custFullAdd1,font8);
		PdfPCell employeeAddressCell = new PdfPCell(employeeAddress);
		employeeAddressCell.setBorder(0);
		table.addCell(employeeAddressCell);
		
		
		
		
		Phrase employeeBranch = new Phrase("Branch :"+employee.getBranchName(),font8);
		PdfPCell employeeBranchCell = new PdfPCell(employeeBranch);
		employeeBranchCell.setBorder(0);
		table.addCell(employeeBranchCell);
		
		  Phrase cellNo = null;
		  if(employee.getLandline()!= null)
		  {
			   cellNo= new Phrase("Cell : "+employee.getCellNumber1()+"   Phone: "+employee.getLandline(),font9); 
		  }
		  else
		  {
			   cellNo= new Phrase("Cell : "+employee.getCellNumber1(),font9); 
		  }
		  
		  PdfPCell cellNoCell = new PdfPCell(cellNo);
		  cellNoCell.setBorder(0);
		  cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  table.addCell(cellNoCell);
		  
		Phrase employeeEmail = new Phrase("Email :"+employee.getEmail(),font8);
		PdfPCell employeeEmailCell = new PdfPCell(employeeEmail);
		employeeEmailCell.setBorder(0);
		table.addCell(employeeEmailCell);
		
		try {
			document.add(para);
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


private void createCompanyNameAsHeader(Document doc, Company comp) {
	
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

	private void createCustomerDetails() {
		/**
		 * Date 14/12/2017
		 * By Jayshree
		 * To Add the quotation id number of colums are increses
		 */
		float[] relativeWidths2Column = { 0.7f, 1.5f,5.0f,1.7f,0.9f };
		
		//    date heading 
		PdfPTable table = new PdfPTable(5);
		//End
		table.setWidthPercentage(100f);
		table.setSpacingBefore(15f);
		table.setSpacingAfter(15f);
		
		try {
			table.setWidths(relativeWidths2Column);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase date= new Phrase("Date :", font8bold);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setBorder(0);
		dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(dateCell);
		
		Phrase dateValue = new Phrase(fmt.format(quotation.getCreationDate()),font8);
		PdfPCell dateValueCell = new PdfPCell(dateValue);
		dateValueCell.setBorder(0);
		dateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(dateValueCell);
		
		
		/**
		 * Date 14/12/2017
		 * By Jayshree
		 * To Add the quotation id number of colums are increses
		 */
		Phrase blank= new Phrase(" ", font8bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(blankCell);
		
		
		Phrase quotationid= new Phrase("Quotation Id :", font8bold);
		PdfPCell quotationidCell = new PdfPCell(quotationid);
		quotationidCell.setBorder(0);
		quotationidCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(quotationidCell);
		
		
		Phrase quotationidval= new Phrase(quotation.getCount()+"", font8);
		PdfPCell quotationidvalCell = new PdfPCell(quotationidval);
		quotationidvalCell.setBorder(0);
		quotationidvalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(quotationidvalCell);
		
		//End By Jayshree
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		String salutation="";
		if(cust.getSalutation()!= null && ! cust.getSalutation().equals(""))
		{
			salutation =cust.getSalutation();
		}
			
		
		//    customer details 

		PdfPTable customerDetailsTable = new PdfPTable(1);
		customerDetailsTable.setWidthPercentage(100f);
		
//		customerDetailsTable.addCell(dearCell);
		String custName="";
		
		//   rohan modified this code for printing printable name 
		
		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
			
			
			custName=cust.getFullname().trim();
			Phrase customerFullName= new Phrase(salutation+" "+custName,font8);
			PdfPCell customerFullNameCell = new PdfPCell(customerFullName);
			customerFullNameCell.setBorder(0);
			customerFullNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			customerDetailsTable.addCell(customerFullNameCell);
			
			
			String companyName="";
			if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
				companyName=cust.getCustPrintableName().trim();
			}
			else
			{
				companyName=cust.getCompanyName().trim();
			}
			
			
			Phrase customercomp= new Phrase(companyName,font8);
			PdfPCell customercompCell = new PdfPCell(customercomp);
			customercompCell.setBorder(0);
			customercompCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			customerDetailsTable.addCell(customercompCell);
				
			
		}
		else
		{
			if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
				custName=cust.getCustPrintableName().trim();
			}
			else
			{
				custName=cust.getFullname().trim();
			}
			
			Phrase customerFullName= new Phrase(salutation+" "+custName,font8);
			PdfPCell customerFullNameCell = new PdfPCell(customerFullName);
			customerFullNameCell.setBorder(0);
			customerFullNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			customerDetailsTable.addCell(customerFullNameCell);
		}
		
		
		
		
		
			if(cust.getRefrNumber1()==null)
			{
				Phrase customerId = new Phrase("Customer Id :" +cust.getRefrNumber1(),font8);
				PdfPCell customerIdCell = new PdfPCell(customerId);
				customerIdCell.setBorder(0);
				customerIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				customerDetailsTable.addCell(customerIdCell);
			}
			
			
			// customer Address details 
			
			String custAdd1="";
			String custFullAdd1="";
			
			if(cust.getAdress()!=null){
				
				if(cust.getAdress().getAddrLine2()!= null && !cust.getAdress().getAddrLine2().equals("")){
					if(cust.getAdress().getLandmark()!= null && !cust.getAdress().getLandmark().equals("")){
						custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
					}else{
						custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
					}
				}else{
					if(cust.getAdress().getLandmark()!= null && !cust.getAdress().getLandmark().equals("")){
						custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
					}else{
						custAdd1=cust.getAdress().getAddrLine1();
					}
				}
				
				if(cust.getAdress().getLocality()!=null && !cust.getAdress().getLocality().equals("")){
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+" - "+cust.getAdress().getPin()+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getState()+"\n"+cust.getAdress().getCountry();
							
				}else{
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+" - "+cust.getAdress().getPin()+"\n"+cust.getAdress().getState()+"\n"+cust.getAdress().getCountry();
				}
			}
			
			
			Phrase custAddInfo = new Phrase(custFullAdd1, font8);
			PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
			custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custAddInfoCell.setBorder(0);
			customerDetailsTable.addCell(custAddInfoCell);
			
			try {
				document.add(customerDetailsTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}
	
	
	private void productDetails() {
		
		Paragraph para = new Paragraph("Services Proposed:",font10bold);
		para.setAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable productTable = new PdfPTable(4);
		productTable.setWidthPercentage(100f);
		productTable.setSpacingBefore(15f);
		productTable.setSpacingAfter(15f);
		
		Phrase productName = new Phrase("Services Proposed", font8bold);
		PdfPCell productNameCell = new PdfPCell(productName);
		productNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productTable.addCell(productNameCell);
		
		
		Phrase serviceCharges = new Phrase("Service Charges(INR)",font8bold);
		PdfPCell serviceChargesCell = new PdfPCell(serviceCharges);
		serviceChargesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productTable.addCell(serviceChargesCell);
	
		
		Phrase noOfService = new Phrase("Number of Services",font8bold);
		PdfPCell noOfServiceCell = new PdfPCell(noOfService);
		noOfServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productTable.addCell(noOfServiceCell);
		
		Phrase unitOfMeasurement = new Phrase("Unit of Measurement",font8bold);
		PdfPCell unitOfMeasurementCell = new PdfPCell(unitOfMeasurement);
		unitOfMeasurementCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productTable.addCell(unitOfMeasurementCell);
		
		
		for (int i = 0; i < quotation.getItems().size(); i++) {
			
			Phrase prodNAme = new Phrase(quotation.getItems().get(i).getProductName(),font8);
			PdfPCell prodNameCell = new PdfPCell(prodNAme);
			prodNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(prodNameCell);
			
			Phrase price = new Phrase(quotation.getItems().get(i).getPrice()+"",font8);
			PdfPCell priceCell = new PdfPCell(price);
			priceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(priceCell);
			
			Phrase noOfServices = new Phrase(quotation.getItems().get(i).getNumberOfServices()+"",font8);
			PdfPCell noOfServicesCell = new PdfPCell(noOfServices);
			noOfServicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(noOfServicesCell);
			
			Phrase uom = new Phrase(quotation.getItems().get(i).getUnitOfMeasurement(),font8);
			PdfPCell uomCell = new PdfPCell(uom);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(uomCell);
		}
		
		
		
		Phrase parahgrap = new Phrase("Service tax shall be charged as applicable in addition to our service charges."+"\n" ,font8);
		
		try {
			document.add(para);
			document.add(productTable);
			document.add(parahgrap);
			document.add(Chunk.NEWLINE);
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void paymentTermsDetails() {
		
		
		Paragraph termsPara = new Paragraph("Terms of Payment  : " ,font10bold);
		termsPara.setAlignment(Element.ALIGN_LEFT);
		Paragraph para = null;
		if(quotation.getPayTerms()!=null && ! quotation.getPayTerms().equals(""))
		{
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			try {
				table.setWidths(tblcol1width1);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			para= new Paragraph("Invoices shall be raised as per frequency decided and paid within 7 days of receipt of Invoices.",font8);
			para.setAlignment(Element.ALIGN_LEFT);
			
			
			PdfPCell termsParaCell = new PdfPCell(termsPara);
			termsParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			termsParaCell.setBorder(0);
			
			PdfPCell paraCell = new PdfPCell(para);
			paraCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paraCell.setBorder(0);
			
			table.addCell(termsParaCell);
			table.addCell(paraCell);
			
			try {
				document.add(table);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		table1.setSpacingBefore(15f);
		
		table1.setSpacingAfter(15f);
		
		try {
			table1.setWidths(tblcol1width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		Phrase days = new Phrase("DAYS",font1);
		Phrase percent = new Phrase("PERCENT",font1);
		Phrase comment = new Phrase("COMMENTS",font1);
		
		
		PdfPCell dayscell = new PdfPCell(days);
		dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell percentcell = new PdfPCell(percent);
		percentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell commentcell = new PdfPCell(comment);
		commentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table1.addCell(dayscell);
		table1.addCell(percentcell);
		table1.addCell(commentcell);
		
		for(int i=0; i<this.quotation.getPaymentTermsList().size(); i++){
			
			Phrase daysValue = new Phrase(quotation.getPaymentTermsList().get(i).getPayTermDays() + "",font8);
			PdfPCell daysValueCell = new PdfPCell(daysValue);
			daysValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase percentValue = new Phrase(quotation.getPaymentTermsList().get(i).getPayTermPercent() + "",font8);
			PdfPCell percentValueCell = new PdfPCell(percentValue);
			percentValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase commentValue = new Phrase(quotation.getPaymentTermsList().get(i).getPayTermComment() + "",font8);
			PdfPCell commentValueCell = new PdfPCell(commentValue);
			commentValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			
			table1.addCell(daysValueCell);
			table1.addCell(percentValueCell);
			table1.addCell(commentValueCell);
			
		}
		
		try {
			document.add(termsPara);
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
	}
	
	private void premisesdetailsTable() {

		
		String dear = "Dear Sir / Madam,";
		Paragraph dearPh = new Paragraph(dear,font8);
		dearPh.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph subjet = new Paragraph("Sub: Quotation for NBHC�s Pest Management Services",font8);
		subjet.setAlignment(Element.ALIGN_CENTER);
		
		String paragraph ="";
		if(quotation.getInspectedBy()!= null && !quotation.getInspectedBy().equals("") && quotation.getInspectionDate()!=null )
		{
			paragraph =  
					"Based on our assessment of pests by "+quotation.getInspectedBy()+" at your premises on "+fmt.format(quotation.getInspectionDate())+" we submit following proposal:";
		}
		else if(quotation.getInspectedBy()!=null && !quotation.getInspectedBy().equals("")){
		paragraph = 
				"Based on our assessment of pests by "+quotation.getInspectedBy()+" at your premises on we submit following proposal:";
		}
		else if(quotation.getInspectionDate()!= null){
		paragraph = 
				"Based on our assessment of pests by at your premises on "+fmt.format(quotation.getInspectionDate())+" we submit following proposal:";
		}
		else
		{
			paragraph = 
					"Based on our assessment of pests at your premises we submit following proposal:";
		}
		
		
		
		
		Paragraph para = new Paragraph(paragraph,font8);
		para.setAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable premisesDetaislTable = new PdfPTable(4);
		premisesDetaislTable.setWidthPercentage(100);
		premisesDetaislTable.setSpacingBefore(15f);
		premisesDetaislTable.setSpacingAfter(15f);
		
		Phrase premisesUnderContract =new Phrase("Premises Under Contract", font8bold);
		PdfPCell premisesUnderContractCell = new PdfPCell(premisesUnderContract);
		premisesUnderContractCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase category =new Phrase("Category", font8bold);
		PdfPCell categoryCell = new PdfPCell(category);
		categoryCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase type =new Phrase("Type", font8bold);
		PdfPCell typeCell = new PdfPCell(type);
		typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase inspectionDt =new Phrase("Date of Inspection", font8bold);
		PdfPCell inspectionDtCell = new PdfPCell(inspectionDt);
		inspectionDtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		premisesDetaislTable.addCell(premisesUnderContractCell);
		premisesDetaislTable.addCell(categoryCell);
		premisesDetaislTable.addCell(typeCell);
		premisesDetaislTable.addCell(inspectionDtCell);
		
		Phrase premisesValue = new Phrase(quotation.getPremisesDesc(),font8);
		PdfPCell premisesValueCell = new PdfPCell(premisesValue);
		premisesValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase categoryValue = new Phrase(quotation.getCategory(),font8);
		PdfPCell categoryValueCell = new PdfPCell(categoryValue);
		categoryValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase typeValue = new Phrase(quotation.getType(),font8);
		PdfPCell typeValueCell = new PdfPCell(typeValue);
		typeValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase inspectiondatePh = null;
		if(quotation.getInspectionDate()!=null)
		{
			inspectiondatePh = new Phrase(fmt.format(quotation.getInspectionDate()),font8);
		}
		else
		{
			inspectiondatePh = new Phrase(" ",font8);
		}
		
		PdfPCell inspectiondatePhCell = new PdfPCell(inspectiondatePh);
		inspectiondatePhCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		premisesDetaislTable.addCell(premisesValueCell);
		premisesDetaislTable.addCell(categoryValueCell);
		premisesDetaislTable.addCell(typeValueCell);
		premisesDetaislTable.addCell(inspectiondatePhCell);
		
		try {
			document.add(subjet);
			document.add(Chunk.NEWLINE);
			document.add(dearPh);
			document.add(para);
			document.add(premisesDetaislTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
