package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateMaterialConsumptionPdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4974802840086733530L;
	
	
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
		  String stringid=request.getParameter("Id");
		   stringid=stringid.trim();
		   int count =Integer.parseInt(stringid);
		   
		   MaterialConsumptionReportPdf reportpdf=new MaterialConsumptionReportPdf();
		   reportpdf.document=new Document();
	   Document document = reportpdf.document;
	   PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	   
	   document.open();
	   
	   reportpdf.setMaterialReproNote(count);
	   reportpdf.createPdf();
	   document.close();
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }
}
