package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.BahtText;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.Declaration;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class ServiceGSTInvoiceForAyendev {
	Logger logger = Logger.getLogger("ServiceGSTInvoice.class");
	List<SalesLineItem> products;
	ArrayList<String> dataStringList;
	ArrayList<Integer> alignmentList;
	ArrayList<Chunk> chunkList;
	ArrayList<Font> fontList;
	int firstBreakPoint = 17;
	float blankLines;

	float[] columnMoreLeftWidths = { 2f, 1f };
	float[] columnMoreLeftHeaderWidths = { 1.7f, 1.3f };

	float[] columnMoreRightWidths = { 0.8f, 2.2f };
	float[] columnMoreRightCheckBoxWidths = { 0.3f, 2.7f };
	float[] columnHalfWidth = { 1f, 1f };
	float[] columnHalfInnerWidth = { 0.65f, 1f };

	float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
	float[] columnCollon6Width = { 1.8f, 0.2f, 7.5f, 1.8f, 0.2f, 7.5f };

	float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.5f };
	float[] columnStateCodeCollonWidth = { 3.5f, 2f, 0.2f, 1f };
//	float[] columnContractPeriodDateCodeCollonWidth = { 3f, 1.5f, 0.2f, 2f };//old
	float[] columnContractPeriodDateCodeCollonWidth = { 2.7f, 2f, 0.2f, 2f };//Date:21-12-2022
	float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };

	float[] column6RowCollonWidth = { 0.5f, 0.2f, 1.5f, 0.8f, 0.2f, 0.5f };
	float[] column16CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.3f };
	float[] columnrohanrrCollonWidth = { 3.0f, 0.2f, 7.5f };

	// float[] column10ProdCollonWidth = { 0.1f, 0.52f, 0.2f, 0.38f, 0.15f,
	// 0.15f,
	// 0.2f, 0.25f, 0.15f, 0.25f };
	// float[] column8ProdCollonWidth = { 0.1f, 0.92f, 0.2f, 0.38f, 0.15f, 0.2f,
	// 0.15f, 0.25f };
	float[] column8ProdCollonWidth = { 0.1f, 1.08f, 0.2f, 0.2f, 0.38f, 0.2f,
			0.15f, 0.25f };
	float[] column9ProdCollonWidth = { 0.1f, 0.92f, 0.16f, 0.2f, 0.19f, 0.19f,
			0.2f, 0.15f, 0.25f };
	float[] column10ProdCollonWidth = { 0.1f, 0.92f, 0.2f, 0.19f, 0.2f, 0.19f,
			0.16f, 0.2f, 0.15f ,0.25f }; //Ashwini Patil
	float[] column4ProdCollonWidth = { 1.75f, 0.2f, 0.15f, 0.25f };

	float[] column2ProdCollonWidth = { 1.9f, 0.25f };
	float[] column3ProdCollonWidth = { 1f, 0.9f, 0.4f };

	float[] column13CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.4f, 0.4f, 0.4f, 0.3f };
	// {Sr No,Services,HSN ACS,UOM,Qty,Rate,Amount,Disc,Taxable
	// amt,CGST,SGST,IGST,Total}

	/**
	 * rohan added this flag for universal pest control This is used to print
	 * vat no and other article information
	 */

	Boolean UniversalFlag = false;
	/**
	 * ends here
	 */

	public Document document;
    boolean branchAsCompany=false;
	Invoice invoiceentity;
	List<BillingDocumentDetails> billingDoc;
	ProcessConfiguration processConfig;
	Customer cust;
	Company comp;
	Contract con;
    // Date : 13-05-2021 Added By Priyanka
	boolean invoicePrefix=false;
	
	List<ContractCharges> billingTaxesLis; // ajinkya added this 03/07/2017
	List<CustomerBranchDetails> custbranchlist;
	List<CustomerBranchDetails> customerbranchlist;
	SimpleDateFormat sdf;
	
	/**
	 * @author Anil
	 * @since 19-01-2022
	 * Amount should be print with comma after each three digit. raised by Nithila and Nitin Sir
	 */
	DecimalFormat df = new DecimalFormat("#,###.00");
//	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat decimalformat = new DecimalFormat("0.00");

	boolean upcflag = false;
	boolean onlyForFriendsPestControl = false;
	boolean authOnLeft = false;
	boolean isPlaneFormat = false;
	/**
	 * Rohan added this for Universal pest for printing
	 */
	Boolean multipleCompanyName = false;
	Boolean printPremiseDetails = false;

	/**
	 * ends here
	 */

	List<PaymentTerms> payTermsLis;

	List<State> stateList;

	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	Font font13 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	
	Font font11 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	
	Phrase blankCell = new Phrase(" ", font10);
	
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);

	Font nameAddressBoldFont=new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);//Ashwini Patil
	Font nameAddressFont=new Font(Font.FontFamily.HELVETICA, 7);//Ashwini Patil
	Font nameAddressFont6 = new Font(Font.FontFamily.HELVETICA, 7);//Ashwini Patil
	/* Total Amount */
	double totalAmount;
	/**
	 * Added By Rahul Verma Max Lines which can be used between products and 50
	 * characters of service Name in product Table represents on Line
	 * */

	int noOfLines = 16;
	/**
	 * This is where lines breaks
	 */
	int prouductCount = 0;
	/**
	 * Date 27-07-2017 By ANIL This flag is used to check whether to print
	 * product description on pdf or not.
	 */
	boolean productDescFlag = false;
	/**
	 * Date 28 Sept 2017 By Rahul This flag is used to check whether to print
	 * product description on pdf or not.
	 */
	boolean printAttnInPdf = false;
	boolean consolidatePrice = false;
	boolean consolidatePriceSet = false; //ashwini patil
	boolean consolidateRateSet = false; //ashwini patil
	boolean consolidateDiscSet = false; //ashwini patil
	/* Added By Rahul Verma on Date 28 Aug 2017 */
	CompanyPayment comppayment;
	Branch branchDt = null;
	private PdfPCell imageSignCell;
	
	
	boolean changeTitle=false;

	// PdfPCell premiseTblCell = new PdfPCell();

	/**
	 * @author Vijay Date 20-11-2020 
	 * Des :- As per Rahul and Nitin sir instruction if Quatity exist in invoice product table then
	 * quontity column will display in product table
	 */
	 boolean qtycolumnFlag =false;
	 
	 float[] column9SerProdCollonWidth = { 0.1f, 1.08f, 0.2f, 0.2f, 0.38f, 0.2f,
				0.15f,0.15f, 0.25f };
	 
	 boolean hideRateAndDiscount=false;
	 
	 Boolean nonbillingInvoice = false;
	 boolean hideGSTINNo=false;
	 
 	/**
	 * Date 22-11-2018 By Vijay 
	 * Des :- if process configuration is active then GST Number will not display
	 * and GST Number will display if GST applicable or not applicable as per nitin sir
	 */
	 boolean gstNumberPrintFlag = true;
	 
   /**
	 * @author Abhinav Bihade
	 * @since 08/02/2020
	 * As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
	 * in case if somebody manages 2 companies under same link of ERP s/w
	 */
	 ArrayList <ArticleType> branchWiseFilteredArticleList;
		
	 boolean invoiceGroupAsSignatory=false;
	 
    /**
	 * @author Anil @since 22-07-2021
	 * Thai font flag
	 */
	 boolean thaiFontFlag=false; 
		
	 boolean AmountInWordsHundreadFormatFlag=false;
	 
	 /**
	 * @author Anil @since 01-10-2021
	 * added print bank details flag and default value for this flag will be true
	 * requirement raised by Rahul Tiwar and Nitin Sir
	 */
//	boolean printBankDetailsFlag=true;
	PdfUtility pdfUtility=new PdfUtility();

	boolean PC_RemoveSalutationFromCustomerOnPdfFlag = false;
	CustomerBranchDetails customerBranch=null;
	
	/**
	 * @author Anil
	 * @since 19-01-2022
	 * If company country is selected as Thailand then thai pdf will print
	 */
	BahtText bahtText=new BahtText();
	boolean thaiPdfFlag=false;
	boolean pageBreakFlag=false;
	List<CompanyPayment> compPayList=null;
	
	String invoiceTitle="";
	String copyTitle="";
	String companyCountry="";
	
	/**
	 * @author Anil
	 * @since 21-01-2022
	 * Need to add service annexure and some modification for service wise bill invoice 
	 * raised by Nithila and Nitin sir for Hygienic Pest and will be generic for all
	 */
	List<BillingDocument> billingList;
	List<Service> serviceList;
	boolean serviceWiseBillInvoice=false;
	String billingPeriod="";
	SimpleDateFormat sdf1;
	SimpleDateFormat sdf2;

	Config numberRangeConfig;
	/**
	 * @author Vijay Date :- 04-01-2023
	 * Des :- added column hiding with below process configuration
	 */
	boolean PC_DoNotPrintContractSericesFlag = false;
	boolean PC_DoNotPrintContractDurationFlag = false;
	boolean PC_DoNotPrintRateFlag = false;
	boolean PC_DoNotPrintDiscFlag = false;
	/**
	 * ends here
	 */
	List<Contract> contractList=null;
	
	public void setInvoice(Long count) {
		// Load Invoice
		invoiceentity = ofy().load().type(Invoice.class).id(count).now();
		//
		// billingDoc=invoiceentity.getArrayBillingDocument();
		// invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
		// arrPayTerms=invoiceentity.getArrPayTerms();
		// Load Customer
		
		if (invoiceentity.getCompanyId() != null) {
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
						thaiFontFlag=true;
//						break;
					}
					if(obj.getProcessType().equalsIgnoreCase(AppConstants.PC_AMOUNTINWORDSHUNDREADSTRUCTURE)&&obj.isStatus()==true){
						AmountInWordsHundreadFormatFlag = true;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font16boldul = new Font(boldFont, 16,Font.UNDERLINE);
				font16bold = new Font(boldFont, 16);
				font14bold = new Font(boldFont, 14);
				font14 = new Font(regularFont, 14);
				font10 = new Font(regularFont, 7);
				font10bold = new Font(boldFont, 7);
				
				font13 = new Font(regularFont, 9);
				font13bold = new Font(boldFont, 9);
				font12bold = new Font(boldFont, 12);
				font8bold = new Font(boldFont, 8);
				font8 = new Font(regularFont, 8);
				font12boldul = new Font(boldFont, 12,Font.UNDERLINE);
				font12 = new Font(regularFont, 12);
				font11 = new Font(regularFont, 10);
				font11bold = new Font(boldFont, 10);
				font6 = new Font(regularFont, 7);
				font6bold = new Font(boldFont, 6);
				font9bold = new Font(boldFont, 9);
				
				BaseFont tahomaFont=BaseFont.createFont("Tahoma Regular font.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont tahomaBoldFont=BaseFont.createFont("TAHOMAB0.TTF",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				nameAddressFont=new Font(tahomaFont, 9);
				nameAddressBoldFont=new Font(tahomaFont, 9);
				nameAddressFont6=new Font(tahomaFont, 7);
				
				BaseFont T_regularFont=BaseFont.createFont("angsa.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont T_boldFont=BaseFont.createFont("angsab.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
//				titlefont=new Font(T_boldFont,14);
				
				titlefont=new Font(tahomaBoldFont,12);
				
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
	

		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		// Load Company
		if (invoiceentity.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		
		if(comp!=null){
			companyCountry=comp.getAddress().getCountry().trim();
		}

		if (invoiceentity.getCompanyId() != null)
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		else
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount()).first()
					.now();

		payTermsLis = con.getPaymentTermsList();

		if (invoiceentity.getCompanyId() == null)
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount()).list();
		else
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		/****************************** vijay ************************/

		System.out.println("Branch name======"
				+ invoiceentity.getCustomerBranch());
		if (invoiceentity.getCompanyId() == null)
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch()).list();
		else
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		System.out.println("Banch updated====="
				+ invoiceentity.getCustomerBranch());

		if (invoiceentity.getCompanyId() != null) {
			comppayment = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		}
		/****************************** vijay ************************/

		stateList = ofy().load().type(State.class)
				.filter("companyId", invoiceentity.getCompanyId()).list();

		/************************************ Letter Head Flag *******************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("OnlyForFriendsPestControl")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						onlyForFriendsPestControl = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("AuthorityOnLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						authOnLeft = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAttnInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printAttnInPdf = true;
					}
					// Added By Priyanka - Des -Add process configuration to print Invoice Prefix 
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_PRINTINVOICENUMBERPREFIX")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						invoicePrefix = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ConsolidatePrice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						consolidatePrice = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PC_RenameProformaInvoiceWithInvoice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						changeTitle = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("NonbillingInvoice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						nonbillingInvoice = true;
					}

					if(invoiceentity.getNumberRange()!=null && !invoiceentity.getNumberRange().equals("")){
						numberRangeConfig = ofy().load().type(Config.class).filter("companyId", invoiceentity.getCompanyId())
											.filter("name", invoiceentity.getNumberRange()).filter("type", 91).first().now();
					}
					if(numberRangeConfig!=null && !numberRangeConfig.isGstApplicable()){ //Ashwini Patil Date:25-01-2024 Pest o shield reported and issue than for non billing invoices Gst number is getting printed
						gstNumberPrintFlag = false;
					}
					logger.log(Level.SEVERE,"gstNumberPrintFlag="+gstNumberPrintFlag);
					
					
					/*** Date 23-11-2018 By Vijay For GST Number Print or not ****/ 
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("EnableDoNotPrintGSTNumber")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						gstNumberPrintFlag = false;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("InvoiceGroupAsSignatory")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						invoiceGroupAsSignatory = true;
					}
					
				}
			}
          /***@author Priyanka 
		 * Date - 02-09-2021
		 * added a process configuration to hide rate and Discount
		 * ***/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "HideRateAndDiscountColumn", comp.getCompanyId())){
			hideRateAndDiscount=true;
			logger.log(Level.SEVERE,"Inside ProcessConfig:" +hideRateAndDiscount);
			}
		}
		
		/** @author Abhinav Bihade
		 * @since 08/02/2020
		 * As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
		 * in case if somebody manages 2 companies under same link of ERP s/w
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			branchWiseFilteredArticleList =getArticleBranchList(comp.getArticleTypeDetails(),invoiceentity.getBranch());
			logger.log(Level.SEVERE,"Inside Branch As Company: " +branchWiseFilteredArticleList.size());
		}
		 
		/***@author Amol
		 * Date - 7-4-202
		 * added a process configuration BranchAsCompany
		 * ***/
		
if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
	        branchAsCompany=true;
			logger.log(Level.SEVERE,"Process active --");
			if(invoiceentity !=null && invoiceentity.getBranch() != null && invoiceentity.getBranch().trim().length()>0){
				
				 branchDt = ofy().load().type(Branch.class).filter("companyId",invoiceentity.getCompanyId()).filter("buisnessUnitName", invoiceentity.getBranch()).first().now();

				/**
					  * @author Ashwini Patil
					  * @since 4-04-2022
					  * Invoice header was not getting updated as per brnach
					  * Raised By Atharva
				  */
				 if(branchDt != null){
						comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				 }
				if(branchDt !=null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){

					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					

					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", invoiceentity.getCompanyId()).first()
								.now();
						
						
//						if(comppayment != null){
//							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
//						}
						
					}
					
					
				}
			}
		}
		
		
if(invoiceentity.getPaymentMode()!=null && invoiceentity.getPaymentMode().trim().length()>0){
	List<String> paymentDt = Arrays.asList(invoiceentity.getPaymentMode().trim().split("/"));
	
	if(paymentDt.get(0).trim().matches("[0-9]+")){
		
		int payId = Integer.parseInt(paymentDt.get(0).trim());
		
		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("count", payId)
				.filter("companyId", invoiceentity.getCompanyId()).first()
				.now();
		
	}
}
		
		
		
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, comp.getCompanyId())){
		PC_RemoveSalutationFromCustomerOnPdfFlag = true;
	}	
		
		
		if(invoiceentity.getCustomerBranch()!=null&&!invoiceentity.getCustomerBranch().equals("")){
		logger.log(Level.SEVERE,"In Side AList1:");
		customerBranch= ofy().load().type(CustomerBranchDetails.class)
					.filter("cinfo.count",con.getCinfo().getCount())
					.filter("companyId", con.getCompanyId())
					.filter("buisnessUnitName", invoiceentity.getCustomerBranch()).first().now();
		
		logger.log(Level.SEVERE,"AList1:" +customerBranch);
	}		
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * If country is selected as thai land then we will print thai specific format designed for Innovative
		 * requirement taken by Nitin Sir and Nithila
		 */
		if(companyCountry!=null&&!companyCountry.equals("")){
			if(companyCountry.equalsIgnoreCase("Thailand")||companyCountry.trim().equalsIgnoreCase("ประเทศไทย")){
				thaiPdfFlag=true;
				thaiFontFlag=true;
				pageBreakFlag=true;
//				recieptPdf=false;
			}
			
			if(thaiPdfFlag){
				compPayList=ofy().load().type(CompanyPayment.class).filter("companyId", invoiceentity.getCompanyId()).filter("paymentStatus", true).list();
			}
			
			/**
			 * @author Anil
			 * @since 19-01-2022
			 * Need to print four copy of invoices for thailand client 
			 * two copy for invoice/proforma invoice or reciept each one is termed as original and copy
			 * raised by Nitin Sir and Nithila for Innovative
			 */
			if(thaiPdfFlag){
				if(pageBreakFlag){
					if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
						invoiceTitle="Invoice / ใบแจ้งหนี้"; 
					}else if(AppConstants.CREATETAXINVOICE.equals(invoiceentity.getInvoiceType().trim())){
						invoiceTitle="Tax Invoice / ใบกำกับภาษี";
					}
					
				}else{
					invoiceTitle="Receipt / ใบเสร็จรับเงิน";
				}
				copyTitle="ต้นฉบับ / ORIGINAL";
			}
			
			/**
			 * @author Anil
			 * @since 21-01-2022
			 * Here we are checking whether it is a service wise bill and loadind data for that
			 * raised by Nithila and Nitin sir for Hygienic Pest
			 */
			sdf1 = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
			
			sdf2 = new SimpleDateFormat("h:mm a");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf2.setTimeZone(TimeZone.getTimeZone("IST"));
			
			if(invoiceentity!=null&&invoiceentity.getRateContractServiceId()!=0){
				serviceWiseBillInvoice=true;
				/**
				 * @author Anil
				 * @since 02-02-2022
				 * Earlier we are calculating billing period on invoice for display purpose only but as 
				 * per updated requirement it should be stored in invoice and will be controlled from there only
				 * raised by Nitin and Jayesh
				 */
//				List<Integer> billingId=new ArrayList<Integer>();
//				for(BillingDocumentDetails obj:invoiceentity.getArrayBillingDocument()){
//					billingId.add(obj.getBillId());
//				}
//				
//				if(billingId.size()!=0){
//					billingList=ofy().load().type(BillingDocument.class).filter("companyId", invoiceentity.getCompanyId())
//							.filter("count IN",billingId).list();
//					if(billingList!=null&&billingList.size()!=0){
//						List<Integer> serviceId=new ArrayList<Integer>();
//						for(BillingDocument obj:billingList){
//							serviceId.add(obj.getRateContractServiceId());
//						}
//						if(serviceId.size()!=0){
//							serviceList=ofy().load().type(Service.class).filter("companyId", invoiceentity.getCompanyId())
//									.filter("count IN",serviceId).list();
//					
//							if(serviceList!=null&&serviceList.size()!=0){
//								Comparator<Service> comp=new Comparator<Service>() {
//									@Override
//									public int compare(Service o1, Service o2) {
//										return o1.getServiceDate().compareTo(o2.getServiceDate());
//									}
//								};
//								Collections.sort(serviceList, comp);
//								
//								Date fromDt=serviceList.get(0).getServiceDate();
//								Date toDt=serviceList.get(serviceList.size()-1).getServiceDate();
//								
//								fromDt=DateUtility.getStartDateofMonth(fromDt);
//								toDt=DateUtility.getEndDateofMonth(toDt);
//								billingPeriod=sdf1.format(fromDt)+" - "+sdf1.format(toDt);
//							}
//						}
//					}
//				}
				if(invoiceentity.getBillingPeroidFromDate()!=null && invoiceentity.getBillingPeroidToDate()!=null) //Ashwini Patil Date:30-03-2022 Description: To avoid exception if billing period is not mentioned in invoice
					billingPeriod=sdf1.format(invoiceentity.getBillingPeroidFromDate())+" - "+sdf1.format(invoiceentity.getBillingPeroidToDate());
			}
		}	
		
		
		
		PC_DoNotPrintContractSericesFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.Invoice, AppConstants.PC_DONOTPRINTCONTRACTSERVICES, invoiceentity.getCompanyId());
		PC_DoNotPrintContractDurationFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.Invoice, AppConstants.PC_DONOTPRINTCONTRACTDURATION, invoiceentity.getCompanyId());
		PC_DoNotPrintRateFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.Invoice, AppConstants.PC_DONOTPRINTRATE, invoiceentity.getCompanyId());
		PC_DoNotPrintDiscFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.Invoice, AppConstants.PC_DONOTPRINTDISCOUNT, invoiceentity.getCompanyId());
	
		List<Integer> orderId=new ArrayList<Integer>();
		for(BillingDocumentDetails obj:invoiceentity.getArrayBillingDocument()){
			orderId.add(obj.getOrderId());
		}
		if(orderId.size()>0) {
			contractList=ofy().load().type(Contract.class).filter("companyId", invoiceentity.getCompanyId())
					.filter("count IN",orderId).list();
			if(contractList!=null)
				logger.log(Level.SEVERE,"contractList size="+contractList.size());
		}
		
}

	public void createPdf(String preprintStatus) {
		
		/**
		 * @author Anil
		 * @since 01-02-2022
		 * initializing no of lines 
		 */
		noOfLines = 16;

		sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		// /////////////////
		if (upcflag == false && preprintStatus.equals("plane")) {
			createLogo(document, comp);
			createHeader();
			isPlaneFormat = true;
			// createCompanyAddress();
		} else {
			isPlaneFormat = false;
			if (preprintStatus.equals("yes")) {
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			if (preprintStatus.equals("no")) {
				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}

				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				createBlankforUPC();
			}
		}

		// /////////////////
		// createHeader();
		// createStaticHeader(preprintStatus);
		double discount = 0, roundOff = 0;
		discount = invoiceentity.getDiscountAmt();
		roundOff = invoiceentity.getDiscount();
		if (discount != 0) {
			noOfLines = noOfLines - 1;
		}
		if (roundOff != 0) {
			noOfLines = noOfLines - 1;
		}
		if (invoiceentity.getOtherCharges().size() > 0) {
			noOfLines = noOfLines - 1;
		}
		createInvoiceDetails();
		createCustomerDetails();
		if (con.isContractRate()) {
			createProductDetailsForRate();
		} else {
			createProductDetails();
		}
		System.out.println("invoiceentity.getSalesOrderProducts().size()"
				+ invoiceentity.getSalesOrderProducts().size());
		// HashSet<Integer> contractCount=new HashSet<Integer>();
		HashSet<String> productCode = new HashSet<String>();

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			// contractCount.add(invoiceentity.getSalesOrderProducts().get(i).getOrderId());
			productCode.add(invoiceentity.getSalesOrderProducts().get(i)
					.getProdCode());
		}
		logger.log(Level.SEVERE,"productCode"+productCode.size());
		
		if (con.isContractRate()) {
			createProductDetailsValForRate(productCode);
		} else {
			createProductDetailsValCopy(productCode);
//			createProductDetailsVal(productCode);
		}
		createFooterAmountPart();
		if (discount != 0) {
			createFooterDisCountAfterPart(discount);
		}
		if (invoiceentity.getOtherCharges().size() > 0) {
			createFooterOtherChargesPart2();
		}
		createFooterTaxPart();
		if (roundOff != 0) {
			createFooterDisCountBeforeNetPayPart(roundOff);
		}
		createFooterAmountInWords_NetPayPart();
		
//		createTermsAndCondition();
//		createFooterLastPart(preprintStatus);
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * For Innovative if it is not proforma invoice then terms and condition to be printed from invoice declaration
		 * raised by Nithila and Nitin sir
		 */
		if(thaiPdfFlag){
			if(invoiceentity.getInvoiceType().equals("Proforma Invoice")){
				createTermsAndCondition();
				createBankDetailsAndSignatoryTable();
			}else{
				createDeclarationTable();
				createBankDetailsAndSignatoryTable();
			}
		}else{
			createTermsAndCondition();
			createFooterLastPart(preprintStatus);
		}
		
		createProductDescription();
		
		/**
		 * @author Anil
		 * @since 21-01-2022
		 * Annexure for service wise bill invoices
		 */
		if(serviceWiseBillInvoice){
			createAnnexureDetailsTblForServiceWiseBill();
		}
		
		/**
		 * @author Anil
		 * @since 21-01-2022
		 * Below part is used to print mutiple copies of invoice
		 */
		if(thaiPdfFlag){
			if(pageBreakFlag){
				pageBreakFlag=false;
				
				//Invoice Copy
				logger.log(Level.SEVERE,"Inside printing another pdf.."+pageBreakFlag);
				try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
					invoiceTitle="Invoice / ใบแจ้งหนี้"; 
				}else if(AppConstants.CREATETAXINVOICE.equals(invoiceentity.getInvoiceType().trim())){
					invoiceTitle="Tax Invoice / ใบกำกับภาษี";
				}
				copyTitle="สำเนา / Copy";
				createPdf(preprintStatus);
					
				//If added by Ashwini Patil  Desciption: Innovative client want receipts only for TaxInvoice
				if(AppConstants.CREATETAXINVOICE.equals(invoiceentity.getInvoiceType().trim())){
				
				//Reciept Original
				logger.log(Level.SEVERE,"Inside printing another Reciept og pdf.."+pageBreakFlag);
				try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				invoiceTitle="Receipt / ใบเสร็จรับเงิน";
				copyTitle="ต้นฉบับ / ORIGINAL";
				createPdf(preprintStatus);
				
				//Reciept Copy
				logger.log(Level.SEVERE,"Inside printing another Reciept copy pdf.."+pageBreakFlag);
				try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				invoiceTitle="Receipt / ใบเสร็จรับเงิน";
				copyTitle="สำเนา / Copy";
				createPdf(preprintStatus);
				}
			}
			
		}

	}
	
	/**
	 * @author Anil
	 * @since 21-01-2022
	 */
	private void createAnnexureDetailsTblForServiceWiseBill() {
		
		if(serviceList!=null&&serviceList.size()!=0){
			
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			float[] columns = { 5f,10f,9f,9f,10f,15f,9f,9f,10f,15f };
			PdfPTable tbl =new PdfPTable(10);
			tbl.setWidthPercentage(100f);
			
			try {
				tbl.setWidths(columns);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			tbl.addCell(pdfUtility.getCell("Annexure - Details of the services which have been completed", font9bold, Element.ALIGN_CENTER, 0, 10, 0));
			
			tbl.addCell(pdfUtility.getCell("Sr Nr", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Service Date", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Completion", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
			tbl.addCell(pdfUtility.getCell("Service Id", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Product Name", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("Signed by", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
			tbl.addCell(pdfUtility.getCell("Status", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			tbl.addCell(pdfUtility.getCell("SR Copy Hyperlink", font8bold, Element.ALIGN_CENTER, 2, 0, 0));
			
			tbl.addCell(pdfUtility.getCell("Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdfUtility.getCell("Time", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdfUtility.getCell("Name", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdfUtility.getCell("Mobile Number", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			int counter=0;
			for(Service service:serviceList){
				counter++;
				String completionDate="";
				String completionTime="";
				String signedByName="";
				String signedByNum="";
				String srCopyLink="";
				
				if(service.getServiceCompletionDate()!=null){
					completionDate=sdf1.format(service.getServiceCompletionDate());
					completionTime=sdf2.format(service.getServiceCompletionDate());
				}
				
				if(service.getCustomerSignName()!=null){
					signedByName=service.getCustomerSignName();
				}
				if(service.getCustomerSignNumber()!=null){
					signedByNum=service.getCustomerSignNumber();
				}
				
				font8boldul.setColor(BaseColor.BLUE);
				srCopyLink= "pdfCustserjob"+"?Id="+service.getId()+"&"+"companyId="+service.getCompanyId();
				Phrase phrase = new Phrase();
				Chunk chunk = new Chunk("SR Copy-"+service.getCount(),font8boldul);
				chunk.setAnchor(srCopyLink);
				phrase.add(chunk);
				PdfPCell cell=new PdfPCell(phrase);
				cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				tbl.addCell(pdfUtility.getCell(counter+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(sdf1.format(service.getServiceDate()), font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(completionDate, font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(completionTime, font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(service.getCount()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(service.getProductName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(signedByName, font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(signedByNum, font8, Element.ALIGN_CENTER, 0, 0, 0));
				tbl.addCell(pdfUtility.getCell(service.getStatus(), font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(cell);
				
				
			}
			
			try {
				document.add(tbl);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
	}
	
private void createBankDetailsAndSignatoryTable() {
		
		PdfPTable tbl = new PdfPTable(1);
		tbl.setWidthPercentage(100);
		
		CompanyPayment bankdetail1=null;
		CompanyPayment bankdetail2=null;
		if(compPayList!=null&&compPayList.size()!=0){
			int tblCol=0;
			if(comppayment!=null){
				bankdetail1=comppayment;
			}
			
			if(compPayList.size()==1){
				tblCol=1;
				if(bankdetail1==null){
					bankdetail1=compPayList.get(0);
				}
			}else if(compPayList.size()>1){
				tblCol=2;
				for(CompanyPayment obj:compPayList){
					if(bankdetail1!=null){
						if(obj.getCount()!=bankdetail1.getCount()){
							bankdetail2=obj;
							break;
						}
					}else{
						bankdetail1=obj;
					}
				}
			}
			
			PdfPTable bankTbl = new PdfPTable(tblCol);
			bankTbl.setWidthPercentage(100);
//			bankTbl.addCell(pdfUtility.getCell("รายละเอียดธนาคาร / Bank Details", font10bold, Element.ALIGN_CENTER, 0, tblCol, 0));
			
			tbl.addCell(pdfUtility.getCell("รายละเอียดธนาคาร / Bank Details", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			float[] columnWidths3 = { 1.7f, 0.35f, 4.5f };
			
			PdfPTable leftBankTbl = new PdfPTable(3);
			leftBankTbl.setWidthPercentage(100f);
			try {
				leftBankTbl.setWidths(columnWidths3);
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
			
			leftBankTbl.addCell(pdfUtility.getCell("Name", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(bankdetail1.getPaymentBankName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			leftBankTbl.addCell(pdfUtility.getCell("Branch", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(bankdetail1.getPaymentBranch(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			leftBankTbl.addCell(pdfUtility.getCell("A/c No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftBankTbl.addCell(pdfUtility.getCell(bankdetail1.getPaymentAccountNo(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			bankTbl.addCell(leftBankTbl);
//			tbl.addCell(leftBankTbl);
			
			if(tblCol==2){
				PdfPTable rightBankTbl = new PdfPTable(3);
				rightBankTbl.setWidthPercentage(100f);
				try {
					rightBankTbl.setWidths(columnWidths3);
				} catch (DocumentException e2) {
					e2.printStackTrace();
				}
				
				rightBankTbl.addCell(pdfUtility.getCell("Name", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(bankdetail2.getPaymentBankName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				rightBankTbl.addCell(pdfUtility.getCell("Branch", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(bankdetail2.getPaymentBranch(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				rightBankTbl.addCell(pdfUtility.getCell("A/c No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightBankTbl.addCell(pdfUtility.getCell(bankdetail2.getPaymentAccountNo(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				
				bankTbl.addCell(rightBankTbl);
//				tbl.addCell(rightBankTbl);
			}
			
			PdfPCell bankCell=new PdfPCell(bankTbl);
			bankCell.setBorder(0);
			tbl.addCell(bankCell);
		}
		
		
		PdfPTable signatoryTbl = new PdfPTable(3);
		signatoryTbl.setWidthPercentage(100);
		
//		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
//		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 3, 0)).setBorder(0);
		
		signatoryTbl.addCell(pdfUtility.getCell("ลูกค้า ผู้รับของ/Receiver", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell("ผู้รับเงิน ผู้ส่งของ/Delivered and Received By", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		signatoryTbl.addCell(pdfUtility.getCell("ลายเซ็นผู้มีอำนาจ /Authorized Signature", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(0);
		
		tbl.addCell(signatoryTbl);
		
		try {
			document.add(tbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createDeclarationTable() {
		PdfPTable tbl = new PdfPTable(1);
		tbl.setWidthPercentage(100);
		
		/**
		 * @author Ashwini Patil
		 * @since 2-02-2022
		 * Innovative client want hardcoded declaration for Tax Invoice and Receipt	
		 */
		if(thaiPdfFlag){
			Declaration declaration=ofy().load().type(Declaration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("status", true).first().now();

			String msg=declaration.getDeclaratiomMsg();
			Phrase pdeclaration=new Phrase(msg,font13bold);
			PdfPCell declarationCell=new PdfPCell(pdeclaration);
			declarationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			tbl.addCell(declarationCell);			
		}else{
			if(invoiceentity.getDeclaration()!=null&&!invoiceentity.getDeclaration().equals("")){
				tbl.addCell(pdfUtility.getCell(invoiceentity.getDeclaration(), font10bold, Element.ALIGN_LEFT, 0, 0, 0));
			}else{
				tbl.addCell(pdfUtility.getCell(" ", font10bold, Element.ALIGN_LEFT, 0, 0, 50));
			}
		}
		
		
		

		
		try {
			document.add(tbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createProductDetailsValCopy(HashSet<String> productCode) {
	
		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			/**
			 * @author Vijay Date :- 24-03-2022
			 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
			 */
			if(!invoiceentity.isConsolidatePrice()){
				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
						invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
					continue;
				}
			}

			
			
			rateAmountProd = rateAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();

			amountAmountProd = amountAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmountProd = discAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();
			
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmountProd = totalAssAmountProd + taxValue;
		}

		logger.log(Level.SEVERE,"rateAmountProd"+rateAmountProd);
		logger.log(Level.SEVERE,"amountAmountProd"+amountAmountProd);
		logger.log(Level.SEVERE,"discAmountProd"+discAmountProd);
		logger.log(Level.SEVERE,"totalAssAmountProd"+totalAssAmountProd);
		
		float blankLines = 0;

		/**
		 * @author Vijay Date 20-11-2020 
		 * Des :- As per Rahul and Nitin sir instruction if Quatity exist in invoice product table then
		 * quantity column will display in product table
		 */
		PdfPTable productTable;
		if(qtycolumnFlag) {
			 productTable = new PdfPTable(9);
			 productTable.setWidthPercentage(100);
				try {
					productTable.setWidths(column9SerProdCollonWidth);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
		}
		else {
			 productTable = new PdfPTable(8);
			 productTable.setWidthPercentage(100);
				try {
					productTable.setWidths(column8ProdCollonWidth);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
		}
		
//		PdfPTable productTable = new PdfPTable(8);
//		productTable.setWidthPercentage(100);
//		try {
//			productTable.setWidths(column8ProdCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		int countToBeDeducted = 0;
		ArrayList<String> productCodeList = new ArrayList<String>(productCode);
		for (int i = 0; i < productCodeList.size(); i++) {
			logger.log(Level.SEVERE,"productCodeList" + productCodeList.get(i));
		}
		
		int colsspan =1;
		if(PC_DoNotPrintContractSericesFlag){
			++colsspan;
		}
		
		if(PC_DoNotPrintContractDurationFlag){
			++colsspan;
		}
		
		for (int k = 0; k < productCodeList.size(); k++) {
			double totalAmountOfProductCode = 0;
			int noOfservices = 0;
			PdfPCell srNoCell = null;
			PdfPCell serviceNameCell = null;
			PdfPCell servicesCell = null;
			PdfPCell hsnCodeCell = null;
			PdfPCell qtyCell = null;
			PdfPCell rateCell = null;
			PdfPCell startDate_endDateCell = null;
			PdfPCell discCell = null;
			PdfPCell taxableValueCell = null;
			PdfPCell premiseblankCell = null;
			PdfPCell premiseCell = null;
			noOfLines = noOfLines - 1;
			int srNoVal = k + 1;
			Phrase srNo = new Phrase(srNoVal + "", font6);
			srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			/*Since we need only once*/
			boolean foundOnce=false;
			double  discValueAtProductLevel = 0;
			double amountValueAtProductLevel=0;
			
			PdfPCell quantitycell= null;
			
			for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
				if (productCodeList
						.get(k)
						.trim()
						.equalsIgnoreCase(
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdCode())) {
					if(!foundOnce){
						
						/**
						 * @author Vijay Date :- 24-03-2022
						 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
						 */
						if(!invoiceentity.isConsolidatePrice()){
							if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
									invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
								continue;
							}
						}

						
						
						
						if (noOfLines == 0) {
							prouductCount = i;
							break;
						}

						countToBeDeducted = countToBeDeducted + 1;

						/**
						 * Date 06/06/2018 By vijay
						 * Des :- Service warranty if exist then it will show in pdf
						 */
						String productName = "";
						if(invoiceentity.getSalesOrderProducts().get(i).getWarrantyPeriod()!=0){
							productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim()
									+"\n"+" Warranty Period - "+invoiceentity.getSalesOrderProducts().get(i).getWarrantyPeriod();
						}else{
							productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim();
						}
						/**
						 * ends here
						 */
						
						// productTable.addCell(srNoCell);
						Phrase serviceName = new Phrase(productName, font6);
						serviceNameCell = new PdfPCell(serviceName);

						Phrase hsnCode = null;
						if (invoiceentity.getSalesOrderProducts().get(i)
								.getHsnCode() != null
								&& !invoiceentity.getSalesOrderProducts().get(i)
										.getHsnCode().equals("")) {
							hsnCode = new Phrase(invoiceentity
									.getSalesOrderProducts().get(i).getHsnCode()
									.trim(), font6);
						} else {
							ServiceProduct serviceProduct = ofy()
									.load()
									.type(ServiceProduct.class)
									.filter("companyId", comp.getCompanyId())
									.filter("count",
											invoiceentity.getSalesOrderProducts()
													.get(i).getProdId()).first()
									.now();
							if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
								hsnCode = new Phrase(serviceProduct.getHsnNumber(),
										font6);
							} else {
								hsnCode = new Phrase("", font6);
							}
						}

						hsnCodeCell = new PdfPCell(hsnCode);
						hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);


						String startDateStr = "", endDateStr = "";
						Date startDate=null;
						Date endDate=null;
						for (int j = 0; j < con.getItems().size(); j++) {
							System.out.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()"
											+ invoiceentity.getSalesOrderProducts()
													.get(i).getPrduct().getCount());
							System.out.println("con.getItems().get(j).getPrduct().getCount()"
											+ con.getItems().get(j).getPrduct()
													.getCount());
							System.out.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
											+ invoiceentity.getSalesOrderProducts()
													.get(i).getOrderDuration());
							System.out.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
											+ invoiceentity.getSalesOrderProducts()
													.get(i).getOrderDuration());
							System.out.println("con.getItems().get(j).getDuration()"
											+ con.getItems().get(j).getDuration());

							SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
									"dd/MM/yyyy");
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
							simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
							if ((invoiceentity.getSalesOrderProducts().get(i)
									.getPrduct().getCount() == con.getItems()
									.get(j).getPrduct().getCount())
									&& (invoiceentity.getSalesOrderProducts()
											.get(i).getOrderDuration() == con
											.getItems().get(j).getDuration())) {
								if (con.getItems().get(j).getEndDate() != null) {
									startDateStr = simpleDateFmt.format(con
											.getItems().get(j).getStartDate());
									endDateStr = simpleDateFmt.format(con
											.getItems().get(j).getEndDate());
								} else {
									Calendar c = Calendar.getInstance();
									c.setTime(con.getItems().get(j).getStartDate());
									c.add(Calendar.DATE, con.getItems().get(j)
											.getDuration());
									Date endDt = c.getTime();
									startDateStr = simpleDateFmt.format(con
											.getItems().get(j).getStartDate());
									endDateStr = simpleDateFmt.format(endDt);
								}
								//Ashwini Patil Date:20-01-2023
								if(thaiPdfFlag) {
									logger.log(Level.SEVERE, "thaiPdfFlag Contract duration condition1");
									Calendar ct = Calendar.getInstance();
									ct.setTime(con.getItems().get(j).getStartDate());
									ct.add(Calendar.MINUTE, 90);							
									startDate=ct.getTime();
									
									ct.add(Calendar.DATE, con.getItems().get(j).getDuration());
									endDate = ct.getTime();
								}
							}
							if(thaiPdfFlag) {
								SimpleDateFormat thai = new SimpleDateFormat("dd/MM/yyyy");
								if(startDate!=null)
								startDateStr = thai.format(startDate);
								if(endDate!=null)
								endDateStr = thai.format(endDate);
								logger.log(Level.SEVERE, "thaiPdfFlag startdate="+startDateStr+"enddate="+endDateStr);					
							}
						}

						
						Phrase startDate_endDate = new Phrase(startDateStr + " - "
								+ endDateStr, font6);

						startDate_endDateCell = new PdfPCell(startDate_endDate);
						startDate_endDateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						
						/** @Sheetal : 10-02-2022
						   * Des : Switching Billing period and Contract duration with below process configuration,
						          requirment by Pest-O-Shield **/

						if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PC_INV_Quot_Con_SWAP_BILLING_AND_CONTRACT_PERIOD", invoiceentity.getCompanyId())){
							SimpleDateFormat simpleDateFmt = new SimpleDateFormat("dd/MM/yyyy");
							startDateStr=simpleDateFmt.format(invoiceentity.getBillingPeroidFromDate());
							endDateStr=simpleDateFmt.format(invoiceentity.getBillingPeroidToDate());
							Phrase startDate_endDate1 = new Phrase(startDateStr + " To "+ endDateStr, font6);
							startDate_endDateCell = new PdfPCell(startDate_endDate1);
							startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							logger.log(Level.SEVERE, "Inside PC_INV_Quot_Con_SWAP_BILLING_AND_CONTRACT_PERIOD");
						}
						
						/**
						 * @author Anil
						 * @since 21-01-2022
						 * If service wise bill type of invoice is printing then need to change label from Contract Duration to
						 * Billing Period. raised by Nithila and Nitin Sir
						 */
						if(serviceWiseBillInvoice&&!billingPeriod.equals("")){
							startDate_endDate = new Phrase(billingPeriod, font6);
							startDate_endDateCell = new PdfPCell(startDate_endDate);
							startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						}
						
						Phrase qty = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getQuantity()
								+ "", font6);
						qtyCell = new PdfPCell(qty);
						qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						

						Phrase rate = null;
						
						
						
						/**
						 * @author Ashwini Patil
						 * @since 7-03-2022
						 * to print consolidated rate in rate column of product details table
						 */
						if (consolidatePrice || invoiceentity.isConsolidatePrice()) {
//							if (i == 0) {
							if(!consolidateRateSet){
								rate = new Phrase(df.format(rateAmountProd) + "",	font6);
								rateCell = new PdfPCell(rate);
								if(invoiceentity.getSalesOrderProducts().size() > 1) 
									rateCell.setBorderWidthBottom(0);
								rateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
								consolidateRateSet=true;
							} else {
								rate = new Phrase(" ", font6);

								rateCell = new PdfPCell(rate);
								if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
									rateCell.setBorderWidthTop(0);
								}
//								else{
//									rateCell.setBorderWidthBottom(0);
//									rateCell.setBorderWidthTop(0);
//								}
								rateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//								rateCell.setBorderWidthBottom(0);
								rateCell.setBorderWidthTop(0);

							}
						} else {
							rate = new Phrase(df.format(invoiceentity
									.getSalesOrderProducts().get(i).getPrice())
									+ "", font6);

							rateCell = new PdfPCell(rate);
							rateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						}

					}
					foundOnce=true;
					

//					noOfservices = noOfservices + 1;
					
					/**
					 * Date 01-06-2018
					 * Developer :- Vijay
					 * Des :- if condition old code added for service wise billing only and else block for
					 * no of services should come from contract as per nitin sir
					 */
					if(con.isServiceWiseBilling()){
						noOfservices = noOfservices + 1;
					}else{
						/*
						 * Ashwini Patil
						 * Date:11-06-2024
						 * When we process bills of multiple contract to create one invoice. No of services column gets set as zero. Reported by ultima search
						 */
						if(contractList!=null&&contractList.size()>1) {
							noOfservices=getNumberOfServicesFromContract(invoiceentity
									.getSalesOrderProducts().get(i).getProdCode(),contractList);
					
						}else {
							for(int l =0;l<con.getItems().size();l++){
								if(con.getItems().get(l).getProductCode().equals(invoiceentity
										.getSalesOrderProducts().get(i).getProdCode()) && con.getItems().get(l).getProductSrNo() == invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber()){
									noOfservices = con.getItems().get(l).getNumberOfServices();
									
								}
							}
						
						}
						}
					/**
					 * ends here
					 */
					
					discValueAtProductLevel=discValueAtProductLevel+invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
					double taxValue = 0;
					if (invoiceentity.getSalesOrderProducts().get(i)
							.getBasePaymentAmount() != 0
							&& invoiceentity.getSalesOrderProducts().get(i)
									.getPaymentPercent() != 0) {

						taxValue = invoiceentity.getSalesOrderProducts().get(i)
								.getBasePaymentAmount();
					} else {
						taxValue = invoiceentity.getSalesOrderProducts().get(i)
								.getBaseBillingAmount();
					}
					amountValueAtProductLevel=amountValueAtProductLevel+taxValue;
					
					
					if(qtycolumnFlag) {
						String quantity = "";
						try {
							
							if(invoiceentity.getSalesOrderProducts().get(i).getArea()!=null && 
									!invoiceentity.getSalesOrderProducts().get(i).getArea().equals("")){
								quantity = Double.parseDouble(invoiceentity.getSalesOrderProducts().get(i).getArea()) +"";
							}
							
						} catch (Exception e) {
							e.printStackTrace();
						}
						Phrase phQuantity = new Phrase(quantity, font6);
						quantitycell = new PdfPCell(phQuantity);
						quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);

					}

				}
				if (i + 1 == invoiceentity.getSalesOrderProducts().size()&&foundOnce) {//Ashwini Patil  Date:29-09-2022 added foundonce
					
					productTable.addCell(srNoCell);
					
					serviceNameCell.setColspan(colsspan);
					productTable.addCell(serviceNameCell);
					
					if(!PC_DoNotPrintContractSericesFlag){
						Phrase services = new Phrase(noOfservices + "", font6);
						servicesCell = new PdfPCell(services);
						servicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						productTable.addCell(servicesCell);
					}

					
					productTable.addCell(hsnCodeCell);
					
					if(!PC_DoNotPrintContractDurationFlag){
						productTable.addCell(startDate_endDateCell);
					}
					
					if(qtycolumnFlag && quantitycell!=null){
						productTable.addCell(quantitycell);
					}
					

//					if(!hideRateAndDiscount){
//						productTable.addCell(rateCell);
//					}
					
					int ratediscColspan = 3;
					if(!PC_DoNotPrintRateFlag){
						productTable.addCell(rateCell);
						ratediscColspan = ratediscColspan - 1;
					}
					
					
					Phrase discPhrase = null;
			
					
					
					/**
					 * @author Ashwini Patil
					 * @since 7-03-2022
					 * to print condolidated discount in discount column of product details
					 */
					if(consolidatePrice || invoiceentity.isConsolidatePrice()){
						System.out.println("in consolidate discount i="+i);
						if (!consolidateDiscSet) {
							if(discValueAtProductLevel>0) {
								 discPhrase = new Phrase(decimalformat.format(discValueAtProductLevel)
											+ "", font6);
							}
							else {
								 discPhrase = new Phrase("0", font6);
							}

							discCell = new PdfPCell(discPhrase);
							if(invoiceentity.getSalesOrderProducts().size() > 1)
							discCell.setBorderWidthBottom(0);
							discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							consolidateDiscSet=true;
						} else {
							discPhrase = new Phrase(" ", font6);

							discCell = new PdfPCell(discPhrase);
							if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
								
								discCell.setBorderWidthTop(0);
							}else{
								discCell.setBorderWidthBottom(0);
								discCell.setBorderWidthTop(0);
							}
							discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							discCell.setBorderWidthTop(0);
						}
					} else {
						if(discValueAtProductLevel>0) {
							 discPhrase = new Phrase(decimalformat.format(discValueAtProductLevel)
										+ "", font6);
						}
						else {
							 discPhrase = new Phrase("0", font6);
						}
						discCell = new PdfPCell(discPhrase);
						discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
									
					}

//					if(!hideRateAndDiscount){
//						productTable.addCell(discCell);
//					}	
					
					if(!PC_DoNotPrintDiscFlag){
						productTable.addCell(discCell);
						ratediscColspan = ratediscColspan - 1;
					}
	
		
					Phrase taxableValue;
					/**
					 * @author Ashwini Patil
					 * @since 7-03-2022
					 * to print consolidated amount in amount column of product details table
					 */
					if(consolidatePrice || invoiceentity.isConsolidatePrice()){

						if (!consolidatePriceSet) {
							taxableValue = new Phrase(df.format(totalAssAmountProd)
									+ "", font6);
							taxableValueCell = new PdfPCell(taxableValue);
							if(invoiceentity.getSalesOrderProducts().size() > 1)
							taxableValueCell.setBorderWidthBottom(0);
							taxableValueCell
									.setHorizontalAlignment(Element.ALIGN_CENTER);
							consolidatePriceSet=true;
						} else {
							System.out.println("in else i=0");
							taxableValue = new Phrase(" ", font6);
							taxableValueCell = new PdfPCell(taxableValue);
							if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
								
								taxableValueCell.setBorderWidthTop(0);
							}else{
								taxableValueCell.setBorderWidthBottom(0);
								taxableValueCell.setBorderWidthTop(0);
							}
							taxableValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							//taxableValueCell.setBorderWidthBottom(0);
							taxableValueCell.setBorderWidthTop(0);
						}
					} else {
						taxableValue = new Phrase(df.format(amountValueAtProductLevel) + "", font6);
						taxableValueCell = new PdfPCell(taxableValue);
						taxableValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					}
					
					
//					if(hideRateAndDiscount){
//						taxableValueCell.setColspan(3);
//					}
					taxableValueCell.setColspan(ratediscColspan);
					productTable.addCell(taxableValueCell);
					
					
					//productTable.addCell(taxableValueCell);
					// productTable.addCell(taxableValueCell);
					if (printPremiseDetails) {
						if(premiseblankCell!=null)
						productTable.addCell(premiseblankCell);
						if(premiseCell!=null)
						productTable.addCell(premiseCell);
					}

				}
			}
		}
		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines != 0) {
			remainingLines = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);

		if (remainingLines != 0) {
			for (int i = 0; i < remainingLines; i++) {
				System.out.println("i::::" + i);
				Phrase blankPhrase = new Phrase(" ", font10);
				PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
				blankPhraseCell.setBorder(0);
				blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
			}
		}
		PdfPCell productTableCell = null;
		if (noOfLines == 0) {
			Phrase my = new Phrase("Please Refer Annexure For More Details",
					font9bold);
			productTableCell = new PdfPCell(my);

		} else {
			productTableCell = new PdfPCell(blankCell);
		}

		productTableCell.setBorder(0);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		tab.addCell(productTableCell);
		tab.setSpacingAfter(blankLines);

		// last code for both table to be added in one table

		PdfPCell tab1;
		tab1 = new PdfPCell(productTable);
		// tab1.setBorder(0);

		PdfPCell tab2 = new PdfPCell(tab);
		// tab2.setBorder(0);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		mainTable.addCell(tab2);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	
	}

	private void createProductDetailsValForRate(HashSet<String> productCode) {

		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			/**
			 * @author Vijay Date :- 24-03-2022
			 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
			 */
			if(!invoiceentity.isConsolidatePrice()){

				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
						invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
					continue;
				}	
			}

			
			rateAmountProd = rateAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();//Ashwini Patil

			amountAmountProd = amountAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmountProd = discAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmountProd = totalAssAmountProd + taxValue;
		}

		int firstBreakPoint = 5;
		float blankLines = 0;

		PdfPTable productTable = new PdfPTable(10);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column10ProdCollonWidth);//column9ProdCollonWidth
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int countToBeDeducted = 0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			/**
			 * @author Vijay Date :- 24-03-2022
			 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
			 */
			if(!invoiceentity.isConsolidatePrice()){
				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
						invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
					continue;
				}
			}

			
			
			if (noOfLines == 0) {
				prouductCount = i;
				break;
			}
			countToBeDeducted = countToBeDeducted + 1;
			noOfLines = noOfLines - 1;

			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font6);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);// 1
			System.out.println("getProdName().trim().length()"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getProdName().trim().length());
			if (invoiceentity.getSalesOrderProducts().get(i).getProdName()
					.trim().length() > 42) {
				noOfLines = noOfLines - 1;
			}
			
			/**
			 * Date 06/06/2018 By vijay
			 * Des :- Service warranty if exist then it will show in pdf
			 */
			String productName = "";
			if(invoiceentity.getSalesOrderProducts().get(i).getWarrantyPeriod()!=0){
				productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim()
						+"\n"+" Warranty Period - "+invoiceentity.getSalesOrderProducts().get(i).getWarrantyPeriod();
			}else{
				productName = invoiceentity.getSalesOrderProducts().get(i).getProdName().trim();
			}
			/**
			 * ends here
			 */
			
			Phrase serviceName = new Phrase(productName, font6);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.addElement();
			if(PC_DoNotPrintContractSericesFlag){
				serviceNameCell.setColspan(2);
			}
			productTable.addCell(serviceNameCell);// 2

		
			Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getHsnCode().equals("")) {
				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font6);
			} else {
				ServiceProduct serviceProduct = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count",
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdId()).first().now();
				if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
					hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
				} else {
					hsnCode = new Phrase("", font6);
				}
			}

			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			productTable.addCell(hsnCodeCell);// 4
			
			Phrase services = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getOrderServices()
					+ "", font6);
			PdfPCell servicesCell = new PdfPCell(services);
			// serviceNameCell.addElement();
			if(!PC_DoNotPrintContractSericesFlag){
				productTable.addCell(servicesCell);// 3
			}

			
			//Ashwini Patil added service date value
			Service s=ofy().load().type(Service.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("billingCount",invoiceentity.getSalesOrderProducts()
							.get(i).getBiilingId()).first().now();
			
			logger.log(Level.SEVERE,"bill id="+invoiceentity.getSalesOrderProducts()
							.get(i).getBiilingId());
			
			Phrase serviceDate;
			if(s.getServiceCompletionDate()!=null)			
				serviceDate=new Phrase(sdf1.format(s.getServiceCompletionDate())+ "", font6);//changed ServiceDate to ServiceCompletionDate as per HPC requirement
			else		
				serviceDate=new Phrase(" ", font6);
				
			PdfPCell serviceDateCell = new PdfPCell(serviceDate);
	
			// serviceNameCell.addElement();
			productTable.addCell(serviceDateCell);			
			
			String startDateStr = "", endDateStr = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getPrduct().getCount());
				System.out
						.println("con.getItems().get(j).getPrduct().getCount()"
								+ con.getItems().get(j).getPrduct().getCount());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out.println("con.getItems().get(j).getDuration()"
						+ con.getItems().get(j).getDuration());

				SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
						"dd/MM/yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
				if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
						.getCount() == con.getItems().get(j).getPrduct()
						.getCount())
						&& (invoiceentity.getSalesOrderProducts().get(i)
								.getOrderDuration() == con.getItems().get(j)
								.getDuration())) {
					if (con.getItems().get(j).getEndDate() != null) {
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(con.getItems().get(j)
								.getEndDate());
					} else {
						Calendar c = Calendar.getInstance();
						c.setTime(con.getItems().get(j).getStartDate());
						c.add(Calendar.DATE, con.getItems().get(j)
								.getDuration());
						Date endDt = c.getTime();
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(endDt);
					}
				}
			}

			Phrase startDate_endDate = new Phrase(startDateStr + " - "
					+ endDateStr, font6);

			PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
			startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(startDate_endDateCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getArea()
					+ "", font6);
			PdfPCell qtyCell = new PdfPCell(qty);
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(qtyCell);// 5
			
			/***
			 *   Date : 08-06-2021
			 *   By : Priyanka
			 *   Des : Invoice is not getting print because UOM not selected. issue raised by Vaishnavi Mam.
			 */
			
			Phrase uom = null;
			if(invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement()!=null &&
					!invoiceentity.getSalesOrderProducts().get(i).getUnitOfMeasurement().trim().equals("")){
				
				 uom = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getUnitOfMeasurement().trim(), font6);
				 logger.log(Level.SEVERE,"In product UOM ="+invoiceentity.getSalesOrderProducts()
						.get(i).getUnitOfMeasurement().trim());
			}else{
				
				ServiceProduct serviceProduct = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("productCode",
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdCode().trim()).first().now();
				if (serviceProduct.getUnitOfMeasurement() != null) {
					 uom = new Phrase(serviceProduct.getUnitOfMeasurement().trim(), font6);
					 
					 logger.log(Level.SEVERE,"In product master UOM ="+serviceProduct.getUnitOfMeasurement().trim());
				} else {
					uom = new Phrase("", font6);
				}
			}
			
			
			PdfPCell uomCell = new PdfPCell(uom);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(uomCell);// /6

			Phrase rate = null;
			PdfPCell rateCell = null;
			
			int rateDiscColsSpan = 3;
			
//			if (consolidatePrice) {
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				//if (i == 0) {
				
				if(!consolidateRateSet){
					rate = new Phrase(df.format(rateAmountProd) + "", font6);

					rateCell = new PdfPCell(rate);
					rateCell.setBorderWidthBottom(0);
					rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					consolidateRateSet=true;
				} else {
					rate = new Phrase(" ", font6);

					rateCell = new PdfPCell(rate);
					rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					// rateCell.setBorderWidthBottom(0);
					rateCell.setBorderWidthTop(0);

				}
			} else {
				rate = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getPrice())
						+ "", font6);

				rateCell = new PdfPCell(rate);

				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}
			if(!PC_DoNotPrintRateFlag){
				productTable.addCell(rateCell);// 7
				rateDiscColsSpan = rateDiscColsSpan - 1;
			}

			// Phrase rate = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getPrice())
			// + "", font6);
			// PdfPCell rateCell = new PdfPCell(rate);
			// rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(rateCell);
			// amountAmount=amountAmount+invoiceentity.getSalesOrderProducts().get(i)
			// .getPrice()
			// * invoiceentity.getSalesOrderProducts().get(i)
			// .getQuantity();
			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);

			// discAmount=discAmount+invoiceentity
			// .getSalesOrderProducts().get(i).getFlatDiscount();
			Phrase disc = null;
			PdfPCell discCell = null;
			if (consolidatePrice) {
				if (i == 0) {
					disc = new Phrase(df.format(discAmountProd) + "", font6);

					discCell = new PdfPCell(disc);
					discCell.setBorderWidthBottom(0);
					discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					disc = new Phrase(" ", font6);

					discCell = new PdfPCell(disc);
					discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discCell.setBorderWidthTop(0);
				}
			} else {
//				disc = new Phrase(df.format(invoiceentity
//						.getSalesOrderProducts().get(i).getFlatDiscount())
//						+ "", font6);

				if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()>0) {
					disc = new Phrase(decimalformat.format(invoiceentity
							.getSalesOrderProducts().get(i).getFlatDiscount())
							+ "", font6);
				}
				else {
					disc = new Phrase("", font6);
				}
				
				discCell = new PdfPCell(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}

			if(!PC_DoNotPrintDiscFlag){
				productTable.addCell(discCell);// 8
				rateDiscColsSpan = rateDiscColsSpan - 1;
			}

			Phrase taxableValue;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}

			/**
			 * @author Vijay Date - 22-10-2021
			 * Des :- decimal format issue so updated code decimal format amount
			 */
			taxableValue = new Phrase(df.format(taxValue) + "", font6);

			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			taxableValueCell.setColspan(rateDiscColsSpan);
			productTable.addCell(taxableValueCell);// 9

			PdfPCell cellIGST;

			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());

			logger.log(Level.SEVERE, "VAT TAX ::::Config Name"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getTaxConfigName()
					+ "VAT TAx:::Tax Name"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getTaxName()
					+ "Ser TAX ::::Config Name"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getTaxConfigName()
					+ "Ser TAx:::Tax Name"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getTaxName());
			logger.log(Level.SEVERE, "VAT TAX ::::"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage()
					+ "Service Tax::::"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getPercentage());

			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount() && invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
					premisesVal = con.getItems().get(j).getPremisesDetails();
				}

			}
			System.out.println("noOfLines in product" + noOfLines);
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					noOfLines = noOfLines - 1;
					Phrase blankValPhrs = new Phrase("", font8);
					PdfPCell premiseCell = new PdfPCell(blankValPhrs);
					premiseCell.setColspan(1);

					productTable.addCell(premiseCell);// 1

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(9); //One more column serviceDate added so colspan set to 9 from 8
					productTable.addCell(premiseCell);
				}
			} else {

			}
			boolean taxPresent = validateTaxes(invoiceentity
					.getSalesOrderProducts().get(i));
		}
		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines != 0) {
			remainingLines = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);

		if (remainingLines != 0) {
			for (int i = 0; i < remainingLines; i++) {
				System.out.println("i::::" + i);
				Phrase blankPhrase = new Phrase(" ", font10);
				PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
				blankPhraseCell.setBorder(0);
				blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
			}
		}
		PdfPCell productTableCell = null;
		if (noOfLines == 0) {
			Phrase my = new Phrase("Please Refer Annexure For More Details",
					font9bold);
			productTableCell = new PdfPCell(my);

		} else {
			productTableCell = new PdfPCell(blankCell);
		}

		productTableCell.setBorder(0);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		tab.addCell(productTableCell);
		tab.setSpacingAfter(blankLines);

		// last code for both table to be added in one table

		PdfPCell tab1;
		tab1 = new PdfPCell(productTable);
		// tab1.setBorder(0);

		PdfPCell tab2 = new PdfPCell(tab);
		// tab2.setBorder(0);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		mainTable.addCell(tab2);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createProductDetailsForRate() {
		// TODO Auto-generated method stub
		
		/**
		 * @author Ashwini Patil
		 * @since 4-05-2022
		 * Perfect Pest control raised this point that they want service date on invoice print.
		 * As per Nitin sir's instruction adding that column and changing column sequence.
		 */

		PdfPTable productTable = new PdfPTable(10);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column10ProdCollonWidth);//column9ProdCollonWidth
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase srNophrase = new Phrase("Sr No", font10bold);
		PdfPCell srNoCell = new PdfPCell(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

//		Phrase servicePhrase = new Phrase("Particulars", font10bold);
		/**@Sheetal : 02-03-2022 Renaming Particulars to Service, customization for pest-o-sheild**/
		Phrase servicePhrase = new Phrase("Service", font10bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2
		if(PC_DoNotPrintContractSericesFlag){
			servicePhraseCell.setColspan(2);
		}


		Phrase serviceQuantityPhrase = new Phrase("Services", font10bold);
		PdfPCell serviceQuantityPhraseCell = new PdfPCell(serviceQuantityPhrase);
		serviceQuantityPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceQuantityPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		serviceQuantityPhraseCell.setRowspan(2);// 3

		Phrase hsnCode = new Phrase("HSN/SAC", font10bold);
		PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 4

		Phrase UOMphrase = new Phrase("UOM", font10bold);
		PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 5

		Phrase qtyPhrase = new Phrase("Qty", font10bold);
		PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 6

		Phrase ratePhrase = new Phrase("Rate", font10bold);
		PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 7

		Phrase amountPhrase = new Phrase("Amount", font10bold);
		PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 8

		Phrase dicphrase = new Phrase("Disc", font10bold);
		PdfPCell dicphraseCell = new PdfPCell(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 9

		Phrase taxValPhrase = new Phrase("Amount", font10bold);
		PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 10

		Phrase serviceServDate = new Phrase("Duration", font10bold);
		PdfPCell serviceServDateCell = new PdfPCell(serviceServDate);
		serviceServDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceServDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase serviceDate = new Phrase("Service Date", font10bold);
		PdfPCell serviceDateCell = new PdfPCell(serviceDate);
		serviceDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		int rateDiscColsSpan = 3;

		productTable.addCell(srNoCell);// 1
		productTable.addCell(servicePhraseCell);// 2
		// productTable.addCell(serviceServDateCell);
		productTable.addCell(hsnCodeCell);// 4
		if(!PC_DoNotPrintContractSericesFlag){
			productTable.addCell(serviceQuantityPhraseCell);// 3
		}

		productTable.addCell(serviceDateCell); //added by Ashwini Patil

		productTable.addCell(qtyPhraseCell);// 5
		productTable.addCell(UOMphraseCell);// 6
		
		if(!PC_DoNotPrintRateFlag){
			productTable.addCell(ratePhraseCell);// 7
			rateDiscColsSpan = rateDiscColsSpan - 1;
		}
		// productTable.addCell(amountPhraseCell);
		if(!PC_DoNotPrintDiscFlag){
			productTable.addCell(dicphraseCell);// 8
			rateDiscColsSpan = rateDiscColsSpan - 1;
		}
		taxValPhraseCell.setColspan(rateDiscColsSpan);
		productTable.addCell(taxValPhraseCell);// 9

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterOtherChargesPart2() {
		// TODO Auto-generated method stub

		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// String amtInWordsVal = "Amount in Words : Rupees "
		// + SalesInvoicePdf.convert(invoiceentity.getNetPayable())
		// + " Only/-";
		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		otherChargesTable.addCell(amtInWordsValCell);

		Phrase otherCharges = new Phrase("Other Charges", font10bold);

		PdfPCell netPayCell = new PdfPCell(otherCharges);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		double totalOtherCharges = 0;
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			totalOtherCharges = totalOtherCharges
					+ invoiceentity.getOtherCharges().get(i).getAmount();
		}
		Phrase netPayVal = new Phrase(totalOtherCharges + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		otherChargesTable.addCell(netPayableCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountBeforeNetPayPart(double roundOff) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Round Off", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(roundOff) + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountAfterPart(double discount) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Discount Amt", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(discount) + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterOtherChargesPart() {
		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");
		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase blank = new Phrase("", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		otherChargesTable.addCell(blankCell);

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// for (int i = 0; i < invoiceentity.getBillingOtherCharges().size();
		// i++) {
		Phrase chargeName, taxes, assVal;
		PdfPCell pCell;
		// if(i==0){
		chargeName = new Phrase("Charge Name", font10bold);
		taxes = new Phrase("Taxes", font10bold);
		assVal = new Phrase("Amt", font10bold);
		pCell = new PdfPCell(chargeName);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(taxes);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(assVal);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		// }else{
		chargeName = new Phrase("Transport", font10);
		taxes = new Phrase("SGST@9/CGST@9"/*
										 * invoiceentity.getBillingOtherCharges()
										 * .get(i).get()
										 */, font10);
		assVal = new Phrase("100", font10);
		pCell = new PdfPCell(chargeName);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(taxes);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(assVal);
		otherCharges.addCell(pCell);
		// }

		// }
		PdfPCell leftCell = new PdfPCell(otherCharges);
		otherChargesTable.addCell(leftCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createTermsAndCondition() {

		String friends = "";
		if (onlyForFriendsPestControl) {
			friends = "1.If you are not satisfied with the treatment within 15 days of treatment, free treatment will be provided. \n"
					+ "2.You will receive two reminders for each of your treatments by call, email and by SMS. Our obligation limits to reminders only. \n"
					+ "3.It is essential to avail the treatment within the due dates to validate the warranty. \n"
					+ "4.Contract period cannot be extended for any reason. \n"
					+ "5.Once the due date is over the treatment cannot be carry forwarded to extend the contract period. \n"
					+ "6.It is mandatory to avail the treatment within a period of fifteen days before or after due date.Otherwise the treatment will be considered as lapsed.\n"
					+ "THEREFOR PLEASE INSURE THE SCHEDULE MENTION HERE IS STRICTLY FOLLOWED";
		} else if(invoiceentity.getComment()!=null&&!invoiceentity.getComment().equals("")) {
			//logger.log(Level.SEVERE,"in remark condition 2");
			friends = invoiceentity.getComment().trim();
		}
		else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PrintDescriptionOnInvoiceFromContract" , invoiceentity.getCompanyId())){
		//	logger.log(Level.SEVERE,"in remark condition 3");
			if(con.getDescription()!=null) {
			//	logger.log(Level.SEVERE,"in remark condition 3.1");
				friends=con.getDescription();
			}
		}else {
			//logger.log(Level.SEVERE,"in remark condition 4");
			friends="";
		}

		int remainingLinesForTerms = 5;

		if (friends.length() > (138 * 5)) {
			friends = friends.substring(0, (138 * 5));
		}
		Phrase termNcondVal = new Phrase("Remarks: \n" + friends, font10bold);

		
		/**
		 * @author Vijay Date :- 01-03-2022 
		 * Des :- if QR Code exist then Remark with QR code will print
		 * and if QR code does not exist then remark will print as it is
		 */
		logger.log(Level.SEVERE, "comppayment "+comppayment);
		if(comppayment!=null && comppayment.getQrCodeDocument()!=null && comppayment.getQrCodeDocument().getUrl()!=null && !comppayment.getQrCodeDocument().getUrl().equals("")){
			
			DocumentUpload digitalDocument = comppayment.getQrCodeDocument();
			String hostUrl;
			String environment = System
					.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
				String applicationId = System
						.getProperty("com.google.appengine.application.id");
				String version = System
						.getProperty("com.google.appengine.application.version");
				hostUrl = "http://" + version + "." + applicationId
						+ ".appspot.com/";
			} else {
				hostUrl = "http://localhost:8888";
			}
			PdfPCell qrCodeCell = null;
			Image image2 = null;
			logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
			try {
				logger.log(Level.SEVERE, "comppayment.getQrCodeDocument() "+comppayment.getQrCodeDocument());

				image2 = Image.getInstance(new URL(hostUrl
						+ digitalDocument.getUrl()));
//				image2=Image.getInstance("images/logo15.jpg");
				image2.scalePercent(35f);
				image2.scaleAbsoluteWidth(100f);

				qrCodeCell = new PdfPCell(image2);
				qrCodeCell.setBorder(0);
				qrCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
			float[] columnWidths = { 2f, 0.60f };

			PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
			termNcondValCell.setBorderWidthBottom(0);
			termNcondValCell.setBorderWidthTop(0);
			
			PdfPTable pdfTable = new PdfPTable(2);
			pdfTable.setWidthPercentage(100);
			try {
				pdfTable.setWidths(columnWidths);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			pdfTable.addCell(termNcondValCell);
			
			PdfPCell pdfPqrcodecell = new PdfPCell(qrCodeCell);
			pdfPqrcodecell.setBorder(0);


			PdfPTable pdfTable2 = new PdfPTable(1);
			pdfTable2.setWidthPercentage(100);
			
			Phrase phQrCodeTitle = new Phrase("Scan QR Code to Pay",font10bold);
			PdfPCell qrCodeTitleCell=new PdfPCell(phQrCodeTitle);
			qrCodeTitleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			qrCodeTitleCell.setBorder(0);
			qrCodeTitleCell.setPaddingBottom(4);
			
			pdfTable2.addCell(qrCodeTitleCell);
			pdfTable2.addCell(pdfPqrcodecell);

			pdfTable.addCell(pdfTable2);
			
			PdfPCell pdfPcell = new PdfPCell(pdfTable);
			pdfPcell.setBorder(0);
			pdfPcell.setFixedHeight(100f);
			System.out.println("Remakrs table height"+pdfPcell.getHeight());

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(pdfPcell);
			try {
				document.add(table1);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
		else{
			
			PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
			termNcondValCell.setBorderWidthBottom(0);
			termNcondValCell.setBorderWidthTop(0);
			PdfPTable pdfTable = new PdfPTable(1);
			pdfTable.setWidthPercentage(100);
			pdfTable.addCell(termNcondValCell);

			Phrase blankPhrase = new Phrase(" ", font10bold);
			PdfPCell blank = new PdfPCell(blankPhrase);
			blank.setBorderWidthBottom(0);
			blank.setBorderWidthTop(0);
			remainingLinesForTerms = remainingLinesForTerms
					- (friends.length() / (138));
			System.out.println("remainingLinesForTerms" + remainingLinesForTerms);
			for (int i = 0; i < remainingLinesForTerms; i++) {
				pdfTable.addCell(blank);
			}
			PdfPCell pdfPcell = new PdfPCell(pdfTable);
			pdfPcell.setBorder(0);

			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100);
			table1.addCell(pdfPcell);
			try {
				document.add(table1);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
		
		

	}

	private void createFooterAmountInWords_NetPayPart() {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		String amtInWordsVal = "Amount in Words : Rupees "
//				+ SalesInvoicePdf.convert(invoiceentity.getNetPayable())
//				+ " Only/-";
		
		/**
		 *  Added By Priyanka : 19-08-2021
		 *  Des : For Thai client no need to print rupees word.
		 */
		String amtInWordsVal="";
		/**
		 * @author Ashwini Patil
		 * @since 27-04-2022
		 * Amount in words was not printing decimal part reported by Atharva.
		 * pdfUtility.getAmountInWords() method will return correct amount in word value as per all process configs,currency and country
		 */
		amtInWordsVal=pdfUtility.getAmountInWords(invoiceentity.getNetPayable(), invoiceentity.getCompanyId(), cust);
		
		/**
		 * @author Vijay Date :- 29-09-2021
		 * Des :- if below process config is active then amount will print in hundread structure  
		 */
//		if(AmountInWordsHundreadFormatFlag){
//			amtInWordsVal = EnglishNumberToWords.convert(invoiceentity.getNetPayable());
//		}
//		else{
//			amtInWordsVal = ServiceInvoicePdf.convert(invoiceentity.getNetPayable());
//		}
		/**
		 * ends here
		 */
		
		if(thaiPdfFlag){
			/**
			 * @author Anil
			 * @since 19-01-2022
			 * Amount in words to be in thai language using BahtText. Raised by Nithila and Nitin Sir
			 */
//			amtInWordsVal = "Amount in Words : "+ amtInWordsVal	+ " Only/-";
			amtInWordsVal = bahtText.getBath(df.format(invoiceentity.getNetPayable()));
		}else if(thaiFontFlag){
			amtInWordsVal = "Amount in Words : "
					+ amtInWordsVal;//	+ " Only/-";
		}else
			amtInWordsVal = "Amount in Words : "+ amtInWordsVal;//	+ " Only/-";
//		else if(AmountInWordsHundreadFormatFlag){
//			amtInWordsVal = "Amount in Words : "
//					+ amtInWordsVal	+ " Only/-";
//		}
//		else{
//			amtInWordsVal = "Amount in Words : Rupees "
//					+ amtInWordsVal	+ " Only/-";
//		}
		
//		if(thaiFontFlag){
//		amtInWordsVal = "Amount in Words : "
//				+ SalesInvoicePdf.convert(invoiceentity.getNetPayable())
//				+ " Only/-";
//		}else{
//		amtInWordsVal = "Amount in Words : Rupees "
//				+ SalesInvoicePdf.convert(invoiceentity.getNetPayable())
//				+ " Only/-";
//		}
		/**
		 *  End
		 */
		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font10bold);
		
		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Net Payable", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(invoiceentity.getNetPayable())
				+ "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterAmountPart() {

		double totalAssAmount = 0;
		double rateAmount = 0;
		double amountAmount = 0;
		double discAmount = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			/**
			 * @author Vijay Date :- 24-03-2022
			 * Des :- As per nitin sir if payable amount is zero then dont show that product on invoice pdf
			 */
			if(!invoiceentity.isConsolidatePrice()){
				if(invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0 || 
						invoiceentity.getSalesOrderProducts().get(i).getBasePaymentAmount()==0.00){
					continue;
				}
			}
			
			rateAmount = rateAmount
					+ invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();//Ashwini Patil

			amountAmount = amountAmount
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmount = discAmount
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmount = totalAssAmount + taxValue;
		}
		PdfPTable productTable = new PdfPTable(4);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column4ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase totalamount = new Phrase("Total", font10bold);
		PdfPCell totalAmountCell = new PdfPCell(totalamount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totalAmountCell.setColspan(2);
		productTable.addCell(totalAmountCell);
		
		
//		if(!hideRateAndDiscount){
//			Phrase rateValue = new Phrase(df.format(rateAmount), font10);
//			PdfPCell rateValueCell = new PdfPCell(rateValue);
//			rateValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			productTable.addCell(rateValueCell);
//			}
			
			Phrase amountValue = new Phrase(df.format(amountAmount), font10);
			PdfPCell amountValueCell = new PdfPCell(amountValue);
			amountValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountValueCell);

			if(!hideRateAndDiscount){
//				Phrase discamount = new Phrase(df.format(discAmount), font10);
				Phrase discamount = null; 
				if(discAmount>0) {
					discamount = new Phrase(decimalformat.format(discAmount), font10);
				}
				else {
					discamount = new Phrase("0.00", font10);
				}
			PdfPCell discamountCell = new PdfPCell(discamount);
			discamountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(discamountCell);
			}
			
			if(!hideRateAndDiscount){
			System.out.println("totalAssAmount:::" + totalAssAmount);
			Phrase totalValamount = new Phrase(df.format(totalAssAmount), font10);
			PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
			totalAmountValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(totalAmountValCell);
			}else{
				Phrase totalValamount = new Phrase(df.format(totalAssAmount), font10);
				PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
				totalAmountValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalAmountValCell.setColspan(3);
				productTable.addCell(totalAmountValCell);
			}

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createtotalAmount() {

		PdfPTable productTable = new PdfPTable(2);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase totalamount = new Phrase("Total Ass Amount", font10bold);
		PdfPCell totalAmountCell = new PdfPCell(totalamount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountCell);

		Phrase totalValamount = new Phrase(df.format(0.00), font10);
		PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
		totalAmountValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountValCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// /////////////////////////////////////////// Ajinkya Code Start Here
	// //////////////////
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,725f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	// ////////////////////////////// Code End Here
	// /////////////////////////////////

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 10f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,40f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	// ///////////////////////// Ajinkya Code end Here ///////////////

	private void createBlankforUPC() {

		Image uncheckedImg = null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(9);

		// Phrase phrAlp=new Phrase("  ALP",font9);
		// Paragraph paraAlp=new Paragraph();
		// paraAlp.setIndentationLeft(10f);
		// paraAlp.add(new Chunk(uncheckedImg, 0, 0, true));
		// paraAlp.add(phrAlp);

		// rohan added this code
		// float[] myWidth = {1,3,20,17,3,30,17,3,20,1};

		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setSpacingAfter(5f);
		mytbale.setWidthPercentage(100f);
		// try {
		// mytbale.setWidths(myWidth);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		Phrase myblank = new Phrase("   ", font10);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero = new Phrase(" ", font10);
		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat1Phrase = new Phrase("  Original for Receipient", font10);
		Paragraph para1 = new Paragraph();
		para1.setIndentationLeft(10f);
		para1.add(myblank);
		para1.add(new Chunk(uncheckedImg, 0, 0, true));
		para1.add(stat1Phrase);
		para1.setAlignment(Element.ALIGN_MIDDLE);

		PdfPCell stat1PhraseCell = new PdfPCell(para1);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat2Phrase = new Phrase("  Duplicate for Supplier/Transporter",
				font10);
		Paragraph para2 = new Paragraph();
		para2.setIndentationLeft(10f);
		para2.add(new Chunk(uncheckedImg, 0, 0, true));
		para2.add(stat2Phrase);
		para2.setAlignment(Element.ALIGN_CENTER);

		PdfPCell stat2PhraseCell = new PdfPCell(para2);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat3Phrase = new Phrase("  Triplicate for Supplier", font10);
		Paragraph para3 = new Paragraph();
		para3.setIndentationLeft(10f);
		para3.add(new Chunk(uncheckedImg, 0, 0, true));
		para3.add(stat3Phrase);
		para3.setAlignment(Element.ALIGN_JUSTIFIED);

		PdfPCell stat3PhraseCell = new PdfPCell(para3);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// mytbale.addCell(myblankborderZero);
		// mytbale.addCell(myblankborderZero);
		// mytbale.addCell(stat1PhraseCell);
		// // mytbale.addCell(myblankborderZeroCell);
		// // mytbale.addCell(myblankCell);
		// mytbale.addCell(stat2PhraseCell);
		// // mytbale.addCell(myblankborderZeroCell);
		// // mytbale.addCell(myblankCell);
		// mytbale.addCell(stat3PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);

		// ends here
		String titlepdf = "";

//		if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
//				.getInvoiceType().trim())
//				|| invoiceentity.getInvoiceType().trim()
//						.equals(AppConstants.CREATEPROFORMAINVOICE))
//			//titlepdf = "Proforma Invoice";
//			if(changeTitle){
//				titlepdf = "Invoice";
//			}else{
//				titlepdf = "Proforma Invoice";
//			}
//		else
//			titlepdf = "Tax Invoice";

		/***
		 * Date 21-5-2018 by jayshree
		 */

		if (nonbillingInvoice == true) {
			if (invoiceentity.getBillingTaxes().size() == 0) {
				/**
				 * Date 25-9-2020 by Amol commented this line raised by Rahul
				 * Tiwari.
				 **/
				titlepdf = "Estimate";

				/**
				 * Date 23-12-2020 by Priyanka Bhagwat Des: Om pest control-
				 * replace invoice word to estimate on non billing invoice pdf.
				 */

				// titlepdf = "Invoice";

				/**
				 * @author Anil @since 09-04-2021 If non billing process
				 *         configuration is active and no tax selected then for
				 *         Proforma invoice it should print title as Proforma
				 *         Invoice instead Estimate Raised by Ashwini for Ultra
				 *         Pest Control
				 */
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
					titlepdf = "Proforma Invoice";
				}

			} else {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
					titlepdf = "Proforma Invoice";
				else
					titlepdf = "Tax Invoice";
			}
		} else {
			/**
			 * @author Anil @since 12-04-2021
			 * For ultra pest control, if no tax is selected and non billing process configurationj is off then print 
			 * Invoice on PDF else it will be Tax Invoice
			 * Raised by Ashwini 
			 */
//			if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
//				titlepdf = "Proforma Invoice";
//			else
//				titlepdf = "Tax Invoice";
			
			titlepdf = "Invoice";
			if (invoiceentity.getBillingTaxes().size() == 0) {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
					titlepdf = "Proforma Invoice";
				}

			} else {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE)){
					titlepdf = "Proforma Invoice";
				}else{
					titlepdf = "Tax Invoice";
				}
			}

		}
		
		/**
	    * @author Anil @since 01-10-2021
	    */
	   titlepdf=pdfUtility.getInvoiceTitle(invoiceentity, branchDt, titlepdf);
	   logger.log(Level.SEVERE,"After getInvoiceTitle Method : "+titlepdf +" | "+pdfUtility.printBankDetailsFlag);
		
	   /**
		 * @author Anil
		 * @since 19-01-2022
		 * if we are printing invoice for thai client then title will be hard coded as provided by client
		 * raised by Nitin And Nithila
		 */
		if(thaiPdfFlag){
			if(!invoiceTitle.equals("")){
				titlepdf=invoiceTitle;
			}
		}
		
		//Ashwini Patil Date:15-01-2025 If client is going to maintain invoices in zoho then it's illegal to maintain tax invoice in eva as well. So changing title of tax  invoice to proforma invoice as per nitin sir's instruction
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "EnableInvoiceIntegrationWithZohoBooks", invoiceentity.getCompanyId())) {
					titlepdf="Proforma Invoice";			
		}
		Phrase titlephrase = new Phrase(titlepdf, titlefont);//Ashwini Patil
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		titlecell.setVerticalAlignment(Element.ALIGN_BOTTOM); //set by ashwini patil 
		

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		
		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 2f, 1f };
		try {
			titlepdftable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(10f);
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * Need to add invoice copy name 
		 * raised by Nithila and Nitin for Innovative
		 */
		if(thaiPdfFlag){
			if(!copyTitle.equals("")){
				parent.addCell(pdfUtility.getCell(copyTitle, font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			}
		}

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		/*
		 * Commented by Ashwini
		 */
//		try {
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(mytbale);
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}

		
		/*
		 * Date:30/07/2018
		 * Developer:Ashwini
		 * Des:To increase headerspace while printing invoice
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , invoiceentity.getCompanyId())){
			 Paragraph blank1 =new Paragraph();
			    blank1.add(Chunk.NEWLINE);
			    
			
			    
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(mytbale);
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		else{
		Paragraph blank2 = new Paragraph();
		blank2.add(Chunk.NEWLINE);
		 System.out.println("Pune");
		
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(mytbale);
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}
		/*
		 * End by Ashwini
		 */
}
	private void createLogo(Document doc, Company comp2) {

		// ********************logo for server ********************
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl
//					+ document.getUrl()));
//			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f, 750f);
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		/**
		 * @author Vijay Date :- 22-10-2021
		 * Des :- Dor Mojo Logo showing 2 times so updated the code
		 */
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + document.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
			
			logger.log(Level.SEVERE, "imageSignCell  == "+image2);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,785f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

	}

	private void createFooterLastPart(String preprintStatus) {
		// TODO Auto-generated method stub
//		PdfPTable bottomTable = new PdfPTable(3);
//		bottomTable.setWidthPercentage(100);
//		float[] columnThreePartWidths = { 1f, 1f, 1f };
//		try {
//			bottomTable.setWidths(columnThreePartWidths);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		PdfPTable leftTable = new PdfPTable(1);
//		leftTable.setWidthPercentage(100);
//		// rohan added this code for universal pest
//		if (UniversalFlag) {
//			if (con.getGroup().equalsIgnoreCase("Universal Pest Control Pvt. Ltd.")) {
//				if (!preprintStatus.equalsIgnoreCase("Plane")) {
//					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//						if (comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")) {
//
//							Phrase articalType = new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()
//									+ " : "+ comp.getArticleTypeDetails().get(i).getArticleTypeValue(), font10bold);
//							PdfPCell articalTypeCell = new PdfPCell();
//							articalTypeCell.setBorder(0);
//							articalTypeCell.addElement(articalType);
//							leftTable.addCell(articalTypeCell);
//						}
//					}
//				}
//			}
//		} else {
//			// if (!preprintStatus.equalsIgnoreCase("Plane")) {
//			// leftTable.addCell(articalTypeCell);asa
//			ServerAppUtility serverApp = new ServerAppUtility();
//			String gstin = "", gstinText = "";
//			if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
//				logger.log(Level.SEVERE, "GST Applicable");
//				gstin = serverApp.getGSTINOfCompany(comp, invoiceentity.getBranch().trim());
//				System.out.println("gstin" + gstin);
//			} else {
//				logger.log(Level.SEVERE, "GST Not Applicable");
//				gstinText = comp.getCompanyGSTTypeText().trim();
//				System.out.println("gstinText" + gstinText);
//			}
//
//			Phrase articalType2 = null;
//			if (!gstin.trim().equals("")) {
//				logger.log(Level.SEVERE, "GST Present");
//				articalType2 = new Phrase("GSTIN" + " : " + gstin, font10bold);
//			} else if (!gstinText.trim().equalsIgnoreCase("")) {
//				logger.log(Level.SEVERE, "GST Not Present");
//				articalType2 = new Phrase(gstinText, font10bold);
//			} else {
//				logger.log(Level.SEVERE, "Nothing Present");
//				articalType2 = new Phrase("", font10bold);
//			}
//
//			if (!gstin.equals("")) {
//				PdfPCell articalType2Cell = new PdfPCell(articalType2);
//				articalType2Cell.setBorder(0);
//				leftTable.addCell(articalType2Cell);
//
//				String stateCodeStr = serverApp.getStateOfCompany(comp,invoiceentity.getBranch().trim(), stateList);
//				Phrase stateCode = new Phrase("State Code" + " : "+ stateCodeStr, font10bold);
//
//				PdfPCell stateCodeCell = new PdfPCell(stateCode);
//				stateCodeCell.setBorder(0);
//				leftTable.addCell(stateCodeCell);
//			} else {
//				PdfPCell articalType2Cell = new PdfPCell(articalType2);
//				articalType2Cell.setBorder(0);
//				leftTable.addCell(articalType2Cell);
//			}
//
//		}
//			
//		
//		/** 
//		 *  Date : 02-07-2021 BY Priyanka
//		 *  Des : need to map company details from company master req by Perfectpestcontrol raised by Rahul Tiwari
//		 */
//			PdfPTable articleTab = new PdfPTable(3);
//			articleTab.setWidthPercentage(100);
//
//			try {
//				articleTab.setWidths(new float[] { 30, 5, 65 });
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			Phrase typename;
//			Phrase typevalue;
//			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
//				/**
//				 *  Comment below code By Priyanka 
//				 */
////				/**
////				 * Date : 14-12-2018 BY ANIL
////				 * Earlier we checking the condition that article type should not be GSTIN ,right now we displaying only for ServiceInvoice
////				 */
////				if (comp.getArticleTypeDetails().get(i).getDocumentName().trim().equalsIgnoreCase("ServiceInvoice")
////						&&comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")) {
//////				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName().trim().equalsIgnoreCase("GSTIN")) {
////					Phrase articalType = new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()
////							+ " : "+ comp.getArticleTypeDetails().get(i).getArticleTypeValue(), font10bold);
////					PdfPCell articalTypeCell = new PdfPCell(articalType);
////					articalTypeCell.setBorder(0);
////					leftTable.addCell(articalTypeCell);
////				}
//				
//				if (comp.getArticleTypeDetails().get(i).getArticlePrint()
//						.equalsIgnoreCase("YES")
//						&& (comp.getArticleTypeDetails().get(i)
//								.getDocumentName().equals("Invoice Details") || comp
//								.getArticleTypeDetails().get(i)
//								.getDocumentName().equals("ServiceInvoice"))) {
//
//					typename = new Phrase(comp.getArticleTypeDetails().get(i)
//							.getArticleTypeName(), font10bold);
//					typevalue = new Phrase(comp.getArticleTypeDetails().get(i)
//							.getArticleTypeValue(), font10bold);
//
//					PdfPCell tymanecell = new PdfPCell();
//					tymanecell.addElement(typename);
//					tymanecell.setBorder(0);
//					tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//					Phrase typeblank = new Phrase(":", font10bold);
//					PdfPCell typeCell = new PdfPCell(typeblank);
//					typeCell.addElement(typeblank);
//					typeCell.setBorder(0);
//					typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//					PdfPCell typevalcell = new PdfPCell();
//					typevalcell.addElement(typevalue);
//					typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//					typevalcell.setBorder(0);
//					
//						articleTab.addCell(tymanecell);
//						articleTab.addCell(typeCell);
//						articleTab.addCell(typevalcell);
//					
//				}
//			}
//			
//			PdfPCell articleCell = new PdfPCell(articleTab);
//			articleCell.setBorder(0);
//			leftTable.addCell(articleCell);
//		
//		
//		
//		PdfPCell leftCell = new PdfPCell();
//		leftCell.addElement(leftTable);
//
//		PdfPTable rightTable = new PdfPTable(1);
//		rightTable.setWidthPercentage(100);
//
//		PdfPTable innerRightTable = new PdfPTable(3);
//		innerRightTable.setWidthPercentage(100);
//
//		Phrase colon = new Phrase(" :", font10bold);
//		PdfPCell colonCell = new PdfPCell();
//		colonCell.setBorder(0);
//		colonCell.addElement(colon);
//
//		Phrase blank = new Phrase(" ", font10bold);
//		PdfPCell blankCell = new PdfPCell(blank);
//		blankCell.setBorder(0);
//		logger.log(Level.SEVERE, "Branchas company "+branchAsCompany);
//		String companyname = "";
//		if (multipleCompanyName) {
//			if (con.getGroup() != null && !con.getGroup().equals("")) {
//				companyname = con.getGroup().trim().toUpperCase();
//			} else {
//				companyname = comp.getBusinessUnitName().trim().toUpperCase();
//			}
//
//		}
//		else if(branchAsCompany){
//			logger.log(Level.SEVERE, " Inside Branchcompany "+branchAsCompany);
//			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
//				companyname= branchDt.getCorrespondenceName();
//				logger.log(Level.SEVERE,"branch correspondence name "+companyname);
//			}else{
//				companyname = comp.getBusinessUnitName().trim().toUpperCase();
//				logger.log(Level.SEVERE,"Company Name "+companyname);
//			}
//		}
//		else {
//			companyname = comp.getBusinessUnitName().trim().toUpperCase();
//		}
//		
//		
//		
//		
//		
//		
//		
//		
//		
//		
//
//		// ends here
//
//		// rightTable.addCell(rightUpperCell);
//		Phrase companyPhrase = new Phrase("For " + companyname, font10bold);
//		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
//		companyParaCell.setBorder(0);
//		if (authOnLeft) {
//			companyParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else {
//			companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//
//		rightTable.addCell(companyParaCell);
//
//		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
//		String hostUrl;
//		String environment = System
//				.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//			String applicationId = System
//					.getProperty("com.google.appengine.application.id");
//			String version = System
//					.getProperty("com.google.appengine.application.version");
//			hostUrl = "http://" + version + "." + applicationId
//					+ ".appspot.com/";
//		} else {
//			hostUrl = "http://localhost:8888";
//		}
//		imageSignCell = null;
//		Image image2 = null;
//		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
//		try {
//			image2 = Image.getInstance(new URL(hostUrl
//					+ digitalDocument.getUrl()));
//			image2.scalePercent(15f);
//			image2.scaleAbsoluteWidth(100f);
//
//			imageSignCell = new PdfPCell(image2);
//			imageSignCell.setBorder(0);
//			if (authOnLeft) {
//				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			} else {
//				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		// Image image1=Image.getInstance("images/digisign2copy.png");
//		// image1.scalePercent(15f);
//		// image1.scaleAbsoluteWidth(100f);
//		// imageCell=new PdfPCell(image1);
//		// imageCell.setBorder(0);
//		// if (authOnLeft) {
//		// imageCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		// } else {
//		// imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		// }
//		// }
//		// catch(Exception e)
//		// {
//		// e.printStackTrDayace();
//		// }
//		if (imageSignCell != null) {
//			rightTable.addCell(imageSignCell);
//		} else {
//			Phrase blank1 = new Phrase(" ", font10);
//			PdfPCell blank1Cell = new PdfPCell(blank1);
//			blank1Cell.setBorder(0);
//			rightTable.addCell(blank1Cell);
//			rightTable.addCell(blank1Cell);
//			rightTable.addCell(blank1Cell);
//			rightTable.addCell(blank1Cell);
//
//		}
//		Phrase signAuth;
//		if (comp.getSignatoryText() != null
//				&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
//			signAuth = new Phrase("Authorised Signatory", font10bold);
//		} else {
//			signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
//		}
//		// Paragraph signPara = new Paragraph();
//		// signPara.add(signAuth);
//		// signPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell signParaCell = new PdfPCell(signAuth);
//		// signParaCell.addElement();
//
//		signParaCell.setBorder(0);
//		if (authOnLeft) {
//			signParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		} else {
//			signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		}
//		rightTable.addCell(signParaCell);
//
//		PdfPCell lefttableCell = new PdfPCell(leftTable);
//		// lefttableCell.addElement();
//		PdfPCell righttableCell = new PdfPCell(rightTable);
//		// if(authOnLeft){
//		// righttableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		// }else{
//		// righttableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		// }
//
//		PdfPTable middletTable = new PdfPTable(1);
//		middletTable.setWidthPercentage(100);
//		if (true) {
//			if (comppayment != null) {
//
//				PdfPTable bankDetailsTable = new PdfPTable(1);
//				bankDetailsTable.setWidthPercentage(100f);
//
//				String favourOf = "";
//				if (comppayment.getPaymentComName() != null
//						&& !comppayment.getPaymentComName().equals("")) {
//					favourOf = "Cheque should be in favour of '"
//							+ comppayment.getPaymentComName() + "'";
//				}
//				Phrase favouring = new Phrase(favourOf, font8bold);
//				PdfPCell favouringCell = new PdfPCell(favouring);
//				favouringCell.setBorder(0);
//				bankDetailsTable.addCell(favouringCell);
//
//				Phrase heading = new Phrase("Bank Details", font8bold);
//				PdfPCell headingCell = new PdfPCell(heading);
//				headingCell.setBorder(0);
//				bankDetailsTable.addCell(headingCell);
//
//				float[] columnWidths3 = { 1.5f, 0.35f, 4.5f };
//				PdfPTable bankDetails3Table = new PdfPTable(3);
//				bankDetails3Table.setWidthPercentage(100f);
//				try {
//					bankDetails3Table.setWidths(columnWidths3);
//				} catch (DocumentException e2) {
//					// TODO Auto-generated catch block
//					e2.printStackTrace();
//				}
//				Phrase bankNamePh = new Phrase("Name", font8bold);
//				PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
//				bankNamePhCell.setBorder(0);
//				bankDetails3Table.addCell(bankNamePhCell);
//
//				Phrase dot = new Phrase(":", font8bold);
//				PdfPCell dotCell = new PdfPCell(dot);
//				dotCell.setBorder(0);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankName = "";
//				if (comppayment.getPaymentBankName() != null
//						&& !comppayment.getPaymentBankName().equals("")) {
//					bankName = comppayment.getPaymentBankName();
//				}
//				Phrase headingValue = new Phrase(bankName, font8);
//				PdfPCell headingValueCell = new PdfPCell(headingValue);
//				headingValueCell.setBorder(0);
//				headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(headingValueCell);
//
//				// this is for branch
//				Phrase bankBranch = new Phrase("Branch", font8bold);
//				PdfPCell bankBranchCell = new PdfPCell(bankBranch);
//				bankBranchCell.setBorder(0);
//				bankDetails3Table.addCell(bankBranchCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankBranchValue = "";
//				if (comppayment.getPaymentBranch() != null
//						&& !comppayment.getPaymentBranch().equals("")) {
//					bankBranchValue = comppayment.getPaymentBranch();
//				}
//				Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
//				PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
//				bankBranchValuePhCell.setBorder(0);
//				bankBranchValuePhCell
//						.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankBranchValuePhCell);
//
//				Phrase bankAc = new Phrase("A/c No", font8bold);
//				PdfPCell bankAcCell = new PdfPCell(bankAc);
//				bankAcCell.setBorder(0);
//				bankDetails3Table.addCell(bankAcCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankAcNo = "";
//				if (comppayment.getPaymentAccountNo() != null
//						&& !comppayment.getPaymentAccountNo().equals("")) {
//					bankAcNo = comppayment.getPaymentAccountNo();
//				}
//				Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
//				PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
//				bankAcNoValueCell.setBorder(0);
//				bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankAcNoValueCell);
//
//				Phrase bankIFSC = new Phrase("IFS Code", font8bold);
//				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
//				bankIFSCCell.setBorder(0);
//				bankDetails3Table.addCell(bankIFSCCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankIFSCNo = "";
//				if (comppayment.getPaymentIFSCcode() != null
//						&& !comppayment.getPaymentIFSCcode().equals("")) {
//					bankIFSCNo = comppayment.getPaymentIFSCcode();
//				}
//				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
//				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
//				bankIFSCNoValueCell.setBorder(0);
//				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankIFSCNoValueCell);
//
//				PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
//				bankDetails3TableCell.setBorder(0);
//				bankDetailsTable.addCell(bankDetails3TableCell);
//
//				PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
//				bankDetailsTableCell.setBorder(0);
//				middletTable.addCell(bankDetailsTableCell);
//			}
//		}
//
//		PdfPCell middletTableCell = new PdfPCell(middletTable);
//
//		bottomTable.addCell(lefttableCell);
//		bottomTable.addCell(middletTableCell);
//		bottomTable.addCell(righttableCell);

		PdfPTable bottomTable = new PdfPTable(3);
		bottomTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			bottomTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);


		String gstin = "", gstinText = "";
		// rohan added this code for universal pest
		if (UniversalFlag) {
			if (con.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {
				if (!preprintStatus.equalsIgnoreCase("Plane")) {
					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

						if (comp.getArticleTypeDetails().get(i)
								.getArticlePrint().equalsIgnoreCase("Yes")) {

							Phrase articalType = new Phrase(comp
									.getArticleTypeDetails().get(i)
									.getArticleTypeName()
									+ " : "
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeValue(), font10bold);
							PdfPCell articalTypeCell = new PdfPCell();
							articalTypeCell.setBorder(0);
							articalTypeCell.addElement(articalType);
							leftTable.addCell(articalTypeCell);
						}
					}
				}
			}
		} else {
			
			if(numberRangeConfig!=null && !numberRangeConfig.isGstApplicable()){
				leftTable.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
			else{
				
			// if (!preprintStatus.equalsIgnoreCase("Plane")) {
			// leftTable.addCell(articalTypeCell);asa
			ServerAppUtility serverApp = new ServerAppUtility();
            
			//String gstin = "", gstinText = "";
			if (comp.getCompanyGSTType().trim()
					.equalsIgnoreCase("GST Applicable")) {
				logger.log(Level.SEVERE, "GST Applicable");
				gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
						.getBranch().trim());
				System.out.println("gstin" + gstin);
			} else {
				logger.log(Level.SEVERE, "GST Not Applicable");
				gstinText = comp.getCompanyGSTTypeText().trim();
				System.out.println("gstinText" + gstinText);
			}
			}
			
			
			
			/**
			 * Date : 8-2-2019
			 * Developer : Amol
			 * Description: if no taxes are selected then no gstin no will print
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "HideGSTINNumber", invoiceentity.getCompanyId())){
				hideGSTINNo=true;
			}
		
			if(hideGSTINNo){
				logger.log(Level.SEVERE, "INSIDE hideGSTINNo NUMBER FLAG");
			Phrase articalType2 = null;
			ServerAppUtility serverApp = new ServerAppUtility();
			System.out.println("billingtaxessize"+invoiceentity.getBillingTaxes().size());
				if(invoiceentity.getBillingTaxes().size()==0){
					
					articalType2 = new Phrase("");
					PdfPCell articalType2Cell = new PdfPCell(articalType2);
					articalType2Cell.setBorder(0);
					leftTable.addCell(articalType2Cell);
	
				}else{
					String gstinno = "";
//					ServerAppUtility serverApp = new ServerAppUtility();
					if (comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")) {
						logger.log(Level.SEVERE, "GST Applicable");
						gstinno = serverApp.getGSTINOfCompany(comp, invoiceentity.getBranch().trim());
						System.out.println("gstin" + gstinno);
						} else {
						logger.log(Level.SEVERE, "GST Not Applicable");
						gstinno = comp.getCompanyGSTTypeText().trim();
						System.out.println("gstinText" + gstinno);
					}
					articalType2 = new Phrase("GSTIN" + " : " + gstinno, font10bold);
					PdfPCell articalType2Cell = new PdfPCell(articalType2);
					articalType2Cell.setBorder(0);
					leftTable.addCell(articalType2Cell);
	
					
				}
				String stateCodeStr = serverApp.getStateOfCompany(comp,
						invoiceentity.getBranch().trim(), stateList);
				Phrase stateCode = new Phrase("State Code" + " : "
						+ stateCodeStr, font10bold);// Date 9/12/2017 By
													// Jayshree To increse the
													// fontsize by one

				PdfPCell stateCodeCell = new PdfPCell(stateCode);
				stateCodeCell.setBorder(0);
				leftTable.addCell(stateCodeCell);
			}
			
			logger.log(Level.SEVERE, "gstNumberPrintFlag "+gstNumberPrintFlag);
			logger.log(Level.SEVERE, "hideGSTINNo "+hideGSTINNo);
		/**
			 * Date 23-11-2018 By VIjay 
			 * for GST Number not print with process Config 
			 */
			
			if(gstNumberPrintFlag&&!hideGSTINNo){
				//String gstin="";  String gstinText="";
				logger.log(Level.SEVERE, "INSIDE EnableDoNotPrintGSTNumber FLAG");
				Phrase articalType2 = null;
			if (!gstin.trim().equals("")) {
				logger.log(Level.SEVERE, "GST Present");
				articalType2 = new Phrase("GSTIN" + " : " + gstin, font10bold);
			} else if (!gstinText.trim().equalsIgnoreCase("")) {
				logger.log(Level.SEVERE, "GST Not Present");
				articalType2 = new Phrase(gstinText, font10bold);
			} else {
				logger.log(Level.SEVERE, "Nothing Present");
				articalType2 = new Phrase("", font10bold);
			}
//		
				
			    	if (!gstin.equals("")) {
			    		ServerAppUtility serverApp = new ServerAppUtility();
					PdfPCell articalType2Cell = new PdfPCell(articalType2);
					articalType2Cell.setBorder(0);
					leftTable.addCell(articalType2Cell);
	
					String stateCodeStr = serverApp.getStateOfCompany(comp,
							invoiceentity.getBranch().trim(), stateList);
					Phrase stateCode = new Phrase("State Code" + " : "
							+ stateCodeStr, font10bold);// Date 9/12/2017 By
														// Jayshree To increse the
														// fontsize by one
	
					PdfPCell stateCodeCell = new PdfPCell(stateCode);
					stateCodeCell.setBorder(0);
					leftTable.addCell(stateCodeCell);
				} 
//				else {
//					PdfPCell articalType2Cell = new PdfPCell(articalType2);
//					articalType2Cell.setBorder(0);
//					leftTable.addCell(articalType2Cell);
//				}

			}
			
	  }
			/**
			 * ends here
			 */

			PdfPTable articleTab = new PdfPTable(3);
			articleTab.setWidthPercentage(100);

			try {
				articleTab.setWidths(new float[] { 30, 5, 65 });
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(numberRangeConfig!=null && !numberRangeConfig.isGstApplicable()){
				articleTab.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				articleTab.addCell(pdfUtility.getCell(":", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				articleTab.addCell(pdfUtility.getCell("", font10bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
			else{
				
			/**
			 * @author Abhinav Bihade
			 * @since 08/02/2020
			 * As per Vaishnavi Pawar's Requirement for ISPC - Need article information section in branch screen,
			 * in case if somebody manages 2 companies under same link of ERP s/w
			 */
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())&&branchWiseFilteredArticleList!=null && branchWiseFilteredArticleList.size()!=0 ){	
				logger.log(Level.SEVERE,"Inside Branch As Company Printing1:" +branchWiseFilteredArticleList);
				for(ArticleType artict :branchWiseFilteredArticleList){
					Phrase typename;
					Phrase typevalue;
					if (artict.getArticlePrint()
							.equalsIgnoreCase("YES")
							&& (artict
									.getDocumentName().equals("Invoice Details") || artict
									.getDocumentName().equals("ServiceInvoice"))) {
						

						typename = new Phrase(artict.getArticleTypeName(), font10bold);
						typevalue = new Phrase(artict
								.getArticleTypeValue(), font10bold);
						logger.log(Level.SEVERE,"Inside Branch As Company Printing11111:" +typevalue);
						logger.log(Level.SEVERE,"Inside Branch As Company Printing22222:" +typename);

						PdfPCell tymanecell = new PdfPCell();
						tymanecell.addElement(typename);
						tymanecell.setBorder(0);
						tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

						Phrase typeblank = new Phrase(":", font10bold);
						PdfPCell typeCell = new PdfPCell(typeblank);
						typeCell.addElement(typeblank);
						typeCell.setBorder(0);
						typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

						PdfPCell typevalcell = new PdfPCell();
						typevalcell.addElement(typevalue);
						typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						typevalcell.setBorder(0);
						/**
						 * Date 22-11-2018 By Vijay 
						 * Des :- if process configuration is active then GST Number will not display
						 * and GST Number will display if GST applicable or not applicable as per nitin sir
						 */
						
						//if(gstNumberPrintFlag || !artict.getArticleTypeName().equals("GSTIN") ){
							articleTab.addCell(tymanecell);
							articleTab.addCell(typeCell);
							articleTab.addCell(typevalcell);
							logger.log(Level.SEVERE,"Inside Branch As Company Printing3333:" +tymanecell);
							logger.log(Level.SEVERE,"Inside Branch As Company Printing4444:" +typeCell);
							logger.log(Level.SEVERE,"Inside Branch As Company Printing5555:" +typevalcell);
							
							
						//}
					}
				}
				
			}
			
			}
			else{
				
				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					logger.log(Level.SEVERE,"Inside Branch As Company Printing2:");


					// if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
					// .trim().equalsIgnoreCase("GSTIN")) {
					// Phrase articalType = new Phrase(comp
					// .getArticleTypeDetails().get(i)
					// .getArticleTypeName()
					// + " : "
					// + comp.getArticleTypeDetails().get(i)
					// .getArticleTypeValue(), font10bold);
					// PdfPCell articalTypeCell = new PdfPCell(articalType);
					// articalTypeCell.setBorder(0);
					// leftTable.addCell(articalTypeCell);
					//
					// }

					/**
					 * Date 13/12/2017 By Jayshree add the this
					 */

					Phrase typename;
					Phrase typevalue;
					if (comp.getArticleTypeDetails().get(i).getArticlePrint()
							.equalsIgnoreCase("YES")
							&& (comp.getArticleTypeDetails().get(i)
									.getDocumentName().equals("Invoice Details") || comp
									.getArticleTypeDetails().get(i)
									.getDocumentName().equals("ServiceInvoice"))) {

						typename = new Phrase(comp.getArticleTypeDetails().get(i)
								.getArticleTypeName(), font10bold);
						typevalue = new Phrase(comp.getArticleTypeDetails().get(i)
								.getArticleTypeValue(), font10bold);

						PdfPCell tymanecell = new PdfPCell();
						tymanecell.addElement(typename);
						tymanecell.setBorder(0);
						tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

						Phrase typeblank = new Phrase(":", font10bold);
						PdfPCell typeCell = new PdfPCell(typeblank);
						typeCell.addElement(typeblank);
						typeCell.setBorder(0);
						typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

						PdfPCell typevalcell = new PdfPCell();
						typevalcell.addElement(typevalue);
						typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						typevalcell.setBorder(0);
						/**
						 * Date 22-11-2018 By Vijay 
						 * Des :- if process configuration is active then GST Number will not display
						 * and GST Number will display if GST applicable or not applicable as per nitin sir
						 */
						
						if(gstNumberPrintFlag || !comp.getArticleTypeDetails().get(i).getArticleTypeName().equals("GSTIN") ){
							articleTab.addCell(tymanecell);
							articleTab.addCell(typeCell);
							articleTab.addCell(typevalcell);
						}
					}
				}

			}

		}
			PdfPCell articleCell = new PdfPCell(articleTab);
			articleCell.setBorder(0);
			leftTable.addCell(articleCell);
		
		
	
	PdfPCell leftCell = new PdfPCell();
	leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font10bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		logger.log(Level.SEVERE, "Branchas company "+branchAsCompany);
		String companyname = "";
		if (multipleCompanyName) {
			if (con.getGroup() != null && !con.getGroup().equals("")) {
				companyname = con.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}

		}
		/**
		 * @author Abhinav Bihade
		 * @since 27/12/2019
		 * For Bitco by Rahul Tiwari, "Correspondence Name" should print	 
		 */
		else if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname= branchDt.getCorrespondenceName();
			logger.log(Level.SEVERE,"3rd:");
		}
		else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		if(invoiceGroupAsSignatory){
			companyname=invoiceentity.getInvoiceGroup();
			logger.log(Level.SEVERE,"invoiceentity.getInvoiceGroup()()"+invoiceentity.getInvoiceGroup());
		}
		// ends here

		// rightTable.addCell(rightUpperCell);
		Phrase companyPhrase = new Phrase("For " + companyname, font10bold);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setBorder(0);
		if (authOnLeft) {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		rightTable.addCell(companyParaCell);

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			if (authOnLeft) {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Image image1=Image.getInstance("images/digisign2copy.png");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(100f);
		// imageCell=new PdfPCell(image1);
		// imageCell.setBorder(0);
		// if (authOnLeft) {
		// imageCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// } else {
		// imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// }
		// }
		// catch(Exception e)
		// {
		// e.printStackTrDayace();
		// }
		if (imageSignCell != null) {
			rightTable.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);

		}
		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		// Paragraph signPara = new Paragraph();
		// signPara.add(signAuth);
		// signPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell signParaCell = new PdfPCell(signAuth);
		// signParaCell.addElement();

		signParaCell.setBorder(0);
		if (authOnLeft) {
			signParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell(leftTable);
		// lefttableCell.addElement();
		PdfPCell righttableCell = new PdfPCell(rightTable);
		// if(authOnLeft){
		// righttableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// }else{
		// righttableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// }
        /*Added by Sheetal:30-11-2021*/
		PdfPTable middletTable = new PdfPTable(1);
		middletTable.setWidthPercentage(100);
		if (pdfUtility.printBankDetailsFlag) {
			if (comppayment != null) {

//				PdfPTable bankDetailsTable = new PdfPTable(1);
//				bankDetailsTable.setWidthPercentage(100f);
//
//				/*
//				 * nidhi
//				 * 9-04-2018
//				 * favour of company payment will print instand of company name
//				 * 
//				 * String favourOf = "";
//				if (comppayment.getPaymentComName() != null
//						&& !comppayment.getPaymentComName().equals("")) {
//					favourOf = "Cheque should be in favour of '"
//							+ comppayment.getPaymentComName() + "'";
//				}*/
//				
//				
//				String favourOf = "";
//				if (comppayment.getPaymentFavouring() != null
//						&& !comppayment.getPaymentFavouring().equals("")) {
//					favourOf = "Cheque should be in favour of '"
//							+ comppayment.getPaymentFavouring() + "'";
//				}
//				Phrase favouring = new Phrase(favourOf, font8bold);
//				PdfPCell favouringCell = new PdfPCell(favouring);
//				favouringCell.setBorder(0);
//				bankDetailsTable.addCell(favouringCell);
//
//				Phrase heading = new Phrase("Bank Details", font8bold);
//				PdfPCell headingCell = new PdfPCell(heading);
//				headingCell.setBorder(0);
//				bankDetailsTable.addCell(headingCell);
//
//				float[] columnWidths3 = { 1.7f, 0.35f, 4.5f };
//				PdfPTable bankDetails3Table = new PdfPTable(3);
//				bankDetails3Table.setWidthPercentage(100f);
//				try {
//					bankDetails3Table.setWidths(columnWidths3);
//				} catch (DocumentException e2) {
//					// TODO Auto-generated catch block
//					e2.printStackTrace();
//				}
//				Phrase bankNamePh = new Phrase("Name", font8bold);
//				PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
//				bankNamePhCell.setBorder(0);
//				bankDetails3Table.addCell(bankNamePhCell);
//
//				Phrase dot = new Phrase(":", font8bold);
//				PdfPCell dotCell = new PdfPCell(dot);
//				dotCell.setBorder(0);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankName = "";
//				if (comppayment.getPaymentBankName() != null
//						&& !comppayment.getPaymentBankName().equals("")) {
//					bankName = comppayment.getPaymentBankName();
//				}
//				Phrase headingValue = new Phrase(bankName, font8);
//				PdfPCell headingValueCell = new PdfPCell(headingValue);
//				headingValueCell.setBorder(0);
//				headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(headingValueCell);
//
//				// this is for branch
//				Phrase bankBranch = new Phrase("Branch", font8bold);
//				PdfPCell bankBranchCell = new PdfPCell(bankBranch);
//				bankBranchCell.setBorder(0);
//				bankDetails3Table.addCell(bankBranchCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankBranchValue = "";
//				if (comppayment.getPaymentBranch() != null
//						&& !comppayment.getPaymentBranch().equals("")) {
//					bankBranchValue = comppayment.getPaymentBranch();
//				}
//				Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
//				PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
//				bankBranchValuePhCell.setBorder(0);
//				bankBranchValuePhCell
//						.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankBranchValuePhCell);
//
//				Phrase bankAc = new Phrase("A/c No", font8bold);
//				PdfPCell bankAcCell = new PdfPCell(bankAc);
//				bankAcCell.setBorder(0);
//				bankDetails3Table.addCell(bankAcCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankAcNo = "";
//				if (comppayment.getPaymentAccountNo() != null
//						&& !comppayment.getPaymentAccountNo().equals("")) {
//					bankAcNo = comppayment.getPaymentAccountNo();
//				}
//				Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
//				PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
//				bankAcNoValueCell.setBorder(0);
//				bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankAcNoValueCell);
//
//				Phrase bankIFSC = new Phrase("IFSC Code", font8bold);
//				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
//				bankIFSCCell.setBorder(0);
//				bankDetails3Table.addCell(bankIFSCCell);
//				bankDetails3Table.addCell(dotCell);
//
//				String bankIFSCNo = "";
//				if (comppayment.getPaymentIFSCcode() != null
//						&& !comppayment.getPaymentIFSCcode().equals("")) {
//					bankIFSCNo = comppayment.getPaymentIFSCcode();
//				}
//				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
//				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
//				bankIFSCNoValueCell.setBorder(0);
//				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				bankDetails3Table.addCell(bankIFSCNoValueCell);
//
//				PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
//				bankDetails3TableCell.setBorder(0);
//				bankDetailsTable.addCell(bankDetails3TableCell);
//
//				PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
//				bankDetailsTableCell.setBorder(0);
//				middletTable.addCell(bankDetailsTableCell);
				
			
		    PdfUtility BankDetails = new PdfUtility();
			middletTable.addCell(BankDetails.getBankDetails(comppayment));
			
			}
		}

		PdfPCell middletTableCell = new PdfPCell(middletTable);

		bottomTable.addCell(lefttableCell);
		if(con.getNumberRange()!=null &&!con.getNumberRange().equals("")){
		if(nonbillingInvoice==true && con.getNumberRange().equalsIgnoreCase("NonBilling")){
			
			Phrase blankphrase=new Phrase(" ");
			PdfPCell blankphrCell=new PdfPCell(blankphrase);
			blankphrCell.setFixedHeight(80);
			bottomTable.addCell(blankphrCell);
			bottomTable.addCell(blankphrCell);
			System.out.println("nonbillingInvoice"+nonbillingInvoice);
		
			
		}
		else{
			bottomTable.addCell(middletTableCell);
			bottomTable.addCell(righttableCell);
		}
		}
		else{
			bottomTable.addCell(middletTableCell);
			bottomTable.addCell(righttableCell);
			
		}
		
		//

		Paragraph para = new Paragraph(
				"Note : This is computer generated invoice therefore no physical signature is required.",
				font8);

		try {
			document.add(bottomTable);
			// if(comp.getUploadDigitalSign()!=null){
			if (imageSignCell != null) {
				document.add(para);
			}
			// }
			if (noOfLines == 0 && prouductCount != 0) {
				createAnnexureForRemainingProduct(prouductCount);
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createAnnexureForRemainingProduct(int count) {

		Paragraph para = new Paragraph("Annexure 1 :", font10bold);
		para.setAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(Chunk.NEWLINE);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (con.isContractRate()) {
			createProductDetailsForRate();
		} else {
			createProductDetails();
		}
		if (con.isContractRate()) {
			createProductDetailsMOreThanFiveForRate(prouductCount);
		} else {
			createProductDetailsMOreThanFive(prouductCount);
		}

	}

	private void createProductDetailsMOreThanFiveForRate(int count) {

		for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			PdfPTable productTable = new PdfPTable(8);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column8ProdCollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// 1
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);
			// 2
			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(),
					nameAddressFont6);//font10 Ashwini Patil
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.addElement();
			productTable.addCell(serviceNameCell);
			// 3
			Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getHsnCode().trim().equals("")) {

				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font10);
			} else {
				ServiceProduct serviceProduct = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count",
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdId()).first().now();
				if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
					hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
				} else {
					hsnCode = new Phrase("", font6);
				}
			}
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			// hsnCodeCell.addElement();
			productTable.addCell(hsnCodeCell);

			String startDateStr = "", endDateStr = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getPrduct().getCount());
				System.out
						.println("con.getItems().get(j).getPrduct().getCount()"
								+ con.getItems().get(j).getPrduct().getCount());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out.println("con.getItems().get(j).getDuration()"
						+ con.getItems().get(j).getDuration());

				SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
						"dd/MM/yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
				if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
						.getCount() == con.getItems().get(j).getPrduct()
						.getCount())
						&& (invoiceentity.getSalesOrderProducts().get(i)
								.getOrderDuration() == con.getItems().get(j)
								.getDuration())) {
					if (con.getItems().get(j).getEndDate() != null) {
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(con.getItems().get(j)
								.getEndDate());
					} else {
						Calendar c = Calendar.getInstance();
						c.setTime(con.getItems().get(j).getStartDate());
						c.add(Calendar.DATE, con.getItems().get(j)
								.getDuration());
						Date endDt = c.getTime();
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(endDt);
					}
				}
			}
			Phrase startDate_endDate = new Phrase(startDateStr + " - "
					+ endDateStr, font6);

			PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
			startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// productTable.addCell(startDate_endDateCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getArea()
					+ "", font10);
			PdfPCell qtyCell = new PdfPCell(qty);
			// qtyCell.addElement();
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(qtyCell);

			Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getUnitOfMeasurement().trim(), font10);
			PdfPCell uomCell = new PdfPCell(uom);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(uomCell);

			Phrase rate = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getPrice())
					+ "", font6);
			PdfPCell rateCell = new PdfPCell(rate);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			Phrase blank = new Phrase(" ", font6);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			if (consolidatePrice) {
				if (i == count) {
					blankCell.setBorderWidthBottom(0);
					productTable.addCell(blankCell);
				} else {
					blankCell.setBorderWidthTop(0);
					productTable.addCell(blankCell);
				}
			} else {
				productTable.addCell(rateCell);
			}
			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);
			Phrase disc = null;
			if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()>0) {
				disc = new Phrase(decimalformat.format(invoiceentity
						.getSalesOrderProducts().get(i).getFlatDiscount())
						+ "", font6);
			}
			else {
				disc = new Phrase("0", font6);
			}
//			Phrase disc = new Phrase(df.format(invoiceentity
//					.getSalesOrderProducts().get(i).getFlatDiscount())
//					+ "", font6);
			PdfPCell discCell = new PdfPCell(disc);
			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			if (consolidatePrice) {
				if (i == count) {
					blankCell.setBorderWidthBottom(0);
					productTable.addCell(blankCell);
				} else {
					blankCell.setBorderWidthTop(0);
					productTable.addCell(blankCell);
				}
			} else {
				productTable.addCell(discCell);
			}

			Phrase taxableValue;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBaseBillingAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			// totalAssAmount=totalAssAmount+taxValue;
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			if (consolidatePrice) {
				if (i == count) {
					blankCell.setBorderWidthBottom(0);
					productTable.addCell(blankCell);
				} else {
					blankCell.setBorderWidthTop(0);
					productTable.addCell(blankCell);
				}
			} else {
				productTable.addCell(taxableValueCell);
			}

			PdfPCell cellIGST;
			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount()  && invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
					premisesVal = con.getItems().get(j).getPremisesDetails();
				}

			}
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					Phrase blankValPhrs = new Phrase("", font8);
					PdfPCell premiseCellBlank = new PdfPCell(blankValPhrs);
					premiseCellBlank.setColspan(1);

					productTable.addCell(premiseCellBlank);

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font10);
					PdfPCell premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					// premiseCell.addElement();
					productTable.addCell(premiseCell);
				}
			}
			try {
				document.add(productTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage() != 0
			// && invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getPercentage() != 0) {
			// logger.log(Level.SEVERE, "Inside NON Zero:::::");
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getTaxPrintName()
			// .equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else {
			//
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("CGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// // igstRateValCell.setBorder(0);
			// // igstRateValCell.setBorderWidthBottom(0);
			// // igstRateValCell.setBorderWidthTop(0);
			// // igstRateValCell.setBorderWidthRight(0);
			// // igstRateValCell.addElement(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("SGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			//
			// productTable.addCell(totalCell);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
			// }
			// } else {
			// logger.log(Level.SEVERE, "Inside Zero else:::::");
			//
			// PdfPCell cell = new PdfPCell(new Phrase("NA", font10));
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// Phrase totalPhrase = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getTotalAmount())
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			//
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
		}
	}

	private void createProductDetailsMOreThanFive(int count) {

		for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			PdfPTable productTable = new PdfPTable(8);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column8ProdCollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// 1
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);
			// 2
			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(),
					nameAddressFont6);//font10 Ashwini Patil
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.addElement();
			productTable.addCell(serviceNameCell);
			// 3
			Phrase hsnCode = null;
			if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getHsnCode().trim().equals("")) {

				hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
						.get(i).getHsnCode().trim(), font10);
				// if(hsC)
			} else {
				ServiceProduct serviceProduct = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("companyId", comp.getCompanyId())
						.filter("count",
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdId()).first().now();
				if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
					hsnCode = new Phrase(serviceProduct.getHsnNumber(), font6);
				} else {
					hsnCode = new Phrase("", font6);
				}
			}
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			// hsnCodeCell.addElement();
			productTable.addCell(hsnCodeCell);

			String startDateStr = "", endDateStr = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getPrduct().getCount());
				System.out
						.println("con.getItems().get(j).getPrduct().getCount()"
								+ con.getItems().get(j).getPrduct().getCount());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out
						.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
								+ invoiceentity.getSalesOrderProducts().get(i)
										.getOrderDuration());
				System.out.println("con.getItems().get(j).getDuration()"
						+ con.getItems().get(j).getDuration());

				SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
						"dd/MM/yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
				if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
						.getCount() == con.getItems().get(j).getPrduct()
						.getCount())
						&& (invoiceentity.getSalesOrderProducts().get(i)
								.getOrderDuration() == con.getItems().get(j)
								.getDuration())) {
					if (con.getItems().get(j).getEndDate() != null) {
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(con.getItems().get(j)
								.getEndDate());
					} else {
						Calendar c = Calendar.getInstance();
						c.setTime(con.getItems().get(j).getStartDate());
						c.add(Calendar.DATE, con.getItems().get(j)
								.getDuration());
						Date endDt = c.getTime();
						startDateStr = simpleDateFmt.format(con.getItems()
								.get(j).getStartDate());
						endDateStr = simpleDateFmt.format(endDt);
					}
				}
			}
			Phrase startDate_endDate = new Phrase(startDateStr + " - "
					+ endDateStr, font6);

			PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
			startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			productTable.addCell(startDate_endDateCell);
			// Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
			// .get(i).getUnitOfMeasurement().trim(), font10);
			// PdfPCell uomCell = new PdfPCell(uom);
			// uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(uomCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getQuantity()
					+ "", font10);
			PdfPCell qtyCell = new PdfPCell(qty);
			// qtyCell.addElement();
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getPrice())
					+ "", font6);
			PdfPCell rateCell = new PdfPCell(rate);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(rateCell);

			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);

//			Phrase disc = new Phrase(df.format(invoiceentity
//					.getSalesOrderProducts().get(i).getFlatDiscount())
//					+ "", font6);
			
			Phrase disc = null;
			if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()>0) {
				disc = new Phrase(decimalformat.format(invoiceentity
						.getSalesOrderProducts().get(i).getFlatDiscount())
						+ "", font6);
			}
			else {
				disc = new Phrase("0", font6);
			}
			PdfPCell discCell = new PdfPCell(disc);
			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(discCell);

			Phrase taxableValue;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBaseBillingAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			// totalAssAmount=totalAssAmount+taxValue;
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;
			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount()  && invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
					premisesVal = con.getItems().get(j).getPremisesDetails();
				}

			}
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font10);
					PdfPCell premiseCell = new PdfPCell();
					premiseCell.setColspan(7);
					premiseCell.addElement(premisesValPhrs);
					productTable.addCell(premiseCell);
				}
			}
			try {
				document.add(productTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage() != 0
			// && invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getPercentage() != 0) {
			// logger.log(Level.SEVERE, "Inside NON Zero:::::");
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getTaxPrintName()
			// .equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else {
			//
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("CGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// // igstRateValCell.setBorder(0);
			// // igstRateValCell.setBorderWidthBottom(0);
			// // igstRateValCell.setBorderWidthTop(0);
			// // igstRateValCell.setBorderWidthRight(0);
			// // igstRateValCell.addElement(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("SGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			//
			// productTable.addCell(totalCell);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
			// }
			// } else {
			// logger.log(Level.SEVERE, "Inside Zero else:::::");
			//
			// PdfPCell cell = new PdfPCell(new Phrase("NA", font10));
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// Phrase totalPhrase = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getTotalAmount())
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			//
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
		}
	}

	private void createFooterTaxPart() {
		// TODO Auto-generated method stub
		PdfPTable pdfPTaxTable = new PdfPTable(3);
		pdfPTaxTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			pdfPTaxTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

		float[] column3widths = { 2f, 2f, 6f };
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// heading

		Phrase day = new Phrase("Day", font8bold);
		PdfPCell dayCell = new PdfPCell(day);
		dayCell.setBorder(0);

		Phrase percent = new Phrase("Percent", font8bold);
		PdfPCell percentCell = new PdfPCell(percent);
		percentCell.setBorder(0);

		Phrase comment = new Phrase("Comment", font8bold);
		PdfPCell commentCell = new PdfPCell(comment);
		commentCell.setBorder(0);

		// Phrase terms = new Phrase("Payment Terms", font8bold);
		// PdfPCell termsCell = new PdfPCell(terms);
		//
		// Phrase termsBlank = new Phrase(" ", font10bold);
		// PdfPCell termsBlankCell = new PdfPCell(termsBlank);
		// termsBlankCell.setBorder(0);
		// leftTable.addCell(termsCell);
		// leftTable.addCell(termsBlankCell);
		// leftTable.addCell(termsBlankCell);
		leftTable.addCell(dayCell);
		leftTable.addCell(percentCell);
		leftTable.addCell(commentCell);

		// Values
		for (int i = 0; i < invoiceentity.getArrPayTerms().size(); i++) {
			Phrase dayValue = new Phrase(invoiceentity.getArrPayTerms().get(i)
					.getPayTermDays()
					+ "", font8);
			PdfPCell dayValueCell = new PdfPCell(dayValue);
			dayValueCell.setBorder(0);
			leftTable.addCell(dayValueCell);

			Phrase percentValue = new Phrase(df.format(invoiceentity
					.getArrPayTerms().get(i).getPayTermPercent()), font8);
			PdfPCell percentValueCell = new PdfPCell(percentValue);
			percentValueCell.setBorder(0);
			leftTable.addCell(percentValueCell);

			Phrase commentValue = new Phrase(invoiceentity.getArrPayTerms()
					.get(i).getPayTermComment(), font8);
			PdfPCell commentValueCell = new PdfPCell(commentValue);
			commentValueCell.setBorder(0);
			leftTable.addCell(commentValueCell);
		}

		// try {
		// document.add(leftTable);
		// } catch (DocumentException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax",
				font10bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);

		Phrase amtB4TaxValphrase = new Phrase(totalAmount + "", font10bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {
			if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();
				Phrase IGSTphrase = new Phrase("IGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorder(0);

				Phrase IGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(IGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(IGSTValphraseCell);

			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase SGSTphrase = new Phrase("SGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorder(0);
				// SGSTphraseCell.addElement(SGSTphrase);

				Phrase SGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(SGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(SGSTValphraseCell);
			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase CGSTphrase = new Phrase("CGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				// CGSTphraseCell.addElement(CGSTphrase);

				Phrase CGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				rightInnerTable.addCell(CGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(CGSTValphraseCell);
			}
		}

		Phrase GSTphrase = new Phrase("Total GST", font10bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
				font10bold);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
		// innerRightCell.addElement();

		rightTable.addCell(innerRightCell);

		PdfPTable middleTable = new PdfPTable(1);
		middleTable.setWidthPercentage(100);

		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");
		// PdfPTable otherChargesTable = new PdfPTable(1);
		// otherChargesTable.setWidthPercentage(100);
		// try {
		// otherChargesTable.setWidths(columnMoreLeftWidths);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// Phrase blank=new Phrase("",font10bold);
		// PdfPCell blankCell=new PdfPCell(blank);
		// otherChargesTable.addCell(blankCell);

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HashSet<String> chargeNameSet = new HashSet<String>();
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			chargeNameSet.add(invoiceentity.getOtherCharges().get(i)
					.getOtherChargeName());
		}
		ArrayList<String> chargeNameList = new ArrayList<String>(chargeNameSet);

		for (int k = 0; k < chargeNameList.size(); k++) {
			Phrase chargeName, taxes, assVal;
			PdfPCell pCell;
			if (k == 0) {
				chargeName = new Phrase("Charge Name", font10bold);
				taxes = new Phrase("Taxes", font10bold);
				assVal = new Phrase("Amt", font10bold);
				pCell = new PdfPCell(chargeName);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
				pCell = new PdfPCell(taxes);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
				pCell = new PdfPCell(assVal);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
			}
			double otherAmount = 0;
			for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
				if (chargeNameList
						.get(k)
						.trim()
						.equalsIgnoreCase(
								invoiceentity.getOtherCharges().get(i)
										.getOtherChargeName().trim())) {
					chargeName = new Phrase(invoiceentity.getOtherCharges()
							.get(i).getOtherChargeName(), font10);
					String taxNames = " ";
					if (invoiceentity.getOtherCharges().get(i).getTax1()
							.getPercentage() != 0
							&& invoiceentity.getOtherCharges().get(i).getTax2()
									.getPercentage() != 0) {
						taxNames = invoiceentity.getOtherCharges().get(i)
								.getTax1().getTaxConfigName()
								+ "/"
								+ invoiceentity.getOtherCharges().get(i)
										.getTax2().getTaxConfigName();
					} else {
						if (invoiceentity.getOtherCharges().get(i).getTax1()
								.getPercentage() != 0) {
							taxNames = invoiceentity.getOtherCharges().get(i)
									.getTax1().getTaxConfigName();
						} else if (invoiceentity.getOtherCharges().get(i)
								.getTax2().getPercentage() != 0) {
							taxNames = invoiceentity.getOtherCharges().get(i)
									.getTax2().getTaxConfigName();
						} else {
							taxNames = " ";
						}
					}
					taxes = new Phrase(taxNames /*
												 * invoiceentity.
												 * getOtherCharges()
												 * .get(i).get()
												 */, font10);
					otherAmount = otherAmount
							+ invoiceentity.getOtherCharges().get(i)
									.getAmount();

					if ((i + 1) == invoiceentity.getOtherCharges().size()) {
						pCell = new PdfPCell(chargeName);
						otherCharges.addCell(pCell);
						pCell = new PdfPCell(taxes);
						otherCharges.addCell(pCell);
						assVal = new Phrase(otherAmount + "", font10);
						pCell = new PdfPCell(assVal);
						otherCharges.addCell(pCell);
					}
				}
			}
		}
		// PdfPCell left2Cell=new PdfPCell(otherCharges);
		// otherChargesTable.addCell();

		PdfPCell left22Cell = new PdfPCell(otherCharges);
		middleTable.addCell(left22Cell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);
		// rightCell.addElement();
		PdfPCell middleCell = new PdfPCell(middleTable);

		PdfPCell leftCell = new PdfPCell(leftTable);
		// leftCell.setBorder(0);
		// leftCell.addElement();

		pdfPTaxTable.addCell(leftCell);
		if (invoiceentity.getOtherCharges().size() > 0) {
			pdfPTaxTable.addCell(middleCell);
		} else {
			Phrase blankPhrase = new Phrase(" ", font10);
			middleCell = new PdfPCell(blankPhrase);
			pdfPTaxTable.addCell(middleCell);
		}
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createProductDetailsVal(HashSet<String> productCode) {

		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			rateAmountProd = rateAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getBaseBillingAmount();

			amountAmountProd = amountAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmountProd = discAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmountProd = totalAssAmountProd + taxValue;
		}

		logger.log(Level.SEVERE,"rateAmountProd"+rateAmountProd);
		logger.log(Level.SEVERE,"amountAmountProd"+amountAmountProd);
		logger.log(Level.SEVERE,"discAmountProd"+discAmountProd);
		logger.log(Level.SEVERE,"totalAssAmountProd"+totalAssAmountProd);
		
		float blankLines = 0;

		/**
		 * @author Vijay Date 20-11-2020 
		 * Des :- As per Rahul and Nitin sir instruction if Quatity exist in invoice product table then
		 * quantity column will display in product table
		 */
		PdfPTable productTable;
		if(qtycolumnFlag) {
			 productTable = new PdfPTable(9);
		}
		else {
			 productTable = new PdfPTable(8);
		}
//		PdfPTable productTable = new PdfPTable(8);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column8ProdCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		int countToBeDeducted = 0;
		ArrayList<String> productCodeList = new ArrayList<String>(productCode);
		for (int i = 0; i < productCodeList.size(); i++) {
			logger.log(Level.SEVERE,"productCodeList" + productCodeList.get(i));
		}
		
		for (int k = 0; k < productCodeList.size(); k++) {
			double totalAmountOfProductCode = 0;
			int noOfservices = 0;
			PdfPCell srNoCell = null;
			PdfPCell serviceNameCell = null;
			PdfPCell servicesCell = null;
			PdfPCell hsnCodeCell = null;
			PdfPCell qtyCell = null;
			PdfPCell rateCell = null;
			PdfPCell startDate_endDateCell = null;
			PdfPCell discCell = null;
			PdfPCell taxableValueCell = null;
			PdfPCell premiseblankCell = null;
			PdfPCell premiseCell = null;
			noOfLines = noOfLines - 1;

			PdfPCell quantitycell = null;

			for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
				if (productCodeList
						.get(k)
						.trim()
						.equalsIgnoreCase(
								invoiceentity.getSalesOrderProducts().get(i)
										.getProdCode())) {
					if (noOfLines == 0) {
						prouductCount = i;
						break;
					}
					countToBeDeducted = countToBeDeducted + 1;

					int srNoVal = k + 1;
					Phrase srNo = new Phrase(srNoVal + "", font6);
					srNoCell = new PdfPCell(srNo);
					srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					// productTable.addCell(srNoCell);

					logger.log(Level.SEVERE,"getProdName().trim().length()"
							+ invoiceentity.getSalesOrderProducts().get(i)
									.getProdName().trim().length());
					if (invoiceentity.getSalesOrderProducts().get(i)
							.getProdName().trim().length() > 42) {
						noOfLines = noOfLines - 1;
					}
					Phrase serviceName = new Phrase(invoiceentity
							.getSalesOrderProducts().get(i).getProdName()
							.trim(), nameAddressFont6);//font6 Ashwini Patil
					serviceNameCell = new PdfPCell(serviceName);

					// noOfservices=noOfservices+invoiceentity
					// .getSalesOrderProducts().get(i).getOrderDuration();
					noOfservices = noOfservices + 1;

					Phrase hsnCode = null;
					if (invoiceentity.getSalesOrderProducts().get(i)
							.getHsnCode() != null
							&& !invoiceentity.getSalesOrderProducts().get(i)
									.getHsnCode().equals("")) {
						hsnCode = new Phrase(invoiceentity
								.getSalesOrderProducts().get(i).getHsnCode()
								.trim(), font6);
					} else {
						ServiceProduct serviceProduct = ofy()
								.load()
								.type(ServiceProduct.class)
								.filter("companyId", comp.getCompanyId())
								.filter("count",
										invoiceentity.getSalesOrderProducts()
												.get(i).getProdId()).first()
								.now();
						if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
							hsnCode = new Phrase(serviceProduct.getHsnNumber(),
									font6);
						} else {
							hsnCode = new Phrase("", font6);
						}
					}

					hsnCodeCell = new PdfPCell(hsnCode);
					hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

					String startDateStr = "", endDateStr = "";
					for (int j = 0; j < con.getItems().size(); j++) {
						System.out.println("invoiceentity.getSalesOrderProducts().get(i).getPrduct().getCount()"
										+ invoiceentity.getSalesOrderProducts()
												.get(i).getPrduct().getCount());
						System.out.println("con.getItems().get(j).getPrduct().getCount()"
										+ con.getItems().get(j).getPrduct()
												.getCount());
						System.out.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
										+ invoiceentity.getSalesOrderProducts()
												.get(i).getOrderDuration());
						System.out.println("invoiceentity.getSalesOrderProducts().get(i).getOrderDuration()"
										+ invoiceentity.getSalesOrderProducts()
												.get(i).getOrderDuration());
						System.out.println("con.getItems().get(j).getDuration()"
										+ con.getItems().get(j).getDuration());

						SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
								"dd/MM/yyyy");
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));
						simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
						if ((invoiceentity.getSalesOrderProducts().get(i)
								.getPrduct().getCount() == con.getItems()
								.get(j).getPrduct().getCount())
								&& (invoiceentity.getSalesOrderProducts()
										.get(i).getOrderDuration() == con
										.getItems().get(j).getDuration())) {
							if (con.getItems().get(j).getEndDate() != null) {
								startDateStr = simpleDateFmt.format(con
										.getItems().get(j).getStartDate());
								endDateStr = simpleDateFmt.format(con
										.getItems().get(j).getEndDate());
							} else {
								Calendar c = Calendar.getInstance();
								c.setTime(con.getItems().get(j).getStartDate());
								c.add(Calendar.DATE, con.getItems().get(j)
										.getDuration());
								Date endDt = c.getTime();
								startDateStr = simpleDateFmt.format(con
										.getItems().get(j).getStartDate());
								endDateStr = simpleDateFmt.format(endDt);
							}
						}
					}

					Phrase startDate_endDate = new Phrase(startDateStr + " - "
							+ endDateStr, font6);

					startDate_endDateCell = new PdfPCell(startDate_endDate);
					startDate_endDateCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);

					Phrase qty = new Phrase(invoiceentity
							.getSalesOrderProducts().get(i).getQuantity()
							+ "", font6);
					qtyCell = new PdfPCell(qty);
					qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					// productTable.addCell(qtyCell);

					// Phrase uom = new
					// Phrase(invoiceentity.getSalesOrderProducts()
					// .get(i).getUnitOfMeasurement().trim(), font6);
					// PdfPCell uomCell = new PdfPCell(uom);
					// uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					// productTable.addCell(uomCell);

					Phrase rate = null;

					if (consolidatePrice) {
						if (i == 0) {
							rate = new Phrase(df.format(rateAmountProd) + "",
									font6);

							rateCell = new PdfPCell(rate);
							rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						} else {
							rate = new Phrase(" ", font6);

							rateCell = new PdfPCell(rate);
							rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							rateCell.setBorderWidthBottom(0);
							rateCell.setBorderWidthTop(0);

						}
					} else {
						rate = new Phrase(df.format(invoiceentity
								.getSalesOrderProducts().get(i).getPrice())
								+ "", font6);

						rateCell = new PdfPCell(rate);
						rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}

					// amountAmount=amountAmount+invoiceentity.getSalesOrderProducts().get(i)
					// .getPrice()
					// * invoiceentity.getSalesOrderProducts().get(i)
					// .getQuantity();
					double amountValue = invoiceentity.getSalesOrderProducts()
							.get(i).getPrice()
							* invoiceentity.getSalesOrderProducts().get(i)
									.getQuantity();
					double disPercentTotalAmount = 0, disConTotalAmount = 0;
					if (invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == null
							|| invoiceentity.getSalesOrderProducts().get(i)
									.getProdPercDiscount() == 0) {
						disPercentTotalAmount = 0;
					} else {
						disPercentTotalAmount = getPercentAmount(invoiceentity
								.getSalesOrderProducts().get(i), false);
					}

					if (invoiceentity.getSalesOrderProducts().get(i)
							.getDiscountAmt() == 0) {
						disConTotalAmount = 0;
					} else {
						disConTotalAmount = invoiceentity
								.getSalesOrderProducts().get(i)
								.getDiscountAmt();
					}

					totalAmount = totalAmount + amountValue;
					Phrase disc = null;
					// PdfPCell discCell = null;
					if (consolidatePrice) {
						if (i == 0) {
							disc = new Phrase(df.format(discAmountProd) + "",
									font6);

							discCell = new PdfPCell(disc);
							discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						} else {
							disc = new Phrase(" ", font6);

							discCell = new PdfPCell(disc);
							discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

							discCell.setBorderWidthBottom(0);
							discCell.setBorderWidthTop(0);
						}
					} else {
						if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()>0) {
							disc = new Phrase(decimalformat.format(invoiceentity
									.getSalesOrderProducts().get(i)
									.getFlatDiscount())
									+ "", font6);
						}
						else {
							disc = new Phrase("0", font6);
						}

						discCell = new PdfPCell(disc);
						discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					}

//					double taxValue = 0;
//					if (invoiceentity.getSalesOrderProducts().get(i)
//							.getBasePaymentAmount() != 0
//							&& invoiceentity.getSalesOrderProducts().get(i)
//									.getPaymentPercent() != 0) {
//
//						taxValue = invoiceentity.getSalesOrderProducts().get(i)
//								.getBasePaymentAmount();
//					} else {
//						taxValue = invoiceentity.getSalesOrderProducts().get(i)
//								.getBaseBillingAmount();
//					}
//					totalAmountOfProductCode = totalAmountOfProductCode
//							+ taxValue;

					if(qtycolumnFlag) {
						String quantity = "";
						try {
							
							if(invoiceentity.getSalesOrderProducts().get(i).getArea()!=null && 
									!invoiceentity.getSalesOrderProducts().get(i).getArea().equals("")){
								quantity = Double.parseDouble(invoiceentity.getSalesOrderProducts().get(i).getArea()) +"";
							}
							
						} catch (Exception e) {
							
						}
						Phrase phQuantity = new Phrase(quantity, font6);
						quantitycell = new PdfPCell(phQuantity);
						quantitycell.setHorizontalAlignment(Element.ALIGN_CENTER);

					}
					
					String premisesVal = "";
					for (int j = 0; j < con.getItems().size(); j++) {
						if (invoiceentity.getSalesOrderProducts().get(i)
								.getProdId() == con.getItems().get(j)
								.getPrduct().getCount()  && invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
							premisesVal = con.getItems().get(j)
									.getPremisesDetails();
						}

					}
					if (premisesVal != null) {
						if (printPremiseDetails && !premisesVal.equals("")) {
							noOfLines = noOfLines - 1;
							premiseblankCell = new PdfPCell();
							premiseblankCell.setColspan(1);
							Phrase blankValPhrs = new Phrase(" ", font8);
							premiseblankCell.addElement(blankValPhrs);

							productTable.addCell(premiseblankCell);

							Phrase premisesValPhrs = new Phrase(
									"Premise Details : " + premisesVal, font8);
							premiseCell = new PdfPCell();
							premiseCell = new PdfPCell();
							premiseCell.setColspan(7);
							premiseCell.addElement(premisesValPhrs);
						}
					}
				}
				if (i + 1 == invoiceentity.getSalesOrderProducts().size()) {
					productTable.addCell(srNoCell);
					productTable.addCell(serviceNameCell);
					Phrase services = new Phrase(noOfservices + "", font6);
					servicesCell = new PdfPCell(services);

					productTable.addCell(servicesCell);
					productTable.addCell(hsnCodeCell);
					productTable.addCell(startDate_endDateCell);
					productTable.addCell(rateCell);
					
					if(qtycolumnFlag && quantitycell!=null){
					productTable.addCell(quantitycell);
					}
					
					productTable.addCell(discCell);
					Phrase taxableValue;
					taxableValue = new Phrase(
							df.format(totalAmountOfProductCode) + "", font6);
					taxableValueCell = new PdfPCell(taxableValue);
					taxableValueCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					productTable.addCell(taxableValueCell);
					if (printPremiseDetails) {
						productTable.addCell(premiseblankCell);
						productTable.addCell(premiseCell);
					}

				}
			}
		}
		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines != 0) {
			remainingLines = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);

		if (remainingLines != 0) {
			for (int i = 0; i < remainingLines; i++) {
				System.out.println("i::::" + i);
				Phrase blankPhrase = new Phrase(" ", font10);
				PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
				blankPhraseCell.setBorder(0);
				blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
			}
		}
		PdfPCell productTableCell = null;
		if (noOfLines == 0) {
			Phrase my = new Phrase("Please Refer Annexure For More Details",
					font9bold);
			productTableCell = new PdfPCell(my);

		} else {
			productTableCell = new PdfPCell(blankCell);
		}

		productTableCell.setBorder(0);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		tab.addCell(productTableCell);
		tab.setSpacingAfter(blankLines);

		// last code for both table to be added in one table

		PdfPCell tab1;
		tab1 = new PdfPCell(productTable);
		// tab1.setBorder(0);

		PdfPCell tab2 = new PdfPCell(tab);
		// tab2.setBorder(0);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		mainTable.addCell(tab2);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean validateTaxes(
			SalesOrderProductLineItem salesOrderProductLineItem) {
		// TODO Auto-generated method stub
		if (salesOrderProductLineItem.getVatTax().getPercentage() != 0) {
			return true;
		} else {
			if (salesOrderProductLineItem.getServiceTax().getPercentage() != 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}

	private void createProductDetails() {
		
		/**
		 * @author Vijay Date 20-11-2020 
		 * Des :- As per Rahul and Nitin sir instaruction if Quatity exist in invoice product table then
		 * quontity column will display in product table
		 */
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			double quantity=0;
			try {
				if(invoiceentity.getSalesOrderProducts().get(i).getArea()!=null && 
						!invoiceentity.getSalesOrderProducts().get(i).getArea().equals("")){
					quantity = Double.parseDouble(invoiceentity.getSalesOrderProducts().get(i).getArea());
					if(quantity>0){
						qtycolumnFlag = true;
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/**
		 * ends here
		 */
		PdfPTable productTable;
		if(qtycolumnFlag) {
			 productTable = new PdfPTable(9);
			 productTable.setWidthPercentage(100);
				try {
					productTable.setWidths(column9SerProdCollonWidth);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
		}
		else {
			productTable = new PdfPTable(8);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column8ProdCollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
//		PdfPTable productTable = new PdfPTable(8);
//		productTable.setWidthPercentage(100);
//		try {
//			productTable.setWidths(column8ProdCollonWidth);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}

		int colsspan =1;
		if(PC_DoNotPrintContractSericesFlag){
			++colsspan;
		}
		
		if(PC_DoNotPrintContractDurationFlag){
			++colsspan;
		}
		
		
		Phrase srNophrase = new Phrase("Sr No", font10bold);
		PdfPCell srNoCell = new PdfPCell(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

//		Phrase servicePhrase = new Phrase("Particulars", font10bold);
		/**@Sheetal : 02-03-2022 Renaming Particulars to Service, customization for pest-o-sheild**/
		Phrase servicePhrase = new Phrase("Service", font10bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2
		servicePhraseCell.setColspan(colsspan);

		Phrase serviceQuantityPhrase = new Phrase("Contract Services", font10bold); //Ashwini Patil Date:10-08-2022 Changes "Services" label to "Contract services" as per Pest O Shields requirement
		PdfPCell serviceQuantityPhraseCell = new PdfPCell(serviceQuantityPhrase);
		serviceQuantityPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceQuantityPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		serviceQuantityPhraseCell.setRowspan(2);// 2

		Phrase hsnCode = new Phrase("SAC", font10bold);
		PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 3

		Phrase UOMphrase = new Phrase("UOM", font10bold);
		PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 4

		Phrase qtyPhrase = new Phrase("No ", font10bold);
		PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 5

		Phrase ratePhrase = new Phrase("Rate", font10bold);
		PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 6

		Phrase amountPhrase = new Phrase("Amount", font10bold);
		PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 7

		Phrase dicphrase = new Phrase("Disc", font10bold);
		PdfPCell dicphraseCell = new PdfPCell(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 8
		
		Phrase quantityPhrase = new Phrase("Qty", font10bold);
		PdfPCell quantityCell = new PdfPCell(quantityPhrase);
		quantityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		quantityCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		quantityCell.setRowspan(2);// 7

		Phrase taxValPhrase = new Phrase("Amount", font10bold);
		PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 9
		
		/**
		 * @author Anil
		 * @since 21-01-2022
		 * If service wise bill type of invoice is printing then need to change label from Contract Duration to
		 * Billing Period. raised by Nithila and Nitin Sir
		 */
		
		String lable="Duration";
		if(serviceWiseBillInvoice&&!billingPeriod.equals("")){
			lable="Billing Duration";
		}
	     /** @Sheetal : 10-02-2022
	   *      Des : Switching Billing period and Contract duration with below process configuration,
                    requirment by Pest-O-Shield **/
	     if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PC_INV_Quot_Con_SWAP_BILLING_AND_CONTRACT_PERIOD", invoiceentity.getCompanyId())){
		   lable="Billing Period";
	     }

//		Phrase serviceServDate = new Phrase("Duration", font10bold);
		Phrase serviceServDate = new Phrase(lable, font10bold);
		PdfPCell serviceServDateCell = new PdfPCell(serviceServDate);
		serviceServDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceServDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
	
		if(!PC_DoNotPrintContractSericesFlag){
			productTable.addCell(serviceQuantityPhraseCell);

		}

		productTable.addCell(hsnCodeCell);
		
		if(!PC_DoNotPrintContractDurationFlag){
			productTable.addCell(serviceServDateCell);

		}


		// productTable.addCell(qtyPhraseCell);
		// productTable.addCell(UOMphraseCell);
		//productTable.addCell(ratePhraseCell);
		// productTable.addCell(amountPhraseCell);
		
		if(qtycolumnFlag){
			productTable.addCell(quantityCell);
		}
		
		
//		if(!hideRateAndDiscount){
//			productTable.addCell(ratePhraseCell);
//			productTable.addCell(dicphraseCell);
//		}else{
//				taxValPhraseCell.setColspan(3);
//			}
//			productTable.addCell(taxValPhraseCell);
		
		int ratediscountcolspan =3;
		if(!PC_DoNotPrintRateFlag){
			productTable.addCell(ratePhraseCell);
			ratediscountcolspan = ratediscountcolspan - 1;
		}
		if(!PC_DoNotPrintDiscFlag){
			productTable.addCell(dicphraseCell);
			ratediscountcolspan = ratediscountcolspan - 1;
		}
		taxValPhraseCell.setColspan(ratediscountcolspan);
		productTable.addCell(taxValPhraseCell);

		//productTable.addCell(dicphraseCell);
		//productTable.addCell(taxValPhraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private double getPercentAmount(
			SalesOrderProductLineItem salesOrderProductLineItem,
			boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		if (isAreaPresent) {
			percentAmount = ((salesOrderProductLineItem.getPrice()
					* Double.parseDouble(salesOrderProductLineItem.getArea()
							.trim()) * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		} else {
			percentAmount = ((salesOrderProductLineItem.getPrice() * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		}
		return percentAmount;
	}

	private void createCustomerDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		//By Ashwini Patil to generate colon cell with padding
		Phrase colon_bottonaligned = new Phrase(":", font10bold);
		PdfPCell colonCell_bottonaligned  = new PdfPCell(colon_bottonaligned);
		colonCell_bottonaligned.setBorder(0);
		colonCell_bottonaligned.setHorizontalAlignment(Element.ALIGN_LEFT);
		colonCell_bottonaligned.setPaddingTop(5);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font10bold);
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCell.setPaddingTop(5);		//Ashwini Patil 

		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017

		String tosir = null;
		String custName = "";
		
		
		/**
		 * @author Ashwini Patil
		 * @since 27-04-2022
		 * Nikhil reported and Nitin Sir asked to Print Company name/Full name/Branch name in service address.
		 * System was printing Correspondance name in Service Address which was wrong
		 */
		String custNameForServiceAddress=""; 
		boolean isSociety=false;
		
		//Ashwini Patil Date:5-08-2024 If the customer is society then do not print M/S against it as per ultima requirement
		if((cust.getCategory()!=null&&cust.getCategory().equalsIgnoreCase("Society"))||(cust.getType()!=null&&cust.getType().equalsIgnoreCase("Society")))
			isSociety=true;
		if(cust.getServiceAddressName()!=null&&!cust.getServiceAddressName().equals("")) {
			custNameForServiceAddress=cust.getServiceAddressName();
		}else if (cust.isCompany() == true && cust.getCompanyName() != null) {
			if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
				custNameForServiceAddress = cust.getCompanyName().trim();
			}
			else{
				custNameForServiceAddress = "M/S " + " " + cust.getCompanyName().trim();
			}
		} else if (cust.getSalutation() != null
				&& !cust.getSalutation().equals("")) {
			custNameForServiceAddress = cust.getSalutation() + " "
					+ cust.getFullname().trim();
		} else {
			custNameForServiceAddress = cust.getFullname().trim();
		}
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				if(PC_RemoveSalutationFromCustomerOnPdfFlag||isSociety){
					custName = cust.getCompanyName().trim();
				}
				else{
					custName = "M/S " + cust.getCompanyName().trim();
				}
			}
			else if (cust.getSalutation() != null
					&& !cust.getSalutation().equals("")) {
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			}
			else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here

		Phrase nameCellVal = new Phrase(fullname, nameAddressBoldFont);//font10bold Ashwini Patil
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCellValCell.setPaddingTop(5); //by Ashwini Patil

		// //////////////////Ajinkya Added this for Ultra pest Control

		Phrase email = new Phrase("Email", font10bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase emailVal = new Phrase(cust.getEmail(), font10);
		PdfPCell emailValCell = new PdfPCell(emailVal);
		// nameCellValCell.addElement(nameCellVal);asd
		emailValCell.setBorder(0);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo = new Phrase("Mobile", font10bold);
		PdfPCell mobNoCell = new PdfPCell(mobNo);
		mobNoCell.setBorder(0);
		mobNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * Date 13-09-2017 added by vijay for customer address first it will
		 * check from contract if in contract customer address is available then
		 * it will print else it will get customer address from customer below
		 * line old code commented
		 */
		// String adrsValString = cust.getAdress().getCompleteAddress().trim();
		String countryName ="";
		String adrsValString = "";
		if (con.getNewcustomerAddress() != null
				&& con.getNewcustomerAddress().getAddrLine1() != null
				&& !con.getNewcustomerAddress().getAddrLine1().equals("")
				&& con.getNewcustomerAddress().getCountry() != null
				&& !con.getNewcustomerAddress().getCountry().equals("")) {
			adrsValString = con.getNewcustomerAddress().getCompleteAddress()
					.trim();
			countryName = con.getNewcustomerAddress().getCountry();
		} else {
			adrsValString = cust.getAdress().getCompleteAddress().trim();
			countryName = cust.getAdress().getCountry();

		}
		ServerAppUtility serverappUtility = new ServerAppUtility();
		String cellNumber = serverappUtility.getMobileNoWithCountryCode(cust.getCellNumber1()+"", countryName, comp.getCompanyId());

		Phrase mobVal = new Phrase(cellNumber + "", font10);
		PdfPCell mobValCell = new PdfPCell(mobVal);
		// nameCellValCell.addElement(nameCellVal);asd
		mobValCell.setBorder(0);
		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ///////////////// End Here

		Phrase address = new Phrase("Address", font10bold);
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addressCell.setPaddingTop(5);//Ashwini Patil

		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
		adrsValString=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Invoice", adrsValString);

		Phrase addressVal = new Phrase(adrsValString, nameAddressFont); //font10 Ashwini Patil
		PdfPCell addressValCell = new PdfPCell(addressVal);
		// addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addressValCell.setPaddingTop(5);//Ashwini Patil

		String gstTinStr = "";
		if (invoiceentity.getGstinNumber() != null
				&& !invoiceentity.getGstinNumber().equals("")) {
			gstTinStr = invoiceentity.getGstinNumber().trim();

		} else {
			ServerAppUtility serverAppUtility = new ServerAppUtility();
			/**
			 * @author Anil , Date : 06-09-2019
			 * Uncommented this part
			 * as Perfect pest control faces issue when printing printing multiple bill invoice
			 * raised By Rahul Tiwari 
			 */
			gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,
					invoiceentity.getCustomerBranch(), "ServiceInvoice");
			
			
			
			// date
																			// 1/2/2018
																			// by
																			// jayshree
																			// add
																			// parameter
			// for (int j = 0; j < cust.getArticleTypeDetails().size(); j++) {
			// if (cust.getArticleTypeDetails().get(j).getArticleTypeName()
			// .equalsIgnoreCase("GSTIN")) {
			// gstTinStr = cust.getArticleTypeDetails().get(j)
			// .getArticleTypeValue().trim();
			// }
			// }
		}

		Phrase gstTin = new Phrase("GSTIN", font10bold);
		PdfPCell gstTinCell = new PdfPCell(gstTin);
		// gstTinCell.addElement(gstTin);
		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTinVal = new Phrase(gstTinStr, font10);
		PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
		// gstTinValCell.addElement(gstTinVal);
		gstTinValCell.setBorder(0);
		gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn = new Phrase("Attn", font10bold);
		PdfPCell attnCell = new PdfPCell(attn);
		// gstTinCell.addElement(gstTin);
		attnCell.setBorder(0);
		attnCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		//5-08-2024 need salutation before poc name as per ultima search
		Phrase attnVal = null;
		if (cust.getSalutation() != null&& !cust.getSalutation().equals(""))
			attnVal=new Phrase(cust.getSalutation() + " "+invoiceentity.getPersonInfo().getPocName(),
				font10);
		else
			attnVal=new Phrase(invoiceentity.getPersonInfo().getPocName(),
					font10);
		PdfPCell attnValCell = new PdfPCell(attnVal);
		// gstTinValCell.addElement(gstTinVal);
		attnValCell.setBorder(0);
		attnValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell_bottonaligned); //colonCell_bottonaligned set by Ashwini Patil
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attnCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attnValCell);
		}
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell_bottonaligned);//Ashwini Patil
		colonTable.addCell(addressValCell);

		PdfPTable colonTable2 = new PdfPTable(3);
		colonTable2.setWidthPercentage(100);
		try {
			colonTable2.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable email_MobNumber = new PdfPTable(3);
		email_MobNumber.setWidthPercentage(100);
		try {
			email_MobNumber.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon2 = new Phrase(":", font10bold);
		PdfPCell colon2Cell = new PdfPCell(colon2);
		colon2Cell.setBorder(0);
		colon2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		email_MobNumber.addCell(emailCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(emailValCell);
		email_MobNumber.addCell(mobNoCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(mobValCell);

		if(gstNumberPrintFlag){//29-01-2029 gstNumberPrintFlag added as GST was getting prinetd on NonBilling invoices as well
		colonTable2.addCell(gstTinCell);
		colonTable2.addCell(colon2Cell);
		colonTable2.addCell(gstTinValCell);
		}

		PdfPCell cell1 = new PdfPCell(colonTable);
		cell1.setBorder(0);

		PdfPCell email_Cell = new PdfPCell(email_MobNumber);
		email_Cell.setBorder(0);

		PdfPCell cell12 = new PdfPCell(colonTable2);
		cell12.setBorder(0);

		Phrase state = new Phrase("State", font10bold);
		PdfPCell stateCell = new PdfPCell(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(cust.getAdress().getState().trim(), font10);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase stateCode = new Phrase("State Code", font10bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(stCo, font10);
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		PdfPTable pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfStateTable.addCell(stateValCell);
		pdfStateTable.addCell(stateCodeCell);
		pdfStateTable.addCell(colonCell);
		pdfStateTable.addCell(stateCodeValCell);

		PdfPCell state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell); // stateTableCell.addElement(statetable);

		PdfPCell stateCell_stateCode = new PdfPCell(colonTable);
		stateCell_stateCode.setBorder(0);

		part1Table.addCell(cell1);
		part1Table.addCell(stateCell_stateCode);
		part1Table.addCell(email_Cell);
		part1Table.addCell(cell12);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		/* Ends Part 1 */

		String countryName2 ="";
		// ///////////////////////// Ajinkya added branch code for customer
				// Branch On Date : 15/07/2017 //////////////////////////////
				String adrsValString1 = "";

				// //////////////////////////////
				String customerPoc = null;
				if (!cust.getSecondaryAdress().getAddrLine1().equals("")
						&& customerbranchlist.size() == 0) {
					System.out.println("Inside service Addrss");

					// old code commented by vijay new code added below
					// adrsValString1 = cust.getSecondaryAdress().getCompleteAddress()
					// .trim();

					/**
					 * Date 13-09-2017 added by vijay for customer address first it will
					 * check from contract if in contract customer address is available
					 * then it will print else it will get customer address from
					 * customer
					 * 
					 */

					if (con.getCustomerServiceAddress() != null
							&& con.getCustomerServiceAddress().getAddrLine1() != null
							&& !con.getCustomerServiceAddress().getAddrLine1()
									.equals("")
							&& con.getCustomerServiceAddress().getCountry() != null
							&& !con.getCustomerServiceAddress().getCountry().equals("")) {
						adrsValString1 = con.getCustomerServiceAddress()
								.getCompleteAddress().trim();
						countryName2 = con.getCustomerServiceAddress().getCountry();
					} else {
						adrsValString1 = cust.getSecondaryAdress().getCompleteAddress()
								.trim();
						countryName2 = cust.getSecondaryAdress().getCountry();
					}
					customerPoc = invoiceentity.getPersonInfo().getPocName();
				} else {
					System.out.println("Inside Customer branch  "
							+ customerbranchlist.size());
					for (int i = 0; i < customerbranchlist.size(); i++) {
						// if()
						adrsValString1 = customerbranchlist.get(i).getAddress()
								.getCompleteAddress();
						customerPoc = customerbranchlist.get(i).getPocName();
					}

				}
				
		/* Part 2 Start */
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font10bold);
		PdfPCell name2Cell = new PdfPCell(name2);
		// name2Cell.addElement();
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil @since 29-10-2021
		 * if customer branch is selected on invoice then print branch name as name on invoice shipping details section
		 * requirement raised by IndoGulf , Nitin Sir and Poonam
		 */
		String custBranchName=custNameForServiceAddress; 
		if(customerBranch!=null){
			if(customerBranch.getServiceAddressName()!=null&&!customerBranch.getServiceAddressName().equals(""))
				custBranchName=customerBranch.getServiceAddressName();
			else
				custBranchName=customerBranch.getBusinessUnitName();
		}

//		Phrase name2CellVal = new Phrase(fullname, font10bold);
		Phrase name2CellVal = new Phrase(custBranchName, nameAddressBoldFont);//font10bold Ashwini Patil
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		name2CellValCell.setPaddingTop(5); //by Ashwini Patil

		// //////////////// Ajinkya added this
		Phrase email1 = new Phrase("Email", font10bold);
		PdfPCell email1Cell = new PdfPCell(email1);
		email1Cell.setBorder(0);
		email1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);


		Phrase email1Val = new Phrase(cust.getEmail(), font10);
		PdfPCell email1ValCell = new PdfPCell(email1Val);
		// nameCellValCell.addElement(nameCellVal);asd
		email1ValCell.setBorder(0);
		email1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo2 = new Phrase("Mobile", font10bold);
		PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
		mobNo2Cell.setBorder(0);
		mobNo2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custcellNumber = serverappUtility.getMobileNoWithCountryCode(cust.getCellNumber1()+"", countryName2, comp.getCompanyId());

		Phrase mobNo2Val = new Phrase(custcellNumber, font10);
		PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);
		// nameCellValCell.addElement(nameCellVal);asd
		mobNo2ValCell.setBorder(0);
		mobNo2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// //////////////////

		Phrase address2 = new Phrase("Address", font10bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		address2Cell.setPaddingTop(5);//Ashwini Patil

		
		/**
		 * @author Anil @since 31-05-2021
		 * This invoice is printed if invoice is made from multiple bill of same contract
		 * So if the mentioned process configuration is active then print branch name in service section instead of client name
		 */
		if (customerbranchlist!=null&&customerbranchlist.size()!=0) {
			if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "CustomerBranchNameInInvoiceServiceAddress",invoiceentity.getCompanyId())) {
				name2CellVal = new Phrase(customerbranchlist.get(0).getBusinessUnitName(),font10bold);
				name2CellValCell = new PdfPCell(name2CellVal);
				name2CellValCell.setBorder(0);
				name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			}
		}
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
		adrsValString1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Invoice", adrsValString1);

		Phrase address2Val = new Phrase(adrsValString1, nameAddressFont); //font10
		PdfPCell address2ValCell = new PdfPCell(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		address2ValCell.setPaddingTop(5);//Ashwini Patil
		String gstTin2Str = "";
		if (invoiceentity.getGstinNumber() != null
				&& !invoiceentity.getGstinNumber().equalsIgnoreCase("")) {
			gstTin2Str = invoiceentity.getGstinNumber().trim();
		} else {
			ServerAppUtility serverAppUlAppUtility = new ServerAppUtility();

//			gstTin2Str = serverAppUlAppUtility.getGSTINOfCustomer(cust,
//					invoiceentity.getCustomerBranch(), "ServiceInvoice");// Date
																			// 1/2/2018
																			// by
																			// jayshree
																			// add
																			// parameter

		}

		Phrase gstTin2 = new Phrase("GSTIN", font10bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font10);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2 = new Phrase("Attn", font10bold);
		PdfPCell attn2Cell = new PdfPCell(attn2);
		attn2Cell.setBorder(0);
		attn2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2Val = null;
		if (custName != null) {
			attn2Val = new Phrase(customerPoc, font10);
		} else {
			attn2Val = new Phrase("", font10);
		}
		PdfPCell attn2ValCell = new PdfPCell(attn2Val);
		attn2ValCell.setBorder(0);
		attn2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell_bottonaligned); //colonCell_bottonaligned set by Ashwini Patil
//		colonTable.addCell(nameCellValCell);
		colonTable.addCell(name2CellValCell);
		
		if (printAttnInPdf) {
			colonTable.addCell(attn2Cell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attn2ValCell);
		}
		colonTable.addCell(address2Cell);
		colonTable.addCell(colonCell_bottonaligned);//Ashwini Patil
		colonTable.addCell(address2ValCell);

		PdfPTable colonTable22 = new PdfPTable(3);
		colonTable22.setWidthPercentage(100);
		try {
			colonTable22.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable email_MobNumber2 = new PdfPTable(3);
		email_MobNumber2.setWidthPercentage(100);
		try {
			email_MobNumber2.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		email_MobNumber2.addCell(emailCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(emailValCell);
		email_MobNumber2.addCell(mobNoCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(mobValCell);

		// colonTable22.addCell(gstTin2Cell);
		// colonTable22.addCell(colonCell);
		// colonTable22.addCell(gstTin2ValCell);

		PdfPCell cell2 = new PdfPCell(colonTable);
		cell2.setBorder(0);

		PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
		email_MobNumber2Cell.setBorder(0);

		PdfPCell cell22 = new PdfPCell(colonTable22);
		cell22.setBorder(0);

		Phrase state2 = new Phrase("State", font10bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stateName = "";
		if (!cust.getSecondaryAdress().getAddrLine1().equals("")
				&& customerbranchlist.size() == 0) {
			stateName = cust.getSecondaryAdress().getState().trim();
		} else {
			for (int i = 0; i < customerbranchlist.size(); i++) {
				// if()
				stateName = customerbranchlist.get(i).getAddress().getState()
						.trim();
			}

		}
		Phrase state2Val = new Phrase(stateName, font10);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// stateValCell.addElement(stateVal);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String st2Co = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(stateName.trim())) {
				st2Co = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font10bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(st2Co, font10);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);

		// dc
		// PdfPTable pdfState2Table = new PdfPTable(4);
		// pdfState2Table.setWidthPercentage(100);
		// try {
		// pdfState2Table.setWidths(columnStateCodeCollonWidth);
		// } catch (DocumentException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		// pdfState2Table.addCell(state2ValCell);
		// pdfState2Table.addCell(state2CodeCell);
		// pdfState2Table.addCell(colonCell);
		// pdfState2Table.addCell(state2CodeValCell);
		//
		// PdfPCell state4Cell2 = new PdfPCell(pdfState2Table);
		// state4Cell2.setBorder(0);
		// state4Cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// colonTable.addCell(state4Cell2); //
		// stateTableCell.addElement(statetable);

		PdfPCell stateCell_stateCode2 = new PdfPCell(colonTable);
		stateCell_stateCode2.setBorder(0);

		part2Table.addCell(cell2);
		part2Table.addCell(stateCell_stateCode2);
		part2Table.addCell(email_MobNumber2Cell);
		part2Table.addCell(cell22);
		
		boolean doNotPrintGSTNOServiceAddress=false;
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","DONOTPRINTGSTNUMBERINSERVICEADDRESS", cust.getCompanyId())){
			doNotPrintGSTNOServiceAddress=true;
		}
		PdfPTable gstTable = new PdfPTable(3);
		gstTable.setWidthPercentage(100);
		try {
			gstTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Ashwini Patil Date:16-11-2023 Integrated pest reported that gst number not getting printed
		if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
			logger.log(Level.SEVERE,"Cust and article list not null");
			for(ArticleType type:cust.getArticleTypeDetails()){
				if(type.getDocumentName().equalsIgnoreCase("Invoice Details")&&type.getArticlePrint().equalsIgnoreCase("Yes")){
					if(type.getArticleTypeValue()!=null && !type.getArticleTypeValue().equals("")){
						if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")&&!gstNumberPrintFlag){
							continue;
						}			
						if(doNotPrintGSTNOServiceAddress){
							logger.log(Level.SEVERE,"in if of article");
							if(!type.getArticleTypeName().equalsIgnoreCase("GSTIN")){
								gstTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeName(), font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
								gstTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
								gstTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeValue(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));		
							}								
						}else{							
							logger.log(Level.SEVERE,"in else of article");
							gstTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeName(), font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
							gstTable.addCell(pdfUtility.getPdfCell(":", font10bold, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
							gstTable.addCell(pdfUtility.getPdfCell(type.getArticleTypeValue(), font10, Element.ALIGN_LEFT, null, 0, 0, 0,0,0,-1,-1,-1,-1));
						}
					}
										
				}
			}
		}
		PdfPCell gstCell = new PdfPCell(gstTable);
		gstCell.setBorder(0);
		part2Table.addCell(gstCell);
		

		
		if(invoiceentity.isDonotprintServiceAddress()){
			part1TableCell.setColspan(2);
		}
		
		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);
		/* Part 2 Ends */
		mainTable.addCell(part1TableCell);
		
		if(!invoiceentity.isDonotprintServiceAddress()){
			mainTable.addCell(part2TableCell);
			mainTable.addCell(blankCell);
			mainTable.addCell(blankCell);
		}
		else{
			mainTable.addCell(blankCell);
		}

		
//		mainTable.addCell(part2TableCell);
//		mainTable.addCell(blankCell);
//		mainTable.addCell(blankCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createInvoiceDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = { 3.5f, 0.2f, 6.8f };
		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseCharge = new Phrase("Reverse Charge (Y/N) ", font10bold);
		PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseChargeVal = new Phrase("NO", font10);
		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNo = new Phrase("Invoice No", font10bold);
		PdfPCell invoiceNoCell = new PdfPCell(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
			/**
			 * Date :13-05-2021 By PRIYANKA 
			 * DES : Print Invoice Prefix on Pdf issue raised by Vaishnavi Mam.
			 */
		
		
		String invoiceId="";
		logger.log(Level.SEVERE, "invoicePrefix flag "+invoicePrefix);
		logger.log(Level.SEVERE, "invoicePrefix value "+invoicePrefix);
		
		if(invoicePrefix){
			if(invoiceentity.getInvRefNumber()!=null&&!invoiceentity.getInvRefNumber().equals("")){
				invoiceId=invoiceentity.getInvRefNumber();
			}else{
				invoiceId=invoiceentity.getCount()+"";
			}
		}else{
			invoiceId=invoiceentity.getCount()+"";
		}
		
		logger.log(Level.SEVERE, "invoice no "+invoiceId);
		Phrase invoiceNoVal = new Phrase(invoiceId, font10);
		PdfPCell invoiceNoValCell = new PdfPCell(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * printing Ref no/Po/Wo as per the data for each available
		 * Raised by Nithila and Nitin Sir for Innovative
		 */
		String refNoHeading="";
		String refNoValue="";
		
		if(invoiceentity.getRefNumber()!=null&&!invoiceentity.getRefNumber().equals("")){
			refNoHeading=refNoHeading+"Ref No/";
			refNoValue=refNoValue+invoiceentity.getRefNumber()+"/";
		}
		
		if(invoiceentity.getWoNumber()!=null&&!invoiceentity.getWoNumber().equals("")){
			refNoHeading=refNoHeading+"WO No/";
			refNoValue=refNoValue+invoiceentity.getWoNumber()+"/";
		}
		
		if(invoiceentity.getPoNumber()!=null&&!invoiceentity.getPoNumber().equals("")){
			refNoHeading=refNoHeading+"PO No";
			refNoValue=refNoValue+invoiceentity.getPoNumber();
		}
		
		if(!refNoHeading.equals("")){
			if(refNoHeading.substring(refNoHeading.length()-1).contains("/")){
				refNoHeading=refNoHeading.substring(0, refNoHeading.length()-1);
			}
		}
		
		if(!refNoValue.equals("")){
			if(refNoValue.substring(refNoValue.length()-1).contains("/")){
				refNoValue=refNoValue.substring(0, refNoValue.length()-1);
			}
		}

//		Phrase workOrder_PoNo = new Phrase("Ref No/WO", font10bold);
		Phrase workOrder_PoNo = new Phrase(refNoHeading, font10bold);
		PdfPCell workOrder_PoNoCell = new PdfPCell(workOrder_PoNo);
		workOrder_PoNoCell.setBorder(0);
		workOrder_PoNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase workOrder_PoNoVal = new Phrase(invoiceentity.getRefNumber() + "", font10);
		Phrase workOrder_PoNoVal = new Phrase(refNoValue + "", font10);
		PdfPCell workOrder_PoNoValCell = new PdfPCell(workOrder_PoNoVal);
		workOrder_PoNoValCell.setBorder(0);
		workOrder_PoNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);

		Phrase invoiceDate = new Phrase("Invoice Date", font10bold);//Date:21-12-2022
		PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceDateVal = new Phrase(sdf.format(invoiceentity
				.getInvoiceDate()), font10);
		PdfPCell invoiceDateValCell = new PdfPCell(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable invcolonTable = new PdfPTable(3);
		invcolonTable.setWidthPercentage(100);

		try {
			invcolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable pdfInvDataTable = new PdfPTable(4);
		pdfInvDataTable.setWidthPercentage(100);
		try {
			pdfInvDataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfInvDataTable.addCell(invoiceNoValCell);
		pdfInvDataTable.addCell(invoiceDateCell);
		pdfInvDataTable.addCell(colonCell);
		pdfInvDataTable.addCell(invoiceDateValCell);

		PdfPCell pdfPInvoiceDataCell = new PdfPCell(pdfInvDataTable);
		pdfPInvoiceDataCell.setBorder(0);
		pdfPInvoiceDataCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		invcolonTable.addCell(pdfPInvoiceDataCell); // stateTableCell.addElement(statetable);

		PdfPCell invoiceDataCell = new PdfPCell(pdfInvDataTable);
		invoiceDataCell.setBorder(0);

		colonTable.addCell(invoiceDataCell);
		if(!refNoHeading.equals("")){
			colonTable.addCell(workOrder_PoNoCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(workOrder_PoNoValCell);
		}
		
		colonTable.setSpacingAfter(10f);

		PdfPCell pdfCell = new PdfPCell(colonTable);
		pdfCell.setBorder(0);
		// pdfCell.addElement();

		part1Table.addCell(pdfCell);

		Phrase state = new Phrase("State", font10bold);
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(),
				font10);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCode = new Phrase("State Code", font10bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(" ", font10);
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		// stateCodeValCell.addElement();
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);

		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);

		PdfPCell stateTableCell = new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		Phrase contractId = new Phrase("Contract No", font10bold);
		PdfPCell contractIdCell = new PdfPCell(contractId);
		// contractIdCell.addElement(contractId);
		contractIdCell.setBorder(0);
		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase contractIdVal = new Phrase(
				invoiceentity.getContractCount() + "", font10);
		PdfPCell contractIdValCell = new PdfPCell(contractIdVal);
		// contractIdValCell.addElement(contractIdVal);
		contractIdValCell.setBorder(0);
		contractIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase contractDate = new Phrase("Contract Date", font10bold);
		// PdfPCell contractDateCell = new PdfPCell(contractDate);
		// // contractIdCell.addElement(contractId);
		// contractDateCell.setBorder(0);
		// contractDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		//
		//
		// PdfPTable billperiodtable = new PdfPTable(2);
		// billperiodtable.setWidthPercentage(100);
		//
		// PdfPTable billFromcolonTable = new PdfPTable(3);
		// billFromcolonTable.setWidthPercentage(100);
		// try {
		// billFromcolonTable.setWidths(columnDateCollonWidth);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// billFromcolonTable.addCell(periodstartDateCell);
		// billFromcolonTable.addCell(colonCell);
		// billFromcolonTable.addCell(periodstartDateValCell);
		//
		// PdfPTable billTocolonTable = new PdfPTable(3);
		// billTocolonTable.setWidthPercentage(100);
		// try {
		// billTocolonTable.setWidths(columnDateCollonWidth);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// billTocolonTable.addCell(periodendDateCell);
		// billTocolonTable.addCell(colonCell);
		// billTocolonTable.addCell(periodendDateValCell);

		// PdfPCell fromDate = new PdfPCell(billFromcolonTable);
		// fromDate.setBorder(0);
		// // fromDate.addElement(billFromcolonTable);
		//
		// billperiodtable.addCell(fromDate);
		// fromDate = new PdfPCell(billTocolonTable);
		// fromDate.setBorder(0);
		// // fromDate.addElement();
		// billperiodtable.addCell(fromDate);
		/**
		 *
		 */
		PdfPTable periodtable = new PdfPTable(2);
		periodtable.setWidthPercentage(100);

		PdfPTable concolonTable = new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		concolonTable.addCell(contractIdCell);
		concolonTable.addCell(colonCell);

		Phrase contractDate = new Phrase("Contract Date", font10bold);//Date:21-12-2022
		PdfPCell contractDateCell = new PdfPCell(contractDate);
		contractDateCell.setBorder(0);
		contractDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase contractDateVal = new Phrase(sdf.format(con.getContractDate()),
				font10);
		PdfPCell contractDateValCell = new PdfPCell(contractDateVal);
		contractDateValCell.setBorder(0);
		contractDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable contractColTable = new PdfPTable(3);
		contractColTable.setWidthPercentage(100);

		try {
			contractColTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable pdfConDataTable = new PdfPTable(4);
		pdfConDataTable.setWidthPercentage(100);
		try {
			pdfConDataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfConDataTable.addCell(contractIdValCell);
		pdfConDataTable.addCell(contractDateCell);
		pdfConDataTable.addCell(colonCell);
		pdfConDataTable.addCell(contractDateValCell);

		PdfPCell pdfPConDataCell = new PdfPCell(pdfConDataTable);
		pdfPConDataCell.setBorder(0);
		pdfPConDataCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		concolonTable.addCell(pdfPConDataCell); // stateTableCell.addElement(statetable);

		PdfPCell contractDataCell = new PdfPCell(concolonTable);
		contractDataCell.setBorder(0);

		concolonTable.addCell(contractDataCell);

		Phrase startDate = new Phrase("Contract From", font10bold);
		PdfPCell startDateCell = new PdfPCell(startDate);
		startDateCell.setBorder(0);
		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase startDateVal = new Phrase(sdf.format(invoiceentity
				.getContractStartDate()), font10);
		PdfPCell startDateValCell = new PdfPCell(startDateVal);
		// stateValCell.addElement(stateVal);
		startDateValCell.setBorder(0);
		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDate = new Phrase("To", font10bold);
		PdfPCell endDateCell = new PdfPCell(endDate);
		endDateCell.setBorder(0);
		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDateVal = new Phrase(sdf.format(invoiceentity
				.getContractEndDate()), font10);
		PdfPCell endDateValCell = new PdfPCell(endDateVal);
		endDateValCell.setBorder(0);
		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(startDateCell);
		colonTable.addCell(colonCell);
		PdfPTable pdfContractPeriodTable = new PdfPTable(4);
		pdfContractPeriodTable.setWidthPercentage(100);
		try {
			pdfContractPeriodTable
					.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfContractPeriodTable.addCell(startDateValCell);
		pdfContractPeriodTable.addCell(endDateCell);
		pdfContractPeriodTable.addCell(colonCell);
		pdfContractPeriodTable.addCell(endDateValCell);

		PdfPCell state4Cell = new PdfPCell(pdfContractPeriodTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell); // stateTableCell.addElement(statetable);

		PdfPCell periodTableCell = new PdfPCell(colonTable);
		periodTableCell.setBorder(0);
		/**
		 * Billing Period
		 */
		
		/**
		 * @author Anil
		 * @since 21-01-2022
		 * For Hygeinic Pest we will print contract duration at the top and billing duration on product line item
		 * level. raised by Nithila and Nitin sir
		 */
		
		String headingFrom="Billing From";
		String headingTo="To"; 
		String fromValue="";
		if (invoiceentity.getBillingPeroidFromDate() != null) {
			fromValue=sdf.format(invoiceentity.getBillingPeroidFromDate());
		}
		String toValue="";
		if (invoiceentity.getBillingPeroidToDate() != null) {
			toValue=sdf.format(invoiceentity.getBillingPeroidToDate());
		}
		
		if(serviceWiseBillInvoice&&!con.isContractRate()){ //Ashwini Patil Date:22-03-2022 Description: To avoid double contract from -to in rate contract invoice print
			headingFrom="Contract From";
			headingTo="To"; 
			
			if (invoiceentity.getContractStartDate()!= null) {
				fromValue=sdf.format(invoiceentity.getContractStartDate());
			}
			if (invoiceentity.getContractEndDate()!= null) {
				toValue=sdf.format(invoiceentity.getContractEndDate());
			}
		}
		/** @Sheetal : 10-02-2022
		   * Des : Switching Billing period and Contract duration with below process configuration,
                 requirment by Pest-O-Shield **/
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice","PC_INV_Quot_Con_SWAP_BILLING_AND_CONTRACT_PERIOD", invoiceentity.getCompanyId())){
			headingFrom="Contract From";
			headingTo="To"; 
			
			if (invoiceentity.getContractStartDate()!= null) {
				fromValue=sdf.format(invoiceentity.getContractStartDate());
			}
			if (invoiceentity.getContractEndDate()!= null) {
				toValue=sdf.format(invoiceentity.getContractEndDate());
			}
			System.out.println("sheetal");
		}
		/**end**/
		
//		Phrase periodstartDate = new Phrase("Billing From", font10bold);
		Phrase periodstartDate = new Phrase(headingFrom, font10bold);
		PdfPCell periodstartDateCell = new PdfPCell(periodstartDate);
		// periodstartDateCell.addElement(periodstartDate);
		periodstartDateCell.setBorder(0);
		periodstartDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan adde this code
		Phrase periodstartDateVal = null;
//		if (invoiceentity.getBillingPeroidFromDate() != null) {
//			periodstartDateVal = new Phrase(sdf.format(invoiceentity.getBillingPeroidFromDate()), font10);
//		} else {
//			periodstartDateVal = new Phrase(" ", font10);
//		}
		
		periodstartDateVal = new Phrase(fromValue, font10);

		PdfPCell periodstartDateValCell = new PdfPCell(periodstartDateVal);
		// periodstartDateValCell.addElement(periodstartDateVal);
		periodstartDateValCell.setBorder(0);
		periodstartDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase periodendDate = new Phrase("To", font10bold);
		Phrase periodendDate = new Phrase(headingTo, font10bold);
		PdfPCell periodendDateCell = new PdfPCell(periodendDate);
		// periodendDateCell.addElement(periodendDate);
		periodendDateCell.setBorder(0);
		periodendDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan adde this code
		Phrase periodendDateVal = null;
//		if (invoiceentity.getBillingPeroidToDate() != null) {
//			periodendDateVal = new Phrase(sdf.format(invoiceentity.getBillingPeroidToDate()), font10);
//		} else {
//			periodendDateVal = new Phrase(" ", font10);
//		}
		
		periodendDateVal = new Phrase(toValue, font10);

		PdfPCell periodendDateValCell = new PdfPCell(periodendDateVal);
		// periodendDateValCell.addElement(periodendDateVal);
		periodendDateValCell.setBorder(0);
		periodendDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(periodstartDateCell);
		colonTable.addCell(colonCell);
		PdfPTable pdfBillingPeriodTable = new PdfPTable(4);
		pdfBillingPeriodTable.setWidthPercentage(100);
		try {
			pdfBillingPeriodTable
					.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfBillingPeriodTable.addCell(periodstartDateValCell);
		pdfBillingPeriodTable.addCell(periodendDateCell);
		pdfBillingPeriodTable.addCell(colonCell);
		pdfBillingPeriodTable.addCell(periodendDateValCell);
		
		state4Cell = new PdfPCell(pdfBillingPeriodTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell);
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * adding proforma invoice number on tax invoice. raised by Nithil and Nitin Sir for innovative
		 */
		if(invoiceentity.getInvoiceType().equals("Tax Invoice")){
			if(invoiceentity.getProformaCount()!=null&&invoiceentity.getProformaCount()!=0){
				Phrase blankphrase1 = new Phrase("", font10);
				PdfPCell blankCell1 = new PdfPCell();
				blankCell1.addElement(blankphrase1);
				blankCell1.setBorder(0);
				
				Phrase proformaPh = new Phrase("Proforma Invoice No", font10bold);
				PdfPCell proformaCell = new PdfPCell(proformaPh);
				proformaCell.setBorder(0);
				proformaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase proformaPh1 = new Phrase(invoiceentity.getProformaCount()+"", font10);
				PdfPCell proformaCell1 = new PdfPCell(proformaPh1);
				proformaCell1.setBorder(0);
				proformaCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				
				colonTable.addCell(proformaCell);
				colonTable.addCell(colonCell);
				
				PdfPTable pdfBillingPeriodTable1 = new PdfPTable(4);
				pdfBillingPeriodTable1.setWidthPercentage(100);
				try {
					pdfBillingPeriodTable1.setWidths(columnContractPeriodDateCodeCollonWidth);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
				
				pdfBillingPeriodTable1.addCell(proformaCell1);
				pdfBillingPeriodTable1.addCell(blankCell1);
				pdfBillingPeriodTable1.addCell(blankCell1);
				pdfBillingPeriodTable1.addCell(blankCell1);
				
				state4Cell = new PdfPCell(pdfBillingPeriodTable1);
				state4Cell.setBorder(0);
				state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				colonTable.addCell(state4Cell);
			}
		}

		// periodTableCell.addElement();

		PdfPCell concolonTableCell = new PdfPCell(concolonTable);
		concolonTableCell.setBorder(0);
		// concolonTableCell.addElement();

		PdfPCell billperiodtableCell = new PdfPCell(colonTable);
		billperiodtableCell.setBorder(0);
		// billperiodtableCell.addElement();

		part2Table.addCell(concolonTableCell);
		if (con.isContractRate()) {
			part2Table.addCell(periodTableCell);
		}
		part2Table.addCell(billperiodtableCell);

		PdfPCell part1Cell = new PdfPCell(part1Table);
		part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement();

		mainTable.addCell(part1Cell);

		part1Cell = new PdfPCell(part2Table);
		// part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement();
		mainTable.addCell(part1Cell);

		// mainTable.addCell(blankCell);
		// mainTable.addCell(blankCell);
		Phrase billingAddress = new Phrase("Billing Address", font8bold);
		// Paragraph billingpara=new Paragraph();
		// billingpara.add(billingAddress);
		// billingpara.setAlignment(Element.ALIGN_CENTER);
		// billingpara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		// billAdressCell.addElement(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		if(invoiceentity.isDonotprintServiceAddress()){
			billAdressCell.setColspan(2);
		}

		mainTable.addCell(billAdressCell);
		
		if(!invoiceentity.isDonotprintServiceAddress()){
			
		Phrase serviceaddress = new Phrase("Service Address", font8bold);
		// Paragraph servicepara=new Paragraph();
		// servicepara.add(serviceaddress);
		// servicepara.setAlignment(Element.ALIGN_CENTER);
		// servicepara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		// serviceCell.addElement(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		mainTable.addCell(serviceCell);
		
		}
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createProductDescription() {

		/**
		 * Date : 27-07-2017 By ANIL
		 */
		if (!productDescFlag) {
			return;
		}
		/**
		 * End
		 */

		PdfPTable prodDescriptionTbl = new PdfPTable(2);
		prodDescriptionTbl.setWidthPercentage(100);

		try {
			prodDescriptionTbl.setWidths(new float[] { 40, 60 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase prodDescriptionVal = new Phrase("", font8);
		String prodDescriptionValue = "";
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			Phrase productIdLbl = new Phrase("Product Id : "
					+ invoiceentity.getSalesOrderProducts().get(i).getProdId(),
					font10bold);
			PdfPCell productIdLblCell = new PdfPCell(productIdLbl);
			productIdLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			productIdLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productIdLblCell.setBorder(0);
			prodDescriptionTbl.addCell(productIdLblCell);

			String prodNameValue = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();

			Phrase prodNameLbl = new Phrase("Product Name : " + prodNameValue,
					font10bold);
			PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
			prodlblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodlblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodlblCell.setBorder(0);

			prodDescriptionTbl.addCell(prodlblCell);

			prodDescriptionValue = "";
			//Ashiwini Patil Date:13-09-2022 Product description 3 and 4 were not getting printed so commented old code written this new logic
			
			SuperProduct sup=invoiceentity.getSalesOrderProducts().get(i).getPrduct();

			if(sup.getComment()!=null&&!sup.getComment().equals("")){
				prodDescriptionValue = sup.getComment();
			}
			if(sup.getCommentdesc()!=null&&!sup.getCommentdesc().equals("")){
				prodDescriptionValue +="\n"+ sup.getCommentdesc();
			}
			if(sup.getCommentdesc1()!=null&&!sup.getCommentdesc1().equals("")){
				prodDescriptionValue +="\n"+ sup.getCommentdesc1();
			}
			if(sup.getCommentdesc2()!=null&&!sup.getCommentdesc2().equals("")){
				prodDescriptionValue +="\n"+ sup.getCommentdesc2();
			}

//			if (invoiceentity.getSalesOrderProducts().get(i).getProdDesc1() != null
//					&& !invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc1().equals("")
//					&& invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc2() != null
//					&& !invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc2().equals("")) {
//				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
//						.get(i).getProdDesc1()
//						+ invoiceentity.getSalesOrderProducts().get(i)
//								.getProdDesc2();
//			} else if (invoiceentity.getSalesOrderProducts().get(i)
//					.getProdDesc1() == null
//					&& invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc1().equals("")
//					&& invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc2() != null
//					&& !invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc2().equals("")) {
//				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
//						.get(i).getProdDesc2();
//			} else if (invoiceentity.getSalesOrderProducts().get(i)
//					.getProdDesc1() != null
//					&& !invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc1().equals("")
//					&& invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc2() == null
//					&& invoiceentity.getSalesOrderProducts().get(i)
//							.getProdDesc2().equals("")) {
//				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
//						.get(i).getProdDesc1();
//			} else {
//				prodDescriptionValue = "";
//
//			}

			prodDescriptionVal = new Phrase("" + prodDescriptionValue, font8);
		}
		Paragraph value = new Paragraph(prodDescriptionVal);
		value.setAlignment(Element.ALIGN_LEFT);

		try {
			/**
			 * Date :27-07-2017 By ANIL If no product description is found no
			 * table will be printed on pdf
			 */
			if (!prodDescriptionValue.equals("")) {
				document.add(Chunk.NEXTPAGE);
				document.add(prodDescriptionTbl);
				document.add(value);
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Added description code End Here
	 */

	// private void createStaticHeader(String preprintStatus) {
	// // TODO Auto-generated method stub
	// PdfPTable mainTable=new PdfPTable(2);
	// mainTable.setWidthPercentage(100);
	// try {
	// mainTable.setWidths(columnMoreLeftHeaderWidths);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// // rohan added this code for printing INVOICE heading when process config
	// is off
	// Phrase pdfHeading=null;
	// if(preprintStatus.equalsIgnoreCase("Plane")){
	// if(invoiceentity.getInvoiceType().equalsIgnoreCase("Proforma Invoice")){
	// pdfHeading=new Phrase("Proform Invoice",font14bold);
	// }
	// else{
	// pdfHeading=new Phrase("Tax Invoice",font14bold);
	// }
	//
	// }
	// else{
	// pdfHeading=new Phrase(" ",font14bold);
	// }
	//
	// Paragraph invPara=new Paragraph();
	// invPara.add(pdfHeading);
	// invPara.setAlignment(Element.ALIGN_RIGHT);
	// PdfPCell pdfHeadingCell=new PdfPCell();
	// pdfHeadingCell.addElement(invPara);
	// pdfHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// mainTable.addCell(pdfHeadingCell);
	//
	// PdfPTable partialTable=new PdfPTable(2);
	// partialTable.setWidthPercentage(100);
	// try {
	// partialTable.setWidths(columnMoreRightCheckBoxWidths);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// Phrase blankPhrase=new Phrase(" ",font10);
	// PdfPCell blankCell=new PdfPCell(blankPhrase);
	//
	// // blankCell.addElement(blankPhrase);
	// // blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// /*Just to create little spacing between boxes*/
	// // PdfPTable pdfTable=new PdfPTable(1);
	// // pdfTable.addCell(blankCell);
	// // pdfTable.setWidthPercentage(100);
	// //
	// // PdfPCell blank2Cell=new PdfPCell();
	// // blank2Cell.addElement(pdfTable);
	// // blank2Cell.setBorder(0);
	// // blank2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// Phrase stat1Phrase=new Phrase("Original for Receipient",font10);
	// PdfPCell stat1PhraseCell=new PdfPCell(stat1Phrase);
	// // stat1PhraseCell.addElement(stat1Phrase);
	// stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase stat2Phrase=new
	// Phrase("Duplicate for Supplier/Transporter",font10);
	// PdfPCell stat2PhraseCell=new PdfPCell(stat2Phrase);
	// // stat2PhraseCell.addElement(stat2Phrase);
	// stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase stat3Phrase=new Phrase("Triplicate for Supplier",font10);
	// PdfPCell stat3PhraseCell=new PdfPCell(stat3Phrase);
	// // stat3PhraseCell.addElement(stat3Phrase);
	// stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat1PhraseCell);
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat2PhraseCell);
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat3PhraseCell);
	//
	// PdfPCell pdfPCell=new PdfPCell();
	// pdfPCell.addElement(partialTable);
	// // pdfPCell.setBorder(0);
	// pdfPCell.setBorderWidthLeft(0);
	// pdfPCell.setBorderWidthBottom(0);
	// pdfPCell.setBorderWidthTop(0);
	//
	// mainTable.addCell(pdfPCell);
	// try {
	// document.add(mainTable);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	private void createHeader() {
		
		// Date 20/11/2017 By Jayshree
				// to add the logo in Table
				DocumentUpload logodocument = comp.getLogo();

				// patch
				String hostUrl;
				String environment = System
						.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
					String applicationId = System
							.getProperty("com.google.appengine.application.id");
					String version = System
							.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId
							+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}
				PdfPCell imageSignCell = null;
				Image image2 = null;
				try {
					image2 = Image
							.getInstance(new URL(hostUrl + logodocument.getUrl()));
					image2.scalePercent(20f);
					// image2.setAbsolutePosition(40f,765f);
					// doc.add(image2);

					imageSignCell = new PdfPCell(image2);
					imageSignCell.setBorder(0);
					imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					imageSignCell.setFixedHeight(40);
				} catch (Exception e) {
					e.printStackTrace();
				}
				/**
				 * Ends For Jayshree
				 */

				/**
				 * Developer : Jayshree Date : 21 Nov 2017 Description : Logo position
				 * adjustment
				 */
				PdfPTable logoTab = new PdfPTable(1);
				logoTab.setWidthPercentage(100);

				if (imageSignCell != null) {
					logoTab.addCell(imageSignCell);
				} else {
					Phrase logoblank = new Phrase(" ");
					PdfPCell logoblankcell = new PdfPCell(logoblank);
					logoblankcell.setBorder(0);
					logoTab.addCell(logoblankcell);
				}
				/**
				 * Ends for Jayshree
				 */
				
				
		Phrase companyName = null;
		if (comp != null) {
			companyName = new Phrase(comp.getBusinessUnitName().trim(),
					font16bold);
		}

		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(companyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyAddr = null;
		if (comp != null) {
			companyAddr = new Phrase("          "
					+ comp.getAddress().getCompleteAddress().trim(), font12);
		}
		Paragraph companyAddrpara = new Paragraph();
		companyAddrpara.add(companyAddr);
		companyAddrpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyAddrCell = new PdfPCell();
		companyAddrCell.addElement(companyAddrpara);
		companyAddrCell.setBorder(0);
		companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyGSTTIN = null;
		String gstinValue = "";
		if (UniversalFlag) {
			if (con.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
						break;
					}
				}

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (!comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = gstinValue
								+ ","
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
					}
				}
			}
		} else {

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
					break;
				}
			}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = gstinValue
							+ ","
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
				}
			}
		}

		if (!gstinValue.equals("")) {
			companyGSTTIN = new Phrase(gstinValue, font12bold);
		}

		Paragraph companyGSTTINpara = new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyGSTTINCell = new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		String email = "       Email : " + comp.getEmail();
		String number = "";
		if (comp.getCellNumber2() != 0) {
			number = "Mobile " + comp.getCellNumber1() + ","
					+ comp.getCellNumber2();
		} else {
			number = "Mobile " + comp.getCellNumber1();
		}

		PdfPCell companyEmailandNumberCell = new PdfPCell(new Phrase(email
				+ " " + number, font12));
		// companyGSTTINCell.addElement(companyGSTTINpara);
		companyEmailandNumberCell.setBorder(0);
		companyEmailandNumberCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pdfPTable = new PdfPTable(1);
		// pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		pdfPTable.addCell(companyEmailandNumberCell);

//		try {
//			document.add(pdfPTable);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		/**
		 * Developer:Jayshree Date 21/11/2017 Description:changes are done to
		 * add the logo and website at proper position
		 */
		PdfPTable mainheader = new PdfPTable(2);
		mainheader.setWidthPercentage(100);

		try {
			mainheader.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		

		if(con.getNumberRange()!=null &&!con.getNumberRange().equals("")){
		if(nonbillingInvoice==true &&con.getNumberRange().equalsIgnoreCase("NonBilling")){
			Phrase blank =new Phrase(" ");
			PdfPCell blankcell = new PdfPCell(blank);
			blankcell.setBorder(0);
			blankcell.setColspan(2);
			mainheader.addCell(blankcell);
		}
		else{
			if (imageSignCell != null) {
			PdfPCell leftCell = new PdfPCell(logoTab);
			leftCell.setBorder(0);
			mainheader.addCell(leftCell);

			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			mainheader.addCell(rightCell);
		} else {
			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			mainheader.addCell(rightCell);
		
				}
			}
		}
		else{
			if (imageSignCell != null) {
			PdfPCell leftCell = new PdfPCell(logoTab);
			leftCell.setBorder(0);
			mainheader.addCell(leftCell);

			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			mainheader.addCell(rightCell);
		} else {
			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			mainheader.addCell(rightCell);
		
				}
	
	
		
		}
		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// rohan added this code
		float[] myWidth = { 1, 3, 20, 17, 3, 30, 17, 3, 20, 1 };
		PdfPTable mytbale = new PdfPTable(10);
		mytbale.setWidthPercentage(100f);
		mytbale.setSpacingAfter(5f);
		mytbale.setSpacingBefore(5f);

		try {
			mytbale.setWidths(myWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Phrase myblank = new Phrase(" ", font10);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero = new Phrase(" ", font10);
		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat1Phrase = new Phrase("Original for Receipient", font10);
		PdfPCell stat1PhraseCell = new PdfPCell(stat1Phrase);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stat1PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase stat2Phrase = new Phrase("Duplicate for Supplier/Transporter",
				font10);
		PdfPCell stat2PhraseCell = new PdfPCell(stat2Phrase);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stat2PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase stat3Phrase = new Phrase("Triplicate for Supplier", font10);
		PdfPCell stat3PhraseCell = new PdfPCell(stat3Phrase);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stat3PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		// mytbale.addCell(stat1PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		// mytbale.addCell(stat2PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		// mytbale.addCell(stat3PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(mytbale);
		tab.addCell(cell);
		// try {
		// // document.add(tab);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		// ends here
		String titlepdf = "";

//		if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
//				.getInvoiceType().trim())
//				|| invoiceentity.getInvoiceType().trim()
//						.equals(AppConstants.CREATEPROFORMAINVOICE))
//			//titlepdf = "Proforma Invoice";
//			if(changeTitle){
//				titlepdf = "Invoice";
//			}else{
//				titlepdf = "Proforma Invoice";
//			}
//		else
//			titlepdf = "Tax Invoice";

		if(nonbillingInvoice==true){
			if(invoiceentity.getBillingTaxes().size()==0){
				/**Date 25-9-2020 by Amol commented this line raised by Rahul Tiwari.**/
				titlepdf = "Estimate";
				
				/**Date 23-12-2020 by Priyanka commented this line raised by Ashwini for Om pest control.**/
	//			titlepdf = "Invoice";
				
				/**
				 * @author Anil @since 09-04-2021
				 * If non billing process configuration is active and no tax selected then for Proforma invoice it should print title as
				 * Proforma Invoice instead Estimate
				 * Raised by Ashwini for Ultra Pest Control
				 */
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())){
					titlepdf = "Proforma Invoice";
				}
			}
		else{
			if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
					.getInvoiceType().trim())
					|| invoiceentity.getInvoiceType().trim()
							.equals(AppConstants.CREATEPROFORMAINVOICE))
				titlepdf = "Proforma Invoice";
			else
				titlepdf = "Tax Invoice";
			
			}
		}
		else{
			/**
			 * @author Anil @since 12-04-2021
			 * For ultra pest control, if no tax is selected and non billing process configurationj is off then print 
			 * Invoice on PDF else it will be Tax Invoice
			 * Raised by Ashwini 
			 */
//			if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
//				titlepdf = "Proforma Invoice";
//			else
//				titlepdf = "Tax Invoice";
			
			titlepdf = "Invoice";
			if (invoiceentity.getBillingTaxes().size() == 0) {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())) {
					titlepdf = "Proforma Invoice";
				}

			} else {
				if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())|| invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE)){
					titlepdf = "Proforma Invoice";
				}else{
					titlepdf = "Tax Invoice";
				}
			}
			
			
		}
		/**
		 * @author Anil @since 01-10-2021
		 */
		titlepdf=pdfUtility.getInvoiceTitle(invoiceentity, branchDt, titlepdf);
		logger.log(Level.SEVERE,"After getInvoiceTitle Method : "+titlepdf +" | "+pdfUtility.printBankDetailsFlag);
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * if we are printing invoice for thai client then title will be hard coded as provided by client
		 * raised by Nitin And Nithila
		 */
		if(thaiPdfFlag){
			if(!invoiceTitle.equals("")){
				titlepdf=invoiceTitle;
			}
		}
		
		Phrase titlephrase = new Phrase(titlepdf, titlefont); //Ashwini Patil
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		/**
		 * @author Anil
		 * @since 19-01-2022
		 * Need to add invoice copy name 
		 * raised by Nithila and Nitin for Innovative
		 */
		if(thaiPdfFlag){
			if(!copyTitle.equals("")){
				parent.addCell(pdfUtility.getCell(copyTitle, font8bold, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			}
		}

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private ArrayList<ArticleType> getArticleBranchList(ArrayList<ArticleType> articleTypeDetails, String branch) {
		ArrayList<ArticleType> artilist =new ArrayList<ArticleType>();
		for(ArticleType at:articleTypeDetails){
			logger.log(Level.SEVERE,"at.getArticleDescription() " +at.getArticleDescription()+" / "+branch);
			/**
			 * @author Vijay Date :- 11-08-2021
			 * Des :- added 2 condition description!=null && description not blank then only it will check branch to manage 2 different companies
			 * otherwise it will work normal flow 
			 */
			if(at.getArticleDescription()!=null && !at.getArticleDescription().equals("") ){
				if(at.getArticleDescription().equals(branch)){
					artilist.add(at);
				}
				
				logger.log(Level.SEVERE,"Inside Branch As Company Method:" +artilist);
			}
			else{
				artilist.add(at);
			}
			
		}
		
		return artilist;
	}
	
	private int getNumberOfServicesFromContract(String pcode,List<Contract> conlist) {
		int no=0;
		if(conlist!=null) {
			for(Contract cont:conlist) {
				for(int l =0;l<cont.getItems().size();l++){
					if(cont.getItems().get(l).getProductCode().equals(pcode)){						
						no	+= cont.getItems().get(l).getNumberOfServices();		
						
					}
				}
			}		
		}
		return no;
	}
}
