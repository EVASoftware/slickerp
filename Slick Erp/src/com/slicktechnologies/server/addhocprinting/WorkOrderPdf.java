package com.slicktechnologies.server.addhocprinting;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.net.URL;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrderProductDetails;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class WorkOrderPdf {
	
	Contract co;
	SalesOrder so;
	WorkOrder wo;
	List<WorkOrder> workorder;
	List<WorkOrderProductDetails> products;
	ArrayList<SuperProduct>stringlis= new ArrayList<SuperProduct>();
	Company comp;
	public Document document;
	SuperProduct sup;
/****************************************************************************************/

	int flag=0;
	Phrase chunk;
	int firstBreakPoint = 25;
	int BreakPoint=5;
	float size;
	int count=0;
	int rohan=123;
	float blankLines;
	
	PdfPCell pdfsrno,pdfitemname,pdfgrnqty,pdfaccpqty,pdfrejqty,pdfreasonforrej,pdfuom;
	
	Logger logger=Logger.getLogger("Purchaseorder");
	
//	float[] columnWidths3 = {1.0f, 0.1f, 1.0f, 1.0f, 0.1f, 1.0f,1.0f, 0.1f, 1.0f};
	float[] columnWidths3 = {1.0f, 0.1f, 1.0f, 1.0f, 0.1f, 1.0f};
	float[] columnWidths = {0.8f, 2.5f, 1.2f, 1.2f,1.2f,1.2f,1.2f};
	float[] columnWidths2={1.5f,0.2f,1.2f,1.6f,0.2f,1.4f};
	private Font font16boldul, font12bold, font8bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	final static String disclaimerText = "I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";

	public WorkOrderPdf() {
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	}

	public void setWorkOrder(long count) {
		// Load WorkOrder
		wo = ofy().load().type(WorkOrder.class).id(count).now();
	
		// Load Company
		if (wo.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", wo.getCompanyId()).first().now();
	
		for(int i=0;i<wo.getBomTable().size();i++){
			if(wo.getCompanyId()!=null){
				sup=ofy().load().type(SuperProduct.class).filter("companyId",wo.getCompanyId()).
				filter("count", wo.getBomTable().get(i).getPrduct().getCount()).first().now();
				
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
				
				stringlis.add(superprod);
			}
			else{
				sup=ofy().load().type(SuperProduct.class).filter("count", wo.getWoTable().get(i).getProductId()).first().now();
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
			}
		}
		if(wo.getOrderId()!=0){
			so=ofy().load().type(SalesOrder.class).filter("companyId", wo.getCompanyId()).filter("count", wo.getOrderId()).first().now();
			
			if(so==null){
				co=ofy().load().type(Contract.class).filter("companyId", wo.getCompanyId()).filter("count", wo.getOrderId()).first().now();
			}
		}
		
		
		products = wo.getWoTable();

	}
	public  void createPdfForEmail(Company comp,WorkOrder wo) {
		 
	    this.comp=comp;
	    this.wo=wo;
	    this.products=wo.getWoTable();
	    for(int i=0;i<wo.getBomTable().size();i++){
			if(wo.getCompanyId()!=null){
				sup=ofy().load().type(SuperProduct.class).filter("companyId",wo.getCompanyId()).
				filter("count", wo.getBomTable().get(i).getPrduct().getCount()).first().now();
				
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment()+" "+superprod.getCommentdesc());
				
				stringlis.add(superprod);
			}
			else{
				sup=ofy().load().type(SuperProduct.class).filter("count", wo.getWoTable().get(i).getProductId()).first().now();
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment());
			}
		}
	    if(wo.getOrderId()!=0){
			so=ofy().load().type(SalesOrder.class).filter("companyId", wo.getCompanyId()).filter("count", wo.getOrderId()).first().now();
			
			if(so==null){
				co=ofy().load().type(Contract.class).filter("companyId", wo.getCompanyId()).filter("count", wo.getOrderId()).first().now();
			}
		}
	    
	    
	    Createblank();
	    if(comp.getLogo()!=null)
	    {
//	    	createLogo(document,comp);// Date 22/11/2017 Comment by Jayshree to remove logo method
	    }
	    createCompanyAddress();
	    CreateRefDetails();
		createProductInfo();
		footerInfo();
	      
	}	
	
	
	public void createPdf() {
		Createblank();
//		createLogo(document,comp);// Date 22/11/2017 Comment by Jayshree to remove logo method
		createCompanyAddress();
		CreateRefDetails();
		createProductInfo();
		footerInfo();
	}
	
	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	
	public  void createCompanyAddress()
	{
		//Dev.By Jayshree
		//Date 22/11/2017 
		//Des.to add the logo in Table
		DocumentUpload logodocument =comp.getLogo();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		Image image1=null;
//		try
//		{
//		 image1=Image.getInstance("images/ipclogo4.jpg");//images/ipclogo4.jpg
//		image1.scalePercent(20f);
////		image1.setAbsolutePosition(40f,765f);	
////		doc.add(image1);
//		
//		
//		
//		
//		
//		imageSignCell=new PdfPCell();
//		imageSignCell.addElement(image1);
//		imageSignCell.setFixedHeight(25);
//		imageSignCell.setBorder(0);
//		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}

		
		PdfPTable logoTab=new PdfPTable(1);
		logoTab.setWidthPercentage(100);
		
		if(imageSignCell!=null)
		{
			System.out.println("check image present ");
			logoTab.addCell(imageSignCell);
		}
		else
		{
			System.out.println("check image not present ");
			Phrase logoblank=new Phrase(" ");
			PdfPCell logoblankcell=new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
/**
 * End
 */
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
		/**
		 * date 22/11/2017
		 * By Jayshree
		 * add the logoTab 
		 */
		
		PdfPTable header=new PdfPTable(2);
		header.setWidthPercentage(100);
		
		try {
			header.setWidths(new float[]{15,75});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(imageSignCell!=null){
			System.out.println("image present");
		PdfPCell left=new PdfPCell(logoTab);
		left.setBorder(0);
		header.addCell(left);
		
		PdfPCell right=new PdfPCell(companyDetails);
		right.setBorder(0);
		header.addCell(right);
		}
		else
		{
			System.out.println("image not present");
			PdfPCell right=new PdfPCell(companyDetails);
			right.setBorder(0);
			right.setColspan(2);
			header.addCell(right);
			
		}
		//End By Jayshree
		
		String title1 = "";
		title1 = "Work Order";
	
		String countinfo = "";
		WorkOrder purentity = (WorkOrder) wo;
		countinfo = "ID : "+purentity.getCount() + "";
	
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String creationdateinfo = "";
		WorkOrder purchentity = (WorkOrder) wo;
		creationdateinfo = "Date: " + fmt.format(purchentity.getCreationDate());
	
		Phrase titlephrase = new Phrase(title1, font12bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);
	
		Phrase idphrase = new Phrase(countinfo, font12);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);
	
		Phrase dateofpdf = new Phrase(creationdateinfo, font12);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);
	
		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);
//		titlepdftable.setSpacingBefore(10f);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(header);//By Jayshree add the header tab in cell
//		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
//	public void createCompanyHedding() {
//
//		PdfPTable parentTable=new PdfPTable(1);
//		parentTable.setWidthPercentage(100);
//		
//		Phrase companyName = new Phrase(comp.getBusinessUnitName(),font16boldul);
//		Paragraph p = new Paragraph();
//
//		p.add(Chunk.NEWLINE);
//		p.add(companyName);
//		p.setAlignment(Element.ALIGN_CENTER);
//		
//		PdfPCell comCell=new PdfPCell();
//		comCell.addElement(p);
//		comCell.setBorder(0);
//		parentTable.addCell(comCell);
//		
//		
//		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),font12);
//		Phrase adressline2 = null;
//		if (comp.getAddress().getAddrLine2() != null) {
//			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font12);
//		}
//
//		Phrase landmark = null;
//		Phrase locality = null;
//		if (comp.getAddress().getLandmark() != null && comp.getAddress().getLocality().equals("") == false) {
//			String landmarks = comp.getAddress().getLandmark() + " , ";
//			locality = new Phrase(landmarks + comp.getAddress().getLocality()
//					+ " , " + comp.getAddress().getCity() + " , "
//					+ comp.getAddress().getPin(), font12);
//		} else {
//			locality = new Phrase(comp.getAddress().getLocality() + " , "
//					+ comp.getAddress().getCity() + " , "
//					+ comp.getAddress().getPin(), font12);
//		}
//		
//		Paragraph adressPragraph = new Paragraph();
//		adressPragraph.add(adressline1);
//		adressPragraph.add(Chunk.NEWLINE);
//		if (adressline2 != null) {
//			adressPragraph.add(adressline2);
//			adressPragraph.add(Chunk.NEWLINE);
//		}
//
//		adressPragraph.add(locality);
//
//		adressPragraph.setAlignment(Element.ALIGN_CENTER);
//		adressPragraph.setSpacingBefore(10);
//		adressPragraph.setSpacingAfter(10);
//
//		// Phrase for phone,landline ,fax and email
//
//		Phrase titlecell = new Phrase("Mob :", font8bold);
//		Phrase titleTele = new Phrase("Tele :", font8bold);
//		Phrase titleemail = new Phrase("Email :", font8bold);
//		Phrase titlefax = new Phrase("Fax :", font8bold);
//
//		Phrase titleservicetax = new Phrase("Service Tax No     :", font8bold);
//		Phrase titlevatatx = new Phrase("Vat Tax No :    ", font8bold);
//		String servicetax = comp.getServiceTaxNo();
//		String vatttax = comp.getVatTaxNo();
//		Phrase servicetaxphrase = null;
//		Phrase vattaxphrase = null;
//
//		if (servicetax != null && servicetax.trim().equals("") == false) {
//			servicetaxphrase = new Phrase(servicetax, font8);
//		}
//
//		if (vatttax != null && vatttax.trim().equals("") == false) {
//			vattaxphrase = new Phrase(vatttax, font8);
//		}
//
//		// cell number logic
//		String stringcell1 = comp.getContact().get(0).getCellNo1() + "";
//		String stringcell2 = null;
//		Phrase mob = null;
//		if (comp.getContact().get(0).getCellNo2() != -1)
//			stringcell2 = comp.getContact().get(0).getCellNo2() + "";
//		if (stringcell2 != null)
//			mob = new Phrase(stringcell1 + " / " + stringcell2, font8);
//		else
//			mob = new Phrase(stringcell1, font8);
//
//		// LANDLINE LOGIC
//		Phrase landline = null;
//		if (comp.getContact().get(0).getLandline() != -1)
//			landline = new Phrase(comp.getContact().get(0).getLandline() + "",
//					font8);
//
//		// fax logic
//		Phrase fax = null;
//		if (comp.getContact().get(0).getFaxNo() != null)
//			fax = new Phrase(comp.getContact().get(0).getFaxNo() + "", font8);
//		// email logic
//		Phrase email = new Phrase(comp.getContact().get(0).getEmail(), font8);
//
//		adressPragraph.add(Chunk.NEWLINE);
//		adressPragraph.add(titlecell);
//		adressPragraph.add(mob);
//		adressPragraph.add(new Chunk("            "));
//
//		if (landline != null) {
//
//			adressPragraph.add(titleTele);
//			adressPragraph.add(landline);
//			adressPragraph.add(new Chunk("            "));
//		}
//
//		if (fax != null) {
//
//			adressPragraph.add(titlefax);
//			adressPragraph.add(fax);
//			adressPragraph.add(Chunk.NEWLINE);
//		}
//
//		adressPragraph.add(titleemail);
//		adressPragraph.add(new Chunk("            "));
//		adressPragraph.add(email);
//		adressPragraph.add(Chunk.NEWLINE);
//
//		if (servicetaxphrase != null) {
//			adressPragraph.add(titleservicetax);
//			adressPragraph.add(servicetaxphrase);
//
//		}
//
//		if (vattaxphrase != null) {
//			adressPragraph.add(titlevatatx);
//			adressPragraph.add(vattaxphrase);
//			adressPragraph.add(new Chunk("            "));
//
//		}
//
//		PdfPCell newcell =new PdfPCell();
//		newcell.addElement(adressPragraph);
//		newcell.setBorder(0);
//		parentTable.addCell(newcell);
//
//		PdfPTable superParentTable=new PdfPTable(1);
//		superParentTable.setWidthPercentage(100);
//		superParentTable.addCell(parentTable);
//		
//		
//		
//		
//		
//		
//		
//		String title1 = "";
//		title1 = "Work Order";
//	
//		String countinfo = "";
//		WorkOrder purentity = (WorkOrder) wo;
//		countinfo = "ID : "+purentity.getCount() + "";
//	
//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		String creationdateinfo = "";
//		WorkOrder purchentity = (WorkOrder) wo;
//		creationdateinfo = "Date: " + fmt.format(purchentity.getCreationDate());
//	
//		Phrase titlephrase = new Phrase(title1, font12bold);
//		Paragraph titlepdfpara = new Paragraph();
//		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//		titlepdfpara.add(Chunk.NEWLINE);
//		PdfPCell titlepdfcell = new PdfPCell();
//		titlepdfcell.addElement(titlepdfpara);
//		titlepdfcell.setBorderWidthRight(0);
//		titlepdfcell.setBorderWidthLeft(0);
//		titlepdfcell.addElement(Chunk.NEWLINE);
//	
//		Phrase idphrase = new Phrase(countinfo, font12);
//		Paragraph idofpdfpara = new Paragraph();
//		idofpdfpara.add(idphrase);
//		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
//		idofpdfpara.add(Chunk.NEWLINE);
//		PdfPCell countcell = new PdfPCell();
//		countcell.addElement(idofpdfpara);
//		countcell.setBorderWidthRight(0);
//		countcell.addElement(Chunk.NEWLINE);
//	
//		Phrase dateofpdf = new Phrase(creationdateinfo, font12);
//		Paragraph creatndatepara = new Paragraph();
//		creatndatepara.add(dateofpdf);
//		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
//		creatndatepara.add(Chunk.NEWLINE);
//		PdfPCell creationcell = new PdfPCell();
//		creationcell.addElement(creatndatepara);
//		creationcell.setBorderWidthLeft(0);
//		creationcell.addElement(Chunk.NEWLINE);
//	
//		PdfPTable titlepdftable = new PdfPTable(3);
//		titlepdftable.setWidthPercentage(100);
//		titlepdftable.addCell(countcell);
//		titlepdftable.addCell(titlepdfcell);
//		titlepdftable.addCell(creationcell);
//		
//		
//		
//		try {
//			document.add(superParentTable);
//			document.add(titlepdftable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
//	}

	private void CreateRefDetails() {
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		Phrase semicolon =new Phrase(":",font1);
		PdfPCell semicolonCell=new PdfPCell(semicolon);
		semicolonCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		semicolonCell.setBorder(0);
		
		PdfPTable table = new PdfPTable(6);
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase refno = new Phrase("REF. NO.", font1);
		Phrase refdate = new Phrase("REF. DATE", font1);
		Phrase completiondate = new Phrase("DUE DATE",font1);
		Phrase grnid = new Phrase("ORDER ID", font1);
		Phrase grndate = new Phrase("ORDER DATE", font1);
		Phrase inspectedby = new Phrase("APPROVER", font1);
		Phrase custid = new Phrase("CUSTOMER ID", font1);
		Phrase custname = new Phrase("CUSTOMER NAME", font1);
		Phrase custcell = new Phrase("CUSTOMER CELL", font1);
		
		Phrase perscell = new Phrase("PERSON RESPONSIBLE", font1);
		
		
	        
	      
	        	 
		PdfPCell cellrefno = new PdfPCell(refno);
		cellrefno.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefno.setBorder(0);
		
		PdfPCell cellrefdate = new PdfPCell(refdate);
		cellrefdate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefdate.setBorder(0);
		
		PdfPCell cellcompldate = new PdfPCell(completiondate);
		cellcompldate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcompldate.setBorder(0);
		
		PdfPCell cellgrnid = new PdfPCell(grnid);
		cellgrnid.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrnid.setBorder(0);
		
		PdfPCell cellgrndate = new PdfPCell(grndate);
		cellgrndate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrndate.setBorder(0);
		
		PdfPCell cellinspectedby = new PdfPCell(inspectedby);
		cellinspectedby.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinspectedby.setBorder(0);
		
		PdfPCell cellcustid = new PdfPCell(custid);
		cellcustid.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcustid.setBorder(0);
		
		PdfPCell cellcustname = new PdfPCell(custname);
		cellcustname.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcustname.setBorder(0);
		
		PdfPCell cellcustcell = new PdfPCell(custcell);
		cellcustcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcustcell.setBorder(0);
		
		
		//Person Responsible
		
		PdfPCell cellperscell = new PdfPCell(perscell);
		cellperscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellperscell.setBorder(0);
		
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(wo.getRefNum()!=null){
			chunk = new Phrase(wo.getRefNum(), font8);
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellrefnoval = new PdfPCell(chunk);
		cellrefnoval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefnoval.setBorder(0);
		
		if(wo.getRefDate()!=null){
			chunk = new Phrase(fmt.format(wo.getRefDate()) + "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellrefdateval = new PdfPCell(chunk);
		cellrefdateval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellrefdateval.setBorder(0);
		
		if(wo.getCompletionDate()!=null){
			chunk = new Phrase(fmt.format(wo.getCompletionDate())+"", font8);
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellorderidval = new PdfPCell(chunk);
		cellorderidval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellorderidval.setBorder(0);
		
		if(wo.getOrderId()!=0){
			chunk = new Phrase(wo.getOrderId()+"", font8);
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellgrnidval = new PdfPCell(chunk);
		cellgrnidval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrnidval.setBorder(0);
		
		if(wo.getOrderDate()!=null){
			chunk = new Phrase(fmt.format(wo.getOrderDate()) + "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellgrndateval = new PdfPCell(chunk);
		cellgrndateval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellgrndateval.setBorder(0);
		
		if(wo.getApproverName()!=null){
			chunk = new Phrase(wo.getApproverName()+ "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellinspectedbyval = new PdfPCell(chunk);
		cellinspectedbyval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellinspectedbyval.setBorder(0);
		
		
		//Person responsible
		
		if(wo.getEmployee()!=null){
			chunk = new Phrase(wo.getEmployee()+ "", font8);
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellpersonval = new PdfPCell(chunk);
		cellpersonval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpersonval.setBorder(0);
		
		
		
		if(wo.getOrderId()!=0){
			if(so!=null){
				chunk = new Phrase(so.getCustomerId()+"", font8);
			}else{
				chunk = new Phrase(co.getCustomerId()+"", font8);
			}
		}else{
			 chunk = new Phrase("");
		}
		PdfPCell cellcustidval = new PdfPCell(chunk);
		cellcustidval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcustidval.setBorder(0);
		
		if(wo.getOrderDate()!=null){
			if(so!=null){
				chunk = new Phrase(so.getCustomerFullName() + "", font8);
			}else{
				chunk = new Phrase(co.getCustomerFullName() + "", font8);
			}
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellcustnameval = new PdfPCell(chunk);
		cellcustnameval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcustnameval.setBorder(0);
		
		if(wo.getOrderDate()!=null){
			if(so!=null){
				chunk = new Phrase(so.getCustomerCellNumber()+ "", font8);
			}else{
				chunk = new Phrase(co.getCustomerCellNumber()+ "", font8);
			}
		}else{
			chunk = new Phrase("");
		}
		PdfPCell cellcustcellval = new PdfPCell(chunk);
		cellcustcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellcustcellval.setBorder(0);
		
		
		
		
		
		
		table.addCell(cellrefno);
		table.addCell(semicolonCell);
		table.addCell(cellrefnoval);
		
		table.addCell(cellrefdate);
		table.addCell(semicolonCell);
		table.addCell(cellrefdateval);
		
		//order ID
		table.addCell(cellgrnid);
		table.addCell(semicolonCell);
		table.addCell(cellgrnidval);
				
		//order Date
		table.addCell(cellgrndate);
		table.addCell(semicolonCell);
		table.addCell(cellgrndateval);
		
		
		//Approver
		table.addCell(cellinspectedby);
		table.addCell(semicolonCell);
		table.addCell(cellinspectedbyval);
		
		//Person responsible
		table.addCell(cellperscell);
		table.addCell(semicolonCell);
		table.addCell(cellpersonval);		
		
		//Cust Id
		table.addCell(cellcustid);
		table.addCell(semicolonCell);
		table.addCell(cellcustidval);
		
		//CustName
		table.addCell(cellcustname);
		table.addCell(semicolonCell);
		table.addCell(cellcustnameval);
		
		//Cust Cell
		table.addCell(cellcustcell);
		table.addCell(semicolonCell);
		table.addCell(cellcustcellval);

		//Due Date
		table.addCell(cellcompldate);
		table.addCell(semicolonCell);
		table.addCell(cellorderidval);		
		
		
		Phrase blank = new Phrase(" ", font1);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		
		
//		table.addCell(blankcell);
//		table.addCell(blankcell);
//		table.addCell(blankcell);
//		
//		table.addCell(blankcell);
//		table.addCell(blankcell);
//		table.addCell(blankcell);
		
		
		PdfPCell tablecell =new PdfPCell();
		tablecell.addElement(table);
		
		
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(tablecell);
		
		
		
		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
	}


	public void createProductInfo() {
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		  PdfPCell parentcell=new PdfPCell();
		  
		  
		  if( this.products.size()<= firstBreakPoint){
				int size =  firstBreakPoint - this.products.size();
				blankLines = size*(160/15);
				System.out.println("blankLines size ="+blankLines);
				System.out.println("blankLines size ="+blankLines);
			}
			else{
				blankLines = 10f;
			}
			
		
		for(int k=0;k<wo.getBomTable().size();k++){
		
		
		// For Product Label & Value
		Phrase productphrase=new Phrase("Product : "+wo.getBomTable().get(k).getProductName(),font10bold);
		Paragraph productpara=new Paragraph(productphrase);
		
		
		//For Product Description
		
		Phrase productdesc=null;
		String prodDesc=null;
		
		for(int i=0;i<this.stringlis.size();i++){
			if(wo.getBomTable().get(k).getPrduct().getCount()==stringlis.get(i).getCount()){
				 if(!stringlis.get(i).getComment().equals("")){
					 productdesc = new Phrase(stringlis.get(i).getComment(),font8);
					 prodDesc=stringlis.get(i).getComment();
					System.out.println("pro coment+++   "+stringlis.get(i).getComment());
				 }else{
					 productdesc = new Phrase(" ",font8);
				 }
			}
		 }
		Paragraph prodesc=new Paragraph(productdesc); 
		
		// For Material Label
		
		Paragraph para = null;
		
		Phrase productdetails= new Phrase("Material Required",font10bold);
		para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		
		
		
		//Table with product details
	
		PdfPTable table = new PdfPTable(7);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase srno = new Phrase("SR.NO.", font1);
		Phrase refno = new Phrase("ITEM DETAILS", font1);
		Phrase grnqty = new Phrase("CODE",font1);
		Phrase accqty = new Phrase("CATEGORY", font1);
		Phrase rejqty = new Phrase("QUANTITY", font1);
		Phrase uom = new Phrase("UOM", font1);
		Phrase remark = new Phrase("PRICE", font1);
		
		
		
	        
	      
	        	 
		PdfPCell cellsrno = new PdfPCell(srno);
		cellsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcategory = new PdfPCell(refno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(grnqty);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(accqty);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rejqty);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(remark);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celluom = new PdfPCell(uom);
		celluom.setHorizontalAlignment(Element.ALIGN_CENTER);
	
		table.addCell(cellsrno);
		table.addCell(cellcategory);
		table.addCell(cellrate);
		table.addCell(cellperdisc);
		table.addCell(cellservicetax);
		table.addCell(celluom);
		table.addCell(celltotal);
		
		
		
		for (int i = 0; i < this.products.size(); i++) {
			
			if(wo.getBomTable().get(k).getPrduct().getCount()==this.products.get(i).getCompProdId()){
				
				chunk = new Phrase((i+1)+"",font8);
				pdfsrno = new PdfPCell(chunk);
				pdfsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
					
				if(products.get(i).getProductName()!=null){
					chunk = new Phrase(products.get(i).getProductName(), font8);
				}else{
					 chunk = new Phrase("");
				}
				pdfitemname = new PdfPCell(chunk);
				
				if(products.get(i).getProductCode()!=null){
					chunk = new Phrase(products.get(i).getProductCode() + "", font8);
				}else{
					chunk = new Phrase("");
				}
				
				pdfgrnqty = new PdfPCell(chunk);
				pdfgrnqty.setHorizontalAlignment(Element.ALIGN_CENTER);
				 
				 
				if(products.get(i).getProductCategory()!=null){
					 chunk =new Phrase (products.get(i).getProductCategory()+"",font8);
				}
				else{
					 chunk= new Phrase("");
				}
				pdfaccpqty = new PdfPCell(chunk);
				pdfaccpqty.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				if(products.get(i).getProductQty()!=0){
					 chunk =new Phrase (products.get(i).getProductQty()+"",font8);
				}
				else{
					 chunk= new Phrase("");
				}
				pdfrejqty = new PdfPCell(chunk);
				pdfrejqty.setHorizontalAlignment(Element.ALIGN_CENTER);
				
					
				if(products.get(i).getProductPrice()!=0){
					chunk = new Phrase(products.get(i).getProductPrice()+"", font8);
				}else{
					 chunk = new Phrase("");
				}
				pdfreasonforrej = new PdfPCell(chunk);
				pdfreasonforrej.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				if(products.get(i).getProductUOM()!=null){
					chunk = new Phrase(products.get(i).getProductUOM()+"", font8);
				}else{
					 chunk = new Phrase("");
				}
				pdfuom = new PdfPCell(chunk);
				pdfuom.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				table.addCell(pdfsrno);
				table.addCell(pdfitemname);
				table.addCell(pdfgrnqty);
				table.addCell(pdfaccpqty);
				table.addCell(pdfrejqty);
				table.addCell(pdfuom);
				table.addCell(pdfreasonforrej);
				
				
				
			
			}
			
//			if(count==this.products.size()|| count==firstBreakPoint){
//    		rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
//			flag=firstBreakPoint;
//			break;
//		}
//		System.out.println("flag value" + flag);
//		if(firstBreakPoint==flag){
//			footerInfo();
//		}
		
			
			  if(k==wo.getBomTable().size()-1){
				  table.setSpacingAfter(blankLines);
			  }
			
			
		}
		
			PdfPCell prodtablecell=new PdfPCell();
			
			prodtablecell.addElement(productpara);
			System.out.println(" iiiiiiiiii ");
			if(prodDesc!=null){
				System.out.println(" inside phrase not null");
				prodtablecell.addElement(prodesc);
			}
			prodtablecell.addElement(para);
			prodtablecell.addElement(table);
//			prodtablecell.addElement(rephrse);
			
			PdfPTable parentTableProd=new PdfPTable(1);
		    parentTableProd.setWidthPercentage(100);
		    parentTableProd.addCell(prodtablecell);
		    parentcell.addElement(parentTableProd);
		  
		   
		    
		    
		    try {
				document.add(parentTableProd);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		       
			
		}	
		
			
	}
	
	public void footerInfo()
	{
		
		 PdfPTable desctable = new PdfPTable(1);
		 Paragraph descpara=new Paragraph();
		 String description=null;
		 
		 
		 Phrase descheading=null;
		 
		 
		 Phrase paraphr=null;
		 if(!wo.getWoDescription().equals("")){
			 description=wo.getWoDescription();
			 descheading=new Phrase("DESCRIPTION:",font10boldul);
			 paraphr=new Phrase(description,font8);
		 }
		 descpara.add(descheading);
		 descpara.add(Chunk.NEWLINE);
		 descpara.add(paraphr);
		 descpara.setAlignment(Element.ALIGN_LEFT);
		
		 PdfPCell desccell=new PdfPCell();
		 desccell.addElement(descpara);
		 desccell.setBorder(0);
		 
		 desctable.addCell(desccell);
		 desctable.setWidthPercentage(100);
		 
		 
		
		 String companyname=comp.getBusinessUnitName().trim().toUpperCase();
		 Paragraph companynamepara=new Paragraph();
		 companynamepara.add("FOR "+companyname);
		 companynamepara.setAlignment(Element.ALIGN_CENTER);
		 
	
		 //******************end of document***********************
		 
		 String endDucument = "*************************X--X--X*************************";
		 Paragraph endpara=new Paragraph();
		 endpara.add(endDucument);
		 endpara.setAlignment(Element.ALIGN_CENTER);
		 
		 //***************************************************
		 
		 
		 String authsign="AUTHORISED SIGNATORY";
	     Paragraph authpara=new Paragraph();
	     authpara.add(authsign);
	     authpara.setAlignment(Element.ALIGN_CENTER);
	     
	     PdfPCell companynamecell=new PdfPCell();
	     companynamecell.addElement(companynamepara);
	     companynamecell.setBorder(0);
	     
	     PdfPCell authsigncell=new PdfPCell();
	     authsigncell.addElement(authpara);
	     authsigncell.setBorder(0);
	
	     Phrase ppp=new Phrase(" ");
	     PdfPCell blacell=new PdfPCell();
	     blacell.addElement(ppp);
	     blacell.setBorder(0);
	     PdfPTable table = new PdfPTable(1);
		 table.addCell(companynamecell);
		 table.addCell(blacell);
		 table.addCell(blacell);
		 table.addCell(blacell);
		 table.addCell(authsigncell);
		 
		 table.setWidthPercentage(100);
		 
		 
		  PdfPTable parentbanktable=new PdfPTable(2);
		  parentbanktable.setSpacingAfter(40f);
		  parentbanktable.setWidthPercentage(100);
	      try {
	    	  parentbanktable.setWidths(new float[]{70,30});
		  } 
	      catch (DocumentException e1) {
			e1.printStackTrace();
		}
	      
	      
	      PdfPCell bankdatacell1 = new PdfPCell();
		  PdfPCell authorisedcell = new PdfPCell();
		  
		  bankdatacell1.addElement(desctable);
		  authorisedcell.addElement(table);
		  
		  parentbanktable.addCell(bankdatacell1);
		  parentbanktable.addCell(authorisedcell);
		  
		  
		  
		  Phrase refphrase=new Phrase("Annexure 1 ",font10bold);
		  Paragraph repara=new Paragraph(refphrase);
		  
		  
		  try {
				document.add(parentbanktable);
				
				if(this.products.size() > 30){
				document.newPage();
				document.add(repara);
				settingRemainingRowsToPDF(31);
				}if(this.products.size() > 90){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(91);
				}if(this.products.size() > 150){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(151);
				}if(this.products.size() > 210){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(211);
				}
				if(this.products.size()>30){
					document.add(endpara);
				}
			} 
			catch (DocumentException e) {
				e.printStackTrace();
			}
		  
		  
		  			 
	}

	private void settingRemainingRowsToPDF(int startindex) {
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		PdfPTable table = new PdfPTable(6);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		Phrase srno = new Phrase("SR.NO.", font1);
		Phrase refno = new Phrase("ITEM DETAILS", font1);
		Phrase grnqty = new Phrase("CODE",font1);
		Phrase accqty = new Phrase("CATEGORY", font1);
		Phrase rejqty = new Phrase("QUANTITY", font1);
		Phrase remark = new Phrase("PRICE", font1);
		Phrase uom = new Phrase("UOM", font1);
		
		
	        
	      
	        	 
		PdfPCell cellsrno = new PdfPCell(srno);
		cellsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcategory = new PdfPCell(refno);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(grnqty);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(accqty);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservicetax = new PdfPCell(rejqty);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(remark);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celluom = new PdfPCell(uom);
		celluom.setHorizontalAlignment(Element.ALIGN_CENTER);
	
		table.addCell(cellsrno);
		table.addCell(cellcategory);
		table.addCell(cellrate);
		table.addCell(cellperdisc);
		table.addCell(cellservicetax);
		table.addCell(celltotal);
		table.addCell(celluom);
		
		
		//************************here i have made changes
		//*************calculating spaces here*************
		if( this.products.size()<= firstBreakPoint){
			int size =  firstBreakPoint - this.products.size();
			blankLines = size*(160/15);
			System.out.println("blankLines size ="+blankLines);
			System.out.println("blankLines size ="+blankLines);
		}
		else{
			blankLines = 10f;
		}
		
		table.setSpacingAfter(blankLines);	
	
		Phrase rephrse=null;
		
		for (int i = 0; i < this.products.size(); i++) {
			
			chunk = new Phrase((i+1)+"",font8);
			pdfsrno = new PdfPCell(chunk);
			pdfsrno.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			if(products.get(i).getProductName()!=null){
				chunk = new Phrase(products.get(i).getProductName(), font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfitemname = new PdfPCell(chunk);
			
			if(products.get(i).getProductCode()!=null){
				chunk = new Phrase(products.get(i).getProductCode() + "", font8);
			}else{
				chunk = new Phrase("");
			}
			
			pdfgrnqty = new PdfPCell(chunk);
			pdfgrnqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 
			if(products.get(i).getProductCategory()!=null){
				 chunk =new Phrase (products.get(i).getProductCategory()+"",font8);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfaccpqty = new PdfPCell(chunk);
			pdfaccpqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(products.get(i).getProductQty()!=0){
				 chunk =new Phrase (products.get(i).getProductQty()+"",font8);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfrejqty = new PdfPCell(chunk);
			pdfrejqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
				
			if(products.get(i).getProductPrice()!=0){
				chunk = new Phrase(products.get(i).getProductPrice()+"", font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfreasonforrej = new PdfPCell(chunk);
			
			if(products.get(i).getProductUOM()!=null){
				chunk = new Phrase(products.get(i).getProductUOM()+"", font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfuom = new PdfPCell(chunk);
			
			table.addCell(pdfsrno);
			table.addCell(pdfitemname);
			table.addCell(pdfgrnqty);
			table.addCell(pdfaccpqty);
			table.addCell(pdfrejqty);
			table.addCell(pdfreasonforrej);
			table.addCell(pdfuom);
			
			startindex=startindex+1;
			if(startindex==91||startindex==151||startindex==211){
			break;
			}
		}
		
			PdfPCell prodtablecell=new PdfPCell();
			prodtablecell.addElement(table);
			prodtablecell.addElement(rephrse);	
			PdfPTable parentTableProd=new PdfPTable(1);
		    parentTableProd.setWidthPercentage(100);
		    parentTableProd.addCell(prodtablecell);
		       
				
			try {
				document.add(parentTableProd);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
			
	}

}
