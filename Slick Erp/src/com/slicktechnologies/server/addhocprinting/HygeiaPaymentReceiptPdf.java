package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


public class HygeiaPaymentReceiptPdf {


	Logger logger = Logger.getLogger("Name of logger");

	ProcessConfiguration processConfig;
	boolean hygeiaPest = false;
	boolean isCash = false;
	boolean isCheque = false;

	public Document document;

	Company comp;
	Customer cust;
	Phrase chunk;
	String str;
	Invoice invoice;
	CustomerPayment pay;

	Phrase blank;
	Phrase col;

	float[] colWidth1 = { 0.3f, 1.1f };

	// float[] columnWidth = {};
	// float[] columnWidth1 = { 5f, 0.5f, 8f };
	// float[] colWidth = { 0.5f, 4f, 3f, 2f, 2.9f, 2.4f };
	// float[] colWidth1 = { 20f, 10f, 70f };
	// float[] colWidth2 = { 20f, 10f, 20f, 50f };
	// // float[] colWidth4 = { 0.2f, 0.8f };
	// float[] colWidth5 = { 0.1f, 0.2f };
	// float[] colWidth6 = { 0.1f, 0.3f, 0.1f, 0.2f, 0.2f, 0.2f };

	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul, font9boldul;

	public HygeiaPaymentReceiptPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setpdfreceipt(Long count) {

		pay = ofy().load().type(CustomerPayment.class).id(count).now();

		// Load Customer
		if (pay.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", pay.getPersonInfo().getCount()).first()
					.now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", pay.getPersonInfo().getCount())
					.filter("companyId", pay.getCompanyId()).first().now();

		// Load Company
		if (pay.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", pay.getCompanyId()).first().now();

		// Load Invoice
		if (pay.getCompanyId() == null)
			invoice = ofy().load().type(Invoice.class).first().now();
		else
			invoice = ofy().load().type(Invoice.class)
					.filter("companyId", pay.getCompanyId())
					.filter("invoiceCount", pay.getInvoiceCount()).first()
					.now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		// ***********Process config code*******************
		if (pay.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", pay.getCompanyId())
					.filter("processName", "CustomerPayment")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						hygeiaPest = true;
					}
				}
			}
		}

		String amountInWord = ServiceInvoicePdf.convert(pay
				.getPaymentReceived());

	}

	public void createPdf(String preprintStatus) {

		
		if( preprintStatus.equals("plane")){
		createLogo(document, comp);
		createBlankHeading();
		}else{
			
			if(preprintStatus.equals("yes")){
				
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			
			if(preprintStatus.equals("no")){
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}
			
		}
		
		
	
		createTitle();
		createPaymentInfo();
		createCustInfo();
		createInWordsDetail();
		createCashAndChequeInfo();
		createCustBillInfo();
		createCompanyInfo();

	}
	
	

	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	
private void createBlankforUPC() {
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createBlankHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createTitle() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase rec = new Phrase("RECEIPT", font14boldul);
		PdfPCell reccell = new PdfPCell(rec);
		reccell.setBorder(0);
		reccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable rectable = new PdfPTable(1);
		rectable.setWidthPercentage(100);
		rectable.addCell(reccell);
		rectable.addCell(blcell);

		PdfPCell cell = new PdfPCell(rectable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		// table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createPaymentInfo() {

		PdfPTable payTable = new PdfPTable(2);

		try {
			payTable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		payTable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase no = null;
		if (pay.getCount() != 0) {
			no = new Phrase("No. : " + pay.getCount(), font10);
		}

		Phrase date = null;
		if (pay.getPaymentDate() != null) {
			date = new Phrase("Date : " + fmt.format(pay.getPaymentDate()),
					font10);
		}

		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		payTable.addCell(nocell);
		payTable.addCell(datecell);

		PdfPCell cell = new PdfPCell(payTable);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustInfo() {

		PdfPTable custTable = new PdfPTable(2);

		try {
			custTable.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		custTable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase rec = new Phrase("RECEIVED with thanks from", font10);
		PdfPCell reccell = new PdfPCell(rec);
		reccell.setBorder(0);
		reccell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cust_comp = null;
		if (cust.isCompany() == true) {
			cust_comp = new Phrase("" + cust.getCompanyName(), font10);
		} else {
			cust_comp = new Phrase("" + cust.getFullname(), font10);
		}
		PdfPCell cust_compcell = new PdfPCell(cust_comp);
		cust_compcell.setBorder(0);
		cust_compcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		custTable.addCell(reccell);
		custTable.addCell(cust_compcell);

		PdfPCell cell = new PdfPCell(custTable);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createInWordsDetail() {

		PdfPTable rstable = new PdfPTable(2);

		try {
			rstable.setWidths(colWidth1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		rstable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase rs = new Phrase("the Sum of Rupees : ", font10);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String amountInWord = ServiceInvoicePdf.convert(pay
				.getPaymentReceived());
		Phrase bl1 = new Phrase("" + amountInWord + " Only ", font10);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		rstable.addCell(rscell);
		rstable.addCell(bl1cell);

		PdfPCell cell = new PdfPCell(rstable);
		cell.setBorder(0);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(cell);
		amttable.addCell(blcell);
		// amttable.addCell(blcell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCashAndChequeInfo() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// Phrase cash = null;
		// if (isCash==true) {
		// cash = new Phrase("by Cash "+" / "+"Cheque No. "+"", font12);
		// }else if(isCheque==true){
		// cash = new Phrase("by Cash"+" / "+"Cheque No. ", font12);
		// }
		// PdfPCell cashcell = new PdfPCell(cash);
		// cashcell.setBorder(0);
		// cashcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/***************************************************************************/

		Phrase vide = new Phrase("by Cash / Cheque No. ", font10);
		PdfPCell videcell = new PdfPCell(vide);
		videcell.setBorder(0);
		videcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase blank2 = new Phrase(" ");

		Phrase blank2 = null;
		// if (pay.getStatus().equals("Closed")) {
		if (pay.getPaymentMethod().equalsIgnoreCase("Cash")) {
			blank2 = new Phrase("");
		} else {
			blank2 = new Phrase(pay.getChequeNo(), font10);
		}

		// } else {
		// blank2 = new Phrase(" ");
		// }

		PdfPCell blank2cell = new PdfPCell(blank2);
		blank2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blank2cell.setBorderWidthTop(0);
		blank2cell.setBorderWidthRight(0);
		blank2cell.setBorderWidthLeft(0);

		Phrase dated = new Phrase("      Dtd", font10);
		PdfPCell datedcell = new PdfPCell(dated);
		datedcell.setBorder(0);
		datedcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase blank3 = new Phrase(" ");

		Phrase blank3 = null;
		// if (pay.getStatus().equals("Closed")) {
		if (pay.getPaymentMethod().equalsIgnoreCase("Cash")) {
			blank3 = new Phrase("");
		} else if (pay.getPaymentMethod().equalsIgnoreCase("Cheque")) {
			blank3 = new Phrase(fmt.format(pay.getChequeDate()) + "", font10);
		} else {
			blank3 = new Phrase(" ", font10);
		}

		// } else {
		// blank3 = new Phrase(" ");
		// }
		PdfPCell blank3cell = new PdfPCell(blank3);
		blank3cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blank3cell.setBorderWidthTop(0);
		blank3cell.setBorderWidthRight(0);
		blank3cell.setBorderWidthLeft(0);

		Phrase bank = new Phrase("Drawn On", font10);
		PdfPCell bankcell = new PdfPCell(bank);
		bankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankcell.setBorder(0);

		// Phrase blank4 = new Phrase(" ");
		Phrase blank4 = null;
		// if (pay.getStatus().equals("Closed")) {
		if (pay.getBankName() != null) {
			blank4 = new Phrase(pay.getBankName() + "", font10);
		} else {
			blank4 = new Phrase(" ");
		}
		// } else {
		// blank4 = new Phrase(" ");
		// }

		PdfPCell blank4cell = new PdfPCell(blank4);
		blank4cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blank4cell.setBorderWidthTop(0);
		blank4cell.setBorderWidthRight(0);
		blank4cell.setBorderWidthLeft(0);

		PdfPTable table = new PdfPTable(6);

		try {
			table.setWidths(new float[] { 20, 15, 10, 15, 10, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		table.addCell(videcell);
		table.addCell(blank2cell);
		table.addCell(datedcell);
		table.addCell(blank3cell);
		table.addCell(bankcell);
		table.addCell(blank4cell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustBillInfo() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase bill = null;
		if (pay.getInvoiceCount() != 0) {
			bill = new Phrase(
					"towards Full " + " / " + "Part Payment of our Bill No. : "
							+ pay.getInvoiceCount(), font10);
		}
		PdfPCell billcell = new PdfPCell(bill);
		billcell.setBorder(0);
		billcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable billTable = new PdfPTable(1);
		billTable.addCell(billcell);
		billTable.setWidthPercentage(100);

		PdfPCell cell = new PdfPCell(billTable);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		leftTable.setWidthPercentage(100);

		Phrase bl1 = null;
		if (pay.getPaymentReceived() != 0) {
			bl1 = new Phrase("" + Math.round(pay.getPaymentReceived()) + " /-",
					font12bold);
		}
		PdfPCell bl1cell = new PdfPCell(bl1);
		// bl1cell.setBorder(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable blTable = new PdfPTable(1);
		blTable.addCell(bl1cell);

		leftTable.addCell(blTable);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);

		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// *************righ table************
		PdfPTable righTable = new PdfPTable(1);
		righTable.setWidthPercentage(100);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("For " + comp.getBusinessUnitName(),
					font12bold);
		} else {
			company = new Phrase(" ", font12bold);
		}
		PdfPCell compcell = new PdfPCell(company);
		compcell.setBorder(0);
		compcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase sign = new Phrase("Authorized Signatory ", font12bold);
		PdfPCell signcell = new PdfPCell(sign);
		signcell.setBorder(0);
		signcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		righTable.addCell(compcell);
		righTable.addCell(blcell);
		righTable.addCell(blcell);
		righTable.addCell(signcell);

		PdfPCell rightCell = new PdfPCell(righTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPdfForEmail(CustomerPayment customerPay, Company comp2,Customer c) {
		pay=customerPay;
		cust = c;
		comp =comp2;
		
		// Load Invoice
		if (pay.getCompanyId() == null)
			invoice = ofy().load().type(Invoice.class).first().now();
		else
			invoice = ofy().load().type(Invoice.class)
					.filter("companyId", pay.getCompanyId())
					.filter("invoiceCount", pay.getInvoiceCount()).first()
					.now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		// ***********Process config code*******************
		if (pay.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", pay.getCompanyId())
					.filter("processName", "CustomerPayment")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						hygeiaPest = true;
					}
				}
			}
		}

		String amountInWord = ServiceInvoicePdf.convert(pay
				.getPaymentReceived());
		
			if(comp.getUploadHeader()!=null){
				createBlankforUPC();
				createCompanyNameAsHeader(document,comp);
			}
			else
			{
				createLogo(document, comp);
				createBlankHeading();
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
	
		createTitle();
		createPaymentInfo();
		createCustInfo();
		createInWordsDetail();
		createCashAndChequeInfo();
		createCustBillInfo();
		createCompanyInfo();
	}
}
