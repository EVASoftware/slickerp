package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CreateSRFormatVersion1Servlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9003078302764914859L;
	Logger logger = Logger.getLogger("CreateSRFormatVersion1Servlet");

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("application/pdf"); 
		
		String technicianName = request.getParameter("technicianName");
		
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		df.setTimeZone(TimeZone.getTimeZone("IST"));
		String frmDate1=request.getParameter("fromDate");
		Date fromDate=null;
		try {
			fromDate = df.parse(frmDate1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date frmDate = null;

		if(fromDate!=null){
			Calendar cal = Calendar.getInstance();
			cal.setTime(fromDate);
			cal.add(Calendar.DATE, 0);
			
			try {
				frmDate=df.parse(df.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,00);
				cal.set(Calendar.MINUTE,00);
				cal.set(Calendar.SECOND,00);
				cal.set(Calendar.MILLISECOND,000);
				frmDate=cal.getTime();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		String toDate1=request.getParameter("toDate");
		Date toDate=null;
		try {
			toDate = df.parse(toDate1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Date ToDate = null;

		if(toDate!=null){
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(toDate);
			cal1.add(Calendar.DATE, 0);
			
			try {
				ToDate=df.parse(df.format(cal1.getTime()));
				cal1.set(Calendar.HOUR_OF_DAY,23);
				cal1.set(Calendar.MINUTE,59);
				cal1.set(Calendar.SECOND,59);
				cal1.set(Calendar.MILLISECOND,999);
				ToDate=cal1.getTime();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		/**
		 * ends 
		 */
		
		String companyid=request.getParameter("companyId");
		Long companyId=Long.parseLong(companyid);
		String preprintStatus=request.getParameter("preprint").trim();
		
		
		if(frmDate!=null && ToDate!=null){
			
			try {
				
				List<Service> servicelist = ofy().load().type(Service.class)
						.filter("companyId", companyId)
						.filter("serviceDate >=", frmDate)
						.filter("serviceDate <=", ToDate)
						.filter("employee", technicianName)
						.list();
				
				logger.log(Level.SEVERE, "servicelist size"+servicelist.size());
				
				if(servicelist.size()>0){
					
					Comparator<Service> servDateComp=new Comparator<Service>() {
						@Override
						public int compare(Service arg0, Service arg1) {
							return arg0.getServiceDate().compareTo(arg1.getServiceDate());
						}
					};
					Collections.sort(servicelist, servDateComp);
					
					String stringid=servicelist.get(0).getId()+"";
					Long count =Long.parseLong(stringid);
				    
					Service serviceEntity = ofy().load().type(Service.class).id(count).now();
					ServerAppUtility serverUtility = new ServerAppUtility();
					Company companyEntity = serverUtility.loadCompany(serviceEntity.getCompanyId());
							
					SRFormatVersion1 srformatv1 = new SRFormatVersion1();
					srformatv1.document = new Document(); 
			 	    Document document = srformatv1.document;
			 	    PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
			 	    
			 	    Service serviceentity=ofy().load().type(Service.class).id(count).now();
			 	    
			 	    document.setPageSize(PageSize.A4);
			 	  	document.setMargins(20, 20, 120, 100);
			 	  	document.setMarginMirroring(false);
					
		 	  	   HeaderFooterPageEvent event = new HeaderFooterPageEvent(companyEntity,serviceentity.getBranch(),preprintStatus);
		 	  	   writer.setPageEvent(event);
			 	   document.open();
				   srformatv1.loadMultipleSRCopies(servicelist,preprintStatus);
			 	   srformatv1.createPdf(preprintStatus);
			 	   document.close();
				}

			}catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		else{
			
			try {
				String stringid=SRFormatVersion1.servicearaylist.get(0).getId()+"";
				Long count =Long.parseLong(stringid);
			    
				Service serviceEntity = ofy().load().type(Service.class).id(count).now();
				ServerAppUtility serverUtility = new ServerAppUtility();
				Company companyEntity = serverUtility.loadCompany(serviceEntity.getCompanyId());
						
				SRFormatVersion1 srformatv1 = new SRFormatVersion1();
				srformatv1.document = new Document(); 
		 	    Document document = srformatv1.document;
		 	    PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
		 	    
		 	    Service serviceentity=ofy().load().type(Service.class).id(count).now();
		 	    
		 	    document.setPageSize(PageSize.A4);
		 	  	document.setMargins(20, 20, 120, 100);
		 	  	document.setMarginMirroring(false);
				
	 	  	   HeaderFooterPageEvent event = new HeaderFooterPageEvent(companyEntity,serviceentity.getBranch(),preprintStatus);
	 	  	   writer.setPageEvent(event);
		 	   document.open();
			   srformatv1.loadMultipleSRCopies(SRFormatVersion1.servicearaylist,preprintStatus);
//		 	   srformatv1.createPdf(preprintStatus);
		 	   document.close();
		 	   SRFormatVersion1.servicearaylist=null;
		 	  
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
