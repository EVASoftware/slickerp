package com.slicktechnologies.server.addhocprinting;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
public class CreateMaterialInfoPdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7459531903495241800L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("CompanyId");
			stringid = stringid.trim();
			Long companyId = Long.parseLong(stringid);
			
			String stringid1 = req.getParameter("docType");
			stringid1 = stringid1.trim();
			
			String stringid2 = req.getParameter("docId");
			stringid2 = stringid2.trim();
			int count = Integer.parseInt(stringid2);
			
			
			MaterialInfoPdf wopdf = new MaterialInfoPdf();
			
			wopdf.document = new Document();
			Document document = wopdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			document.open();
			
			wopdf.setMaterialInfo(stringid1,count,companyId);
			wopdf.createPdf();
			document.close(); 
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	

}
