package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
public class TaxInvoicePdf {

	Logger logger = Logger.getLogger("NameOfYourLogger");

	public Document document;
	Company comp;
	Customer cust;
	Invoice taxinv;
	Phrase blank;
	Phrase chunk;
	Service service;
	Contract con;
	Phrase col;
	ServiceProduct serprod;

	PdfPCell pdfsno, pdftreat, pdfcon, pdfqty, pdfunit, pdfrate, pdfamt;

	float[] columnWidth = { (float) 33.33, (float) 33.33, (float) 33.33 };
	float[] colWidth = { 0.8f, 6f, 4.1f, 1.9f, 2f, 2f, 3f };
	// float[] colWidth1 = {0.4f,0.1f,0.3f};
	float[] colWidth1 = { 0.3f, 0.1f, 0.4f };
	float[] colWidth2 = { 1.0f, 0.5f };
	float[] colWidth3 = { 0.2f, 0.1f, 0.3f };
	float[] colWidth4 = { 0.2f, 0.1f, 0.3f };

	float round = 0.00f;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,
			font9boldul, font14boldul;

	public TaxInvoicePdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setInvoice(Long count) {

		taxinv = ofy().load().type(Invoice.class).id(count).now();

		if (taxinv.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", taxinv.getCustomerId())
					.filter("companyId", taxinv.getCompanyId()).first().now();

		if (taxinv.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", taxinv.getCustomerId()).first().now();

		if (taxinv.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", taxinv.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (taxinv.getCompanyId() == null)
			service = ofy().load().type(Service.class)
					.filter("contractCount", taxinv.getContractCount()).first()
					.now();
		else
			service = ofy().load().type(Service.class)
					.filter("companyId", taxinv.getCompanyId())
					.filter("contractCount", taxinv.getContractCount()).first()
					.now();

		if (taxinv.getCompanyId() == null)
			con = ofy().load().type(Contract.class)
					.filter("count", taxinv.getContractCount()).first().now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", taxinv.getCompanyId())
					.filter("count", taxinv.getContractCount()).first().now();


		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preprintStatus) {

			if (comp.getUploadHeader() != null) {
				createSpcingForHeading();
				createCompanyNameAsHeader(document, comp);
				
			}
			else
			{
				createLogo(document, comp);
			}

			if (comp.getUploadFooter() != null) {
				createCompanyNameAsFooter(document, comp);
			}

		createInvoiceHeading();
		createInvoiceDetail();
		createCustomerDetail();
		createBlankTable();
		createInvoiceTable();
		createInformationTable();
		createInwordsTable();

		/********************************************/
		createBottom1Detail();
		createBottom2Detail();
		createBottom3Detail();

		String amountInWord = ServiceInvoicePdf.convert(taxinv
				.getInvoiceAmount());

	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createSpcingForHeading() {

		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);

		try {
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createInvoiceHeading() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(blank1);
		bl1cell.setBorder(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase head = new Phrase("Tax Invoice", font14bold);
		PdfPCell headcell = new PdfPCell(head);
		headcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable headtable = new PdfPTable(3);
		headtable.addCell(blcell);
		headtable.addCell(headcell);
		headtable.addCell(bl1cell);
		headtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(headtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);

//		try {
//			table.setWidths(columnWidth);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createBottom1Detail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorderWidthTop(0);

		Phrase detail = new Phrase("    DETAILS OF SERVICES", font12bold);
		PdfPCell detailcell = new PdfPCell(detail);
		// detailcell.setBorder(0);
		detailcell.setBorderWidthBottom(0);
		detailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable detailtable = new PdfPTable(1);
		detailtable.addCell(detailcell);
		detailtable.setWidthPercentage(100);
		
		Phrase serv = null;
		
		PdfPTable table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		
		PdfPTable table2 = new PdfPTable(1);
		table2.setWidthPercentage(100);

		PdfPTable table3 = new PdfPTable(1);
		table3.setWidthPercentage(100);
		
		PdfPTable table4 = new PdfPTable(1);
		table4.setWidthPercentage(100);
		
		for(int i=0; i<con.getServiceScheduleList().size(); i++){
			
			if(con.getServiceScheduleList().get(i).getScheduleServiceDate() !=null){
				serv = new Phrase("    "+(i+1)+ "  : "+ fmt.format(con.getServiceScheduleList().get(i).getScheduleServiceDate()), font10);	
			}
			PdfPCell servcell = new PdfPCell(serv);
			 servcell.setBorder(0);
//			servcell.setBorderWidthTop(0);
//			servcell.setBorderWidthBottom(0);
			servcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			if(i<5){
				table1.addCell(servcell);
			}
			else if(i>=5 && i<10){
				table2.addCell(servcell);
			}
			else if(i>=10 && i<15){
				table3.addCell(servcell);
			}
			else if(i>=15 && i<20){
				table4.addCell(servcell);
			}
//			detailtable.addCell(servcell);
			
//			if(i==(con.getServiceScheduleList().size()-1))
//			detailtable.addCell(blankcell);
			
			if(i==20){
				break;
			}
			
			
		}
		
		PdfPTable parenttable = new PdfPTable(4);
		parenttable.setWidthPercentage(100);
		
		PdfPCell cell1 = new PdfPCell(table1);
		cell1.setBorder(0);
//		cell1.setBorderWidthTop(0);
//		cell1.setBorderWidthBottom(0);
//		cell1.setBorderWidthRight(0);
		
		PdfPCell cell2 = new PdfPCell(table2);
		cell2.setBorder(0);
		
		PdfPCell cell3 = new PdfPCell(table3);
		cell3.setBorder(0);
		
		PdfPCell cell4 = new PdfPCell(table4);
		cell4.setBorder(0);
		
		parenttable.addCell(cell1);
		parenttable.addCell(cell2);
		parenttable.addCell(cell3);
		parenttable.addCell(cell4);
		
		
		
		PdfPCell parentcell = new PdfPCell(parenttable);
		parentcell.setBorderWidthTop(0);
		
		detailtable.addCell(parentcell);

		try {
			document.add(detailtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	public void createBottom2Detail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorderWidthTop(0);

		// Phrase note = new Phrase("    Special Notes : ",font10bold);

		Phrase note = null;
		if (con.getDescription() != null) {
			note = new Phrase("    Special Notes : " + con.getDescription(),
					font10bold);
		}

		PdfPCell notecell = new PdfPCell(note);
		notecell.setBorderWidthBottom(0);
		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable notetable = new PdfPTable(1);
		notetable.addCell(notecell);
		notetable.addCell(blankcell);
		notetable.setWidthPercentage(100);

		try {
			document.add(notetable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createBottom3Detail() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorderWidthTop(0);
		blankcell.setBorderWidthBottom(0);

		Phrase blank1 = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank1);
		blcell.setBorderWidthBottom(0);
		blcell.setBorderWidthTop(0);

		Phrase vcare = new Phrase("    For V-Care", font10bold);
		PdfPCell vcarecell = new PdfPCell(vcare);
		// vcarecell.setBorder(0);
		vcarecell.setBorderWidthBottom(0);
		vcarecell.setBorderWidthTop(0);
		vcarecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase autho = new Phrase("    Authorised Signatory", font10bold);
		PdfPCell authocell = new PdfPCell(autho);
		// authocell.setBorder(0);
		authocell.setBorderWidthBottom(0);
		authocell.setBorderWidthTop(0);
		authocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase note = new Phrase(
				"    Note : "
						+ " Kindly issue a cheque in favour of     V-Care "
						+ "\n"
						+ "                Please quote respective Your Contract Number in future correspondance with us. "
						+ "\n"
						+ "                In Case of any issue regarding Invoice please call us within 5 days from the receipt of the Invoice. ",
				font9);
		
		PdfPCell notecell = new PdfPCell(note);
		// notecell.setBorder(0);
		notecell.setBorderWidthTop(0);
		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable notetable = new PdfPTable(1);
		notetable.addCell(vcarecell);
		notetable.addCell(blankcell);
		notetable.addCell(authocell);
		notetable.addCell(blcell);
		notetable.addCell(notecell);
		notetable.setWidthPercentage(100);

		try {
			document.add(notetable);
			
			if(con.getServiceScheduleList().size()>20){
			document.newPage();
				getRemainingServices(21);
			}	
			
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/*************************************************************************************/
	public void createInvoiceDetail() {

		Phrase invNo = null;
		if (taxinv.getCount() !=0) {
			System.out.println("InvoiceCount " + taxinv.getCount());
			invNo = new Phrase("Invoice Number : " + taxinv.getCount(),
					font10bold);
		}

		PdfPCell nocell = new PdfPCell(invNo);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(nocell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorderWidthTop(0);
		leftcell.setBorderWidthLeft(0);
		leftcell.setBorderWidthRight(0);
		/*********************************************************/

		Phrase invDate = null;
		if (taxinv.getInvoiceDate() != null) {
			invDate = new Phrase("Date : "
					+ fmt.format(taxinv.getInvoiceDate()), font10bold);
		}

		PdfPCell dtcell = new PdfPCell(invDate);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(dtcell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorderWidthTop(0);
		rightcell.setBorderWidthRight(0);
		rightcell.setBorderWidthLeft(0);
		/*************************************************************/

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable centertable = new PdfPTable(1);
		centertable.addCell(blankcell);

		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setBorder(0);

		/************************************************************/

		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);
		table.setWidthPercentage(100);

		try {
			table.setWidths(columnWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/***************************************************************************/

	public void createCustomerDetail() {

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);

		Phrase blank1 = new Phrase(" ");
		PdfPCell blank1cell = new PdfPCell(blank1);
		blank1cell.setBorder(0);

		Phrase custname = new Phrase("Customer Name", font9bold);
		PdfPCell custnamecell = new PdfPCell(custname);
		custnamecell.setBorder(0);
		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name = null;
		if (cust.getCompanyName() != null && cust.getName() != null) {
			name = new Phrase(" " + cust.getCompanyName(), font9bold);
		} else {
			name = new Phrase(" " + cust.getName(), font9bold);
		}

		PdfPCell namecell = new PdfPCell(name);
		namecell.setBorder(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/****************************************** customer Address Part begins *********************************/

		Phrase add = new Phrase("Address", font9);
		PdfPCell addcell = new PdfPCell(add);
		addcell.setBorder(0);
		addcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null) {

			if (cust.getAdress().getAddrLine2() != null) {
				if (cust.getAdress().getLandmark() != null) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (cust.getAdress().getLandmark() != null) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null) {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + "\n"
						+ cust.getAdress().getLocality() + " "
						+ cust.getAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}
		}

		Phrase custAddInfo = new Phrase(" " + custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		/************************************* customer cell info ************************************************/

		Phrase mob = new Phrase("Phone Number", font9);
		PdfPCell mobcell = new PdfPCell(mob);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobno = null;
		if (cust.getLandline() != null) {

			mobno = new Phrase(" " + cust.getLandline(), font9);
		}

		PdfPCell mobnocell = new PdfPCell(mobno);
		mobnocell.setBorder(0);
		mobnocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lefttable = new PdfPTable(3);
		lefttable.addCell(custnamecell);
		lefttable.addCell(colcell);
		lefttable.addCell(namecell);

		lefttable.addCell(addcell);
		lefttable.addCell(colcell);
		lefttable.addCell(custAddInfoCell);

		lefttable.addCell(mobcell);
		lefttable.addCell(colcell);
		lefttable.addCell(mobnocell);
		lefttable.setSpacingAfter(5f);

		try {
			lefttable.setWidths(colWidth3);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}

		lefttable.setWidthPercentage(100f);

		PdfPCell leftcell = new PdfPCell(lefttable);
		// leftcell.setBorderWidthLeft(0);
		leftcell.setBorderWidthRight(0);

		/*************************************** for the right side ****************************************/

		/***************************** Site Address starts here *******************************************/

		Phrase add1 = new Phrase("Site Address", font9);
		PdfPCell add1cell = new PdfPCell(add1);
		add1cell.setBorder(0);
		add1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd2 = "";
		String custFullAdd2 = "";

		if (cust.getSecondaryAdress() != null) {

			if (cust.getSecondaryAdress().getAddrLine2() != null) {
				if (cust.getSecondaryAdress().getLandmark() != null) {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getAddrLine2() + ","
							+ cust.getSecondaryAdress().getLandmark();
				} else {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getAddrLine2();
				}
			} else {
				if (cust.getSecondaryAdress().getLandmark() != null) {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getLandmark();
				} else {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1();
				}
			}

			if (cust.getSecondaryAdress().getLocality() != null) {
				custFullAdd1 = custAdd1 + "\n"
						+ cust.getSecondaryAdress().getCountry() + ","
						+ cust.getSecondaryAdress().getState() + ","
						+ cust.getSecondaryAdress().getCity() + "\n"
						+ cust.getSecondaryAdress().getLocality() + " "
						+ cust.getSecondaryAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "\n"
						+ cust.getSecondaryAdress().getCountry() + ","
						+ cust.getSecondaryAdress().getState() + ","
						+ cust.getSecondaryAdress().getCity() + " "
						+ cust.getSecondaryAdress().getPin();
			}
		}

		Phrase custAdd1Info = new Phrase(" " + custFullAdd1, font9);
		PdfPCell custAdd1InfoCell = new PdfPCell(custAddInfo);
		custAdd1InfoCell.setBorder(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/***********************************************************/

		Phrase pname = new Phrase("Contact Person", font9bold);
		PdfPCell pnamecell = new PdfPCell(pname);
		pnamecell.setBorder(0);
		pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase poc = null;
		if (cust.getFullname() != null) {
			poc = new Phrase(" " + cust.getFullname(), font9bold);
		}

		PdfPCell poccell = new PdfPCell(poc);
		poccell.setBorder(0);
		poccell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mail = new Phrase("Email I.D.", font9bold);
		PdfPCell mailcell = new PdfPCell(mail);
		mailcell.setBorder(0);
		mailcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase email = null;
		if (cust.getEmail() != null) {
			email = new Phrase(" " + cust.getEmail(), font9bold);
		}

		PdfPCell emailcell = new PdfPCell(email);
		emailcell.setBorder(0);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable righttable = new PdfPTable(3);

		righttable.addCell(add1cell);
		righttable.addCell(colcell);
		righttable.addCell(custAdd1InfoCell);

		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);

		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);
		righttable.addCell(blank1cell);

		righttable.addCell(pnamecell);
		righttable.addCell(colcell);
		righttable.addCell(poccell);

		righttable.addCell(mailcell);
		righttable.addCell(colcell);
		righttable.addCell(emailcell);
		righttable.setSpacingAfter(5f);

		try {
			righttable.setWidths(colWidth4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		righttable.setWidthPercentage(100f);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorderWidthLeft(0);
		// rightcell.setBorderWidthRight(0);

		PdfPTable parenttable = new PdfPTable(2);
		parenttable.addCell(leftcell);
		parenttable.addCell(rightcell);

		parenttable.setWidthPercentage(100);

		try {
			parenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createBlankTable() {

		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		// blankcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blankcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createInvoiceTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(7);

		try {
			table.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		table.setWidthPercentage(100);

		Phrase serialno = new Phrase("SR", font1);
		Phrase treat = new Phrase("TREATMENT / YOUR REFERENCE NO.", font1);
		Phrase contarct = new Phrase("CONTRACT PREIOD", font1);
		Phrase quantity = new Phrase("QTY", font1);
		Phrase unit = new Phrase("UNIT", font1);
		Phrase rate = new Phrase("RATE", font1);
		Phrase price = new Phrase("AMOUNT", font1);

		PdfPCell nocell = new PdfPCell(serialno);
		nocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell treatcell = new PdfPCell(treat);
		treatcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell contcell = new PdfPCell(contarct);
		contcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell qtycell = new PdfPCell(quantity);
		qtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell unitcell = new PdfPCell(unit);
		unitcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell ratecell = new PdfPCell(rate);
		ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(nocell);
		table.addCell(treatcell);
		table.addCell(contcell);
		table.addCell(qtycell);
		table.addCell(unitcell);
		table.addCell(ratecell);
		table.addCell(pricecell);

		for (int i = 0; i < this.taxinv.getSalesOrderProducts().size(); i++) {

			chunk = new Phrase(i + 1 + "", font8);
			pdfsno = new PdfPCell(chunk);
			pdfsno.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(taxinv.getSalesOrderProducts().get(i)
					.getProdName()
					+ "" + "\n" + con.getRefNo() + "", font8);
			pdftreat = new PdfPCell(chunk);
			pdftreat.setHorizontalAlignment(Element.ALIGN_CENTER);

			logger.log(Level.SEVERE, "Product ID ::::::::::::::::::::: "
					+ taxinv.getSalesOrderProducts().get(i).getPrduct()
							.getCount());
			System.out.println("Product ID ::::::::::::::::::::: "
					+ taxinv.getSalesOrderProducts().get(i).getPrduct()
							.getCount());
			if (taxinv.getSalesOrderProducts().get(i).getPrduct().getCount() != 0) {
				serprod = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("count",
								taxinv.getSalesOrderProducts().get(i)
										.getPrduct().getCount()).first().now();
			}

			System.out.println("Service Product :::::::::::" + serprod);
			logger.log(Level.SEVERE, "Service Product :::::::::::" + serprod);
			if (serprod != null) {
				logger.log(Level.SEVERE, serprod.getTermsoftreatment());
				if ((serprod.getTermsoftreatment()!= null)&&(!serprod.getTermsoftreatment().equals(""))) {
					System.out.println("Inside If");
					logger.log(Level.SEVERE, "Inside If");
					chunk = new Phrase(fmt.format(con.getStartDate()) + ""
							+ " - " + fmt.format(con.getEndDate()) + "\n"
							+ serprod.getTermsoftreatment() + "", font8);
				} else {
					System.out.println("Inside else");
					logger.log(Level.SEVERE, "Inside else");
					chunk = new Phrase(fmt.format(con.getStartDate())
							+ ""
							+ " - "
							+ fmt.format(con.getEndDate())
							+ "\n"
							+ taxinv.getSalesOrderProducts().get(i)
									.getOrderServices() + " Services", font8);
				}
			} else {
				chunk = new Phrase(fmt.format(con.getStartDate())
						+ ""
						+ " - "
						+ fmt.format(con.getEndDate())
						+ "\n"
						+ taxinv.getSalesOrderProducts().get(i)
								.getOrderServices() + " Services", font8);
			}

			pdfcon = new PdfPCell(chunk);
			pdfcon.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(taxinv.getSalesOrderProducts().get(i)
					.getQuantity()
					+ "", font8);
			pdfqty = new PdfPCell(chunk);
			pdfqty.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(taxinv.getSalesOrderProducts().get(i)
					.getUnitOfMeasurement()
					+ "", font8);
			pdfunit = new PdfPCell(chunk);
			pdfunit.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(taxinv.getSalesOrderProducts().get(i)
					.getPrice())
					+ "", font8);
			pdfrate = new PdfPCell(chunk);
			pdfrate.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(taxinv.getSalesOrderProducts().get(i)
					.getTotalAmount())
					+ "", font8);
			pdfamt = new PdfPCell(chunk);
			pdfamt.setHorizontalAlignment(Element.ALIGN_CENTER);

			table.addCell(pdfsno);
			table.addCell(pdftreat);
			table.addCell(pdfcon);
			table.addCell(pdfqty);
			table.addCell(pdfunit);
			table.addCell(pdfrate);
			table.addCell(pdfamt);

		}

		PdfPCell invcell = new PdfPCell();
		invcell.addElement(table);
		invcell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(invcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**************************************************************************************/
	public void createInformationTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		Font fontsample = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
		Font fontsample1 = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		// blcell.setBorder(0);
		blcell.setBorderWidthBottom(0);
		blcell.setBorderWidthTop(0);
		blcell.setBorderWidthLeft(0);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		// colcell.setBorder(0);
		colcell.setBorderWidthLeft(0);
		colcell.setBorderWidthRight(0);
		colcell.setBorderWidthTop(0);
		colcell.setBorderWidthBottom(0);
		colcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		/********************************************* Left table starts from here *****************************************************/

		PdfPTable lefttable = new PdfPTable(3);
		try {
			lefttable.setWidths(colWidth1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		lefttable.setWidthPercentage(100f);

		PdfPCell leftcell = new PdfPCell(lefttable);

		/****************************** For loop for left table starts from here ************************************/

		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			Phrase pan = null;
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
				pan = new Phrase("    "
						+ comp.getArticleTypeDetails().get(i)
								.getArticleTypeName(), font9);
			}
			PdfPCell pancell = new PdfPCell(pan);
			pancell.setBorderWidthRight(0);
			pancell.setBorderWidthTop(0);
			pancell.setBorderWidthBottom(0);
			pancell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase pan1 = null;
			if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
				pan1 = new Phrase("    "
						+ comp.getArticleTypeDetails().get(i)
								.getArticleTypeValue(), font9);
			}
			PdfPCell pan1cell = new PdfPCell(pan1);
			pan1cell.setBorderWidthLeft(0);
			pan1cell.setBorderWidthTop(0);
			pan1cell.setBorderWidthBottom(0);
			pan1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

			lefttable.addCell(pancell);
			lefttable.addCell(colcell);
			lefttable.addCell(pan1cell);

		}

		/********************************************* Left table ends here *************************************************/

		/********************************** Right Table starts from here **********************************************/

		Phrase total = new Phrase("Total", fontsample1);
		PdfPCell totalcell = new PdfPCell(total);
		totalcell.setBorder(0);
		totalcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		double amount = 0;

		for (int i = 0; i < taxinv.getSalesOrderProducts().size(); i++) {
			amount = amount
					+ taxinv.getSalesOrderProducts().get(i).getTotalAmount();
		}

		Phrase ttl = new Phrase("" + amount, font1);
		PdfPCell ttlcell = new PdfPCell(ttl);
		ttlcell.setBorderWidthBottom(0);
		ttlcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase discount = new Phrase("Discount", fontsample);
		PdfPCell discountcell = new PdfPCell(discount);
		discountcell.setBorder(0);
		discountcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase dis = new Phrase(" "
				+ taxinv.getSalesOrderProducts().get(0).getProdPercDiscount(),
				fontsample);
		PdfPCell discell = new PdfPCell(dis);
		discell.setBorderWidthBottom(0);
		discell.setBorderWidthTop(0);
		// discell.setBorder(0);
		discell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase roundoff = new Phrase("Rond off: " + round
				+ "     BILL AMOUNT: ", fontsample);
		PdfPCell roundcell = new PdfPCell(roundoff);
		roundcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase ttlamt = null;
		if (taxinv.getInvoiceAmount() != null) {
			ttlamt = new Phrase(" " + taxinv.getInvoiceAmount(), fontsample1);
		}
		PdfPCell ttlamtcell = new PdfPCell(ttlamt);
		// ttlamtcell.setBorder(0);
		ttlamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable righttable = new PdfPTable(2);
		righttable.addCell(totalcell);
		righttable.addCell(ttlcell);
		righttable.addCell(discountcell);
		righttable.addCell(discell);

		try {
			righttable.setWidths(colWidth2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		righttable.setWidthPercentage(100f);

		/****************************************** For loop for Right table starts from here ******************************************************************/

		for (int i = 0; i < con.getProductTaxes().size(); i++) {
			Phrase tax = new Phrase(" "
					+ con.getProductTaxes().get(i).getChargeName(), fontsample);
			PdfPCell taxcell = new PdfPCell(tax);
			taxcell.setBorder(0);
			taxcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase servtax = new Phrase(
					" "
							+ df.format(con.getProductTaxes().get(i)
									.getChargePayable()), fontsample);
			PdfPCell servtaxcell = new PdfPCell(servtax);
			servtaxcell.setBorderWidthBottom(0);
			servtaxcell.setBorderWidthTop(0);
			// servtaxcell.setBorder(0);
			servtaxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			righttable.addCell(taxcell);
			righttable.addCell(servtaxcell);

		}

		System.out.println("other charges size"
				+ con.getProductCharges().size());
		for (int i = 0; i < con.getProductCharges().size(); i++) {
			Phrase swach = new Phrase(" "
					+ con.getProductCharges().get(i).getChargeName(),
					fontsample);
			PdfPCell swachcell = new PdfPCell(swach);
			swachcell.setBorder(0);
			swachcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase swachtax = new Phrase(" "
					+ df.format(con.getProductCharges().get(i)
							.getChargePayable()), fontsample);
			PdfPCell swachtaxcell = new PdfPCell(swachtax);
			swachtaxcell.setBorderWidthBottom(0);
			swachtaxcell.setBorderWidthTop(0);
			// swachtaxcell.setBorder(0);
			swachtaxcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			righttable.addCell(swachcell);
			righttable.addCell(swachtaxcell);
		}

		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);

		righttable.addCell(roundcell);
		righttable.addCell(ttlamtcell);

		/*********************************************** loop ends here *************************************************************/

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(righttable);
		table.setWidthPercentage(100);

		try {
			table.setWidths(new float[] { 55, 45 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/************************************************************************************/

	public void createInwordsTable() {

		String amountInWord = ServiceInvoicePdf.convert(taxinv
				.getInvoiceAmount());
		Phrase rs = new Phrase("Rs.  " + amountInWord + " Only ", font10bold);
		PdfPCell rscell = new PdfPCell(rs);
		// rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(rscell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	
	public void getRemainingServices(int value)
	{
		System.out.println("in side more than 20 service"+value);
		
		PdfPTable detailtable = new PdfPTable(1);
		detailtable.setWidthPercentage(100);
		
		System.out.println("Service list size "+con.getServiceScheduleList().size());
		for(int i=value; i<con.getServiceScheduleList().size(); i++){
			
			if(con.getServiceScheduleList().get(i).getScheduleServiceDate() !=null){
			Phrase serv = new Phrase("    "+(i)+ "  : "+ fmt.format(con.getServiceScheduleList().get(i).getScheduleServiceDate()), font10);	
			PdfPCell servcell = new PdfPCell(serv);
			servcell.setBorder(0);
			servcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			detailtable.addCell(servcell);
			}
		}
		
		

		try {
			document.add(detailtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 *Rohan Added this method for email pdf for v Care 
	 *This method is used for creating attach document pdf
	 *Date :  16-09-2016
	 **/
	public void createPdfForEmail(Invoice invDetails, Company comp2,
			Customer c, Contract servcont) {
	
		comp = comp2 ;
		cust = c;
		taxinv = invDetails;
		con = servcont;
		Service service;
		
		if (taxinv.getCompanyId() == null)
			service = ofy().load().type(Service.class)
					.filter("contractCount", taxinv.getContractCount()).first()
					.now();
		else
			service = ofy().load().type(Service.class)
					.filter("companyId", taxinv.getCompanyId())
					.filter("contractCount", taxinv.getContractCount()).first()
					.now();
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
	}
	

}
