package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;

public class CreateDutySlipServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4833566611018993867L;



	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			DutySlipPdf ctcpdf = new DutySlipPdf();
			
			ctcpdf.document = new Document();
			Document document = ctcpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			document.open();
			
			ctcpdf.setCtc(count);
			ctcpdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
	}
}
