package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
@SuppressWarnings("serial")
public class ServiceRecordPdfServlet extends HttpServlet{

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
		ProcessConfiguration processConfig;
		Logger logger = Logger.getLogger("Name of logger");
		 
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		  String stringCompanyId=request.getParameter("companyId");
		  String srNumber=request.getParameter("SRNumber");
		  
		  String preePrint = request.getParameter("preprint"); //vijaypreprint
		  Long companyId =Long.parseLong(stringCompanyId); 
			boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "SingleContractSRCopy", companyId);
		  	  ServiceRecordPdf serviceRecordPdf = new ServiceRecordPdf();
			MultipleServicesRecordPdf multipleServicesRecordPdf = new MultipleServicesRecordPdf();
		  	  /**30-10-2017 sagar sore [settig spacing for page]**/
		  	/**
				 * Date 27/11/2017
				 * By jayshree
				 * Des. To set the page in landscape mode changes are made
				 */
			logger.log(Level.SEVERE , "FLAG :"+ flag);
			if(flag){
				multipleServicesRecordPdf.document=new Document();
			}else{
		  	  if(preePrint.equals("no")){
		  		serviceRecordPdf.document=new Document(PageSize.A4.rotate(),20,20,0,0);  
		  	  }else{
		  		serviceRecordPdf.document=new Document(PageSize.A4.rotate(),20,20,30,30);
		  	  }
			} 
		  	
			if(!flag){
				Document document = serviceRecordPdf.document;			  
				 PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				 document.open();	
			   	 serviceRecordPdf.loadAll(count);
			   	 serviceRecordPdf.createPdf(preePrint);
			   	 document.close();   
				   
				   		
			}else{
			  Document document = multipleServicesRecordPdf.document;
			  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); 
			  document.open();
			  multipleServicesRecordPdf.loadAll(count ,srNumber,companyId);
			  multipleServicesRecordPdf.createPdf(preePrint);
		   	  document.close();
			   
			   		
			}
		  
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	}
}
