package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class InstallationReportPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5613748681118582450L;

	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			InstallationReportPdf reppdf = new InstallationReportPdf();
			
			reppdf.document = new Document();
			Document document = reppdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
			
			 document.open();
			 reppdf.setpdfrep(count);
			 reppdf.createPdf();
			 document.close();
		     }
		     catch (DocumentException e)
			{
			e.printStackTrace();
			}
	}
}
