package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;

public class ReferenceLetterPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2339138357533922847L;

	Logger logger = Logger.getLogger("NameOfYourLogger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			
			ReferenceLetterPdf refquot = new ReferenceLetterPdf();
			
			refquot.document = new Document();
			Document document = refquot.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
			
			
			String preprintStatus=req.getParameter("type");
			String[] dataRetrieved=preprintStatus.split("[$]");
						
			String refNo=dataRetrieved[0];
			String refDt=dataRetrieved[1];
			
			 logger.log(Level.SEVERE, " refDt date form popup"+refDt);
			
			String designation=dataRetrieved[2];
			String type=dataRetrieved[3];
			
			
			 document.open();
			
			 refquot.setrefletterquotation(count);
			 refquot.createPdf(refNo,refDt,designation,type);
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
