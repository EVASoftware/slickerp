package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

@SuppressWarnings("serial")
public class CreateCompanyPaymentPDFServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf");
		try {

			CompanyPaymentPDF pdf = new CompanyPaymentPDF();
			pdf.document = new Document();
			Document document = pdf.document;
			PdfWriter.getInstance(document, response.getOutputStream()); 
			
			document.open();
			String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			pdf.getPaymentdetails(count);
			pdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
