package com.slicktechnologies.server.addhocprinting;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.tools.ant.taskdefs.Echo.EchoLevel;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.FormField;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.project.concproject.ProjectPresenter;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class GSTQuotationPdf {
	int var = 0;
	Sales qp;
	ArrayList<Service> service;
	List<SalesLineItem> products;
	List<PaymentTerms> payTermsLis;
	List<ProductOtherCharges> prodCharges;
	List<ProductOtherCharges> prodTaxes;
	Customer cust;
	SuperProduct sup;
	Company comp;
	Quotation quotEntity;
	Contract contEntity;
	List<ArticleType> articletype;
	double total = 0;
	public Document document;
	public GenricServiceImpl impl;
	boolean upcflag = false;
	boolean rexFlag = false;

	// rohan added this flag for printing quotation desc on first page
	boolean printDescOnFirstPageFlag = false;

	boolean printProductPremisesFlag = false;

	//

	/**
	 * rohan added this flag for universal pest control This is used to print
	 * vat no and other article information
	 */

	Boolean UniversalFlag = false;
	/**
	 * ends here
	 */

	/**
	 * Rohan added this for Universal pest for printing
	 */
	Boolean multipleCompanyName = false;
	/**
	 * ends here
	 */
	Boolean printPremiseDetails = false;
	List<State> stateList;

	// ******************rohan taken variables ************8
	int both = 0;
	int discAmt = 0;
	int discPer = 0;
	int nothing = 0;

	ProcessConfiguration processConfig;
	public int serviceno = 1;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font11bold, font7, font10bold,// font10,
			font12boldul, font12, font14bold, font9, font6bold;

	/** Total Amount */
	// Added by Ajinkya
	double totalAmount;
	// PdfPCell premiseTblCell = new PdfPCell();
	Phrase blankCell = new Phrase(" ", font9); // Added by Ajinkya

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	Logger logger = Logger.getLogger("NameOfYourLogger");
	ArrayList<SuperProduct> stringlis = new ArrayList<SuperProduct>();

	Phrase chunk;
	int firstBreakPoint = 17;
	int flag = 0;
	int count = 0;
	float blankLines;
	/**
	 *Date 02-08-2017 By Rahul 
	 *This flag is used to check whether to print product description on  pdf or not.
	 */
	boolean productDescFlag=false;

	float[] columnWidths10 = { 0.5f, 2.4f, 1.5f, 1.0f, 1.0f, 0.8f, 0.7f, 1.2f,
			0.6f, 0.8f };

	float[] columnWidths = { 0.5f, 2.4f, 1.5f, 1.0f, 1.0f, 1.0f, 0.8f, 0.7f,
			1.2f, 0.6f, 0.8f };

	float[] columnWidthsfordicsAMt = { 0.6f, 2.0f, 1.1f, 0.8f, 1.0f, 1.2f,
			1.0f, 0.8f, 1.1f, 0.8f, 0.8f, 0.8f };

	float[] columnWidths1 = { 0.5f, 1f, 2f, 4f };
	float[] columnWidths2 = { 1.2f, 0.2f, 1.5f, 1.2f, 0.2f, 1.5f };
	float[] column16CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.3f };
	float[] columnWidthsForSTandEndDate = { 4f, 3f, 4f };

	DecimalFormat df = new DecimalFormat("0.00");

	final static String disclaimerText = "I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";

	/**
	 * nidhi
	 * 9-08-2018
	 * for print serial no & model no
	 */
	boolean printModelSerailNoFlag =false;
	public GSTQuotationPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		// new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
//		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font8 = new Font(Font.FontFamily.HELVETICA, 6);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		// font10=new Font(Font.FontFamily.HELVETICA,10,Font.NORMAL);
		font7 = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);

		impl = new GenricServiceImpl();
	}

	// PLASE TO APPLY COMPANY SPECIFEC SETTINGS

	public void getContract(long count, String preprint) {

		System.out.println("print status =======" + preprint);
		// Load Contract

		qp = ofy().load().type(Contract.class).id(count).now();
		// makeServices();
		// Load Customer
		if (qp.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId())
					.filter("companyId", qp.getCompanyId()).first().now();
		if (qp.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();

		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (qp.getCompanyId() != null)
			contEntity = ofy().load().type(Contract.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count", qp.getCount()).first().now();
		else
			contEntity = ofy().load().type(Contract.class)
					.filter("count", qp.getCount()).first().now();

		stateList = ofy().load().type(State.class)
				.filter("companyId", contEntity.getCompanyId()).list();

		/************************************ discount Flag *******************************/

		if (qp.getCompanyId() != null) {
			for (int i = 0; i < contEntity.getItems().size(); i++) {
				if ((contEntity.getItems().get(i).getDiscountAmt() > 0)
						&& (contEntity.getItems().get(i)
								.getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (contEntity.getItems().get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (contEntity.getItems().get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}

			}
		}
		System.out.println("Disc nothing" + nothing + "-per" + discPer + "-AMt"
				+ discAmt + "-both" + both);

		/************************************ Letter Head Flag *******************************/

		if (qp.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Contract")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;

					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyNameFontSize10")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						rexFlag = true;
					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"printDescOnFirstPageFlagForContractAndQuotation")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printDescOnFirstPageFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}
					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}

				}
			}
			/**
			 * nidhi
			 * 9-08-2018
			 */
			printModelSerailNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "PrintModelNoAndSerialNo", qp.getCompanyId());
		}

		for (int i = 0; i < contEntity.getItems().size(); i++) {

			System.out.println("Con entity result size===="
					+ contEntity.getItems().size());

			if (qp.getCompanyId() != null) {

				sup = ofy()
						.load()
						.type(SuperProduct.class)
						.filter("companyId", qp.getCompanyId())
						.filter("count",
								contEntity.getItems().get(i).getPrduct()
										.getCount()).first().now();

				SuperProduct superprod = new SuperProduct();

				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment() + " "
						+ sup.getCommentdesc());

				stringlis.add(superprod);

			}

			else {
				sup = ofy().load().type(SuperProduct.class)
						.filter("count", contEntity.getItems().get(i).getId())
						.first().now();

				SuperProduct superprod = new SuperProduct();

				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment() + " "
						+ sup.getCommentdesc());

				stringlis.add(superprod);

			}
		}

		products = qp.getItems();
		prodCharges = qp.getProductCharges();
		prodTaxes = qp.getProductTaxes();
		payTermsLis = qp.getPaymentTermsList();

		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());
		}

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void getQuotation(long count, String preprint) {

		System.out.println("print status **************" + preprint);

		// Load Contract
		qp = ofy().load().type(Quotation.class).id(count).now();
		// makeServices();
		// Load Customer
		if (qp.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId())
					.filter("companyId", qp.getCompanyId()).first().now();
		if (qp.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", qp.getCustomerId()).first().now();

		if (qp.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", qp.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (qp.getCompanyId() != null)
			quotEntity = ofy().load().type(Quotation.class)
					.filter("companyId", qp.getCompanyId())
					.filter("count", qp.getCount()).first().now();
		else
			quotEntity = ofy().load().type(Quotation.class)
					.filter("count", qp.getCount()).first().now();

		stateList = ofy().load().type(State.class)
				.filter("companyId", quotEntity.getCompanyId()).list();
		/************************************ discount Flag *******************************/

		if (qp.getCompanyId() != null) {
			for (int i = 0; i < quotEntity.getItems().size(); i++) {
				if ((quotEntity.getItems().get(i).getDiscountAmt() > 0)
						&& (quotEntity.getItems().get(i)
								.getPercentageDiscount() > 0)) {

					both = both + 1;
				} else if (quotEntity.getItems().get(i).getDiscountAmt() > 0) {
					discAmt = discAmt + 1;
				} else if (quotEntity.getItems().get(i).getPercentageDiscount() > 0) {
					discPer = discPer + 1;
				} else {
					nothing = nothing + 1;
				}
			}
		}
		System.out.println("Disc nothing" + nothing + "-per" + discPer + "-AMt"
				+ discAmt + "-both" + both);

		if (qp.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", qp.getCompanyId())
					.filter("processName", "Quotation")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;

					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"printDescOnFirstPageFlagForContractAndQuotation")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printDescOnFirstPageFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printProductPremisesFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}
					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}

				}
			}
		}

		for (int i = 0; i < quotEntity.getItems().size(); i++) {

			System.out.println("quotEntity entity result size===="
					+ quotEntity.getItems().size());

			if (qp.getCompanyId() != null) {

				sup = ofy()
						.load()
						.type(SuperProduct.class)
						.filter("companyId", qp.getCompanyId())
						.filter("count",
								quotEntity.getItems().get(i).getPrduct()
										.getCount()).first().now();

				SuperProduct superprod = new SuperProduct();

				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment() + " "
						+ sup.getCommentdesc());

				stringlis.add(superprod);

			}

			else {
				sup = ofy().load().type(SuperProduct.class)
						.filter("count", quotEntity.getItems().get(i).getId())
						.first().now();

				SuperProduct superprod = new SuperProduct();

				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment() + " "
						+ sup.getCommentdesc());

				stringlis.add(superprod);

			}
		}

		products = qp.getItems();
		prodCharges = qp.getProductCharges();
		prodTaxes = qp.getProductTaxes();
		payTermsLis = qp.getPaymentTermsList();

		articletype = new ArrayList<ArticleType>();
		if (cust.getArticleTypeDetails().size() != 0) {
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype.addAll(comp.getArticleTypeDetails());
		}

		System.out.println(" cust size ()=="
				+ cust.getArticleTypeDetails().size());
		System.out.println(" comp size ()=="
				+ comp.getArticleTypeDetails().size());

		// articletype=cust.getArticleTypeDetails();
		// articletype=comp.getArticleTypeDetails();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preprint) {

		Date contractDate = null;

		if (qp instanceof Contract) {
			Contract contractEntity = (Contract) qp;
			contractDate = contractEntity.getContractDate();
		} else {
			Quotation quotation = (Quotation) qp;
			contractDate = quotation.getQuotationDate();
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		boolean flag = false;
		try {
			flag = contractDate.after(sdf.parse("30 Jun 2017"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (!flag) {
			System.out.println("My name is  ++++++++++++++++++++++" + preprint);

			if (qp instanceof Contract) {
				System.out.println("111111111");
				if (upcflag == false && preprint.equals("plane")) {

					System.out.println("2222222222222222222222222222222");
					System.out.println("contract inside plane");

					createLogo(document, comp);
					createCompanyHedding();
				} else {

					if (preprint.equals("yes")) {

						System.out.println("333333333333333333333333333333333");
						System.out.println("inside prit yes");
						createBlankforUPC();
					}

					if (preprint.equals("no")) {
						System.out
								.println("4444444444444444444444444444444444444444444");
						System.out.println("inside prit no");

						if (comp.getUploadHeader() != null) {
							createCompanyNameAsHeader(document, comp);
						}

						if (comp.getUploadFooter() != null) {
							createCompanyNameAsFooter(document, comp);
						}
						createBlankforUPC();
					}
				}

			} else {

				System.out.println("upc flag**********88888" + upcflag);

				if (upcflag == false && preprint.equals("plane")) {
					System.out.println("55555555555555555555555555555555");

					createLogo(document, comp);
					createCompanyHedding();
				} else {

					if (preprint.equals("yes")) {
						System.out.println("666666666666666666666666666666666");

						createBlankforUPC();

					}
					if (preprint.equals("no")) {

						System.out
								.println("777777777777777777777777777777777777");
						if (comp.getUploadHeader() != null) {
							createCompanyNameAsHeader(document, comp);
						}

						if (comp.getUploadFooter() != null) {
							createCompanyNameAsFooter(document, comp);
						}
						createBlankforUPC();
					}

					System.out.println("88888888888888888888888888888");
				}

			}

			System.out.println("999999999999999999999999999999");

			if (upcflag == true) {
				createCompanyAddressDetails();
			}

			System.out.println(" rohan printProductPremises Flag "
					+ printProductPremisesFlag);
			if (printProductPremisesFlag) {

				if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
					System.out.println("inside both");
					createProductDetailWithDiscount();
				} else if (nothing > 0) {
					System.out.println("inside nothing");
					createProductWithoutDiscDetail();
				} else {
					System.out.println("inside one of the condition");
					createProductDetail();
				}
			} else {
				if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
					System.out.println("inside both");
					createProductDetailWithDiscount();
					// createProductDetailsVal();

				} else if (nothing > 0) {
					System.out.println("inside nothing");
					createProductWithoutDiscDetail();
				} else {
					System.out.println("inside one of the condition");
					createProductDetail();
				}
			}
			termsConditionsInfo();
			footerInfo();
		} else {
			if (upcflag == false && preprint.equals("plane")) {
				createLogo(document, comp);
				createHeader();
				// createCompanyAddress();
			} else {

				if (preprint.equals("yes")) {
					System.out.println("inside prit yes");
					createBlankforUPC();
				}
				if (preprint.equals("no")) {
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}

					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					createBlankforUPC();
				}
			}
			createInvoiceDetails();
			createCustomerDetails();
			createProductDetails();
			createProductDetailsVal();
			createFooterTaxPart();
			createFooterLastPart(preprint);
			// setDescriptiontoAnnexure();
			createProductDescription(); // ajinkya Added this

		}
	}

	/***12-10-2017 sagar sore [ created to send email with gst format]**/
	public void createPdfForEmailGST(Company comp, Customer cust, Sales qp)
	{

		this.comp = comp;
		this.qp = qp;
		this.cust = cust;
		this.products = qp.getItems();
		this.payTermsLis = qp.getPaymentTermsList();
		this.prodTaxes = qp.getProductTaxes();
		this.prodCharges = qp.getProductCharges();
		this.articletype = cust.getArticleTypeDetails();
        String preprint="plane";
        logger.log(Level.SEVERE ,"Creating new email ");
		Date contractDate = null;

		if (qp instanceof Contract) {
			Contract contractEntity = (Contract) qp;
			contractDate = contractEntity.getContractDate();
			contEntity = contractEntity;
			stateList=ofy().load().type(State.class)
					.filter("companyId", contEntity.getCompanyId()).list();
		} else {
			logger.log(Level.SEVERE,"intializing quotation entity ");
			Quotation quotation = (Quotation) qp;
			contractDate = quotation.getQuotationDate();
			quotEntity = quotation;
			stateList=ofy().load().type(State.class)
					.filter("companyId", quotEntity.getCompanyId()).list();
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		boolean flag = false;
		try {
			flag = contractDate.after(sdf.parse("30 Jun 2017"));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (!flag) {
			logger.log(Level.SEVERE,"My name is  ++++++++++++++++++++++" + preprint);

			if (qp instanceof Contract) {
				logger.log(Level.SEVERE,"111111111");
				if (upcflag == false && preprint.equals("plane")) {

					logger.log(Level.SEVERE,"2222222222222222222222222222222");
					logger.log(Level.SEVERE,"contract inside plane");

					createLogo(document, comp);
					createCompanyHedding();
				} else {

					if (preprint.equals("yes")) {

						logger.log(Level.SEVERE,"333333333333333333333333333333333");
						logger.log(Level.SEVERE,"inside prit yes");
						createBlankforUPC();
					}

					if (preprint.equals("no")) {
						System.out
								.println("4444444444444444444444444444444444444444444");
						logger.log(Level.SEVERE,"inside prit no");

						if (comp.getUploadHeader() != null) {
							createCompanyNameAsHeader(document, comp);
						}

						if (comp.getUploadFooter() != null) {
							createCompanyNameAsFooter(document, comp);
						}
						createBlankforUPC();
					}
				}

			} else {

				logger.log(Level.SEVERE,"upc flag**********88888" + upcflag);

				if (upcflag == false && preprint.equals("plane")) {
					logger.log(Level.SEVERE,"55555555555555555555555555555555");

					createLogo(document, comp);
					createCompanyHedding();
				} else {

					if (preprint.equals("yes")) {
						logger.log(Level.SEVERE,"666666666666666666666666666666666");
						createBlankforUPC();
					}
					if (preprint.equals("no")) {
						System.out
								.println("777777777777777777777777777777777777");
						if (comp.getUploadHeader() != null) {
							createCompanyNameAsHeader(document, comp);
						}

						if (comp.getUploadFooter() != null) {
							createCompanyNameAsFooter(document, comp);
						}
						createBlankforUPC();
					}
					logger.log(Level.SEVERE,"88888888888888888888888888888");
				}
			}

			logger.log(Level.SEVERE,"999999999999999999999999999999");

			if (upcflag == true) {
				createCompanyAddressDetails();
			}

			logger.log(Level.SEVERE," rohan printProductPremises Flag "
					+ printProductPremisesFlag);
			if (printProductPremisesFlag) {

				if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
					logger.log(Level.SEVERE,"inside both");
					createProductDetailWithDiscount();
				} else if (nothing > 0) {
					logger.log(Level.SEVERE,"inside nothing");
					createProductWithoutDiscDetail();
				} else {
					logger.log(Level.SEVERE,"inside one of the condition");
					createProductDetail();
				}
			} else {
				if ((both > 0) || ((discAmt > 0) && (discPer > 0))) {
					logger.log(Level.SEVERE,"inside both");
					createProductDetailWithDiscount();
					// createProductDetailsVal();

				} else if (nothing > 0) {
					logger.log(Level.SEVERE,"inside nothing");
					createProductWithoutDiscDetail();
				} else {
					logger.log(Level.SEVERE,"inside one of the condition");
					createProductDetail();
				}
			}
			termsConditionsInfo();
			footerInfo();
		} else {
			if (upcflag == false && preprint.equals("plane")) {
				createLogo(document, comp);
				createHeader();
				// createCompanyAddress();
			} else {

				if (preprint.equals("yes")) {
					logger.log(Level.SEVERE,"inside prit yes");
					createBlankforUPC();
				}
				if (preprint.equals("no")) {
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document, comp);
					}

					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
					createBlankforUPC();
				}
			}		
			createInvoiceDetails();
			createCustomerDetails();
			createProductDetails();
			
			createProductDetailsVal();
			createFooterTaxPart();
			createFooterLastPart(preprint);	
			createProductDescription(); 

		}
	
		
	}
	/**
	 * Ajinkya Added this Method To display description of product on Date :
	 * 17/07/2017
	 */
	public void createProductDescription() {

		/**
		 * Date : 27-07-2017 By ANIL
		 */
		if(!productDescFlag){
			return;
		}
		/**
		 * End
		 */
		Phrase prodDetailsHeader = new Phrase("Product Details ", font12boldul);
		Paragraph productDeatilsLblPara = new Paragraph();
		productDeatilsLblPara.add(prodDetailsHeader);
		productDeatilsLblPara.add(Chunk.NEWLINE);
		productDeatilsLblPara.add(Chunk.NEWLINE);
		productDeatilsLblPara.setAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(Chunk.NEXTPAGE);
			document.add(productDeatilsLblPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		
		for (int i = 0; i < this.stringlis.size(); i++) {
			
			PdfPTable prodDescriptionTbl = new PdfPTable(2);
			prodDescriptionTbl.setWidthPercentage(100);

			try {
				prodDescriptionTbl.setWidths(new float[] { 40, 60 });
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			Phrase productIdLbl = new Phrase("Product Id : "+ stringlis.get(i).getCount(), font10bold);
			PdfPCell productIdLblCell = new PdfPCell(productIdLbl);
			productIdLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			productIdLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productIdLblCell.setBorder(0);
			prodDescriptionTbl.addCell(productIdLblCell);

			String prodNameValue = stringlis.get(i).getProductName();
			Phrase prodNameLbl = new Phrase("Product Name : " + prodNameValue,font10bold);
			PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
			prodlblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodlblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodlblCell.setBorder(0);

			prodDescriptionTbl.addCell(prodlblCell);

			String prodDescriptionValue = "";
			
			/**
			 * @author Anil,Date : 18-02-2019
			 * issue : product description was not getting printed for service quotation pdf
			 * for bitco raised by Rahul Tiwari
			 */
			System.out.println("INSIDE PRODUCT DESCRIPTION : ");
			SuperProduct prod= stringlis.get(i);
			if(prod.getComment()!=null){
//			if(prod.getComment()!=null&&!prod.getComment().equals("")){
				prodDescriptionValue=prodDescriptionValue+prod.getComment();
				System.out.println("1 :"+prodDescriptionValue);
			}
			
			if(prod.getCommentdesc()!=null){
//			if(prod.getCommentdesc()!=null&&!prod.getCommentdesc().equals("")){
				prodDescriptionValue=prodDescriptionValue+prod.getCommentdesc();
				System.out.println("2 :"+prodDescriptionValue);
			}
			
			if(prod.getCommentdesc1()!=null){
//			if(prod.getCommentdesc1()!=null&&!prod.getCommentdesc1().equals("")){
				prodDescriptionValue=prodDescriptionValue+prod.getCommentdesc1();
				System.out.println("3 :"+prodDescriptionValue);
			}
			
			if(prod.getCommentdesc2()!=null){
//			if(prod.getCommentdesc2()!=null&&!prod.getCommentdesc2().equals("")){
				prodDescriptionValue=prodDescriptionValue+prod.getCommentdesc2();
				System.out.println("4 :"+prodDescriptionValue);
			}
			
			
			System.out.println("PRODUCT DESCRIPTION : "+prodDescriptionValue);
			

//			if (!stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() != null
//					&& !stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() != null
//					&& !stringlis.get(i).getCommentdesc2().equals("")
//					&& stringlis.get(i).getCommentdesc2() != null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc()
//						+ stringlis.get(i).getCommentdesc1()
//						+ stringlis.get(i).getCommentdesc2();
//			} else if (stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() == null
//					&& !stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() != null
//					&& !stringlis.get(i).getCommentdesc2().equals("")
//					&& stringlis.get(i).getCommentdesc2() != null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc1()
//						+ stringlis.get(i).getCommentdesc2();
//			} else if (!stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() != null
//					&& stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() == null
//					&& !stringlis.get(i).getCommentdesc2().equals("")
//					&& stringlis.get(i).getCommentdesc2() != null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc()
//						+ stringlis.get(i).getCommentdesc2();
//			} else if (!stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() != null
//					&& !stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() != null
//					&& stringlis.get(i).getCommentdesc2().equals("")
//					&& stringlis.get(i).getCommentdesc2() == null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc()
//						+ stringlis.get(i).getCommentdesc1();
//			} else if (stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() == null
//					&& stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() == null
//					&& stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc2() != null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc2();
//			} else if (stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() == null
//					&& !stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() != null
//					&& stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() == null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc1();
//			}
//
//			else if (!stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() != null
//					&& stringlis.get(i).getCommentdesc1().equals("")
//					&& stringlis.get(i).getCommentdesc1() == null
//					&& stringlis.get(i).getCommentdesc().equals("")
//					&& stringlis.get(i).getCommentdesc() == null) {
//				prodDescriptionValue = stringlis.get(i).getCommentdesc();
//			} else {
//				prodDescriptionValue = "";
//
//			}

			// Phrase prodDescriptionVal = new
			// Phrase(""+prodDescriptionValue,font8);
			// PdfPCell prodDescriptionValueCell = new
			// PdfPCell(prodDescriptionVal);
			// prodDescriptionValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// prodDescriptionValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// prodDescriptionValueCell.setBorder(0);
			// prodDescriptionTbl.addCell(prodDescriptionValueCell);
			//
			// Phrase blnk = new Phrase("",font8);
			// PdfPCell blnkCell = new PdfPCell(blnk);
			// blnkCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// blnkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// blnkCell.setBorder(0);

			// prodDescriptionTbl.addCell(blnkCell);

			Paragraph value = new Paragraph(prodDescriptionValue,font9);
			value.setAlignment(Element.ALIGN_LEFT);

			try {
//				document.add(Chunk.NEXTPAGE);
//				document.add(productDeatilsLblPara);
				document.add(prodDescriptionTbl);
				document.add(value);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

		}

	}

	private void createProductWithoutDiscDetail() {

		/**
		 * rohan added this code for adding premises in the pdf
		 */
		PdfPTable premisesTable = new PdfPTable(1);
		premisesTable.setWidthPercentage(100f);
		/**
		 * ends here
		 */

		Font font1 = new Font(Font.FontFamily.HELVETICA, 7 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(10);
		table.setWidthPercentage(100);
		Phrase srno = new Phrase("SrNo", font1);
		Phrase product = new Phrase("Item Details", font1);
		Phrase qty = new Phrase("No of Branches", font1);
//		Phrase area = new Phrase("AREA", font1);//commented by AShwini
		Phrase area = new Phrase("Qty", font1); //Added by Ashwini
		Phrase startdate = new Phrase("Start Date", font1);
		Phrase duration = new Phrase("Duration(d)", font1);
		Phrase noservices = new Phrase("Services", font1);
		Phrase rate = new Phrase("Rate", font1);

		Phrase servicetax = null;
		Phrase servicetax1 = null;
		int flag1 = 0;
		int vat = 0;
		int st = 0;
		for (int i = 0; i < products.size(); i++) {
			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() == 0)) {
				servicetax = new Phrase("VAT(%)", font1);
				System.out.println("Rohan1");
				vat = vat + 1;
				System.out.println("phrase value====" + servicetax.toString());
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {
				servicetax = new Phrase("ST(%)", font1);
				System.out.println("Rohan22222222222");
				st = st + 1;
			} else if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {
				servicetax1 = new Phrase("VAT / ST(%)", font1);
				System.out.println("Rohan3333333333333");
				flag1 = flag1 + 1;
				System.out.println("flag value;;;;;" + flag1);
			} else {

				servicetax = new Phrase("TAX(%)", font1);
				System.out.println("Rohan4444444444444");
			}
		}

		Phrase total = new Phrase("Total", font1);

		System.out.println("vat===" + vat);
		System.out.println("st====" + st);
		System.out.println("flag1===" + flag1);
		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		// PdfPCell cellcategory = new PdfPCell(category);
		// cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellarea = new PdfPCell(area);
		cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellstartdate = new PdfPCell(startdate);
		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicecell = new PdfPCell(noservices);
		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellservicetax1 = null;
		PdfPCell cellservicetax = null;
		PdfPCell cellservicetax2 = null;
		Phrase servicetax2 = null;

		if (flag1 > 0) {

			cellservicetax1 = new PdfPCell(servicetax1);
			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);

		} else if (vat > 0 && st > 0) {
			System.out.println("in side condition");
			servicetax2 = new Phrase("VAT / ST(%)", font1);
			cellservicetax2 = new PdfPCell(servicetax2);
			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {

			cellservicetax = new PdfPCell(servicetax);
			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);

		}

		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellsrNo);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellarea);
		table.addCell(cellstartdate);
		table.addCell(durationcell);
		table.addCell(servicecell);
		table.addCell(cellrate);

		if (flag1 > 0) {
			table.addCell(cellservicetax1);
		} else if (vat > 0 && st > 0) {
			table.addCell(cellservicetax2);
		} else {
			table.addCell(cellservicetax);
		}
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidths10);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell tableCell = new PdfPCell(table);
		premisesTable.addCell(tableCell);

		// //////////dummy..............change
		if (this.products.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size();
			blankLines = size * (185 / 17);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 10f;
		}
		// premisesTable.setSpacingAfter(blankLines);

		Paragraph pararefer = null;

		for (int i = 0; i < this.products.size(); i++) {

			PdfPTable table1 = new PdfPTable(10);
			table1.setWidthPercentage(100);

			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font8);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// chunk=new
			// Phrase(products.get(i).getPrduct().getCount()+"",font8);
			// PdfPCell pdfcategcell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setColspan((int) 1.5);
			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getArea() + "", font8);
			PdfPCell pdfareacell = new PdfPCell(chunk);
			pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			String date = fmt.format(products.get(i).getStartDate());
			chunk = new Phrase(date, font8);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
					font8);
			pdfSerCell = new PdfPCell(chunk);
			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getDuration() + "", font8);
			PdfPCell pdfdurCell = new PdfPCell(chunk);
			pdfdurCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;

			chunk = new Phrase(df.format(calculatedPrice), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			PdfPCell pdfservice = null;
			Phrase vatno = null;
			Phrase stno = null;
			if ((products.get(i).getVatTax().getPercentage() == 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {

				if (products.get(i).getServiceTax().getPercentage() != 0) {
					if (st == products.size()) {
						chunk = new Phrase(products.get(i).getServiceTax()
								.getPercentage()
								+ "", font8);
					} else {
						stno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage() + "", font8);
					}
				} else
					chunk = new Phrase("N.A" + "", font8);
				if (st == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(stno);
				}

				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {

				if (products.get(i).getVatTax() != null) {
					if (vat == products.size()) {
						System.out.println("rohan==" + vat);
						chunk = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ "", font8);
					} else {
						System.out.println("mukesh");
						vatno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage(), font8);
					}
				} else {
					chunk = new Phrase("N.A" + "", font8);
				}
				if (vat == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(vatno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {
				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null))

					chunk = new Phrase(products.get(i).getVatTax()
							.getPercentage()
							+ ""
							+ " / "
							+ ""
							+ products.get(i).getServiceTax().getPercentage(),
							font8);
				else
					chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else {

				chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			// if(discPer >0)
			// {
			// if(products.get(i).getPercentageDiscount()!=0){
			// chunk=new
			// Phrase(products.get(i).getPercentageDiscount()+"",font8);
			// }
			// else{
			// chunk=new Phrase("0"+"",font8);
			// }
			// }
			// else
			// {
			// if(products.get(i).getDiscountAmt()!=0){
			// chunk=new Phrase(products.get(i).getDiscountAmt()+"",font8);
			// }
			// else{
			// chunk=new Phrase("0"+"",font8);
			// }
			// }
			// PdfPCell realpercdisccell=new PdfPCell(chunk);
			// realpercdisccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// realpercdisccell.setColspan((int)0.5);

			// rohan remove qty from calculations because we have remove that
			// from contract calculations also
			double totalVal = (products.get(i).getPrice() - taxVal);
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}

			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			count = i;

			table1.addCell(Pdfsrnocell);
			// table.addCell(pdfcategcell);
			table1.addCell(pdfnamecell);
			table1.addCell(pdfqtycell);
			table1.addCell(pdfareacell);
			table1.addCell(pdfdatecell);
			table1.addCell(pdfdurCell);
			table1.addCell(pdfSerCell);
			table1.addCell(pdfspricecell);
			table1.addCell(pdfservice);
			// table1.addCell(pdfvattax);
			// table1.addCell(realpercdisccell);
			table1.addCell(pdftotalproduct);
			try {
				table1.setWidths(columnWidths10);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			/**
			 * rohan added code here for premises details Date : 27/01/2017
			 */

			PdfPCell table123Cell = new PdfPCell(table1);

			premisesTable.addCell(table123Cell);

			String premises = "";
			if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premise Details : "
						+ products.get(i).getPremisesDetails();
			} else {
				premises = "Premise Details : " + "N A";
			}

			Phrase premisesPh = new Phrase(premises, font8);
			PdfPCell premisesCell = new PdfPCell(premisesPh);

			premisesTable.addCell(premisesCell);

			/**
			 * ends here
			 */

			// count=i;

			if (count == firstBreakPoint) {

				Phrase referannexure = new Phrase(
						"Refer Annexure 1 for additional products ", font8);
				pararefer = new Paragraph();
				pararefer.add(referannexure);

				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();
			}
		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(premisesTable);
		prodtablecell.addElement(pararefer);

		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}

	// ////////////////////// Ajinkya Added Product Details on Date : 18/07/2017
	// //////////////////////////
	private void createProductDetailsVal() {

		System.out.println("Inside Prod Details Val");
		// ///////////////////////////////////////////////// Ajinkya
		// //////////////
		if (qp instanceof Contract) {
			System.out.println("Inside Instance con");
			System.out.println("Inside  Condn of Instance con and quot ");

			int firstBreakPoint = 5;
			if(printModelSerailNoFlag && printPremiseDetails){
				firstBreakPoint = 4;
			}
			float blankLines = 0;
			Contract contEntity = (Contract) qp;
			// Contract contractEntity=(Contract)qp;
			if (contEntity.getItems().size() <= firstBreakPoint) {

				int size = firstBreakPoint - contEntity.getItems().size();
				blankLines = size * (100 / firstBreakPoint);
				System.out.println("blankLines size =" + blankLines);
			} else {
				blankLines = 10f;
			}

			// PdfPTable premiseTable=new PdfPTable(1);
			// premiseTable.setWidthPercentage(100);

			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			for (int i = 0; i < contEntity.getItems().size(); i++) {

				if (i == 5) {
					break;
				}

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell();
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				srNoCell.addElement(srNo);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(contEntity.getItems().get(i)
						.getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				serviceNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (contEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
					hsnCode = new Phrase(contEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(contEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
				uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				uomCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(contEntity.getItems().get(i).getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
				qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				qtyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(contEntity.getItems().get(i)
						.getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell(rate);
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(rateCell);

				double amountValue = contEntity.getItems().get(i).getPrice()
						* contEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell(amount);
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(amountCell);

				double disPercentTotalAmount=0;
				if(contEntity.getItems().get(i).getPercentageDiscount()==null||contEntity.getItems().get(i).getPercentageDiscount()==0){
					disPercentTotalAmount=0;
				}else{
					disPercentTotalAmount=getPercentAmount(contEntity.getItems().get(i),false);
					
				}
				Phrase disc = new Phrase(df.format(disPercentTotalAmount+contEntity.getItems().get(i)
						.getDiscountAmt())
						+ "", font8);
				PdfPCell discCell = new PdfPCell(disc);
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(discCell);

				Phrase taxableValue = new Phrase(df.format(getAssessTotalAmount(contEntity.getItems().get(i)))
						+ "", font8);
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				taxableValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(taxableValueCell);

				PdfPCell cellIGST;
				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				boolean taxPresent=validateTaxes(contEntity.getItems().get(i));
				if(taxPresent){
				if (contEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("IGST")) {
					double asstotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
							
					double taxAmount = getTaxAmount(asstotalAmount, contEntity.getItems().get(i)
							.getVatTax().getPercentage());
					double indivTotalAmount = asstotalAmount
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					// igstRateValCell.setBorder(0);
					igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase igstRate = new Phrase(contEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					// igstRateCell.setBorder(0);
					// igstRateCell.setColspan(2);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cell);
					productTable.addCell(cell);

					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);

					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = contEntity.getItems().get(i)
							.getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase(
								"Premise Details : " + premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					/**
					 * nidhi
					 * for print model and serial number
					 */
					int cnnt = 0;
					PdfPCell proModelcell = null ,proSerialNocell = null; 
					String proModelNo = "";
					String proSerialNo = "";
					if(printModelSerailNoFlag){
//						String proModelNo = "";
						if(contEntity.getItems().get(i).getProModelNo()!=null && 
							contEntity.getItems().get(i).getProModelNo().trim().length() >0){
							proModelNo = contEntity.getItems().get(i).getProModelNo();
						}
						
//						String proSerialNo = "";
						if(contEntity.getItems().get(i).getProSerialNo()!=null && 
							contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
							proSerialNo = contEntity.getItems().get(i).getProSerialNo();
						}
						
						if(proModelNo.length()>0){
							Phrase modelValPhrs = new Phrase(
									"Model No : " + proModelNo, font8);
							proModelcell = new PdfPCell();
							proModelcell.setColspan(8);
							proModelcell.addElement(modelValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						if(proSerialNo.length()>0){
							Phrase serialValPhrs = new Phrase(
									"Serial No : " + proSerialNo, font8);
							proSerialNocell = new PdfPCell();
							proSerialNocell.setColspan(8);
							proSerialNocell.addElement(serialValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						
						if(cnnt>1 ){
//							proSerialNocell.setColspan(8);
							/*
							 * Added by Ashwini
							 */
							
							if(proSerialNocell!=null){   //Added by Ashwini
								proSerialNocell.setColspan(8);
								}
							
						}else if(proModelNo.length()>0){
							proModelcell.setColspan(16);
//						}else {
						}else if(proSerialNocell!=null) {  //added by ashwini
							proSerialNocell.setColspan(16);
						}
						
						if(cnnt>0);
						{
//							noOfLines = noOfLines-1;
//							table1.addCell(Pdfsrnocell2);
							if(proModelcell!=null){
								productTable.addCell(proModelcell);
							}
							if(proSerialNocell!=null){
								productTable.addCell(proSerialNocell);
							}
						}
					}
					/**
					 * end
					 */
				}else if(contEntity.getItems().get(i).getServiceTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")){

					double assTotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
					double taxAmount = getTaxAmount(assTotalAmount, contEntity.getItems().get(i).getServiceTax()
							.getPercentage());
					double indivTotalAmount = assTotalAmount+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase igstRate = new Phrase(contEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cell);
					productTable.addCell(cell);
					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setBorder(0);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = contEntity.getItems().get(i).getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase("Premise Details : "
								+ premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					
//					try {
//						document.add(productTable);
//						// document.add(productTableWithOutPremise);
//
//					} catch (DocumentException e) {
//						e.printStackTrace();
//						logger.log(Level.SEVERE, "ERROR Adding in Table!!!!" + e);
//					}
					/**
					 * nidhi
					 * for print model and serial number
					 */
					int cnnt = 0;
					PdfPCell proModelcell = null ,proSerialNocell = null; 
					String proModelNo = "";
					String proSerialNo = "";
					if(printModelSerailNoFlag){
//						String proModelNo = "";
						if(contEntity.getItems().get(i).getProModelNo()!=null && 
							contEntity.getItems().get(i).getProModelNo().trim().length() >0){
							proModelNo = contEntity.getItems().get(i).getProModelNo();
						}
						
//						String proSerialNo = "";
						if(contEntity.getItems().get(i).getProSerialNo()!=null && 
							contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
							proSerialNo = contEntity.getItems().get(i).getProSerialNo();
						}
						
						if(proModelNo.length()>0){
							Phrase modelValPhrs = new Phrase(
									"Model No : " + proModelNo, font8);
							proModelcell = new PdfPCell();
							proModelcell.setColspan(8);
							proModelcell.addElement(modelValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						if(proSerialNo.length()>0){
							Phrase serialValPhrs = new Phrase(
									"Serial No : " + proSerialNo, font8);
							proSerialNocell = new PdfPCell();
							proSerialNocell.setColspan(8);
							proSerialNocell.addElement(serialValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						
						if(cnnt>1 ){
							proSerialNocell.setColspan(8);
						}else if(proModelNo.length()>0){
							proModelcell.setColspan(16);
						}else {
							proSerialNocell.setColspan(16);
						}
						
						if(cnnt>0);
						{
//							noOfLines = noOfLines-1;
//							table1.addCell(Pdfsrnocell2);
							if(proModelcell!=null){
								productTable.addCell(proModelcell);
							}
							if(proSerialNocell!=null){
								productTable.addCell(proSerialNocell);
							}
						}
					}
					/**
					 * end
					 */
				} else {

					if (contEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {
						
						double asstotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount, contEntity.getItems()
								.get(i).getVatTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
						// cgstRateValCell.setBorder(0);
						// cgstRateValCell.setBorderWidthBottom(0);
						// cgstRateValCell.setBorderWidthTop(0);
						// cgstRateValCell.setBorderWidthRight(0);
						cgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						cgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);

						Phrase cgstRate = new Phrase(contEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell(cgstRate);
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount, contEntity.getItems()
								.get(i).getServiceTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
						sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						sgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);

						Phrase sgstRate = new Phrase(contEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell(sgstRate);
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);

						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount+ ctaxValue + staxValue;
						
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

						productTable.addCell(totalCell);

						String premisesVal = "";
						premisesVal = contEntity.getItems().get(i)
								.getPremisesDetails();
						if (printPremiseDetails) {
							Phrase premisesValPhrs = new Phrase(
									"Premise Details : " + premisesVal, font8);
							PdfPCell premiseCell = new PdfPCell();
							premiseCell.setColspan(16);
							premiseCell.addElement(premisesValPhrs);
							productTable.addCell(premiseCell);
						}
						/**
						 * nidhi
						 * for print model and serial number
						 */
						int cnnt = 0;
						PdfPCell proModelcell = null ,proSerialNocell = null; 
						String proModelNo = "";
						String proSerialNo = "";
						if(printModelSerailNoFlag){
//							String proModelNo = "";
							if(contEntity.getItems().get(i).getProModelNo()!=null && 
								contEntity.getItems().get(i).getProModelNo().trim().length() >0){
								proModelNo = contEntity.getItems().get(i).getProModelNo();
							}
							
//							String proSerialNo = "";
							if(contEntity.getItems().get(i).getProSerialNo()!=null && 
								contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
								proSerialNo = contEntity.getItems().get(i).getProSerialNo();
							}
							
							if(proModelNo.length()>0){
								Phrase modelValPhrs = new Phrase(
										"Model No : " + proModelNo, font8);
								proModelcell = new PdfPCell();
								proModelcell.setColspan(8);
								proModelcell.addElement(modelValPhrs);
//								productTable.addCell(premiseCell);
								++cnnt;
							}
							if(proSerialNo.length()>0){
								Phrase serialValPhrs = new Phrase(
										"Serial No : " + proSerialNo, font8);
								proSerialNocell = new PdfPCell();
								proSerialNocell.setColspan(8);
								proSerialNocell.addElement(serialValPhrs);
//								productTable.addCell(premiseCell);
								++cnnt;
							}
							
							if(cnnt>1 ){
								proSerialNocell.setColspan(8);
							}else if(proModelNo.length()>0){
								proModelcell.setColspan(16);
							}else {
								proSerialNocell.setColspan(16);
							}
							
							if(cnnt>0);
							{
//								noOfLines = noOfLines-1;
//								table1.addCell(Pdfsrnocell2);
								if(proModelcell!=null){
									productTable.addCell(proModelcell);
								}
								if(proSerialNocell!=null){
									productTable.addCell(proSerialNocell);
								}
							}
						}
						/**
						 * end
						 */
					} else if (contEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("SGST")) {
						double asstotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount, contEntity.getItems()
								.get(i).getServiceTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
						cgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						cgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);

						Phrase cgstRate = new Phrase(contEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell(cgstRate);
						cgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount, contEntity.getItems()
								.get(i).getVatTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
						sgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						sgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);

						Phrase sgstRate = new Phrase(contEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell(sgstRate);
						sgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);

						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(totalCell);

						String premisesVal = "";
						premisesVal = contEntity.getItems().get(i)
								.getPremisesDetails();
						if (printPremiseDetails) {
							Phrase premisesValPhrs = new Phrase(
									"Premise Details : " + premisesVal, font8);
							PdfPCell premiseCell = new PdfPCell();
							premiseCell.setColspan(16);
							premiseCell.addElement(premisesValPhrs);
							productTable.addCell(premiseCell);
						}
						
						/**
						 * nidhi
						 * for print model and serial number
						 */
						int cnnt = 0;
						PdfPCell proModelcell = null ,proSerialNocell = null; 
						String proModelNo = "";
						String proSerialNo = "";
						if(printModelSerailNoFlag){
//							String proModelNo = "";
							if(contEntity.getItems().get(i).getProModelNo()!=null && 
								contEntity.getItems().get(i).getProModelNo().trim().length() >0){
								proModelNo = contEntity.getItems().get(i).getProModelNo();
							}
							
//							String proSerialNo = "";
							if(contEntity.getItems().get(i).getProSerialNo()!=null && 
								contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
								proSerialNo = contEntity.getItems().get(i).getProSerialNo();
							}
							
							if(proModelNo.length()>0){
								Phrase modelValPhrs = new Phrase(
										"Model No : " + proModelNo, font8);
								proModelcell = new PdfPCell();
								proModelcell.setColspan(8);
								proModelcell.addElement(modelValPhrs);
//								productTable.addCell(premiseCell);
								++cnnt;
							}
							if(proSerialNo.length()>0){
								Phrase serialValPhrs = new Phrase(
										"Serial No : " + proSerialNo, font8);
								proSerialNocell = new PdfPCell();
								proSerialNocell.setColspan(8);
								proSerialNocell.addElement(serialValPhrs);
//								productTable.addCell(premiseCell);
								++cnnt;
							}
							
							if(cnnt>1 ){
//								proSerialNocell.setColspan(8);
								if(proSerialNocell!=null){   //added by Ashwini
									proSerialNocell.setColspan(8);
								}
							}else if(proModelNo.length()>0){
								proModelcell.setColspan(16);
//							}else {                           //commented by Ashwini
							}else if(proSerialNocell!=null){  //Added by Ashwini
								proSerialNocell.setColspan(16);
							}
							
							if(cnnt>0);
							{
//								noOfLines = noOfLines-1;
//								table1.addCell(Pdfsrnocell2);
								if(proModelcell!=null){
									productTable.addCell(proModelcell);
								}
								if(proSerialNocell!=null){
									productTable.addCell(proSerialNocell);
								}
							}
						}
						/**
						 * end
						 */
					}

				}
				}else{

					logger.log(Level.SEVERE,"Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(df.format(getAssessTotalAmount(contEntity.getItems().get(i)))
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if(contEntity.getItems().get(i).getPremisesDetails()!=null){
						premisesVal=contEntity.getItems().get(i).getPremisesDetails();
					}
					
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase("Premise Details : "
								+ premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					
					/**
					 * nidhi
					 * for print model and serial number
					 */
					int cnnt = 0;
					PdfPCell proModelcell = null ,proSerialNocell = null; 
					String proModelNo = "";
					String proSerialNo = "";
					if(printModelSerailNoFlag){
//						String proModelNo = "";
						if(contEntity.getItems().get(i).getProModelNo()!=null && 
							contEntity.getItems().get(i).getProModelNo().trim().length() >0){
							proModelNo = contEntity.getItems().get(i).getProModelNo();
						}
						
//						String proSerialNo = "";
						if(contEntity.getItems().get(i).getProSerialNo()!=null && 
							contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
							proSerialNo = contEntity.getItems().get(i).getProSerialNo();
						}
						
						if(proModelNo.length()>0){
							Phrase modelValPhrs = new Phrase(
									"Model No : " + proModelNo, font8);
							proModelcell = new PdfPCell();
							proModelcell.setColspan(8);
							proModelcell.addElement(modelValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						if(proSerialNo.length()>0){
							Phrase serialValPhrs = new Phrase(
									"Serial No : " + proSerialNo, font8);
							proSerialNocell = new PdfPCell();
							proSerialNocell.setColspan(8);
							proSerialNocell.addElement(serialValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						
						if(cnnt>1 ){
							proSerialNocell.setColspan(8);
						}else if(proModelNo.length()>0){
							proModelcell.setColspan(16);
						}else {
							proSerialNocell.setColspan(16);
						}
						
						if(cnnt>0);
						{
//							noOfLines = noOfLines-1;
//							table1.addCell(Pdfsrnocell2);
							if(proModelcell!=null){
								productTable.addCell(proModelcell);
							}
							if(proSerialNocell!=null){
								productTable.addCell(proSerialNocell);
							}
						}
					}
					/**
					 * end
					 */
				}
			}

			PdfPCell productTableCell = null;
			if (contEntity.getItems().size() > firstBreakPoint) {
				Phrase my = new Phrase(
						"Please Refer Annexure For More Details", font8);
				productTableCell = new PdfPCell(my);

			} else {
				Phrase my = new Phrase("", font8);
				productTableCell = new PdfPCell(my);
			}
			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100f);
			tab.addCell(productTableCell);
			// tab.addCell(premiseTblCell);
			tab.setSpacingAfter(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1;
			tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100f);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);

			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("Inside quotation ");
			System.out.println("Else Condn of Instance con and quot ");
			// Ajinkya Start Here
			int firstBreakPoint = 5;
			float blankLines = 0;
			Quotation quotEntity = (Quotation) qp;

			if (quotEntity.getItems().size() <= firstBreakPoint) {
				int size = firstBreakPoint - quotEntity.getItems().size();
				blankLines = size * (100 / 5);
				System.out.println("blankLines size =" + blankLines);
			} else {
				blankLines = 10f;
			}

			// PdfPTable premiseTbl =new PdfPTable(1);
			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			for (int i = 0; i < quotEntity.getItems().size(); i++) {
				logger.log(Level.SEVERE, "Inside Items " + i);

				if (i == 5) {
					break;
				}

				int srNoVal = i + 1;
				Phrase srNo = new Phrase(srNoVal + "", font8);
				PdfPCell srNoCell = new PdfPCell(srNo);
				// srNoCell.setBorderWidthBottom(0);
				// srNoCell.setBorderWidthTop(0);
				srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(srNoCell);

				Phrase serviceName = new Phrase(quotEntity.getItems().get(i)
						.getProductName().trim(), font8);
				PdfPCell serviceNameCell = new PdfPCell(serviceName);
				// serviceNameCell.setBorder(0);
				// serviceNameCell.setBorderWidthBottom(0);
				// serviceNameCell.setBorderWidthTop(0);
				serviceNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				serviceNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(serviceNameCell);

				Phrase hsnCode = null;

				if (quotEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
					hsnCode = new Phrase(quotEntity.getItems().get(i)
							.getPrduct().getHsnNumber().trim(), font8);
				} else {
					hsnCode = new Phrase("", font8);
				}
				PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
				// hsnCodeCell.setBorder(0);
				// hsnCodeCell.setBorderWidthBottom(0);
				// hsnCodeCell.setBorderWidthTop(0);
				hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				hsnCodeCell.addElement(hsnCode);
				productTable.addCell(hsnCodeCell);

				Phrase uom = new Phrase(quotEntity.getItems().get(i)
						.getUnitOfMeasurement().trim(), font8);
				PdfPCell uomCell = new PdfPCell(uom);
				// uomCell.setBorder(0);
				// uomCell.setBorderWidthBottom(0);
				// uomCell.setBorderWidthTop(0);
				uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				uomCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(uomCell);

				Phrase qty = new Phrase(quotEntity.getItems().get(i).getQty()
						+ "", font8);
				PdfPCell qtyCell = new PdfPCell(qty);
				// qtyCell.setBorder(0);
				// qtyCell.setBorderWidthBottom(0);
				// qtyCell.setBorderWidthTop(0);
				qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				qtyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(qtyCell);

				Phrase rate = new Phrase(df.format(quotEntity.getItems().get(i)
						.getPrice())
						+ "", font8);
				PdfPCell rateCell = new PdfPCell(rate);
				// rateCell.setBorder(0);
				// rateCell.setBorderWidthBottom(0);
				// rateCell.setBorderWidthTop(0);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(rateCell);

				double amountValue = quotEntity.getItems().get(i).getPrice()
						* quotEntity.getItems().get(i).getQty();

				totalAmount = totalAmount + amountValue;
				Phrase amount = new Phrase(df.format(amountValue) + "", font8);
				PdfPCell amountCell = new PdfPCell(amount);
				// amountCell.setBorder(0);
				// amountCell.setBorderWidthBottom(0);
				// amountCell.setBorderWidthTop(0);
				amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(amountCell);

				double disPercentTotalAmount=0;
				if(quotEntity.getItems().get(i).getPercentageDiscount()==null||quotEntity.getItems().get(i).getPercentageDiscount()==0){
					disPercentTotalAmount=0;
				}else{
					disPercentTotalAmount=getPercentAmount(quotEntity.getItems().get(i),false);
					
				}
				Phrase disc = new Phrase(df.format(disPercentTotalAmount+quotEntity.getItems().get(i)
						.getDiscountAmt())
						+ "", font8);
				PdfPCell discCell = new PdfPCell(disc);
				// discCell.setBorder(0);
				// discCell.setBorderWidthBottom(0);
				// discCell.setBorderWidthTop(0);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(discCell);

				Phrase taxableValue = new Phrase(df.format(getAssessTotalAmount(quotEntity.getItems().get(i)))
						+ "", font8);
				PdfPCell taxableValueCell = new PdfPCell(taxableValue);
				// taxableValueCell.setBorder(0);
				// taxableValueCell.setBorderWidthBottom(0);
				// taxableValueCell.setBorderWidthTop(0);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				taxableValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(taxableValueCell);

				// PdfPCell cellIGST;

				// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
				boolean taxPresent=validateTaxes(quotEntity.getItems().get(i));
				if(taxPresent){
				if (quotEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("IGST")) {
					double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
					double taxAmount = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i)
							.getVatTax().getPercentage());
					double indivTotalAmount = asstotalAmount
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase igstRate = new Phrase(quotEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cell);
					productTable.addCell(cell);
					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setBorder(0);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = quotEntity.getItems().get(i)
							.getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase(
								"Premise Details : " + premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
				} else if(quotEntity.getItems().get(i).getServiceTax()
						.getTaxPrintName().equalsIgnoreCase("IGST")){

					double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
					double taxAmount = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i).getServiceTax()
							.getPercentage());
					double indivTotalAmount = asstotalAmount
							+ taxAmount;
					totalAmount = totalAmount + indivTotalAmount;

					Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
							font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase igstRate = new Phrase(quotEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					/* for Cgst */

					Phrase cgst = new Phrase("-", font8);
					PdfPCell cell = new PdfPCell(cgst);
					// cell.addElement(cgst);
					// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cell);
					productTable.addCell(cell);
					/* for Sgst */
					Phrase sgst = new Phrase("-", font8);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(igstRateCell);
					productTable.addCell(igstRateValCell);

					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setBorder(0);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = quotEntity.getItems().get(i).getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase("Premise Details : "
								+ premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					try {
						document.add(productTable);
						// document.add(productTableWithOutPremise);

					} catch (DocumentException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "ERROR Adding in Table!!!!" + e);
					}
				
				} else {

					if (quotEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("CGST")) {
						double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount, quotEntity.getItems()
								.get(i).getVatTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
						cgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						cgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);

						Phrase cgstRate = new Phrase(quotEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell(cgstRate);
						cgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount, quotEntity.getItems()
								.get(i).getServiceTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
						sgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);
						sgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);

						Phrase sgstRate = new Phrase(quotEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell(sgstRate);
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

						productTable.addCell(totalCell);

						String premisesVal = "";
						premisesVal = quotEntity.getItems().get(i)
								.getPremisesDetails();
						if (printPremiseDetails) {
							Phrase premisesValPhrs = new Phrase(
									"Premise Details : " + premisesVal, font8);
							PdfPCell premiseCell = new PdfPCell();
							premiseCell.setColspan(16);
							premiseCell.addElement(premisesValPhrs);
							productTable.addCell(premiseCell);
						}
					} else if (quotEntity.getItems().get(i).getVatTax()
							.getTaxPrintName().equalsIgnoreCase("SGST")) {
						double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
						double ctaxValue = getTaxAmount(asstotalAmount, quotEntity.getItems()
								.get(i).getServiceTax().getPercentage());

						Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
								+ "", font8);
						PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
						cgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);

						Phrase cgstRate = new Phrase(quotEntity.getItems()
								.get(i).getVatTax().getPercentage()
								+ "", font8);
						PdfPCell cgstRateCell = new PdfPCell(cgstRate);
						// cgstRateCell.setBorder(0);
						// cgstRateCell.setBorderWidthBottom(0);
						// cgstRateCell.setBorderWidthTop(0);
						// cgstRateCell.setBorderWidthLeft(0);
						cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						productTable.addCell(cgstRateCell);
						productTable.addCell(cgstRateValCell);

						double staxValue = getTaxAmount(asstotalAmount, quotEntity.getItems()
								.get(i).getVatTax().getPercentage());
						Phrase sgstRateVal = new Phrase(df.format(staxValue)
								+ "", font8);
						PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
						// sgstRateValCell.setBorder(0);
						// sgstRateValCell.setBorderWidthBottom(0);
						// sgstRateValCell.setBorderWidthTop(0);
						// sgstRateValCell.setBorderWidthRight(0);
						sgstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);
						sgstRateValCell
								.setHorizontalAlignment(Element.ALIGN_RIGHT);

						Phrase sgstRate = new Phrase(quotEntity.getItems()
								.get(i).getServiceTax().getPercentage()
								+ "", font8);
						PdfPCell sgstRateCell = new PdfPCell(sgstRate);
						// sgstRateCell.setBorder(0);
						// sgstRateCell.setBorderWidthBottom(0);
						// sgstRateCell.setBorderWidthTop(0);
						// sgstRateCell.setBorderWidthLeft(0);
						sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						sgstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						productTable.addCell(sgstRateCell);
						productTable.addCell(sgstRateValCell);

						Phrase igstRateVal = new Phrase("-", font8);
						PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
						// igstRateValCell.setBorder(0);
						// igstRateValCell.setBorderWidthBottom(0);
						// igstRateValCell.setBorderWidthTop(0);
						// igstRateValCell.setBorderWidthRight(0);
						// igstRateValCell.addElement(igstRateVal);
						igstRateValCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateValCell
								.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateValCell);

						Phrase igstRate = new Phrase("-", font8);
						PdfPCell igstRateCell = new PdfPCell(igstRate);
						// igstRateCell.setBorder(0);
						// igstRateCell.setBorderWidthBottom(0);
						// igstRateCell.setBorderWidthTop(0);
						// igstRateCell.setBorderWidthLeft(0);
						// igstRateCell.addElement(igstRate);
						igstRateCell
								.setHorizontalAlignment(Element.ALIGN_CENTER);
						igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
						productTable.addCell(igstRateCell);

						double indivTotalAmount = asstotalAmount
								+ ctaxValue + staxValue;
						totalAmount = totalAmount + indivTotalAmount;
						Phrase totalPhrase = new Phrase(
								df.format(indivTotalAmount) + "", font8);
						PdfPCell totalCell = new PdfPCell(totalPhrase);

						// totalCell.setBorder(0);
						// totalCell.setBorderWidthBottom(0);
						// totalCell.setBorderWidthTop(0);
						totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

						productTable.addCell(totalCell);

						String premisesVal = "";
						premisesVal = quotEntity.getItems().get(i)
								.getPremisesDetails();
						if (printPremiseDetails) {
							Phrase premisesValPhrs = new Phrase(
									"Premise Details : " + premisesVal, font8);
							PdfPCell premiseCell = new PdfPCell();
							premiseCell.setColspan(16);
							premiseCell.addElement(premisesValPhrs);
							productTable.addCell(premiseCell);
						}
					}

				}
				}else{

					logger.log(Level.SEVERE,"Inside Tax Not Applicable");

					PdfPCell cell = new PdfPCell(new Phrase("-", font8));
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					productTable.addCell(cell);
					Phrase totalPhrase = new Phrase(df.format(getAssessTotalAmount(quotEntity.getItems().get(i)))
							+ "", font8);
					PdfPCell totalCell = new PdfPCell();
					// totalCell.setColspan(16);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.addElement(totalPhrase);
					productTable.addCell(totalCell);

					String premisesVal = "";
					if(quotEntity.getItems().get(i).getPremisesDetails()!=null){
						premisesVal=quotEntity.getItems().get(i).getPremisesDetails();
					}
					
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase("Premise Details : "
								+ premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
				}
			}

			PdfPCell productTableCell = new PdfPCell();
			;
			if (quotEntity.getItems().size() > firstBreakPoint) {
				Phrase my = new Phrase(
						"Please Refer Annexure For More Details", font8);
				productTableCell.addElement(my);

			} else {
				Phrase my = new Phrase("", font8);
				productTableCell.addElement(my);
			}

			productTableCell.setBorder(0);

			PdfPTable tab = new PdfPTable(1);
			tab.setWidthPercentage(100);
			tab.addCell(productTableCell);
			// tab.addCell(premiseTblCell);
			tab.setSpacingAfter(blankLines);

			// last code for both table to be added in one table

			PdfPCell tab1 = new PdfPCell(productTable);
			// tab1.setBorder(0);

			PdfPCell tab2 = new PdfPCell(tab);
			// tab2.setBorder(0);

			PdfPTable mainTable = new PdfPTable(1);
			mainTable.setWidthPercentage(100);
			mainTable.addCell(tab1);
			mainTable.addCell(tab2);
			try {
				document.add(mainTable);
				// document.add(productTableWithOutPremise);

			} catch (DocumentException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "ERROR Adding in Table!!!!" + e);
			}
			// }catch(Exception e){
			// e.printStackTrace();
			// logger.log(Level.SEVERE,"ERROR Adding in Table!!!!"+e);
			// }

			// End Here

		}

	}

	private boolean validateTaxes(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		if(salesLineItem.getVatTax()
				.getPercentage() != 0){
			return true;
		}else{
			if(salesLineItem.getServiceTax().getPercentage()!=0){
				return true;
			}else{
				return false;
			}
		}
	}

	private double getAssessTotalAmount(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		double totalAmount=0;
		if(salesLineItem.getArea().trim().equalsIgnoreCase("NA")||salesLineItem.getArea().equalsIgnoreCase("")){
			if(salesLineItem.getPercentageDiscount()==null||salesLineItem.getPercentageDiscount()==0){
				if(salesLineItem.getDiscountAmt()==0){
					totalAmount=salesLineItem.getPrice();
				}else{
					totalAmount=salesLineItem.getPrice()-salesLineItem.getDiscountAmt();
				}
			}else{
				double disPercentAmount=getPercentAmount(salesLineItem,false);
				if(salesLineItem.getDiscountAmt()==0){
					totalAmount=salesLineItem.getPrice()-disPercentAmount;
				}else{
					totalAmount=salesLineItem.getPrice()-disPercentAmount-salesLineItem.getDiscountAmt();
				}
			}
		}else{

			logger.log(Level.SEVERE,"Area Present");
			if(salesLineItem.getPercentageDiscount()==null&&salesLineItem.getPercentageDiscount()==0){
				logger.log(Level.SEVERE,"ZERO Area Present --PercentageDiscount");
				if(salesLineItem.getDiscountAmt()==0){
					logger.log(Level.SEVERE,"ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount=salesLineItem.getPrice()*Double.parseDouble(salesLineItem.getArea().trim());
				}else{
					logger.log(Level.SEVERE,"NON ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount=(salesLineItem.getPrice()*Double.parseDouble(salesLineItem.getArea().trim()))-salesLineItem.getDiscountAmt();
				}
			}else{
				logger.log(Level.SEVERE,"NON ZERO Area Present --PercentageDiscount");
				double disPercentAmount=getPercentAmount(salesLineItem,true);
				if(salesLineItem.getDiscountAmt()==0){
					logger.log(Level.SEVERE,"ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount=(salesLineItem.getPrice()*Double.parseDouble(salesLineItem.getArea().trim()))-disPercentAmount;
				}else{
					logger.log(Level.SEVERE,"NON ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount=(salesLineItem.getPrice()*Double.parseDouble(salesLineItem.getArea().trim()))-disPercentAmount-salesLineItem.getDiscountAmt();
				}
			}
		
		}
		logger.log(Level.SEVERE,"Assesable Value::::"+totalAmount);
		return totalAmount;
	}

	private double getPercentAmount(SalesLineItem salesLineItem, boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount=0;
		if(isAreaPresent){
			percentAmount=((salesLineItem.getPrice()*Double.parseDouble(salesLineItem.getArea().trim())*salesLineItem.getPercentageDiscount())/100);
		}else{
			percentAmount=((salesLineItem.getPrice()*salesLineItem.getPercentageDiscount())/100);
		}
		return percentAmount;
	}

	private void createCompanyAddressDetails() {

		// ************************changes made by rohan for M/s
		// ****************

		// *******************changes ends here
		// ********************sss*************

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true) {

				tosir = "To, M/S";
			} else {
				tosir = "To,";
			}

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname.add(custName);
		} else {
			fullname.add(tosir + "   " + custName);
		}
		fullname.setFont(font6bold);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		String addresslin1 = " ";
		if (!cust.getAdress().getAddrLine1().equals("")) {
			addresslin1 = cust.getAdress().getAddrLine1().trim() + " , ";
		}

		String addressline2 = " ";

		if (!cust.getAdress().getAddrLine2().equals("")) {
			addressline2 = cust.getAdress().getAddrLine2().trim() + " , ";
		}

		String landmark = " ";
		if (!cust.getAdress().getLandmark().equals("")) {
			landmark = cust.getAdress().getLandmark() + " , ";
		}

		String locality = " ";
		if (!cust.getAdress().getLocality().equals("")) {
			locality = cust.getAdress().getLocality() + " , ";
		}

		// Phrase addressline1=new
		// Phrase(addresslin1+addressline2+landmark+locality,font9);
		// PdfPCell fromaddrescell1=new PdfPCell();
		// fromaddrescell1.addElement(addressline1);
		// fromaddrescell1.setBorder(0);

		String city = " ";
		if (!cust.getAdress().getCity().equals("")) {
			city = cust.getAdress().getCity() + ",";
		}
		String state = " ";
		if (!cust.getAdress().getState().equals("")) {
			state = cust.getAdress().getState() + "-";
		}
		String pin = " ";
		if (cust.getAdress().getPin() != 0) {
			pin = cust.getAdress().getPin() + ",";
		}
		String country = " ";
		if (cust.getAdress().getCountry() != null) {
			country = cust.getAdress().getCountry();
		}

		Phrase addressline1 = new Phrase(addresslin1 + addressline2 + landmark
				+ locality + city + state + pin + country, font9);
		PdfPCell fromaddrescell1 = new PdfPCell();
		fromaddrescell1.addElement(addressline1);
		fromaddrescell1.setBorder(0);

		// Phrase blank=new Phrase(" ",font9);
		// PdfPCell blankCell=new PdfPCell();
		// fromaddrescell1.addElement(addressline1);
		// fromaddrescell1.setBorder(0);

		Phrase namePhrase = new Phrase("Customer Details", font6bold);
		PdfPCell headcell = new PdfPCell(namePhrase);
		headcell.setBorder(0);
		headcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// *******************rohan added cust details *****************
		Phrase email = new Phrase("Email :" + cust.getEmail(), font9);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase cellNo = null;
		if (cust.getLandline() != null) {
			cellNo = new Phrase("Cell : " + cust.getCellNumber1()
					+ "   Phone: " + cust.getLandline(), font9);
		} else {
			cellNo = new Phrase("Cell : " + cust.getCellNumber1(), font9);
		}

		PdfPCell cellNoCell = new PdfPCell(cellNo);
		cellNoCell.setBorder(0);
		cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ***********************changes ends here *********************

		PdfPTable addtable = new PdfPTable(1);
		addtable.setWidthPercentage(100f);
		addtable.addCell(headcell);
		addtable.addCell(custnamecell);
		addtable.addCell(fromaddrescell1);
		addtable.addCell(emailCell);
		addtable.addCell(cellNoCell);

		PdfPCell tablecell = new PdfPCell();
		tablecell.addElement(addtable);

		PdfPTable parentaddtable = new PdfPTable(1);
		parentaddtable.setWidthPercentage(100f);
		parentaddtable.addCell(tablecell);

		try {
			document.add(parentaddtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,725f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,40f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	private void createBlankforUPC() {

		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof Contract) {
			Contract conEntity = (Contract) qp;
			conEnt = conEntity.getContractDate();
		} else {
			Quotation quotEnt = (Quotation) qp;
			conEnt = quotEnt.getCreationDate();
		}

		String countinfo = "ID : " + qp.getCount() + "";

		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.setBorderWidthBottom(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14bold);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.setBorderWidthBottom(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14bold);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.setBorderWidthBottom(0);
		creationcell.addElement(Chunk.NEWLINE);

		// *********rohan make changes here for contract start and end date
		// ***********8
		PdfPCell conStartAndEndDateCell = null;
		PdfPCell blankCell = null;
		PdfPCell blankCell1 = null;

		if (qp instanceof Contract) {

			Date conStDt = null;
			Date conEndDt = null;

			Contract conEntity = (Contract) qp;
			conStDt = conEntity.getStartDate();
			conEndDt = conEntity.getEndDate();

			Phrase blank = new Phrase(" ", font8);
			blankCell = new PdfPCell(blank);
			blankCell.setBorderWidthTop(0);
			blankCell.setBorderWidthRight(0);

			Phrase blank1 = new Phrase(" ", font8);
			blankCell1 = new PdfPCell(blank1);
			blankCell1.setBorderWidthTop(0);
			blankCell1.setBorderWidthLeft(0);
			blankCell1.setBorderWidthRight(0);
			Phrase conStartAndEndDate = new Phrase("Start Date :"
					+ fmt.format(conStDt) + "   End Date :"
					+ fmt.format(conEndDt), font12bold);
			conStartAndEndDateCell = new PdfPCell(conStartAndEndDate);
			conStartAndEndDateCell.setBorderWidthLeft(0);
			conStartAndEndDateCell.setBorderWidthTop(0);
			conStartAndEndDateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		}
		// **********************************ends here
		// *******************************

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);

		try {
			titlepdftable.setWidths(columnWidthsForSTandEndDate);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		// *********rohan make changes here for contract start and end date
		// ***********8
		if (qp instanceof Contract) {
			titlepdftable.addCell(blankCell);
			titlepdftable.addCell(blankCell1);
			titlepdftable.addCell(conStartAndEndDateCell);
		}
		// **********************************ends here
		// *******************************
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		//
		// PdfPTable parent=new PdfPTable(1);
		// parent.setWidthPercentage(100);
		//
		// PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		// parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	// patch patch patch
	public void createPdfForEmail(Company comp, Customer cust, Sales qp) {
		this.comp = comp;
		this.qp = qp;
		this.cust = cust;
		this.products = qp.getItems();
		this.payTermsLis = qp.getPaymentTermsList();
		this.prodTaxes = qp.getProductTaxes();
		this.prodCharges = qp.getProductCharges();
		this.articletype = cust.getArticleTypeDetails();

		if (qp instanceof Contract) {
			Contract contractEntity = (Contract) qp;
			contEntity = contractEntity;
			//
			if (qp.getCompanyId() != null) {
				processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", qp.getCompanyId())
						.filter("processName", "Contract")
						.filter("configStatus", true).first().now();
				if (processConfig != null) {
					for (int k = 0; k < processConfig.getProcessList().size(); k++) {
						if (processConfig.getProcessList().get(k)
								.getProcessType().trim()
								.equalsIgnoreCase("CompanyAsLetterHead")
								&& processConfig.getProcessList().get(k)
										.isStatus() == true) {
							upcflag = true;

						}
					}
				}
			}
			//
			// }
			// else{
			// Quotation quotationEntity=(Quotation)qp;
			// quotEntity=quotationEntity;
			// }

			// *******************rohan chnages here for sending logo to email

			if (upcflag == false) {
				createLogo(document, comp);
				createCompanyHedding();
			} else {

				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}

				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				createBlankforUPC();
			}

		} else {

			Quotation quotationEntity = (Quotation) qp;
			quotEntity = quotationEntity;

			if (qp.getCompanyId() != null) {
				processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", qp.getCompanyId())
						.filter("processName", "Contract")
						.filter("configStatus", true).first().now();
				if (processConfig != null) {
					for (int k = 0; k < processConfig.getProcessList().size(); k++) {
						if (processConfig.getProcessList().get(k)
								.getProcessType().trim()
								.equalsIgnoreCase("CompanyAsLetterHead")
								&& processConfig.getProcessList().get(k)
										.isStatus() == true) {
							upcflag = true;

						}
					}
				}
			}

			if (upcflag == false) {
				createLogo(document, comp);
				createCompanyHedding();

			} else {

				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}

				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				createBlankforUPC();
			}
		}
		// ********************chnges ends here ************************

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		// makeServices();
		// createLogo(document,comp);
		// createCompanyHedding();
		createProductDetail();
		termsConditionsInfo();
		footerInfo();
	}

	private void createLogo(Document doc, Company comp) {
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

	}

	public void createCompanyHedding() {
		Phrase companyName = null;
		if (rexFlag == true) {
			companyName = new Phrase("                  "
					+ comp.getBusinessUnitName(), font11bold);
		} else {
			companyName = new Phrase("                  "
					+ comp.getBusinessUnitName(), font12boldul);
		}
		Paragraph p = new Paragraph();
		p.add(Chunk.NEWLINE);
		p.add(companyName);

		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(p);

		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(), font7);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font7);
		}

		// **************************************************8

		String complandmark1 = "";
		Phrase landmar1 = null;

		String localit1 = "";
		Phrase complocality1 = null;

		if (comp.getAddress().getLandmark() != null) {
			complandmark1 = comp.getAddress().getLandmark();
			landmar1 = new Phrase(complandmark1, font7);
		}

		if (comp.getAddress().getLocality() != null) {
			localit1 = comp.getAddress().getLocality();
			complocality1 = new Phrase(localit1, font7);
		}

		Phrase cityState = new Phrase(comp.getAddress().getCity() + " - "
				+ comp.getAddress().getPin(), font7);

		Phrase branch = null;
		if (qp instanceof Contract) {
			if (contEntity.getBranch() != null
					&& !contEntity.getBranch().trim().equals("")) {

				branch = new Phrase("Branch : " + contEntity.getBranch(), font7);
			} else {
				branch = new Phrase(" ", font7);
			}
		} else {
			if (quotEntity.getBranch() != null
					&& !quotEntity.getBranch().trim().equals("")) {

				branch = new Phrase("Branch : " + quotEntity.getBranch(), font7);
			} else {
				branch = new Phrase(" ", font7);
			}
		}

		PdfPCell branchcell = new PdfPCell();
		branchcell.addElement(branch);
		branchcell.setBorder(0);

		PdfPCell custlandcell = new PdfPCell();
		custlandcell.addElement(landmar1);
		custlandcell.setBorder(0);

		PdfPCell custlocalitycell = new PdfPCell();
		custlocalitycell.addElement(complocality1);
		custlocalitycell.setBorder(0);

		PdfPCell custcitycell = new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0);

		// *********************************************88***8*
		// Phrase landmark=null;
		// Phrase locality=null;
		// if(comp.getAddress().getLandmark()!=null&&comp.getAddress().getLocality().equals("")==false)
		// {
		//
		// String landmarks=comp.getAddress().getLandmark()+" , ";
		// locality= new
		// Phrase(landmarks+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" - "
		// +comp.getAddress().getPin(),font12);
		// }
		//
		// else
		// if(comp.getAddress().getLandmark()!=null&&comp.getAddress().getLocality().equals("")==true)
		// {
		// String landmarks=comp.getAddress().getLandmark()+" , ";
		// locality= new Phrase(landmarks+comp.getAddress().getCity()+" - "
		// +comp.getAddress().getPin(),font12);
		// }
		// else
		// if(comp.getAddress().getLandmark()==null&&comp.getAddress().getLocality().equals("")==false)
		// {
		//
		// locality= new
		// Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" - "
		// +comp.getAddress().getPin(),font12);
		// }
		// else
		// {
		// locality= new
		// Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" - "
		// +comp.getAddress().getPin(),font12);
		// }
		//
		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);
		if (adressline2 != null) {
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
		}

		// PdfPCell localitycell=new PdfPCell();
		// localitycell.addElement(locality);
		// localitycell.setBorder(0);
		// localitycell.addElement(Chunk.NEWLINE);

		String contactinfo = "Mobile: " + comp.getContact().get(0).getCellNo1();
		if (comp.getContact().get(0).getCellNo2() != 0)
			contactinfo = contactinfo + ","
					+ comp.getContact().get(0).getCellNo2();
		if (comp.getContact().get(0).getLandline() != 0) {
			contactinfo = contactinfo + "     " + " Phone: 0"
					+ comp.getContact().get(0).getLandline();
		}

		Phrase contactnos = new Phrase(contactinfo, font9);
		Phrase email = new Phrase("Email: "
				+ comp.getContact().get(0).getEmail(), font9);

		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);

		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(addressline1cell);
		if (!comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if (!comp.getAddress().getLandmark().equals("")) {
			companytable.addCell(custlandcell);
		}
		if (!comp.getAddress().getLocality().equals("")) {
			companytable.addCell(custlocalitycell);
		}
		companytable.addCell(custcitycell);
		companytable.addCell(branchcell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);

		// ***************************************************************

		Phrase blankPhase = new Phrase("", font9);
		PdfPCell blankcell = new PdfPCell(blankPhase);
		blankcell.setBorder(0);

		if (comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(blankcell);
		}
		if (comp.getAddress().getLandmark().equals("")) {
			companytable.addCell(blankcell);
		}
		if (comp.getAddress().getLocality().equals("")) {
			companytable.addCell(blankcell);
		}

		// ********************************************************************
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);

		if (comp.getContact() != null && comp.getCompanyId() != null) {
			Phrase realcontact = new Phrase("Contact Person: "
					+ comp.getPocName(), font9);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			companytable.addCell(realcontactcell);
		}

		/**
		 * Customer Info
		 */
		// String tosir="To, M/S";

		// ************************changes made by rohan for M/s
		// ****************
		String tosir = null;
		if (cust.isCompany() == true) {

			tosir = "To, M/S";
		} else {
			tosir = "To,";
		}
		// *******************changes ends here
		// ********************sss*************

		String custName = "";

		// rohan modified this code for printabel name
		// date : 8/11/2016

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {
			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = cust.getCompanyName().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		// Phrase tosirphrase= new Phrase(tosir,font12bold);
		//
		// Phrase customername= new Phrase(custName,font12boldul);
		Paragraph fullname = new Paragraph();
		fullname.add(Chunk.NEWLINE);
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname.add(custName);
		} else {
			fullname.add(tosir + "   " + custName);
		}
		fullname.setFont(font12bold);

		PdfPCell custnamecell = new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);

		Phrase customeradress = new Phrase("                "
				+ cust.getAdress().getAddrLine1(), font7);
		Phrase customeradress2 = null;
		if (cust.getAdress().getAddrLine2() != null) {
			customeradress2 = new Phrase("                "
					+ cust.getAdress().getAddrLine2(), font7);
		}

		PdfPCell custaddress1 = new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);

		PdfPCell custaddress2 = new PdfPCell();
		if (cust.getAdress().getAddrLine2() != null) {
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
		}

		String custlandmark2 = "";
		Phrase landmar2 = null;

		String localit2 = "";
		Phrase custlocality2 = null;

		if (cust.getAdress().getLandmark() != null) {
			custlandmark2 = cust.getAdress().getLandmark();
			landmar2 = new Phrase("                " + custlandmark2, font7);
		}

		if (cust.getAdress().getLocality() != null) {
			localit2 = cust.getAdress().getLocality();
			custlocality2 = new Phrase("                " + localit2, font7);
		}

		Phrase cityState2 = new Phrase("                "
				+ cust.getAdress().getCity() + " - "
				+ cust.getAdress().getPin()
		// +" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
				, font7);

		PdfPCell custlandcell2 = new PdfPCell();
		custlandcell2.addElement(landmar2);
		custlandcell2.setBorder(0);

		PdfPCell custlocalitycell2 = new PdfPCell();
		custlocalitycell2.addElement(custlocality2);
		custlocalitycell2.setBorder(0);

		PdfPCell custcitycell2 = new PdfPCell();
		custcitycell2.addElement(cityState2);
		custcitycell2.setBorder(0);
		// custcitycell2.addElement(Chunk.NEWLINE);

		// rohan modified and added cust landline to customer details
		Phrase custcontact = null;
		String mobileDetails = "";
		if (cust.getLandline() != null) {
			mobileDetails = "Mobile: " + cust.getCellNumber1() + "   Phone: "
					+ cust.getLandline();
		} else {
			mobileDetails = "Mobile: " + cust.getCellNumber1();
		}

		custcontact = new Phrase(mobileDetails, font9);

		PdfPCell custcontactcell = new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);

		Phrase custemail = new Phrase("email: " + cust.getEmail(), font9);
		PdfPCell custemailcell = new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);

		int noserv = (int) totalServices();

		PdfPCell infotablecell = null;
		PdfPCell infotablecell1 = null;
		if (qp instanceof Contract) {

			Phrase startdt = new Phrase("Start Date", font9);
			PdfPCell startdtcell = new PdfPCell();
			startdtcell.addElement(startdt);
			startdtcell.setBorder(0);
			startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase startdtValue = null;
			if (qp instanceof Contract) {
				Contract contractEntityy = (Contract) qp;
				if (contractEntityy.getStartDate() != null) {
					startdtValue = new Phrase(fmt.format(contractEntityy
							.getStartDate()), font9);
				}
			} else {
				startdtValue = new Phrase("", font9);
			}

			PdfPCell startdtValuecell = new PdfPCell();
			startdtValuecell.addElement(startdtValue);
			startdtValuecell.setBorder(0);
			startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase enddt = new Phrase("End Date", font9);
			PdfPCell enddtcell = new PdfPCell();
			enddtcell.addElement(enddt);
			enddtcell.setBorder(0);
			enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase enddtValue = null;
			if (qp instanceof Contract) {
				Contract contractEntity = (Contract) qp;
				if (contractEntity.getEndDate() != null) {

					enddtValue = new Phrase(fmt.format(contractEntity
							.getEndDate()), font9);
				}
			} else {
				enddtValue = new Phrase("", font9);
			}

			PdfPCell enddtValuecell = new PdfPCell();
			enddtValuecell.addElement(enddtValue);
			enddtValuecell.setBorder(0);
			enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refNo = new Phrase("Ref No", font9);
			PdfPCell refNocell = new PdfPCell();
			refNocell.addElement(refNo);
			refNocell.setBorder(0);
			refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refNovalueValue = null;
			if (qp instanceof Contract) {
				Contract contractEntity = (Contract) qp;
				if (contractEntity.getRefNo() != null) {
					refNovalueValue = new Phrase(
							contractEntity.getRefNo() + "", font9);
				}
			} else {
				refNovalueValue = new Phrase("", font9);
			}

			PdfPCell refNovalueValuecell = new PdfPCell();
			refNovalueValuecell.addElement(refNovalueValue);
			refNovalueValuecell.setBorder(0);
			refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refDt = new Phrase("Ref Date", font9);
			PdfPCell refDtcell = new PdfPCell();
			refDtcell.addElement(refDt);
			refDtcell.setBorder(0);
			refDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase refDtValue = null;
			if (qp instanceof Contract) {
				Contract contractEntity = (Contract) qp;
				if (contractEntity.getRefDate() != null) {
					refDtValue = new Phrase(fmt.format(contractEntity
							.getRefDate()), font9);
				}
			} else {
				refDtValue = new Phrase("", font9);
			}

			PdfPCell refDtValuecell = new PdfPCell();
			refDtValuecell.addElement(refDtValue);
			refDtValuecell.setBorder(0);
			refDtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase seperator = new Phrase(":", font9);
			PdfPCell seperatorcell = new PdfPCell();
			seperatorcell.addElement(seperator);
			seperatorcell.setBorder(0);
			seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			Phrase invoicegroupphrase1 = null;
			Phrase invoicegroupphrase3 = null;

			if (!contEntity.getGroup().equals("")) {
				invoicegroupphrase1 = new Phrase("Group", font8);
				invoicegroupphrase3 = new Phrase(contEntity.getGroup(), font8);
			} else {
				invoicegroupphrase1 = new Phrase("", font8);
				invoicegroupphrase3 = new Phrase("", font8);
			}

			PdfPCell invoicegroupcell1 = new PdfPCell();
			invoicegroupcell1.setBorder(0);
			invoicegroupcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			invoicegroupcell1.addElement(invoicegroupphrase1);
			PdfPCell invoicegroupcell3 = new PdfPCell();
			invoicegroupcell3.setBorder(0);
			invoicegroupcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			invoicegroupcell3.addElement(invoicegroupphrase3);

			PdfPTable infotable = new PdfPTable(6);
			infotable.setHorizontalAlignment(100);

			if (contEntity.getStartDate() != null) {
				infotable.addCell(startdtcell);
			}
			if (contEntity.getStartDate() != null) {
				infotable.addCell(seperatorcell);
			}
			if (contEntity.getStartDate() != null) {
				infotable.addCell(startdtValuecell);
			}

			if (contEntity.getEndDate() != null) {
				infotable.addCell(enddtcell);
			}
			if (contEntity.getEndDate() != null) {
				infotable.addCell(seperatorcell);
			}
			if (contEntity.getEndDate() != null) {
				infotable.addCell(enddtValuecell);
			}

			if (!contEntity.getRefNo().equals("")) {
				infotable.addCell(refNocell);
				infotable.addCell(seperatorcell);
				infotable.addCell(refNovalueValuecell);
			}

			if (contEntity.getRefDate() != null) {
				infotable.addCell(refDtcell);
			}
			if (contEntity.getRefDate() != null) {
				infotable.addCell(seperatorcell);
			}
			if (contEntity.getRefDate() != null) {
				infotable.addCell(refDtValuecell);
			}
			if (!contEntity.getGroup().equals("")) {

				infotable.addCell(invoicegroupcell1);
				infotable.addCell(seperatorcell);
				infotable.addCell(invoicegroupcell3);

				PdfPCell blankcell1 = new PdfPCell();
				blankcell1.setBorder(0);
				infotable.addCell(blankcell1);
				infotable.addCell(blankcell1);
				infotable.addCell(blankcell1);
			}

			try {
				infotable.setWidths(columnWidths2);
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}

			infotablecell = new PdfPCell(infotable);
			infotablecell.setBorder(0);

		}

		else {

			if (qp instanceof Quotation) {
				Quotation quot = (Quotation) qp;

				Phrase startdt = new Phrase("Start Date", font8);
				PdfPCell startdtcell = new PdfPCell();
				startdtcell.addElement(startdt);
				startdtcell.setBorder(0);
				startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase startdtValue = null;
				if (quot.getStartDate() != null) {
					startdtValue = new Phrase(fmt.format(quot.getStartDate()),
							font8);

				} else {
					startdtValue = new Phrase("", font8);
				}

				PdfPCell startdtValuecell = new PdfPCell();
				startdtValuecell.addElement(startdtValue);
				startdtValuecell.setBorder(0);
				startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase enddt = new Phrase("End Date", font8);
				PdfPCell enddtcell = new PdfPCell();
				enddtcell.addElement(enddt);
				enddtcell.setBorder(0);
				enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase enddtValue = null;
				if (quot.getEndDate() != null) {

					enddtValue = new Phrase(fmt.format(quot.getEndDate()),
							font8);
				} else {
					enddtValue = new Phrase("", font8);
				}

				PdfPCell enddtValuecell = new PdfPCell();
				enddtValuecell.addElement(enddtValue);
				enddtValuecell.setBorder(0);
				enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase seperator = new Phrase(":", font8);
				PdfPCell seperatorcell = new PdfPCell();
				seperatorcell.addElement(seperator);
				seperatorcell.setBorder(0);
				seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);

				Phrase invoicegroupphrase1 = null;
				Phrase invoicegroupphrase3 = null;

				if (!quotEntity.getGroup().equals("")) {
					invoicegroupphrase1 = new Phrase("Group", font8);
					invoicegroupphrase3 = new Phrase(quotEntity.getGroup(),
							font8);
				} else {
					invoicegroupphrase1 = new Phrase("", font8);
					invoicegroupphrase3 = new Phrase("", font8);
				}

				PdfPCell invoicegroupcell1 = new PdfPCell();
				invoicegroupcell1.setBorder(0);
				invoicegroupcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				invoicegroupcell1.addElement(invoicegroupphrase1);
				PdfPCell invoicegroupcell3 = new PdfPCell();
				invoicegroupcell3.setBorder(0);
				invoicegroupcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
				invoicegroupcell3.addElement(invoicegroupphrase3);

				PdfPTable infotable1 = null;
				infotable1 = new PdfPTable(6);
				infotable1.setHorizontalAlignment(100);

				if (quot.getEndDate() != null) {
					infotable1.addCell(startdtcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(seperatorcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(startdtValuecell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(enddtcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(seperatorcell);
				}
				if (quot.getEndDate() != null) {
					infotable1.addCell(enddtValuecell);
				}

				if (!quotEntity.getGroup().equals("")) {

					infotable1.addCell(invoicegroupcell1);
					infotable1.addCell(seperatorcell);
					infotable1.addCell(invoicegroupcell3);

					PdfPCell blankcell1 = new PdfPCell();
					blankcell1.setBorder(0);
					infotable1.addCell(blankcell1);
					infotable1.addCell(blankcell1);
					infotable1.addCell(blankcell1);
				}
				try {
					infotable1.setWidths(columnWidths2);
				} catch (DocumentException e2) {
					e2.printStackTrace();
				}

				infotablecell1 = new PdfPCell(infotable1);
				infotablecell1.setBorder(0);

			}

		}

		PdfPTable custtable = new PdfPTable(1);
		custtable.setWidthPercentage(100);
		custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
		custtable.addCell(custnamecell);
		custtable.addCell(custaddress1);
		if (!cust.getAdress().getAddrLine2().equals("")) {
			custtable.addCell(custaddress2);
		}
		if (!cust.getAdress().getLandmark().equals("")) {
			custtable.addCell(custlandcell2);
		}
		if (!cust.getAdress().getLocality().equals("")) {

			custtable.addCell(custlocalitycell2);
		}
		custtable.addCell(custcitycell2);
		custtable.addCell(custcontactcell);
		custtable.addCell(custemailcell);

		if (qp instanceof Contract) {
			custtable.addCell(infotablecell);
		} else {
			if (qp instanceof Quotation) {

				custtable.addCell(infotablecell1);
			}
		}

		// if company is true then the name of contact person

		if (cust.isCompany() == true && cust.getCompanyName() != null) {
			Phrase realcontact = new Phrase("Attn: " + cust.getFullname(),
					font7);
			realcontact.add(Chunk.NEWLINE);
			PdfPCell realcontactcell = new PdfPCell();
			realcontactcell.addElement(realcontact);
			realcontactcell.setBorder(0);
			custtable.addCell(realcontactcell);
		}

		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();

		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);

		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Date conEnt = null;
		if (qp instanceof Contract) {
			Contract conEntity = (Contract) qp;
			conEnt = conEntity.getContractDate();
		} else {
			Quotation quotEnt = (Quotation) qp;
			conEnt = quotEnt.getCreationDate();
		}

		String countinfo = "ID : " + qp.getCount() + "";

		String creationdateinfo = "Date: " + fmt.format(conEnt);

		Phrase titlephrase = new Phrase(title, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		Phrase idphrase = new Phrase(countinfo, font14bold);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);

		Phrase dateofpdf = new Phrase(creationdateinfo, font14bold);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);

		try {
			document.add(headparenttable);
			document.add(titlepdftable);

		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createCompanyAdress() {
		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),
				font12);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font12);
		}

		Phrase landmark = null;
		Phrase locality = null;
		if (comp.getAddress().getLandmark() != null
				&& comp.getAddress().getLocality().equals("") == false) {

			String landmarks = comp.getAddress().getLandmark() + " , ";
			locality = new Phrase(landmarks + comp.getAddress().getLocality()
					+ " , " + comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		} else {
			locality = new Phrase(comp.getAddress().getLocality() + " , "
					+ comp.getAddress().getCity() + " , "
					+ comp.getAddress().getPin(), font12);
		}
		Paragraph adressPragraph = new Paragraph();
		adressPragraph.add(adressline1);
		adressPragraph.add(Chunk.NEWLINE);
		if (adressline2 != null) {
			adressPragraph.add(adressline2);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(locality);
		adressPragraph.setAlignment(Element.ALIGN_CENTER);
		adressPragraph.setSpacingBefore(10);
		adressPragraph.setSpacingAfter(10);

		// Phrase for phone,landline ,fax and email

		Phrase titlecell = new Phrase("Mobile :", font8bold);
		Phrase titleTele = new Phrase("Phone :", font8bold);
		Phrase titleemail = new Phrase("Email :", font8bold);
		Phrase titlefax = new Phrase("Fax :", font8bold);

		Phrase titleservicetax = new Phrase("Service Tax No: ", font8bold);
		Phrase titlevatatx = new Phrase(" Vat Tax No: ", font8bold);
		String servicetax = comp.getServiceTaxNo();
		String vatttax = comp.getVatTaxNo();
		Phrase servicetaxphrase = null;
		Phrase vattaxphrase = null;

		if (servicetax != null && servicetax.trim().equals("") == false) {
			servicetaxphrase = new Phrase(servicetax, font8);
		}

		if (vatttax != null && vatttax.trim().equals("") == false) {
			vattaxphrase = new Phrase(vatttax, font8);
		}

		// cell number logic
		String stringcell1 = comp.getContact().get(0).getCellNo1() + "";
		String stringcell2 = null;
		Phrase mob = null;
		if (comp.getContact().get(0).getCellNo2() != -1)
			stringcell2 = comp.getContact().get(0).getCellNo2() + "";
		if (stringcell2 != null)
			mob = new Phrase(stringcell1 + " / " + stringcell2, font8);
		else
			mob = new Phrase(stringcell1, font8);

		// LANDLINE LOGIC
		Phrase landline = null;
		if (comp.getContact().get(0).getLandline() != -1)
			landline = new Phrase(comp.getContact().get(0).getLandline() + "",
					font8);

		// fax logic
		Phrase fax = null;
		if (comp.getContact().get(0).getFaxNo() != null)
			fax = new Phrase(comp.getContact().get(0).getFaxNo() + "", font8);
		// email logic
		Phrase email = new Phrase(comp.getContact().get(0).getEmail(), font8);

		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(titlecell);
		adressPragraph.add(mob);
		adressPragraph.add(new Chunk("         "));

		if (landline != null) {

			adressPragraph.add(titleTele);
			adressPragraph.add(landline);
			adressPragraph.add(new Chunk("         "));
		}

		if (fax != null) {

			adressPragraph.add(titlefax);
			adressPragraph.add(fax);
			adressPragraph.add(Chunk.NEWLINE);
		}

		adressPragraph.add(titleemail);
		adressPragraph.add(new Chunk(" "));
		adressPragraph.add(email);
		adressPragraph.add(Chunk.NEWLINE);

		if (servicetaxphrase != null) {
			adressPragraph.add(titleservicetax);
			adressPragraph.add(servicetaxphrase);

		}

		if (vattaxphrase != null) {
			adressPragraph.add(titlevatatx);
			adressPragraph.add(vattaxphrase);
			adressPragraph.add(new Chunk("         "));

		}

		adressPragraph.add(Chunk.NEWLINE);
		String title = "";
		if (qp instanceof Contract)
			title = "Contract";

		else
			title = "Quotation";

		Phrase Ptitle = new Phrase(title, font12bold);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Ptitle);
		adressPragraph.add(Chunk.NEWLINE);
		adressPragraph.add(Chunk.NEWLINE);

		try {
			document.add(adressPragraph);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createHeaderTable() throws DocumentException {
		Phrase nameTitle = new Phrase("Name :", font8bold);
		Phrase phoneTitle = new Phrase("Phone :", font8bold);
		Phrase adressTitle = new Phrase("Address :", font8bold);
		Phrase emailTitle = new Phrase("Email :", font8bold);

		PdfPCell titlecustnameCell = new PdfPCell();
		titlecustnameCell.addElement(nameTitle);
		titlecustnameCell.setBorder(0);
		PdfPCell titlecustPhonecell = new PdfPCell();
		titlecustPhonecell.addElement(phoneTitle);
		titlecustPhonecell.setBorder(0);
		PdfPCell titleadressCell = new PdfPCell();
		titleadressCell.addElement(adressTitle);
		titleadressCell.setBorder(0);
		PdfPCell titleemailCell = new PdfPCell();
		titleemailCell.addElement(emailTitle);
		titleemailCell.setBorder(0);

		String fullname = cust.getFullname().trim();

		Phrase customername = new Phrase(fullname, font8);
		Phrase phonechunk = new Phrase(cust.getCellNumber1() + "", font8);
		Phrase emailchunk = new Phrase(cust.getEmail().trim() + "", font8);

		Phrase blankphrase = new Phrase("  ");
		PdfPCell blankcell = new PdfPCell(blankphrase);

		Phrase customeradress1 = new Phrase(cust.getAdress().getAddrLine1(),
				font8);
		Phrase customeradress2 = null;
		// Patch
		if (cust.getAdress().getAddrLine2() != null)
			customeradress2 = new Phrase(cust.getAdress().getAddrLine2(), font8);

		Phrase locality = null;

		if (cust.getAdress().getLandmark() != null) {
			locality = new Phrase(cust.getAdress().getLandmark() + " "
					+ cust.getAdress().getLocality() + " "
					+ cust.getAdress().getCity() + " "
					+ cust.getAdress().getPin(), font8);
		} else {
			locality = new Phrase(cust.getAdress().getLocality() + " "
					+ cust.getAdress().getCity() + " "
					+ cust.getAdress().getPin(), font8);
		}

		Phrase blnkphrase = new Phrase(" ");
		PdfPCell blnkcell = new PdfPCell();
		blnkcell.addElement(blnkphrase);
		blnkcell.setBorder(0);
		PdfPCell custnameCell = new PdfPCell();
		custnameCell.addElement(customername);
		custnameCell.setBorder(0);
		PdfPCell custPhonecell = new PdfPCell();
		custPhonecell.addElement(phonechunk);
		custPhonecell.setBorder(0);
		PdfPCell adress1Cell = new PdfPCell();
		adress1Cell.addElement(customeradress1);
		adress1Cell.setBorder(0);
		PdfPCell adress2Cell = new PdfPCell();
		adress2Cell.addElement(customeradress2);
		adress2Cell.setBorder(0);
		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.addElement(emailchunk);
		emailcell.setBorder(0);

		String header;
		header = "ID :";

		Phrase billno = new Phrase(header, font8bold);
		Phrase orederdate = new Phrase("Date :", font8bold);
		Phrase duration = new Phrase("Duration :", font8bold);
		Phrase noofservices = new Phrase("Services:", font8bold);
		Phrase startDate = new Phrase("Start Date :", font8bold);
		Phrase endDate = new Phrase("End Date :", font8bold);
		Phrase refNo = new Phrase("Ref No :", font8bold);
		Phrase refDate = new Phrase("Ref Date :", font8bold);

		PdfPCell titlebillCell = new PdfPCell();
		titlebillCell.addElement(billno);
		PdfPCell titledatecell = new PdfPCell();
		titledatecell.addElement(orederdate);
		PdfPCell titledurationCell = new PdfPCell();
		titledurationCell.addElement(duration);
		PdfPCell titlenoServicesCell = new PdfPCell();
		titlenoServicesCell.addElement(noofservices);
		PdfPCell startdateCell = new PdfPCell();
		startdateCell.addElement(startDate);
		PdfPCell enddateCell = new PdfPCell();
		enddateCell.addElement(endDate);
		PdfPCell refNoCell = new PdfPCell();
		refNoCell.addElement(refNo);
		PdfPCell refDateCell = new PdfPCell();
		refDateCell.addElement(refDate);

		Phrase realqno = null;
		realqno = new Phrase(qp.getCount() + "", font8);

		String date = fmt.format(qp.getCreationDate());
		Phrase realorederdate = new Phrase(date + "", font8);
		realorederdate.trimToSize();
		Phrase relduration = new Phrase(largestduration(), font8);

		int noserv = (int) totalServices();

		Phrase relnoservices = new Phrase(noserv + "", font8);

		Quotation quot = (Quotation) qp;
		String start;
		if (quot.getStartDate() == null) {
			start = "N.A";
		} else {
			start = fmt.format(quot.getStartDate());
		}

		Phrase realstartdate = new Phrase(start, font8);

		if (quot.getEndDate() == null)
			start = "N.A";
		else
			start = fmt.format(quot.getEndDate());

		Phrase realenddate = new Phrase(start, font8);

		Phrase refno = null;
		if (qp instanceof Contract) {
			if (contEntity.getRefNo() != null) {

				refno = new Phrase(contEntity.getRefNo() + "", font8);
			}
		} else {
			refno = new Phrase("", font8);
		}
		Phrase refDt = null;
		if (qp instanceof Contract) {
			if (contEntity != null && contEntity.getRefDate() != null) {
				refDt = new Phrase(fmt.format(contEntity.getRefDate()), font8);
			}
		} else {
			refDt = new Phrase("", font8);
		}

		PdfPCell billCell = new PdfPCell();
		billCell.addElement(realqno);
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(realorederdate);
		PdfPCell durationCell = new PdfPCell();
		durationCell.addElement(relduration);
		PdfPCell noServicesCell = new PdfPCell();
		noServicesCell.addElement(relnoservices);
		PdfPCell realstartdateCell = new PdfPCell();
		realstartdateCell.addElement(realstartdate);
		PdfPCell realenddateCell = new PdfPCell();
		realenddateCell.addElement(realenddate);
		PdfPCell refno1cell = new PdfPCell();
		refno1cell.addElement(refno);
		PdfPCell refdt1cell = new PdfPCell();
		refdt1cell.addElement(refDt);

		PdfPTable table = new PdfPTable(2);

		// 1 st row name phone
		table.addCell(titlecustnameCell);
		table.addCell(custnameCell);

		table.addCell(titlecustPhonecell);
		table.addCell(custPhonecell);

		// 2nd row adress and email
		table.addCell(titleadressCell);
		table.addCell(adress1Cell);
		if (customeradress2 != null) {
			table.addCell(blnkcell);
			table.addCell(adress2Cell);
		}
		table.addCell(blnkcell);
		table.addCell(localitycell);
		table.addCell(titleemailCell);
		table.addCell(emailcell);

		table.setWidths(new float[] { 20, 80 });
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setWidthPercentage(100);

		PdfPTable billtable = new PdfPTable(4);
		billtable.setWidths(new float[] { 20, 30, 20, 30 });
		billtable.setWidthPercentage(100);
		billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

		// 1 row bill number and date
		billtable.addCell(titlebillCell);
		billtable.addCell(billCell);
		billtable.addCell(titledatecell);
		billtable.addCell(datecell);

		// 2 row duration and start date
		billtable.addCell(titledurationCell);
		billtable.addCell(durationCell);
		billtable.addCell(startdateCell);
		billtable.addCell(realstartdateCell);
		billtable.addCell(refno1cell);
		billtable.addCell(refdt1cell);

		// 3 row no serivces and end date
		billtable.addCell(titlenoServicesCell);
		billtable.addCell(noServicesCell);
		billtable.addCell(enddateCell);
		billtable.addCell(realenddateCell);
		billtable.addCell(refNoCell);
		billtable.addCell(refDateCell);

		int row = 3;
		// 4 row valid untill and blank
		if (qp instanceof Quotation) {
			PdfPCell titlevalid = new PdfPCell();
			PdfPCell realvaliduntil = new PdfPCell();

			Phrase valid = null;
			String validuntil = "";
			if (((Quotation) qp).getValidUntill() == null) {
				validuntil = "";
				valid = new Phrase("", font8bold);
			} else {
				System.out.println("................in side else condition");

				valid = new Phrase("Valid Until", font8bold);
				;
				validuntil = fmt.format(((Quotation) qp).getValidUntill());
			}
			Phrase rvalid = new Phrase(validuntil, font8);
			titlevalid.addElement(valid);
			realvaliduntil.addElement(rvalid);
			billtable.addCell(titlevalid);
			billtable.addCell(realvaliduntil);
			billtable.addCell(blankcell);
			billtable.addCell(blankcell);

			row = 4;
		}
		int noCol = table.getNumberOfColumns();
		// for(int i=0;i<2;i++)
		// {
		// PdfPRow temp=table.getRow(i);
		// PdfPCell[] cells= temp.getCells();
		// for(PdfPCell cell:cells)
		// cell.setBorder(Rectangle.NO_BORDER);
		// }
		//
		for (int i = 0; i < row; i++) {
			PdfPRow temp = billtable.getRow(i);
			PdfPCell[] cells = temp.getCells();
			for (PdfPCell cell : cells) {
				cell.setBorder(Rectangle.NO_BORDER);
			}
		}

		PdfPTable parenttable = new PdfPTable(2);
		parenttable.setWidthPercentage(100);
		parenttable.setWidths(new float[] { 50, 50 });

		PdfPCell custinfocell = new PdfPCell();
		PdfPCell billinfocell = new PdfPCell();

		custinfocell.addElement(table);
		billinfocell.addElement(billtable);

		parenttable.addCell(custinfocell);
		parenttable.addCell(billinfocell);

		document.add(parenttable);
	}

	// public void ceateComments()
	// {
	// Font font1 = new Font(Font.FontFamily.HELVETICA , 8,Font.BOLD);
	// Chunk commentChunk= new Chunk("Description :",font1);
	//
	// Chunk deschunk = null;
	// if(qp.getDescription()!=null)
	// deschunk= new Chunk(qp.getDescription(),font8);
	//
	// Paragraph para = new Paragraph();
	// para.add(Chunk.NEWLINE);
	// para.add(commentChunk);
	// para.add(Chunk.NEWLINE);
	// if(deschunk!=null)
	// para.add(deschunk);
	// para.add(Chunk.NEWLINE);
	// para.add(Chunk.NEWLINE);
	// try {
	// document.add(para);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// }

	private void createProductDetailWithDiscount() {

		/**
		 * rohan added this code for adding premises in the pdf
		 */
		PdfPTable premisesTable = new PdfPTable(1);
		premisesTable.setWidthPercentage(100f);
		/**
		 * ends here
		 */

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		PdfPTable table = new PdfPTable(12);
		table.setWidthPercentage(100);
		Phrase srno = new Phrase("SrNo", font1);
		// Phrase category = new Phrase("Prod ID",font1);
		Phrase product = new Phrase("Item Details", font1);
		Phrase qty = new Phrase("No of Branches", font1);
		Phrase area = new Phrase("AREA", font1);

		Phrase startdate = new Phrase("Start Date", font1);
		Phrase duration = new Phrase("Duration(d)", font1);
		Phrase noservices = new Phrase("Services", font1);
		Phrase rate = new Phrase("Rate", font1);

		Phrase servicetax = null;
		Phrase servicetax1 = null;
		int flag1 = 0;
		int vat = 0;
		int st = 0;
		for (int i = 0; i < products.size(); i++) {
			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() == 0)) {
				servicetax = new Phrase("VAT(%)", font1);
				System.out.println("Rohan1");
				vat = vat + 1;
				System.out.println("phrase value====" + servicetax.toString());
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {
				servicetax = new Phrase("ST(%)", font1);
				System.out.println("Rohan22222222222");
				st = st + 1;
			} else if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {
				servicetax1 = new Phrase("VAT / ST(%)", font1);
				System.out.println("Rohan3333333333333");
				flag1 = flag1 + 1;
				System.out.println("flag value;;;;;" + flag1);
			} else {

				servicetax = new Phrase("TAX(%)", font1);
				System.out.println("Rohan4444444444444");
			}
		}

		Phrase percdisctitle = new Phrase("% Disc", font1);

		Phrase discAmt = new Phrase("Disc Amt", font1);

		Phrase total = new Phrase("Total", font1);

		System.out.println("vat===" + vat);
		System.out.println("st====" + st);
		System.out.println("flag1===" + flag1);
		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		// PdfPCell cellcategory = new PdfPCell(category);
		// cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellarea = new PdfPCell(area);
		cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellstartdate = new PdfPCell(startdate);
		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicecell = new PdfPCell(noservices);
		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellservicetax1 = null;
		PdfPCell cellservicetax = null;
		PdfPCell cellservicetax2 = null;
		Phrase servicetax2 = null;

		if (flag1 > 0) {
			cellservicetax1 = new PdfPCell(servicetax1);
			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else if (vat > 0 && st > 0) {
			System.out.println("in side condition");
			servicetax2 = new Phrase("VAT / ST(%)", font1);
			cellservicetax2 = new PdfPCell(servicetax2);
			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellservicetax = new PdfPCell(servicetax);
			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);

		}
		PdfPCell percdisccell = new PdfPCell(percdisctitle);
		percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell discAmtCell = new PdfPCell(discAmt);
		percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellsrNo);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellarea);
		table.addCell(cellstartdate);
		table.addCell(durationcell);
		table.addCell(servicecell);
		table.addCell(cellrate);

		if (flag1 > 0) {
			table.addCell(cellservicetax1);
		} else if (vat > 0 && st > 0) {
			table.addCell(cellservicetax2);
		} else {
			table.addCell(cellservicetax);
		}
		table.addCell(percdisccell);
		table.addCell(discAmtCell);
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidthsfordicsAMt);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell tableCell = new PdfPCell(table);
		premisesTable.addCell(tableCell);

		// //////////dummy..............change
		if (this.products.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size();
			blankLines = size * (185 / 17);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 10f;
		}
		premisesTable.setSpacingAfter(blankLines);

		Paragraph pararefer = null;

		for (int i = 0; i < this.products.size(); i++) {

			PdfPTable table1 = new PdfPTable(12);
			table1.setWidthPercentage(100);

			try {
				table1.setWidths(columnWidthsfordicsAMt);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font8);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// chunk=new
			// Phrase(products.get(i).getPrduct().getCount()+"",font8);
			// PdfPCell pdfcategcell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setColspan((int) 1.5);
			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getArea() + "", font8);
			PdfPCell pdfAreacell = new PdfPCell(chunk);
			pdfAreacell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			String date = fmt.format(products.get(i).getStartDate());
			chunk = new Phrase(date, font8);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
					font8);
			pdfSerCell = new PdfPCell(chunk);
			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getDuration() + "", font8);
			PdfPCell pdfdurCell = new PdfPCell(chunk);
			pdfdurCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;

			chunk = new Phrase(df.format(calculatedPrice), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			PdfPCell pdfservice = null;
			Phrase vatno = null;
			Phrase stno = null;
			if ((products.get(i).getVatTax().getPercentage() == 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {

				if (products.get(i).getServiceTax().getPercentage() != 0) {
					if (st == products.size()) {
						chunk = new Phrase(products.get(i).getServiceTax()
								.getPercentage()
								+ "", font8);
					} else {
						stno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage() + "", font8);
					}
				} else
					chunk = new Phrase("N.A" + "", font8);
				if (st == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(stno);
				}

				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {

				if (products.get(i).getVatTax() != null) {
					if (vat == products.size()) {
						System.out.println("rohan==" + vat);
						chunk = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ "", font8);
					} else {
						System.out.println("mukesh");
						vatno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage(), font8);
					}
				} else {
					chunk = new Phrase("N.A" + "", font8);
				}
				if (vat == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(vatno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {
				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null))

					chunk = new Phrase(products.get(i).getVatTax()
							.getPercentage()
							+ ""
							+ " / "
							+ ""
							+ products.get(i).getServiceTax().getPercentage(),
							font8);
				else
					chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else {

				chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			if (products.get(i).getPercentageDiscount() != 0) {
				chunk = new Phrase(
						products.get(i).getPercentageDiscount() + "", font8);
			} else {
				chunk = new Phrase("0" + "", font8);
			}
			PdfPCell realpercdisccell = new PdfPCell(chunk);
			realpercdisccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			realpercdisccell.setColspan((int) 0.5);

			// **************disc Amt *********************
			if (products.get(i).getDiscountAmt() != 0) {
				chunk = new Phrase(products.get(i).getDiscountAmt() + "", font8);
			} else {
				chunk = new Phrase("0" + "", font8);
			}
			PdfPCell discAmtcell = new PdfPCell(chunk);
			discAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discAmtcell.setColspan((int) 0.5);

			// rohan remove qty from calculations because we have remove that
			// from contract calculations also
			double totalVal = (products.get(i).getPrice() - taxVal);
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			} else if (products.get(i).getDiscountAmt() != 0) {
				totalVal = totalVal - products.get(i).getDiscountAmt();
			}

			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			count = i;

			table1.addCell(Pdfsrnocell);
			// table.addCell(pdfcategcell);
			table1.addCell(pdfnamecell);
			table1.addCell(pdfqtycell);
			table1.addCell(pdfAreacell);
			table1.addCell(pdfdatecell);
			table1.addCell(pdfdurCell);
			table1.addCell(pdfSerCell);
			table1.addCell(pdfspricecell);
			table1.addCell(pdfservice);
			// table.addCell(pdfvattax);
			table1.addCell(realpercdisccell);
			table1.addCell(discAmtcell);
			table1.addCell(pdftotalproduct);
			try {
				table1.setWidths(columnWidthsfordicsAMt);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			/**
			 * rohan added code here for premises details Date : 27/01/2017
			 */

			PdfPCell tableForloopCell = new PdfPCell(table1);

			premisesTable.addCell(tableForloopCell);

			String premises = "";
			if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premise Details : "
						+ products.get(i).getPremisesDetails();
			} else {
				premises = "Premise Details : " + "N A";
			}

			Phrase premisesPh = new Phrase(premises, font8);
			PdfPCell premisesCell = new PdfPCell(premisesPh);

			premisesTable.addCell(premisesCell);

			/**
			 * ends here
			 */

			// count=i;

			if (count == firstBreakPoint) {

				Phrase referannexure = new Phrase(
						"Refer Annexure 1 for additional products ", font8);
				pararefer = new Paragraph();
				pararefer.add(referannexure);

				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();
			}
		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(premisesTable);
		prodtablecell.addElement(pararefer);

		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createProductDetail() {

		/**
		 * rohan added this code for adding premises in the pdf
		 */
		PdfPTable premisesTable = new PdfPTable(1);
		premisesTable.setWidthPercentage(100f);
		/**
		 * ends here
		 */

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(11);
		table.setWidthPercentage(100);
		Phrase srno = new Phrase("SrNo", font1);
		Phrase product = new Phrase("Item Details", font1);
		Phrase qty = new Phrase("No of Branches", font1);
		Phrase area = new Phrase("AREA", font1);
		Phrase startdate = new Phrase("Start Date", font1);
		Phrase duration = new Phrase("Duration(d)", font1);
		Phrase noservices = new Phrase("Services", font1);
		Phrase rate = new Phrase("Rate", font1);

		Phrase servicetax = null;
		Phrase servicetax1 = null;
		int flag1 = 0;
		int vat = 0;
		int st = 0;
		for (int i = 0; i < products.size(); i++) {
			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() == 0)) {
				servicetax = new Phrase("VAT(%)", font1);
				System.out.println("Rohan1");
				vat = vat + 1;
				System.out.println("phrase value====" + servicetax.toString());
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {
				servicetax = new Phrase("ST(%)", font1);
				System.out.println("Rohan22222222222");
				st = st + 1;
			} else if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {
				servicetax1 = new Phrase("VAT / ST(%)", font1);
				System.out.println("Rohan3333333333333");
				flag1 = flag1 + 1;
				System.out.println("flag value;;;;;" + flag1);
			} else {

				servicetax = new Phrase("TAX(%)", font1);
				System.out.println("Rohan4444444444444");
			}
		}

		Phrase percdisctitle = null;
		if (discPer > 0) {
			percdisctitle = new Phrase("% Disc", font1);
		} else if (discAmt > 0) {
			percdisctitle = new Phrase("Disc", font1);
		}
		Phrase total = new Phrase("Total", font1);

		System.out.println("vat===" + vat);
		System.out.println("st====" + st);
		System.out.println("flag1===" + flag1);
		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		// PdfPCell cellcategory = new PdfPCell(category);
		// cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellarea = new PdfPCell(area);
		cellarea.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellstartdate = new PdfPCell(startdate);
		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicecell = new PdfPCell(noservices);
		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellservicetax1 = null;
		PdfPCell cellservicetax = null;
		PdfPCell cellservicetax2 = null;
		Phrase servicetax2 = null;

		if (flag1 > 0) {
			cellservicetax1 = new PdfPCell(servicetax1);
			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else if (vat > 0 && st > 0) {
			System.out.println("in side condition");
			servicetax2 = new Phrase("VAT / ST(%)", font1);
			cellservicetax2 = new PdfPCell(servicetax2);
			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellservicetax = new PdfPCell(servicetax);
			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);

		}
		PdfPCell percdisccell = new PdfPCell(percdisctitle);
		percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellsrNo);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellarea);
		table.addCell(cellstartdate);
		table.addCell(durationcell);
		table.addCell(servicecell);
		table.addCell(cellrate);

		if (flag1 > 0) {
			table.addCell(cellservicetax1);
		} else if (vat > 0 && st > 0) {
			table.addCell(cellservicetax2);
		} else {
			table.addCell(cellservicetax);
		}
		table.addCell(percdisccell);
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell tableCell = new PdfPCell(table);
		premisesTable.addCell(tableCell);

		// //////////dummy..............change
		if (this.products.size() <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size();
			blankLines = size * (185 / 17);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 10f;
		}
		premisesTable.setSpacingAfter(blankLines);

		Paragraph pararefer = null;

		for (int i = 0; i < this.products.size(); i++) {

			PdfPTable table1 = new PdfPTable(11);
			table1.setWidthPercentage(100);

			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font8);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// chunk=new
			// Phrase(products.get(i).getPrduct().getCount()+"",font8);
			// PdfPCell pdfcategcell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setColspan((int) 1.5);

			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getArea() + "", font8);
			PdfPCell pdfareacell = new PdfPCell(chunk);
			pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			String date = fmt.format(products.get(i).getStartDate());
			chunk = new Phrase(date, font8);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
					font8);
			pdfSerCell = new PdfPCell(chunk);
			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getDuration() + "", font8);
			PdfPCell pdfdurCell = new PdfPCell(chunk);
			pdfdurCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;

			chunk = new Phrase(df.format(calculatedPrice), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			PdfPCell pdfservice = null;
			Phrase vatno = null;
			Phrase stno = null;
			if ((products.get(i).getVatTax().getPercentage() == 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {

				if (products.get(i).getServiceTax().getPercentage() != 0) {
					if (st == products.size()) {
						chunk = new Phrase(products.get(i).getServiceTax()
								.getPercentage()
								+ "", font8);
					} else {
						stno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage() + "", font8);
					}
				} else
					chunk = new Phrase("N.A" + "", font8);
				if (st == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(stno);
				}

				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {

				if (products.get(i).getVatTax() != null) {
					if (vat == products.size()) {
						System.out.println("rohan==" + vat);
						chunk = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ "", font8);
					} else {
						System.out.println("mukesh");
						vatno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage(), font8);
					}
				} else {
					chunk = new Phrase("N.A" + "", font8);
				}
				if (vat == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(vatno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {
				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null))

					chunk = new Phrase(products.get(i).getVatTax()
							.getPercentage()
							+ ""
							+ " / "
							+ ""
							+ products.get(i).getServiceTax().getPercentage(),
							font8);
				else
					chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			} else {

				chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

			if (discPer > 0) {
				if (products.get(i).getPercentageDiscount() != 0) {
					chunk = new Phrase(products.get(i).getPercentageDiscount()
							+ "", font8);
				} else {
					chunk = new Phrase("0" + "", font8);
				}
			} else if (discAmt > 0) {
				if (products.get(i).getDiscountAmt() != 0) {
					chunk = new Phrase(products.get(i).getDiscountAmt() + "",
							font8);
				} else {
					chunk = new Phrase("0" + "", font8);
				}
			}
			PdfPCell realpercdisccell = new PdfPCell(chunk);
			realpercdisccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			realpercdisccell.setColspan((int) 0.5);

			// rohan remove qty from calculations because we have remove that
			// from contract calculations also
			double totalVal = (products.get(i).getPrice() - taxVal);
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}

			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			count = i;

			table1.addCell(Pdfsrnocell);
			// table.addCell(pdfcategcell);
			table1.addCell(pdfnamecell);
			table1.addCell(pdfqtycell);
			table1.addCell(pdfareacell);
			table1.addCell(pdfdatecell);
			table1.addCell(pdfdurCell);
			table1.addCell(pdfSerCell);
			table1.addCell(pdfspricecell);
			table1.addCell(pdfservice);
			// table1.addCell(pdfvattax);
			table1.addCell(realpercdisccell);
			table1.addCell(pdftotalproduct);
			try {
				table1.setWidths(columnWidths);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			/**
			 * rohan added code here for premises details Date : 27/01/2017
			 */

			PdfPCell table123Cell = new PdfPCell(table1);

			premisesTable.addCell(table123Cell);

			String premises = "";
			if (products.get(i).getPremisesDetails() != null
					&& !products.get(i).getPremisesDetails().equals("")) {
				premises = "Premise Details : "
						+ products.get(i).getPremisesDetails();
			} else {
				premises = "Premise Details : " + "N A";
			}

			Phrase premisesPh = new Phrase(premises, font8);
			PdfPCell premisesCell = new PdfPCell(premisesPh);

			premisesTable.addCell(premisesCell);

			/**
			 * ends here
			 */

			// count=i;

			if (count == firstBreakPoint) {

				Phrase referannexure = new Phrase(
						"Refer Annexure 1 for additional products ", font8);
				pararefer = new Paragraph();
				pararefer.add(referannexure);

				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();
			}
		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(premisesTable);
		prodtablecell.addElement(pararefer);

		parentTableProd.addCell(prodtablecell);

		try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void termsConditionsInfo() {
		System.out.println("Rohan in side terms condition info");
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

		// **************rohan added valid until ***********************
		PdfPCell validitydatecell = new PdfPCell();
		if (qp instanceof Quotation) {

			Phrase validDate = new Phrase("", font8);

			if (((Quotation) qp).getValidUntill() != null) {
				Phrase validityDate = new Phrase("Valid Until:  "
						+ fmt.format(((Quotation) qp).getValidUntill()) + "",
						font6bold);
				validitydatecell.addElement(validityDate);
				validitydatecell.setBorder(0);
			} else {
				validitydatecell.addElement(validDate);
				validitydatecell.setBorder(0);
			}
		}
		// **************************changes ends here ******************

		int np = (int) ((Quotation) qp).getNetpayable();
		String amtInFigure = ServiceInvoicePdf.convert(np);
		// Phrase netpayam=new Phrase("Rupees : "+amtInFigure,font8);
		Phrase payphrase = new Phrase("Payment Terms", font6bold);
		PdfPCell paytitle = new PdfPCell();
		paytitle.addElement(payphrase);
		paytitle.setBorder(0);

		PdfPTable table = new PdfPTable(3);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell tablecell = new PdfPCell();
		if (this.payTermsLis.size() > 5) {
			tablecell.addElement(table);
		} else {
			tablecell.addElement(table);
			tablecell.setBorder(0);
		}

		Phrase paytermdays = new Phrase("Days", font1);
		Phrase paytermpercent = new Phrase("Percent", font1);
		Phrase paytermcomment = new Phrase("Comment", font1);

		PdfPCell celldays = new PdfPCell(paytermdays);
		celldays.setBorder(0);
		celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellpercent = new PdfPCell(paytermpercent);
		cellpercent.setBorder(0);
		cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellcomment = new PdfPCell(paytermcomment);
		cellcomment.setBorder(0);
		cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(celldays);
		table.addCell(cellpercent);
		table.addCell(cellcomment);

		if (this.payTermsLis.size() <= 5) {
			int size = 5 - this.payTermsLis.size();

			blankLines = size * (80 / 5);

		} else {
			blankLines = 0;
		}
		table.setSpacingAfter(blankLines);

		for (int i = 0; i < this.payTermsLis.size(); i++) {
			Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
					font8);
			PdfPCell pdfdayscell = new PdfPCell(chunk);
			pdfdayscell.setBorder(0);
			pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(payTermsLis.get(i).getPayTermPercent()) + "",
					font8);
			PdfPCell pdfpercentcell = new PdfPCell(chunk);
			pdfpercentcell.setBorder(0);
			pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),
					font8);
			PdfPCell pdfcommentcell = new PdfPCell(chunk);
			pdfcommentcell.setBorder(0);
			pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (i == 5) {
				break;
			}

			table.addCell(pdfdayscell);
			table.addCell(pdfpercentcell);
			table.addCell(pdfcommentcell);
		}

		// /
		PdfPTable table1 = new PdfPTable(3);

		PdfPCell table1cell = new PdfPCell();
		Phrase blank = new Phrase();
		if (this.payTermsLis.size() > 5) {
			table1cell = new PdfPCell(table1);
		} else {
			table1cell = new PdfPCell(blank);
			table1cell.setBorder(0);
		}

		Phrase paytermdays1 = new Phrase("Days", font1);
		Phrase paytermpercent1 = new Phrase("Percent", font1);
		Phrase paytermcomment1 = new Phrase("Comment", font1);

		PdfPCell celldays1;
		PdfPCell cellpercent1;
		PdfPCell cellcomment1;

		System.out.println(this.payTermsLis.size() + " ...........b4 if");
		if (this.payTermsLis.size() > 5) {
			System.out.println(this.payTermsLis.size() + " ...........af if");
			celldays1 = new PdfPCell(paytermdays1);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			celldays1 = new PdfPCell(blank);
			celldays1.setBorder(0);
			celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		if (this.payTermsLis.size() > 5) {

			cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellpercent1 = new PdfPCell(blank);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);

		}

		if (this.payTermsLis.size() > 5) {
			cellcomment1 = new PdfPCell(paytermcomment1);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellcomment1 = new PdfPCell(blank);
			cellcomment1.setBorder(0);
			cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		table1.addCell(celldays1);
		table1.addCell(cellpercent1);
		table1.addCell(cellcomment1);

		for (int i = 5; i < this.payTermsLis.size(); i++) {
			Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
					font8);
			PdfPCell pdfdayscell1 = new PdfPCell(chunk);
			pdfdayscell1.setBorder(0);
			pdfdayscell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(df.format(payTermsLis.get(i).getPayTermPercent()) + "",
					font8);
			PdfPCell pdfpercentcell1 = new PdfPCell(chunk);
			pdfpercentcell1.setBorder(0);
			pdfpercentcell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),
					font8);
			PdfPCell pdfcommentcell1 = new PdfPCell(chunk);
			pdfcommentcell1.setBorder(0);
			pdfcommentcell1.setHorizontalAlignment(Element.ALIGN_CENTER);

			table1.addCell(pdfdayscell1);
			table1.addCell(pdfpercentcell1);
			table1.addCell(pdfcommentcell1);

			if (i == 9) {
				break;
			}
		}

		PdfPTable maitable = new PdfPTable(2);
		maitable.setWidthPercentage(100);
		maitable.addCell(tablecell);
		maitable.addCell(table1cell);
		maitable.setSpacingAfter(2f);

		PdfPCell mainTable = new PdfPCell();
		mainTable.addElement(maitable);
		mainTable.setBorder(0);

		Phrase descPhrase = null;
		if (qp.getPaymentTermsList() != null) {

			String titleterms = "Payment Terms";
			Phrase termsphrase = new Phrase(titleterms, font12boldul);
			termsphrase.add(Chunk.NEWLINE);
			PdfPCell termstitlecell = new PdfPCell();
			termstitlecell.addElement(termsphrase);
			termstitlecell.setBorder(0);
			PdfPTable termsTable = new PdfPTable(1);
			termsTable.addCell(termstitlecell);

			descPhrase = new Phrase();
		} else {
			descPhrase = new Phrase("  ", font7);
		}

		PdfPCell desccell = new PdfPCell();
		desccell.addElement(descPhrase);
		desccell.setBorder(0);

		PdfPCell ammountcell = new PdfPCell();
		// desccell.addElement(netpayam);
		desccell.setBorder(0);

		//
		// //******************************************
		//
		//
		//
		// String heading="Description :";
		// String decsInfo="";
		// Phrase desHead=null;
		// if(qp.getDescription()!=null){
		// decsInfo=qp.getDescription();
		// desHead= new Phrase(heading,font10bold);
		// }
		//
		// if(contEntity.getDescription()!=null){
		//
		// decsInfo=qp.getDescription();
		// desHead= new Phrase(heading,font10bold);
		// }
		//
		//
		// Phrase desphase= new Phrase(decsInfo,font8);
		// PdfPCell descell = new PdfPCell();
		// // descell.addElement(desHead);
		// descell.addElement(desphase);
		// descell.setBorder(0);

		// Chunk blank12 = null;
		// Chunk commentChunk=null;
		// Chunk deschunk = null;
		// System.out.println(qp.getDescription()+"description.......................");
		// if(qp.getDescription()!=null){
		// commentChunk= new Chunk("Description :",font10bold);
		// deschunk= new Chunk(qp.getDescription(),font8);
		// }
		// else{
		// commentChunk= new Chunk(blank12);
		// deschunk= new Chunk(blank12);
		// }
		//
		// Paragraph para = new Paragraph();
		// para.add(commentChunk);
		// para.add(Chunk.NEWLINE);
		// if(deschunk!=null){
		// para.add(deschunk);
		// }
		//
		// para.add(Chunk.NEWLINE);
		// para.add(Chunk.NEWLINE);
		// // PdfPTable decriptiontable=new PdfPTable(1);
		// // decriptiontable.setWidthPercentage(100);
		// // decriptiontable.addCell(para);
		//
		//
		// PdfPCell des = new PdfPCell();
		// des.addElement(para);
		// des.setBorder(0);
		// *******************************************
		PdfPTable termsTable = new PdfPTable(1);
		termsTable.setWidthPercentage(100);
		termsTable.setSpacingAfter(2f);

		// ************rohan added this for valid until *****************
		if (qp instanceof Quotation) {
			termsTable.addCell(validitydatecell);
		}

		// ***********changes ends here ***************************

		termsTable.addCell(paytitle);
		termsTable.addCell(mainTable);
		termsTable.addCell(desccell);
		// termsTable.addCell(descell);

		// termsTable.addCell(ammountcell);

		PdfPTable chargetaxtable = new PdfPTable(2);
		chargetaxtable.setWidthPercentage(100);
		chargetaxtable.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
		PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
		totalAmtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totalAmtCell.setBorder(0);
		double totalAmt = 0;
		totalAmt = qp.getTotalAmount();

		Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
		PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
		realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		realtotalAmtCell.setBorder(0);
		chargetaxtable.addCell(totalAmtCell);
		chargetaxtable.addCell(realtotalAmtCell);

		int firstBreakPoint = 7;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();
		System.out.println("cnt value===" + cnt);

		List<String> myList1 = new ArrayList<>();
		List<String> myList2 = new ArrayList<>();

		List<String> myList3 = new ArrayList<>();
		List<String> myList4 = new ArrayList<>();
		if (cnt > firstBreakPoint) {

			Phrase blank1 = null;
			for (int i = 0; i < this.prodTaxes.size(); i++) {
				PdfPCell pdfservicecell;
				PdfPCell pdftaxamtcell;
				double taxAmt = 0;
				double taxAmt1 = 0;
				double taxAmt2 = 0;
				double taxAmt3 = 0;
				double taxAmt4 = 0;
				PdfPCell pdftaxamt1cell = null;
				PdfPCell pdftaxamt2cell = null;
				PdfPCell pdftaxamt3cell = null;

				// *************rohan 24/4/2015*************************

				if (prodTaxes.get(i).getChargePercent() != 0) {

					if ((prodTaxes.get(i).getChargeName().equals("VAT"))) {
						// Phrase chunk = new
						// Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
						// pdfservicecell = new PdfPCell(chunk);
						// pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// pdfservicecell.setBorder(0);
						// chargetaxtable.addCell(pdfservicecell);
						//
						// taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
						// chunk=new Phrase(df.format(taxAmt),font1);
						// pdftaxamtcell = new PdfPCell(chunk);
						// pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// pdftaxamtcell.setBorder(0);
						// chargetaxtable.addCell(pdftaxamtcell);

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						myList1.add(name);
						myList2.add(df.format(taxAmt));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() != 12.36)
							&& (prodTaxes.get(i).getChargePercent() != 14.5)
							&& (prodTaxes.get(i).getChargePercent() != 15))

					{
						// chunk = new
						// Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
						// pdfservicecell = new PdfPCell(chunk);
						// pdfservicecell.setBorder(0);
						// chargetaxtable.addCell(pdfservicecell);
						//
						// taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
						// chunk=new Phrase(df.format(taxAmt),font1);
						// pdftaxamtcell = new PdfPCell(chunk);
						// pdftaxamtcell.setBorder(0);
						// pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(pdftaxamtcell);

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						String str = " ";
						myList3.add(name);
						myList4.add(df.format(taxAmt1));
						myList4.add(str);

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 12.36)) {
						System.out.println("Rohan in side 12.36");
						String name = "Service Tax " + "@ 12.00";
						taxAmt2 = (prodTaxes.get(i).getAssessableAmount() * 12) / 100;
						String name1 = "Education Cess " + "@ 2.00";
						taxAmt3 = (taxAmt2 * 2.00) / 100;
						String name2 = "High Education Cess " + "@ 1.00";
						taxAmt4 = (taxAmt2 * 1.00) / 100;
						String str = " ";

						myList3.add(name);
						myList3.add(name1);
						myList3.add(name2);

						myList4.add(df.format(taxAmt2));
						myList4.add(df.format(taxAmt3));
						myList4.add(str);
						myList4.add(df.format(taxAmt4));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 14.5)) {
						System.out.println("Rohan in side 14.5");
						String str = "Service Tax " + " @ " + " 14%";
						double serTaxAmt14 = 14 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						String str123 = "Swachha Bharat Cess" + " @ " + " 0.5%";
						double sbcTaxAmt = 0.5 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						myList3.add(str);
						myList3.add(str123);

						myList4.add(df.format(serTaxAmt14));
						myList4.add(df.format(sbcTaxAmt));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 15)) {
						System.out.println("Rohan in side 15"
								+ prodTaxes.get(i).getChargeName() + "-"
								+ prodTaxes.get(i).getChargePercent());
						String str = "Service Tax " + " @ " + " 14%";
						double serTaxAmt14 = 14 * prodTaxes.get(i)
								.getAssessableAmount() / 100;
						String blidnk = " ";

						// Phrase strp = new Phrase(str,font8);
						// PdfPCell strcell =new PdfPCell(strp);
						// strcell.setBorder(0);
						// strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// chargetaxtable.addCell(strcell);

						// Phrase taxAmt1p = new
						// Phrase(df.format(taxAmt1),font8);
						// PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
						// taxAmt1cell.setBorder(0);
						// taxAmt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(taxAmt1cell);
						//
						// myList33.add(str);
						// myList44.add(df.format(taxAmt1));
						// myList44.add(blidnk);

						String str123 = "Swachha Bharat Cess" + " @ " + " 0.5%";
						double sbcTaxAmt = 0.5 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						// Phrase str123p = new Phrase(str123,font8);
						// PdfPCell str123cell =new PdfPCell(str123p);
						// str123cell.setBorder(0);
						// str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// chargetaxtable.addCell(str123cell);

						// Phrase taxAmtp = new Phrase(df.format(taxAmt),font8);
						// PdfPCell taxAmtcell =new PdfPCell(taxAmtp);
						// taxAmtcell.setBorder(0);
						// taxAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(taxAmtcell);

						String kkc = "Krishi Kalyan Cess" + " @ " + " 0.5%";
						double kkctaxAmt = 0.5 * prodTaxes.get(i)
								.getAssessableAmount() / 100;

						myList3.add(str);
						myList3.add(str123);
						myList3.add(kkc);

						myList4.add(df.format(serTaxAmt14));
						myList4.add(df.format(sbcTaxAmt));
						myList4.add(blidnk);
						myList4.add(df.format(kkctaxAmt));
					}

					// //*****************************************************
				} else {
					pdfservicecell = new PdfPCell(blank1);
					pdfservicecell.setBorder(0);
					pdftaxamtcell = new PdfPCell(blank1);
					pdftaxamtcell.setBorder(0);
				}
			}

			PdfPCell pdfservicecell = null;

			PdfPTable other1table = new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for (int j = 0; j < myList1.size(); j++) {
				chunk = new Phrase(myList1.get(j), font8);
				pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);

			}

			PdfPCell pdfservicecell1 = null;

			PdfPTable other2table = new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for (int j = 0; j < myList2.size(); j++) {
				chunk = new Phrase(myList2.get(j), font8);
				pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);

			}

			PdfPCell chargescell = null;
			for (int j = 0; j < myList3.size(); j++) {
				chunk = new Phrase(myList3.get(j), font8);
				chargescell = new PdfPCell(chunk);
				chargescell.setBorder(0);
				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(chargescell);

			}

			PdfPCell chargescell1 = null;
			for (int j = 0; j < myList4.size(); j++) {
				chunk = new Phrase(myList4.get(j), font8);
				chargescell1 = new PdfPCell(chunk);
				chargescell1.setBorder(0);
				chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(chargescell1);

			}

			PdfPCell othercell = new PdfPCell();
			othercell.addElement(other1table);
			othercell.setBorder(0);
			chargetaxtable.addCell(othercell);

			PdfPCell othercell1 = new PdfPCell();
			othercell1.addElement(other2table);
			othercell1.setBorder(0);
			chargetaxtable.addCell(othercell1);

			PdfPCell chargecell = null;
			PdfPCell chargeamtcell = null;
			PdfPCell otherchargecell = null;
			for (int i = 0; i < this.prodCharges.size(); i++) {
				Phrase chunk = null;
				double chargeAmt = 0;
				PdfPCell pdfchargeamtcell = null;
				PdfPCell pdfchargecell = null;
				if (prodCharges.get(i).getChargePercent() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName()
							+ " @ " + prodCharges.get(i).getChargePercent(),
							font1);
					pdfchargecell = new PdfPCell(chunk);
					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargePercent()
							* prodCharges.get(i).getAssessableAmount() / 100;
					chunk = new Phrase(df.format(chargeAmt), font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);

				}
				if (prodCharges.get(i).getChargeAbsValue() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
							font1);
					pdfchargecell = new PdfPCell(chunk);

					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargeAbsValue();
					chunk = new Phrase(chargeAmt + "", font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
				}
				pdfchargecell.setBorder(0);
				total = total + chargeAmt;

				if (total != 0) {
					chunk = new Phrase("Other charges total", font1);
					chargecell = new PdfPCell(chunk);
					chargecell.setBorder(0);
					chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

					chunk = new Phrase(df.format(total), font8);
					chargeamtcell = new PdfPCell(chunk);
					chargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					chargeamtcell.setBorder(0);
				} else {

				}

				chunk = new Phrase("Refer other charge details", font1);
				otherchargecell = new PdfPCell(chunk);
				otherchargecell.setBorder(0);
				otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			}
			if (total != 0) {
				chargetaxtable.addCell(chargecell);
				chargetaxtable.addCell(chargeamtcell);
				chargetaxtable.addCell(otherchargecell);
			}

		} else if (cnt <= firstBreakPoint) {

			Phrase blank1 = null;
			for (int i = 0; i < this.prodTaxes.size(); i++) {
				PdfPCell pdfservicecell;
				PdfPCell pdftaxamtcell;
				double taxAmt = 0;
				double taxAmt1 = 0;
				double taxAmt2 = 0;
				double taxAmt3 = 0;
				double taxAmt4 = 0;
				PdfPCell pdftaxamt1cell = null;
				PdfPCell pdftaxamt2cell = null;
				PdfPCell pdftaxamt3cell = null;

				// *************rohan 24/4/2015*************************

				if (prodTaxes.get(i).getChargePercent() != 0) {

					if ((prodTaxes.get(i).getChargeName().equals("VAT"))) {
						// Phrase chunk = new
						// Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
						// pdfservicecell = new PdfPCell(chunk);
						// pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// pdfservicecell.setBorder(0);
						// chargetaxtable.addCell(pdfservicecell);
						//
						// taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
						// chunk=new Phrase(df.format(taxAmt),font1);
						// pdftaxamtcell = new PdfPCell(chunk);
						// pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// pdftaxamtcell.setBorder(0);
						// chargetaxtable.addCell(pdftaxamtcell);

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;
						myList1.add(name);
						myList2.add(df.format(taxAmt));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() != 12.36))

					{
						// chunk = new
						// Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
						// pdfservicecell = new PdfPCell(chunk);
						// pdfservicecell.setBorder(0);
						// chargetaxtable.addCell(pdfservicecell);
						//
						// taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
						// chunk=new Phrase(df.format(taxAmt),font1);
						// pdftaxamtcell = new PdfPCell(chunk);
						// pdftaxamtcell.setBorder(0);
						// pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(pdftaxamtcell);

						String name = prodTaxes.get(i).getChargeName() + " @ "
								+ prodTaxes.get(i).getChargePercent();
						taxAmt1 = prodTaxes.get(i).getChargePercent()
								* prodTaxes.get(i).getAssessableAmount() / 100;

						myList1.add(name);
						myList2.add(df.format(taxAmt1));

					}

					if ((prodTaxes.get(i).getChargeName().equals("Service Tax"))
							&& (prodTaxes.get(i).getChargePercent() == 12.36)) {

						// Phrase chunk1 = new
						// Phrase("Service Tax "+"@ 12.00",font1);
						// PdfPCell pdfservcell = new PdfPCell(chunk1);
						// pdfservcell.setBorder(0);
						// pdfservcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// chargetaxtable.addCell(pdfservcell);
						//
						//
						// taxAmt1=(prodTaxes.get(i).getAssessableAmount()*12)/100;
						// chunk=new Phrase(df.format(taxAmt1),font1);
						// pdftaxamt1cell = new PdfPCell(chunk);
						// pdftaxamt1cell.setBorder(0);
						// pdftaxamt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(pdftaxamt1cell);
						//
						//
						// Phrase chunk2 = new
						// Phrase("Education Cess "+"@ 2.00",font1);
						// PdfPCell pdfEducesscell = new PdfPCell(chunk2);
						// pdfEducesscell.setBorder(0);
						// pdfEducesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// chargetaxtable.addCell(pdfEducesscell);
						//
						// taxAmt2=(taxAmt1*2.00)/100;
						// chunk=new Phrase(df.format(taxAmt2),font1);
						// pdftaxamt2cell = new PdfPCell(chunk);
						// pdftaxamt2cell.setBorder(0);
						// pdftaxamt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(pdftaxamt2cell);
						//
						//
						// Phrase chunk3 = new
						// Phrase("High Education Cess "+"@ 1.00",font1);
						// PdfPCell pdfHighcesscell = new PdfPCell(chunk3);
						// pdfHighcesscell.setBorder(0);
						// pdfHighcesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
						// chargetaxtable.addCell(pdfHighcesscell);
						//
						// taxAmt3=(taxAmt1*1.00)/100;
						// chunk=new Phrase(df.format(taxAmt3),font1);
						// pdftaxamt3cell = new PdfPCell(chunk);
						// pdftaxamt3cell.setBorder(0);
						// pdftaxamt3cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						// chargetaxtable.addCell(pdftaxamt3cell);

						String name = "Service Tax " + "@ 12.00";
						taxAmt2 = (prodTaxes.get(i).getAssessableAmount() * 12) / 100;
						String name1 = "Education Cess " + "@ 2.00";
						taxAmt3 = (taxAmt2 * 2.00) / 100;
						String name2 = "High Education Cess " + "@ 1.00";
						taxAmt4 = (taxAmt2 * 1.00) / 100;
						String str = " ";
						myList1.add(name);
						myList1.add(name1);
						myList1.add(name2);

						myList2.add(df.format(taxAmt2));
						myList2.add(df.format(taxAmt3));
						myList2.add(str);
						myList2.add(df.format(taxAmt4));

					}

					// *****************************************************
				}
				// else{
				// pdfservicecell = new PdfPCell(blank1);
				// pdfservicecell.setBorder(0);
				// pdftaxamtcell = new PdfPCell(blank1);
				// pdftaxamtcell.setBorder(0);
				// }
			}

			PdfPCell pdfservicecell = null;

			PdfPTable other1table = new PdfPTable(1);
			other1table.setWidthPercentage(100);
			for (int j = 0; j < myList1.size(); j++) {
				chunk = new Phrase(myList1.get(j), font8);
				pdfservicecell = new PdfPCell(chunk);
				pdfservicecell.setBorder(0);
				pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservicecell);

			}

			PdfPCell pdfservicecell1 = null;

			PdfPTable other2table = new PdfPTable(1);
			other2table.setWidthPercentage(100);
			for (int j = 0; j < myList2.size(); j++) {
				chunk = new Phrase(myList2.get(j), font8);
				pdfservicecell1 = new PdfPCell(chunk);
				pdfservicecell1.setBorder(0);
				pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservicecell1);

			}

			PdfPCell othercell = new PdfPCell();
			othercell.addElement(other1table);
			othercell.setBorder(0);
			chargetaxtable.addCell(othercell);

			PdfPCell othercell1 = new PdfPCell();
			othercell1.addElement(other2table);
			othercell1.setBorder(0);
			chargetaxtable.addCell(othercell1);

			for (int i = 0; i < this.prodCharges.size(); i++) {
				Phrase chunk = null;
				double chargeAmt = 0;
				PdfPCell pdfchargeamtcell = null;
				PdfPCell pdfchargecell = null;
				if (prodCharges.get(i).getChargePercent() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName()
							+ " @ " + prodCharges.get(i).getChargePercent(),
							font1);
					pdfchargecell = new PdfPCell(chunk);
					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargePercent()
							* prodCharges.get(i).getAssessableAmount() / 100;
					chunk = new Phrase(df.format(chargeAmt), font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
					chargetaxtable.addCell(pdfchargecell);
					chargetaxtable.addCell(pdfchargeamtcell);

				}
				if (prodCharges.get(i).getChargeAbsValue() != 0) {
					chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
							font1);
					pdfchargecell = new PdfPCell(chunk);

					pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					chargeAmt = prodCharges.get(i).getChargeAbsValue();
					chunk = new Phrase(chargeAmt + "", font8);
					pdfchargeamtcell = new PdfPCell(chunk);
					pdfchargeamtcell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					pdfchargeamtcell.setBorder(0);
					chargetaxtable.addCell(pdfchargecell);
					chargetaxtable.addCell(pdfchargeamtcell);
				}
				// pdfchargecell.setBorder(0);
				// chargetaxtable.addCell(pdfchargecell);
				// chargetaxtable.addCell(pdfchargeamtcell);

			}
		}

		// Phrase blank1=null;
		// for(int i=0;i<this.prodTaxes.size();i++)
		// {
		// PdfPCell pdfservicecell;
		// PdfPCell pdftaxamtcell;
		// double taxAmt=0;
		// double taxAmt1=0;
		// double taxAmt2=0;
		// double taxAmt3=0;
		// PdfPCell pdftaxamt1cell = null;
		// PdfPCell pdftaxamt2cell = null;
		// PdfPCell pdftaxamt3cell = null;
		//
		// //*************rohan 24/4/2015*************************
		//
		// if(prodTaxes.get(i).getChargePercent()!=0){
		//
		// if((prodTaxes.get(i).getChargeName().equals("VAT")))
		// {
		// Phrase chunk = new
		// Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
		// pdfservicecell = new PdfPCell(chunk);
		// pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// pdfservicecell.setBorder(0);
		// chargetaxtable.addCell(pdfservicecell);
		//
		// taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
		// chunk=new Phrase(df.format(taxAmt),font1);
		// pdftaxamtcell = new PdfPCell(chunk);
		// pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// pdftaxamtcell.setBorder(0);
		// chargetaxtable.addCell(pdftaxamtcell);
		//
		// }
		//
		// if((prodTaxes.get(i).getChargeName().equals("Service Tax"))&&(prodTaxes.get(i).getChargePercent()!=12.36))
		//
		// {
		// chunk = new
		// Phrase(prodTaxes.get(i).getChargeName()+" @ "+prodTaxes.get(i).getChargePercent(),font1);
		// pdfservicecell = new PdfPCell(chunk);
		// pdfservicecell.setBorder(0);
		// chargetaxtable.addCell(pdfservicecell);
		//
		// taxAmt=prodTaxes.get(i).getChargePercent()*prodTaxes.get(i).getAssessableAmount()/100;
		// chunk=new Phrase(df.format(taxAmt),font1);
		// pdftaxamtcell = new PdfPCell(chunk);
		// pdftaxamtcell.setBorder(0);
		// pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// chargetaxtable.addCell(pdftaxamtcell);
		// }
		//
		//
		// if((prodTaxes.get(i).getChargeName().equals("Service Tax"))&&(prodTaxes.get(i).getChargePercent()==12.36))
		// {
		//
		// Phrase chunk1 = new Phrase("Service Tax "+"@ 12.00",font1);
		// PdfPCell pdfservcell = new PdfPCell(chunk1);
		// pdfservcell.setBorder(0);
		// pdfservcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// chargetaxtable.addCell(pdfservcell);
		//
		//
		// taxAmt1=(prodTaxes.get(i).getAssessableAmount()*12)/100;
		// chunk=new Phrase(df.format(taxAmt1),font1);
		// pdftaxamt1cell = new PdfPCell(chunk);
		// pdftaxamt1cell.setBorder(0);
		// pdftaxamt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// chargetaxtable.addCell(pdftaxamt1cell);
		//
		//
		// Phrase chunk2 = new Phrase("Education Cess "+"@ 2.00",font1);
		// PdfPCell pdfEducesscell = new PdfPCell(chunk2);
		// pdfEducesscell.setBorder(0);
		// pdfEducesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// chargetaxtable.addCell(pdfEducesscell);
		//
		// taxAmt2=(taxAmt1*2.00)/100;
		// chunk=new Phrase(df.format(taxAmt2),font1);
		// pdftaxamt2cell = new PdfPCell(chunk);
		// pdftaxamt2cell.setBorder(0);
		// pdftaxamt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// chargetaxtable.addCell(pdftaxamt2cell);
		//
		//
		// Phrase chunk3 = new Phrase("High Education Cess "+"@ 1.00",font1);
		// PdfPCell pdfHighcesscell = new PdfPCell(chunk3);
		// pdfHighcesscell.setBorder(0);
		// pdfHighcesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// chargetaxtable.addCell(pdfHighcesscell);
		//
		// taxAmt3=(taxAmt1*1.00)/100;
		// chunk=new Phrase(df.format(taxAmt3),font1);
		// pdftaxamt3cell = new PdfPCell(chunk);
		// pdftaxamt3cell.setBorder(0);
		// pdftaxamt3cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// chargetaxtable.addCell(pdftaxamt3cell);
		//
		// }
		//
		// //*****************************************************
		// }
		// else{
		// pdfservicecell = new PdfPCell(blank1);
		// pdfservicecell.setBorder(0);
		// pdftaxamtcell = new PdfPCell(blank1);
		// pdftaxamtcell.setBorder(0);
		// }
		//
		// }

		/****************************************************************************************/

		PdfPTable netpaytable = new PdfPTable(2);
		netpaytable.setWidthPercentage(100);
		netpaytable.setHorizontalAlignment(Element.ALIGN_BOTTOM);

		// for(int i=0;i<this.prodCharges.size();i++)
		// {
		// Phrase chunk = null;
		// double chargeAmt=0;
		// PdfPCell pdfchargeamtcell = null;
		// PdfPCell pdfchargecell = null;
		// if(prodCharges.get(i).getChargePercent()!=0){
		// chunk = new
		// Phrase(prodCharges.get(i).getChargeName()+" @ "+prodCharges.get(i).getChargePercent(),font1);
		// pdfchargecell=new PdfPCell(chunk);
		// pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// chargeAmt=prodCharges.get(i).getChargePercent()*prodCharges.get(i).getAssessableAmount()/100;
		// chunk=new Phrase(df.format(chargeAmt),font8);
		// pdfchargeamtcell = new PdfPCell(chunk);
		// pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// pdfchargeamtcell.setBorder(0);
		//
		// }
		// if(prodCharges.get(i).getChargeAbsValue()!=0){
		// chunk = new Phrase(prodCharges.get(i).getChargeName()+"",font1);
		// pdfchargecell=new PdfPCell(chunk);
		//
		// pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// chargeAmt=prodCharges.get(i).getChargeAbsValue();
		// chunk=new Phrase(chargeAmt+"",font8);
		// pdfchargeamtcell = new PdfPCell(chunk);
		// pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// pdfchargeamtcell.setBorder(0);
		// }
		// pdfchargecell.setBorder(0);
		// chargetaxtable.addCell(pdfchargecell);
		// chargetaxtable.addCell(pdfchargeamtcell);
		//
		// }
		for (int i = 0; i < this.prodCharges.size(); i++) {
			System.out
					.println("======I  " + i + "   size" + prodCharges.size());
			if (i == prodCharges.size() - 1) {
				chunk = new Phrase("Net Payable", font1);
				PdfPCell pdfnetpaycell = new PdfPCell(chunk);
				pdfnetpaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
				pdfnetpaycell.setBorder(0);
				Double netPayAmt = (double) 0;

				netPayAmt = ((Quotation) qp).getNetpayable();
				var = (int) netPayAmt.doubleValue();
				chunk = new Phrase(var + "", font8);
				PdfPCell pdfnetpayamt = new PdfPCell(chunk);
				pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfnetpayamt.setBorder(0);

				netpaytable.addCell(pdfnetpaycell);
				netpaytable.addCell(pdfnetpayamt);
				// chargetaxtable.addCell(pdfnetpaycell);
				// chargetaxtable.addCell(pdfnetpayamt);
			}
		}

		if (this.prodCharges.size() == 0) {
			Double netPayAmt = (double) 0;
			Phrase chunknetpaytitle = new Phrase("Net Payable", font1);
			PdfPCell netpaytitlecell = new PdfPCell(chunknetpaytitle);
			netpaytitlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			netpaytitlecell.setBorder(0);
			netPayAmt = ((Quotation) qp).getNetpayable();
			var = (int) netPayAmt.doubleValue();
			Phrase chunknetpay = new Phrase(var + "", font8);
			PdfPCell pdfnetpayamt = new PdfPCell(chunknetpay);
			pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			pdfnetpayamt.setBorder(0);
			// chargetaxtable.addCell(netpaytitlecell);
			// chargetaxtable.addCell(pdfnetpayamt);

			netpaytable.addCell(netpaytitlecell);
			netpaytable.addCell(pdfnetpayamt);
		}

		PdfPTable taxinfotable = new PdfPTable(1);
		taxinfotable.setWidthPercentage(100);
		taxinfotable.setHorizontalAlignment(Element.ALIGN_LEFT);
		taxinfotable.addCell(chargetaxtable);
		// taxinfotable.setSpacingAfter(10f);

		PdfPTable parenttaxtable = new PdfPTable(2);
		parenttaxtable.setWidthPercentage(100);
		try {
			parenttaxtable.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell termsdatacell = new PdfPCell();
		PdfPCell taxdatacell = new PdfPCell();

		termsdatacell.addElement(termsTable);
		taxdatacell.addElement(chargetaxtable);
		parenttaxtable.addCell(termsdatacell);
		termsdatacell.setBorder(0);
		parenttaxtable.addCell(taxdatacell);

		try {

			document.add(parenttaxtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// *****************for amt in
		// words****************************************

		double netPayInFigures = 0;

		String amtInWords = "";

		// if(qp instanceof SalesQuotation){
		// netPayInFigures=((SalesQuotation) qp).getNetpayable();
		// }
		// if(qp instanceof SalesOrder){
		// netPayInFigures=((SalesOrder) qp).getNetpayable();
		// }

		System.out.println("amt" + netPayInFigures);
		// Phrase netpayam=new Phrase("Rupees : "+amtInFigure,font8);
		Phrase amtwords = new Phrase(amtInWords, font1);
		Phrase netPay1 = new Phrase("Rupees: " + ServiceInvoicePdf.convert(np)
				+ " Only", font8);

		PdfPCell amtWordsCell = new PdfPCell();
		amtWordsCell.addElement(amtwords);
		amtWordsCell.addElement(netPay1);
		amtWordsCell.setBorder(0);

		PdfPTable amonttable = new PdfPTable(1);
		amonttable.setWidthPercentage(100);
		amonttable.addCell(amtWordsCell);
		amonttable.setSpacingAfter(5f);

		PdfPTable parenttaxtable1 = new PdfPTable(2);
		parenttaxtable1.setWidthPercentage(100);
		try {
			parenttaxtable1.setWidths(new float[] { 65, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell termsdatacell2 = new PdfPCell();
		PdfPCell taxdatacell2 = new PdfPCell();

		termsdatacell2.addElement(amonttable);
		taxdatacell2.addElement(netpaytable);
		parenttaxtable1.addCell(termsdatacell2);
		parenttaxtable1.addCell(taxdatacell2);

		try {
			document.add(parenttaxtable1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// ////////////.................................................

	public void footerInfo() {
		PdfPTable bankinfotable = new PdfPTable(1);
		bankinfotable.setWidthPercentage(100);
		// bankinfotable.setSpacingBefore(35f);
		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankc = new PdfPCell();
		blankc.addElement(blank);
		blankc.setBorder(0);
		System.out.println(" condition===============1");
		if (qp instanceof Contract) {

			Phrase confirmation = new Phrase("   I/We hereby confirm ", font8);
			PdfPCell confirmationcell = new PdfPCell();
			confirmationcell.addElement(confirmation);
			confirmationcell.setBorder(0);
			bankinfotable.addCell(confirmationcell);

			bankinfotable.addCell(blankc);
			bankinfotable.addCell(blankc);
			bankinfotable.addCell(blankc);

			Phrase dottedline = new Phrase("---------------------------");
			PdfPCell dottedlinecell = new PdfPCell();
			dottedlinecell.addElement(dottedline);
			dottedlinecell.setBorder(0);
			bankinfotable.addCell(dottedlinecell);

			Phrase custsignphrase = new Phrase("Customer Signature");
			PdfPCell custsigncell = new PdfPCell();
			custsigncell.addElement(custsignphrase);
			custsigncell.setBorder(0);
			bankinfotable.addCell(custsigncell);

		} else {

			System.out.println("else condition===============1");
			Phrase typename = null;
			Phrase typevalue = null;
			PdfPTable artictable = new PdfPTable(2);
			artictable.setWidthPercentage(100);
			artictable.setHorizontalAlignment(Element.ALIGN_LEFT);
			System.out.println("b4 for loop of Articles type == "
					+ this.articletype.size());

			for (int i = 0; i < this.articletype.size(); i++) {

				System.out.println("all docu name if out == "
						+ articletype.get(i).getDocumentName());
				if (articletype.get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& articletype.get(i).getDocumentName()
								.equals("ServiceQuotation")) {

					System.out.println("all docu name if in == "
							+ articletype.get(i).getDocumentName());

					typename = new Phrase(articletype.get(i)
							.getArticleTypeName(), font8);
					typevalue = new Phrase(articletype.get(i)
							.getArticleTypeValue(), font8);

					PdfPCell tymanecell = new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);
					tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					PdfPCell typevalcell = new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					typevalcell.setBorder(0);
					PdfPCell typevalcell1 = new PdfPCell();
					typevalcell1.setBorder(0);

					artictable.addCell(tymanecell);
					artictable.addCell(typevalcell);
					artictable.addCell(typevalcell1);
					artictable.addCell(typevalcell1);
				}

				// if(comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("YES")
				// &&
				// comp.getArticleTypeDetails().get(i).getArDocumentName().equals("ServiceQuotation")){
				//
				//
				// System.out.println("cust docu name if in == "+comp.getArticleTypeDetails().get(i).getArDocumentName());
				//
				// typename = new
				// Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName(),font8);
				// typevalue = new
				// Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeValue(),font8);
				//
				// PdfPCell tymanecell=new PdfPCell();
				// tymanecell.addElement(typename);
				// tymanecell.setBorder(0);
				// tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// PdfPCell typevalcell=new PdfPCell();
				// typevalcell.addElement(typevalue);
				// typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// typevalcell.setBorder(0);
				// PdfPCell typevalcell1=new PdfPCell();
				// typevalcell1.setBorder(0);
				//
				// artictable.addCell(tymanecell);
				// artictable.addCell(typevalcell);
				// artictable.addCell(typevalcell1);
				// artictable.addCell(typevalcell1);
				// }

			}
			PdfPCell pdfcell = new PdfPCell(artictable);
			pdfcell.setBorder(0);
			pdfcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			bankinfotable.addCell(pdfcell);
		}
		System.out.println("1 condition===============1");

		PdfPTable descTable = new PdfPTable(1);
		descTable.setWidthPercentage(100f);

		if (qp instanceof Contract) {

			Phrase typename = null;
			Phrase typevalue = null;
			PdfPTable artictable1 = new PdfPTable(2);
			artictable1 = new PdfPTable(2);
			artictable1.setWidthPercentage(100);
			artictable1.setHorizontalAlignment(Element.ALIGN_LEFT);
			for (int i = 0; i < this.articletype.size(); i++) {

				if (articletype.get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& articletype.get(i).getDocumentName()
								.equals("Contract")) {

					typename = new Phrase(articletype.get(i)
							.getArticleTypeName(), font8);
					typevalue = new Phrase(articletype.get(i)
							.getArticleTypeValue(), font8);

					PdfPCell tymanecell = new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);
					tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					PdfPCell typevalcell = new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					typevalcell.setBorder(0);
					PdfPCell typevalcell1 = new PdfPCell();
					typevalcell1.setBorder(0);

					artictable1.addCell(tymanecell);
					artictable1.addCell(typevalcell);
					artictable1.addCell(typevalcell1);
					artictable1.addCell(typevalcell1);
				}

			}
			PdfPCell pdfcell = new PdfPCell(artictable1);
			pdfcell.setBorder(0);
			pdfcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			descTable.addCell(pdfcell);

			if (printDescOnFirstPageFlag) {
				if (contEntity.getDescription() != null
						&& !contEntity.getDescription().equals("")) {
					Phrase disc = new Phrase(contEntity.getDescription(), font8);
					PdfPCell discCell = new PdfPCell(disc);
					discCell.setBorder(0);
					descTable.addCell(discCell);
				} else {
					Phrase blankspace = new Phrase(" ", font8);
					PdfPCell blankspaceCell = new PdfPCell(blankspace);
					blankspaceCell.setBorder(0);
					descTable.addCell(blankspaceCell);
				}

			} else {
				Phrase blankspace = new Phrase(" ", font8);
				PdfPCell blankspaceCell = new PdfPCell(blankspace);
				blankspaceCell.setBorder(0);
				descTable.addCell(blankspaceCell);
			}

		} else {
			if (printDescOnFirstPageFlag) {
				if (quotEntity.getDescription() != null
						&& !quotEntity.getDescription().equals("")) {
					Phrase disc = new Phrase(quotEntity.getDescription(), font8);
					PdfPCell discCell = new PdfPCell(disc);
					discCell.setBorder(0);
					descTable.addCell(discCell);
				} else {
					Phrase blankspace = new Phrase(" ", font8);
					PdfPCell blankspaceCell = new PdfPCell(blankspace);
					blankspaceCell.setBorder(0);
					descTable.addCell(blankspaceCell);
				}
			} else {
				Phrase blankspace = new Phrase(" ", font8);
				PdfPCell blankspaceCell = new PdfPCell(blankspace);
				blankspaceCell.setBorder(0);
				descTable.addCell(blankspaceCell);
			}

		}

		System.out.println("2 condition===============1");

		Phrase recieverSinature = new Phrase("", font12boldul);
		Phrase blankphrase = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);
		Phrase blankphrase1 = new Phrase(" ");
		Phrase blankphrase2 = new Phrase(" ");
		System.out.println("3 condition===============1");
		String companyname = "FOR "
				+ comp.getBusinessUnitName().trim().toUpperCase();
		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font9bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		// Phrase companyname= new
		// Phrase(comppayment.getPaymentComName().trim(),font12bold);
		// Phrase authorizedsignatory=null;
		// authorizedsignatory = new
		// Phrase("AUTHORISED SIGNATORY"+"",font10bold);
		String authsign = "AUTHORISED SIGNATORY";
		Paragraph authpara = new Paragraph();
		authpara.setFont(font9bold);
		authpara.add(authsign);
		authpara.setAlignment(Element.ALIGN_CENTER);
		System.out.println("4 condition===============1");
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);

		PdfPCell authsigncell = new PdfPCell();
		authsigncell.addElement(authpara);
		authsigncell.setBorder(0);
		System.out.println("5 condition===============1");
		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		// table.addCell(blankcell);
		table.addCell(authsigncell);

		table.setWidthPercentage(100);

		System.out.println("6 condition===============1");

		// //******************end of document***********************
		// String endDucument =
		// "*************************X--X--X*************************";
		// Paragraph endpara=new Paragraph();
		// endpara.add(endDucument);
		// endpara.setAlignment(Element.ALIGN_CENTER);
		// //***************************************************

		PdfPTable parentbanktable = new PdfPTable(3);
		parentbanktable.setWidthPercentage(100);
		parentbanktable.setSpacingAfter(10f);
		try {
			parentbanktable.setWidths(new float[] { 25, 40, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		PdfPCell authorisedcell1 = new PdfPCell();
		PdfPCell bankdatacell = new PdfPCell();
		PdfPCell authorisedcell = new PdfPCell();
		authorisedcell1.addElement(descTable);
		System.out.println("7 condition===============1");
		bankdatacell.addElement(bankinfotable);
		authorisedcell.addElement(table);
		parentbanktable.addCell(bankdatacell);
		parentbanktable.addCell(authorisedcell1);
		parentbanktable.addCell(authorisedcell);

		System.out.println("8 condition===============1");
		Phrase ref = new Phrase("Annexure 1 ", font6bold);
		Paragraph pararef = new Paragraph();
		pararef.add(ref);

		try {
			System.out.println("9 condition===============1");
			document.add(parentbanktable);
			System.out.println("10 condition===============1");
			logger.log(Level.SEVERE, this.products.size() + "");
			if (this.products.size() > 17) {
				logger.log(Level.SEVERE, this.products.size() + "");
				document.newPage();
				document.add(pararef);
				settingRemainingRowsToPDF(18);
			}
			if (this.products.size() > 78) {
				logger.log(Level.SEVERE, this.products.size() + "");
				document.newPage();
				document.add(pararef);
				settingRemainingRowsToPDF(79);
			}
			if (this.products.size() > 139) {
				logger.log(Level.SEVERE, this.products.size() + "");
				document.newPage();
				settingRemainingRowsToPDF(140);
			}
			if (this.products.size() > 200) {
				logger.log(Level.SEVERE, this.products.size() + "");
				document.newPage();
				document.add(pararef);
				settingRemainingRowsToPDF(201);
			}
			// if(this.products.size()>20){
			// document.add(endpara);
			// }
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// if(service!=null){
		try {
			createServices();
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// }

		int firstBreakPoint = 7;
		int cnt = this.prodTaxes.size() + this.prodCharges.size();
		System.out.println("cnt value===" + cnt);
		if (cnt > firstBreakPoint) {

			otherchargestoAnnexur();
		}
	}

	private void setDescriptiontoAnnexure() {

		// ******************************************

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_LEFT);

		PdfPTable descriptiontable = new PdfPTable(4);
		descriptiontable.setWidthPercentage(100);
		descriptiontable.setSpacingBefore(10);

		Phrase srno = new Phrase("SrNo", font1);
		Phrase category = new Phrase("Prod ID", font1);
		Phrase product = new Phrase("Prod Name", font1);
		Phrase qty = new Phrase("Description", font1);

		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellprodID = new PdfPCell(category);
		cellprodID.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellprodName = new PdfPCell(product);
		cellprodName.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellDesc = new PdfPCell(qty);
		cellDesc.setHorizontalAlignment(Element.ALIGN_CENTER);

		descriptiontable.addCell(cellsrNo);
		descriptiontable.addCell(cellprodID);
		descriptiontable.addCell(cellprodName);
		descriptiontable.addCell(cellDesc);

		for (int i = 0; i < stringlis.size(); i++) {

			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font8);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(stringlis.get(i).getCount() + "", font8);
			PdfPCell pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(stringlis.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);

			chunk = new Phrase(stringlis.get(i).getComment(), font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_LEFT);

			descriptiontable.addCell(Pdfsrnocell);
			descriptiontable.addCell(pdfcategcell);
			descriptiontable.addCell(pdfnamecell);
			descriptiontable.addCell(pdfqtycell);

		}
		try {
			descriptiontable.setWidths(columnWidths1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// try {
		//
		// document.newPage();
		// document.add(para);
		// document.add(descriptiontable);
		// } catch (DocumentException e) {
		// e.printStackTrace();
		// }
	}

	public void createServices() throws DocumentException {
		if (qp instanceof Contract) {
			int flagContractData = 0;
			ProcessConfiguration processConfig = ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", AppConstants.PROCESSCONFIGCO)
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int i = 0; i < processConfig.getProcessList().size(); i++) {
					if (processConfig
							.getProcessList()
							.get(i)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									AppConstants.PROCESSTYPECONTRACTSERVDATA)
							&& processConfig.getProcessList().get(i).isStatus() == true) {
						flagContractData = 1;
					}
				}
				if (flagContractData == 1 && processConfig != null) {
					displayServices();
				}
			}
		} else {
			int flagQuotationData = 0;
			ProcessConfiguration processConfig = ofy().load()
					.type(ProcessConfiguration.class)
					.filter("companyId", comp.getCompanyId())
					.filter("processName", AppConstants.PROCESSCONFIGQUO)
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int i = 0; i < processConfig.getProcessList().size(); i++) {
					if (processConfig
							.getProcessList()
							.get(i)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									AppConstants.PROCESSTYPEQUOTATIONSERVDATA)
							&& processConfig.getProcessList().get(i).isStatus() == true) {
						flagQuotationData = 1;
					}
				}
				if (flagQuotationData == 1 && processConfig != null) {
					displayServices();
				}
			}
		}
	}

	private void displayServices() throws DocumentException {

		// Add new line

		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		document.add(nextpage);
		if (upcflag == true) {
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		}
		String terms = "Terms And Conditions";
		Phrase term = new Phrase(terms, font12bold);

		Paragraph para1 = new Paragraph();
		para1.add(term);
		para1.setAlignment(Element.ALIGN_CENTER);

		String decsInfo = "";

		if (qp instanceof Quotation) {
			if (qp.getDescription() != null) {
				decsInfo = qp.getDescription();
			}
		}

		if (qp instanceof Contract) {
			if (contEntity.getDescription() != null) {
				decsInfo = qp.getDescription();
			}
		}

		Phrase desphase = new Phrase(decsInfo, font8);
		Paragraph para2 = new Paragraph();
		para2.add(desphase);

		document.add(para1);
		document.add(para2);
		// addServiceDetail();

		String desc = "";
		for (int i = 0; i < stringlis.size(); i++) {

			Phrase service = new Phrase("Service Product :"
					+ stringlis.get(i).getProductName(), font12bold);

			Paragraph servicePara = new Paragraph();
			servicePara.add(service);

			desc = stringlis.get(i).getComment();
			Phrase prodDesc = new Phrase(desc, font8);
			Paragraph parades = new Paragraph(prodDesc);

			PdfPTable serviceTable = new PdfPTable(4);
			serviceTable.setWidthPercentage(100);

			Phrase serno = new Phrase("Service No", font6bold);
			Phrase serDate = new Phrase("Service Date", font6bold);
			Phrase serStatus = new Phrase("Service Status", font6bold);
			Phrase custBranch = new Phrase("Customer Branch", font6bold);

			PdfPCell sernocell = new PdfPCell(serno);
			PdfPCell serDatecell = new PdfPCell(serDate);
			PdfPCell serStatuscell = new PdfPCell(serStatus);
			PdfPCell custBranchcell = new PdfPCell(custBranch);

			serviceTable.addCell(sernocell);
			serviceTable.addCell(serDatecell);
			serviceTable.addCell(serStatuscell);
			serviceTable.addCell(custBranchcell);
			serviceTable.setSpacingBefore(10f);

			Phrase chunk = null;

			if (qp instanceof Contract) {
				System.out.println("service Schedule list size"
						+ contEntity.getServiceScheduleList().size());

				for (int k = 0; k < contEntity.getServiceScheduleList().size(); k++) {

					if (stringlis.get(i).getCount() == contEntity
							.getServiceScheduleList().get(k)
							.getScheduleProdId()) {
						chunk = new Phrase(contEntity.getServiceScheduleList()
								.get(k).getScheduleServiceNo()
								+ "", font8);
						PdfPCell srno = new PdfPCell(chunk);

						String serviceDt = fmt1.format(contEntity
								.getServiceScheduleList().get(k)
								.getScheduleServiceDate());
						chunk = new Phrase(serviceDt, font8);
						PdfPCell srDate = new PdfPCell(chunk);

						chunk = new Phrase("Scheduled", font8);
						PdfPCell srStatus = new PdfPCell(chunk);

						chunk = new Phrase(contEntity.getServiceScheduleList()
								.get(k).getScheduleProBranch(), font8);
						PdfPCell srBranch = new PdfPCell(chunk);

						serviceTable.addCell(srno);
						serviceTable.addCell(srDate);
						serviceTable.addCell(srStatus);
						serviceTable.addCell(srBranch);
					}
				}
			} else {
				for (int k = 0; k < quotEntity.getServiceScheduleList().size(); k++) {

					if (stringlis.get(i).getCount() == quotEntity
							.getServiceScheduleList().get(k)
							.getScheduleProdId()) {
						chunk = new Phrase(quotEntity.getServiceScheduleList()
								.get(k).getScheduleServiceNo()
								+ "", font8);
						PdfPCell srno = new PdfPCell(chunk);

						chunk = new Phrase(fmt1.format(quotEntity
								.getServiceScheduleList().get(k)
								.getScheduleServiceDate()), font8);
						PdfPCell srDate = new PdfPCell(chunk);

						chunk = new Phrase("Scheduled", font8);
						PdfPCell srStatus = new PdfPCell(chunk);

						chunk = new Phrase(quotEntity.getServiceScheduleList()
								.get(k).getScheduleProBranch(), font8);
						PdfPCell srBranch = new PdfPCell(chunk);

						serviceTable.addCell(srno);
						serviceTable.addCell(srDate);
						serviceTable.addCell(srStatus);
						serviceTable.addCell(srBranch);

					}
				}
			}
			try {
				document.add(servicePara);
				document.add(parades);
				document.add(serviceTable);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
	}

	private void createServiceHeader(PdfPTable table) {

		Chunk seviceno = new Chunk("Service Number", this.font8bold);
		PdfPCell servicenumbercell = new PdfPCell();
		servicenumbercell.addElement(seviceno);

		Chunk chunk = new Chunk();
		chunk = new Chunk("Date", this.font8bold);

		PdfPCell datecell = new PdfPCell();
		datecell.addElement(chunk);

		chunk = new Chunk("Status", this.font8bold);

		PdfPCell statuscell = new PdfPCell();
		statuscell.addElement(chunk);

		table.addCell(servicenumbercell);
		table.addCell(datecell);
		// table.addCell(serviceEngineercell);
		// table.addCell(serviceEngineercell1);
		if (qp instanceof Contract) {
			table.addCell(statuscell);
			table.setHorizontalAlignment(Element.ALIGN_LEFT);
		}

	}

	public double total(SalesLineItem salesLineItem) {
		double tax = 0;

		if (salesLineItem.getServiceTax() != null
				&& salesLineItem.getServiceTax().isInclusive() == false)
			tax = tax
					+ (salesLineItem.getServiceTax().getPercentage()
							* salesLineItem.getPrice() / 100);
		if (salesLineItem.getVatTax() != null
				&& salesLineItem.getVatTax().isInclusive() == false)
			tax = tax
					+ (salesLineItem.getVatTax().getPercentage()
							* salesLineItem.getPrice() / 100);
		System.out.println("Returning Total "
				+ (salesLineItem.getPrice() + tax) * salesLineItem.getQty());
		return ((salesLineItem.getPrice() + tax) * salesLineItem.getQty());
	}

	public double totalServices() {
		double sum = 0;
		for (int i = 0; i < products.size(); i++)
			if (products.get(i).getNumberOfServices() != 0)
				sum = sum + products.get(i).getNumberOfServices();
		return sum;
	}

	public String largestduration() {
		int max = 0, duration = 0;

		for (SalesLineItem prod : products) {
			try {
				duration = prod.getDuration();
				if (duration > max) {
					max = duration;
				}
			} catch (Exception e) {

			}
		}
		return max + "";

	}

	public void addtaxparagraph() {
		Paragraph taxpara = new Paragraph();
		Phrase titleservicetax = new Phrase("Service Tax No     :", font8bold);
		Phrase titlevatatx = new Phrase("Vat Tax No :    ", font8bold);
		Phrase titlelbttax = new Phrase("LBT Tax No :    ", font8bold);
		Phrase space = new Phrase("									");
		String serv = null, lbt, vat = null;
		if (comp.getServiceTaxNo() == null)
			serv = "";
		else
			serv = comp.getServiceTaxNo();
		if (comp.getVatTaxNo() == null)
			vat = "";
		else
			vat = comp.getVatTaxNo();

		if (serv.equals("") == false) {
			taxpara.add(titleservicetax);
			Phrase servp = new Phrase(serv, font8);
			taxpara.add(servp);
		}
		taxpara.add(space);

		if (vat.equals("") == false) {
			taxpara.add("                ");
			taxpara.add(titlevatatx);
			Phrase vatpp = new Phrase(vat, font8);
			taxpara.add(vatpp);
		}

		try {
			taxpara.setSpacingAfter(10);
			document.add(taxpara);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void addType() {
		Phrase header = null;
		if (qp instanceof Contract)
			header = new Phrase("Contract", font12boldul);

		else
			header = new Phrase("Quotation", font12boldul);

		Paragraph para = new Paragraph(header);
		para.setAlignment(Element.ALIGN_CENTER);
		para.setSpacingBefore(10);
		para.setSpacingAfter(20);
		try {
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/*
	 * This method extracts unique products in a hash set form services array
	 * list we will create the returned list.
	 */

	public TreeSet<String> returnUniqueProducts() {
		TreeSet<String> productset = new TreeSet<String>(
				new ProductComparator());
		for (Service s : this.service) {
			productset.add(s.getProduct().getProductName());
		}

		return productset;
	}

	/*
	 * This method takes a product name and returns a vector of services for
	 * that product name.
	 */

	public HashMap<String, ArrayList<Service>> returnServicesWithRespectToProduct() {
		ArrayList<Service> alist = null;
		TreeSet<String> treeset = returnUniqueProducts();
		Iterator<String> prodname = treeset.iterator();
		HashMap<String, ArrayList<Service>> hashServices = new HashMap<String, ArrayList<Service>>();

		// Get the product name form iterator
		while (prodname.hasNext()) {
			String prodName = prodname.next();
			alist = new ArrayList<Service>();
			hashServices.put(prodName, alist);
			for (Service s : this.service) {
				if (prodName.trim().equals(s.getProduct().getProductName()))
					alist.add(s);

			}

			Collections.sort(alist, new ServiceComparator());
		}

		Set<Entry<String, ArrayList<Service>>> xy = hashServices.entrySet();
		Iterator<Entry<String, ArrayList<Service>>> it = xy.iterator();

		return hashServices;
	}

	public void addFooter() {

		Phrase blankphrase = new Phrase(" ");

		PdfPCell blankcell = new PdfPCell();
		blankcell.addElement(blankphrase);
		blankcell.setBorder(0);

		Phrase companyname = new Phrase("For " + comp.getBusinessUnitName(),
				font12bold);
		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companyname);
		companynamecell.setBorder(0);
		Phrase authorizedsignatory = new Phrase("AUTHORISED SIGNATORY" + "",
				font12boldul);
		PdfPCell authorisedcell = new PdfPCell();
		authorisedcell.addElement(authorizedsignatory);
		authorisedcell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(40);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(companynamecell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(blankcell);
		table.addCell(authorisedcell);
		table.setHorizontalAlignment(Element.ALIGN_RIGHT);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void addServiceDetail() {
		Phrase header = null;
		header = new Phrase("Service Details", font6bold);
		Paragraph para = new Paragraph(header);
		para.setAlignment(Element.ALIGN_LEFT);
		para.setSpacingBefore(20);
		para.setSpacingAfter(10);
		try {
			document.add(para);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// Create Services on the Fly with the help of Contract or Quotation item
	public void makeServices() {
		this.service = new ArrayList<Service>();
		Customer cust = null;
		Address addr = null;

		System.out.println("Inside Service Creation Code");
		for (SalesLineItem item : qp.getItems()) {
			if (item.getPrduct() instanceof ServiceProduct) {
				long noServices = (long) (item.getNumberOfServices() * item
						.getQty());
				int noOfdays = item.getDuration();
				int interval = (int) (noOfdays / item.getNumberOfServices());
				Date servicedate = item.getStartDate();
				Date d = new Date(servicedate.getTime());
				System.out.println("Interval is " + interval);
				System.out.println("Number of Services " + noServices);

				if (qp.getCinfo() != null) {
					if (qp.getCompanyId() != null) {
						cust = ofy().load().type(Customer.class)
								.filter("count", qp.getCinfo().getCount())
								.filter("companyId", qp.getCompanyId()).first()
								.now();
					} else {
						cust = ofy().load().type(Customer.class)
								.filter("count", qp.getCinfo().getCount())
								.first().now();
					}

				}
				// A patch as Neither Customer nor Contract has Customer Address
				if (cust != null)
					addr = cust.getAdress();

				for (int k = 0; k < noServices; k++) {
					Service temp = new Service();

					temp.setPersonInfo(qp.getCinfo());
					temp.setContractCount(qp.getCount());
					temp.setProduct((ServiceProduct) item.getPrduct());
					temp.setBranch(qp.getBranch());
					temp.setStatus(Service.SERVICESTATUSSCHEDULE);
					temp.setEmployee(qp.getEmployee());
					// temp.setContractStartDate(qp.getStartDate());
					// temp.setContractEndDate(qp.getEndDate());
					if (k == 0) {
						temp.setServiceDate(d);
					}

					if (item.getQty() > 1
							&& k % (noServices / item.getQty()) == 0) {
						System.out.println("K Value" + k);
						d = null;
						servicedate = item.getStartDate();
						d = new Date(servicedate.getTime());
					}

					if (k != 0) {
						temp.setServiceDate(d);
					}

					Calendar c = Calendar.getInstance();
					c.setTime(servicedate);
					c.add(Calendar.DATE, interval);
					servicedate = c.getTime();
					Date tempdate = new Date(servicedate.getTime());
					d = GenricServiceImpl.parseDate(tempdate);

					temp.setAddress(addr);
					service.add(temp);

					System.out.println("Created Service With Date "
							+ temp.getServiceDate());
				}
			}
		}
	}

	public void createDefaultText() {
		PdfPTable disclaimrtable = new PdfPTable(1);
		disclaimrtable.setWidthPercentage(100);

		PdfPTable companyinfotable = new PdfPTable(2);
		companyinfotable.setWidthPercentage(60);
		companyinfotable.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase discaimertextphrase = new Phrase(disclaimerText, font8);
		PdfPCell realtext = new PdfPCell(discaimertextphrase);
		realtext.setBorder(0);
		disclaimrtable.addCell(realtext);

		Phrase serviceTaxNr = new Phrase("Service Tax Nr  ", font8);
		Phrase vatTaxNr = new Phrase("VAT Registration Nr  ", font8);
		Phrase licenseNr = new Phrase("License Nr  ", font8);
		Phrase pancardTaxNr = new Phrase("PAN No  ", font8);

		PdfPCell serviceTaxCell = new PdfPCell(serviceTaxNr);
		serviceTaxCell.setBorder(0);
		PdfPCell vatRegNrCell = new PdfPCell(vatTaxNr);
		vatRegNrCell.setBorder(0);
		PdfPCell PANNoCell = new PdfPCell(pancardTaxNr);
		PANNoCell.setBorder(0);
		PdfPCell licenseNrCell = new PdfPCell(licenseNr);
		licenseNrCell.setBorder(0);

		Phrase realservicetax = new Phrase(comp.getServiceTaxNo() + "", font8);
		Phrase realvattax = new Phrase(comp.getVatTaxNo() + "", font8);
		Phrase realpanno = new Phrase(comp.getPanCard() + "", font8);
		Phrase reallicenseno = new Phrase(comp.getLicense() + "", font8);

		PdfPCell realserviceTaxCell = new PdfPCell(realservicetax);
		realserviceTaxCell.setBorder(0);
		PdfPCell realvatRegNrCell = new PdfPCell(realvattax);
		realvatRegNrCell.setBorder(0);
		PdfPCell realPANNoCell = new PdfPCell(realpanno);
		realPANNoCell.setBorder(0);
		PdfPCell reallicenseNrCell = new PdfPCell(reallicenseno);
		reallicenseNrCell.setBorder(0);

		if (comp.getServiceTaxNo() != null) {
			companyinfotable.addCell(serviceTaxCell);
			companyinfotable.addCell(realserviceTaxCell);
		}
		if (comp.getVatTaxNo() != null) {
			companyinfotable.addCell(vatRegNrCell);
			companyinfotable.addCell(realvatRegNrCell);
		}
		if (comp.getPanCard() != null) {
			companyinfotable.addCell(PANNoCell);
			companyinfotable.addCell(realPANNoCell);
		}
		if (comp.getLicense() != null) {
			companyinfotable.addCell(licenseNrCell);
			companyinfotable.addCell(reallicenseNrCell);
		}

		try {
			document.add(Chunk.NEWLINE);
			document.add(companyinfotable);
			document.add(disclaimrtable);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public double removeAllTaxes(SuperProduct entity) {
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;

		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			// Here if both are inclusive then first remove service tax and then
			// on that amount
			// calculate vat.
			double removeServiceTax = (entity.getPrice() / (1 + service / 100));

			// double taxPerc=service+vat;
			// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
			// below
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPrice() - retrServ;
		}
		tax = retrVat + retrServ;
		return tax;
	}

	public void settingRemainingRowsToPDF(int flag) {
		logger.log(Level.SEVERE, flag + "===============this is flag value");
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);

		PdfPTable table = new PdfPTable(10);
		table.setWidthPercentage(100);
		Phrase srno = new Phrase("SrNo", font1);
		Phrase product = new Phrase("Item Details", font1);
		Phrase qty = new Phrase("No of Branches", font1);
		Phrase startdate = new Phrase("Start Date", font1);
		Phrase duration = new Phrase("Duration(d)", font1);
		Phrase noservices = new Phrase("Services", font1);
		Phrase rate = new Phrase("Rate", font1);

		Phrase servicetax = null;
		Phrase servicetax1 = null;
		int both = 0;
		int vat = 0;
		int st = 0;
		for (int i = 0; i < products.size(); i++) {
			if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() == 0)) {
				servicetax = new Phrase("VAT(%)", font1);
				vat = vat + 1;
				System.out.println("phrase value====" + servicetax.toString());
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() == 0)) {
				servicetax = new Phrase("ST(%)", font1);
				st = st + 1;
			} else if ((products.get(i).getVatTax().getPercentage() > 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {
				servicetax1 = new Phrase("VAT / ST(%)", font1);
				both = both + 1;
				System.out.println("flag value;;;;;" + both);
			} else {

				servicetax = new Phrase("TAX(%)", font1);
			}
		}

		Phrase percdisctitle = new Phrase("% Disc", font1);
		Phrase total = new Phrase("Total", font1);

		System.out.println("vat===" + vat);
		System.out.println("st====" + st);
		System.out.println("flag===" + both);
		PdfPCell cellsrNo = new PdfPCell(srno);
		cellsrNo.setHorizontalAlignment(Element.ALIGN_CENTER);

		// PdfPCell cellcategory = new PdfPCell(category);
		// cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellstartdate = new PdfPCell(startdate);
		cellstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell servicecell = new PdfPCell(noservices);
		servicecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cellservicetax1 = null;
		PdfPCell cellservicetax = null;
		PdfPCell cellservicetax2 = null;
		Phrase servicetax2 = null;

		if (both > 0) {
			cellservicetax1 = new PdfPCell(servicetax1);
			cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else if (vat > 0 && st > 0) {
			System.out.println("in side condition");
			servicetax2 = new Phrase("VAT / ST(%)", font1);
			cellservicetax2 = new PdfPCell(servicetax2);
			cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
		} else {
			cellservicetax = new PdfPCell(servicetax);
			cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);

		}
		PdfPCell percdisccell = new PdfPCell(percdisctitle);
		percdisccell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(cellsrNo);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(cellstartdate);
		table.addCell(durationcell);
		table.addCell(servicecell);
		table.addCell(cellrate);

		if (both > 0) {
			table.addCell(cellservicetax1);
		} else if (vat > 0 && st > 0) {
			table.addCell(cellservicetax2);
		} else {
			table.addCell(cellservicetax);
		}
		table.addCell(percdisccell);
		table.addCell(celltotal);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		logger.log(Level.SEVERE, "flag value before printing elements" + flag);
		for (int i = flag; i < this.products.size(); i++) {
			logger.log(Level.SEVERE, "i =========" + i);

			System.out.println("iiiii-=----" + i);
			chunk = new Phrase((i + 1) + "", font8);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// chunk=new
			// Phrase(products.get(i).getPrduct().getCount()+"",font8);
			// PdfPCell pdfcategcell = new PdfPCell(chunk);

			chunk = new Phrase(products.get(i).getProductName(), font8);
			PdfPCell pdfnamecell = new PdfPCell(chunk);
			pdfnamecell.setColspan((int) 1.5);
			chunk = new Phrase(products.get(i).getQty() + "", font8);
			PdfPCell pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			PdfPCell pdfdatecell;
			PdfPCell pdfSerCell;

			String date = fmt.format(products.get(i).getStartDate());
			chunk = new Phrase(date, font8);
			pdfdatecell = new PdfPCell(chunk);
			pdfdatecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			chunk = new Phrase(products.get(i).getNumberOfServices() + "",
					font8);
			pdfSerCell = new PdfPCell(chunk);
			pdfSerCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			chunk = new Phrase(products.get(i).getDuration() + "", font8);
			PdfPCell pdfdurCell = new PdfPCell(chunk);
			pdfdurCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedPrice = products.get(i).getPrice() - taxVal;

			chunk = new Phrase(df.format(calculatedPrice), font8);
			PdfPCell pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			PdfPCell pdfservice = null;
			Phrase vatno = null;
			Phrase stno = null;
			if ((products.get(i).getVatTax().getPercentage() == 0)
					&& (products.get(i).getServiceTax().getPercentage() > 0)) {

				if (products.get(i).getServiceTax().getPercentage() != 0) {
					if (st == products.size()) {
						chunk = new Phrase(products.get(i).getServiceTax()
								.getPercentage()
								+ "", font8);
					} else {
						stno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage() + "", font8);
					}
				} else
					chunk = new Phrase("N.A" + "", font8);
				if (st == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(stno);
				}

				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() == 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {

				if (products.get(i).getVatTax() != null) {
					if (vat == products.size()) {
						System.out.println("rohan==" + vat);
						chunk = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ "", font8);
					} else {
						System.out.println("mukesh");
						vatno = new Phrase(products.get(i).getVatTax()
								.getPercentage()
								+ ""
								+ " / "
								+ products.get(i).getServiceTax()
										.getPercentage(), font8);
					}
				} else {
					chunk = new Phrase("N.A" + "", font8);
				}
				if (vat == products.size()) {
					pdfservice = new PdfPCell(chunk);
				} else {
					pdfservice = new PdfPCell(vatno);
				}
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			} else if ((products.get(i).getServiceTax().getPercentage() > 0)
					&& (products.get(i).getVatTax().getPercentage() > 0)) {

				if ((products.get(i).getVatTax() != null)
						&& (products.get(i).getServiceTax() != null)) {

					chunk = new Phrase(products.get(i).getVatTax()
							.getPercentage()
							+ ""
							+ " / "
							+ ""
							+ products.get(i).getServiceTax().getPercentage(),
							font8);
				} else {
					chunk = new Phrase("N.A" + "", font8);
					pdfservice = new PdfPCell(chunk);
					pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
				}
			} else {

				chunk = new Phrase("N.A" + "", font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);

			}

			if (products.get(i).getPercentageDiscount() != 0) {
				chunk = new Phrase(
						products.get(i).getPercentageDiscount() + "", font8);
			} else {
				chunk = new Phrase("0" + "", font8);
			}
			PdfPCell realpercdisccell = new PdfPCell(chunk);
			realpercdisccell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			realpercdisccell.setColspan((int) 0.5);

			double totalVal = (products.get(i).getPrice() - taxVal)
					* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal
						- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}

			chunk = new Phrase(df.format(totalVal), font8);
			PdfPCell pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			count = i;

			table.addCell(Pdfsrnocell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfdatecell);
			table.addCell(pdfdurCell);
			table.addCell(pdfSerCell);
			table.addCell(pdfspricecell);
			table.addCell(pdfservice);
			table.addCell(realpercdisccell);
			table.addCell(pdftotalproduct);

			System.out.println("flag value..............." + flag);
			flag = flag + 1;
			logger.log(Level.SEVERE, "flag value inside looop===" + flag);
			if (flag == 78 || flag == 139 || flag == 200) {
				break;
			}
		}

		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable parentTableProd = new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);

		PdfPCell prodtablecell = new PdfPCell();
		prodtablecell.addElement(table);
		parentTableProd.addCell(prodtablecell);
		parentTableProd.setSpacingBefore(5f);

		try {
			document.add(parentTableProd);

		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void otherchargestoAnnexur() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		String charge = "Other charge details";
		Chunk prodchunk = new Chunk(charge, font6bold);
		Paragraph parag = new Paragraph();
		parag.add(prodchunk);

		PdfPTable chargetable = new PdfPTable(2);
		chargetable.setWidthPercentage(100);
		// ********************
		double total = 0;
		for (int i = 0; i < this.prodCharges.size(); i++) {
			Phrase chunk = null;
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			if (prodCharges.get(i).getChargePercent() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + " @ "
						+ prodCharges.get(i).getChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargePercent()
						* prodCharges.get(i).getAssessableAmount() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);

			}
			if (prodCharges.get(i).getChargeAbsValue() != 0) {
				chunk = new Phrase(prodCharges.get(i).getChargeName() + "",
						font1);
				pdfchargecell = new PdfPCell(chunk);

				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				chargeAmt = prodCharges.get(i).getChargeAbsValue();
				chunk = new Phrase(chargeAmt + "", font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
			}
			pdfchargecell.setBorder(0);

			total = total + chargeAmt;
			chargetable.addCell(pdfchargecell);
			chargetable.addCell(pdfchargeamtcell);

		}

		Phrase totalchunk = new Phrase(
				"Total                                                                      "
						+ "                                  "
						+ df.format(total), font6bold);
		PdfPCell totalcell = new PdfPCell();
		totalcell.addElement(totalchunk);

		PdfPCell chargecell = new PdfPCell();
		chargecell.addElement(chargetable);
		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(70);
		parent.setSpacingBefore(10f);
		parent.addCell(chargecell);
		parent.addCell(totalcell);
		parent.setHorizontalAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(parag);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// rohan added this code for GST PDF of contract and quotation

	private void createHeader() {
		Phrase companyName = null;
		if (comp != null) {
			companyName = new Phrase(comp.getBusinessUnitName().trim(),
					font16boldul);
		}

		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(companyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyAddr = null;
		if (comp != null) {
			companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
					.trim(), font12);
		}
		Paragraph companyAddrpara = new Paragraph();
		companyAddrpara.add(companyAddr);
		companyAddrpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyAddrCell = new PdfPCell();
		companyAddrCell.addElement(companyAddrpara);
		companyAddrCell.setBorder(0);
		companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		if (qp instanceof Contract) {

		} else {

		}

		Phrase companyGSTTIN = null;
		String gstinValue = "";
		if (UniversalFlag) {
			if (qp.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
						break;
					}
				}

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (!comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = gstinValue
								+ ","
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
					}
				}
			}
		} else {

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
					break;
				}
			}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = gstinValue
							+ ","
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
				}
			}
		}

		if (!gstinValue.equals("")) {
			companyGSTTIN = new Phrase(gstinValue, font12bold);
		}

		Paragraph companyGSTTINpara = new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyGSTTINCell = new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pdfPTable = new PdfPTable(1);
		pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		pdfPTable.addCell(companyGSTTINCell);

		try {
			document.add(pdfPTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		// rohan added this code
		float[] myWidth = { 1, 3, 20, 17, 3, 30, 17, 3, 20, 1 };
		PdfPTable mytbale = new PdfPTable(10);
		mytbale.setWidthPercentage(100f);
		mytbale.setSpacingAfter(5f);
		mytbale.setSpacingBefore(5f);

		try {
			mytbale.setWidths(myWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase myblank = new Phrase(" ", font7);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * @author Vijay Date 21-11-2020
		 * Des :- below code is no use its bug so commeted the code. its showing like sales quoation
		 */
//		Phrase myblankborderZero = new Phrase(" ", font7);
//		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
//		// stat1PhraseCell.addElement(stat1Phrase);
//		myblankborderZeroCell.setBorder(0);
//		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stat1Phrase = new Phrase("Original for Receipient", font7);
//		PdfPCell stat1PhraseCell = new PdfPCell(stat1Phrase);
//		stat1PhraseCell.setBorder(0);
//		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stat2Phrase = new Phrase("Duplicate for Supplier/Transporter",
//				font7);
//		PdfPCell stat2PhraseCell = new PdfPCell(stat2Phrase);
//		stat2PhraseCell.setBorder(0);
//		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		Phrase stat3Phrase = new Phrase("Triplicate for Supplier", font7);
//		PdfPCell stat3PhraseCell = new PdfPCell(stat3Phrase);
//		stat3PhraseCell.setBorder(0);
//		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
//		mytbale.addCell(stat1PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
//		mytbale.addCell(stat2PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//		mytbale.addCell(myblankCell);
//		mytbale.addCell(stat3PhraseCell);
//		mytbale.addCell(myblankborderZeroCell);
//
//		PdfPTable tab = new PdfPTable(1);
//		tab.setWidthPercentage(100f);
//
//		PdfPCell cell = new PdfPCell(mytbale);
//		tab.addCell(cell);
//		try {
//			document.add(tab);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		/**
		 * ends here
		 */

		// ends here
		String titlepdf = "";

		if (qp instanceof Contract) {
			titlepdf = "Contract";
		} else {
			titlepdf = "Quotation";
		}

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createFooterTaxPart() {
		float[] columnMoreLeftWidths = { 2f, 1f };
		PdfPTable pdfPTaxTable = new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/**
		 * rohan added this code for payment terms for invoice details
		 */

		float[] column3widths = { 2f, 2f, 6f };
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		// heading

		Phrase day = new Phrase("Day", font8bold);
		PdfPCell dayCell = new PdfPCell(day);
		dayCell.setBorder(0);

		Phrase percent = new Phrase("Percent", font8bold);
		PdfPCell percentCell = new PdfPCell(percent);
		percentCell.setBorder(0);

		Phrase comment = new Phrase("Comment", font8bold);
		PdfPCell commentCell = new PdfPCell(comment);
		commentCell.setBorder(0);

		leftTable.addCell(dayCell);
		leftTable.addCell(percentCell);
		leftTable.addCell(commentCell);

		// Values
		for (int i = 0; i < qp.getPaymentTermsList().size(); i++) {
			Phrase dayValue = new Phrase(qp.getPaymentTermsList().get(i)
					.getPayTermDays()
					+ "", font8);
			PdfPCell dayValueCell = new PdfPCell(dayValue);
			dayValueCell.setBorder(0);
			leftTable.addCell(dayValueCell);

			Phrase percentValue = new Phrase(df.format(qp.getPaymentTermsList().get(i)
					.getPayTermPercent())
					+ "", font8);
			PdfPCell percentValueCell = new PdfPCell(percentValue);
			percentValueCell.setBorder(0);
			leftTable.addCell(percentValueCell);

			Phrase commentValue = new Phrase(qp.getPaymentTermsList().get(i)
					.getPayTermComment(), font8);
			PdfPCell commentValueCell = new PdfPCell(commentValue);
			commentValueCell.setBorder(0);
			leftTable.addCell(commentValueCell);
		}

		// try {
		// document.add(leftTable);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.3f };
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.addElement(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax", font6bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		// totalAmount
		Phrase amtB4TaxValphrase = new Phrase("", font6bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < qp.getProductTaxes().size(); i++) {
			if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			} else if (qp.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ qp.getProductTaxes().get(i).getChargePayable();
			}
		}

		Phrase CGSTphrase = new Phrase("CGST", font6bold);
		PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
		CGSTphraseCell.setBorder(0);
		// CGSTphraseCell.addElement(CGSTphrase);

		Phrase CGSTValphrase = new Phrase(cgstTotalVal + "", font7);
		// Paragraph CGSTValphrasePara=new Paragraph();
		// CGSTValphrasePara.add(CGSTValphrase);
		// CGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
		CGSTValphraseCell.setBorder(0);
		CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// CGSTValphraseCell.addElement(CGSTValphrasePara);

		Phrase SGSTphrase = new Phrase("SGST", font6bold);
		PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
		SGSTphraseCell.setBorder(0);
		// SGSTphraseCell.addElement(SGSTphrase);

		Phrase SGSTValphrase = new Phrase(df.format(sgstTotalVal) + "", font7);
		// Paragraph SGSTValphrasePara=new Paragraph();
		// SGSTValphrasePara.add(SGSTValphrase);
		// SGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
		SGSTValphraseCell.setBorder(0);
		// SGSTValphraseCell.addElement(SGSTValphrasePara);
		SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase IGSTphrase = new Phrase("IGST", font6bold);
		PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
		IGSTphraseCell.setBorder(0);
		// IGSTphraseCell.addElement(IGSTphrase);

		Phrase IGSTValphrase = new Phrase(df.format(igstTotalVal) + "", font7);
		// Paragraph IGSTValphrasePara=new Paragraph();
		// IGSTValphrasePara.add(IGSTValphrase);
		// IGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
		IGSTValphraseCell.setBorder(0);
		IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// IGSTValphraseCell.addElement(IGSTValphrasePara);

		Phrase GSTphrase = new Phrase("Total GST", font6bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		// GSTphraseCell.addElement(GSTphrase);

		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "", font6bold);
		// Paragraph GSTValphrasePara=new Paragraph();
		// GSTValphrasePara.add(GSTValphrase);
		// GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// GSTValphraseCell.addElement(GSTValphrasePara);

		rightInnerTable.addCell(CGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(CGSTValphraseCell);
		rightInnerTable.addCell(SGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(SGSTValphraseCell);
		rightInnerTable.addCell(IGSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(IGSTValphraseCell);
		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell();
		innerRightCell.setBorder(0);
		innerRightCell.addElement(rightInnerTable);

		rightTable.addCell(innerRightCell);

		PdfPCell rightCell = new PdfPCell();
		// rightCell.setBorder(0);
		rightCell.addElement(rightTable);

		PdfPCell leftCell = new PdfPCell();
		// leftCell.setBorder(0);
		leftCell.addElement(leftTable);

		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createFooterLastPart(String preprintStatus) {
		PdfPTable bottomTable = new PdfPTable(2);
		bottomTable.setWidthPercentage(100);
		float[] columnMoreLeftWidths = { 2f, 1f };
		try {
			bottomTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);
		String amtInWordsVal = "";
		String comment = "";
		double netpayable = 0;
		if (qp instanceof Contract) {
			Contract contractEntity = (Contract) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(contractEntity.getNetpayable());
			netpayable = contractEntity.getNetpayable();
			if (contractEntity.getDescription() != null) {
				comment = contractEntity.getDescription();
			}
		} else {
			Quotation quotation = (Quotation) qp;
			amtInWordsVal = "Amount in Words : Rupees "
					+ SalesInvoicePdf.convert(quotation.getNetpayable());
			netpayable = quotation.getNetpayable();
			if (quotation.getDescription() != null) {
				comment = quotation.getDescription();
			}
		}

		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font6bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtInWordsValCell.setBorderWidthTop(0);
		amtInWordsValCell.setBorderWidthLeft(0);
		amtInWordsValCell.setBorderWidthRight(0);
		leftTable.addCell(amtInWordsValCell);

		Phrase termNcond = new Phrase("Terms and Conditions:", font6bold);
		PdfPCell termNcondCell = new PdfPCell();
		termNcondCell.setBorder(0);
		termNcondCell.addElement(termNcond);

		Phrase termNcondVal = new Phrase(comment, font6bold);
		PdfPCell termNcondValCell = new PdfPCell();
		termNcondValCell.setBorder(0);
		termNcondValCell.addElement(termNcondVal);

		leftTable.addCell(termNcondCell);
		leftTable.addCell(termNcondValCell);

		// rohan added this code for universal pest
		if (UniversalFlag) {
			if (qp.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {
				if (!preprintStatus.equalsIgnoreCase("Plane")) {
					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

						if (comp.getArticleTypeDetails().get(i)
								.getArticlePrint().equalsIgnoreCase("Yes")) {

							Phrase articalType = new Phrase(comp
									.getArticleTypeDetails().get(i)
									.getArticleTypeName()
									+ " : "
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeValue(), font6bold);
							PdfPCell articalTypeCell = new PdfPCell();
							articalTypeCell.setBorder(0);
							articalTypeCell.addElement(articalType);
							leftTable.addCell(articalTypeCell);
						}
					}
				}
			}
		} else {
			if (!preprintStatus.equalsIgnoreCase("Plane")) {
				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

					if (comp.getArticleTypeDetails().get(i).getArticlePrint()
							.equalsIgnoreCase("Yes")) {

						Phrase articalType = new Phrase(comp
								.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue(), font6bold);
						PdfPCell articalTypeCell = new PdfPCell();
						articalTypeCell.setBorder(0);
						articalTypeCell.addElement(articalType);
						leftTable.addCell(articalTypeCell);
					}
				}
			}
		}
		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font6bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font6bold);
		PdfPCell blankCell = new PdfPCell();
		blankCell.setBorder(0);
		blankCell.addElement(blank);

		Phrase netPay = new Phrase("Net Payable", font6bold);

		PdfPCell netPayCell = new PdfPCell();
		netPayCell.setBorder(0);
		netPayCell.addElement(netPay);

		Phrase netPayVal = new Phrase(netpayable + "", font6bold);
		// Paragraph netPayPara=new Paragraph();
		// netPayPara.add(netPayVal);
		// netPayPara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorder(0);
		// netPayValCell.addElement(netPayPara);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase gstReverseCharge = new Phrase("GST Payable on Reverse Charge",
				font6bold);
		PdfPCell gstReverseChargeCell = new PdfPCell();
		gstReverseChargeCell.setBorder(0);
		gstReverseChargeCell.addElement(gstReverseCharge);

		Phrase gstReverseChargeVal = new Phrase(" ", font7);
		PdfPCell gstReverseChargeValCell = new PdfPCell();
		gstReverseChargeValCell.setBorder(0);
		gstReverseChargeValCell.addElement(gstReverseChargeVal);

		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);
		innerRightTable.addCell(blankCell);
		innerRightTable.addCell(blankCell);
		innerRightTable.addCell(blankCell);
		innerRightTable.addCell(gstReverseChargeCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(gstReverseChargeValCell);

		PdfPCell rightUpperCell = new PdfPCell();
		rightUpperCell.addElement(innerRightTable);
		rightUpperCell.setBorderWidthLeft(0);
		rightUpperCell.setBorderWidthRight(0);
		rightUpperCell.setBorderWidthTop(0);

		// rohan added this code for universal pest and printing multiple
		// company name

		String companyname = "";
		if (multipleCompanyName) {
			if (qp.getGroup() != null && !qp.getGroup().equals("")) {
				companyname = qp.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}

		} else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		// ends here

		rightTable.addCell(rightUpperCell);
		Phrase companyPhrase = new Phrase("For , " + companyname, font6bold);
		Paragraph companyPara = new Paragraph();
		companyPara.add(companyPhrase);
		companyPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell companyParaCell = new PdfPCell();
		companyParaCell.addElement(companyPara);
		companyParaCell.setBorder(0);

		rightTable.addCell(companyParaCell);
		rightTable.addCell(blankCell);
		rightTable.addCell(blankCell);
		rightTable.addCell(blankCell);
		Phrase signAuth = new Phrase("Authorised Signatory", font6bold);
		Paragraph signPara = new Paragraph();
		signPara.add(signAuth);
		signPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell signParaCell = new PdfPCell();
		signParaCell.addElement(signPara);
		signParaCell.setBorder(0);
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell();
		lefttableCell.addElement(leftTable);
		PdfPCell righttableCell = new PdfPCell();
		righttableCell.addElement(rightTable);

		bottomTable.addCell(lefttableCell);
		bottomTable.addCell(righttableCell);

		//

		Paragraph para = new Paragraph(
				"Note : This is computer generated hence no signature required.",
				font8);

		//
		try {
			document.add(bottomTable);
			document.add(para);
			if (qp instanceof Contract) {
				if (contEntity.getItems().size() > 5) {
					int firstBreakPoint = 5;
					if(printModelSerailNoFlag && printPremiseDetails){
						createAnnexureForRemainingContractProduct(4);
					}else{
						createAnnexureForRemainingContractProduct(5);
					}
					
				}
			} else {
				if (quotEntity.getItems().size() > 5) {
					createAnnexureForRemainingQuoatationProduct(5);
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createAnnexureForRemainingQuoatationProduct(int i) {
		// TODO Auto-generated method stub

		Paragraph para = new Paragraph("Annexure 1 :", font10bold);
		para.setAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(Chunk.NEWLINE);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		createProductDetails();
		
		createProductDetailsMOreThanFive4Quotation(5);
	}

	private void createProductDetailsMOreThanFive4Quotation(int count) {
		System.out.println("Inside quotation ");
		System.out.println("Else Condn of Instance con and quot ");
		// PdfPTable premiseTbl =new PdfPTable(1);
		

		for (int i = count; i < quotEntity.getItems().size(); i++) {
			logger.log(Level.SEVERE, "Inside Items " + i);

			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font8);
			PdfPCell srNoCell = new PdfPCell(srNo);
			// srNoCell.setBorderWidthBottom(0);
			// srNoCell.setBorderWidthTop(0);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(quotEntity.getItems().get(i)
					.getProductName().trim(), font8);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.setBorder(0);
			// serviceNameCell.setBorderWidthBottom(0);
			// serviceNameCell.setBorderWidthTop(0);
			serviceNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			serviceNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;

			if (quotEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
				hsnCode = new Phrase(quotEntity.getItems().get(i).getPrduct()
						.getHsnNumber().trim(), font8);
			} else {
				hsnCode = new Phrase("", font8);
			}
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			// hsnCodeCell.setBorder(0);
			// hsnCodeCell.setBorderWidthBottom(0);
			// hsnCodeCell.setBorderWidthTop(0);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			hsnCodeCell.addElement(hsnCode);
			productTable.addCell(hsnCodeCell);

			Phrase uom = new Phrase(quotEntity.getItems().get(i)
					.getUnitOfMeasurement().trim(), font8);
			PdfPCell uomCell = new PdfPCell(uom);
			// uomCell.setBorder(0);
			// uomCell.setBorderWidthBottom(0);
			// uomCell.setBorderWidthTop(0);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			uomCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(uomCell);

			Phrase qty = new Phrase(quotEntity.getItems().get(i).getQty() + "",
					font8);
			PdfPCell qtyCell = new PdfPCell(qty);
			// qtyCell.setBorder(0);
			// qtyCell.setBorderWidthBottom(0);
			// qtyCell.setBorderWidthTop(0);
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			qtyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(quotEntity.getItems().get(i)
					.getPrice())
					+ "", font8);
			PdfPCell rateCell = new PdfPCell(rate);
			// rateCell.setBorder(0);
			// rateCell.setBorderWidthBottom(0);
			// rateCell.setBorderWidthTop(0);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			rateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(rateCell);

			double amountValue = quotEntity.getItems().get(i).getPrice()
					* quotEntity.getItems().get(i).getQty();

			totalAmount = totalAmount + amountValue;
			Phrase amount = new Phrase(df.format(amountValue) + "", font8);
			PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(quotEntity.getItems().get(i)
					.getDiscountAmt())
					+ "", font8);
			PdfPCell discCell = new PdfPCell(disc);
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(discCell);

			Phrase taxableValue = new Phrase(df.format(getAssessTotalAmount(quotEntity.getItems().get(i)))
					+ "", font8);
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			// taxableValueCell.setBorder(0);
			// taxableValueCell.setBorderWidthBottom(0);
			// taxableValueCell.setBorderWidthTop(0);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			taxableValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(taxableValueCell);

			// PdfPCell cellIGST;

			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			boolean taxPresent=validateTaxes(contEntity.getItems().get(i));
			if(taxPresent){
			if (quotEntity.getItems().get(i).getVatTax().getTaxPrintName()
					.equalsIgnoreCase("IGST")) {
				double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
				double taxAmount = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i).getVatTax()
						.getPercentage());
				double indivTotalAmount = asstotalAmount
						+ taxAmount;
				totalAmount = totalAmount + indivTotalAmount;

				Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
						font8);
				PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
				igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				Phrase igstRate = new Phrase(quotEntity.getItems().get(i)
						.getVatTax().getPercentage()
						+ "", font8);
				PdfPCell igstRateCell = new PdfPCell(igstRate);
				igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				/* for Cgst */

				Phrase cgst = new Phrase("-", font8);
				PdfPCell cell = new PdfPCell(cgst);
				// cell.addElement(cgst);
				// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(cell);
				productTable.addCell(cell);
				/* for Sgst */
				Phrase sgst = new Phrase("-", font8);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(igstRateCell);
				productTable.addCell(igstRateValCell);

				Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
						+ "", font8);
				PdfPCell totalCell = new PdfPCell(totalPhrase);
				// totalCell.setBorder(0);
				totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				productTable.addCell(totalCell);

				String premisesVal = "";
				premisesVal = quotEntity.getItems().get(i).getPremisesDetails();
				if (printPremiseDetails) {
					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					PdfPCell premiseCell = new PdfPCell();
					premiseCell.setColspan(16);
					premiseCell.addElement(premisesValPhrs);
					productTable.addCell(premiseCell);
				}
				try {
					document.add(productTable);
					// document.add(productTableWithOutPremise);

				} catch (DocumentException e) {
					e.printStackTrace();
					logger.log(Level.SEVERE, "ERROR Adding in Table!!!!" + e);
				}
			}else if(quotEntity.getItems().get(i).getServiceTax()
					.getTaxPrintName().equalsIgnoreCase("IGST")){

				double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
				double taxAmount = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i).getServiceTax()
						.getPercentage());
				double indivTotalAmount = asstotalAmount
						+ taxAmount;
				totalAmount = totalAmount + indivTotalAmount;

				Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
						font8);
				PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
				igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				Phrase igstRate = new Phrase(quotEntity.getItems().get(i)
						.getServiceTax().getPercentage()
						+ "", font8);
				PdfPCell igstRateCell = new PdfPCell(igstRate);
				igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				/* for Cgst */

				Phrase cgst = new Phrase("-", font8);
				PdfPCell cell = new PdfPCell(cgst);
				// cell.addElement(cgst);
				// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(cell);
				productTable.addCell(cell);
				/* for Sgst */
				Phrase sgst = new Phrase("-", font8);
				productTable.addCell(cell);
				productTable.addCell(cell);
				productTable.addCell(igstRateCell);
				productTable.addCell(igstRateValCell);

				Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
						+ "", font8);
				PdfPCell totalCell = new PdfPCell(totalPhrase);
				// totalCell.setBorder(0);
				totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				// totalCell.setBorderWidthBottom(0);
				// totalCell.setBorderWidthTop(0);
				productTable.addCell(totalCell);

				String premisesVal = "";
				premisesVal = quotEntity.getItems().get(i).getPremisesDetails();
				if (printPremiseDetails) {
					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					PdfPCell premiseCell = new PdfPCell();
					premiseCell.setColspan(16);
					premiseCell.addElement(premisesValPhrs);
					productTable.addCell(premiseCell);
				}
				try {
					document.add(productTable);
					// document.add(productTableWithOutPremise);

				} catch (DocumentException e) {
					e.printStackTrace();
					logger.log(Level.SEVERE, "ERROR Adding in Table!!!!" + e);
				}
			
			} else {

				if (quotEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("CGST")) {
					double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
					double ctaxValue = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i)
							.getVatTax().getPercentage());

					Phrase cgstRateVal = new Phrase(df.format(ctaxValue) + "",
							font8);
					PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
					cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase cgstRate = new Phrase(quotEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell cgstRateCell = new PdfPCell(cgstRate);
					cgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cgstRateCell);
					productTable.addCell(cgstRateValCell);

					double staxValue = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i)
							.getServiceTax().getPercentage());
					Phrase sgstRateVal = new Phrase(df.format(staxValue) + "",
							font8);
					PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
					sgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					sgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase sgstRate = new Phrase(quotEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell sgstRateCell = new PdfPCell(sgstRate);
					// sgstRateCell.setBorder(0);
					// sgstRateCell.setBorderWidthBottom(0);
					// sgstRateCell.setBorderWidthTop(0);
					// sgstRateCell.setBorderWidthLeft(0);
					sgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(sgstRateCell);
					productTable.addCell(sgstRateValCell);

					Phrase igstRateVal = new Phrase("-", font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateValCell);

					Phrase igstRate = new Phrase("-", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateCell);

					double indivTotalAmount = asstotalAmount
							+ ctaxValue + staxValue;
					totalAmount = totalAmount + indivTotalAmount;
					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = quotEntity.getItems().get(i)
							.getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase(
								"Premise Details : " + premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					try {
						document.add(productTable);
						// document.add(productTableWithOutPremise);

					} catch (DocumentException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "ERROR Adding in Table!!!!"
								+ e);
					}
				} else if (quotEntity.getItems().get(i).getVatTax()
						.getTaxPrintName().equalsIgnoreCase("SGST")) {
					double asstotalAmount=getAssessTotalAmount(quotEntity.getItems().get(i));
					double ctaxValue = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i)
							.getServiceTax().getPercentage());

					Phrase cgstRateVal = new Phrase(df.format(ctaxValue) + "",
							font8);
					PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
					cgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

					Phrase cgstRate = new Phrase(quotEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell cgstRateCell = new PdfPCell(cgstRate);
					// cgstRateCell.setBorder(0);
					// cgstRateCell.setBorderWidthBottom(0);
					// cgstRateCell.setBorderWidthTop(0);
					// cgstRateCell.setBorderWidthLeft(0);
					cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					productTable.addCell(cgstRateCell);
					productTable.addCell(cgstRateValCell);

					double staxValue = getTaxAmount(asstotalAmount, quotEntity.getItems().get(i)
							.getVatTax().getPercentage());
					Phrase sgstRateVal = new Phrase(df.format(staxValue) + "",
							font8);
					PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
					// sgstRateValCell.setBorder(0);
					// sgstRateValCell.setBorderWidthBottom(0);
					// sgstRateValCell.setBorderWidthTop(0);
					// sgstRateValCell.setBorderWidthRight(0);
					sgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					sgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

					Phrase sgstRate = new Phrase(quotEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell sgstRateCell = new PdfPCell(sgstRate);
					// sgstRateCell.setBorder(0);
					// sgstRateCell.setBorderWidthBottom(0);
					// sgstRateCell.setBorderWidthTop(0);
					// sgstRateCell.setBorderWidthLeft(0);
					sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					sgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					productTable.addCell(sgstRateCell);
					productTable.addCell(sgstRateValCell);

					Phrase igstRateVal = new Phrase("-", font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					// igstRateValCell.setBorder(0);
					// igstRateValCell.setBorderWidthBottom(0);
					// igstRateValCell.setBorderWidthTop(0);
					// igstRateValCell.setBorderWidthRight(0);
					// igstRateValCell.addElement(igstRateVal);
					igstRateValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateValCell);

					Phrase igstRate = new Phrase("-", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					// igstRateCell.setBorder(0);
					// igstRateCell.setBorderWidthBottom(0);
					// igstRateCell.setBorderWidthTop(0);
					// igstRateCell.setBorderWidthLeft(0);
					// igstRateCell.addElement(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateCell);

					double indivTotalAmount = asstotalAmount
							+ ctaxValue + staxValue;
					totalAmount = totalAmount + indivTotalAmount;
					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);

					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = quotEntity.getItems().get(i)
							.getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase(
								"Premise Details : " + premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					try {
						document.add(productTable);
						// document.add(productTableWithOutPremise);

					} catch (DocumentException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "ERROR Adding in Table!!!!"
								+ e);
					}
				}

			}
		}else{


			logger.log(Level.SEVERE,"Inside Tax Not Applicable");

			PdfPCell cell = new PdfPCell(new Phrase("-", font8));
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			Phrase totalPhrase = new Phrase(df.format(getAssessTotalAmount(quotEntity.getItems().get(i)))
					+ "", font8);
			PdfPCell totalCell = new PdfPCell();
			// totalCell.setColspan(16);
			// totalCell.setBorder(0);
			// totalCell.setBorderWidthBottom(0);
			// totalCell.setBorderWidthTop(0);
			totalCell.addElement(totalPhrase);
			productTable.addCell(totalCell);

			String premisesVal = "";
			if(contEntity.getItems().get(i).getPremisesDetails()!=null){
				premisesVal=contEntity.getItems().get(i).getPremisesDetails();
			}
			
			if (printPremiseDetails) {
				Phrase premisesValPhrs = new Phrase("Premise Details : "
						+ premisesVal, font8);
				PdfPCell premiseCell = new PdfPCell();
				premiseCell.setColspan(16);
				premiseCell.addElement(premisesValPhrs);
				productTable.addCell(premiseCell);
			}
		
		}}
	}

	private void createAnnexureForRemainingContractProduct(int i) {

		Paragraph para = new Paragraph("Annexure 1 :", font10bold);
		para.setAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(Chunk.NEWLINE);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		createProductDetails();
		if(printModelSerailNoFlag && printPremiseDetails){
			createAnnexureForRemainingContractProduct(4);
		}else{
			createProductDetailsMOreThanFive4Contract(5);
		}
		
	}

	private void createProductDetailsMOreThanFive4Contract(int count) {
		System.out.println("Inside Instance con");
		System.out.println("Inside  Condn of Instance con and quot ");

		for (int i = count; i < contEntity.getItems().size(); i++) {
			logger.log(Level.SEVERE,"i value::"+i);
			PdfPTable productTable = new PdfPTable(16);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column16CollonWidth);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font8);
			PdfPCell srNoCell = new PdfPCell();
			// srNoCell.setBorderWidthBottom(0);
			// srNoCell.setBorderWidthTop(0);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			srNoCell.addElement(srNo);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(contEntity.getItems().get(i)
					.getProductName().trim(), font8);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.setBorder(0);
			// serviceNameCell.setBorderWidthBottom(0);
			// serviceNameCell.setBorderWidthTop(0);
			serviceNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			serviceNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(serviceNameCell);

			Phrase hsnCode = null;

			if (contEntity.getItems().get(i).getPrduct().getHsnNumber() != null) {
				hsnCode = new Phrase(contEntity.getItems().get(i).getPrduct()
						.getHsnNumber().trim(), font8);
			} else {
				hsnCode = new Phrase("", font8);
			}
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			// hsnCodeCell.setBorder(0);
			// hsnCodeCell.setBorderWidthBottom(0);
			// hsnCodeCell.setBorderWidthTop(0);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(hsnCodeCell);

			Phrase uom = new Phrase(contEntity.getItems().get(i)
					.getUnitOfMeasurement().trim(), font8);
			PdfPCell uomCell = new PdfPCell(uom);
			// uomCell.setBorder(0);
			// uomCell.setBorderWidthBottom(0);
			// uomCell.setBorderWidthTop(0);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			uomCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(uomCell);

			Phrase qty = new Phrase(contEntity.getItems().get(i).getQty() + "",
					font8);
			PdfPCell qtyCell = new PdfPCell(qty);
			// qtyCell.setBorder(0);
			// qtyCell.setBorderWidthBottom(0);
			// qtyCell.setBorderWidthTop(0);
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			qtyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(contEntity.getItems().get(i)
					.getPrice())
					+ "", font8);
			PdfPCell rateCell = new PdfPCell(rate);
			// rateCell.setBorder(0);
			// rateCell.setBorderWidthBottom(0);
			// rateCell.setBorderWidthTop(0);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			rateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(rateCell);

			double amountValue = contEntity.getItems().get(i).getPrice()
					* contEntity.getItems().get(i).getQty();

			totalAmount = totalAmount + amountValue;
			Phrase amount = new Phrase(df.format(amountValue) + "", font8);
			PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			amountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(contEntity.getItems().get(i)
					.getDiscountAmt())
					+ "", font8);
			PdfPCell discCell = new PdfPCell(disc);
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(discCell);

			Phrase taxableValue = new Phrase(df.format(getAssessTotalAmount(contEntity.getItems().get(i)))
					+ "", font8);
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			// taxableValueCell.setBorder(0);
			// taxableValueCell.setBorderWidthBottom(0);
			// taxableValueCell.setBorderWidthTop(0);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			taxableValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;
			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());

			boolean taxPresent=validateTaxes(contEntity.getItems().get(i));
			if(taxPresent){
			if (contEntity.getItems().get(i).getVatTax().getTaxPrintName()
					.equalsIgnoreCase("IGST")) {
				double assTotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
				double taxAmount = getTaxAmount(assTotalAmount, contEntity.getItems().get(i).getVatTax()
						.getPercentage());
				double indivTotalAmount = assTotalAmount
						+ taxAmount;
				totalAmount = totalAmount + indivTotalAmount;

				Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
						font8);
				PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
				// igstRateValCell.setBorder(0);
				igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				Phrase igstRate = new Phrase(contEntity.getItems().get(i)
						.getVatTax().getPercentage()
						+ "", font8);
				PdfPCell igstRateCell = new PdfPCell(igstRate);
				// igstRateCell.setBorder(0);
				// igstRateCell.setColspan(2);
				igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				/* for Cgst */

				Phrase cgst = new Phrase("-", font8);
				PdfPCell cell = new PdfPCell(cgst);
				// cell.addElement(cgst);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(cell);
				productTable.addCell(cell);

				/* for Sgst */
				Phrase sgst = new Phrase("-", font8);
				productTable.addCell(cell);
				productTable.addCell(cell);

				productTable.addCell(igstRateCell);
				productTable.addCell(igstRateValCell);

				Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
						+ "", font8);
				PdfPCell totalCell = new PdfPCell(totalPhrase);
				totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(totalCell);

				String premisesVal = "";
				premisesVal = contEntity.getItems().get(i).getPremisesDetails();
				if (printPremiseDetails) {
					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					PdfPCell premiseCell = new PdfPCell();
					premiseCell.setColspan(16);
					premiseCell.addElement(premisesValPhrs);
					productTable.addCell(premiseCell);
				}
				
				/**
				 * nidhi
				 * for print model and serial number
				 */
				int cnnt = 0;
				PdfPCell proModelcell = null ,proSerialNocell = null; 
				String proModelNo = "";
				String proSerialNo = "";
				if(printModelSerailNoFlag){
//					String proModelNo = "";
					if(contEntity.getItems().get(i).getProModelNo()!=null && 
						contEntity.getItems().get(i).getProModelNo().trim().length() >0){
						proModelNo = contEntity.getItems().get(i).getProModelNo();
					}
					
//					String proSerialNo = "";
					if(contEntity.getItems().get(i).getProSerialNo()!=null && 
						contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
						proSerialNo = contEntity.getItems().get(i).getProSerialNo();
					}
					
					if(proModelNo.length()>0){
						Phrase modelValPhrs = new Phrase(
								"Model No : " + proModelNo, font8);
						proModelcell = new PdfPCell();
						proModelcell.setColspan(8);
						proModelcell.addElement(modelValPhrs);
//						productTable.addCell(premiseCell);
						++cnnt;
					}
					if(proSerialNo.length()>0){
						Phrase serialValPhrs = new Phrase(
								"Serial No : " + proSerialNo, font8);
						proSerialNocell = new PdfPCell();
						proSerialNocell.setColspan(8);
						proSerialNocell.addElement(serialValPhrs);
//						productTable.addCell(premiseCell);
						++cnnt;
					}
					
					if(cnnt>1 ){
						proSerialNocell.setColspan(8);
					}else if(proModelNo.length()>0){
						proModelcell.setColspan(16);
					}else {
						proSerialNocell.setColspan(16);
					}
					
					if(cnnt>0);
					{
//						noOfLines = noOfLines-1;
//						table1.addCell(Pdfsrnocell2);
						if(proModelcell!=null){
							productTable.addCell(proModelcell);
						}
						if(proSerialNocell!=null){
							productTable.addCell(proSerialNocell);
						}
					}
				}
				/**
				 * end
				 */
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}else if(contEntity.getItems().get(i).getServiceTax().getTaxPrintName()
					.equalsIgnoreCase("IGST")){

				double assTotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
				double taxAmount = getTaxAmount(assTotalAmount, contEntity.getItems().get(i).getServiceTax()
						.getPercentage());
				double indivTotalAmount = assTotalAmount
						+ taxAmount;
				totalAmount = totalAmount + indivTotalAmount;

				Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
						font8);
				PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
				// igstRateValCell.setBorder(0);
				igstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				Phrase igstRate = new Phrase(contEntity.getItems().get(i)
						.getVatTax().getPercentage()
						+ "", font8);
				PdfPCell igstRateCell = new PdfPCell(igstRate);
				// igstRateCell.setBorder(0);
				// igstRateCell.setColspan(2);
				igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				/* for Cgst */

				Phrase cgst = new Phrase("-", font8);
				PdfPCell cell = new PdfPCell(cgst);
				// cell.addElement(cgst);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(cell);
				productTable.addCell(cell);

				/* for Sgst */
				Phrase sgst = new Phrase("-", font8);
				productTable.addCell(cell);
				productTable.addCell(cell);

				productTable.addCell(igstRateCell);
				productTable.addCell(igstRateValCell);

				Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
						+ "", font8);
				PdfPCell totalCell = new PdfPCell(totalPhrase);
				totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				productTable.addCell(totalCell);

				String premisesVal = "";
				premisesVal = contEntity.getItems().get(i).getPremisesDetails();
				if (printPremiseDetails) {
					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					PdfPCell premiseCell = new PdfPCell();
					premiseCell.setColspan(16);
					premiseCell.addElement(premisesValPhrs);
					productTable.addCell(premiseCell);
				}
				/**
				 * nidhi
				 * for print model and serial number
				 */
				int cnnt = 0;
				PdfPCell proModelcell = null ,proSerialNocell = null; 
				String proModelNo = "";
				String proSerialNo = "";
				if(printModelSerailNoFlag){
//					String proModelNo = "";
					if(contEntity.getItems().get(i).getProModelNo()!=null && 
						contEntity.getItems().get(i).getProModelNo().trim().length() >0){
						proModelNo = contEntity.getItems().get(i).getProModelNo();
					}
					
//					String proSerialNo = "";
					if(contEntity.getItems().get(i).getProSerialNo()!=null && 
						contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
						proSerialNo = contEntity.getItems().get(i).getProSerialNo();
					}
					
					if(proModelNo.length()>0){
						Phrase modelValPhrs = new Phrase(
								"Model No : " + proModelNo, font8);
						proModelcell = new PdfPCell();
						proModelcell.setColspan(8);
						proModelcell.addElement(modelValPhrs);
//						productTable.addCell(premiseCell);
						++cnnt;
					}
					if(proSerialNo.length()>0){
						Phrase serialValPhrs = new Phrase(
								"Serial No : " + proSerialNo, font8);
						proSerialNocell = new PdfPCell();
						proSerialNocell.setColspan(8);
						proSerialNocell.addElement(serialValPhrs);
//						productTable.addCell(premiseCell);
						++cnnt;
					}
					
					if(cnnt>1 ){
						proSerialNocell.setColspan(8);
					}else if(proModelNo.length()>0){
						proModelcell.setColspan(16);
					}else {
						proSerialNocell.setColspan(16);
					}
					
					if(cnnt>0);
					{
//						noOfLines = noOfLines-1;
//						table1.addCell(Pdfsrnocell2);
						if(proModelcell!=null){
							productTable.addCell(proModelcell);
						}
						if(proSerialNocell!=null){
							productTable.addCell(proSerialNocell);
						}
					}
				}
				/**
				 * end
				 */
				try {
					document.add(productTable);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			
			
			}else {
			}

				if (contEntity.getItems().get(i).getVatTax().getTaxPrintName()
						.equalsIgnoreCase("CGST")) {

					double assTotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
					double ctaxValue = getTaxAmount(assTotalAmount, contEntity.getItems().get(i)
							.getVatTax().getPercentage());

					Phrase cgstRateVal = new Phrase(df.format(ctaxValue) + "",
							font8);
					PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
					// cgstRateValCell.setBorder(0);
					// cgstRateValCell.setBorderWidthBottom(0);
					// cgstRateValCell.setBorderWidthTop(0);
					// cgstRateValCell.setBorderWidthRight(0);
					cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase cgstRate = new Phrase(contEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell cgstRateCell = new PdfPCell(cgstRate);
					// cgstRateCell.setBorder(0);
					// cgstRateCell.setBorderWidthBottom(0);
					// cgstRateCell.setBorderWidthTop(0);
					// cgstRateCell.setBorderWidthLeft(0);
					cgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cgstRateCell);
					productTable.addCell(cgstRateValCell);

					double staxValue = getTaxAmount(assTotalAmount, contEntity.getItems().get(i)
							.getServiceTax().getPercentage());
					Phrase sgstRateVal = new Phrase(df.format(staxValue) + "",
							font8);
					PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
					sgstRateValCell.setBorder(0);
					// sgstRateValCell.setBorderWidthBottom(0);
					// sgstRateValCell.setBorderWidthTop(0);
					// sgstRateValCell.setBorderWidthRight(0);
					sgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					sgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase sgstRate = new Phrase(contEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell sgstRateCell = new PdfPCell(sgstRate);
					// sgstRateCell.setBorder(0);
					// sgstRateCell.setBorderWidthBottom(0);
					// sgstRateCell.setBorderWidthTop(0);
					// sgstRateCell.setBorderWidthLeft(0);
					sgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(sgstRateCell);
					productTable.addCell(sgstRateValCell);

					Phrase igstRateVal = new Phrase("-", font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					// igstRateValCell.setBorder(0);
					// igstRateValCell.setBorderWidthBottom(0);
					// igstRateValCell.setBorderWidthTop(0);
					// igstRateValCell.setBorderWidthRight(0);
					// igstRateValCell.addElement(igstRateVal);

					igstRateValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateValCell);

					Phrase igstRate = new Phrase("-", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					// igstRateCell.setBorder(0);
					// igstRateCell.setBorderWidthBottom(0);
					// igstRateCell.setBorderWidthTop(0);
					// igstRateCell.setBorderWidthLeft(0);
					// igstRateCell.addElement(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateCell);

					double indivTotalAmount = assTotalAmount
							+ ctaxValue + staxValue;
					totalAmount = totalAmount + indivTotalAmount;
					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);
					// totalCell.setBorder(0);
					// totalCell.setBorderWidthBottom(0);
					// totalCell.setBorderWidthTop(0);
					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = contEntity.getItems().get(i)
							.getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase(
								"Premise Details : " + premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					
					/**
					 * nidhi
					 * for print model and serial number
					 */
					int cnnt = 0;
					PdfPCell proModelcell = null ,proSerialNocell = null; 
					String proModelNo = "";
					String proSerialNo = "";
					if(printModelSerailNoFlag){
//						String proModelNo = "";
						if(contEntity.getItems().get(i).getProModelNo()!=null && 
							contEntity.getItems().get(i).getProModelNo().trim().length() >0){
							proModelNo = contEntity.getItems().get(i).getProModelNo();
						}
						
//						String proSerialNo = "";
						if(contEntity.getItems().get(i).getProSerialNo()!=null && 
							contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
							proSerialNo = contEntity.getItems().get(i).getProSerialNo();
						}
						
						if(proModelNo.length()>0){
							Phrase modelValPhrs = new Phrase(
									"Model No : " + proModelNo, font8);
							proModelcell = new PdfPCell();
							proModelcell.setColspan(8);
							proModelcell.addElement(modelValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						if(proSerialNo.length()>0){
							Phrase serialValPhrs = new Phrase(
									"Serial No : " + proSerialNo, font8);
							proSerialNocell = new PdfPCell();
							proSerialNocell.setColspan(8);
							proSerialNocell.addElement(serialValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						
						if(cnnt>1 ){
							proSerialNocell.setColspan(8);
						}else if(proModelNo.length()>0){
							proModelcell.setColspan(16);
						}else {
							proSerialNocell.setColspan(16);
						}
						
						if(cnnt>0);
						{
//							noOfLines = noOfLines-1;
//							table1.addCell(Pdfsrnocell2);
							if(proModelcell!=null){
								productTable.addCell(proModelcell);
							}
							if(proSerialNocell!=null){
								productTable.addCell(proSerialNocell);
							}
						}
					}
					/**
					 * end
					 */
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				} else if (contEntity.getItems().get(i).getVatTax()
						.getTaxPrintName().equalsIgnoreCase("SGST")) {
					double assTotalAmount=getAssessTotalAmount(contEntity.getItems().get(i));
					
					double ctaxValue = getTaxAmount(assTotalAmount, contEntity.getItems().get(i)
							.getServiceTax().getPercentage());

					Phrase cgstRateVal = new Phrase(df.format(ctaxValue) + "",
							font8);
					PdfPCell cgstRateValCell = new PdfPCell(cgstRateVal);
					cgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase cgstRate = new Phrase(contEntity.getItems().get(i)
							.getVatTax().getPercentage()
							+ "", font8);
					PdfPCell cgstRateCell = new PdfPCell(cgstRate);
					cgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(cgstRateCell);
					productTable.addCell(cgstRateValCell);

					double staxValue = getTaxAmount(assTotalAmount, contEntity.getItems().get(i)
							.getVatTax().getPercentage());
					Phrase sgstRateVal = new Phrase(df.format(staxValue) + "",
							font8);
					PdfPCell sgstRateValCell = new PdfPCell(sgstRateVal);
					sgstRateValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					sgstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

					Phrase sgstRate = new Phrase(contEntity.getItems().get(i)
							.getServiceTax().getPercentage()
							+ "", font8);
					PdfPCell sgstRateCell = new PdfPCell(sgstRate);
					sgstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					sgstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(sgstRateCell);
					productTable.addCell(sgstRateValCell);

					Phrase igstRateVal = new Phrase("-", font8);
					PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
					igstRateValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateValCell);

					Phrase igstRate = new Phrase("-", font8);
					PdfPCell igstRateCell = new PdfPCell(igstRate);
					igstRateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					igstRateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(igstRateCell);

					double indivTotalAmount = assTotalAmount
							+ ctaxValue + staxValue;
					totalAmount = totalAmount + indivTotalAmount;
					Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
							+ "", font8);
					PdfPCell totalCell = new PdfPCell(totalPhrase);

					totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					productTable.addCell(totalCell);

					String premisesVal = "";
					premisesVal = contEntity.getItems().get(i)
							.getPremisesDetails();
					if (printPremiseDetails) {
						Phrase premisesValPhrs = new Phrase(
								"Premise Details : " + premisesVal, font8);
						PdfPCell premiseCell = new PdfPCell();
						premiseCell.setColspan(16);
						premiseCell.addElement(premisesValPhrs);
						productTable.addCell(premiseCell);
					}
					/**
					 * nidhi
					 * for print model and serial number
					 */
					int cnnt = 0;
					PdfPCell proModelcell = null ,proSerialNocell = null; 
					String proModelNo = "";
					String proSerialNo = "";
					if(printModelSerailNoFlag){
//						String proModelNo = "";
						if(contEntity.getItems().get(i).getProModelNo()!=null && 
							contEntity.getItems().get(i).getProModelNo().trim().length() >0){
							proModelNo = contEntity.getItems().get(i).getProModelNo();
						}
						
//						String proSerialNo = "";
						if(contEntity.getItems().get(i).getProSerialNo()!=null && 
							contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
							proSerialNo = contEntity.getItems().get(i).getProSerialNo();
						}
						
						if(proModelNo.length()>0){
							Phrase modelValPhrs = new Phrase(
									"Model No : " + proModelNo, font8);
							proModelcell = new PdfPCell();
							proModelcell.setColspan(8);
							proModelcell.addElement(modelValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						if(proSerialNo.length()>0){
							Phrase serialValPhrs = new Phrase(
									"Serial No : " + proSerialNo, font8);
							proSerialNocell = new PdfPCell();
							proSerialNocell.setColspan(8);
							proSerialNocell.addElement(serialValPhrs);
//							productTable.addCell(premiseCell);
							++cnnt;
						}
						
						if(cnnt>1 ){
							proSerialNocell.setColspan(8);
						}else if(proModelNo.length()>0){
							proModelcell.setColspan(16);
						}else {
							proSerialNocell.setColspan(16);
						}
						
						if(cnnt>0);
						{
//							noOfLines = noOfLines-1;
//							table1.addCell(Pdfsrnocell2);
							if(proModelcell!=null){
								productTable.addCell(proModelcell);
							}
							if(proSerialNocell!=null){
								productTable.addCell(proSerialNocell);
							}
						}
					}
					/**
					 * end
					 */
					try {
						document.add(productTable);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				}

		}else{


			logger.log(Level.SEVERE,"Inside Tax Not Applicable");

			PdfPCell cell = new PdfPCell(new Phrase("-", font8));
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			productTable.addCell(cell);
			Phrase totalPhrase = new Phrase(df.format(getAssessTotalAmount(contEntity.getItems().get(i)))
					+ "", font8);
			PdfPCell totalCell = new PdfPCell();
			// totalCell.setColspan(16);
			// totalCell.setBorder(0);
			// totalCell.setBorderWidthBottom(0);
			// totalCell.setBorderWidthTop(0);
			totalCell.addElement(totalPhrase);
			productTable.addCell(totalCell);

			String premisesVal = "";
			if(contEntity.getItems().get(i).getPremisesDetails()!=null){
				premisesVal=contEntity.getItems().get(i).getPremisesDetails();
			}
			
			if (printPremiseDetails) {
				Phrase premisesValPhrs = new Phrase("Premise Details : "
						+ premisesVal, font8);
				PdfPCell premiseCell = new PdfPCell();
				premiseCell.setColspan(16);
				premiseCell.addElement(premisesValPhrs);
				productTable.addCell(premiseCell);
			}
			
			/**
			 * nidhi
			 * for print model and serial number
			 */
			int cnnt = 0;
			PdfPCell proModelcell = null ,proSerialNocell = null; 
			String proModelNo = "";
			String proSerialNo = "";
			if(printModelSerailNoFlag){
//				String proModelNo = "";
				if(contEntity.getItems().get(i).getProModelNo()!=null && 
					contEntity.getItems().get(i).getProModelNo().trim().length() >0){
					proModelNo = contEntity.getItems().get(i).getProModelNo();
				}
				
//				String proSerialNo = "";
				if(contEntity.getItems().get(i).getProSerialNo()!=null && 
					contEntity.getItems().get(i).getProSerialNo().trim().length() >0){
					proSerialNo = contEntity.getItems().get(i).getProSerialNo();
				}
				
				if(proModelNo.length()>0){
					Phrase modelValPhrs = new Phrase(
							"Model No : " + proModelNo, font8);
					proModelcell = new PdfPCell();
					proModelcell.setColspan(8);
					proModelcell.addElement(modelValPhrs);
//					productTable.addCell(premiseCell);
					++cnnt;
				}
				if(proSerialNo.length()>0){
					Phrase serialValPhrs = new Phrase(
							"Serial No : " + proSerialNo, font8);
					proSerialNocell = new PdfPCell();
					proSerialNocell.setColspan(8);
					proSerialNocell.addElement(serialValPhrs);
//					productTable.addCell(premiseCell);
					++cnnt;
				}
				
				if(cnnt>1 ){
					proSerialNocell.setColspan(8);
				}else if(proModelNo.length()>0){
					proModelcell.setColspan(16);
				}else {
					proSerialNocell.setColspan(16);
				}
				
				if(cnnt>0);
				{
//					noOfLines = noOfLines-1;
//					table1.addCell(Pdfsrnocell2);
					if(proModelcell!=null){
						productTable.addCell(proModelcell);
					}
					if(proSerialNocell!=null){
						productTable.addCell(proSerialNocell);
					}
				}
			}
			/**
			 * end
			 */
			try {
				document.add(productTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}

		}

		
	}

	private void createCustomerDetails() {
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		float[] columnHalfWidth = { 1f, 1f };
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font6bold);
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			// if(cust.isCompany()==true){
			//
			//
			// tosir="M/S";
			// }
			// else{
			// tosir="";
			// }

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S" + cust.getCompanyName().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here

		Phrase nameCellVal = new Phrase(fullname + "       Mob : "
				+ cust.getCellNumber1(), font7);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ////////////////// Ajinkya added this for Ultra
		Phrase email = new Phrase("Email Id ", font10bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase emailVal = new Phrase("" + cust.getEmail(), font7);
		PdfPCell emailValCell = new PdfPCell(emailVal);
		// nameCellValCell.addElement(nameCellVal);asd
		emailValCell.setBorder(0);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// /////////////////

		Phrase address = new Phrase("Address", font6bold);
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		

		Phrase addressVal = new Phrase(cust.getAdress().getCompleteAddress()
				.trim(), font7);
		PdfPCell addressValCell = new PdfPCell(addressVal);
		// addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		String gstTinStr = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstTinStr = cust.getArticleTypeDetails().get(i)
						.getArticleTypeValue().trim();
			}
		}

		Phrase gstTin = new Phrase("GSTIN", font6bold);
		PdfPCell gstTinCell = new PdfPCell(gstTin);
		// gstTinCell.addElement(gstTin);
		gstTinCell.setBorder(0);
		gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTinVal = new Phrase(gstTinStr, font7);
		PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
		// gstTinValCell.addElement(gstTinVal);
		gstTinValCell.setBorder(0);
		gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);
		colonTable.addCell(gstTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(gstTinValCell);

		PdfPCell cell1 = new PdfPCell();
		cell1.setBorder(0);
		cell1.addElement(colonTable);

		Phrase state = new Phrase("State", font6bold);
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(cust.getAdress().getState().trim(), font7);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase stateCode = new Phrase("State Code", font6bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(stCo, font7);
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		// stateCodeValCell.addElement(stateCodeVal);
		// stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);

		PdfPCell colonTableState = new PdfPCell();
		colonTableState.setBorder(0);
		colonTableState.addElement(colonTable);
		statetable.addCell(colonTableState);

		colonTable = new PdfPTable(4);
		colonTable.setWidthPercentage(100);
		float[] columnStateCodeCollonWidth = { 35, 5, 15, 45 };
		try {
			colonTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase blak = new Phrase(" ", font8);
		PdfPCell blakCell = new PdfPCell(blak);
		blakCell.setBorder(0);

		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		colonTable.addCell(blakCell);

		PdfPCell colonTableValState = new PdfPCell();
		colonTableValState.setBorder(0);
		colonTableValState.addElement(colonTable);
		statetable.addCell(colonTableValState);

		PdfPCell stateTableCell = new PdfPCell(statetable);
		stateTableCell.setBorder(0);
		stateTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// stateTableCell.addElement(statetable);

		part1Table.addCell(cell1);
		part1Table.addCell(stateTableCell);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		/* Ends Part 1 */

		/* Part 2 Start */
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font6bold);
		PdfPCell name2Cell = new PdfPCell();
		name2Cell.addElement(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name2CellVal = new Phrase(fullname + "       Mob : "
				+ cust.getCellNumber1(), font7);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////////Ajinkya added this for Ultra
		Phrase email1 = new Phrase("Email Id ", font6bold);
		PdfPCell email1Cell = new PdfPCell(email1);
		email1Cell.setBorder(0);
		email1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase email1Val = new Phrase("" + cust.getEmail(), font7);
		PdfPCell email1ValCell = new PdfPCell(email1Val);
		// nameCellValCell.addElement(nameCellVal);asd
		email1ValCell.setBorder(0);
		email1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// /////////////////

		Phrase address2 = new Phrase("Address", font6bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase address2Val = new Phrase(cust.getSecondaryAdress()
//				.getCompleteAddress().trim(), font7);
//		PdfPCell address2ValCell = new PdfPCell(address2Val);
//		// address2ValCell.addElement(address2Val);
//		address2ValCell.setBorder(0);
//		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/*
		 *Date:14/09/2018
		 *Developer:Ashwini
		 *Des:To add service address updated on contract screen
		 */
//		System.out.println("Service Address of GST Quatation pdf::"+contEntity.getCustomerServiceAddress().getCompleteAddress());
		//Phrase address2Val = new Phrase(contEntity.getCustomerServiceAddress().getCompleteAddress().trim(), font7);
		Address serviceAddress=null;
		String custFullAdd1="";
		/**
		 * @author Anil, Date : 18-02-2019
		 * Issue pdf load issue : when we were printing quotation pdf
		 * for bitco raised by Rahul tiwari
		 */
		if (contEntity!=null&&contEntity.getCustomerServiceAddress() != null) {
			serviceAddress = contEntity.getCustomerServiceAddress();
			custFullAdd1 = contEntity.getCustomerServiceAddress().getCompleteAddress();

			logger.log(Level.SEVERE,"GST Service Address::"+custFullAdd1);
		} else {
			serviceAddress = cust.getSecondaryAdress();
			custFullAdd1 = cust.getSecondaryAdress().getCompleteAddress();
			logger.log(Level.SEVERE,"GST Service Address1::"+custFullAdd1);
		}
		
		Phrase address2Val1 = new Phrase (custFullAdd1,font7);
		PdfPCell address2ValCell1 = new PdfPCell(address2Val1);
	// address2ValCell.addElement(address2Val);
		address2ValCell1.setBorder(0);
		address2ValCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/*
		 * End by Ashwini
		 */
		
		
		String gstTin2Str = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstTin2Str = cust.getArticleTypeDetails().get(i)
						.getArticleTypeName().trim();
			}
		}

		Phrase gstTin2 = new Phrase("GSTIN", font6bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		// gstTin2Cell.addElement(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font7);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		// gstTin2ValCell.addElement(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
//		colonTable.addCell(address2ValCell);
		colonTable.addCell(address2ValCell1); //Added by Ashwini
		colonTable.addCell(gstTinCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(gstTinValCell);

		PdfPCell cell2 = new PdfPCell();
		cell2.setBorder(0);
		cell2.addElement(colonTable);

		Phrase state2 = new Phrase("State", font6bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		// state2Cell.addElement(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2Val = new Phrase(cust.getSecondaryAdress().getState()
				.trim(), font7);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// state2ValCell.addElement(state2Val);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stsecCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList
					.get(i)
					.getStateName()
					.trim()
					.equalsIgnoreCase(
							cust.getSecondaryAdress().getState().trim())) {
				stsecCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font6bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		// state2CodeCell.addElement(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(stsecCo, font7);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		// state2CodeValCell.addElement(state2CodeVal);
		// state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable state2table = new PdfPTable(2);
		state2table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);
		PdfPCell state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		colonTable = new PdfPTable(4);
		colonTable.setWidthPercentage(100);
		// float[] columnStateCodeCollonWidth = {35,5,15,45};
		try {
			colonTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2CodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2CodeValCell);
		colonTable.addCell(blakCell);

		state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		PdfPCell state2TableCell = new PdfPCell(state2table);
		state2TableCell.setBorder(0);
		// state2TableCell.addElement(state2table);

		part2Table.addCell(cell2);
		part2Table.addCell(state2TableCell);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);
		/* Part 2 Ends */

		Phrase blankCell = new Phrase(" ", font7);
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createProductDetails() {
		PdfPTable productTable = new PdfPTable(16);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column16CollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase srNophrase = new Phrase("Sr No", font8bold);
		PdfPCell srNoCell = new PdfPCell();
		srNoCell.addElement(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

		Phrase servicePhrase = new Phrase("Services", font8bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2

		Phrase hsnCode = new Phrase("SAC", font8bold);
		PdfPCell hsnCodeCell = new PdfPCell();
		hsnCodeCell.addElement(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 3

		Phrase UOMphrase = new Phrase("UOM", font8bold);
		PdfPCell UOMphraseCell = new PdfPCell();
		UOMphraseCell.addElement(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 4

		Phrase qtyPhrase = new Phrase("Qty", font8bold);
		PdfPCell qtyPhraseCell = new PdfPCell();
		qtyPhraseCell.addElement(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 5

		Phrase ratePhrase = new Phrase("Rate", font8bold);
		PdfPCell ratePhraseCell = new PdfPCell();
		ratePhraseCell.addElement(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 6

		Phrase amountPhrase = new Phrase("Amount", font8bold);
		PdfPCell amountPhraseCell = new PdfPCell();
		amountPhraseCell.addElement(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 7

		Phrase dicphrase = new Phrase("Disc", font8bold);
		PdfPCell dicphraseCell = new PdfPCell();
		dicphraseCell.addElement(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 8

		Phrase taxValPhrase = new Phrase("Ass Val", font8bold);
		PdfPCell taxValPhraseCell = new PdfPCell();
		taxValPhraseCell.addElement(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 9

		// PdfPTable cgstcellTable=new PdfPTable(1);
		// cgstcellTable.setWidthPercentage(100);

		Phrase cgstphrase = new Phrase("CGST", font8bold);
		PdfPCell cgstphraseCell = new PdfPCell(cgstphrase);
		// cgstphraseCell.addElement(cgstphrase);
		// cgstphraseCell.setBorder(0);
		cgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// cgstcellTable.addCell(cgstphraseCell);
		cgstphraseCell.setColspan(2);
		// cgstphraseCell.setRowspan(2);

		Phrase sgstphrase = new Phrase("SGST", font8bold);
		PdfPCell sgstphraseCell = new PdfPCell(sgstphrase);
		// sgstphraseCell.setBorder(0);
		sgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// sgstphraseCell.addElement(sgstphrase);
		// sgstcellTable.addCell(sgstphraseCell);
		sgstphraseCell.setColspan(2);
		// sgstphraseCell.setRowspan(2);

		Phrase igstphrase = new Phrase("IGST", font8bold);
		PdfPCell igstphraseCell = new PdfPCell(igstphrase);
		// igstphraseCell.setBorder(0);
		igstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// igstphraseCell.addElement(igstphrase);
		// igstcellTable.addCell(igstphraseCell);
		igstphraseCell.setColspan(2);
		// igstphraseCell.setRowspan(2);

		Phrase totalPhrase = new Phrase("Total", font8bold);
		PdfPCell totalPhraseCell = new PdfPCell();
		totalPhraseCell.addElement(totalPhrase);
		totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		totalPhraseCell.setRowspan(2);// 2

		Phrase cgstpercentphrase = new Phrase("%", font6bold);
		PdfPCell cgstpercentphraseCell = new PdfPCell();
		// cgstpercentphraseCell.setBorderWidthBottom(0);
		// cgstpercentphraseCell.setBorderWidthTop(0);
		// cgstpercentphraseCell.setBorderWidthLeft(0);
		cgstpercentphraseCell.addElement(cgstpercentphrase);
		// innerCgstTable.addCell(cgstpercentphraseCell);

		Phrase cgstamtphrase = new Phrase("Amt", font6bold);
		PdfPCell cgstamtphraseCell = new PdfPCell();
		cgstamtphraseCell.addElement(cgstamtphrase);
		//

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(hsnCodeCell);
		productTable.addCell(UOMphraseCell);
		productTable.addCell(qtyPhraseCell);
		productTable.addCell(ratePhraseCell);
		productTable.addCell(amountPhraseCell);
		productTable.addCell(dicphraseCell);
		productTable.addCell(taxValPhraseCell);

		productTable.addCell(cgstphraseCell);
		productTable.addCell(sgstphraseCell);
		productTable.addCell(igstphraseCell);

		productTable.addCell(totalPhraseCell);

		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);
		productTable.addCell(cgstpercentphraseCell);
		productTable.addCell(cgstamtphraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createInvoiceDetails() {

		float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		float[] columnHalfWidth = { 1f, 1f };
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = { 3.5f, 0.2f, 6.8f };
		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font6bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseCharge = new Phrase("Reverse Charge (Y/N) ", font6bold);
		PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
		// reverseChargeCell.addElement(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase reverseChargeVal = new Phrase("No", font7);
		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		// reverseChargeValCell.addElement(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNo = new Phrase(" ", font6bold);
		PdfPCell invoiceNoCell = new PdfPCell(invoiceNo);
		// invoiceNoCell.addElement(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNoVal = new Phrase("", font7); // qp.getCount()+ removed
														// Value As per
														// Suggestion of Rohan
														// sir
		PdfPCell invoiceNoValCell = new PdfPCell(invoiceNoVal);
		// invoiceNoValCell.addElement(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceDate = new Phrase(" ", font6bold);
		PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		// invoiceDateCell.addElement(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// sdf.format(invoiceentity.getInvoiceDate())
		Phrase invoiceDateVal = new Phrase("", font7);
		PdfPCell invoiceDateValCell = new PdfPCell(invoiceDate); // invoiceDateVal
																	// for Blnk
																	// Val
																	// changes
		// invoiceDateValCell.addElement(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blnk = new Phrase(" ", font7);
		PdfPCell blnkCell = new PdfPCell(blnk);
		blnkCell.setBorder(0);
		blnkCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(blnkCell);
		colonTable.addCell(invoiceNoValCell);
		colonTable.addCell(invoiceDateCell);
		colonTable.addCell(blnkCell);
		colonTable.addCell(invoiceDateValCell);

		PdfPCell pdfCell = new PdfPCell();
		pdfCell.setBorder(0);
		pdfCell.addElement(colonTable);

		part1Table.addCell(pdfCell);

		Phrase state = new Phrase("State", font6bold);
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(), font7);
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCode = new Phrase("State Code", font6bold);
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(" ", font7);
		PdfPCell stateCodeValCell = new PdfPCell();
		stateCodeValCell.addElement(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);

		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);

		PdfPCell stateTableCell = new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);
		Phrase contractId;
		if (qp instanceof Contract) {
			contractId = new Phrase("Contract Id", font6bold);
		} else {
			contractId = new Phrase("Quotation Id", font6bold);
		}
		PdfPCell contractIdCell = new PdfPCell(contractId);
		// contractIdCell.addElement(contractId);
		contractIdCell.setBorder(0);
		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// " "+invoiceentity.getContractCount()+
		Phrase contractIdVal;
		if (qp instanceof Contract) {
			Contract contEntity = (Contract) qp;
			contractIdVal = new Phrase(contEntity.getCount() + "", font7);
		} else {
			Quotation quotEntity = (Quotation) qp;
			contractIdVal = new Phrase(quotEntity.getCount() + "", font7);
		}

		PdfPCell contractIdValCell = new PdfPCell(contractIdVal);
		// contractIdValCell.addElement(contractIdVal);
		contractIdValCell.setBorder(0);
		contractIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase startDate;
		if (qp instanceof Contract) {
			startDate = new Phrase("Start Date", font6bold);
		} else {
			startDate = new Phrase("Quotation Date", font6bold);
		}
		PdfPCell startDateCell = new PdfPCell(startDate);
		// startDateCell.addElement(startDate);
		startDateCell.setBorder(0);
		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// sdf.format(q.getContractStartDate())

		String strtDateVal = "";
		String endDteVal = "";

		if (qp instanceof Contract) {
			System.out.println("In Contract");
			strtDateVal = fmt.format(contEntity.getStartDate());
			endDteVal = fmt.format(contEntity.getEndDate());
		} else {
			System.out.println("In Q");
			strtDateVal = fmt.format(quotEntity.getQuotationDate());
			endDteVal = fmt.format(quotEntity.getValidUntill());
		}

		Phrase startDateVal = new Phrase(strtDateVal, font7);
		PdfPCell startDateValCell = new PdfPCell(startDateVal);
		System.out.println("stDate" + strtDateVal);
		// startDateValCell.addElement(startDateVal);
		startDateValCell.setBorder(0);
		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDate;
		if (qp instanceof Contract) {
			endDate = new Phrase("End Date", font6bold);
		} else {
			endDate = new Phrase("Valid Until", font6bold);
		}
		PdfPCell endDateCell = new PdfPCell(endDate);
		// endDateCell.addElement(endDate);
		endDateCell.setBorder(0);
		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// sdf.format(invoiceentity.getContractEndDate())

		Phrase endDateVal = new Phrase("" + endDteVal, font7);
		PdfPCell endDateValCell = new PdfPCell(endDateVal);
		System.out.println("edDate" + endDteVal);
		// endDateValCell.addElement(endDateVal);
		endDateValCell.setBorder(0);
		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * End Here
		 */

		PdfPTable billperiodtable = new PdfPTable(2);
		billperiodtable.setWidthPercentage(100);

		PdfPTable billFromcolonTable = new PdfPTable(3);
		billFromcolonTable.setWidthPercentage(100);
		try {
			billFromcolonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPTable billTocolonTable = new PdfPTable(3);
		billTocolonTable.setWidthPercentage(100);
		try {
			billTocolonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPCell fromDate = new PdfPCell(billFromcolonTable);
		fromDate.setBorder(0);

		billperiodtable.addCell(fromDate);
		fromDate = new PdfPCell();
		fromDate.setBorder(0);
		fromDate.addElement(billTocolonTable);
		billperiodtable.addCell(fromDate);
		/**
	 * 
	 */
		PdfPTable periodtable = new PdfPTable(2);
		periodtable.setWidthPercentage(100);
		float[] columnrohanrrCollonWidth = { 2.8f, 0.2f, 7.7f };
		PdfPTable concolonTable = new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		concolonTable.addCell(contractIdCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(contractIdValCell);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(startDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(startDateValCell);

		PdfPCell startcolonTableCell = new PdfPCell(colonTable);
		startcolonTableCell.setBorder(0);
		// startcolonTableCell.addElement(colonTable);
		periodtable.addCell(startcolonTableCell);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(endDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(endDateValCell);

		PdfPCell endcolonTableCell = new PdfPCell();
		endcolonTableCell.setBorder(0);
		endcolonTableCell.addElement(colonTable);
		periodtable.addCell(endcolonTableCell);

		PdfPCell periodTableCell = new PdfPCell();
		periodTableCell.setBorder(0);
		periodTableCell.addElement(periodtable);

		PdfPCell concolonTableCell = new PdfPCell();
		concolonTableCell.setBorder(0);
		concolonTableCell.addElement(concolonTable);

		PdfPCell billperiodtableCell = new PdfPCell();
		billperiodtableCell.setBorder(0);
		billperiodtableCell.addElement(billperiodtable);

		part2Table.addCell(concolonTableCell);
		part2Table.addCell(periodTableCell);
		part2Table.addCell(billperiodtableCell);

		PdfPCell part1Cell = new PdfPCell();
		part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part1Table);

		mainTable.addCell(part1Cell);

		part1Cell = new PdfPCell();
		// part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part2Table);
		mainTable.addCell(part1Cell);

		// mainTable.addCell(blankCell);
		// mainTable.addCell(blankCell);
		Phrase billingAddress = new Phrase("Billing Address", font8bold);
		// Paragraph billingpara=new Paragraph();
		// billingpara.add(billingAddress);
		// billingpara.setAlignment(Element.ALIGN_CENTER);
		// billingpara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		// billAdressCell.addElement(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);

		mainTable.addCell(billAdressCell);
		Phrase serviceaddress = new Phrase("Service Address", font8bold);
		// Paragraph servicepara=new Paragraph();
		// servicepara.add(serviceaddress);
		// servicepara.setAlignment(Element.ALIGN_CENTER);
		// servicepara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		// serviceCell.addElement(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		mainTable.addCell(serviceCell);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
