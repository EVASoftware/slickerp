package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;

public class MultipleExpenseDetailspdf {

	public Document document;
	MultipleExpenseMngt mulExp;
	
	private Font font16boldul, font12bold, font8bold, font8, font16bold,
	font12boldul, font12, font14bold, font10, font10bold, font10boldul,
	font9, font9bold, font14boldul;

	
	float round = 0.00f;
	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	float[] colWidth = { 70f, 15f, 15f };
	float[] columnWidth = { (float) 33.33, (float) 33.33, (float) 33.33 };
	float[] col1Width = { 10f, 90f };
	float[] col2Width = { 25f, 75f };
	
	float[] col8Width = { 0.5f,1f,1.5f,1.5f,1.0f,1.0f,2.0f,1.0f };
	
	
	public void setpdfMultipleexpense(Long count) {
		
		mulExp = ofy().load().type(MultipleExpenseMngt.class).id(count).now();
	}
	
	public MultipleExpenseDetailspdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	}

	public void createPdf() {
		
		
		createVoucherHeading();
		createExpenseDetails();
		createAccountDetail();
		createMultipleExpenseDetails();
		createInWordsTable();
		createSignature();
		String amountInWord = ServiceInvoicePdf.convert(mulExp.getTotalAmount());

	}

	private void createMultipleExpenseDetails() {
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(40f);
		
		try {
			table.setWidths(col8Width);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase srNo = new Phrase("Sr NO",font8bold);
		PdfPCell srnoCell = new PdfPCell(srNo);
		srnoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(srnoCell);
		
		Phrase invoiceNO = new Phrase("Invoice Id",font8bold);
		PdfPCell invoiceNOCell = new PdfPCell(invoiceNO);
		invoiceNOCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(invoiceNOCell);
		
		Phrase expCat = new Phrase("Exp Category",font8bold);
		PdfPCell expCatCell = new PdfPCell(expCat);
		expCatCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(expCatCell);
		
		Phrase expType = new Phrase("Exp Type",font8bold);
		PdfPCell expTypeCell = new PdfPCell(expType);
		expTypeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(expTypeCell);
		
		Phrase expDate = new Phrase("Exp Date",font8bold);
		PdfPCell expDateCell = new PdfPCell(expDate);
		expDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(expDateCell);
		
		Phrase expAmt = new Phrase("Exp Amount",font8bold);
		PdfPCell expAmtCell = new PdfPCell(expAmt);
		expAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(expAmtCell);
		
		Phrase expPersonResponsible= new Phrase("Exp Person Responsible",font8bold);
		PdfPCell expPersonResponsibleCell = new PdfPCell(expPersonResponsible);
		expPersonResponsibleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(expPersonResponsibleCell);
		
		Phrase expVendor = new Phrase("Exp Vendor",font8bold);
		PdfPCell expVendorCell = new PdfPCell(expVendor);
		expVendorCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(expVendorCell);
		
		for (int i = 0; i < mulExp.getExpenseList().size(); i++) 
		{
			Phrase srNoValue = new Phrase(i+1+"",font8);
			PdfPCell srNoValueCell = new PdfPCell(srNoValue);
			srNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(srNoValueCell);
			
			Phrase invoiceIdValue;
			if(mulExp.getExpenseList().get(i).getInvoiceNumber()!= null && !mulExp.getExpenseList().get(i).getInvoiceNumber().equals(""))
			{
				invoiceIdValue = new Phrase(mulExp.getExpenseList().get(i).getInvoiceNumber(),font8);
				
			}
			else
			{
				invoiceIdValue = new Phrase("NA",font8);
			}
			
			PdfPCell invoiceIdValueCell = new PdfPCell(invoiceIdValue);
			invoiceIdValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(invoiceIdValueCell);
			
			
			Phrase expCatValue;
			if(mulExp.getExpenseList().get(i).getExpenseCategory()!= null && !mulExp.getExpenseList().get(i).getExpenseCategory().equals(""))
			{
				expCatValue = new Phrase(mulExp.getExpenseList().get(i).getExpenseCategory(),font8);
				
			}
			else
			{
				expCatValue = new Phrase("NA",font8);
			}
			
			PdfPCell expCatValueCell = new PdfPCell(expCatValue);
			expCatValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(expCatValueCell);
			
			
			Phrase expTypeValue;
			if(mulExp.getExpenseList().get(i).getExpenseType()!= null && !mulExp.getExpenseList().get(i).getExpenseType().equals(""))
			{
				expTypeValue = new Phrase(mulExp.getExpenseList().get(i).getExpenseType(),font8);
				
			}
			else
			{
				expTypeValue = new Phrase("NA",font8);
			}
			
			PdfPCell expTypeValueCell = new PdfPCell(expTypeValue);
			expTypeValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(expTypeValueCell);
			
			
			Phrase expDateValue;
			if(mulExp.getExpenseList().get(i).getExpenseDate()!= null)
			{
				expDateValue = new Phrase(fmt.format(mulExp.getExpenseList().get(i).getExpenseDate()),font8);
				
			}
			else
			{
				expDateValue = new Phrase("NA",font8);
			}
			
			PdfPCell expDateValueCell = new PdfPCell(expDateValue);
			expDateValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(expDateValueCell);
			
			
			Phrase expAmtValue;
			if(mulExp.getExpenseList().get(i).getExpenseDate()!= null)
			{
				expAmtValue = new Phrase(mulExp.getExpenseList().get(i).getAmount()+"",font8);
				
			}
			else
			{
				expAmtValue = new Phrase("NA",font8);
			}
			
			PdfPCell expAmtValueCell = new PdfPCell(expAmtValue);
			expAmtValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(expAmtValueCell);
			
			Phrase expPersonResponsibleValue;
			if(mulExp.getExpenseList().get(i).getEmployee()!= null && !mulExp.getExpenseList().get(i).getEmployee().equals(""))
			{
				expPersonResponsibleValue = new Phrase(mulExp.getExpenseList().get(i).getEmployee(),font8);
				
			}
			else
			{
				expPersonResponsibleValue = new Phrase("NA",font8);
			}
			
			PdfPCell expPersonResponsibleValueCell = new PdfPCell(expPersonResponsibleValue);
			expPersonResponsibleValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(expPersonResponsibleValueCell);
			
			
			Phrase expVendorValue;
			if(mulExp.getExpenseList().get(i).getVendor()!= null && !mulExp.getExpenseList().get(i).getVendor().equals(""))
			{
				expVendorValue = new Phrase(mulExp.getExpenseList().get(i).getVendor(),font8);
				
			}
			else
			{
				expVendorValue = new Phrase("NA",font8);
			}
			
			PdfPCell expVendorValueCell = new PdfPCell(expVendorValue);
			expVendorValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(expVendorValueCell);
			
		}
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void createExpenseDetails() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase no = null;
		if (mulExp.getCount() != 0) {
			no = new Phrase("No. : " + mulExp.getCount(), font10bold);
		}

		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_CENTER);


		PdfPTable detailtable = new PdfPTable(3);
		detailtable.addCell(nocell);
		detailtable.addCell(blcell);
		detailtable.addCell(blcell);
		detailtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(detailtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);


		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createVoucherHeading() {
		
		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);


		Phrase head = new Phrase("Expense Voucher", font14boldul);
		PdfPCell headcell = new PdfPCell(head);
		headcell.setBorder(0);
		headcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable headtable = new PdfPTable(3);
		headtable.addCell(blcell);
		headtable.addCell(headcell);
		headtable.addCell(blcell);
		headtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(headtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);
		
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createInWordsTable() {

		String amountInWord = ServiceInvoicePdf.convert(mulExp.getTotalAmount());

		Phrase blank = new Phrase(" " + amountInWord + " Only ", font10bold);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorderWidthLeft(0);
		blcell.setBorderWidthRight(0);
		blcell.setBorderWidthTop(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase acc = new Phrase("Rupees in Words : ", font10bold);
		PdfPCell accountcell = new PdfPCell(acc);
		accountcell.setBorder(0);
		accountcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(blank1);
		bl1cell.setBorder(0);

		PdfPTable acctable = new PdfPTable(2);
		acctable.addCell(accountcell);
		acctable.addCell(blcell);

		try {
			acctable.setWidths(col2Width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		acctable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(acctable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);

		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createSignature() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase sign1 = new Phrase(" ", font10bold);
		PdfPCell sign1cell = new PdfPCell(sign1);
		sign1cell.setBorder(0);
		sign1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase sign2 = new Phrase("Signature ", font10bold);
		PdfPCell sign2cell = new PdfPCell(sign2);
		sign2cell.setBorder(0);
		sign2cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable detailtable = new PdfPTable(3);
		detailtable.addCell(sign1cell);
		detailtable.addCell(blcell);
		detailtable.addCell(sign2cell);

		try {
			detailtable.setWidths(columnWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		detailtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(detailtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createAccountDetail() {

		
		Phrase blank = new Phrase( mulExp.getAccount()+"", font10bold);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorderWidthLeft(0);
		blcell.setBorderWidthRight(0);
		blcell.setBorderWidthTop(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase acc = new Phrase("A/c. : ", font10bold);
		PdfPCell accountcell = new PdfPCell(acc);
		accountcell.setBorder(0);
		accountcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable acctable = new PdfPTable(2);
		
		acctable.setWidthPercentage(100f);
		acctable.setSpacingAfter(40f);

		acctable.addCell(accountcell);
		acctable.addCell(blcell);

		try {
			acctable.setWidths(col1Width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

	
		
		

		try {
			document.add(acctable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
