package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.customer.CustomerForm;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.ProjectAllocation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.HrProject;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeCTCTemplate;

public class PayslipStatementPdf {

	public Document document;
	Company comp;
	
	List<EmployeeInfo> empInfoList;
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul,font16bold,font7;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	SimpleDateFormat salFmt = new SimpleDateFormat("yyyy-MMM");
	SimpleDateFormat salMFmt = new SimpleDateFormat("MMMM yyyy");
	
	ArrayList<PaySlip> paySlipList=new ArrayList<PaySlip>();
	
	float[] columnWidths15={0.30f,0.70f,1.35f,0.50f,0.65f,0.65f,0.65f,0.65f,0.65f,0.65f,0.65f,0.75f,0.75f,0.75f,0.75f};
	
	DecimalFormat df=new DecimalFormat("0.00");
	DecimalFormat df1=new DecimalFormat("0");
	
	boolean isInternal=false;
	
	int pageCounter=0;
	boolean nextPageFlag=false;
	int index=0;
	int perPageLimit=0;
	
	
	double sumOfBasic=0;
	double sumOfMedical=0;
	double sumOfBonus=0;
	double sumOfPl=0;
	double sumOfWashingAllowance=0;
	double sumOfPayAdjust=0;
	double sumOfDa=0;
	double sumOfBnP=0;
	double sumOfTravelling=0;
	double sumOfHra=0;
	double sumOfOt=0;
	double sumOfGross=0;
	double sumOfEsic=0;
	double sumOfLwf=0;
	double sumOfPf=0;
	double sumOfAdavance=0;
	double sumOfPt=0;
	double sumOfTotalDed=0;
	double sumOfNetEarning=0;
	double sumOfArrearsDa=0;
	double sumofOtherEarn=0;
	double sumOfArrearsHra=0;
	double sumOfOtherAllowance=0;
	double sumOfArrearsBasic=0;
	
	/**
	 * Date : 03-09-2018 BY ANIL
	 * here we are loading Employee and CTC entity for showing rate Traveling allowance,Monthly Bonus and Paid Leave
	 */
	
	List<Employee> empList;
	List<CTC> ctcList;
	Date fromDate;
	Date toDate;
	
	/**
	 * End
	 */
	
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	Customer customer;
	boolean customerNameFlag=false;
	PaySlip payslip;
	HrProject hrproject;
	ProjectAllocation projectallocation;
	boolean hideColumn=false;
	
	public PayslipStatementPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		salFmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		salMFmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setPayslip(long companyId, ArrayList<PaySlip> paySlipList,boolean isInternal){
		this.paySlipList=paySlipList;
		
		Date date=null;
		try {
			date=salFmt.parse(this.paySlipList.get(0).getSalaryPeriod());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		fromDate=DateUtility.getStartDateofMonth(date);
		toDate=DateUtility.getEndDateofMonth(date);
		
		System.out.println("DATE :: "+date);
		System.out.println("FORM DATE :: "+fromDate);
		System.out.println("TO DATE :: "+toDate);
		Comparator<PaySlip> comparator=new Comparator<PaySlip>() {
			@Override
			public int compare(PaySlip arg0, PaySlip arg1) {
				Integer count1=arg0.getEmpid();
				Integer count2=arg1.getEmpid();
				return count1.compareTo(count2);
			}
		};
		Collections.sort(this.paySlipList, comparator);
		
		
		this.isInternal=isInternal;
		System.out.println("PAY SLIP SIZE "+this.paySlipList.size());
		
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		if(paySlipList!=null){
			hrproject=ofy().load().type(HrProject.class).filter("companyId", companyId).filter("projectName", paySlipList.get(0).getProjectName()).first().now();
		}
		
		if(hrproject!=null){
			customer=ofy().load().type(Customer.class).filter("count",hrproject.getPersoninfo().getCount()).filter("companyId",companyId).first().now();
		}
		HashSet<Integer> empIdHs=new HashSet<Integer>();
		for(PaySlip ps:this.paySlipList){
			empIdHs.add(ps.getEmpid());
		}
		
		ArrayList<Integer>empIdList=new ArrayList<Integer>(empIdHs);
		
//		empInfoList=new ArrayList<EmployeeInfo>();
//		if(empIdList.size()!=0){
//			empInfoList=ofy().load().type(EmployeeInfo.class).filter("companyId", companyId).filter("empCount IN", empIdList).list();
//		}
		
		empList=new ArrayList<Employee>();
		if(empIdList.size()!=0){
			empList=ofy().load().type(Employee.class).filter("companyId", companyId).filter("count IN", empIdList).list();
			System.out.println("EMP LIST SIZE :: "+empList.size());
		}
		
		ctcList=new ArrayList<CTC>();
		if(empIdList.size()!=0){
			ctcList=ofy().load().type(CTC.class).filter("companyId", companyId).filter("empid IN", empIdList).list();
			System.out.println("CTC LIST SIZE :: "+ctcList.size());
		}
		
	}
	
	
	public double getBonusRate(int empId){
		double bonusRate=0;
		
		for(CTC ctc:ctcList){
			if(ctc.getEmpid()==empId){
//				if(ctc.getTodate()!=null&&ctc.getTodate().after(fromDate)&&ctc.getTodate().before(toDate)){
//					if(ctc.getBonusAmt()!=0){
//						bonusRate=ctc.getBonusAmt();
//						break;
//					}
//				}
				
				if(ctc.getTodate()!=null&&fromDate.before(ctc.getTodate())){
					if(ctc.getBonusAmt()!=0){
						bonusRate=ctc.getBonusAmt();
						break;
					}
				}
			}
		}
		return bonusRate;
	}
	
	public double getPaidLeaveRate(int empId){
		double paidLeaveRate=0;
		
		for(CTC ctc:ctcList){
			if(ctc.getEmpid()==empId){
//				if(ctc.getTodate()!=null&&ctc.getTodate().before(fromDate)&&ctc.getTodate().after(toDate)){
//					if(ctc.getPaidLeaveAmt()!=0){
//						paidLeaveRate=ctc.getPaidLeaveAmt();
//						break;
//					}
//				}
				if(ctc.getTodate()!=null&&fromDate.before(ctc.getTodate())){
					if(ctc.getPaidLeaveAmt()!=0){
						paidLeaveRate=ctc.getPaidLeaveAmt();
						break;
					}
				}
			}
		}
		return paidLeaveRate;
	}
	
	public double getTravelingAllowanceRate(int empId){
		double travelinngRate=0;
		for(Employee emp:empList){
			if(emp.getCount()==empId){
				if(emp.getEmployeeCTCList()!=null&&emp.getEmployeeCTCList().size()!=0){
					for(EmployeeCTCTemplate temp:emp.getEmployeeCTCList()){
//						if(temp.getCtcTemplateName().trim().equalsIgnoreCase("Travelling Allowance")
//								&&temp.getEndDate().before(fromDate)
//								&&temp.getEndDate().after(toDate)){
//							travelinngRate=temp.getAmount();
//							break;
//						}
						
						if(temp.getCtcTemplateName().trim().equalsIgnoreCase("Travelling Allowance")
								&&fromDate.before(temp.getEndDate())){
							travelinngRate=temp.getAmount();
							break;
						}
					}
				}
			}
		}
		return travelinngRate;
	}
	
	
	public String getProjectName(int empId){
		String projName="";
		for(EmployeeInfo info:empInfoList){
			if(info.getEmpCount()==empId){
				projName=info.getProjectName();
				break;
			}
		}
		return projName;
	}
	
	
	public void createPdf(){
		createCompanyHeaderTable();
		createPaySlipStatementTable();
	}

	private void createPaySlipStatementTable() {
		PdfPTable table=new PdfPTable(15);
		table.setWidthPercentage(98);
		try {
			table.setWidths(columnWidths15);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		perPageLimit=1;
		if(!nextPageFlag){
			index=0;
			pageCounter=1;
		}else{
			pageCounter++;
//			System.out.println("SETTING BLANK SPACE");
			Paragraph blankP=new Paragraph();
			Phrase blankPh=new Phrase("",font8);
			blankP.add(Chunk.NEWLINE);
			blankP.add(blankPh);
			
			PdfPCell blankCell=new PdfPCell(blankP);
			blankCell.setBorder(0);
			blankCell.setColspan(15);
			table.addCell(blankCell);
			
		}
//		System.out.println("NEXT PAGE FLAG : "+nextPageFlag+" INDEX : "+index+" PAGE COUNTER: "+pageCounter);
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip","OtherEarningComponent",comp.getCompanyId())){
			hideColumn=true;
		}
		
		//COL 1
		Phrase srNoPh = new Phrase("Sr.No.", font7);
		Paragraph srNoP = new Paragraph();
//		srNoP.add(Chunk.NEWLINE);
		srNoP.add(srNoPh);
		srNoP.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell srNoCell = new PdfPCell();
		srNoCell.addElement(srNoP);
		srNoCell.setBorderWidthLeft(0);
		srNoCell.setBorderWidthRight(0);
		
		
		//COL 2
		Phrase empIdPh = new Phrase("Emp. ID", font7);
		Phrase pfNoPh = new Phrase("PF No.", font7);
		Phrase esicNoPh = new Phrase("ESIC No.", font7);
		Phrase payModePh = new Phrase("Pay Mode", font7);
		
		Paragraph empIdP = new Paragraph();
		
		empIdP.add(empIdPh);
		empIdP.add(Chunk.NEWLINE);
		empIdP.add(pfNoPh);
		empIdP.add(Chunk.NEWLINE);
		empIdP.add(esicNoPh);
		empIdP.add(Chunk.NEWLINE);
		empIdP.add(payModePh);
		empIdP.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell empIdCell = new PdfPCell();
		empIdCell.addElement(empIdP);
		empIdCell.setBorderWidthLeft(0);
		empIdCell.setBorderWidthRight(0);
		
		//COL 3
		Phrase empNmPh = new Phrase("Emp. Name", font7);
		Phrase designationPh = new Phrase("Designation", font7);
		Phrase attDetailsPh = new Phrase("Att. Details", font7);
//		Phrase banckPh = new Phrase("Account No.", font7);
		Phrase banckPh = new Phrase("Account/Happay No.", font7);
		
		Paragraph empNmP = new Paragraph();
		
		empNmP.add(empNmPh);
		empNmP.add(Chunk.NEWLINE);
		empNmP.add(designationPh);
		empNmP.add(Chunk.NEWLINE);
		empNmP.add(attDetailsPh);
		empNmP.add(Chunk.NEWLINE);
		empNmP.add(banckPh);
		empNmP.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell empNmCell = new PdfPCell();
		empNmCell.addElement(empNmP);
		empNmCell.setBorderWidthLeft(0);
		empNmCell.setBorderWidthRight(0);
		
		//COL 4
		Phrase daysPh = new Phrase("Days", font7);
		Paragraph daysP = new Paragraph();
		daysP.add(daysPh);
//		daysP.add(Chunk.NEWLINE);
//		daysP.add(designationPh);
		daysP.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell daysCell = new PdfPCell();
		daysCell.addElement(daysP);
		daysCell.setBorderWidthLeft(0);
		daysCell.setBorderWidthRight(0);
		
		//COL 5
		Phrase medicalPh =null;
		Phrase baiscPh = new Phrase("Basic", font7);
		if(hideColumn){
		medicalPh = new Phrase("Arrears Basic", font7);
		}else{
		medicalPh = new Phrase("Medical", font7);
		}
		Phrase monthlyBonusPh = new Phrase("Monthly Bonus", font7);
	
		Paragraph basicP = new Paragraph();
		basicP.add(baiscPh);
		basicP.add(Chunk.NEWLINE);
		basicP.add(medicalPh);
		basicP.add(Chunk.NEWLINE);
		if(isInternal){
			basicP.add(monthlyBonusPh);
		}
		basicP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell basicCell = new PdfPCell();
		basicCell.addElement(basicP);
		basicCell.setBorderWidthLeft(0);
		basicCell.setBorderWidthRight(0);
		
		
		//COL 6
		Phrase paidLeavePh = new Phrase("Paid Leave", font7);
		/*by Amol*/
		Phrase daPh = new Phrase("DA", font7);
//		Phrase washingPf = new Phrase("Washing", font7);
//		Phrase payAdj = new Phrase("Pay Adj (E)", font7);
//		Phrase bnpPh = new Phrase("B and P", font7);
//		Phrase othPh = new Phrase("Other", font7);
		Phrase arrearsDAPh = new Phrase("Arrears DA", font7);
		
		Paragraph plP = new Paragraph();
		if(isInternal){
		plP.add(paidLeavePh);
		plP.add(Chunk.NEWLINE);
		}
		plP.add(daPh);
//		plP.add(washingPf);
		plP.add(Chunk.NEWLINE);
//		plP.add(payAdj);
//		plP.add(othPh);
		plP.add(arrearsDAPh);
		plP.add(Chunk.NEWLINE);
		plP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell plCell = new PdfPCell();
		plCell.addElement(plP);
		plCell.setBorderWidthLeft(0);
		plCell.setBorderWidthRight(0);
		
		
		//COL 7
		Phrase hraPh = new Phrase("HRA", font7);
//		Phrase daPh = new Phrase("DA", font7);
//		Phrase bnpPh = new Phrase("B and P", font7);
//		Phrase arrearsDAPh = new Phrase("Arrears DA", font7);
		Phrase hraArrarsPh = new Phrase("Arrears HRA", font7);
		Phrase travellingPh = new Phrase("Travelling", font7);
		
		Paragraph daP = new Paragraph();
		daP.add(hraPh);
//		daP.add(daPh);
		daP.add(Chunk.NEWLINE);
//		daP.add(bnpPh);
//		daP.add(Chunk.NEWLINE);
//		daP.add(arrearsDAPh);
		daP.add(hraArrarsPh);
		daP.add(Chunk.NEWLINE);
		
		if(isInternal){
			daP.add(travellingPh);
		}
		daP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell daCell = new PdfPCell();
		daCell.addElement(daP);
		daCell.setBorderWidthLeft(0);
		daCell.setBorderWidthRight(0);
		
		//COL 8
		Phrase washingPf = new Phrase("Washing", font7);
//		Phrase hraPh = new Phrase("HRA", font7);
//		Phrase hraArrarsPh = new Phrase("Arrears HRA", font7);
		Phrase othPh =null;
		if(hideColumn){
		othPh = new Phrase("Other Earning", font7);
		}else{
		othPh = new Phrase("Other", font7);
		}
		Phrase otPh = new Phrase("OT", font7);
		Paragraph hraP = new Paragraph();
		
//		hraP.add(hraPh);
		hraP.add(washingPf);
		hraP.add(Chunk.NEWLINE);
//		hraP.add(hraArrarsPh);
		hraP.add(othPh);
		hraP.add(Chunk.NEWLINE);
		if(isInternal){
			hraP.add(otPh);
		}
		hraP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell hraCell = new PdfPCell();
//		hraCell.addElement(washingPf);
		hraCell.addElement(hraP);
		hraCell.setBorderWidthLeft(0);
		hraCell.setBorderWidthRight(0);
		

		//COL 9
		Phrase grossPh = new Phrase("Gross", font7);
		Paragraph grossP = new Paragraph();
		grossP.add(grossPh);
		grossP.add(Chunk.NEWLINE);
		grossP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell grossCell = new PdfPCell();
		grossCell.addElement(grossP);
		grossCell.setBorderWidthLeft(0);
		grossCell.setBorderWidthRight(0);
		

		//COL 10
		Phrase esicPh = new Phrase("ESIC", font7);
		Phrase lwfPh = new Phrase("LWF", font7);
		Paragraph esicP = new Paragraph();
		esicP.add(esicPh);
		esicP.add(Chunk.NEWLINE);
		esicP.add(lwfPh);
		esicP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell esicCell = new PdfPCell();
		esicCell.addElement(esicP);
		esicCell.setBorderWidthLeft(0);
		esicCell.setBorderWidthRight(0);
		

		//COL 10
		Phrase pfPh = new Phrase("PF", font7);
		Phrase advancePh = new Phrase("Advance", font7);
		Paragraph pfP = new Paragraph();
		pfP.add(pfPh);
		pfP.add(Chunk.NEWLINE);
		pfP.add(advancePh);
		pfP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell pfCell = new PdfPCell();
		pfCell.addElement(pfP);
		pfCell.setBorderWidthLeft(0);
		pfCell.setBorderWidthRight(0);
		

		//COL 11
		Phrase ptPh = new Phrase("Ptax", font7);
		Paragraph ptP = new Paragraph();
		ptP.add(ptPh);
		ptP.add(Chunk.NEWLINE);
		ptP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell ptCell = new PdfPCell();
		ptCell.addElement(ptP);
		ptCell.setBorderWidthLeft(0);
		ptCell.setBorderWidthRight(0);
		

		//COL 13
		Phrase totDedPh = new Phrase("Tot Ded.", font7);
		Paragraph totDedP = new Paragraph();
		totDedP.add(totDedPh);
		totDedP.add(Chunk.NEWLINE);
		totDedP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell toDedCell = new PdfPCell();
		toDedCell.addElement(totDedP);
		toDedCell.setBorderWidthLeft(0);
		toDedCell.setBorderWidthRight(0);
		

		//COL 14
		Phrase npPh = new Phrase("Net Payable", font7);
		Paragraph npP = new Paragraph();
		npP.add(npPh);
		npP.add(Chunk.NEWLINE);
		ptP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell npCell = new PdfPCell();
		npCell.addElement(npP);
		npCell.setBorderWidthLeft(0);
		npCell.setBorderWidthRight(0);
		

		//COL 15
		Phrase signaturePh = new Phrase("Signature", font7);
		Paragraph signatureP = new Paragraph();
		signatureP.add(signaturePh);
		signatureP.add(Chunk.NEWLINE);
		ptP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell signatureCell = new PdfPCell();
		signatureCell.addElement(signatureP);
		signatureCell.setBorderWidthLeft(0);
		signatureCell.setBorderWidthRight(0);
		
		
		
		
		
		
		table.addCell(srNoCell);
		table.addCell(empIdCell);
		table.addCell(empNmCell);
		table.addCell(daysCell);
		table.addCell(basicCell);
		
		table.addCell(plCell);
		table.addCell(daCell);
		table.addCell(hraCell);
		table.addCell(grossCell);
		table.addCell(esicCell);
		
		table.addCell(pfCell);
		table.addCell(ptCell);
		table.addCell(toDedCell);
		table.addCell(npCell);
		table.addCell(signatureCell);
		
		
		for(int i=index;i<paySlipList.size();i++){
			
			if(pageCounter==1){
//				System.out.println("FIRST PAGE & PER PAGE LIMIT : "+perPageLimit+" ");
				if(index<paySlipList.size()&&perPageLimit==4){
					nextPageFlag=true;
					break;
				}else{
					nextPageFlag=false;
				}
			}else{
//				System.out.println("PAGE "+pageCounter+" "+"& PER PAGE LIMIT : "+perPageLimit+" ");
				if(index<paySlipList.size()&&perPageLimit==6){
					nextPageFlag=true;
					break;
				}else{
					nextPageFlag=false;
				}
			}
			PaySlip ps=paySlipList.get(i);
			//Row 1
			
			// cell 1 to 3
			Phrase blankPh=new Phrase("",font8);
			PdfPCell blankCell=new PdfPCell(blankPh);
			blankCell.setColspan(3);
			blankCell.setBorder(0);
			table.addCell(blankCell);
			
//			Phrase projectNamePh=new Phrase("Project : "+getProjectName(ps.getEmpid()),font8);
//			PdfPCell projectNameCell=new PdfPCell(projectNamePh);
//			projectNameCell.setColspan(3);
//			projectNameCell.setBorder(0);
//			table.addCell(projectNameCell);
			
			// cell 4 to 5
			Phrase genderPh=new Phrase("Gender : "+ps.getGender(),font7);
			PdfPCell genderCell=new PdfPCell(genderPh);
			genderCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			genderCell.setColspan(2);
			genderCell.setBorder(0);
			
			table.addCell(genderCell);
			
			// cell 6 to 7
			Phrase dojPh=null;
			if(ps.getDoj()!=null){
				dojPh=new Phrase("DOJ : "+fmt.format(ps.getDoj()),font7);
			}else{
				dojPh=new Phrase("DOJ : ",font7 );
			}
			
			PdfPCell dojCell=new PdfPCell(dojPh);
			dojCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			dojCell.setColspan(2);
			dojCell.setBorder(0);
			table.addCell(dojCell);
			
			// cell 8 to 9
			Phrase wrkingShift=null;
			if(ps.getShiftName()!=null){
				wrkingShift=new Phrase("Working Shift : "+ps.getShiftName(),font7);
			}else{
				wrkingShift=new Phrase("Working Shift : ",font7 );
			}
			PdfPCell wrkingCell=new PdfPCell(wrkingShift);
			wrkingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			wrkingCell.setColspan(2);
			wrkingCell.setBorder(0);
			table.addCell(wrkingCell);
			
			// cell 10 to 11
			Phrase dutyHrsPh=null;
			if(ps.getShiftFrom()!=null){
				dutyHrsPh=new Phrase("Duty Hrs.: "+ps.getShiftFrom()+" To "+ps.getShiftTo(),font7);
			}else{
				dutyHrsPh=new Phrase("Duty Hrs.: ",font7 );
			}
			PdfPCell dutyHrsCell=new PdfPCell(dutyHrsPh);
			dutyHrsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			dutyHrsCell.setColspan(2);
			dutyHrsCell.setBorder(0);
			table.addCell(dutyHrsCell);
			
			// cell 12 to 15
			Phrase intervalPh=null;
			intervalPh=new Phrase("Interval Time : ",font7 );
			PdfPCell intervalCell=new PdfPCell(intervalPh);
			intervalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			intervalCell.setColspan(4);
			intervalCell.setBorder(0);
			table.addCell(intervalCell);
			
			//Row 2
			//cell 1 to 3
			
			Phrase blankPh1=new Phrase("",font8);
			PdfPCell blankCell1=new PdfPCell(blankPh1);
			blankCell1.setColspan(3);
			blankCell1.setBorder(0);
			table.addCell(blankCell1);
			
			
			//cell4
			Phrase prDaysPh=new Phrase(ps.getMonthlyDays()+"",font7);
			PdfPCell prDaysCell=new PdfPCell(prDaysPh);
			prDaysCell.setBorder(0);
			prDaysCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(prDaysCell);
			
			//cell 5
			Phrase basicPh=null;
			double basicAmount=getEarningRates("Basic", ps);
			if(basicAmount!=0){
				basicPh=new Phrase(df.format(basicAmount)+"",font7);
			}else{
				basicPh=new Phrase("",font7);
			}
			
			Phrase medPh=null;
			double medicalAmt=getEarningRates("Medical", ps);
			if(medicalAmt!=0){
				medPh=new Phrase(df.format(medicalAmt)+"",font7);
			}else{
				medPh=new Phrase("",font7);
			}
			
			/**Date 10-7-2019 by Amol **/
			Phrase arrearsBasic=null;
			double arrearsBasicAmount=getEarningRates("Arrears Basic", ps);
			if(arrearsBasicAmount!=0){
				arrearsBasic=new Phrase(df.format(arrearsBasicAmount)+"",font7);
			}else{
				arrearsBasic=new Phrase("",font7);
			}
			
			
			
			
			
			
			
			Phrase bonusPh=null;
			double bonusAmt=getBonusRate(ps.getEmpid());
			if(bonusAmt!=0){
				bonusPh=new Phrase(df.format(bonusAmt)+"",font7);
			}else{
				bonusPh=new Phrase("",font7);
			}
			
			Paragraph basicValP = new Paragraph();
			basicValP.add(basicPh);
			basicValP.add(Chunk.NEWLINE);
			if(hideColumn){
			basicValP.add(arrearsBasic);
			}else{
			basicValP.add(medPh);
			}
			basicValP.add(Chunk.NEWLINE);
			if(isInternal){
			basicValP.add(bonusPh);
			}
			basicValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell basicRtCell=new PdfPCell(basicValP);
//			basicRtCell.setBorder(0);
			basicRtCell.setBorderWidthLeft(0);
			basicRtCell.setBorderWidthRight(0);
			basicRtCell.setBorderWidthTop(0);
			basicRtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(basicRtCell);
			
			//cell 6
			Phrase plRtPh=null;
//			double plRtAmt=getEarningRates("Paid Leave", ps);
			double plRtAmt=getPaidLeaveRate(ps.getEmpid());
			if(plRtAmt!=0){
				plRtPh=new Phrase(df.format(plRtAmt)+"",font7);
			}else{
				plRtPh=new Phrase("",font7);
			}
			
//			Phrase washRtPh=null;
//			double washRtAmt=getEarningRates("Washing Allowance", ps);
//			if(washRtAmt!=0){
//				washRtPh=new Phrase(df.format(washRtAmt)+"",font7);
//			}else{
//				washRtPh=new Phrase("",font7);
//			}
			
			Phrase daRtPh=null;
			double daRtAmt=getEarningRates("DA", ps);
			if(daRtAmt!=0){
				daRtPh=new Phrase(df.format(daRtAmt)+"",font7);
			}else{
				daRtPh=new Phrase("",font7);
			}
			
			Phrase arrDaPh=null;
//			double arrDaAmt=getEarningRates("Arrears Da", ps);
			double arrDaAmt=0;
			if(arrDaAmt!=0){
				arrDaPh=new Phrase(df.format(arrDaAmt)+"",font7);
			}else{
				arrDaPh=new Phrase("",font7);
			}
			
			
			
//			Phrase payARtPh=null;
//			double pauRtAgjAmt=getEarningRates("Pay Adjust", ps);
//			if(pauRtAgjAmt!=0){
//				payARtPh=new Phrase(pauRtAgjAmt+"",font7);
//			}else{
//				payARtPh=new Phrase("",font7);
//			}
//			Phrase bnpRtPh=null;
//			double bnpRtAmt=getEarningRates("B AND P", ps);
//			if(bnpRtAmt!=0){
//				bnpRtPh=new Phrase(bnpRtAmt+"",font7);
//			}else{
//				bnpRtPh=new Phrase("",font7);
//			}
			
//			Phrase othAllRtPh=null;
//			double othAllRtAmt=getEarningRates("Other Allowance", ps);
//			if(othAllRtAmt!=0){
//				othAllRtPh=new Phrase(df.format(othAllRtAmt)+"",font7);
//			}else{
//				othAllRtPh=new Phrase("",font7);
//			}
			
			Paragraph plRtP = new Paragraph();
			if(isInternal){
			plRtP.add(plRtPh);
			plRtP.add(Chunk.NEWLINE);
			}
			plRtP.add(daRtPh);
//			plRtP.add(washRtPh);
			plRtP.add(Chunk.NEWLINE);
//			plRtP.add(payARtPh);
//			plRtP.add(othAllRtPh);
			plRtP.add(arrDaPh);
			
			plRtP.add(Chunk.NEWLINE);
			plRtP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell plRtCell=new PdfPCell(plRtP);
//			plRtCell.setBorder(0);
			plRtCell.setBorderWidthLeft(0);
			plRtCell.setBorderWidthRight(0);
			plRtCell.setBorderWidthTop(0);
			plRtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(plRtCell);
			
			//cell 7
//			Phrase daRtPh=null;
//			double daRtAmt=getEarningRates("DA", ps);
//			if(daRtAmt!=0){
//				daRtPh=new Phrase(df.format(daRtAmt)+"",font7);
//			}else{
//				daRtPh=new Phrase("",font7);
//			}
			
			Phrase hraRtPh=null;
			double hraRtAmt=getEarningRates("HRA", ps);
			if(hraRtAmt!=0){
				hraRtPh=new Phrase(df.format(hraRtAmt)+"",font7);
			}else{
				hraRtPh=new Phrase("",font7);
			}
			
			Phrase arrHraPh=null;
//			double arrHraAmt=getEarningRates("Arrears Hra", ps);
			double arrHraAmt=0;
			if(arrHraAmt!=0){
				arrHraPh=new Phrase(df.format(arrHraAmt)+"",font7);
			}else{
				arrHraPh=new Phrase("",font7);
			}
			
			
//			Phrase bnpRtPh=null;
//			double bnpRtAmt=getEarningRates("B AND P", ps);
//			if(bnpRtAmt!=0){
//				bnpRtPh=new Phrase(bnpRtAmt+"",font7);
//			}else{
//				bnpRtPh=new Phrase("",font7);
//			}
			
//			Phrase arrDaPh=null;
////			double arrDaAmt=getEarningRates("Arrears Da", ps);
//			double arrDaAmt=0;
//			if(arrDaAmt!=0){
//				arrDaPh=new Phrase(df.format(arrDaAmt)+"",font7);
//			}else{
//				arrDaPh=new Phrase("",font7);
//			}
			
			Phrase travRtPh=null;
			double travRtAmt=getTravelingAllowanceRate(ps.getEmpid());
			if(travRtAmt!=0){
				travRtPh=new Phrase(df.format(travRtAmt)+"",font7);
			}else{
				travRtPh=new Phrase("",font7);
			}
			
			Paragraph daRtP = new Paragraph();
			daRtP.add(hraRtPh);
//			daRtP.add(daRtPh);
			daRtP.add(Chunk.NEWLINE);
//			daRtP.add(arrDaPh);
			daRtP.add(arrHraPh);
			daRtP.add(Chunk.NEWLINE);
			if(isInternal){
			daRtP.add(travRtPh);
			}
			daRtP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell daRtCell=new PdfPCell(daRtP);
//			daRtCell.setBorder(0);
			daRtCell.setBorderWidthLeft(0);
			daRtCell.setBorderWidthRight(0);
			daRtCell.setBorderWidthTop(0);
			daRtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(daRtCell);
			
			//cell 8
//			Phrase hraRtPh=null;
//			double hraRtAmt=getEarningRates("HRA", ps);
//			if(hraRtAmt!=0){
//				hraRtPh=new Phrase(df.format(hraRtAmt)+"",font7);
//			}else{
//				hraRtPh=new Phrase("",font7);
//			}
			
			Phrase washRtPh=null;
			double washRtAmt=getEarningRates("Washing Allowance", ps);
			if(washRtAmt!=0){
				washRtPh=new Phrase(df.format(washRtAmt)+"",font7);
			}else{
				washRtPh=new Phrase("",font7);
			}
			
			Phrase othAllRtPh=null;
			double othAllRtAmt=getEarningRates("Other Allowance", ps);
			if(othAllRtAmt!=0){
				othAllRtPh=new Phrase(df.format(othAllRtAmt)+"",font7);
			}else{
				othAllRtPh=new Phrase("",font7);
			}
			
			/**Date 10-7-2019 by Amol**/
			Phrase otherEarning=null;
			double otherEarningAmt=getOtherEarningValue("Other Earning", ps);
			if(otherEarningAmt!=0){
				otherEarning=new Phrase("",font7);
			}else{
				otherEarning=new Phrase("",font7);
			}
			
			
			
			
			
			
//			Phrase arrHraPh=null;
////			double arrHraAmt=getEarningRates("Arrears Hra", ps);
//			double arrHraAmt=0;
//			if(arrHraAmt!=0){
//				arrHraPh=new Phrase(df.format(arrHraAmt)+"",font7);
//			}else{
//				arrHraPh=new Phrase("",font7);
//			}
			
			Phrase otRtPh=null;
			double otRtAmt=getEarningRates("OT", ps);
			if(otRtAmt!=0){
				otRtPh=new Phrase(otRtAmt+"",font7);
			}else{
				otRtPh=new Phrase("",font7);
			}
			
			
			Paragraph hraRtP = new Paragraph();
			hraRtP.add(washRtPh);
//			hraRtP.add(hraRtPh);
			hraRtP.add(Chunk.NEWLINE);
//			hraRtP.add(arrHraPh);
			if(hideColumn){
			hraRtP.add(otherEarning);	
			}else{
			hraRtP.add(othAllRtPh);
			}
			hraRtP.add(Chunk.NEWLINE);
			if(isInternal){
			hraRtP.add(otRtPh);
			}
			hraRtP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell hraRtCell=new PdfPCell(hraRtP);
//			hraRtCell.setBorder(0);
			hraRtCell.setBorderWidthLeft(0);
			hraRtCell.setBorderWidthRight(0);
			hraRtCell.setBorderWidthTop(0);
			hraRtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(hraRtCell);
			
			//cell 9
			Phrase grossRtPh=null;
			double grossRtAmt=getEarningRates("Gross Earning", ps);
			if(grossRtAmt!=0){
				grossRtPh=new Phrase(df.format(grossRtAmt)+"",font7);
			}else{
				grossRtPh=new Phrase("",font7);
			}
			
			Phrase grossRate=null;
			double grossRtAm=getEarningRates("Gross Earning", ps);
			if(grossRtAm!=0){
				grossRate=new Phrase("",font7);
			}else{
				grossRate=new Phrase("",font7);
			}
			
			
			
			
			
			Paragraph grossRtP = new Paragraph();
			
			if(hideColumn){
				grossRtP.add(grossRate);
			}else{
				grossRtP.add(grossRtPh);
			}
			
			
			grossRtP.add(Chunk.NEWLINE);
			grossRtP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell grossRtCell=new PdfPCell(grossRtP);
//			grossRtCell.setBorder(0);
			grossRtCell.setBorderWidthLeft(0);
			grossRtCell.setBorderWidthRight(0);
			grossRtCell.setBorderWidthTop(0);
			grossRtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(grossRtCell);
			
			//cell 10 to 15
			Phrase blank6Ph=new Phrase("",font7);
			PdfPCell blank6Cell=new PdfPCell(blank6Ph);
			blank6Cell.setBorder(0);
			blank6Cell.setColspan(6);
			table.addCell(blank6Cell);
			
			
			
			//Row 3
			//cell 1
			Phrase srNoValPh=null;
			srNoValPh=new Phrase((i+1)+"",font7);
			
			Paragraph srNoValP = new Paragraph();
			srNoValP.add(srNoValPh);
			srNoValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell srNoValCell=new PdfPCell(srNoValP);
			srNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			srNoValCell.setBorderWidthLeft(0);
			srNoValCell.setBorderWidthRight(0);
			srNoValCell.setBorderWidthTop(0);
			table.addCell(srNoValCell);
			
			
			//cell 2
			Phrase empIdValPh=null;
			empIdValPh=new Phrase(ps.getEmpid()+"",font7);
			
			Phrase pfNoValPh=null;
			if(ps.getPfNumber()!=null){
				pfNoValPh=new Phrase(ps.getPfNumber()+"",font7);
			}else{
				pfNoValPh=new Phrase("",font7);
			}
			
			Phrase escicNoValPh=null;
			if(ps.getEsicNumber()!=null){
				escicNoValPh=new Phrase(ps.getEsicNumber()+"",font7);
			}else{
				escicNoValPh=new Phrase("",font7);
			}
			
			Phrase payModeValPh=null;
			if(ps.getPayMode()!=null){
				payModeValPh=new Phrase(ps.getPayMode()+"",font7);
			}else{
				payModeValPh=new Phrase("",font7);
			}
			
			Paragraph empIdValP = new Paragraph();
			empIdValP.add(empIdValPh);
			empIdValP.add(Chunk.NEWLINE);
			empIdValP.add(pfNoValPh);
			empIdValP.add(Chunk.NEWLINE);
			empIdValP.add(escicNoValPh);
			empIdValP.add(Chunk.NEWLINE);
			empIdValP.add(payModeValPh);
			empIdValP.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell empIdValCell=new PdfPCell(empIdValP);
			empIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			empIdValCell.setBorderWidthLeft(0);
			empIdValCell.setBorderWidthRight(0);
			empIdValCell.setBorderWidthTop(0);
			table.addCell(empIdValCell);
			
		
			//cell 3
			Phrase empNameValPh=null;
			empNameValPh=new Phrase(ps.getEmployeeName()+"",font7);
			
			Phrase empDesValPh=null;
			if(ps.getEmployeedDesignation()!=null&&!ps.getEmployeedDesignation().equals("")){
				empDesValPh=new Phrase(ps.getEmployeedDesignation(),font7);
			}else{
				empDesValPh=new Phrase("",font7);
			}
			Phrase attDetValPh=null;
//			attDetValPh=new Phrase("P:"+ps.getPaidDays()+" WO:"+ps.getWeekDays()+" AB:"+(ps.getMonthlyDays()-(ps.getEligibleDays()+ps.getWeekDays()))+" OT:"+ps.getOverTimehrs(),font7);
			double presentDays=ps.getPaidDays()-(ps.getWeekDays()+ps.getHoliday());
			
			if(isInternal){
				attDetValPh=new Phrase("P:"+df1.format(presentDays)+" WO:"+df1.format(ps.getWeekDays())+" AB:"+df1.format(ps.getUnpaidDay())+" OT:"+ps.getOverTimehrs(),font7);
			}else{
				attDetValPh=new Phrase("P:"+df1.format(presentDays)+" WO:"+df1.format(ps.getWeekDays())+" AB:"+df1.format(ps.getUnpaidDay()),font7);
			
			}
			
			/**
			 * Date : 11-09-2018 By ANIL
			 */
			
//			Phrase bankAcValPh=null;
//			if(ps.getAccountNumber()!=null){
//				bankAcValPh=new Phrase(ps.getAccountNumber()+"",font7);
//			}else{
//				bankAcValPh=new Phrase("",font7);
//			}
			
			Phrase bankAcValPh=null;
			if(ps.getHappayCardNumber()!=null&&!ps.getHappayCardNumber().equals("")){
				bankAcValPh=new Phrase(ps.getHappayCardNumber()+"",font7);
			}else if(ps.getAccountNumber()!=null){
				bankAcValPh=new Phrase(ps.getAccountNumber()+"",font7);
			}else{
				bankAcValPh=new Phrase("",font7);
			}
			
			/**
			 * End
			 */
			
			Paragraph empNmValP = new Paragraph();
			empNmValP.add(empNameValPh);
			empNmValP.add(Chunk.NEWLINE);
			empNmValP.add(empDesValPh);
			empNmValP.add(Chunk.NEWLINE);
			empNmValP.add(attDetValPh);
			empNmValP.add(Chunk.NEWLINE);
			empNmValP.add(bankAcValPh);
			empNmValP.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell empNmValCell=new PdfPCell(empNmValP);
			empNmValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			empNmValCell.setBorderWidthLeft(0);
			empNmValCell.setBorderWidthRight(0);
			empNmValCell.setBorderWidthTop(0);
			table.addCell(empNmValCell);
		
		
			//cell 4
			Phrase daysValPh=null;
			daysValPh=new Phrase(ps.getPaidDays()+"",font7);
		
			Paragraph daysValP = new Paragraph();
			daysValP.add(daysValPh);
			daysValP.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell daysValCell=new PdfPCell(daysValP);
			daysValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			daysValCell.setBorderWidthLeft(0);
			daysValCell.setBorderWidthRight(0);
			daysValCell.setBorderWidthTop(0);
			table.addCell(daysValCell);
			
			//cell 5

			Phrase baiscValPh=null;
			double basicAmt=getEarningActulValue("Basic", ps);
			if(basicAmt!=0){
				baiscValPh=new Phrase(df.format(basicAmt)+"",font7);
			}else{
				baiscValPh=new Phrase("",font7);
			}
			sumOfBasic+=basicAmt;
			
			Phrase medValPh=null;
			
			if(hideColumn){
			    double medAmt=getEarningActulValue("Arrears Basic", ps);
			    if(medAmt!=0){
			    medValPh=new Phrase(df.format(medAmt)+"",font7);
			    }else{
				medValPh=new Phrase("",font7);
				}
				sumOfArrearsBasic+=medAmt;
				}
			else{
			double medAmt=getEarningActulValue("Medical", ps);
			if(medAmt!=0){
				medValPh=new Phrase(df.format(medAmt)+"",font7);
			}else{
				medValPh=new Phrase("",font7);
			}
			sumOfMedical+=medAmt;
			}
			
			
			
			
			Phrase monBonusPh=null;
//			double bonusAmount=getEarningActulValue("Monthly Bonus", ps);
			double bonusAmount=ps.getBonus();
			if(bonusAmount!=0){
				monBonusPh=new Phrase(df.format(bonusAmount)+"",font7);
			}else{
				monBonusPh=new Phrase("",font7);
			}
			sumOfBonus+=bonusAmount;
		
			
			Paragraph basicValueP = new Paragraph();
			basicValueP.add(baiscValPh);
			basicValueP.add(Chunk.NEWLINE);
			basicValueP.add(medValPh);
			basicValueP.add(Chunk.NEWLINE);
			if(isInternal){
				basicValueP.add(monBonusPh);
			}
			basicValueP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell basicValueCell=new PdfPCell(basicValueP);
			basicValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			basicValueCell.setBorderWidthLeft(0);
			basicValueCell.setBorderWidthRight(0);
			basicValueCell.setBorderWidthTop(0);
			table.addCell(basicValueCell);
		

			//cell 6

			Phrase plValPh=null;
//			double plAmt=getEarningActulValue("Paid Leave", ps);
			double plAmt=ps.getPaidLeaves();
			if(plAmt!=0){
				plValPh=new Phrase(df.format(plAmt)+"",font7);
			}else{
				plValPh=new Phrase("",font7);
			}
			sumOfPl+=plAmt;
			
//			Phrase washValPh=null;
//			double washAmt=getEarningActulValue("Washing Allowance", ps);
//			if(washAmt!=0){
//				washValPh=new Phrase(df.format(washAmt)+"",font7);
//			}else{
//				washValPh=new Phrase("",font7);
//			}
//			sumOfWashingAllowance+=washAmt;
			
//			Phrase payAdjustPh=null;
//			double payAdjAmt=getEarningActulValue("Pay Adjust", ps);
//			if(payAdjAmt!=0){
//				payAdjustPh=new Phrase(payAdjAmt+"",font7);
//			}else{
//				payAdjustPh=new Phrase("",font7);
//			}
//			sumOfPayAdjust+=payAdjAmt;
			
//			Phrase bAndPPh=null;
//			double bandPAmt=getEarningActulValue("B AND P", ps);
//			if(bandPAmt!=0){
//				bAndPPh=new Phrase(bandPAmt+"",font7);
//			}else{
//				bAndPPh=new Phrase("",font7);
//			}
//			sumOfBnP+=bandPAmt;
			
//			Phrase othAllVlPh=null;
//			double othAllAmt=getEarningActulValue("Other Allowance", ps);
//			if(othAllAmt!=0){
//				othAllVlPh=new Phrase(df.format(othAllAmt)+"",font7);
//			}else{
//				othAllVlPh=new Phrase("",font7);
//			}
//			sumOfOtherAllowance+=othAllAmt;
			
			Phrase daValPh=null;
			double daAmt=getEarningActulValue("DA", ps);
			if(daAmt!=0){
				daValPh=new Phrase(df.format(daAmt)+"",font7);
			}else{
				daValPh=new Phrase("",font7);
			}
			sumOfDa+=daAmt;
			
			
			Phrase arrDap=null;
			double arrAmtDa=getEarningActulValue("Arrears Da", ps);
			if(arrAmtDa!=0){
				arrDap=new Phrase(df.format(arrAmtDa)+"",font7);
			}else{
				arrDap=new Phrase("",font7);
			}
			sumOfArrearsDa+=arrAmtDa;
			
			
			
			
			
			Paragraph plValP = new Paragraph();
			if(isInternal){
				plValP.add(plValPh);
				plValP.add(Chunk.NEWLINE);
			}
//			plValP.add(washValPh);
			plValP.add(daValPh);
			plValP.add(Chunk.NEWLINE);
//			plValP.add(payAdjustPh);
//			plValP.add(othAllVlPh);
			plValP.add(arrDap);
			plValP.add(Chunk.NEWLINE);
			plValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell plValCell=new PdfPCell(plValP);
			plValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			plValCell.setBorderWidthLeft(0);
			plValCell.setBorderWidthRight(0);
			plValCell.setBorderWidthTop(0);
			table.addCell(plValCell);


			
			//cell 7

//			Phrase daValPh=null;
//			double daAmt=getEarningActulValue("DA", ps);
//			if(daAmt!=0){
//				daValPh=new Phrase(df.format(daAmt)+"",font7);
//			}else{
//				daValPh=new Phrase("",font7);
//			}
//			sumOfDa+=daAmt;
			
//			Phrase bAndPPh=null;
//			double bandPAmt=getEarningActulValue("B AND P", ps);
//			if(bandPAmt!=0){
//				bAndPPh=new Phrase(bandPAmt+"",font7);
//			}else{
//				bAndPPh=new Phrase("",font7);
//			}
//			sumOfBnP+=bandPAmt;
			
//			Phrase arrDap=null;
//			double arrAmtDa=getEarningActulValue("Arrears Da", ps);
//			if(arrAmtDa!=0){
//				arrDap=new Phrase(df.format(arrAmtDa)+"",font7);
//			}else{
//				arrDap=new Phrase("",font7);
//			}
//			sumOfArrearsDa+=arrAmtDa;
			//
			Phrase hraValPh=null;
			double hraAmt=getEarningActulValue("HRA", ps);
			if(hraAmt!=0){
				hraValPh=new Phrase(df.format(hraAmt)+"",font7);
			}else{
				hraValPh=new Phrase("",font7);
			}
			sumOfHra+=hraAmt;
			
			
			Phrase arrHrap=null;
			double arrAmtHra=getEarningActulValue("Arrears Hra", ps);
			if(arrAmtHra!=0){
				arrHrap=new Phrase(df.format(arrAmtHra)+"",font7);
			}else{
				arrHrap=new Phrase("",font7);
			}
			sumOfArrearsHra+=arrAmtHra;
			
			
			Phrase travValPh=null;
//			double travValAmt=getEarningActulValue("Travelling Allowance", ps);
			double travValAmt=getTravellingAllowanceValue("Travelling Allowance", ps);
			if(travValAmt!=0){
				travValPh=new Phrase(df.format(travValAmt)+"",font7);
			}else{
				travValPh=new Phrase("",font7);
			}
			sumOfTravelling+=travValAmt;
			
			//
			
			Paragraph daValP = new Paragraph();
//			daValP.add(daValPh);
			daValP.add(hraValPh);
			daValP.add(Chunk.NEWLINE);
//			daValP.add(arrDap);
			daValP.add(arrHrap);
			daValP.add(Chunk.NEWLINE);
			if(isInternal){
				daValP.add(travValPh);
			}
			daValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell daValCell=new PdfPCell(daValP);
			daValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			daValCell.setBorderWidthLeft(0);
			daValCell.setBorderWidthRight(0);
			daValCell.setBorderWidthTop(0);
			table.addCell(daValCell);


			//cell 8
//			Phrase hraValPh=null;
//			double hraAmt=getEarningActulValue("HRA", ps);
//			if(hraAmt!=0){
//				hraValPh=new Phrase(df.format(hraAmt)+"",font7);
//			}else{
//				hraValPh=new Phrase("",font7);
//			}
//			sumOfHra+=hraAmt;
//			
//			Phrase arrHrap=null;
//			double arrAmtHra=getEarningActulValue("Arrears Hra", ps);
//			if(arrAmtHra!=0){
//				arrHrap=new Phrase(df.format(arrAmtHra)+"",font7);
//			}else{
//				arrHrap=new Phrase("",font7);
//			}
//			sumOfArrearsHra+=arrAmtHra;
			
			Phrase washValPh=null;
			double washAmt=getEarningActulValue("Washing Allowance", ps);
			if(washAmt!=0){
				washValPh=new Phrase(df.format(washAmt)+"",font7);
			}else{
				washValPh=new Phrase("",font7);
			}
			sumOfWashingAllowance+=washAmt;
			
			Phrase othAllVlPh=null;
			if(hideColumn){
				double othAllAmt=getOtherEarningValue("Other Earning", ps);
				
				if(othAllAmt!=0){
					othAllVlPh=new Phrase(othAllAmt+"",font7);
				}else{
					othAllVlPh=new Phrase("",font7);
				}
				sumofOtherEarn+=othAllAmt;
				
			}else{
				double othAllAmt=getEarningActulValue("Other Allowance", ps);
				if(othAllAmt!=0){
					othAllVlPh=new Phrase(df.format(othAllAmt)+"",font7);
				}else{
					othAllVlPh=new Phrase("",font7);
				}
				sumOfOtherAllowance+=othAllAmt;
				
			}
			
			Phrase otValPh=null;
//			double bandPAmt=getEarningActulValue("B AND P", ps);
			double otAmt=ps.getOvertimeSalary();
			if(otAmt!=0){
				otValPh=new Phrase(df.format(otAmt)+"",font7);
			}else{
				otValPh=new Phrase("",font7);
			}
			sumOfOt+=otAmt;
			
			Paragraph hraValP = new Paragraph();
//			hraValP.add(hraValPh);
			hraValP.add(washValPh);
			hraValP.add(Chunk.NEWLINE);
//			hraValP.add(arrHrap);
			hraValP.add(othAllVlPh);
			hraValP.add(Chunk.NEWLINE);
			if(isInternal){
			hraValP.add(otValPh);
			}
			hraValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell hraValCell=new PdfPCell(hraValP);
			hraValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			hraValCell.setBorderWidthLeft(0);
			hraValCell.setBorderWidthRight(0);
			hraValCell.setBorderWidthTop(0);
			table.addCell(hraValCell);


			//cell 9
			Phrase grossValPh=null;
			double grossAmount=ps.getGrossEarningWithoutOT();
			if(grossAmount!=0){
				grossValPh=new Phrase(df.format(grossAmount)+"",font7);
			}else{
				grossValPh=new Phrase("",font7);
			}
			sumOfGross+=grossAmount;
			
			Paragraph grossValP = new Paragraph();
			grossValP.add(grossValPh);
			grossValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell grossValCell=new PdfPCell(grossValP);
			grossValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			grossValCell.setBorderWidthLeft(0);
			grossValCell.setBorderWidthRight(0);
			grossValCell.setBorderWidthTop(0);
			table.addCell(grossValCell);

			
			//cell 10
			Phrase esicValPh=null;
			double esicAmt=getDeductionActulValue("ESIC", ps);
			if(esicAmt!=0){
				esicValPh=new Phrase(df.format(esicAmt)+"",font7);
			}else{
				esicValPh=new Phrase("",font7);
			}
			sumOfEsic+=esicAmt;
			
			Phrase lwfValPh=null;
//			double bandPAmt=getEarningActulValue("B AND P", ps);
			double lwfAmt=getDeductionActulValue("LWF", ps);
			if(lwfAmt!=0){
				lwfValPh=new Phrase(df.format(lwfAmt)+"",font7);
			}else{
				lwfValPh=new Phrase("",font7);
			}
			sumOfLwf+=lwfAmt;
			
			Paragraph esicValP = new Paragraph();
			esicValP.add(esicValPh);
			esicValP.add(Chunk.NEWLINE);
			esicValP.add(lwfValPh);
			esicValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell esicValCell=new PdfPCell(esicValP);
			esicValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			esicValCell.setBorderWidthLeft(0);
			esicValCell.setBorderWidthRight(0);
			esicValCell.setBorderWidthTop(0);
			table.addCell(esicValCell);

			//cell 11
			Phrase pfValPh=null;
			double pfAmt=getDeductionActulValue("PF", ps);
			if(pfAmt!=0){
				pfValPh=new Phrase(df.format(pfAmt)+"",font7);
			}else{
				pfValPh=new Phrase("",font7);
			}
			sumOfPf+=pfAmt;
			
			Phrase advValPh=null;
			double advAmt=getDeductionActulValue("ADVANCE", ps);
			if(advAmt!=0){
				advValPh=new Phrase(df.format(advAmt)+"",font7);
			}else{
				advValPh=new Phrase("",font7);
			}
			sumOfAdavance+=advAmt;
			
			Paragraph pfValP = new Paragraph();
			pfValP.add(pfValPh);
			pfValP.add(Chunk.NEWLINE);
			pfValP.add(advValPh);
			pfValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell pfValCell=new PdfPCell(pfValP);
			pfValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			pfValCell.setBorderWidthLeft(0);
			pfValCell.setBorderWidthRight(0);
			pfValCell.setBorderWidthTop(0);
			table.addCell(pfValCell);
		
			//cell 12
			Phrase ptValPh=null;
			double ptAmt=getDeductionActulValue("PT", ps);
			if(ptAmt!=0){
				ptValPh=new Phrase(df.format(ptAmt)+"",font7);
			}else{
				ptValPh=new Phrase("",font7);
			}
			sumOfPt+=ptAmt;
			
			
			Paragraph ptValP = new Paragraph();
			ptValP.add(ptValPh);
			ptValP.add(Chunk.NEWLINE);
			//ptValP.add(advValPh);
			ptValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell ptValCell=new PdfPCell(ptValP);
			ptValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			ptValCell.setBorderWidthLeft(0);
			ptValCell.setBorderWidthRight(0);
			ptValCell.setBorderWidthTop(0);
			table.addCell(ptValCell);
		
			//cell 13
			Phrase totalDedPh=null;
			double totDedAmt=ps.getTotalDeduction();
			if(totDedAmt!=0){
				totalDedPh=new Phrase(df.format(totDedAmt)+"",font7);
			}else{
				totalDedPh=new Phrase("",font7);
			}
			sumOfTotalDed+=totDedAmt;
			
			
			Paragraph totalDedP = new Paragraph();
			totalDedP.add(totalDedPh);
			totalDedP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell totalDedCell=new PdfPCell(totalDedP);
			totalDedCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			totalDedCell.setBorderWidthLeft(0);
			totalDedCell.setBorderWidthRight(0);
			totalDedCell.setBorderWidthTop(0);
			table.addCell(totalDedCell);
		
			//cell 14
			Phrase npValPh=null;
			double npAmt=0;
			if(hideColumn){
			npAmt=ps.getNetEarning();
			}else{
			npAmt=ps.getNetEarningWithoutOT();
			}
			
			if(npAmt!=0){
				npValPh=new Phrase(df.format(npAmt)+"",font7);
			}else{
				npValPh=new Phrase("",font7);
			}
			
			sumOfNetEarning+=npAmt;
			
			Paragraph npValP = new Paragraph();
			npValP.add(npValPh);
			npValP.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell npValCell=new PdfPCell(npValP);
			npValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			npValCell.setBorderWidthLeft(0);
			npValCell.setBorderWidthRight(0);
			npValCell.setBorderWidthTop(0);
			table.addCell(npValCell);
			
			//cell 15
			
			Phrase blank1Ph=new Phrase("",font8);
			PdfPCell blank1Cell=new PdfPCell(blank1Ph);
			blank1Cell.setBorderWidthLeft(0);
			blank1Cell.setBorderWidthRight(0);
			blank1Cell.setBorderWidthTop(0);
		
			table.addCell(blank1Cell);
			
			perPageLimit++;
			index++;
		}
		
		
		if(index==paySlipList.size()){
			if(pageCounter==1){
				if(perPageLimit<4){
					addGrandTotal(table);
					nextPageFlag=false;
				}else{
					nextPageFlag=true;
				}
			}else{
				if(perPageLimit<6){
					addGrandTotal(table);
					nextPageFlag=false;
				}else{
					nextPageFlag=true;
				}
			}
		}
		
		
		try {
			table.setSpacingBefore(10f);
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		if(nextPageFlag){
			System.out.println("INSIDE NEXT PAGE ");
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			createPaySlipStatementTable();
		}

	}

	
	private double getTravellingAllowanceValue(String traAll, PaySlip ps) {
		// TODO Auto-generated method stub
		double amount = 0;
		
		if(ps.getEmployeeCTCList()!=null&&ps.getEmployeeCTCList().size()!=0){
			for(EmployeeCTCTemplate temp:ps.getEmployeeCTCList()){
				if(temp.getCtcTemplateName().trim().equalsIgnoreCase(traAll)){
					amount=temp.getAmount();
					break;
				}
			}
		}
		return amount;
	}

	private void addGrandTotal(PdfPTable table) {
		

		//cell 1 to 4
		Phrase grandTotPh1=null;
		grandTotPh1=new Phrase("",font7);
		
		Paragraph grandTotP1 = new Paragraph();
		grandTotP1.add(grandTotPh1);
		grandTotP1.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell grandTotCell1=new PdfPCell(grandTotP1);
		grandTotCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		grandTotCell1.setBorderWidthLeft(0);
		grandTotCell1.setBorderWidthRight(0);
		grandTotCell1.setBorderWidthTop(0);
		grandTotCell1.setColspan(4);
		table.addCell(grandTotCell1);
		
		
		//cell 5

		Phrase baiscValPh1=null;
		baiscValPh1=new Phrase("Basic",font7);
		Phrase medValPh1=null;
		medValPh1=new Phrase("Medical",font7);
		/**by Amol**/
		Phrase arrearsBasicc=null;
		arrearsBasicc=new Phrase("Arrears Basic",font7);
		Phrase monBonusPh1=null;
		monBonusPh1=new Phrase("Monthly Bonus",font7);
	
		
		Paragraph basicValueP1 = new Paragraph();
		basicValueP1.add(baiscValPh1);
		basicValueP1.add(Chunk.NEWLINE);
		if(hideColumn){
			basicValueP1.add(arrearsBasicc);
		}else{
			basicValueP1.add(medValPh1);
		}
		
		basicValueP1.add(Chunk.NEWLINE);
		if(isInternal){
			basicValueP1.add(monBonusPh1);
		}
		basicValueP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell basicValueCell1=new PdfPCell(basicValueP1);
		basicValueCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		basicValueCell1.setBorderWidthLeft(0);
		basicValueCell1.setBorderWidthRight(0);
		basicValueCell1.setBorderWidthTop(0);
		table.addCell(basicValueCell1);
	

		//cell 6
		Phrase daValPh1=null;
		daValPh1=new Phrase("DA",font7);
		
		Phrase plValPh1=null;
		plValPh1=new Phrase("Paid Leave",font7);
		
//		Phrase washValPh1=null;
//		
//		washValPh1=new Phrase("Washing",font7);
		
//		Phrase payAdjustPh1=null;
//		payAdjustPh1=new Phrase("Pay Adjust",font7);
		
//		Phrase bAndPPh1=null;
//		bAndPPh1=new Phrase("B AND P",font7);
		
//		Phrase othAllPh=null;
//		othAllPh=new Phrase("Other",font7);
		
		Phrase arrdaValPh1=null;
		arrdaValPh1=new Phrase("Arrears DA",font7);
		
		
		
		Paragraph plValP1 = new Paragraph();
		if(isInternal){
			plValP1.add(plValPh1);
			plValP1.add(Chunk.NEWLINE);
		}
		plValP1.add(daValPh1);
//		plValP1.add(washValPh1);
		plValP1.add(Chunk.NEWLINE);
//		plValP1.add(payAdjustPh1);
//		plValP1.add(othAllPh);
		plValP1.add(arrdaValPh1);
		plValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell plValCell1=new PdfPCell(plValP1);
		plValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		plValCell1.setBorderWidthLeft(0);
		plValCell1.setBorderWidthRight(0);
		plValCell1.setBorderWidthTop(0);
		table.addCell(plValCell1);


		
		//cell 7

//		Phrase daValPh1=null;
//		daValPh1=new Phrase("DA",font7);
		Phrase hraValPh1=null;
		hraValPh1=new Phrase("HRA",font7);
		
//		Phrase arrdaValPh1=null;
//		arrdaValPh1=new Phrase("Arrears DA",font7);
		
//		Phrase bAndPPh1=null;
//		bAndPPh1=new Phrase("B AND P",font7);
		Phrase arrhraValPh1=null;
		arrhraValPh1=new Phrase("Arrears HRA",font7);
		
		//
		Phrase travValPh1=null;
		travValPh1=new Phrase("Travelling",font7);
		
		//
		
		Paragraph daValP1 = new Paragraph();
		daValP1.add(hraValPh1);
//		daValP1.add(daValPh1);
		daValP1.add(Chunk.NEWLINE);
//		daValP1.add(bAndPPh1);
//		daValP1.add(arrdaValPh1);
		daValP1.add(arrhraValPh1);
		daValP1.add(Chunk.NEWLINE);
		if(isInternal){
			daValP1.add(travValPh1);
		}
		daValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell daValCell1=new PdfPCell(daValP1);
		daValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		daValCell1.setBorderWidthLeft(0);
		daValCell1.setBorderWidthRight(0);
		daValCell1.setBorderWidthTop(0);
		table.addCell(daValCell1);


		//cell 8
//		Phrase hraValPh1=null;
//			hraValPh1=new Phrase("HRA",font7);
        Phrase washValPh1=null;
		washValPh1=new Phrase("Washing",font7);
			
//		Phrase arrhraValPh1=null;
//		arrhraValPh1=new Phrase("Arrears HRA",font7);
		
		Phrase othAllPh=null;
		othAllPh=new Phrase("Other",font7);
		
		/**by Amol***/
		Phrase otherEarn=null;
		otherEarn=new Phrase("Other Earning",font7);
		
		
		Phrase otValPh1=null;
			otValPh1=new Phrase("OT",font7);
		
		Paragraph hraValP1 = new Paragraph();
		hraValP1.add(washValPh1);
//		hraValP1.add(hraValPh1);
		hraValP1.add(Chunk.NEWLINE);
//		hraValP1.add(arrhraValPh1);
		if(hideColumn){
		hraValP1.add(otherEarn);
		}else{
		hraValP1.add(othAllPh);
		}
		hraValP1.add(Chunk.NEWLINE);
		if(isInternal){
		hraValP1.add(otValPh1);
		}
		hraValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell hraValCell1=new PdfPCell(hraValP1);
		hraValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		hraValCell1.setBorderWidthLeft(0);
		hraValCell1.setBorderWidthRight(0);
		hraValCell1.setBorderWidthTop(0);
		table.addCell(hraValCell1);


		//cell 9
		Phrase grossValPh1=null;
			grossValPh1=new Phrase("Gross",font7);
		
		Paragraph grossValP1 = new Paragraph();
		grossValP1.add(grossValPh1);
		grossValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell grossValCell1=new PdfPCell(grossValP1);
		grossValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		grossValCell1.setBorderWidthLeft(0);
		grossValCell1.setBorderWidthRight(0);
		grossValCell1.setBorderWidthTop(0);
		table.addCell(grossValCell1);

		
		//cell 10
		Phrase esicValPh1=null;
			esicValPh1=new Phrase("ESIC",font7);
		
		Phrase lwfValPh1=null;
			lwfValPh1=new Phrase("LWF",font7);
		
		Paragraph esicValP1 = new Paragraph();
		esicValP1.add(esicValPh1);
		esicValP1.add(Chunk.NEWLINE);
		esicValP1.add(lwfValPh1);
		esicValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell esicValCell1=new PdfPCell(esicValP1);
		esicValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		esicValCell1.setBorderWidthLeft(0);
		esicValCell1.setBorderWidthRight(0);
		esicValCell1.setBorderWidthTop(0);
		table.addCell(esicValCell1);

		//cell 11
		Phrase pfValPh1=null;
			pfValPh1=new Phrase("PF",font7);
		
		Phrase advValPh1=null;
			advValPh1=new Phrase("Advance",font7);
		
		Paragraph pfValP1 = new Paragraph();
		pfValP1.add(pfValPh1);
		pfValP1.add(Chunk.NEWLINE);
		pfValP1.add(advValPh1);
		pfValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell pfValCell1=new PdfPCell(pfValP1);
		pfValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pfValCell1.setBorderWidthLeft(0);
		pfValCell1.setBorderWidthRight(0);
		pfValCell1.setBorderWidthTop(0);
		table.addCell(pfValCell1);
	
		//cell 12
		Phrase ptValPh1=null;
			ptValPh1=new Phrase("PTax",font7);
		
		
		Paragraph ptValP1 = new Paragraph();
		ptValP1.add(ptValPh1);
		ptValP1.add(Chunk.NEWLINE);
//		ptValP1.add(advValPh1);
		ptValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell ptValCell1=new PdfPCell(ptValP1);
		ptValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		ptValCell1.setBorderWidthLeft(0);
		ptValCell1.setBorderWidthRight(0);
		ptValCell1.setBorderWidthTop(0);
		table.addCell(ptValCell1);
	
		//cell 13
		Phrase totalDedPh1=null;
			totalDedPh1=new Phrase("Total Deduction",font7);
		
		
		Paragraph totalDedP1 = new Paragraph();
		totalDedP1.add(totalDedPh1);
		totalDedP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell totalDedCell1=new PdfPCell(totalDedP1);
		totalDedCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalDedCell1.setBorderWidthLeft(0);
		totalDedCell1.setBorderWidthRight(0);
		totalDedCell1.setBorderWidthTop(0);
		table.addCell(totalDedCell1);
	
		//cell 14
		Phrase npValPh1=null;
			npValPh1=new Phrase("Net Payable",font7);
		
		Paragraph npValP1 = new Paragraph();
		npValP1.add(npValPh1);
		npValP1.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell npValCell1=new PdfPCell(npValP1);
		npValCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		npValCell1.setBorderWidthLeft(0);
		npValCell1.setBorderWidthRight(0);
		npValCell1.setBorderWidthTop(0);
		table.addCell(npValCell1);
		
		//cell 15
		 
		Phrase blank1Ph1=new Phrase("",font8);
		PdfPCell blank1Cell1=new PdfPCell(blank1Ph1);
		blank1Cell1.setBorderWidthLeft(0);
		blank1Cell1.setBorderWidthRight(0);
		blank1Cell1.setBorderWidthTop(0);
	
		table.addCell(blank1Cell1);
		
		/**
		 * 
		 */
		
		
		//cell 1 to 4
		Phrase grandTotPh=null;
		grandTotPh=new Phrase("Grand Total",font7);
		
		Paragraph grandTotP = new Paragraph();
		grandTotP.add(grandTotPh);
		grandTotP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell grandTotCell=new PdfPCell(grandTotP);
		grandTotCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		grandTotCell.setBorderWidthLeft(0);
		grandTotCell.setBorderWidthRight(0);
		grandTotCell.setBorderWidthTop(0);
		grandTotCell.setColspan(4);
		table.addCell(grandTotCell);
		
		
		//cell 5

		Phrase baiscValPh=null;
//				double basicAmt=getEarningActulValue("Basic", ps);
		if(sumOfBasic!=0){
			baiscValPh=new Phrase(df.format(sumOfBasic)+"",font7);
		}else{
			baiscValPh=new Phrase("",font7);
		}
//				sumOfBasic+=basicAmt;
		
		Phrase medValPh=null;
//				double medAmt=getEarningActulValue("Medical", ps);
		if(sumOfMedical!=0){
			medValPh=new Phrase(df.format(sumOfMedical)+"",font7);
		}else{
			medValPh=new Phrase("",font7);
		}
		
		Phrase arrearsbas=null;
//		double medAmt=getEarningActulValue("Medical", ps);
        if(sumOfArrearsBasic!=0){
        	arrearsbas=new Phrase(df.format(sumOfArrearsBasic)+"",font7);
         }else{
        	 arrearsbas=new Phrase("",font7);
         }
		
		
//				sumOfMedical+=medAmt;
		
		Phrase monBonusPh=null;
//				double bonusAmount=getEarningActulValue("Monthly Bonus", ps);
//				double bonusAmount=ps.getBonus();
		if(sumOfBonus!=0){
			monBonusPh=new Phrase(df.format(sumOfBonus)+"",font7);
		}else{
			monBonusPh=new Phrase("",font7);
		}
//				sumOfBonus+=bonusAmount;
	
		
		Paragraph basicValueP = new Paragraph();
		basicValueP.add(baiscValPh);
		basicValueP.add(Chunk.NEWLINE);
		if(hideColumn){
		basicValueP.add(arrearsbas);
		}
		basicValueP.add(medValPh);
		basicValueP.add(Chunk.NEWLINE);
		if(isInternal){
			basicValueP.add(monBonusPh);
		}
		basicValueP.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell basicValueCell=new PdfPCell(basicValueP);
		basicValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		basicValueCell.setBorderWidthLeft(0);
		basicValueCell.setBorderWidthRight(0);
		basicValueCell.setBorderWidthTop(0);
		table.addCell(basicValueCell);
	

		//cell 6

		Phrase plValPh=null;
//				double plAmt=getEarningActulValue("Paid Leave", ps);
//				double plAmt=ps.getPaidLeaves();
		if(sumOfPl!=0){
			plValPh=new Phrase(df.format(sumOfPl)+"",font7);
		}else{
			plValPh=new Phrase("",font7);
		}
//				sumOfPl+=plAmt;
		
//		Phrase washValPh=null;
////				double washAmt=getEarningActulValue("Washing Allowance", ps);
//		if(sumOfWashingAllowance!=0){
//			washValPh=new Phrase(df.format(sumOfWashingAllowance)+"",font7);
//		}else{
//			washValPh=new Phrase("",font7);
//		}
		Phrase daValPh=null;
		if(sumOfDa!=0){
			daValPh=new Phrase(df.format(sumOfDa)+"",font7);
		}else{
			daValPh=new Phrase("",font7);
		}
		
	
		
//				sumOfWashingAllowance+=sumOfWashingAllowance;
		
//		Phrase payAdjustPh=null;
////				double payAdjAmt=getEarningActulValue("Pay Adjust", ps);
//		if(sumOfPayAdjust!=0){
//			payAdjustPh=new Phrase(df.format(sumOfPayAdjust)+"",font7);
//		}else{
//			payAdjustPh=new Phrase("",font7);
//		}
////				sumOfPayAdjust+=payAdjAmt;
//		Phrase bAndPPh=null;
//		//		double bandPAmt=getEarningActulValue("B AND P", ps);
//		if(sumOfBnP!=0){
//			bAndPPh=new Phrase(df.format(sumOfBnP)+"",font7);
//		}else{
//			bAndPPh=new Phrase("",font7);
//		}
//		//		sumOfBnP+=bandPAmt;
		
//		Phrase otherAllPh=null;
//		if(sumOfOtherAllowance!=0){
//			otherAllPh=new Phrase(df.format(sumOfOtherAllowance)+"",font7);
//		}else{
//			otherAllPh=new Phrase("",font7);
//		}
		
		Phrase arrdaValPh=null;
//		double daAmt=getEarningActulValue("DA", ps);
        if(sumOfArrearsDa!=0){
	    arrdaValPh=new Phrase(df.format(sumOfArrearsDa)+"",font7);
        }else{
	     arrdaValPh=new Phrase("",font7);
         }
	
		
		Paragraph plValP = new Paragraph();
		if(isInternal){
			plValP.add(plValPh);
			plValP.add(Chunk.NEWLINE);
		}
		
		plValP.add(daValPh);
//		plValP.add(washValPh);
		plValP.add(Chunk.NEWLINE);
//		plValP.add(payAdjustPh);
//		plValP.add(otherAllPh);
		plValP.add(arrdaValPh);
		plValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell plValCell=new PdfPCell(plValP);
		plValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		plValCell.setBorderWidthLeft(0);
		plValCell.setBorderWidthRight(0);
		plValCell.setBorderWidthTop(0);
		table.addCell(plValCell);


		
		//cell 7

//		Phrase daValPh=null;
////				double daAmt=getEarningActulValue("DA", ps);
//		if(sumOfDa!=0){
//			daValPh=new Phrase(df.format(sumOfDa)+"",font7);
//		}else{
//			daValPh=new Phrase("",font7);
//		}
		Phrase hraValPh=null;
		if(sumOfHra!=0){
			hraValPh=new Phrase(df.format(sumOfHra)+"",font7);
		}else{
			hraValPh=new Phrase("",font7);
		}
//				sumOfDa+=daAmt;
		
		Phrase arrhraValPh=null;
//		double hraAmt=getEarningActulValue("HRA", ps);
        if(sumOfArrearsHra!=0){
	    arrhraValPh=new Phrase(df.format(sumOfArrearsHra)+"",font7);
        }else{
	    arrhraValPh=new Phrase("",font7);
        }

//		Phrase arrdaValPh=null;
////				double daAmt=getEarningActulValue("DA", ps);
//		if(sumOfArrearsDa!=0){
//			arrdaValPh=new Phrase(df.format(sumOfArrearsDa)+"",font7);
//		}else{
//			arrdaValPh=new Phrase("",font7);
//		}
//				sumOfDa+=daAmt;
		
		
		
		//
		Phrase travValPh=null;
//				double travValAmt=getEarningActulValue("Travelling Allowance", ps);
		if(sumOfTravelling!=0){
			travValPh=new Phrase(df.format(sumOfTravelling)+"",font7);
		}else{
			travValPh=new Phrase("",font7);
		}
//				sumOfTravelling+=travValAmt;
		
		//
		
		Paragraph daValP = new Paragraph();
		daValP.add(hraValPh);
//		daValP.add(daValPh);
		daValP.add(Chunk.NEWLINE);
//		daValP.add(bAndPPh);
//		daValP.add(arrdaValPh);
		daValP.add(arrhraValPh);
		daValP.add(Chunk.NEWLINE);
		if(isInternal){
			daValP.add(travValPh);
		}
		daValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell daValCell=new PdfPCell(daValP);
		daValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		daValCell.setBorderWidthLeft(0);
		daValCell.setBorderWidthRight(0);
		daValCell.setBorderWidthTop(0);
		table.addCell(daValCell);


		//cell 8
		
//		Phrase hraValPh=null;
////				double hraAmt=getEarningActulValue("HRA", ps);
//		if(sumOfHra!=0){
//			hraValPh=new Phrase(df.format(sumOfHra)+"",font7);
//		}else{
//			hraValPh=new Phrase("",font7);
//		}
		
		Phrase washValPh=null;
//		double washAmt=getEarningActulValue("Washing Allowance", ps);
		
        if(sumOfWashingAllowance!=0){
	    washValPh=new Phrase(df.format(sumOfWashingAllowance)+"",font7);
        }else{
	    washValPh=new Phrase("",font7);
        }
        
        Phrase otherAllPh=null;
		if(sumOfOtherAllowance!=0){
			otherAllPh=new Phrase(df.format(sumOfOtherAllowance)+"",font7);
		}else{
			otherAllPh=new Phrase("",font7);
		}
        
		 
        Phrase otherearnAll=null;
		if(sumofOtherEarn!=0){
			otherearnAll=new Phrase(df.format(sumofOtherEarn)+"",font7);
		}else{
			otherearnAll=new Phrase("",font7);
		}
		
		
		
//				sumOfHra+=hraAmt;
		

//		Phrase arrhraValPh=null;
////				double hraAmt=getEarningActulValue("HRA", ps);
//		if(sumOfArrearsHra!=0){
//			arrhraValPh=new Phrase(df.format(sumOfArrearsHra)+"",font7);
//		}else{
//			arrhraValPh=new Phrase("",font7);
//		}
//				sumOfHra+=hraAmt;
        
        
        
        
		
		Phrase otValPh=null;
//				double bandPAmt=getEarningActulValue("B AND P", ps);
//				double otAmt=ps.getOvertimeSalary();
		if(sumOfOt!=0){
			otValPh=new Phrase(df.format(sumOfOt)+"",font7);
		}else{
			otValPh=new Phrase("",font7);
		}
//				sumOfOt+=otAmt;
		
		Paragraph hraValP = new Paragraph();
		hraValP.add(washValPh);
//		hraValP.add(hraValPh);
		hraValP.add(Chunk.NEWLINE);
//		hraValP.add(arrhraValPh);
		
		if(hideColumn){
		hraValP.add(otherearnAll);
		}else{
		hraValP.add(otherAllPh);
		}
		hraValP.add(Chunk.NEWLINE);
		if(isInternal){
		hraValP.add(otValPh);
		}
		hraValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell hraValCell=new PdfPCell(hraValP);
		hraValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		hraValCell.setBorderWidthLeft(0);
		hraValCell.setBorderWidthRight(0);
		hraValCell.setBorderWidthTop(0);
		table.addCell(hraValCell);


		//cell 9
		Phrase grossValPh=null;
//				double grossAmount=ps.getGrossEarningWithoutOT();
		if(sumOfGross!=0){
			grossValPh=new Phrase(df.format(sumOfGross)+"",font7);
		}else{
			grossValPh=new Phrase("",font7);
		}
//				sumOfGross+=grossAmount;
		
		Paragraph grossValP = new Paragraph();
		grossValP.add(grossValPh);
		grossValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell grossValCell=new PdfPCell(grossValP);
		grossValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		grossValCell.setBorderWidthLeft(0);
		grossValCell.setBorderWidthRight(0);
		grossValCell.setBorderWidthTop(0);
		table.addCell(grossValCell);

		
		//cell 10
		Phrase esicValPh=null;
//				double esicAmt=getDeductionActulValue("ESIC", ps);
		if(sumOfEsic!=0){
			esicValPh=new Phrase(df.format(sumOfEsic)+"",font7);
		}else{
			esicValPh=new Phrase("",font7);
		}
//				sumOfEsic+=esicAmt;
		
		Phrase lwfValPh=null;
//				double bandPAmt=getEarningActulValue("B AND P", ps);
//				double lwfAmt=getDeductionActulValue("LWF", ps);
		if(sumOfLwf!=0){
			lwfValPh=new Phrase(df.format(sumOfLwf)+"",font7);
		}else{
			lwfValPh=new Phrase("",font7);
		}
//				sumOfLwf+=lwfAmt;
		
		Paragraph esicValP = new Paragraph();
		esicValP.add(esicValPh);
		esicValP.add(Chunk.NEWLINE);
		esicValP.add(lwfValPh);
		esicValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell esicValCell=new PdfPCell(esicValP);
		esicValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		esicValCell.setBorderWidthLeft(0);
		esicValCell.setBorderWidthRight(0);
		esicValCell.setBorderWidthTop(0);
		table.addCell(esicValCell);

		//cell 11
		Phrase pfValPh=null;
//				double pfAmt=getDeductionActulValue("PF", ps);
		if(sumOfPf!=0){
			pfValPh=new Phrase(df.format(sumOfPf)+"",font7);
		}else{
			pfValPh=new Phrase("",font7);
		}
//				sumOfPf+=pfAmt;
		
		Phrase advValPh=null;
//				double advAmt=getDeductionActulValue("ADVANCE", ps);
		if(sumOfAdavance!=0){
			advValPh=new Phrase(df.format(sumOfAdavance)+"",font7);
		}else{
			advValPh=new Phrase("",font7);
		}
//				sumOfAdavance+=advAmt;
		
		Paragraph pfValP = new Paragraph();
		pfValP.add(pfValPh);
		pfValP.add(Chunk.NEWLINE);
		pfValP.add(advValPh);
		pfValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell pfValCell=new PdfPCell(pfValP);
		pfValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pfValCell.setBorderWidthLeft(0);
		pfValCell.setBorderWidthRight(0);
		pfValCell.setBorderWidthTop(0);
		table.addCell(pfValCell);
	
		//cell 12
		Phrase ptValPh=null;
//				double ptAmt=getDeductionActulValue("PT", ps);
		if(sumOfPt!=0){
			ptValPh=new Phrase(df.format(sumOfPt)+"",font7);
		}else{
			ptValPh=new Phrase("",font7);
		}
//				sumOfPt+=ptAmt;
		
		
		Paragraph ptValP = new Paragraph();
		ptValP.add(ptValPh);
		ptValP.add(Chunk.NEWLINE);
//		ptValP.add(advValPh);
		ptValP.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell ptValCell=new PdfPCell(ptValP);
		ptValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		ptValCell.setBorderWidthLeft(0);
		ptValCell.setBorderWidthRight(0);
		ptValCell.setBorderWidthTop(0);
		table.addCell(ptValCell);
	
		//cell 13
		Phrase totalDedPh=null;
//				double totDedAmt=ps.getTotalDeduction();
		if(sumOfTotalDed!=0){
			totalDedPh=new Phrase(df.format(sumOfTotalDed)+"",font7);
		}else{
			totalDedPh=new Phrase("",font7);
		}
//				sumOfTotalDed+=totDedAmt;
		
		
		Paragraph totalDedP = new Paragraph();
		totalDedP.add(totalDedPh);
		totalDedP.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell totalDedCell=new PdfPCell(totalDedP);
		totalDedCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalDedCell.setBorderWidthLeft(0);
		totalDedCell.setBorderWidthRight(0);
		totalDedCell.setBorderWidthTop(0);
		table.addCell(totalDedCell);
	
		//cell 14
		Phrase npValPh=null;
//				double npAmt=ps.getNetEarningWithoutOT();
		if(sumOfNetEarning!=0){
			npValPh=new Phrase(df.format(sumOfNetEarning)+"",font7);
		}else{
			npValPh=new Phrase("",font7);
		}
		
//				sumOfNetEarning+=npAmt;
		
		Paragraph npValP = new Paragraph();
		npValP.add(npValPh);
		npValP.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell npValCell=new PdfPCell(npValP);
		npValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		npValCell.setBorderWidthLeft(0);
		npValCell.setBorderWidthRight(0);
		npValCell.setBorderWidthTop(0);
		table.addCell(npValCell);
		
		//cell 15
		
		Phrase blank1Ph=new Phrase("",font8);
		PdfPCell blank1Cell=new PdfPCell(blank1Ph);
		blank1Cell.setBorderWidthLeft(0);
		blank1Cell.setBorderWidthRight(0);
		blank1Cell.setBorderWidthTop(0);
	
		table.addCell(blank1Cell);
		
		/**
		 * 
		 */
		
	} 
	/***
	 * @author Amol
	 * @date 10-7-2019
	 * @Description-for Other Earning Component
	 */
//	public double getOtherEarningRates(String ctcComponentName,PaySlip ps){
//		double amount=0;
//		     for(int i=0;i<ps.getEarningList().size();i++){
//		    	if(!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("BASIC")&&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("DA")
//		    	   &&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("HRA")&&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Washing Allowance")
//		    	   &&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Arrears Da")&&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Arrears Basic")
//		    	   &&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Arrears Hra")){
//		    		amount+=ps.getEarningList().get(i).getAmount()/ 12;
//		    		logger.log(Level.SEVERE," Inside Other Earning Method"+amount);
//		    	 }
//		  }
//		 return amount;
//	 }
	
	public double getOtherEarningActualRates(String ctcComponentName,PaySlip ps){
		double amount=0;
		     for(int i=0;i<ps.getEarningList().size();i++){
		    	if(!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("BASIC")&&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("DA")
		    	   &&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("HRA")&&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Washing Allowance")
		    	   &&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Arrears Da")&&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Arrears Basic")
		    	   &&!ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Arrears Hra")){
		    		amount+=ps.getEarningList().get(i).getActualAmount();
		    		logger.log(Level.SEVERE," Inside Other Earning Method"+amount);
//		    		ps.getEarningList().get(i).getArrearOf().trim().equalsIgnoreCase("BASIC")
		    		
//		    		double basicdahrawitharra;
//		    		double grossEarningwithoutot=payslip.getGrossEarningWithoutOT();
//		    		double otherearning=grossEarningwithoutot-basicdahrawitharra;
		    		
		    	 }
		  }
		 return amount;
	 }
	
	
	
	
	
	
	public double getOtherEarningValue(String ctcComponentName,
			PaySlip ps) {
		double amount = 0;
		double basicdahrawitharra = 0;
		for (int i = 0; i < ps.getEarningList().size(); i++) {
			if (ps.getEarningList().get(i).getShortName().trim()
					.equalsIgnoreCase("BASIC")
					|| ps.getEarningList().get(i).getShortName().trim()
							.equalsIgnoreCase("DA")
					|| ps.getEarningList().get(i).getShortName().trim()
							.equalsIgnoreCase("HRA")
					|| ps.getEarningList().get(i).getShortName().trim()
							.equalsIgnoreCase("Washing Allowance")
					|| ps.getEarningList().get(i).getArrearOf().trim()
							.equalsIgnoreCase("BASIC")
					||ps.getEarningList().get(i).getArrearOf().trim()
							.equalsIgnoreCase("DA")
					|| ps.getEarningList().get(i).getArrearOf().trim()
							.equalsIgnoreCase("HRA")) {
				basicdahrawitharra += ps.getEarningList().get(i).getActualAmount();
				logger.log(Level.SEVERE, " Inside Other Earning Method"
						+ basicdahrawitharra);

			}

		}
		double grossEarningWithoutOt = ps.getGrossEarningWithoutOT();
		amount = grossEarningWithoutOt - basicdahrawitharra;
		logger.log(Level.SEVERE, " outside Other Earning Method"+ amount);
		return amount;

	}
	
	
	
	
	
	
	
	
	
	
	
	

	public double getEarningRates(String ctcComponentName, PaySlip ps) {
		double amount=0;
		
		//Washing Allowance
		//Medical Allowance,Med
		//Bonus,Monthly Bonus
		//Paid Leavs
		//B AND P
		//OT
		//Gross
		//Pay Adj(E)
		if(ctcComponentName.equals("Gross Earning")){
			for (CtcComponent earnings : ps.getEarningList()) {
				double earning = earnings.getAmount() / 12;
				amount=amount+earning;
			}
		}else{
			for(int i=0;i<ps.getEarningList().size();i++){
				if(ctcComponentName.equals("Medical")){
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
							||ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Med")
							||ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Medical Allowance")){
						amount=ps.getEarningList().get(i).getAmount()/ 12;
					}
				}else if(ctcComponentName.equalsIgnoreCase("Basic")){
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
							||ps.getEarningList().get(i).getName().trim().equalsIgnoreCase(ctcComponentName)){
						amount=ps.getEarningList().get(i).getAmount()/12;
					}
				}else if(ctcComponentName.equalsIgnoreCase("Arrears Da")){
					if(ps.getEarningList().get(i).getArrearOf()!=null&&
							ps.getEarningList().get(i).getArrearOf().equalsIgnoreCase("DA")){
						amount=ps.getEarningList().get(i).getAmount()/12;
					}
				}else if(ctcComponentName.equalsIgnoreCase("Other Allowance")){
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
							||ps.getEarningList().get(i).getName().trim().equalsIgnoreCase(ctcComponentName)){
						amount=ps.getEarningList().get(i).getAmount()/12;
					}
				}else if(ctcComponentName.equalsIgnoreCase("Arrears Basic")){
					if(ps.getEarningList().get(i).getArrearOf()!=null&&
							ps.getEarningList().get(i).getArrearOf().equalsIgnoreCase("Basic")){
						amount=ps.getEarningList().get(i).getAmount()/12;
					}
				}
				else{
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)){
						amount=ps.getEarningList().get(i).getAmount()/ 12;
					}
				}
			}
		}
		return Double.parseDouble(df.format(amount));
	}
	
	public double getOtherEmpAllowanceActulValue(String ctcComponentName, PaySlip ps) {
		double amount=0;
		
		for(int i=0;i<ps.getEmployeeCTCList().size();i++){
			if(ps.getEmployeeCTCList().get(i).getCtcTemplateName().trim().equalsIgnoreCase(ctcComponentName)){
				amount=ps.getEmployeeCTCList().get(i).getAmount();
			}
		}
		return Double.parseDouble(df.format(amount));
	}
	
	public double getEarningActulValue(String ctcComponentName, PaySlip ps) {
		double amount=0;
		
		//Washing Allowance
		if(ctcComponentName.equals("Gross Earning")){
			for (CtcComponent earnings : ps.getEarningList()) {
				double earning = earnings.getActualAmount();
				amount=amount+earning;
			}
		}else{
			for(int i=0;i<ps.getEarningList().size();i++){
				if(ctcComponentName.equalsIgnoreCase("Medical")){
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
							||ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Med")
							||ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase("Medical Allowance")){
						amount=ps.getEarningList().get(i).getActualAmount();
					}
				}else if(ctcComponentName.equalsIgnoreCase("Basic")){
//					logger.log(Level.SEVERE, "BASIC :: "+ctcComponentName+" SN :: "+ps.getEarningList().get(i).getShortName()+" NM :: "+ps.getEarningList().get(i).getName()+" AAMT :: "+ps.getEarningList().get(i).getActualAmount());
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
							||ps.getEarningList().get(i).getName().trim().equalsIgnoreCase(ctcComponentName)){
						amount=ps.getEarningList().get(i).getActualAmount();
//						logger.log(Level.SEVERE, "BASIC ");
					}
				}else if(ctcComponentName.equalsIgnoreCase("Arrears Da")){
					if(ps.getEarningList().get(i).getArrearOf()!=null&&
							ps.getEarningList().get(i).getArrearOf().equalsIgnoreCase("DA")){
						amount=ps.getEarningList().get(i).getActualAmount();
					}
				}else if(ctcComponentName.equalsIgnoreCase("Arrears Hra")){
					if(ps.getEarningList().get(i).getArrearOf()!=null&&
							ps.getEarningList().get(i).getArrearOf().equalsIgnoreCase("HRA")){
						amount=ps.getEarningList().get(i).getActualAmount();
					}
				}else if(ctcComponentName.equalsIgnoreCase("Other Allowance")){
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
							||ps.getEarningList().get(i).getName().trim().equalsIgnoreCase(ctcComponentName)){
						amount=ps.getEarningList().get(i).getActualAmount();
					}
				}else if(ctcComponentName.equalsIgnoreCase("Arrears Basic")){
					if(ps.getEarningList().get(i).getArrearOf()!=null&&
							ps.getEarningList().get(i).getArrearOf().equalsIgnoreCase("Basic")){
						amount=ps.getEarningList().get(i).getActualAmount();
					}
				}
				else{
					if(ps.getEarningList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)){
						amount=ps.getEarningList().get(i).getActualAmount();
					}
				}
			}
		}
		return Double.parseDouble(df.format(amount));
	}
	
	
	public double getDeductionActulValue(String ctcComponentName, PaySlip ps) {
		double amount=0;
		
		for(int i=0;i<ps.getDeductionList().size();i++){
			if(ctcComponentName.trim().equalsIgnoreCase("Advance")){
				if(ps.getDeductionList().get(i).getShortName().trim().equalsIgnoreCase(ctcComponentName)
						||ps.getDeductionList().get(i).getName().trim().equalsIgnoreCase(ctcComponentName)){
					amount+=ps.getDeductionList().get(i).getActualAmount();
				}
			}else{
				if(ps.getDeductionList().get(i).getShortName().trim().equals(ctcComponentName)){
					amount+=ps.getDeductionList().get(i).getActualAmount();
				}
			}
		}
		return Double.parseDouble(df.format(amount));
	}

	public  void createCompanyHeaderTable()
	{
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		
		PdfPCell imageSignCell = null;
		if(comp!=null&&comp.getLogo()!=null){
			DocumentUpload logodocument =comp.getLogo();
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			Image image2=null;
			try {
				image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
				image2.scalePercent(20f);
				imageSignCell = new PdfPCell();
				imageSignCell.addElement(image2);
				imageSignCell.setBorder(0);
				imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				imageSignCell.setFixedHeight(15);
			} catch (Exception e) {
	//			e.printStackTrace();
			}
		}
		
		PdfPTable parenTbl = new PdfPTable(3);
		parenTbl.setWidthPercentage(96);
		try {
			parenTbl.setWidths(new float[]{15,70,15});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		
		
		Phrase companyName= new Phrase(comp.getBusinessUnitName(),font12bold);
	    Paragraph p =new Paragraph();
	    p.add(companyName);
	    p.add(Chunk.NEWLINE);
	    p.setAlignment(Element.ALIGN_CENTER);
	     
		String addressline1="";
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2();
		}else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		if(comp.getAddress().getLandmark()!=null&&!comp.getAddress().getLandmark().equals("")){
			addressline1=addressline1+", "+comp.getAddress().getLandmark()+",";
		}
		Phrase addressline=new Phrase(addressline1,font9);
		p.add(addressline);
		p.add(Chunk.NEWLINE);
		
		Phrase locality=null;
		if(comp.getAddress().getLocality().equals("")==false){
			locality= new Phrase(comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+"-"+comp.getAddress().getPin()+", "+comp.getAddress().getCountry()+".",font9);
		}
		else if(comp.getAddress().getLocality().equals("")==true){
			locality= new Phrase(comp.getAddress().getCity()+"-"+comp.getAddress().getPin()+", "+comp.getAddress().getCountry()+".",font9);
		}
		p.add(locality);
		p.add(Chunk.NEWLINE);
		
		String landline="";
		if(comp.getLandline()!=null){
			landline="Tel: +"+comp.getLandline()+" / "+comp.getCellNumber1();
		}else{
			landline="Tel: "+comp.getCellNumber1();
		}
		Phrase contactinfo=new Phrase(landline,font8);
		p.add(contactinfo);
		p.add(Chunk.NEWLINE);
		
		
		PdfPCell companyNameCell=new PdfPCell();
	    companyNameCell.addElement(p);
	    companyNameCell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		companyDetails.addCell(companyNameCell);
		companyDetails.setSpacingAfter(10f);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		comapnyCell.setBorder(0);
		parent.addCell(comapnyCell);
		
		if(imageSignCell!=null){
			parenTbl.addCell(imageSignCell);
		}else{
			parenTbl.addCell(blankCell);
		}
		parenTbl.addCell(comapnyCell);
		parenTbl.addCell(blankCell);
		
		try {
			document.add(parenTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		/*******
		 * 
		 */
		

		
		String salMonthTitle = "";
		String formTitle = "";
		String formName = "";
		String ruleTitle = "";
		
		Date yearWise=null;
		try {
			yearWise=salFmt.parse(paySlipList.get(0).getSalaryPeriod().trim());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		if(yearWise==null){
			yearWise=new Date();
		}
		
		
		String branch="";
		if(paySlipList!=null&&paySlipList.size()!=0){
			branch=paySlipList.get(0).getBranch();
		}
		if(branch.trim().equalsIgnoreCase("Banglore")){
			formTitle="Karnataka Shops & Commercial Establishments Rules";
			formName="FORM T";
			ruleTitle="See Rule 24(9-B)";
//			salMonthTitle="Muster Roll cum Wage Register for the Month - "+salMFmt.format(yearWise);
			salMonthTitle="Wage Register for the Month - "+salMFmt.format(yearWise);
		}else{
			formTitle="Maharashtra Minimum Wages Rules";
			formName="FORM II";
			ruleTitle="See Rule 27 (1)";
			salMonthTitle="Wage Register for the Month - "+salMFmt.format(yearWise);
		}
		
		
		/**
		 * 
		 * 
		 */

		
		/**
		 * Date : 24-08-2018 BY ANIL
		 * added FORM X as header on salary register as per sonu's instruction
		 */
		
//		String formTitle = "";
//		formTitle = "FORM X";
		
		Paragraph p1=new Paragraph();
		p1.setAlignment(Element.ALIGN_CENTER);
		Phrase formPh = new Phrase(formTitle, font9bold);
		p1.add(formPh);
		p1.add(Chunk.NEWLINE);
		
		Phrase formNmPh = new Phrase(formName, font9bold);
		p1.add(formNmPh);
		p1.add(Chunk.NEWLINE);
		
//		String formTitle1 = "";
//		formTitle1 = "Register of wages";
		Phrase formPh1 = new Phrase(ruleTitle, font9);
		p1.add(formPh1);
		p1.add(Chunk.NEWLINE);
		
//		String title1 = "";
//		salMonthTitle = "Salary Register for the Month "+paySlipList.get(0).getSalaryPeriod();
		Phrase titlephrase = new Phrase(salMonthTitle, font9bold);
		p1.add(titlephrase);
		p1.add(Chunk.NEWLINE);
		
		
		PdfPCell salInfoCell=new PdfPCell();
	    salInfoCell.addElement(p1);
	    salInfoCell.setBorder(0);
	    salInfoCell.setColspan(2);
		
		
		/**
		 * 
		 */
		
		
	    /**
		 * Date : 10-10-2018 BY ANIL
		 */
	    boolean showProjectNameFlag=false;
		HashSet<String> projectHs=new HashSet<String>();
		for(PaySlip pay:paySlipList){
			projectHs.add(pay.getProjectName());
		}
		if(projectHs.size()>1){
			showProjectNameFlag=true;
		}
		
		/**
		 * End
		 */
		
		
		PdfPTable locTbl=new PdfPTable(2);
		locTbl.setWidthPercentage(98);
		Phrase phProjectName=null;
		if(showProjectNameFlag){
			phProjectName=new Phrase("Project Name: ",font8bold);
		}else{
			phProjectName=new Phrase("Project Name: "+paySlipList.get(0).getProjectName(),font8bold);
		}
		
		PdfPCell projectNmCell=new PdfPCell(phProjectName);
		projectNmCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		projectNmCell.setBorder(0);
		
		Phrase phBranchNm=new Phrase("Branch Name: "+paySlipList.get(0).getBranch(),font8bold);
		PdfPCell branchNmCell=new PdfPCell(phBranchNm);
		branchNmCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		branchNmCell.setBorder(0);
		
		/**@author Amol
		 *Date 7-6-2019
		 * Added a customer Correspondance name
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PaySlip","CustomerCorrespondanceName",comp.getCompanyId())){
			customerNameFlag=true;
		}
		
		String customerName=null;
		if(customer!=null){
			if (customer.getCustPrintableName() != null&&!customer.getCustPrintableName().equals("")) {
				customerName = customer.getCustPrintableName();
			} else {
				customerName = customer.getFullname();
			}
		}else{
			customerName="";
		}
	
		Phrase customerCorrespondanceName=new Phrase("Correspondance Name: "+customerName,font8bold);		
		PdfPCell customerNamecell=new PdfPCell(customerCorrespondanceName);
		customerNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNamecell.setBorder(0);
		
		Phrase blankc=new Phrase(" ",font8bold);
		PdfPCell blankcell=new PdfPCell(blankc);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankcell.setBorder(0);
		
		
		
		
		
		locTbl.addCell(salInfoCell);
		if(customerNameFlag){
			locTbl.addCell(customerNamecell);
			locTbl.addCell(blankcell);
		}
		locTbl.addCell(projectNmCell);
		locTbl.addCell(branchNmCell);
		
		try {
			document.add(locTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
