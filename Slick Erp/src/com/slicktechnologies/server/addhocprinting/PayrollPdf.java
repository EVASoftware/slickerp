package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.humanresourcelayer.advancelayer.Emi;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.AvailedLeaves;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaidEmis;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlip;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipPrintHelper;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;


public class PayrollPdf {

	PaySlipPrintHelper payslip;
	Company comp;
	PaySlip ps;
	Employee emp;
	EmployeeInfo info;
	
	ProcessConfiguration processConfig;
	
	boolean rateColumnFlag=false;
	
	public Document document;
	private Font font16boldul, font12bold, font8bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9;
	final static String footertext = "This is system generated pdf. It does not require signature";
	float[] columnWidths2={2.5f,0.5f,4.5f,2.5f,0.5f,2.5f};
	float[] columnWidths3={2.5f,0.5f,4.5f};
	float[] columnWidths4={2.6f,0.5f,2.5f};
	float[] columnWidths5={3.0f,1.5f,1.5f,3.0f,1.5f,1.5f};
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	int firstBreakPoint=20;
	float blankLines=0;
	Logger logger=Logger.getLogger("Pay Pdf Logger");
	
	public PayrollPdf() {
		
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		
	}
	
	
	
	public void setPaySlip(long count,PaySlipPrintHelper payslip) {
		// Load PaySlip
		ps = ofy().load().type(PaySlip.class).id(count).now();
		logger.log(Level.SEVERE," inside PDF payslip :::  "+ps.getCompanyId());
		// Load Company
		if (ps.getCompanyId() == null){
			comp = ofy().load().type(Company.class).first().now();
		}
		else{
			comp = ofy().load().type(Company.class).filter("companyId", ps.getCompanyId()).first().now();
		}
	
		// Load Company
		if (ps.getCompanyId() == null){
			emp = ofy().load().type(Employee.class).filter("count", ps.getEmpid()).first().now();
		}
		else{
			emp = ofy().load().type(Employee.class).filter("companyId", ps.getCompanyId()).filter("count", ps.getEmpid()).first().now();
		}
		
		if(emp!=null){
			info=ofy().load().type(EmployeeInfo.class).filter("companyId", ps.getCompanyId()).filter("empCount", ps.getEmpid()).first().now();
		}
		this.payslip=payslip;
		
		
		if (ps.getCompanyId() != null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", ps.getCompanyId()).filter("processName", "PaySlip").filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if(processConfig!=null){
				System.out.println("PROCESS CONFIG NOT NULL");
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideRateColumn")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						System.out.println("PROCESS CONFIG FLAG FALSE");
						rateColumnFlag=true;
					}
				}
			}
		}
		
	}
	
	
	
	

	public void createPdf() {
		
		Createblank();
		createLogo(document,comp);
		createCompanyAddress();
		createEmployeeDetails();
		createEmployeeBankDetails();
		createEmployeeWorkingDayDetails();
		earningAndDeductionDetails();
		
	}

	/******************************** Create blank Line ********************************************/
	
	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	/******************************** Create company Logo ********************************************/
	
	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
	/******************************** print company address ********************************************/
	public  void createCompanyAddress()
	{
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
		String salaryPeriod=payslip.getSalaryPeriod();
		String[] res=salaryPeriod.split("-",0);
		String month =res[1].trim();
		String year =res[0].trim();
			
		String title1 = "";
		title1 = "Payslip For The Month Of "+month+"-"+year;
	
	
		Phrase titlephrase = new Phrase(title1, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);
	
		PdfPTable titlepdftable = new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(titlepdfcell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createEmployeeDetails(){
		
		/****************************** Employee information *******************************************/

		// Employee Information

		Phrase employeeidtitle = new Phrase("Employee ID", font8bold);
		Phrase employeenametitle = new Phrase("Employee Name", font8bold);
		Phrase empdesignationtitle = new Phrase("Designation", font8bold);
		Phrase empdepartmenttitle = new Phrase("Department", font8bold);
		Phrase employeedojtitle=new Phrase("Date Of Joining",font8bold);
		Phrase employeepannotitle = new Phrase("Pan No", font8bold);
		Phrase employeepfnotitle = new Phrase("PF A/C No.", font8bold);
		Phrase employeeesictitle = new Phrase("ESIC No.", font8bold);

		PdfPCell titleempid = new PdfPCell();
		titleempid.setBorder(0);
		titleempid.addElement(employeeidtitle);
		
		PdfPCell titleempname = new PdfPCell();
		titleempname.setBorder(0);
		titleempname.addElement(employeenametitle);
		
		PdfPCell titleempdesignation = new PdfPCell();
		titleempdesignation.setBorder(0);
		titleempdesignation.addElement(empdesignationtitle);
		
		PdfPCell titleempdepartment = new PdfPCell();
		titleempdepartment.setBorder(0);
		titleempdepartment.addElement(empdepartmenttitle);
		
		PdfPCell titleempdob = new PdfPCell();
		titleempdob.setBorder(0);
		 titleempdob.addElement(employeedojtitle);
		 
		PdfPCell titleemppanno = new PdfPCell();
		titleemppanno.setBorder(0);
		titleemppanno.addElement(employeepannotitle);
		
		PdfPCell titleemppfno = new PdfPCell();
		titleemppfno.setBorder(0);
		titleemppfno.addElement(employeepfnotitle);
		
		PdfPCell titleempesic = new PdfPCell();
		titleempesic.setBorder(0);
		titleempesic.addElement(employeeesictitle);

		Phrase retempid = new Phrase(payslip.getEmpid() + "", font8);
		Phrase retempname = new Phrase(payslip.getEmployeeName(), font8);
		Phrase retempdesignation = new Phrase(payslip.getEmployeedDesignation(), font8);
		Phrase retempdepartment = new Phrase(payslip.getDepartment(), font8);
		Phrase retempdoj=null;
		if(emp.getJoinedAt()!=null){
			retempdoj=new Phrase(fmt.format(emp.getJoinedAt()),font8);
		}else{
			retempdoj=new Phrase(" ",font8);
		}
		Phrase retemppanno = new Phrase(payslip.getPanNumber(), font8);
		Phrase retemppfno = new Phrase(payslip.getPfNumber(), font8);
		Phrase retempesicno = new Phrase(payslip.getEsicNumber(), font8);

		PdfPCell employeeidcell = new PdfPCell();
		employeeidcell.setBorder(0);
		employeeidcell.addElement(retempid);
		
		PdfPCell employeenamecell = new PdfPCell();
		employeenamecell.setBorder(0);
		employeenamecell.addElement(retempname);
		
		PdfPCell empdesignationcell = new PdfPCell();
		empdesignationcell.setBorder(0);
		empdesignationcell.addElement(retempdesignation);
		
		PdfPCell empdepartmentcell = new PdfPCell();
		empdepartmentcell.setBorder(0);
		empdepartmentcell.addElement(retempdepartment);
		
		PdfPCell empdojcell = new PdfPCell();
		empdojcell.setBorder(0);
		empdojcell.addElement(retempdoj);
		
		PdfPCell emppannocell = new PdfPCell();
		emppannocell.setBorder(0);
		emppannocell.addElement(retemppanno);
		
		PdfPCell emppfnocell = new PdfPCell();
		emppfnocell.setBorder(0);
		emppfnocell.addElement(retemppfno);
		
		PdfPCell empesicnocell = new PdfPCell();
		empesicnocell.setBorder(0);
		empesicnocell.addElement(retempesicno);

		
		Phrase semicolon =new Phrase(":",font8);
		PdfPCell semicoloncell = new PdfPCell();
		semicoloncell.setBorder(0);
		semicoloncell.addElement(semicolon);
		
		PdfPTable employeedetailstable = new PdfPTable(6);
		employeedetailstable.setWidthPercentage(100);
		try {
			employeedetailstable.setWidths(columnWidths2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		employeedetailstable.addCell(titleempid);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(employeeidcell);
		
		
		employeedetailstable.addCell(titleemppfno);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(emppfnocell);
		
		employeedetailstable.addCell(titleempname);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(employeenamecell);
		
		employeedetailstable.addCell(titleemppanno);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(emppannocell);
		
		employeedetailstable.addCell(titleempdepartment);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empdepartmentcell);
		
		employeedetailstable.addCell(titleempesic);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empesicnocell);
		
		employeedetailstable.addCell(titleempdesignation);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empdesignationcell);
		
		employeedetailstable.addCell(titleempdob);
		employeedetailstable.addCell(semicoloncell);
		employeedetailstable.addCell(empdojcell);
		
		
		
		
		
		
		
		
		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell parentcell = new PdfPCell();
		parentcell.addElement(employeedetailstable);
		parent.addCell(parentcell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createEmployeeBankDetails(){
		/********************************** Employee Bank Details ****************************************/

		// Employee Bank Information

		Phrase banknametitle = new Phrase("Bank Name ", font8bold);
		Phrase accnotitle = new Phrase("A/C No. ", font8bold);
		Phrase branchtitle = new Phrase("Branch", font8bold);
		Phrase ifsctitle = new Phrase("IFSC", font8bold);

		PdfPCell titlebankname = new PdfPCell();
		titlebankname.setBorder(0);
		titlebankname.addElement(banknametitle);
		PdfPCell titleaccno = new PdfPCell();
		titleaccno.setBorder(0);
		titleaccno.addElement(accnotitle);
		PdfPCell titlebranch = new PdfPCell();
		titlebranch.setBorder(0);
		titlebranch.addElement(branchtitle);
		PdfPCell titleifsc = new PdfPCell();
		titleifsc.setBorder(0);
		titleifsc.addElement(ifsctitle);

		Phrase employeebank = new Phrase(payslip.getEmpBankName(), font8);
		Phrase employeebankaccno = new Phrase(payslip.getAccountNumber(),font8);
		Phrase employeebankbranch = new Phrase(payslip.getBankBranch(),font8);
		Phrase ifsccode = new Phrase(payslip.getIfscNumber(), font8);

		PdfPCell empbanknamecell = new PdfPCell();
		empbanknamecell.setBorder(0);
		empbanknamecell.addElement(employeebank);
		PdfPCell empbankaccnocell = new PdfPCell();
		empbankaccnocell.setBorder(0);
		empbankaccnocell.addElement(employeebankaccno);
		PdfPCell empbankbranchcell = new PdfPCell();
		empbankbranchcell.setBorder(0);
		empbankbranchcell.addElement(employeebankbranch);
		PdfPCell ifsccell = new PdfPCell();
		ifsccell.setBorder(0);
		ifsccell.addElement(ifsccode);
		
		Phrase semicolon =new Phrase(":",font8);
		PdfPCell semicoloncell = new PdfPCell();
		semicoloncell.setBorder(0);
		semicoloncell.addElement(semicolon);
		

		PdfPTable empbankdetailstable = new PdfPTable(6);
		empbankdetailstable.setWidthPercentage(100);
		
		
		try {
			empbankdetailstable.setWidths(columnWidths2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		empbankdetailstable.addCell(titlebankname);
		empbankdetailstable.addCell(semicoloncell);
		empbankdetailstable.addCell(empbanknamecell);
		
		empbankdetailstable.addCell(titleaccno);
		empbankdetailstable.addCell(semicoloncell);
		empbankdetailstable.addCell(empbankaccnocell);
		
		empbankdetailstable.addCell(titlebranch);
		empbankdetailstable.addCell(semicoloncell);
		empbankdetailstable.addCell(empbankbranchcell);
		
		empbankdetailstable.addCell(titleifsc);
		empbankdetailstable.addCell(semicoloncell);
		empbankdetailstable.addCell(ifsccell);
		
		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell parentcell = new PdfPCell();
		parentcell.addElement(empbankdetailstable);
		parent.addCell(parentcell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createEmployeeWorkingDayDetails(){
		/******************************** Days Information ********************************************/

		Phrase monthlyDays = new Phrase("Monthly Days", font8bold);
		Phrase weekDays = new Phrase("Weekly Off", font8bold);
		Phrase holiday = new Phrase("Holiday", font8bold);
//		Phrase eligibledaystitle = new Phrase("Eligible Days", font8bold);
		
		Phrase overtime = new Phrase("Overtime(In Hrs)",font8bold);
		
		
		PdfPCell monthlyDaysTitleCell = new PdfPCell();
		monthlyDaysTitleCell.setBorder(0);
		monthlyDaysTitleCell.addElement(monthlyDays);
		
		PdfPCell weekDaysTitleCell = new PdfPCell();
		weekDaysTitleCell.setBorder(0);
		weekDaysTitleCell.addElement(weekDays);
		
		PdfPCell holidayTitleCell = new PdfPCell();
		holidayTitleCell.setBorder(0);
		holidayTitleCell.addElement(holiday);

//		PdfPCell eligibleDaysTitleCell = new PdfPCell();
//		eligibleDaysTitleCell.setBorder(0);
//		eligibleDaysTitleCell.addElement(eligibledaystitle);
		
		PdfPCell overtimeTitleCell = new PdfPCell();
		overtimeTitleCell.setBorder(0);
		overtimeTitleCell.addElement(overtime);


		
		Phrase monthlDaysValue = new Phrase(payslip.getPaySlip().getMonthlyDays() + "",font8);
		Phrase weekDaysValue = new Phrase(payslip.getPaySlip().getWeekDays() + "", font8);
		Phrase holidayValue = new Phrase(payslip.getPaySlip().getHoliday() + "",font8);
//		Phrase reteligibledays = new Phrase(payslip.getEligibleDays() + "",font8);
		
		Phrase overtimeValue = new Phrase(payslip.getPaySlip().getOverTimehrs() + "",font8);

		PdfPCell monthlDaysValueCell = new PdfPCell();
		monthlDaysValueCell.setBorder(0);
		monthlDaysValueCell.addElement(monthlDaysValue);
		
		PdfPCell weekDaysValueCell = new PdfPCell();
		weekDaysValueCell.setBorder(0);
		weekDaysValueCell.addElement(weekDaysValue);
		
		PdfPCell holidayValueCell = new PdfPCell();
		holidayValueCell.setBorder(0);
		holidayValueCell.addElement(holidayValue);
		
//		PdfPCell eligibledayscell = new PdfPCell();
//		eligibledayscell.setBorder(0);
//		eligibledayscell.addElement(reteligibledays);
		
		PdfPCell overtimeValueCell = new PdfPCell();
		overtimeValueCell.setBorder(0);
		overtimeValueCell.addElement(overtimeValue);
		
		
		
		
		
		
		Phrase semicolon =new Phrase(":",font8);
		PdfPCell semicoloncell = new PdfPCell();
		semicoloncell.setBorder(0);
		semicoloncell.addElement(semicolon);
		
		
		Phrase demo =new Phrase(" ",font8);
		PdfPCell demoCell = new PdfPCell();
		demoCell.setBorder(0);
		demoCell.addElement(demo);
		

		PdfPTable empattendancetable = new PdfPTable(3);
		empattendancetable.setWidthPercentage(100);
		
		try {
			empattendancetable.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		empattendancetable.addCell(monthlyDaysTitleCell);
		empattendancetable.addCell(semicoloncell);
		empattendancetable.addCell(monthlDaysValueCell);
		
		empattendancetable.addCell(weekDaysTitleCell);
		empattendancetable.addCell(semicoloncell);
		empattendancetable.addCell(weekDaysValueCell);
		
		empattendancetable.addCell(holidayTitleCell);
		empattendancetable.addCell(semicoloncell);
		empattendancetable.addCell(holidayValueCell);
		
//		empattendancetable.addCell(eligibleDaysTitleCell);
//		empattendancetable.addCell(semicoloncell);
//		empattendancetable.addCell(eligibledayscell);
		
		if(payslip.getPaySlip().getOverTimehrs()!=0){
			empattendancetable.addCell(overtimeTitleCell);
			empattendancetable.addCell(semicoloncell);
			empattendancetable.addCell(overtimeValueCell);
		}
		
		
		
		PdfPTable empLeaveTable = new PdfPTable(3);
		empLeaveTable.setWidthPercentage(100);
		
		try {
			empLeaveTable.setWidths(columnWidths4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase paiddaystitle = new Phrase("Paid Days", font8bold);
		PdfPCell paidDaysTitleCell = new PdfPCell();
		paidDaysTitleCell.setBorder(0);
		paidDaysTitleCell.addElement(paiddaystitle);
		
		Phrase retpaiddays = new Phrase(payslip.getPaidDays()+"", font8);
		PdfPCell paiddayscell = new PdfPCell();
		paiddayscell.setBorder(0);
		paiddayscell.addElement(retpaiddays);
		
		
		
		Phrase earnedLeavetitle = new Phrase("Earned Leave", font8bold);
		PdfPCell earnedLeaveTitleCell = new PdfPCell();
		earnedLeaveTitleCell.setBorder(0);
		earnedLeaveTitleCell.addElement(earnedLeavetitle);
		
		Phrase earnedLeave = new Phrase(df.format(payslip.getPaySlip().getEarnedLeave())+"", font8);
		PdfPCell earnedLeavecell = new PdfPCell();
		earnedLeavecell.setBorder(0);
		earnedLeavecell.addElement(earnedLeave);
		
		
		
		if(payslip.getPaySlip().getAviledLeaves()!=null &&payslip.getPaySlip().getAviledLeaves().size()!=0){
		
		for (AvailedLeaves avLeave : payslip.getPaySlip().getAviledLeaves()) {
					
			Phrase leaveName = new Phrase(avLeave.getLeaveName(), font8bold);
			
			PdfPCell leaveNameCell = new PdfPCell();
			leaveNameCell.addElement(leaveName);
			leaveNameCell.setBorder(0);
			leaveNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			empLeaveTable.addCell(leaveNameCell);
			empLeaveTable.addCell(semicoloncell);
			
			Phrase leaveValue1=null;
			if(avLeave.isUnPaid()==true){
				double hrsInDays=avLeave.getLeavehour()/info.getLeaveCalendar().getWorkingHours();
				leaveValue1 = new Phrase(hrsInDays + " (Unpaid)", font8bold);
			}
			else{
				double hrsInDays=avLeave.getLeavehour()/info.getLeaveCalendar().getWorkingHours();
				leaveValue1 = new Phrase(hrsInDays + "", font8);
			}
			
			PdfPCell leaveValue1Cell = new PdfPCell();
			leaveValue1Cell.setBorder(0);
			leaveValue1Cell.addElement(leaveValue1);
			
			empLeaveTable.addCell(leaveValue1Cell);
		}
		}

		
		empLeaveTable.addCell(earnedLeaveTitleCell);
		empLeaveTable.addCell(semicoloncell);
		empLeaveTable.addCell(earnedLeavecell);
		
		empLeaveTable.addCell(paidDaysTitleCell);
		empLeaveTable.addCell(semicoloncell);
		empLeaveTable.addCell(paiddayscell);
		
		
		
		
		
		
		
		
		
		PdfPTable parent = new PdfPTable(2);
		parent.setWidthPercentage(100);
		try {
			parent.setWidths(new float[] { 57, 43 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPCell parentcell = new PdfPCell();
		parentcell.addElement(empattendancetable);
		parentcell.setBorder(0);
		parent.addCell(parentcell);
		
		PdfPCell empLeaveTblCell = new PdfPCell();
		empLeaveTblCell.addElement(empLeaveTable);
		empLeaveTblCell.setBorder(0);
		parent.addCell(empLeaveTblCell);
		
		PdfPTable bigparent = new PdfPTable(1);
		bigparent.setWidthPercentage(100);
		bigparent.addCell(parent);
		
		try {
			document.add(bigparent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void earningAndDeductionDetails(){
		
		PdfPTable empEarngsDeductionTable = new PdfPTable(2);
		empEarngsDeductionTable.setWidthPercentage(100);
		
		
		try {
			empEarngsDeductionTable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPTable empEarngsTable;
		PdfPTable empDeductionTable;
		if(rateColumnFlag==true){
			empEarngsTable = new PdfPTable(3);
			empEarngsTable.setWidthPercentage(100);
			
			empDeductionTable = new PdfPTable(3);
			empDeductionTable.setWidthPercentage(100);
		}else{
			empEarngsTable = new PdfPTable(4);
			empEarngsTable.setWidthPercentage(100);
			
			empDeductionTable = new PdfPTable(4);
			empDeductionTable.setWidthPercentage(100);
		}
		
		
		
//		PdfPTable empEarngsTable = new PdfPTable(4);
//		empEarngsTable.setWidthPercentage(100);
		
//		PdfPTable empDeductionTable = new PdfPTable(4);
//		empDeductionTable.setWidthPercentage(100);
		
		//Earning
		PdfPTable empErnHdTbl = new PdfPTable(2);
		empErnHdTbl.setWidthPercentage(100);
		
		PdfPTable empErnRateTbl = null;
		if(rateColumnFlag==false){
			empErnRateTbl = new PdfPTable(1);
			empErnRateTbl.setWidthPercentage(100);
		}
		
		PdfPTable empErnAmtTbl = new PdfPTable(1);
		empErnAmtTbl.setWidthPercentage(100);
		
		
		//Deduction
		PdfPTable empDedHdTbl = new PdfPTable(2);
		empDedHdTbl.setWidthPercentage(100);
		
		PdfPTable empDedRateTbl=null;
		if(rateColumnFlag==false){
			empDedRateTbl = new PdfPTable(1);
			empDedRateTbl.setWidthPercentage(100);
		}
		
		PdfPTable empDedAmtTbl = new PdfPTable(1);
		empDedAmtTbl.setWidthPercentage(100);
		
		
		//Earning Head Title
		Phrase earningstitle = new Phrase("Earning Head", font10bold);
		PdfPCell titleearningcell = new PdfPCell();
		titleearningcell.setColspan(2);
		titleearningcell.addElement(earningstitle);
		
		
		PdfPCell titlerupeescell = null;
		if(rateColumnFlag==false){
			Phrase rupeestitle = new Phrase("Rate", font10bold);
			titlerupeescell = new PdfPCell();
			titlerupeescell.addElement(rupeestitle);
		}
		
		Phrase earnedAmt = new Phrase("Earned", font10bold);
		PdfPCell earnedAmtCell = new PdfPCell();
		earnedAmtCell.addElement(earnedAmt);
		
		
		
		//Deduction Head Title
		Phrase deductionTitle = new Phrase("Deduction Head", font10bold);
		PdfPCell deductionTitleCell = new PdfPCell();
		deductionTitleCell.setColspan(2);
		deductionTitleCell.addElement(deductionTitle);
		
		PdfPCell deductionRateCell=null;
		if(rateColumnFlag==false){
			Phrase deductionRate = new Phrase("Rate", font10bold);
			deductionRateCell = new PdfPCell();
			deductionRateCell.addElement(deductionRate);
		}
		
		Phrase deductionAmt = new Phrase("Deduction", font10bold);
		PdfPCell deductionAmtCell = new PdfPCell();
		deductionAmtCell.addElement(deductionAmt);
		
		//Earning
		empErnHdTbl.addCell(titleearningcell);
		if(rateColumnFlag==false){
			empErnRateTbl.addCell(titlerupeescell);
		}
		empErnAmtTbl.addCell(earnedAmtCell);
		
		
		//Deduction
		empDedHdTbl.addCell(deductionTitleCell);
		if(rateColumnFlag==false){
			empDedRateTbl.addCell(deductionRateCell);
		}
		empDedAmtTbl.addCell(deductionAmtCell);
		
		
		
		
		double sum = 0;
		double actualSum=0;
		for (CtcComponent earnings : payslip.getEarningList()) {
			
			Phrase earnCompNm = new Phrase(earnings.getName(), font8bold);
			PdfPCell earnCompNmcell = new PdfPCell();
			earnCompNmcell.setColspan(2);
			earnCompNmcell.setBorder(0);
			earnCompNmcell.addElement(earnCompNm);
			empErnHdTbl.addCell(earnCompNmcell);
			
			//Rate Column
			double earning = earnings.getAmount() / 12;
			sum = sum + earning;
			
			PdfPCell earnCompAmtcell=null;
			
			if(rateColumnFlag==false){
				Phrase earnCompAmt = new Phrase(df.format(earning) + "", font8bold);
				earnCompAmtcell = new PdfPCell();
				earnCompAmtcell.setBorder(0);
				earnCompAmtcell.addElement(earnCompAmt);
				empErnRateTbl.addCell(earnCompAmtcell);
			}
			
			
			double oneDayEarning=earning/payslip.getPaySlip().getMonthlyDays();
			double actualearning=oneDayEarning*payslip.getPaidDays();
			actualSum=actualSum+actualearning;
			
			Phrase actuaEearnCompAmt = new Phrase(df.format(actualearning) + "", font8bold);
			PdfPCell actuaEearnCompAmtcell = new PdfPCell();
			actuaEearnCompAmtcell.setBorder(0);
			actuaEearnCompAmtcell.addElement(actuaEearnCompAmt);
			empErnAmtTbl.addCell(actuaEearnCompAmtcell);

		}
		
		
		
		
		
		if(payslip.getPaySlip().getOvertimeSalary()!=0){
		
			Phrase overtimeNm = new Phrase("Overtime Amount", font8bold);
			PdfPCell overtimeNmcell = new PdfPCell();
			overtimeNmcell.setColspan(2);
			overtimeNmcell.setBorder(0);
			overtimeNmcell.addElement(overtimeNm);
			empErnHdTbl.addCell(overtimeNmcell);
			
			
			//Rate Column
			PdfPCell overtimeAmtcell=null;
			if(rateColumnFlag==false){
				Phrase overtimeAmt = new Phrase("-", font8bold);
				overtimeAmtcell = new PdfPCell();
				overtimeAmtcell.setBorder(0);
				overtimeAmtcell.addElement(overtimeAmt);
				empErnRateTbl.addCell(overtimeAmtcell);
			}
			
			
			
			Phrase actuaovertimeAmt = new Phrase(df.format(payslip.getPaySlip().getOvertimeSalary()) + "", font8bold);
			PdfPCell actuaovertimeAmtcell = new PdfPCell();
			actuaovertimeAmtcell.setBorder(0);
			actuaovertimeAmtcell.addElement(actuaovertimeAmt);
			empErnAmtTbl.addCell(actuaovertimeAmtcell);
			
			actualSum=actualSum+payslip.getPaySlip().getOvertimeSalary();
		
		}
		
		
		
		
		
		
		
		//////////////////////////////////////////////////////////////////////////////////////
		
		int totalEarningListSize=0;
		
		if(payslip.getPaySlip().getOvertimeSalary()!=0){
			totalEarningListSize=payslip.getEarningList().size()+1;
		}
		else{
			totalEarningListSize=payslip.getEarningList().size();
		}
		
		int totalDeductionListSize=payslip.getLoneEmis().size()+payslip.getDeductionList().size();
		
		int extraSpace=0;
		if(totalEarningListSize<totalDeductionListSize){
			extraSpace=totalDeductionListSize-totalEarningListSize;
			extraSpace=extraSpace*6;
		}
		
		if( totalEarningListSize<= firstBreakPoint){
			int size =  firstBreakPoint - totalEarningListSize;
			blankLines = size*(185/17)+extraSpace;
			System.out.println("blankLines size ="+blankLines);
		 }
		else{
			blankLines = 10f;
		}
		
		empErnHdTbl.setSpacingAfter(blankLines);	
		
		
		//////////////////////////////////////////////////////////////////////////////////////
		PdfPTable empGrossEarnTable=null;
		if(rateColumnFlag==false){
			empGrossEarnTable = new PdfPTable(4);
			empGrossEarnTable.setWidthPercentage(100);
		}else{
			empGrossEarnTable = new PdfPTable(3);
			empGrossEarnTable.setWidthPercentage(100);
		}
		
		
		
		
		Phrase grossearningstitle = new Phrase("Gross Earnings", font8bold);
		PdfPCell empGrossEarnTitleCell = new PdfPCell();
		empGrossEarnTitleCell.addElement(grossearningstitle);
		empGrossEarnTitleCell.setColspan(2);
		empGrossEarnTable.addCell(empGrossEarnTitleCell);
		
		//Rate Column 
		double grossEarning = sum;
		Phrase retgrossearnings = new Phrase(df.format(grossEarning) + "", font8bold);
		
		PdfPCell empGrossEarnValuCell=null;
		if(rateColumnFlag==false){
			empGrossEarnValuCell = new PdfPCell();
			empGrossEarnValuCell.addElement(retgrossearnings);
			empGrossEarnTable.addCell(empGrossEarnValuCell);
		}
		
		double actualgrossEarning = actualSum;
		Phrase actualgrossearnings = new Phrase(df.format(actualgrossEarning) + "", font8bold);
		
		PdfPCell empActualGrossEarnValuCell = new PdfPCell();
		empActualGrossEarnValuCell.addElement(actualgrossearnings);
		empGrossEarnTable.addCell(empActualGrossEarnValuCell);
		
		
		
		
/***************************** DEDUCTION *******************************************/
		
		double actualSumDeadAmount=0;
		double sumOfDedAmt=0;
		
		for (CtcComponent deductions : payslip.getDeductionList()) {
			
			Phrase deductCompNm = new Phrase(deductions.getName(), font8bold);
			PdfPCell deductCompNmcell = new PdfPCell();
			deductCompNmcell.setColspan(2);
			deductCompNmcell.setBorder(0);
			deductCompNmcell.addElement(deductCompNm);
			empDedHdTbl.addCell(deductCompNmcell);
			
			
			//Deduction Rate Column
			
			double dedAmount = 0;
			PdfPCell deductCompAmtcell=null;
			
//			if (deductions.getMaxPerOfCTC() != null) {
//				double ctcAmount = payslip.getCtcAmount();
//				dedAmount = (int) (ctcAmount*(deductions.getMaxPerOfCTC()) / 100);
//				dedAmount = dedAmount / 12;
//				sumOfDedAmt=sumOfDedAmt+dedAmount;
//			}
//			else {
//				dedAmount = deductions.getMaxAmount() / 12;
//				sumOfDedAmt=sumOfDedAmt+dedAmount;
//			}

			dedAmount = deductions.getAmount();
			sumOfDedAmt=sumOfDedAmt+dedAmount;	
			
			if(rateColumnFlag==false){
				Phrase deductCompAmt = new Phrase(df.format(dedAmount) + "", font8bold);
				deductCompAmtcell = new PdfPCell();
				deductCompAmtcell.setBorder(0);
				deductCompAmtcell.addElement(deductCompAmt);
				empDedRateTbl.addCell(deductCompAmtcell);
			}
			
			
			//Deduction Amount Column
			
			double oneDayDeduction=dedAmount/payslip.getPaySlip().getMonthlyDays();
			double actualDeduction=oneDayDeduction*payslip.getPaidDays();
			
			actualSumDeadAmount=actualSumDeadAmount+actualDeduction;
			
			Phrase actualDeductCompAmt = new Phrase(df.format(actualDeduction) + "", font8bold);
			PdfPCell actualDeductCompAmtcell = new PdfPCell();
			actualDeductCompAmtcell.setBorder(0);
			actualDeductCompAmtcell.addElement(actualDeductCompAmt);
			empDedAmtTbl.addCell(actualDeductCompAmtcell);
		}
		
		
		/*************************** Advance Deduction ***************************************/
		// First Get the Advance Emi corresponding to Employee
		{
			// Get Current Month in String
			SimpleDateFormat formatMonth = new SimpleDateFormat("MMMM");
			SimpleDateFormat formatYear = new SimpleDateFormat("YYYY");

			String currMonth = formatMonth.format(new Date()).trim();
			String currYear = formatYear.format(new Date()).trim();

			for (PaidEmis info : payslip.getLoneEmis()) {
				// Get Emi Months
				String emiMonth = formatMonth.format(info.getDate()).trim();
				String emiYear = formatYear.format(info.getDate()).trim();

				System.out.println("Emi Year " + emiYear);
				System.out.println("Current Year " + currYear);
				System.out.println("Current Month " + currMonth);
				System.out.println("Emi Month " + emiMonth);
				
				boolean monthEqaul = emiMonth.equals(currMonth);
				boolean yearEqaul = emiYear.equals(currYear);

				System.out.println("Month Equal " + monthEqaul);
				System.out.println("Year Equal " + yearEqaul);

				Phrase temp1 = new Phrase("Advance " + "", font8bold);
				PdfPCell temp1cell = new PdfPCell();
				temp1cell.setColspan(2);
				temp1cell.setBorder(0);
				temp1cell.addElement(temp1);
				empDedHdTbl.addCell(temp1cell);
				
				PdfPCell temp0cell=null;
				if(rateColumnFlag==false){
					Phrase temp0 = new Phrase("-", font8bold);
					temp0cell = new PdfPCell();
					temp0cell.setBorder(0);
					temp0cell.addElement(temp0);
					empDedRateTbl.addCell(temp0cell);
				}

				double dedAmount = info.getMonthlyPayment();
				actualSumDeadAmount=actualSumDeadAmount+dedAmount;
				
				Phrase temp2 = new Phrase(df.format(dedAmount) + "", font8bold);
				PdfPCell temp2cell = new PdfPCell();
				temp2cell.setBorder(0);
				temp2cell.addElement(temp2);
				empDedAmtTbl.addCell(temp2cell);
				info.setMonthlyStatus(Emi.PAID);
			}
		}
		
		
		//////////////////////////////////////////////////////////////////////////////////////
		int extraSpace1=0;
		if(totalEarningListSize>totalDeductionListSize){
			extraSpace1=totalEarningListSize-totalDeductionListSize;
			extraSpace1=extraSpace1*6;
		}
		
		if( totalDeductionListSize<= firstBreakPoint){
			int size =  firstBreakPoint - totalDeductionListSize;
			blankLines = size*(185/17)+extraSpace1;
			System.out.println("blankLines size ="+blankLines);
		 }
		else{
			blankLines = 10f;
		}
		
		empDedHdTbl.setSpacingAfter(blankLines);	
		
		//////////////////////////////////////////////////////////////////////////////////////
		PdfPTable empTotalDeductionTable=null;
		if(rateColumnFlag==false){
			empTotalDeductionTable = new PdfPTable(4);
			empTotalDeductionTable.setWidthPercentage(100);
		}else{
			empTotalDeductionTable = new PdfPTable(3);
			empTotalDeductionTable.setWidthPercentage(100);
		}
		
		
		Phrase totalDeductionTitle = new Phrase("Total Deduction", font8bold);
		PdfPCell totalDeductionTitleCell = new PdfPCell();
		totalDeductionTitleCell.setColspan(2);
		totalDeductionTitleCell.addElement(totalDeductionTitle);
		empTotalDeductionTable.addCell(totalDeductionTitleCell);
		
		//Rate
		PdfPCell empTotalDeductionValuCell=null;
		if(rateColumnFlag==false){
			double deductionAmount = sumOfDedAmt;
			Phrase deduction = new Phrase(df.format(deductionAmount) + "", font8bold);
			empTotalDeductionValuCell = new PdfPCell();
			empTotalDeductionValuCell.addElement(deduction);
			empTotalDeductionTable.addCell(empTotalDeductionValuCell);
		}
		
		double actualDeductionAmount = actualSumDeadAmount;
		Phrase actualDeductionAmountPh = new Phrase(df.format(actualDeductionAmount) + "", font8bold);
		PdfPCell empActualTotalDeductionValuCell = new PdfPCell();
		empActualTotalDeductionValuCell.addElement(actualDeductionAmountPh);
		empTotalDeductionTable.addCell(empActualTotalDeductionValuCell);
		
		
		///
		PdfPCell earningHDTableCell = new PdfPCell();
		earningHDTableCell.setColspan(2);
		earningHDTableCell.addElement(empErnHdTbl);
		
		PdfPCell earningRATETableCell=null;
		if(rateColumnFlag==false){
			earningRATETableCell = new PdfPCell();
			earningRATETableCell.addElement(empErnRateTbl);
		}
		
		PdfPCell earningAMTTableCell = new PdfPCell();
		earningAMTTableCell.addElement(empErnAmtTbl);
		
		
		///
		
		PdfPCell deductionHDTableCell = new PdfPCell();
		deductionHDTableCell.setColspan(2);
		deductionHDTableCell.addElement(empDedHdTbl);
		
		PdfPCell deductionRATETableCell=null;
		
		if(rateColumnFlag==false){
			deductionRATETableCell = new PdfPCell();
			deductionRATETableCell.addElement(empDedRateTbl);
		}
		
		PdfPCell deductionAMTTableCell = new PdfPCell();
		deductionAMTTableCell.addElement(empDedAmtTbl);
		
		///
		
		empEarngsTable.addCell(earningHDTableCell);
		if(rateColumnFlag==false){
				empEarngsTable.addCell(earningRATETableCell);
		}
		empEarngsTable.addCell(earningAMTTableCell);
		
		empDeductionTable.addCell(deductionHDTableCell);
		if(rateColumnFlag==false){
			empDeductionTable.addCell(deductionRATETableCell);
		}
		empDeductionTable.addCell(deductionAMTTableCell);
		
		
		///
		
		PdfPCell earningTableCell = new PdfPCell();
		earningTableCell.addElement(empEarngsTable);
		
		PdfPCell deductionTableCell = new PdfPCell();
		deductionTableCell.addElement(empDeductionTable);
		
		PdfPCell grossEarningTableCell = new PdfPCell();
		grossEarningTableCell.addElement(empGrossEarnTable);
		
		PdfPCell totalDeductionTableCell = new PdfPCell();
		totalDeductionTableCell.addElement(empTotalDeductionTable);
		
		
		
		double salary=Math.round(actualSum - actualSumDeadAmount);
		
		
		String salaryInWords=ServiceInvoicePdf.convert(salary);
		
		
		
		Phrase netPay = new Phrase("Net Salary :"+ salary,font8bold);
		PdfPCell tempCell = new PdfPCell();
		tempCell.setColspan(2);
		tempCell.addElement(netPay);
		
		Phrase netPayWrds = new Phrase("Net Salary In Words :"+ salaryInWords +" Only",font8bold);
		PdfPCell tempCell1 = new PdfPCell();
		tempCell1.setColspan(2);
		tempCell1.addElement(netPayWrds);
		
		empEarngsDeductionTable.addCell(earningTableCell);
		empEarngsDeductionTable.addCell(deductionTableCell);
		empEarngsDeductionTable.addCell(grossEarningTableCell);
		empEarngsDeductionTable.addCell(totalDeductionTableCell);
		empEarngsDeductionTable.addCell(tempCell);
		empEarngsDeductionTable.addCell(tempCell1);
		
		Phrase footertitle = new Phrase(footertext, font8bold);
		PdfPCell footercell = new PdfPCell();
		footercell.addElement(footertitle);
		
		PdfPTable footertable = new PdfPTable(1);
		footertable.setWidthPercentage(100);
		footertable.addCell(footercell);
		
		
		try {
			document.add(empEarngsDeductionTable);
			document.add(footertable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
}
