package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EmployeeIDCardPdf {

	public Document document;
	Employee employee;
	EmployeeAdditionalDetails empadditionaldetails;
	Company comp;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	
	Font font18bold = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
	PdfUtility pdf=new PdfUtility();
	
	/**
	 * @author Anil @since 12-03-2021
	 * For Alkosh instead of aadhar card  number we have to print employee's mobile number
	 */
	boolean printMobileNoOnIdCard=false;
	
	public void getEmployeeDeatils(Long count) {
		employee = ofy().load().type(Employee.class).id(count).now();

		if (employee.getCompanyId() != null)
			comp = ofy().load().type(Company.class).filter("companyId", employee.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();
		
		printMobileNoOnIdCard=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "printMobileNoOnIdCard", employee.getCompanyId());
	}

	public void createPdf() {
		//WIDTH(5.1 cm) - 2.00787 * 72 =144.56664
		//HEIGHT(8.3 cm) - 3.26772 * 72 =235.27584
		//outertable width - 
		
		//NAME TAG ()
		//WIDTH(6.2 cm) - 2.44094 * 72 = 175.74768
		//HEIGHT(1.1 cm) - 0.433071 * 72 =31.181112
		//HEIGHT(1.8 cm) - 0.709 * 72 =51.048
		
		/**
		 * Id card :-
		 * Height - 8.35cm	3.287402 * 72 = 236.692944 (3.28=236.16)
		 * Width - 5.2cm	2.04724  * 72 =	147.40128  (2.04=146.88)
		 */
		
		/**
		 * Name Tag :-
		 * Height - 1.7cm	0.669291 * 72 = 48.188952 (0.66=47.52)
		 * Width - 6.2cm	2.44094  * 72 = 175.74768 (2.44=175.68)
		 */
		
		
		
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);

		
		table.addCell(createIdCardHeaderCell());  // 40
		
		PdfPCell photoCell=new PdfPCell();
		photoCell.addElement(createIdCardPhotoTbl());	//60
		table.addCell(photoCell).setBorder(0);
		
		PdfPCell idDetailsCell=new PdfPCell(createIdCardDetails());
		idDetailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		idDetailsCell.setVerticalAlignment(Element.ALIGN_CENTER);
		idDetailsCell.setFixedHeight(83.692944f);			//83
		/**
		 * @author Anil @since 01-03-2021
		 * 0.2cm - 0.0787402 inch *72 = 5.6692944
		 */
		idDetailsCell.setPaddingLeft(5.6692944f);
		table.addCell(idDetailsCell).setBorder(0);
		
		table.addCell(createIdCardFooterCell());			//53
		
		table.setTotalWidth(147.40128f);//144.70f
		table.setLockedWidth(true);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		
		PdfPTable outerTble  = new PdfPTable(1);
		outerTble.setWidthPercentage(100f);
		
		PdfPCell outerCell=new PdfPCell();
		outerCell.addElement(table);
		outerCell.setPaddingLeft(0);
		outerCell.setPaddingRight(0);
		outerCell.setPaddingBottom(0);
		outerCell.setFixedHeight(236.692944f);// 234.0f
		outerTble.addCell(outerCell);
		
		outerTble.setTotalWidth(147.40128f);  //144.70f
//		outerTble.setTotalWidth(144.56664f);
	//	outerTble.setTotalWidth(145.005f);
		outerTble.setLockedWidth(true);
		outerTble.setHorizontalAlignment(Element.ALIGN_LEFT);
		outerTble.setSpacingAfter(25f);
		
		
		try {
			document.add(outerTble);  
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable nameTagTbl=new PdfPTable(1);
		nameTagTbl.setWidthPercentage(100f);
		
		PdfPCell parentCell=new PdfPCell();
		parentCell.addElement(createNameTagTbl());
//		parentCell.setPaddingLeft(0.4f);
//		parentCell.setPaddingRight(0.4f);
//		parentCell.setPaddingBottom(0.4f);
//		parentCell.setPaddingTop(0.4f);
//		parentCell.setFixedHeight(45.048f);
		parentCell.setFixedHeight(45.70f);//47.00f
		
		nameTagTbl.addCell(parentCell);
		
		nameTagTbl.setTotalWidth(173.10f);//174.50f
		nameTagTbl.setLockedWidth(true);
		nameTagTbl.setHorizontalAlignment(Element.ALIGN_LEFT);
		try {
			document.add(nameTagTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public PdfPCell createIdCardHeaderCell(){
		PdfPCell cell=null;
//		if(comp.getIdCarduploadHeader()!=null&&!comp.getIdCarduploadHeader().getUrl().equals("")){
//			cell=pdf.getPhotoCell(comp.getIdCarduploadHeader(), 20f, 37, 0, 0);  
//		}else{
//			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 37);
//		}
		if(comp.getIdCarduploadHeader()!=null&&!comp.getIdCarduploadHeader().getUrl().equals("")){
			cell=pdf.getPhotoCell(comp.getIdCarduploadHeader(), 20f, 40, 0, 0,0);  
		}else{
			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 40);
		}
		cell.setBorder(0);
//		cell.setUseVariableBorders(true);
		cell.setPaddingTop(0);
		cell.setPaddingBottom(0);
		cell.setPaddingLeft(0);
		cell.setPaddingRight(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		return cell;
	}
	
	public PdfPCell createIdCardFooterCell(){
		PdfPCell cell=null;
//		if(comp.getIdCarduploadFooter()!=null&&!comp.getIdCarduploadFooter().getUrl().equals("")){
//			cell=pdf.getPhotoCell(comp.getIdCarduploadFooter(), 20f, 38, 0, 0);
//		}else{
//			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 38);
//		}
		if(comp.getIdCarduploadFooter()!=null&&!comp.getIdCarduploadFooter().getUrl().equals("")){
			cell=pdf.getPhotoCell(comp.getIdCarduploadFooter(), 20f, 53, 0, 0,0); 
		}else{
			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 53);
		}
		cell.setBorder(0);
		cell.setUseVariableBorders(true);
		cell.setPaddingTop(0);
		cell.setPaddingBottom(0);
		cell.setPaddingLeft(0);
		cell.setPaddingRight(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		return cell;
	}
	
	public PdfPTable createIdCardPhotoTbl(){
		PdfPTable tbl=new PdfPTable(3);
		tbl.setWidthPercentage(100f);
		
		try {
			tbl.setWidths(new float[]{25,50,25});
		} catch (DocumentException e) {
			e.printStackTrace();
		}	
			
		tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		
		PdfPCell cell=new PdfPCell();
		cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 60);
		if(employee.getPhoto()!=null&&!employee.getPhoto().getUrl().equals("")){
			cell=pdf.getPhotoCell(employee.getPhoto(), 20f, 60, 0, 0,1);
		}
		cell.setBorder(0);  // remove photo border
		tbl.addCell(cell);

		
		tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 60)).setBorder(0);
		return tbl;
	}
	
	public PdfPTable createIdCardDetails() {
		
		PdfPTable tbl=new PdfPTable(3);
		tbl.setWidthPercentage(80f);
		
		try {
			tbl.setWidths(new float[]{30,5,65});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		Phrase colonPh=new Phrase(":",font8);
		PdfPCell colonCell=new PdfPCell();
		colonCell.addElement(colonPh);
		colonCell.setBorder(0);
		
		tbl.addCell(getCell("Name",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl.addCell(colonCell);
		tbl.addCell(getCell(employee.getFullname(),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		tbl.addCell(getCell("Desg.",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl.addCell(colonCell);
		tbl.addCell(getCell(employee.getDesignation(),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		tbl.addCell(getCell("Emp. Id",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl.addCell(colonCell);
		tbl.addCell(getCell(employee.getCount()+"",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		tbl.addCell(getCell("D.O.J.",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl.addCell(colonCell);
		tbl.addCell(getCell(fmt.format(employee.getJoinedAt()),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(printMobileNoOnIdCard){
			if(employee.getCellNumber1()!=0){
				tbl.addCell(getCell("Mobile No.",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				tbl.addCell(colonCell);
				String aadharNum=employee.getCellNumber1()+"";
				tbl.addCell(getCell(aadharNum+"",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			}
		}else{
			tbl.addCell(getCell("Aadhar No.",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			tbl.addCell(colonCell);
			String aadharNum="";
			if(employee.getAadharNumber()!=0){
				aadharNum=employee.getAadharNumber()+"";
			}
			tbl.addCell(getCell(aadharNum+"",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		
		return tbl;

	}

	public PdfPTable createNameTagTbl(){
		
		PdfPTable tbl=new PdfPTable(2);
		tbl.setWidthPercentage(100f);
		
		try {
			tbl.setWidths(new float[]{20,80});
//			tbl.setWidths(new float[]{16,80});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPCell cell=new PdfPCell();
		cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 0);
		if(comp.getIdCardLogo()!=null&&!comp.getIdCardLogo().getUrl().equals("")){
			cell=pdf.getPhotoCell(comp.getIdCardLogo(), 20f, 47.52f, 0, 0,0);
		}
		cell.setBorder(0);
		cell.setUseVariableBorders(true);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cell.setPaddingTop(4);
//		cell.setPaddingBottom(4);
//		cell.setPaddingLeft(2);
		tbl.addCell(cell);
		
			
		String firstName=employee.getFullName();
		String[] nameArray = firstName.split("\\s+");
		if(nameArray.length>0){
			firstName=nameArray[0].toUpperCase();
		}
		
		PdfPCell cell1=new PdfPCell();
		cell1=pdf.getCell(firstName, font18bold, Element.ALIGN_CENTER, 0, 0, 0);
		cell1.setVerticalAlignment(Element.ALIGN_CENTER);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setPaddingTop(10);
//		cell.setPaddingBottom(4);
		cell1.setFixedHeight(47.52f);
		tbl.addCell(cell1).setBorder(0);
		
		return tbl;
	}
	
	public PdfPCell getCell(String phName,Font font,int horizontalAlignment,int rowspan,int colspan,int fixedHeight){
		
		Phrase phrase;
		if(phName != null) {
			phrase = new Phrase(phName,font);
		}else {
			phrase = new Phrase("",font);
		}
		
		PdfPCell cell=new PdfPCell();
		cell.addElement(phrase);
		cell.setHorizontalAlignment(horizontalAlignment);
		if(fixedHeight!=0){
			cell.setFixedHeight(fixedHeight);
		}
		if(rowspan!=0){
			cell.setRowspan(rowspan);
		}if(colspan!=0){
			cell.setColspan(colspan);
		}
		return cell;
	}
	
}
