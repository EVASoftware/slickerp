package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class EmployeeIDCardlandscapePdf {

	public Document document;
	Employee employee;
	EmployeeAdditionalDetails empadditionaldetails;
	Company comp;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	

	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	PdfUtility pdf=new PdfUtility();
	
	public void getEmployeeDeatils(Long count) {
		employee = ofy().load().type(Employee.class).id(count).now();

		if (employee.getCompanyId() != null)
			comp = ofy().load().type(Company.class).filter("companyId", employee.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		
	}

	public void createPdf() {
//		if (comp.getUploadHeader() != null) {
//			createCompanyNameAsHeader(document, comp);
//		}
//		createEmployeePhoto();
//		if (comp.getUploadFooter() != null) {
//			createCompanyNameAsFooter(document, comp);
//		}
		
		/**
		 * @author Anil @since 18-03-2021
		 * Width  : 86mm - 8.6cm - 3.38583inch - 243.77976
		 * Height : 54mm - 5.4cm - 2.12598inch - 153.07056
		 */
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		table.addCell(createIdCardHeaderCell());  // 45
		
		
		PdfPTable empDetTbl = new PdfPTable(2);
		empDetTbl.setWidthPercentage(100f);
		
		try {
			empDetTbl.setWidths(new float[]{35,65});
		} catch (DocumentException e) {
			e.printStackTrace();
		}	
		
		PdfPCell photoCell=new PdfPCell(createIdCardPhotoTbl());
//		photoCell.addElement(createIdCardPhotoTbl());	//93.07056
		photoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		photoCell.setVerticalAlignment(Element.ALIGN_CENTER);
		photoCell.setFixedHeight(93.07056f);			//93.07056
		
		PdfPCell idDetailsCell=new PdfPCell(createIdCardDetails());
		idDetailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		idDetailsCell.setVerticalAlignment(Element.ALIGN_CENTER);
		idDetailsCell.setFixedHeight(93.07056f);			//93.07056
//		idDetailsCell.setPaddingLeft(5.6692944f);
		
		empDetTbl.addCell(photoCell).setBorder(0);
		empDetTbl.addCell(idDetailsCell).setBorder(0);
		
		PdfPCell empDetCell=new PdfPCell();
		empDetCell.addElement(empDetTbl);
		empDetCell.setBorder(0);
		empDetCell.setFixedHeight(93.07056f);
		
		table.addCell(empDetCell);
		
//		table.addCell(createIdCardFooterCell());			//15
		PdfPCell footerCell=new PdfPCell(createIdCardFooterCell());
		footerCell.setBorder(0);
//		footerCell.setPaddingTop(0);
//		footerCell.setPaddingBottom(0);
//		footerCell.setPaddingLeft(0);
//		footerCell.setPaddingRight(0);
		footerCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		footerCell.setFixedHeight(15f);
		table.addCell(footerCell);
		
		table.setTotalWidth(243.77976f);
		table.setLockedWidth(true);
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		
		
		try {
			document.add(table);  
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		PdfPTable table2 = new PdfPTable(1);
		table2.setWidthPercentage(100f);
		
		table2.addCell(createIdCardBackSideCell()); //153.07056
		
		table2.setTotalWidth(243.77976f);
		table2.setLockedWidth(true);
		table2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(Chunk.NEXTPAGE);
			document.add(table2);  
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public PdfPCell createIdCardBackSideCell(){
		PdfPCell cell=null;
		if(comp.getIdCardBackSideLogo()!=null&&!comp.getIdCardBackSideLogo().getUrl().equals("")){
			cell=pdf.getPhotoCell(comp.getIdCardBackSideLogo(), 20f, 153, 0, 0,0);  
		}else{
			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 153);
		}
		cell.setBorder(0);
//		cell.setUseVariableBorders(true);
		cell.setPaddingTop(0);
		cell.setPaddingBottom(0);
		cell.setPaddingLeft(0);
		cell.setPaddingRight(0);
		cell.setFixedHeight(153.07056f);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		return cell;
	}
	
	public PdfPCell createIdCardHeaderCell(){
		
		PdfPCell cell=new PdfPCell();
		if(comp.getIdCarduploadHeader()!=null&&!comp.getIdCarduploadHeader().getUrl().equals("")){
			cell=pdf.getPhotoCell(comp.getIdCarduploadHeader(), 20f, 45, 0, 0,0);  
		}else{
			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 45);
		}
		cell.setBorder(0);
//		cell.setUseVariableBorders(true);
		cell.setPaddingTop(10);
		cell.setPaddingBottom(0);
		cell.setPaddingLeft(15);
		cell.setPaddingRight(15);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		return cell;
	}
	
	public PdfPTable createIdCardFooterCell(){
		PdfPTable tbl=new PdfPTable(1);
		tbl.setWidthPercentage(100f);
		
		PdfPCell cell=new PdfPCell();
		if(comp.getIdCarduploadFooter()!=null&&!comp.getIdCarduploadFooter().getUrl().equals("")){
			cell=pdf.getPhotoCell(comp.getIdCarduploadFooter(), 20f, 15, 0, 0,0); 
		}else{
			cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 15);
		}
		cell.setBorder(0);
		cell.setUseVariableBorders(true);
		cell.setPaddingTop(0);
		cell.setPaddingBottom(0);
		cell.setPaddingLeft(0);
		cell.setPaddingRight(0);
		cell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		tbl.addCell(cell);
		return tbl;
	}
	
	public PdfPTable createIdCardPhotoTbl(){
		PdfPTable tbl=new PdfPTable(1);
		tbl.setWidthPercentage(100f);
		
		PdfPCell cell=new PdfPCell();
		cell=pdf.getCell("", font8, Element.ALIGN_CENTER, 0, 0, 93);
		if(employee.getPhoto()!=null&&!employee.getPhoto().getUrl().equals("")){
			cell=pdf.getPhotoCell(employee.getPhoto(), 20f, 93, 0, 0,1);
//			cell.setBorder(0);
		}
		cell.setBorder(0);  // remove photo border
		cell.setFixedHeight(93.07056f);
		
		cell.setPaddingTop(0);
		cell.setPaddingBottom(0);
		cell.setPaddingLeft(15);
		cell.setPaddingRight(10);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		tbl.addCell(cell);
		
		return tbl;
	}
	
	public PdfPTable createIdCardDetails() {
		
		PdfPTable tbl=new PdfPTable(3);
		tbl.setWidthPercentage(100f);
		
		try {
			tbl.setWidths(new float[]{30,5,65});
		} catch (DocumentException e) {
			e.printStackTrace();
		}	
		
		Phrase colonPh=new Phrase(":",font8);
		PdfPCell colonCell=new PdfPCell(colonPh);
//		colonCell.addElement(colonPh);
		colonCell.setBorder(0);
		
		tbl.addCell(getCell("Name",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl.addCell(colonCell);
		tbl.addCell(getCell(employee.getFullname(),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getDesignation()!=null&&!employee.getDesignation().equals("")){
			tbl.addCell(getCell("Designation",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			tbl.addCell(colonCell);
			tbl.addCell(getCell(employee.getDesignation(),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
//		tbl.addCell(getCell(" ",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		tbl.addCell(getCell(" ",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//		tbl.addCell(getCell(" ",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		tbl.addCell(getCell("Employee ID",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		tbl.addCell(colonCell);
		tbl.addCell(getCell(employee.getCount()+"",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(employee.getDob()!=null){
			tbl.addCell(getCell("DOB",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			tbl.addCell(colonCell);
			tbl.addCell(getCell(fmt.format(employee.getDob()),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		if(employee.getJoinedAt()!=null){
			tbl.addCell(getCell("DOJ",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			tbl.addCell(colonCell);
			tbl.addCell(getCell(fmt.format(employee.getJoinedAt()),font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		if(employee.getCellNumber1()!=0){
			tbl.addCell(getCell("Contact No",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			tbl.addCell(colonCell);
			String aadharNum=employee.getCellNumber1()+"";
			tbl.addCell(getCell(aadharNum+"",font7bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		
		return tbl;

	}
	
public PdfPCell getCell(String phName,Font font,int horizontalAlignment,int rowspan,int colspan,int fixedHeight){
		
		Phrase phrase;
		if(phName != null) {
			phrase = new Phrase(phName,font);
		}else {
			phrase = new Phrase("",font);
		}
		
		PdfPCell cell=new PdfPCell(phrase);
//		cell.addElement(phrase);
		cell.setHorizontalAlignment(horizontalAlignment);
		if(fixedHeight!=0){
			cell.setFixedHeight(fixedHeight);
		}
		if(rowspan!=0){
			cell.setRowspan(rowspan);
		}if(colspan!=0){
			cell.setColspan(colspan);
		}
		return cell;
	}

	private void createCompanyNameAsFooter(Document doc, Company comp2) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl
//					+ document.getUrl()));
//			image2.scalePercent(18f);
//			image2.scaleAbsoluteWidth(240f);
//			image2.setAbsolutePosition(0.7f, 0.5f);
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		 try
		 {
		 Image
		 image1=Image.getInstance("images/portratfooter.jpg");
		 image1.scalePercent(18f);
		 image1.scaleAbsoluteWidth(240f);
		 image1.setAbsolutePosition(1f,0.5f);
		 doc.add(image1);
		 }
		 catch(Exception e)
		 {
		 e.printStackTrace();
		 }
	}

	private void createEmployeePhoto() {
		
		DocumentUpload photodocument = employee.getPhoto();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + photodocument.getUrl()));
			image2.scalePercent(36f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
//			imageSignCell.addElement(image2);
//			imageSignCell.setPadding(2);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 Image image1=null;
//		 try
//		 {
//		 image1=Image.getInstance("images/Jayshree Pannu1.jpg");
//		 image1.scalePercent(36f);
//		 // image1.setAbsolutePosition(40f,765f);
//		 // doc.add(image1);
//		
//		 imageSignCell=new PdfPCell(image1);
////		 imageSignCell.addElement(image1);
//		 imageSignCell.setFixedHeight(40);
//		 imageSignCell.setBorder(0);
//		 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }

		PdfPTable photoTab = new PdfPTable(1);
		photoTab.setWidthPercentage(100);
//		photoTab.setSpacingBefore(10);
//		photoTab.setSpacingAfter(10);

		try {
			photoTab.setWidths(new float[] {33});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase blank =new Phrase(" ",font10);
		PdfPCell blankcell=new PdfPCell(blank);
		blankcell.setBorder(0);
		if (imageSignCell != null) {
			
//			photoTab.addCell(blankcell);
			photoTab.addCell(imageSignCell);
//			photoTab.addCell(blankcell);
			} 
			else {
			
//			photoTab.addCell(blankcell);
			photoTab.addCell(blankcell);
//			photoTab.addCell(blankcell);

		}
		
//		try {
//			document.add(photoTab);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		// logo tab Complete
	
		

		
		
		PdfPTable infotab= new PdfPTable (1);
		infotab.setWidthPercentage(100);
		
		Phrase name=new Phrase("   "+employee.getFullname(),font8);
		PdfPCell namecell=new PdfPCell(name);
		namecell.setBorder(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infotab.addCell(namecell);
		
		Phrase designation=new Phrase("   "+employee.getDesignation(),font8);
		PdfPCell designationcell=new PdfPCell(designation);
		designationcell.setBorder(0);
		designationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infotab.addCell(designationcell);
		
		Phrase empId=new Phrase("   EMP ID : "+employee.getCount(),font8);
		PdfPCell empIdcell=new PdfPCell(empId);
		empIdcell.setBorder(0);
		empIdcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infotab.addCell(empIdcell);
		
		Phrase empdob=new Phrase("   D.O.B. : "+fmt.format(employee.getDob()),font8);
		PdfPCell empdobcell=new PdfPCell(empdob);
		empdobcell.setBorder(0);
		empdobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infotab.addCell(empdobcell);
		
		Phrase empdoj=new Phrase("   D.O.J. : "+fmt.format(employee.getJoinedAt()),font8);
		PdfPCell empdojcell=new PdfPCell(empdoj);
		empdojcell.setBorder(0);
		empdojcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infotab.addCell(empdojcell);
		
//		try {
//			document.add(infotab);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		PdfPTable maintab=new PdfPTable(2);
		maintab.setWidthPercentage(100);
		
		maintab.setSpacingAfter(20);
		maintab.setSpacingBefore(10);
		
		try {
			maintab.setWidths(new float[]{70,30});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell infocell=new PdfPCell(infotab);
		infocell.setBorder(0);
		maintab.addCell(infocell);
		
		
		PdfPCell photocell=new PdfPCell(photoTab);
		photocell.setBorder(0);
		photocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		maintab.addCell(photocell);
		
		try {
			document.add(maintab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createCompanyNameAsHeader(Document doc, Company comp2) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl
//					+ document.getUrl()));
//			image2.scalePercent(18f);
//			image2.scaleAbsoluteWidth(240f);
////			image2.setAbsolutePosition(40f, 725f);
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		 try
		 {
		 Image
		 image1=Image.getInstance("images/portrateheader2.jpg");
		 image1.scalePercent(18f);
		 image1.scaleAbsoluteWidth(240f);
//		 image1.setAbsolutePosition(40f,725f);
		 doc.add(image1);
		 }
		 catch(Exception e)
		 {
		 e.printStackTrace();
		 }
	}

}
