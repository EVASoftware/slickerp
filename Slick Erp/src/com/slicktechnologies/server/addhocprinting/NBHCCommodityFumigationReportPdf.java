package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class NBHCCommodityFumigationReportPdf {
		
 
	public Document document;
	Service service;
	Company comp;
	
	List<ServiceProject> projectlist = new ArrayList<ServiceProject>();
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font11bold, font12boldul, font12, font10bold, font10, font14bold,
	font9;
	
	SimpleDateFormat fmt =new SimpleDateFormat("dd-MM-yyyy");
	/**30-10-2017 sagar sore [to display partitioned data]**/
//	float[] columnWidths = {1.2f, 0.3f, 8.0f};
	float[] columnWidths = {1.4f, 0.3f, 7.8f};
	float[] columnWidths1 = {1.2f, 0.3f, 1.5f, 1.5f, 0.3f, 1.5f,1.5f,0.3f,1.5f};
	
	float[] columnWidths2 = {1.2f, 0.3f, 0.3f, 0.3f, 0.3f, 0.3f,0.3f,0.3f,0.3f,0.3f};
	float[] columnWidths3 = {1.2f, 1.0f, 1.3f, 1.0f, 1.0f};
	float[] columnWidths4 = {1.2f, 0.5f, 1.2f, 1.2f, 0.5f,1.2f};
	
	/**30-10-2017 sagar sore [changed for partitioned displaying]**/
	//float[] FumigantTableWidths = {1.4f,4.1f,0.9f,1.0f,1.3f,1.8f,1.0f};
	float[] FumigantTableWidths = {1.2f,4.5f,0.9f,0.9f,1.4f,1.6f,0.9f};
	
	///////////////////////////////////////////////// Ajinkya added this to print Pdf twice on 1 Page //////////////////// 
	PdfPTable a5parentTbl = new PdfPTable(2);   
	PdfPTable a5tbl1 = new PdfPTable(1);
	PdfPTable a5tbl2 = new PdfPTable(1);
	
	                      ///////////////////////////// End Here ////////////////////
	
	/*
	 * Date:22-01-2018
	 * @author Ashwini
	 */
	ProcessConfiguration processconfig; 
	Boolean rowflag = false;
	Boolean headerFooter = false;
	/*
	 * end by Ashwini
	 */
	public NBHCCommodityFumigationReportPdf() {

		/**
		 * Date 27/11/2017
		 * By jayshree
		 * Des.Increse the font size by one
		 */
		new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 17, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//checking font 6 by Ajinkya
		font8 = new Font(Font.FontFamily.HELVETICA, 6);//checking font 6 by Ajinkya
		font9 = new Font(Font.FontFamily.HELVETICA, 10);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 13);
		font11bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);//checking font 8 by Ajinkya
		font10 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);   //checking font 8 by Ajinkya
		font14bold = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		//End by Jayshree
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	
	
	
	
	public void getfumigationReport(Long count) {
		
		service=ofy().load().type(Service.class).id(count).now();
		
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", service.getCompanyId()).first().now();
		
		if (service.getCompanyId() == null)
		projectlist = ofy().load().type(ServiceProject.class).filter("serviceId",service.getCount()).list();
		else
		projectlist = ofy().load().type(ServiceProject.class).filter("companyId", service.getCompanyId()).filter("serviceId",service.getCount()).list();
		
		System.out.println("service list size "+projectlist.size());
		
		/*
		 * Date:21-01-2018
		 * @author Ashwini
		 * Des:To dispaly by defaulty 4 rows in in table .
		 */
		
		if(service.getCompanyId()!=null){
			processconfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Service")
					.filter("configStatus", true).first().now();
			if(processconfig!=null){
				for(int k= 0 ;k<processconfig.getProcessList().size() ; k++){
					
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("DecreaseTableRow")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						rowflag = true;
						System.out.println("rowflag:::::");
					}
					
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderFooter")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						headerFooter = true;
						
					}
				}
			}
			
			
		}
		
		/*
		 * end by Ashwini
		 */
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preprint) {
		
		/**
		 * Date 27/11/2017
		 * By Ashwini
		 * Des.to add header and footer
		 */
		//createBlankForLetterHead();
		if(headerFooter){
		if(preprint.equals("no")){
			if(comp.getUploadHeader()!=null){
				/**
				 * Date 06-03-2019 by vijay
				 */
				Paragraph blank = new Paragraph();
				blank.add(Chunk.NEWLINE);
				try {
					document.add(blank);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/**
				 * ends here
				 */
				createCompanyNameAsHeader(document , comp);
			}
				
		}
		
		
		
		if(preprint.equals("yes")){
			createBlankforUPC();
			
		}
		}	
		
//		if(comp.getUploadHeader()!=null){
//			createCompanyNameAsHeader(document , comp);
//		}
			
//		createBlankForLetterHead();
		
		//End By Jayshree
		createserviceFumigationDetails();
		createFumigationnDetails();
		bioassayTestResults();
		commodityDetails();
		feedbackFromCustomer();
		createEmployeeSign();
		remarkColumn();
		
		
		
	
		/*
		 * Date:25-01-2018
		 * @author Ashwini
		 */
		if(headerFooter){
			if(preprint.equals("no")){
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);//Date 27/11/2017 by jayshree comment this line
				}
			} 
		}
		
		
		
		/*
		 * end by Ashwini
		 */
		
		
		/*
		 * commented by Ashwini
		 */
		
//		if(comp.getUploadFooter()!=null){
//			createCompanyNameAsFooter(document,comp);//Date 27/11/2017 by jayshree comment this line
//		}
		
		//End by Jayshree
		addToParentTable();
		
	}
	
	
	
	
	private void createBlankforUPC() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
private void addToParentTable() 

{
	a5parentTbl.setWidthPercentage(100);
	
	try {
		a5parentTbl.setWidths(new float[] {50,50} );
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	PdfPCell a5tbl1Cell = new PdfPCell();
	a5tbl1Cell.setBorder(0);
	a5tbl1Cell.addElement(a5tbl1);
	/**
	 * Date 27/11/2017
	 * By jayshree
	 * Des.to set the right padding
	 */
	a5tbl1Cell.setPaddingRight(20f);
	//End By Jayshree
	PdfPCell a5tbl2Cell = new PdfPCell();
	a5tbl2Cell.setBorder(0);
	a5tbl2Cell.addElement(a5tbl2);
	/**
	 * Date 27/11/2017
	 * By jayshree
	 * Des.to set the left padding
	 */
	a5tbl2Cell.setPaddingLeft(20f);
	//End By jayshree
	a5parentTbl.addCell(a5tbl1Cell);
	a5parentTbl.addCell(a5tbl2Cell);
	
	try {
		document.add(a5parentTbl);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
		
	}


/**
 * Updated by: Viraj
 * Date: 21-02-2019
 * Description: To show header and footer dynamically
 * @param doc
 * @param comp
 */
private void createCompanyNameAsHeader(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadHeader();

	// patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System.getProperty("com.google.appengine.application.id");
		String version = System.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 40f);
//		doc.add(image2);
		a5tbl1.addCell(image2);
		a5tbl2.addCell(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

private void createCompanyNameAsFooter(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadFooter();

	// patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System.getProperty("com.google.appengine.application.id");
		String version = System.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 40f);
//		doc.add(image2);
		a5tbl1.addCell(image2);
		a5tbl2.addCell(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}
/** Ends **/
	
	private void createserviceFumigationDetails() {
		
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		Paragraph para = new Paragraph("Commodity Fumigation Report",font10bold);
		para.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell paraCell = new PdfPCell();
		paraCell.setBorder(0);
		paraCell.addElement(para);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell);
//		try {
//			document.add(para);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.setSpacingBefore(10f);
		
		
		PdfPTable table1 =new PdfPTable(9);
		table1.setWidthPercentage(100f);
		try {
			table1.setWidths(columnWidths1);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase dotted = new Phrase(" : ",font8bold);
		PdfPCell dottedCell = new PdfPCell(dotted);
		dottedCell.setBorder(0);
		
		Phrase srno = new Phrase("Serial No",font8bold);
		PdfPCell srnoCell = new PdfPCell(srno);
		srnoCell.setBorder(0);
		table1.addCell(srnoCell);
		table1.addCell(dottedCell);
		
		Phrase srnoValue = new Phrase(service.getCount()+"",font8);
		PdfPCell srnoValueCell = new PdfPCell(srnoValue);
		srnoValueCell.setBorder(0);
		table1.addCell(srnoValueCell);
		
		
		
		Phrase branch = new Phrase("Branch",font8);
		PdfPCell branchCell = new PdfPCell(branch);
		branchCell.setBorder(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table1.addCell(branchCell);
		table1.addCell(dottedCell);
		
		Phrase branchValue = new Phrase(service.getBranch(),font8);
		PdfPCell branchValueCell = new PdfPCell(branchValue);
		branchValueCell.setBorder(0);
		table1.addCell(branchValueCell);
		
		
		Phrase date = new Phrase("Date",font8);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setBorder(0);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table1.addCell(dateCell);
		table1.addCell(dottedCell);
		
		Phrase dateValue = new Phrase(fmt.format(service.getServiceDate()),font8);
		PdfPCell dateValueCell = new PdfPCell(dateValue);
		dateValueCell.setBorder(0);
		table1.addCell(dateValueCell);
		
		PdfPCell table1Cell = new PdfPCell(table1);
		table1Cell.setBorder(0);
		
		
		//    for customer name 
		PdfPTable table2 = new PdfPTable(3);
		table2.setWidthPercentage(100f);
		try {
			table2.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		Phrase custName = new Phrase("Customer", font8bold);
		PdfPCell custNameCell = new PdfPCell(custName);
		custNameCell.setBorder(0);
		table2.addCell(custNameCell);
		table2.addCell(dottedCell);
		
		Phrase custNameValue = new Phrase(service.getCustomerName(),font8);
		PdfPCell custNameValueCell = new PdfPCell(custNameValue);
		custNameValueCell.setBorder(0);
		table2.addCell(custNameValueCell);
		
		
	
		float[] mycolumnWidths = {2.5f,0.5f,8.0f};
		
		
		PdfPTable table3= new PdfPTable(3);
		table3.setWidthPercentage(100f);
		try {
			table3.setWidths(mycolumnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		Phrase warehouse =  new Phrase("Warehouse / Compartment",font8bold);
		PdfPCell warehouseCell = new PdfPCell(warehouse);
		warehouseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		warehouseCell.setBorder(0);
		table2.addCell(warehouseCell);
		
		Phrase dottedPh =  new Phrase(" : ",font8bold);
		PdfPCell dottedPhCell = new PdfPCell(dottedPh);
		dottedPhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dottedPhCell.setBorder(0);
		table2.addCell(dottedPhCell);
		
		Phrase warehouseValue =  new Phrase(service.getPremises(),font8);
		PdfPCell warehouseValueCell = new PdfPCell(warehouseValue);
		warehouseValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		warehouseValueCell.setBorder(0);
		table2.addCell(warehouseValueCell);
		
		
		PdfPCell table2Cell = new PdfPCell(table2);
		table2Cell.setBorder(0);
		
		
		parentTable.addCell(table1Cell);
		parentTable.addCell(table2Cell);
		
		PdfPCell parentTblCell = new PdfPCell();
		parentTblCell.setBorder(0);
		parentTblCell.addElement(parentTable);
		
		a5tbl1.addCell(parentTblCell);
		a5tbl2.addCell(parentTblCell);
		
//		try {
//			document.add(parentTable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}





	private void remarkColumn() {
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		/** 30-10-2017 sagar sore [column width changed to show it on sigle line**/
		//float[] columnWidths = {1.0f,9.0f};
		float[] columnWidths = {1.2f,8.8f};
		
		Phrase remark = new Phrase("Remark : ",font10);
		PdfPCell remarkCell = new PdfPCell(remark);
		remarkCell.setBorder(0);
		
		PdfPCell remarkValueCell=null;
		if(service.getRemark()!=null && !service.getRemark().equals(""))
		{
			Phrase remarkValue = new Phrase(service.getRemark(),font10);
			remarkValueCell = new PdfPCell(remarkValue);
			remarkValueCell.setBorder(0);
		}
		else
		{
			Phrase remarkValue = new Phrase(" ",font10);
			remarkValueCell = new PdfPCell(remarkValue);
			remarkValueCell.setBorderWidthLeft(0);
			remarkValueCell.setBorderWidthTop(0);
			remarkValueCell.setBorderWidthRight(0);
		}

		
		/*
		 * commented by Ashwini
		 */
//		PdfPCell extraLine = null;
//		if(service.getRemark()== null && service.getRemark().equals(""))
//			{	
//				Phrase remarkValue = new Phrase(" ",font10);
//				extraLine = new PdfPCell(remarkValue);
//				extraLine.setBorderWidthLeft(0);
//				extraLine.setBorderWidthTop(0);
//				extraLine.setBorderWidthRight(0);
//			}
		/*
		 * end by Ashwini
		 */
		
		PdfPTable table = new PdfPTable(2);    
		
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		table.addCell(remarkCell);
		table.addCell(remarkValueCell);

		/*
		 * commented by Ashwini
		 */
		
//		table.addCell(remarkValueCell);
//		table.addCell(remarkValueCell);
		
		
//		if(service.getRemark()== null && service.getRemark().equals(""))
//		{
//			table.addCell(extraLine);
//			table.addCell(extraLine);
//		}
		
		/*
		 * end here
		 */
		PdfPCell tableCell = new PdfPCell();
		tableCell.setBorder(0);
		tableCell.addElement(table);
		
		a5tbl1.addCell(tableCell);
		a5tbl2.addCell(tableCell);
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}





	private void createEmployeeSign() {
	
		float[] columnwidths = {3f,2f,3f,2f};
		PdfPTable table = new PdfPTable(columnwidths);
		table.setWidthPercentage(100f);
		
		Phrase fumigationStDt = new Phrase("Fumigation Start Date",font10);
		PdfPCell fumigationStDtCell = new PdfPCell(fumigationStDt);
		fumigationStDtCell.setBorder(0);
		table.addCell(fumigationStDtCell);
		
		Phrase phPercentage = new Phrase("RH%",font10);
		PdfPCell phPercentageCell = new PdfPCell(phPercentage);
		phPercentageCell.setBorderWidthRight(0);
		phPercentageCell.setBorderWidthBottom(0);
		phPercentageCell.setBorderWidthTop(0);
		table.addCell(phPercentageCell);
		
		
		Phrase fumigationEndDt = new Phrase("Fumigation End Date ",font10);
		PdfPCell fumigationEndDtCell = new PdfPCell(fumigationEndDt);
		fumigationEndDtCell.setBorderWidthTop(0);
		fumigationEndDtCell.setBorderWidthRight(0);
		fumigationEndDtCell.setBorderWidthBottom(0);
		table.addCell(fumigationEndDtCell);
		table.addCell(phPercentageCell);
		
		Phrase fumifationStDate = new Phrase(fmt.format(service.getServiceDate()),font10);
		PdfPCell fumifationStDateCell = new PdfPCell(fumifationStDate);
		fumifationStDateCell.setBorder(0);
		table.addCell(fumifationStDateCell);
		
		
		Phrase signBlank = new Phrase(" ",font10);
		PdfPCell signBlankCell = new PdfPCell(signBlank);
		signBlankCell.setBorderWidthTop(0);
		signBlankCell.setBorderWidthRight(0);
		signBlankCell.setBorderWidthBottom(0);
		table.addCell(signBlankCell);
		
		 Calendar now = Calendar.getInstance();
		 now.setTime(service.getServiceDate());
		 now.add(Calendar.DATE, 7);
		
		Phrase fumifationEndDate = new Phrase(fmt.format(now.getTime()),font10);
		PdfPCell fumifationEndDateCell = new PdfPCell(fumifationEndDate);
		fumifationEndDateCell.setBorderWidthBottom(0);
		fumifationEndDateCell.setBorderWidthTop(0);
		fumifationEndDateCell.setBorderWidthRight(0);
		table.addCell(fumifationEndDateCell);
		table.addCell(signBlankCell);
		
		
			
		PdfPCell tableCell = new PdfPCell();
		tableCell.addElement(table);

		PdfPTable table1 = new PdfPTable(6);
		table1.setWidthPercentage(100f);
		try {
			table1.setWidths(columnWidths4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
			Phrase blank = new Phrase(" ",font10);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setBorder(0);
			table1.addCell(blankCell);
			table1.addCell(blankCell);
			table1.addCell(blankCell);
			Phrase leftVisibleLine = new Phrase();
			PdfPCell leftVisibleLineCell = new PdfPCell(leftVisibleLine);
			leftVisibleLineCell.setBorderWidthTop(0);
			leftVisibleLineCell.setBorderWidthBottom(0);
			leftVisibleLineCell.setBorderWidthRight(0);
			
			table1.addCell(leftVisibleLineCell);
			table1.addCell(blankCell);
			table1.addCell(blankCell);
	
		
		Phrase employeeSignature = new Phrase("Employee Signature",font10);
		PdfPCell employeeSignatureCell = new PdfPCell(employeeSignature);
		employeeSignatureCell.setBorder(0);
		table1.addCell(employeeSignatureCell);
		table1.addCell(blankCell);
		
		Phrase customerSignature = new Phrase("Customer Signature",font10);
		PdfPCell customerSignatureCell = new PdfPCell(customerSignature);
		customerSignatureCell.setBorder(0);
		table1.addCell(customerSignatureCell);
		
		
		Phrase employeeSignature1 = new Phrase("Employee Signature",font10);
		PdfPCell employeeSignature1Cell = new PdfPCell(employeeSignature1);
		employeeSignature1Cell.setBorderWidthRight(0);
		employeeSignature1Cell.setBorderWidthBottom(0);
		employeeSignature1Cell.setBorderWidthTop(0);
		table1.addCell(employeeSignature1Cell);
		table1.addCell(blankCell);
		table1.addCell(customerSignatureCell);
		
		PdfPCell table1Cell =new PdfPCell();
		table1Cell.addElement(table1);
		
		PdfPTable parentTable  = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.setSpacingBefore(10f);
		
		parentTable.addCell(tableCell);
		parentTable.addCell(table1Cell);
		
		PdfPCell parentTblCell = new PdfPCell(); 
		parentTblCell.setBorder(0);
		parentTblCell.addElement(parentTable);
		
		a5tbl1.addCell(parentTblCell);
		a5tbl2.addCell(parentTblCell);
//		try {
//			document.add(parentTable);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}
	
	private void commodityDetails() {
		
		Paragraph para = new Paragraph("Details of Commodity Fumigated:",font10);
		para.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell paraCell = new PdfPCell();
		paraCell.addElement(para);
		paraCell.setBorder(0);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell);
		
//		try {
//			document.add(para);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	
		PdfPTable table1 =new PdfPTable(3);
		table1.setWidthPercentage(100f);
//		table1.setSpacingBefore(10f);//Date27/11/2017 by jayshree to cmment the code to remove space
		try {
			table1.setWidths(columnWidths3);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase commodity = new Phrase("Commodity",font8bold);
		PdfPCell commodityCell = new PdfPCell(commodity);
		commodityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table1.addCell(commodityCell);
		
		Phrase stackNo = new Phrase("Stack / Silo Number",font8bold);
		PdfPCell stackNoCell = new PdfPCell(stackNo);
		stackNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table1.addCell(stackNoCell);
		
		Phrase quantityperbag = new Phrase("Quantity in MT",font8bold);
		PdfPCell quantityperbagCell = new PdfPCell(quantityperbag);
		quantityperbagCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table1.addCell(quantityperbagCell);
		
//		Phrase noofbags = new Phrase("No. of bags",font8bold);
//		PdfPCell noofbagsCell = new PdfPCell(noofbags);
//		noofbagsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		table1.addCell(noofbagsCell);
//		
//		Phrase Quantity = new Phrase("Quantity (Kg)/(MT)",font8bold);
//		PdfPCell QuantityCell = new PdfPCell(Quantity);
//		QuantityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		table1.addCell(QuantityCell);
		
		PdfPCell blankspaceCell = null;
		/*
		 * commented by Ashwini
		 */
//		for (int i = 0; i < 30; i++) {
//			
//			Phrase blankspace = new Phrase(" ",font8bold);
//			blankspaceCell = new PdfPCell(blankspace);
//			blankspaceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table1.addCell(blankspaceCell);
//		}

		/*
		 * Date:25-01-2018
		 * @author Ashwini
		 */
		if(rowflag){
			
			for (int i = 0; i < 12; i++) {
	               Phrase blankspace = new Phrase(" ",font8bold);
					blankspaceCell = new PdfPCell(blankspace);
					blankspaceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table1.addCell(blankspaceCell);
				}
			
		}else{
			
			for (int i = 0; i < 30; i++) {
               Phrase blankspace = new Phrase(" ",font8bold);
				blankspaceCell = new PdfPCell(blankspace);
				blankspaceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table1.addCell(blankspaceCell);
			}
		}
		table1.addCell(blankspaceCell);
//		table1.addCell(blankspaceCell);
//		table1.addCell(blankspaceCell);
		
		
		Phrase total = new Phrase("Total Quantity",font8bold);
		PdfPCell totalCell = new PdfPCell(total);
		totalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table1.addCell(totalCell);
		table1.addCell(blankspaceCell);
		
		PdfPCell tableCell = new PdfPCell();
		tableCell.setBorder(0);
		tableCell.addElement(table1);
		
		a5tbl1.addCell(tableCell);
		a5tbl2.addCell(tableCell);
		
		
//		try {
//			document.add(table1);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	
	}





private void createBlankForLetterHead(){
		
		a5tbl1.setWidthPercentage(100);
		a5tbl2.setWidthPercentage(100);
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
	  
		PdfPCell paraCell = new PdfPCell(blank);
		paraCell.setBorder(0);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell); 

	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
		PdfPCell blTableCell = new PdfPCell();
		blTableCell.setBorder(0);
		blTableCell.addElement(bltable);

		System.out.println("BLANK CELL ADDED IN DOCUMENT...");

	}

	private void createFumigationnDetails() {
		
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100f);
//		table.setSpacingBefore(10f); //Date27/11/2017 by jayshree to cmment the code to remove space
		
		try {
			table.setWidths(FumigantTableWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		System.out.println("M2 Ajinkya added FumigationTbl ");
		
		Phrase fumigantUsed =new Phrase("Fumigant Used",font8bold);
		PdfPCell fumigantUsedCell = new PdfPCell(fumigantUsed);
		/** 30-10-2017 sagar sore []**/
		fumigantUsedCell.setPadding(1);
		fumigantUsedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(fumigantUsedCell);
		
		Phrase packType =new Phrase("Pack Type",font8bold);
		PdfPCell packTypeCell = new PdfPCell(packType);
		packTypeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(packTypeCell);
		
		Phrase noUsed =new Phrase("No. Used",font8bold);
		PdfPCell noUsedCell = new PdfPCell(noUsed);
		noUsedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(noUsedCell);
		
		Phrase qtyUsed =new Phrase("Qty. Used",font8bold);
		PdfPCell qtyUsedCell  = new PdfPCell(qtyUsed);
		qtyUsedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(qtyUsedCell);
		
		Phrase manufacturer  =new Phrase("Manufacturer",font8bold);
		PdfPCell manufacturerCell  = new PdfPCell(manufacturer);
		/** 30-10-2017 sagar sore []**/
		manufacturerCell.setPadding(1);
		manufacturerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(manufacturerCell);
		
		
		Phrase manufacturerDate  =new Phrase("Date of Manufacture",font8bold);
		PdfPCell manufacturerDateCell  = new PdfPCell(manufacturerDate);
		/** 30-10-2017 sagar sore []**/
		manufacturerDateCell.setPadding(1);
		manufacturerDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(manufacturerDateCell);
		
		Phrase batchNo  =new Phrase("Batch No",font8bold);
		PdfPCell batchNoCell  = new PdfPCell(batchNo);
		batchNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(batchNoCell);
		
		
		Phrase blank1 = new Phrase(" ",font8);
		PdfPCell blank1Cell = new PdfPCell(blank1);
		blank1Cell.setBorder(0);
		
		PdfPCell blank2Cell = new PdfPCell(blank1);
		
		Phrase serviceALP = new Phrase("[ ] ALP",font8);
		PdfPCell serviceTypeCell = new PdfPCell(serviceALP);
		serviceTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase servicePackType = new Phrase("[ ] 3gm Tablets  [ ] 10 gm Pouch  [ ] 34 gm Pouch",font8);
		PdfPCell servicePackTypeCell = new PdfPCell(servicePackType);
        /** 30-10-2017 sagar sore[]**/
		servicePackTypeCell.setPadding(1);
		servicePackTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable gmTbale = new PdfPTable(2);
		gmTbale.setWidthPercentage(100);
//		gmTbale.setSpacingAfter(4f);//Date27/11/2017 by jayshree to cmment the code to remove space
		
		gmTbale.addCell(blank1Cell);
		
		Phrase gm = new Phrase("gm",font8);
		PdfPCell gmCell = new PdfPCell(gm);
		gmCell.setBorder(0);
		gmTbale.addCell(gmCell);
		
		PdfPCell gmColumnCell = new PdfPCell(gmTbale);
		
		System.out.println("service list size "+projectlist.size());
		
		table.addCell(serviceTypeCell);
		table.addCell(servicePackTypeCell);
		table.addCell(blank2Cell);
		table.addCell(gmColumnCell);
		table.addCell(blank2Cell);
		table.addCell(blank2Cell);
		table.addCell(blank2Cell);
		
		Phrase serviceMBR = new Phrase("[ ] MBR",font8);
		PdfPCell serviceMBRCell = new PdfPCell(serviceMBR);
		serviceMBRCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase serviceMBRPackType = new Phrase("[ ] 1 lb Can  [ ] 1.5 lb can   [ ] Cylinder",font8);
		PdfPCell serviceMBRPackTypeCell = new PdfPCell(serviceMBRPackType);
		serviceMBRPackTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(serviceMBRCell);
		table.addCell(serviceMBRPackTypeCell);
		table.addCell(blank2Cell);
		table.addCell(gmColumnCell);
		table.addCell(blank2Cell);
		table.addCell(blank2Cell);
		table.addCell(blank2Cell);
		
		
		//   old code commented by rohan 
		
//		if(projectlist.size()==0){
//		table.addCell(serviceTypeCell);
//		table.addCell(blank2Cell);
//		table.addCell(blank2Cell);
//		table.addCell(gmColumnCell);
//		table.addCell(blank2Cell);
//		table.addCell(blank2Cell);
//		table.addCell(blank2Cell);
//		}
//		else
//		{
//			for (int i = 0; i < projectlist.size(); i++) {
//				
//				System.out.println("project list size"+ projectlist.get(i).getProdDetailsList().size());
//				for (int j = 0; j < projectlist.get(i).getProdDetailsList().size(); j++) {
//					
//					Phrase type = new Phrase(projectlist.get(i).getProdDetailsList().get(j).getName(),font8);
//					PdfPCell typeCell = new PdfPCell(type);
//					typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//					
//					table.addCell(serviceTypeCell);
//					table.addCell(typeCell);
//					table.addCell(blank2Cell);
//					table.addCell(gmColumnCell);
//					table.addCell(blank2Cell);
//					table.addCell(blank2Cell);
//					table.addCell(blank2Cell);
//				}
//			}
//			
//			
//		}
		
		PdfPCell tblCell = new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table);
		
		a5tbl1.addCell(tblCell);
		a5tbl2.addCell(tblCell);
		
		
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
	}

	
	
	private void bioassayTestResults() {
		
		System.out.println("M3 Ajinkya added A5sizeTable");
		
		Paragraph para = new Paragraph("Bioassay Test Results",font10);
		para.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell paraCell = new PdfPCell();
		paraCell.setBorder(0);
		paraCell.addElement(para);
		
		a5tbl1.addCell(paraCell);
		a5tbl2.addCell(paraCell);
		
		
//		try {
//			document.add(para);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	
		PdfPTable table1 =new PdfPTable(10);
		table1.setWidthPercentage(100f);
//		table1.setSpacingBefore(10f);//Date27/11/2017 by jayshree to cmment the code to remove space
		try {
			table1.setWidths(columnWidths2);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase viralUsed = new Phrase("Vials used:",font8bold);
		PdfPCell viralUsedCell = new PdfPCell(viralUsed);
		table1.addCell(viralUsedCell);
		
		for (int i = 1; i < 10; i++) {
			
			Phrase number = new Phrase(i+"",font8bold);
			PdfPCell numberCell = new PdfPCell(number);
			numberCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table1.addCell(numberCell);
		}
		
		Phrase liveInsects = new Phrase("No. of live insects",font8);
		PdfPCell liveInsectsCell = new PdfPCell(liveInsects);
		table1.addCell(liveInsectsCell);
		
		for (int i = 1; i < 10; i++) {
			
			Phrase number = new Phrase(" ",font8bold);
			PdfPCell numberCell = new PdfPCell(number);
			table1.addCell(numberCell);
		}
		
		Phrase deadInsects = new Phrase("No. of dead insects",font8);
		PdfPCell deadInsectsCell = new PdfPCell(deadInsects);
		table1.addCell(deadInsectsCell);
		
		for (int i = 1; i < 10; i++) {
			
			Phrase number = new Phrase(" ",font8bold);
			PdfPCell numberCell = new PdfPCell(number);
			table1.addCell(numberCell);
		}
		
		PdfPCell tblCell = new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table1);
		
		a5tbl1.addCell(tblCell);
		a5tbl2.addCell(tblCell);
		
//		try {
//			document.add(table1);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}
	
	private void createCompanyDetails() {

		System.out.println("M4 Ajinkya added A5sizeTable");
		
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		Phrase reg = new Phrase("Registered Office: ", font10bold);
		PdfPCell regCell = new PdfPCell(reg);
		regCell.setBorder(0);
		regCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(regCell);
		
		
		Phrase name = new Phrase(comp.getBusinessUnitName(), font10bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nameCell);

		Phrase hardcoded = new Phrase(
				"Division: Commodity Care and Pest Management", font8bold);
		PdfPCell hardcodedCell = new PdfPCell(hardcoded);
		hardcodedCell.setBorder(0);
		hardcodedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hardcodedCell);

		// / address *************

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (comp.getAddress() != null) {

			if (!comp.getAddress().getAddrLine2().equals("")) {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (!comp.getAddress().getLocality().equals("")) {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getLocality()
						+ "," + comp.getAddress().getCity() + "-"
						+ comp.getAddress().getPin() + ","
						+ comp.getAddress().getState()
						+ comp.getAddress().getCountry();

			} else {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getCity()
						+ "-" + comp.getAddress().getPin() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCountry();
			}
		}

		Phrase address = new Phrase(custFullAdd1, font8);
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		addressCell.setBorder(0);
		table.addCell(addressCell);

		// ends here *******************

		// contact details ************

		String contactInfo = "Tel :" + comp.getCellNumber1();

		if (comp.getFaxNumber() != null) {
			contactInfo = contactInfo + " | Fax :" + comp.getFaxNumber();
		}

		if (comp.getEmail() != null) {
			contactInfo = contactInfo + " | Email :" + comp.getEmail();
		}
		if (comp.getWebsite() != null) {
			contactInfo = contactInfo + " | Website :" + comp.getWebsite();
		}

		Phrase contactDetailsInfo = new Phrase(contactInfo, font8);
		PdfPCell contactDetailsInfoCell = new PdfPCell(contactDetailsInfo);
		contactDetailsInfoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		contactDetailsInfoCell.setBorder(0);
		table.addCell(contactDetailsInfoCell);
		// ends here **********************

		PdfPCell tblCell = new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table);
		
		a5tbl1.addCell(tblCell);
		a5tbl2.addCell(tblCell);
		
//		
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
	}
	
	
//  rohan added this code for NBHC customer feed back 	
	
	private void feedbackFromCustomer() {
		
		System.out.println("M6 Ajinkya added A5sizeTable");
		
		float[] columnWidths = {49,1,50};
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPTable obsTable = new PdfPTable(1);
		obsTable.setWidthPercentage(100f);
		
		Phrase obsByNBHC = new Phrase("Observations",font8bold);//"Observations by NBHC"
		PdfPCell obsByNBHCCell = new PdfPCell(obsByNBHC);
		obsByNBHCCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		obsByNBHCCell.setBorder(0);
		obsTable.addCell(obsByNBHCCell);
		
		Phrase sideBlank = null;
		if(service.getComment()!=null)
		{
			sideBlank = new Phrase(service.getComment(),font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
			sideBlankCell.setBorder(0);
			obsTable.addCell(sideBlankCell);
		}
		else
		{
			sideBlank = new Phrase(" ",font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
			sideBlankCell.setBorderWidthRight(0);
			sideBlankCell.setBorderWidthLeft(0);
			sideBlankCell.setBorderWidthTop(0);
			
			for (int i = 0; i <= 5; i++) {
				obsTable.addCell(sideBlankCell);
			}
		}
		
		PdfPCell sideBlankCell = new PdfPCell(sideBlank);
		sideBlankCell.setBorder(0);
		obsTable.addCell(sideBlankCell);
		
//		sideBlankCell.setBorderWidthLeft(0);
//		sideBlankCell.setBorderWidthRight(0);
//		sideBlankCell.setBorderWidthTop(0);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
		
		
		PdfPCell obsTableCell = new PdfPCell();
		obsTableCell.addElement(obsTable);
		obsTableCell.setBorder(0);
		
		
		
		
		PdfPTable blankTable = new PdfPTable(1);
		blankTable.setWidthPercentage(100f);
		
		Phrase blank =  new Phrase();
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
		
		PdfPCell blankTableCell = new PdfPCell();
		blankTableCell.addElement(blankTable);
		blankTableCell.setBorder(0);
		
	
		
		PdfPTable feedbackTable = new PdfPTable(1);
		feedbackTable.setWidthPercentage(100f);
	
//		float columnWidths2[] = {2.5f,7.5f}; 
//		try {
//			feedbackTable.setWidths(columnWidths2);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		
		Phrase customerFeedBack = new Phrase("Customer Feedback",font8bold);
		PdfPCell customerFeedBackCell = new PdfPCell(customerFeedBack);
		customerFeedBackCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		customerFeedBackCell.setBorder(0);
		feedbackTable.addCell(customerFeedBackCell);
		
//		Phrase FeedBack = new Phrase(" ",font8bold);
//		PdfPCell FeedBackCell = new PdfPCell(FeedBack);
//		FeedBackCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		FeedBackCell.setBorder(0);
//		feedbackTable.addCell(FeedBackCell);
		
		Phrase blanks = new Phrase(" ",font8bold);
		PdfPCell blanksCell = new PdfPCell(blanks);
		blanksCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blanksCell.setBorder(0);
//		feedbackTable.addCell(blanksCell);
//		feedbackTable.addCell(blanksCell);
		
		
		// Service Quality Rating (Please circle your choice):
		float columnWidths3[] = {50,50}; 
		PdfPTable qualityRatingTable = new PdfPTable(2); 
		qualityRatingTable.setWidthPercentage(100f);
		
		try {
			qualityRatingTable.setWidths(columnWidths3);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		Phrase serviceQualityRating =new Phrase("Service Quality Rating", font8bold);
		PdfPCell serviceQualityRatingCell = new PdfPCell(serviceQualityRating);
		serviceQualityRatingCell.setBorder(0);
		qualityRatingTable.addCell(serviceQualityRatingCell);
		
		
		
		Phrase PleaseCircleYourchoice =new Phrase("(Please circle your choice):", font8);
		PdfPCell PleaseCircleYourchoiceCell = new PdfPCell(PleaseCircleYourchoice);
		PleaseCircleYourchoiceCell.setBorder(0);
		qualityRatingTable.addCell(PleaseCircleYourchoiceCell);
		
		
		PdfPCell qualityRatingTableCell = new PdfPCell(qualityRatingTable);
		qualityRatingTableCell.setBorder(0);
		
		//   rating 
		PdfPTable ratingTabele = new  PdfPTable(1);
		ratingTabele.setWidthPercentage(100f);
		
		/** 30-10-2017 sagar sore [spaces removed to show on one line for partitioned displaying]**/
	//	Phrase ratingPhrase =new Phrase("Excellent        Very Good          Good          Average         Poor" ,font8);
		Phrase ratingPhrase =new Phrase("Excellent     Very Good       Good        Average       Poor" ,font8);
		PdfPCell ratingCell = new PdfPCell(ratingPhrase);
		ratingCell.setBorder(0);
		ratingTabele.addCell(ratingCell);
		
		PdfPCell ratingTabeleCell = new PdfPCell(ratingTabele);
		ratingTabeleCell.setBorder(0);
		
		
		//   commenst 
		PdfPTable commentsTable = new  PdfPTable(2);
		commentsTable.setWidthPercentage(100f);
		
		float mycolumnWidths[] = {27,73}; 
		
		try {
			commentsTable.setWidths(mycolumnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase comentsValue = new Phrase("Comments :",font8);
		PdfPCell comentsValueCell = new PdfPCell(comentsValue);
		comentsValueCell.setBorder(0);
		commentsTable.addCell(comentsValueCell);
		
		Phrase comentsValueBlank = new Phrase(" ",font8);
		PdfPCell comentsValueBlankCell = new PdfPCell(comentsValueBlank);
		comentsValueBlankCell.setBorderWidthRight(0);
		comentsValueBlankCell.setBorderWidthTop(0);
		comentsValueBlankCell.setBorderWidthLeft(0);
		
		commentsTable.addCell(comentsValueBlankCell);
		
		PdfPCell commentsTableCell = new PdfPCell(commentsTable);
		commentsTableCell.setBorder(0);
		
		
				
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.addCell(qualityRatingTableCell);
		parentTable.addCell(ratingTabeleCell);
		parentTable.addCell(blanksCell);
		parentTable.addCell(commentsTableCell);
		parentTable.addCell(comentsValueBlankCell);
		parentTable.addCell(comentsValueBlankCell);
//		parentTable.addCell(comentsValueBlankCell);
		
		PdfPCell parentTableCell = new PdfPCell(parentTable);
//		parentTableCell.setBorderWidthRight(0);
//		parentTableCell.setBorderWidthTop(0);
//		parentTableCell.setBorderWidthBottom(0);
		parentTableCell.setBorder(0);
		
		Phrase customerFeedBackfromService=null;
		PdfPCell customerFeedBackfromServiceCell = null;
		if(service.getCustomerFeedback()!=null && !service.getCustomerFeedback().equals(""))
		{
			customerFeedBackfromService = new Phrase(service.getCustomerFeedback()+" "+service.getRemark(),font8);
			customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
			customerFeedBackfromServiceCell.setBorder(0);
			feedbackTable.addCell(customerFeedBackfromServiceCell);
			feedbackTable.addCell(parentTableCell);
			feedbackTable.addCell(parentTableCell);
		}
		else
		{
//				customerFeedBackfromService = new Phrase(" ",font8);
//				customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
//				customerFeedBackfromServiceCell.setBorderWidthRight(0);
//				customerFeedBackfromServiceCell.setBorderWidthLeft(0);
//				customerFeedBackfromServiceCell.setBorderWidthTop(0);
//				
//				
//				PdfPTable blankTableTable = new PdfPTable(1);
//				blankTableTable.setWidthPercentage(100f);
//				for (int i = 0; i < 5; i++) 
//				{
//					blankTableTable.addCell(customerFeedBackfromServiceCell);
//				}
//				PdfPCell blankTableTableCell = new PdfPCell(blankTableTable);
//				blankTableTableCell.setBorderWidthBottom(0);
//				blankTableTableCell.setBorderWidthLeft(0);
//				blankTableTableCell.setBorderWidthRight(0);
//				
//				feedbackTable.addCell(blankTableTableCell);
				feedbackTable.addCell(parentTableCell);
		}
		
		PdfPCell feedbackTableCell = new PdfPCell();
		feedbackTableCell.addElement(feedbackTable);
		feedbackTableCell.setBorder(0);
		
		table.addCell(obsTableCell);
		table.addCell(blankTableCell);
		table.addCell(feedbackTableCell);
		
		PdfPCell tblCell =new PdfPCell();
		tblCell.setBorder(0);
		tblCell.addElement(table);
		
		Phrase blnk = new Phrase(" ",font8);
		PdfPCell blnkCell  = new PdfPCell();
		blnkCell.setBorder(0);
		blnkCell.addElement(blnk);
		
		a5tbl1.addCell(blnkCell);
		a5tbl1.addCell(tblCell);
		a5tbl2.addCell(blnkCell);
		a5tbl2.addCell(tblCell);
		
		
//		try {
//			document.add(Chunk.NEWLINE);
//			document.add(table);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
	}
	
	public void addTblDoc ()
	{   
		PdfPCell a5tbl1Cell  = new PdfPCell();
		a5tbl1Cell.setBorder(0);
		a5tbl1Cell.addElement(a5tbl1);
		
		PdfPCell a5tbl2Cell  = new PdfPCell();
		a5tbl2Cell.setBorder(0);
		a5tbl2Cell.addElement(a5tbl2);
		
		
		a5parentTbl.addCell(a5tbl1Cell);
		a5parentTbl.addCell(a5tbl2Cell);
		
		try {
			document.add(a5parentTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
	
	
	
}
