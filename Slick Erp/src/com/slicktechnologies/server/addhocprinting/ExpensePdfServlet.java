package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.http.HttpServlet;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;

import java.io.IOException;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
public class ExpensePdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7809000457668177608L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		
			
//			    String stringid1 = req.getParameter("ContractId");
//				stringid1 = stringid1.trim();
//				int count = Integer.parseInt(stringid1);
//				
//				String stringid = req.getParameter("CompanyId");
//			    stringid = stringid.trim();
//				Long companyId = Long.parseLong(stringid);
	
			//***************by jayshree ***************
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			String type= req.getParameter("type");
			
			String stringComid=req.getParameter("companyId");
			stringComid = stringComid.trim();
			Long companyid = Long.parseLong(stringComid);
			
			
			
//			if(type.equalsIgnoreCase("Multiple"))
//			{ 
			 
			

//			ProcessName processName = ofy().load().type(ProcessName.class).filter("processName","CronJob").filter("status",true).filter("companyId", compEntity.get(i).getCompanyId()).first().now();
//			ProcessConfiguration processconfig = ofy().load().type(ProcessConfiguration.class).filter("companyId", compEntity.get(i).getCompanyId()).filter("processName","CronJob").first().now();				
//			
//			boolean processconfigflag = false;
//			if(processconfig!=null){
//				if(processconfig.isConfigStatus()){
//					for(int l=0;l<processconfig.getProcessList().size();l++){
//						if(processconfig.getProcessList().get(l).getProcessName().equalsIgnoreCase("CronJob") && processconfig.getProcessList().get(l).getProcessType().equalsIgnoreCase("ServiceDailyMail") && processconfig.getProcessList().get(l).isStatus()==true){
//							processconfigflag=true;
//						}
//					}
//			   }
//			}
//			logger.log(Level.SEVERE,"process config  SMS Flag =="+processconfigflag);
			
			//********************by jayshree*******************************
			
			//ProcessName processName=ofy().load().type(ProcessName.class).filter("processName","expence").filter("processType","expencePdf").filter("status",true).filter("companyId",companyid).first().now();
			ProcessConfiguration processconfig=ofy().load().type(ProcessConfiguration.class).filter("companyId",companyid).filter("processName","Expense").first().now();
			
			boolean processconfigflag=false;
			if( processconfig!=null){
				if( processconfig.isConfigStatus()){
					for(int i=0;i< processconfig.getProcessList().size();i++){
						
						if( processconfig.getProcessList().get(i).getProcessName().equalsIgnoreCase("expense")&& processconfig.getProcessList().get(i).getProcessType().equalsIgnoreCase("expensePdf")&& processconfig.getProcessList().get(i).isStatus()==true){
							processconfigflag=true;
						}
						
					}
				}
			}
			System.out.print("trueflag"+processconfigflag);
			if(processconfigflag){
				
				
				try {
					ExpensePdfVoucher exppdf = new ExpensePdfVoucher();
					
//					exppdf.document = new Document(PageSize.A4.rotate());
					exppdf.document = new Document();
					Document document = exppdf.document;
					PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream());
					
					 document.open();
					
					 exppdf.setpdfexpense(count);
					 exppdf.createPdf();
					 document.close();
					} catch (DocumentException e) {
						e.printStackTrace();
					}
				
			}else{
				
				
				try {
					
					
					
					
					MultipleExpenseDetailspdf mulexppdf = new MultipleExpenseDetailspdf();
					
					mulexppdf.document = new Document();
					Document mydoc = mulexppdf.document;
					PdfWriter writer=PdfWriter.getInstance(mydoc, resp.getOutputStream());
					
					/**
					 * @author Anil
					 * @since 05-01-21
					 * for printing cancelled and draft watermark
					 * raised by Vaishnavi
					 */
					MultipleExpenseMngt mulExp = ofy().load().type(MultipleExpenseMngt.class).id(count).now();
					
					if (mulExp!=null&&mulExp.getStatus().equals(Invoice.CANCELLED)) {
						writer.setPageEvent(new PdfCancelWatermark());
					} else if (mulExp!=null&&!mulExp.getStatus().equals("Approved")) {
						writer.setPageEvent(new PdfWatermark());
					}
					
					mydoc.open();
					mulexppdf.setpdfMultipleexpense(count);
					mulexppdf.createPdf();
					mydoc.close();
					
				} catch (DocumentException e) {
					e.printStackTrace();
				}

				
			}
			
				//			}
//			else
//			{
//				
//				try {
//					ExpensePdf exppdf = new ExpensePdf();
//					
//					exppdf.document = new Document();
//					Document document = exppdf.document;
//					PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream());
//					
//					 document.open();
//					
//					 exppdf.setpdfexpense(count);
//					 exppdf.createPdf();
//					 document.close();
//					} catch (DocumentException e) {
//						e.printStackTrace();
//					}
//			}
			
			 
		
	}
}
