package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

public class PestoIndiaRenewalLetterPdf{

	Logger logger = Logger.getLogger("Name of logger");

	public Document document;
	ProcessConfiguration processConfig;
	boolean forContractRenewalLetterFlag = false;
	boolean isCompany = false;
	
	Company comp; 
	Customer cust;
	ContractRenewal conRenw;
	Contract con;
	Phrase chunk;
	double totalAmt,sum,taxAmt;
	
	PdfPCell pdfsr,pdftreat,pdfserv,pdfpre,pdfprice,pdfttl,pdftaxAmt,pdfstax;
//	pdfgrttl,pdfstax
	
	float[] colWidth = {50f,50f};
	float[] colWidth1 = {0.1f,0.9f,0.2f,0.2f,0.2f};
	float[] colWidth2 = {0.7f,0.1f};

	float round = 0.00f;
	DecimalFormat df = new DecimalFormat("0.0");
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	
	private Font font16boldul, font12bold, font8bold, font8, font16bold,
	font12boldul, font12, font14bold, font10, font10bold, font10boldul,
	font9, font9bold, font14boldul;

	
	public PestoIndiaRenewalLetterPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);

	}

	
	public void setpdfrenewal(ContractRenewal conRenewal) {
		
		conRenw = conRenewal;
		
		if (conRenw.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", conRenw.getCompanyId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", conRenw.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getCustomerId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", conRenw.getContractId()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getContractId()).first().now();
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
//		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		//************process config code*************************
		
		if (conRenw.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", conRenw.getCompanyId())
					.filter("processName", "ContractRenewal")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ContractRenewalLetterForPestoIndia")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						forContractRenewalLetterFlag = true;
					}
				}
			}
		}
		
	}

	
	public void createPdf() {
		
//		createCompanyHeading();
		createHeading();
		createCustInfo();
		createContractRenewalTable();
		createCalculationTable();
		createInwordsTable();
		createHardCodedInfo();
		
		String amountInWord = ServiceInvoicePdf.convert(sum);
		
	}
	
	
/*************************************************/	
//	public void createCompanyHeading() {
//		
//	Phrase compheading = null;
//	if(comp.getBusinessUnitName() !=null){
//		compheading = new Phrase(""+comp.getBusinessUnitName(),font16bold);
//	}
//	PdfPCell headingcell = new PdfPCell(compheading);
//	headingcell.setBorder(0);
//	headingcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	
//	String str = "";
//	str = "Pest Control and HouseKeeping Services";
//		
//	Phrase strp = new Phrase(str,font10bold);	
//	PdfPCell strpcell = new PdfPCell(strp);	
//	strpcell.setBorder(0);
//	strpcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		
//	PdfPTable headingtable = new PdfPTable(1);	
//	headingtable.addCell(headingcell);
//	headingtable.addCell(strpcell);
//	headingtable.setWidthPercentage(100f);
//		
//	PdfPCell cell = new PdfPCell(headingtable);	
//	cell.setBorder(0);
//		
//	PdfPTable table = new PdfPTable(1);
//	table.addCell(cell);
//	table.setWidthPercentage(100);
//
//	try {
//		document.add(table);
//		System.out.println("INSIDE document "+document.add(table));
//	} catch (DocumentException e) {
//		e.printStackTrace();
//	}	
		
//}	
/**********************************************************/
	
public void createHeading(){
	
	Paragraph blank = new Paragraph();
	blank.add(Chunk.NEWLINE);
	blank.add(Chunk.NEWLINE);
	blank.add(Chunk.NEWLINE);
	PdfPCell blankcell = new PdfPCell(blank);
	blankcell.setBorder(0);
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl); 
	blcell.setBorder(0);
	
	String str = "";
	str = "Renewal Letter";	
	
	Phrase strp = new Phrase(str,font12bold);	
	PdfPCell strpcell = new PdfPCell(strp);	
	strpcell.setBorder(0);
	strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	//*********Left table**************
	PdfPTable lefttable = new PdfPTable(1);
	lefttable.setWidthPercentage(100f);
		
	Phrase cardno = null;
	if(cust.getRefrNumber1() !=null){
		cardno = new Phrase("Card No. : "+cust.getRefrNumber1(),font10);
	}
	PdfPCell cardcell = new PdfPCell(cardno);	
	cardcell.setBorder(0);
	cardcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
	Phrase tel = null;
	if(cust.getLandline() !=null && cust.getCellNumber1() !=null){
		tel = new Phrase("Tel No. : "+cust.getLandline(),font10);
	}	
	PdfPCell telcell = new PdfPCell(tel);	
	telcell.setBorder(0);
	telcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
	
	Phrase comp = null;
	if(cust.isCompany()==true){
		comp = new Phrase("Kind Attn : "+cust.getFullname(),font10bold);	
	}
	else{
		comp = new Phrase("Kind Attn :  ",font12bold);
	}
	PdfPCell compcell = new PdfPCell(comp);	
	compcell.setBorder(0);
	compcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	lefttable.addCell(blankcell);
	lefttable.addCell(strpcell);
	lefttable.addCell(cardcell);
	lefttable.addCell(telcell);
	lefttable.addCell(compcell);
	
	PdfPCell leftcell = new PdfPCell(lefttable);
	leftcell.setBorder(0);
	
		
	//*********Right table**************
		PdfPTable righttable = new PdfPTable(1);
		righttable.setWidthPercentage(100f);	
		
		Date todaysdate = new Date();
//		SimpleDateFormat sdf=new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Phrase dt = null;
		if(todaysdate !=null){
			dt = new Phrase("Date : "+fmt.format(todaysdate),font10);
		}
		System.out.println("Date::::::::::::::"+fmt.format(todaysdate));
		
//		Phrase dt = new Phrase("Date : ",font10);
		PdfPCell dtcell = new PdfPCell(dt);	
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(dtcell);
		
		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);
		
		try {
			table.setWidths(colWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table.setWidthPercentage(100f);	
		
		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
	}	
}


public void createCustInfo(){
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl); 
	blcell.setBorder(0);
	
	String str = "";
	str = "To";	
	
	Phrase company = null;
//	if(cust.getCompanyName() !=null){
	if(cust.isCompany()==true){
//		company = new Phrase(" "+cust.getCompanyName(),font10bold);	
		company = new Phrase(" "+cust.getCompanyName(),font10bold);	
	}
	else{
		company = new Phrase(""+str+","+" \n"+cust.getFullname(),font10bold);
	}
	PdfPCell companycell = new PdfPCell(company);	
	companycell.setBorder(0);
	companycell.setHorizontalAlignment(Element.ALIGN_LEFT);	
	
	
	//********Customer Address**************
	String custAdd1 = "";
	String custFullAdd1 = "";

//	if (cust.getAdress() != null) {
//
//		if (cust.getAdress().getAddrLine2() != null) {
//			if (cust.getAdress().getLandmark() != null) {
//				custAdd1 = cust.getAdress().getAddrLine1() + ","
//						+ cust.getAdress().getAddrLine2() + ","
//						+ cust.getAdress().getLandmark();
//			} else {
//				custAdd1 = cust.getAdress().getAddrLine1() + ","
//						+ cust.getAdress().getAddrLine2();
//			}
//		} else {
//			if (cust.getAdress().getLandmark() != null) {
//				custAdd1 = cust.getAdress().getAddrLine1() + ","
//						+ cust.getAdress().getLandmark();
//			} else {
//				custAdd1 = cust.getAdress().getAddrLine1();
//			}
//		}
//
//		if (cust.getAdress().getLocality() != null) {
//			custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
//					+ "," + cust.getAdress().getState() + ","
//					+ cust.getAdress().getCity() + "\n"
//					+ cust.getAdress().getLocality() + " "+ "\n" +"Pin : "
//					+ cust.getAdress().getPin();
//
//		} else {
//			custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
//					+ "," + cust.getAdress().getState() + ","
//					+ cust.getAdress().getCity() + " "
//					+ cust.getAdress().getPin();
//		}
//	}
/****************************************************************/

	if (cust.getAdress() != null) {
		logger.log(Level.SEVERE,"111111111111111111");
		if (!cust.getAdress().getAddrLine2().equals("")) {
			logger.log(Level.SEVERE,"22222222222222222");
			logger.log(Level.SEVERE,"hiii"+cust.getAdress().getAddrLine2());
			if (!cust.getAdress().getLandmark().equals("")) {
				logger.log(Level.SEVERE,"333333333333333");
				custAdd1 = cust.getAdress().getAddrLine1() 
						+ "," + "\n" 
						+ cust.getAdress().getAddrLine2() 
						+ "," + "\n"
						+ cust.getAdress().getLandmark();
			} else {
				logger.log(Level.SEVERE,"4444444444444");
				custAdd1 = cust.getAdress().getAddrLine1()
						+ "," + "\n"
						+ cust.getAdress().getAddrLine2();
			}
		} else {
			logger.log(Level.SEVERE,"5555555555555");
			if (!cust.getAdress().getLandmark().equals("")) {
				logger.log(Level.SEVERE,"66666666666666666");
				custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
						+ cust.getAdress().getLandmark();
			} else {
				logger.log(Level.SEVERE,"77777777777777777");
				custAdd1 = cust.getAdress().getAddrLine1();
			}
		}

		if (cust.getAdress().getLocality() != null) {
			custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
					+ "," + cust.getAdress().getState() + ","
					+ cust.getAdress().getCity() + "\n"
					+ cust.getAdress().getLocality() + " "+ "\n" +"Pin : "
					+ cust.getAdress().getPin();

		} else {
			custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
					+ "," + cust.getAdress().getState() + ","
					+ cust.getAdress().getCity() + " "
					+ cust.getAdress().getPin();
		}
	}
	
	
	Phrase custAddInfo = new Phrase(custFullAdd1, font9bold);
	PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
	custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	custAddInfoCell.setBorder(0);
	
	
	//***************Msg part***************
	String str1 = "";
	str1 = " Dear Sir / Madam"+",";	
	
	Phrase str1p = new Phrase(str1,font10bold);	
	PdfPCell str1pcell = new PdfPCell(str1p);	
	str1pcell.setBorder(0);
	str1pcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String str2 = "";
	str2 = "Sub : Renewal of contract No. ";
	
//	&& comp.getBusinessUnitName() !=null
	Phrase dt = null;
	if(con.getCount() !=0l && con.getStartDate() !=null){
		dt = new Phrase(" "+str2+con.getCount()+"         Dated : "+fmt.format(con.getStartDate())+"    by "+comp.getBusinessUnitName(),font10bold);	
	}
	PdfPCell dtcell = new PdfPCell(dt);	
	dtcell.setBorder(0);
	dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable custtable = new PdfPTable(1);
	
	custtable.addCell(companycell);
//	custtable.addCell(blcell);
	custtable.addCell(custAddInfoCell);
	custtable.addCell(blcell);
	custtable.addCell(str1pcell);
	custtable.addCell(dtcell);
	custtable.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(custtable);
	cell.setBorder(0);
	
	PdfPTable table = new PdfPTable(1);
	table.addCell(cell);
	table.addCell(blcell);
	table.setWidthPercentage(100);
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
}
	

public void createHardCodedInfo(){
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl); 
	blcell.setBorder(0);
	
	String str = "";
	str = "We invite your attention to our above contract with you for regular pest control which is due to expire on"+"\n";	
	
	Phrase dt = null;
	if(con.getEndDate() !=null){
		dt = new Phrase(str+" "+fmt.format(con.getEndDate())+".",font10);	
	}
	PdfPCell dtcell = new PdfPCell(dt);	
	dtcell.setBorder(0);
	dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String str1 = "";
	str1 = "As you are undoubtedly aware of the manifold advantages of continuing our above services at your premises we are sure you would be interested to renew the contract"+" for a further period."+"\n"+"\n"+
           "Kindly send us your confirmation at earliest to unable us to maintain our high standard services."+"\n"+"\n"+"Thanking you and assuring you of our best services at all times."+"\n"+"\n"+"\n"+"\n"+"Yours Faithfully"+",";
	
	Phrase str1p = new Phrase(str1,font10);	
	PdfPCell str1pcell = new PdfPCell(str1p);	
	str1pcell.setBorder(0);
	str1pcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase company = null;
	if(comp.getBusinessUnitName() !=null){
		company = new Phrase("for "+comp.getBusinessUnitName(),font12bold);	
	}
	
	PdfPCell companycell = new PdfPCell(company);	
	companycell.setBorder(0);
	companycell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String str2 = "";
	str2 = "(AUTHORISED SIGNATORY)";
	
	Phrase str2p = new Phrase(str2,font10bold);	
	PdfPCell str2pcell = new PdfPCell(str2p);	
	str2pcell.setBorder(0);
	str2pcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable hctable = new PdfPTable(1);
		
	hctable.addCell(dtcell);
	hctable.addCell(str1pcell);
	hctable.addCell(companycell);
	hctable.addCell(blcell);
	hctable.addCell(blcell);
	hctable.addCell(blcell);
	hctable.addCell(str2pcell);
	hctable.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(hctable);
	cell.setBorder(0);
	
	PdfPTable table = new PdfPTable(1);
	table.addCell(cell);
	table.setWidthPercentage(100);
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
}
	
	
public void createContractRenewalTable(){
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl); 
	blcell.setBorder(0);
	
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	PdfPTable table = new PdfPTable(5);
	
	try {
		table.setWidths(colWidth1);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	table.setWidthPercentage(100f);	
	
	Phrase srno = new Phrase("Sr.No.",font1);
	Phrase treat = new Phrase("Treatment", font1);
	Phrase serv = new Phrase("No. of Services", font1);
	Phrase pre = new Phrase("Premises", font1);
	Phrase price = new Phrase("Price", font1);
	
	
	PdfPCell srnocell = new PdfPCell(srno);
	srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPCell treatcell = new PdfPCell(treat);
	treatcell.setHorizontalAlignment(Element.ALIGN_CENTER);

	PdfPCell servcell = new PdfPCell(serv);
	servcell.setHorizontalAlignment(Element.ALIGN_CENTER);

	PdfPCell precell = new PdfPCell(pre);
	precell.setHorizontalAlignment(Element.ALIGN_CENTER);

	PdfPCell pricecell = new PdfPCell(price);
	pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	
	table.addCell(srnocell);
	table.addCell(treatcell);
	table.addCell(servcell);
	table.addCell(precell);
	table.addCell(pricecell);
	
	
//	for(int i=0; i<con.getItems().size(); i++){
//		
//		chunk = new Phrase(i+1+"",font8);
//		pdfsr = new PdfPCell(chunk);
//		pdfsr.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//		chunk = new Phrase(con.getItems().get(i).getProductName()+"",font8);	
//		pdftreat = new PdfPCell(chunk);
//		pdftreat.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//		chunk = new Phrase(con.getItems().get(i).getNumberOfServices()+"",font8);
//		pdfserv = new PdfPCell(chunk);
//		pdfserv.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		if(con.getPremisesDesc() !=null){
//			chunk = new Phrase(con.getPremisesDesc()+"",font8);
//		}
//		pdfpre = new PdfPCell(chunk);
//		pdfpre.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		chunk = new Phrase(con.getItems().get(i).getPrice()+"",font8);
//		pdfprice = new PdfPCell(chunk);
//		pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		table.addCell(pdfsr);
//		table.addCell(pdftreat);
//		table.addCell(pdfserv);
//		table.addCell(pdfpre);
//		table.addCell(pdfprice);
//		
//	}
	
	for(int i=0;i<conRenw.getItems().size();i++){
		
		chunk = new Phrase(i+1+"",font8);
		pdfsr = new PdfPCell(chunk);
		pdfsr.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//******************
		if(conRenw.getItems().get(i).getProductName()!=null){
			chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font8);
		}else{                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       			chunk = new Phrase("");
		}
		pdftreat = new PdfPCell(chunk);
		pdftreat.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//******************
		if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
			 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font8);
		}
		else{
			 chunk= new Phrase("");
		}
		pdfserv = new PdfPCell(chunk);
		pdfserv.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//******************
		if(conRenw.getItems().get(i).getPremisesDetails()!=null){
			chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails()+"", font8);
		}else{
			chunk = new Phrase("");
		}
		pdfpre = new PdfPCell(chunk);
		pdfpre.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		//******************
		if(conRenw.getItems().get(i).getPrice()!=0){
			chunk = new Phrase(df.format(conRenw.getItems().get(i).getPrice())+"", font8);
		}else{
			 chunk = new Phrase("");
		}
		pdfprice = new PdfPCell(chunk);
		pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(pdfsr);
		table.addCell(pdftreat);
		table.addCell(pdfserv);
		table.addCell(pdfpre);
		table.addCell(pdfprice);
		
	}
	
	PdfPCell contcell = new PdfPCell(table);
	contcell.setBorder(0);

	PdfPTable parenttable = new PdfPTable(1);
	parenttable.addCell(contcell);
//	parenttable.addCell(blcell);
	parenttable.setWidthPercentage(100);

	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}


public void createCalculationTable(){
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl); 
	blcell.setBorder(0);
	
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	
	//*********left table***********
	PdfPTable lefttable = new PdfPTable(1);	
	lefttable.setWidthPercentage(100f);
	
	Phrase ttl = new Phrase("Total", font1);
	PdfPCell ttlcell = new PdfPCell(ttl);
	ttlcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	lefttable.addCell(ttlcell);
	
//	for (int i=0; i<con.getProductTaxes().size(); i++) {
//	System.out.println("Product taxes:::::::::"+con.getProductTaxes().get(i).getChargeName());
//	System.out.println("Product taxes in %:::::::::"+con.getProductTaxes().get(i).getChargePercent());
//		if(con.getProductTaxes().get(i).getChargeName().equals("Service Tax") && 
//		con.getProductTaxes().get(i).getChargePercent()==14.5	){
//	
//		String str = "Service Tax "+ " @ "+" 14.5%";
//	
//		Phrase strp = new Phrase(str,font1);
//		PdfPCell strcell =new PdfPCell(strp);
//		strcell.setHorizontalAlignment(Element.ALIGN_RIGHT);	
//		lefttable.addCell(strcell);
//			
//		}
//	}
	
	for (int i=0;i<conRenw.getItems().size();i++) {
	if(conRenw.getItems().get(i).getServiceTax()!=null){
		chunk = new Phrase("Service Tax "+" @ "+conRenw.getItems().get(i).getServiceTax()+"", font8);
	}
	pdfstax = new PdfPCell(chunk);
	pdfstax.setHorizontalAlignment(Element.ALIGN_RIGHT);

	}
	lefttable.addCell(pdfstax);
	
	Phrase grttl = new Phrase("Grand Total", font1);
	PdfPCell grttlcell = new PdfPCell(grttl);
	grttlcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	lefttable.addCell(grttlcell);
	
	PdfPCell leftcell = new PdfPCell(lefttable);	
	leftcell.setBorder(0);
	
	
	
	
	
	//*********right table***********
	PdfPTable righttable = new PdfPTable(1);	
	righttable.setWidthPercentage(100f);
	
	
	double totalAmt = 0;
	
	for(int i=0;i<conRenw.getItems().size();i++){	
		totalAmt = totalAmt+conRenw.getItems().get(i).getPrice();	
	}
	
	Phrase totalAmtp = new Phrase(df.format(totalAmt)+"",font8);
	pdfttl = new PdfPCell(totalAmtp);
	pdfttl.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	righttable.addCell(pdfttl);


//	for (int i=0; i<con.getProductTaxes().size(); i++) {
//		System.out.println("Product taxes:::::::::"+con.getProductTaxes().get(i).getChargeName());
//		System.out.println("Product taxes in %:::::::::"+con.getProductTaxes().get(i).getChargePercent());
//		if(con.getProductTaxes().get(i).getChargeName().equals("Service Tax") && 
//			con.getProductTaxes().get(i).getChargePercent()==14.5	){
//	
//	double taxAmt = 14.5* con.getTotalAmount() / 100;
//	
//	Phrase taxAmtp = new Phrase(df.format(taxAmt)+"",font8);
//	pdftaxAmt = new PdfPCell(taxAmtp);
//	pdftaxAmt.setHorizontalAlignment(Element.ALIGN_CENTER);	
//	
//	righttable.addCell(pdftaxAmt);
//	
//	}
//}
	
//	double taxAmt = 0;
	
	for (int i=0;i<conRenw.getItems().size();i++) {
		if(conRenw.getItems().get(i).getServiceTax()!=null){
			taxAmt = 14.5* totalAmt/100;
			
			Phrase staxAmtp = new Phrase(taxAmt+"",font8);
			pdftaxAmt = new PdfPCell(staxAmtp);
			pdftaxAmt.setHorizontalAlignment(Element.ALIGN_CENTER);	
			}
		}
			righttable.addCell(pdftaxAmt);
	
	double sum = 0;
	sum = sum+totalAmt+taxAmt;
	System.out.println("GrandTotal::::::::::::::::::::::;;"+sum);
	this.sum = sum;
	
	Phrase grandTotal = new Phrase(df.format(sum) +"",font8);
	PdfPCell grandTotalcell = new PdfPCell(grandTotal);
	grandTotalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	righttable.addCell(grandTotalcell);

	PdfPCell rightcell = new PdfPCell(righttable);
	rightcell.setBorder(0);	
	
	PdfPTable table = new PdfPTable(2);
	table.addCell(leftcell);
	table.addCell(rightcell);
	
	try {
		table.setWidths(colWidth2);
		} catch (DocumentException e1) {
		e1.printStackTrace();
		}
	table.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(table);
	cell.setBorder(0);
	
	PdfPTable parenttable = new PdfPTable(1);
	parenttable.addCell(cell);
//	parenttable.addCell(blcell);
	parenttable.setWidthPercentage(100);
	
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

public void createInwordsTable() {
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl); 
	blcell.setBorder(0);

	String amountInWord = ServiceInvoicePdf.convert(sum);
	Phrase rs = new Phrase("Rs.  " + amountInWord + " Only ", font10bold);
	PdfPCell rscell = new PdfPCell(rs);
	rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

	PdfPTable amttable = new PdfPTable(1);
	amttable.addCell(rscell);
	amttable.addCell(blcell);
	amttable.setWidthPercentage(100);

	try {
		document.add(amttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}
}
