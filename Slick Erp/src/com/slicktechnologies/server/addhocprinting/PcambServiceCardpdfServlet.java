package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

public class PcambServiceCardpdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 514219929132126986L;

	protected  void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException ,IOException {
			 //super.doGet(request, response);
		    response.setContentType("application/pdf");
		    
	  try{
		   String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			PcambServiceCardPdf pdf= new  PcambServiceCardPdf();
			pdf.document= new Document(PageSize.A5.rotate(),25,25,25,25);
			  Document document = pdf.document;
			  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
			 
			  document.open();
			  pdf.setContract(count);
			  pdf.createPdf();
			  document.close();			    
	  }catch (DocumentException e) {
		  e.printStackTrace();
	  }
	}
}
