package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Sales;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

public class SalesOrderPdf {
	Sales qp;
	List<SalesLineItem> products;
	
	Customer cust;
	Company comp;
	public  Document document;
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");

	public SalesOrderPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
	}

	public void setSalesOrder(long count)
	{
		
		//Load Contract 
				qp=ofy().load().type(SalesOrder.class).id(count).now();
				int contractCount=qp.getCount();
			   //Load Customer
			   cust=ofy().load().type(Customer.class).filter("count",qp.getCustomerId()).first().now();
			  //Load Company
			   //comp=ofy().load().type(Company.class).id(qp.getCompanyId()).now();
			   if(qp.getCompanyId()==null)
			        comp=ofy().load().type(Company.class).first().now();
			   else
				   comp=ofy().load().type(Company.class).
						   filter("companyId",qp.getCompanyId()).first().now();
			   products=qp.getItems();
	}
	
	public void createPdf() {

		 showLogo(document, comp);  
		 createCompanyHedding();
		 createCompanyAdress();

		 try {
			createHeaderTable();
			addFooter();
		 }
		 catch (DocumentException e1) {
				e1.printStackTrace();
			}
 	}
		 
		 public  void createCompanyHedding()
			{
				 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16boldul);
			     Paragraph p =new Paragraph();
			     p.add(Chunk.NEWLINE);
			     p.add(companyName);
			     p.setAlignment(Element.ALIGN_CENTER);
			     try {
					document.add(p);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			public  void createCompanyAdress()
			{
				
				Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font12);
				Phrase adressline2=null;
				if(comp.getAddress().getAddrLine2()!=null)
				{
					adressline2= new Phrase(comp.getAddress().getAddrLine2(),font12);
				}
				
				Phrase landmark=null;
				Phrase locality=null;
				if(comp.getAddress().getLandmark()!=null&&comp.getAddress().getLocality().equals("")==false)
				{
					
					String landmarks=comp.getAddress().getLandmark()+" , ";
					locality= new Phrase(landmarks+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
						      +comp.getAddress().getPin(),font12);
				}
				else
				{
					locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
						      +comp.getAddress().getPin(),font12);
				}
				Paragraph adressPragraph=new Paragraph();
				adressPragraph.add(adressline1);
				adressPragraph.add(Chunk.NEWLINE);
				if(adressline2!=null)
				{
						adressPragraph.add(adressline2);
						adressPragraph.add(Chunk.NEWLINE);
				}
				
				
				
				adressPragraph.add(locality);
				
				adressPragraph.setAlignment(Element.ALIGN_CENTER);
				adressPragraph.setSpacingBefore(10);
				adressPragraph.setSpacingAfter(10);
				
				
				// Phrase for phone,landline ,fax and email
				
				Phrase titlecell=new Phrase("Mob :",font8bold);
				Phrase titleTele = new Phrase("Tele :",font8bold);
				Phrase titleemail= new Phrase("Email :",font8bold);
				Phrase titlefax= new Phrase("Fax :",font8bold);
				
				Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
				Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
				String servicetax=comp.getServiceTaxNo()+"               ";
				String vatttax=comp.getVatTaxNo();
				Phrase servicetaxphrase=null;
				Phrase vattaxphrase=null;
				
				if(servicetax!=null&&servicetax.trim().equals("")==false)
				{
					servicetaxphrase=new Phrase(servicetax,font8);
				}
					

				if(vatttax!=null&&vatttax.trim().equals("")==false)
				{
					vattaxphrase=new Phrase(vatttax,font8);
				}
				
				// cell number logic
				String stringcell1=comp.getCellNumber1()+"";
				String stringcell2=null;
				Phrase mob=null;
				if(comp.getCellNumber2()!=-1)
					stringcell2=comp.getCellNumber2()+"";
				if(stringcell2!=null)
				    mob=new Phrase(stringcell1+" / "+stringcell2,font8);
				else
					 mob=new Phrase(stringcell1,font8);
				
				
				//LANDLINE LOGIC
				Phrase landline=null;
				if(comp.getLandline()!=-1)
					landline=new Phrase(comp.getLandline()+"",font8);
				
				// fax logic
				Phrase fax=null;
				if(comp.getFaxNumber()!=null)
					fax=new Phrase(comp.getFaxNumber()+"",font8);
				// email logic
				Phrase email= new Phrase(comp.getEmail(),font8);
				
				adressPragraph.add(Chunk.NEWLINE);
				adressPragraph.add(titlecell);
				adressPragraph.add(mob);
				adressPragraph.add(new Chunk("            "));
				
				if(landline!=null)
				{
					adressPragraph.add(titleTele);
					adressPragraph.add(landline);
					adressPragraph.add(new Chunk("            "));
				}
				
				if(fax!=null)
				{
					adressPragraph.add(titlefax);
					adressPragraph.add(fax);
					adressPragraph.add(Chunk.NEWLINE);
				}
				
				adressPragraph.add(titleemail);
				adressPragraph.add(new Chunk("            "));
				adressPragraph.add(email);
				adressPragraph.add(Chunk.NEWLINE);
				
				if(servicetaxphrase!=null)
				{
					adressPragraph.add(titleservicetax);
					adressPragraph.add(servicetaxphrase);
				}
				
				if(vattaxphrase!=null)
				{
					adressPragraph.add(titlevatatx);
					adressPragraph.add(vattaxphrase);
					adressPragraph.add(new Chunk("            "));
					
				}
				
				adressPragraph.add(Chunk.NEWLINE);
				String title="";
				if(qp instanceof SalesOrder)
					title="Sales Order";
				else
					title="Quotation";
				
				Phrase Ptitle= new Phrase(title,font12bold);
				adressPragraph.add(Chunk.NEWLINE);
				adressPragraph.add(Chunk.NEWLINE);
				adressPragraph.add(Ptitle);
				adressPragraph.add(Chunk.NEWLINE);
				adressPragraph.add(Chunk.NEWLINE);
				
				try {
					document.add(adressPragraph);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		  }
			
			public  void createHeaderTable() throws DocumentException
			{
				 Phrase nameTitle = new Phrase("Name :",font8bold);
				 Phrase phoneTitle= new Phrase("Phone :",font8bold);
				 Phrase adressTitle= new Phrase("Adress :",font8bold);
				 Phrase emailTitle= new Phrase("Email :",font8bold);
				 
				 PdfPCell titlecustnameCell=new PdfPCell();
				 titlecustnameCell.addElement(nameTitle);
				 PdfPCell titlecustPhonecell = new PdfPCell();
				 titlecustPhonecell.addElement(phoneTitle);
				 PdfPCell titleadressCell = new PdfPCell();
				 titleadressCell.addElement(adressTitle);
				 PdfPCell titleemailCell = new PdfPCell(emailTitle);
				 titleemailCell.addElement(emailTitle);
				
				 String fullname=cust.getFullname() ;
				
				Phrase customername = new Phrase(fullname,font8);
				Phrase phonechunk = new Phrase(cust.getCellNumber1()+"",font8);
				Phrase emailchunk = new Phrase(cust.getEmail()+"",font8);
				
				Phrase blankphrase = new Phrase("  ");
				PdfPCell blankcell = new PdfPCell(blankphrase);
				
				Phrase customeradress= new Phrase(cust.getAdress().getAddrLine1(),font8);
				Phrase customeradress2=null;
				//Patch
				if(cust.getAdress().getAddrLine2()!=null)
				   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font8);
				Phrase landmark=null;
				Phrase locality= null;
				
				
				if(cust.getAdress().getLandmark()!=null)
				{
				    landmark = new Phrase(cust.getAdress().getLandmark());
				    locality=new Phrase(landmark+" "+cust.getAdress().getLocality(),font8);
				}
				else
					locality=new Phrase(cust.getAdress().getLocality(),font8);
				
				Phrase cityState=new Phrase(cust.getAdress().getCity()
						+" "+cust.getAdress().getState(),font8);
				Phrase country=new Phrase("  "+cust.getAdress().getCountry(),font8);
				Phrase pin=new Phrase("  "+cust.getAdress().getPin()+"",font8);
				 
				
				Paragraph adresspara = new Paragraph();
				 adresspara.add(customeradress);
				 adresspara.add(Chunk.NEWLINE);
				 if(customeradress2!=null)
					 adresspara.add(customeradress2);
				 adresspara.add(locality);
				
				adresspara.add(cityState);
				
				adresspara.add(country);
				adresspara.add(pin);
				 
				 PdfPCell custnameCell=new PdfPCell();
				 custnameCell.addElement(customername);
				 PdfPCell custPhonecell = new PdfPCell();
				 custPhonecell.addElement(phonechunk);
				 PdfPCell adressCell = new PdfPCell();
				 adressCell.addElement(adresspara);
				 PdfPCell emailCell = new PdfPCell(emailTitle);
				 emailCell.addElement(emailchunk);
				
				String header;
				header = "ID     :";
				
				Phrase billno = new Phrase(header,font8bold);
				Phrase orederdate= new Phrase("Date :",font8bold);
				Phrase startDate = new Phrase("Start Date :",font8bold);
				Phrase endDate = new Phrase("End Date :",font8bold);
				
				 PdfPCell titlebillCell=new PdfPCell();
				 titlebillCell.addElement(billno);
				 PdfPCell titledatecell = new PdfPCell();
				 titledatecell.addElement(orederdate);
				 PdfPCell startdateCell = new PdfPCell();
				 startdateCell.addElement(startDate);
				 PdfPCell enddateCell = new PdfPCell();
				 enddateCell.addElement(endDate);
				 
				Phrase realqno=null;
			    realqno = new Phrase(qp.getCount()+"",font8);
				
				
				String date=fmt.format(qp.getCreationDate());
				Phrase realorederdate= new Phrase(date+"",font8);
				realorederdate.trimToSize();

				SalesQuotation quot=(SalesQuotation) qp;
				String start;
				if(quot.getStartDate()==null)
					start="N.A";
				else
					start=fmt.format(quot.getStartDate());
				
				Phrase realstartdate = new Phrase(start,font8);
				
				if(quot.getEndDate()==null)
					start="N.A";
				else
					start=fmt.format(quot.getEndDate());
				
				Phrase realenddate = new Phrase(start,font8);
				
			    PdfPCell billCell=new PdfPCell();
				billCell.addElement(realqno);
				PdfPCell datecell = new PdfPCell();
				datecell.addElement(realorederdate);
				PdfPCell realstartdateCell = new PdfPCell();
				realstartdateCell.addElement(realstartdate);
				PdfPCell realenddateCell = new PdfPCell();
				realenddateCell.addElement(realenddate);
			     
				 PdfPTable table = new PdfPTable(4);
				 
				 // 1 st row name phone
				 table.addCell(titlecustnameCell);
				 table.addCell(custnameCell);
				 
				 table.addCell(titlecustPhonecell);
				 table.addCell(custPhonecell);
				 
				 // 2nd row adress and email
				 table.addCell(titleadressCell);
				 table.addCell(adressCell);
				 
				 table.addCell(titleemailCell);
				 table.addCell(emailCell);
				 
				table.setWidths(new float[]{18,32,18,32});
				table.setHorizontalAlignment(Element.ALIGN_LEFT);
				table.setWidthPercentage(100);
				 
				PdfPTable billtable = new PdfPTable(4);
				billtable.setWidths(new float[]{20,30,20,30});
				billtable.setWidthPercentage(100);
				billtable.setHorizontalAlignment(Element.ALIGN_LEFT);

				//1  row bill number and date
				 billtable.addCell(titlebillCell);
				 billtable.addCell(billCell);
				 billtable.addCell(titledatecell);
				 billtable.addCell(realorederdate);
				 
				//2  row duration   and start date
				 billtable.addCell(startdateCell);
				 billtable.addCell(realstartdate);
				 
				 billtable.addCell(enddateCell);
				 billtable.addCell(realenddate);
				 
				 int noCol=table.getNumberOfColumns();
				 for(int i=0;i<2;i++)
				 {
					 PdfPRow temp=table.getRow(i);
					PdfPCell[] cells= temp.getCells();
					for(PdfPCell cell:cells)
						cell.setBorder(Rectangle.NO_BORDER);
				}
				 
				 for(int i=0;i<2;i++)
				 {
					 PdfPRow temp=billtable.getRow(i);
					PdfPCell[] cells= temp.getCells();
					for(PdfPCell cell:cells)
						cell.setBorder(Rectangle.NO_BORDER);
				}
				 
				PdfPTable parenttable= new PdfPTable(2);
				parenttable.setWidthPercentage(100);
				parenttable.setWidths(new float[]{50,50});
				
				PdfPCell custinfocell = new PdfPCell();
				PdfPCell  billinfocell = new PdfPCell();
				
				custinfocell.addElement(table);
				billinfocell.addElement(billtable);
				parenttable.addCell(custinfocell);
				parenttable.addCell(billinfocell);
			
				document.add(parenttable);
			}
			
			public void addtaxparagraph()
			{
				Paragraph taxpara= new Paragraph();
				Phrase titleservicetax=new Phrase("Service Tax No     :",font8bold);
				Phrase titlevatatx=new Phrase("Vat Tax No :    ",font8bold);
				Phrase titlelbttax=new Phrase("LBT Tax No :    ",font8bold);
				Phrase space = new Phrase("									");
				String serv = null,lbt,vat = null;
				if(comp.getServiceTaxNo()==null)
					serv="";
				else
					serv=comp.getServiceTaxNo();
				if(comp.getVatTaxNo()==null)
					vat="";
				else
					vat=comp.getVatTaxNo();
				
				if(serv.equals("")==false)
				{
					taxpara.add(titleservicetax);
					Phrase servp= new Phrase(serv,font8);
					taxpara.add(servp);
				}
				taxpara.add(space);
				
				if(vat.equals("")==false)
				{
					taxpara.add("                ");
					taxpara.add(titlevatatx);
					Phrase vatpp= new Phrase(vat,font8);
					taxpara.add(vatpp);
				}  	 
				
				
				
				
				try {
					taxpara.setSpacingAfter(10);
					document.add(taxpara);
					
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			public void addFooter()
			{
				 
				 Phrase recieverSinature = new Phrase("Reciever Signature",font12boldul);
				 Phrase blankphrase = new Phrase(" ");
				 Phrase blankphrase1 = new Phrase(" ");
				 Phrase blankphrase2 = new Phrase(" ");
				 Phrase companyname= new Phrase("For "+comp.getBusinessUnitName(),font12bold);
				 Phrase authorizedsignatory=null;
			     authorizedsignatory = new Phrase("AUTHORISED SIGNATORY"+"",font12boldul);
				 PdfPTable table = new PdfPTable(2);
				 try {
					table.setWidths(new float[]{50,50});
					table.setSpacingBefore(60);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				 table.addCell(recieverSinature);
				 table.addCell(companyname);
				 table.addCell(blankphrase);
				 table.addCell(blankphrase1);
				 table.addCell(blankphrase2);
				 table.addCell(authorizedsignatory);
				 
				 table.setWidthPercentage(100);
				 
				 for(int i=0;i<3;i++)
				 {
					 PdfPRow temp=table.getRow(i);
					 
					PdfPCell[] cells= temp.getCells();
					for(PdfPCell cell:cells)
						cell.setBorder(Rectangle.NO_BORDER);
				}
				 
				
			
				try {
					document.add(table);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			
			public static void showLogo(Document doc,Company comp)
			{
				DocumentUpload document =comp.getLogo();
				
				//patch
				String hostUrl; 
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
				}
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scaleAbsolute(60f,60f);
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
			     }
		     }
      }
