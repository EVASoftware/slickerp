package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.MaterialConsumptionReport;
import com.slicktechnologies.shared.common.inventory.MaterialIssueNote;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;


public class MaterialConsumptionReportPdf {

	public  Document document;
	Company comp;
	Customer cust;
	Contract conEntity;
	MaterialIssueNote minEntity;
	MaterialMovementNote mmnEntity;
	SuperProduct product;
	List<MaterialIssueNote> minlist = new ArrayList<MaterialIssueNote>();
	List<MaterialMovementNote> mmnlist = new ArrayList<MaterialMovementNote>();
	List<MaterialConsumptionReport> reportlist = new ArrayList<MaterialConsumptionReport>();
	List<MaterialConsumptionReport> reportListMin = new ArrayList<MaterialConsumptionReport>();
	
	List<MaterialConsumptionReport> reportList12 = new ArrayList<MaterialConsumptionReport>();
	List<MaterialConsumptionReport> reportListMmn = new ArrayList<MaterialConsumptionReport>();
	
	List<SuperProduct> prodPrizelis= new ArrayList<SuperProduct>();
	
	public GenricServiceImpl impl;
	
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font16bold,font10,font10bold,font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	public MaterialConsumptionReportPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
		
		impl = new GenricServiceImpl();
	}
	
public void setMaterialReproNote(int count) {
	
	conEntity=ofy().load().type(Contract.class).filter("count", count).first().now();
	
	// load con entity 
	if(conEntity.getCompanyId()==null)
		cust=ofy().load().type(Customer.class).filter("count",conEntity.getCinfo().getCount()).first().now();
	else
		cust=ofy().load().type(Customer.class).filter("count",conEntity.getCinfo().getCount()).filter("companyId", conEntity.getCompanyId()).first().now();
		
	//Load Company
		if(conEntity.getCompanyId()==null)
		   comp=ofy().load().type(Company.class).first().now();
		else
		   comp=ofy().load().type(Company.class).filter("companyId",conEntity.getCompanyId()).first().now();
		
		
		//    load MMN 
		
		if(conEntity.getCompanyId()==null)
			mmnlist=ofy().load().type(MaterialMovementNote.class).filter("orderID", conEntity.getCount()+"").list();
		else
			mmnlist=ofy().load().type(MaterialMovementNote.class).filter("orderID", conEntity.getCount()+"").filter("companyId",conEntity.getCompanyId()).list();
		System.out.println("mmn list size ====="+mmnlist.size());
		
		
		if(conEntity.getCompanyId()==null){
			minlist=ofy().load().type(MaterialIssueNote.class).filter("minSoId", conEntity.getCount()).list();
		}
		else{
			minlist=ofy().load().type(MaterialIssueNote.class).filter("minSoId", conEntity.getCount()).filter("companyId",conEntity.getCompanyId()).list();
		}
		
		System.out.println(" min list size"+minlist.size());
		
		for(int j=0;j<minlist.size();j++){
		
		
			System.out.println("status value "+minlist.get(j).getStatus());
			if(minlist.get(j).getStatus().equals(MaterialIssueNote.APPROVED)){
				
				for(int i=0;i<minlist.get(j).getSubProductTablemin().size();i++)
				{
					MaterialConsumptionReport report = new MaterialConsumptionReport();
					report.setContractId(minlist.get(j).getMinSoId());
					report.setServiceId(minlist.get(j).getServiceId());
					report.setDocID(minlist.get(j).getCount());
					report.setProdId(minlist.get(j).getSubProductTablemin().get(i).getMaterialProductId());
					report.setProdName(minlist.get(j).getSubProductTablemin().get(i).getMaterialProductName());
					report.setProdCode(minlist.get(j).getSubProductTablemin().get(i).getMaterialProductCode());
					report.setProdUOM(minlist.get(j).getSubProductTablemin().get(i).getMaterialProductUOM());
					report.setQuantity(minlist.get(j).getSubProductTablemin().get(i).getMaterialProductRequiredQuantity());
					
					reportlist.add(report);
				}
			
		}
		}
		
		
		System.out.println("reportlist size ======"+ reportlist.size());
		int rowindex=0;
		for(int i=0;i<reportlist.size();i++){
			
			prodPrizelis=ofy().load().type(SuperProduct.class).filter("count", reportlist.get(i).getProdId()).filter("productCode", reportlist.get(i).getProdCode()).list();	
		
				
				System.out.println("prod prize"+prodPrizelis.get(0).getPrice());
				System.out.println("rowindex value ===="+rowindex);
				MaterialConsumptionReport materialreport =new MaterialConsumptionReport();
				materialreport.setContractId(reportlist.get(rowindex).getContractId());
				materialreport.setServiceId(reportlist.get(rowindex).getServiceId());
				materialreport.setDocType("MIN");
				materialreport.setDocID(reportlist.get(rowindex).getDocID());
				materialreport.setProdId(reportlist.get(rowindex).getProdId());
				System.out.println("reportlist.get(rowindex).getProdId());"+reportlist.get(rowindex).getProdId());
				materialreport.setProdName(reportlist.get(rowindex).getProdName());
				materialreport.setProdCode(reportlist.get(rowindex).getProdCode());
				materialreport.setProdUOM(reportlist.get(rowindex).getProdUOM());
				materialreport.setQuantity(reportlist.get(rowindex).getQuantity());
				
					for(int  j=0;j<prodPrizelis.size();j++){	
						System.out.println("reportList.get(j).getPrize() value  ============="+prodPrizelis.get(j).getPrice());
						materialreport.setPrice(prodPrizelis.get(j).getPrice());
						materialreport.setTotal(prodPrizelis.get(j).getPrice()*reportlist.get(rowindex).getQuantity());
					}
					
					
					reportListMin.add(materialreport);
				
				System.out.println("materialReportlis"+reportListMin.size());	
				
				if(rowindex==reportlist.size()-1){
					
					
			    	 double amt=0;
			    	  for(int k=0;k<reportListMin.size();k++){
			    		amt=amt+reportListMin.get(k).getTotal();
			    	  }
			    	  
			    	  getReturnmaterialFromContractCount(amt); 
				}	
			
		
			if(rowindex!=0){
				rowindex=rowindex+1;
			}
			else{
				rowindex=1;
			}
				
		}
		
	
	}

int rowmmnindex=0;
	private void getReturnmaterialFromContractCount(double amt) {
		
		System.out.println("companyId" +conEntity.getCompanyId());
		if(conEntity.getCompanyId()==null)
		{
			mmnlist=ofy().load().type(MaterialMovementNote.class).filter("orderID", conEntity.getCount()+"").list();
		System.out.println("in side if");
		}
		else
		{
			mmnlist=ofy().load().type(MaterialMovementNote.class).filter("companyId",conEntity.getCompanyId()).filter("orderID", conEntity.getCount()+"").list();
		System.out.println("in side else ");
		}
		System.out.println(" mmn list size"+mmnlist.size());
		
		if(mmnlist.size()==0){
			reportListMmn=reportListMin;
			System.out.println("reportListMmn size in side zero condotion"+reportListMmn.size());
		}
		else
		{
		for(int j=0;j<mmnlist.size();j++){
		if(mmnlist.get(j).getStatus().equals(MaterialMovementNote.APPROVED)){
			System.out.println("table size==========="+mmnlist.get(j).getSubProductTableMmn().size());
			for(int i=0;i<mmnlist.get(j).getSubProductTableMmn().size();i++)
			{
				
			
				MaterialConsumptionReport report = new MaterialConsumptionReport();
			report.setContractId(Integer.parseInt(mmnlist.get(j).getOrderID()+""));
			report.setServiceId(mmnlist.get(j).getServiceId());
			report.setDocID(mmnlist.get(j).getCount());
			report.setProdId(mmnlist.get(j).getSubProductTableMmn().get(i).getMaterialProductId());
			report.setProdName(mmnlist.get(j).getSubProductTableMmn().get(i).getMaterialProductName());
			report.setProdCode(mmnlist.get(j).getSubProductTableMmn().get(i).getMaterialProductCode());
			report.setProdUOM(mmnlist.get(j).getSubProductTableMmn().get(i).getMaterialProductUOM());
			report.setQuantity(mmnlist.get(j).getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity());
			
			reportList12.add(report);
			
		}
			
		}
		}
		
		for(int  i=0;i<reportList12.size();i++){
			prodPrizelis.clear();
				prodPrizelis=ofy().load().type(SuperProduct.class).filter("count", reportList12.get(i).getProdId()).filter("productCode", reportList12.get(i).getProdCode()).list();	
			
				System.out.println("prod prize"+prodPrizelis.get(0).getPrice());
				
				MaterialConsumptionReport materialReport =new MaterialConsumptionReport();
				materialReport.setContractId(reportList12.get(rowmmnindex).getContractId());
				materialReport.setServiceId(reportList12.get(rowmmnindex).getServiceId());
				materialReport.setDocType("MMN");
				materialReport.setDocID(reportList12.get(rowmmnindex).getDocID());
				materialReport.setProdId(reportList12.get(rowmmnindex).getProdId());
				materialReport.setProdName(reportList12.get(rowmmnindex).getProdName());
				materialReport.setProdCode(reportList12.get(rowmmnindex).getProdCode());
				materialReport.setProdUOM(reportList12.get(rowmmnindex).getProdUOM());
				materialReport.setQuantity(reportList12.get(rowmmnindex).getQuantity());
				
					for(int  j=0;j<prodPrizelis.size();j++){	
						materialReport.setPrice(prodPrizelis.get(j).getPrice());
						materialReport.setTotal(-(prodPrizelis.get(j).getPrice()*reportList12.get(rowmmnindex).getQuantity()));
					}
					reportListMin.add(materialReport);
				
				System.out.println("materialReportlis"+reportListMin.size());	
					
				
				
				if(rowmmnindex==reportList12.size()-1){
//					
					System.out.println("materialReportlis inside if"+reportListMin.size());
				
					    	 double picamt=0;
					    	  for(int k=0;k<reportListMin.size();k++){
					    		picamt=picamt+reportListMin.get(k).getTotal();
					    	  }
					    	  
				}	
					if(rowmmnindex!=0){
						rowmmnindex=rowmmnindex+1;
					}
					else{
						rowmmnindex=1;
					}
		}
		}
	
}
	
	
	
	
	public void createPdf() {
	createLogo(document,comp);
	createCompanyHedding();
//	getContractDetails();
	createProductDetail();
		
	System.out.println("in side create pdf method");	
	
	}
	
	
	
	private void createLogo(Document doc, Company comp) {
		

		//********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	}
	
	public  void createCompanyHedding()
	{
		
		Phrase companyName= new Phrase("                  "+comp.getBusinessUnitName(),font12boldul);
	    Paragraph p =new Paragraph();
	    p.add(Chunk.NEWLINE);
	    p.add(companyName);
	    
	    PdfPCell companyHeadingCell=new PdfPCell();
	    companyHeadingCell.setBorder(0);
	    companyHeadingCell.addElement(p);
	    
	    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
		Phrase adressline2=null;
		if(comp.getAddress().getAddrLine2()!=null)
		{
			adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
		}
		
		String complandmark1="";
		Phrase landmar1=null;
		
		String  localit1="";
		Phrase complocality1= null;
		
		if(comp.getAddress().getLandmark()!=null)
		{
			complandmark1 = comp.getAddress().getLandmark();
			landmar1=new Phrase(complandmark1,font10);
		}
		
		
		if(comp.getAddress().getLocality()!=null){
			localit1=comp.getAddress().getLocality();
			complocality1=new Phrase(localit1,font10);
			}
		
		Phrase cityState=new Phrase(comp.getAddress().getCity()
				+" - "+comp.getAddress().getPin()
				,font10);
		
		Phrase branch=null;
		if(cust.getBranch() !=null){
			
		 branch =new Phrase("Branch : "+cust.getBranch(),font10);
		}
		else
			{ 
			branch =new Phrase(cust.getBranch()); 
			}
		
		PdfPCell branchcell=new PdfPCell();
		branchcell.addElement(branch);
		branchcell.setBorder(0);
		
		
		PdfPCell custlandcell=new PdfPCell();
		custlandcell.addElement(landmar1);
		custlandcell.setBorder(0);
		
		PdfPCell custlocalitycell=new PdfPCell();
		custlocalitycell.addElement(complocality1);
		custlocalitycell.setBorder(0);
		
		PdfPCell custcitycell=new PdfPCell();
		custcitycell.addElement(cityState);
		custcitycell.setBorder(0); 

		PdfPCell addressline1cell=new PdfPCell();
		PdfPCell addressline2cell=new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);
		if(adressline2!=null)
		{
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
		}
		
		String contactinfo="Mobile: "+comp.getContact().get(0).getCellNo1();
		if(comp.getContact().get(0).getCellNo2()!=-1)
			contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
		if(comp.getContact().get(0).getLandline()!=-1){
			contactinfo=contactinfo+"     "+" Phone: 0"+comp.getContact().get(0).getLandline();
		}
		
		Phrase contactnos=new Phrase(contactinfo,font9);
		Phrase email= new Phrase("email: "+comp.getContact().get(0).getEmail(),font9);
		
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell=new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);
		
		
		
		
		PdfPTable companytable=new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(addressline1cell);
		if(!comp.getAddress().getAddrLine2().equals(""))
		{
			companytable.addCell(addressline2cell);
		}
		if(!comp.getAddress().getLandmark().equals("")){
		companytable.addCell(custlandcell);
		}
		if(!comp.getAddress().getLocality().equals("")){
		companytable.addCell(custlocalitycell);
		}
		companytable.addCell(custcitycell);
		companytable.addCell(branchcell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		
		
		Phrase blankPhase= new Phrase("",font9);
		PdfPCell blankcell = new PdfPCell(blankPhase);
		blankcell.setBorder(0);
		
		
		if(comp.getAddress().getAddrLine2().equals("")){
			companytable.addCell(blankcell);
		}
		if(comp.getAddress().getLandmark().equals("")){
			companytable.addCell(blankcell);
		}
		if(comp.getAddress().getLocality().equals("")){
			companytable.addCell(blankcell);
		}
		
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);

		

		if(comp.getContact()!=null&&comp.getCompanyId()!=null){
				Phrase realcontact=new Phrase("Contact Person: "+comp.getPocName(),font9);
				realcontact.add(Chunk.NEWLINE);
				PdfPCell realcontactcell=new PdfPCell();
				realcontactcell.addElement(realcontact);
				realcontactcell.setBorder(0);
				companytable.addCell(realcontactcell);
			}
		
		
		/**
		 * Customer Info
		 */
		String tosir="To, M/S";
		String custName="";
		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
			custName=cust.getCompanyName().trim();
		}
		else{
			custName=cust.getFullname().trim();
		}
		
		Phrase tosirphrase= new Phrase(tosir,font12bold);
		
		Phrase customername= new Phrase(custName,font12boldul);
	    Paragraph fullname =new Paragraph();
	    fullname.add(Chunk.NEWLINE);
	    fullname.add(tosir+"   "+custName);
	    fullname.setFont(font12bold);
	    
			PdfPCell custnamecell=new PdfPCell();
			custnamecell.addElement(fullname);
			custnamecell.setBorder(0);
			 
			Phrase customeradress= new Phrase("                "+cust.getAdress().getAddrLine1(),font10);
		Phrase customeradress2=null;
		if(cust.getAdress().getAddrLine2()!=null){
		   customeradress2= new Phrase("                "+cust.getAdress().getAddrLine2(),font10);
		}
		
		PdfPCell custaddress1=new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);
		
		PdfPCell custaddress2=new PdfPCell();
		if(cust.getAdress().getAddrLine2()!=null){
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
			}
		
		
		String custlandmark2="";
		Phrase landmar2=null;
		
		String  localit2="";
		Phrase custlocality2= null;
		
		if(cust.getAdress().getLandmark()!=null)
		{
			custlandmark2 = cust.getAdress().getLandmark();
			landmar2=new Phrase("                "+custlandmark2,font10);
		}
		
		
		if(cust.getAdress().getLocality()!=null){
			localit2=cust.getAdress().getLocality();
			custlocality2=new Phrase("                "+localit2,font10);
			}
		
		Phrase cityState2=new Phrase("                "+cust.getAdress().getCity()
				+" - "+cust.getAdress().getPin()
//				+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
				,font10);
		
		PdfPCell custlandcell2=new PdfPCell();
		custlandcell2.addElement(landmar2);
		custlandcell2.setBorder(0);
		
		PdfPCell custlocalitycell2=new PdfPCell();
		custlocalitycell2.addElement(custlocality2);
		custlocalitycell2.setBorder(0);
		
		PdfPCell custcitycell2=new PdfPCell();
		custcitycell2.addElement(cityState2);
		custcitycell2.setBorder(0); 
//		custcitycell2.addElement(Chunk.NEWLINE);
			 
		Phrase custcontact=new Phrase("Mobile: "+cust.getCellNumber1()+"",font9);
			PdfPCell custcontactcell=new PdfPCell();
			custcontactcell.addElement(custcontact);
			custcontactcell.setBorder(0);
			
			Phrase custemail=new Phrase("email: "+cust.getEmail(),font9);
			PdfPCell custemailcell=new PdfPCell();
			custemailcell.addElement(custemail);
			custemailcell.setBorder(0);
			
				
			PdfPTable custtable=new PdfPTable(1);
			custtable.setWidthPercentage(100);
			custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
			custtable.addCell(custnamecell);
			custtable.addCell(custaddress1);
			if(!cust.getAdress().getAddrLine2().equals("")){
				custtable.addCell(custaddress2);
			}
			if(!cust.getAdress().getLandmark().equals("")){
				custtable.addCell(custlandcell2);
			}
			if(!cust.getAdress().getLocality().equals("")){
				
			custtable.addCell(custlocalitycell2);
			}
			custtable.addCell(custcitycell2);
			custtable.addCell(custcontactcell);
			custtable.addCell(custemailcell);
			
			
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				Phrase realcontact=new Phrase("Attn: "+cust.getFullname(),font10);
				realcontact.add(Chunk.NEWLINE);
				PdfPCell realcontactcell=new PdfPCell();
				realcontactcell.addElement(realcontact);
				realcontactcell.setBorder(0);
				custtable.addCell(realcontactcell);
			}
			 
			
			PdfPTable headparenttable= new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[]{50,50});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPCell  companyinfocell = new PdfPCell();
		PdfPCell custinfocell = new PdfPCell();
		
		
		companyinfocell.addElement(companytable);
		custinfocell.addElement(custtable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(custinfocell);
		
		
		 String title="Material Consumption Report";
			
			Phrase titlephrase = new Phrase(title,font14bold);
			PdfPCell cell = new PdfPCell(titlephrase);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			PdfPTable table = new PdfPTable(1);
			table.setWidthPercentage(100f);
			table.addCell(cell);

		try {
			document.add(headparenttable);
			document.add(table);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		}
	
	private void getContractDetails(){
		
		
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8);
		Phrase productdetails= new Phrase("Contract Details",font10bold);
		
		PdfPCell productdetailscell = new PdfPCell(productdetails);
		productdetailscell.setBorder(0);
		
		
		
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100);
		table.setSpacingBefore(10f);
		Phrase conId = new  Phrase("Contract ID",font8bold);
		Phrase conStatus = new Phrase("Contract Status",font8bold);
	    Phrase constartdt = new Phrase("Contract Start Date",font8bold);
	    Phrase conenddt = new Phrase("Contract End Date",font8bold);
	    
	    PdfPCell cellconId = new PdfPCell(conId);
	    cellconId.setHorizontalAlignment(Element.ALIGN_CENTER);
	    
		 PdfPCell cellconStatus = new PdfPCell(conStatus);
		 cellconStatus.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
	     PdfPCell cellconstartdt = new PdfPCell(constartdt);
	     cellconstartdt.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	     PdfPCell cellconenddt = new PdfPCell(conenddt);
	     cellconenddt.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	     
	     table.addCell(cellconId);
	     table.addCell(cellconStatus);
	     table.addCell(cellconstartdt);
	     table.addCell(cellconenddt);
	     
	     
	     Phrase conIdvalue = new  Phrase(conEntity.getCount()+"",font1);
			Phrase conStatusvalue = new Phrase(conEntity.getStatus(),font1);
		    Phrase constartdtvalue = new Phrase(conEntity.getStartDate()+"",font1);
		    Phrase conenddtvalue = new Phrase(conEntity.getEndDate()+"",font1);
		    
		    PdfPCell cellconIdvalue = new PdfPCell(conIdvalue);
		    cellconId.setHorizontalAlignment(Element.ALIGN_CENTER);
		    
			 PdfPCell cellconStatusvalue = new PdfPCell(conStatusvalue);
			 cellconStatus.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
		     PdfPCell cellconstartdtvalue = new PdfPCell(constartdtvalue);
		     cellconstartdt.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
		     PdfPCell cellconenddtvalue = new PdfPCell(conenddtvalue);
		     cellconenddt.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
		     
		     table.addCell(cellconIdvalue);
		     table.addCell(cellconStatusvalue);
		     table.addCell(cellconstartdtvalue);
		     table.addCell(cellconenddtvalue);
	     
		     
		     PdfPTable parentTable=new PdfPTable(1);
		     parentTable.setWidthPercentage(100);
		     parentTable.addCell(productdetailscell);
		     
		     PdfPCell tablecell = new PdfPCell(parentTable);
		     tablecell.setBorder(0);
		     
		     PdfPTable parentTableProd=new PdfPTable(1);
		     parentTableProd.setWidthPercentage(100);
		     PdfPCell prodtablecell=new PdfPCell();
		     prodtablecell.addElement(table);
		     prodtablecell.setBorder(0);
		     
		     parentTableProd.addCell(tablecell);
		     parentTableProd.addCell(prodtablecell);
		     
		     PdfPCell paterntcell = new PdfPCell();
		     paterntcell.addElement(parentTableProd);
//		     paterntcell.setBorder(0);
		     
		     PdfPTable parent=new PdfPTable(1);
		     parent.setWidthPercentage(100);
		     parent.addCell(paterntcell);
		     
		 	try {
		 		document.add(parent);
		 	} catch (DocumentException e) {
		 		e.printStackTrace();
		 	}
		
	}
	
private void createProductDetail() {
	
	//****************************************************
	Font font1 = new Font(Font.FontFamily.HELVETICA  , 8);
	Phrase productcontractdetails= new Phrase("Contract Details",font10bold);
	
	PdfPCell productcontractdetailscell = new PdfPCell(productcontractdetails);
	productcontractdetailscell.setBorder(0);
	
	
	
	PdfPTable contracttable = new PdfPTable(4);
	contracttable.setWidthPercentage(100);
	contracttable.setSpacingBefore(10f);
	Phrase conId = new  Phrase("Contract ID",font8bold);
	Phrase conStatus = new Phrase("Contract Status",font8bold);
    Phrase constartdt = new Phrase("Contract Start Date",font8bold);
    Phrase conenddt = new Phrase("Contract End Date",font8bold);
    
    PdfPCell cellconId = new PdfPCell(conId);
    cellconId.setHorizontalAlignment(Element.ALIGN_CENTER);
    
	 PdfPCell cellconStatus = new PdfPCell(conStatus);
	 cellconStatus.setHorizontalAlignment(Element.ALIGN_CENTER);
	 
     PdfPCell cellconstartdt = new PdfPCell(constartdt);
     cellconstartdt.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell cellconenddt = new PdfPCell(conenddt);
     cellconenddt.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     
     contracttable.addCell(cellconId);
     contracttable.addCell(cellconStatus);
     contracttable.addCell(cellconstartdt);
     contracttable.addCell(cellconenddt);
     
     
     Phrase conIdvalue = new  Phrase(conEntity.getCount()+"",font1);
		Phrase conStatusvalue = new Phrase(conEntity.getStatus(),font1);
	    Phrase constartdtvalue = new Phrase(fmt.format(conEntity.getStartDate()),font1);
	    Phrase conenddtvalue = new Phrase(fmt.format(conEntity.getEndDate()),font1);
	    
	    PdfPCell cellconIdvalue = new PdfPCell();
	    cellconIdvalue.addElement(conIdvalue);
	    cellconId.setHorizontalAlignment(Element.ALIGN_CENTER);
	    
		 PdfPCell cellconStatusvalue = new PdfPCell();
		 cellconStatusvalue.addElement(conStatusvalue);
		 cellconStatus.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
	     PdfPCell cellconstartdtvalue = new PdfPCell();
	     cellconstartdtvalue.addElement(constartdtvalue);
	     cellconstartdt.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	     PdfPCell cellconenddtvalue = new PdfPCell();
	     cellconenddtvalue.addElement(conenddtvalue);
	     cellconenddt.setHorizontalAlignment(Element.ALIGN_CENTER);
	     
	     
	     contracttable.addCell(cellconIdvalue);
	     contracttable.addCell(cellconStatusvalue);
	     contracttable.addCell(cellconstartdtvalue);
	     contracttable.addCell(cellconenddtvalue);
     
	     
	     PdfPTable parentTable=new PdfPTable(1);
	     parentTable.setWidthPercentage(100);
	     parentTable.addCell(productcontractdetailscell);
	     
	     PdfPCell tablecell = new PdfPCell(parentTable);
	     tablecell.setBorder(0);
	     
	     PdfPTable parentTableProd=new PdfPTable(1);
	     parentTableProd.setWidthPercentage(100);
	     PdfPCell prodtablecell=new PdfPCell();
	     prodtablecell.addElement(contracttable);
	     prodtablecell.setBorder(0);
	     
	     parentTableProd.addCell(tablecell);
	     parentTableProd.addCell(prodtablecell);
	     
	     PdfPCell paterntcell = new PdfPCell();
	     paterntcell.addElement(parentTableProd);
//	     paterntcell.setBorder(0);
	     
	     PdfPTable parent=new PdfPTable(1);
	     parent.setWidthPercentage(100);
	     parent.addCell(paterntcell);
	     
	 	
	
	//****************************************************
	
	Phrase productdetails= new Phrase("Material Details",font10bold);
	
	PdfPCell productdetailscell = new PdfPCell(productdetails);
	productdetailscell.setBorder(0);
	
		
	PdfPTable table = new PdfPTable(11);
	table.setWidthPercentage(100);
	table.setSpacingBefore(10f);
	Phrase contractCount = new  Phrase("Contract Id",font8bold);
	Phrase serviceId = new  Phrase("Service Id",font8bold);
	Phrase docType = new  Phrase("Doc Type",font8bold);
	Phrase docId = new  Phrase("Doc Id",font8bold);
	Phrase prodId = new Phrase("Prod ID",font8bold);
    Phrase prodCode = new Phrase("Code",font8bold);
    Phrase prodName = new Phrase("Name",font8bold);
    Phrase qty = new Phrase("Qty",font8bold);
    Phrase uom = new Phrase("UOM",font8bold);
    Phrase prize = new Phrase("Prod Prize",font8bold);
    Phrase total = new Phrase("Total",font8bold);
    
    PdfPCell cellcontractCount = new PdfPCell(contractCount);
    cellcontractCount.setHorizontalAlignment(Element.ALIGN_CENTER);
    
    PdfPCell cellserviceId = new PdfPCell(serviceId);
    cellserviceId.setHorizontalAlignment(Element.ALIGN_CENTER);
    
     PdfPCell celldocType = new PdfPCell(docType);
     celldocType.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell celldocId = new PdfPCell(docId);
     celldocId.setHorizontalAlignment(Element.ALIGN_CENTER);
    
	 PdfPCell cellprodId = new PdfPCell(prodId);
	 cellprodId.setHorizontalAlignment(Element.ALIGN_CENTER);
	 
     PdfPCell cellprodCode = new PdfPCell(prodCode);
     cellprodCode.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell cellprodName = new PdfPCell(prodName);
     cellprodName.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell cellqty = new PdfPCell(qty);
     cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell  celluom = new PdfPCell(uom);
     celluom.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell cellprize = new PdfPCell(prize);
     cellprize.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     PdfPCell celltotal = new PdfPCell(total);
     celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
     
     table.addCell(cellcontractCount);
     table.addCell(cellserviceId);
     table.addCell(celldocType);
     table.addCell(celldocId);
     table.addCell(cellprodId);
     table.addCell(cellprodCode);
     table.addCell(cellprodName);
     table.addCell(cellqty);
     table.addCell(celluom);
     table.addCell(cellprize);
     table.addCell(celltotal);
     
     
     
     for(int i=0;i<reportListMin.size();i++){
    	 
    	 	Phrase contractCountValue = new  Phrase(reportListMin.get(i).getContractId()+"",font1);
    		Phrase serviceIdValue = new  Phrase(reportListMin.get(i).getServiceId()+"",font1);
    		Phrase docTypeValue = new  Phrase(reportListMin.get(i).getDocType(),font1);
    		Phrase docIdValue = new  Phrase(reportListMin.get(i).getDocID()+"",font1);
    		Phrase prodIdValue = new Phrase(reportListMin.get(i).getProdId()+"",font1);
    	    Phrase prodCodeValue = new Phrase(reportListMin.get(i).getProdCode(),font1);
    	    Phrase prodNameValue = new Phrase(reportListMin.get(i).getProdName(),font1);
    	    Phrase qtyValue = new Phrase(reportListMin.get(i).getQuantity()+"",font1);
    	    Phrase uomValue = new Phrase(reportListMin.get(i).getProdUOM(),font1);
    	    Phrase prizeValue = new Phrase(reportListMin.get(i).getPrice()+"",font1);
    	    Phrase totalValue = new Phrase(reportListMin.get(i).getTotal()+"",font1);
    	 
    	    
    	    PdfPCell cellcontractCountValue = new PdfPCell(contractCountValue);
    	    cellcontractCountValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	    
    	    PdfPCell cellserviceIdValue = new PdfPCell(serviceIdValue);
    	    cellserviceIdValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	    
    	     PdfPCell celldocTypeValue = new PdfPCell(docTypeValue);
    	     celldocTypeValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     PdfPCell celldocIdValue = new PdfPCell(docIdValue);
    	     celldocIdValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	    
    		 PdfPCell cellprodIdValue = new PdfPCell(prodIdValue);
    		 cellprodIdValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    		 
    	     PdfPCell cellprodCodeValue = new PdfPCell(prodCodeValue);
    	     cellprodCodeValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     PdfPCell cellprodNameValue = new PdfPCell(prodNameValue);
    	     cellprodNameValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     PdfPCell cellqtyValue = new PdfPCell(qtyValue);
    	     cellqtyValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     PdfPCell  celluomValue = new PdfPCell(uomValue);
    	     celluomValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     PdfPCell cellprizeValue = new PdfPCell(prizeValue);
    	     cellprizeValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     PdfPCell celltotalValue = new PdfPCell(totalValue);
    	     celltotalValue.setHorizontalAlignment(Element.ALIGN_CENTER);
    	     
    	     table.addCell(cellcontractCountValue);
    	     table.addCell(cellserviceIdValue);
    	     table.addCell(celldocTypeValue);
    	     table.addCell(celldocIdValue);
    	     table.addCell(cellprodIdValue);
    	     table.addCell(cellprodCodeValue);
    	     table.addCell(cellprodNameValue);
    	     table.addCell(cellqtyValue);
    	     table.addCell(celluomValue);
    	     table.addCell(cellprizeValue);
    	     table.addCell(celltotalValue);
     }
     
     
 	PdfPTable parentTableProd12=new PdfPTable(1);
 	parentTableProd12.setWidthPercentage(100);
    
    
    PdfPCell prodtablecell12=new PdfPCell();
    parentTableProd12.addCell(productdetailscell);
    prodtablecell12.setBorder(0);
    
    prodtablecell12.addElement(table);
    parentTableProd12.addCell(prodtablecell12);
    
    
    
    PdfPCell parentcell = new PdfPCell();
    parentcell.addElement(parentTableProd12);
    
    PdfPTable parent12=new PdfPTable(1);
    parent12.setWidthPercentage(100);
    parent12.addCell(parentcell);
    
    
    Phrase netpay = new Phrase("Net Amount",font10bold);
    PdfPCell netpaycell = new PdfPCell(netpay);
//    netpaycell.addElement(netpay);
    netpaycell.setHorizontalAlignment(Element.ALIGN_CENTER);
    netpaycell.setBorder(0);
    
    double netPay= 0;
    for(int i=0;i<reportListMin.size();i++){
    	netPay= netPay+reportListMin.get(i).getTotal();
    }
    Phrase netpayvalue = new Phrase(netPay+"",font10bold);
    PdfPCell netpayvaluecell = new PdfPCell(netpayvalue);
//    netpayvaluecell.addElement(netpayvalue);
    netpayvaluecell.setHorizontalAlignment(Element.ALIGN_CENTER);

    
    
    Phrase blankp= new Phrase(" ",font1);
    PdfPCell nlankcell = new PdfPCell();
    nlankcell.addElement(blankp);
    nlankcell.setBorder(0);
    
    PdfPTable netpaytable = new PdfPTable(2);
    netpaytable.setWidthPercentage(100);
    netpaytable.addCell(nlankcell);
    netpaytable.addCell(nlankcell);
    netpaytable.addCell(nlankcell);
    netpaytable.addCell(netpaycell);
    netpaytable.addCell(nlankcell);
    netpaytable.addCell(netpayvaluecell);
    
    try {
    	netpaytable.setWidths(new float[]{80,20});
    } catch (DocumentException e1) {
    	e1.printStackTrace();
    }
    
    
	try {
		document.add(parent);
		document.add(parent12);
		document.add(netpaytable);
		
	} catch (DocumentException e) {
		e.printStackTrace();
	}
     
	}
	
	
	
	
}
