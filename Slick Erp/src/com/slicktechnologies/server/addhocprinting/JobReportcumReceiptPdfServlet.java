package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;


public class JobReportcumReceiptPdfServlet extends HttpServlet {

	private static final long serialVersionUID = 2372131781719790232L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			System.out.println("Id:::::::::::::::::::::::::::"+stringid.trim());
			Long count = Long.parseLong(stringid);
			
			JobReportcumReceiptPdf jobpdf = new JobReportcumReceiptPdf();
			
			jobpdf.document = new Document();
			Document document = jobpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
			 
			 document.open();
			 jobpdf.setpdfjob(count);
			 jobpdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
