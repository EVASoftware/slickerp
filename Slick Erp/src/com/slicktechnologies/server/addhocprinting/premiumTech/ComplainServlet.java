package com.slicktechnologies.server.addhocprinting.premiumTech;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.ContractPdf;

public class ComplainServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1379526514627937774L;
	Logger logger = Logger.getLogger("Name of logger");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		logger.log(Level.SEVERE, "inside doget complain pdf servlet");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			
//			 String preprintStatus=req.getParameter("preprint");
			 
			ComplainPdf compdf = new ComplainPdf();
			
			compdf.document = new Document();
			Document document = compdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			 document.open();
			 compdf.getComplain(count);
			 compdf.createPdf();
//			  if(preprintStatus.contains("yes")){
//				  
//					System.out.println("yes ");
//					conpdf.createPdf(preprintStatus);
//			  }
//			  else
//			  {
//					System.out.println("no ");
//					conpdf.createPdf(preprintStatus);
//			  }
//			 
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	
	
	
	
	
	
}
