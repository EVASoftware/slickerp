package com.slicktechnologies.server.addhocprinting.premiumTech;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class ComplainPdf {

	private Font  font12bold,   font9bold,font14bold, font10;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	Complain complain;
	Company comp;
	Customer cust;
	public Document document;
	Logger logger = Logger.getLogger("Name of logger");
	
	/**
	 * @author Anil @since 22-07-2021
	 * Thai font flag
	 */
	boolean thaiFontFlag=false;
	
	public ComplainPdf(){
		font12bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 7);
		

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		


	
	}
	public void getComplain(long count) {
		logger.log(Level.SEVERE, "inside doget complain pdf getComplain");
		complain = ofy().load().type(Complain.class).id(count).now();
		
		if (complain.getCompanyId() != null) {
			
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", complain.getCompanyId())
					.filter("processName", "Company")
					.filter("configStatus", true).first().now();
			
			if(processConfig!=null){
				for(ProcessTypeDetails obj:processConfig.getProcessList()){
					if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
						thaiFontFlag=true;
						break;
					}
				}
			}
		}
		try {
			logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
			
			if(thaiFontFlag){
				BaseFont regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				BaseFont boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
				
				font12bold = new Font(boldFont, 13);
				font14bold = new Font(boldFont, 15);
				font9bold = new Font(boldFont, 8);
				font10 = new Font(regularFont, 7);
			}
			
			
		} catch (DocumentException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
		} catch (IOException e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
		}catch (Exception e1) {
			e1.printStackTrace();
			logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
		}
		
		if (complain.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", complain.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (complain.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", complain.getPersoninfo().getCount())
					.filter("companyId", complain.getCompanyId()).first().now();
		if (complain.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count",complain.getPersoninfo().getCount()).first().now();

		
		
	}
	public void createPdf() {
		companyDetails();
		ticketDetails();
		customerDetails();
		complainDescription();
		
	}
	private void complainDescription() {
		PdfPTable complainDescTable=new PdfPTable(3);
		complainDescTable.setWidthPercentage(100);
//		complainDescTable.setSpacingBefore(15);
		
		
		try {
			complainDescTable.setWidths(new float[]{20,40,40});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		
		String state="Complaint Description";
		Phrase statepr =new Phrase(state,font12bold);
		PdfPCell stateCell=new PdfPCell(statepr);
		stateCell.setColspan(3);
		stateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		String complainn="Complaint Description: ";
		Phrase complainpr =new Phrase(complainn,font9bold);
		PdfPCell complainprCell=new PdfPCell(complainpr);
//		complainprCell.setBorder(0);
		complainprCell.setBorderWidthTop(0);
		complainprCell.setBorderWidthBottom(0);
		complainprCell.setBorderWidthRight(0);
		complainprCell.setFixedHeight(25f);
		
		String complainValue=" ";
		if(complain.getDestription()!=null){
			complainValue=complain.getDestription();
		}else{
			complainValue="";
		}
		Phrase complainValuepr =new Phrase(complainValue,font10);
		PdfPCell complainValueCell=new PdfPCell(complainValuepr);
		complainValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		complainprCell.setFixedHeight(25f);
		complainValueCell.setColspan(2);
		
		
		Phrase blank2 =new Phrase(" ");
		PdfPCell blank2Cell=new PdfPCell(blank2);
		blank2Cell.setBorder(0);
		
		String typeOfServ="Types of Services.: ";
		
		Phrase typeOfServpr =new Phrase(typeOfServ,font9bold);
		PdfPCell typeOfServprCell=new PdfPCell(typeOfServpr);
		typeOfServprCell.setBorderWidthTop(0);
		typeOfServprCell.setBorderWidthBottom(0);
		typeOfServprCell.setBorderWidthRight(0);

		String typeOfServValue="";
		if(complain.getCategory()!=null){
			typeOfServValue=complain.getCategory();
		}else{
			typeOfServValue="";
		}
		Phrase typeOfServValuepr =new Phrase(typeOfServValue,font10);
		PdfPCell typeOfServValueCell=new PdfPCell(typeOfServValuepr);
		
		Phrase blk =new Phrase("",font10);
		PdfPCell bkCell=new PdfPCell(blk);
		bkCell.setBorderWidthTop(0);
		bkCell.setBorderWidthBottom(0);
		bkCell.setBorderWidthLeft(0);
		
		
		
		String typeofproduct="Types of Products.: ";
		Phrase typeofproductpr =new Phrase(typeofproduct,font9bold);
		PdfPCell typeofproductprCell=new PdfPCell(typeofproductpr);
		typeofproductprCell.setBorderWidthTop(0);
		typeofproductprCell.setBorderWidthBottom(0);
		typeofproductprCell.setBorderWidthRight(0);
		
		String typeofproductValue="";
		if(complain.getPic().getProductName()!=null){
			typeofproductValue=complain.getPic().getProductName();//Ashwini Patil Date:10-08-2022
		}else{
			typeofproductValue="";
		}
		
		Phrase typeofproductValuepr =new Phrase(typeofproductValue,font10);
		PdfPCell typeofproductValueCell=new PdfPCell(typeofproductValuepr);
		
		
		Phrase blank5 =new Phrase(" ");
		PdfPCell blank5Cell=new PdfPCell(blank5);
		blank5Cell.setBorderWidthBottom(0);
		blank5Cell.setBorderWidthTop(0);
		blank5Cell.setColspan(3);
		
		Phrase blank6 =new Phrase(" ");
		PdfPCell blank6Cell=new PdfPCell(blank6);
		blank6Cell.setBorderWidthTop(0);
		blank6Cell.setColspan(3);
		
		
		
		complainDescTable.addCell(stateCell);
		complainDescTable.addCell(blank5Cell);
		
		complainDescTable.addCell(complainprCell);
		complainDescTable.addCell(complainValueCell);
		
		complainDescTable.addCell(blank5Cell);
		
		complainDescTable.addCell(typeOfServprCell);
		complainDescTable.addCell(typeOfServValueCell);
		complainDescTable.addCell(bkCell);
		
		complainDescTable.addCell(blank5Cell);
		
		complainDescTable.addCell(typeofproductprCell);
		complainDescTable.addCell(typeofproductValueCell);
		complainDescTable.addCell(bkCell);
		
		complainDescTable.addCell(blank6Cell);
		
		
		
		try {
			document.add(complainDescTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	private void customerDetails() {
		
		PdfPTable custDetailTable=new PdfPTable(5);
		custDetailTable.setWidthPercentage(100);
		try {
		custDetailTable.setWidths(new float[]{18,32,1,16,33});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		Phrase blank1 =new Phrase(" ");
		PdfPCell blank1Cell=new PdfPCell(blank1);
		blank1Cell.setBorder(0);
		blank1Cell.setPaddingTop(10);
		blank1Cell.setPaddingBottom(10);
		
		String custinfo="CUSTOMER INFORMATION";
		Phrase custinfophrase =new Phrase(custinfo,font12bold);
		PdfPCell custinfoCell=new PdfPCell(custinfophrase);
		custinfoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		custinfoCell.setColspan(5);
		
		
		Phrase blank4 =new Phrase(" ");
		PdfPCell blank4Cell=new PdfPCell(blank4);
//		blank4Cell.setBorder(0);
		blank4Cell.setBorderWidthBottom(0);
		blank4Cell.setBorderWidthTop(0);
		blank4Cell.setColspan(5);
		
		Phrase blankkk =new Phrase(" ");
		PdfPCell blankkkCell=new PdfPCell(blankkk);
		blankkkCell.setBorder(0);
		
		String custName="Customer Name : ";
		Phrase custNamepr =new Phrase(custName,font9bold);
		PdfPCell custNameCell=new PdfPCell(custNamepr);
		custNameCell.setBorderWidthBottom(0);
		custNameCell.setBorderWidthTop(0);
		custNameCell.setBorderWidthRight(0);
		
		String custNameValue="";
		if(complain.getPersoninfo().getFullName()!=null){
			custNameValue=complain.getPersoninfo().getFullName();
		}else{
			custNameValue="";
		}
		
		Phrase custNameValuepr =new Phrase(custNameValue,font10);
		PdfPCell custNameValueCell=new PdfPCell(custNameValuepr);
//		custNameValueCell.setBorder(0);
		
		
		
		
		String custPhon="Customer Phone : ";
		Phrase custPhonpr =new Phrase(custPhon,font9bold);
		PdfPCell custPhonprCell=new PdfPCell(custPhonpr);
		custPhonprCell.setBorder(0);
		
		String custPhonValue=" ";
		if(complain.getPersoninfo().getCellNumber()!=null){
			custPhonValue=complain.getPersoninfo().getCellNumber()+"";
		}else{
			custPhonValue="";
		}
		
		/**
		 * @author Vijay Date :- 19-10-2021
		 * Des :- Adding country code(from country master) in cell number.
		 */
		ServerAppUtility serverappUtility = new ServerAppUtility();
		String countryName = "";
		List<Country> countrylist = ofy().load().type(Country.class).filter("companyId", comp.getCompanyId()).list();
		
		if(complain.getServiceAddress()!=null){
			String address =complain.getServiceAddress()+"";
			String []addresss = address.split("\\,");
			for(int i=0;i<addresss.length;i++){
				for(Country country : countrylist){
					if(country.getCountryName().trim().equals(addresss[i].trim())){
						countryName = country.getCountryName().trim();
					}
				}
			}
		}
		custPhonValue = serverappUtility.getMobileNoWithCountryCode(custPhonValue, countryName, comp.getCompanyId());
	
		
		Phrase custPhonValuepr =new Phrase(custPhonValue,font10);
		PdfPCell custPhonValueCell=new PdfPCell(custPhonValuepr);
//		custPhonValueCell.setBorder(0);
		
		
		
		String compName="Customer Branch : ";
		Phrase compNamepr =new Phrase(compName,font9bold);
		PdfPCell compNameCell=new PdfPCell(compNamepr);
//		compNameCell.setBorder(0);
		compNameCell.setBorderWidthBottom(0);
		compNameCell.setBorderWidthTop(0);
		compNameCell.setBorderWidthRight(0);
		
		String compNameValue="";
		if(complain.getCustomerBranch()!=null){
			compNameValue=complain.getCustomerBranch()+"";
		}else{
			compNameValue="";
		}
		
		Phrase compNameValuepr =new Phrase(compNameValue,font10);
		PdfPCell compNameValueCell=new PdfPCell(compNameValuepr);
//		compNameValueCell.setBorder(0);
		
		String servadd="Service Address : ";
		Phrase servaddpr =new Phrase(servadd,font9bold);
		PdfPCell servaddCell=new PdfPCell(servaddpr);
		servaddCell.setBorder(0);
		
		String servaddValue="";
		if(complain.getServiceAddress()!=null){
			servaddValue=complain.getServiceAddress()+"";
		}else{
			servaddValue="";
		}
		Phrase servaddValuepr =new Phrase(servaddValue,font10);
		PdfPCell servaddValueCell=new PdfPCell(servaddValuepr);
//		servaddValueCell.setBorder(0);
		
		
		String contpersn="Contact Person Name : ";
		Phrase contpersnpr =new Phrase(contpersn,font9bold);
		PdfPCell contpersnCell=new PdfPCell(contpersnpr);
		contpersnCell.setBorderWidthBottom(0);
		contpersnCell.setBorderWidthTop(0);
		contpersnCell.setBorderWidthRight(0);
		
		String contpersnValue=" ";
		if(complain.getCallerName()!=null){
			contpersnValue=complain.getCallerName()+"";
		}else{
			contpersnValue="";
		}
		
		Phrase contpersnValuepr =new Phrase(contpersnValue,font10);
		PdfPCell contpersnValueCell=new PdfPCell(contpersnValuepr);
//		contpersnValueCell.setBorder(0);
		
		

		String contpersnNo="Contact Person No : ";
		Phrase contpersnNopr =new Phrase(contpersnNo,font9bold);
		PdfPCell contpersnNoCell=new PdfPCell(contpersnNopr);
		contpersnNoCell.setBorder(0);
		
		String contpersnNoValue="";
		if(complain.getCallerCellNo()!=0){
			contpersnNoValue=complain.getCallerCellNo()+"";
		}else{
			contpersnNoValue="";
		}
		if(contpersnNoValue!=null && !contpersnNoValue.equals(""))
		contpersnNoValue = serverappUtility.getMobileNoWithCountryCode(contpersnNoValue, countryName, comp.getCompanyId());

		Phrase contpersnNoValuepr =new Phrase(contpersnNoValue,font10);
		PdfPCell contpersnNoValueCell=new PdfPCell(contpersnNoValuepr);
//		contpersnNoValueCell.setBorder(0);
		
		custDetailTable.addCell(custinfoCell);
		custDetailTable.addCell(blank4Cell);
		
		
		custDetailTable.addCell(custNameCell);
		custDetailTable.addCell(custNameValueCell);
		custDetailTable.addCell(blankkkCell);
		custDetailTable.addCell(custPhonprCell);
		custDetailTable.addCell(custPhonValueCell);
		
		custDetailTable.addCell(blank4Cell);
		
		custDetailTable.addCell(compNameCell);
		custDetailTable.addCell(compNameValueCell);
		custDetailTable.addCell(blankkkCell);
		custDetailTable.addCell(servaddCell);
		custDetailTable.addCell(servaddValueCell);
		
		custDetailTable.addCell(blank4Cell);
		
		custDetailTable.addCell(contpersnCell);
		custDetailTable.addCell(contpersnValueCell);
		custDetailTable.addCell(blankkkCell);
		custDetailTable.addCell(contpersnNoCell);
		custDetailTable.addCell(contpersnNoValueCell);
		
		custDetailTable.addCell(blank4Cell);
		try {
			document.add(custDetailTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	private void ticketDetails() {
		PdfPTable ticketDetailTable=new PdfPTable(6);
		ticketDetailTable.setWidthPercentage(100);
		ticketDetailTable.setSpacingBefore(15);
		
		String custdetail="CUSTOMER COMPLAINT TICKET";
		Phrase custhead =new Phrase(custdetail,font12bold);
		PdfPCell custheadCell=new PdfPCell(custhead);
		custheadCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		custheadCell.setColspan(6);
		
		Phrase blank2 =new Phrase(" ");
		PdfPCell blank2Cell=new PdfPCell(blank2);
		blank2Cell.setBorderWidthTop(0);
		blank2Cell.setBorderWidthBottom(0);
		blank2Cell.setColspan(6);
		
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		
		String compDate="Date.: ";
		
		Phrase compDatePhrase =new Phrase(compDate,font9bold);
		PdfPCell compDateCell=new PdfPCell(compDatePhrase);
		compDateCell.setBorderWidthTop(0);
		compDateCell.setBorderWidthBottom(0);
		compDateCell.setBorderWidthRight(0);
		
		String compDateValue="";
		if(complain.getDate()!=null){
			compDateValue=fmt.format(complain.getDate());
		}else{
			compDateValue="";
		}
		Phrase compDateValuePhrase =new Phrase(compDateValue,font10);
		PdfPCell compDateValueCell=new PdfPCell(compDateValuePhrase);
		compDateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		compDateValueCell.setBorder(0);
		
        String ticketno="Ticket No.: ";
		
		Phrase ticketnoPhrase =new Phrase(ticketno,font9bold);
		PdfPCell ticketnoCell=new PdfPCell(ticketnoPhrase);
		ticketnoCell.setBorder(0);
		
		String ticketnoValue="";
		if(complain.getCount()!=0){
			ticketnoValue=complain.getCount()+"";
		}else{
			ticketnoValue="";
		}
		Phrase ticketnoValuePhrase =new Phrase(ticketnoValue,font10);
		PdfPCell ticketnoValueCell=new PdfPCell(ticketnoValuePhrase);
//		ticketnoValueCell.setBorder(0);
		
		
//		ticketDetailTable.addCell(blank2Cell);
		
		ticketDetailTable.addCell(custheadCell);
		ticketDetailTable.addCell(blank2Cell);
		
		ticketDetailTable.addCell(compDateCell);
		ticketDetailTable.addCell(compDateValueCell);
		ticketDetailTable.addCell(blankCell);
		ticketDetailTable.addCell(blankCell);
		ticketDetailTable.addCell(ticketnoCell);
		ticketDetailTable.addCell(ticketnoValueCell);
		
		ticketDetailTable.addCell(blank2Cell);
		
		
		try {
			document.add(ticketDetailTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	private void companyDetails() {
		PdfPTable compDetailable=new PdfPTable(3);
		compDetailable.setWidthPercentage(100);
		try {
			compDetailable.setWidths(new float[]{20,70,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setSpacingAfter(5);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{8,92});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		DocumentUpload logodocument ;
		if(comp.getLogo()!=null){
			logodocument =comp.getLogo();
		}else{
			logodocument =null;
		}
		
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setImage(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if(imageSignCell != null)
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		}
		else
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}
		
		PdfPTable compNametable=new PdfPTable(1);
		compNametable.setWidthPercentage(100);
		compNametable.setSpacingAfter(10);
		
		//Set Company Name
		String businessunit=null;
		if(comp.getBusinessUnitName()!=null) {
			businessunit=comp.getBusinessUnitName();
		} else {
			businessunit="";
		}
		
		Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
				font14bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		compNametable.addCell(companyNameCell);
		
		
		PdfPCell logocell=new PdfPCell(logoTable);
		logocell.setBorder(0);
		
		PdfPCell compNamecell=new PdfPCell(compNametable);
		compNamecell.setBorder(0);
		
		Phrase rightph = new Phrase("",font14bold);
		PdfPCell rightcell=new PdfPCell(rightph);
		rightcell.setBorder(0);
		
		compDetailable.addCell(logocell);
		compDetailable.addCell(compNamecell);
		compDetailable.addCell(rightcell);
		
		try {
			document.add(compDetailable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
