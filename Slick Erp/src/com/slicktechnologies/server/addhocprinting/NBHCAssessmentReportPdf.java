package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.AssesmentReport;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class NBHCAssessmentReportPdf {

	public Document document;
	AssesmentReport report;
	Company comp;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;

	float[] tblcol1width = { 1.2f, 7.5f };
	float[] tblcol7width = { 1.5f,1.5f,2f,2f,2.5f,3.5f,3.5f };
	
	public NBHCAssessmentReportPdf(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}
	
	public void setAssessmentDetails(Long count) {
		
		report=ofy().load().type(AssesmentReport.class).id(count).now();
		
		if (report.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", report.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preprintStatus) {
		
		if(preprintStatus.equals("yes")){
			
			System.out.println("inside prit yes");
			createBlankforUPC();
		}
		
		if(preprintStatus.equals("no")){
		System.out.println("inside prit no");
		
		if(comp.getUploadHeader()!=null){
		createCompanyNameAsHeader(document,comp);
		}
		
		if(comp.getUploadFooter()!=null){
		createCompanyNameAsFooter(document,comp);
		}
		createBlankforUPC();
		}
		
		
		createCustomerDetails();
		
		createProductDetails();
		createAssessByInformation();
		createAccompaniedByInformation();

	}
	
	

	private void createProductDetails() {
		
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(tblcol7width);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		float[] tblcol6width = { 1.5f,1.5f,2f,2f,2.5f,7f };
		PdfPTable blankTable = new PdfPTable(6);
		blankTable.setWidthPercentage(100f);
		try {
			blankTable.setWidths(tblcol6width);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int i = 0; i < 5; i++) {
			
		Phrase actionPlane = new Phrase(" ",font10bold); 
		PdfPCell actionPlaneCell = new PdfPCell(actionPlane);
		actionPlaneCell.setBorderWidthBottom(0);
		actionPlaneCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankTable.addCell(actionPlaneCell);
		}
		
		Phrase actionPlane = new Phrase("Action Plan",font10bold); 
		PdfPCell actionPlaneCell = new PdfPCell(actionPlane);
		actionPlaneCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankTable.addCell(actionPlaneCell);
		
		
		Phrase area = new Phrase("Area",font10bold); 
		PdfPCell areaCell = new PdfPCell(area);
		areaCell.setBorderWidthTop(0);
		areaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(areaCell);
		
		Phrase location = new Phrase("Location",font10bold);
		PdfPCell locationCell = new PdfPCell(location);
		locationCell.setBorderWidthTop(0);
		locationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(locationCell);
		
		Phrase category = new Phrase("Category",font10bold);
		PdfPCell categoryCell = new PdfPCell(category);
		categoryCell.setBorderWidthTop(0);
		categoryCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(categoryCell);
		
		Phrase deficiency = new Phrase("Deficiency",font10bold);
		PdfPCell deficencyCell = new PdfPCell(deficiency);
		deficencyCell.setBorderWidthTop(0);
		deficencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(deficencyCell);
		
		Phrase consequences = new Phrase("Likely Consequences",font10bold);
		PdfPCell consequencesCell = new PdfPCell(consequences);
		consequencesCell.setBorderWidthTop(0);
		consequencesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(consequencesCell);
		
		Phrase company = new Phrase("By Company",font10bold);
		PdfPCell comanyCell = new PdfPCell(company);
		comanyCell.setBorderWidthTop(0);
		comanyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(comanyCell);
		
		Phrase customer = new Phrase("Recommendation to Customer",font10bold);
		PdfPCell customerCell = new PdfPCell(customer);
		customerCell.setBorderWidthTop(0);
		customerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(customerCell);
		
		Phrase areaValue = null;
		Phrase locationValue = null;
		Phrase categoryValue = null;
		Phrase deficiencyValue = null;
		Phrase consequencesValue = null;
		Phrase companyValue = null;
		Phrase customerValue = null;
		
		for (int i = 0; i < report.getAssessmentDetailsLIst().size(); i++) 
		{
			
			if(report.getAssessmentDetailsLIst().get(i).getArea()!= null)
			{
				areaValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getArea(),font10);
			}
			else
			{
				areaValue = new Phrase(" ",font10);
			}
			
			PdfPCell areaValueCell = new PdfPCell(areaValue);
			table.addCell(areaValueCell);
			
			
			if(report.getAssessmentDetailsLIst().get(i).getLocation()!= null)
			{
				locationValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getLocation(),font10);
			}
			else
			{
				locationValue = new Phrase(" ",font10);
			}
			
			PdfPCell locationValueCell = new PdfPCell(locationValue);
			table.addCell(locationValueCell);
			
			if(report.getAssessmentDetailsLIst().get(i).getCategory()!= null)
			{
				categoryValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getCategory(),font10);
			}
			else
			{
				categoryValue = new Phrase(" ",font10);
			}
			
			PdfPCell categoryValueCell = new PdfPCell(categoryValue);
			table.addCell(categoryValueCell);
			
			
			if(report.getAssessmentDetailsLIst().get(i).getDeficiencyType()!= null)
			{
				deficiencyValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getDeficiencyType(),font10);
			}
			else
			{
				deficiencyValue = new Phrase(" ",font10);
			}
			
			PdfPCell deficiencyValueCell = new PdfPCell(deficiencyValue);
			table.addCell(deficiencyValueCell);
			
			
			
			if(report.getAssessmentDetailsLIst().get(i).getConsequences()!= null)
			{
				consequencesValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getConsequences(),font10);
			}
			else
			{
				consequencesValue = new Phrase(" ",font10);
			}
			
			PdfPCell consequencesValueCell = new PdfPCell(consequencesValue);
			table.addCell(consequencesValueCell);
			
			
			
			if(report.getAssessmentDetailsLIst().get(i).getActionPlanForCompany()!= null)
			{
				companyValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getActionPlanForCompany(),font10);
			}
			else
			{
				companyValue = new Phrase(" ",font10);
			}
			
			PdfPCell companyValueCell = new PdfPCell(companyValue);
			table.addCell(companyValueCell);
			
			if(report.getAssessmentDetailsLIst().get(i).getActionPlanForCustomer()!= null)
			{
				customerValue = new Phrase(report.getAssessmentDetailsLIst().get(i).getActionPlanForCustomer(),font10);
			}
			else
			{
				customerValue = new Phrase(" ",font10);
			}
			
			PdfPCell customerValueCell = new PdfPCell(customerValue);
			table.addCell(customerValueCell);
			
			
		}
		
		Phrase blank = null;
		PdfPCell blankcell = null;
		int blankLine = 6 - report.getAssessmentDetailsLIst().size();
		System.out.println("rohan 123" + blankLine);
		if (blankLine > 0) {
			for (int j = 0; j < blankLine; j++) {
				for (int i = 0; i < 17; i++) {
					blank = new Phrase(" ", font8);
					blankcell = new PdfPCell(blank);
					table.addCell(blankcell);
				}
			}
		}
		
		
		try {
			document.add(blankTable);
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createAccompaniedByInformation() {
		
		String accompaniedBy1="";
	
		
		if(report.getAccompainedByPerson1()!=null && !report.getAccompainedByPerson1().equals(""))
		{
			accompaniedBy1 ="1. "+ report.getAccompainedByPerson1();
			
			if(report.getAccompainedByPerson2()!=null && !report.getAccompainedByPerson2().equals(""))
			{
				accompaniedBy1 =accompaniedBy1+"          "+"2. "+ report.getAccompainedByPerson2();
				
				if(report.getAccompainedByPerson3()!=null && !report.getAccompainedByPerson3().equals(""))
				{
					accompaniedBy1 =accompaniedBy1+"          "+"3. "+ report.getAccompainedByPerson3();
				}
			}
			else
			{
				if(report.getAccompainedByPerson3()!=null && !report.getAccompainedByPerson3().equals(""))
				{
					accompaniedBy1 =accompaniedBy1+"          "+"2. "+ report.getAccompainedByPerson3();
				}
			}
		}
		else
		{
			if(report.getAccompainedByPerson2()!=null && !report.getAccompainedByPerson2().equals(""))
			{
				accompaniedBy1 =accompaniedBy1+"          "+"1. "+ report.getAccompainedByPerson2();
				
				if(report.getAccompainedByPerson3()!=null && !report.getAccompainedByPerson3().equals(""))
				{
					accompaniedBy1 =accompaniedBy1+"          "+"2. "+ report.getAccompainedByPerson3();
				}
			}
			else
			{
				if(report.getAccompainedByPerson3()!=null && !report.getAccompainedByPerson3().equals(""))
				{
					accompaniedBy1 =accompaniedBy1+"          "+"1. "+ report.getAccompainedByPerson3();
				}
			}
		}
		
		
		
		PdfPTable table= new PdfPTable(2);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);
		try {
			table.setWidths(tblcol1width);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase accompaniedByph = new Phrase("Accompanied By : ",font10bold);
		PdfPCell accompaniedByCell = new PdfPCell(accompaniedByph);
		accompaniedByCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		accompaniedByCell.setBorder(0);
		table.addCell(accompaniedByCell);
		
		Phrase person1Phrase = new Phrase(accompaniedBy1,font10);
		PdfPCell person1Cell = new PdfPCell(person1Phrase);
		person1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		person1Cell.setBorder(0);
		table.addCell(person1Cell);
		
		
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createAssessByInformation() {
		
		String person1="";
	
		if(report.getAssessedPeron1()!=null && !report.getAssessedPeron1().equals(""))
		{
			person1 ="1. "+ report.getAssessedPeron1();
			
			if(report.getAssessedPeron2()!=null && !report.getAssessedPeron2().equals(""))
			{
				person1 =person1+"          "+"2. "+ report.getAssessedPeron2();
				
				if(report.getAssessedPeron3()!=null && !report.getAssessedPeron3().equals(""))
				{
					person1 =person1+"          "+"3. "+ report.getAssessedPeron3();
				}
			}
			else
			{
				if(report.getAssessedPeron3()!=null && !report.getAssessedPeron3().equals(""))
				{
					person1 =person1+"          "+"2. "+ report.getAssessedPeron3();
				}
			}
		}
		else
		{
			if(report.getAssessedPeron2()!=null && !report.getAssessedPeron2().equals(""))
			{
				person1 =person1+"          "+"1. "+ report.getAssessedPeron2();
				
				if(report.getAssessedPeron3()!=null && !report.getAssessedPeron3().equals(""))
				{
					person1 =person1+"          "+"2. "+ report.getAssessedPeron3();
				}
			}
			else
			{
				if(report.getAssessedPeron3()!=null && !report.getAssessedPeron3().equals(""))
				{
					person1 =person1+"          "+"1. "+ report.getAssessedPeron3();
				}
			}
		}
		
		
		
		
		
		
		
		
		
		PdfPTable table= new PdfPTable(2);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(20f);
		try {
			table.setWidths(tblcol1width);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase assessBy = new Phrase("Assessed By     :",font10bold);
		PdfPCell assessByCell = new PdfPCell(assessBy);
		assessByCell.setBorder(0);
		table.addCell(assessByCell);
		
		
		
		
		Phrase person1Phrase = new Phrase(person1,font10);
		PdfPCell person1Cell = new PdfPCell(person1Phrase);
		person1Cell.setBorder(0);
		table.addCell(person1Cell);
		
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createCustomerDetails() {
		
		
		Phrase heading = new Phrase("Pest Management Assessment Report",font12bold);
		Paragraph headingPara = new Paragraph(heading);
		headingPara.setAlignment(Element.ALIGN_CENTER);
		
		float[] relative4Widths = { 0.8f,3.7f,3.7f,1.5f };
		
		PdfPTable custNameTAble = new PdfPTable(4);
		custNameTAble.setWidthPercentage(100f);
		try {
			custNameTAble.setWidths(relative4Widths);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase customerName = new Phrase("Customer :",font10bold);
		PdfPCell customerNameCell=new PdfPCell(customerName);
		customerNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNameCell.setBorder(0);
		custNameTAble.addCell(customerNameCell);
		
		Phrase customerNameValue = new Phrase(report.getCinfo().getFullName(),font10);
		PdfPCell customerNameValueCell=new PdfPCell(customerNameValue);
		customerNameValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNameValueCell.setBorder(0);
		custNameTAble.addCell(customerNameValueCell);
		
		Phrase dateOfAssessment = new Phrase("Date of Assessment :",font10bold);
		PdfPCell dateOfAssessmentCell=new PdfPCell(dateOfAssessment);
		dateOfAssessmentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateOfAssessmentCell.setBorder(0);
		custNameTAble.addCell(dateOfAssessmentCell);
//		
//		Phrase dateOfAssessmentValue = new Phrase("dae",font10);
		Phrase dateOfAssessmentValue = new Phrase(fmt.format(report.getAssessmentDate()),font10);
		PdfPCell dateOfAssessmentValueCell=new PdfPCell(dateOfAssessmentValue);
		dateOfAssessmentValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateOfAssessmentValueCell.setBorder(0);
		custNameTAble.addCell(dateOfAssessmentValueCell);
		
		PdfPCell custNameTableCell = new PdfPCell(custNameTAble);
		custNameTableCell.setBorder(0);
		
		
		float[] tblcwidth = { 0.8f, 9.0f };
		PdfPTable addressTable= new PdfPTable(2);
		addressTable.setWidthPercentage(100f);
		try {
			addressTable.setWidths(tblcwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
/** date 12.02.2018 commented by komal 	as per nbhc requirement **/	
//		Phrase address = new Phrase("Address    :",font10bold);
//		PdfPCell addressCell=new PdfPCell(address);
//		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addressCell.setBorder(0);
//		addressTable.addCell(addressCell);
//		
//		Phrase addressValue = new Phrase(report.getNewcustomerAddress().getCompleteAddress(),font10);
//		PdfPCell addressValueCell=new PdfPCell(addressValue);
//		addressValueCell.setBorder(0);
//		addressValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addressTable.addCell(addressValueCell);
//		
//		PdfPCell addressTableCell = new PdfPCell(addressTable);
//		addressTableCell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.addCell(custNameTableCell);
	//	table.addCell(addressTableCell);
		
		try {
			document.add(headingPara);
			document.add(Chunk.NEWLINE);
			document.add(table);
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		 blank.add(Chunk.NEWLINE);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


private void createCompanyNameAsHeader(Document doc, Company comp) {
	
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(755f);
		image2.setAbsolutePosition(40f,500f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(755f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/NBHC_Footer.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(755f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}
	/**
	 * @author Vijay Chougule
	 * Des :- NBHC CCPM to send an email
	 */
   	public void createPDFForEmail(long count){
   		setAssessmentDetails(count);
   		createPdf("no");
   	}

}
