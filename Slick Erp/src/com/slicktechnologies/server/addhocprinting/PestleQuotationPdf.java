package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class PestleQuotationPdf {

	public Document document;
	Quotation quot;
	Customer cust;
	Company comp;
	Employee employee;
	Lead lead;
	private  Font font10underline,font16boldul,font12bold,font8bold,font8,font9bold,font12boldul,font12,font10bold,font10,font14bold,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
	
	DecimalFormat df=new DecimalFormat("0.00");
	float[] colwidthfortable = { 0.5f, 2.0f, 2.0f, 1.5f, 2.5f, 1.5f };
	float[] colwidth = { 1.5f, 0.1f, 6.5f, 1.5f, 0.1f, 1.5f };
	float[] colwidthforsubtable = { 1.0f, 0.1f, 7.0f};
	float[] colwidthforempdetails={ 1.0f, 0.1f, 7.0f};
	float[] colwidthfortreatment={ 1.4f, 0.1f, 7.0f};
	float[] colwidthforpayterms={ 1.8f, 0.1f, 6.5f};
	
	Phrase chunk;
	PdfPCell pdfno, pdftreatment, pdfpest, pdffrequency, pdfpremise, pdfprice;
	PdfPCell  pdfsubttl,pdfttlamt;
	public PestleQuotationPdf() {
		// TODO Auto-generated constructor stub
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font9= new Font(Font.FontFamily.HELVETICA,9);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10bold= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD);
		font10underline= new Font(Font.FontFamily.HELVETICA,10,Font.UNDERLINE);
		
		font10=new Font(Font.FontFamily.HELVETICA,10,Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font9bold= new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
	}
	
	public void getPestleQuotation(Long count) {
		// TODO Auto-generated method stub

		
		System.out.println("in side setdirectquotation method");
		//Quoatation Loaded
		quot = ofy().load().type(Quotation.class).id(count).now();
		
		
		if (quot.getCompanyId() != null){
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId())
					.filter("companyId", quot.getCompanyId()).first().now();
			
			lead=ofy().load().type(Lead.class).filter("companyId", quot.getCompanyId()).filter("count", quot.getLeadCount()).first().now();
			
		}
			
		
		if (quot.getCompanyId() == null){
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId()).first().now();
			
			lead=ofy().load().type(Lead.class).filter("count", quot.getLeadCount()).first().now();
		}
			

		if (quot.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", quot.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		
		if (quot.getCompanyId() != null)
			employee = ofy().load().type(Employee.class)
					.filter("companyId", quot.getCompanyId()).filter("fullname", quot.getEmployee()).first().now();
		else
			employee = ofy().load().type(Employee.class).filter("fullname", quot.getEmployee()).first().now();
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	
	}
	

	public void createPdf(String printStatus) {
		// TODO Auto-generated method stub

		if(printStatus.contains("yes"))
		{
			System.out.println("in side yes condition");
			createLogo(document,comp);
			createSpcingForHeading();
		}
		else
		{
			System.out.println("in side no condition");
			createSpcingForHeading();
		    if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
			}
						
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
		}
		createTitleHeading();
		createReferenceHeading();
		createCustDetails();
		createresponse();
		createtreatmentdetails();
		createquotationtable();
		createassuranceandvalidity();
		createpaymentterms();
		createstaticpart();
		createEmployeeDetails();
	}

	

private void createSpcingForHeading() {
		
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
			document.add(spacingPara);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

private void createLogo(Document doc, Company comp) {

	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
		}
	}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}
	





	private void createTitleHeading() {
		// TODO Auto-generated method stub

		
		System.out.println("inside create tilte heading");
		
		String title = "Quotation";
		Phrase quotation = new Phrase(title,font16boldul);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase blk = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blk);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);

		try {
			document.add(qtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	
	}

	private void createReferenceHeading() {
		// TODO Auto-generated method stub

		
		Phrase quotid= new Phrase("Quotation Id ",font10bold);
		PdfPCell quotidstatic = new PdfPCell(quotid);
		quotidstatic.setBorder(0);
		quotidstatic.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase quotiddynamic = null;
		
		if( quot.getCount()!=0){
			quotiddynamic = new Phrase(quot.getCount()+"",font10bold);	
		}
		
		PdfPCell quotiddynamiccell = new PdfPCell(quotiddynamic);
//		quotiddynamiccell.addElement(quotiddynamic);
		quotiddynamiccell.setBorder(0);
		quotiddynamiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":",font10bold);
		PdfPCell colcell = new PdfPCell(colon);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date",font10bold);
		PdfPCell datecellstatic = new PdfPCell(date);
		datecellstatic.setBorder(0);
		datecellstatic.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase dt = null;
		if(quot.getQuotationDate() !=null){
			dt = new Phrase(fmt.format(quot.getQuotationDate()),font10bold);
		}
		System.out.println("Quotation Date :"+fmt.format(quot.getQuotationDate()));
		
		PdfPCell dtcelldynamic = new PdfPCell(dt);
		dtcelldynamic.setBorder(0);
		dtcelldynamic.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase blank = new Phrase(" ");
//		PdfPCell blankcell = new PdfPCell(blank); 
//		blankcell.setBorder(0);
//		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable rdttable = new PdfPTable(6);
		rdttable.setWidthPercentage(100f);

		try {
			 rdttable.setWidths(colwidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		rdttable.addCell(quotidstatic);
		rdttable.addCell(colcell);
		rdttable.addCell(quotiddynamiccell);
		rdttable.addCell(datecellstatic);
		rdttable.addCell(colcell);
		rdttable.addCell(dtcelldynamic);
//		rdttable.addCell(blankcell);
		
		
		PdfPCell cell3 = new PdfPCell(rdttable); 		
		cell3.setBorder(0);
		
		PdfPTable table2 = new PdfPTable(1);
		table2.addCell(cell3);
		table2.setWidthPercentage(100);
		
		try {
			document.add(table2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}
	
	private void createCustDetails() {
		// TODO Auto-generated method stub
		
		
//		Phrase to=new Phrase("To,",font10bold);
//		PdfPCell tocell=new PdfPCell(to);
//		tocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		tocell.setBorder(0);
		
		String refname="";
		
		String custAdd1="";
		String custFullAdd1="";
		
		boolean company=cust.isCompany();
		System.out.println("COmpany :"+company);
		
		if(cust.isCompany()){
			refname=cust.getCompanyName().trim();
		}else{
			refname=cust.getFullname().trim();
		}
//		Phrase custname=new Phrase(refname,font10bold);
//		PdfPCell custnamecell=new PdfPCell(custname);
//		custnamecell.setBorder(0);
		
		
		if(cust.getSecondaryAdress()!=null){
			
	if(cust.getSecondaryAdress().getAddrLine2()!=null){
				
				if(cust.getSecondaryAdress().getLandmark()!=null){
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2()+"\n"+cust.getSecondaryAdress().getLandmark();
				}else{
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getAddrLine2();
				}
			}else{
				if(cust.getSecondaryAdress().getLandmark()!=null){
					custAdd1=cust.getSecondaryAdress().getAddrLine1()+", "+cust.getSecondaryAdress().getLandmark();
				}else{
					custAdd1=cust.getSecondaryAdress().getAddrLine1();
				}
			}
			
			if(cust.getSecondaryAdress().getLocality()!=null){
				custFullAdd1=custAdd1+", "+cust.getSecondaryAdress().getLocality()+"\n"+cust.getSecondaryAdress().getCity()+" "+cust.getSecondaryAdress().getPin()+", "+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getSecondaryAdress().getCity()+" "+cust.getSecondaryAdress().getPin()+", "+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry();
			}
		
		}
		
		Phrase custAddInfo = new Phrase("To,"+"\n"+refname+"\n"+custFullAdd1, font10bold);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		PdfPTable custTable=new PdfPTable(1);
		custTable.addCell(newline);
//		custTable.addCell(tocell);
//		custTable.addCell(custnamecell);
		custTable.addCell(custAddInfoCell);
//		custTable.setSpacingAfter(10);
		custTable.setWidthPercentage(100);
		
		
		try {
			document.add(custTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase POC=new Phrase("Kind Attn",font10bold);
		PdfPCell POCcell=new PdfPCell(POC);
		POCcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		POCcell.setBorder(0);
		
		Phrase POCDynamic=new Phrase(cust.getFullname(),font10bold);
		PdfPCell POCDynamicCell=new PdfPCell(POCDynamic);
		POCDynamicCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		POCDynamicCell.setBorder(0);
		
		Phrase subject=new Phrase("Subject",font10bold);
		PdfPCell subjectCell=new PdfPCell(subject);
		subjectCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subjectCell.setBorder(0);
		
		String samplestring=null;
		
		for(int i=0;i<quot.getItems().size();i++){
			
			if(i==0){
				samplestring=quot.getItems().get(i).getProductName();
			}else{
				
				if((i+1)!=(quot.getItems().size())){
					samplestring=samplestring+","+quot.getItems().get(i).getProductName();
				}else{
					samplestring=samplestring+" & "+quot.getItems().get(i).getProductName();
				}
				
			}
			
			
		}
		
		Phrase subjectDynamic=new Phrase(samplestring+" for your Premises at above Address",font10bold);
		PdfPCell subjectDynamiccell=new PdfPCell(subjectDynamic);
		subjectDynamiccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subjectDynamiccell.setBorder(0);
		
		Phrase colon=new Phrase(":");
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		PdfPTable subtable=new PdfPTable(3);
		subtable.setSpacingBefore(3f);
		if(cust.isCompany()){
		subtable.addCell(POCcell);
		subtable.addCell(coloncell);
		subtable.addCell(POCDynamicCell);
		}
		subtable.addCell(subjectCell);
		subtable.addCell(coloncell);
		subtable.addCell(subjectDynamiccell);
		subtable.setWidthPercentage(100);
		try {
			subtable.setWidths(colwidthforsubtable);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(subtable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createresponse() {
		// TODO Auto-generated method stub
		
		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		String response = null;
		
		System.out.println("Lead : "+null);
		if(lead!=null){
			if(lead.getCreationDate()!=null && quot.getInspectionDate()!=null){
				response="In response to your enquiry dated "+fmt.format(lead.getCreationDate())+" undersigned has carried out inspection of your premises on "+fmt.format(quot.getInspectionDate())+" to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";	
			}else{
				if(quot.getInspectionDate()!=null){
					response="In response to your enquiry, undersigned has carried out inspection of your premises on "+fmt.format(quot.getInspectionDate())+" to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";	
				}
				else
				{
					response="In response to your enquiry, undersigned has carried out inspection of your premises to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";
				}
				}
		}else{
			if(quot.getInspectionDate()!=null){
				response="In response to your enquiry, undersigned has carried out inspection of your premises on "+fmt.format(quot.getInspectionDate())+" to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";	
			}
			else
			{
				response="In response to your enquiry, undersigned has carried out inspection of your premises to identify and assess type of pests and severity of infestation. We would like to make the following recommendations.";
			}
		}
		
		
		
		Phrase responsephrase=new Phrase(response,font10);
		PdfPCell respnsecell=new PdfPCell(responsephrase);
		respnsecell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		respnsecell.setBorder(0);
		
		PdfPTable responsetable=new PdfPTable(1);
		responsetable.setSpacingBefore(5f);
		responsetable.setWidthPercentage(100);
		responsetable.addCell(respnsecell);
//		responsetable.addCell(newline);
		try {
			document.add(responsetable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void createtreatmentdetails() {
		// TODO Auto-generated method stub
		
		
		Phrase colon=new Phrase(":");
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		Phrase controlmeasure=new Phrase("Control Measure",font10bold);
		PdfPCell cntmeasurecell=new PdfPCell(controlmeasure);
		cntmeasurecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cntmeasurecell.setBorder(0);
		String samplestring=null;
		
		for(int i=0;i<quot.getItems().size();i++){
			
			if(i==0){
				samplestring=quot.getItems().get(i).getProductName();
			}else{
				
				if((i+1)!=(quot.getItems().size())){
					samplestring=samplestring+","+quot.getItems().get(i).getProductName();
				}else{
					samplestring=samplestring+" & "+quot.getItems().get(i).getProductName();
				}
				
			}
			
			
		}
		System.out.println("Sample String : "+samplestring);
		String cntstring="We recommend our "+samplestring+" as per enclosed annexure.";
		
		Phrase cntphrase=new Phrase(cntstring,font10);
		PdfPCell cntphrasecell=new PdfPCell(cntphrase);
		cntphrasecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cntphrasecell.setBorder(0);
		
		Phrase freqphrase=new Phrase("Frequency",font10bold);
		PdfPCell freqcell=new PdfPCell(freqphrase);
		freqcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqcell.setBorder(0);
		
		Phrase freqdyphrase=new Phrase(" We advise that our services to be avail as per frequency below.",font10);
		PdfPCell freqdycell=new PdfPCell(freqdyphrase);
		freqdycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqdycell.setBorder(0);
		
		PdfPTable treatmenttable=new PdfPTable(3);
		
		treatmenttable.addCell(cntmeasurecell);
		treatmenttable.addCell(coloncell);
		treatmenttable.addCell(cntphrasecell);	
		treatmenttable.addCell(freqcell);
		treatmenttable.addCell(coloncell);
		treatmenttable.addCell(freqdycell);
		treatmenttable.setWidthPercentage(100);
		try {
			treatmenttable.setWidths(colwidthfortreatment);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			document.add(treatmenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createquotationtable() {
		// TODO Auto-generated method stub

		Phrase chunk;
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);

		try {
			table.setWidths(colwidthfortable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase no = new Phrase("No.",font10bold);
		Phrase treatment = new Phrase("Treatment", font10bold);
		Phrase pest = new Phrase("Pest Covered",font10bold);
		Phrase frequency = new Phrase("Frequency of Services", font10bold);
		Phrase premise = new Phrase("Area to be Treated", font10bold);
		Phrase price = new Phrase("Charges(Rs).", font10bold);
		
		
		PdfPCell nocell = new PdfPCell(no);
		nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell  treatcell = new PdfPCell( treatment);
		treatcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell pestcell = new PdfPCell(pest);
		pestcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell precell = new PdfPCell(premise);
		precell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell freqcell = new PdfPCell(frequency);
		freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(nocell);
		table.addCell(treatcell);
		table.addCell(pestcell);
		table.addCell(freqcell);
		table.addCell(precell);
		table.addCell(pricecell);

		
		for(int i=0; i<this.quot.getItems().size(); i++){
			
		chunk = new Phrase(i+1 +"",font10);	
		pdfno = new PdfPCell(chunk);
		pdfno.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(quot.getItems().get(i).getProductName()+"",font10);
		System.out.println("chunk 1: "+quot.getItems().get(i).getProductName());
		pdftreatment = new PdfPCell(chunk);
		pdftreatment.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(quot.getItems().get(i).getComment()+"",font10);
		System.out.println("chunk 2 :"+quot.getItems().get(i).getComment());
		pdfpest = new PdfPCell(chunk);
		pdfpest.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(quot.getItems().get(i).getNumberOfServices()+"",font10);	
		System.out.println("chunk 3 :"+quot.getItems().get(i).getNumberOfServices());
		pdffrequency = new PdfPCell(chunk);
		pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(quot.getItems().get(i).getPremisesDetails(),font10);
		pdfpremise = new PdfPCell(chunk);
		pdfpremise.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk =  new Phrase((quot.getItems().get(i).getPrice()*quot.getItems().get(i).getQty())+"/-",font10);
		System.out.println("chunk 4 :"+quot.getItems().get(i).getPrice());
		pdfprice = new PdfPCell(chunk);
		pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(pdfno);
		table.addCell(pdftreatment);
		table.addCell(pdfpest);
		table.addCell(pdffrequency);
		table.addCell(pdfpremise);
		table.addCell(pdfprice);
		
	}
		
		Phrase blank=new Phrase(" ",font10);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
//		blankCell.setBorder(0);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);
		blankCell.setBorderWidthLeft(0);
		
		
		Phrase blank1=new Phrase(" ",font10);
		PdfPCell blank1Cell = new PdfPCell();
		blank1Cell.addElement(blank1);
//		blankCell.setBorder(0);
		blank1Cell.setBorderWidthRight(0);
		blank1Cell.setBorderWidthTop(0);
		
		
		
		double subTotal = 0;
		for(int i=0; i<this.quot.getItems().size(); i++){
			
			subTotal = subTotal +(this.quot.getItems().get(i).getPrice()*this.quot.getItems().get(i).getQty());
			
		}
		
		
		
		String name=null;
		double servalue=0;
		for(int i=0;i<quot.getProductTaxes().size();i++){
		if(!quot.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("vat")){
		name = quot.getProductTaxes().get(i).getChargeName();
		servalue =quot.getProductTaxes().get(i).getChargePercent();
		System.out.println("charge %"+servalue);
		System.out.println("charge name "+name);
		}
		}
		
		Phrase stax = new Phrase(name+"@"+df.format(servalue)+"%",font10);
		PdfPCell staxcell = new PdfPCell(stax);
		staxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		staxcell.setBorderWidthLeft(0);
		staxcell.setBorderWidthTop(0);
		
		
//		String name = dir.getItems().get(0).getServiceTax().getTaxName();
//		double servalue =dir.getItems().get(0).getServiceTax().getPercentage();
		Phrase st = new Phrase("Sub Total",font10);
		PdfPCell stcell = new PdfPCell(st);
		stcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		stcell.setBorderWidthLeft(0);
		stcell.setBorderWidthTop(0);
		
		
		Phrase st1 = new Phrase(subTotal+"/-",font10);
		PdfPCell st1cell = new PdfPCell(st1);
		st1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase tamt = new Phrase("Total Amount",font10bold);
		PdfPCell tamtcell = new PdfPCell(tamt);
		tamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		tamtcell.setBorderWidthLeft(0);
		tamtcell.setBorderWidthTop(0);

		table.addCell(blank1Cell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(stcell);
		table.addCell(st1cell);
		
			PdfPCell pdftaxpercent=null;
			if((quot.getItems().get(0).getServiceTax().getPercentage() !=0)){

				double taxvalue = subTotal*(quot.getItems().get(0).getServiceTax().getPercentage()/100);	
					
				chunk = new Phrase(taxvalue+"/-",font10);
				pdftaxpercent = new PdfPCell(chunk);
				pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			}
			else
			{
				chunk = new Phrase(" ",font10);
				pdftaxpercent = new PdfPCell(chunk);
				pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			
			
			
			table.addCell(blank1Cell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(staxcell);
			table.addCell(pdftaxpercent);
			
		
			chunk = new Phrase(quot.getNetpayable()+"/-",font10);
			pdfttlamt = new PdfPCell(chunk);
			pdfttlamt.setHorizontalAlignment(Element.ALIGN_CENTER);

			String amountInWord=ServiceInvoicePdf.convert(quot.getNetpayable());
			Phrase inwords = new Phrase("(In words) Rs. "+amountInWord+" Only.",font8);
			PdfPCell inwordscell = new PdfPCell(inwords);
			inwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			table.addCell(blank1Cell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(tamtcell);
			table.addCell(pdfttlamt);
			
			try {
				document.add(table);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
//		PdfPCell itemcell = new PdfPCell();
//		itemcell.addElement(table);
//		itemcell.setBorder(0);
//		
		PdfPTable parent = new PdfPTable(1);
//		parent.addCell(itemcell);
		parent.addCell(inwordscell);
		parent.setSpacingAfter(10f);
		parent.setWidthPercentage(100);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}

	private void createassuranceandvalidity() {
		// TODO Auto-generated method stub

		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		Phrase colon=new Phrase(":");
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		

		Phrase freqofservice=new Phrase("Frequency of Services",font10bold);
		PdfPCell freqofservicecell=new PdfPCell(freqofservice);
		freqofservicecell.setBorder(0);
		freqofservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase freqofservicedy = null;
		if(quot.getFreqService()!=null){
			freqofservicedy=new Phrase(quot.getFreqService(),font10);
		}
		PdfPCell freqofservicedycell=new PdfPCell(freqofservicedy);
		freqofservicedycell.setBorder(0);
		freqofservicedycell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		Phrase ppcass=new Phrase("PPCS Assurance",font10bold);
		PdfPCell ppcasscell=new PdfPCell(ppcass);
		ppcasscell.setBorder(0);
		ppcasscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase ppcassrest=new Phrase("In case of resurgence of pests under contract, interim calls if any also would be attended to without extra cost.",font10);
		PdfPCell ppcassrestcell=new PdfPCell(ppcassrest);
		ppcassrestcell.setBorder(0);
		ppcassrestcell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		Phrase validity=new Phrase("Validity",font10bold);
		PdfPCell validcell=new PdfPCell(validity);
		validcell.setBorder(0);
		validcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase validityrest=new Phrase("This quotation is valid until "+fmt.format(quot.getValidUntill())+" from date of submission.",font10);
		PdfPCell validityrestcell=new PdfPCell(validityrest);
		validityrestcell.setBorder(0);
		validityrestcell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		PdfPTable table=new PdfPTable(3);
//		table.addCell(newline);
//		table.addCell(newline);
//		table.addCell(newline);
		table.addCell(freqofservicecell);
		table.addCell(coloncell);
		table.addCell(freqofservicedycell);
		table.addCell(ppcasscell);
		table.addCell(coloncell);
		table.addCell(ppcassrestcell);
		table.addCell(validcell);
		table.addCell(coloncell);
		table.addCell(validityrestcell);
		table.setWidthPercentage(100);
		try {
			table.setWidths(colwidthforpayterms);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void createpaymentterms() {
		// TODO Auto-generated method stub
		Phrase colon=new Phrase(":",font10);
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		
		Phrase blank=new Phrase(" ",font10);
		PdfPCell blankcell=new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
		
		
		Phrase payterms=new Phrase("Terms of Payment",font10bold);
		PdfPCell paytermscell=new PdfPCell(payterms);
		paytermscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		paytermscell.setBorder(0);
		
		PdfPTable table=new PdfPTable(3);
		table.setWidthPercentage(100);
		try {
			table.setWidths(colwidthforpayterms);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		table.addCell(paytermscell);
		table.addCell(coloncell);
		
		for(int i=0;i<quot.getPaymentTermsList().size();i++){
			Phrase paypercent=null;
			PdfPCell paypercentcell = null;
//			if(quot.getPaymentTermsList().get(i).getPayTermPercent()==100){
//				paypercent=new Phrase("Our service charges are payable in "+quot.getPaymentTermsList().get(i).getPayTermPercent()+"%"+" Advance along with your purchase order.",font10);
//				paypercentcell=new PdfPCell(paypercent);
//				paypercentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				paypercentcell.setBorder(0);
//			}else &&(quot.getPaymentTermsList().get(i).getPayTermPercent()!=100)
				if(i==0 ){
				paypercent=new Phrase(quot.getPaymentTermsList().get(i).getPayTermPercent()+"%"+" "+quot.getPaymentTermsList().get(i).getPayTermComment(),font10);
				paypercentcell=new PdfPCell(paypercent);
				paypercentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				paypercentcell.setBorder(0);
			}
			
			Phrase paypercentage=new Phrase(quot.getPaymentTermsList().get(i).getPayTermPercent()+"%"+" "+quot.getPaymentTermsList().get(i).getPayTermComment(),font10);
			PdfPCell paypercentagecell=new PdfPCell(paypercentage);
			paypercentagecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paypercentagecell.setBorder(0);
			
			if(i==0){
				table.addCell(paypercentcell);
			}else{
				table.addCell(blankcell);
				table.addCell(blankcell);
				table.addCell(paypercentagecell);
			}
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void createstaticpart() {
		// TODO Auto-generated method stub
		Phrase chunknewline = new Phrase(Chunk.NEWLINE);
		PdfPCell newline = new PdfPCell(chunknewline);
		newline.setHorizontalAlignment(Element.ALIGN_LEFT);
		newline.setBorder(0);
		
		Phrase staticpart=new Phrase("We await your acceptance of this quotation. Should you require further information on the above, please call undersigned; we would be pleased to assist you.",font10);
		PdfPCell staticpartcell=new PdfPCell(staticpart);
		staticpartcell.setBorder(0);
		staticpartcell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		
		Phrase ty=new Phrase("Thanking You",font10);
		PdfPCell tycell=new PdfPCell(ty);
		tycell.setBorder(0);
		tycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase sinpart=new Phrase("Your sincerely,",font10);
		PdfPCell sinpartcell=new PdfPCell(sinpart);
		sinpartcell.setBorder(0);
		sinpartcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		Phrase forcomp=new Phrase("For "+comp.getBusinessUnitName(),font10bold);
		PdfPCell forcompcell=new PdfPCell(forcomp);
		forcompcell.setBorder(0);
		forcompcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase space=new Phrase(Chunk.NEWLINE);
		PdfPCell spacecell=new PdfPCell(space);
		spacecell.setBorder(0);
		spacecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table=new PdfPTable(1);
		table.addCell(newline);
		table.addCell(staticpartcell);
//		table.addCell(newline);
		table.addCell(tycell);
		table.addCell(sinpartcell);
		table.addCell(forcompcell);
		table.addCell(spacecell);
		table.addCell(spacecell);
		table.setWidthPercentage(100);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createEmployeeDetails() {
		// TODO Auto-generated method stub
		Phrase empfullname=new Phrase(employee.getFullname(),font10bold);
		PdfPCell empfullnamecell=new PdfPCell(empfullname);
		empfullnamecell.setBorder(0);
		empfullnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase empdesignation=new Phrase(employee.getDesignation(),font10bold);
		PdfPCell empdesignationcell=new PdfPCell(empdesignation);
		empdesignationcell.setBorder(0);
		empdesignationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable emptable=new PdfPTable(1);
		emptable.addCell(empfullnamecell);
		emptable.addCell(empdesignationcell);
		emptable.setWidthPercentage(100);
		
		try {
			document.add(emptable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase colon=new Phrase(":",font10bold);
		PdfPCell coloncell=new PdfPCell(colon);
		coloncell.setBorder(0);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emplmob=new Phrase("Mobile",font10bold);
		PdfPCell emplmobcell=new PdfPCell(emplmob);
		emplmobcell.setBorder(0);
		emplmobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emplmobNo=new Phrase(employee.getCellNumber1()+"",font10bold);
		PdfPCell emplmobNocell=new PdfPCell(emplmobNo);
		emplmobNocell.setBorder(0);
		emplmobNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase email=new Phrase("Email",font10bold);
		PdfPCell emailcell=new PdfPCell(email);
		emailcell.setBorder(0);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase emailadd=new Phrase(employee.getEmail()+"",font10bold);
		PdfPCell emailaddcell=new PdfPCell(emailadd);
		emailaddcell.setBorder(0);
		emailaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase compland=new Phrase("Office",font10bold);
		PdfPCell complandcell=new PdfPCell(compland);
		complandcell.setBorder(0);
		complandcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase complandno=null;
		if(comp.getLandline()!=null){
			complandno=new Phrase(comp.getLandline()+"",font10bold);
		}else{
			complandno=new Phrase(" ",font10bold);
		}
		
		PdfPCell complandnocell=new PdfPCell(complandno);
		complandnocell.setBorder(0);
		complandnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase web=new Phrase("Website",font10bold);
		PdfPCell webcell=new PdfPCell(web);
		webcell.setBorder(0);
		webcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase website=null;
		if(comp.getLandline()!=null){
			website=new Phrase(comp.getWebsite()+"",font10bold);
		}else{
			website=new Phrase(" ",font10bold);
		}
		
		PdfPCell websitecell=new PdfPCell(website);
		websitecell.setBorder(0);
		websitecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable empltable=new PdfPTable(3);
		empltable.addCell(emplmobcell);
		empltable.addCell(coloncell);
		empltable.addCell(emplmobNocell);
		empltable.addCell(emailcell);
		empltable.addCell(coloncell);
		empltable.addCell(emailaddcell);
		empltable.addCell(complandcell);
		empltable.addCell(coloncell);
		empltable.addCell(complandnocell);
		empltable.addCell(webcell);
		empltable.addCell(coloncell);
		empltable.addCell(websitecell);
		empltable.setWidthPercentage(100);
		try {
			empltable.setWidths(colwidthforempdetails);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			document.add(empltable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
