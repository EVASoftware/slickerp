package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateFumigationAusPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8866383065996247482L;
	

	

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		   
	   FumigationAusPdf fumigationAuspdf=new FumigationAusPdf();
	   fumigationAuspdf.document=new Document();
	   Document document = fumigationAuspdf.document;
	   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	   
	   
	   document.open();
	   String preprintStatus=request.getParameter("preprint");
	   System.out.println("****************"+preprintStatus);
	   if(preprintStatus.contains("yes")){
		   
		   String type=request.getParameter("type");
		   if(type.contains("FA"))
		   {
			   fumigationAuspdf.setFumigation(count,preprintStatus);
		   }
		   
	   }
	   
	   if(preprintStatus.contains("no")){
		   
		   String type=request.getParameter("type");
		   if(type.contains("FA"))
		   {
			   fumigationAuspdf.setFumigation(count,preprintStatus);
		   }
		   
	   }
	   
	   if(preprintStatus.contains("plane")){
		   
		   String type=request.getParameter("type");
		   if(type.contains("FA"))
		   {
			   fumigationAuspdf.setFumigation(count,preprintStatus);
		   }
		   
	   }
	   
//	   fumigationpdf.setFumigation(count);
	   fumigationAuspdf.createPdf(preprintStatus);
//	   fumigationAuspdf.setFumigation(count);
//	   fumigationAuspdf.createPdf();
	   document.close();
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }

}
