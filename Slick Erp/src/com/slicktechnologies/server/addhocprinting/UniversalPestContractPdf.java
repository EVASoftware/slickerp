package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.swing.text.TabExpander;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class UniversalPestContractPdf {

	public Document document;
	Contract contract;
	Company comp;
	Customer cust;
	
	BaseColor mycolor = WebColors.getRGBColor("#C0C0C0");
	
	private  Font font16boldul,font12bold,font10bold,font10,font9bold,font12boldul,font10boldul,font12,bold,
	font16bold,font8,font8bold,font14bold;
	
	float[] relativeWidths2Column = { 2.3f, 7.7f };	
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	public 	UniversalPestContractPdf()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10boldul= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD|Font.UNDERLINE);
		font9bold= new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD |Font.UNDERLINE);
	}
	
	

	public void setservicequotation(Long count) {
		contract=ofy().load().type(Contract.class).id(count).now();
		
		if (contract.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", contract.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		if (contract.getCompanyId() != null)
		{
			cust = ofy().load().type(Customer.class).filter("count", contract.getCustomerId()).filter("companyId", contract.getCompanyId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", contract.getCustomerId()).first().now();
		}
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}
	
	public void createPdf(String preprintStatus) {
		
	//   1st page  
			if(preprintStatus.equals("yes")){
				
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			
			if(preprintStatus.equals("no")){
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}	
			createCustomerCopyHading();
			createContracteeInformation();
			createProductDetails();
			
			//   2nds page
			try {
				document.add(Chunk.NEXTPAGE);
				termsAndCondition();
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			//   3rd page 
			
			if(preprintStatus.equals("yes")){
				
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			
			if(preprintStatus.equals("no")){
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}	
			
			serviceContractHeading();
			createContracteeInformation();
			createProductDetails();
			
			
			
			
			//  4th page 
			
			try {
				document.add(Chunk.NEXTPAGE);
				termsAndCondition();
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		
	}


	private void createCustomerCopyHading() {
		
		Phrase customerCopy = new Phrase("Customer Copy",font14bold);
		PdfPCell customerCopyCell = new PdfPCell(customerCopy);
		customerCopyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		customerCopyCell.setPadding(5);
		customerCopyCell.setBackgroundColor(mycolor);
		
		PdfPTable tabel= new PdfPTable(1);
		tabel.setWidthPercentage(100f);
		tabel.addCell(customerCopyCell);
		
		try {
			document.add(tabel);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	private void serviceContractHeading() {
		
		Phrase serviceContractHeadingpara = new Phrase("Service Contract",font14bold);
		PdfPCell serviceContractHeadingparaCell = new PdfPCell(serviceContractHeadingpara);
		serviceContractHeadingparaCell.setPadding(5);
		serviceContractHeadingparaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceContractHeadingparaCell.setBackgroundColor(mycolor);
		
		
		PdfPTable tabel= new PdfPTable(1);
		tabel.setWidthPercentage(100f);
		tabel.addCell(serviceContractHeadingparaCell);
		
		
		try {
			document.add(tabel);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	private void createProductDetails() {
		
		String treatements ="";
		String serviceFrequency = "";
		String natureOfTreatment ="";
		for (int i = 0; i < contract.getItems().size(); i++) {
			
			/**
			 * Date 20/12/2017
			 * By jayshree
			 * Des.To add only 10 items in the pdf,more than 10 product wiil be skiped
			 */
		
			if(i>=6){
				break;
			}
			//End by jayshree
			ServiceProduct serProd = ofy().load().type(ServiceProduct.class).filter("productCode", contract.getItems().get(i).getProductCode()).first().now();
		// code for getting product name  and number of services 	
			
		if(i==0)
		{
			treatements = contract.getItems().get(i).getProductName();
			serviceFrequency =  contract.getItems().get(i).getNumberOfServices()+" services of "+contract.getItems().get(i).getProductName();
			natureOfTreatment = serProd.getProductCategory();
 		}	
		else
		{
			if(i+1 ==contract.getItems().size()){
				treatements = treatements+"&" +contract.getItems().get(i).getProductName();
				serviceFrequency = serviceFrequency+"&"+contract.getItems().get(i).getNumberOfServices()+" services of "+contract.getItems().get(i).getProductName();
				natureOfTreatment =natureOfTreatment + "&" +serProd.getProductCategory();
			}
			else
			{
				treatements = treatements+"," +contract.getItems().get(i).getProductName();
				serviceFrequency = serviceFrequency+","+contract.getItems().get(i).getNumberOfServices()+" services of "+contract.getItems().get(i).getProductName();
				natureOfTreatment =natureOfTreatment + " ," +serProd.getProductCategory();
			}
		}
		
	}
	
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		
		try {
			table.setWidths(relativeWidths2Column);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase blank = new Phrase(" ",font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setPadding(2f);
	
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase natureOfPests = new Phrase("Nature of Pests :",font10);
		PdfPCell natureOfPestsCell = new PdfPCell(natureOfPests);
		natureOfPestsCell.setBorderWidthRight(0);
		natureOfPestsCell.setPadding(5f);
		table.addCell(natureOfPestsCell);
		
		Phrase natureOfTreatmentPh = new Phrase(natureOfTreatment,font10);
		PdfPCell natureOfTreatmentPhCell =new PdfPCell(natureOfTreatmentPh);
		natureOfTreatmentPhCell.setBorderWidthLeft(0);
		natureOfTreatmentPhCell.setPadding(5f);
		table.addCell(natureOfTreatmentPhCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase premisesType = new Phrase("Premises Type :",font10);
		PdfPCell premisesTypeCell = new PdfPCell(premisesType);
		premisesTypeCell.setBorderWidthRight(0);
		premisesTypeCell.setPadding(5f);
		table.addCell(premisesTypeCell);
		
		Phrase premisesTypePh = new Phrase(contract.getCategory(),font10);
		PdfPCell premisesTypePhCell =new PdfPCell(premisesTypePh);
		premisesTypePhCell.setBorderWidthLeft(0);
		premisesTypePhCell.setPadding(5f);
		table.addCell(premisesTypePhCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		if(contract.getPremisesDesc()!= null)
		{
			Phrase premises = new Phrase("Premises to be treated :",font10);
			PdfPCell premisesCell = new PdfPCell(premises);
			premisesCell.setBorderWidthRight(0);
			premisesCell.setPadding(5f);
			table.addCell(premisesCell);
			
			Phrase premisesValue = new Phrase(contract.getPremisesDesc(),font10);
			PdfPCell premisesValueCell =new PdfPCell(premisesValue);
			premisesValueCell.setBorderWidthLeft(0);
			premisesValueCell.setPadding(5f);
			table.addCell(premisesValueCell);
			
			table.addCell(blankCell);
			table.addCell(blankCell);
		}
		
		Phrase treatment = new Phrase("Treatment :",font10);
		PdfPCell treatmentCell = new PdfPCell(treatment);
		treatmentCell.setBorderWidthRight(0);
		treatmentCell.setPadding(5f);
		table.addCell(treatmentCell);
		
		Phrase treatementsPh = new Phrase(treatements,font10);
		PdfPCell treatementsPhCell =new PdfPCell(treatementsPh);
		treatementsPhCell.setBorderWidthLeft(0);
		treatementsPhCell.setPadding(5f);
		table.addCell(treatementsPhCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
	
		Phrase contractPeriod = new Phrase("Contract period :",font10);
		PdfPCell contractPeriodCell = new PdfPCell(contractPeriod);
		contractPeriodCell.setUseBorderPadding(true);
		contractPeriodCell.setBorderWidthRight(0);
		contractPeriodCell.setPadding(5f);
		table.addCell(contractPeriodCell);
		
		Phrase contractPeriodValue = new Phrase("From :"+fmt.format(contract.getStartDate())+" to "+fmt.format(contract.getEndDate()),font10);
		PdfPCell contractPeriodValueCell =new PdfPCell(contractPeriodValue);
		contractPeriodValueCell.setBorderWidthLeft(0);
		contractPeriodValueCell.setPadding(5f);
		table.addCell(contractPeriodValueCell);
	

		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase frequency = new Phrase("Service Frequency :",font10);
		PdfPCell frequencyCell = new PdfPCell(frequency);
		frequencyCell.setBorderWidthRight(0);
		frequencyCell.setPadding(5f);
		table.addCell(frequencyCell);
		
		Phrase serviceFrequencyPh = new Phrase(serviceFrequency,font10);
		PdfPCell serviceFrequencyPhCell =new PdfPCell(serviceFrequencyPh);
		serviceFrequencyPhCell.setBorderWidthLeft(0);
		serviceFrequencyPhCell.setPadding(5f);
		table.addCell(serviceFrequencyPhCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		
		Phrase contractValue = new Phrase("Contract Value :",font10);
		PdfPCell contractValueCell = new PdfPCell(contractValue);
		contractValueCell.setBorderWidthRight(0);
		contractValueCell.setPadding(5f);
		table.addCell(contractValueCell);
		
		String taxDetails = " ";
		/**
		 * Date 22-08-2017 below code is not required now so commented
		 */
//		   for(int i=0; i<contract.getItems().size();i++){
		      	
//			      if((contract.getItems().get(i).getVatTax().getPercentage()>0)&&(contract.getItems().get(i).getServiceTax().getPercentage()==0)){
//			    	  taxDetails = "Vat Tax Extra";
//			      }
//			      else if((contract.getItems().get(i).getServiceTax().getPercentage()>0)&&(contract.getItems().get(i).getVatTax().getPercentage()==0))
//			      {
//			    	  taxDetails = "Service Tax Extra";
//			      }
//			       else if((contract.getItems().get(i).getVatTax().getPercentage()>0)&&(contract.getItems().get(i).getServiceTax().getPercentage()>0)){
//			    	   taxDetails = "Service Tax and Vat Tax Extra";
//			       }
//			       else{
			      	 
			    	   taxDetails ="GST Extra";
//			       }
//			      }
		
		
		Phrase contractValuePh = new Phrase("Rs "+contract.getTotalAmount()+"/- Only  " + "("+taxDetails+")",font10);
		PdfPCell contractValuePhCell =new PdfPCell(contractValuePh);
		contractValuePhCell.setBorderWidthLeft(0);
		contractValuePhCell.setPadding(5f);
		table.addCell(contractValuePhCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase modeOfPayment = new Phrase("Mode of payment :",font10);
		PdfPCell modeOfPaymentCell = new PdfPCell(modeOfPayment);
		modeOfPaymentCell.setBorderWidthRight(0);
		modeOfPaymentCell.setPadding(5f);
		table.addCell(modeOfPaymentCell);
		
		String payterms = ""; 
		for (int i = 0; i < contract.getPaymentTermsList().size(); i++) {
			
			if(i==0)
			{
				payterms = contract.getPaymentTermsList().get(i).getPayTermPercent()+"%"+contract.getPaymentTermsList().get(i).getPayTermComment();
	 		}	
			else
			{
				if(i+1 ==contract.getPaymentTermsList().size())
				{
					payterms =payterms+"&"+contract.getPaymentTermsList().get(i).getPayTermPercent()+"%"+contract.getPaymentTermsList().get(i).getPayTermComment();
				}
				else
				{
					payterms =payterms +"," +contract.getPaymentTermsList().get(i).getPayTermPercent()+"%"+contract.getPaymentTermsList().get(i).getPayTermComment();
				}
			}
			
		}
		
		
		
		Phrase modeOfPaymentPh = new Phrase(payterms,font10);
		PdfPCell modeOfPaymentPhCell =new PdfPCell(modeOfPaymentPh);
		modeOfPaymentPhCell.setBorderWidthLeft(0);
		modeOfPaymentPhCell.setPadding(5f);
		table.addCell(modeOfPaymentPhCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		//   sign 
		
		Phrase signOfCustomer = new Phrase("Signature of the Client with Stamp",font10bold);
		PdfPCell signOfCustomerCell = new PdfPCell(signOfCustomer);
		signOfCustomerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		signOfCustomerCell.setBorder(0);
		
		Phrase compName = null;
		if(contract.getGroup()!= null && !contract.getGroup().equals(""))
		{
			compName = new Phrase("For,"+ contract.getGroup(),font10bold);
		}
		else
		{
			compName = new Phrase("For,"+ contract.getGroup(),font10bold);
		}
		
		 
		PdfPCell compNameCell = new PdfPCell(compName);
		compNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compNameCell.setBorder(0);
		
		Phrase date = new Phrase("Date :",font10bold);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateCell.setBorder(0);
		
		Phrase director = new Phrase("Director",font10bold);
		PdfPCell directorCell = new PdfPCell(director);
		directorCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		directorCell.setBorder(0);
		
		PdfPTable signTable = new PdfPTable(2);
		signTable.setWidthPercentage(100f);
		
		signTable.addCell(signOfCustomerCell);
		signTable.addCell(compNameCell);
		signTable.addCell(blankCell);
		signTable.addCell(blankCell);
		signTable.addCell(blankCell);
		signTable.addCell(blankCell);
		signTable.addCell(dateCell);
		signTable.addCell(directorCell);
		
		
		
		try {
			document.add(table);
			document.add(signTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	}



	private void createContracteeInformation() {
		
		PdfPTable dateTable = new PdfPTable(1);
		dateTable.setWidthPercentage(100f);
		
		//Date 18/1/2018
		//By Jayshree
		//Changes creation date to contract date
		Phrase date =new Phrase(fmt.format(contract.getContractDate()),font10);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setBorder(0);
		dateTable.addCell(dateCell);
		
		Paragraph declarationPara = new Paragraph("We hereby undertake the Annual Service Contract of the undermentioned premises as per the terms and & conditions given below.",font10);
		declarationPara.setAlignment(Element.ALIGN_JUSTIFIED);
		
		Paragraph contractId = new Paragraph("Your Order No. :" +contract.getCount(),font10);
		contractId.setAlignment(Element.ALIGN_LEFT);
		
		Phrase para = null;
		if(contract.getGroup()!= null && !contract.getGroup().equals(""))
		{
			para = new Phrase("(To be paid by "+contract.getCustomerFullName()+"(Hereinafter called 'The Contractee') on the commencement of service contract to messrs. "+contract.getGroup()+" (Hereinafter called 'The Contractor') or their assigness. Administrators,Representatives and Authorised agents. The Contractor shall undertake to render the Pest Control Treatment to the premises of the Contractee as per particulars given below and additional)",font10);
		}
		else
		{
			para = new Phrase("(To be paid by "+contract.getCustomerFullName()+"(Hereinafter called 'The Contractee') on the commencement of service contract to messrs."+comp.getBusinessUnitName()+" (Hereinafter called 'The Contractor') or their assigness. Administrators,Representatives and Authorised agents. The Contractor shall undertake to render the Pest Control Treatment to the premises of the Contractee as per particulars given below and additional)",font10);
		}
		
		
		PdfPCell paraCell = new PdfPCell(para);
		paraCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraCell.setBorder(0);
		
		PdfPTable declaration =  new PdfPTable(1);
		declaration.addCell(paraCell);
		
		
	
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(relativeWidths2Column);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase contractee = new Phrase("Contractee Information : ",font10);
		PdfPCell contracteeCell = new PdfPCell(contractee);
		contracteeCell.setBorder(0);
		table.addCell(contracteeCell);
		
		Phrase customerName = new Phrase(contract.getCustomerFullName(),font10);
		PdfPCell customerNameCell = new PdfPCell(customerName);
		customerNameCell.setBorderWidthLeft(0);
		customerNameCell.setBorderWidthTop(0);
		customerNameCell.setBorderWidthRight(0);
		table.addCell(customerNameCell);

		Phrase black = new Phrase(" ",font10);
		PdfPCell blackCell = new PdfPCell(black);
		blackCell.setBorder(0);
		table.addCell(blackCell);

		Phrase address = new Phrase(cust.getAdress().getCompleteAddress(),font10);
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setBorder(0);
		table.addCell(addressCell);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.setWidthPercentage(100f);
		/**
		 * Date 20/12/2017
		 * By Jayshree
		 * Des.add the spacing between table
		 */
		parenttable.setSpacingBefore(10f);
		
		PdfPCell tableCell = new PdfPCell(table);
		parenttable.addCell(tableCell);
		parenttable.addCell(blackCell);
		parenttable.addCell(declaration);
		try {
			document.add(dateTable);
//			document.add(Chunk.NEWLINE);//Date 20/12/2017 by Jayshree to remove space
			document.add(declarationPara);
//			document.add(Chunk.NEWLINE);//Date 20/12/2017 by Jayshree to remove space
			document.add(contractId);
//			document.add(Chunk.NEWLINE);//Date 20/12/2017 by Jayshree to remove space
			document.add(parenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		 blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


private void createCompanyNameAsHeader(Document doc, Company comp) {
	
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}


	private void termsAndCondition() {
		
		PdfPTable paterntTable = new PdfPTable(1);
		paterntTable.setWidthPercentage(100f);
		
		Phrase blank = new Phrase(Chunk.NEWLINE);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		
		
		paterntTable.addCell(blankCell);
		
		Phrase termsAndCondition = new Phrase("TERMS & CONDITION:",font12bold);
		PdfPCell para= new PdfPCell(termsAndCondition);
		para.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		para.setBorder(0);
		paterntTable.addCell(para);
		paterntTable.addCell(blankCell);
		
		Phrase note1 = new Phrase("* In case of 'Genral Disinfestation' the premiss must be kept closed for a period of 3 to 4 hours after the thorough treatment, so as to ensure efficacy of the treatment.",font10);
		PdfPCell paraNote1= new PdfPCell(note1);
		paraNote1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote1.setBorder(0);
		paterntTable.addCell(paraNote1);
		paterntTable.addCell(blankCell);
		
		Phrase note2 = new Phrase("* In the contract we are liable to control the said pest only,we are not liable to control any flying insects,however our treatment will be effective on flying insects but there will be no warranty issues bye us for the same in the contract.",font10 );
		PdfPCell paraNote2= new PdfPCell(note2);
		paraNote2.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote2.setBorder(0);
		paterntTable.addCell(paraNote2);
		paterntTable.addCell(blankCell);
		
		Phrase note3 = new Phrase("* Anti Termite Treatments schedule means intial treatment will ne done by drilling, injecting and spaying process followed by once in 6 months check up services and precautionary treatment means there is no drilling to be done,only spraying.",font10);
		PdfPCell paraNote3= new PdfPCell(note3);
		paraNote3.setBorder(0);
		paraNote3.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paterntTable.addCell(paraNote3);
		paterntTable.addCell(blankCell);
		
		Phrase note4 = new Phrase("* The contractee must make sure that pets in the house, if any are kept away from the prenises as to ensure complete safety to pet animals.",font10);
		PdfPCell paraNote4= new PdfPCell(note4);
		paraNote4.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote4.setBorder(0);
		paterntTable.addCell(paraNote4);
		paterntTable.addCell(blankCell);
		
		Phrase note5 = new Phrase("* We shall not be responsible;loss or damage of any articles especilly food-stuffs, plastic materials and any valuables etc. while treating the prenises under contract.To avoid such possibillties it shall be the responsiblity of contractee to allow our persons to perform the treatment in the presence of responsible person of their choice. We are  not responsible for any mishandling,thefts etc.",font10);
		PdfPCell paraNote5= new PdfPCell(note5);
		paraNote5.setBorder(0);
		paraNote5.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paterntTable.addCell(paraNote5);
		paterntTable.addCell(blankCell);
		
		Phrase note6 = new Phrase("* We shall try to keep the schedule of our services. However, we shall not be held responsible for any delay or postponement of the schedule due to circumstances beyond our control and in order to facilitate prompt and smooth services the premises should be made avallable for fixed timing or period as agreeable to both.",font10);
		PdfPCell paraNote6= new PdfPCell(note6);
		paraNote6.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote6.setBorder(0);
		paterntTable.addCell(paraNote6);
		paterntTable.addCell(blankCell);
		
		Phrase note7 = new Phrase("* An intimation card proposing the date and time of the treatment will be sent in advance, our service Dept, should be informed immediately for anycharge.",font10);
		PdfPCell paraNote7= new PdfPCell(note7);
		paraNote7.setBorder(0);
		paraNote7.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paterntTable.addCell(paraNote7);
		paterntTable.addCell(blankCell);
		
		
		Phrase note8 = null;
		if(contract.getGroup()!= null && !contract.getGroup().equals(""))
		{
			 note8 = new Phrase("* All payment should be made by crossed cheque drawn in favour of "+contract.getGroup()+" as per the mode of payment terms of the contract.",font10);
		}
		else
		{
			 note8 = new Phrase("* All payment should be made by crossed cheque drawn in favour of "+comp.getBusinessUnitName()+" as per the mode of payment terms of the contract.",font10);
		}
		
		
		PdfPCell paraNote8= new PdfPCell(note8);
		paraNote8.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote8.setBorder(0);
		paterntTable.addCell(paraNote8);
		paterntTable.addCell(blankCell);
		
		Phrase note9 = new Phrase("* The treatment will be done as mentioned and agreed between,the terms of the contract will be agred by the contractee.",font10);
		PdfPCell paraNote9= new PdfPCell(note9);
		paraNote9.setBorder(0);
		paraNote9.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paterntTable.addCell(paraNote9);
		paterntTable.addCell(blankCell);
		
		Phrase note10 = new Phrase("* Charges in this timing will not be allowed unless dully paid extraon wages invoived.",font10);
		PdfPCell paraNote10= new PdfPCell(note10);
		paraNote10.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote10.setBorder(0);
		paterntTable.addCell(paraNote10);
		paterntTable.addCell(blankCell);
		
		Phrase note11 = new Phrase("* We may at our discretion accept payment after the service.However, interest at 20% p.a. will be charged against all delayed payment.",font10);
		PdfPCell paraNote11= new PdfPCell(note11);
		paraNote11.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote11.setBorder(0);
		paterntTable.addCell(paraNote11);
		paterntTable.addCell(blankCell);
		
		
		Phrase note12 = new Phrase("* The contract is not transferable.",font10);
		PdfPCell paraNote12= new PdfPCell(note12);
		paraNote12.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote12.setBorder(0);
		paterntTable.addCell(paraNote12);
		paterntTable.addCell(blankCell);
		
		Phrase note13 = new Phrase("* GST Tax will be charged as applicable and if would not be as included in the contract amount.",font10);
		PdfPCell paraNote13= new PdfPCell(note13);
		paraNote13.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote13.setBorder(0);
		paterntTable.addCell(paraNote13);
		paterntTable.addCell(blankCell);
		
		Phrase note14 = new Phrase("* Payment should be released within 7 days of submission of the bill.",font10);
		PdfPCell paraNote14= new PdfPCell(note14);
		paraNote14.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote14.setBorder(0);
		paterntTable.addCell(paraNote14);
		paterntTable.addCell(blankCell);
		
		Phrase note15 = new Phrase("* Jurisdiction at Surat(Gujarat).",font10);
		PdfPCell paraNote15= new PdfPCell(note15);
		paraNote15.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote15.setBorder(0);
		paterntTable.addCell(paraNote15);
		paterntTable.addCell(blankCell);
		
		Phrase note16 = new Phrase("* Your representative shouls be present during the spraying operation to remove or shift articles.",font10);
		PdfPCell paraNote16= new PdfPCell(note16);
		paraNote16.setBorder(0);
		paraNote16.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paterntTable.addCell(paraNote16);
		paterntTable.addCell(blankCell);
		
		Phrase note17 = new Phrase("* Computer,electronic items should be properly covered.",font10);
		PdfPCell paraNote17= new PdfPCell(note17);
		paraNote17.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote17.setBorder(0);
		paterntTable.addCell(paraNote17);
		paterntTable.addCell(blankCell);
		
		Phrase note18 = new Phrase("* Closets,Bathroom and Drainage inspection pit should be kept open.",font10);
		PdfPCell paraNote18= new PdfPCell(note18);
		paraNote18.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		paraNote18.setBorder(0);
		paterntTable.addCell(paraNote18);
		paterntTable.addCell(blankCell);

		PdfPCell parentTableCell = new PdfPCell(paterntTable);
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		table.addCell(parentTableCell);
	
		try {
		document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}




}
