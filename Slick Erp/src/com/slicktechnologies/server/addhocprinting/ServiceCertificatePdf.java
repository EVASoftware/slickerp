package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.text.ParseException;

import org.apache.poi.hssf.record.RightMarginRecord;
import org.apache.poi.ss.formula.functions.Vlookup;

import com.google.appengine.api.images.Image.Format;
import com.google.gwt.aria.client.TablistRole;
import com.google.gwt.thirdparty.javascript.rhino.head.Ref;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.collection.PdfTargetDictionary;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import java.util.logging.Level;
import java.util.logging.Logger;
public class ServiceCertificatePdf {


	public Document document;
	
	Logger logger = Logger.getLogger("NameOfYourLogger");
	Company comp;
	Customer cust;
	Contract con;
	Phrase blank;
	Phrase chunk;
	Service service;
	Phrase col;
	boolean checkBranchdata=false;
	private PdfPCell imageSignCell;
	Branch branchDt = null;
	boolean branchAsCompany=false;
	CompanyPayment companyPayment;
	float[] columnWidth = {(float) 33.33, (float) 33.33, (float) 33.33};
	
	private Font font16boldul, font12bold, font8boldul,font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9, font9boldul,
	font14boldul,font16bold,font16,font10boldul;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat sdf = new SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy",  Locale.ENGLISH);
	boolean digitalSignatureFlag = false;
	boolean rexConfig=false;
	
	Date validUntilDate =null;
	Date validTillDate = null;
	ServiceProject project;
	
	public ServiceCertificatePdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD  | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font16 = new Font(Font.FontFamily.HELVETICA, 16);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font8boldul= new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD
				| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setService(Long count,String stringdate) {
		
		logger.log(Level.SEVERE,"VALID UNTILL DATE in side set method ::: "+stringdate);
		System.out.println("VALID UNTILL DATE ::: "+stringdate);
		service = ofy().load().type(Service.class).id(count).now();
		
		try {
			service.setCertificateValidTillDate(stringdate);
			ofy().save().entity(service); //23-01-2024
			validUntilDate =  sdf.parse(stringdate);
			System.out.println("VALID UNTILL DATE == ::: "+fmt.format(validUntilDate));
			
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
		
		if (service.getCompanyId() == null){
			cust = ofy().load().type(Customer.class).filter("count", service.getCustomerId()).first().now();
		}else{
			cust = ofy().load().type(Customer.class).filter("companyId", service.getCompanyId()).filter("count", service.getCustomerId()).first().now();
		}
		
		if(service.getCompanyId() == null){
			con=ofy().load().type(Contract.class).filter("count", service.getContractCount()).first().now();
		}else{
			con=ofy().load().type(Contract.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
		}
		
		
		branchDt = ofy().load().type(Branch.class)
				.filter("companyId", service.getCompanyId())
				.filter("buisnessUnitName", service.getBranch()).first().now();
		if (ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())) {
			checkBranchdata=true;
			logger.log(Level.SEVERE, "Process active --");

			if (branchDt != null && branchDt.getPaymentMode() != null
					&& !branchDt.getPaymentMode().trim().equals("")) {

				logger.log(Level.SEVERE,
						"Process active --" + branchDt.getPaymentMode());

				List<String> paymentDt = Arrays.asList(branchDt
						.getPaymentMode().trim().split("/"));

				if (paymentDt.get(0).trim().matches("[0-9]+")) {

					int payId = Integer.parseInt(paymentDt.get(0).trim());

					companyPayment = ofy().load().type(CompanyPayment.class)
							.filter("count", payId)
							.filter("companyId", service.getCompanyId()).first().now();

//					if (companyPayment != null) {
//						comp = ServerAppUtility.changeBranchASCompany(branchDt,comp);
//					}

				}
				comp = ServerAppUtility.changeBranchASCompany(branchDt,comp);
			}

		}
		
		digitalSignatureFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", AppConstants.PC_ServiceCertificate, comp.getCompanyId());
		
	 	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	 	
	 	rexConfig=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "RexServiceCertificate", comp.getCompanyId());
		 	
	 	
		if (stringdate != null) {
			SimpleDateFormat fmt123 = new SimpleDateFormat("EEE MMM dd HH:mm:ss z SSS yyyy");
			
			try {
				validTillDate = fmt123.parse(stringdate);
			} catch (ParseException e1) {
//				e1.printStackTrace();
			}
		}
		
		if (service != null) {
			project = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId())
					.filter("contractId", service.getContractCount())
					.filter("serviceId", service.getCount()).first().now();
		}
	 	
	}
	
	
public void createPdf(String date, String preprintStatus) {
	
//	if(preprintStatus.contains("plane"))
//	{
//		createPestControlHeading();
//		createLicenseHeading();
//	}
//	else if(preprintStatus.contains("yes"))
//	{
//		createBlankforUPC();
//	}
//	else if(preprintStatus.contains("no"))
//	{
//		if(comp.getUploadHeader()!=null){
//	    	createCompanyNameAsHeader(document,comp);
//		}
//		
//		if(comp.getUploadFooter()!=null){
//			createCompanyNameAsFooter(document,comp);
//		}
//	
//	createBlankforUPC();
//	}
	
//	createLine1();
//	createCustomerDetail();
//	createCustomerAddress();
//	createBlank(date);
//	createAuthoTitle();
	
	if(rexConfig){
		if(preprintStatus.contains("plane"))
		{
		    createHeader();
		    createRexServiceCertificate();
		}
	else if(preprintStatus.contains("yes"))
		{
			createBlankforUPC();
			createRexServiceCertificate();
		}
	else if(preprintStatus.contains("no")){
		if(comp.getUploadHeader()!=null){
	    	createCompanyNameAsHeader(document,comp);
	    	blankCells();
		}
		
		createRexServiceCertificate();
		
		if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
		}
		
	}
	
	}else{
		if(preprintStatus.contains("plane"))
		{
		    createHeader();
			createPestControlHeading();
			createFirstPara();
			createSecondPara();
			footerPart();
			
			signatoryPart();
		}
	else if(preprintStatus.contains("yes"))
		{
			createBlankforUPC();
			createPestControlHeading();
			createFirstPara();
			createSecondPara();
			footerPart();
			signatoryPart();
		}
	else if(preprintStatus.contains("no")){
		
		if(comp.getUploadHeader()!=null){
	    	createCompanyNameAsHeader(document,comp);
	    	blankCells();
		}
		createPestControlHeading();
		createFirstPara();
		createSecondPara();
		footerPart();
		signatoryPart();
		
		if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
		}
		
	}
	
	}
	
	
	
	
	}


private void createHeader() {
	
	
	
	PdfPTable headerTAB=new PdfPTable(3);
	headerTAB.setWidthPercentage(100);
	try {
		headerTAB.setWidths(new float[]{20,60,20});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	PdfPTable headertable=new PdfPTable(1);
	headertable.setWidthPercentage(100);
	headertable.setSpacingAfter(10);
	
	//Set Company Name
	String businessunit="";
	if(checkBranchdata && branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
		businessunit= branchDt.getCorrespondenceName();
		logger.log(Level.SEVERE,"companyName header "+businessunit);
	}
	else {
		businessunit = comp.getBusinessUnitName().trim().toUpperCase();
	}
	
	Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
			font14bold);
	PdfPCell companyNameCell = new PdfPCell(companyNameph);
	companyNameCell.setBorder(0);
	companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headertable.addCell(companyNameCell);
	
	//Set Address of Company
	String address = "";
	if(checkBranchdata) {
		address=branchDt.getAddress().getCompleteAddress();
	} else {
		address=comp.getAddress().getCompleteAddress();
	}
	
	Phrase companyAddph = new Phrase(address, font10);
	PdfPCell companyAddCell = new PdfPCell(companyAddph);
	companyAddCell.setBorder(0);
	companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headertable.addCell(companyAddCell);

	//Email ID
	String emailid="";
	if(checkBranchdata) {
		emailid = branchDt.getEmail();
	} else {
			emailid = comp.getEmail();
			System.out.println("server method " + emailid);
		} 
	
	Phrase companyemailph = new Phrase("E-Mail : "+emailid, font10);
	PdfPCell companyemailCell = new PdfPCell(companyemailph);
	companyemailCell.setBorder(0);
	companyemailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headertable.addCell(companyemailCell);
	
	/**
	 * @author Vijay Date :- 24-10-2020
	 * Des :- if below process config is true then GST No and PAN Number will print
	 * requirement raised by vaishnavi for Universal pest Control
	 */
	if(digitalSignatureFlag){
		ServerAppUtility serverapp = new ServerAppUtility();
		
		boolean flag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId());
		String GSTInVal = "";
		String PANNumberVal = "";
		if(flag){
			GSTInVal = serverapp.getGSTINOfCompany(comp, service.getBranch());
			PANNumberVal = serverapp.getPANNumber(comp, service.getBranch(), "Service Certificate");
		}
		else{
			GSTInVal = serverapp.getGSTINOfCompany(comp, "");
			PANNumberVal = serverapp.getPANNumber(comp, "", "Service Certificate");
		}
		if(GSTInVal!=null && !GSTInVal.equals("")){
			Phrase GSTIN = new Phrase("GST No : "+GSTInVal, font10);
			PdfPCell GSTINCell = new PdfPCell(GSTIN);
			GSTINCell.setBorder(0);
			GSTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headertable.addCell(GSTINCell);
		}
		if(PANNumberVal!=null && !PANNumberVal.equals("")){
			Phrase panNo = new Phrase("PAN No : "+PANNumberVal, font10);
			PdfPCell panNoCell = new PdfPCell(panNo);
			panNoCell.setBorder(0);
			panNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			headertable.addCell(panNoCell);
		}
		
	}
	
	//WebSite
	String website="";
	if(comp.getWebsite()==null || comp.getWebsite().equals(""))
	{
		website="";
	}
	else
	{
		
		website="Website : "+comp.getWebsite();
	}
	Phrase companyWebph = new Phrase(website, font10);
	PdfPCell companyWebCell = new PdfPCell(companyWebph);
	companyWebCell.setBorder(0);
	companyWebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headertable.addCell(companyWebCell);
	
	
	//Contact
   String contactinfo="";
	if(checkBranchdata) {
		if (branchDt.getCellNumber1() != null && branchDt.getCellNumber1() != 0) {
			System.out.println("pn11");
			contactinfo = branchDt.getBranchCode() + branchDt.getCellNumber1() + "";
		}
		if (branchDt.getCellNumber2() != null && branchDt.getCellNumber2() != 0) {
			if (!contactinfo.trim().isEmpty()) {
				contactinfo = contactinfo + " / " + branchDt.getBranchCode()
						+ branchDt.getCellNumber2() + "";
			} else {
				contactinfo = branchDt.getBranchCode() + branchDt.getCellNumber2()
						+ "";
			}
			System.out.println("pn33" + contactinfo);
		}
		if (branchDt.getLandline() != 0 && branchDt.getLandline() != null) {
			if (!contactinfo.trim().isEmpty()) {
				contactinfo = contactinfo + " / " + branchDt.getBranchCode()
						+ branchDt.getLandline() + "";
			} else {
				contactinfo = branchDt.getBranchCode() + branchDt.getLandline() + "";
			}
			System.out.println("pn44" + contactinfo);
		}
	} else {
		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			contactinfo = comp.getCountryCode() + comp.getCellNumber1() + "";
		}
		if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
			if (!contactinfo.trim().isEmpty()) {
				contactinfo = contactinfo + " / " + comp.getCountryCode()
						+ comp.getCellNumber2() + "";
			} else {
				contactinfo = comp.getCountryCode() + comp.getCellNumber2()
						+ "";
			}
			System.out.println("pn33" + contactinfo);
		}
		if (comp.getLandline() != 0 && comp.getLandline() != null) {
			if (!contactinfo.trim().isEmpty()) {
				contactinfo = contactinfo + " / " + comp.getStateCode()
						+ comp.getLandline() + "";
			} else {
				contactinfo = comp.getStateCode() + comp.getLandline() + "";
			}
			System.out.println("pn44" + contactinfo);
		}
	}
	Phrase companymobph = new Phrase("Phone :  "+contactinfo,font10);
	PdfPCell companymobCell = new PdfPCell(companymobph);
	companymobCell.setBorder(0);
	companymobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headertable.addCell(companymobCell);
	
	PdfPTable logoTable = new PdfPTable(2);
	logoTable.setSpacingAfter(5);
	logoTable.setWidthPercentage(100f);
	try {
		logoTable.setWidths(new float[]{8,92});
	} catch (DocumentException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	DocumentUpload logodocument ;
	if(checkBranchdata) {
		logodocument = branchDt.getLogo();
	} else {
		logodocument =comp.getLogo();
	}
	
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	PdfPCell imageSignCell = null;
	Image image2=null;
	try {
		image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
		image2.scalePercent(20f);
//		image2.setAbsolutePosition(40f,765f);	
//		doc.add(image2);
		
		imageSignCell = new PdfPCell();
		imageSignCell.addElement(image2);
		imageSignCell.setImage(image2);
		imageSignCell.setBorder(0);
		imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		imageSignCell.setFixedHeight(20);
	} catch (Exception e) {
		e.printStackTrace();
	}
	Phrase blank =new Phrase(" ");
	PdfPCell blankCell=new PdfPCell(blank);
	blankCell.setBorder(0);
	if(imageSignCell != null)
	{
		logoTable.addCell(blankCell);
		logoTable.addCell(imageSignCell);
	}
	else
	{
		logoTable.addCell(blankCell);
		logoTable.addCell(blankCell);
	}

	PdfPTable titletab = new PdfPTable(1);
	titletab.setWidthPercentage(100f);
	titletab.setSpacingAfter(5);
	
	Phrase title =new Phrase("",font9bold);
	PdfPCell titlecell=new PdfPCell(title);
	titlecell.setBorder(0);
	titlecell.setPaddingRight(3);
	titlecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	titletab.addCell(titlecell);
	
	
	PdfPCell logocell=new PdfPCell(logoTable);
	logocell.setBorderWidthRight(0);
	
	PdfPCell headerCell=new PdfPCell(headertable);
	headerCell.setBorderWidthLeft(0);
	headerCell.setBorderWidthRight(0);
	
	PdfPCell titlcell=new PdfPCell(titletab);
	titlcell.setBorderWidthLeft(0);
	
	headerTAB.addCell(logocell);
	headerTAB.addCell(headerCell);
	headerTAB.addCell(titlcell);
	
	
	
	
	
	
	try {
		document.add(headerTAB);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	
	
	
}
private void blankCells() {
	 Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	try {
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
	}catch(Exception e){
	e.printStackTrace();	
	}
}
private void signatoryPart() {

	PdfPTable signTable = new PdfPTable(1);
	signTable.setWidthPercentage(100);
	
	DocumentUpload digitalDocument = null;
	
	if(checkBranchdata){
		digitalDocument = branchDt.getUploadDigitalSign();
	}
	else{
		digitalDocument = comp.getUploadDigitalSign();
	}
	
//	DocumentUpload digitalDocument = comp.getUploadDigitalSign();
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}
	imageSignCell = null;
	Image image2 = null;
	logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
	try {
		image2 = Image.getInstance(new URL(hostUrl
				+ digitalDocument.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(100f);

		imageSignCell = new PdfPCell(image2);
		imageSignCell.setBorder(0);
		
		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	} catch (Exception e) {
		e.printStackTrace();
	}

	if (imageSignCell != null) {
		signTable.addCell(imageSignCell);
	} else {
		Phrase blank1 = new Phrase(" ", font10);
		PdfPCell blank1Cell = new PdfPCell(blank1);
//		blank1Cell.setBorderWidthBottom(0);
//		blank1Cell.setBorderWidthTop(0);
		blank1Cell.setBorder(0);
		signTable.addCell(blank1Cell);
		signTable.addCell(blank1Cell);
		signTable.addCell(blank1Cell);
		signTable.addCell(blank1Cell);

	}
	
	String authSign="";
	if(checkBranchdata){
		if(branchDt.getPocName()!=null){
			authSign=branchDt.getPocName();
		}else{
			authSign=comp.getPocName();
		}
	}else{
		authSign=comp.getPocName();
	}
	
	
	if(digitalSignatureFlag){
		authSign = comp.getPocName();
	}
	
	Phrase authSignpr = new Phrase(authSign, font12);
	PdfPCell authSignprCell = new PdfPCell(authSignpr);
	authSignprCell.setBorder(0);
	authSignprCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	signTable.addCell(authSignprCell);
	
	
	
	
	
	String companyName="";
	if(checkBranchdata && branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
		companyName= branchDt.getCorrespondenceName();
		logger.log(Level.SEVERE,"companyName "+companyName);
	}
	else {
		companyName = comp.getBusinessUnitName().trim().toUpperCase();
	}
	
	
	
	logger.log(Level.SEVERE, "company name "+companyName);
	Phrase company = new Phrase(companyName, font12);
	PdfPCell companyCell = new PdfPCell(company);
	companyCell.setBorder(0);
	companyCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	signTable.addCell(companyCell);
	
	
	try {
		document.add(signTable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
	
	
	
	
	
}

private void footerPart() {
		

	if(digitalSignatureFlag){
		
	PdfPTable serviceDateInfoTable = new PdfPTable(1);
	serviceDateInfoTable.setWidthPercentage(100);
	
	Phrase serviceDate = new Phrase("Service Date", font12);
	PdfPCell serviceDateCell = new PdfPCell(serviceDate);
	serviceDateCell.setBorder(0);
	serviceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	serviceDateInfoTable.addCell(serviceDateCell);
	
	String serviceDateval = "Granted : "+ fmt.format(service.getServiceDate());
	Phrase pherviceDateval = new Phrase(serviceDateval, font12);
	PdfPCell pherviceDatevalCell = new PdfPCell(pherviceDateval);
	pherviceDatevalCell.setBorder(0);
	pherviceDatevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	serviceDateInfoTable.addCell(pherviceDatevalCell);
	
	if(validUntilDate!=null){
	String validUntil = "Valid Until : "+ fmt.format(validUntilDate);
	Phrase phvalidUntil = new Phrase(validUntil, font12);
	PdfPCell phvalidUntilCell = new PdfPCell(phvalidUntil);
	phvalidUntilCell.setBorder(0);
	phvalidUntilCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	serviceDateInfoTable.addCell(phvalidUntilCell);
	}
	
	try {
		document.add(serviceDateInfoTable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
	
	}
	
	PdfPTable licTable = new PdfPTable(2);
	licTable.setWidthPercentage(100);
	try {
		licTable.setWidths(new float[] {25,75});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	licTable.setSpacingBefore(60);
    String pestLicNo="Pesticide License No : ";
	
	Phrase phrase1=new Phrase(pestLicNo,font12);
	PdfPCell pestLicNoCell=new PdfPCell(phrase1);
	pestLicNoCell.setBorder(0);
	pestLicNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	pestLicNoCell.setPaddingBottom(10);
	
	    String blank2=" ";
		
		Phrase blankpr2=new Phrase(blank2,font12);
		PdfPCell blankpr2Cell=new PdfPCell(blankpr2);
		blankpr2Cell.setBorder(0);
//		blankpr2Cell.setPaddingBottom(10);
	
	licTable.addCell(pestLicNoCell);
	String lic = "";
	PdfPCell pestLicNoValueCell=null;
	for(int i=0;i<comp.getArticleTypeDetails().size();i++)
	{
		if(comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("ServiceCertificate")
				&&(comp.getArticleTypeDetails().get(i).getArticleTypeName() !=null && comp.getArticleTypeDetails().get(i).getArticleTypeValue() !=null))
		{
			if(comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")){
			lic = " "+comp.getArticleTypeDetails().get(i).getArticleTypeName()+": "+comp.getArticleTypeDetails().get(i).getArticleTypeValue();	

			Phrase phrase2=new Phrase(lic,font12bold);
			pestLicNoValueCell=new PdfPCell(phrase2);
			pestLicNoValueCell.setBorder(0);
			pestLicNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			pestLicNoValueCell.setPaddingBottom(10);
			licTable.addCell(pestLicNoValueCell);
			licTable.addCell(blankpr2Cell);
			}
		}
	}
	
	try {
		document.add(licTable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
	
	PdfPTable table3 = new PdfPTable(2);
	table3.setWidthPercentage(100);
	try {
		table3.setWidths(new float[] {50,50});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	table3.setSpacingBefore(20);
	
	
    String bmcRegNo="BMC Registration No :";
	
	Phrase phraseThree=new Phrase(bmcRegNo,font12);
	PdfPCell bmcRegNoCell=new PdfPCell(phraseThree);
	bmcRegNoCell.setBorder(0);
	bmcRegNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	bmcRegNoCell.setPaddingBottom(20);
	
    String blank=" ";
	
	Phrase blankpr=new Phrase(blank,font12);
	PdfPCell blankprCell=new PdfPCell(blankpr);
	blankprCell.setBorder(0);
	blankprCell.setPaddingBottom(10);
	
    String blank1=" ";
	
	Phrase blankpr1=new Phrase(blank1,font12);
	PdfPCell blankpr1Cell=new PdfPCell(blankpr1);
	blankpr1Cell.setBorder(0);
	blankpr1Cell.setPaddingBottom(20);
	
     String authSign="Signature of Authorised person ";
	
	Phrase authSignpr=new Phrase(authSign,font12);
	PdfPCell authSignprCell=new PdfPCell(authSignpr);
	authSignprCell.setBorder(0);
	authSignprCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	
	
//	table3.addCell(bmcRegNoCell);
//	table3.addCell(blankpr1Cell);
//	table3.addCell(blankpr1Cell);
//	table3.addCell(blankpr1Cell);
	
	
	table3.addCell(authSignprCell);
	table3.addCell(blankprCell);
//	table3.addCell(blankprCell);
//	table3.addCell(blankprCell);
	
	
	try {
		document.add(table3);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
}

private void createSecondPara() {
    PdfPTable table2 = new PdfPTable(1);
    table2.setWidthPercentage(100);
    table2.setSpacingBefore(10);
    
    
    
    String serviceCompDate="";
    if(service.getServiceCompletionDate()!=null){
    	serviceCompDate=fmt.format(service.getServiceCompletionDate());
    }else{
    	serviceCompDate="";
    }
    
	String para2= "";
	if(digitalSignatureFlag){
		para2 = "Your premise has been successfully treated by us";
	}
	else{
		if(validTillDate!=null)
			para2 = "As per our service contract,we carried out "+service.getProductName()+" on "+serviceCompDate+". This certificate is valid until "+fmt.format(validTillDate);
		else
			para2 = "As per our service contract,we carried out "+service.getProductName()+" on "+serviceCompDate+".";
		
	}
	
	Phrase phrase=new Phrase(para2,font12);
	PdfPCell secondParaCell=new PdfPCell(phrase);
	secondParaCell.setBorder(0);
	secondParaCell.setPaddingBottom(10);
	
	String para3= "";
	/**
	 *  Added By Priyanka Date : 30-01-2021
	 *  Des : company type != Pest control then below sentence will not print on PDF issue raised by Vaishnavi Mam.
	 * */
	
	if(comp.getCompanyType()!=null && comp.getCompanyType().equalsIgnoreCase("Pest Control")){
	if(!digitalSignatureFlag){
	    para3="This premise remains pest free immediately after our service.";
	}
	}

	Phrase phraseThree=new Phrase(para3,font12);
	PdfPCell thirdParaCell=new PdfPCell(phraseThree);
	thirdParaCell.setBorder(0);
	
	table2.addCell(secondParaCell);
	
	table2.addCell(thirdParaCell);
	
	
	
	try {
		document.add(table2);
	} catch (DocumentException e) {
		e.printStackTrace();
	}	
}

private void createFirstPara() {
	PdfPTable table1 = new PdfPTable(1);
	table1.setWidthPercentage(100);
	table1.setSpacingBefore(30);
	String custname = "";
	if(cust !=null){
		if(cust.getCompanyName() !=null && !cust.getCompanyName().equals("")){
			custname = "M/s "+cust.getCompanyName();	
		}
		else{
			custname = " "+cust.getFullname();
		}
		}
	String para="This is to certify that the premises of "+custname+" situated at "+service.getAddress().getCompleteAddress()+" is being treated by us on regular basis as per our service contract "+service.getContractCount()+" dated "+fmt.format(con.getContractDate())+".";
	
	Phrase phrase=new Phrase(para,font12);
	PdfPCell firstParaCell=new PdfPCell(phrase);
	firstParaCell.setBorder(0);
	firstParaCell.setVerticalAlignment(5);
	
	table1.addCell(firstParaCell);
	
	try {
		document.add(table1);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

private void createCompanyNameAsHeader(Document doc, Company comp) {
	
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createBlankforUPC() {

//	Phrase blankphrase=new Phrase("",font8);
//	PdfPCell blankCell=new PdfPCell();
//	blankCell.addElement(blankphrase);
//	blankCell.setBorder(0);
	
//	PdfPTable titlepdftable=new PdfPTable(3);
//	titlepdftable.setWidthPercentage(100);
//	titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
//	titlepdftable.addCell(blankCell);
//	titlepdftable.addCell(titlecell);
//	titlepdftable.addCell(blankCell);
	
	 Paragraph blank =new Paragraph();
	 blank.add(Chunk.NEWLINE);
	
//	PdfPTable parent=new PdfPTable(1);
//	parent.setWidthPercentage(100);
	
//	PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
//	parent.addCell(titlePdfCell);
	
	
	try {
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
//		document.add(parent);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	
	
	
}


public void createPestControlHeading() {
	
//	Phrase blank = new Phrase(" ");
//	PdfPCell blcell = new PdfPCell(blank); 
//	blcell.setBorder(0);
//	blcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	
//	Phrase blank1 = new Phrase(" ");
//	PdfPCell bl1cell = new PdfPCell(blank1); 
//	bl1cell.setBorder(0);
//	bl1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	
//	Phrase blank2 = new Phrase(" ");
//	PdfPCell bl2cell = new PdfPCell(blank2); 
//	bl2cell.setBorder(0);
//	bl2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase pest = new Phrase("TO WHOMSOEVER  IT  MAY  CONCERN",font14bold);
//	Phrase pest = new Phrase("Pest Control Certificate",font14bold);
	PdfPCell headcell = new PdfPCell(pest);
	headcell.setBorder(0);
	headcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
//	PdfPTable headtable = new PdfPTable(3);
////	headtable.addCell(blcell);
//	headtable.addCell(pestcell);
////	headtable.addCell(bl1cell);
	
	
//	PdfPCell cell = new PdfPCell(headtable);
//	cell.setBorder(0);

	PdfPTable table = new PdfPTable(1);
	table.setWidthPercentage(100f);
	table.addCell(headcell);
	
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

public void createLicenseHeading(){
	
	Phrase blank = new Phrase(" ");
	PdfPCell blankcell = new PdfPCell(blank);
	blankcell.setBorder(0);
	PdfPTable lictable = new PdfPTable(1);
	lictable.setWidthPercentage(100);
	Phrase lic = null;
	PdfPCell liccell=null;
	for(int i=0;i<comp.getArticleTypeDetails().size();i++)
	{
		if(comp.getArticleTypeDetails().get(i).getArticleTypeName() !=null && comp.getArticleTypeDetails().get(i).getArticleTypeValue() !=null)
		{
			lic = new Phrase(" "+comp.getArticleTypeDetails().get(i).getArticleTypeName()+" "+comp.getArticleTypeDetails().get(i).getArticleTypeValue(),font12bold);	
				
				liccell = new PdfPCell(lic);
				liccell.setHorizontalAlignment(Element.ALIGN_CENTER);
				liccell.setBorder(0);
				lictable.addCell(liccell);
		}
	}
	
	
	
	
//	Phrase licno = null;
//	if(comp.getArticleTypeDetails().get(0).getArticleTypeValue() !=null){
//		licno = new Phrase(" "+comp.getArticleTypeDetails().get(0).getArticleTypeValue());	
//	}
//	
//	PdfPCell licnocell = new PdfPCell(licno);
//	licnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	licnocell.setBorder(0);
	
	

//	lictable.addCell(licnocell);
	lictable.addCell(blankcell);
	lictable.addCell(blankcell);
	
	
	try {
		document.add(lictable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

public void createLine1(){
	
	Phrase blank = new Phrase(" ");
	PdfPCell blankcell = new PdfPCell(blank);
	blankcell.setBorder(0);
	
Phrase line1 = new Phrase("This is to certify that the Premises of"+"situated at "+"belonging to "+"having their registered office at ",font12);	
PdfPCell line1cell = new PdfPCell(line1);	
line1cell.setBorder(0);
line1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
    PdfPTable line1table = new PdfPTable(1);
    line1table.setWidthPercentage(100);
	line1table.addCell(line1cell);
	line1table.addCell(blankcell);
	line1table.setWidthPercentage(100);
	
	try {
		document.add(line1table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}
	
	public void createCustomerDetail(){
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
	Phrase custname = null;
	
	if(cust !=null){
	if(cust.getCompanyName() !=null && !cust.getCompanyName().equals("")){
		custname = new Phrase("M/s "+cust.getCompanyName(),font16boldul);	
	}
	else{
		custname = new Phrase(" "+cust.getFullname(),font16boldul);
	}
	}
	
	
	PdfPCell custnamecell = new PdfPCell(custname);
	custnamecell.setBorder(0);
	custnamecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPTable custnametable = new PdfPTable(1);
	custnametable.addCell(custnamecell);
	custnametable.addCell(blankcell);
	custnametable.setWidthPercentage(100);
	
	try {
		document.add(custnametable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

public void createCustomerAddress(){
	
	Phrase blank = new Phrase(" ");
	PdfPCell blankcell = new PdfPCell(blank);
	blankcell.setBorder(0);
	
	String custAdd1 = "";
	String custFullAdd1 = "";

	if (cust.getAdress() != null) {

		if (cust.getAdress().getAddrLine2() != null) {
			if (cust.getAdress().getLandmark() != null) {
				custAdd1 = cust.getAdress().getAddrLine1() + ","
						+ cust.getAdress().getAddrLine2() + ","
						+ cust.getAdress().getLandmark();
			} else {
				custAdd1 = cust.getAdress().getAddrLine1() + ","
						+ cust.getAdress().getAddrLine2();
			}
		} else {
			if (cust.getAdress().getLandmark() != null) {
				custAdd1 = cust.getAdress().getAddrLine1() + ","
						+ cust.getAdress().getLandmark();
			} else {
				custAdd1 = cust.getAdress().getAddrLine1();
			}
		}

		if (cust.getAdress().getLocality() != null) {
			custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
					+ "," + cust.getAdress().getState() + ","
					+ cust.getAdress().getCity() + "\n"
					+ cust.getAdress().getLocality() + " "
					+ cust.getAdress().getPin();

		} else {
			custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
					+ "," + cust.getAdress().getState() + ","
					+ cust.getAdress().getCity() + " "
					+ cust.getAdress().getPin();
		}
	}

	Phrase custAddInfo = new Phrase("Located at "+custFullAdd1+" has been pest controlled by "+"M/s "+comp.getBusinessUnitName()+" in accordance of the provisions of the Insecticides act, 1968 and rules thereunder."
 , font12);
	PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
	custAddInfoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	custAddInfoCell.setBorder(0);
	
	PdfPTable custaddtable = new PdfPTable(1);
	custaddtable.addCell(custAddInfoCell);
	custaddtable.addCell(blankcell);
	custaddtable.addCell(blankcell);
	custaddtable.addCell(blankcell);
	custaddtable.setWidthPercentage(100);
	
	try {
		document.add(custaddtable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}


public void createBlank(String stringdate){
	
	Phrase blank = new Phrase(" ");
	PdfPCell blankcell = new PdfPCell(blank);
	blankcell.setBorderWidthLeft(0);
	blankcell.setBorderWidthTop(0);
	blankcell.setBorderWidthRight(0);
	blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase bl1 = new Phrase(" ");
	PdfPCell bl1cell = new PdfPCell(bl1);
	bl1cell.setBorder(0);
	bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase bl2 = new Phrase(" ");
	PdfPCell bl2cell = new PdfPCell(bl2);
	bl2cell.setBorder(0);
//	bl2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase servdate = new Phrase("Service Date",font16);
	PdfPCell datecell = new PdfPCell(servdate);
	datecell.setBorder(0);
	datecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	
	Phrase granted = null;
	if(service.getServiceDate() !=null){
	granted = new Phrase("Granted: "+fmt.format(service.getServiceDate()) ,font16);	
	}
	
	PdfPCell dtcell = new PdfPCell(granted);
	dtcell.setBorder(0);
	dtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	/****************************************************************************************************/
	System.out.println("date before parsing"+stringdate);
	
	Date stringdate1 = null;
	if (stringdate != null) {
		SimpleDateFormat fmt123 = new SimpleDateFormat("EEE MMM dd HH:mm:ss z SSS yyyy");
		
		try {
			stringdate1 = fmt123.parse(stringdate);
			logger.log(Level.SEVERE,"date after parsing  ::: "+stringdate1);
			System.out.println("RRRRRRR    ref date " + stringdate1);
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
	}
	
	Phrase valid = null;
	if(stringdate1!=null){
		valid = new Phrase("Valid Until: "+fmt.format(stringdate1) ,font16);
	}else{
		valid = new Phrase("Valid Until: " ,font16);
	}
	PdfPCell validcell = new PdfPCell(valid);
	validcell.setBorder(0);
	validcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	PdfPTable bltable = new PdfPTable(2);
	bltable.addCell(bl1cell);
	bltable.addCell(datecell);
	bltable.addCell(bl2cell);
	bltable.addCell(bl2cell);
	bltable.addCell(bl1cell);
	bltable.addCell(dtcell);
	bltable.addCell(bl2cell);
	bltable.addCell(bl2cell);
	bltable.addCell(bl1cell);
	bltable.addCell(validcell);
	bltable.addCell(bl2cell);
	bltable.addCell(bl2cell);
	bltable.addCell(bl1cell);
	bltable.addCell(blankcell);
	
	bltable.setWidthPercentage(100);
	
	try {
		document.add(bltable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

public void createAuthoTitle(){
	
	Phrase bl = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(bl);
	blcell.setBorder(0);
//	bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase autho = new Phrase("Authorized Sign",font12);
	PdfPCell authocell = new PdfPCell(autho);
	authocell.setBorder(0);
	authocell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	PdfPTable authotable = new PdfPTable(1);
	authotable.addCell(blcell);
	authotable.addCell(authocell);
	authotable.setWidthPercentage(100);
	
	try {
		document.add(authotable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

private void createRexServiceCertificate(){
	PdfUtility pdfUtility = new PdfUtility();
	SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	Company comp = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
	
	BaseFont boldFont = null;
	try {
		boldFont = BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	Font font24boldul = new Font(boldFont, 24,Font.UNDERLINE);
	
	PdfPTable table = new PdfPTable(1);
	table.setWidthPercentage(100f);
	
//	table.addCell(pdfUtility.getPdfCell("Warranty Certificate", font24boldul, Element.ALIGN_CENTER, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	table.addCell(pdfUtility.getPdfCell("Warranty Certificate",font24boldul,Element.ALIGN_CENTER,Element.ALIGN_CENTER, 0,
			0,0,0,0,0, 0,0,0,20,0,0 ));
	
	PdfPTable infotable = new PdfPTable(3);
	infotable.setWidthPercentage(100f);
	try {
		infotable.setWidths(new float[]{17,3,80});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	infotable.addCell(pdfUtility.getPdfCell("Client Name", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(""+service.getPersonInfo().getFullName(), font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	
	infotable.addCell(pdfUtility.getPdfCell("Address", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	if(service.getAddress()!=null)
		infotable.addCell(pdfUtility.getPdfCell(""+service.getAddress().getCompleteAddress(), font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	else
		infotable.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));

	
	infotable.addCell(pdfUtility.getPdfCell("Mobile No.", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(""+cust.getCellNumber1(), font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));

	infotable.addCell(pdfUtility.getPdfCell("", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell("", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell("", font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));

	infotable.addCell(pdfUtility.getPdfCell("Certificate No", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	infotable.addCell(pdfUtility.getPdfCell(""+service.getCount(), font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));

	PdfPCell infotableCell=new PdfPCell(infotable);
	infotableCell.setBorder(0);
	table.addCell(infotableCell);
	
	
	PdfPTable validitytable = new PdfPTable(6);
	validitytable.setWidthPercentage(100f);
	try {
		validitytable.setWidths(new float[]{17,3,51,16,3,10});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	validitytable.addCell(pdfUtility.getPdfCell("Date of Issue", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	validitytable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	if(service.getServiceCompletionDate()!=null)
		validitytable.addCell(pdfUtility.getPdfCell(""+dateformat.format(service.getServiceCompletionDate()), font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	else
		validitytable.addCell(pdfUtility.getPdfCell(""+dateformat.format(service.getServiceDate()), font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	
	validitytable.addCell(pdfUtility.getPdfCell("Warranty End Date", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	validitytable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	if(validTillDate!=null)
		validitytable.addCell(pdfUtility.getPdfCell(""+dateformat.format(validTillDate), font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	else
		validitytable.addCell(pdfUtility.getPdfCell("", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
	

	if(service.getTechnicians()!=null){
		validitytable.addCell(pdfUtility.getPdfCell("Treated by", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
		validitytable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));		
		String technicians="";
		int count=1;
		for(EmployeeInfo e:service.getTechnicians()){
			technicians+="("+count+") "+e.getFullName()+"\n";
			count++;
		}
		validitytable.addCell(pdfUtility.getPdfCell(technicians, font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 4, 0, 0, 0, 0, 0, 0, 0));
	}else if(service.getEmployee()!=null&&!service.getEmployee().equals("")){
		validitytable.addCell(pdfUtility.getPdfCell("Treated by", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));
		validitytable.addCell(pdfUtility.getPdfCell(":", font8bold, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 0, 0, 0, 0, 0, 0, 0, 0));		
		validitytable.addCell(pdfUtility.getPdfCell("(1) "+service.getEmployee(), font8, Element.ALIGN_LEFT, Element.ALIGN_CENTER, 0, 4, 0, 0, 0, 0, 0, 0, 0));
	}
	
	PdfPCell validitytableCell=new PdfPCell(validitytable);
	validitytableCell.setBorder(0);
	validitytableCell.setPaddingBottom(10);
	table.addCell(validitytableCell);
	
	Font font12boldulgreen=new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	font12boldulgreen.setColor(BaseColor.GREEN);
	table.addCell(pdfUtility.getPdfCell("WARRANTY TERMS:",font12boldulgreen,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,10,0,0 ));
	
	Phrase para1 = new Phrase();
	Chunk chk1 = new Chunk(""+comp.getBusinessUnitName(), font12bold);//REX Environment Science Private Limited 
	Chunk chk2 = new Chunk("hereby certifies that the ", font12);
	Chunk chk3 = new Chunk(service.getProductName(), font12boldul);
	Chunk chk4 = new Chunk(" carried out at the above-mentioned address is performed using premium quality chemicals", font12);
	para1.add(chk1);
	para1.add(chk2);
	para1.add(chk3);
	para1.add(chk4);
	if(project!=null){
		if(project!=null&&project.getProdDetailsList()!=null&&project.getProdDetailsList().size()!=0){
			String material=" i.e. ";
			int count=1;
			for(ProductGroupList group:project.getProdDetailsList()){
				if(count>1)
					material+=", "+group.getName();
				else{
					material+=group.getName();
					count++;
				}
			}
			Chunk chk5 = new Chunk(material, font12bold);
			para1.add(chk5);
		}
			
	}
	
	Chunk chk6 = new Chunk(" and as per industry-approved techniques and IS standards.", font12);
	para1.add(chk6);
	
	
	PdfPCell para1Cell=new PdfPCell(para1);
	para1Cell.setBorder(0);
	para1Cell.setPaddingBottom(5);
	table.addCell(para1Cell);
	
	
	Phrase para2 = new Phrase();
	Chunk chk21 = new Chunk("This Warranty Certificate guarantees the Post Construction Anti Termite Treatment for a period of ", font12);
	double year=0;
	DecimalFormat df = new DecimalFormat("0.0");
	if(service.getCompletedDate()!=null){
		long difference=0;
		int  diffDays=0;
		
		Date validdate = DateUtility.getDateWithTimeZone("IST", validTillDate);
		difference= validdate.getTime()  - DateUtility.getDateWithTimeZone("IST",service.getCompletedDate()).getTime();
		
		float daysBetween = (difference / (1000*60*60*24));
		
		year=daysBetween/365;
		logger.log(Level.SEVERE,"daysBetween="+daysBetween+"year="+year); 
		
	}else{
		long difference=0;
		int  diffDays=0;
		Date validdate = DateUtility.getDateWithTimeZone("IST", validTillDate);
		difference= validdate.getTime()  - DateUtility.getDateWithTimeZone("IST",service.getServiceDate()).getTime();
		
		float daysBetween = (difference / (1000*60*60*24));
		year=daysBetween/365;
		logger.log(Level.SEVERE,"daysBetween="+daysBetween+"year="+year); 
		
	}	
	
	Chunk chk22 = new Chunk(df.format(year)+" years", font12bold);
	Chunk chk23 = new Chunk(" from the date of treatment. Should the treated premises show any signs of termite infestation during the period covered by this warranty:", font12);
	para2.add(chk21);
	para2.add(chk22);
	para2.add(chk23);
	PdfPCell para2Cell=new PdfPCell(para2);
	para2Cell.setBorder(0);
	para2Cell.setPaddingBottom(5);
	table.addCell(para2Cell);
	
	String point1="1. "+comp.getBusinessUnitName()+" commits to provide the necessary retreatment at no additional charge.";
	String point2="2. All retreatment will be done in accordance with our standard procedures and quality norms.";
	table.addCell(pdfUtility.getPdfCell(point1,font12,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell(point2,font12,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,5,0,0 ));
	
	Font font8boldulblue=new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	font8boldulblue.setColor(BaseColor.BLUE);
	table.addCell(pdfUtility.getPdfCell("LIMITATIONS & CONDITIONS:",font8boldulblue,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,10,10,0,0 ));
	
	table.addCell(pdfUtility.getPdfCell("1. This warranty is non-transferable and valid only for the address mentioned above.",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell("2. It is the client's responsibility to notify "+comp.getBusinessUnitName()+" immediately upon any signs of termite re-infestation.",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell("3. "+comp.getBusinessUnitName()+" must be granted access to the premises to inspect and evaluate the condition if reported.",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell("4. Any structural or environmental changes at the treated premises that can lead to termite re-infestation would void this warranty.",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell("5. This warranty does not cover damages caused by termites or any other pests, nor any incidental or consequential damages.",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell("6. If the Home owner fails to do all the necessary rectifications suggested by "+comp.getBusinessUnitName()+" (i.e. Change damage furniture, structural changes, repair wet surface near treatment areas etc.)",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell("7. If the new termite infestation occurs from the untreated part of the structure.",font8,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,5,0,0 ));
	
	Font font12boldulred=new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	font12boldulred.setColor(BaseColor.RED);
	table.addCell(pdfUtility.getPdfCell("CLAIM PROCEDURE:",font12boldulred,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,10,10,0,0 ));
	table.addCell(pdfUtility.getPdfCell("For any claims under this warranty, the client should contact "+comp.getBusinessUnitName()+" through our official contact channels along with the original copy of this certificate and details of the problem faced.",font12,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,5,0,0 ));
	table.addCell(pdfUtility.getPdfCell("For, "+comp.getBusinessUnitName()+",",font12bold,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,10,40,0,0 ));
	table.addCell(pdfUtility.getPdfCell("Authorized Signature:",font12bold,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	table.addCell(pdfUtility.getPdfCell(comp.getPocName(),font12,Element.ALIGN_LEFT,Element.ALIGN_CENTER, 0,
			0,0,0,0,0,0,0,0,0,0,0 ));
	
	
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}

}
}
