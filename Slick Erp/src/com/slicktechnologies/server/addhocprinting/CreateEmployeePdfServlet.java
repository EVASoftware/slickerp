package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class CreateEmployeePdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4016402355466777845L;


	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("application/pdf");
		try {
			
			String stringid = request.getParameter("Id");
			String pdfType = request.getParameter("type");
			String subtype = request.getParameter("subtype");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("inside servlet");

			if(pdfType.equals("EmployeeForm")){
			
				if(subtype.equals("EmployeeFormPortrate")){
					Employee employee = ofy().load().type(Employee.class).id(count).now();
					/**
					 * Date : 19-07-2018 BY ANIL
					 * for both type of employee ,we will print bio data pdf as per Nitin sir's instruction
					 */
//					if(!employee.getNumberRange().trim().equalsIgnoreCase("Direct")){
//						EmployeePdf pdf = new EmployeePdf();
//						pdf.document = new Document();
//						Document document = pdf.document;
//						PdfWriter.getInstance(document, response.getOutputStream()); 
//						document.open();
//						pdf.getEmployeeDeatils(count);
//						pdf.createPdf();
//						document.close();
//					}else if(employee.getNumberRange().trim().equalsIgnoreCase("Direct")){
						
						EmployeeBio_Data bippdf = new EmployeeBio_Data();
						bippdf.document = new Document(PageSize.A4,15,15,10,10);
						Document document = bippdf.document;
						PdfWriter.getInstance(document, response.getOutputStream()); 
						
						document.open();
						bippdf.getEmployeeDeatils(count);
						bippdf.createPdf();
						document.close();
//					}
				}
			}
			else if(pdfType.equals("EmployeeIDCard")){
				if(subtype.equals("EmployeeIDCardPortrate")){
					EmployeeIDCardPdf cardpdf = new EmployeeIDCardPdf();
					//Rectangle pageSize = new Rectangle(149,240);
					cardpdf.document = new Document(PageSize.A4,20,20,20,20);
					Document document = cardpdf.document;
					PdfWriter.getInstance(document, response.getOutputStream()); 
					document.open();
					cardpdf.getEmployeeDeatils(count);
					cardpdf.createPdf();
					document.close();
				}
				else if(subtype.equals("EmployeeIDCardLandscape")){

					/**
					 * @author Anil @since 18-03-2021
					 * Width  : 86mm - 8.6cm - 3.38583inch - 243.77976
					 * Height : 54mm - 5.4cm - 2.12598inch - 153.07056
					 */
					EmployeeIDCardlandscapePdf cardpdf = new EmployeeIDCardlandscapePdf();
//					Rectangle pageSize = new Rectangle(149,240);
					Rectangle pageSize = new Rectangle(153.07056f,243.77976f);
					cardpdf.document = new Document(pageSize.rotate(),0,0,0,0);
					Document document = cardpdf.document;
					PdfWriter.getInstance(document, response.getOutputStream()); 
					document.open();
					cardpdf.getEmployeeDeatils(count);
					cardpdf.createPdf();
					document.close();
				
				}
				
			}else if(pdfType.equals("employmentCard")){
				EmploymentCardPdf empCardPdf = new EmploymentCardPdf();
				// width 5.5 cm : 2.17 inch * 72 =157;; height 8.5 cm:3.34 inch * 72=242
//				Rectangle pageSize = new Rectangle(157,242);
//				pageSize.setBorder(Rectangle.BOX);
//				pageSize.setBorderWidth(3);
//				pageSize.setBorderColor(BaseColor.BLACK);
				empCardPdf.document = new Document(PageSize.A4,2,2,2,2);
				Document document = empCardPdf.document;
				PdfWriter.getInstance(document, response.getOutputStream()); 
				
				document.open();
				empCardPdf.loadEmployeeData(count);
				empCardPdf.createPdf();
				document.close();
			}
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	
	
}
