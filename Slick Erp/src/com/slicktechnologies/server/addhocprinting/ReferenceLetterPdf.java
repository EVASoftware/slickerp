package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class ReferenceLetterPdf {

	public Document document;
	
	Company comp;
	Customer cust;
	Phrase colon;
	Phrase chunk;
	Font bul;
	Chunk bullet; 
	float[] col1width = { 0.9f, 0.1f, 7.0f, 0.9f, 0.1f, 1.5f};

	private Font font16boldul,font17boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	
	Logger logger = Logger.getLogger("NameOfYourLogger");

	

	DecimalFormat df = new DecimalFormat("0.00");
	
//	private PdfPCell custlandcell;
	
	public ReferenceLetterPdf()
    {
	new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font17boldul = new Font(Font.FontFamily.HELVETICA, 17,Font.BOLD);
	font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	font8 = new Font(Font.FontFamily.HELVETICA, 8);
	font9 = new Font(Font.FontFamily.HELVETICA, 9);
	font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	font12 = new Font(Font.FontFamily.HELVETICA, 12);
	font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
	font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
	font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	bul = new Font(FontFamily.ZAPFDINGBATS,6);
}

	public void setrefletterquotation(Long count) {
		
		cust = ofy().load().type(Customer.class).id(count).now();
		
		if (cust.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", cust.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();
		
	}

	public void createPdf(String refNo,String refDt,String designation,String type){
		
		System.out.println("refNo "+refNo);
		System.out.println("refDt "+refDt);
		System.out.println("designation "+designation);
		System.out.println("type "+type);
		
//		if(status.equals("yes"))
//		{
//			createBlank1Heading();
//			createLogo(document,comp);
//		}
//		else if(status.equals("no"))
//		{
//			createBlank1Heading();
//		    	if(comp.getUploadHeader()!=null){
//				createCompanyNameAsHeader(document,comp);
//				}
				
//				if(comp.getUploadFooter()!=null){
//				createCompanyNameAsFooter(document,comp);
//				}
//		}
		
		
		
		createBlank1Heading();
		createTitleHeading();
		createReferenceHeading(refNo,refDt);
		createCustomerDetailsHeading(designation);
		createServicesHeading(type);
		createHeading();
		
	}
	
	public void createTitleHeading(){
		
		System.out.println("inside create tilte heading");
		
		String title = "Reference Letter";
		Phrase quotation = new Phrase(title,font14bold);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase blk = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blk);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.addElement(qtable);
		cell2.setBorder(0);
		
		PdfPTable table1 = new PdfPTable(1);
		table1.addCell(cell2);
		table1.setWidthPercentage(100);
	
		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
//private void createCompanyNameAsHeader(Document doc, Company comp) {
//		
//		DocumentUpload document =comp.getUploadHeader();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(15f);
//			image2.scaleAbsoluteWidth(520f);
//			image2.setAbsolutePosition(40f,725f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		
//		
////		try
////		{
////			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(520f);
////		image1.setAbsolutePosition(40f,725f);	
////		doc.add(image1);
////		}
////		catch(Exception e)
////		{
////			e.printStackTrace();
////		}
//		}

//	private void createCompanyNameAsFooter(Document doc, Company comp) {
//		
//		
//		DocumentUpload document =comp.getUploadFooter();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(15f);
//			image2.scaleAbsoluteWidth(520f);
//			image2.setAbsolutePosition(40f,40f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
////		try
////		{
////		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(520f);
////		image1.setAbsolutePosition(40f,40f);	
////		doc.add(image1);
////		}
////		catch(Exception e)
////		{
////			e.printStackTrace();
////		}
//		}

	public void createBlank1Heading() {
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
//		PdfPCell cell = new PdfPCell();
//		cell.addElement(bltable);
//		
//		
//		cell.setBorderWidthBottom(0);
//		cell.setBorderWidthRight(0);
//		cell.setBorderWidthLeft(0);
//		cell.setBorderWidthTop(0);
//		
//		
//		PdfPTable table = new PdfPTable(1);
//		table.addCell(cell);
//		table.setWidthPercentage(100);
//	
		try {
			document.add(bltable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createReferenceHeading(String refNo, String refDate){
		
		Phrase ref = new Phrase("Ref. No.",font10);
		PdfPCell refcell = new PdfPCell(ref);
		refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase refNum = null;
		
		if(refNo !=null){
			refNum = new Phrase(refNo,font10);	
		}
		
		PdfPCell rcell = new PdfPCell(refNum);
		rcell.setBorder(0);
		rcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":",font10);
		PdfPCell colcell = new PdfPCell(colon);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date",font10);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		logger.log(Level.SEVERE, "date after parsing before parsing"+refDate);	
		Phrase refdt = null;
		Date refDt = null;
		
		if(refDate!=null){
			 SimpleDateFormat fmt123 = new SimpleDateFormat("EEE MMM dd HH:mm:ss z SSS yyyy");
		try {
			refDt = fmt123.parse(refDate);
		logger.log(Level.SEVERE, "date after parsing "+refDt);	
		System.out.println("RRRRRRR    ref date "+refDt);	
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("aaaaaaaaaaaaaa"+fmt.format(refDt));
			refdt = new Phrase(fmt.format(refDt),font10);
		}
		
		
		
		PdfPCell dtcell = new PdfPCell(refdt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank); 
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable rdttable = new PdfPTable(6);
		rdttable.addCell(refcell);
		rdttable.addCell(colcell);
		rdttable.addCell(rcell);
		rdttable.addCell(datecell);
		rdttable.addCell(colcell);
		rdttable.addCell(dtcell);
		rdttable.setWidthPercentage(100f);
		
		try {
			 rdttable.setWidths(col1width);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell cell1 = new PdfPCell(rdttable); 		
		cell1.setBorder(0);
		
		PdfPTable table1 = new PdfPTable(1);
		table1.addCell(cell1);
		table1.addCell(blankcell);
		table1.setWidthPercentage(100);
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	public void createCustomerDetailsHeading(String designation){
		
		
		Phrase to = new Phrase("To,",font9);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setBorder(0);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase designationPhrase = new Phrase(designation,font9);
		PdfPCell designationCell = new PdfPCell(designationPhrase);
		designationCell.setBorder(0);
		designationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase comname=null;
		if(cust.getCompanyName()!=null)
		{
			comname= new Phrase(cust.getCompanyName(),font9);
		}
		else
		{
			comname= new Phrase(cust.getFullname(),font9);
		}
		
		PdfPCell comnameCell = new PdfPCell(comname);
		comnameCell.setBorder(0);
		comnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String custAdd1="";
		String custFullAdd1="";
		
	if(cust.getAdress()!=null){
			
			if(!cust.getAdress().getAddrLine2().equals("")){
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
				}
			}else{
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(!cust.getAdress().getLocality().equals("")){
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
			}
		}
		
		Phrase mobno = null;
		if(cust.getLandline() !=null && cust.getLandline()!=0){
			mobno = new Phrase("Tel No. : "+cust.getCellNumber1()+", 022-"+cust.getLandline(),font9);
		}
		else
		{
			mobno = new Phrase("Tel No. : "+cust.getCellNumber1(),font9);
		}
		
		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		Phrase blank = new Phrase(" ", font9);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankcell.setBorder(0);
		
		Phrase dear = new Phrase("Dear Sir/Madam"+","+"\n",font9);
		PdfPCell dearcell = new PdfPCell(dear);
		dearcell.setBorder(0);
		dearcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase msg = new Phrase("We would like to take this opportunity to introduce ourselves as reliable and innovative pest management company in Mumbai. We offer integrated solutions for your pest management needs through a comprehensive range of professional services. Our knowledgeable, qualified and experienced people are always at hand to provide you effective services. we have developed pest control methods which are best suited to individual environment. ",font9);
		PdfPCell msgcell = new PdfPCell(msg);
		msgcell.setBorder(0);
		msgcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable cdtable = new PdfPTable(1);
		cdtable.addCell(toCell);
		cdtable.addCell(designationCell);
		cdtable.addCell(comnameCell);
		cdtable.addCell(custAddInfoCell);
		cdtable.addCell(mobcell);
//		cdtable.addCell(pnamecell);
		cdtable.addCell(blankcell);
//		cdtable.addCell(subcell);
//		cdtable.addCell(blankCell);
		cdtable.addCell(dearcell);
		cdtable.addCell(msgcell);

		cdtable.setWidthPercentage(100f);

		PdfPCell cell2 = new PdfPCell(cdtable);
		cell2.setBorder(0);
		
		PdfPTable table2 = new PdfPTable(1);
		table2.addCell(cell2);
//		table2.addCell(blankcell);
		table2.setWidthPercentage(100);
		
		try {
			document.add(table2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createServicesHeading(String type){
		
		
		bullet = new Chunk(String.valueOf((char)108), bul);
		
		Phrase ph = new Phrase("Our Services For Residence, Society, Commercial and Industies: ",font9);
		PdfPCell phcell = new PdfPCell(ph);
		phcell.setBorder(0);
		
		Phrase ph1 = new Phrase("- General(Cockroaches) Pest Managemnt(GD)",font9bold);
		PdfPCell ph1cell = new PdfPCell(ph1);
		ph1cell.setBorder(0);
		
		Phrase ph2 = new Phrase("- Rodent Management(RM)",font9bold);
		PdfPCell ph2cell = new PdfPCell(ph2);
		ph2cell.setBorder(0);
		
		Phrase ph3 = new Phrase("- Termite Management(Per/Post Construction)(T.M)",font9bold);
		PdfPCell ph3cell = new PdfPCell(ph3);
		ph3cell.setBorder(0);
		
		Phrase ph4 = new Phrase("- Bed Bugs Management(BBM)",font9bold);
		PdfPCell ph4cell = new PdfPCell(ph4);
		ph4cell.setBorder(0);
		
		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(phcell);
		lefttable.addCell(ph1cell);	
		lefttable.addCell(ph2cell);
		lefttable.addCell(ph3cell);
		lefttable.addCell(ph4cell);
		lefttable.setWidthPercentage(100f);
		
		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		
		Phrase ph5 = new Phrase("- Wood Borer Management(WBM)",font9bold);
		PdfPCell ph5cell = new PdfPCell(ph5);
		ph5cell.setBorder(0);

		Phrase ph6 = new Phrase("- Molluscs Management",font9bold);
		PdfPCell ph6cell = new PdfPCell(ph6);
		ph6cell.setBorder(0);
		
		Phrase ph7 = new Phrase("- Fly and Mosquitoes Management",font9bold);
		PdfPCell ph7cell = new PdfPCell(ph7);
		ph7cell.setBorder(0);
		
		Phrase ph8 = new Phrase("- Weed Management",font9bold);
		PdfPCell ph8cell = new PdfPCell(ph8);
		ph8cell.setBorder(0);
		
		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(blankcell);
		righttable.addCell(ph5cell);
		righttable.addCell(ph6cell);	
		righttable.addCell(ph7cell);
		righttable.addCell(ph8cell);
		righttable.setWidthPercentage(100f);
		
		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(2);
		parenttable.addCell(leftcell);
		parenttable.addCell(rightcell);
//		parenttable.addCell(blankcell);
//		parenttable.addCell(blankcell);
		parenttable.setWidthPercentage(100f);
		
		try {
			parenttable.setWidths(new float[]{65,35});
			} catch (DocumentException e1) {
			e1.printStackTrace();
			}
		
		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/********************************************************************************************/

		if(type.equalsIgnoreCase("all"))
		{
		Phrase ph9 = new Phrase("We are serving many customers As: ",font9);
		PdfPCell ph9cell = new PdfPCell(ph9);
		ph9cell.setBorder(0);
		
		Phrase ph10 = new Phrase("- Corporate Offices & Banks",font9bold);
		PdfPCell ph10cell = new PdfPCell(ph10);
		ph10cell.setBorder(0);
		
		Phrase ph11 = new Phrase("- Co-operative Societies",font9bold);
		PdfPCell ph11cell = new PdfPCell(ph11);
		ph11cell.setBorder(0);
		
		Phrase ph12 = new Phrase("- Vehicles",font9bold);
		PdfPCell ph12cell = new PdfPCell(ph12);
		ph12cell.setBorder(0);
		
		PdfPTable left1table = new PdfPTable(1);
		left1table.addCell(ph9cell);
		left1table.addCell(ph10cell);
		left1table.addCell(ph11cell);
		left1table.addCell(ph12cell);
		
		PdfPCell left1cell = new PdfPCell(left1table);
		left1cell.setBorder(0);
		
		
		Phrase ph13 = new Phrase("- Hospitals, Hotels & Restaurants",font9bold);
		PdfPCell ph13cell = new PdfPCell(ph13);
		ph13cell.setBorder(0);

		Phrase ph14 = new Phrase("- Individual Flats And Bunglows",font9bold);
		PdfPCell ph14cell = new PdfPCell(ph14);
		ph14cell.setBorder(0);
		
		Phrase ph15 = new Phrase("- Warehouses",font9bold);
		PdfPCell ph15cell = new PdfPCell(ph15);
		ph15cell.setBorder(0);
		
		PdfPTable right1table = new PdfPTable(1);
		right1table.addCell(blankcell);
		right1table.addCell(ph13cell);
		right1table.addCell(ph14cell);
		right1table.addCell(ph15cell);
		
		PdfPCell right1cell = new PdfPCell(right1table);
		right1cell.setBorder(0);
		
		PdfPTable parent1table = new PdfPTable(2);
		parent1table.addCell(left1cell);
		parent1table.addCell(right1cell);
//		parent1table.addCell(blankcell);
//		parent1table.addCell(blankcell);
		parent1table.setWidthPercentage(100f);
		
		try {
			parent1table.setWidths(new float[]{65,35});
			} catch (DocumentException e1) {
			e1.printStackTrace();
			}
		
		try {
			document.add(parent1table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		else
		{
			Phrase ph9 = new Phrase("Following Banks are our already Customers: ",font9);
			PdfPCell ph9cell = new PdfPCell(ph9);
			ph9cell.setBorder(0);
			
			Phrase ph10 = new Phrase("- Branches of Union Bank of India",font9bold);
			PdfPCell ph10cell = new PdfPCell(ph10);
			ph10cell.setBorder(0);
			
			Phrase ph11 = new Phrase("- Branches of Bharat Co Op Bank Ltd",font9bold);
			PdfPCell ph11cell = new PdfPCell(ph11);
			ph11cell.setBorder(0);
			
			Phrase ph12 = new Phrase("- Branches of Corporation Bank",font9bold);
			PdfPCell ph12cell = new PdfPCell(ph12);
			ph12cell.setBorder(0);
			

			Phrase ph13 = new Phrase("- Branches of Deccan Merchant Bank",font9bold);
			PdfPCell ph13cell = new PdfPCell(ph13);
			ph13cell.setBorder(0);
			

			Phrase ph14 = new Phrase("- Branches of Central Bank of India",font9bold);
			PdfPCell ph14cell = new PdfPCell(ph14);
			ph14cell.setBorder(0);
			

			Phrase ph15 = new Phrase("- Branches of Industrial Development Bank of India",font9bold);
			PdfPCell ph15cell = new PdfPCell(ph15);
			ph15cell.setBorder(0);
			

			Phrase ph16 = new Phrase("- Branches of State Bank Hyderabad",font9bold);
			PdfPCell ph16cell = new PdfPCell(ph16);
			ph16cell.setBorder(0);
			

			Phrase ph17 = new Phrase("- Branches of SBBJ",font9bold);
			PdfPCell ph17cell = new PdfPCell(ph17);
			ph17cell.setBorder(0);
			
			PdfPTable left1table = new PdfPTable(1);
			left1table.addCell(ph9cell);
			left1table.addCell(ph10cell);
			left1table.addCell(ph11cell);
			left1table.addCell(ph12cell);
			left1table.addCell(ph13cell);
			left1table.addCell(ph14cell);
			left1table.addCell(ph15cell);
			left1table.addCell(ph16cell);
			left1table.addCell(ph17cell);
			
			PdfPCell left1cell = new PdfPCell(left1table);
			left1cell.setBorder(0);
			
			Phrase rowblank = new Phrase(" ",font9bold);
			PdfPCell rowblankCell = new PdfPCell(rowblank);
			rowblankCell.setBorder(0);
			
			Phrase ph20 = new Phrase("- Branches of Canara Bank",font9bold);
			PdfPCell ph20cell = new PdfPCell(ph20);
			ph20cell.setBorder(0);
			
			Phrase ph21 = new Phrase("- Branches of State Bank of India",font9bold);
			PdfPCell ph21cell = new PdfPCell(ph21);
			ph21cell.setBorder(0);
			
			Phrase ph22 = new Phrase("- Branches of Bank of Baroda",font9bold);
			PdfPCell ph22cell = new PdfPCell(ph22);
			ph22cell.setBorder(0);
			

			Phrase ph23 = new Phrase("- Branches of State Bank of Patiala",font9bold);
			PdfPCell ph23cell = new PdfPCell(ph23);
			ph23cell.setBorder(0);
			

			Phrase ph24 = new Phrase("- Branches of UCO Bank",font9bold);
			PdfPCell ph24cell = new PdfPCell(ph24);
			ph24cell.setBorder(0);
			

			Phrase ph25 = new Phrase("- Branches of Bank of India",font9bold);
			PdfPCell ph25cell = new PdfPCell(ph25);
			ph25cell.setBorder(0);
			

			Phrase ph26 = new Phrase("- Branches of Dena Bank ",font9bold);
			PdfPCell ph26cell = new PdfPCell(ph26);
			ph26cell.setBorder(0);
			

			Phrase ph27 = new Phrase("- Branches of SBBJ",font9bold);
			PdfPCell ph27cell = new PdfPCell(ph27);
			ph27cell.setBorder(0);
			
			PdfPTable righthandtable = new PdfPTable(1);
			righthandtable.addCell(rowblankCell);
			righthandtable.addCell(ph20cell);
			righthandtable.addCell(ph21cell);
			righthandtable.addCell(ph22cell);
			righthandtable.addCell(ph23cell);
			righthandtable.addCell(ph24cell);
			righthandtable.addCell(ph25cell);
			righthandtable.addCell(ph26cell);
			righthandtable.addCell(ph27cell);
			
			PdfPCell righthandtableCell = new PdfPCell(righthandtable);
			righthandtableCell.setBorder(0);
			
			
			PdfPTable parentforBothtable = new PdfPTable(2);
			parentforBothtable.addCell(left1cell);
			parentforBothtable.addCell(righthandtableCell);
//			parentforBothtable.addCell(blankcell);
//			parentforBothtable.addCell(blankcell);
			parentforBothtable.setWidthPercentage(100f);
			
			try {
				parentforBothtable.setWidths(new float[]{65,35});
				} catch (DocumentException e1) {
				e1.printStackTrace();
				}
			
			try {
				document.add(parentforBothtable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		/*******************************************************************/
		
		Phrase line = new Phrase("Our Speciality: ",font9);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setBorder(0);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase line2 = new Phrase("- Advance Gel technology or Odorless Spray treatment for Cockroaches, Ants(Red-Blank), Silver Fish, Fire Brats & Spiders."+"\n"+"- Special treatment for rodents, rats, woodborer"+"\n"+"- Our treatment are Eco Friendly and Safe."+"\n"+"- No need to remove utensils and eatable items."+"\n"+"- No need to close Flats, Office and Bank after or before Service."+"\n"+"- We are members of Pest Managemnet Association & An ISO 9001-2000 company."+"\n"+"\n"+"Please refer attachment for more information and other services. Contact us immediately for free inspection and suggestions. We assure you best services always. ",font10);
//		PdfPCell line2Cell = new PdfPCell(line2);
//		line2Cell.setBorder(0);
//		line2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		PdfPTable abctable = new PdfPTable(1); 
//		abctable.addCell(line1cell);
//		abctable.addCell(line2Cell);
//		abctable.addCell(blankcell);
//		abctable.setWidthPercentage(100f);
//		
//		PdfPCell cell3 = new PdfPCell(abctable);
//		cell3.setBorder(0);
//		
//		PdfPTable table3 = new PdfPTable(1);
//		table3.addCell(cell3);
//		table3.addCell(blankcell);
//		table3.setWidthPercentage(100);
//		
//		try {
//			document.add(table3);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		/*********************************************************************************/
		
	String msg =  "-"+"    "+"Advance Gel technology or Odorless Spray treatment for Cockroaches, Ants(Red-Black), Silver Fish, Fire Brats &   Spiders.";
	String msg1 = "-"+"    "+"Special treatment for rodents, rats, woodborer.";
	String msg2 = "-"+"    "+"Our treatment are Eco Friendly,Nontoxic and Safe.";
	String msg6 = "-"+"    "+"There are no side effects for Old peoples,Childrens,Asthmatic patients and Pets.";
	String msg3 = "-"+"    "+"No need to remove utensils and eatable items.";
	String msg4 = "-"+"    "+"No need to close Flats, Office and Bank after or before Service.";
	String msg7 = "-"+"    "+"We Provide Guaranteed Services at a Reasonable Rate.";
	String msg5 = "-"+"    "+"We are members of 'Pest Managemnet Association' & An ISO 9001-2000 company.";
	
	
	Phrase line7 = new Phrase(msg6,font9);	 
	Phrase line8 = new Phrase(msg7,font9);	 
	
	Phrase line1 = new Phrase(msg,font9);	 
//	line1.add(bullet);
//	line1.add(msg);
	
	PdfPCell line1cell = new PdfPCell(line1);
	line1cell.setBorder(0);
	
	Phrase line2 = new Phrase(msg1,font9);	 
//	line2.add(bullet);
//	line2.add(msg1);
	
	PdfPCell line2cell = new PdfPCell(line2);
	line2cell.setBorder(0);
		
	Phrase line3 = new Phrase(msg2,font9);	 
//	line3.add(bullet);
//	line3.add(msg2);
	
	PdfPCell line3cell = new PdfPCell(line3);
	line3cell.setBorder(0);	
		
	Phrase line4 = new Phrase(msg3,font9);	 
//	line4.add(bullet);
//	line4.add(msg3);
	
	PdfPCell line4cell = new PdfPCell(line4);
	line4cell.setBorder(0);	
		
	Phrase line5 = new Phrase(msg4,font8);	 
//	line5.add(bullet);
//	line5.add(msg4);
	
	PdfPCell line5cell = new PdfPCell(line5);
	line5cell.setBorder(0);	
		
	Phrase line6 = new Phrase(msg5,font8);	 
//	line6.add(bullet);
//	line6.add(msg5);
	
	PdfPCell line6cell = new PdfPCell(line6);
	line6cell.setBorder(0);	
	
	
	
	PdfPCell line7cell = new PdfPCell(line7);
	line7cell.setBorder(0);	
	
	PdfPCell line8cell = new PdfPCell(line8);
	line8cell.setBorder(0);	
	
		
	PdfPTable msgtable = new PdfPTable(1);
	msgtable.addCell(linecell);
	msgtable.addCell(line1cell);	
	msgtable.addCell(line2cell);
	msgtable.addCell(line7cell);
	msgtable.addCell(line3cell);
	msgtable.addCell(line4cell);
	msgtable.addCell(line5cell);
	msgtable.addCell(line8cell);
	msgtable.addCell(line6cell);
	msgtable.setWidthPercentage(100f);
	
	PdfPCell cell3 = new PdfPCell(msgtable);
	cell3.setBorder(0);
	
	PdfPTable table3 = new PdfPTable(1);
	table3.addCell(cell3);
//	table3.addCell(blankcell);
	table3.setWidthPercentage(100);
		
	try {
		document.add(table3);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		
		
		
		
		
	}	
	
	public void createHeading(){
		
		
		Phrase note = new Phrase("Please refer attachment for more information and other services. Contact us immediately for free inspection and suggestions. We assure you best services always.",font9);
		PdfPCell notecell = new PdfPCell(note);
		notecell.setBorder(0);
		
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankcell.setBorder(0);
		
	Phrase abc1 = new Phrase("Call us for our Quotation or free inspection"+"\n"+comp.getCellNumber1()+"    "+"022-"+comp.getLandline()+"    022-"+comp.getCellNumber2()+"\n"+"Email:"+comp.getEmail() +"     /     "+"Website:"+comp.getWebsite(),font17boldul);
	PdfPCell abc1cell = new PdfPCell(abc1);
	abc1cell.setBorder(0);
	abc1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase abc2 = new Phrase("Thanking you"+","+"\n"+"For "+comp.getBusinessUnitName() + "\n" + "\n"
			+ "Authorised Signatory. ", font9bold);
	PdfPCell abc2cell = new PdfPCell(abc2);
	abc2cell.setBorder(0);
	abc2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable headingtable = new PdfPTable(1);
	headingtable.addCell(notecell);
	headingtable.addCell(abc1cell);
	headingtable.addCell(blankcell);
	headingtable.addCell(abc2cell);
	headingtable.setWidthPercentage(100f);
	
	PdfPCell cell4 = new PdfPCell(headingtable);
	cell4.setBorder(0);	
	
	PdfPTable table4 = new PdfPTable(1);
	table4.addCell(cell4);
	table4.setWidthPercentage(100);
	
	try {
		document.add(table4);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	}
	
}
