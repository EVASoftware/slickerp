package com.slicktechnologies.server.addhocprinting.purchaseOrderPdf;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.logging.client.DefaultLevel.Severe;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.ProductSerialNoMapping;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;

public class SalesOrderPdfUpdated {
	
	Logger logger = Logger.getLogger("Salesorder");
	public Document document;
	SalesOrder so;
	Customer cust;
	Company comp;
	boolean productDescriptionBelowProduct=false;
	
	
	int prouductCount=0;
	List<State> stateList;
	private Font font16boldul, font12bold, font8bold,font9bold, font8, font12boldul,font12,font14bold,font10,font10bold,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df2 = new DecimalFormat("0");
	/**AMol 13-6-2019**/
	List<SalesLineItem> products;
	
	int noOfLines = 12;
//	int noOfLines =20;
	int productCount = 0;
	
	
	/**@author Amol
	 * Date 14-6-2019
	 * 
	 */
	int totalNoOfTaxItem=0;
	int totalNoOfDescriptionLines=0;
	int totalLines ;
	int totalDescLines;
	
	/**
	 * nidhi
	 * 9-08-2018
	 * for print serial no & model no
	 */
	boolean printModelSerailNoFlag =false,moreDt=false;
	int prSrDtCnt=0;
	
	public void setSalesOrder(Long count) {
		so = ofy().load().type(SalesOrder.class).id(count).now();
		if (so.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", so.getCustomerId())
					.filter("companyId", so.getCompanyId()).first().now();
		if (so.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", so.getCustomerId()).first().now();
		
		if (so.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", so.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();
		
		stateList = ofy().load().type(State.class)
				.filter("companyId", so.getCompanyId()).list();
		
		totalNoOfTaxItem=so.getProductTaxes().size();
		
//		totalNoOfDescriptionLines=products.get(0).getPrduct().getComment().length()+products.get(0).getPrduct().getCommentdesc().length();
		products=so.getItems();
		/**
		 * nidhi
		 * 9-08-2018
		 */
		printModelSerailNoFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("HVASC", "PrintModelNoAndSerialNo", so.getCompanyId());
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SalesOrder", "ProductDescriptionBelowTheProduct", so.getCompanyId())){
			productDescriptionBelowProduct=true;
		}
	}

	public SalesOrderPdfUpdated() {
		
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	public void createPdf() {
		
		totalLines = getTotalNoOfLines();
		totalDescLines = getTotalNoOfLinesInDescription();
		productDescriptionBelowProduct = true;

		if (!productDescriptionBelowProduct) {
			logger.log(Level.SEVERE, "InSIDE MAIN METHOD");
			Createletterhead();
			Createtitle();
			createAddressDetailTab();
			createProductTitleTab();
			createProductDetailsTab();
			createTotalTab();
			createFooterTab();
			// createFooterlogo();
			if (comp.getUploadFooter() != null) {
				createCompanyNameAsFooter(document, comp);
			}
		}

		if (productDescriptionBelowProduct) {
			logger.log(Level.SEVERE, "TOTAL LINES: "+totalLines);
			logger.log(Level.SEVERE, "TOTAL DESCRIPTION LINES: "+totalDescLines);
			logger.log(Level.SEVERE, "TOTALNO OF TAXITEM "+totalNoOfTaxItem);
			
			logger.log(Level.SEVERE, "INSIDE PRODUCTDESCRIPTIONBELOWPROJECT :: "+(totalLines + totalDescLines + totalNoOfTaxItem));
			Createletterhead();
			Createtitle();
			createAddressDetailTab();
			createProductTitleTab();
			createProductDetailsTab();
			
			if ((totalLines + totalDescLines + totalNoOfTaxItem) <= 12) {
				logger.log(Level.SEVERE,"INSIDE IF PRODUCTDESCRIPTIONBELOWPROJECT");
				createTotalTab();
				createFooterTab();
			}else if ((totalLines + totalDescLines +totalNoOfTaxItem) > 12) {
				createTotalTab();
				createFooterTab();
				createAnnexureForRemainingProduct(prouductCount);
				logger.log(Level.SEVERE, "ELSE PRODUCTDESCRIPTIONBELOWPROJECT");
			}
		}

		if (printModelSerailNoFlag) {
			boolean lineValidate = (noOfLines == 0 && prouductCount != 0);

			if (lineValidate || moreDt) {
				System.out.println("noOfLines...." + noOfLines);
				createAnnexureForRemainingProduct(prouductCount);
			}
		}else{
			if ( noOfLines == 0 && prouductCount != 0) {
				System.out.println("noOfLines...."+noOfLines);
				createAnnexureForRemainingProduct(prouductCount);
			}
		}

	}
	
	        public int getTotalNoOfLines(){
		    int totalLines = 0;
		    for(SalesLineItem so: so.getItems()){
			int length=so.getProductName().length();
			int lines=(int)Math.ceil(length/55.0);
			totalLines+= lines;
			System.out.println("TotalLines"+totalLines);
			}
		    return  totalLines ;
	        }
	       
	        
	        public int getTotalNoOfLinesInDescription(){
	        	int totalDescLines=0;
	        	logger.log(Level.SEVERE,"PRODUCT SIZE"+products.size());
//	        	logger.log(Level.SEVERE,"PRODUCT "+products.getPrduct().getComment().length()+products.getPrduct().getCommentdesc().length());
	        	for(SalesLineItem so:products){
	        		
//	        		logger.log(Level.SEVERE,"PRODUCT SIZE"+products);
//	        		int length=so.getPrduct().getComment().length()+so.getPrduct().getCommentdesc().length();
//	        		logger.log(Level.SEVERE,"COMMENT LENGTH"+length);
//	        		int lines=(int)Math.ceil(length/55.0);
//	        		logger.log(Level.SEVERE,"COMMENT LINES"+lines);
	        		
	        		
	        		String desc=so.getPrduct().getComment()+" "+so.getPrduct().getCommentdesc();
					String descrArray[]=desc.split("\\r\\n");
					System.out.println("ARRAY SIZE ::" +descrArray.length);
					logger.log(Level.SEVERE,"ARRAY SIZE"+descrArray.length);
					
					int lines=0;
					for(int a=0;a<descrArray.length;a++){
						String description=descrArray[a];
						int line=(int) Math.ceil((description.length()/55.0));
						lines+=line;
						logger.log(Level.SEVERE,"LINES SIZE"+lines);
					}
	        		
	        		totalDescLines+= lines;
	        		logger.log(Level.SEVERE,"TOTALLINES"+totalDescLines);
	        	}
	        	 return  totalDescLines ;
	        }
	
	
	
	
	

	private void createCompanyNameAsFooter(Document doc, Company comp2) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 20f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 try
//		 {
//		 Image
//		 image1=Image.getInstance("images/header.jpg");
//		 image1.scalePercent(13f);
//		 image1.scaleAbsoluteWidth(520f);
//		 image1.setAbsolutePosition(40f,20f);
//		 doc.add(image1);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }
	}

	private void createFooterlogo() {

		/**** image ***/
		DocumentUpload footerdocument = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
//		Image image2 = null;
//		try {
//			image2 = Image.getInstance(new URL(hostUrl
//					+ footerdocument.getUrl()));
//			image2.scalePercent(20f);
//			// image2.setAbsolutePosition(40f,765f);
//			// doc.add(image2);
//
//			imageSignCell = new PdfPCell();
//			imageSignCell.setBorder(0);
//			imageSignCell.setImage(image2);
//			// imageSignCell.setPaddingTop(8);
//			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			// imageSignCell.setFixedHeight(40);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		Image image1 = null;
		try {
			image1 = Image.getInstance("images/Capture.JPG");
			image1.scalePercent(20f);
			// image1.setAbsolutePosition(40f,765f);
			// doc.add(image1);

			imageSignCell = new PdfPCell(image1);
			// imageSignCell.addElement();
			imageSignCell.setImage(image1);
			// imageSignCell.setFixedHeight(40);
			imageSignCell.setBorder(0);
			// imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		} catch (Exception e) {
			e.printStackTrace();
		}

		PdfPTable letterheadTab = new PdfPTable(1);
		letterheadTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			letterheadTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			letterheadTab.addCell(logoblankcell);
		}

		try {
			document.add(letterheadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
	
	}

	private void createAnnexureForRemainingProduct(int Count) {
		System.out.println("inside annexture"+Count);
		noOfLines=80;
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Createtitle();
		createProductTitleTab();
		createProductDetailsForAnnextureTab(Count);
//		createFooterlogo();
		createCompanyNameAsFooter(document, comp);
		
	}

	private void createProductDetailsForAnnextureTab(int Count) {
		
System.out.println("inside annexture product detail");
		
		PdfPTable productDetailstab = new PdfPTable(9);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,12,53,10,10,10,10,7,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int proCount = Count;
		
		/*if(moreDt){
			proCount = 0;
		}*/
		boolean prdMore = false;
		if(printModelSerailNoFlag && proCount<prouductCount && moreDt){
				if(so.getItems().get(Count).getProSerialNoDetails()!=null && so.getItems().get(Count).getProSerialNoDetails().containsKey(0)
						&& so.getItems().get(Count).getProSerialNoDetails().get(0) != null && so.getItems().get(Count).getProSerialNoDetails().get(0).size()>0	){
					if(so.getItems().get(Count).getProSerialNoDetails().get(0).size()>prSrDtCnt && moreDt){
						prdMore = true;
					}
					
				}
		}
		
		totalNoOfTaxItem=so.getProductTaxes().size();
		noOfLines=noOfLines-(totalNoOfTaxItem-2);
		
		for (int i = Count; i < so.getItems().size(); i++) {
			productDescriptionBelowProduct=true;
			if(productDescriptionBelowProduct){
				
				prouductCount = i;
				if (noOfLines <= 0) {
					prouductCount = i;
					break;
				}
				
				int lenght=so.getItems().get(i).getProductName().length();
				logger.log(Level.SEVERE,"Length : "+lenght);
				int lines= (int) Math.ceil((lenght/55.0));
				logger.log(Level.SEVERE,"LINES : "+lines);
				
				int length2=products.get(i).getPrduct().getComment().length()+products.get(i).getPrduct().getCommentdesc().length();
				logger.log(Level.SEVERE,"Length : "+length2);
//				int lines2=(int) Math.ceil((length2/55.0));
				
				
				String desc=products.get(i).getPrduct().getComment()+" "+products.get(i).getPrduct().getCommentdesc();
				String descrArray[]=desc.split("\\r\\n");
				System.out.println("ARRAY SIZE ::" +descrArray.length);
				int lines2=0;
				for(int a=0;a<descrArray.length;a++){
					String description=descrArray[a];
					int line=(int) Math.ceil((description.length()/55.0));
					lines2+=line;
				}
//				int lines2=(int) Math.ceil((length2/55.0));
				logger.log(Level.SEVERE,"LINES2222 : "+lines2);
				
				
				
				
				
				
				noOfLines = noOfLines - lines+lines2;
				
				logger.log(Level.SEVERE,"LINES2 : "+lines2);
				logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
				/**
				 * End
				 */
			}
			
			
			
			
//			if (noOfLines == 0) {
//				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
//				prouductCount = i;
//				break;
//			}
//			
//			noOfLines = noOfLines - 1;
		
		Phrase srnoValph = new Phrase(i+1+"", font8);
		PdfPCell srnoValcell = new PdfPCell(srnoValph);
		srnoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		srnoValcell.setBorderWidthBottom(0);
		srnoValcell.setBorderWidthTop(0);
		productDetailstab.addCell(srnoValcell);
		
		Phrase prodcodevalph = new Phrase(( so.getItems().get(i).getProductCode()), font8);
		PdfPCell prodcodevalcell = new PdfPCell(prodcodevalph);
		prodcodevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		prodcodevalcell.setBorderWidthBottom(0);
		prodcodevalcell.setBorderWidthTop(0);
		productDetailstab.addCell(prodcodevalcell);
		
		Phrase descriptionValph = new Phrase( so.getItems().get(i).getProductName(), font8);
		PdfPCell descriptionValcell = new PdfPCell(descriptionValph);
		descriptionValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptionValcell.setBorderWidthBottom(0);
		descriptionValcell.setBorderWidthTop(0);
		productDetailstab.addCell(descriptionValcell);
		
		Phrase hsnSacValph = new Phrase(" ", font8);
		PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
		hsnSacValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		hsnSacValcell.setBorderWidthBottom(0);
		hsnSacValcell.setBorderWidthTop(0);
		productDetailstab.addCell(hsnSacValcell);
		
		double cgstper=0;
		double sgstper=0;
		double igstper=0;
		
		double gstrate=0;
		double taxamt=0;
	
		if(so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("CGST")||
				so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("SGST")){
			cgstper=so.getItems().get(i).getServiceTax().getPercentage();
			sgstper=so.getItems().get(i).getVatTax().getPercentage();
		}
		else if(so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("SGST")||
				so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("CGST")){
			sgstper=so.getItems().get(i).getServiceTax().getPercentage();
			cgstper=so.getItems().get(i).getVatTax().getPercentage();
		}
		else if(so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("IGST")||
				so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("")){
			
			igstper=so.getItems().get(i).getServiceTax().getPercentage();
		}
		else if(so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("IGST")||
				so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase(" ")){
			
			igstper=so.getItems().get(i).getVatTax().getPercentage();
		}
		
		if(cgstper!=0&&sgstper!=0){
			gstrate=cgstper+sgstper;
			
		}
		else if(igstper!=0){
			gstrate=igstper;
		}
		
		taxamt=(so.getItems().get(i).getPrice()*gstrate)/100;
		
		
		Phrase gstRateph = new Phrase(gstrate+"%", font8);
		PdfPCell gstRatecell = new PdfPCell(gstRateph);
		gstRatecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		gstRatecell.setBorderWidthBottom(0);
		gstRatecell.setBorderWidthTop(0);
		productDetailstab.addCell(gstRatecell);
		
		Phrase quantityph = new Phrase(df2.format(so.getItems().get(i).getQty())+"", font8);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		quantitycell.setBorderWidthBottom(0);
		quantitycell.setBorderWidthTop(0);
		productDetailstab.addCell(quantitycell);
		
		Phrase rateph = new Phrase(df.format(so.getItems().get(i).getPrice())+"", font8);
		PdfPCell ratecell = new PdfPCell(rateph);
		ratecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		ratecell.setBorderWidthBottom(0);
		ratecell.setBorderWidthTop(0);
		productDetailstab.addCell(ratecell);
		
		Phrase perph = new Phrase(so.getItems().get(i).getUnitOfMeasurement(), font8);
		PdfPCell percell = new PdfPCell(perph);
		percell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		percell.setBorderWidthBottom(0);
		percell.setBorderWidthTop(0);
		productDetailstab.addCell(percell);
		
	/*******/
		
		boolean taxPresent = validateTaxes(so.getItems().get(i));
		double indivTotalAmount=0;
		if (taxPresent) {
			if (so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("IGST"))
			{
				double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));

				double taxAmount = getTaxAmount(asstotalAmount, so.getItems().get(i).getVatTax().getPercentage());
				indivTotalAmount = asstotalAmount + taxAmount;
//				totalAmount = totalAmount + indivTotalAmount;

			} 
			else if (so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("IGST")) 
			{
				double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));

				double taxAmount = getTaxAmount(asstotalAmount, so.getItems().get(i).getServiceTax().getPercentage());
				 indivTotalAmount = asstotalAmount + taxAmount;
//				totalAmount = totalAmount + indivTotalAmount;

				

			} else {

				if (so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("CGST")) 
				{
					double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));
					double ctaxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getVatTax().getPercentage());
					double staxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getServiceTax().getPercentage());
					indivTotalAmount = asstotalAmount + ctaxValue+ staxValue;
//					totalAmount = totalAmount + indivTotalAmount;
					
				 } 
					else if (so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("SGST")) {
					double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));
					double ctaxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getServiceTax().getPercentage());
					double staxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getVatTax().getPercentage());
					indivTotalAmount = asstotalAmount + ctaxValue+ staxValue;
//					totalAmount = totalAmount + indivTotalAmount;
					
				}
			}
		}
		
		/******/
		
		
		
		Phrase amountph = new Phrase(df.format(indivTotalAmount)+"", font8);
		PdfPCell amountcell = new PdfPCell(amountph);
		amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		amountcell.setBorderWidthBottom(0);
		amountcell.setBorderWidthTop(0);
		productDetailstab.addCell(amountcell);
		
		
		
		
		
		
		
		
		
		
		
		/**@author Amol
		 * Date 13-6-2019
		 * 
		 */
		
		if(productDescriptionBelowProduct){
		Phrase blanckCell = new Phrase("  ", font8);
		PdfPCell blanckPCell = new PdfPCell(blanckCell);
		blanckPCell.setBorderWidthBottom(0);
		blanckPCell.setBorderWidthTop(0);
		
		
		String desc="";
		String desc1="";
//		System.out.println("desc: "+products.get(i).getPrduct().getComment());
//		System.out.println("desc1: "+products.get(i).getPrduct().getCommentdesc());
		if(products.get(i).getPrduct().getComment() != null 
				&& !products.get(i).getPrduct().getComment().equals("")) {
			desc = products.get(i).getPrduct().getComment();
		}
		if(products.get(i).getPrduct().getCommentdesc() != null 
				&& !products.get(i).getPrduct().getCommentdesc().equals("")) {
			desc1 = products.get(i).getPrduct().getCommentdesc();
		}
		
		String Description=desc+" "+desc1;
		Phrase prodDesc =null;
		if(Description != null) {
			prodDesc = new Phrase(Description, font8);
//			noOfLines = noOfLines - totalNoOfDescriptionLines;
		}	
		
//		if(desc1 != null) {
//			Phrase prodDesc1 = new Phrase(desc1, font8);
//			PdfPCell prodDesc1Cell = new PdfPCell(prodDesc1);
//			prodDesc1Cell.setColspan(12);
//			noOfLines = noOfLines - 1;
//		}
		PdfPCell prodDescCell = new PdfPCell(prodDesc);
		prodDescCell.setBorderWidthBottom(0);
		prodDescCell.setBorderWidthTop(0);
		
		
		
		
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(prodDescCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		}
		/**end amol code
		 * 
		 */
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		if(printModelSerailNoFlag){
			Phrase modelCell = null;
			if(so.getItems().get(i).getProModelNo()!=null && so.getItems().get(i).getProModelNo().trim().length()>0){
				
				Phrase blackCell = new Phrase("  ", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				
				
				
				 modelCell = new Phrase("Model No : " +so.getItems().get(i).getProModelNo().trim(), font8);
				PdfPCell modelPCell = new PdfPCell(modelCell);
				modelPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				modelPCell.setBorderWidthBottom(0);
				modelPCell.setBorderWidthTop(0);
				
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(modelPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				noOfLines--;
			}
			
			if(so.getItems().get(i).getProSerialNoDetails()!=null && so.getItems().get(i).getProSerialNoDetails().containsKey(0)
				&& so.getItems().get(i).getProSerialNoDetails().get(0) != null && so.getItems().get(i).getProSerialNoDetails().get(0).size()>0	){
				
				if(!prdMore && i == prSrDtCnt){
					continue ;
				}
				
				Phrase blackCell = new Phrase("", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				String str = "";
				int count = 0;
				/*for(ProductSerialNoMapping  proSrn : so.getItems().get(i).getProSerialNoDetails().get(0)){
					
					if(proSrn.getProSerialNo().trim().length()>0){
						str = str + proSrn.getProSerialNo() + ", ";
					}
				}*/
				
				int proSrCount = 0;
				for(ProductSerialNoMapping  proSrn : so.getItems().get(i).getProSerialNoDetails().get(0)){
					count++;
					if(i == proCount && count< prSrDtCnt){
						continue ;
					}
					if(proSrCount==0){
						str ="Sr No : " + str;
					}
					if(proSrn.getProSerialNo().trim().length()>0){
						str = str + proSrn.getProSerialNo() + ", ";
						proSrCount++;
					}
					if((proSrCount%2==0 && proSrCount!=0) || proSrCount == so.getItems().get(i).getProSerialNoDetails().get(0).size()){
						
						/*if(str.trim().length()>0){
							System.out.println(str.substring(0,str.trim().length()-1));
							str = str.substring(0,str.trim().length()-1);
						}*/
						
						Phrase srNoCell = new Phrase(str, font8);
						PdfPCell srPCell = new PdfPCell(srNoCell);
						srPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						srPCell.setBorderWidthBottom(0);
						srPCell.setBorderWidthTop(0);
						
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(srPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						noOfLines--;
						
						str = "";
						
						if (noOfLines == 0) {
							prSrDtCnt = proSrCount; 
							moreDt = true;
							prouductCount = i;
							if(so.getItems().get(i).getProSerialNoDetails().get(0).size()>proSrCount){
								prouductCount = i;
							}else{
								prouductCount = i+1;
							}
							System.out.println("prouductCount------"+prouductCount);
							break;
						}
					}
				
				
				}
				
			}
			
		}
		if(productDescriptionBelowProduct){
			prouductCount++;
		}
		
		}
		
		Phrase blankremainig = new Phrase(" ", font8);
		PdfPCell blankremainigcell = new PdfPCell(blankremainig);
		blankremainigcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankremainigcell.setBorderWidthBottom(0);
		blankremainigcell.setBorderWidthLeft(0);
		blankremainigcell.setBorderWidthRight(0);
		blankremainigcell.setColspan(9);
		productDetailstab.addCell(blankremainigcell);
		
//		int remainingLines=0;
//		if (noOfLines != 0) {
//			remainingLines = 15 - (15 - noOfLines);
//		}
//		System.out.println("remainingLines" + remainingLines);
//		for (int j = 0; j < remainingLines; j++) {
//			PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
//			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			pCell.setColspan(8);
//			pCell.setBorderWidthBottom(0);
//			pCell.setBorderWidthTop(0);
//			productDetailstab.addCell(pCell);
//		}
		
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	private void createFooterTab() {
		
		PdfPTable leftFootertab = new PdfPTable(1);
		leftFootertab.setWidthPercentage(100);
		
		Phrase amtChargeableph = new Phrase("Amount In Words", font8);
		PdfPCell amtChargeablecell = new PdfPCell(amtChargeableph);
		amtChargeablecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtChargeablecell.setBorder(0);
		leftFootertab.addCell(amtChargeablecell);
		
		String amtInWordsVal = "";
		amtInWordsVal = SalesInvoicePdf.convert(so.getNetpayable())+" Rupees Only";
		Phrase amtInWordph = new Phrase(amtInWordsVal, font8);
		PdfPCell amtInWordcell = new PdfPCell(amtInWordph);
		amtInWordcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amtInWordcell.setBorder(0);
		leftFootertab.addCell(amtInWordcell);
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankcell.setBorder(0);
		leftFootertab.addCell(blankcell);
		
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		leftFootertab.addCell(blankcell);
		
		Phrase remarkph = new Phrase("Remarks :", font8);
		PdfPCell remarkcell = new PdfPCell(remarkph);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell.setBorder(0);
		leftFootertab.addCell(remarkcell);
		
		
		Phrase headOfcph = new Phrase("Head Office", font8);
		PdfPCell headOfccell = new PdfPCell(headOfcph);
		headOfccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		headOfccell.setBorder(0);
		leftFootertab.addCell(headOfccell);
		
		
		String pan=null;
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("PAN"))
			pan=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			
		}
		
		Phrase companyPanph = new Phrase("Company's PAN    : "+pan , font8);
		PdfPCell companyPancell = new PdfPCell(companyPanph);
		companyPancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		companyPancell.setBorder(0);
		leftFootertab.addCell(companyPancell);
		
		
		PdfPTable rightFootertab = new PdfPTable(3);
		rightFootertab.setWidthPercentage(100);
		try {
			rightFootertab.setWidths(new float[]{33,33,33});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase companyNameph = new Phrase("For "+comp.getBusinessUnitName() , font8);
		PdfPCell companyNamecell = new PdfPCell(companyNameph);
		companyNamecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		companyNamecell.setBorder(0);
		companyNamecell.setColspan(3);
		rightFootertab.addCell(companyNamecell);
		
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		rightFootertab.addCell(blankcell);
		
		Phrase preparedbyph = new Phrase("Prepared by", font8);
		PdfPCell preparedbycell = new PdfPCell(preparedbyph);
		preparedbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		preparedbycell.setBorder(0);
		preparedbycell.setPaddingBottom(5);
		rightFootertab.addCell(preparedbycell);
		
		Phrase verifiedbyph = new Phrase("Verified by", font8);
		PdfPCell verifiedbycell = new PdfPCell(verifiedbyph);
		verifiedbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		verifiedbycell.setBorder(0);
		verifiedbycell.setPaddingBottom(5);
		rightFootertab.addCell(verifiedbycell);
		
		
		Phrase authorisedph = new Phrase("Authorised Signatory", font8);
		PdfPCell authorisedcell = new PdfPCell(authorisedph);
		authorisedcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		authorisedcell.setBorder(0);
		authorisedcell.setPaddingBottom(5);
		rightFootertab.addCell(authorisedcell);
		
		PdfPTable outerFootertab = new PdfPTable(2);
		outerFootertab.setWidthPercentage(100);
		
		PdfPCell uppercell = new PdfPCell(leftFootertab);
		uppercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		uppercell.setBorderWidthBottom(0);
		uppercell.setBorderWidthRight(0);
		outerFootertab.addCell(uppercell);
		
		Phrase footerblank=new Phrase (" ",font8);
		PdfPCell footerblankcell = new PdfPCell(footerblank);
		footerblankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerblankcell.setBorderWidthBottom(0);
		footerblankcell.setBorderWidthLeft(0);
		outerFootertab.addCell(footerblankcell);
		
		Phrase footerblank2=new Phrase (" ",font8);
		PdfPCell footerblankcell2 = new PdfPCell(footerblank2);
		footerblankcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerblankcell2.setBorderWidthTop(0);
		footerblankcell2.setBorderWidthRight(0);
		footerblankcell2.setPaddingBottom(5);
		outerFootertab.addCell(footerblankcell2);
		
		
		PdfPCell bottomcell = new PdfPCell(rightFootertab);
		bottomcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		bottomcell.setBorderWidthTop(0);
		bottomcell.setBorderWidthLeft(0);
		outerFootertab.addCell(bottomcell);
		
		
		Phrase declarationph=new Phrase ("This is a Computer Generated Document ",font8);
		PdfPCell declarationcell = new PdfPCell(declarationph);
		declarationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		declarationcell.setBorder(0);
		declarationcell.setColspan(2);
		declarationcell.setPaddingTop(5);
		outerFootertab.addCell(declarationcell);
		
		
		try {
			document.add(outerFootertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void createTotalTab() {
		
		PdfPTable productDetailstab = new PdfPTable(9);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,12,53,10,10,10,10,7,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productDetailstab.addCell(blankcell);
		
		productDetailstab.addCell(blankcell);
		
		Phrase totalph = new Phrase("Total", font8);
		PdfPCell totalcell = new PdfPCell(totalph);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalcell);
		
		productDetailstab.addCell(blankcell);
		productDetailstab.addCell(blankcell);
		
		double totalQut=0;
		for (int i = 0; i < so.getItems().size(); i++) {
			totalQut=totalQut+so.getItems().get(i).getQty();
		}
		Phrase totalQtph = new Phrase(df2.format(totalQut)+"", font8);
		PdfPCell totalQtcell = new PdfPCell(totalQtph);
		totalQtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalQtcell);
		
		
		productDetailstab.addCell(blankcell);
		productDetailstab.addCell(blankcell);
		
		
		System.out.println("so.getNetpayable()"+so.getNetpayable());
		Phrase totalAmtph = new Phrase(df.format(so.getNetpayable()), font8);
		PdfPCell totalAmtcell = new PdfPCell(totalAmtph);
		totalAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productDetailstab.addCell(totalAmtcell);
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createProductDetailsTab() {
		
		PdfPTable productDetailstab = new PdfPTable(9);
		productDetailstab.setWidthPercentage(100);
		
		try {
			productDetailstab.setWidths(new float[]{7,12,53,10,10,10,10,7,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("so.getItems().size()----"+so.getItems().size());
		
			
		totalNoOfTaxItem=so.getProductTaxes().size();
		noOfLines=noOfLines-(totalNoOfTaxItem-2);
			
		outerloop:
		for (int i = 0; i < so.getItems().size(); i++) {
			productDescriptionBelowProduct=true;
			
			if(productDescriptionBelowProduct){
				
				prouductCount = i;
				if (noOfLines <= 0) {
					prouductCount = i;
					break;
				}
				
				int lenght=so.getItems().get(i).getProductName().length();
				logger.log(Level.SEVERE,"Length : "+lenght);
				int lines= (int) Math.ceil((lenght/55.0));
				logger.log(Level.SEVERE,"LINES : "+lines);
				
				int length2=products.get(i).getPrduct().getComment().length()+products.get(i).getPrduct().getCommentdesc().length();
				logger.log(Level.SEVERE,"Length : "+length2);
				
				/***
				 * 
				 */
				
				String desc=products.get(i).getPrduct().getComment()+" "+products.get(i).getPrduct().getCommentdesc();
				String descrArray[]=desc.split("\\r\\n");
				System.out.println("ARRAY SIZE ::" +descrArray.length);
				int lines2=0;
				for(int a=0;a<descrArray.length;a++){
					String description=descrArray[a];
					int line=(int) Math.ceil((description.length()/55.0));
					lines2+=line;
				}
//				int lines2=(int) Math.ceil((length2/55.0));
				logger.log(Level.SEVERE,"LINES2222 : "+lines2);
				
				/**
				 * 
				 */
				
				noOfLines = noOfLines - (lines+lines2);
				logger.log(Level.SEVERE,"NO OF LINES : "+noOfLines+" Lines "+lines);
				/**
				 * End
				 */
			}
			
			
			
			
			
			
			System.out.println("int i----"+i);
			System.out.println("noOfLines-----"+noOfLines);
			
			if(!productDescriptionBelowProduct){
			if (noOfLines == 0) {
				prSrDtCnt = 0; 
				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
				prouductCount = i;
				System.out.println("prouductCount------"+prouductCount);
				break;
			}
			
			}
			if(!productDescriptionBelowProduct){
			   noOfLines = noOfLines - 1;
			}
			
			if(printModelSerailNoFlag){
				if (noOfLines == 0) {
					prSrDtCnt = 0; 
					// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
					prouductCount = i;
					System.out.println("prouductCount------"+prouductCount);
					break;
				}
			}
			
			
		
		Phrase srnoValph = new Phrase(i+1+"", font8);
		PdfPCell srnoValcell = new PdfPCell(srnoValph);
		srnoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		srnoValcell.setBorderWidthBottom(0);
		srnoValcell.setBorderWidthTop(0);
		productDetailstab.addCell(srnoValcell);
		
		Phrase prodcodevalph = new Phrase((so.getItems().get(i).getProductCode()), font8);
		PdfPCell prodcodevalcell = new PdfPCell(prodcodevalph);
		prodcodevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		prodcodevalcell.setBorderWidthBottom(0);
		prodcodevalcell.setBorderWidthTop(0);
		productDetailstab.addCell(prodcodevalcell);
		
		Phrase descriptionValph = new Phrase(so.getItems().get(i).getProductName(), font8);
		PdfPCell descriptionValcell = new PdfPCell(descriptionValph);
		descriptionValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptionValcell.setBorderWidthBottom(0);
		descriptionValcell.setBorderWidthTop(0);
		productDetailstab.addCell(descriptionValcell);
		
		Phrase hsnSacValph = new Phrase(so.getItems().get(i).getPrduct().getHsnNumber(), font8);
		PdfPCell hsnSacValcell = new PdfPCell(hsnSacValph);
		hsnSacValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		hsnSacValcell.setBorderWidthBottom(0);
		hsnSacValcell.setBorderWidthTop(0);
		productDetailstab.addCell(hsnSacValcell);
		
		double cgstper=0;
		double sgstper=0;
		double igstper=0;
		
		double gstrate=0;
		double taxamt=0;
	
		if(so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("CGST")||
				so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("SGST")){
			cgstper=so.getItems().get(i).getServiceTax().getPercentage();
			sgstper=so.getItems().get(i).getVatTax().getPercentage();
		}
		else if(so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("SGST")||
				so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("CGST")){
			sgstper=so.getItems().get(i).getServiceTax().getPercentage();
			cgstper=so.getItems().get(i).getVatTax().getPercentage();
		}
		else if(so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("IGST")||
				so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("")){
			
			igstper=so.getItems().get(i).getServiceTax().getPercentage();
		}
		else if(so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("IGST")||
				so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase(" ")){
			
			igstper=so.getItems().get(i).getVatTax().getPercentage();
		}
		
		if(cgstper!=0&&sgstper!=0){
			gstrate=cgstper+sgstper;
			
		}
		else if(igstper!=0){
			gstrate=igstper;
		}
		
		taxamt=(so.getTotalAmount()*gstrate)/100;
		
		
		Phrase gstRateph = new Phrase(gstrate+"%", font8);
		PdfPCell gstRatecell = new PdfPCell(gstRateph);
		gstRatecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		gstRatecell.setBorderWidthBottom(0);
		gstRatecell.setBorderWidthTop(0);
		productDetailstab.addCell(gstRatecell);
		
		Phrase quantityph = new Phrase(df2.format(so.getItems().get(i).getQty())+"", font8);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		quantitycell.setBorderWidthBottom(0);
		quantitycell.setBorderWidthTop(0);
		productDetailstab.addCell(quantitycell);
		
		System.out.println("so.getItems().get(i).getPrice()"+so.getItems().get(i).getPrice());
		
		
		SuperProduct product=so.getItems().get(i).getPrduct();
		double tax=removeAllTaxes(product);
		double origPrice=so.getItems().get(i).getPrice()-tax;
		Phrase rateph = new Phrase(df.format(origPrice)+"", font8);
		
		PdfPCell ratecell = new PdfPCell(rateph);
		ratecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		ratecell.setBorderWidthBottom(0);
		ratecell.setBorderWidthTop(0);
		productDetailstab.addCell(ratecell);
		
		Phrase perph = new Phrase(so.getItems().get(i).getUnitOfMeasurement(), font8);
		PdfPCell percell = new PdfPCell(perph);
		percell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		percell.setBorderWidthBottom(0);
		percell.setBorderWidthTop(0);
		productDetailstab.addCell(percell);
		
		
		
		
		
		
		/*******/
		
		boolean taxPresent = validateTaxes(so.getItems().get(i));
		double indivTotalAmount=0;
		if (taxPresent) {
			if (so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("IGST"))
			{
				double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));

				double taxAmount = getTaxAmount(asstotalAmount, so.getItems().get(i).getVatTax().getPercentage());
				indivTotalAmount = asstotalAmount + taxAmount;
//				totalAmount = totalAmount + indivTotalAmount;

			} 
			else if (so.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("IGST")) 
			{
				double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));

				double taxAmount = getTaxAmount(asstotalAmount, so.getItems().get(i).getServiceTax().getPercentage());
				 indivTotalAmount = asstotalAmount + taxAmount;
//				totalAmount = totalAmount + indivTotalAmount;

				

			} else {

				if (so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("CGST")) 
				{
					double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));
					double ctaxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getVatTax().getPercentage());
					double staxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getServiceTax().getPercentage());
					indivTotalAmount = asstotalAmount + ctaxValue+ staxValue;
//					totalAmount = totalAmount + indivTotalAmount;
					
				 } 
					else if (so.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase("SGST")) {
					double asstotalAmount = getAssessTotalAmount(so.getItems().get(i));
					double ctaxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getServiceTax().getPercentage());
					double staxValue = getTaxAmount(asstotalAmount,so.getItems().get(i).getVatTax().getPercentage());
					indivTotalAmount = asstotalAmount + ctaxValue+ staxValue;
//					totalAmount = totalAmount + indivTotalAmount;
					
				}
			}
		}
		
		/******/

		Phrase amountph = new Phrase(df.format(indivTotalAmount)+"", font8);
		PdfPCell amountcell = new PdfPCell(amountph);
		amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		amountcell.setBorderWidthBottom(0);
		amountcell.setBorderWidthTop(0);
		productDetailstab.addCell(amountcell);
		
		/**@author Amol
		 * Date 13-6-2019
		 * 
		 */
		
		
		
		if(productDescriptionBelowProduct){
			
		Phrase blanckCell = new Phrase("  ", font8);
		PdfPCell blanckPCell = new PdfPCell(blanckCell);
		blanckPCell.setBorderWidthBottom(0);
		blanckPCell.setBorderWidthTop(0);
		
		
		String desc="";
		String desc1="";
//		System.out.println("desc: "+products.get(i).getPrduct().getComment());
//		System.out.println("desc1: "+products.get(i).getPrduct().getCommentdesc());
		if(products.get(i).getPrduct().getComment() != null 
				&& !products.get(i).getPrduct().getComment().equals("")) {
			desc = products.get(i).getPrduct().getComment();
		}
		if(products.get(i).getPrduct().getCommentdesc() != null 
				&& !products.get(i).getPrduct().getCommentdesc().equals("")) {
			desc1 = products.get(i).getPrduct().getCommentdesc();
		}

		String description=desc+" "+desc1;
		
		
		Phrase prodDesc =null;
		if(description != null) {
			prodDesc = new Phrase(description, font8);
//			noOfLines = noOfLines -totalNoOfDescriptionLines;
		}	
		
//		if(desc1 != null) {
//			Phrase prodDesc1 = new Phrase(desc1, font8);
//			PdfPCell prodDesc1Cell = new PdfPCell(prodDesc1);
//			prodDesc1Cell.setColspan(12);
//			noOfLines = noOfLines - 1;
//		}
		PdfPCell prodDescCell = new PdfPCell(prodDesc);
		prodDescCell.setBorderWidthBottom(0);
		prodDescCell.setBorderWidthTop(0);
		
		
		
		if(description!=null&&!description.equals("")){
			
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(prodDescCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		productDetailstab.addCell(blanckPCell);
		}
		 }
		/**end amol code
		 * 
		 */
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		if(printModelSerailNoFlag){
			Phrase modelCell = null;
			ItemProduct itemPr = (ItemProduct) so.getItems().get(i).getPrduct();
			
			if(itemPr != null && itemPr.getModel() !=null &&  itemPr.getModel().trim().length()>0){
				
				Phrase blackCell = new Phrase("  ", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				
				
				
				 modelCell = new Phrase("Model No : " + itemPr.getModel().trim(), font8);
				PdfPCell modelPCell = new PdfPCell(modelCell);
				modelPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				modelPCell.setBorderWidthBottom(0);
				modelPCell.setBorderWidthTop(0);
				
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(modelPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				productDetailstab.addCell(blackPCell);
				noOfLines--;
				if (noOfLines == 0) {
					prSrDtCnt = 0; 
					// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
					prouductCount = i;
					System.out.println("prouductCount------"+prouductCount);
					break outerloop;
				}
			}
			
			if(so.getItems().get(i).getProSerialNoDetails()!=null && so.getItems().get(i).getProSerialNoDetails().containsKey(0)
				&& so.getItems().get(i).getProSerialNoDetails().get(0) != null && so.getItems().get(i).getProSerialNoDetails().get(0).size()>0	){
				
				
				Phrase blackCell = new Phrase("", font8);
				PdfPCell blackPCell = new PdfPCell(blackCell);
				blackPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				blackPCell.setBorderWidthBottom(0);
				blackPCell.setBorderWidthTop(0);
				String str = "";
				int proSrCount = 0;
				for(ProductSerialNoMapping  proSrn : so.getItems().get(i).getProSerialNoDetails().get(0)){
					if(proSrCount==0){
						str ="Sr No : " + str;
					}
					if(proSrn.getProSerialNo().trim().length()>0){
						str = str + proSrn.getProSerialNo() + ", ";
						proSrCount++;
					}
					if((proSrCount%2==0 && proSrCount!=0) || proSrCount == so.getItems().get(i).getProSerialNoDetails().get(0).size()){
						
						/*if(str.trim().length()>0){
							System.out.println(str.substring(0,str.trim().length()-1));
							str = str.substring(0,str.trim().length()-1);
						}*/
						
						Phrase srNoCell = new Phrase(str, font8);
						PdfPCell srPCell = new PdfPCell(srNoCell);
						srPCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						srPCell.setBorderWidthBottom(0);
						srPCell.setBorderWidthTop(0);
						
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(srPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						productDetailstab.addCell(blackPCell);
						
						
						
						
						
						
						noOfLines--;
						
						str = "";
						
						if (noOfLines == 0) {
							prSrDtCnt = proSrCount+1; 
							moreDt = true;
							prouductCount = i;
							System.out.println("prouductCount------"+prouductCount);
							break outerloop;
						}
					}
				
				
				}
				
			}
			
		}      if(productDescriptionBelowProduct){
		          prouductCount++;
		           }
		
		}
		System.out.println("prouductCount by me"+prouductCount);
		System.out.println("noOfLines by me"+noOfLines);
		/**
		 * Tax Table
		 */
		
		
		Phrase blankph=new Phrase (" ",font8);
		PdfPCell blankcell=new PdfPCell(blankph);
		blankcell.setBorderWidthBottom(0);
		blankcell.setBorderWidthTop(0);
		
		
		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < so.getProductTaxes().size(); i++) {
			if ( so.getProductTaxes().get(i).getChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal+  so.getProductTaxes().get(i).getChargePayable();
				
				Phrase IGSTphrase = new Phrase(" Input IGST ", font8bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorderWidthBottom(0);
				IGSTphraseCell.setBorderWidthTop(0);
				IGSTphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				Phrase IGSTValphrase = new Phrase(df.format(igstTotalVal), font8bold);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorderWidthBottom(0);
				IGSTValphraseCell.setBorderWidthTop(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(IGSTphraseCell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(IGSTValphraseCell);

			} else if (so.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal+ so.getProductTaxes().get(i).getChargePayable();

				Phrase SGSTphrase = new Phrase(" Input SGST ", font8bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorderWidthBottom(0);
				SGSTphraseCell.setBorderWidthTop(0);
				SGSTphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				Phrase SGSTValphrase = new Phrase(df.format(sgstTotalVal), font8bold);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorderWidthTop(0);;
				SGSTValphraseCell.setBorderWidthBottom(0);
				
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(SGSTphraseCell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(SGSTValphraseCell);
			}
			else if (so.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal+ so.getProductTaxes().get(i).getChargePayable();

				Phrase CGSTphrase = new Phrase("Input CGST ",font8bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorderWidthBottom(0);
				CGSTphraseCell.setBorderWidthTop(0);
				CGSTphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				Phrase CGSTValphrase = new Phrase(df.format(cgstTotalVal), font8bold);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorderWidthBottom(0);
				CGSTValphraseCell.setBorderWidthTop(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(CGSTphraseCell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(blankcell);
				productDetailstab.addCell(CGSTValphraseCell);
			}
		}
		
		 if(!productDescriptionBelowProduct){
		int remainingLines=0;
		if (noOfLines != 0) {
			remainingLines = 10 - (10 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);
		for (int j = 0; j < remainingLines; j++) {
			PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
			pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			pCell.setColspan(9);
			pCell.setBorderWidthBottom(0);
			pCell.setBorderWidthTop(0);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
			productDetailstab.addCell(pCell);
		}
		 }
		
		try {
			document.add(productDetailstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}

	private double getAssessTotalAmount(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		double totalAmount = 0;

		System.out.println("Inside getAssessTotalAmount");
		SuperProduct prod = salesLineItem.getPrduct();
		//return inclusive tax amount
		double taxAmt = removeAllTaxes(prod);
		
		
		if (salesLineItem.getArea().trim().equalsIgnoreCase("NA")
				|| salesLineItem.getArea().equalsIgnoreCase("")) {
			System.out.println("Area Not Present");
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				System.out.println("Discount Zero");
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount=salesLineItem.getPrice()-taxAmt;
//					totalAmount = salesLineItem.getPrice()*salesLineItem.getQty();
					totalAmount = totalAmount*salesLineItem.getQty();
				} else {
					
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = (totalAmount*salesLineItem.getQty())
							- salesLineItem.getDiscountAmt();
				}
			} else {
				System.out.println("Discount Not Zero");
				double disPercentAmount = getPercentAmount(salesLineItem, false);
				if (salesLineItem.getDiscountAmt() == 0) {
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = (totalAmount*salesLineItem.getQty()) - disPercentAmount;
				} else {
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = (totalAmount*salesLineItem.getQty()) - disPercentAmount
							- salesLineItem.getDiscountAmt();
				}
			}
		} else {

			System.out.println("Area Present");
			if (salesLineItem.getPercentageDiscount() == null
					|| salesLineItem.getPercentageDiscount() == 0) {
				System.out.println(
						"ZERO Area Present --PercentageDiscount");
				if (salesLineItem.getDiscountAmt() == 0) {
					System.out.println(
							"ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = totalAmount
							* Double.parseDouble(salesLineItem.getArea().trim());
				} else {
					System.out.println(
							"NON ZERO Area Present --PercentageDiscount--Discount Amt");
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = (totalAmount * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- salesLineItem.getDiscountAmt();
				}
			} else {
				System.out.println(
						"NON ZERO Area Present --PercentageDiscount");
				double disPercentAmount = getPercentAmount(salesLineItem, true);
				if (salesLineItem.getDiscountAmt() == 0) {
					System.out.println(
							"ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = (totalAmount * Double
							
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount;
				} else {
					System.out.println(
							"NON ZERO Area Present --PercentageDiscount--Disc Amt");
					totalAmount=salesLineItem.getPrice()-taxAmt;
					totalAmount = (totalAmount * Double
							.parseDouble(salesLineItem.getArea().trim()))
							- disPercentAmount - salesLineItem.getDiscountAmt();
				}
			}

		}
		System.out.println( "Assesable Value::::" + totalAmount);
		return totalAmount;
	}

	private double removeAllTaxes(SuperProduct entity) {
		
		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		
		if (entity instanceof ServiceProduct) {
			
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax()!= null&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = (entity.getPrice() / (1 + (vat / 100)));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = (entity.getPrice() / (1 + service / 100));
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {


			/**if condition added by vijay and old code added in else block
			 * Date 2 August  2017 added by vijay for GST
			 */
			
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
			   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
			{

				double dot = service + vat;
				retrServ=(entity.getPrice()/(1+dot/100));
				retrServ=entity.getPrice()-retrServ;
//				retrVat=0;

			}else{
				
			
			// Here if both are inclusive then first remove service tax and then
			// on that amount
			// calculate vat.
			double removeServiceTax = (entity.getPrice() / (1 + service / 100));

			// double taxPerc=service+vat;
			// retrServ=(entity.getPurchasePrice()/(1+taxPerc/100)); //line changed
			// below
			retrServ = (removeServiceTax / (1 + vat / 100));
			retrServ = entity.getPrice() - retrServ;
			
			}
		}
		tax = retrVat + retrServ;
		return tax;
	}

	private double getPercentAmount(SalesLineItem salesLineItem, boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		SuperProduct prod = salesLineItem.getPrduct();
		//return inclusive tax amount
		double taxAmt = removeAllTaxes(prod);
		if (isAreaPresent) {
			percentAmount=salesLineItem.getPrice()-taxAmt;
			percentAmount = ((percentAmount
					* Double.parseDouble(salesLineItem.getArea().trim()) * salesLineItem
					.getPercentageDiscount()) / 100);
		} else {
			percentAmount=salesLineItem.getPrice()-taxAmt;
			percentAmount = ((salesLineItem.getQty()*percentAmount * salesLineItem //add qty by jayshree
					.getPercentageDiscount()) / 100);
		}
		return percentAmount;
	}

	private boolean validateTaxes(SalesLineItem salesLineItem) {
		// TODO Auto-generated method stub
		if (salesLineItem.getVatTax().getPercentage() != 0) {
			return true;
		} else {
			if (salesLineItem.getServiceTax().getPercentage() != 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	private void createProductTitleTab() {
		
		PdfPTable productTitletab = new PdfPTable(9);
		productTitletab.setWidthPercentage(100);
		
		try {
			productTitletab.setWidths(new float[]{7,12,53,10,10,10,10,7,10});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase srnoph = new Phrase("Sr.No", font8);
		PdfPCell srnocell = new PdfPCell(srnoph);
		srnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(srnocell);
		
		Phrase prodcodeph = new Phrase("Prodcuct Code", font8);
		PdfPCell prodcodecell = new PdfPCell(prodcodeph);
		prodcodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(prodcodecell);
		
		Phrase descriptionph = new Phrase("Description of Goods", font8);
		PdfPCell descriptioncell = new PdfPCell(descriptionph);
		descriptioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(descriptioncell);
		
		Phrase hsnSacph = new Phrase("HSN/SAC", font8);
		PdfPCell hsnSaccell = new PdfPCell(hsnSacph);
		hsnSaccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(hsnSaccell);
		
		Phrase gstRateph = new Phrase("GST Rate", font8);
		PdfPCell gstRatecell = new PdfPCell(gstRateph);
		gstRatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(gstRatecell);
		
		Phrase quantityph = new Phrase("Quantity", font8);
		PdfPCell quantitycell = new PdfPCell(quantityph);
		quantitycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(quantitycell);
		
		Phrase rateph = new Phrase("Rate", font8);
		PdfPCell ratecell = new PdfPCell(rateph);
		ratecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(ratecell);
		
		Phrase perph = new Phrase("Unit", font8);
		PdfPCell percell = new PdfPCell(perph);
		percell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(percell);
		
		Phrase amountph = new Phrase("Total", font8);
		PdfPCell amountcell = new PdfPCell(amountph);
		amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTitletab.addCell(amountcell);
		
		try {
			document.add(productTitletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createAddressDetailTab() {
		PdfPTable invoiceAddtab = new PdfPTable(3);
		invoiceAddtab.setWidthPercentage(100);
		
		try {
			invoiceAddtab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase invoiceto = new Phrase("Invoice To", font8bold);
		PdfPCell invoicetocell = new PdfPCell(invoiceto);
		invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		invoicetocell.setColspan(3);
		invoicetocell.setBorder(0);
		invoiceAddtab.addCell(invoicetocell);
		
		Phrase compname = new Phrase(cust.getCustomerName(), font8);
		PdfPCell compnamecell = new PdfPCell(compname);
		compnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell.setColspan(3);
		compnamecell.setBorder(0);
		invoiceAddtab.addCell(compnamecell);
		
		
		/**Date 11-7-2020 by Amol billing address should be printed here raised by Vaishnavi Pawar **/
		Phrase compadd = new Phrase(cust.getAdress().getCompleteAddress(), font8);
		PdfPCell compaddcell = new PdfPCell(compadd);
		compaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compaddcell.setColspan(3);
		compaddcell.setBorder(0);
		invoiceAddtab.addCell(compaddcell);
		
		Phrase gstinph = new Phrase("GSTIN/UIN", font8);
		PdfPCell gstincell = new PdfPCell(gstinph);
		gstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstincell.setBorder(0);
		invoiceAddtab.addCell(gstincell);
		
		Phrase colonph = new Phrase(":", font8);
		PdfPCell coloncell = new PdfPCell(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
		invoiceAddtab.addCell(coloncell);
		
		
		ServerAppUtility serverApp = new ServerAppUtility();

		String gstin = "", gstinText = "";
		if (comp.getCompanyGSTType().trim()
				.equalsIgnoreCase("GST Applicable")) {
			gstin = serverApp.getGSTINOfCompany(comp, so.getBranch().trim());
			System.out.println("gstin" + gstin);

		} else {
			gstinText = comp.getCompanyGSTTypeText().trim();
			System.out.println("gstinText" + gstinText);
		}

		Phrase gstinvalph = null;
		if (!gstin.trim().equals("")) {
			gstinvalph = new Phrase( gstin, font8);
		} else if (!gstinText.trim().equalsIgnoreCase("")) {
			gstinvalph = new Phrase(gstinText, font8);
		} else {
			gstinvalph = new Phrase("", font8);

		}
		
		
		String gstinval=null;
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {

			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("GSTIN")) {

				gstinval=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			}
		
		
		Phrase custgstinvalph=new Phrase (gstinval,font8);
		PdfPCell gstinvalcell = new PdfPCell(custgstinvalph);
		gstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell.setBorder(0);
		invoiceAddtab.addCell(gstinvalcell);
		
		
		Phrase statename = new Phrase("State Name", font8);
		PdfPCell statenamecell = new PdfPCell(statename);
		statenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamecell.setBorder(0);
		invoiceAddtab.addCell(statenamecell);
		
		invoiceAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statecodetab=new PdfPTable(3);
		statecodetab.setWidthPercentage(100);
		try {
			statecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevalname = new Phrase(cust.getAdress().getState(), font8);
		
		PdfPCell statenamevalcell = new PdfPCell(statevalname);
		statenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevalcell.setBorder(0);
		statecodetab.addCell(statenamevalcell);
		
		Phrase statecode = new Phrase("Code :", font8);
		PdfPCell statecodecell = new PdfPCell(statecode);
		statecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodecell.setBorder(0);
		statecodetab.addCell(statecodecell);
		
		String stCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(cust.getSecondaryAdress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase stcodeval=new Phrase(stCo,font8);
		PdfPCell stateCodeCell = new PdfPCell(stcodeval);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodetab.addCell(stateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell statecodetabcell = new PdfPCell(statecodetab);
		statecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statecodetabcell.setBorder(0);
		invoiceAddtab.addCell(statecodetabcell);
		
		
		Phrase cinph = new Phrase("CIN", font8);
		PdfPCell cincell = new PdfPCell(cinph);
		cincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cincell.setBorder(0);
		invoiceAddtab.addCell(cincell);
		
		invoiceAddtab.addCell(coloncell);
		
		String cinval=null;
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {

			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().contains("CIN")) {

				cinval=cust.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
			}
			
		Phrase cinvalph = new Phrase(cinval, font8);
		PdfPCell cinvalphCell = new PdfPCell(cinvalph);
		cinvalphCell.setBorder(0);
		invoiceAddtab.addCell(cinvalphCell);
			
		
		Phrase emailph = new Phrase("E-Mail", font8);
		PdfPCell emailcell = new PdfPCell(emailph);
		emailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailcell.setBorder(0);
		invoiceAddtab.addCell(emailcell);
		
		invoiceAddtab.addCell(coloncell);
		
		Phrase emailvalph = new Phrase(cust.getEmail(), font8);
		PdfPCell emailvalcell = new PdfPCell(emailvalph);
		emailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailvalcell.setBorder(0);
		invoiceAddtab.addCell(emailvalcell);
		
		
		/*invoice to tab complet*/
		
		PdfPTable deliveryAddtab = new PdfPTable(3);
		deliveryAddtab.setWidthPercentage(100);
		
		try {
			deliveryAddtab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase despatchto = new Phrase("Despatch To", font8bold);
		PdfPCell despatchtocell = new PdfPCell(despatchto);
		despatchtocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		despatchtocell.setColspan(3);
		despatchtocell.setBorder(0);
		deliveryAddtab.addCell(despatchtocell);
		
		Phrase compname2 = new Phrase(cust.getCustomerName(), font8);
		PdfPCell compnamecell2 = new PdfPCell(compname2);
		compnamecell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell2.setColspan(3);
		compnamecell2.setBorder(0);
		deliveryAddtab.addCell(compnamecell2);
		
		Phrase compadd2 = new Phrase(so.getShippingAddress().getCompleteAddress(), font8);
		PdfPCell compaddcell2 = new PdfPCell(compadd2);
		compaddcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		compaddcell2.setColspan(3);
		compaddcell2.setBorder(0);
		deliveryAddtab.addCell(compaddcell2);
		
		
		Phrase devemailph = new Phrase("E-Mail", font8);
		PdfPCell devemailcell = new PdfPCell(devemailph);
		devemailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devemailcell.setBorder(0);
		deliveryAddtab.addCell(devemailcell);
		
		deliveryAddtab.addCell(coloncell);
		
		Phrase devemailvalph = new Phrase(cust.getEmail(), font8);
		PdfPCell devemailvalcell = new PdfPCell(devemailvalph);
		devemailvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devemailvalcell.setBorder(0);
		deliveryAddtab.addCell(devemailvalcell);
		
		
		Phrase devgstinph = new Phrase("GSTIN/UIN", font8);
		PdfPCell devgstincell = new PdfPCell(devgstinph);
		devgstincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devgstincell.setBorder(0);
		deliveryAddtab.addCell(devgstincell);
		
		Phrase devcolonph = new Phrase(":", font8);
		PdfPCell devcoloncell = new PdfPCell(devcolonph);
		devcoloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devcoloncell.setBorder(0);
		deliveryAddtab.addCell(devcoloncell);
		
		
//		ServerAppUtility devserverApp = new ServerAppUtility();
//
//		String devgstin = "", devgstinText = "";
//		if (comp.getCompanyGSTType().trim()
//				.equalsIgnoreCase("GST Applicable")) {
//			devgstin = devserverApp.getGSTINOfCompany(comp, po.getBranch().trim());
//			System.out.println("gstin" + gstin);
//
//		} else {
//			devgstinText = comp.getCompanyGSTTypeText().trim();
//			System.out.println("gstinText" + gstinText);
//		}
//
//		Phrase devgstinvalph = null;
//		if (!gstin.trim().equals("")) {
//			gstinvalph = new Phrase( gstin, font8);
//		} else if (!gstinText.trim().equalsIgnoreCase("")) {
//			gstinvalph = new Phrase(gstinText, font8);
//		} else {
//			gstinvalph = new Phrase("", font8);
//
//		}
	
		Phrase custgstin=new Phrase (gstinval,font8);
		PdfPCell devgstinvalcell = new PdfPCell(custgstin);
		devgstinvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devgstinvalcell.setBorder(0);
		deliveryAddtab.addCell(devgstinvalcell);
		
		
		Phrase devstatename = new Phrase("State Name", font8);
		PdfPCell devstatenamecell = new PdfPCell(devstatename);
		devstatenamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatenamecell.setBorder(0);
		deliveryAddtab.addCell(devstatenamecell);
		
		deliveryAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable devstatecodetab=new PdfPTable(3);
		devstatecodetab.setWidthPercentage(100);
		try {
			devstatecodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase devstatevalname = new Phrase(so.getShippingAddress().getState(), font8);
		PdfPCell devstatenamevalcell = new PdfPCell(devstatevalname);
		devstatenamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatenamevalcell.setBorder(0);
		devstatecodetab.addCell(devstatenamevalcell);
		
		Phrase devstatecode = new Phrase("Code :", font8);
		PdfPCell devstatecodecell = new PdfPCell(devstatecode);
		devstatecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatecodecell.setBorder(0);
		devstatecodetab.addCell(devstatecodecell);
		
		String sostCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(so.getShippingAddress().getState().trim())) {
				sostCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}
		Phrase devstateCode = new Phrase(sostCo, font8);

		PdfPCell devstateCodeCell = new PdfPCell(devstateCode);
		devstateCodeCell.setBorder(0);
		devstatecodetab.addCell(devstateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell devstatecodetabcell = new PdfPCell(devstatecodetab);
		devstatecodetabcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		devstatecodetabcell.setBorder(0);
		deliveryAddtab.addCell(devstatecodetabcell);
		
		
		/* despatch to*/
		
		
		PdfPTable vendorAddtab = new PdfPTable(3);
		vendorAddtab.setWidthPercentage(100);
		
		try {
			vendorAddtab.setWidths(new float[]{18,3,79});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase supplier = new Phrase("Supplier", font8bold);
		PdfPCell suppliercell = new PdfPCell(supplier);
		suppliercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		suppliercell.setColspan(3);
		suppliercell.setBorder(0);
		vendorAddtab.addCell(suppliercell);
		
		Phrase compname3 = new Phrase(comp.getBusinessUnitName(), font8);
		PdfPCell compnamecell3 = new PdfPCell(compname3);
		compnamecell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnamecell3.setColspan(3);
		compnamecell3.setBorder(0);
		vendorAddtab.addCell(compnamecell3);
		
		Phrase vendoradd = new Phrase(comp.getAddress().getCompleteAddress(), font8);
		PdfPCell vendoraddcell = new PdfPCell(vendoradd);
		vendoraddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendoraddcell.setColspan(3);
		vendoraddcell.setBorder(0);
		vendorAddtab.addCell(vendoraddcell);
		
		Phrase vendorPhoneph = new Phrase("Phone No", font8);
		PdfPCell vendorPhonecell = new PdfPCell(vendorPhoneph);
		vendorPhonecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorPhonecell.setBorder(0);
		vendorAddtab.addCell(vendorPhonecell);
		
		vendorAddtab.addCell(coloncell);
		
		Phrase vendorPhonephval = new Phrase(comp.getCellNumber1()+"", font8);
		PdfPCell vendorPhonecellval = new PdfPCell(vendorPhonephval);
		vendorPhonecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorPhonecellval.setBorder(0);
		vendorAddtab.addCell(vendorPhonecellval);
		
		Phrase vendorConph = new Phrase("Contact No", font8);
		PdfPCell vendorConcell = new PdfPCell(vendorConph);
		vendorConcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorConcell.setBorder(0);
		vendorAddtab.addCell(vendorConcell);
		
		vendorAddtab.addCell(coloncell);
		
		Phrase vendorConphval = new Phrase(comp.getCellNumber2()+"", font8);
		PdfPCell vendorConcellval = new PdfPCell(vendorConphval);
		vendorConcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendorConcellval.setBorder(0);
		vendorAddtab.addCell(vendorConcellval);
		
		Phrase emailVenph = new Phrase("E-Mail", font8);
		PdfPCell emailVencell = new PdfPCell(emailVenph);
		emailVencell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailVencell.setBorder(0);
		vendorAddtab.addCell(emailVencell);
		
		vendorAddtab.addCell(coloncell);
		
		Phrase emailVenvalph = new Phrase(comp.getEmail(), font8);
		PdfPCell emailVenvalcell = new PdfPCell(emailVenvalph);
		emailVenvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailVenvalcell.setBorder(0);
		vendorAddtab.addCell(emailVenvalcell);
		
		Phrase gstinvendorph = new Phrase("GSTIN/UIN", font8);
		PdfPCell gstinvendorcell = new PdfPCell(gstinvendorph);
		gstinvendorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvendorcell.setBorder(0);
		vendorAddtab.addCell(gstinvendorcell);
		
		
		vendorAddtab.addCell(coloncell);
		
		
//		String gstinvalue=null;
//		for (int i = 0; i < vendor.getArticleTypeDetails().size(); i++) {
//
//			if (vendor.getArticleTypeDetails().get(i).getArticleTypeName().contains("GSTIN")) {
//
//				gstinvalue=vendor.getArticleTypeDetails().get(i).getArticleTypeValue();
//			}
//			}

		
//		Phrase gstinval2=new Phrase (gstinvalph,font8);
		PdfPCell gstinvalcell2 = new PdfPCell(gstinvalph);
		gstinvalcell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinvalcell2.setBorder(0);
		vendorAddtab.addCell(gstinvalcell2);
		
		Phrase statevendorname = new Phrase("State Name", font8);
		PdfPCell statenamevendorcell = new PdfPCell(statevendorname);
		statenamevendorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevendorcell.setBorder(0);
		vendorAddtab.addCell(statenamevendorcell);
		
		vendorAddtab.addCell(coloncell);
		
		
		/* state code table*/
		
		PdfPTable statevendorcodetab=new PdfPTable(3);
		statevendorcodetab.setWidthPercentage(100);
		try {
			statevendorcodetab.setWidths(new float[]{40,20,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase statevendorvalname = new Phrase(comp.getAddress().getState(), font8);
		PdfPCell statenamevendorvalcell = new PdfPCell(statevendorvalname);
		statenamevendorvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		statenamevendorvalcell.setBorder(0);
		statevendorcodetab.addCell(statenamevendorvalcell);
		
		
		statevendorcodetab.addCell(statecodecell);
		
		String compstCo = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(comp.getAddress().getState().trim())) {
				compstCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase VendorstateCode=new Phrase(compstCo,font8);
		PdfPCell venstateCodeCell = new PdfPCell(VendorstateCode);
		venstateCodeCell.setBorder(0);
		statevendorcodetab.addCell(venstateCodeCell);
		
		/*state code tab complete*/
		
		PdfPCell venstatecodecell = new PdfPCell(statevendorcodetab);
		venstatecodecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		venstatecodecell.setBorder(0);
		vendorAddtab.addCell(venstatecodecell);
		
		
		PdfPTable leftoutertab=new PdfPTable(1);
		leftoutertab.setWidthPercentage(100);
			
		PdfPCell invoicecell=new PdfPCell(invoiceAddtab);
		invoicecell.setPaddingBottom(5);
		PdfPCell delivery=new PdfPCell(deliveryAddtab);
		delivery.setPaddingBottom(5);
		PdfPCell vendorcell=new PdfPCell(vendorAddtab);
		vendorcell.setPaddingBottom(5);
		
		leftoutertab.addCell(invoicecell);
		leftoutertab.addCell(delivery);
		leftoutertab.addCell(vendorcell);
		
		/*Left Tab complete***/
		
		PdfPTable dateAndVoucherTab=new PdfPTable(2);
		dateAndVoucherTab.setWidthPercentage(100);
		try {
			dateAndVoucherTab.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Phrase vouchernoph = new Phrase("Sales Order No", font8);
		PdfPCell vouchernocell = new PdfPCell(vouchernoph);
		vouchernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vouchernocell.setBorderWidthBottom(0);
		dateAndVoucherTab.addCell(vouchernocell);
		
		Phrase datedph = new Phrase("Dated", font8);
		PdfPCell datedcell = new PdfPCell(datedph);
		datedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedcell.setBorderWidthBottom(0);
		dateAndVoucherTab.addCell(datedcell);
		
		Phrase vouchernovalph = new Phrase(so.getCount()+"", font8);
		PdfPCell vouchernoValcell = new PdfPCell(vouchernovalph);
		vouchernoValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		vouchernoValcell.setBorderWidthTop(0);
		dateAndVoucherTab.addCell(vouchernoValcell);
		
		Phrase datedValph = new Phrase(fmt.format(so.getSalesOrderDate()), font8);
		PdfPCell datedValcell = new PdfPCell(datedValph);
		datedValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedValcell.setBorderWidthTop(0);
		dateAndVoucherTab.addCell(datedValcell);
		
		Phrase createddayph = new Phrase("Credit Days", font8);
		PdfPCell createdDaycell = new PdfPCell(createddayph);
		createdDaycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		createdDaycell.setBorderWidthBottom(0);
		dateAndVoucherTab.addCell(createdDaycell);
		
		Phrase modeph = new Phrase("Mode/Terms of Payment", font8);
		PdfPCell modecell = new PdfPCell(modeph);
		modecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		modecell.setBorderWidthBottom(0);
		dateAndVoucherTab.addCell(modecell);
		
//		int days=0;
//		for (int i = 0; i <so.getPaymentTermsList().size(); i++) {
//			days=so.getPaymentTermsList().get(0).getPayTermDays();
//		}
//		
		Phrase createddayValph = new Phrase(so.getCreditPeriod()+"", font8);
		PdfPCell createdDayValcell = new PdfPCell(createddayValph);
		createdDayValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		createdDayValcell.setBorderWidthTop(0);
		dateAndVoucherTab.addCell(createdDayValcell);
		
		
		
		double percent=0;
		String comment=null;
		int days=0;
		for (int i = 0; i <so.getPaymentTermsList().size(); i++) {
			percent=so.getPaymentTermsList().get(0).getPayTermPercent();
			comment=so.getPaymentTermsList().get(0).getPayTermComment();
			days=so.getPaymentTermsList().get(0).getPayTermDays();
		}
		
		Phrase modeValph = new Phrase(so.getPaymentMethod()+"/"+days+"/"+df2.format(percent)+"%"+"/"+comment, font8);
		PdfPCell modeValcell = new PdfPCell(modeValph);
		modeValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		modeValcell.setBorderWidthTop(0);
		modeValcell.setColspan(2);
		dateAndVoucherTab.addCell(modeValcell);
		
//		Phrase supplierph = new Phrase("Supplier's Ref/Order No", font8);
//		PdfPCell supplicell = new PdfPCell(supplierph);
//		supplicell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		supplicell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(supplicell);
//		
//		Phrase otherph = new Phrase("Other Reference", font8);
//		PdfPCell othercell = new PdfPCell(otherph);
//		othercell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		othercell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(othercell);
//		
//		Phrase suppliervalph = new Phrase(" ", font8);
//		PdfPCell supplivalcell = new PdfPCell(suppliervalph);
//		supplivalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		supplivalcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(supplivalcell);
//		
//		Phrase otherValph = new Phrase(" ", font8);
//		PdfPCell otherValcell = new PdfPCell(otherValph);
//		otherValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		otherValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(otherValcell);
//		
//		Phrase despathph = new Phrase("Despath Through", font8);
//		PdfPCell despathcell = new PdfPCell(despathph);
//		despathcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		despathcell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(despathcell);
//		
//		
//		Phrase destinationph = new Phrase("Destination", font8);
//		PdfPCell destinationcell = new PdfPCell(destinationph);
//		destinationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		destinationcell.setBorderWidthBottom(0);
//		dateAndVoucherTab.addCell(destinationcell);
//		
//		Phrase despathValph=null;
////		if(po.getPOName()!=null){
////			 despathValph = new Phrase(so.get, font8);
////		}
////		else{
//			 despathValph = new Phrase("  ", font8);
////		}
//		
//		PdfPCell despathValcell = new PdfPCell(despathValph);
//		despathValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		despathValcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(despathValcell);
//		
//		
//		
////		String warehouse=null;
////		for (int i = 0; i <po.getProductDetails().size(); i++) {
////			warehouse=po.getProductDetails().get(0).getItemProductWarehouseName();
////		}
//		
//		Phrase destinationValph=null;
////		if(po.getPoWarehouseName()!=null){
////			
////			 destinationValph = new Phrase(po.getPoWarehouseName(), font8);
////		}
////		else{
//			destinationValph = new Phrase("  ", font8);
////		}
//		PdfPCell destinationValphcell = new PdfPCell(destinationValph);
//		destinationValphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		destinationValphcell.setBorderWidthTop(0);
//		dateAndVoucherTab.addCell(destinationValphcell);
		
		
		Phrase termsOfDelph = new Phrase("Terms Of Delivery", font8);
		PdfPCell termsOfDelcell = new PdfPCell(termsOfDelph);
		termsOfDelcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		termsOfDelcell.setBorderWidthBottom(0);
		termsOfDelcell.setColspan(2);
		dateAndVoucherTab.addCell(termsOfDelcell);
		
		Phrase termsOfDelValph=null;
		if(so.getDescription()!=null){
			 termsOfDelValph = new Phrase(so.getDescription(), font8);
		}
		else{
			 termsOfDelValph = new Phrase(" ", font8);
		}
		PdfPCell termsOfDelValcell = new PdfPCell(termsOfDelValph);
		termsOfDelValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		termsOfDelValcell.setBorderWidthTop(0);
		termsOfDelValcell.setColspan(2);
		dateAndVoucherTab.addCell(termsOfDelValcell);
		
		
		PdfPTable mainout=new PdfPTable(2) ;
		mainout.setWidthPercentage(100);
		
		try {
			mainout.setWidths(new float[]{60,40});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell leftcell=new PdfPCell(leftoutertab);
		PdfPCell rightCell=new PdfPCell(dateAndVoucherTab);
		
		mainout.addCell(leftcell);
		mainout.addCell(rightCell);
		
		
		try {
			document.add(mainout);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void Createtitle() {
		
		PdfPTable titleTab = new PdfPTable(1);
		titleTab.setWidthPercentage(100);
		
		Phrase titlephbl = new Phrase(" ", font10);
		PdfPCell titleCellbl = new PdfPCell(titlephbl);
		titleCellbl.setBorder(0);
		titleTab.addCell(titleCellbl);
//		titleTab.addCell(titleCellbl);
//		titleTab.addCell(titleCellbl); 
		
		Phrase titleph = new Phrase("SALES ORDER", font12bold);
		PdfPCell titleCell = new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setPaddingBottom(10);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(titleCell);
		
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private void Createletterhead() {
		/**** image ***/
		DocumentUpload letterheaddocument = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ letterheaddocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.setBorder(0);
			imageSignCell.setImage(image2);
			// imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		Image image1 = null;
//		try {
//			image1 = Image.getInstance("images/Capture.JPG");
//			image1.scalePercent(20f);
//			// image1.setAbsolutePosition(40f,765f);
//			// doc.add(image1);
//
//			imageSignCell = new PdfPCell(image1);
//			// imageSignCell.addElement();
//			imageSignCell.setImage(image1);
//			// imageSignCell.setFixedHeight(40);
//			imageSignCell.setBorder(0);
//			// imageSignCell.setPaddingTop(8);
//			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		PdfPTable letterheadTab = new PdfPTable(1);
		letterheadTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			letterheadTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			letterheadTab.addCell(logoblankcell);
		}

		try {
			document.add(letterheadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
