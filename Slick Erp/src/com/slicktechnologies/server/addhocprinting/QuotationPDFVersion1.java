package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PRAcroForm;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class QuotationPDFVersion1 {

	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 11);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
	Font font12Ul = new Font(Font.FontFamily.HELVETICA, 12, Font.UNDERLINE);


	public Document document;
	Quotation quotation;
	Company comp;
	Customer cust;
	Branch branchEntity;
	Employee employee;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat decimalformat = new DecimalFormat("0.00");
	
	PdfPCell cellBlank;
	
//	float[] column8SerProdCollonWidth = { 0.1f, 0.50f, 0.61f, 0.29f, 0.38f,
//			0.33f, 0.40f };
	
	float[] column8SerProdCollonWidth = { 0.10f, 0.60f, 0.61f, 0.38f,
			0.33f, 0.40f };
	
	boolean branchAsCompanyFlag = false;
	
	public void setservicequotation(Long count) {

		quotation=ofy().load().type(Quotation.class).id(count).now();
		
		if (quotation.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		
		if (quotation.getCompanyId() != null)
		{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).first().now();
		}
		
		branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", quotation.getBranch()).filter("companyId", quotation.getCompanyId()).first().now();
		employee = ofy().load().type(Employee.class).filter("fullname", quotation.getEmployee()).filter("companyId", quotation.getCompanyId()).first().now();
				
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Phrase phblank = new Phrase(" ",font6);
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
		cellBlank=new PdfPCell(phblank);
		cellBlank.setBorder(0);
		
		branchAsCompanyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId());
		
		try {
			BaseFont verdanafont = BaseFont.createFont("verdana.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
			font12 = new Font(verdanafont, 11);
			font12bold = new Font(verdanafont, 11, Font.BOLD);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void createPdf(String preprintStatus) {
		
		
//		if(preprintStatus.equals("yes")){
//			createBlankforUPC();
//		}
//		
//		if(preprintStatus.equals("no")){
//		
////			if(comp.getUploadHeader()!=null){
////				createCompanyNameAsHeader(document,comp);
////			}
////			
////			if(comp.getUploadFooter()!=null){
////				createCompanyNameAsFooter(document,comp);
////			}
////			createBlankforUPC();
//		}
		
		createCustomerInfo();
		createSubjectAndData();
		createProductDetails(preprintStatus);
		CreateTotalChargeAndRemaingPart();
		
	}


	private void CreateTotalChargeAndRemaingPart() {

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100);
		table.setHorizontalAlignment(Element.ALIGN_CENTER);
		

		table.addCell(cellBlank);
		
		String amtInWordsVal = ServiceInvoicePdf.convert(quotation.getNetpayable())+" Only";

		String totalCharges = "Total charges: "+decimalformat.format(quotation.getNetpayable())+"/- ("+amtInWordsVal+")";
		Phrase phsubject = new Phrase(totalCharges,font12);
		PdfPCell cellSubject=new PdfPCell(phsubject);
		cellSubject.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellSubject.setBorder(0);
		table.addCell(cellSubject);
		
		table.addCell(cellBlank);
		
		if(branchAsCompanyFlag && branchEntity!=null){
			if(branchEntity.getBranchCode().equalsIgnoreCase("Pvt Ltd")){
				Phrase gstNote = new Phrase("GST will be applicable at the rate of 18 % extra",font12);
				PdfPCell gstNoteCell=new PdfPCell(gstNote);
				gstNoteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				gstNoteCell.setBorder(0);
				table.addCell(gstNoteCell);
				table.addCell(cellBlank);
			}
			
		}
		
		
		
		Phrase paymentTerms = new Phrase("Payment Term:"+quotation.getPayTerms(),font12);
		PdfPCell paymentTermsCell=new PdfPCell(paymentTerms);
		paymentTermsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		paymentTermsCell.setBorder(0);
		table.addCell(paymentTermsCell);
		table.addCell(cellBlank);
		
		Phrase para1 = new Phrase("Point to be noted, our service center situated in your neighborhood for prompt & quality service. Hence by signing up with PESTMASTERS your total peace of mind is guaranteed. ",font12);
		PdfPCell para1Cell=new PdfPCell(para1);
		para1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para1Cell.setBorder(0);
		table.addCell(para1Cell);
		table.addCell(cellBlank);
		
		Phrase para2 = new Phrase("Await your valuable order.",font12);
		PdfPCell para2Cell=new PdfPCell(para2);
		para2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para2Cell.setBorder(0);
		table.addCell(para2Cell);
		table.addCell(cellBlank);
		table.addCell(cellBlank);

		
		Phrase para3 = new Phrase("Thanking you",font12);
		PdfPCell para3Cell=new PdfPCell(para3);
		para3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para3Cell.setBorder(0);
		table.addCell(para3Cell);
		table.addCell(cellBlank);
		
		Phrase para4 = new Phrase("Yours faithfully,",font12);
		PdfPCell para4Cell=new PdfPCell(para4);
		para4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para4Cell.setBorder(0);
		table.addCell(para4Cell);
		table.addCell(cellBlank);
		table.addCell(cellBlank);
		table.addCell(cellBlank);
		
		Phrase salesPersonName = new Phrase(quotation.getEmployee(),font12);
		PdfPCell salesPersonNameCell=new PdfPCell(salesPersonName);
		salesPersonNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		salesPersonNameCell.setBorder(0);
		table.addCell(salesPersonNameCell);
		
		String companyName = "For";
		if(branchAsCompanyFlag && branchEntity!=null){
			companyName +=" "+branchEntity.getBusinessUnitName();
		}
		else{
			companyName +=" "+comp.getBusinessUnitName();
		}
		
		Phrase phcompanyName = new Phrase(companyName,font12);
		PdfPCell phcompanyNameCell=new PdfPCell(phcompanyName);
		phcompanyNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		phcompanyNameCell.setBorder(0);
		table.addCell(phcompanyNameCell);
		
		
		String cellNo ="";
		if(branchAsCompanyFlag){
			if(employee!=null && employee.getCellNumber1()!=0)
			cellNo +=employee.getCellNumber1();
			
			if(branchEntity!=null){
				if(cellNo.length()==0){
					cellNo +=branchEntity.getCellNumber1();
				}
				else{
					cellNo +=" / "+branchEntity.getCellNumber1();
				}
			}
			
		}
		else{
				if(employee!=null && employee.getCellNumber1()!=0)
				cellNo +=employee.getCellNumber1();
				
					if(cellNo.length()==0){
						cellNo +=comp.getCellNumber1();
					}
					else{
						cellNo +=" / "+comp.getCellNumber1();
					}
		}
		
		Phrase phCellNo = new Phrase(cellNo,font12);
		PdfPCell phCellNoCell=new PdfPCell(phCellNo);
		phCellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		phCellNoCell.setBorder(0);
		table.addCell(phCellNoCell);
		
		table.addCell(cellBlank);

		
		Phrase para5 = new Phrase("Enclosed:",font12);
		PdfPCell para5Cell=new PdfPCell(para5);
		para5Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para5Cell.setBorder(0);
		table.addCell(para5Cell);
		table.addCell(cellBlank);
		
		Phrase para6 = new Phrase("   1) Chemical Details",font12);
		PdfPCell para6Cell=new PdfPCell(para6);
		para6Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para6Cell.setBorder(0);
		table.addCell(para6Cell);
		
		Phrase para7 = new Phrase("   2) Experience Certificates",font12);
		PdfPCell para7Cell=new PdfPCell(para7);
		para7Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para7Cell.setBorder(0);
		table.addCell(para7Cell);
		
		Phrase para8 = new Phrase("   2) Clientele List",font12);
		PdfPCell para8Cell=new PdfPCell(para8);
		para8Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para8Cell.setBorder(0);
		table.addCell(para8Cell);
		
		Phrase para9 = new Phrase("   2) Bouchers",font12);
		PdfPCell para9Cell=new PdfPCell(para9);
		para9Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		para9Cell.setBorder(0);
		table.addCell(para9Cell);
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private void createProductDetails(String preprintStatus) {
		
		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		if(preprintStatus.equals("yes")){
//			createBlankforUPC();
//		}
//		
//		if(preprintStatus.equals("no")){
//		
//			if(comp.getUploadHeader()!=null){
//				createCompanyNameAsHeader(document,comp);
//			}
//			
//			if(comp.getUploadFooter()!=null){
//				createCompanyNameAsFooter(document,comp);
//			}
//			createBlankforUPC();
//		}

		PdfPTable quotationDetailstable = new PdfPTable(1);
		quotationDetailstable.setWidthPercentage(100);
		quotationDetailstable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase phsubject = new Phrase("Quotation Details:",font12Ul);
		PdfPCell cellSubject=new PdfPCell(phsubject);
		cellSubject.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellSubject.setBorder(0);
		quotationDetailstable.addCell(cellSubject);
		quotationDetailstable.addCell(cellBlank);
		
		try {
			document.add(quotationDetailstable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		PdfPTable producttableheader = new PdfPTable(6);
		producttableheader.setWidthPercentage(100);
		try {
			producttableheader.setWidths(column8SerProdCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		producttableheader.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase srNophrase = new Phrase("Sr.No", font12bold);
		PdfPCell srNoCell = new PdfPCell(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase servicesPhase = new Phrase("Services", font12bold);
		PdfPCell servicesCell = new PdfPCell(servicesPhase);
		servicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase areaCoverage = new Phrase("Area Coverage", font12bold);
		PdfPCell areaCoverageCell = new PdfPCell(areaCoverage);
		areaCoverageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		areaCoverageCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
//		Phrase contractTerm = new Phrase("Contract Term", font12bold);
//		PdfPCell contractTermCell = new PdfPCell(contractTerm);
//		contractTermCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		contractTermCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase remark = new Phrase("Contract Term", font12bold);
		PdfPCell remarkCell = new PdfPCell(remark);
		remarkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		remarkCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase serviceFrequency = new Phrase("Service Frequency", font12bold);
		PdfPCell serviceFrequencyCell = new PdfPCell(serviceFrequency);
		serviceFrequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceFrequencyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase charges = new Phrase("Charges", font12bold);
		PdfPCell chargesCell = new PdfPCell(charges);
		chargesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		chargesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		producttableheader.addCell(srNoCell);
		producttableheader.addCell(servicesCell);
		producttableheader.addCell(areaCoverageCell);
//		producttableheader.addCell(contractTermCell);
		producttableheader.addCell(remarkCell);
		producttableheader.addCell(serviceFrequencyCell);
		producttableheader.addCell(chargesCell);
		
		try {
			document.add(producttableheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable producttable = new PdfPTable(6);
		producttable.setWidthPercentage(100);
		try {
			producttable.setWidths(column8SerProdCollonWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		producttable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		for(int i=0;i<quotation.getItems().size();i++){
			
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font12);
			PdfPCell srNocell = new PdfPCell(srNo);
			srNocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(srNocell);
			
			Phrase productName = new Phrase(quotation.getItems().get(i).getProductName(), font12);
			PdfPCell productNameCell = new PdfPCell(productName);
			productNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(productNameCell);
			
			System.out.println("productName.getContent().length() "+productName.getContent().length());
			System.out.println("quotation.getItems().get(i).getProductName() "+quotation.getItems().get(i).getProductName().length());

			
			Phrase coverageArea = new Phrase(quotation.getItems().get(i).getPremisesDetails(), font12);
			PdfPCell coverageAreaCell = new PdfPCell(coverageArea);
			coverageAreaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(coverageAreaCell);
			
//			Phrase phcontractTerm = new Phrase(quotation.getItems().get(i).getDuration()+"", font12);
//			PdfPCell phcontractTermCell = new PdfPCell(phcontractTerm);
//			phcontractTermCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			producttable.addCell(phcontractTermCell);
			
			
			Phrase phRemark = new Phrase(quotation.getItems().get(i).getRemark()+"", font12);
			PdfPCell phRemarkCell = new PdfPCell(phRemark);
			phRemarkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(phRemarkCell);
			
			Phrase phserviceFrequency = new Phrase(quotation.getItems().get(i).getNumberOfServices()+"", font12);
			PdfPCell phserviceFrequencyCell = new PdfPCell(phserviceFrequency);
			phserviceFrequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(phserviceFrequencyCell);
			
			double totalAmount = getTotalAmount(quotation.getItems().get(i));
			
			Phrase phtotalAmount = new Phrase("Rs. "+decimalformat.format(totalAmount)+"/-", font12);
			PdfPCell totalAmountCell = new PdfPCell(phtotalAmount);
			totalAmountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(totalAmountCell);
			
		}
		

		System.out.println("producttable.getRows().size() "+producttable.getRows());
		
		try {
			document.add(producttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}


	private double getTotalAmount(SalesLineItem object) {
		
		double total = 0;
		SuperProduct product = object.getPrduct();
		double tax = removeAllTaxes(product);
		double origPrice = object.getPrice() - tax;
		
		if (object.isServiceRate()&&(object.getArea()==null||object.getArea().equals("")||object.getArea().equalsIgnoreCase("NA"))) {
			origPrice=origPrice*object.getNumberOfServices();
		}

		if ((object.getPercentageDiscount() == null && object.getPercentageDiscount() == 0)&& (object.getDiscountAmt() == 0)) {

			System.out.println("inside both 0 condition");
			// total=origPrice;

			// new code
			/********************
			 * Square Area Calculation code added in if condition and
			 * without square area calculation code in else block
			 ***************************/
			System.out.println("Get value from area =="+ object.getArea());
			System.out.println("total amount before area calculation =="+ total);
			if (!object.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(object.getArea());
				total = origPrice * area;
				System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+ total);
			} else {
				total = origPrice;
			}
		}
		else if ((object.getPercentageDiscount() != null)&& (object.getDiscountAmt() != 0)) {

			System.out.println("inside both not null condition");

			// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
			// total=total-object.getDiscountAmt();
			// total=total*object.getQty();

			if (!object.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(object.getArea());
				total = origPrice * area;
//				System.out.println("total before discount per ===="+ total);
				total = total- (total * object.getPercentageDiscount() / 100);
//				System.out.println("after discount per total === "+ total);
				total = total - object.getDiscountAmt();
//				System.out.println("after discount AMT total === "+ total);
//				System.out.println(" Final TOTAL   discount per &  discount Amt ==="+ total);
			} else {
//				System.out.println(" normal === total " + total);
				total = origPrice;
				total = total- (total * object.getPercentageDiscount() / 100);
				total = total - object.getDiscountAmt();
			}

		} else {
			System.out.println("inside oneof the null condition");

			if (object.getPercentageDiscount() != null) {
				// System.out.println("inside getPercentageDiscount oneof the null condition");
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);

				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
//					System.out.println("total before discount per ===="+ total);
					total = total- (total * object.getPercentageDiscount() / 100);
//					System.out.println("after discount per total === "+ total);
				} else {
//					System.out.println("old code");
					total = origPrice;
					total = total- (total * object.getPercentageDiscount() / 100);
				}
			} else {
				// System.out.println("inside getDiscountAmt oneof the null condition");
				// total=origPrice-object.getDiscountAmt();

				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
//					System.out.println("total before discount amt ===="+ total);
					total = total - object.getDiscountAmt();
//					System.out.println("after discount amt total === "+ total);

				} else {
					total = total - object.getDiscountAmt();
				}
			}

			// total=total*object.getQty();

		}

		return total;
	}


	private double removeAllTaxes(SuperProduct entity) {


		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			/**
			 * Date 13-06-2018
			 * By Vijay 
			 * Des :- above old code commented and new code added below
			 */
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			/**
			 * ends here
			 */
		}
		tax = retrVat + retrServ;
		return tax;
	
		
	}


	private void createSubjectAndData() {

		PdfPTable subjecttable = new PdfPTable(1);
		subjecttable.setWidthPercentage(100);
		subjecttable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		subjecttable.addCell(cellBlank);
		
		Phrase phsubject = new Phrase("Subject: Quotation for various Pest Management Services",font12bold);
		PdfPCell cellSubject=new PdfPCell(phsubject);
		cellSubject.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellSubject.setBorder(0);
		subjecttable.addCell(cellSubject);
		
		subjecttable.addCell(cellBlank);

		Phrase phdearsir = new Phrase("Dear Sir,",font12);
		PdfPCell celldearsir=new PdfPCell(phdearsir);
		celldearsir.setHorizontalAlignment(Element.ALIGN_LEFT);
		celldearsir.setBorder(0);
		subjecttable.addCell(celldearsir);
		
		subjecttable.addCell(cellBlank);

		Phrase para1 = new Phrase("We are happy to introduce pest control services by PESTMASTERS.",font12);
		PdfPCell cellpara1=new PdfPCell(para1);
		cellpara1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara1.setBorder(0);
		subjecttable.addCell(cellpara1);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para2 = new Phrase("Its founders, with over 25 years of industry experience & having worked with top companies from the field, have based its pest control philosophy on customer safety, health, quality & costs concerns in mind.",font12);
		PdfPCell cellpara2=new PdfPCell(para2);
		cellpara2.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara2.setBorder(0);
		subjecttable.addCell(cellpara2);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para3 = new Phrase("Cockroaches, Ants, Rats, Termite & Mosquitoes are highly recommended pest management services.",font12);
		PdfPCell cellpara3=new PdfPCell(para3);
		cellpara3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara3.setBorder(0);
		subjecttable.addCell(cellpara3);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para4 = new Phrase("Cockroaches breed in large numbers & infest in home and commercial premises like kitchen, food counters, Restaurants, hospital, washrooms, manholes, drainage chambers etc. Apart from their big nos presence, are known to spread diseases like salmonellosis, typhoid, cholera, gastro, dysentery & allergies. Ants are also nuisance & some cause painful bites.",font12);
		PdfPCell cellpara4=new PdfPCell(para4);
		cellpara4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara4.setBorder(0);
		subjecttable.addCell(cellpara4);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para5 = new Phrase("Rats are best kept away from any premises for their destructive ways. Known for spoiling raw materials, interior, upholstery & contaminating food through their droppings, urine & feeding. Apart from spread of several diseases, probability of salmonellosis / food poisoning is real. Damage to electrical is common which has its own risks.",font12);
		PdfPCell cellpara5=new PdfPCell(para5);
		cellpara5.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara5.setBorder(0);
		subjecttable.addCell(cellpara5);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para6 = new Phrase("Termites as a pest stands at no.1 with regards to irreversible damage to property which include all types of woodwork, furniture, clothing, upholstery, books & structural damage to civil structure. Termites can cause severe damage to human homes.",font12);
		PdfPCell cellpara6=new PdfPCell(para6);
		cellpara6.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara6.setBorder(0);
		subjecttable.addCell(cellpara6);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para7 = new Phrase("Mosquito is no.1 nuisance pest & serious health hazard, responsible for spreading fatal diseases such as malaria, dengue & other debilitating health conditions. Long lasting residual action spray application coupled with fogging in the common areas shall be carried out to get relief from mosquito menace. ",font12);
		PdfPCell cellpara7=new PdfPCell(para7);
		cellpara7.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara7.setBorder(0);
		subjecttable.addCell(cellpara7);
		
		subjecttable.addCell(cellBlank);
		
		Phrase para8 = new Phrase("PESTMASTERS, with its vast experience, shall employ very latest pest control technologies & practices for your society.",font12);
		PdfPCell cellpara8=new PdfPCell(para8);
		cellpara8.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellpara8.setBorder(0);
		subjecttable.addCell(cellpara8);
		subjecttable.addCell(cellBlank);
		
		try {
			document.add(subjecttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void createCustomerInfo() {
		
		PdfPTable customerInfotable = new PdfPTable(1);
		customerInfotable.setWidthPercentage(100);
		customerInfotable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase phdate = new Phrase("Date : "+fmt.format(quotation.getQuotationDate()), font12);
		PdfPCell cellDate=new PdfPCell(phdate);
		cellDate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellDate.setBorder(0);
		customerInfotable.addCell(cellDate);
		
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
//		PdfPCell cellBlank=new PdfPCell(blank);
//		cellBlank.setBorder(0);
		
		customerInfotable.addCell(cellBlank);

		Phrase phTo = new Phrase("To,", font12);
		PdfPCell cellTo=new PdfPCell(phTo);
		cellTo.setBorder(0);
		cellTo.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerInfotable.addCell(cellTo);

		customerInfotable.addCell(cellBlank);

		String customerName="";
		if(cust.isCompany()){
			customerName = cust.getCompanyName();
		}
		else{
			customerName = cust.getFullname();
		}
		
		Phrase phcustomerName = new Phrase(customerName, font12);
		PdfPCell cellcustomerName=new PdfPCell(phcustomerName);
		cellcustomerName.setBorder(0);
		cellcustomerName.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerInfotable.addCell(cellcustomerName);
		
//		String customerAddress = "";
//		if(cust.getAdress().getAddrLine1()!=null && !cust.getAdress().getAddrLine1().equals("")){
//			customerAddress += cust.getAdress().getAddrLine1()+",";
//		}
//		if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
//			customerAddress += cust.getAdress().getAddrLine2()+",";
//		}
//		if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
//			customerAddress += cust.getAdress().getLandmark()+",";
//		}
//		if(cust.getAdress().getLocality()!=null && !cust.getAdress().getLocality().equals("")){
//			customerAddress += cust.getAdress().getLocality()+",";
//		}
		
		try {
			document.add(customerInfotable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		float[] columnWidths = { 1f, 2f };
		
		PdfPTable customerInfotable2 = new PdfPTable(2);
		customerInfotable2.setWidthPercentage(100);
		try {
			customerInfotable2.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		customerInfotable2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase phcustomerAddress = new Phrase(cust.getAdress().getCompleteAddress(), font12);
		PdfPCell cellcustomerAddress=new PdfPCell(phcustomerAddress);
		cellcustomerAddress.setBorder(0);
		cellcustomerAddress.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerInfotable2.addCell(cellcustomerAddress);
		
		customerInfotable2.addCell(cellBlank);
		
		try {
			document.add(customerInfotable2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();
		if(branchAsCompanyFlag && branchEntity!=null && branchEntity.getUploadHeader()!=null){
			document = branchEntity.getUploadHeader();
		}
		
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		if(branchAsCompanyFlag && branchEntity!=null && branchEntity.getUploadFooter()!=null){
			document = branchEntity.getUploadFooter();
		}
		
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
