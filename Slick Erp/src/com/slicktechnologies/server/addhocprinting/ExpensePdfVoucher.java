package com.slicktechnologies.server.addhocprinting;

//package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.account.multipleexpensemanagment.ExpenseManagement;
//import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class ExpensePdfVoucher {

	
	Logger logger = Logger.getLogger("ExpensePdf.class");

	public Document document;

	Company comp;
	Customer cust;
	MultipleExpenseMngt exp;
	Phrase chunk;
	float round = 0.00f;
	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
	float[] colWidth = { 70f, 15f, 15f };
	float[] columnWidth = { (float) 33.33, (float) 33.33, (float) 33.33 };
	float[] col1Width = { 10f, 90f };
	float[] col2Width = { 25f, 75f };

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul , font9boldul, font9ul;

	private Object expenseList;		
            
	public ExpensePdfVoucher() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
		font9ul = new Font(Font.FontFamily.HELVETICA, 8,Font.UNDERLINE);
	}

	public void setpdfexpense(Long count)
	{
		  exp = ofy().load().type(MultipleExpenseMngt.class).id(count).now();
		  System.out.println("Exp is "+ exp.getCompanyId());
	
//		 load Company 
		
		if (exp.getCompanyId()!=null)
		{
			System.out.println("Exp Not Null "+ exp.getCompanyId());
			comp = ofy().load().type(Company.class).filter("companyId", exp.getCompanyId()).first().now();
		}
		else
		{
			System.out.println("Else condition "+ exp.getCompanyId());
			comp = ofy().load().type(Company.class).first().now();
		}
		
//		Load Customer
		
		if (exp.getCompanyId() != null){
			cust = ofy().load().type(Customer.class).filter("companyId", exp.getCompanyId()).filter("count", exp.getUserId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", exp.getUserId()).first().now();
		}
		
		 fmt.setTimeZone(TimeZone.getTimeZone("IST"));

	}

	public void createPdf() {		
		
		if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document , comp);
		}
		
		headerTable();
		custDetails();
		descriptionTbl();
		footer();
		
	}


private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}



private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}



	private void headerTable()    
	{   
		PdfPTable headerTbl = new PdfPTable(3);
		headerTbl.setWidthPercentage(100);
		
		try {
			headerTbl.setWidths(new float[]{30,40,30});
		} catch (DocumentException e) 
		{
			e.printStackTrace();
		}
		
		Phrase blank=new Phrase ();
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		headerTbl.addCell(blankCell);
		
		
		//Phrase compName = new Phrase(comp.getBusinessUnitName(),font12);
		Phrase compName=null;
		if(comp.getBusinessUnitName()!=null){
			compName=new Phrase(comp.getBusinessUnitName(),font14bold);
		}else{
			compName=new Phrase(" ",font10bold);
		}
		PdfPCell compNameCell = new PdfPCell(compName);
		compNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		compNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        compNameCell.setBorder(0);
        headerTbl.addCell(compNameCell);
        
        
        
        
    	Phrase vmum = new Phrase("",font9);    // Value  unknown 
		PdfPCell vmumCell = new PdfPCell(vmum);
		vmumCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		vmumCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		vmumCell.setBorder(0);
		headerTbl.addCell(vmumCell);
		
		
		Phrase blank2=new Phrase ();
		PdfPCell blankCell2=new PdfPCell(blank2);
		blankCell2.setBorder(0);
		headerTbl.addCell(blankCell2);
		
		
		Phrase vouvherLbl = new Phrase("VOUCHER RECEIPT",font9boldul);  //Underline
		PdfPCell voucherLblCell = new PdfPCell(vouvherLbl);
		voucherLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		voucherLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		voucherLblCell.setBorder(0);
		headerTbl.addCell(voucherLblCell);
		
		
		Phrase blank3=new Phrase ();
		PdfPCell blankCell3=new PdfPCell(blank3);
		blankCell3.setBorder(0);
		headerTbl.addCell(blankCell3);
		
		try {
			document.add(headerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
		/**
		 * End Here
		 */
		
		
		
		/**
		 * Ajinkya code for Normal way to display header 
		 */
		
		
//		Phrase compName = new Phrase(comp.getBusinessUnitName(),font12);
//		PdfPCell compNameCell = new PdfPCell(compName);
//        compNameCell.setBorder(0);
//		
//        Phrase blnk = new Phrase(" ",font12);
//		PdfPCell blnk1Cell = new PdfPCell(blnk);
//        blnk1Cell.setBorder(0);
//        
//		Phrase vmum = new Phrase("V/MUM/ __________",font9);    // Value  unknown 
//		PdfPCell vmumCell = new PdfPCell(vmum);
//		vmumCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		vmumCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		vmumCell.setBorder(0);
//		
//		
//		headerTbl.addCell(blnk1Cell);
//		headerTbl.addCell(blnk1Cell);
//		headerTbl.addCell(blnk1Cell);
//		headerTbl.addCell(blnk1Cell);
//		headerTbl.addCell(compNameCell);
//		headerTbl.addCell(vmumCell);
//		headerTbl.addCell(compNameCell);
//		headerTbl.addCell(compNameCell);
	
		/**
		 *   End Here 
		 */
		
		
		
	

	private void custDetails()
	{ 
	  
	 PdfPTable customerDetailsTbl = new PdfPTable(6);	
	 customerDetailsTbl.setWidthPercentage(100);
	 customerDetailsTbl.setSpacingBefore(10);
	 
	 try {
		customerDetailsTbl.setWidths(new float [] {7,2,50,20,2,15});
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	Phrase paidto = new Phrase("Paid to ",font9);
	PdfPCell paidtoCell = new PdfPCell(paidto);
	paidtoCell.setBorder(0);
	paidtoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	paidtoCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(paidtoCell);
	
	Phrase colon = new Phrase (":",font9);	 
	PdfPCell colonCell = new PdfPCell(colon);
	colonCell.setBorder(0);
	colonCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	colonCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(colonCell);
	
	//Phrase paidtoVal = new Phrase (" ",font9ul);
	System.out.print("1111"+exp.getEmployee());
	Phrase paidtoVal=null;
	
	if(exp.getExpenseList().get(0).getExpenseDate()!=null){
		
		paidtoVal=new Phrase(exp.getEmployee(),font9);
	}else{
		paidtoVal=new Phrase(" ",font10bold);
	}
	PdfPCell paidtoValCell = new PdfPCell(paidtoVal);
	paidtoValCell.setBorderWidthLeft(0);
	paidtoValCell.setBorderWidthRight(0);
	paidtoValCell.setBorderWidthTop(0);
	//paidtoValCell.setBorder(0);
	paidtoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	paidtoValCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(paidtoValCell);
	
	
	Phrase voucherNo = new Phrase ("Voucher No.",font9);
	PdfPCell voucherNoCell = new PdfPCell(voucherNo);
	voucherNoCell.setBorder(0);
	voucherNoCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	voucherNoCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(voucherNoCell);
	
	
	Phrase colon2 = new Phrase (":",font9);	 
	PdfPCell colonCell2 = new PdfPCell(colon2);
	colonCell2.setBorder(0);
	colonCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
	colonCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(colonCell2);
	
	
	Phrase voucherNoVal= new Phrase ();
	System.out.print("222"+exp.getCount());
	if(exp.getCount()!=0){
		voucherNoVal=new Phrase(exp.getCount()+"",font9);
	}else{
		voucherNoVal=new Phrase(" ",font9ul);
	}
	PdfPCell voucherNoValCell = new PdfPCell(voucherNoVal);
	//voucherNoValCell.setBorder(0);
	voucherNoValCell.setBorderWidthLeft(0);
	voucherNoValCell.setBorderWidthRight(0);
	voucherNoValCell.setBorderWidthTop(0);
	voucherNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	voucherNoValCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(voucherNoValCell);
	
	Phrase project = new Phrase ("Project",font9);
	PdfPCell projectCell = new PdfPCell(project);
	projectCell.setBorder(0);
	projectCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	projectCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(projectCell);
	
	Phrase colon3 = new Phrase (":",font9);	 
	PdfPCell colonCell3 = new PdfPCell(colon3);
	colonCell3.setBorder(0);
	colonCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	colonCell3.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(colonCell3);
	
	//Phrase projectVal = new Phrase ("",font9);
	Phrase projectVal=null;
	if(exp.getAccount()!=null){
		projectVal=new Phrase(exp.getAccount(),font9);
	}else{
		projectVal=new Phrase(" ",font10bold);
	}
	PdfPCell projectValCell = new PdfPCell(projectVal);
	//projectValCell.setBorder(0);
	projectValCell.setBorderWidthLeft(0);
	projectValCell.setBorderWidthRight(0);
	projectValCell.setBorderWidthTop(0);
	projectValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	projectValCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(projectValCell);
	
	
	Phrase date = new Phrase("Date",font9);
	PdfPCell dateCell = new PdfPCell(date);
	dateCell.setBorder(0);
	dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	dateCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(dateCell);
	
	Phrase colon4 = new Phrase (":",font9);	 
	PdfPCell colonCell4 = new PdfPCell(colon4);
	colonCell4.setBorder(0);
	colonCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
	colonCell4.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(colonCell4);
	
	
	//Phrase dateVal = new Phrase(" ",font9ul);
	Phrase dateVal=null;
	if(exp.getExpenseList().get(0).getExpenseDate()!=null){
		dateVal=new Phrase(fmt2.format(exp.getExpenseList().get(0).getExpenseDate()),font9);
	}else{
		dateVal=new Phrase(" ",font9ul);
	}
	PdfPCell dateValCell = new PdfPCell(dateVal);
	dateValCell.setBorderWidthLeft(0);
	dateValCell.setBorderWidthRight(0);
	dateValCell.setBorderWidthTop(0);
	//dateValCell.setBorder(0);
	dateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dateValCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	customerDetailsTbl.addCell(dateValCell);
	
	
	try {
		document.add(customerDetailsTbl);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
	
//	
////	Phrase blnk = new Phrase(" ",font9);
////	PdfPCell blnkCell = new PdfPCell(blnk);
////	blnkCell.setBorder(0);
//	
//	//cells added into tbl 
//	
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//							
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//	
//	
//	customerDetailsTbl.addCell(colonCell);
//	customerDetailsTbl.addCell(paidtoValCell);
//	
//	customerDetailsTbl.addCell(voucherNoCell);
//	customerDetailsTbl.addCell(colonCell);
//	customerDetailsTbl.addCell(voucherNoValCell);
//	
//	customerDetailsTbl.addCell(projectCell);
//	customerDetailsTbl.addCell(colonCell);
//	customerDetailsTbl.addCell(projectValCell);
//	
//	customerDetailsTbl.addCell(dateCell);
//	customerDetailsTbl.addCell(colonCell);
//	customerDetailsTbl.addCell(dateValCell);
//	
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//							
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//	customerDetailsTbl.addCell(blnkCell);
//	
	
			
	
	private void descriptionTbl() 
      {
	  
	  PdfPTable descriptionTable = new PdfPTable(6);
	  descriptionTable.setWidthPercentage(100);
	  descriptionTable.setSpacingBefore(10);
	  
	  
	  try {
		descriptionTable.setWidths(new float[] {10,15,25,25,15,10});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
			
	  Phrase srNo = new Phrase("Sr. No.",font9bold);
	  PdfPCell  srNoCell = new PdfPCell(srNo);
	  srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  srNoCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	  descriptionTable.addCell(srNoCell);
	  
	  Phrase invoice = new Phrase("Invoice No",font9bold);
	  PdfPCell  invoiceCell = new PdfPCell(invoice);
	  invoiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  invoiceCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	  descriptionTable.addCell(invoiceCell);
	  
	  Phrase group = new Phrase("Group",font9bold);
	  PdfPCell  groupcell = new PdfPCell(group);
	  groupcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  groupcell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	  descriptionTable.addCell(groupcell);
	  
	  
	  Phrase catagory = new Phrase("Catagory",font9bold);
	  PdfPCell  catagoryCell = new PdfPCell(catagory);
	  catagoryCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  catagoryCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	  descriptionTable.addCell(catagoryCell);
	  
	  
	  Phrase type = new Phrase("Type",font9bold);
	  PdfPCell  typeCell = new PdfPCell(type);
	  typeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  typeCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	  descriptionTable.addCell(typeCell);
	  
	  
	  
	  Phrase amount  = new Phrase("Amount",font9bold);
	  PdfPCell amountCell = new PdfPCell(amount);
	  amountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	  amountCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	  descriptionTable.addCell(amountCell);
	  
	  try {
		document.add(descriptionTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	 
	     
	  PdfPTable descriptionValTable = new PdfPTable(6);
	  descriptionValTable.setWidthPercentage(100);
	  
	  try {
		  descriptionValTable.setWidths(new float[] {10,15,25,25,15,10});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	  
	  int expListSize=exp.getExpenseList().size();
	  
		for (int i = 0; i < exp.getExpenseList().size(); i++) {

			System.out.print("first forloop" + exp.getExpenseList());
			chunk = new Phrase((i + 1) + "", font9);
			PdfPCell Pdfsrnocell = new PdfPCell(chunk);
			Pdfsrnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			Pdfsrnocell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			descriptionValTable.addCell(Pdfsrnocell);

			chunk = new Phrase(exp.getExpenseList().get(i)
					.getInvoiceNumber(), font9);
			PdfPCell invoicevalcell = new PdfPCell(chunk);
			invoicevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			invoicevalcell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			descriptionValTable.addCell(invoicevalcell);

			chunk = new Phrase(exp.getExpenseList().get(i)
					.getExpenseGroup(), font9);
			PdfPCell groupvalcell = new PdfPCell(chunk);
			groupvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			groupvalcell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			descriptionValTable.addCell(groupvalcell);

			chunk = new Phrase(exp.getExpenseList().get(i)
					.getExpenseCategory(), font9);
			PdfPCell catagoryvalcell = new PdfPCell(chunk);
			catagoryvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			catagoryvalcell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			descriptionValTable.addCell(catagoryvalcell);

			chunk = new Phrase(
					exp.getExpenseList().get(i).getExpenseType(), font9);
			PdfPCell typevalcell = new PdfPCell(chunk);
			typevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			typevalcell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			descriptionValTable.addCell(typevalcell);

			chunk = new Phrase(df.format(exp.getExpenseList().get(i)
					.getAmount()), font9);
			PdfPCell amountvalcell = new PdfPCell(chunk);
			amountvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			amountvalcell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			descriptionValTable.addCell(amountvalcell);

		}
		
		if(expListSize<5){
			int noOfBlankRow=(5-expListSize)*6;
			
			  for(int i=0;i<noOfBlankRow;i++){
			  Phrase blank=new Phrase(" ", font9);
			  PdfPCell blankcell=new PdfPCell();
			  blankcell.addElement(blank);
			  descriptionValTable.addCell(blankcell);
			}
		}
//	
		Phrase totalRsPh=new Phrase("Total Rs. ", font9);
		PdfPCell totalRsCell=new PdfPCell(totalRsPh);
		//totalRsCell.addElement(totalRsPh);
		totalRsCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalRsCell.setBorder(0);
		totalRsCell.setColspan(5);
		descriptionValTable.addCell(totalRsCell);
		
		 Phrase totalVal = new Phrase();
		 // Phrase totalVal;
			if(exp.getTotalAmount()!=0){
				totalVal=new Phrase(df.format(exp.getTotalAmount()),font9);
			}else{
				totalVal=new Phrase(" ",font9);
			}
		  PdfPCell totalValCell = new PdfPCell(totalVal);
		  totalValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		  totalValCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		 // totalCell.setColspan(4);
		 // totalCell.setBorder(0);
		  descriptionValTable.addCell(totalValCell); 
		  
		  
	  
	  try {
			document.add(descriptionValTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	  
//	  PdfPTable descriptionValTable2 = new PdfPTable(2);
//	  descriptionValTable2.setWidthPercentage(100);
//	  
//	  try {
//		  descriptionValTable2.setWidths(new float[] {90,10});
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//	  
//	  
//	  Phrase total = new Phrase(" Total Rs.",font9);
//	  PdfPCell totalCell = new PdfPCell(total);
//	  totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	  totalCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	  totalCell.setBorder(0);
//	  descriptionValTable2.addCell(totalCell); 
//	  
//	  
//	 
//	  
//	  
//	  try {
//		document.add(descriptionValTable2);
//	} catch (DocumentException e) {
//		e.printStackTrace();
//	}
	  
	     }  
		

   private void footer() 
   {
	  PdfPTable footerTbl = new PdfPTable(3); 
	  footerTbl.setWidthPercentage(100);
	  footerTbl.setSpacingBefore(10);
	  
	  
	  try {
		  footerTbl.setWidths(new float[] {20,60,34});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  Phrase amountInWords = new Phrase("Amount in words: ",font9bold);
	  PdfPCell aiwCell = new PdfPCell();
	  aiwCell.addElement(amountInWords);
	  aiwCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  aiwCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  aiwCell.setBorder(0);
	  footerTbl.addCell(aiwCell);
	  
	  //Phrase amountVal = new Phrase("amount",font9ul);
	  String amountInWord = ServiceInvoicePdf.convert(exp.getTotalAmount());
	  Phrase amountVal = new Phrase(" " + amountInWord + " Only ", font9);
	  PdfPCell amountValcell  = new PdfPCell();
	  amountValcell.addElement(amountVal);
	  amountValcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  amountValcell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  //amountValcell.setBorder(0);
	  amountValcell.setBorderWidthLeft(0);
	  amountValcell.setBorderWidthRight(0);
	  amountValcell.setBorderWidthTop(0);
	  footerTbl.addCell(amountValcell);
	  
	  Phrase blank=new Phrase(" ", font9);
	  PdfPCell blankcell=new PdfPCell();
	  blankcell.addElement(blank);
	  blankcell.setBorder(0);
	  footerTbl.addCell(blankcell);
	 
	  
	  
//	  Phrase description = new Phrase("Description: ",font9);
//	  PdfPCell descriptionCell = new PdfPCell();
//	  descriptionCell.addElement(description);
//	  descriptionCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	  descriptionCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	  descriptionCell.setBorder(0);
//	  footerTbl.addCell(descriptionCell);
////	  
	 // Phrase descriptionVal = new Phrase("amount",font9ul);
	  System.out.print("333"+exp.getDescription());
	  Phrase descriptionVal=null;
		if(exp.getDescription()!=null){
			
			descriptionVal=new Phrase("Description"+exp.getDescription(),font9);
		}else{
			descriptionVal=new Phrase(" ",font9);
		}
	  PdfPCell descriptionValCell  = new PdfPCell();
	  descriptionValCell.addElement(descriptionVal);
	  descriptionValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	  descriptionValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	  descriptionValCell.setBorder(0);
	  descriptionValCell.setColspan(2);
	  footerTbl.addCell(descriptionValCell);
	  
	  Phrase blank6=new Phrase(" ", font9);
	  PdfPCell blankcel6=new PdfPCell();
	  blankcel6.addElement(blank6);
	  blankcel6.setBorder(0);
	  footerTbl.addCell(blankcel6);
	 
	 
	  
	  try {
		document.add(footerTbl);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	   
	   PdfPTable signatureTbl = new PdfPTable (3);
	   signatureTbl.setWidthPercentage(80);
	   signatureTbl.setSpacingBefore(40);
	   
	   try {
		signatureTbl.setWidths(new float [] {20,30,30});
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
       
	   Phrase accounts = new Phrase("Accounts ",font9bold);
	   PdfPCell accCell = new PdfPCell(accounts);
	   accCell.setBorder(0);
	   accCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	   accCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	   signatureTbl.addCell(accCell);
	   
//	// Phrase accountsval = new Phrase("Accounts",font9);
//	   Phrase accountsval=null;
//	   System.out.print("333"+exp.getAccount());
//		if(exp.getAccount()!=null){
//			accountsval=new Phrase(exp.getAccount(),font9);
//		}else{
//			accountsval=new Phrase(" ",font10bold);
//		}
//	   PdfPCell accValCell = new PdfPCell(accountsval);
//	   accValCell.setBorder(0);
//	   accValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	   accValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	   signatureTbl.addCell(accValCell);
	   
	  
	   Phrase approvedBy = new Phrase("   Approved By ",font9bold);
	   PdfPCell aprdbyCell = new PdfPCell(approvedBy);
	   aprdbyCell.setBorder(0);
	   aprdbyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	   aprdbyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	   signatureTbl.addCell(aprdbyCell);
	   
	   
//	// Phrase approvedByval = new Phrase("Approved By",font9);
//	   Phrase approvedByval=null;
//		if(exp.getApproverName()!=null){
//			approvedByval=new Phrase(exp.getApproverName(),font9);
//		}else{
//			approvedByval=new Phrase(" ",font10bold);
//		}
//	   PdfPCell aprdbyValCell = new PdfPCell(approvedByval);
//	   aprdbyValCell.setBorder(0);
//	   aprdbyValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	   aprdbyValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	   signatureTbl.addCell(aprdbyValCell);
//	 
	   
	   Phrase receivedBy = new Phrase("Received By ",font9bold);
	   PdfPCell revdByCell = new PdfPCell(receivedBy);
	   revdByCell.setBorder(0);
	   revdByCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	   revdByCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	   signatureTbl.addCell(revdByCell);
	  
	   
//	   Phrase receivedByval = new Phrase("Received By",font9);
////	   Phrase receivedByval=null;
////		if(exp.get!=null){
////			receivedByval=new Phrase(fmt.format(exp.getExpenseList().get(0).getExpenseDate()),font9);
////		}else{
////			receivedByval=new Phrase(" ",font10bold);
////		}
//	   PdfPCell revdByValCell = new PdfPCell(receivedByval);
//	   revdByValCell.setBorder(0);
//	   revdByValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	   revdByValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	   signatureTbl.addCell(revdByValCell);
	   
	   try {
		document.add(signatureTbl);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   
	   
	   
   }

	
	/**
	 *  code End Here 
	 */
}
