package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.EmployeeAdditionalDetails;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.personlayer.EmployeeProjectHistory;
import com.slicktechnologies.shared.common.personlayer.PromotionDetails;

public class EmployeeBio_Data {
	Logger logger = Logger.getLogger("NameOfYourLogger");
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);

	float[] columnWidths = { 1f, 0.5f, 3f, 1f, 0.5f, 3f };
	float[] addressemployeewidths = { 1.5f, 0.5f, 3f, 1.5f, 0.5f, 3f };
	float[] contactemployeewidths = { 2f, 0.5f, 3f, 2f, 0.5f, 3f };
	float[] infoemployeewidths = { 1.55f, 0.15f, 3f, 1.55f, 0.15f, 3f };

	Employee employee;
	EmployeeAdditionalDetails empadditionaldetails;
	Company comp;
	List<ArticleType> articletype = new ArrayList<ArticleType>();
	
	int nooflines = 4;
	int workline = 2;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	public Document document;
	
	DecimalFormat df=new DecimalFormat("0");
	
	
	/**
	 * @author Anil , Date : 04-05-2019
	 * this flag checks whether to show ctc along with emloyee bio dat
	 */
	boolean showCtc=false;
	
	/**Date 11-8-2020 by Amol
	 * this flag is used to hide promotion history table
	 */
	boolean hidePromotionHistoryTable=false;

	public EmployeeBio_Data() {
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	
	
	public void getEmployeeDeatils(Long count) {
		employee = ofy().load().type(Employee.class).id(count).now();
		if (employee != null) {
			articletype = employee.getArticleTypeDetails();
		}
		if (employee.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", employee.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		// if (employee.getCompanyId() != null) {
		empadditionaldetails = ofy().load()
				.type(EmployeeAdditionalDetails.class)
				.filter("companyId", employee.getCompanyId())
				.filter("empInfo.count", employee.getCount()).first().now();
		// } else {
		// empadditionaldetails =
		// ofy().load().type(EmployeeAdditionalDetails.class).first().now();
		// }

		System.out.println("empadditionaldetails" + empadditionaldetails);
		System.out.println("empInfo.count, employee.getCount()"
				+ employee.getCount());
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "PrintCtcAlongWithEmployeeBiodata", employee.getCompanyId())){
			showCtc=true;
		}

		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Employee", "RemovePromotionHistoryFromBiodata", employee.getCompanyId())){
			hidePromotionHistoryTable=true;
		}
	}

	public void createPdf() {

		// if (comp.getUploadHeader() != null) {
		// createCompanyNameAsHeader(document, comp);
		// }

		Createletterhead();
		createHader();
		Createfamilyinfo();
		createAddress();
		createworkexperiencetab();
		createreferencetab();
		createdeclaration();
		createfootertab();
		if(!hidePromotionHistoryTable){
		createPromotionHistoryTable();
		}
		if(showCtc){
			showCtc();
		}
	}

	private void showCtc() {
		CTC ctc = ofy().load().type(CTC.class).filter("companyId",employee.getCompanyId()).filter("empid",employee.getCount()).filter("status","Active").first().now();
		if (ctc != null) {
			CtcPdf ctcpdf = new CtcPdf();
			ctcpdf.setCtc(ctc.getId());
			try {
				document.add(Chunk.NEXTPAGE);
				Paragraph blank = new Paragraph();
				blank.add(Chunk.NEWLINE);
				try {
					document.add(blank);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				document.add(ctcpdf.createCompanyAddress());
				document.add(ctcpdf.createEmployeeDetails());
				document.add(ctcpdf.creatCtcStructure());
			}catch (DocumentException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void Createletterhead() {
		/**** image ***/
		DocumentUpload letterheaddocument = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ letterheaddocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell();
			imageSignCell.setBorder(0);
			imageSignCell.setImage(image2);
			// imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Image image1 = null;
		try {
			image1 = Image.getInstance("images/Capture.JPG");
			image1.scalePercent(20f);
			// image1.setAbsolutePosition(40f,765f);
			// doc.add(image1);

			imageSignCell = new PdfPCell(image1);
			// imageSignCell.addElement();
			imageSignCell.setImage(image1);
			// imageSignCell.setFixedHeight(40);
			imageSignCell.setBorder(0);
			// imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(60);
		} catch (Exception e) {
			e.printStackTrace();
		}

		PdfPTable letterheadTab = new PdfPTable(1);
		letterheadTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			letterheadTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			letterheadTab.addCell(logoblankcell);
		}

		try {
			document.add(letterheadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createfootertab() {

		PdfPTable empsigntab = new PdfPTable(1);
		empsigntab.setWidthPercentage(100);
		try {
			empsigntab.setWidths(new float[] { 100 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase blankph = new Phrase(" ", font8);
		PdfPCell blankcell = new PdfPCell(blankph);
		blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankcell.setBorder(0);
		
		
		
		/****
		 * 
		 */
		
//		empsigntab.addCell(blankcell);
//		empsigntab.addCell(blankcell);
//		empsigntab.addCell(blankcell);
		
		DocumentUpload uploadSign = employee.getUploadSign();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell uplodEmpSignCell = null;
		Image image2 = null;
		try {
			System.out.println("TRY");
			image2 = Image.getInstance(new URL(hostUrl + uploadSign.getUrl()));
			image2.scalePercent(20f);

			uplodEmpSignCell = new PdfPCell(image2);
			uplodEmpSignCell.setBorder(0);
			uplodEmpSignCell.setPaddingTop(8);
			uplodEmpSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			uplodEmpSignCell.setFixedHeight(40);
			uplodEmpSignCell.setRowspan(3);
			empsigntab.addCell(uplodEmpSignCell);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("CATCH");
			
			empsigntab.addCell(blankcell);
			empsigntab.addCell(blankcell);
			empsigntab.addCell(blankcell);
			
//			Phrase blankPh=new Phrase(" ",font8);
//			uplodEmpSignCell = new PdfPCell();
//			uplodEmpSignCell.addElement(blankPh);
//			uplodEmpSignCell.setBorder(0);
//			uplodEmpSignCell.setRowspan(3);
		}
		
		
		
		/***
		 * 
		 */

		Phrase blanklineph = new Phrase("_______________________ ", font8);
		PdfPCell blanklinecell = new PdfPCell(blanklineph);
		blanklinecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blanklinecell.setBorder(0);
		empsigntab.addCell(blanklinecell);

		Phrase empsignph = new Phrase("Signature (Employee) ", font8);
		PdfPCell empsigncell = new PdfPCell(empsignph);
		empsigncell.setHorizontalAlignment(Element.ALIGN_CENTER);
		empsigncell.setBorder(0);
		empsigntab.addCell(empsigncell);

		// //************

		PdfPTable createdbytab = new PdfPTable(1);
		createdbytab.setWidthPercentage(100);
		try {
			createdbytab.setWidths(new float[] { 100 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		createdbytab.addCell(blankcell);
		createdbytab.addCell(blankcell);
//		createdbytab.addCell(blankcell);
		
		String createdBy="";
		
		if(employee.getCreatedBy()!=null){
			createdBy=employee.getCreatedBy();
			/**
			 * Date : 13-12-2018 By ANIL
			 * @author Anil
			 * @since 15-01-2021
			 * Print only created by Name
			 * Raised by Rahul Tiwari For SASHA
			 */
//			if(employee.getUpdatedBy()!=null&&!employee.getUpdatedBy().equals("")){
//				createdBy=employee.getUpdatedBy();
//			}
			/**
			 * End
			 */
		}
		Phrase createdByPh = new Phrase(createdBy, font8);
		PdfPCell createdByCell = new PdfPCell(createdByPh);
		createdByCell.setBorder(0);
		createdByCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		createdbytab.addCell(createdByCell);

		createdbytab.addCell(blanklinecell);

		Phrase createdsignph = new Phrase("(Created By Name Signature & Date )", font8);
		PdfPCell createdsigncell = new PdfPCell(createdsignph);
		createdsigncell.setBorder(0);
		createdsigncell.setHorizontalAlignment(Element.ALIGN_CENTER);
		createdbytab.addCell(createdsigncell);

		PdfPTable approvaltab = new PdfPTable(1);
		approvaltab.setWidthPercentage(100);
		try {
			createdbytab.setWidths(new float[] { 100 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		approvaltab.addCell(blankcell);
		approvaltab.addCell(blankcell);
//		approvaltab.addCell(blankcell);
		

		String approvedBy="";
		if(employee.getApproverName()!=null){
			approvedBy=employee.getApproverName();
		}
		Phrase approvedByPh = new Phrase(approvedBy, font8);
		PdfPCell approvedByCell = new PdfPCell(approvedByPh);
		approvedByCell.setBorder(0);
		approvedByCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		approvaltab.addCell(approvedByCell);

		approvaltab.addCell(blanklinecell);

		Phrase approvalsignph = new Phrase("Approval Signature (Name Signature & Date)", font8);
		PdfPCell approvalsigncell = new PdfPCell(approvalsignph);
		approvalsigncell.setHorizontalAlignment(Element.ALIGN_CENTER);
		approvalsigncell.setBorder(0);
		approvaltab.addCell(approvalsigncell);

		PdfPTable footertab = new PdfPTable(3);
		footertab.setWidthPercentage(100);
		try {
			footertab.setWidths(new float[] { 33, 33, 33 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell leftcell = new PdfPCell(empsigntab);
		leftcell.setPaddingBottom(5);
		PdfPCell middleCell = new PdfPCell(createdbytab);
		middleCell.setPaddingBottom(5);
		PdfPCell rightCell = new PdfPCell(approvaltab);
		rightCell.setPaddingBottom(5);

		footertab.addCell(leftcell);
		footertab.addCell(middleCell);
		footertab.addCell(rightCell);

		try {
			document.add(footertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createdeclaration() {

		PdfPTable declarationtab = new PdfPTable(1);
		declarationtab.setWidthPercentage(100);
		try {
			declarationtab.setWidths(new float[] { 100 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String age="";
		if(employee.getNomineeAge()!=0){
			age=employee.getNomineeAge()+"";
		}

		String nomineeInfo="      I Affirm that ,i nominate Mr/Mrs. "+employee.getNomineeName()+" ,Aged:"+ age+" Years ,who is related to me as "+employee.getNomineeRelation()+" is residing at "+employee.getNomineeAddress()+" is declared as my nominee for E.S.I.,P.F.,gratuity and /or other monetory compensation of employment in case of my illness/emergency need /accidental injury and /or death.  ";
		
//		Phrase declarationph = new Phrase(
//				"      I Affirm that ,i nominate Mr/Mrs._______________________,Aged: ______ Years ,whois related to me as _______________ is residing at __________________________ is declared as my nominee for E.S.I.,P.F.,gratuity and /or other monetory compensation of employment in case of my illness/emergency need /accidental injury and /or death.  ",
//				font8bold);
		Phrase declarationph = new Phrase(nomineeInfo,font8bold);
		PdfPCell declarationcell = new PdfPCell();
		declarationcell.addElement(declarationph);
		declarationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// declarationcell.setFixedHeight(30);
		declarationtab.addCell(declarationcell);

		Phrase declaration2ph = new Phrase(
				"I hereby declare that the above particulars are correct to the best of my knowledge and belief.I also undertake to intimate any changes (if any ) within 15 days of such changes having occurred.",
				font8bold);
		PdfPCell declaration2cell = new PdfPCell();
		declaration2cell.addElement(declaration2ph);
		declaration2cell.setBorderWidthBottom(0);
		declaration2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		declarationtab.addCell(declaration2cell);

		Phrase declaration3ph = new Phrase(
				"Aadhar Card Xerox, cancelled cheque, Xerox copy of first pageof S.B.A/c pass book & Xerox copy of Driving licence is enclosed. ",font8bold);
		PdfPCell declaration3cell = new PdfPCell(declaration3ph);
		// declaration3cell.addElement(declaration3ph);
		declaration3cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		declaration3cell.setBorderWidthTop(0);
		declaration3cell.setPaddingBottom(5);
		declaration3cell.setVerticalAlignment(Element.ALIGN_CENTER);
//		declarationtab.addCell(declaration3cell);

		try {
			document.add(declarationtab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createreferencetab() {

		PdfPTable referencetab = new PdfPTable(4);
		referencetab.setWidthPercentage(100);
		try {
			referencetab.setWidths(new float[] { 40, 10, 25, 25 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase workexpph = new Phrase("REFERENCE", font8bold);
		PdfPCell workexpcell = new PdfPCell(workexpph);
		workexpcell.setColspan(4);
		workexpcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		workexpcell.setFixedHeight(17);
		referencetab.addCell(workexpcell);

		Phrase nameph = new Phrase("Name", font8bold);
		PdfPCell namecell = new PdfPCell(nameph);
		namecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		namecell.setFixedHeight(17);
		referencetab.addCell(namecell);

		Phrase idph = new Phrase("ID.NO.", font8bold);
		PdfPCell idcell = new PdfPCell(idph);
		idcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		idcell.setFixedHeight(17);
		referencetab.addCell(idcell);

		Phrase designationph = new Phrase("Designation", font8bold);
		PdfPCell designationcell = new PdfPCell(designationph);
		designationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		designationcell.setFixedHeight(17);
		referencetab.addCell(designationcell);

		Phrase Remarkph = new Phrase("Unit/Remarks", font8bold);
		PdfPCell Remarkcell = new PdfPCell(Remarkph);
		Remarkcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		Remarkcell.setFixedHeight(17);
		referencetab.addCell(Remarkcell);
		Employee refEmployee = null;
		if (employee.getRefEmployee() != null
				&& !employee.getRefEmployee().equals("")) {
			refEmployee = ofy().load().type(Employee.class)
					.filter("companyId", comp.getCompanyId())
					.filter("fullname", employee.getRefEmployee()).first()
					.now();
		}
		Phrase namevalph = null;
		if (refEmployee != null)
			namevalph = new Phrase(refEmployee.getFullname(), font8);
		else
			namevalph = new Phrase(" ", font8);

		PdfPCell namevalcell = new PdfPCell(namevalph);
		namevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		namevalcell.setFixedHeight(17);
		referencetab.addCell(namevalcell);

		Phrase idvalph = null;
		if (refEmployee != null)
			idvalph = new Phrase(refEmployee.getCount() + "", font8);
		else
			idvalph = new Phrase(" ", font8);

		PdfPCell idvalcell = new PdfPCell(idvalph);
		idvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		idvalcell.setFixedHeight(17);
		referencetab.addCell(idvalcell);

		Phrase designationvalph = null;
		if (refEmployee != null)
			designationvalph = new Phrase(refEmployee.getDesignation() + "",
					font8);
		else
			designationvalph = new Phrase(" ", font8);

		PdfPCell designationvalcell = new PdfPCell(designationvalph);
		designationvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		designationvalcell.setFixedHeight(17);
		referencetab.addCell(designationvalcell);

		Phrase Remarkvalph = new Phrase(" ", font8);
		PdfPCell Remarkvalcell = new PdfPCell(Remarkvalph);
		Remarkvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		Remarkvalcell.setFixedHeight(17);
		referencetab.addCell(Remarkvalcell);

		try {
			document.add(referencetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable sitetab = new PdfPTable(7);
		sitetab.setWidthPercentage(100);
		try {
			sitetab.setWidths(new float[] { 10, 15, 15, 15, 15, 15, 15 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase sitecodeph = new Phrase("Site Code", font8bold);
		PdfPCell sitecodecell = new PdfPCell(sitecodeph);
		sitecodecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sitecodecell.setFixedHeight(17);
		sitetab.addCell(sitecodecell);

		Phrase sitenameph = new Phrase("Site Name", font8bold);
		PdfPCell sitenamecell = new PdfPCell(sitenameph);
		sitenamecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sitenamecell.setFixedHeight(17);
		sitetab.addCell(sitenamecell);

		Phrase fstdayph = new Phrase("1st Day Training", font8bold);
		PdfPCell fstdaycell = new PdfPCell(fstdayph);
		fstdaycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		fstdaycell.setFixedHeight(17);
		sitetab.addCell(fstdaycell);

		Phrase secdayph = new Phrase("2st Day Training", font8bold);
		PdfPCell secdaycell = new PdfPCell(secdayph);
		secdaycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		secdaycell.setFixedHeight(17);
		sitetab.addCell(secdaycell);

		Phrase grosssalaryph = new Phrase("Gross Salary", font8bold);
		PdfPCell grosssalarycell = new PdfPCell(grosssalaryph);
		grosssalarycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		grosssalarycell.setFixedHeight(17);
		sitetab.addCell(grosssalarycell);

		Phrase interviewbyph = new Phrase("Interviewed By", font8bold);
		PdfPCell interviewbycell = new PdfPCell(interviewbyph);
		interviewbycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		interviewbycell.setFixedHeight(17);
		sitetab.addCell(interviewbycell);

		Phrase salarybyph = new Phrase("Salary Authorised By ", font8bold);
		PdfPCell salarybycell = new PdfPCell(salarybyph);
		salarybycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		salarybycell.setFixedHeight(17);
		sitetab.addCell(salarybycell);

		/**
		 * Date : 14-08-2018 By Anil
		 */
		String siteCode="";
		String siteName="";
		
		String firstDayTraining="";
		String secondDayTraining="";
		
		if(employee.getEmployeeProjectHistoryList()!=null&&employee.getEmployeeProjectHistoryList().size()!=0){
//			EmployeeProjectHistory empProjHis=employee.getEmployeeProjectHistoryList().get(employee.getEmployeeProjectHistoryList().size()-1);
			EmployeeProjectHistory empProjHis=employee.getEmployeeProjectHistoryList().get(0);
			if(empProjHis.getProjectCode()!=null){
				siteCode=empProjHis.getProjectCode();
			}
			if(empProjHis.getProjectName()!=null){
				siteName=empProjHis.getProjectName();
			}
		}
		
		if(employee.getTrainingInfoList()!=null){
			if(employee.getTrainingInfoList().size()!=0){
				if(employee.getTrainingInfoList().size()==1){
					firstDayTraining=fmt.format(employee.getTrainingInfoList().get(0).getTrainingDate());
				}else if(employee.getTrainingInfoList().size()>=2){
					firstDayTraining=fmt.format(employee.getTrainingInfoList().get(0).getTrainingDate());
					secondDayTraining=fmt.format(employee.getTrainingInfoList().get(1).getTrainingDate());
				}
			}
		}
		/**
		 * End
		 */
		
		Phrase sitecodevalph = new Phrase(siteCode, font8);
		PdfPCell sitecodevalcell = new PdfPCell(sitecodevalph);
		sitecodevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sitecodevalcell.setFixedHeight(17);
		sitetab.addCell(sitecodevalcell);

		Phrase sitenamevalph = new Phrase(siteName, font8);
		PdfPCell sitenamevalcell = new PdfPCell(sitenamevalph);
		sitenamevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sitenamevalcell.setFixedHeight(17);
		sitetab.addCell(sitenamevalcell);

		Phrase fstdayvalph = new Phrase(firstDayTraining, font8);
		PdfPCell fstdayvalcell = new PdfPCell(fstdayvalph);
		fstdayvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		fstdayvalcell.setFixedHeight(17);
		sitetab.addCell(fstdayvalcell);

		Phrase secdayvalph = new Phrase(secondDayTraining, font8);
		PdfPCell secdayvalcell = new PdfPCell(secdayvalph);
		secdayvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		secdayvalcell.setFixedHeight(17);
		sitetab.addCell(secdayvalcell);
		
		Phrase grosssalaryvalph =null;
		if(employee.getSalaryAmt()!=0){
			grosssalaryvalph = new Phrase(df.format(employee.getSalaryAmt()) +"", font8);
		}else{
			grosssalaryvalph = new Phrase(" ", font8);
		}
//		Phrase grosssalaryvalph = new Phrase(" ", font8);
		
		PdfPCell grosssalaryvalcell = new PdfPCell(grosssalaryvalph);
		grosssalaryvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		grosssalaryvalcell.setFixedHeight(17);
		sitetab.addCell(grosssalaryvalcell);

		String interviewedBy="";
		String salAuthBy="";
		if(employee.getInterviewedBy()!=null){
			interviewedBy=employee.getInterviewedBy();
		}
		if(employee.getSalaryAuthorizedBy()!=null){
			salAuthBy=employee.getSalaryAuthorizedBy();
		}
				
		Phrase interviewbyvalph = new Phrase(interviewedBy, font8);
		PdfPCell interviewbyvalcell = new PdfPCell(interviewbyvalph);
		interviewbyvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		interviewbyvalcell.setFixedHeight(17);
		sitetab.addCell(interviewbyvalcell);

		Phrase salarybyvalph = new Phrase(salAuthBy, font8);
		PdfPCell salarybyvalcell = new PdfPCell(salarybyvalph);
		salarybyvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		salarybyvalcell.setFixedHeight(17);
		sitetab.addCell(salarybyvalcell);

		try {
			document.add(sitetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**
		 * Date : 18-09-2018 By ANIL
		 * Project History ,last 4 sites
		 */
		
		PdfPTable projHisTbl=new PdfPTable(4);
		projHisTbl.setWidthPercentage(100);
		
		Phrase titlePh=new Phrase("Project History",font8bold);
		PdfPCell titleCell=new PdfPCell(titlePh);
//		titleCell.addElement(titlePh);
		titleCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell.setColspan(4);
		titleCell.setFixedHeight(17);
		projHisTbl.addCell(titleCell);
		
		Phrase titlePh1=new Phrase("Site 1",font8bold);
		PdfPCell titleCell1=new PdfPCell(titlePh1);
		titleCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell1.setFixedHeight(17);
		projHisTbl.addCell(titleCell1);
		
		Phrase titlePh2=new Phrase("Site 2",font8bold);
		PdfPCell titleCell2=new PdfPCell(titlePh2);
		titleCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell2.setFixedHeight(17);
		projHisTbl.addCell(titleCell2);
		
		Phrase titlePh3=new Phrase("Site 3",font8bold);
		PdfPCell titleCell3=new PdfPCell(titlePh3);
		titleCell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell3.setFixedHeight(17);
		projHisTbl.addCell(titleCell3);
		
		Phrase titlePh4=new Phrase("Site 4",font8bold);
		PdfPCell titleCell4=new PdfPCell(titlePh4);
		titleCell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell4.setFixedHeight(17);
		projHisTbl.addCell(titleCell4);
		
		Phrase titlePh1vl=new Phrase(getSiteName(0),font8);
		PdfPCell titleCell1vl=new PdfPCell(titlePh1vl);
		titleCell1vl.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell1vl.setFixedHeight(17);
		projHisTbl.addCell(titleCell1vl);
		
		Phrase titlePh2vl=new Phrase(getSiteName(1),font8);
		PdfPCell titleCell2vl=new PdfPCell(titlePh2vl);
		titleCell2vl.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell2vl.setFixedHeight(17);
		projHisTbl.addCell(titleCell2vl);
		
		Phrase titlePh3vl=new Phrase(getSiteName(2),font8);
		PdfPCell titleCell3vl=new PdfPCell(titlePh3vl);
		titleCell3vl.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell3vl.setFixedHeight(17);
		projHisTbl.addCell(titleCell3vl);
		
		Phrase titlePh4vl=new Phrase(getSiteName(3),font8);
		PdfPCell titleCell4vl=new PdfPCell(titlePh4vl);
		titleCell4vl.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleCell4vl.setFixedHeight(17);
		projHisTbl.addCell(titleCell4vl);
		
		
		
		try {
			document.add(projHisTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createPromotionHistoryTable(){
		/**
		 * Date : 20-10-2018 By ANIL
		 * Promotion History
		 */
		
		PdfPTable promotionHisTbl=new PdfPTable(6);
		promotionHisTbl.setWidthPercentage(100);
		promotionHisTbl.addCell(getCell("Promotion History", font8bold, Element.ALIGN_CENTER, 1, 0, 6, 17));
		
		promotionHisTbl.addCell(getCell("Previous Designation", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		promotionHisTbl.addCell(getCell("Previous Salary", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		promotionHisTbl.addCell(getCell("Current Designation", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		promotionHisTbl.addCell(getCell("Current Salary", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		promotionHisTbl.addCell(getCell("Date of Promotion", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		promotionHisTbl.addCell(getCell("Promoted By", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		
		if(employee.getPromotionHistoryList()!=null&&employee.getPromotionHistoryList().size()!=0){
			int counter=0;
			while(counter<2){
				PromotionDetails promotionDetails=getPromotionDetails(counter);
				if(promotionDetails!=null){
					promotionHisTbl.addCell(getCell(promotionDetails.getPreviousDesignation(), font8, Element.ALIGN_CENTER, 1, 0, 0, 17));
					promotionHisTbl.addCell(getCell(promotionDetails.getPreviousSalary()+"", font8, Element.ALIGN_CENTER, 1, 0, 0, 17));
					promotionHisTbl.addCell(getCell(promotionDetails.getCurrentDesignation(), font8, Element.ALIGN_CENTER, 1, 0, 0, 17));
					promotionHisTbl.addCell(getCell(promotionDetails.getCurrentSalary()+"", font8, Element.ALIGN_CENTER, 1, 0, 0, 17));
					promotionHisTbl.addCell(getCell(fmt.format(promotionDetails.getDateOfPromotion()), font8, Element.ALIGN_CENTER, 1, 0, 0, 17));
					promotionHisTbl.addCell(getCell(promotionDetails.getPromotedBy(), font8, Element.ALIGN_CENTER, 1, 0, 0, 17));
				}
				counter++;
			}
			
		}else{
			promotionHisTbl.addCell(getCell(" ", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
			promotionHisTbl.addCell(getCell(" ", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
			promotionHisTbl.addCell(getCell(" ", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
			promotionHisTbl.addCell(getCell(" ", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
			promotionHisTbl.addCell(getCell(" ", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
			promotionHisTbl.addCell(getCell(" ", font8bold, Element.ALIGN_CENTER, 1, 0, 0, 17));
		}
		
		try {
			document.add(Chunk.NEXTPAGE);
			document.add(promotionHisTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**
		 * End
		 */
	}
	
	/**
	 * Date : 20-10-2018 BY ANIL
	 * @param phName-Cell Value
	 * @param font-font size
	 * @param horizontalAlignment
	 * @param border-default-1,for no border-0
	 * @param rowspan-default-1
	 * @param colspan-default-1
	 * @param fixedHeight-default-0
	 * @return
	 */
	
	public PdfPCell getCell(String phName,Font font,int horizontalAlignment,int border,int rowspan,int colspan,int fixedHeight){
//		System.out.println(".");
		Phrase phrase=new Phrase(phName,font);
		PdfPCell cell=new PdfPCell(phrase);
		cell.setHorizontalAlignment(horizontalAlignment);
		if(fixedHeight!=0){
			cell.setFixedHeight(fixedHeight);
		}
		if(rowspan!=0){
			cell.setRowspan(rowspan);
		}if(colspan!=0){
			cell.setColspan(colspan);
		}
//		cell.setBorder(border);
		
		return cell;
	}
	
	private PromotionDetails getPromotionDetails(int i) {
		PromotionDetails promotionDetails=null;
		
		if(employee.getPromotionHistoryList()!=null
				&&employee.getPromotionHistoryList().size()!=0){
			int constant=2;
			int actualSize=employee.getPromotionHistoryList().size();
			int elementNo=actualSize-constant;
			if(elementNo<0){
				elementNo=0;
			}
			int index=elementNo+i;
			if(index<actualSize){
				promotionDetails=employee.getPromotionHistoryList().get(index);
			}
		}
		return promotionDetails;
	}
	

	private String getSiteName(int i) {
		String siteName="";
		
		if(employee.getEmployeeProjectHistoryList()!=null
				&&employee.getEmployeeProjectHistoryList().size()!=0){
			int constant=4;
			int actualSize=employee.getEmployeeProjectHistoryList().size();
			int elementNo=actualSize-constant;
			if(elementNo<0){
				elementNo=0;
			}
			int index=elementNo+i;
			if(index<actualSize){
				siteName=employee.getEmployeeProjectHistoryList().get(index).getProjectName();
			}
		}
		return siteName;
	}

	private void createworkexperiencetab() {

		PdfPTable workexperiencetab = new PdfPTable(5);
		workexperiencetab.setWidthPercentage(100);
		try {
			workexperiencetab.setWidths(new float[] { 25, 15, 20, 20, 20 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase workexpph = new Phrase("WORK EXPERIENCE", font8bold);
		PdfPCell workexpcell = new PdfPCell(workexpph);
		workexpcell.setColspan(5);
		workexpcell.setFixedHeight(17);
		workexpcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		workexperiencetab.addCell(workexpcell);

		/**
		 * Date : 14-08-2018 By ANIL
		 */
		Phrase oldempph = new Phrase("Previous Employer Details", font8bold);
//		Phrase oldempph = new Phrase("Name Of the Old Employer", font8bold);
		PdfPCell oldempcell = new PdfPCell(oldempph);
		oldempcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldempcell.setFixedHeight(17);
		workexperiencetab.addCell(oldempcell);

		Phrase periodofworkph = new Phrase("Period Of Work", font8bold);
		PdfPCell periodofworkcell = new PdfPCell(periodofworkph);
		periodofworkcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		periodofworkcell.setFixedHeight(17);
		workexperiencetab.addCell(periodofworkcell);

		Phrase dateofleavph = new Phrase("Date Of Leaving", font8bold);
		PdfPCell dateofleavcell = new PdfPCell(dateofleavph);
		dateofleavcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dateofleavcell.setFixedHeight(17);
		workexperiencetab.addCell(dateofleavcell);

		Phrase salarydrawnph = new Phrase("Salary Drawn", font8bold);
		PdfPCell salaryDrawncell = new PdfPCell(salarydrawnph);
		salaryDrawncell.setHorizontalAlignment(Element.ALIGN_CENTER);
		salaryDrawncell.setFixedHeight(17);
		workexperiencetab.addCell(salaryDrawncell);

		Phrase reasonofleavph = new Phrase("Reason Of Leaving", font8bold);
		PdfPCell reasonofleavcell = new PdfPCell(reasonofleavph);
		reasonofleavcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		reasonofleavcell.setFixedHeight(17);
		workexperiencetab.addCell(reasonofleavcell);

		if (employee.getPreviousCompanyHistory().size() != 0) {
			for (int i = 0; i < employee.getPreviousCompanyHistory().size(); i++) {
				workline = workline - 1;
				Phrase oldempvalph = new Phrase(employee
						.getPreviousCompanyHistory().get(i).getCompany(), font8);
				PdfPCell oldempvalcell = new PdfPCell(oldempvalph);
				oldempvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				oldempvalcell.setFixedHeight(17);
				workexperiencetab.addCell(oldempvalcell);

				/**
				 * Date : 13-08-2018 BY ANIL
				 * in period of work ,both from date and todate was coming
				 * it should be from date only
				 */
//				Phrase periodofworkvalph = new Phrase(employee.getPreviousCompanyHistory().get(i).getFromDate()
//						+ "-"
//						+ employee.getPreviousCompanyHistory().get(i).getToDate(), font8);
				Phrase periodofworkvalph = new Phrase(employee.getPreviousCompanyHistory().get(i).getFromDate(), font8);
				/**
				 * End
				 */
				PdfPCell periodofworkvalcell = new PdfPCell(periodofworkvalph);
				periodofworkvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				periodofworkvalcell.setFixedHeight(17);
				workexperiencetab.addCell(periodofworkvalcell);

				Phrase dateofleavvalph = new Phrase(employee
						.getPreviousCompanyHistory().get(i).getToDate(), font8);
				PdfPCell dateofleavvalcell = new PdfPCell(dateofleavvalph);
				dateofleavvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				dateofleavvalcell.setFixedHeight(17);
				workexperiencetab.addCell(dateofleavvalcell);

				Phrase salarydrawnvalph = new Phrase(df.format(employee.getPreviousCompanyHistory().get(i).getSalary())
						+ "", font8);
				PdfPCell salaryDrawnvalcell = new PdfPCell(salarydrawnvalph);
				salaryDrawnvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				salaryDrawnvalcell.setFixedHeight(17);
				workexperiencetab.addCell(salaryDrawnvalcell);

				String reasonOfLeaving="";
				if(employee.getPreviousCompanyHistory().get(i).getReasonOfLeaving()!=null){
					reasonOfLeaving=employee.getPreviousCompanyHistory().get(i).getReasonOfLeaving();
				}else{
					reasonOfLeaving="";
				}
				
				
				Phrase reasonofleavvalph = new Phrase(reasonOfLeaving, font8);
				PdfPCell reasonofleavvalcell = new PdfPCell(reasonofleavvalph);
				reasonofleavvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				reasonofleavvalcell.setPaddingLeft(8);
				reasonofleavvalcell.setPaddingRight(8);
//				reasonofleavvalcell.setFixedHeight(17);
				workexperiencetab.addCell(reasonofleavvalcell);
			}

			int remainingLines = 0;
			if (workline != 0) {
				remainingLines = 2 - (2 - workline);
			}
			System.out.println("remainingLines" + remainingLines);
			for (int j = 0; j < remainingLines; j++) {
				PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
				pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				pCell.setFixedHeight(17);
				// pCell.setBorder(0);
				// pCell.setColspan(3);
				// pCell.setBorderWidthBottom(0);
				workexperiencetab.addCell(pCell);
				workexperiencetab.addCell(pCell);
				workexperiencetab.addCell(pCell);
				workexperiencetab.addCell(pCell);
				workexperiencetab.addCell(pCell);

			}
		} else {
			Phrase blank = new Phrase(" ", font8);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setFixedHeight(17);
			workexperiencetab.addCell(blankCell);
			workexperiencetab.addCell(blankCell);
			workexperiencetab.addCell(blankCell);
			workexperiencetab.addCell(blankCell);
			workexperiencetab.addCell(blankCell);
		}
		try {
			document.add(workexperiencetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable workexperiencetab2 = new PdfPTable(6);
		workexperiencetab2.setWidthPercentage(100);
		try {
			workexperiencetab2
					.setWidths(new float[] { 10, 15, 15, 20, 20, 20 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase postph = new Phrase("Post", font8bold);
		PdfPCell postcell = new PdfPCell(postph);
		postcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		postcell.setFixedHeight(17);
		workexperiencetab2.addCell(postcell);

		Phrase pancardph = new Phrase("Pan Card No", font8bold);
		PdfPCell pancardcell = new PdfPCell(pancardph);
		pancardcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		pancardcell.setFixedHeight(17);
		workexperiencetab2.addCell(pancardcell);

		Phrase adharcardph = new Phrase("Aadhar Card No.", font8bold);
		PdfPCell adharcardcell = new PdfPCell(adharcardph);
		adharcardcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		adharcardcell.setFixedHeight(17);
		workexperiencetab2.addCell(adharcardcell);

		/**
		 * Date : 14-08-2018 By ANIL
		 */
		Phrase oldESICph = new Phrase("ESIC No", font8bold);
//		Phrase oldESICph = new Phrase("Old ESIC No", font8bold);
		PdfPCell oldESICcell = new PdfPCell(oldESICph);
		oldESICcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldESICcell.setFixedHeight(17);
		workexperiencetab2.addCell(oldESICcell);

		Phrase oldPFnoph = new Phrase("PF A/c No", font8bold);
//		Phrase oldPFnoph = new Phrase("Old PF A/c No", font8bold);
		PdfPCell oldPFnocell = new PdfPCell(oldPFnoph);
		oldPFnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldPFnocell.setFixedHeight(17);
		workexperiencetab2.addCell(oldPFnocell);

		Phrase oldfamilyacnoph = new Phrase("Family Pension A/C No",font8bold);
//		Phrase oldfamilyacnoph = new Phrase("Old Family Pension A/C No",font8bold);
		PdfPCell oldfamilyacnocell = new PdfPCell(oldfamilyacnoph);
		oldfamilyacnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldfamilyacnocell.setFixedHeight(17);
		workexperiencetab2.addCell(oldfamilyacnocell);

		Phrase postvalph = new Phrase(" ", font8);
		PdfPCell postvalcell = new PdfPCell(postvalph);
		postvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		postvalcell.setFixedHeight(17);
		workexperiencetab2.addCell(postvalcell);

		Phrase pancardvalph = new Phrase(employee.getEmployeePanNo(), font8);
		PdfPCell pancardvalcell = new PdfPCell(pancardvalph);
		pancardvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		pancardvalcell.setFixedHeight(17);
		workexperiencetab2.addCell(pancardvalcell);

		/**
		 * Date : 27-09-2018 BY ANIL
		 * Aadhar number is picked from article type ,now we are storing it separately
		 */
		String adharno = "";
//		for (int i = 0; i < articletype.size(); i++) {
//			if (articletype.get(i).getArticleTypeName().contains("AADHAR")) {
//				adharno = articletype.get(i).getArticleTypeValue();
//			}
//		}
		
		if(employee.getAadharNumber()!=0){
			adharno=employee.getAadharNumber()+"";
		}
		/**
		 * End
		 */
		

		Phrase adharcardvalph = new Phrase(adharno, font8);
		PdfPCell adharcardvalcell = new PdfPCell(adharcardvalph);
		adharcardvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		adharcardvalcell.setFixedHeight(17);
		workexperiencetab2.addCell(adharcardvalcell);

		String oldESIC = "";
//		for (int i = 0; i < articletype.size(); i++) {
//			if (articletype.get(i).getArticleTypeName().contains("old ESIC")) {
//				oldESIC = articletype.get(i).getArticleTypeValue();
//			}
//		}
		if(employee.getEmployeeESICcode()!=null){
			oldESIC=employee.getEmployeeESICcode();
		}

		Phrase oldESICvalph = new Phrase(oldESIC, font8);
		PdfPCell oldESICvalcell = new PdfPCell(oldESICvalph);
		oldESICvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldESICvalcell.setFixedHeight(17);
		workexperiencetab2.addCell(oldESICvalcell);

		String oldPFno = "";
//		for (int i = 0; i < articletype.size(); i++) {
//			if (articletype.get(i).getArticleTypeName().contains("old PF")) {
//				oldPFno = articletype.get(i).getArticleTypeValue();
//			}
//		}
		
		if(employee.getPPFNaumber()!=null){
			oldPFno=employee.getPPFNaumber();
		}

		Phrase oldPFnovalph = new Phrase(oldPFno, font8);
		PdfPCell oldPFnovalcell = new PdfPCell(oldPFnovalph);
		oldPFnovalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldPFnovalcell.setFixedHeight(17);
		workexperiencetab2.addCell(oldPFnovalcell);

		String oldfamilyacno = "";
		for (int i = 0; i < articletype.size(); i++) {

			if (articletype.get(i).getArticleTypeName().contains("Family Pension A/C No.")) {
//			if (articletype.get(i).getArticleTypeName().contains("old Family Pension")) {
				oldfamilyacno = articletype.get(i).getArticleTypeValue();
			}
		}

		Phrase oldfamilyacnovalph = new Phrase(oldfamilyacno, font8);
		PdfPCell oldfamilyacnovalcell = new PdfPCell(oldfamilyacnovalph);
		oldfamilyacnovalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		oldfamilyacnovalcell.setFixedHeight(17);
		workexperiencetab2.addCell(oldfamilyacnovalcell);

		try {
			document.add(workexperiencetab2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createAddress() {
		PdfPTable addresstab = new PdfPTable(2);
		addresstab.setWidthPercentage(100);
		try {
			addresstab.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase presentaddtitleph = new Phrase("Local Present Address",font8bold);
		PdfPCell presentaddtitlecell = new PdfPCell(presentaddtitleph);
		presentaddtitlecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		presentaddtitlecell.setFixedHeight(17);
		addresstab.addCell(presentaddtitlecell);

		Phrase nativeplaceph = new Phrase("Native Place Permanent Address",font8bold);
		PdfPCell nativeplacecell = new PdfPCell(nativeplaceph);
		nativeplacecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		nativeplacecell.setFixedHeight(17);
		addresstab.addCell(nativeplacecell);

		Phrase presentaddtitlevalph = new Phrase(employee.getAddress().getCompleteAddress(), font8);
		PdfPCell presentaddtitlevalcell = new PdfPCell(presentaddtitlevalph);
		presentaddtitlevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		presentaddtitlevalcell.setFixedHeight(34);
		addresstab.addCell(presentaddtitlevalcell);

		/**
		 * Date : 18-09-2018 By ANIL
		 */
		Phrase nativeaddvalph = null;
		if(employee.getSecondaryAddress()!=null){
			nativeaddvalph = new Phrase(employee.getSecondaryAddress().getCompleteAddress(), font8);
		}else{
			nativeaddvalph = new Phrase(" ", font8);
		}
//		Phrase nativeaddvalph = new Phrase(employee.getAddress().getCompleteAddress(), font8);
		PdfPCell nativeaddvalcell = new PdfPCell(nativeaddvalph);
		nativeaddvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nativeaddvalcell.setFixedHeight(34);
		addresstab.addCell(nativeaddvalcell);
		/**
		 * End
		 */
		
		try {
			document.add(addresstab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void Createfamilyinfo() {

		PdfPTable familytitle = new PdfPTable(5);
		familytitle.setWidthPercentage(100);
		try {
			familytitle.setWidths(new float[] { 15, 10, 10, 5, 60 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase familydetailsph = new Phrase("Family Details", font8bold);
		PdfPCell familydetailscell = new PdfPCell(familydetailsph);
		familydetailscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		familydetailscell.setColspan(5);
		familydetailscell.setFixedHeight(17);
		familytitle.addCell(familydetailscell);

		Phrase nameph = new Phrase("Name", font8bold);
		PdfPCell namecell = new PdfPCell(nameph);
		namecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		namecell.setFixedHeight(17);
		familytitle.addCell(namecell);

		Phrase relationph = new Phrase("Relationship", font8bold);
		PdfPCell relationcell = new PdfPCell(relationph);
		relationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		relationcell.setFixedHeight(17);
		familytitle.addCell(relationcell);

		Phrase birthph = new Phrase("Birth Date", font8bold);
		PdfPCell birthcell = new PdfPCell(birthph);
		birthcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		birthcell.setFixedHeight(17);
		familytitle.addCell(birthcell);

		Phrase ageph = new Phrase("Age", font8bold);
		PdfPCell agecell = new PdfPCell(ageph);
		agecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		agecell.setFixedHeight(17);
		familytitle.addCell(agecell);

		Phrase addressph = new Phrase("Address", font8bold);
		PdfPCell adresscell = new PdfPCell(addressph);
		adresscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		adresscell.setFixedHeight(17);
		familytitle.addCell(adresscell);

		try {
			document.add(familytitle);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable familydetailtab = new PdfPTable(5);
		familydetailtab.setWidthPercentage(100);
		try {
			familydetailtab.setWidths(new float[] { 15, 10, 10, 5, 60 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// System.out.println("empadditionaldetails.getEmpFamDetList().size()"+empadditionaldetails.getEmpFamDetList().size());
		if (empadditionaldetails != null) {
			if (empadditionaldetails.getEmpFamDetList().size() != 0) {
				for (int i = 0; i < empadditionaldetails.getEmpFamDetList()
						.size(); i++) {

					nooflines = nooflines - 1;
					Phrase namephval = new Phrase(empadditionaldetails
							.getEmpFamDetList().get(i).getFname(), font8);
					PdfPCell namevalcell = new PdfPCell(namephval);
					namevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					namevalcell.setFixedHeight(17);
					familydetailtab.addCell(namevalcell);

					Phrase relationphval = new Phrase(empadditionaldetails
							.getEmpFamDetList().get(i).getFamilyRelation(),
							font8);
					PdfPCell relationcellval = new PdfPCell(relationphval);
					relationcellval
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					relationcellval.setFixedHeight(17);
					familydetailtab.addCell(relationcellval);

					Phrase birthphval = new Phrase(
							fmt.format(empadditionaldetails.getEmpFamDetList()
									.get(i).getDob()), font8);
					PdfPCell birthcellval = new PdfPCell(birthphval);
					birthcellval.setHorizontalAlignment(Element.ALIGN_CENTER);
					birthcellval.setFixedHeight(17);
					familydetailtab.addCell(birthcellval);

					Date birthdate = null;
					birthdate = empadditionaldetails.getEmpFamDetList().get(i)
							.getDob();
					/** Calculate Age ***/

					Calendar birth = Calendar.getInstance();
					birth.setTime(birthdate);
					Calendar today = Calendar.getInstance();

					int yearDifference = today.get(Calendar.YEAR)
							- birth.get(Calendar.YEAR);

					if (today.get(Calendar.MONTH) < birth.get(Calendar.MONTH)) {
						yearDifference--;
					} else {
						if (today.get(Calendar.MONTH) == birth
								.get(Calendar.MONTH)
								&& today.get(Calendar.DAY_OF_MONTH) < birth
										.get(Calendar.DAY_OF_MONTH)) {
							yearDifference--;
						}

					}

					System.out.println("age" + yearDifference);

					Phrase agephval = new Phrase(yearDifference + "", font8);
					PdfPCell agecellval = new PdfPCell(agephval);
					agecellval.setHorizontalAlignment(Element.ALIGN_CENTER);
					agecellval.setFixedHeight(17);
					familydetailtab.addCell(agecellval);

					// for (int j = 0; j <
					// empadditionaldetails.getEmpAddDetList().size(); j++) {
                    /**Date 2-10-2019 by Amol **/
					String address="";
					
					if(empadditionaldetails.getEmpFamDetList()!=null&&empadditionaldetails.getEmpFamDetList().size()!=0){
					if(empadditionaldetails.getEmpFamDetList().get(i).getAddress()!=null){
						address=empadditionaldetails.getEmpFamDetList().get(i).getAddress();
					}else{
						address="";
					}
				}
					logger.log(Level.SEVERE, "reatives address "+address);
					Phrase addressval = new Phrase(address, font8);
					PdfPCell addressvalcell = new PdfPCell(addressval);
					addressvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					addressvalcell.setPaddingLeft(15);
					addressvalcell.setPaddingRight(15);
					familydetailtab.addCell(addressvalcell);
					// }
				}

				int remainingLines = 0;
				if (nooflines != 0) {
					remainingLines = 4 - (4 - nooflines);
				}
				System.out.println("remainingLines" + remainingLines);
				for (int j = 0; j < remainingLines; j++) {
					PdfPCell pCell = new PdfPCell(new Phrase(" ", font8));
					pCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					pCell.setFixedHeight(17);
					// pCell.setBorder(0);
					// pCell.setColspan(3);
					// pCell.setBorderWidthBottom(0);
//					familydetailtab.addCell(pCell);
//					familydetailtab.addCell(pCell);
//					familydetailtab.addCell(pCell);
//					familydetailtab.addCell(pCell);
//					familydetailtab.addCell(pCell);

				}

			} else {
				Phrase blank = new Phrase(" ", font8);
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setFixedHeight(17);
				familydetailtab.addCell(blankCell);
				familydetailtab.addCell(blankCell);
				familydetailtab.addCell(blankCell);
				familydetailtab.addCell(blankCell);
				familydetailtab.addCell(blankCell);
			}
		} else {
			Phrase blank = new Phrase(" ", font8);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setFixedHeight(17);
			familydetailtab.addCell(blankCell);
			familydetailtab.addCell(blankCell);
			familydetailtab.addCell(blankCell);
			familydetailtab.addCell(blankCell);
			familydetailtab.addCell(blankCell);
		}

		try {
			document.add(familydetailtab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createHader() {

		PdfPTable titletab = new PdfPTable(2);
		titletab.setWidthPercentage(100);
		// titletab.setSpacingBefore(40);
		try {
			titletab.setWidths(new float[] { 12, 88 });
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		Phrase titlephbl = new Phrase(" ", font10);
		PdfPCell titleCellbl = new PdfPCell(titlephbl);
		titleCellbl.setBorder(0);
//		titletab.addCell(titleCellbl);
//		titletab.addCell(titleCellbl);
//		titletab.addCell(titleCellbl);
		// titletab.addCell(titleCellbl);
		// titletab.addCell(titleCellbl);
		// titletab.addCell(titleCellbl);
		// titletab.addCell(titleCellbl);
		// titletab.addCell(titleCellbl);
		// titletab.addCell(titleCellbl);

		Phrase titlemainph = new Phrase("Bio - Data", font12bold);
		PdfPCell titlemainCell = new PdfPCell(titlemainph);
		titlemainCell.setBorder(0);
		titlemainCell.setPaddingBottom(8);
		titlemainCell.setColspan(2);
		titlemainCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titletab.addCell(titlemainCell);
		
		
		Phrase titleph = new Phrase("Branch :", font10bold);
		PdfPCell titleCell = new PdfPCell(titleph);
		titleCell.setBorder(0);
		titleCell.setPaddingBottom(8);
		titleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titletab.addCell(titleCell);

		/**
		 * Date : 14-08-2018 BY ANIL
		 * removed project name from branch
		 */
		Phrase titlevalph = new Phrase(employee.getBranchName(), font10);
//		Phrase titlevalph = new Phrase(employee.getBranchName()+" / "+employee.getProjectName(), font10);
		PdfPCell titlevalCell = new PdfPCell(titlevalph);
		titlevalCell.setBorder(0);
		titlevalCell.setPaddingBottom(10);
		titlevalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		titletab.addCell(titlevalCell);

		

		try {
			document.add(titletab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable emplyeeinfo = new PdfPTable(2);
		emplyeeinfo.setWidthPercentage(100);

		try {
			emplyeeinfo.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable empleftTab = new PdfPTable(2);
		empleftTab.setWidthPercentage(100);
		try {
			empleftTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase dateofjoin = new Phrase("Date Of Joining ", font8bold);
		PdfPCell dateofjoincell = new PdfPCell(dateofjoin);
		dateofjoincell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateofjoincell.setFixedHeight(17);
		empleftTab.addCell(dateofjoincell);

		Phrase dateofjoinval = new Phrase(fmt.format(employee.getJoinedAt()),
				font8);
		PdfPCell dateofjoincellval = new PdfPCell(dateofjoinval);
		dateofjoincellval.setFixedHeight(17);
		dateofjoincellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		empleftTab.addCell(dateofjoincellval);

		Phrase designation = new Phrase("Designation ", font8bold);
		PdfPCell designationcell = new PdfPCell(designation);
		designationcell.setFixedHeight(17);
		designationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		empleftTab.addCell(designationcell);

		PdfPTable desgAndDeptab = new PdfPTable(3);
		desgAndDeptab.setWidthPercentage(100);
		try {
			desgAndDeptab.setWidths(new float[] { 60, 20, 20 });
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Phrase desgVal = new Phrase(employee.getDesignation(), font8);
		PdfPCell desgValcellval = new PdfPCell(desgVal);
		desgValcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		desgValcellval.setBorder(0);
		desgAndDeptab.addCell(desgValcellval);

		Phrase depName = new Phrase("Department : ", font8bold);
		PdfPCell depNamecell = new PdfPCell(depName);
		depNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		depNamecell.setBorderWidthRight(0);
		depNamecell.setBorderWidthTop(0);
		depNamecell.setBorderWidthBottom(0);
		desgAndDeptab.addCell(depNamecell);

		Phrase depNameval = new Phrase(employee.getDepartMent()+ "", font8);
		PdfPCell depNamevalcell = new PdfPCell(depNameval);
		depNamevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		depNamevalcell.setBorder(0);
		desgAndDeptab.addCell(depNamevalcell);

		PdfPCell deptabcellval = new PdfPCell(desgAndDeptab);
		deptabcellval.setFixedHeight(17);
		deptabcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		empleftTab.addCell(deptabcellval);
		

		Phrase firstname = new Phrase("Employee Fullname", font8bold);
		PdfPCell firstnamecell = new PdfPCell(firstname);
		firstnamecell.setFixedHeight(17);
		firstnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		empleftTab.addCell(firstnamecell);

		PdfPTable nametab = new PdfPTable(3);
		nametab.setWidthPercentage(100);
		try {
			nametab.setWidths(new float[] { 60, 20, 20 });
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Phrase firstnameval = new Phrase(employee.getFullname(), font8);
		PdfPCell firstnamecellval = new PdfPCell(firstnameval);
		// firstnamecellval.setFixedHeight(17);
		firstnamecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		firstnamecellval.setBorder(0);
		nametab.addCell(firstnamecellval);

		Phrase empno = new Phrase("Emp No.", font8bold);
		PdfPCell empnocell = new PdfPCell(empno);
		// firstnamecellval.setFixedHeight(17);
		empnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		empnocell.setBorderWidthRight(0);
		empnocell.setBorderWidthTop(0);
		empnocell.setBorderWidthBottom(0);
		nametab.addCell(empnocell);

		Phrase empnoval = new Phrase(employee.getCount() + "", font8);
		PdfPCell empnovalcell = new PdfPCell(empnoval);
		// firstnamecellval.setFixedHeight(17);
		empnovalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		empnovalcell.setBorder(0);
		nametab.addCell(empnovalcell);

		/*** name value tab complet ***/

		// Phrase firstnametabval=new Phrase(employee.getFullname(),font8);
		PdfPCell firstnametabcellval = new PdfPCell(nametab);
		firstnamecellval.setFixedHeight(17);
		firstnametabcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		empleftTab.addCell(firstnametabcellval);

		Phrase midlename = new Phrase("Father/Husband Name", font8bold);
		PdfPCell middlecell = new PdfPCell(midlename);
		middlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		middlecell.setFixedHeight(17);
		empleftTab.addCell(middlecell);

		String relativename = "";
		
		/**Date 11-8-2020 by Amol 
		 * added condition first it will check the father name from employee master
		 */
		
		if(employee.getHusbandName()!=null&&!employee.getHusbandName().equals("")){
			relativename=employee.getHusbandName();
		}else{
			if (empadditionaldetails != null) {
				for (int i = 0; i < empadditionaldetails.getEmpFamDetList().size(); i++) {
					
					if(empadditionaldetails.getEmpFamDetList().get(i).getFamilyRelation().equalsIgnoreCase("Father")
							||empadditionaldetails.getEmpFamDetList().get(i).getFamilyRelation().equalsIgnoreCase("Husband"))
					
				relativename = empadditionaldetails.getEmpFamDetList().get(i)
							.getFname();
				}
			}
		}
		Phrase midlenameval = new Phrase(relativename, font8);
		PdfPCell middlecellval = new PdfPCell(midlenameval);
		middlecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		middlecellval.setFixedHeight(17);
		empleftTab.addCell(middlecellval);

		// Phrase surname=new Phrase("Surname",font8bold);
		// PdfPCell surnamecell=new PdfPCell(surname);
		// surnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// surnamecell.setFixedHeight(17);
		// empleftTab.addCell(surnamecell);
		//
		// Phrase surnameval=new Phrase(employee.getLastName(),font8);
		// PdfPCell surnamecellval=new PdfPCell(surnameval);
		// surnamecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		// surnamecellval.setFixedHeight(17);
		// empleftTab.addCell(surnamecellval);

		Phrase maritalstatus = new Phrase("Marital Status", font8bold);
		PdfPCell maritalstatuscell = new PdfPCell(maritalstatus);
		maritalstatuscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		maritalstatuscell.setFixedHeight(17);
		empleftTab.addCell(maritalstatuscell);

		/*** image **/

		PdfPTable checkboxtab = new PdfPTable(6);
		checkboxtab.setWidthPercentage(100);
		try {
			checkboxtab.setWidths(new float[] { 5, 25, 5, 25, 5, 25 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Image blankImg = null;
		try {
			blankImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		blankImg.scalePercent(9);
		blankImg.scaleAbsoluteHeight(8);
		blankImg.scaleAbsoluteWidth(8);

		Image tickImg = null;
		try {
			tickImg = Image.getInstance("images/checked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		tickImg.scalePercent(9);
		tickImg.scaleAbsoluteHeight(8);
		tickImg.scaleAbsoluteWidth(8);

		PdfPCell imagebl = null;
		if (employee.getMaritalStatus().equalsIgnoreCase("Married")) {
			imagebl = new PdfPCell(tickImg);
		} else {
			imagebl = new PdfPCell(blankImg);
		}

		imagebl.setBorder(0);
		imagebl.setPaddingTop(3);
		imagebl.setHorizontalAlignment(Element.ALIGN_RIGHT);
		checkboxtab.addCell(imagebl);
		// Married
		Phrase married = new Phrase("Married", font8bold);
		PdfPCell marriedcell = new PdfPCell(married);
		marriedcell.setBorder(0);
		marriedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		checkboxtab.addCell(marriedcell);
		// Unmarried
		if (employee.getMaritalStatus().equalsIgnoreCase("Unmarried")) {
			imagebl = new PdfPCell(tickImg);
		} else {
			imagebl = new PdfPCell(blankImg);
		}
		imagebl.setBorder(0);
		imagebl.setPaddingTop(3);
		imagebl.setHorizontalAlignment(Element.ALIGN_RIGHT);
		checkboxtab.addCell(imagebl);

		Phrase unmarried = new Phrase("Unmarried", font8bold);
		PdfPCell unmarriedcell = new PdfPCell(unmarried);
		unmarriedcell.setBorder(0);
		unmarriedcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		checkboxtab.addCell(unmarriedcell);

		if (employee.getMaritalStatus().equalsIgnoreCase("Other")) {
			imagebl = new PdfPCell(tickImg);
		} else {
			imagebl = new PdfPCell(blankImg);
		}
		imagebl.setBorder(0);
		imagebl.setPaddingTop(3);
		imagebl.setHorizontalAlignment(Element.ALIGN_RIGHT);
		checkboxtab.addCell(imagebl);
		// Other
		Phrase other = new Phrase("Other", font8bold);
		PdfPCell othercell = new PdfPCell(other);
		othercell.setBorder(0);
		othercell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		if (employee.getMaritalStatus().equalsIgnoreCase("Other")) {
//			imagebl = new PdfPCell(tickImg);
//		} else {
//			imagebl = new PdfPCell(blankImg);
//		}
		checkboxtab.addCell(othercell);

		// complet

		// Phrase maritalstatusval=new Phrase(" ",font8);
		PdfPCell maritalstatuscellval = new PdfPCell(checkboxtab);
		maritalstatuscellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		maritalstatuscellval.setFixedHeight(17);
		maritalstatuscellval.setPaddingLeft(7);
		empleftTab.addCell(maritalstatuscellval);

		Phrase dateofbirth = new Phrase("Date of Birth", font8bold);
		PdfPCell dateofbirthcell = new PdfPCell(dateofbirth);
		dateofbirthcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateofbirthcell.setFixedHeight(17);
		empleftTab.addCell(dateofbirthcell);

		PdfPTable dobtab = new PdfPTable(5);
		dobtab.setWidthPercentage(100);
		try {
			dobtab.setWidths(new float[] { 30,30,30, 20, 20 });
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Phrase dateofbirthval = new Phrase(fmt.format(employee.getDob()), font8);
		PdfPCell dateofbirthcellval = new PdfPCell(dateofbirthval);
		dateofbirthcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateofbirthcellval.setBorder(0);
		dobtab.addCell(dateofbirthcellval);
		
		
		Phrase genderph = new Phrase("Gender : ", font8bold);
		PdfPCell gendercell = new PdfPCell(genderph);
		gendercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gendercell.setBorderWidthRight(0);
		gendercell.setBorderWidthBottom(0);
		gendercell.setBorderWidthTop(0);
		// agecell.setFixedHeight(17);
		dobtab.addCell(gendercell);
		
		
		Phrase gendervalph = new Phrase(employee.getGender(), font8);
		PdfPCell gendervalcell = new PdfPCell(gendervalph);
		gendervalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gendervalcell.setBorderWidthLeft(0);
		gendervalcell.setBorderWidthBottom(0);
		gendervalcell.setBorderWidthTop(0);
		// agecell.setFixedHeight(17);
		dobtab.addCell(gendervalcell);
		

		Phrase ageph = new Phrase("Age : ", font8bold);
		PdfPCell agecell = new PdfPCell(ageph);
		agecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		agecell.setBorderWidthRight(0);
		agecell.setBorderWidthBottom(0);
		agecell.setBorderWidthTop(0);
		// agecell.setFixedHeight(17);
		dobtab.addCell(agecell);

		Date birthdate = null;
		birthdate = employee.getDob();
		/** Calculate Age ***/

		Calendar birth = Calendar.getInstance();
		birth.setTime(birthdate);
		Calendar today = Calendar.getInstance();

		int yearDifference = today.get(Calendar.YEAR)
				- birth.get(Calendar.YEAR);

		if (today.get(Calendar.MONTH) < birth.get(Calendar.MONTH)) {
			yearDifference--;
		} else {
			if (today.get(Calendar.MONTH) == birth.get(Calendar.MONTH)
					&& today.get(Calendar.DAY_OF_MONTH) < birth
							.get(Calendar.DAY_OF_MONTH)) {
				yearDifference--;
			}

		}

		Phrase ageval = new Phrase(yearDifference + "", font8);
		PdfPCell agecellval = new PdfPCell(ageval);
		agecellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		// agecellval.setFixedHeight(17);
		agecellval.setBorder(0);
		dobtab.addCell(agecellval);

		// Phrase agetabval=new Phrase(yearDifference+"",font8);
		PdfPCell agetabcellval = new PdfPCell(dobtab);
		agetabcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		agecellval.setFixedHeight(17);
		// agetabcellval.setBorder(0);
		empleftTab.addCell(agetabcellval);

		Phrase educationph = new Phrase("Educational Qualification", font8bold);
		PdfPCell educationcell = new PdfPCell(educationph);
		educationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		educationcell.setFixedHeight(17);
		empleftTab.addCell(educationcell);

		String edu = "";
		for (int i = 0; i < employee.getEducationalInfo().size(); i++) {
			edu = edu+ employee.getEducationalInfo().get(i).getDegreeObtained()+ "/";
		}
		
		/**
		 * Date  : 18-09-2018 BY ANIL
		 */
		if(!edu.equals(""))
			edu=edu.substring(0, edu.length()-1);
		/**
		 * End
		 */

		Phrase educationphval = new Phrase(edu, font8);
		PdfPCell educationcellval = new PdfPCell(educationphval);
		educationcellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		educationcellval.setFixedHeight(17);
		empleftTab.addCell(educationcellval);

		/**** image ***/
		DocumentUpload photodocument = employee.getPhoto();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + photodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setPaddingTop(8);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Image image1=null;
		// try
		// {
		// image1=Image.getInstance("images/ipclogo4.jpg");
		// image1.scalePercent(20f);
		// // image1.setAbsolutePosition(40f,765f);
		// // doc.add(image1);
		//
		//
		//
		//
		//
		// imageSignCell=new PdfPCell(image1);
		// // imageSignCell.addElement();
		// // imageSignCell.setImage(image1);
		// imageSignCell.setFixedHeight(40);
		// imageSignCell.setBorder(0);
		// imageSignCell.setPaddingTop(8);
		// imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}

		PdfPCell leftcell = new PdfPCell(empleftTab);
		PdfPCell rightCell = new PdfPCell(logoTab);

		emplyeeinfo.addCell(leftcell);
		emplyeeinfo.addCell(rightCell);

		try {
			document.add(emplyeeinfo);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Contacttable

		PdfPTable contacttab = new PdfPTable(4);
		contacttab.setWidthPercentage(100);
		try {
			contacttab.setWidths(new float[] { 20, 20, 35, 25 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase contactph = new Phrase("Contact No.", font8bold);
		PdfPCell contactcell = new PdfPCell(contactph);
		contactcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// contactcell.setColspan(2);
		contactcell.setFixedHeight(17);
		contacttab.addCell(contactcell);

		Phrase contactvalph = new Phrase(employee.getCellNumber1() + "", font8);
		PdfPCell contactvalcell = new PdfPCell(contactvalph);
		contactvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// contactcell.setColspan(2);
		contactvalcell.setFixedHeight(17);
		contacttab.addCell(contactvalcell);

		Phrase altcontactph = new Phrase("Alternate Contact No.", font8bold);
		PdfPCell altcontactcell = new PdfPCell(altcontactph);
		altcontactcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		altcontactcell.setFixedHeight(17);
		contacttab.addCell(altcontactcell);

		Phrase altcontactvalph = new Phrase(employee.getCellNumber2() + "",
				font8);
		PdfPCell altcontactvalcell = new PdfPCell(altcontactvalph);
		altcontactvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		altcontactvalcell.setFixedHeight(17);
		contacttab.addCell(altcontactvalcell);

		Phrase emegcontactph = new Phrase("Emergency Contact No", font8bold);
		PdfPCell emegcontactcell = new PdfPCell(emegcontactph);
		emegcontactcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emegcontactcell.setFixedHeight(17);
		emegcontactcell.setColspan(2);
		contacttab.addCell(emegcontactcell);

		String emgcontact = "";
		/**
		 * Date : 18-09-2018 BY ANIL
		 */
//		if (empadditionaldetails != null) {
//			for (int i = 0; i < empadditionaldetails.getEmpAddDetList().size(); i++) {
//				emgcontact = empadditionaldetails.getEmpFamDetList().get(0).getCellNo()+ "";
//			}
//		}
		if(employee.getLandline()!=null&&employee.getLandline()!=0){
			emgcontact=employee.getLandline()+"";
		}
		
		/**
		 * End
		 */

		Phrase emegcontactvalph = new Phrase(emgcontact, font8);
		PdfPCell emegcontactvalcell = new PdfPCell(emegcontactvalph);
		emegcontactvalcell.setColspan(2);
		emegcontactvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emegcontactvalcell.setFixedHeight(17);
		contacttab.addCell(emegcontactvalcell);

		// Phrase languph=new Phrase("Language Spoken",font8);
		// PdfPCell langucell=new PdfPCell(languph);
		// langucell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// langucell.setFixedHeight(17);
		// contacttab.addCell(langucell);
		//
		// Phrase languphval=new Phrase("",font8);
		// PdfPCell languvalcell=new PdfPCell(languphval);
		// languvalcell.setColspan(2);
		// languvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// languvalcell.setFixedHeight(17);
		// contacttab.addCell(languvalcell);

		// contacttab end

		PdfPTable skilltab = new PdfPTable(6);
		skilltab.setWidthPercentage(100);
		try {
			skilltab.setWidths(new float[] { 20, 5, 30, 5, 25, 5 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Image skillImg = null;
		try {
			skillImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		skillImg.scalePercent(9);
		skillImg.scaleAbsoluteHeight(8);
		skillImg.scaleAbsoluteWidth(8);
		
		
		Phrase skill = new Phrase("Skilled", font8);
		PdfPCell skilledCell = new PdfPCell(skill);
		skilledCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		skilledCell.setFixedHeight(17);
		skilledCell.setBorderWidthRight(0);
		skilltab.addCell(skilledCell);

		/**
		 * Date : 18-09-2018 BY ANIL
		 */
		PdfPCell imageskill = null;
		if(employee.isSkilled()==true){
			imageskill = new PdfPCell(tickImg);
		}else{
			imageskill = new PdfPCell(skillImg);
		}
		
//		PdfPCell imageskill = new PdfPCell(skillImg);
		imageskill.setBorder(0);
		imageskill.setPaddingTop(3);
		imageskill.setHorizontalAlignment(Element.ALIGN_LEFT);
		imageskill.setBorderWidthLeft(0);
		skilltab.addCell(imageskill);

		Phrase semiskill = new Phrase("Semi-Skilled", font8);
		PdfPCell semiskilledCell = new PdfPCell(semiskill);
		semiskilledCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		semiskilledCell.setFixedHeight(17);
		semiskilledCell.setBorderWidthRight(0);
		skilltab.addCell(semiskilledCell);
		
		/**
		 * Date : 18-09-2018 BY ANIL
		 */
		PdfPCell semiSkilledCell = null;
		if(employee.isSemiSkilled()==true){
			semiSkilledCell = new PdfPCell(tickImg);
		}else{
			semiSkilledCell = new PdfPCell(skillImg);
		}
		semiSkilledCell.setBorder(0);
		semiSkilledCell.setPaddingTop(3);
		semiSkilledCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		semiSkilledCell.setBorderWidthLeft(0);
		skilltab.addCell(semiSkilledCell);
		/**
		 * End
		 */

		Phrase unskill = new Phrase("Unskilled", font8);
		PdfPCell unskilledCell = new PdfPCell(unskill);
		unskilledCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		unskilledCell.setFixedHeight(17);
		unskilledCell.setBorderWidthRight(0);
		skilltab.addCell(unskilledCell);

		/**
		 * Date : 18-09-2018 BY ANIL
		 */
		PdfPCell unSkilledCell = null;
		if(employee.isUnskilled()==true){
			unSkilledCell = new PdfPCell(tickImg);
		}else{
			unSkilledCell = new PdfPCell(skillImg);
		}
		unSkilledCell.setBorder(0);
		unSkilledCell.setPaddingTop(3);
		unSkilledCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		unSkilledCell.setBorderWidthLeft(0);
		skilltab.addCell(unSkilledCell);
		/**
		 * End
		 */

		// Phrase empno1=new Phrase("Emp.No",font8);
		// PdfPCell empCell=new PdfPCell(empno1);
		// empCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// empCell.setFixedHeight(17);
		// skilltab.addCell(empCell);
		//
		//
		// Phrase blankph=new Phrase(" ",font8);
		// PdfPCell blankCell=new PdfPCell(blankph);
		// blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// blankCell.setFixedHeight(17);
		// skilltab.addCell(blankCell);

		// Phrase skillval=new Phrase(" ",font8);
		// PdfPCell skilledCellval=new PdfPCell(skillval);
		// skilledCellval.setHorizontalAlignment(Element.ALIGN_CENTER);
		// skilledCellval.setFixedHeight(17);
		// skilltab.addCell(skilledCellval);
		//
		// Phrase semiskillval=new Phrase(" ",font8);
		// PdfPCell semiskilledvalCell=new PdfPCell(semiskillval);
		// semiskilledvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// semiskilledvalCell.setFixedHeight(17);
		// skilltab.addCell(semiskilledvalCell);
		//
		// Phrase unskillval=new Phrase(" ",font8);
		// PdfPCell unskilledvalCell=new PdfPCell(unskillval);
		// unskilledvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// unskilledvalCell.setFixedHeight(17);
		// skilltab.addCell(unskilledvalCell);
		//
		//
		// Phrase empnoval1=new Phrase(" ",font8);
		// PdfPCell empnovalCell=new PdfPCell(empnoval1);
		// empnovalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// empnovalCell.setFixedHeight(17);
		// skilltab.addCell(empnovalCell);
		//
		// skilltab.addCell(blankCell);

		Phrase languph = new Phrase("Language Spoken", font8bold);
		PdfPCell langucell = new PdfPCell(languph);
		langucell.setHorizontalAlignment(Element.ALIGN_LEFT);
		langucell.setFixedHeight(17);
		langucell.setColspan(2);
		skilltab.addCell(langucell);

		/**
		 * Date : 18-09-2018 By ANIL
		 */
		Phrase languphval = null;
		if(employee.getLanuageSpoken()!=null){
			languphval = new Phrase(employee.getLanuageSpoken(), font8);
		}else{
			languphval = new Phrase(" ", font8);
		}
//		Phrase languphval = new Phrase(" ", font8);
		PdfPCell languvalcell = new PdfPCell(languphval);
		languvalcell.setColspan(4);
		languvalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		languvalcell.setFixedHeight(17);
		skilltab.addCell(languvalcell);

		// Phrase email=new Phrase("Email ID",font8);
		// PdfPCell emailCell=new PdfPCell(email);
		// emailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// emailCell.setFixedHeight(17);
		// skilltab.addCell(emailCell);
		//
		// Phrase emailval=new Phrase(employee.getEmail(),font8);
		// PdfPCell emailCellval=new PdfPCell(emailval);
		// emailCellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		// emailCellval.setColspan(4);
		// emailCellval.setFixedHeight(17);
		// skilltab.addCell(emailCellval);

		// Skill tab complet

		PdfPTable secondheader = new PdfPTable(2);
		secondheader.setWidthPercentage(100);
		try {
			secondheader.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell leftseccell = new PdfPCell(contacttab);
		PdfPCell rightseccell = new PdfPCell(skilltab);

		secondheader.addCell(leftseccell);
		secondheader.addCell(rightseccell);

		Phrase email = new Phrase("Email ID :  " + employee.getEmail(),
				font8bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailCell.setFixedHeight(17);
		emailCell.setColspan(2);
		if (employee.getEmail() != null) {
			secondheader.addCell(emailCell);
		}
		// Phrase emailval=new Phrase(employee.getEmail(),font8);
		// PdfPCell emailCellval=new PdfPCell(emailval);
		// emailCellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		// emailCellval.setFixedHeight(17);
		// contacttab.addCell(emailCellval);

		try {
			document.add(secondheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		// try {
		// Image image2 = Image.getInstance(new URL(hostUrl
		// + document.getUrl()));
		// image2.scalePercent(15f);
		// image2.scaleAbsoluteWidth(520f);
		// image2.setAbsolutePosition(40f, 725f);
		// doc.add(image2);
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		try {
			Image image1 = Image.getInstance("images/Capture.JPG");
			image1.scalePercent(15f);
			image1.scaleAbsoluteWidth(520f);
			image1.setAbsolutePosition(40f, 725f);
			doc.add(image1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
