package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.TimeZone;

import com.google.gwt.core.client.GWT;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class ServiceRecordPdf {

	public Document document;
	Service service;
	Customer cust;
	Company comp;
	
	/**
	 * Date : 29-05-2017 by ANIL
	 */
	Contract contract;
	
	/**
	 * End
	 */
	
	/*
	 * Date :21-01-2018
	 * @author Ashwini
	 */
	Boolean rowflag =false;
	Boolean headerFooter = false;
	ProcessConfiguration processconfig;
	
	/*
	 * end by Ashwini
	 */
	
	
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font11bold, font12boldul, font12, font10bold, font10, font14bold,
			font9;
	
	SimpleDateFormat fmt =new SimpleDateFormat("dd-MM-yyyy");
	PdfPTable a4Table = new PdfPTable(2);

	PdfPTable a5Table1 = new PdfPTable(1);
	PdfPTable a5Table2 = new PdfPTable(1);
	
	public ServiceRecordPdf() {
      
		/**30-10-2017 sagar sore [ partitioning page vertically in middle and displaying same data on left and right side of page ]**/
		a5Table1.setSpacingAfter(0);
		a5Table1.setSpacingBefore(0);
		a5Table2.setSpacingAfter(0);
		a5Table2.setSpacingBefore(0);
		a5Table1.setWidthPercentage(100);
		a5Table2.setWidthPercentage(100);
		a4Table.setWidthPercentage(100);
		/** 30-10-2017 sagar sore []**/
//		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
//		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
//		font8 = new Font(Font.FontFamily.HELVETICA, 8);
//		font9 = new Font(Font.FontFamily.HELVETICA, 9);
//		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
//		font12 = new Font(Font.FontFamily.HELVETICA, 12);
//		font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
//		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
//		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
//		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
//		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
//		
		
		
		/**
		 * Date 27/11/2017
		 * By jayshree 
		 * font Size increse by one
		 */
		new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 17, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//checking font 6 by Ajinkya
		font8 = new Font(Font.FontFamily.HELVETICA, 6);//checking font 6 by Ajinkya
		font9 = new Font(Font.FontFamily.HELVETICA, 10);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 13, Font.BOLD);
		font12 = new Font(Font.FontFamily.HELVETICA, 13);
		font11bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font10bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);//checking font 8 by Ajinkya
		font10 = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);   //checking font 8 by Ajinkya
		font14bold = new Font(Font.FontFamily.HELVETICA, 15, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		//End By Jayshree
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	
	public void loadAll(Long count) {

		service=ofy().load().type(Service.class).id(count).now();
		
		if(service.getCompanyId()==null)
			cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).first().now();
		else
			cust=ofy().load().type(Customer.class).filter("count",service.getPersonInfo().getCount()).filter("companyId", service.getCompanyId()).first().now();

		
		
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
		
		/**
		 * Date : 29-05-2017 By ANIL
		 * Loading Contract
		 */
		if(service.getCompanyId()!= null){
			contract=ofy().load().type(Contract.class).filter("companyId", service.getCompanyId()).filter("count", service.getContractCount()).first().now();
		}
		/**
		 * End
		 */
		
		/*
		 * Date:21-01-2018
		 * @author Ashwini
		 * Des:To dispaly by defaulty 4 rows in in table .
		 */
		
		if(service.getCompanyId()!=null){
			processconfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", service.getCompanyId())
					.filter("processName", "Service")
					.filter("configStatus", true).first().now();
			if(processconfig!=null){
				for(int k= 0 ;k<processconfig.getProcessList().size() ; k++){
					
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("DecreaseTableRow")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						rowflag = true;
						System.out.println("rowflag:::::");
					}
					
					
					if (processconfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderFooter")
							&& processconfig.getProcessList().get(k).isStatus() == true) {
						headerFooter = true;
						
					}
					
				}
			}
			
			
		}
		
		/*
		 * end by Ashwini
		 */
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}


	public void createPdf(String preprintStatus) {
/**
 * Date 27/11/2017
 * By jayshree 
 * comment method to remove header
 */
		
		if(headerFooter){
			
			if(preprintStatus.equals("no")){
				if(comp.getUploadHeader()!=null){
					/**
					 * Date 06-03-2019 by vijay
					 */
					Paragraph blank = new Paragraph();
					blank.add(Chunk.NEWLINE);
					try {
						document.add(blank);
						document.add(blank);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/**
					 * ends here
					 */
					
					createCompanyNameAsHeader(document , comp);
					}
			}
		}
		
		
						
//		createBlankHeading();
//        createBlankForLetterHead();By jayshree comment method
		//End By Jayshree
		
		/**
		 * Date 15-09-2018 By Vijay
		 * Des :- added header and footer They will print on preeprint stationary so if preprint is no them it will print as old pdf settingd
		 * if yes then it will print on proper with preeprint stationary
		 */
		
		if(preprintStatus.equals("yes")){
			createBlankforUPC();
		}
		/**
		 * ends here
		 */
		createHeading();
		serviceinformation();
		
		servicesCarriedOut();
		trapCatchesList();
		feedbackFromCustomer();
		sinatureofCustAndNBHCRepresentative();
	
		
		/**
		 * Date 28-01-2018
		 * By Ashwini
		 * To add footer in pdf 
		 */
		if(headerFooter){
			
			
			if(preprintStatus.equals("no")){
				if(comp.getUploadFooter()!=null){
					/**
					 * Date 06-03-2019 by vijay
					 */
					Paragraph blank = new Paragraph();
					blank.add(Chunk.NEWLINE);
					try {
						document.add(blank);
						document.add(blank);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					/**
					 * ends here
					 */
					createCompanyNameAsFooter(document,comp);//By jayshree comment method
				}
			}
		}
		
		
		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
		addTablesToParentTable();
		/**31-10-2017 sagar sore [to make copy of pdf on half side of page]**/
		
//		createCompanyDetails();
	}
//End By Jayshree

	private void createBlankforUPC() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Updated by: Viraj
	 * Date: 21-02-2019
	 * Description: To show header and footer dynamically
	 * @param doc
	 * @param comp
	 */
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
//			doc.add(image2);
			a5Table1.addCell(image2);
			a5Table2.addCell(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
//			doc.add(image2);
			a5Table1.addCell(image2);
			a5Table2.addCell(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/** Ends **/
//	private void createCompanyNameAsFooter(Document doc, Company comp) {
//		
//		
//		DocumentUpload document =comp.getUploadFooter();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
////			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			Image image2 = Image.getInstance("images/NBHC_Footer.png");
////			image2.scalePercent(10f);
////			image2.scaleAbsoluteWidth(270f);
//			image2.setAbsolutePosition(50f,730f);	
//			image2.scalePercent(35f);
//			image2.scaleAbsoluteWidth(375f);
//			image2.setSpacingBefore(5f);
////			image2.setAbsolutePosition(0f,725f);
//			
//		//	Image image3 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			Image image3 = Image.getInstance("images/NBHC_Footer.png");
////			image3.scalePercent(15f);
////			image3.scaleAbsoluteWidth(270f);
//		/*	image3.setAbsolutePosition(480f,20f);
//			image3.scalePercent(20f);
//			image3.scaleAbsoluteWidth(350f);*/
//			
////			image3.setAbsolutePosition(0f,725f);
//			/*** 30-10-2017 sagar sore [To show header two time on vertically divided page]**/
//		/*	PdfPCell blankCell = new PdfPCell();
//			PdfPCell imageCell = new PdfPCell();
//			imageCell.addElement(image2);
//			imageCell.setBorder(0);
//		
//			//		doc.add(image2);
//				doc.add(image2);
//				doc.add(image3);*/
//				  /** end **/
//				
//				PdfPCell imageCell = new PdfPCell();
//				imageCell.addElement(image2);
//				imageCell.setBorder(0);
//              
//				/*
//				 * commented by Ashwini
//				 */
////						a5Table1.addCell(imageCell);
////						a5Table2.addCell(imageCell);
//						a5Table1.addCell(image2);
//						a5Table2.addCell(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
////		try
////		{
////		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(520f);
////		image1.setAbsolutePosition(40f,40f);	
////		doc.add(image1);
////		}
////		catch(Exception e)
////		{
////			e.printStackTrace();
////		}
//		}
//	
private void createBlankForLetterHead(){
		
		a5Table1.setWidthPercentage(100);
		a5Table2.setWidthPercentage(100);
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
	  
		PdfPCell paraCell = new PdfPCell(blank);
		paraCell.setBorder(0);
		
		a5Table1.addCell(paraCell);
		a5Table2.addCell(paraCell); 

	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
		PdfPCell blTableCell = new PdfPCell();
		blTableCell.setBorder(0);
		blTableCell.addElement(bltable);

		System.out.println("BLANK CELL ADDED IN DOCUMENT...");

	}
	private void createBlankHeading() {
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	
	    
		
	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//	    try {
//			document.add(blank);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//       
	    /**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
	    PdfPCell blankCell = new PdfPCell();
	    blankCell.setBorder(0);
	    blankCell.addElement(blank);		
//		a5Table1.addCell(blankCell);
//		a5Table2.addCell(blankCell);
		
	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
//		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/	    
//		try {
//			document.add(bltable);
//			
//			
//			System.out.println("BLANK CELL ADDED IN DOCUMENT...");
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/

		a5Table1.addCell(blankCell);
		a5Table2.addCell(blankCell);
	}
	
//private void createCompanyNameAsHeader(Document doc, Company comp) {
//		
//		DocumentUpload document =comp.getUploadHeader();
//
//		//patch
//		String hostUrl;
//		String environment = System.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//		    String applicationId = System.getProperty("com.google.appengine.application.id");
//		    String version = System.getProperty("com.google.appengine.application.version");
//		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//		} else {
//		    hostUrl = "http://localhost:8888";
//		}
//		
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(15f);
//			image2.scaleAbsoluteWidth(520f);
//			image2.setAbsolutePosition(40f,725f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//		
//		
////		try
////		{
////			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(520f);
////		image1.setAbsolutePosition(40f,725f);	
////		doc.add(image1);
////		}
////		catch(Exception e)
////		{
////			e.printStackTrace();
////		}
//		}

	
	private void createHeading() {
		a5Table1.setWidthPercentage(100);
		a5Table2.setWidthPercentage(100);
		Paragraph para = new Paragraph("Pest Management Service Record",font10bold);
		para.setAlignment(Element.ALIGN_CENTER);

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(para);
//			
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
        PdfPCell paraCell = new PdfPCell();
        paraCell.setBorder(0);
        paraCell.addElement(para);
        
		a5Table1.addCell(paraCell);
		a5Table2.addCell(paraCell);
	}

	private void serviceinformation() {
        /** 30-10-2017 sagar sore [widths updated]**/
	
		float[] columnWidths = {1.5f, 0.1f, 2.0f,1.0f, 0.1f, 2.1f,0.8f, 0.1f, 2.3f};
		PdfPTable table = new PdfPTable(9);
		
		table.setSpacingBefore(0f);
		table.setWidthPercentage(100f);

		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase serialNo =  new Phrase("Serial No.",font8bold);
		PdfPCell serialNoCell = new PdfPCell(serialNo);
		serialNoCell.setBorder(0);
		serialNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase branch =new Phrase("Branch",font8bold);
		PdfPCell branchCell = new PdfPCell(branch);
		branchCell.setBorder(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase serdate =new Phrase("Date",font8bold);
		PdfPCell serdateCell = new PdfPCell(serdate);
		serdateCell.setBorder(0);
		serdateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase dotted =new Phrase(":",font8);
		PdfPCell dottedCell = new PdfPCell(dotted);
		dottedCell.setBorder(0);
		dottedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase serialNoValue =new Phrase(service.getCount()+"",font8);
		PdfPCell serdateValueCell = new PdfPCell(serialNoValue);
		serdateValueCell.setBorder(0);
		serdateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase branchValue =new Phrase(service.getBranch(),font8);
		PdfPCell branchValueCell = new PdfPCell(branchValue);
		branchValueCell.setBorder(0);
		branchValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase servicedateValue =new Phrase(fmt.format(service.getServiceDate()),font8);
		PdfPCell servicedateValueCell = new PdfPCell(servicedateValue);
		servicedateValueCell.setBorder(0);
		servicedateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/**
		 * Date : 29-05-2017 By ANIL
		 * Adding Blank Cell
		 */
		
		Phrase blank =new Phrase("",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * End
		 */
		
		table.addCell(serialNoCell);
		table.addCell(dottedCell);
		/**
		 *  Date : 29-05-2017 By ANIL
		 */
//		table.addCell(serdateVaslueCell);
		table.addCell(serdateValueCell);
		/**
		 * End
		 */
		
		table.addCell(branchCell);
		table.addCell(dottedCell);
		table.addCell(branchValueCell);
		
		table.addCell(serdateCell);
		table.addCell(dottedCell);
		/**
		 *  Date : 29-05-2017 By ANIL
		 */
//		table.addCell(servicedateValueCell);
		table.addCell(servicedateValueCell);
		/**
		 * End
		 */
		
		
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.setWidthPercentage(100);
		
		parenttable.setSpacingBefore(10f);//By Jayshree Add the table spacing
//		parenttable.setSpacingAfter(20f);
		
		PdfPCell tableCell = new PdfPCell(table);
		tableCell.setBorder(0);
		parenttable.addCell(tableCell);
		Phrase customerName = new Phrase("Customer : "+cust.getCustomerName(),font8);
		PdfPCell customerNameCell = new PdfPCell(customerName);
		customerNameCell.addElement(customerName);
		customerNameCell.setBorder(0);
		parenttable.addCell(customerNameCell);
		
				
		String custAdd1="";
		String custFullAdd1="";
		
	if(service.getAddress()!=null){
			
			if(!service.getAddress().getAddrLine2().equals("")){
				if(!service.getAddress().getLandmark().equals("")){
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getAddrLine2()+","+service.getAddress().getLandmark();
				}else{
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getAddrLine2();
				}
			}else{
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=service.getAddress().getAddrLine1()+","+service.getAddress().getLandmark();
				}else{
					custAdd1=service.getAddress().getAddrLine1();
				}
			}
			
			if(!service.getAddress().getLocality().equals("")){
				custFullAdd1=custAdd1+","+service.getAddress().getLocality()+","+service.getAddress().getCity()+"-"+service.getAddress().getPin()+","+service.getAddress().getState()+","+service.getAddress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+","+service.getAddress().getCity()+"-"+service.getAddress().getPin()+","+service.getAddress().getState()+","+service.getAddress().getCountry();
			}
		}
		
	
	Phrase custFullAdd1p = new Phrase("Address : "+custFullAdd1,font8);
	PdfPCell custFullAdd1pCell = new PdfPCell(custFullAdd1p);
	custFullAdd1pCell.setBorder(0);
	parenttable.addCell(custFullAdd1pCell);
		
	
	
	
	//   this code is for service timing   
	Phrase serviceTime = null;
	if(!service.getFromTime().equals("") && !service.getToTime().equals("")){
		 serviceTime = new Phrase("Time Of Service : From "+service.getFromTime()+" To "+service.getToTime(),font8);
	}
	else
	{
		 serviceTime = new Phrase("Time Of Service : From "+"_______"+" To "+"_______",font8);
	}
	PdfPCell serviceTimeCell = new PdfPCell(serviceTime);
	serviceTimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	serviceTimeCell.setBorder(0);
	
	parenttable.addCell(serviceTimeCell);

    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(parenttable);
//			
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
		PdfPCell parentTableCell = new PdfPCell();
		parentTableCell.setBorder(0);
		parentTableCell.addElement(parenttable);
		a5Table1.addCell(parentTableCell);
		a5Table2.addCell(parentTableCell);
		
	}

	
	private void servicesCarriedOut(){
		
		Paragraph servicepara = new Paragraph("Summary of Pest Treatments :",font8bold);
		servicepara.setAlignment(Element.ALIGN_LEFT);
		
		Paragraph product = new Paragraph(service.getProductName(),font8);
		product.setAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100f);
		
		table.setSpacingBefore(5f);
		
		Phrase service = new Phrase("Service",font8bold);
		Phrase targetPest = new Phrase("Target Pest",font8bold);
		Phrase treatments = new Phrase("Treatments",font8bold);
		Phrase materials = new Phrase("Materials",font8bold);
		
		Phrase blanks = new Phrase(" ",font8);
		
		PdfPCell serviceCell = new PdfPCell(service);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell targetPestCell = new PdfPCell(targetPest);
		targetPestCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell treatmentsCell = new PdfPCell(treatments);
		treatmentsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell materialsCell = new PdfPCell(materials);
		materialsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell blanksCell = new PdfPCell(blanks);
		
		table.addCell(serviceCell);
		table.addCell(targetPestCell);
		table.addCell(treatmentsCell);
		table.addCell(materialsCell);
		
		/**
		 * Date : 29-05-2017 By ANIL
		 * Showing all the services of contract in the table
		 */
//		table.addCell(product);
//		table.addCell(blanksCell);
//		table.addCell(blanksCell);
//		table.addCell(blanksCell);
		
//		for (int i = 0; i <= 7; i++) {
//			
//			table.addCell(blanksCell);
//			table.addCell(blanksCell);
//			table.addCell(blanksCell);
//			table.addCell(blanksCell);
//		}
		
		if(contract!=null){
			HashSet<String> hset=new HashSet<String>();
			for(SalesLineItem item:contract.getItems()){
				hset.add(item.getProductName());
			}
			
			ArrayList<String> stringLis=new ArrayList<String>(hset); 
			if(stringLis.size()!=0){
				int size=stringLis.size();
//				int blankRows=9-size; //commented by Ashwini
				
				/*
				 * Date:21-01-2018
				 * @author AShwini
//				 */
				int blankRows;
				if(rowflag){
					 blankRows=4-size;
				}else{
					 blankRows=9-size; 
				}
			
				/*
				 * end by Ashwini
				 */
				
				for(String obj:stringLis){
					Phrase itemPh=new Phrase(obj,font8);
					PdfPCell itemCell = new PdfPCell(itemPh);
					itemCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					table.addCell(itemCell);
					table.addCell(blanksCell);
					table.addCell(blanksCell);
					table.addCell(blanksCell);
				}
				if(blankRows>0){
					for (int i = 0; i < blankRows; i++) {
						table.addCell(blanksCell);
						table.addCell(blanksCell);
						table.addCell(blanksCell);
						table.addCell(blanksCell);
					}
				}
				
			}else{
				for (int i = 0; i <= 8; i++) {
					table.addCell(blanksCell);
					table.addCell(blanksCell);
					table.addCell(blanksCell);
					table.addCell(blanksCell);
				}
			}
			
			/**
			 * End
			 */
		}

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(servicepara);
//			document.add(table);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
	    PdfPCell serviceParaCell = new PdfPCell();
	    PdfPCell tableCell = new PdfPCell();
	    serviceParaCell.setBorder(0);
	    tableCell.setBorder(0);
	    serviceParaCell.addElement(servicepara);
	    tableCell.addElement(table);
	    a5Table1.addCell(serviceParaCell);
		a5Table2.addCell(serviceParaCell);
		a5Table1.addCell(tableCell);
		a5Table2.addCell(tableCell);
	}
	
	
	private void trapCatchesList() {
		
		System.out.println("in side trap catch list");
		
		Phrase trapCatchesPhrase = new Phrase("Trap Catches :",font8bold);
		Paragraph para = new Paragraph(trapCatchesPhrase);
		para.setAlignment(Element.ALIGN_LEFT);
		
		Phrase pestNameHeading = null;
		PdfPCell pestNameHeadingCell= null;
		
		Phrase trapCatches =null;
		PdfPCell trapCatchesCell = null;
		
		Phrase details = null;
		PdfPCell detailsCell = null;
		
		 pestNameHeading = new  Phrase("Pest Name",font8bold);
		 pestNameHeadingCell = new PdfPCell(pestNameHeading);
		 pestNameHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
		 trapCatches = new  Phrase("Trap Catches",font8bold);
		 trapCatchesCell = new PdfPCell(trapCatches);
		 trapCatchesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		 details = new  Phrase("Details",font8bold);
		 detailsCell = new PdfPCell(details);
		 detailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
		if(service.getCatchtrapList().size()!=0)
		{
			if(service.getCatchtrapList().size() <= 5)
			{
				System.out.println("in side trap catch list in side less than 5 ");
				PdfPTable table = new PdfPTable(3);
				table.setWidthPercentage(100f);
				
				table.setSpacingBefore(5f);
				table.setSpacingAfter(10f);
				
				
				table.addCell(pestNameHeadingCell);
				table.addCell(trapCatchesCell);
				table.addCell(detailsCell);
				
				
				for(int i=0; i<service.getCatchtrapList().size();i++)
				{
					Phrase pestName = new Phrase(service.getCatchtrapList().get(i).getPestName(),font8);
					PdfPCell pestNameCell = new PdfPCell(pestName);
					pestNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(pestNameCell);
					
					Phrase catches = new Phrase(service.getCatchtrapList().get(i).getCount()+"",font8);
					PdfPCell catchesCell = new PdfPCell(catches);
					catchesCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(catchesCell);
					
					Phrase detailsValue = new Phrase(" ",font8);
					PdfPCell detailsValueCell = new PdfPCell(detailsValue);
					catchesCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(detailsValueCell);
				}
				
				PdfPCell tableCell = new PdfPCell(table);
				tableCell.setBorder(0);
				
				Phrase blank = new Phrase(" ", font8);
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setBorder(0);
				
				
				PdfPTable parentTable = new PdfPTable(2);
				parentTable.setWidthPercentage(100f);
				
				parentTable.setSpacingBefore(5f);
				
				parentTable.addCell(tableCell);
				parentTable.addCell(blankCell);
				

			    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//				try {
//					document.add(para);
//					document.add(parentTable);
//				} catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			
				/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
				 PdfPCell paraCell = new PdfPCell();
				    PdfPCell parentTableCell = new PdfPCell();
				    paraCell.setBorder(0);
				    parentTableCell.setBorder(0);
				    paraCell.addElement(para);
				    parentTableCell.addElement(parentTable);
				    a5Table1.addCell(paraCell);
					a5Table2.addCell(paraCell);
					a5Table1.addCell(parentTableCell);
					a5Table2.addCell(parentTableCell);
		
			}
			else
			{
				PdfPTable table = new PdfPTable(6);
				table.setWidthPercentage(100f);
				table.setWidthPercentage(100f);
				
				table.setSpacingBefore(5f);
				table.setSpacingAfter(10f);
				
				
				table.addCell(pestNameHeadingCell);
				table.addCell(trapCatchesCell);
				table.addCell(detailsCell);
			
				table.addCell(pestNameHeadingCell);
				table.addCell(trapCatchesCell);
				table.addCell(detailsCell);
				
				for(int i=0; i<service.getCatchtrapList().size();i++)
				{
					Phrase pestName = new Phrase(service.getCatchtrapList().get(i).getPestName(),font8);
					PdfPCell pestNameCell = new PdfPCell(pestName);
					pestNameCell.setBorder(0);
					table.addCell(pestNameCell);
					
					Phrase catches = new Phrase(service.getCatchtrapList().get(i).getCount()+"",font8);
					PdfPCell catchesCell = new PdfPCell(catches);
					catchesCell.setBorder(0);
					table.addCell(catchesCell);
					
					Phrase detailsValue = new Phrase(" ",font8);
					PdfPCell detailsValueCell = new PdfPCell(detailsValue);
					detailsValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					table.addCell(detailsValueCell);
				}
				

			    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//				try {
//					document.add(para);
//					document.add(table);
//				} catch (DocumentException e) {
//					e.printStackTrace();
//				}
				/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
				
				 PdfPCell paraCell = new PdfPCell();
				    PdfPCell tableCell = new PdfPCell();
				    paraCell.setBorder(0);
				    tableCell.setBorder(0);
				    paraCell.addElement(para);
				    tableCell.addElement(table);
				    a5Table1.addCell(paraCell);
					a5Table2.addCell(paraCell);
					a5Table1.addCell(tableCell);
					a5Table2.addCell(tableCell);
		
			
			}
		}
		else
		{
			PdfPTable table = new PdfPTable(6);
			table.setWidthPercentage(100f);
			
			table.setSpacingBefore(5f);
			table.setSpacingAfter(10f);
			table.addCell(pestNameHeadingCell);
			table.addCell(trapCatchesCell);
			table.addCell(detailsCell);
		
			table.addCell(pestNameHeadingCell);
			table.addCell(trapCatchesCell);
			table.addCell(detailsCell);
			
			Phrase detailsValue = new Phrase(" ",font8);
			PdfPCell detailsValueCell = new PdfPCell(detailsValue);
			detailsValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			/*
			 * commented by Ashwini
			 */
			
//			for (int i = 0; i <=48 ; i++) {
//				
//				table.addCell(detailsValueCell);
//			}
//			
			/*
			 * @author Ashwini
			 * Date:21-01-2018
			 */
            if(rowflag){
            	for (int i = 0; i <=24 ; i++) {
    				
    				table.addCell(detailsValueCell);
    			}
            }else{
            	for (int i = 0; i <=48 ; i++) {
 				
    				table.addCell(detailsValueCell);
    			}
   			
            }
			
			
			/*
			 * end by Ashwini
			 */

		    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//			try {
//				document.add(para);
//				document.add(table);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
			 PdfPCell paraCell = new PdfPCell();
			    PdfPCell tableCell = new PdfPCell();
			    paraCell.setBorder(0);
			    tableCell.setBorder(0);
			    paraCell.addElement(para);
			    tableCell.addElement(table);
			    a5Table1.addCell(paraCell);
				a5Table2.addCell(paraCell);
				a5Table1.addCell(tableCell);
				a5Table2.addCell(tableCell);
	
		}
	}
	
	
	private void feedbackFromCustomer() 
	{
		
		
		float[] columnWidths = {2f, 0.1f, 2f};
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPTable obsTable = new PdfPTable(1);
		obsTable.setWidthPercentage(100f);
		
		Phrase obsByNBHC = new Phrase("Observations",font8bold);//Observations by NBHC
		PdfPCell obsByNBHCCell = new PdfPCell(obsByNBHC);
		obsByNBHCCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		obsByNBHCCell.setBorder(0);
		obsTable.addCell(obsByNBHCCell);
		
		Phrase sideBlank = null;
		if(service.getComment()!=null)
		{
			sideBlank = new Phrase(service.getComment(),font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
			sideBlankCell.setBorder(0);
			obsTable.addCell(sideBlankCell);
		}
		else
		{
			sideBlank = new Phrase(" ",font8);
			PdfPCell sideBlankCell = new PdfPCell(sideBlank);
			sideBlankCell.setBorderWidthRight(0);
			sideBlankCell.setBorderWidthLeft(0);
			sideBlankCell.setBorderWidthTop(0);
			
			for (int i = 0; i <= 5; i++) {
				obsTable.addCell(sideBlankCell);
			}
		}
		
		PdfPCell sideBlankCell = new PdfPCell(sideBlank);
		sideBlankCell.setBorder(0);
		obsTable.addCell(sideBlankCell);
		
//		sideBlankCell.setBorderWidthLeft(0);
//		sideBlankCell.setBorderWidthRight(0);
//		sideBlankCell.setBorderWidthTop(0);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
//		obsTable.addCell(sideBlankCell);
		
		
		PdfPCell obsTableCell = new PdfPCell();
		obsTableCell.addElement(obsTable);
		obsTableCell.setBorder(0);
		
		
		
		
		PdfPTable blankTable = new PdfPTable(1);
		blankTable.setWidthPercentage(100f);
		
		Phrase blank =  new Phrase();
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
//		blankTable.addCell(blankcell);
		
		PdfPCell blankTableCell = new PdfPCell();
		blankTableCell.addElement(blankTable);
		blankTableCell.setBorder(0);
		
	
		
		PdfPTable feedbackTable = new PdfPTable(1);
		feedbackTable.setWidthPercentage(100f);
	
//		float columnWidths2[] = {2.5f,7.5f}; 
//		try {
//			feedbackTable.setWidths(columnWidths2);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		
		Phrase customerFeedBack = new Phrase("Customer Feedback",font8bold);
		PdfPCell customerFeedBackCell = new PdfPCell(customerFeedBack);
		customerFeedBackCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		customerFeedBackCell.setBorder(0);
		feedbackTable.addCell(customerFeedBackCell);
		
//		Phrase FeedBack = new Phrase(" ",font8bold);
//		PdfPCell FeedBackCell = new PdfPCell(FeedBack);
//		FeedBackCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		FeedBackCell.setBorder(0);
//		feedbackTable.addCell(FeedBackCell);
		
		Phrase blanks = new Phrase(" ",font8bold);
		PdfPCell blanksCell = new PdfPCell(blanks);
		blanksCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blanksCell.setBorder(0);
//		feedbackTable.addCell(blanksCell);
//		feedbackTable.addCell(blanksCell);
		
		
		// Service Quality Rating (Please circle your choice):
		/** 30-10-2017 sagar sore[]**/
	//	float columnWidths3[] = {3.8f,6.2f}; 
		float columnWidths3[] = {4.0f,6.0f}; 
		PdfPTable qualityRatingTable = new PdfPTable(2); 
		qualityRatingTable.setWidthPercentage(100f);
		
		try {
			qualityRatingTable.setWidths(columnWidths3);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase serviceQualityRating =new Phrase("Service Quality Rating", font8bold);
		PdfPCell serviceQualityRatingCell = new PdfPCell(serviceQualityRating);
		serviceQualityRatingCell.setBorder(0);
		qualityRatingTable.addCell(serviceQualityRatingCell);
		
		
		
		Phrase PleaseCircleYourchoice =new Phrase("(Please circle your choice):", font8);
		PdfPCell PleaseCircleYourchoiceCell = new PdfPCell(PleaseCircleYourchoice);
		PleaseCircleYourchoiceCell.setBorder(0);
		qualityRatingTable.addCell(PleaseCircleYourchoiceCell);
		
		
		PdfPCell qualityRatingTableCell = new PdfPCell(qualityRatingTable);
		qualityRatingTableCell.setBorder(0);
		
		//   rating 
		PdfPTable ratingTabele = new  PdfPTable(1);
		ratingTabele.setWidthPercentage(100f);
	
		/**30-10-2017 sagar sore[]**/
	//	Phrase ratingPhrase =new Phrase("Excellent          Very Good          Good          Average          Poor" ,font8);
		Phrase ratingPhrase =new Phrase("Excellent      Very Good      Good      Average     Poor" ,font8);
		PdfPCell ratingCell = new PdfPCell(ratingPhrase);
		ratingCell.setBorder(0);
		ratingTabele.addCell(ratingCell);
		
		PdfPCell ratingTabeleCell = new PdfPCell(ratingTabele);
		ratingTabeleCell.setBorder(0);
		
		
		//   commenst 
		PdfPTable commentsTable = new  PdfPTable(2);
		commentsTable.setWidthPercentage(100f);
		
		/** 30-10-2017 sagar sore [changing width]**/
		//float mycolumnWidths[] = {2.0f,8.0f}; 
		float mycolumnWidths[] = {3.0f,7.0f}; 
		
		try {
			commentsTable.setWidths(mycolumnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase comentsValue = new Phrase("Comments :",font8);
		PdfPCell comentsValueCell = new PdfPCell(comentsValue);
		comentsValueCell.setBorder(0);
		commentsTable.addCell(comentsValueCell);
		
		Phrase comentsValueBlank = new Phrase(" ",font8);
		PdfPCell comentsValueBlankCell = new PdfPCell(comentsValueBlank);
		comentsValueBlankCell.setBorderWidthRight(0);
		comentsValueBlankCell.setBorderWidthTop(0);
		comentsValueBlankCell.setBorderWidthLeft(0);
		
		commentsTable.addCell(comentsValueBlankCell);
		
		PdfPCell commentsTableCell = new PdfPCell(commentsTable);
		commentsTableCell.setBorder(0);
		
		
				
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.addCell(qualityRatingTableCell);
		parentTable.addCell(ratingTabeleCell);
		parentTable.addCell(blanksCell);
		parentTable.addCell(commentsTableCell);
		parentTable.addCell(comentsValueBlankCell);
		parentTable.addCell(comentsValueBlankCell);
//		parentTable.addCell(comentsValueBlankCell);
		
		PdfPCell parentTableCell = new PdfPCell(parentTable);
//		parentTableCell.setBorderWidthRight(0);
//		parentTableCell.setBorderWidthTop(0);
//		parentTableCell.setBorderWidthBottom(0);
		parentTableCell.setBorder(0);
		
		Phrase customerFeedBackfromService=null;
		PdfPCell customerFeedBackfromServiceCell = null;
		if(service.getCustomerFeedback()!=null && !service.getCustomerFeedback().equals(""))
		{
			customerFeedBackfromService = new Phrase(service.getCustomerFeedback()+" "+service.getRemark(),font8);
			customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
			customerFeedBackfromServiceCell.setBorder(0);
			feedbackTable.addCell(customerFeedBackfromServiceCell);
			feedbackTable.addCell(parentTableCell);
			feedbackTable.addCell(parentTableCell);
		}
		else
		{
//				customerFeedBackfromService = new Phrase(" ",font8);
//				customerFeedBackfromServiceCell = new PdfPCell(customerFeedBackfromService);
//				customerFeedBackfromServiceCell.setBorderWidthRight(0);
//				customerFeedBackfromServiceCell.setBorderWidthLeft(0);
//				customerFeedBackfromServiceCell.setBorderWidthTop(0);
//				
//				
//				PdfPTable blankTableTable = new PdfPTable(1);
//				blankTableTable.setWidthPercentage(100f);
//				for (int i = 0; i < 5; i++) 
//				{
//					blankTableTable.addCell(customerFeedBackfromServiceCell);
//				}
//				PdfPCell blankTableTableCell = new PdfPCell(blankTableTable);
//				blankTableTableCell.setBorderWidthBottom(0);
//				blankTableTableCell.setBorderWidthLeft(0);
//				blankTableTableCell.setBorderWidthRight(0);
//				
//				feedbackTable.addCell(blankTableTableCell);
				feedbackTable.addCell(parentTableCell);
		}
		
		PdfPCell feedbackTableCell = new PdfPCell();
		feedbackTableCell.addElement(feedbackTable);
		feedbackTableCell.setBorder(0);
		
		table.addCell(obsTableCell);
		table.addCell(blankTableCell);
		table.addCell(feedbackTableCell);

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(Chunk.NEWLINE);
//			document.add(table);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/
//		a5Table1.add(Chunk.NEWLINE);
//		a5Table2.add();
	    PdfPCell tableCell = new PdfPCell();
	    tableCell.setBorder(0);
	    tableCell.addElement(table);	   
		a5Table1.addCell(tableCell);
		a5Table2.addCell(tableCell);
		
	}
	
	private void sinatureofCustAndNBHCRepresentative() {
		/**30-10-2017 sagar sore** [added column widths]**/
			float sinatureofCustAndNBHCRepresentative[] = {4.0f,3.0f,3.0f};
		PdfPTable table = new PdfPTable(sinatureofCustAndNBHCRepresentative);
		table.setWidthPercentage(100f);
		
          table.setSpacingBefore(5f);
		table.setSpacingAfter(5f);
		
		Phrase name = new Phrase("Name",font8bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nameCell);
		
		Phrase signature = new Phrase("Signature",font8bold);
		PdfPCell signatureCell = new PdfPCell(signature);
		signatureCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(signatureCell);
		
		Phrase date = new Phrase("Date",font8bold);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(dateCell);
		
		Phrase NBHCrepresentative = new Phrase("NBHC Representative      : "+service.getEmployee(),font8);
		PdfPCell NBHCrepresentativeCell = new PdfPCell(NBHCrepresentative);
		table.addCell(NBHCrepresentativeCell);
		
		Phrase blankThreeSide = new Phrase(" ",font8);
		PdfPCell blankThreeSideCell = new PdfPCell(blankThreeSide);
		table.addCell(blankThreeSideCell);
		table.addCell(blankThreeSideCell);
		
		
		Phrase customerRepresentative = new Phrase("Customer Representative : " +cust.getFullname(),font8);
		PdfPCell customerRepresentativeCell = new PdfPCell(customerRepresentative);
		table.addCell(customerRepresentativeCell);
		
		table.addCell(blankThreeSideCell);
		table.addCell(blankThreeSideCell);

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		/**28-10-2017 sagar sore [to make copy of pdf on half side of page]**/

	    PdfPCell tableCell = new PdfPCell();
	    tableCell.setBorder(0);	  
	    tableCell.addElement(table);
		a5Table1.addCell(tableCell);
		a5Table2.addCell(tableCell);

	}
	
	

	private void createCompanyDetails(){


		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		Phrase reg = new Phrase("Registered Office: ", font10bold);
		PdfPCell regCell = new PdfPCell(reg);
		regCell.setBorder(0);
		regCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(regCell);
		
		
		Phrase name = new Phrase(comp.getBusinessUnitName(), font10bold);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nameCell);

		Phrase hardcoded = new Phrase(
				"Division: Commodity Care and Pest Management", font8bold);
		PdfPCell hardcodedCell = new PdfPCell(hardcoded);
		hardcodedCell.setBorder(0);
		hardcodedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(hardcodedCell);

		// / address *************

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (comp.getAddress() != null) {

			if (!comp.getAddress().getAddrLine2().equals("")) {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (!comp.getAddress().getLandmark().equals("")) {
					custAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getLandmark();
				} else {
					custAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (!comp.getAddress().getLocality().equals("")) {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getLocality()
						+ "," + comp.getAddress().getCity() + "-"
						+ comp.getAddress().getPin() + ","
						+ comp.getAddress().getState()
						+ comp.getAddress().getCountry();

			} else {
				custFullAdd1 = custAdd1 + "," + comp.getAddress().getCity()
						+ "-" + comp.getAddress().getPin() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCountry();
			}
		}

		Phrase address = new Phrase(custFullAdd1, font8);
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		addressCell.setBorder(0);
		table.addCell(addressCell);

		// ends here *******************

		// contact details ************

		String contactInfo = "Tel :" + comp.getCellNumber1();

		if (comp.getFaxNumber() != null) {
			contactInfo = contactInfo + " | Fax :" + comp.getFaxNumber();
		}

		if (comp.getEmail() != null) {
			contactInfo = contactInfo + " | Email :" + comp.getEmail();
		}
		if (comp.getWebsite() != null) {
			contactInfo = contactInfo + " | Website :" + comp.getWebsite();
		}

		Phrase contactDetailsInfo = new Phrase(contactInfo, font8);
		PdfPCell contactDetailsInfoCell = new PdfPCell(contactDetailsInfo);
		contactDetailsInfoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		contactDetailsInfoCell.setBorder(0);
		table.addCell(contactDetailsInfoCell);
		// ends here **********************

	    /** 30-10-2017 sagar sore [commented to show same content on left and right side after vertically partitioned ]**/
//		try {
//			document.add(table);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

			
	}
	
	private void addTablesToParentTable() {
		PdfPCell a5TableCell1 = new PdfPCell(a5Table1);
		a5TableCell1.setBorder(0);
//		a5TableCell1.addElement(a5Table1);//Date 11/12/2017 comment By Jayshree To remaove the space
		a5TableCell1.setPaddingRight(20);//Date 11/12/2017 by Jayshree set the padding to increse the size
		PdfPCell a5TableCell2 = new PdfPCell(a5Table2);
		a5TableCell2.setBorder(0);
//		a5TableCell2.addElement(a5Table2);//Date 11/12/2017 comment By Jayshree To remaove the space
		a5TableCell2.setPaddingLeft(20);//Date 11/12/2017 by Jayshree set the padding to increse the size
		a4Table.addCell(a5TableCell1);
		a4Table.addCell(a5TableCell2);
		try {
			document.add(a4Table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
