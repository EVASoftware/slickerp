package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.purchaseOrderPdf.SalesOrderPdfUpdated;
import com.slicktechnologies.server.addhocprinting.sasha.SalesOrderPdfForSasha;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;

import static com.googlecode.objectify.ObjectifyService.ofy;



@SuppressWarnings("serial")
public class CreateSalesPdfServlet extends HttpServlet {
	
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {

		  String stringid=request.getParameter("Id");
		   stringid=stringid.trim();
		   System.out.println("iddddd === "+stringid);
		   Long count =Long.parseLong(stringid);
		   String stringType=request.getParameter("type");
		   stringType=stringType.trim();
		  
		   String preprintStatus=request.getParameter("preprint");
		   preprintStatus=preprintStatus.trim();
		   String subtype=request.getParameter("subtype");//add by jayshree
		   
		   
		   if(subtype != null && subtype.equalsIgnoreCase("POPDFV1")){
			   
			   SalesOrderPdfForSasha sopdfsasha = new SalesOrderPdfForSasha();
			   sopdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				Document document = sopdfsasha.document;
				PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
				document.open();
				sopdfsasha.setSalesOrder(count);
				sopdfsasha.createPdf();
				document.close();
		   }
		   else if(subtype != null && subtype.equalsIgnoreCase("SalesPdfUpdated")){
			   SalesOrderPdfUpdated sopdfsasha = new SalesOrderPdfUpdated();
			   sopdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				Document document = sopdfsasha.document;
				PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
				document.open();
				sopdfsasha.setSalesOrder(count);
				sopdfsasha.createPdf();
				document.close();
		   }
		   else{

		   SalesOrder so=null;
		   SalesQuotation sq=null;
		   
		   if(stringType.equalsIgnoreCase("so")){
			  so=ofy().load().type(SalesOrder.class).id(count).now();
		   }
		   else{
			   sq=ofy().load().type(SalesQuotation.class).id(count).now();
		   }
	  
	   PdfUpdated pdf=new PdfUpdated();
	   pdf.document=new Document();
	   Document document = pdf.document;
	   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	   
	   if(stringType.equalsIgnoreCase("so")){
		   
		   if(so.getStatus().equals("Cancelled")){
			   writer.setPageEvent(new PdfCancelWatermark());	
		   }else 
		   if(!so.getStatus().equals("Approved")){
			   writer.setPageEvent(new PdfWatermark());	
		   }
	   }
	   
	   if(stringType.equalsIgnoreCase("sq")){
		  
		   if(sq.getStatus().equals("Cancelled")){
			   writer.setPageEvent(new PdfCancelWatermark());	
		   }else 
		   if(!sq.getStatus().equals("Approved")&&!sq.getStatus().equals("Successful")&&!sq.getStatus().equals("UnSuccessful")){
			   writer.setPageEvent(new PdfWatermark());	
		   }
	   }
	   
	   document.open();
	   String type=request.getParameter("type");
	   if(type.contains("sq"))
	   {
		   pdf.getSalesQuotation(count);
	   }
	   
	   if(type.contains("so"))
	   {
		   pdf.getSalesOrder(count);
	   }
	  
	   
	   pdf.createPdf(preprintStatus);
	   document.close();
	  } 
	  }
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }


}
