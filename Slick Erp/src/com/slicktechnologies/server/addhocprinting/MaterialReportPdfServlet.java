package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.PaySlipPrintHelper;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
public class MaterialReportPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 610959093893777155L;

//	public static ArrayList<ProductGroupList> list = new ArrayList<ProductGroupList>();
//	public static HashMap<String, ArrayList<ProductGroupList>> hmap=new HashMap<String, ArrayList<ProductGroupList>>();
	public static HashMap<String, ArrayList<SummaryReport>> hmap=new HashMap<String, ArrayList<SummaryReport>>();
	
	
	@Override
	 protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException 
	{
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.

	  try {
		  
		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  System.out.println("AAAAAAAAAAAAAAAAAAAAAAA          stringid"+stringid);
		  Long count =Long.parseLong(stringid);
		  
			  MaterialReportPdf mrpdf = new MaterialReportPdf();
				
				mrpdf.document = new Document();
				Document document = mrpdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
			  
		   	document.open();
		   	
//	   		mrpdf.setpdfmr(count, stringdate, stringdate1, team);
	   		
//		   	mrpdf.setpdfmr1(count,list);
		   	
			mrpdf.setpdfmr2(count,hmap);
		    mrpdf.createPdf();
	        document.close();
		  
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	}
}
