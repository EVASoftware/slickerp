package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
public class HygeiaInvoicePdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6772445941433824175L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			System.out.println("Vaishnavi +++++++++++++++" + stringid);

			HygeiaInvoicePdf invoicepdf = new HygeiaInvoicePdf();

			invoicepdf.document = new Document();
			Document document = invoicepdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream()); // write

			document.open();

			invoicepdf.setpdfinvoice(count);
			invoicepdf.createPdf();

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
