package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import com.google.appengine.labs.repackaged.com.google.common.collect.Table;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CTC;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class DutySlipPdf {
	
	public Document document;
	
	Invoice invoiceEntity;
	
	private Font font16boldul, font12bold, font8bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold;

	float[] columnWidths = {2.0f, 0.5f, 3.0f,0.5f, 1.2f,0.5f,1.5f};
	float[] columnWidths1 = {1.0f, 2.0f, 7.5f};
	
	
	float[] columnWidths3 = {1f,1f,2f,2f,2f,2f,2f,1.5f};
	float[] columnWidths4 = {1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1f,1.5f};
	
	
	public DutySlipPdf() {
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	}
	
	
	public void setCtc(long count){
		// Load CTC
		invoiceEntity = ofy().load().type(Invoice.class).id(count).now();
	}


	public void createPdf() 
	{
		createHeadingDutySlip();
		createGuestAndVechicleInfo();
		gtTravellingSummury();
		createNoteHeading();
		guestCommentsTable();
		suggestionsOrComments();
	}



	private void createHeadingDutySlip(){
		
		String heading="DUTY SLIP";
		Phrase headingPhrase = new Phrase(heading,font16bold);
		Paragraph headingPara = new Paragraph(headingPhrase);
		headingPara.setAlignment(Element.ALIGN_CENTER);
		
		
		//    spacing for header 
		
		Phrase newline = new Phrase(Chunk.NEWLINE);
		Paragraph newlinePara = new Paragraph(newline);
		
		
		
		
		try 
		{
			document.add(newlinePara);
			document.add(newlinePara);
			document.add(newlinePara);
			document.add(newlinePara);
			
			document.add(headingPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createGuestAndVechicleInfo() {
		
		PdfPTable parentTable = new PdfPTable(7);
		parentTable.setWidthPercentage(100f);
		parentTable.setSpacingAfter(10f);
		try 
		{
			parentTable.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase totalblank = new Phrase(" ",font8);
		PdfPCell totalblankCell = new PdfPCell(totalblank);
		totalblankCell.setBorder(0);
		
		Phrase blank = new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);
		blankCell.setBorderWidthLeft(0);
		
		Phrase dotPhrase = new Phrase(" : ",font8);
		PdfPCell dotCell = new PdfPCell(dotPhrase);
		dotCell.setBorder(0);
		
		Phrase billNo = new Phrase("Bill No",font8bold);
		PdfPCell billNoCell = new PdfPCell(billNo);
		billNoCell.setBorder(0);
		
		Phrase compName = new Phrase("NAME OF THE COMPANY" ,font8);
		PdfPCell compNameCell = new PdfPCell(compName);
		compNameCell.setBorder(0);
		
		Phrase RepDate = new Phrase("REP. DATE",font8);
		PdfPCell RepDateCell = new PdfPCell(RepDate);
		RepDateCell.setBorder(0);
		
		Phrase bookedBy = new Phrase("BOOKED BY",font8);
		PdfPCell bookedByCell = new PdfPCell(bookedBy);
		bookedByCell.setBorder(0);
		
		Phrase vehType = new Phrase("VEH. TYPE",font8);
		PdfPCell vehTypeCell = new PdfPCell(vehType);
		vehTypeCell.setBorder(0);
		
		Phrase userName = new Phrase("USER'S NAME",font8);
		PdfPCell userNameCell = new PdfPCell(userName);
		userNameCell.setBorder(0);
		
		Phrase vehNo = new Phrase("VEH. NO",font8);
		PdfPCell vehNoCell = new PdfPCell(vehNo);
		vehNoCell.setBorder(0);
		
		Phrase guestCellNo = new Phrase("GUEST CELL NO.",font8);
		PdfPCell guestCellNoCell = new PdfPCell(guestCellNo);
		guestCellNoCell.setBorder(0);
		
		Phrase driverName = new Phrase("DRIVER NAME",font8);
		PdfPCell driverNameCell = new PdfPCell(driverName);
		driverNameCell.setBorder(0);
		
		Phrase reportingAddrs = new Phrase("REPORTING ADDRESS",font8);
		PdfPCell reportingAddrsCell = new PdfPCell(reportingAddrs);
		reportingAddrsCell.setBorder(0);
		
		Phrase usedCity = new Phrase("USED CITY",font8);
		PdfPCell usedCityCell = new PdfPCell(usedCity);
		usedCityCell.setBorder(0);
		
		Phrase repTime = new Phrase("REP. TIME",font8);
		PdfPCell repTimeCell = new PdfPCell(repTime);
		repTimeCell.setBorder(0);
		
		Phrase dutyDetails = new Phrase("DUTY DETAILS",font8);
		PdfPCell dutyDetailsCell = new PdfPCell(dutyDetails);
		dutyDetailsCell.setBorder(0);
		
		parentTable.addCell(totalblankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(billNoCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(compNameCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(RepDateCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(bookedByCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(vehTypeCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(userNameCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(vehNoCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(guestCellNoCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(driverNameCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(reportingAddrsCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(usedCityCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(totalblankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(repTimeCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		parentTable.addCell(totalblankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(totalblankCell);
		parentTable.addCell(dutyDetailsCell);
		parentTable.addCell(dotCell);
		parentTable.addCell(blankCell);
		
		try 
		{
			document.add(parentTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	private void gtTravellingSummury() {
	
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		parentTable.setSpacingAfter(10f);
		
	
		//   Code for time and kms information 
		
		
		PdfPTable headingTable = new PdfPTable(8);
		headingTable.setWidthPercentage(100f);
		
		try {
			headingTable.setWidths(columnWidths3);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		
		PdfPTable combineTable = new PdfPTable(1);
		combineTable.setWidthPercentage(100f);
		
		
		
		Phrase bookingRefNo1 = new Phrase("Booking Ref No.",font8);
		PdfPCell bookingRefNo1Cell = new PdfPCell(bookingRefNo1);
		bookingRefNo1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		bookingRefNo1Cell.setBorderWidthBottom(0);
		
		Phrase racordBlank = new Phrase("DATE",font8);
		PdfPCell racordBlankCell = new PdfPCell(racordBlank);
		racordBlankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		racordBlankCell.setBorderWidthBottom(0);
		
		Phrase garageStarting = new Phrase("GARAGE STARTING",font8);
		PdfPCell garageStartingCell = new PdfPCell(garageStarting);
		garageStartingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		garageStartingCell.setBorderWidthBottom(0);
		
		Phrase releasedAt = new Phrase("RELEASED AT",font8bold);
		PdfPCell releasedAtCell = new PdfPCell(releasedAt);
		releasedAtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		releasedAtCell.setBorderWidthBottom(0);
		
		Phrase garageClosing = new Phrase("GARAGE CLOSING",font8);
		PdfPCell garageClosingCell = new PdfPCell(garageClosing);
		garageClosingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		garageClosingCell.setBorderWidthBottom(0);
		
		Phrase total = new Phrase("TOTAL",font8bold);
		PdfPCell totalCell = new PdfPCell(total);
		totalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalCell.setBorderWidthBottom(0);
		
		Phrase extra = new Phrase("EXTRA",font8bold);
		PdfPCell extraCell = new PdfPCell(extra);
		extraCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		extraCell.setBorderWidthBottom(0);
		
		Phrase signature1 = new Phrase("Signature of the GUEST",font8bold);
		PdfPCell signature1Cell = new PdfPCell(signature1);
		signature1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		signature1Cell.setBorderWidthBottom(0);
		
		
		
		headingTable.addCell(bookingRefNo1Cell);
		headingTable.addCell(racordBlankCell);
		headingTable.addCell(garageStartingCell);
		headingTable.addCell(releasedAtCell);
		headingTable.addCell(garageClosingCell);
		headingTable.addCell(totalCell);
		headingTable.addCell(extraCell);
		headingTable.addCell(signature1Cell);
		
		PdfPCell headingTableCell = new PdfPCell();
		headingTableCell.addElement(headingTable);
		combineTable.addCell(headingTableCell);
		
		
		PdfPTable mainTable = new PdfPTable(13);
		mainTable.setWidthPercentage(100f);
		try {
			mainTable.setWidths(columnWidths4);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase bookingRefNo = new Phrase(" ",font8);
		PdfPCell bookingRefNoCell = new PdfPCell(bookingRefNo);
		bookingRefNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		bookingRefNoCell.setBorderWidthTop(0);
		
		Phrase date1 = new Phrase(" ",font8);
		PdfPCell date1Cell = new PdfPCell(date1);
		date1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		date1Cell.setBorderWidthTop(0);
		
		Phrase timePhrase = new Phrase("Time",font8);
		PdfPCell timeCell = new PdfPCell(timePhrase);
		timeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		timeCell.setBorderWidthTop(0);
		
		Phrase kmsPhrase = new Phrase("Kms.",font8);
		PdfPCell kmsCell = new PdfPCell(kmsPhrase);
		kmsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		kmsCell.setBorderWidthTop(0);
		
		Phrase timeBoldPhrase = new Phrase("Time",font8bold);
		PdfPCell timeBoldCell = new PdfPCell(timeBoldPhrase);
		timeBoldCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		timeBoldCell.setBorderWidthTop(0);
		
		Phrase kmsBoldPhrase = new Phrase("Kms.",font8bold);
		PdfPCell kmsBoldCell = new PdfPCell(kmsBoldPhrase);
		kmsBoldCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		kmsBoldCell.setBorderWidthTop(0);
		
		Phrase signature = new Phrase(" ",font8bold);
		PdfPCell signatureCell = new PdfPCell(signature);
		signatureCell.setBorderWidthTop(0);
		
		Phrase blankrecord = new Phrase(" ",font8bold);
		PdfPCell blankrecordCell = new PdfPCell(blankrecord);
		
		
		mainTable.addCell(bookingRefNoCell);
		mainTable.addCell(date1Cell);
		mainTable.addCell(timeCell);
		mainTable.addCell(kmsCell);
		
		mainTable.addCell(timeBoldCell);
		mainTable.addCell(kmsBoldCell);
		
		mainTable.addCell(timeCell);
		mainTable.addCell(kmsCell);
		
		mainTable.addCell(timeCell);
		mainTable.addCell(kmsCell);
		
		mainTable.addCell(timeCell);
		mainTable.addCell(kmsCell);
		
		mainTable.addCell(signatureCell);
		
		
		//******blank added to the table **************
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		
		
		
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		
		
		
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		
		
		
		
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		mainTable.addCell(blankrecordCell);
		
		mainTable.addCell(blankrecordCell);
		
		//**************changes ends here ************
		
		PdfPCell mainTableCell = new PdfPCell();
		mainTableCell.addElement(mainTable);
		mainTableCell.setBorder(0);
		combineTable.addCell(mainTableCell);
		
		
		
		PdfPCell combineTableCell = new PdfPCell(combineTable);
		parentTable.addCell(combineTableCell);
		
		//         code ends here 
		
		// code for Releasing point 
		PdfPTable releasingPointTable = new PdfPTable(1);
		releasingPointTable.setWidthPercentage(100f);
		
		Phrase releasingPointPhrase =new Phrase("RELEASING POINT :",font9);
		PdfPCell releasingPointCell = new PdfPCell(releasingPointPhrase);
		releasingPointCell.setBorder(0);
		releasingPointTable.addCell(releasingPointCell);
		
		PdfPCell releasingPointTableCell = new PdfPCell(releasingPointTable);
		parentTable.addCell(releasingPointTableCell);
		// code for Releasing point ends here  
		
		
		
		// code for Instruction for next day 
		PdfPTable instructionTable = new PdfPTable(1);
		instructionTable.setWidthPercentage(100f);
				
		Phrase instruction =new Phrase("INSTRUCTION IF ANY FOR NEXT DAY USE ",font9);
		PdfPCell instructionCell = new PdfPCell(instruction);
		releasingPointCell.setBorder(0);
		instructionTable.addCell(instructionCell);
				
		PdfPCell instructionTableCell = new PdfPCell(instructionTable);
		parentTable.addCell(instructionTableCell);
		//  code for Instruction for next day ends here  
		
		
		// code for Address info  
		PdfPTable ddressInfoTable = new PdfPTable(3);
		ddressInfoTable.setWidthPercentage(100f);
		try 
		{
			ddressInfoTable.setWidths(columnWidths1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase date =new Phrase("DATE",font9);
		Phrase reportingTime =new Phrase("REPORTING TIME",font9);
		Phrase address =new Phrase("ADDRESS",font9);
		Phrase blank =new Phrase(" ",font9);
		
		
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell reportingTimeCell = new PdfPCell(reportingTime);
		reportingTimeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell blank123Cell = new PdfPCell(blank);
		blank123Cell.setBorderWidthTop(0);
		blank123Cell.setBorderWidthBottom(0);
		
		ddressInfoTable.addCell(dateCell);
		ddressInfoTable.addCell(reportingTimeCell);
		ddressInfoTable.addCell(addressCell);
		
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		ddressInfoTable.addCell(blank123Cell);
		
		
		PdfPCell ddressInfoTableCell = new PdfPCell(ddressInfoTable);
		parentTable.addCell(ddressInfoTableCell);
		//  code for  Address info ends here  
		
		
		
		try
		{
			document.add(parentTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	private void createNoteHeading() {
		
		String note1 = "1. Please mention Mileage & Time at releasing Point.                            "+"Toll & Parking Rs."; 
		String note2 = "2. Time & Kms will be calculated from garage to garage.";
		String note3 = "3. Goods left in the car at owner's risk.";
		String note4 = "4. Toll charge / Parking charges as per actual.";
		
		Phrase heading = new Phrase("Note :",font8bold);
		PdfPCell headingCell = new PdfPCell(heading);
		headingCell.setBorder(0);
		
		Phrase note1Phrase = new Phrase(note1,font8bold);
		PdfPCell note1Cell = new PdfPCell(note1Phrase);
		note1Cell.setBorder(0);
		
		Phrase note2Phrase = new Phrase(note2,font8);
		PdfPCell note2Cell = new PdfPCell(note2Phrase);
		note2Cell.setBorder(0);
		
		Phrase note3Phrase = new Phrase(note3,font8);
		PdfPCell note3Cell = new PdfPCell(note3Phrase);
		note3Cell.setBorder(0);
		
		Phrase note4Phrase = new Phrase(note4,font8);
		PdfPCell note4Cell = new PdfPCell(note4Phrase);
		note4Cell.setBorder(0);
		
		PdfPTable noteTable=new PdfPTable(1);
		noteTable.setWidthPercentage(100f);
		
		noteTable.addCell(headingCell);
		noteTable.addCell(note1Cell);
		noteTable.addCell(note2Cell);
		noteTable.addCell(note3Cell);
		noteTable.addCell(note4Cell);
		
		
		try 
		{
			document.add(noteTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void guestCommentsTable() {
		
		PdfPTable table = new PdfPTable(1); 
		table.setWidthPercentage(100f);
		
		String name = "GUEST COMMENTS";
		
		Phrase namePhrase = new Phrase(name,font16boldul);
		PdfPCell nameCell = new PdfPCell(namePhrase);
		nameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		nameCell.setBorder(0);
	
		
		
		Phrase blank1 = new Phrase(" ",font12);
		PdfPCell blank1Cell = new PdfPCell(blank1);
		blank1Cell.setBorder(0);
		
		PdfPTable guestCommentTable = new PdfPTable(5);
		guestCommentTable.setWidthPercentage(100f);
		
		Phrase Category = new Phrase("", font8);
		PdfPCell CategoryCell = new PdfPCell(Category);
		
		Phrase vechicleQuality = new Phrase("Vechicle Quality", font8);
		PdfPCell vechicleQualityCell = new PdfPCell(vechicleQuality);
		vechicleQualityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase airConditioner = new Phrase("Air - Conditioner", font8);
		PdfPCell airConditionerCell = new PdfPCell(airConditioner);
		airConditionerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase vechicleAmbience = new Phrase("Vechicle Ambience",font8);
		PdfPCell vechicleAmbienceCell = new PdfPCell(vechicleAmbience);
		vechicleAmbienceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase chauffeur = new Phrase("Chauffeur", font8);
		PdfPCell chauffeurCell = new PdfPCell(chauffeur);
		chauffeurCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase blank =new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		
		
		guestCommentTable.addCell(CategoryCell);
		guestCommentTable.addCell(vechicleQualityCell);
		guestCommentTable.addCell(airConditionerCell);
		guestCommentTable.addCell(vechicleAmbienceCell);
		guestCommentTable.addCell(chauffeurCell);
		
		
		//    for Excellent 
		
		Phrase excellent =new Phrase("Excellent",font8bold);
		PdfPCell excellentCell = new PdfPCell(excellent);
		excellentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		guestCommentTable.addCell(excellentCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		
		//	    for Good 
		
		Phrase good =new Phrase("Good",font8bold);
		PdfPCell goodCell = new PdfPCell(good);
		goodCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		guestCommentTable.addCell(goodCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		
		//	    for Satisfactory 
		
		Phrase satisfactory =new Phrase("Satisfactory",font8bold);
		PdfPCell satisfactoryCell = new PdfPCell(satisfactory);
		satisfactoryCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		guestCommentTable.addCell(satisfactoryCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		
		//	    for Poor 
		
		Phrase poor =new Phrase("Poor",font8bold);
		PdfPCell poorCell = new PdfPCell(poor);
		poorCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		guestCommentTable.addCell(poorCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		guestCommentTable.addCell(blankCell);
		
		PdfPCell guestCommentTableCell= new PdfPCell(guestCommentTable);
		
		
		table.addCell(nameCell);
		table.addCell(blank1Cell);
		table.addCell(guestCommentTableCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void suggestionsOrComments() {
		
		String name= "Any Suggestion / Comments :";
		
		Phrase namephrase = new Phrase(name,font8);
		PdfPCell nameCell = new PdfPCell(namephrase);
		nameCell.setBorder(0);
		
		Phrase blank = new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthTop(0);
		
		Phrase blank1 = new Phrase(" ",font8);
		PdfPCell blank1Cell = new PdfPCell(blank1);
		blank1Cell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.setSpacingBefore(10f);
		table.setWidthPercentage(100f);
		table.addCell(nameCell);
		table.addCell(blank1Cell);
		table.addCell(blankCell);
		
		try 
		{
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
