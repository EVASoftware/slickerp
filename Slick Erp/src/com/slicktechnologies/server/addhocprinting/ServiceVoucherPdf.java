package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class ServiceVoucherPdf {

	public Document document;
	Company comp;
	Customer cust;
	Service service;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private Font font16boldul, font12bold, font8bold, font8, font12boldul,
	font12, font16bold, font10, font10bold, font14bold, font14, font9 ,font9bold;
	boolean checkEmailId=false;
	ProcessConfiguration processConfig;
	/** Updated by: Viraj Date:19-04-2019 Description:To show branch details as header and footer **/
	boolean checkBranchdata = false;
	Branch branchDt;
	Logger logger=Logger.getLogger("ServiceVoucherPdf.class");
	/** Ends **/
	
	/***14-12-2019 Deepak Salve added this line for mergin ***/
	boolean authOnLeft=false;
	/***End***/
	
	public ServiceVoucherPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 10);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL);
		font9 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font9bold=new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	}

	public void loadServiceVoucher(long count) {
		// loading service
				service = ofy().load().type(Service.class).id(count).now();

				// loading customer
				if (service.getCompanyId() == null)
					cust = ofy().load().type(Customer.class)
							.filter("count", service.getPersonInfo().getCount())
							.first().now();
				else
					cust = ofy().load().type(Customer.class)
							.filter("count", service.getPersonInfo().getCount())
							.filter("companyId", service.getCompanyId()).first().now();

				// loading company
				if (service.getCompanyId() == null)
					comp = ofy().load().type(Company.class).first().now();
				else
					comp = ofy().load().type(Company.class)
							.filter("companyId", service.getCompanyId()).first().now();
				
				if (service.getCompanyId() != null) {
					
				
					processConfig = ofy().load().type(ProcessConfiguration.class)
							.filter("companyId", service.getCompanyId())
							.filter("processName", "Service")
							.filter("configStatus", true).first().now();
					if (processConfig != null) {
						for (int k = 0; k < processConfig.getProcessList().size(); k++) {
							if (processConfig.getProcessList().get(k).getProcessType()
									.trim().equalsIgnoreCase("ActiveBranchEmailId")
									&& processConfig.getProcessList().get(k).isStatus() == true) {
								checkEmailId = true;
							}
							/**
							 * Updated By: Viraj
							 * Date: 19-04-2019
							 * Description: To show branch details as header and footer
							 */
							if (processConfig.getProcessList().get(k).getProcessType()
									.trim().equalsIgnoreCase("BranchAsCompany")
									&& processConfig.getProcessList().get(k).isStatus() == true){
								checkBranchdata = true;
							}

							}
					}
				}
				/**
				 * Updated By: Viraj
				 * Date: 19-04-2019
				 * Description: To show branch details as header and footer
				 */
//				if(checkBranchdata) {
					branchDt = ofy().load().type(Branch.class).filter("companyId",service.getCompanyId()).filter("buisnessUnitName", service.getBranch()).first().now();
//				}
				/** Ends **/
	}

	public void createPdf() {
		/**
		 * Updated By: Viraj
		 * Date: 19-04-2019
		 * Description: To show branch details as header and footer
		 */
//		if(checkBranchdata) {
//			createBranchHeader(document);
//		} else{
			createheder();
//		}
		createaddress();
		createtimedate();
//		if(checkBranchdata) {
//			createBranchFooter(document);
//		} else {
//			createfootertab();//Commented on 12-1-2023 as option is available for all clients but printing hardcoded branches and ipm footer. As per Nitin sir's instruction removing this hardcoded part
//		}
		/** Ends **/
	}
	
	/**
	 * Updated By: Viraj
	 * Date: 19-04-2019
	 * Description: To show branch details as header and footer
	 */
	private void createBranchFooter(Document doc) {
		DocumentUpload document = branchDt.getUploadFooter();
		logger.log(Level.SEVERE,"document footer url: "+document);
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			logger.log(Level.SEVERE,"image footer url: "+document.getUrl());
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
		//	image2.setAbsolutePosition(40f, 500f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createBranchHeader(Document doc) {
		DocumentUpload document = branchDt.getUploadHeader();
		logger.log(Level.SEVERE,"document header url: "+document);
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			logger.log(Level.SEVERE,"image header url: "+document.getUrl());
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
//			image2.setAbsolutePosition(40f, 500f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Ends **/
	
	private void createfootertab() {
		
		PdfPTable footertab=new PdfPTable(1);
		footertab.setWidthPercentage(100);
		try {
			footertab.setWidths(new float[]{100});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable footertableft=new PdfPTable(1);
		footertableft.setWidthPercentage(100);
		footertableft.setSpacingAfter(5);
		
		Phrase compname=new Phrase(comp.getBusinessUnitName(),font8);
		PdfPCell compnamecell=new PdfPCell(compname);
		compnamecell.setBorder(0);
		compnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableft.addCell(compnamecell);
		
		
		PdfPTable footertableright=new PdfPTable(7);
		footertableright.setWidthPercentage(100);
		footertableright.setSpacingAfter(5);
		try {
			footertableright.setWidths(new float[]{14,14,14,14,14,15,15});
		} catch (DocumentException e) {
			// TODO Auto-0 catch block
			e.printStackTrace();
		}
		
		Phrase  branches=new Phrase("Our Presence in : ",font8bold);
		PdfPCell branchescell=new PdfPCell(branches);
		branchescell.setBorder(0);
		branchescell.setColspan(7);
		branchescell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchescell);
		
		Phrase  branchmum=new Phrase(" Mumbai",font8);
		PdfPCell branchmumcell=new PdfPCell(branchmum);
		branchmumcell.setBorder(0);
		branchmumcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchmumcell);
		
		Phrase  branchnash=new Phrase("Nashik ",font8);
		PdfPCell branchnashcell=new PdfPCell(branchnash);
		branchnashcell.setBorder(0);
		branchnashcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchnashcell);
		
		Phrase  branchindo=new Phrase("Surat",font8);
		PdfPCell branchindocell=new PdfPCell(branchindo);
		branchindocell.setBorder(0);
		branchindocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchindocell);
		
		Phrase  branchahm=new Phrase("Ahmadabad",font8);
		PdfPCell branchahmcell=new PdfPCell(branchahm);
		branchahmcell.setBorder(0);
		branchahmcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchahmcell);
		
		Phrase  branchbhopal=new Phrase("Bhopal ",font8);
		PdfPCell branchbhopalcell=new PdfPCell(branchbhopal);
		branchbhopalcell.setBorder(0);
		branchbhopalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchbhopalcell);
		
		Phrase  branchhaidra=new Phrase("Hyderabad",font8);
		PdfPCell branchhaidracell=new PdfPCell(branchhaidra);
		branchhaidracell.setBorder(0);
		branchhaidracell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchhaidracell);
		
		
		Phrase  branchcoim=new Phrase("Coimbatore",font8);
		PdfPCell branchcoimcell=new PdfPCell(branchcoim);
		branchcoimcell.setBorder(0);
		branchcoimcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchcoimcell);
		
		Phrase  branchpune=new Phrase(" Pune ",font8);
		PdfPCell branchpunecell=new PdfPCell(branchpune);
		branchpunecell.setBorder(0);
		branchpunecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchpunecell);
		
		Phrase  branchnag=new Phrase("Nagpur",font8);
		PdfPCell branchnagcell=new PdfPCell(branchnag);
		branchnagcell.setBorder(0);
		branchnagcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchnagcell);
		
		Phrase  branchvad=new Phrase("Vadodara",font8);
		PdfPCell branchvadcell=new PdfPCell(branchvad);
		branchvadcell.setBorder(0);
		branchvadcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchvadcell);
		
		Phrase  branchind=new Phrase("Indore",font8);
		PdfPCell branchindcell=new PdfPCell(branchind);
		branchindcell.setBorder(0);
		branchindcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchindcell);
		
		
		Phrase  branchraypur=new Phrase("Raipur",font8);
		PdfPCell branchraypurcell=new PdfPCell(branchraypur);
		branchraypurcell.setBorder(0);
		branchraypurcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchraypurcell);
		
		
		Phrase  branchchennai=new Phrase("Chennai",font8);
		PdfPCell branchchennaicell=new PdfPCell(branchchennai);
		branchchennaicell.setBorder(0);
		branchchennaicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchchennaicell);
		
		
		Phrase  branchguj=new Phrase("Goa",font8);
		PdfPCell branchgujcell=new PdfPCell(branchguj);
		branchgujcell.setBorder(0);
		branchgujcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footertableright.addCell(branchgujcell);
		
		
		
//		
//		PdfPCell leftcell=new PdfPCell(footertableft);
//		leftcell.setBorderWidthRight(0);
		
		PdfPCell rightcell=new PdfPCell(footertableright);
		
//		footertab.addCell(leftcell);
		footertab.addCell(rightcell);
		
		try {
			document.add(footertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable footertabnext=new PdfPTable(1);
		footertabnext.setWidthPercentage(100);
		try {
			footertabnext.setWidths(new float[]{100});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase footercap=new Phrase("IPM For Pest Control And Pest Prevention ",font9bold);
		PdfPCell footercapcell=new PdfPCell(footercap);
		footercapcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		footercapcell.setPaddingBottom(4);
		footertabnext.addCell(footercapcell);
		
		try {
			document.add(footertabnext);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createtimedate() {
		
		PdfPTable timetab=new PdfPTable(2);
		timetab.setWidthPercentage(100);
		try {
			timetab.setWidths(new float[]{40,60});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable servicetime=new PdfPTable(3);
		servicetime.setWidthPercentage(100);
		servicetime.setSpacingAfter(5);
		
		try {
			servicetime.setWidths(new float[]{40,30,30});
//			servicetime.setWidths(new float[]{25,25,25,25});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase ontime=new Phrase("On "+fmt.format(service.getServiceDate()),font8);
		PdfPCell ontimecell=new PdfPCell(ontime);
		ontimecell.setBorder(0);
//		ontimecell.setColspan(colspan);
		ontimecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		servicetime.addCell(ontimecell);
		
		Phrase from=new Phrase("from",font8);
		PdfPCell fromcell=new PdfPCell(from);
		fromcell.setBorder(0);
//		fromcell.setColspan(colspan);
		fromcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		servicetime.addCell(fromcell);
		
		Phrase to=new Phrase("to",font8);
		PdfPCell tocell=new PdfPCell(to);
		tocell.setBorder(0);
//		tocell.setColspan(colspan);
		tocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		servicetime.addCell(tocell);
		
		Phrase blank1=new Phrase(" ",font8);
		PdfPCell blankcell1=new PdfPCell(blank1);
		blankcell1.setBorder(0);
		blankcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//		signtab.addCell(blankcell);
//		signtab.addCell(blankcell);
//		signtab.addCell(blankcell);
		
		
		/****14-12-2019 Deepak Salve added this code for Digital Signature ***/
		PdfPCell imageSignCell = null;
		if((comp!=null&&comp.getUploadDigitalSign()!=null)||(branchDt!=null&&branchDt.getUploadDigitalSign()!=null)){
//		if(comp!=null&&comp.getUploadDigitalSign()!=null){
			
			logger.log(Level.SEVERE,"Inside Image Upload :: ");
			DocumentUpload digitalSignUploaded=null;
			if(branchDt!=null&&branchDt.getUploadDigitalSign()!=null){
				logger.log(Level.SEVERE,"Inside Image Upload  from Branch:: ");
			digitalSignUploaded=branchDt.getUploadDigitalSign();
			}else{
			 logger.log(Level.SEVERE,"Inside Image Upload  from company:: ");
			 digitalSignUploaded=comp.getUploadDigitalSign();
			}
			if(digitalSignUploaded!=null){
				logger.log(Level.SEVERE,"Inside Image Upload  from Branch set 1:: ");
			}else{
				logger.log(Level.SEVERE,"Inside Image Upload  from Branch not set 2:: ");
				digitalSignUploaded=comp.getUploadDigitalSign();
			}
			String hostUrl;
			String environment1 = System.getProperty("com.google.appengine.runtime.environment");
			if (environment1.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			    System.out.println(" Url "+hostUrl);
			}
			Image image2=null;
			try {
				System.out.println(" Url server "+hostUrl+" Documents Url"+digitalSignUploaded);
				image2 = Image.getInstance(new URL(hostUrl+digitalSignUploaded.getUrl()));
				image2.scalePercent(32f);
				image2.scaleAbsoluteWidth(120f);
				
				imageSignCell = new PdfPCell(image2);
//				imageSignCell.addElement(image2);
				imageSignCell.setBorder(0);
				logger.log(Level.SEVERE,"authOnLeft value :: "+authOnLeft);
				if (authOnLeft) {
					logger.log(Level.SEVERE,"authOnLeft value :: "+authOnLeft+"/ Inside if ");
					imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
				} else {
					logger.log(Level.SEVERE,"authOnLeft value :: "+authOnLeft+"/ Inside Else ");
					imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				}
//				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				imageSignCell.setFixedHeight(39f);
//				imageSignCell.setRowspan(3);
				imageSignCell.setColspan(3);
				imageSignCell.setPaddingLeft(3);
//				signtab.addCell(imageSignCell);
				logger.log(Level.SEVERE,"imageSignCell value :: "+imageSignCell+"/ Inside Else ");
				if (imageSignCell != null) {
					servicetime.addCell(imageSignCell);
				} else {
					servicetime.addCell(blankcell1);
					servicetime.addCell(blankcell1);
					servicetime.addCell(blankcell1);
					servicetime.addCell(blankcell1);
				}	
				
			} catch (Exception e) {
			}
		}else{
			
			servicetime.addCell(blankcell1);
			servicetime.addCell(blankcell1);
			servicetime.addCell(blankcell1);
			
		}
		/***Ens***/
		
		
		
		
//		Phrase deepak=new Phrase("Deepak",font8);
//		PdfPCell deepakcell=new PdfPCell(deepak);
//		deepakcell.setBorder(0);
////		deepakcell.setColspan(colspan);
//		deepakcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		servicetime.addCell(deepakcell);
		
		
		PdfPTable signtab=new PdfPTable(1);
		signtab.setWidthPercentage(100);
		signtab.setSpacingAfter(5);
		
		Phrase declare=new Phrase("to our entire satisfaction & without any damage or loss of our property ",font8);
		PdfPCell declarecell=new PdfPCell(declare);
		declarecell.setBorder(0);
		declarecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		signtab.addCell(declarecell);
		
		Phrase blank=new Phrase(" ",font8);
		PdfPCell blankcell=new PdfPCell(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		signtab.addCell(blankcell);
		signtab.addCell(blankcell);
		signtab.addCell(blankcell);
		
//		/****14-12-2019 Deepak Salve added this code for Digital Signature ***/
//		PdfPCell imageSignCell = null;
////		if((comp!=null&&comp.getUploadDigitalSign()!=null)||(branchDt!=null&&branchDt.getUploadDigitalSign()!=null)){
//		if(comp!=null&&comp.getUploadDigitalSign()!=null){
//			
//			logger.log(Level.SEVERE,"Inside Image Upload :: ");
//			DocumentUpload digitalSignUploaded=null;
//			if(comp!=null&&comp.getUploadDigitalSign()!=null){
//			digitalSignUploaded =comp.getUploadDigitalSign();
//			}else{
//			 digitalSignUploaded=branchDt.getUploadDigitalSign();
//			}
//			String hostUrl;
//			String environment1 = System.getProperty("com.google.appengine.runtime.environment");
//			if (environment1.equals("Production")) {
//			    String applicationId = System.getProperty("com.google.appengine.application.id");
//			    String version = System.getProperty("com.google.appengine.application.version");
//			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
//			} else {
//			    hostUrl = "http://localhost:8888";
//			    System.out.println(" Url "+hostUrl);
//			}
//			Image image2=null;
//			try {
//				System.out.println(" Url server "+hostUrl+" Documents Url"+digitalSignUploaded);
//				image2 = Image.getInstance(new URL(hostUrl+digitalSignUploaded.getUrl()));
//				image2.scalePercent(32f);
//				image2.scaleAbsoluteWidth(120f);
//				
//				imageSignCell = new PdfPCell(image2);
////				imageSignCell.addElement(image2);
//				imageSignCell.setBorder(0);
//				logger.log(Level.SEVERE,"authOnLeft value :: "+authOnLeft);
//				if (authOnLeft) {
//					logger.log(Level.SEVERE,"authOnLeft value :: "+authOnLeft+"/ Inside if ");
//					imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//					
//				} else {
//					logger.log(Level.SEVERE,"authOnLeft value :: "+authOnLeft+"/ Inside Else ");
//					imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				}
////				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				imageSignCell.setFixedHeight(39f);
//				imageSignCell.setRowspan(3);
////				signtab.addCell(imageSignCell);
//				logger.log(Level.SEVERE,"imageSignCell value :: "+imageSignCell+"/ Inside Else ");
//				if (imageSignCell != null) {
//					servicetime.addCell(imageSignCell);
//				} else {
//					signtab.addCell(blankcell);
//					signtab.addCell(blankcell);
//					signtab.addCell(blankcell);
//					signtab.addCell(blankcell);
//				}	
//				
//			} catch (Exception e) {
//			}
//		}else{
//			
//			signtab.addCell(blankcell);
//			signtab.addCell(blankcell);
//			signtab.addCell(blankcell);
//			
//		}
//		/***Ens***/
		
		
		Phrase sign=new Phrase("Name & Signature of the Occupant / Client with Seal",font8);
		PdfPCell signcell=new PdfPCell(sign);
		signcell.setBorder(0);
		signcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		signtab.addCell(signcell);
		
		PdfPCell leftcell=new PdfPCell(servicetime);
		leftcell.setBorderWidthRight(0);
		
		PdfPCell rightcell=new PdfPCell(signtab);
		rightcell.setBorderWidthLeft(0);
		
		timetab.addCell(leftcell);
		timetab.addCell(rightcell);
		
		try {
			document.add(timetab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createaddress() {
		PdfPTable addtab=new PdfPTable(2);
		addtab.setWidthPercentage(100);
		try {
			addtab.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable serviceadd=new PdfPTable(1);
		serviceadd.setWidthPercentage(100);
		serviceadd.setSpacingAfter(5);
		
		Phrase serviceph=new Phrase("This is to certify that Pest Control Service was provided at the premises of ",font8bold);
		PdfPCell servicecell=new PdfPCell(serviceph);
		servicecell.setBorder(0);
		servicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceadd.addCell(servicecell);
		
		Phrase custname=new Phrase(service.getCustomerName(),font8);
		PdfPCell custnamecell=new PdfPCell(custname);
		custnamecell.setBorder(0);
		custnamecell.setPaddingTop(8);
		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceadd.addCell(custnamecell);
		
		
		Phrase custadd=new Phrase(service.getAddress().getCompleteAddress(),font8);
		PdfPCell custaddcell=new PdfPCell(custadd);
		custaddcell.setBorder(0);
		custaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceadd.addCell(custaddcell);
		
		PdfPTable serviceprod=new PdfPTable(1);
		serviceprod.setWidthPercentage(100);
		serviceprod.setSpacingAfter(5);
		
		Phrase serviceprovide=new Phrase("Service was provided for",font8bold);
		PdfPCell serviceprovidecell=new PdfPCell(serviceprovide);
		serviceprovidecell.setBorder(0);
		serviceprovidecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceprod.addCell(serviceprovidecell);
		
		Phrase serviceprodname=new Phrase(service.getProductName(),font8);
		PdfPCell serviceprodcell=new PdfPCell(serviceprodname);
		serviceprodcell.setBorder(0);
		serviceprodcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceprodcell.setPaddingTop(5);
		serviceprod.addCell(serviceprodcell);
		
		PdfPCell addcell=new PdfPCell(serviceadd);
		/**
		 * @author Vijay Date :- 11-06-2022
		 * Des :- IMP and Fumigation reported service address not showing on voucher
		 * due to fixedHeiht so i have commented below code fixed height
		 */
//		addcell.setFixedHeight(90);
		PdfPCell prodcell=new PdfPCell(serviceprod);
//		prodcell.setFixedHeight(90);
		
		
		addtab.addCell(addcell);
		addtab.addCell(prodcell);
		
		
		Phrase serby = null;
		String remark="";
		if(service.getRemark()!=null&&!service.getRemark().equals("")) {
		remark=service.getRemark();
		if (remark.length() > 500) {
			System.out.println("to check the lenth more 500"
					+ remark.substring(0, 500));
			serby = new Phrase("By : "+""+remark.substring(0,
					500), font8);
		} else {
			System.out.println("to check the lenth less 500"
					+ remark);
			serby = new Phrase("By : "+""+remark,font8);
		}
		}else if(service.getEmployee()!=null) { //12-1-2023
			serby = new Phrase("By : "+service.getEmployee(), font8);
		}

		
		PdfPCell serbycell=new PdfPCell(serby);
		serbycell.setColspan(2);
		serbycell.setFixedHeight(25);
		serbycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		addtab.addCell(serbycell);
		
		
		try {
			document.add(addtab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

	private void createheder() {
		
		PdfPTable hedertab=new PdfPTable(3);
		hedertab.setWidthPercentage(100);
		try {
			hedertab.setWidths(new float[]{20,60,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		//Date 16/11/2017 By Jayshree to add the logo
			/**
			 * Updated By: Viraj
			 * Date: 27-06-2019
			 * Description: To show logo from branch when process config is on
			 */
				DocumentUpload logodocument ;
				if(checkBranchdata) {
					logodocument = branchDt.getLogo();
				} else {
					logodocument =comp.getLogo();
				}
			 /** Ends **/
				//patch
				String hostUrl;
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
				}
				PdfPCell imageSignCell = null;
				Image image2=null;
				try {
					image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
					image2.scalePercent(20f);
//					image2.setAbsolutePosition(40f,765f);	
//					doc.add(image2);
					
					imageSignCell = new PdfPCell();
					imageSignCell.addElement(image2);
					imageSignCell.setImage(image2);
					imageSignCell.setBorder(0);
					imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					imageSignCell.setFixedHeight(20);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
//				Image image1=null;
//				try
//				{
//				 image1=Image.getInstance("images/ipclogo4.jpg");
//				image1.scalePercent(20f);
////				image1.setAbsolutePosition(40f,765f);	
////				doc.add(image1);
//				
//				
//				
//				
//				
//				imageSignCell=new PdfPCell();
////				imageSignCell.addElement(image1);
//				imageSignCell.setImage(image1);
//				imageSignCell.setFixedHeight(20);
//				imageSignCell.setBorder(0);
//				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				}
//				catch(Exception e)
//				{
//					e.printStackTrace();
//				}
				
				PdfPTable logoTable = new PdfPTable(2);
				logoTable.setSpacingAfter(5);
				logoTable.setWidthPercentage(100f);
				try {
					logoTable.setWidths(new float[]{8,92});
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				
				Phrase blank =new Phrase(" ");
				PdfPCell blankCell=new PdfPCell(blank);
				blankCell.setBorder(0);
				if(imageSignCell != null)
				{
					logoTable.addCell(blankCell);
					logoTable.addCell(imageSignCell);
				}
				else
				{
					logoTable.addCell(blankCell);
					logoTable.addCell(blankCell);
				}
				
				//Logo tab complet
				
				
				PdfPTable headTab = new PdfPTable(1);
				headTab.setWidthPercentage(100f);
				headTab.setSpacingAfter(5);
				/**
				 * Updated By: Viraj
				 * Date: 06-07-2019
				 * Description: To show branch details when process config is on
				 */
				String businessunit=null;
				if(checkBranchdata) {
					businessunit=branchDt.getBusinessUnitName();
				} else {
					businessunit=comp.getBusinessUnitName();
				}
				
				Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
						font14bold);
				PdfPCell companyNameCell = new PdfPCell(companyNameph);
				companyNameCell.setBorder(0);
				companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				headTab.addCell(companyNameCell);
				
				/**
				 * Updated By: Viraj
				 * Date: 06-07-2019
				 * Description: To show branch details when process config is on
				 */
				String address = "";
				if(checkBranchdata) {
					address=branchDt.getAddress().getCompleteAddress();
				} else {
					address=comp.getAddress().getCompleteAddress();
				}
				
				Phrase companyAddph = new Phrase(address, font9);
				/** Ends **/
				PdfPCell companyAddCell = new PdfPCell(companyAddph);
				companyAddCell.setBorder(0);
				companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				headTab.addCell(companyAddCell);

				

				String contactinfo = "";
				/**
				 * Updated By: Viraj
				 * Date: 06-07-2019
				 * Description: To show branch details when process config is on
				 */
				if(checkBranchdata) {
					if (branchDt.getCellNumber1() != null && branchDt.getCellNumber1() != 0) {
						System.out.println("pn11");
						contactinfo = branchDt.getBranchCode() + branchDt.getCellNumber1() + "";
					}
					if (branchDt.getCellNumber2() != null && branchDt.getCellNumber2() != 0) {
						if (!contactinfo.trim().isEmpty()) {
							contactinfo = contactinfo + " / " + branchDt.getBranchCode()
									+ branchDt.getCellNumber2() + "";
						} else {
							contactinfo = branchDt.getBranchCode() + branchDt.getCellNumber2()
									+ "";
						}
						System.out.println("pn33" + contactinfo);
					}
					if (branchDt.getLandline() != 0 && branchDt.getLandline() != null) {
						if (!contactinfo.trim().isEmpty()) {
							contactinfo = contactinfo + " / " + branchDt.getBranchCode()
									+ branchDt.getLandline() + "";
						} else {
							contactinfo = branchDt.getBranchCode() + branchDt.getLandline() + "";
						}
						System.out.println("pn44" + contactinfo);
					}
				} else {
					if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
						System.out.println("pn11");
						contactinfo = comp.getCountryCode() + comp.getCellNumber1() + "";
					}
					if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
						if (!contactinfo.trim().isEmpty()) {
							contactinfo = contactinfo + " / " + comp.getCountryCode()
									+ comp.getCellNumber2() + "";
						} else {
							contactinfo = comp.getCountryCode() + comp.getCellNumber2()
									+ "";
						}
						System.out.println("pn33" + contactinfo);
					}
					if (comp.getLandline() != 0 && comp.getLandline() != null) {
						if (!contactinfo.trim().isEmpty()) {
							contactinfo = contactinfo + " / " + comp.getStateCode()
									+ comp.getLandline() + "";
						} else {
							contactinfo = comp.getStateCode() + comp.getLandline() + "";
						}
						System.out.println("pn44" + contactinfo);
					}
				}
				/** Ends **/
				Phrase companymobph = new Phrase("Phone :  "+contactinfo,
						font9);
				PdfPCell companymobCell = new PdfPCell(companymobph);
				companymobCell.setBorder(0);
				companymobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				headTab.addCell(companymobCell);
				
				
				String website="";
				String emailid="";
				ServerAppUtility serverApp = new ServerAppUtility();
				if(comp.getWebsite()==null || comp.getWebsite().equals(""))
				{
					website="";
				}
				else
				{
					
					website="Website : "+comp.getWebsite();
				}
				
//				1if(comp.getEmail()==null|| comp.getEmail().equals(""))
//				{
//					
//					emailid="";
//				}
//				else
//				{
//					emailid="E-Mail  : " + comp.getEmail();
//				}
				/**
				 * Updated By: Viraj
				 * Date: 06-07-2019
				 * Description: To show branch details when process config is on
				 */
				if(checkBranchdata) {
					emailid = branchDt.getEmail();
				} else {
					if (checkEmailId == true) {
						emailid = serverApp.getBranchEmail(comp,
								service.getBranch());
						System.out.println("server method " + emailid);

					} else {
						emailid = serverApp.getCompanyEmail(comp.getCompanyId());
						System.out.println("server method 22" + emailid);
					}
				}
				
				Phrase companyemailph = new Phrase("E-Mail : "+emailid, font9);
				PdfPCell companyemailCell = new PdfPCell(companyemailph);
				companyemailCell.setBorder(0);
				companyemailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				headTab.addCell(companyemailCell);
				
				Phrase companyWebph = new Phrase(website, font9);
				PdfPCell companyWebCell = new PdfPCell(companyWebph);
				companyWebCell.setBorder(0);
				companyWebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				headTab.addCell(companyWebCell);
				
			
				PdfPTable titletab = new PdfPTable(1);
				titletab.setWidthPercentage(100f);
				titletab.setSpacingAfter(5);
				
				Phrase title =new Phrase("SERVICE VOUCHER    ",font9bold);
				PdfPCell titlecell=new PdfPCell(title);
				titlecell.setBorder(0);
				titlecell.setPaddingRight(3);
				titlecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				titletab.addCell(titlecell);
				
				PdfPCell logocell=new PdfPCell(logoTable);
				logocell.setBorderWidthRight(0);
				PdfPCell headerCell=new PdfPCell(headTab);
				headerCell.setBorderWidthLeft(0);
				headerCell.setBorderWidthRight(0);
				PdfPCell titlcell=new PdfPCell(titletab);
				titlcell.setBorderWidthLeft(0);
				
				hedertab.addCell(logocell);
				hedertab.addCell(headerCell);
				hedertab.addCell(titlcell);
				
				try {
					document.add(hedertab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				PdfPTable headertab2=new PdfPTable(2);
				headertab2.setWidthPercentage(100);
				try {
					headertab2.setWidths(new float[]{50,50});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				Phrase custid=new Phrase("Customer Code : "+service.getCustomerId(),font8);
				PdfPCell custidcell=new PdfPCell(custid);
				custidcell.setBorderWidthRight(0);
				custidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				custidcell.setPaddingBottom(5);
				headertab2.addCell(custidcell);
				
				Phrase date=new Phrase("Date : "+fmt.format(service.getServiceDate()),font8);
				PdfPCell datecell=new PdfPCell(date);
				datecell.setBorderWidthLeft(0);
				datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				datecell.setPaddingBottom(5);
				headertab2.addCell(datecell);
				
				
				try {
					document.add(headertab2);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
	}

}

