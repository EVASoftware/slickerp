package com.slicktechnologies.server.addhocprinting.intech;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.addhocprinting.intech.IntechPestControlPdf;
import com.slicktechnologies.server.addhocprinting.nbhc.ServiceSchedulePdf;
import com.slicktechnologies.server.utility.ServerAppUtility;

public class IntechPestControlPdfServlet extends HttpServlet{

	/**Date 25/10/2017 
	 * By jayshree 
	 * Description to called the intech Pest Control technician pdf 
	 */
	private static final long serialVersionUID = 4231399476538520743L;
	
//	 public class Rotate extends PdfPageEventHelper {
//	        protected PdfNumber rotation = PdfPage.PORTRAIT;
//	        public void setRotation(PdfNumber rotation) {
//	            this.rotation = rotation;
//	        }
//	        public void onEndPage(PdfWriter writer, Document document) {
//	            writer.addPageDictEntry(PdfName.ROTATE, rotation);
//	        }
//	    }
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf"); 
		System.out.println("intech pdf222 ");
		try {
			
			System.out.println("intech pdf");
			
//			String stringId = req.getParameter("Id");
			
			String team=req.getParameter("techTeam");
			
			String technicianId=req.getParameter("technicianId");
			Integer technicianid = 0;
			try{
				technicianid =Integer.parseInt(technicianId);
			}catch(Exception e){
				technicianid = 0;
			}
			
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

			/**
			 * Date 21-07-2018 By Vijay
			 */
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			df.setTimeZone(TimeZone.getTimeZone("IST"));
			/**
			 * ends here
			 */
			String frmDate1=req.getParameter("fromDate");
			Date fromDate=null;
			try {
				fromDate = df.parse(frmDate1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			/**
			 * Date 21-07-2018 By Vijay
			 * Des :- for From Date time setting to fetch database records properly
			 */
			Calendar cal = Calendar.getInstance();
			cal.setTime(fromDate);
			cal.add(Calendar.DATE, 0);
			Date frmDate = null;
			
			try {
				frmDate=df.parse(df.format(cal.getTime()));
				cal.set(Calendar.HOUR_OF_DAY,00);
				cal.set(Calendar.MINUTE,00);
				cal.set(Calendar.SECOND,00);
				cal.set(Calendar.MILLISECOND,000);
				frmDate=cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/**
			 * ends here
			 */
			
			String toDate1=req.getParameter("toDate");
			Date toDate=null;
			try {
				toDate = df.parse(toDate1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			/**
			 * Date 21-07-2018 By Vijay
			 * Des :- for To Date time setting to fetch database records properly
			 */
			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(toDate);
			cal1.add(Calendar.DATE, 0);
			Date ToDate = null;
			
			try {
				ToDate=df.parse(df.format(cal1.getTime()));
				cal1.set(Calendar.HOUR_OF_DAY,23);
				cal1.set(Calendar.MINUTE,59);
				cal1.set(Calendar.SECOND,59);
				cal1.set(Calendar.MILLISECOND,999);
				ToDate=cal1.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			/**
			 * ends 
			 */
			
			String companyid=req.getParameter("companyId");
			System.out.println("companyid333"+companyid);
			Long companyId=Long.parseLong(companyid);
			 String preprintStatus=req.getParameter("preprint");
			   preprintStatus=preprintStatus.trim();
			   
			 String status="";
			 try {
				 status=req.getParameter("status");
			 }catch(Exception e) {
				 
			 }
			 
			
//			stringId = stringId.trim();
//			Long count = Long.parseLong(stringId);
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "DateTechnicianWiseServicePopup" , companyId)){
				ServiceSchedulePdf serviceSchedulePdf = new ServiceSchedulePdf();
				serviceSchedulePdf.document = new Document(PageSize.A4.rotate());
				Document document = serviceSchedulePdf.document;
				
				
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				serviceSchedulePdf.loadData(team,technicianid,frmDate, ToDate,companyId,status);

				serviceSchedulePdf.createpdf();
				document.close();
			}else{
			IntechPestControlPdf intechpdf = new IntechPestControlPdf();
			intechpdf.document = new Document(PageSize.A4.rotate());
			Document document = intechpdf.document;
			
			
			PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
			document.open();
			intechpdf.loadIntech(team,technicianid,frmDate, ToDate,companyId);

			intechpdf.createpdf(preprintStatus);
			document.close();
			}
		}

		catch (DocumentException e) {
			e.printStackTrace();
		}
}
}