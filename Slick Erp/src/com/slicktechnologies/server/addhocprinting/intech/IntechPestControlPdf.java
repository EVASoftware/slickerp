package com.slicktechnologies.server.addhocprinting.intech;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

import com.google.apphosting.api.ApiProxy.LogRecord.Level;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocprinting.ServiceGSTInvoice;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Date 25/10/2017 By jayshree Description to print the technician schedule pdf
 * for Intech pest Control
 * 
 *
 */
public class IntechPestControlPdf {
	Logger logger=Logger.getLogger("IntechPestControlPdf.class");
	public Document document;
	HashSet<Integer> invoiceIdSet;

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12Wbold, font12boldul, font10boldul, font12, font16bold,
			font10, font10bold, font10ul, font9boldul, font14bold, font9,
			font7, font7bold, font9red, font9boldred, font12boldred, font23;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a zzz");
	SimpleDateFormat sdf;
	List<ServiceProject> serviceprolist;
	int tecnicianId;
	String technicianTeam;
	Date fromDATE;
	Date toDATE;
	int servicesrn;
	Company comp;
	Invoice invoiceentity;
	

	public IntechPestControlPdf() {
		super();

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL
				| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,
				BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,
				BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	
		dateTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	}

	/**
	 * Date 25/10/2017 By jayshree Description to this mthod is used to load the
	 * data
	 */
	public void loadIntech(String team, Integer technicianid, Date fromDate,
			Date toDate, long companyId) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		tecnicianId = technicianid;
		technicianTeam = team;
		fromDATE = fromDate;
		toDATE = toDate;
		System.out.println("team" + team);
		System.out.println("technician id" + technicianid);

//		if (!team.equals("")) {
//			System.out.println("team");
//			serviceprolist = ofy().load().type(ServiceProject.class)
//					.filter("companyId", companyId)
//					.filter("serviceDate >=", fromDate)
//					.filter("serviceDate <=", toDate)
//					.filter("technicians.fullName", team).list();
//			System.out.println("serviceprolist size for team =="
//					+ serviceprolist.size());
//			System.out.println("technician full name");
//		}
		 if (technicianid != 0) {

			serviceprolist = ofy().load().type(ServiceProject.class)
					.filter("companyId", companyId)
					.filter("serviceDate >=", fromDate)
					.filter("serviceDate <=", toDate)
					.filter("technicians.empCount", technicianid).list();
			System.out
					.println("serviceprolist size ==" + serviceprolist.size());
			System.out.println("companyid inside if" + companyId);
			System.out.println("frmdate inside if" + fromDate);
			System.out.println("todate inside if" + toDate);
			System.out.println("technician id if" + technicianid);
		}

		comp = ofy().load().type(Company.class).filter("companyId", companyId)
				.first().now();

		

	}

	public void createpdf(String printStatus) {
		invoiceIdSet = new HashSet<Integer>();
		createHeaderTable();
		createTitleTable();
		createSortedservices();
		
		createfooterTab();
//		createRemainingservicetable();
		loadInvoice(printStatus);

	}

	

	private void loadInvoice(String prePrintStatus) {
		// TODO Auto-generated method stub
		logger.log(java.util.logging.Level.SEVERE, "prePrintStatus value "+prePrintStatus);
		if(invoiceIdSet.size()!=0){
			List<Invoice> invoiceList = ofy().load().type(Invoice.class)
					.filter("companyId", comp.getCompanyId())
					.filter("count IN", invoiceIdSet).list();
			System.out.println("invoiceListsize==" + invoiceList.size());
		
		
		

		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Invoice invoice : invoiceList) {

			
			ServiceGSTInvoice invpdf = new ServiceGSTInvoice();
			invpdf.document = new Document();
			document.setPageSize(PageSize.A4);
			document.newPage();
			document.setPageSize(PageSize.A4);
			invpdf.document = this.document;
			invpdf.setInvoice(invoice.getId());
			invpdf.createPdf(prePrintStatus);

		}
		
		}
	}

	private void createHeaderTable() {
		/**
		 * Date 25/10/2017 By jayshree Description to this mthod is used to
		 * print the header section part of the pdf
		 */

		PdfPTable headerTab = new PdfPTable(1);
		headerTab.setWidthPercentage(100);
		headerTab.setSpacingAfter(10);

		Phrase comName = new Phrase(comp.getBusinessUnitName(), font12bold);
		PdfPCell ComNameCell = new PdfPCell(comName);
		ComNameCell.setBorder(0);
		ComNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTab.addCell(ComNameCell);

		try {
			document.add(headerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable headerTab2 = new PdfPTable(12);
		headerTab2.setWidthPercentage(100);

		try {
			headerTab2.setWidths(new float[] { 12, 13, 7, 6, 10, 10, 8, 8, 7, 8,
					6, 5 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String technicianName = null;

		for (int i = 0; i < serviceprolist.size(); i++) {

			for (int j = 0; j < serviceprolist.get(i).getTechnicians().size(); j++) {

				if (tecnicianId == serviceprolist.get(i).getTechnicians()
						.get(j).getEmpCount()) {
					technicianName = serviceprolist.get(i).getTechnicians()
							.get(j).getFullName();
				}
			}

		}

		Phrase TechName = new Phrase("Technician's Name :", font9bold);
		PdfPCell techNameCell = new PdfPCell(TechName);
		techNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		techNameCell.setBorder(0);
		headerTab2.addCell(techNameCell);// c1

		Phrase TechNameVal = new Phrase(technicianName, font9);
		PdfPCell techNameValCell = new PdfPCell(TechNameVal);
		techNameValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		techNameValCell.setBorder(0);
		headerTab2.addCell(techNameValCell);// c2

		Phrase teamPh = new Phrase(" ", font9bold);
		PdfPCell teamCell = new PdfPCell(teamPh);
		teamCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		teamCell.setBorder(0);
		headerTab2.addCell(teamCell);// c3

		Phrase teamValPh = new Phrase(technicianTeam, font9);
		PdfPCell teamValCell = new PdfPCell(teamValPh);
		teamValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		teamValCell.setBorder(0);
		headerTab2.addCell(teamValCell);// c4

		// Phrase BlankPh = new Phrase(" ", font9);
		// PdfPCell BlankCell = new PdfPCell(BlankPh);
		// BlankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// BlankCell.setBorder(0);
		// BlankCell.setRowspan(2);
		// headerTab2.addCell(BlankCell);

		Phrase intimeph = new Phrase("IN-      ", font8bold);
		PdfPCell intimeCell = new PdfPCell(intimeph);
		intimeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		intimeCell.setBorder(0);
		headerTab2.addCell(intimeCell);// c5

		Phrase intimeph22 = new Phrase("Out-      ", font8bold);
		PdfPCell intimeCell22 = new PdfPCell(intimeph22);
		intimeCell22.setHorizontalAlignment(Element.ALIGN_LEFT);
		intimeCell22.setBorder(0);
		headerTab2.addCell(intimeCell22);// c6

		Phrase fromeDatePh = new Phrase("From Date :", font9bold);
		PdfPCell fromdateCell = new PdfPCell(fromeDatePh);
		fromdateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		fromdateCell.setBorder(0);
		headerTab2.addCell(fromdateCell);// c7

		Phrase fromeDateValPh = new Phrase(sdf.format(fromDATE) + "", font9);
		PdfPCell fromdateValCell = new PdfPCell(fromeDateValPh);
		fromdateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		fromdateValCell.setBorder(0);
		headerTab2.addCell(fromdateValCell);// c8

		Phrase endDatePh = new Phrase("To Date :", font9bold);
		PdfPCell endDateCell = new PdfPCell(endDatePh);
		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		endDateCell.setBorder(0);
		headerTab2.addCell(endDateCell);

		Phrase endDateValPh = new Phrase(sdf.format(toDATE) + "", font9);
		PdfPCell endDateValCell = new PdfPCell(endDateValPh);
		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		endDateValCell.setBorder(0);
		headerTab2.addCell(endDateValCell);

		Phrase pagenoPh = new Phrase("Page No :", font9bold);
		PdfPCell pagenoCell = new PdfPCell(pagenoPh);
		pagenoCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pagenoCell.setBorder(0);
		headerTab2.addCell(pagenoCell);// c9

		
		Phrase pagenoValPh = new Phrase((document.getPageNumber() + 1) + "",
				font9);
		PdfPCell pagenoValCell = new PdfPCell(pagenoValPh);
		pagenoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pagenoValCell.setBorder(0);
		headerTab2.addCell(pagenoValCell);// c
		try {
			document.add(headerTab2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createTitleTable() {
		PdfPTable titleTab = new PdfPTable(13);
		titleTab.setWidthPercentage(100);
		titleTab.setSpacingBefore(5);
		try {
			titleTab.setWidths(new float[] { 4, 6, 5, 17, 12, 9,8, 7, 7, 7, 7,5,8});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase Srph = new Phrase("Sr.No", font8bold);
		PdfPCell srCell = new PdfPCell(Srph);
		srCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(srCell);// c1
		
		Phrase dateph = new Phrase("Date", font8bold);
		PdfPCell DateCell = new PdfPCell(dateph);
		DateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titleTab.addCell(DateCell);// c1

		Phrase timeph = new Phrase("Time", font8bold);
		PdfPCell timeCell = new PdfPCell(timeph);
		timeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// timeCell.setRowspan(2);
		titleTab.addCell(timeCell);// c2

		Phrase custNameph = new Phrase("Customer Name/Address", font8bold);
		PdfPCell custNameCell = new PdfPCell(custNameph);
		custNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// custNameCell.setRowspan(2);
		titleTab.addCell(custNameCell);// c3

		// Phrase mobph = new Phrase("Cell Number", font8bold);
		// PdfPCell mobCell = new PdfPCell(mobph);
		// mobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// // mobCell.setRowspan(2);
		// titleTab.addCell(mobCell);// c4

		Phrase serviceNameph = new Phrase("Service Name", font8bold);
		PdfPCell serviceNameCell = new PdfPCell(serviceNameph);
		serviceNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// mobCell.setRowspan(2);
		titleTab.addCell(serviceNameCell);// c4

		Phrase chemicalph = new Phrase("Material Issued/Used Qty", font8bold);
		PdfPCell chemicalCell = new PdfPCell(chemicalph);
		chemicalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// chemicalCell.setRowspan(2);
		titleTab.addCell(chemicalCell);// c5

		Phrase technameph = new Phrase("Technician Name", font8bold);
		PdfPCell technameCell = new PdfPCell(technameph);
		technameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// technameCell.setRowspan(2);
		titleTab.addCell(technameCell);// c6

		Phrase valofContractph = new Phrase("Value Of Contract", font8bold);
		PdfPCell valofContractCell = new PdfPCell(valofContractph);
		valofContractCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// valofContractCell.setRowspan(2);
		titleTab.addCell(valofContractCell);// c7

		Phrase revAmtph = new Phrase("Received Amount", font8bold);
		PdfPCell revAmtCell = new PdfPCell(revAmtph);
		revAmtCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// revAmtCell.setRowspan(2);
		titleTab.addCell(revAmtCell);// c8

		Phrase revsignph = new Phrase("Receiver Signature ", font8bold);
		PdfPCell revsignCell = new PdfPCell(revsignph);
		revsignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// revsignCell.setRowspan(2);
		titleTab.addCell(revsignCell);// c9
		
		Phrase otherph = new Phrase("Remark ", font8bold);				//Updated by:Viraj Date:26-12-2018 Description:Changed others to remark 
		PdfPCell otherCell = new PdfPCell(otherph);
		otherCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// revsignCell.setRowspan(2);
		titleTab.addCell(otherCell);// c9
		
		
		Phrase inOutph = new Phrase("IN/OUT Time ", font8bold);
		PdfPCell inoutCell = new PdfPCell(inOutph);
		inoutCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// revsignCell.setRowspan(2);
		titleTab.addCell(inoutCell);// c9
		
		
		Phrase custfeedph = new Phrase("Customer Feedback ", font8bold);
		PdfPCell custfeedCell = new PdfPCell(custfeedph);
		custfeedCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// revsignCell.setRowspan(2);
		titleTab.addCell(custfeedCell);// c9

		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	//Date 16/11/2017
	//By jayshree to sort the date and time changes are made
	private void createSortedservices() 
	{
		
		HashSet<Integer> serviceId=new HashSet<Integer>();
		for (int i = 0; i < serviceprolist.size(); i++) 
		{
			serviceId.add(serviceprolist.get(i).getserviceId());
		}
		/**
		 * Updated By: Viraj
		 * Date: 26-12-2018
		 * Description: To check if the size of serviceId List is greater than zero 
		 */
		System.out.println("size of serviceId: "+serviceId.size());
		if(serviceId.size() > 0) {
			List<Service> serviceLoadList=ofy().load().type(Service.class)
					.filter("companyId",comp.getCompanyId())
					.filter("count IN", serviceId).list();
			
			System.out.println("inside creat sorted"+serviceLoadList.size());
			
			List<Service> sortedServiceList =new ArrayList<Service>();
			sortedServiceList.addAll(getSortedList(serviceLoadList));
	
			int initialposition = 0;
			createServiceDetailTable(initialposition,sortedServiceList);
		} 
	}		
		
	
	private List<Service> getSortedList(List<Service> sortedServiceList)
	{
		
		/**
		 * Date 07-02-2018 By vijay for sorting Date and time services
		 * old code commented and new code added
		 */
		DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		
		for(int i=0;i<sortedServiceList.size();i++){
			
			if(sortedServiceList.get(i).getServiceTime().equalsIgnoreCase("Flexible")){
				
				Date serviceDate=DateUtility.getDateWithTimeZone("IST", sortedServiceList.get(i).getServiceDate());
				Calendar cal=Calendar.getInstance();
				cal.setTime(serviceDate);
				cal.add(Calendar.DATE, 0);
				
				Date servicedate=null;
				
				try {
					servicedate=fmt1.parse(fmt1.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,00);
					cal.set(Calendar.SECOND,00);
					servicedate=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				sortedServiceList.get(i).setServiceDate(servicedate);
			}
			else{
						
				String serviceTime[] = sortedServiceList.get(i).getServiceTime().split(":"); 
				int hh=0;
				int mm =0;
				int ss=0;
				if(sortedServiceList.get(i).getServiceTime().contains("AM")){
					if(!serviceTime[0].contains("--")){
						  hh = Integer.parseInt(serviceTime[0]);
					}
					if(!serviceTime[1].contains("--")){
						if(serviceTime[1].contains("AM")){
							String strAM[] = serviceTime[1].split("AM");
							mm = Integer.parseInt(strAM[0]);
						}
					}
					
				}
				
				if(sortedServiceList.get(i).getServiceTime().contains("PM")){
					 
					if(!serviceTime[0].contains("12")){
						hh = 12;	
					}
					if(!serviceTime[0].contains("--")){
						  hh += Integer.parseInt(serviceTime[0]);
					}		
					if(!serviceTime[1].contains("--")){
						if(serviceTime[1].contains("PM")){
							String strPM[] = serviceTime[1].split("PM");
							mm = Integer.parseInt(strPM[0]);
						}
					}
					
				}
				
				Date serviceDate=DateUtility.getDateWithTimeZone("IST", sortedServiceList.get(i).getServiceDate());
				Calendar cal=Calendar.getInstance();
				cal.setTime(serviceDate);
				cal.add(Calendar.DATE, 0);
				
				Date servicedate=null;
				
				try {
					servicedate=fmt1.parse(fmt1.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,hh);
					cal.set(Calendar.MINUTE,mm);
					cal.set(Calendar.SECOND,ss);
					servicedate=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				sortedServiceList.get(i).setServiceDate(servicedate);
			}
		}
		
		Comparator<Service> compServicelist = new Comparator<Service>() {
			
			@Override
			public int compare(Service s1, Service s2) {
				
				Date serviceDate = s1.getServiceDate();
				Date serviceDate2 = s2.getServiceDate();
				
				return serviceDate.compareTo(serviceDate2);
			}
		};
		Collections.sort(sortedServiceList,compServicelist);
		
		return sortedServiceList;
		
		/**
		 * ends here
		 */
		
//		System.out.println("inside get sorted"+sortedServiceList.size());
//		Service temp;
//		String firstDate=null;
//		String secondDate=null;
//		Date firstDate2 = null;
//		Date secondDate2 = null; 
//		 Date date1 = null;
//		 Date date2=null;
//		
//		 
//		for (int i = 0; i <sortedServiceList.size()-1; i++) {
//			System.out.println("sortedServiceList.size()-1=="+sortedServiceList.size());
//		for (int j = 0; j <(sortedServiceList.size()-i-1); j++) {
//			System.out.println("sortedServiceList.size()"+j);
//			
//		SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
//	    SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm a");
//	   
//	    String serviceTime=sortedServiceList.get(j).getServiceTime();
//	    if(serviceTime.trim().contains("-")){
//	    	serviceTime=serviceTime.trim().replaceAll("-", "0");
//	    	if(serviceTime.trim().equals("00:0000")){
//	    		serviceTime="00:00";
//	    	}
//	    }
//	    
//	    try {
//	    	if(serviceTime.equalsIgnoreCase("Flexible"))
//	    	{
//	    		date1 = displayFormat.parse("00:00");	
//	    	}
//	    	
//	    	else{
//	    		date1 = displayFormat.parse(serviceTime);
//	    	}
//			
//			System.out.println("date1===="+date1 );
//			
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	    String serviceTime2=sortedServiceList.get(j+1).getServiceTime();
//	    if(serviceTime2.trim().contains("-")){
//	    	serviceTime2=serviceTime2.trim().replaceAll("-", "0");
//	    	if(serviceTime2.trim().equals("00:0000")){
//	    		serviceTime2="00:00";
//	    	}
//	    }
//		try {
//			if(serviceTime2.equalsIgnoreCase("Flexible"))
//			{
//				date2=displayFormat.parse("00:00");
//			}
//			else
//			{
//			date2=displayFormat.parse(serviceTime2);
//			}
//			System.out.println("date2==="+date2);
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		System.out.println("111"+date2);
//		
//	    System.out.println(parseFormat.format(date1) + " = " + displayFormat.format(date1));
//	    System.out.println(parseFormat.format(date2) + " = " + displayFormat.format(date2));
//	   
//	    SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
//		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//		
//		firstDate=sdf.format(sortedServiceList.get(j).getServiceDate())+" "+displayFormat.format(date1);
//		secondDate=sdf.format(sortedServiceList.get(j+1).getServiceDate())+" "+displayFormat.format(date2);
//		System.out.println("swappin111"+firstDate);
//		System.out.println("swappin222"+secondDate);
//		
//		SimpleDateFormat datefor = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//		datefor.setTimeZone(TimeZone.getTimeZone("IST"));
//		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//		 try {
//			 firstDate2=datefor.parse(firstDate);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 try {
//			 secondDate2=datefor.parse(secondDate);
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 if(firstDate2.compareTo(secondDate2)>0)
//		 {
//			 temp = sortedServiceList.get(j);
//			 sortedServiceList.set(j,sortedServiceList.get(j+1));
//			 sortedServiceList.set(j+1,temp);
//
//			 System.out.println("bubble sort"+sortedServiceList.set(j+1,temp));
//		 }
//		
//		 
//		}
//		}
//		for (int m = 0; m < sortedServiceList.size(); m++) 
//		{
//			System.out.println("sorted list==="+sdf.format(sortedServiceList.get(m).getServiceDate())+sortedServiceList.get(m).getServiceTime());
//		}
//		System.out.println("");
//		System.out.println("sortedServiceList!!!!!"+sortedServiceList.size());
//	
//		return sortedServiceList ;
		
	}
	//end
	private void createServiceDetailTable(int position,
			List<Service> sortedServiceList) {
		int noOfLine = 0;
		int lineNo = 0;
		int linecount = 0;
		System.out.println("serviceproductlist size===outfor"
				+ serviceprolist.size());

		ArrayList<Service> serviceList = new ArrayList<Service>();

		Contract contract=null; 
		Boolean serviceMoreThanPage = false;
		System.out.println("intial position value==" + position);
		for (int i = position; i < sortedServiceList.size(); i++) {

			System.out.println("servicelist size==" + sortedServiceList.size());
			System.out.println("intial position value222==" + i);

			// serviceSet.add(serviceprolist.get(i).getserviceId());

			// List<Service> serviceLoadList = ofy().load().type(Service.class).
			// filter("companyId",serviceprolist.get(i).getCompanyId())
			// .filter("count IN",serviceSet.toArray()).list();

			// System.out.println("serviceLoadList"+serviceLoadList);
			
			//date 16/11/2017 By jayshree changes are made to add sorting 
			Service service = new Service();
			service = sortedServiceList.get(i);//
			//End by Jayshree
			
			// System.out.println("servicelist size==" + serviceList.size());
			//
			// System.out.println("hi vijay how r u");
			// if (serviceList.size() != 0) {
			// for (Service serviceEntity : serviceList)
			// {
			// if (serviceEntity.getCount() == serviceprolist.get(i)
			// .getserviceId()) {
			// service = serviceEntity;
			// break;
			// }
			// }
			// } else {
			// service = ofy()
			// .load()
			// .type(Service.class)
			// .filter("companyId",
			// serviceprolist.get(i).getCompanyId())
			// .filter("count", serviceprolist.get(i).getserviceId())
			// .first().now();
			// serviceList.add(service);
			// }
			//
			// if (service == null) {
			// service = ofy()
			// .load()
			// .type(Service.class)
			// .filter("companyId",
			// serviceprolist.get(i).getCompanyId())
			// .filter("count", serviceprolist.get(i).getserviceId())
			// .first().now();
			// serviceList.add(service);
			// }

			if (service != null) {
				if (service.getInvoiceId() != 0) {
					System.out.println("invoice id" + service.getInvoiceId());
					System.out.println("invoice id inside if"
							+ service.getInvoiceId());
					invoiceIdSet.add(service.getInvoiceId());

					System.out.println("invoiceid set"
							+ invoiceIdSet.add(service.getInvoiceId()));

				}
			}
			System.out.println("service==" + service);
			System.out.println("service id"
					+ serviceprolist.get(i).getserviceId());
			System.out.println("company id"
					+ serviceprolist.get(i).getCompanyId());

			if (service.getInvoiceId() != 0) {
				invoiceentity = ofy()
						.load()
						.type(Invoice.class)
						.filter("companyId", service.getCompanyId())
						.filter("count", service.getInvoiceId())
						.filter("typeOfOrder", AppConstants.ORDERTYPESERVICE)
						.filter("accountType",
								AppConstants.BILLINGACCOUNTTYPEAR).first()
						.now();
			} else {
				invoiceentity = ofy()
						.load()
						.type(Invoice.class)
						.filter("companyId", service.getCompanyId())
						.filter("contractCount", service.getContractCount())
						.filter("typeOfOrder", AppConstants.ORDERTYPESERVICE)
						.filter("accountType",
								AppConstants.BILLINGACCOUNTTYPEAR).first()
						.now();

			}
			/**
			 * Date 25/10/1027 By Jayshree to print the rowSpan if more than one
			 * Quanty is present in productdetaillist and more than one
			 * technician is present in technician list
			 */

			int rowSpan = 0;
			if (serviceprolist.get(i).getProdDetailsList().size() > serviceprolist
					.get(i).getTechnicians().size()) {

				rowSpan = serviceprolist.get(i).getProdDetailsList().size();
				System.out.println("rowspan value111===" + rowSpan);
			} else if (serviceprolist.get(i).getTechnicians().size() > serviceprolist
					.get(i).getProdDetailsList().size()) {
				rowSpan = serviceprolist.get(i).getTechnicians().size();
				System.out.println("rowspan value222===" + rowSpan);
			}

			System.out.println("row span value" + rowSpan);
			noOfLine = noOfLine + rowSpan;
			if (service.getAddress().getCompleteAddress().length() > 20) {
				noOfLine = noOfLine + 5;
			}
			System.out.println("number of line" + noOfLine);
			// linecount=linecount+rowSpan;
			if (noOfLine >= 48) {

				lineNo = i;
				System.out.println("break point" + lineNo);
				serviceMoreThanPage = true;
				break;
			}
			if (service != null) {

				PdfPTable serviceDetailTab = new PdfPTable(13);
				serviceDetailTab.setWidthPercentage(100);

				try {
					serviceDetailTab.setWidths(new float[] { 4, 6, 5, 17, 12, 9,8, 7, 7, 7, 7,5,8});
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Service temp;
				// String firstDate=null;
				// String secondDate=null;
				//

				// Collections.sort(serviceLoadList, new MyServiceComp());
				// for(Service serviceSorted: serviceLoadList){
				// System.out.println(" Service date"+
				// serviceSorted.getServiceDate()+serviceSorted.getServiceTime());
				//
				// }

				// for (int m = 0; m < serviceLoadList.size(); m++) {
				// for (int k = 0; k < (serviceLoadList.size()-k); k++)
				// {
				// Date firstDate2 = null;
				// Date secondDate2 = null;
				//
				// System.out.println("dateformat"+serviceLoadList.get(m).getServiceDate());
				// System.out.println("dateformat"+serviceLoadList.get(m).getServiceTime());
				// if(!serviceLoadList.get(m).getServiceTime().equals("Flexible")){
				//
				// }
				//
				// SimpleDateFormat displayFormat = new
				// SimpleDateFormat("HH:mm");
				// SimpleDateFormat parseFormat = new
				// SimpleDateFormat("HH:mm a");
				// Date date1 = null;
				// Date date2=null;
				//
				// System.out.println("Time -- i "+
				// serviceLoadList.get(m+1).getServiceTime()
				// + " \n time- i+ 2-- "
				// +serviceLoadList.get(m+1).getServiceTime());
				//
				// try {
				// date1 =
				// displayFormat.parse(serviceLoadList.get(m).getServiceTime().trim());
				//
				//
				// } catch (Exception e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
				// try {
				// date2=displayFormat.parse(serviceLoadList.get(m+1).getServiceTime());
				// } catch (Exception e1) {
				// // TODO Auto-generated catch block
				// e1.printStackTrace();
				// }
				// System.out.println("111"+date2);
				//
				// System.out.println(parseFormat.format(date1) + " = " +
				// displayFormat.format(date1));
				// System.out.println(parseFormat.format(date1) + " = " +
				// displayFormat.format(date2));
				//
				// SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
				// sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				//
				// firstDate=sdf.format(serviceLoadList.get(m).getServiceDate())+" "+displayFormat.format(date1);
				// secondDate=sdf.format(serviceLoadList.get(m+1).getServiceDate())+" "+displayFormat.format(date2);
				// System.out.println("swappin111"+firstDate);
				// System.out.println("swappin222"+secondDate);
				//
				// SimpleDateFormat datefor = new
				// SimpleDateFormat("dd/MM/yyyy HH:mm");
				// datefor.setTimeZone(TimeZone.getTimeZone("IST"));
				// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				// try {
				// firstDate2=datefor.parse(firstDate);
				// } catch (ParseException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// try {
				// secondDate2=datefor.parse(secondDate);
				// } catch (ParseException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				//
				// if( firstDate2.compareTo(secondDate2)<0)
				// {
				// // Collections.swap(serviceLoadList,m,m+1);
				// //// temp=firstDate2;
				// //// firstDate2=secondDate2;
				// //// secondDate2=temp;
				// //
				// temp = serviceLoadList.get(m);
				// serviceLoadList.set(m,serviceLoadList.get(m+1) );
				// serviceLoadList.set(m+1, temp);
				//
				// }
				//
				//
				//
				//
				//
				//
				//
				// }
				// }
				
				Phrase srno = null;
				srno = new Phrase(i+1+"", font8);
				PdfPCell srCell = new PdfPCell(srno);
				srCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				srCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(srCell);// c1
				

				Phrase dateValph2 = null;
				if (service.getServiceDate() != null) {
					dateValph2 = new Phrase(
							sdf.format(service.getServiceDate()), font8);
				} else {
					dateValph2 = new Phrase(" ", font8);
				}

				PdfPCell datevalCell = new PdfPCell(dateValph2);
				datevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				datevalCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(datevalCell);// c1

				Phrase timeValph = new Phrase(service.getServiceTime(), font8);
				PdfPCell timeValCell = new PdfPCell(timeValph);
				timeValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				timeValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(timeValCell);// c2

				Phrase custNameValph = new Phrase(service.getCustomerName()
						+ "\n" + service.getAddress().getCompleteAddress()
						+ "\n" + service.getCellNumber(), font8);
				PdfPCell custNameValCell = new PdfPCell(custNameValph);
				custNameValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				custNameValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(custNameValCell);// c3

				System.out.println("lenthofname=="
						+ service.getCustomerName().length());
				System.out.println("lenthofadd=="
						+ service.getAddress().getCompleteAddress().length());
				// Phrase mobValph = new Phrase(service.getCellNumber() + "",
				// font8);
				// PdfPCell mobValCell = new PdfPCell(mobValph);
				// mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// mobValCell.setRowspan(rowSpan);
				// serviceDetailTab.addCell(mobValCell);// c4

				
				/**
				 * Date 14-05-2018 
				 * Developer : Vijay
				 * Des : For highlighting Service No And Last Service
				 */

				String lastService="";
				boolean flag = false;
				if(contract==null){
				 contract = ofy().load().type(Contract.class).filter("companyId",service.getCompanyId()).filter("count", service.getContractCount()).first().now();
				}else{
					if(contract.getCount()!=service.getContractCount())
					 contract = ofy().load().type(Contract.class).filter("companyId",service.getCompanyId()).filter("count", service.getContractCount()).first().now();
				}
				if(contract!=null){
					for(int j=0;j<contract.getItems().size();j++){
						if(contract.getItems().get(j).getProductSrNo()==service.getServiceSrNo()){
							if(contract.getItems().get(j).getNumberOfServices() == service.getServiceSerialNo()){
								flag = true;
						}
					}
				}
				}	
				System.out.println("Hi vijay no of services =="+lastService);
				String serviceNo ="";
				if(flag){
					serviceNo = "("+service.getServiceSerialNo()+""+" Last Service)";
				}else{
					serviceNo ="("+service.getServiceSerialNo()+""+" Service)";
				}
						
				String productNameWithSrNo = service.getProductName()+"\n"+serviceNo;
				/**
				 * ends here
				 */
				
				Phrase serviceNameValph = new Phrase(productNameWithSrNo,
						font8);
				PdfPCell serviceNameValCell = new PdfPCell(serviceNameValph);
				serviceNameValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				serviceNameValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(serviceNameValCell);// c5

				/**
				 * Date 25/10/1027 By Jayshree to print the table if more than
				 * one Quanty is present in productdetaillist
				 */

				// *****chemical quatity table****
				PdfPTable quantityTab = new PdfPTable(1);
				quantityTab.setWidthPercentage(100);

				int remainingrowforquanty = rowSpan
						- serviceprolist.get(i).getProdDetailsList().size();
				if (remainingrowforquanty != 0) {
					for (int j = 0; j < remainingrowforquanty; j++) {
						Phrase blank2 = new Phrase("");
						PdfPCell blankCell2 = new PdfPCell(blank2);
						quantityTab.addCell(blankCell2);
					}
				}

				for (int l = 0; l < serviceprolist.get(i).getProdDetailsList()
						.size(); l++) {

					Phrase chemicalValph = null;
					if (serviceprolist.get(i).getProdDetailsList().get(l)
							.getCode() != null
							|| serviceprolist.get(i).getProdDetailsList()
									.get(l).getQuantity() != 0) {
						chemicalValph = new Phrase(serviceprolist.get(i)
								.getProdDetailsList().get(l).getCode()
								+ " "
								+ "/"
								+ serviceprolist.get(i).getProdDetailsList()
										.get(l).getQuantity()
								+ " "
								+ "/"
								+ "_____", font8);
					} else {
						chemicalValph = new Phrase(" ");
					}
					PdfPCell chemicalValCell = new PdfPCell(chemicalValph);
					chemicalValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					quantityTab.addCell(chemicalValCell);
				}
				// *****end******

				PdfPCell chemicalVal2 = new PdfPCell(quantityTab);
				chemicalVal2.setHorizontalAlignment(Element.ALIGN_CENTER);
				serviceDetailTab.addCell(chemicalVal2);// c6

				/**
				 * Date 25/10/1027 By Jayshree to print the table if more than
				 * one technician is present in technicianlist
				 */

				// ****Technician Table
				PdfPTable technicinnameTab = new PdfPTable(1);
				technicinnameTab.setWidthPercentage(100);

				int remainingrowfortechnitian = rowSpan
						- serviceprolist.get(i).getTechnicians().size();
				if (remainingrowfortechnitian != 0) {
					for (int k = 0; k < remainingrowfortechnitian; k++)

					{
						Phrase blank = new Phrase("");
						PdfPCell blankCell = new PdfPCell(blank);
						technicinnameTab.addCell(blankCell);
					}
				}

				System.out.println("technician size ==="
						+ serviceprolist.get(i).getTechnicians().size());
				for (int j = 0; j < serviceprolist.get(i).getTechnicians()
						.size(); j++) {

					Phrase technameVal = new Phrase(serviceprolist.get(i)
							.getTechnicians().get(j).getFullName(), font8);
					PdfPCell technameValCell = new PdfPCell(technameVal);
					technameValCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					technicinnameTab.addCell(technameValCell);
				}

				// *****End****

				PdfPCell technameValCell = new PdfPCell(technicinnameTab);
				technameValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				serviceDetailTab.addCell(technameValCell);// c7

				if (invoiceentity != null) {
					Phrase valofContractph2 = null;
					if (invoiceentity.getNetPayable() != 0) {
						valofContractph2 = new Phrase(
								invoiceentity.getNetPayable() + "", font8);
					} else {
						valofContractph2 = new Phrase(" ", font8);
					}
					// Phrase valofContractph2 = new
					// Phrase(invoiceentity.getNetPayable()+"", font8);
					PdfPCell valofContractCell2 = new PdfPCell(valofContractph2);
					valofContractCell2
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					valofContractCell2.setRowspan(rowSpan);
					serviceDetailTab.addCell(valofContractCell2);// c8

				} else {
					Phrase valofContractph3 = new Phrase(" ", font8);
					PdfPCell valofContractCell3 = new PdfPCell(valofContractph3);
					valofContractCell3
							.setHorizontalAlignment(Element.ALIGN_CENTER);
					valofContractCell3.setRowspan(rowSpan);
					serviceDetailTab.addCell(valofContractCell3);// c8
				}

				Phrase revAmtValph = new Phrase(" ", font8);
				PdfPCell revAmtValCell = new PdfPCell(revAmtValph);
				revAmtValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				revAmtValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(revAmtValCell);// c9

				Phrase revsignValph = new Phrase(" ", font8);
				PdfPCell revsignValCell = new PdfPCell(revsignValph);
				revsignValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				revsignValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(revsignValCell);// c10

				Phrase otherValph = null;
				 if(service.getTechPlanOtherInfo()!=null)
				{
				 otherValph = new Phrase(service.getTechPlanOtherInfo(), font8bold);
				 }
				 else
				{
				 otherValph = new Phrase(" ", font8bold);
				 }
//				 Phrase otherValph = new Phrase(, font8bold);
				PdfPCell otherValCell = new PdfPCell(otherValph);
				otherValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				// revsignCell.setRowspan(2);
				serviceDetailTab.addCell(otherValCell);// c9

				// Phrase signph = new Phrase("      ", font8);
				// PdfPCell signCell = new PdfPCell(signph);
				// signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				// serviceDetailTab.addCell(signCell);// c11

				Phrase inoutValph = new Phrase(" ", font8);
				PdfPCell inoutValCell = new PdfPCell(inoutValph);
				inoutValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				inoutValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(inoutValCell);// c10
				
				Phrase custValph = new Phrase(" ", font8);
				PdfPCell custValCell = new PdfPCell(custValph);
				custValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				custValCell.setRowspan(rowSpan);
				serviceDetailTab.addCell(custValCell);// c10
				
				try {
					document.add(serviceDetailTab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

			/**
			 * Date 25/10/1027 By Jayshree to print the table in next page if
			 * services are not arrange in single page
			 */

			noOfLine++;
			System.out.println("linecount11==" + noOfLine);
		}
		System.out.println("linecount22==" + noOfLine);
		if (serviceMoreThanPage) {
			System.out.println("flag true");
			createfooterTab();

			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			createHeaderTable();
			createTitleTable();
			createServiceDetailTable(lineNo, sortedServiceList);

		}
	}
	

	private void createfooterTab() {

		PdfPTable footerTab = new PdfPTable(3);
		footerTab.setWidthPercentage(100);
		footerTab.setSpacingBefore(10);

		try {
			footerTab.setWidths(new float[] { 33, 33, 34 });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase operatersignph = new Phrase("Sign of Operator", font8bold);
		PdfPCell operatersignCell = new PdfPCell(operatersignph);
		operatersignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		operatersignCell.setBorder(0);
		footerTab.addCell(operatersignCell);// c1

		Phrase adminsignph = new Phrase("Sign Of Admin", font8bold);
		PdfPCell adminsignCell = new PdfPCell(adminsignph);
		adminsignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		adminsignCell.setBorder(0);
		footerTab.addCell(adminsignCell);// c3

		Phrase branchHeadsignph = new Phrase("Sign Of Branch Head", font8bold);
		PdfPCell branchHeadCell = new PdfPCell(branchHeadsignph);
		branchHeadCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		branchHeadCell.setBorder(0);
		footerTab.addCell(branchHeadCell);// c5

		Phrase blank = new Phrase(" ");
		PdfPCell blCell = new PdfPCell(blank);
		blCell.setBorder(0);
		footerTab.addCell(blCell);

		footerTab.addCell(blCell);

		footerTab.addCell(blCell);

		Phrase operatersignValph = new Phrase("_______________", font8);
		PdfPCell operatersignValCell = new PdfPCell(operatersignValph);
		operatersignValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		operatersignValCell.setBorder(0);
		// operatersignValCell.setBorderWidthRight(0);
		// operatersignValCell.setBorderWidthTop(0);
		footerTab.addCell(operatersignValCell);// c2

		Phrase adminsignValph = new Phrase("_______________", font8);
		PdfPCell adminsignValCell = new PdfPCell(adminsignValph);
		adminsignValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		adminsignValCell.setBorder(0);
		// adminsignValCell.setBorderWidthRight(0);
		// adminsignValCell.setBorderWidthTop(0);
		footerTab.addCell(adminsignValCell);// c4

		Phrase branchHeadsignValph = new Phrase("_______________", font8);
		PdfPCell branchHeadValCell = new PdfPCell(branchHeadsignValph);
		branchHeadValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		branchHeadValCell.setBorder(0);
		// branchHeadValCell.setBorderWidthRight(0);
		// branchHeadValCell.setBorderWidthTop(0);
		footerTab.addCell(branchHeadValCell);// c6

		try {
			document.add(footerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	// By jayshree 
//	class MyServiceComp implements Comparator<Service>{
//		 
//	    @Override
//	    public int compare(Service e1, Service e2) {
//	    	String firstDate="",secondDate="";
//	    	Date d1=null,d2=null;
//	    	if(!e1.getServiceTime().equals("Flexible")){
//				firstDate=e1.getServiceDate()+" "+e1.getServiceTime();
//	    	}
//				if(!e2.getServiceTime().equals("Flexible")){
//					secondDate=e2.getServiceDate()+" "+e2.getServiceTime();
//				}
//			
//			try {
//				d1=dateTimeFormat.parse(firstDate);
//				d2=dateTimeFormat.parse(secondDate);
//			} catch (ParseException e) {
//				e.printStackTrace();
//			}
//			if(d1!=null &&d2!=null){
//	        if(d1.compareTo(d2)<0){
//	            return 1;
//	        } else {
//	            return -1;
//	        }
//			}
//			else return 0;
//	    }
//	}
}	
//	private void createRemainingservicetable() {
//		// if(lineNo!=0)
//		// {
//		//
//
//		createAnextureproduct(lineNo);
//		// }
//
//	}

//	private void createAnextureproduct(int lineNo2) {
//		// TODO Auto-generated method stub
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		createHeaderTable();
//		createTitleTable();
//		ArrayList<Service> serviceList = new ArrayList<Service>();
//
//		// noOfLine=35;
//		// Boolean serviceMoreThanPage = false;
//
//		for (int i = lineNo; i < serviceprolist.size(); i++) {
//			System.out.println("servicelist==" + serviceList);
//
//			Service service = null;
//			System.out.println("servicelist size==" + serviceList.size());
//
//			System.out.println("hi vijay how r u");
//			if (serviceList.size() != 0) {
//				for (Service serviceEntity : serviceList) {
//					if (serviceEntity.getCount() == serviceprolist.get(i)
//							.getserviceId()) {
//						service = serviceEntity;
//						break;
//					}
//				}
//			} else {
//				service = ofy()
//						.load()
//						.type(Service.class)
//						.filter("companyId",
//								serviceprolist.get(i).getCompanyId())
//						.filter("count", serviceprolist.get(i).getserviceId())
//						.first().now();
//				serviceList.add(service);
//			}
//
//			if (service == null) {
//				service = ofy()
//						.load()
//						.type(Service.class)
//						.filter("companyId",
//								serviceprolist.get(i).getCompanyId())
//						.filter("count", serviceprolist.get(i).getserviceId())
//						.first().now();
//				serviceList.add(service);
//			}
//
//			if (service != null) {
//				if (service.getInvoiceId() != 0) {
//					System.out.println("invoice id" + service.getInvoiceId());
//					System.out.println("invoice id inside if"
//							+ service.getInvoiceId());
//					invoiceIdSet.add(service.getInvoiceId());
//
//					System.out.println("invoiceid set"
//							+ invoiceIdSet.add(service.getInvoiceId()));
//
//				}
//			}
//			System.out.println("service==" + service);
//			System.out.println("service id"
//					+ serviceprolist.get(i).getserviceId());
//			System.out.println("company id"
//					+ serviceprolist.get(i).getCompanyId());
//
//			/**
//			 * Date 25/10/1027 By Jayshree to print the rowSpan if more than one
//			 * Quanty is present in productdetaillist and more than one
//			 * technician is present in technician list
//			 */
//
//			int rowSpan = 0;
//			if (serviceprolist.get(i).getProdDetailsList().size() > serviceprolist
//					.get(i).getTechnicians().size()) {
//
//				rowSpan = serviceprolist.get(i).getProdDetailsList().size();
//				System.out.println("rowspan value111===" + rowSpan);
//			} else if (serviceprolist.get(i).getTechnicians().size() > serviceprolist
//					.get(i).getProdDetailsList().size()) {
//				rowSpan = serviceprolist.get(i).getTechnicians().size();
//				System.out.println("rowspan value222===" + rowSpan);
//			}
//
//			System.out.println("row span value" + rowSpan);
//			noOfLine = noOfLine - rowSpan;
//			System.out.println("number of line" + noOfLine);
//
//			// if(noOfLine<=0)
//			// {
//			//
//			// lineNo=i;
//			// // serviceMoreThanPage = true;
//			// break;
//			// }
//
//			if (service != null) {
//
//				PdfPTable serviceDetailTab = new PdfPTable(9);
//				serviceDetailTab.setWidthPercentage(100);
//
//				try {
//					serviceDetailTab.setWidths(new float[] { 7, 5, 20, 15, 13,
//							10, 10, 10, 10 });
//				} catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//				// Phrase blankphrase = new Phrase(" ");
//				// PdfPCell blCell = new PdfPCell(blankphrase);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//				// serviceDetailTab.addCell(blCell);
//
//				Phrase dateValph2 = null;
//				if (service.getServiceDate() != null) {
//					dateValph2 = new Phrase(
//							sdf.format(service.getServiceDate()), font8);
//				} else {
//					dateValph2 = new Phrase(" ", font8);
//				}
//
//				PdfPCell datevalCell = new PdfPCell(dateValph2);
//				datevalCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				datevalCell.setRowspan(rowSpan);
//				serviceDetailTab.addCell(datevalCell);// c1
//
//				Phrase timeValph = new Phrase(service.getServiceTime(), font8);
//				PdfPCell timeValCell = new PdfPCell(timeValph);
//				timeValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				timeValCell.setRowspan(rowSpan);
//				serviceDetailTab.addCell(timeValCell);// c2
//
//				Phrase custNameValph = new Phrase(service.getCustomerName()
//						+ "\n" + service.getAddress().getCompleteAddress()
//						+ "\n" + service.getCellNumber(), font8);
//				PdfPCell custNameValCell = new PdfPCell(custNameValph);
//				custNameValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				custNameValCell.setRowspan(rowSpan);
//				serviceDetailTab.addCell(custNameValCell);// c3
//
//				System.out.println("lenthofname=="
//						+ service.getCustomerName().length());
//				System.out.println("lenthofadd=="
//						+ service.getAddress().getCompleteAddress().length());
//				// Phrase mobValph = new Phrase(service.getCellNumber() + "",
//				// font8);
//				// PdfPCell mobValCell = new PdfPCell(mobValph);
//				// mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				// mobValCell.setRowspan(rowSpan);
//				// serviceDetailTab.addCell(mobValCell);// c4
//
//				Phrase serviceNameValph = new Phrase(service.getProductName(),
//						font8);
//				PdfPCell serviceNameValCell = new PdfPCell(serviceNameValph);
//				serviceNameValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				serviceNameValCell.setRowspan(rowSpan);
//				serviceDetailTab.addCell(serviceNameValCell);// c5
//
//				/**
//				 * Date 25/10/1027 By Jayshree to print the table if more than
//				 * one Quanty is present in productdetaillist
//				 */
//
//				// *****chemical quatity table****
//				PdfPTable quantityTab = new PdfPTable(1);
//				quantityTab.setWidthPercentage(100);
//
//				int remainingrowforquanty = rowSpan
//						- serviceprolist.get(i).getProdDetailsList().size();
//				if (remainingrowforquanty != 0) {
//					for (int j = 0; j < remainingrowforquanty; j++) {
//						Phrase blank2 = new Phrase("");
//						PdfPCell blankCell2 = new PdfPCell(blank2);
//						quantityTab.addCell(blankCell2);
//					}
//				}
//
//				System.out.println("product list size=="
//						+ serviceprolist.get(i).getProdDetailsList().size());
//				for (int l = 0; l < serviceprolist.get(i).getProdDetailsList()
//						.size(); l++) {
//
//					Phrase chemicalValph = null;
//					if (serviceprolist.get(i).getProdDetailsList().get(l)
//							.getCode() != null
//							|| serviceprolist.get(i).getProdDetailsList()
//									.get(l).getQuantity() != 0) {
//						chemicalValph = new Phrase(serviceprolist.get(i)
//								.getProdDetailsList().get(l).getCode()
//								+ " "
//								+ "/"
//								+ serviceprolist.get(i).getProdDetailsList()
//										.get(l).getQuantity()
//								+ " "
//								+ "/"
//								+ "_____", font8);
//					} else {
//						chemicalValph = new Phrase(" ");
//					}
//					PdfPCell chemicalValCell = new PdfPCell(chemicalValph);
//					chemicalValCell
//							.setHorizontalAlignment(Element.ALIGN_CENTER);
//					quantityTab.addCell(chemicalValCell);
//				}
//				// *****end******
//
//				PdfPCell chemicalVal2 = new PdfPCell(quantityTab);
//				chemicalVal2.setHorizontalAlignment(Element.ALIGN_CENTER);
//				serviceDetailTab.addCell(chemicalVal2);// c6
//
//				/**
//				 * Date 25/10/1027 By Jayshree to print the table if more than
//				 * one technician is present in technicianlist
//				 */
//
//				// ****Technician Table
//				PdfPTable technicinnameTab = new PdfPTable(1);
//				technicinnameTab.setWidthPercentage(100);
//
//				int remainingrowfortechnitian = rowSpan
//						- serviceprolist.get(i).getTechnicians().size();
//				if (remainingrowfortechnitian != 0) {
//					for (int k = 0; k < remainingrowfortechnitian; k++)
//
//					{
//						Phrase blank = new Phrase("");
//						PdfPCell blankCell = new PdfPCell(blank);
//						technicinnameTab.addCell(blankCell);
//					}
//				}
//
//				System.out.println("technician size ==="
//						+ serviceprolist.get(i).getTechnicians().size());
//				for (int j = 0; j < serviceprolist.get(i).getTechnicians()
//						.size(); j++) {
//
//					Phrase technameVal = new Phrase(serviceprolist.get(i)
//							.getTechnicians().get(j).getFullName(), font8);
//					PdfPCell technameValCell = new PdfPCell(technameVal);
//					technameValCell
//							.setHorizontalAlignment(Element.ALIGN_CENTER);
//					technicinnameTab.addCell(technameValCell);
//				}
//
//				// *****End****
//
//				PdfPCell technameValCell = new PdfPCell(technicinnameTab);
//				technameValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				serviceDetailTab.addCell(technameValCell);// c7
//
//				Phrase valofContractph2 = new Phrase(" ", font8);
//				PdfPCell valofContractCell2 = new PdfPCell(valofContractph2);
//				valofContractCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//				valofContractCell2.setRowspan(rowSpan);
//				serviceDetailTab.addCell(valofContractCell2);// c8
//
//				Phrase revAmtValph = new Phrase(invoiceentity.getNetPayable()
//						+ "", font8);
//				PdfPCell revAmtValCell = new PdfPCell(revAmtValph);
//				revAmtValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				revAmtValCell.setRowspan(rowSpan);
//				serviceDetailTab.addCell(revAmtValCell);// c9
//
//				Phrase revsignValph = new Phrase(" ", font8);
//				PdfPCell revsignValCell = new PdfPCell(revsignValph);
//				revsignValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				revsignValCell.setRowspan(rowSpan);
//				serviceDetailTab.addCell(revsignValCell);// c10
//
//				// Phrase signph = new Phrase("      ", font8);
//				// PdfPCell signCell = new PdfPCell(signph);
//				// signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				// serviceDetailTab.addCell(signCell);// c11
//
//				try {
//					document.add(serviceDetailTab);
//				} catch (DocumentException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//
//			/**
//			 * Date 25/10/1027 By Jayshree to print the table in next page if
//			 * services are not arrange in single page
//			 */
//
//		}
//		createfooterTab();
//
//	}
//}
