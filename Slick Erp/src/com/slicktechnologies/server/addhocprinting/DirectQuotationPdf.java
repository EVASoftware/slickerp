package com.slicktechnologies.server.addhocprinting;



import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;


public class DirectQuotationPdf {
	
public Document document;
	
	Company comp;
	Customer cust;
	Quotation dir;
	Phrase colon;
	
	Phrase chunk;
	PdfPCell pdfno, pdftreatment, pdfpest, pdffrequency, pdfpremise, pdfprice;
//	PdfPCell pdfdays, pdfpercent, pdfcomment;
	PdfPCell  pdfsubttl,pdfttlamt;
	
	float[] colwidth = { 1.0f, 0.1f, 7.0f, 1.5f, 0.1f, 1.5f };
	float[] col1width = { 1.5f, 0.1f, 1.5f,3.0f, 1.5f, 0.1f, 1.5f};
	float[] tblcol1width = { 1.5f, 1.5f, 7.0f };
	float[] tblcowidth = { 1.5f, 0.2f, 7.f };
	float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f };
	
	float[] tblcolwidth12 = { 0.3f, 1.0f, 1.0f, 1.0f, 1.0f ,1.0f};

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");
	
	private PdfPCell custlandcell;
	
	
	public DirectQuotationPdf()
	    {
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	}
	

	public void setdirectquotation(Long count,String status) {
		
		System.out.println("in side setdirectquotation method");
		
		dir = ofy().load().type(Quotation.class).id(count).now();
		
		if (dir.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", dir.getCustomerId())
					.filter("companyId", dir.getCompanyId()).first().now();
		
		if (dir.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", dir.getCustomerId()).first().now();

		if (dir.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", dir.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}


	public void createPdf(String status) {
		
		System.out.println("status value "+status);
		if(status.equals("yes"))
		{
			createBlank1Heading();
			createLogo(document,comp);
		}
		else if(status.equals("no"))
		{
			createBlank1Heading();
		    	if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
		}
		
		
		createTitleHeading();
//		System.out.println("title has been created");
		createReferenceHeading();
//		System.out.println("reference heading has been created");
		createCustomerDetailsHeading();
		createQuotationTable();
		createOPCSHeading();
		createNoteHeading();
	}

	
private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	public void createBlank1Heading() {
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
	    
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
	    
	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
//		PdfPCell cell1 = new PdfPCell();
//		cell1.addElement(bltable);
////		cell1.setBorder(0);
//		cell1.setBorderWidthTop(0);
//		cell1.setBorderWidthLeft(0);
//		cell1.setBorderWidthRight(0);
//		
//		PdfPTable table = new PdfPTable(1);
//		table.addCell(cell1);
//		table.setWidthPercentage(100);
	
	    
		try {
			document.add(bltable);
			System.out.println("BLANK CELL ADDED IN DOCUMENT...");
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	    private void createLogo(Document doc, Company comp) {
			//********************logo for server ********************
				DocumentUpload document =comp.getLogo();
				//patch
				String hostUrl; 
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
			}
				
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scalePercent(20f);
					image2.setAbsolutePosition(40f,765f);	
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
			}			
	}
	    
	
	public void createTitleHeading(){
		
		System.out.println("inside create tilte heading");
		
		String title = "Quotation";
		Phrase quotation = new Phrase(title,font16boldul);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase blk = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blk);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.addElement(qtable);
		cell2.setBorder(0);
		
		PdfPTable table1 = new PdfPTable(1);
		table1.addCell(cell2);
		table1.setWidthPercentage(100);
	
		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public void createReferenceHeading(){
		
		Phrase ref = new Phrase("Ref. no.",font8);
		PdfPCell refcell = new PdfPCell(ref);
		refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase r = null;
		
		if(dir.getReferenceNumber() !=null){
		r = new Phrase(dir.getReferenceNumber()+"",font8);	
		}
		
		PdfPCell rcell = new PdfPCell(r);
		rcell.setBorder(0);
		rcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase column = new Phrase(":",font8);
		PdfPCell colcell = new PdfPCell(column);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date",font8);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase dt = null;
		if(dir.getReferenceDate() !=null){
			dt = new Phrase(fmt.format(dir.getQuotationDate()),font8);
		}
		
		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank); 
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable rdttable = new PdfPTable(6);
		rdttable.addCell(refcell);
		rdttable.addCell(colcell);
		rdttable.addCell(rcell);
		rdttable.addCell(datecell);
		rdttable.addCell(colcell);
		rdttable.addCell(dtcell);
//		rdttable.addCell(blankcell);
		rdttable.setWidthPercentage(100f);
		
		try {
			 rdttable.setWidths(colwidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPCell cell3 = new PdfPCell(rdttable); 		
		cell3.setBorder(0);
		
		PdfPTable table2 = new PdfPTable(1);
		table2.addCell(cell3);
		table2.setWidthPercentage(100);
		
		try {
			document.add(table2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createCustomerDetailsHeading(){
		
		Phrase to = new Phrase("To",font8);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setBorder(0);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String custAdd1="";
		String custFullAdd1="";
		
		
		Phrase designation = null;
		PdfPCell designationCell = null;
		if(cust.getCustCorresponence()!=null)
		{
			designation = new Phrase(cust.getCustCorresponence(),font8);
			
			designationCell = new PdfPCell(designation);
			designationCell.setBorder(0);
		}
//		else
//		{
//			designation = new Phrase(" " ,font8);
//			
//			designationCell = new PdfPCell(designation);
//			designationCell.setBorder(0);
//		}
		Phrase custName = null;
		
		if(cust.isCompany()==true)
		{
			custName = new Phrase(cust.getCompanyName(),font8);
		}
		else
		{
			custName = new Phrase(cust.getFullname(),font8);
		}
		PdfPCell custNameCell =  new PdfPCell(custName);
		custNameCell.setBorder(0);
		
		
		if(cust.getAdress()!=null){
			
			if(!cust.getAdress().getAddrLine2().equals("")){
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
				}
			}else{
				if(!cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(!cust.getSecondaryAdress().getLocality().equals("")){
				custFullAdd1=custAdd1+","+cust.getSecondaryAdress().getLocality()+"\n"+cust.getSecondaryAdress().getCity()+"-"+cust.getSecondaryAdress().getPin()+","+cust.getSecondaryAdress().getState()+","+cust.getSecondaryAdress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getSecondaryAdress().getCity()+"-"+cust.getSecondaryAdress().getPin()+","+cust.getSecondaryAdress().getState()+","+cust.getSecondaryAdress().getCountry();
			}
		}
		
		Phrase mobno = null;
		if (cust.getLandline() != null && cust.getLandline()!=0) {
			mobno = new Phrase("Tel No. : " + cust.getCellNumber1()+" ,022-"+cust.getLandline(), font8);
		}
		else
		{
			mobno = new Phrase("Tel No. : " + cust.getCellNumber1(),font8);
		}
		
		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase poc = new Phrase("POC: "+dir.getCustomerFullName(),font10);
//		PdfPCell poccell = new PdfPCell(poc);
//		poccell.setBorder(0);
//		poccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		//**************patch for comp  name **********8
		
		
		
		//**************changes ends here **************
		Phrase pname = null;
		PdfPCell pnamecell =null;
		if(cust.isCompany()==true){
			if(!dir.getDesignation().equals(""))
			{
				pname = new Phrase("CTC: " + cust.getFullname()+"-"+dir.getDesignation(), font8);
			}
			else
			{
				pname = new Phrase("CTC: " + cust.getFullname(), font8);
			}
			
			pnamecell = new PdfPCell(pname);
			pnamecell.setBorder(0);
			pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
		
		PdfPCell cellNo2=null;
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
		{
			Phrase phNo2 = new Phrase("Cell No. : "+cust.getCellNumber2(),font8); 
			cellNo2 = new PdfPCell(phNo2);
			cellNo2.setBorder(0);
		}
		
		Phrase sub=null;
		if(dir.getPremisesDesc()!=null){
		 sub = new Phrase("Sub: Quotation for Pest Management Services "+dir.getPremisesDesc()+".",font8);
		}
		else
		{
			 sub = new Phrase("Sub: Quotation for Pest Management Services.",font8);
		}
		 PdfPCell subcell = new PdfPCell(sub);
		subcell.setBorder(0);
		subcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase custAddInfo = new Phrase(custFullAdd1, font8);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		Phrase blank = new Phrase(" ", font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		Phrase dear = new Phrase("Dear Sir / Madam"+","+"\n",font8);
		PdfPCell dearcell = new PdfPCell(dear);
		dearcell.setBorder(0);
		dearcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		//   rohan added this 
		Phrase msg=null;
		if(dir.getPremisesDesc()!=null){
			 msg = new Phrase("With reference to your enquiry, we would like to quote our best rates for the pest management services "+dir.getPremisesDesc()+". Following are our recommended services along with their respective charges. ",font8);
		}
		else
		{
		    msg = new Phrase("With reference to your enquiry, we would like to quote our best rates for the pest management services for your Entire Office area. Following are our recommended services along with their respective charges. ",font8);
		}
		
		PdfPCell msgcell = new PdfPCell(msg);
		msgcell.setBorder(0);
		msgcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable cdtable = new PdfPTable(1);
		cdtable.addCell(toCell);
		if(cust.getCustCorresponence()!=null)
		{
		cdtable.addCell(designationCell);
		}
		cdtable.addCell(custNameCell);
		cdtable.addCell(custAddInfoCell);
	
		if(cust.isCompany()==true){
		cdtable.addCell(pnamecell);
		}
		cdtable.addCell(mobcell);
		if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
		{
		cdtable.addCell(cellNo2);
		}
		cdtable.addCell(blankCell);
		cdtable.addCell(subcell);
		cdtable.addCell(blankCell);
		cdtable.addCell(dearcell);
		cdtable.addCell(msgcell);
		
		cdtable.setSpacingAfter(10f);
		
		
		cdtable.setWidthPercentage(100f);

		PdfPCell cell4 = new PdfPCell(cdtable);
		cell4.setBorder(0);
		
		PdfPTable table3 = new PdfPTable(1);
		table3.addCell(cell4);
		table3.setWidthPercentage(100);
		
		
		try {
			document.add(table3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void createOPCSHeading(){
		
		Phrase bl = new Phrase(" ",font8);
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);
		
		Phrase opcs = new Phrase("OPCS Assurance",font8bold);
		PdfPCell opcscell = new PdfPCell(opcs); 
		opcscell.setBorder(0);
		opcscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":",font8);
		PdfPCell colcell = new PdfPCell(colon); 
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase line = new Phrase("In case of pest resurgence during the contarct period, interim calls, if any, would be attended to, without any extra cost. ",font8);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setBorder(0);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable otable = new PdfPTable(3);
		otable.addCell(opcscell);
		otable.addCell(colcell);
		otable.addCell(linecell);
		otable.setWidthPercentage(100f);
		otable.setSpacingBefore(10f);
		try {
			otable.setWidths(tblcowidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
//		PdfPCell ocell = new PdfPCell(otable);
//		ocell.setBorder(0);
		
//		PdfPTable table4 = new PdfPTable(1);
//		table4.addCell(ocell);
//		table4.setWidthPercentage(100);
		
		try {
			document.add(otable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void createNoteHeading(){
		
		PdfPCell descCell=null;
		PdfPCell bnoteCell=null;
		if(dir.getDescription()!=null){
			
			Phrase bnote = new  Phrase("Notes : ", font8bold);
			bnoteCell = new PdfPCell(bnote);
			bnoteCell.setBorder(0);
			
			Phrase description =new Phrase(dir.getDescription(),font8);
			descCell = new PdfPCell(description);
			descCell.setBorder(0);
		}
		
//		Phrase notes = new Phrase("Notes "+"-",font10bold);
//		PdfPCell notecell = new PdfPCell(notes);
//		notecell.setBorder(0);
//		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase abc = new Phrase("    -     Full Payment in Advance."+"\n"+"    -     Our treatment are Eco Friendly, Nontoxic & Odourless."+"\n"+"    -     No need to keep your premise close or vacant before or after service."+"\n"+"    -     Service details, SOP's, MSDS and Service schedule will be submitted after signing the contract."+"\n"+"    -     We are members of Pest Managemnet Association"+"\n"+"    -     An ISO 9001-2000 company.",font10);
//		PdfPCell abcCell = new PdfPCell(abc);
//		abcCell.setBorder(0);
//		abcCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase abc1 = new Phrase("This treatment makes human life easy & regular from pest problem. We assure you best pest management services with advanced technologies. Your positive response will be appreciated."+"\n"+"\n"+"\n"+"\n"+"Thanks & Regards, ",font8);
		PdfPCell abc1cell = new PdfPCell(abc1);
		abc1cell.setBorder(0);
		abc1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase abc2 = new Phrase("For Om Pest Control Services "+"\n"+"\n"+"\n"+"Authorised Signatory. ",font8bold);
		PdfPCell abc2cell = new PdfPCell(abc2);
		abc2cell.setBorder(0);
		abc2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase spacing =new Phrase(Chunk.NEWLINE);
		PdfPCell spacinngCell = new PdfPCell(spacing);
		spacinngCell.setBorder(0);
		
		
		PdfPTable notetable = new PdfPTable(1);
//		notetable.addCell(notecell);
//		notetable.addCell(abcCell);
		if(dir.getDescription()!=null){
			
			notetable.addCell(bnoteCell);
			notetable.addCell(descCell);
		}
		notetable.addCell(spacinngCell);
		notetable.addCell(abc1cell);
		notetable.addCell(abc2cell);
		notetable.setWidthPercentage(100f);
		
		PdfPCell cell6 = new PdfPCell(notetable);
		cell6.setBorder(0);
		
		PdfPTable table5 = new PdfPTable(1);
		table5.addCell(cell6);
		table5.setWidthPercentage(100);
		
		try {
			document.add(table5);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/****************************************************************************/
	
	public void createQuotationTable(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		try {
			table.setWidths(tblcolwidth12);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Phrase no = new Phrase("No.",font1);
		Phrase treatment = new Phrase("Treatment", font1);
		Phrase pest = new Phrase("Pest Covered",font1);
		Phrase frequency = new Phrase("Frequency per annum(Services)", font1);
		Phrase premise = new Phrase("Premises under contract", font1);
		Phrase price = new Phrase("AMC Charges(Rs).", font1);
		
		
		PdfPCell nocell = new PdfPCell(no);
		nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell  treatcell = new PdfPCell( treatment);
		treatcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell pestcell = new PdfPCell(pest);
		pestcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell freqcell = new PdfPCell(frequency);
		freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell precell = new PdfPCell(premise);
		precell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(nocell);
		table.addCell(treatcell);
		table.addCell(pestcell);
		table.addCell(freqcell);
		table.addCell(precell);
		table.addCell(pricecell);

		
		for(int i=0; i<this.dir.getItems().size(); i++){
			
		chunk = new Phrase(i+1 +"",font8);	
		pdfno = new PdfPCell(chunk);
		pdfno.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(dir.getItems().get(i).getProductName()+"",font8);
		System.out.println("chunk 1: "+dir.getItems().get(i).getProductName());
		pdftreatment = new PdfPCell(chunk);
		pdftreatment.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(dir.getItems().get(i).getComment()+"",font8);
		System.out.println("chunk 2 :"+dir.getItems().get(i).getComment());
		pdfpest = new PdfPCell(chunk);
		pdfpest.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(dir.getItems().get(i).getNumberOfServices()+"",font8);	
		System.out.println("chunk 3 :"+dir.getItems().get(i).getNumberOfServices());
		pdffrequency = new PdfPCell(chunk);
		pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		if(dir.getItems().get(i).getPremisesDetails()!=null){
			chunk = new Phrase(dir.getItems().get(i).getPremisesDetails(),font8);
			}
		else
		{
			chunk = new Phrase(" ",font8);
		}
			pdfpremise = new PdfPCell(chunk);
			pdfpremise.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk =  new Phrase((dir.getItems().get(i).getPrice()*dir.getItems().get(i).getQty())+"",font8);
		System.out.println("chunk 4 :"+dir.getItems().get(i).getPrice());
		pdfprice = new PdfPCell(chunk);
		pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(pdfno);
		table.addCell(pdftreatment);
		table.addCell(pdfpest);
		table.addCell(pdffrequency);
		table.addCell(pdfpremise);
		table.addCell(pdfprice);
		
	}
		
		Phrase blank=new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
//		blankCell.setBorder(0);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);
		blankCell.setBorderWidthLeft(0);
		
		
		Phrase blank1=new Phrase(" ",font8);
		PdfPCell blank1Cell = new PdfPCell();
		blank1Cell.addElement(blank1);
//		blankCell.setBorder(0);
//		blank1Cell.setBorderWidthRight(0);
		blank1Cell.setBorderWidthTop(0);
		
		
		
		double subTotal = 0;
		for(int i=0; i<this.dir.getItems().size(); i++){
			
			subTotal = subTotal +(this.dir.getItems().get(i).getPrice()*this.dir.getItems().get(i).getQty());
			
		}
		
		
		
		String name=null;
		double servalue=0;
		for(int i=0;i<dir.getProductTaxes().size();i++){
		if(!dir.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("vat")){
		name = dir.getProductTaxes().get(i).getChargeName();
		servalue =dir.getProductTaxes().get(i).getChargePercent();
		System.out.println("charge %"+servalue);
		System.out.println("charge name "+name);
		}
		}
		Phrase stax = new Phrase(name+"@"+servalue+"%",font8bold);
		PdfPCell staxcell = new PdfPCell(stax);
		staxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		staxcell.setBorderWidthLeft(0);
		staxcell.setBorderWidthTop(0);
		
		
//		String name = dir.getItems().get(0).getServiceTax().getTaxName();
//		double servalue =dir.getItems().get(0).getServiceTax().getPercentage();
		Phrase st = new Phrase("Sub Total",font8bold);
		PdfPCell stcell = new PdfPCell(st);
		stcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		stcell.setBorderWidthLeft(0);
		stcell.setBorderWidthTop(0);
		
		
		Phrase st1 = new Phrase(df.format(subTotal),font8bold);
		PdfPCell st1cell = new PdfPCell(st1);
		st1cell.setBorderWidthTop(0);
		st1cell.setBorderWidthLeft(0);
		st1cell.setBorderWidthBottom(0);
		st1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase tamt = new Phrase("Total Amount",font8bold);
		PdfPCell tamtcell = new PdfPCell(tamt);
		tamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		tamtcell.setBorderWidthLeft(0);
		tamtcell.setBorderWidthTop(0);

		table.addCell(blank1Cell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(stcell);
		table.addCell(st1cell);
		
			PdfPCell pdftaxpercent=null;
			if((dir.getItems().get(0).getServiceTax().getPercentage() !=0)){

				double taxvalue = subTotal*(dir.getItems().get(0).getServiceTax().getPercentage()/100);	
					
				chunk = new Phrase(df.format(taxvalue),font8bold);
				pdftaxpercent = new PdfPCell(chunk);
				pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			}
			else
			{
				chunk = new Phrase(" ",font8);
				pdftaxpercent = new PdfPCell(chunk);
				pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			
			
			
			table.addCell(blank1Cell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(staxcell);
			table.addCell(pdftaxpercent);
			
		
			chunk = new Phrase(df.format(dir.getNetpayable()),font8bold);
			pdfttlamt = new PdfPCell(chunk);
			pdfttlamt.setHorizontalAlignment(Element.ALIGN_CENTER);

			//  rohan added this code 
			String amountInWord = ServiceInvoicePdf.convert(dir.getNetpayable());
			
			String amountInWordWithLowerCase="";
			String[] spliturl=amountInWord.split(" ");
			
			System.out.println("RRR   spliturl.length"+spliturl.length);
			for(int i=1; i < spliturl.length;i++)
			{
				amountInWordWithLowerCase = amountInWordWithLowerCase+spliturl[i].toLowerCase()+" ";
			}
			
			
			
			Phrase inwords = new Phrase("(In words) Rs. "+spliturl[0]+" "+amountInWordWithLowerCase + "only.", font8bold);
			PdfPCell inwordscell = new PdfPCell(inwords);
			inwordscell.setBorderWidthTop(0);
			inwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			table.addCell(blank1Cell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(tamtcell);
			table.addCell(pdfttlamt);
			
			
//		PdfPCell itemcell = new PdfPCell();
//		itemcell.addElement(table);
//		itemcell.setBorder(0);
		
		PdfPTable xx = new PdfPTable(1);
		xx.setWidthPercentage(100f);
		xx.addCell(inwordscell);
		
//		PdfPCell xxtableCell =new PdfPCell();
//		xxtableCell.addElement(xx);
//		xxtableCell.setBorder(0);
		
//		PdfPTable parent = new PdfPTable(1);
//		parent.setWidthPercentage(100);
//		parent.addCell(itemcell);
//		parent.addCell(xxtableCell);
	
		
		try {
			document.add(table);
			document.add(xx);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
}
	
	
}
