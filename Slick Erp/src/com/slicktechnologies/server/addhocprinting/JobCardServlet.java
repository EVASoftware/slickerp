package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class JobCardServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9071272234335148745L;
	
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {

	   JobCardPdf pdf=new JobCardPdf();
	   pdf.document=new Document();
	   Document document = pdf.document;
	   PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	   document.open();
	   String stringid=request.getParameter("Id");
	   stringid=stringid.trim();
	   Long count =Long.parseLong(stringid);
	   pdf.setContract(count);
	   pdf.createPdf();
	   document.close();
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }

}
