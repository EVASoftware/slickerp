package com.slicktechnologies.server.addhocprinting;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.aria.client.MathRole;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.BranchDetails;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ContractRenewalPdf {
	ContractRenewal conRenw;
	Logger logger = Logger.getLogger("NameOfYourLogger");
	Company comp;
	Customer cust;
	Contract con;
	public Document document;
	//int noOfLine=2;// commented by Priyanka
	int noOfLine=6;
//	int noOfLine=8;

	int productcount=0;
	int vat;
	int st;
	boolean checkEmailId=false;
	boolean checkOldFormat=false;
	List<Branch> branchList=new ArrayList<Branch>();//By Jayshree
	List<CompanyPayment>comppayment=new ArrayList<CompanyPayment>();//By Jayshree
	CompanyPayment comppayments;
	private Font font16boldul, font12bold, font8bold, font8,font7bold, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold;

	Phrase chunk;
	PdfPCell pdfcode,pdfname,pdfduration,pdfservices,pdfprice,pdftax,pdfnetPay,pdfPremises;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("#.00");

	boolean oldConDetFlag=false;
	/*
	 * Date:10/11/2018
	 * Developer:Ashwini
	 * 
	 */
	 boolean pecoppflag = false;
	 boolean pepcoppflag = false;
	 Branch branchDt = null;
     boolean branchFlag=false;
     
	 List<ServiceProduct> serviceproductlist = null;
	 boolean hideOldPriceColumnFromPecoppPDF=false;

		
	public ContractRenewalPdf() {
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
	   font7bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//added by ashwini
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	}

	public void setContractRewnewal(ContractRenewal conRenewal){
		conRenw=conRenewal;
		
		if (conRenw.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", conRenw.getCompanyId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", conRenw.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getCustomerId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", conRenw.getContractId()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getContractId()).first().now();
		
		
		/**Date 4/1/2017
		 * By Jayshree
		 * To load the branchlist,comppayment
		 * 
		 */
		branchList=ofy().load().type(Branch.class).filter("companyId",conRenw.getCompanyId()).filter("buisnessUnitName",conRenw.getBranch()).list();

		
		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", conRenw.getCompanyId()).filter(" paymentStatus", true).list();
		
		if (conRenw.getCompanyId() != null) {
			comppayments = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", conRenw.getCompanyId()).first().now();
		}
		
		/**
		 * Date : 05-12-2017 BY ANIL
		 * Loading process configuration,for checking whether to show old contract details or not
		 */
		if(conRenw.getCompanyId()!=null){
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", conRenw.getCompanyId()).filter("processName", "ContractRenewal").filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowOldContractDetails")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						oldConDetFlag=true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OldFormatRenewalLetter")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkOldFormat = true;
					}
					
					/*
					 * Date:10/11/2018
					 * Added by Ashwini
					 * Des:To made changes for pecopp only
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyforPecopp")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pecoppflag = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyforPepcopp")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pepcoppflag = true;
					}
					/*
					 * end by Ashwini
					 */
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("HideOldPriceColumnFromPecoppPDF")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						hideOldPriceColumnFromPecoppPDF=true;
					}
				}
			}
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
				branchFlag=true;
				logger.log(Level.SEVERE,"Process active --");
				if(conRenw !=null && conRenw.getBranch() != null && conRenw.getBranch().trim().length() > 0){
					
					branchDt = ofy().load().type(Branch.class).filter("companyId",conRenw.getCompanyId()).filter("buisnessUnitName", conRenw.getBranch()).first().now();
					
					
					if(branchDt != null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
						logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());	
						List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
						
						if(paymentDt.get(0).trim().matches("[0-9]+")){
							
							
							
							int payId = Integer.parseInt(paymentDt.get(0).trim());
							
							comppayments = ofy().load().type(CompanyPayment.class)
									.filter("count", payId)
									.filter("companyId", conRenw.getCompanyId()).first()
									.now();
							
							
							if(comppayments != null){
								comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
							}
							
						}
						
						
					}
				}
			}
		
		}
		
		
	}



	public void createPdf(String preprintStatus) {
		
		
		/*
		 * Date:10/11/2018
		 * Added by Ashwini
		 * Des:To add preprintstatus for pepcopp
		 */
		if(pecoppflag){
			if(preprintStatus.contains("plane")){
				Createblank();
//				createLogo(document,comp);
				createCompanyAddress();
			}else if(preprintStatus.contains("yes")){
				createBlankforUPC();
			}else if(preprintStatus.contains("no")){
				createBlankforUPC();
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
			}
		}else{
		
		
		if(preprintStatus.contains("plane")){
			Createblank();
//			createLogo(document,comp);
			createCompanyAddress();
		}else if(preprintStatus.contains("yes")){
			createBlankforUPC();
		}else if(preprintStatus.contains("no")){
			createBlankforUPC();
			
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}	
			
		}
		}
		
		/*
		 * end by Ashwini
		 */
		
//		/*
//		 * commented by Ashwini
//		 */
//		if(preprintStatus.contains("plane")){
//			Createblank();
////			createLogo(document,comp);
//			createCompanyAddress();
//		}else if(preprintStatus.contains("yes")){
//			createBlankforUPC();
//		}else if(preprintStatus.contains("no")){
//			createBlankforUPC();
//			if(comp.getUploadHeader()!=null){
//		    	createCompanyNameAsHeader(document,comp);
//			}
//			if(comp.getUploadFooter()!=null){
//				createCompanyNameAsFooter(document,comp);
//			}
//		}
		
		/*
		 * end by Ashwini
		 */
		
	
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg();
		if(checkOldFormat==true){
			if(oldConDetFlag){
				createOldContractProductDetails();
			}
//			createProductInfo();
			
			/*
			 *Date:10/11/2018
			 * Added by Ashwini
			 */
			if(pecoppflag){
				createproductHeading1();
				createProductInfonew1();
				
			}else{
				createproductHeading();
				createProductInfonew();
				cretetotal();

			}

			
		//	cretetotal();
			
//			if(pecoppflag){
//				createnote();
//			}
//			if(pecoppflag){
//				newFooterInfo();
//			}
//			else{
//				footerInfo();
//			}

			/*
			 * end by AShwini
			 */
			
		
			
			/*
			 * commented by Ashwini
			 */
			
//			if(noOfLine==0&&productcount!=0){
//		try {
//			document.add(Chunk.NEXTPAGE);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		 if(preprintStatus.contains("yes")){
//			createBlankforUPC();
//		}else if(preprintStatus.contains("no")){
//			createBlankforUPC();
//			if(comp.getUploadHeader()!=null){
//		    	createCompanyNameAsHeader(document,comp);
//			}
//			if(comp.getUploadFooter()!=null){
//				createCompanyNameAsFooter(document,comp);
//			}
//		}
//		Phrase ref = new Phrase("Annexure  ", font10bold);
//		Paragraph pararef = new Paragraph();
//		pararef.add(ref);
//		pararef.setSpacingAfter(15);
//	   
//	    try {
//			document.add(pararef);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	    
//	    createRemainingProduct(productcount);
//	    
//		}
			
			/*
			 * end by Ashwini
			 */
			
			/*
			 * Date:12/10/2018
			 * Developer:Ashwini
			 */
			
			if(pepcoppflag==false){
				
				if(noOfLine==0&&productcount!=0){
					footerInfo();
					
					try {
						document.add(Chunk.NEXTPAGE);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 if(preprintStatus.contains("yes")){
						createBlankforUPC();
					}else if(preprintStatus.contains("no")){
						createBlankforUPC();
						if(comp.getUploadHeader()!=null){
					    	createCompanyNameAsHeader(document,comp);
						}
						if(comp.getUploadFooter()!=null){
							createCompanyNameAsFooter(document,comp);
						}
					}
					Phrase ref = new Phrase("  ", font10bold);
					Paragraph pararef = new Paragraph();
					pararef.add(ref);
					pararef.setSpacingAfter(15);
				   
				    try {
						document.add(pararef);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
									e.printStackTrace();
					}
				    
				    
				    createRemainingProduct(productcount);
				   // newFooterInfo();
				    
				    
					}
					else{
						footerInfo();
					}
//				cretetotal();
				
			}
			
			if(pecoppflag){
				createnote();
			}
			if(pecoppflag){
				newFooterInfo();
			}
			else{
//				footerInfo();
			}
		}
		
		else{
			
			footerInfo();
			
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if(preprintStatus.contains("yes")){
				createBlankforUPC();
			}else if(preprintStatus.contains("no")){
				createBlankforUPC();
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
			}
			Phrase ref = new Phrase("Annexure  ", font10bold);
			Paragraph pararef = new Paragraph();
			pararef.add(ref);
			pararef.setSpacingAfter(15);
		   
		    try {
				document.add(pararef);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    if(oldConDetFlag){
				createOldContractProductDetails();
			}
//			createProductInfo();
			createproductHeading();
			createProductInfonew();
			cretetotal();
			
			
		}
	    
	    
		
//		createProductInfo();
//		createproductHeading();
//		createProductInfonew();
//		cretetotal();
		
	    
//	    if(checkOldFormat==true){
//		if(noOfLine==0&&productcount!=0){
//			System.out.println("new method");
//			createRemainingProduct(productcount);
//			
//		}
//		}
	}


	private void newFooterInfo() {

		Paragraph blankpara = new Paragraph();
		blankpara.add(Chunk.NEWLINE);
		PdfPCell blankparaCell = new PdfPCell(blankpara);
		blankparaCell.setBorder(0);
		/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/
		//String msg1 = "We seek a nominal increase in our annual services charges partly to meet the hike in our operational costs.";
		String msg2 = "We reiterate our commitment to ensure your complete satisfaction and look forward to hearing from you soon.";
		String msg3 = "Should you require further information on above please call or email us and we would gladly assist you.";
	
		
//		Phrase msg = new Phrase(msg1, font10);
//		PdfPCell msgCell = new PdfPCell(msg);
//		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
//		msgCell.setBorder(0);
		
		Phrase phmsgpart2 = new Phrase(msg2, font10);
		PdfPCell msgpart2Cell = new PdfPCell(phmsgpart2);
		msgpart2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgpart2Cell.setBorder(0);
		
		Phrase phmsgpart3 = new Phrase(msg3, font10);
		PdfPCell msgpart3Cell = new PdfPCell(phmsgpart3);
		msgpart3Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgpart3Cell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		//parent.addCell(blankparaCell);
		//parent.addCell(msgCell);
		parent.addCell(blankparaCell);
		parent.addCell(msgpart2Cell);
		parent.addCell(blankparaCell);
		parent.addCell(msgpart3Cell);
		parent.setSpacingAfter(10f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		PdfPTable parent2=new PdfPTable(1);
		parent2.setWidthPercentage(100);
		
		
		String title2 = "Thank You";
		
		Phrase thank = new Phrase(title2, font10);
		PdfPCell thankCell = new PdfPCell(thank);
		thankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		thankCell.setBorder(0);
		
		String compName="";
		if(branchFlag){
			if(branchDt!=null && branchDt.getCorrespondenceName()!=null){
				compName=branchDt.getCorrespondenceName();
			}else{
				compName="";
			}
		}else{
			compName=comp.getBusinessUnitName();
		}
		
		
		Phrase compn = new Phrase(compName, font10bold);
		PdfPCell compCell = new PdfPCell(compn);
        compCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		compCell.setBorder(0);
		
		parent2.addCell(blankparaCell);
		//parent2.addCell(blankparaCell);
		parent2.addCell(thankCell);
		//parent2.addCell(blankparaCell);
		parent2.addCell(compCell);
		//parent2.addCell(blankparaCell);
		parent2.addCell(blankparaCell);

		String footermsg1 = "This is a computer-generated document and does not carry a signature";
	
		
		Phrase phfootermsg1 = new Phrase(footermsg1, font10);
		PdfPCell phfootermsg1Cell = new PdfPCell(phfootermsg1);
		phfootermsg1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		phfootermsg1Cell.setBorder(0);
		
		parent2.addCell(phfootermsg1Cell);
		parent2.setSpacingAfter(15f);
		
		try {
			document.add(parent2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createRemainingProduct(int productcount2) {
//		try {
//			document.add(Chunk.NEXTPAGE);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		 
//		
//		
//		
//		Phrase ref = new Phrase("Annexure  ", font10bold);
//		Paragraph pararef = new Paragraph();
//		pararef.add(ref);
//		pararef.setSpacingAfter(15);
//		
//		try {
//			document.add(pararef);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		
		if(pecoppflag){
			createproductHeading1();
			
		}else{
			
			createproductHeading();
		}
//		createproductHeading();    //Commented by Ashwini
		
		/**
		 * Date 4/1/2018 
		 * By Jayshree
		 * Des.to add the product details in new tables
		 */
		
		
		
		PdfPTable table2 ;
		if(pecoppflag){
//			 table2 = new PdfPTable(6); //Added by Ashwini
			if(hideOldPriceColumnFromPecoppPDF) {
				table2=new PdfPTable(5);
				/**end**/
				table2.setWidthPercentage(100);
				try {
					table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f});
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}		
			}else {
				table2=new PdfPTable(6);
				/**end**/
				table2.setWidthPercentage(100);
				try {
					table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}else{
			 table2 = new PdfPTable(8);
		}
		
		
		/**end**/
		table2.setWidthPercentage(100);
		try {
			//table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f,1f});
			if(pecoppflag){
//				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f}); //Added by Ashwini
			}else{
				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f,1f});
			}
			
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfUtility pdfutility = new PdfUtility();
		
		for(int i=productcount2;i<conRenw.getItems().size();i++){
//			
//			
//			
////			chunk = new Phrase((i+1)+"",font9);
////			pdfcode = new PdfPCell(chunk);
////			pdfcode.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//		
//			/**
//			 * Date 5-6-2018 by jayshree
//			 * des.to print the old taxincluding amt
//			 */
//			double oldTaxinclAmt;
//			if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
//				oldTaxinclAmt=conRenw.getItems().get(i).getOldProductPrice();
//	     	}
//			else{
//			
//			 oldTaxinclAmt = calculateOldTaxAmt(conRenw.getItems().get(i).getOldProductPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
//					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
//			}
//			
//			/**Manisha added old price cell in table**/
////			Phrase pdfoldprice;
////			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
////				pdfoldprice = new Phrase(conRenw.getItems().get(i).getOldProductPrice() +"",font9);
////			}else{
////				pdfoldprice=new Phrase("");
////			}
//			
//			Phrase pdfoldprice;
//			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
//				pdfoldprice = new Phrase(df.format(Math.round(oldTaxinclAmt))+"" ,font9);
//			}else{
//				pdfoldprice=new Phrase("" ,font9);
//			}
//			
//			
//			
//			
//			/**End******/
//			
//			////
//			if(conRenw.getItems().get(i).getProductName()!=null){
//				chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
//			}else{
//				chunk = new Phrase("");
//			}
//			
//			pdfname = new PdfPCell(chunk);
//			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 
//			 /////
//			if(conRenw.getItems().get(i).getDuration()!=-1){
//				 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
//			}
//			else{
//				 chunk= new Phrase("");
//			}
//			pdfduration = new PdfPCell(chunk);
//			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////
////			if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
////
////					 chunk =new Phrase (conRenw.getItems().get(i).getPrduct().getSpecification()+"",font9);
////				
////			}
////			else{
////				 chunk= new Phrase("");
////			}
//			if(pecoppflag){
//			if(conRenw.getItems().get(i).getPrduct().getSpecification()!=null
//					&& conRenw.getItems().get(i).getPrduct().getSpecification().trim().length()>0){
//			 chunk =new Phrase (conRenw.getItems().get(i).getPrduct().getSpecification()+"",font9);
//			 System.out.println("Product Type::"+conRenw.getItems().get(i).getPrduct().getSpecification());
//			}
//			else{
//				String productmasterspecification = getsepcificationFromProductMaster(conRenw.getItems().get(i).getPrduct().getCount(), serviceproductlist);
//				 chunk =new Phrase (productmasterspecification,font9);
//			}
////			else{
////				chunk= new Phrase("");f
////
////			}
//			}else{
//				if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
//					 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font9);
//				}
//				else{
//					 chunk= new Phrase("");
//				}
//			}
//			pdfservices = new PdfPCell(chunk);
//			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////	
//			
//			/**
//			 * Date 5-6-2018 by jayshree
//			 * des.add new price tax including amt
//			 */
//			
//		double newTaxAmount=0 ;
//	 	
//	 	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
//	 		newTaxAmount=conRenw.getItems().get(i).getPrice();
//	 	}
//	 	else{
//	 	
//	 		newTaxAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
//				conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
//	 	}
//			
//			if(conRenw.getItems().get(i).getPrice()!=0){
//				chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfprice = new PdfPCell(chunk);
//			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////
////			if(conRenw.getItems().get(i).getServiceTax()!=null){
////				chunk = new Phrase(conRenw.getItems().get(i).getServiceTax()+"", font9);
////			}else{
////				 chunk = new Phrase("");
////			}
////			pdfstax = new PdfPCell(chunk);
////			pdfstax.setHorizontalAlignment(Element.ALIGN_CENTER);
////			
////			////
////			if(conRenw.getItems().get(i).getVatTax()!=null){
////				chunk = new Phrase(conRenw.getItems().get(i).getVatTax()+"", font9);
////			}else{
////				 chunk = new Phrase("");
////			}
////			pdfvat = new PdfPCell(chunk);
////			pdfvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//			///// 
//			
//		   	 PdfPCell pdfservice =null;
//	      	 Phrase vatno=null;
//	      	 Phrase stno=null;
//	      	 if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0))
//	      	 {
//	      	 
//	      		 if(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0){
//	      		 if(st==conRenw.getItems().size()){
//	      			 chunk = new Phrase(conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	      		 }
//	      		 else{
//	      			 stno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	      		 }
//	      		 }
//	      	    else
//	      		 chunk = new Phrase("N.A"+"",font9);
//	      		 if(st==conRenw.getItems().size()){
//	      	    pdfservice = new PdfPCell(chunk);
//	      		 }else{
//	      			 pdfservice = new PdfPCell(stno);
//	      		 }
//	      	  
//	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      	 }
//	      	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()==0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0))
//	           {
//	      		 
//	      		 if(conRenw.getItems().get(i).getVatTax()!=null){
//	      			 if(vat==conRenw.getItems().size()){
//	      				 System.out.println("rohan=="+vat);
//	      				 chunk = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+"",font9);
//	      			 }else{
//	      				 System.out.println("mukesh");
//	           	    vatno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
//	      		 }
//	      		 }
//	           	    else{
//	           		 chunk = new Phrase("N.A"+"",font9);
//	           	    }
//	      		 if(vat==conRenw.getItems().size()){
//	           	  pdfservice = new PdfPCell(chunk);
//	      		 }else{ pdfservice = new PdfPCell(vatno);}
//	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      	 }
//	      	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0)){
//	      		 if((conRenw.getItems().get(i).getVatTax()!=null)&& (conRenw.getItems().get(i).getServiceTax()!=null))
//	      			 
//	            	    chunk = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
//	            	    +conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
//	            	 else
//	            		 chunk = new Phrase("N.A"+"",font9);
//	            	  pdfservice = new PdfPCell(chunk);
//	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      		 
//	      	 }else{
//	      		 
//	      		 chunk = new Phrase("N.A"+"",font9);
//	            	  pdfservice = new PdfPCell(chunk);
//	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      		 
//	      	 }
//			
//	      	pdftax = new PdfPCell(chunk);
//	      	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//			
//	      	/**   Date : 24-11-2017 BY MANISHA
//	  	   * Description :To get the total amount on pdf..!!
//	  	   * **/
//	      	
//			double netPayAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
//					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
//			
//			/**Ends**/
//			
//			if(netPayAmount!=0){
//				chunk = new Phrase(df.format(Math.round(netPayAmount))+"", font9);
//			}else{
//				 chunk = new Phrase(" ",font9);
//			}
//			pdfnetPay = new PdfPCell(chunk);
//			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			Phrase startph= null;
//			
//			if(pecoppflag){
//				
//				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
//				Date oldcontractEndDate = con.getEndDate();
//				logger.log(Level.SEVERE,"Date Before Adding  oldcontractEndDate"+oldcontractEndDate);
//				Calendar cal1=Calendar.getInstance();
//				cal1.setTime(oldcontractEndDate);
//				cal1.add(Calendar.DATE, +1);
//				
//				Date newcontractStartDate=null;
//				
//				try {
//					newcontractStartDate=dateFormat.parse(dateFormat.format(cal1.getTime()));
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//				logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
//				
//				Calendar cal=Calendar.getInstance();
//				cal.setTime(newcontractStartDate);
//				cal.add(Calendar.DATE, +conRenw.getItems().get(i).getDuration());
//				
//				Date newcontractEndDate=null;
//				
//				try {
//					newcontractEndDate=dateFormat.parse(dateFormat.format(cal.getTime()));
//				} catch (ParseException e) {
//					e.printStackTrace();
//				}
//				logger.log(Level.SEVERE,"newcontractEndDate"+newcontractEndDate);
//				
//				if(conRenw.getItems().get(i).getStartDate()!=null && conRenw.getItems().get(i).getEndDate()!=null){
//					
//						logger.log(Level.SEVERE,"conRenw.getItems().get(i).getStartDate()"+conRenw.getItems().get(i).getStartDate());
//						logger.log(Level.SEVERE,"conRenw.getItems().get(i).getEndDate()"+conRenw.getItems().get(i).getEndDate());
//
//						startph = new Phrase (fmt.format(conRenw.getItems().get(i).getStartDate())+"\n"+fmt.format(conRenw.getItems().get(i).getEndDate()),font9);
//				}
//				else{
//					
//					if(newcontractStartDate!=null && newcontractEndDate!=null){
//						logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
//						startph = new Phrase (fmt.format(newcontractStartDate)+"\n"+fmt.format(newcontractEndDate),font9);
//					}
//					else{
//						startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
//					}
//					
//				}
////				if(newcontractStartDate!=null && newcontractEndDate!=null){
////					logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
////					startph = new Phrase (fmt.format(newcontractStartDate)+"\n"+fmt.format(newcontractEndDate),font9);
////				}
////				else{
////					startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
////				}
//
//				
//			}
//			else{
//				/**
//				 * Date 4/1/2018
//				 * By Jayshree
//				 * Des.To add the start date and end date
//				 */
//				
//				startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
//				
//			}
//			PdfPCell startcell=new PdfPCell(startph);
//			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//			/**
//			 * @author Vijay Date :- 14-11-2022
//			 * Des :- Ankita need premises so if premises added then it will print
//			 */
//			if(conRenw.getItems().get(i).getPremisesDetails()!=null && !conRenw.getItems().get(i).getPremisesDetails().equals("")){
//
//				chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails(), font9);
//
//				pdfPremises = new PdfPCell(chunk);
//				pdfPremises.setHorizontalAlignment(Element.ALIGN_LEFT);
//				pdfPremises.setColspan(8);
//				
//				table2.addCell(pdfPremises);
//				
//			}
//			/**
//			 * ends here
//			 */
//			
//			table2.addCell(pdfname);
////			table2.addCell(startph);
//			table2.addCell(startcell);
//			table2.addCell(pdfduration);
//			table2.addCell(pdfservices);
//			/**Manisha add column old price **/
//		
//			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
//			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table2.addCell(oldpricevalcell);
//			
//			/**End**/
//			table2.addCell(pdfprice);
//			/*
//			 * Date:16-10-2018
//			 * Developer:Ashwini
//			 */
//			if(pecoppflag==false){
//				table2.addCell(pdftax);
//				table2.addCell(pdfnetPay);
//				
//			}
//			/*
//			 * commented by Ashwini
//			 */
////			table2.addCell(pdftax);
////			table2.addCell(pdfnetPay);
//		
			
			/*
			 * Ashwini Patil 
			 * Date: 22-12-2023
			 * commented old code in for loop
			 * reported issue by Ankita pest: Old and new price of last product showing wrong on renewal letter
			 * due to no of line restriction few products goes to next page.
			 * for next page products this createRemainingProduct method is getting called but code was different at 1st page product table and this product table so copied same code
			 * copied for loop code from method createProductInfoNew method
			 */
			//

						
			Phrase pdfoldprice;
			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(conRenw.getItems().get(i).getOldProductPrice())+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			
			/**End******/

			
			
			////
			if(conRenw.getItems().get(i).getProductName()!=null){
				
				chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
				
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(conRenw.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=conRenw.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+conRenw.getItems().get(i).getPrice());
	     	//End By Jayshreee
			
//			if(conRenw.getItems().get(i).getPrice()!=0){
//				chunk = new Phrase(df.format(newTaxAmount)+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
	     	if(checkOldFormat){
	     		if(conRenw.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(conRenw.getItems().get(i).getPrice())+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}else{
	     		if(conRenw.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(newTaxAmount)+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}
			/**
			 * ends here
			 */
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			

		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0){
//	     		 if(st==conRenw.getItems().size()){
	     			chunktax = new Phrase(conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
//	     		 else{
//	     			 stno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     		 if(st==conRenw.getItems().size()){
	     	    pdfservice = new PdfPCell(chunktax);
//	     		 }else{
//	     			 pdfservice = new PdfPCell(stno);
//	     		 }
	     	     /**
	     		  * ends here
	     		  */
	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()==0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(conRenw.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==conRenw.getItems().size()){
	     			 if(conRenw.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 System.out.println("mukesh");
//	          	    vatno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==conRenw.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((conRenw.getItems().get(i).getVatTax()!=null)&& (conRenw.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	pdftax = new PdfPCell(chunktax);
	     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=conRenw.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
			pdfnetPay = new PdfPCell(netpayable);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
//			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			
			/**
			 * @author Vijay Date :- 27-10-2021
			 * Des :- we have given start date end date to modify on revise and the default logic added here
			 */
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date oldcontractEndDate = con.getEndDate();
			logger.log(Level.SEVERE,"Date Before Adding  oldcontractEndDate"+oldcontractEndDate);
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(oldcontractEndDate);
			cal1.add(Calendar.DATE, +1);
			
			Date newcontractStartDate=null;
			
			try {
				newcontractStartDate=dateFormat.parse(dateFormat.format(cal1.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(newcontractStartDate);
			cal.add(Calendar.DATE, +conRenw.getItems().get(i).getDuration());
			
			Date newcontractEndDate=null;
			
			try {
				newcontractEndDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"newcontractEndDate"+newcontractEndDate);
			
			Phrase startph= null;
			
			/**
			 * @author Vijay Date :- 27-10-2021
			 * Des :- we have given start date end date to modify on revise and the default logic added here
			 */
			if(conRenw.getItems().get(i).getStartDate()!=null && conRenw.getItems().get(i).getEndDate()!=null){
				
				logger.log(Level.SEVERE,"conRenw.getItems().get(i).getStartDate()"+conRenw.getItems().get(i).getStartDate());
				logger.log(Level.SEVERE,"conRenw.getItems().get(i).getEndDate()"+conRenw.getItems().get(i).getEndDate());

				startph = new Phrase (fmt.format(conRenw.getItems().get(i).getStartDate())+"\n"+fmt.format(conRenw.getItems().get(i).getEndDate()),font9);
			}
			else{
			
				if(newcontractStartDate!=null && newcontractEndDate!=null){
					logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
					startph = new Phrase (fmt.format(newcontractStartDate)+"\n"+fmt.format(newcontractEndDate),font9);
				}
				else{
					startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
				}
			}
			
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table2.addCell(pdfname);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			/**Manisha add column old price **/
		
			
			
			
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if(!hideOldPriceColumnFromPecoppPDF)
				table2.addCell(oldpricevalcell);
			
			/**End**/
			table2.addCell(pdfprice);
			table2.addCell(pdftax);
			table2.addCell(pdfnetPay);
			
			
			/**
			 * @author Vijay Date :- 14-11-2022
			 * Des :- Ankita need premises so if premises added then it will print
			 */
			if(conRenw.getItems().get(i).getPremisesDetails()!=null && !conRenw.getItems().get(i).getPremisesDetails().equals("")){
				
				chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails(), font9);

				pdfPremises = new PdfPCell(chunk);
				pdfPremises.setHorizontalAlignment(Element.ALIGN_LEFT);
				pdfPremises.setColspan(8);
				
				table2.addCell(pdfPremises);
			}
			/**
			 * ends here
			 */
			
		}
		
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	private double calculateNewTaxAmt(double newPrice , double serTax , double vatTax,String vatname,String sertaxname) {
		
		
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = newPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = newPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  newPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = newPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
		
		return totalAmt;
	}

	private double calculateOldTaxAmt(double oldPrice , double serTax , double vatTax,String vatname,String sertaxname) {
		
		
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = oldPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = oldPrice * (serTax/100);
				totalAmt = oldPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = oldPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = oldPrice * (serTax/100);
				totalAmt = oldPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  oldPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = oldPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
		
		return totalAmt;
	}

	private void cretetotal() {
		// TODO Auto-generated method stub
		/**   Date : 24-11-2017 BY MANISHA
		   * Description :To get the total amount on pdf..!!
		   * **/
		PdfPTable table3 ;
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
//		PdfPTable table3 = new PdfPTable(8); //commented by Ashwini
		if(pecoppflag){
		 table3 = new PdfPTable(6);//Added by Ashwini
		}else{
		table3 = new PdfPTable(8);
		}
		/**end**/
		table3.setWidthPercentage(100);
		try {
			if(pecoppflag){
				table3.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
			}else{
			table3.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f,1f});
			}
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/**
		 * Date 9/1/2018
		 * By Jayshree
		 * Des.To add the amount in words 
		 */
		
//		Phrase blkph1=new Phrase("");
//		PdfPCell blkcell=new PdfPCell(blkph1);
//		blkcell.setBorderWidthRight(0);
//		table3.addCell(blkcell);
	//	
//		Phrase blkph2=new Phrase("");
//		PdfPCell blkcell2=new PdfPCell(blkph2);
//		blkcell2.setBorderWidthLeft(0);
//		blkcell2.setBorderWidthRight(0);
//		table3.addCell(blkcell2);
//		table3.addCell(blkcell2);
//		table3.addCell(blkcell2);
//		table3.addCell(blkcell2);
//		table3.addCell(blkcell2);
		String amtInWordsVal = "Amount in Words : Rupees "
				+ SalesInvoicePdf.convert(conRenw.getNetPayable())
				+ " Only/-";
		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font1);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		if(pecoppflag){
			amtInWordsValCell.setColspan(4);	
		}else{
			amtInWordsValCell.setColspan(6);
		}
		
		table3.addCell(amtInWordsValCell);
		
		/*
		 * Added by Ashwini
		 */
		
	
			
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table3.addCell(totcell);
		
		Phrase totalval=new Phrase(df.format(conRenw.getNetPayable())+"",font9);		// change by Viraj for decimal on Date 25-10-2018
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table3.addCell(totalvalcell);
		
		
		
//		Phrase totalph=new Phrase("TOTAL",font1);
//		PdfPCell totcell=new PdfPCell(totalph);
//		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		table3.addCell(totcell);
//		
//		Phrase totalval=new Phrase(Math.round(conRenw.getNetPayable())+"",font9);
//		PdfPCell totalvalcell=new PdfPCell(totalval);
//		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		table3.addCell(totalvalcell);
		
		
		try {
			document.add(table3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**End for Manisha**/
	}
	
	/*
	 * Date:10/11/2018
	 * Added by Ashwini
	 * Des:To add note coloumn
	 */
	
	private void createnote(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 10 ,Font.BOLD);
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		String  note = "Note : The service charges are inclusive of applicable taxes" ;
		Phrase notephrase = new Phrase(note,font1);
		PdfPCell notecell = new PdfPCell(notephrase);
		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		notecell.setBorder(0);
		
		Paragraph blankpara = new Paragraph();
		blankpara.add(Chunk.NEWLINE);
		PdfPCell blankparaCell = new PdfPCell(blankpara);
		blankparaCell.setBorder(0);
		
		parent.addCell(blankparaCell);
		parent.addCell(notecell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createProductInfonew1(){
		

		PdfPTable table2 = null;
		
		if(hideOldPriceColumnFromPecoppPDF) {
			table2=new PdfPTable(6);
			/**end**/
			table2.setWidthPercentage(100);
			try {
				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}		
		}else {
			table2=new PdfPTable(7);
			/**end**/
			table2.setWidthPercentage(100);
			try {
				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f});
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		boolean specificationflag = false;
		HashSet<Integer> hsproductid = new HashSet<Integer>();
		for(int i=0;i<conRenw.getItems().size();i++){
			if(conRenw.getItems().get(i).getPrduct().getSpecification()!=null
					&& conRenw.getItems().get(i).getPrduct().getSpecification().trim().length()==0){
				specificationflag = true;
				hsproductid.add(conRenw.getItems().get(i).getPrduct().getCount());
				
			}
		
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date oldcontractEndDate = con.getEndDate();
		logger.log(Level.SEVERE,"Date Before Adding  oldcontractEndDate"+oldcontractEndDate);
		Calendar cal1=Calendar.getInstance();
		cal1.setTime(oldcontractEndDate);
		cal1.add(Calendar.DATE, +1);
		
		Date newcontractStartDate=null;
		
		try {
			newcontractStartDate=dateFormat.parse(dateFormat.format(cal1.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
		
		
		if(specificationflag && hsproductid.size()!=0){
			ArrayList<Integer> productid = new ArrayList<Integer>(hsproductid);
			serviceproductlist = ofy().load().type(ServiceProduct.class).filter("count IN", productid).filter("companyId", comp.getCompanyId()).list();
		}

		for(int i=0;i<conRenw.getItems().size();i++){
			
			
			
			if(checkOldFormat==true){
			if(noOfLine==0){
				System.out.println("no of lins");
				productcount=i;
				break;
			}
			noOfLine=noOfLine-1;
			}

			Phrase pdfoldprice;
			/** Added by Viraj For Amt including Tax Calculation of oldPrice and newPrice **/
			
				double oldPrice = conRenw.getItems().get(i).getOldProductPrice();
				double serOldPriceTax = oldPrice * (conRenw.getItems().get(i).getServiceTax().getPercentage()/100);
				double vatOldPriceTax = oldPrice * (conRenw.getItems().get(i).getVatTax().getPercentage()/100);
				System.out.println("ServiceTax: "+ serOldPriceTax);
				System.out.println("vatTax: "+ vatOldPriceTax);
				oldPrice = Math.round( conRenw.getItems().get(i).getOldProductPrice() + serOldPriceTax + vatOldPriceTax );
			/** Ends **/
			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(oldPrice)+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			System.out.println("oldPrice: "+pdfoldprice);
		
			if(conRenw.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			if(conRenw.getItems().get(i).getPremisesDetails()!=null && !conRenw.getItems().get(i).getPremisesDetails().equals("")){
				chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails() + "", font9);				
			}else {
				chunk = new Phrase("");
			}
			pdfPremises = new PdfPCell(chunk);
			pdfPremises.setHorizontalAlignment(Element.ALIGN_CENTER);
			 /////
			if(conRenw.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
		
				System.out.println("Product Type::"+conRenw.getItems().get(i).getPrduct().getSpecification());
				
				if(conRenw.getItems().get(i).getPrduct().getSpecification()!=null
						&& conRenw.getItems().get(i).getPrduct().getSpecification().trim().length()>0){
				 chunk =new Phrase (conRenw.getItems().get(i).getPrduct().getSpecification()+"",font9);
				 System.out.println("Product Type::"+conRenw.getItems().get(i).getPrduct().getSpecification());
				}
				else{
					String productmasterspecification = getsepcificationFromProductMaster(conRenw.getItems().get(i).getPrduct().getCount(), serviceproductlist);
					 chunk =new Phrase (productmasterspecification,font9);
				}
				
//			
//			else{
//				 chunk= new Phrase("");
//			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=conRenw.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+conRenw.getItems().get(i).getPrice());
	     	logger.log(Level.SEVERE,"newTaxAmount="+newTaxAmount);
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
	     	if(checkOldFormat){
	     		if(conRenw.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}else{
	     		if(conRenw.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}
			/**
			 * ends here
			 */
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
		
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0){
//	     		 if(st==conRenw.getItems().size()){
	     			chunktax = new Phrase(conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
//	     		 else{
//	     			 stno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     		 if(st==conRenw.getItems().size()){
	     	    pdfservice = new PdfPCell(chunktax);
//	     		 }else{
//	     			 pdfservice = new PdfPCell(stno);
//	     		 }
	     	     /**
	     		  * ends here
	     		  */
	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()==0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(conRenw.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==conRenw.getItems().size()){
	     			 if(conRenw.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 System.out.println("mukesh");
//	          	    vatno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==conRenw.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((conRenw.getItems().get(i).getVatTax()!=null)&& (conRenw.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	pdftax = new PdfPCell(chunktax);
	     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=conRenw.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(df.format(netPayAmount)+"", font9);  // change by Viraj for decimal on Date 25-10-2018
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
			pdfnetPay = new PdfPCell(netpayable);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
//			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(newcontractStartDate);
			cal.add(Calendar.DATE, +conRenw.getItems().get(i).getDuration());
			
			Date newcontractEndDate=null;
			
			try {
				newcontractEndDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"newcontractEndDate"+newcontractEndDate);
			
			
			Phrase startph= null;
			
			if(conRenw.getItems().get(i).getStartDate()!=null && conRenw.getItems().get(i).getEndDate()!=null){
				
				logger.log(Level.SEVERE,"conRenw.getItems().get(i).getStartDate()"+conRenw.getItems().get(i).getStartDate());
				logger.log(Level.SEVERE,"conRenw.getItems().get(i).getEndDate()"+conRenw.getItems().get(i).getEndDate());

				startph = new Phrase (fmt.format(conRenw.getItems().get(i).getStartDate())+"\n"+fmt.format(conRenw.getItems().get(i).getEndDate()),font9);
			}
			else{
			
				if(newcontractStartDate!=null && newcontractEndDate!=null){
					logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
					startph = new Phrase (fmt.format(newcontractStartDate)+"\n"+fmt.format(newcontractEndDate),font9);
				}
				else{
					startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
				}
			}
			
			Phrase totalProductAmount =null;
			if(conRenw.getItems().get(i).getRenewalProductNetpayable()!=0){
				totalProductAmount = new Phrase(df.format(conRenw.getItems().get(i).getRenewalProductNetpayable())+"", font9);  // change by Viraj for decimal on Date 25-10-2018
			}else
				totalProductAmount = new Phrase(df.format(conRenw.getItems().get(i).getPrice())+"", font9);  // change by Viraj for decimal on Date 25-10-2018
			PdfPCell totalProductAmountcell=new PdfPCell(totalProductAmount);

			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table2.addCell(pdfname);
			table2.addCell(pdfPremises);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			/**Manisha add column old price **/	
			
			
			
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			if(!hideOldPriceColumnFromPecoppPDF)
				table2.addCell(oldpricevalcell);
			
			/**End**/
			table2.addCell(totalProductAmountcell);
//			table2.addCell(pdfprice);
//			table2.addCell(pdftax);
//			table2.addCell(pdfnetPay);
			
			
			
		}
		
		
		
		if(checkOldFormat==true){
			if(noOfLine==0&&productcount!=0){
				Phrase remainph=new Phrase (" ",font9bold);
				PdfPCell remaincell=new PdfPCell(remainph);
				remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
				remaincell.setColspan(8);
				table2.addCell(remaincell);
			}
		}
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
	}

	private String getsepcificationFromProductMaster(int productId,	List<ServiceProduct> serviceproductlist) {
		if(serviceproductlist!=null && serviceproductlist.size()!=0){
			for(ServiceProduct serviceprod : serviceproductlist){
				if(serviceprod.getCount() == productId){
					if(serviceprod.getSpecification()!=null){
						return serviceprod.getSpecification();
					}
				}
			}
		}
		
		return "";
	}

	private void createProductInfonew() {
//		 double totalExcludingTax=calculateTotalExcludingTax();
//		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
	//	
//		/**Date :25/11/2017 BY: Manisha
//		 * Description:TO add old price column in pdf..!!!
//		 */
//		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f,1f,1f};
	//	
//		PdfPTable table = new PdfPTable(8);
//		/**end**/
//		table.setWidthPercentage(100);
//		try {
//			table.setWidths(relativeWidths);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		 
//		Phrase netPay = new Phrase("NET PAYABLE", font1);
//		Phrase name = new Phrase("NAME", font1);
//		Phrase duration = new Phrase("DURATION",font1);
//		Phrase services = new Phrase("SERVICES", font1);
//		/**Date :25/11/2017 BY: Manisha
//		 * Description:TO add old price column in pdf..!!!
//		 */
//		Phrase oldprice=new Phrase("OLD PRICE",font1);
//		Phrase price = new Phrase("NEW PRICE", font1);
//		/**enD**/
	//	
//		/** Date 4/1/2018
//		 * By jayshree
//		 * Des.add startdate and end date column
//		 */
//		Phrase startdate=new Phrase ("START DATE/END DATE",font1);
	//	
////		Phrase stax = new Phrase("S.TAX", font1);
////		Phrase vat = new Phrase("VAT", font1);
	//	
//		   Phrase servicetax= null;
//		      Phrase servicetax1= null;
//		      int flag=0;
//		      int vat=0;
//		      int st=0;
//		      
//		      /*Date :22/11/2017  By:Manisha
//		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
//		       */
//		   for(int i=0; i<conRenw.getItems().size();i++){
//		    	  
//		    if (conRenw.getItems().get(i).getVatTax().getTaxPrintName()!= null
//							&& !conRenw.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
//		    {
//		    	
//		    	       if(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()!=null
//		    			&& !conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase(""))
//		    			{
//		    		
//		    		       servicetax=new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+"/"
//		    		                    +conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
//		    		       flag=flag+1;
//		    			} 
//		    	       else
//		    	       {
//		    	    	   
//		    	    	   servicetax=new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
//		    	    	   vat=vat+1;
//		    	    	   
//		    	       }
//		    	
//		    	
//		    }
//		    
//		    else{
//		    	   
//		    	if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==0)){
//		  	       servicetax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxName()+"%",font1);
//		  	       vat=vat+1;
//		  	       System.out.println("phrase value===="+servicetax.toString());
//		  	      }
//		    	
	//
//			      else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()==0)
//			    		  &&(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
//			      {
//			      	  servicetax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
//			      	  st=st+1;
//			      }
//		    	
//			      else if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0)
//			    		  && (conRenw.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (conRenw.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
//			    		 && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
//			       {
//			      	  servicetax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxName()+conRenw.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
//			      	  flag=flag+1;
//			      	  System.out.println("flag value;;;;;"+flag);
//			      }
//		    	 
//			    else if ((conRenw.getItems().get(i).getServiceTax().getPercentage()>0) && (conRenw.getItems().get(i).getVatTax().getPercentage()==0)
//			    		&&(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
//			    {
//			    	  servicetax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
//			    	  st=st+1;
//				}
//		    }
//		     
//		      
//		      
////		       else{
////		      	 
////		      	  servicetax = new Phrase("TAX %",font1);
////		       }
//		    }
//		      
//		      
//		      /**End for Manisha**/
//		      
	//	
//		  	PdfPCell celltax = new PdfPCell(servicetax);
//		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
	//	
//		PdfPCell cellname = new PdfPCell(name);
//		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
	//	
//		/** Date 4/1/2018
//		 * By jayshree
//		 * Des.add startdate and end date column
//		 */
//		PdfPCell startdateph = new PdfPCell(startdate);
//		startdateph.setHorizontalAlignment(Element.ALIGN_CENTER);
	//	
//		PdfPCell cellduration = new PdfPCell(duration);
//		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellservices = new PdfPCell(services);
//		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellprice = new PdfPCell(price);
//		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
////		PdfPCell cellvat = new PdfPCell(vat);
////		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
////		PdfPCell cellstax = new PdfPCell(stax);
////		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellNetPay = new PdfPCell(netPay);
//		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
	//	
//		/**Manisha add column old price **/
//		PdfPCell celloldprice=new PdfPCell(oldprice);
//		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		/**End**/
	//	
//		table.addCell(cellname);
//		table.addCell(cellduration);
//		table.addCell(cellservices);
//		/**Manisha add column old price **/
//		table.addCell(celloldprice);
//		/**end*/
//		table.addCell(cellprice);
//		table.addCell(celltax);
//		table.addCell(cellNetPay);
		
		
		
		/**
		 * Date 4/1/2018 
		 * By Jayshree
		 * Des.to add the product details in new tables
		 */
		
		PdfUtility pdfutility = new PdfUtility();
		
		PdfPTable table2 = new PdfPTable(8);
		/**end**/
		table2.setWidthPercentage(100);
		try {
			table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f,1f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int i=0;i<conRenw.getItems().size();i++){
			
			System.out.println("No of lines::"+noOfLine);
			System.out.println("product count::"+productcount);
			
			/*
			 *Date:12/10/2018
			 * Added by Ashwini
			 */
			if(pepcoppflag==false){
				
				if(checkOldFormat==true){
					if(noOfLine==0){
						System.out.println("no of lins");
						productcount=i;
						break;
						
					}
					noOfLine=noOfLine-1;
					}
			}
			
			/*
			 * Commented by Ashwini
			 */
			
//			if(checkOldFormat==true){
//			if(noOfLine==0){
//				System.out.println("no of lins");
//				productcount=i;
//				break;
//				
//			}
//			noOfLine=noOfLine-1;
//			}
		    
			/*
			 * end by Ashwini
			 */
			
//			chunk = new Phrase((i+1)+"",font9);
//			pdfcode = new PdfPCell(chunk);
//			pdfcode.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			/**Manisha added old price cell in table**/
//			Phrase pdfoldprice;
//			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
//				pdfoldprice = new Phrase(conRenw.getItems().get(i).getOldProductPrice() +"",font9);
//			}else{
//				pdfoldprice=new Phrase("");
//			}
//			
//			1/**End******/
		
			/**
			 * Date 04-08-2018 By Vijay
			 * Des As disscussed with sonu old price shold be display from old contract net payable only
			 */
			
//			/**
//			 * Date 5-6-2018 by jayshree
//			 * des.to print the old taxincluding amt
//			 */
//			double oldTaxinclAmt;
//			if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
//				oldTaxinclAmt=conRenw.getItems().get(i).getOldProductPrice();
//	     	}
//			else{
//			
//			 oldTaxinclAmt = calculateOldTaxAmt(conRenw.getItems().get(i).getOldProductPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
//					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
//			}
//			
			Phrase pdfoldprice;
			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(conRenw.getItems().get(i).getOldProductPrice())+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			
			/**End******/

			
			
			////
			if(conRenw.getItems().get(i).getProductName()!=null){
				
				chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
				
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(conRenw.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=conRenw.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+conRenw.getItems().get(i).getPrice());
	     	//End By Jayshreee
			
//			if(conRenw.getItems().get(i).getPrice()!=0){
//				chunk = new Phrase(df.format(newTaxAmount)+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
	     	if(checkOldFormat){
	     		if(conRenw.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(conRenw.getItems().get(i).getPrice())+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}else{
	     		if(conRenw.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(newTaxAmount)+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}
			/**
			 * ends here
			 */
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
//			if(conRenw.getItems().get(i).getServiceTax()!=null){
//				chunk = new Phrase(conRenw.getItems().get(i).getServiceTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfstax = new PdfPCell(chunk);
//			pdfstax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////
//			if(conRenw.getItems().get(i).getVatTax()!=null){
//				chunk = new Phrase(conRenw.getItems().get(i).getVatTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfvat = new PdfPCell(chunk);
//			pdfvat.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			///// 
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0){
//	     		 if(st==conRenw.getItems().size()){
	     			chunktax = new Phrase(conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
//	     		 else{
//	     			 stno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     		 if(st==conRenw.getItems().size()){
	     	    pdfservice = new PdfPCell(chunktax);
//	     		 }else{
//	     			 pdfservice = new PdfPCell(stno);
//	     		 }
	     	     /**
	     		  * ends here
	     		  */
	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()==0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(conRenw.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==conRenw.getItems().size()){
	     			 if(conRenw.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 System.out.println("mukesh");
//	          	    vatno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==conRenw.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((conRenw.getItems().get(i).getVatTax()!=null)&& (conRenw.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	pdftax = new PdfPCell(chunktax);
	     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=conRenw.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
			pdfnetPay = new PdfPCell(netpayable);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
//			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			
			/**
			 * @author Vijay Date :- 27-10-2021
			 * Des :- we have given start date end date to modify on revise and the default logic added here
			 */
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			Date oldcontractEndDate = con.getEndDate();
			logger.log(Level.SEVERE,"Date Before Adding  oldcontractEndDate"+oldcontractEndDate);
			Calendar cal1=Calendar.getInstance();
			cal1.setTime(oldcontractEndDate);
			cal1.add(Calendar.DATE, +1);
			
			Date newcontractStartDate=null;
			
			try {
				newcontractStartDate=dateFormat.parse(dateFormat.format(cal1.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(newcontractStartDate);
			cal.add(Calendar.DATE, +conRenw.getItems().get(i).getDuration());
			
			Date newcontractEndDate=null;
			
			try {
				newcontractEndDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE,"newcontractEndDate"+newcontractEndDate);
			
			Phrase startph= null;
			
			/**
			 * @author Vijay Date :- 27-10-2021
			 * Des :- we have given start date end date to modify on revise and the default logic added here
			 */
			if(conRenw.getItems().get(i).getStartDate()!=null && conRenw.getItems().get(i).getEndDate()!=null){
				
				logger.log(Level.SEVERE,"conRenw.getItems().get(i).getStartDate()"+conRenw.getItems().get(i).getStartDate());
				logger.log(Level.SEVERE,"conRenw.getItems().get(i).getEndDate()"+conRenw.getItems().get(i).getEndDate());

				startph = new Phrase (fmt.format(conRenw.getItems().get(i).getStartDate())+"\n"+fmt.format(conRenw.getItems().get(i).getEndDate()),font9);
			}
			else{
			
				if(newcontractStartDate!=null && newcontractEndDate!=null){
					logger.log(Level.SEVERE,"newcontractStartDate"+newcontractStartDate);
					startph = new Phrase (fmt.format(newcontractStartDate)+"\n"+fmt.format(newcontractEndDate),font9);
				}
				else{
					startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
				}
			}
			
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table2.addCell(pdfname);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			/**Manisha add column old price **/
		
			
			
			
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(oldpricevalcell);
			
			/**End**/
			table2.addCell(pdfprice);
			table2.addCell(pdftax);
			table2.addCell(pdfnetPay);
			
			
			/**
			 * @author Vijay Date :- 14-11-2022
			 * Des :- Ankita need premises so if premises added then it will print
			 */
			if(conRenw.getItems().get(i).getPremisesDetails()!=null && !conRenw.getItems().get(i).getPremisesDetails().equals("")){
				
				chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails(), font9);

				pdfPremises = new PdfPCell(chunk);
				pdfPremises.setHorizontalAlignment(Element.ALIGN_LEFT);
				pdfPremises.setColspan(8);
				
				table2.addCell(pdfPremises);
				noOfLine = noOfLine-1;
			}
			/**
			 * ends here
			 */
			
		}
		
		/*
		 * commented by Ashwini
		 */
//		if(checkOldFormat==true){
//			if(noOfLine==0&&productcount!=0){
//				Phrase remainph=new Phrase ("Please Refer annexure For Remaining Product",font9bold);
//				PdfPCell remaincell=new PdfPCell(remainph);
//				remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				remaincell.setColspan(8);
//				table2.addCell(remaincell);
//			}
//		}
		
		/*
		 * Date:12/10/2018
		 * Developer:Ashwini
		 * Des:To add products in same page(only for pepcop)
		 */
		if(pepcoppflag==false){
			
			if(checkOldFormat==true){
				if(noOfLine==0&&productcount!=0){
					//Please Refer annexure For Remaining Product
					Phrase remainph=new Phrase (" ",font9bold);
					PdfPCell remaincell=new PdfPCell(remainph);
					remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
					remaincell.setColspan(8);
					table2.addCell(remaincell);
				}
			}
			
		}
		
		/*
		 * end by Ashwini
		 */
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		  
		
//		PdfPCell prodtablecell=new PdfPCell();
////		prodtablecell.addElement(table);
//		prodtablecell.setBorder(0);
	//	
	//	
//		PdfPTable parent=new PdfPTable(1);
//		parent.setWidthPercentage(100);
	//	
//		parent.addCell(prodtablecell);
//		parent.setSpacingAfter(15f);
	//	
//		try {
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		 
	}
	
	
	/*
	 *Date:12/10/2018
	 * Added by Ashwini
	 * Des:To change the heading for pecopp
	 */
	
	private void createproductHeading1(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f,1f};
		
		PdfPTable table = null;
		
		if(hideOldPriceColumnFromPecoppPDF) {
			table=new PdfPTable(6);
			float[] relativeWidth = {2.5f,1.5f,1f,1f,1f,1f};
			/**end**/
			table.setWidthPercentage(100);
			try {
				table.setWidths(relativeWidth);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		}else {
			table=new PdfPTable(7);
			/**end**/
			table.setWidthPercentage(100);
			try {
				table.setWidths(relativeWidths);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
		
//		Phrase name = new Phrase("NAME", font1);
//		PdfPCell cellname = new PdfPCell(name);
//		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		Phrase startdate=new Phrase ("START/END DATE",font1);
//		PdfPCell startdateph = new PdfPCell(startdate);
//		startdateph.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		
//		Phrase duration = new Phrase("DURATION",font1);
//		PdfPCell cellduration = new PdfPCell(duration);
//		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		Phrase services = new Phrase("SERVICES", font1);
//		PdfPCell cellservices = new PdfPCell(services);
//		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		Phrase oldprice=new Phrase("PREVIOUS PRICE",font1);
//		PdfPCell celloldprice=new PdfPCell(oldprice);
//		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		Phrase price = new Phrase("NEW PRICE", font1);
//		PdfPCell cellprice = new PdfPCell(price);
//		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase name = new Phrase("Services with frequency", font1);
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase premises=new Phrase ("Premises",font1);
		PdfPCell premisesph = new PdfPCell(premises);
		premisesph.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase startdate=new Phrase ("New contract period",font1);
		PdfPCell startdateph = new PdfPCell(startdate);
		startdateph.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase duration = new Phrase("Contract duration in days",font1);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase services = new Phrase("Treatments", font1);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase oldprice=new Phrase("Last year�s service charges INR",font1);
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase price = new Phrase("Proposed service charges INR", font1);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(cellname);
		table.addCell(premisesph);
		table.addCell(startdateph);
		table.addCell(cellduration);
		table.addCell(cellservices);
		if(!hideOldPriceColumnFromPecoppPDF)
			table.addCell(celloldprice);
		table.addCell(cellprice);
		
		try{
			document.add(table);
			
		}catch(DocumentException e1){
			e1.printStackTrace();
			
		}
		
}

	/*
	 * end by Ashwini
	 */
	private void createproductHeading() {
		
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f,1f,1f};
		
		PdfPTable table = new PdfPTable(8);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		
		//Phrase oldprice=new Phrase("OLD PRICE",font1);  //commented by Ashwini
		/*
		 * Date:12/10/2018
		 * Developer:Ashwini
		 */
		Phrase oldprice = null;
		if(pepcoppflag){
			 oldprice=new Phrase("PREVIOUS PRICE",font1);
		}else{
			 oldprice=new Phrase("OLD PRICE",font1);
		}
		
		/*
		 * end by Ashwini
		 */
		Phrase price = new Phrase("NEW PRICE", font1);
		/**enD**/
		
		/** Date 4/1/2018
		 * By jayshree
		 * Des.add startdate and end date column
		 */
		Phrase startdate=new Phrase ("START/END DATE",font1);
		
//		Phrase stax = new Phrase("S.TAX", font1);
//		Phrase vat = new Phrase("VAT", font1);
		
		   Phrase servicetax= null;
		      Phrase servicetax1= null;
		      int flag=0;
		      int vat=0;
		      int st=0;
		      
		      /*Date :22/11/2017  By:Manisha
		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
		       */
		   for(int i=0; i<conRenw.getItems().size();i++){
		    	  
		    if (conRenw.getItems().get(i).getVatTax().getTaxPrintName().trim()!= null
							&& !conRenw.getItems().get(i).getServiceTax().getTaxPrintName().trim().equalsIgnoreCase(""))
		    {
		    	
		    	       if(conRenw.getItems().get(i).getVatTax().getTaxPrintName().trim()!=null
		    			&& !conRenw.getItems().get(i).getServiceTax().getTaxPrintName().trim().equalsIgnoreCase(""))
		    			{
		    		
		    		       servicetax=new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+"/"
		    		                    +conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
		    		       flag=flag+1;
		    			} 
		    	       else
		    	       {
		    	    	   
		    	    	   servicetax=new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
		    	    	   vat=vat+1;
		    	    	   
		    	       }
		    	
		    	
		    }
		    
		    else{
		    	   
		    	if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==0)){
		  	       servicetax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxName()+"%",font1);
		  	       vat=vat+1;
		  	       System.out.println("phrase value===="+servicetax.toString());
		  	      }
		    	

			      else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()==0)
			    		  &&(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
			      {
			      	  servicetax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  st=st+1;
			      }
		    	
			      else if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0)
			    		  && (conRenw.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (conRenw.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
			    		 && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
			       {
			      	  servicetax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxName()+conRenw.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  flag=flag+1;
			      	  System.out.println("flag value;;;;;"+flag);
			      }
		    	 
			    else if ((conRenw.getItems().get(i).getServiceTax().getPercentage()>0) && (conRenw.getItems().get(i).getVatTax().getPercentage()==0)
			    		&&(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
			    {
			    	  servicetax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
			    	  st=st+1;
				}else{
			    	  servicetax = new Phrase(" ",font1);

				}
		    }
		     
		      
		      
//		       else{
//		      	 
//		      	  servicetax = new Phrase("TAX %",font1);
//		       }
		    }
		      
		      
		      /**End for Manisha**/
		      
		
		  	PdfPCell celltax = new PdfPCell(servicetax);
		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/** Date 4/1/2018
		 * By jayshree
		 * Des.add startdate and end date column
		 */
		PdfPCell startdateph = new PdfPCell(startdate);
		startdateph.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellvat = new PdfPCell(vat);
//		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellstax = new PdfPCell(stax);
//		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/**Manisha add column old price **/
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**End**/
		
		table.addCell(cellname);
		table.addCell(startdateph);
		table.addCell(cellduration);
		table.addCell(cellservices);
		/**Manisha add column old price **/
		table.addCell(celloldprice);
		/**end*/
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createOldContractProductDetails() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		Phrase oldContDetPh = new Phrase("Old contract details : ", font9bold);
		PdfPCell oldContDetCell = new PdfPCell(oldContDetPh);
		oldContDetCell.setBorder(0);
		
		
		
		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f};
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		Phrase price = new Phrase("PRICE", font1);
		
		
		
		HashSet<String> taxName=new HashSet<String>();
		for(ProductOtherCharges tax:con.getProductTaxes()){
			taxName.add(tax.getChargeName().trim());
		}
		Iterator<String> it = taxName.iterator();
		String taxHeader="";
	    while(it.hasNext()){
	    	taxHeader=taxHeader+it.next()+"/";
	    }
	    if(!taxHeader.equals("")){
	    	taxHeader=taxHeader.substring(0,taxHeader.length()-1);
	    }
	    Phrase servicetax= new Phrase(taxHeader,font1);	
	  	PdfPCell celltax = new PdfPCell(servicetax);
	  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellname);
		table.addCell(cellduration);
		table.addCell(cellservices);
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		for(int i=0;i<con.getItems().size();i++){
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(con.getItems().get(i).getPrice()+"", font9);
			}else{
				chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if ((con.getItems().get(i).getVatTax().getPercentage() == 0)
					&& (con.getItems().get(i).getServiceTax().getPercentage() > 0)) {

				if (con.getItems().get(i).getServiceTax().getPercentage() != 0) {
						chunk = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+ "", font9);
				}else{
					chunk = new Phrase("N.A" + "", font9);
				}
			} else if ((con.getItems().get(i).getServiceTax().getPercentage() == 0)
					&& (con.getItems().get(i).getVatTax().getPercentage() > 0)) {

				if (con.getItems().get(i).getVatTax() != null) {
					chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+ "", font9);
				} else {
					chunk = new Phrase("N.A" + "", font9);
				}
			} else if ((con.getItems().get(i).getServiceTax().getPercentage() > 0)
					&& (con.getItems().get(i).getVatTax().getPercentage() > 0)) {
				if ((con.getItems().get(i).getVatTax() != null)&& (con.getItems().get(i).getServiceTax() != null)){
					chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()
							+ ""+ " / "+ ""+ con.getItems().get(i).getServiceTax().getPercentage(), font9);
				}else{
					chunk = new Phrase("N.A" + "", font9);
				}
			} else {
				chunk = new Phrase("N.A" + "", font9);
			}
			pdftax = new PdfPCell(chunk);
			pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);

			double netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
			
			
			if(netPayAmount!=0){
				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				chunk = new Phrase(" ",font9);
			}
			pdfnetPay = new PdfPCell(chunk);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table.addCell(pdfname);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			
			table.addCell(pdfprice);
			table.addCell(pdftax);
			table.addCell(pdfnetPay);
		}
		

		Phrase blkph1=new Phrase("");
		PdfPCell blkcell=new PdfPCell(blkph1);
		blkcell.setBorderWidthRight(0);
		table.addCell(blkcell);
		
		Phrase blkph2=new Phrase("");
		PdfPCell blkcell2=new PdfPCell(blkph2);
		blkcell2.setBorderWidthLeft(0);
		blkcell2.setBorderWidthRight(0);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
//		table.addCell(blkcell2);
		
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(totcell);
		
//		Phrase totalval=new Phrase(" ",font9);
		Phrase totalval=new Phrase(Math.round(con.getNetpayable())+"",font9);
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(totalvalcell);
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.addCell(oldContDetCell);
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		
		DocumentUpload  document= comp.getUploadHeader();
		
		
		
		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//		
//		try
//		{
//		Image image1=Image.getInstance("images/header.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		
		 DocumentUpload document= comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,10f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/pestomatic _letterhead-1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createBlankforUPC() {
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			//document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPdfAttachment(Contract con,Company company,ContractRenewal conRen,Customer cust){
		this.con=con;
		comp=company;
		conRenw=conRen;
		this.cust=cust;
		branchList=ofy().load().type(Branch.class).filter("companyId",conRenw.getCompanyId()).filter("buisnessUnitName",conRenw.getBranch()).list();
		branchFlag =false;
		
		logger.log(Level.SEVERE,"branchFlag value--"+branchFlag);
		
		
		if (conRenw.getCompanyId() != null) {
			comppayments = ofy().load().type(CompanyPayment.class)
					.filter("paymentDefault", true)
					.filter("companyId", conRenw.getCompanyId()).first().now();
		}
		
		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", conRenw.getCompanyId()).filter(" paymentStatus", true).list();
		
		logger.log(Level.SEVERE,"Process active BranchAsCompany11111111111--"+branchFlag);
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			branchFlag=true;
			logger.log(Level.SEVERE,"Process active BranchAsCompany--"+branchFlag);
			
			if(conRenw !=null && conRenw.getBranch() != null && conRenw.getBranch().trim().length() > 0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",conRenw.getCompanyId()).filter("buisnessUnitName", conRenw.getBranch()).first().now();
				
				
				if(branchDt != null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
					
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());	
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayments = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", conRenw.getCompanyId()).first()
								.now();
						
						
						if(comppayments != null){
							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
						}
						
					}
					
					
				}
			}
		}
		
		
		
		
		
		
		
		
		Createblank();
//		createLogo(document,comp);
		createCompanyAddress();
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg();
		createProductInfo();
		footerInfo();
		
	}



	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	public  void createCompanyAddress()
	{
		
		/**
		 * Date 3/1/2018
		 * By jayshree
		 * Des.To add the logo at proper position
		 */
		
		DocumentUpload logodocument = comp.getLogo();
		
		 

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

//		 Image image1=null;
//		 try
//		 {
//		 image1=Image.getInstance("images/ipclogo4.jpg");
//		 image1.scalePercent(20f);
//		 // image1.setAbsolutePosition(40f,765f);
//		 // doc.add(image1);
	//	
//		 imageSignCell=new PdfPCell();
//		 imageSignCell.addElement(image1);
//		 imageSignCell.setFixedHeight(20);
//		 imageSignCell.setBorder(0);
//		 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		 }
//		 catch(Exception e)
//		 {
//		 e.printStackTrace();
//		 }

		
		 PdfPTable logotab=new PdfPTable(1);
		 logotab.setWidthPercentage(100);
		 
		 if (imageSignCell != null) {
			 logotab.addCell(imageSignCell);
			} 
		 else {
				Phrase blank = new Phrase(" ");
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setBorder(0);
				logotab.addCell(blankCell);
			}
		
		 //End by jayshree
		 String compNamee="";
		if (branchFlag) {
			if (branchDt.getCorrespondenceName() != null) {
				compNamee = branchDt.getCorrespondenceName();
			} else {
				compNamee = comp.getBusinessUnitName();
			}

		}else{
			 compNamee=comp.getBusinessUnitName();
		 }
		 Phrase companyName= new Phrase(compNamee,font16bold);
		 
//	     Paragraph p =new Paragraph();
//	     p.add(Chunk.NEWLINE);
//	     p.add(companyName);
//	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell(companyName);
	     companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     companyNameCell.setFixedHeight(0);
	    
//	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
			if(comp.getAddress().getAddrLine2()!=null){
				addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
			}
			else{
				addressline1=comp.getAddress().getAddrLine1();
			}
		
		
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		
		/**
		 * Date 10/1/2018 
		 * By jayshree
		 * Des.to check the null condition for pin number remove the comma from add
		 */
		String pinno;
		
			if(comp.getAddress().getPin()!=0){
				pinno=","+comp.getAddress().getPin()+",";
			}
			else
			{
				pinno="  ";
			}	
		
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+
				      pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()
				      +pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()
				      +pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()
				      +pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		/**
		 * Date 4/1/2018
		 * By jayshree
		 * To handle the null pointer condition
		 */
		
		String mobile="";
		String landline="";
		String email="";
		
		

			if(comp.getCellNumber1()!=0){
				mobile=comp.getCellNumber1()+"";
			}
			else{
				mobile="";
			}	
		
		
		
		
		if(comp.getLandline()!=0){
			landline=comp.getLandline()+"";
		}
		else{
			landline="";
		}
		
		
		String branchmail = "";
		ServerAppUtility serverApp = new ServerAppUtility();
          if(branchFlag){
        	  if(branchDt.getEmail()!=null){
        		  branchmail=branchDt.getEmail();
        	  }else{
        		  branchmail="";
        	  }
          }else{
        	  if (checkEmailId == true) {
      			branchmail = serverApp.getBranchEmail(comp,
      					conRenw.getBranch());
      			System.out.println("server method " + branchmail);

      		} else {
      			branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
      			System.out.println("server method 22" + branchmail);
      		}  
          }
		
		
		if(branchmail!=null){
			email=branchmail;
		}
		else{
			email="";
		}
//		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Phrase contactinfo=new Phrase("Phone: "+mobile+"  Email: "+email,font10);

		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
	//	
		/**
		 * Date 4/1/2018
		 * By jayshree
		 * Des.To add the logo at proper position
		 */
		
		PdfPTable parent=new PdfPTable(2);
		parent.setWidthPercentage(100);
		
		try {
			parent.setWidths(new float[]{30,70});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell logocell=new PdfPCell(logotab);
		logocell.setBorderWidthTop(0);
		logocell.setBorderWidthLeft(0);
		logocell.setBorderWidthRight(0);
		parent.addCell(logocell);
		
		//End By Jayshree
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		comapnyCell.setBorderWidthTop(0);
		comapnyCell.setBorderWidthLeft(0);
		comapnyCell.setBorderWidthRight(0);
		comapnyCell.setFixedHeight(0);
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}




	private void createHeading() {
		String title1 = "";
		if(pecoppflag){
			title1 = "Renewal Offer";
		}
		else{
			title1 = "Renewal Letter";
		}

		Phrase titlephrase = new Phrase(title1, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorder(0);
		titlepdfcell.setPaddingTop(10);//5-08-2024
		titlepdfcell.addElement(Chunk.NEWLINE);

		
	/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/	
//		Phrase custInfo = new Phrase("Customer Id :", font10);
//		PdfPCell custInfoCell = new PdfPCell(custInfo);
//		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		custInfoCell.setBorder(0);
//		
//		
//		
//		
//		
//		Phrase custInfovalue = new Phrase(conRenw.getCustomerId()+"", font10);
//		PdfPCell custInfovalueCell = new PdfPCell(custInfovalue);
//		custInfovalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		custInfovalueCell.setBorder(0);
		
		Phrase custInfo = new Phrase("Contract Id  :", font10);
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		Phrase dvalue;
		dvalue=new Phrase(conRenw.getContractId()+"", font10);
		
		
		//Phrase custInfovalue = new Phrase(conRenw.getContractId()+"", font10);
		PdfPCell custInfovalueCell = new PdfPCell(dvalue);
		custInfovalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfovalueCell.setBorder(0);
		
		
		
		Phrase date = new Phrase("Date :", font10);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setBorder(0);
		
		Phrase datevalue;
		if(pecoppflag == true){
			datevalue = new Phrase(fmt.format(new Date())+"", font10);
		}else{
			 datevalue = new Phrase(fmt.format(conRenw.getDate())+"", font10);
		}
		
		
		PdfPCell datevalueCell = new PdfPCell(datevalue);
		datevalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datevalueCell.setBorder(0);
		
		
		
		
		
		
		float[] relativeWidths1 = {1.2f,5.2f,1f,2.4f};
		
		PdfPTable headTbl=new PdfPTable(4);
		headTbl.setWidthPercentage(100f);
		try {
			headTbl.setWidths(relativeWidths1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		headTbl.addCell(custInfoCell);
		headTbl.addCell(custInfovalueCell);
		headTbl.addCell(dateCell);
		headTbl.addCell(datevalueCell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell1=new PdfPCell(titlepdfcell);
		titlePdfCell1.setBorder(0);
		
		PdfPCell headIfo=new PdfPCell(headTbl);
		headIfo.setBorder(0);
		
		parent.addCell(titlePdfCell1);
		parent.addCell(headIfo);
		
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createCustomerDetails() {
		
		Phrase to = new Phrase("To,", font10);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		toCell.setBorder(0);
		
		Phrase custInfo ;
		if(cust.isCompany()){
			custInfo = new Phrase(cust.getCompanyName(), font10);
		}else{
			custInfo = new Phrase(cust.getFullname(), font10);
		}
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			if(cust.getAdress().getAddrLine2()!=null){
				if(cust.getAdress().getLandmark()!=null){
//					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
					custAdd1=cust.getAdress().getAddrLine1();
					if(!cust.getAdress().getAddrLine2().equals("")){
						custAdd1 +=","+cust.getAdress().getAddrLine2();
					}
					if(!cust.getAdress().getLandmark().equals("")){
						custAdd1 +=","+cust.getAdress().getLandmark();
					}
				}else{
//					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
					custAdd1=cust.getAdress().getAddrLine1();
					if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
						custAdd1=","+cust.getAdress().getAddrLine2();
					}
				}
			}else{
				if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			/**
			 * Date 4/1/2018
			 * By jayshree
			 * Des.To change the sequence of country ,state ,city	And handle the null pointer for pin number	
			 */
			
			String pin="";
			if(cust.getAdress().getPin()!=0){
				pin=cust.getAdress().getPin()+"";
			}
			else{
				pin="";
			}
			if(cust.getAdress().getLocality()!=null){
//				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
				
//				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+cust.getAdress().getLocality()+" "+pin;
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry(); 
				if(cust.getAdress().getLocality()!=null && !cust.getAdress().getLocality().equals("")){
					custFullAdd1 += "\n"+cust.getAdress().getLocality()+" "+pin;
				}
				else{
					custFullAdd1 += "\n"+pin;
				}
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+" "+pin;
			}
		}
		
		
		Phrase custAddInfo = new Phrase(custFullAdd1, font10);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		
		PdfPTable customerTable=new PdfPTable(1);
		customerTable.setWidthPercentage(100);
		
		customerTable.addCell(toCell);
		customerTable.addCell(custInfoCell);
		customerTable.addCell(custAddInfoCell);
		
		
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(customerTable);
		customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		customerCell.setBorder(0);
		
		
		
		PdfPTable headTbl=new PdfPTable(2);
		headTbl.setWidthPercentage(100f);
		
		headTbl.addCell(customerCell);
		headTbl.addCell(blankCell);
		
		PdfPCell parentCell = new PdfPCell();
		parentCell.addElement(headTbl);
		parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		parentCell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(parentCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
	}

	private void createSubjectAndMsg() {
		
		Phrase sub=null;
//		if(pecoppflag || pepcoppflag){
//			 sub = new Phrase("Subject : Renewal of Contract Id "+conRenw.getContractId()+" ending on "+fmt.format(con.getEndDate()), font10bold);
//		}else{
//			 sub = new Phrase("Subject : Renewal of Contract Id "+conRenw.getContractId()+" ending on "+fmt.format(con.getEndDate()), font10);
//		}
		
		/**
		 * Date 21/12/2020
		 * By Priyanka Bhagwat
		 * Des.Change in Subject of Contract Renewal Pdf as a requirement of PECOP
		 */
		
		if(pepcoppflag){
			 sub = new Phrase("Subject : Proposal for renewal of your Contract Id "+conRenw.getContractId()+" ending on "+fmt.format(con.getEndDate()), font10bold);
		}
		else if(pecoppflag){
			 sub = new Phrase("Subject : Renewal offer for your pest management contract ending on "+fmt.format(con.getEndDate()), font10bold);
		}
		else{
			 sub = new Phrase("Subject : Proposal for renewal of your Contract Id "+conRenw.getContractId()+" ending on "+fmt.format(con.getEndDate()), font10);
		}
		
		/**
		 *  End
		 */
		
//		Phrase sub = new Phrase("Subject : Renewal of Contract Id "+conRenw.getContractId()+" ending on "+fmt.format(con.getEndDate()), font10);
		PdfPCell subCell = new PdfPCell(sub);
		subCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		subCell.setBorder(0);
		
		Phrase blank = new Phrase("", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell.setBorder(0);
		
		Phrase dear = new Phrase("Dear Sir/Madam,", font10bold);
		PdfPCell dearCell = new PdfPCell(dear);
		dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dearCell.setBorder(0);
		
		Paragraph blankpara = new Paragraph();
		blankpara.add(Chunk.NEWLINE);
		PdfPCell blankparaCell = new PdfPCell(blankpara);
		blankparaCell.setBorder(0);
		
//		String title1 = "";
//		title1 = "It has been our privilege to have served you over the past year and we truly appreciate & value our association with you. We trust you have found our services exemplery & to your complete satisfaction. Your current contract for following services expires on "+fmt.format(con.getEndDate())+" and we request you to renew the same. Renewal charges are mentioned below.";
		
		/**
		 * Date 21/12/2020
		 * By Priyanka Bhagwat
		 * Des.Gramatically change in Contract Renewal Pdf as a requirement of PECOP
		 */
		
		String title1 = "";
		if(pecoppflag){
			title1 = "We are privileged to have served your pest management requirements over the past year.  We genuinely appreciate and value our association with you. We trust you have found our services exemplary and to your complete satisfaction.";
		}
		else{
			title1 = "We are privileged to have served you over the past year. We truly appreciate and value our association with you. We trust you have found our services exemplary and to your complete satisfaction. Your current contract for following services with us is valid till "+fmt.format(con.getEndDate())+" and we request you to renew the same for continuation of our services.We have shared our service charges for renewal below.";
		}
		
		/**
		 *  End
		 */
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		String title2 = "";
		if(pecoppflag){
			title2 = "As your current contract with us is valid till "+fmt.format(con.getEndDate())+", we request you to renew it with following terms for continuation of our services.";
		}

		Phrase phmsgpart2 = new Phrase(title2, font10);
		PdfPCell msgpart2Cell = new PdfPCell(phmsgpart2);
		msgpart2Cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgpart2Cell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(subCell);
		parent.addCell(blankCell);
		parent.addCell(blankparaCell);
		parent.addCell(dearCell);
		parent.addCell(blankparaCell);

		parent.addCell(msgCell);
		if(pecoppflag){
			parent.addCell(msgpart2Cell);
		}
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createProductInfo() {
//		 double totalExcludingTax=calculateTotalExcludingTax();
		
		
		/**
		 * Date : 06-12-2017 BY ANIL
		 */
		Phrase newContDetPh = new Phrase("New contract details : ", font9bold);
		PdfPCell newContDetCell = new PdfPCell(newContDetPh);
		newContDetCell.setBorder(0);
		/**
		 * End
		 */
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f,1f};
		
		PdfPTable table = new PdfPTable(7);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		Phrase oldprice=new Phrase("OLD PRICE",font1);
		Phrase price = new Phrase("NEW PRICE", font1);
		/**enD**/
		
//		Phrase stax = new Phrase("S.TAX", font1);
//		Phrase vat = new Phrase("VAT", font1);
		
		   Phrase servicetax= null;
		      Phrase servicetax1= null;
		      int flag=0;
		      int vat=0;
		      int st=0;
		      
		      /*Date :22/11/2017  By:Manisha
		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
		       */
		   for(int i=0; i<conRenw.getItems().size();i++){
		    	  
		    if (conRenw.getItems().get(i).getVatTax().getTaxPrintName()!= null
							&& !conRenw.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
		    {
		    	
		    	       if(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()!=null
		    			&& !conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase(""))
		    			{
		    		
		    		       servicetax=new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+"/"
		    		                    +conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
		    		       flag=flag+1;
		    			} 
		    	       else
		    	       {
		    	    	   
		    	    	   servicetax=new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
		    	    	   vat=vat+1;
		    	    	   
		    	       }
		    	
		    	
		    }
		    
		    else{
		    	   
		    	if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==0)){
		  	       servicetax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxName()+"%",font1);
		  	       vat=vat+1;
		  	       System.out.println("phrase value===="+servicetax.toString());
		  	      }
		    	

			      else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()==0)
			    		  &&(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
			      {
			      	  servicetax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  st=st+1;
			      }
		    	
			      else if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0)
			    		  && (conRenw.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (conRenw.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
			    		 && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
			       {
			      	  servicetax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxName()+conRenw.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  flag=flag+1;
			      	  System.out.println("flag value;;;;;"+flag);
			      }
		    	 
			    else if ((conRenw.getItems().get(i).getServiceTax().getPercentage()>0) && (conRenw.getItems().get(i).getVatTax().getPercentage()==0)
			    		&&(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!conRenw.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
			    {
			    	  servicetax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
			    	  st=st+1;
				}
		    }
		     
		      
		      
//		       else{
//		      	 
//		      	  servicetax = new Phrase("TAX %",font1);
//		       }
		    }
		      
		      
		      /**End for Manisha**/
		      
		
		  	PdfPCell celltax = new PdfPCell(servicetax);
		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellvat = new PdfPCell(vat);
//		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellstax = new PdfPCell(stax);
//		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/**Manisha add column old price **/
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**End**/
		
		table.addCell(cellname);
		table.addCell(cellduration);
		table.addCell(cellservices);
		/**Manisha add column old price **/
		table.addCell(celloldprice);
		/**end*/
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		
		for(int i=0;i<conRenw.getItems().size();i++){
//			chunk = new Phrase((i+1)+"",font9);
//			pdfcode = new PdfPCell(chunk);
//			pdfcode.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			/**Manisha added old price cell in table**/
			Phrase pdfoldprice;
			if(conRenw.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(conRenw.getItems().get(i).getOldProductPrice() +"",font9);
			}else{
				pdfoldprice=new Phrase("");
			}
			/**End******/
		
			
			
			
			////
			if(conRenw.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(conRenw.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			if(conRenw.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(conRenw.getItems().get(i).getPrice()+"", font9);
			}else{
				 chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
//			if(conRenw.getItems().get(i).getServiceTax()!=null){
//				chunk = new Phrase(conRenw.getItems().get(i).getServiceTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfstax = new PdfPCell(chunk);
//			pdfstax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////
//			if(conRenw.getItems().get(i).getVatTax()!=null){
//				chunk = new Phrase(conRenw.getItems().get(i).getVatTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfvat = new PdfPCell(chunk);
//			pdfvat.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			///// 
			
		   	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==conRenw.getItems().size()){
	      			 chunk = new Phrase(conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	      		 }
	      		 else{
	      			 stno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font9);
	      		 if(st==conRenw.getItems().size()){
	      	    pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()==0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(conRenw.getItems().get(i).getVatTax()!=null){
	      			 if(vat==conRenw.getItems().size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+"",font9);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font9);
	           	    }
	      		 if(vat==conRenw.getItems().size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0)){
	      		 if((conRenw.getItems().get(i).getVatTax()!=null)&& (conRenw.getItems().get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(conRenw.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +conRenw.getItems().get(i).getServiceTax().getPercentage(),font9);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font9);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font9);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
			
	      	pdftax = new PdfPCell(chunk);
	      	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	      	/**   Date : 24-11-2017 BY MANISHA
	  	   * Description :To get the total amount on pdf..!!
	  	   * **/
	      	
			double netPayAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
					conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
			
			/**Ends**/
			
			if(netPayAmount!=0){
				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				 chunk = new Phrase(" ",font9);
			}
			pdfnetPay = new PdfPCell(chunk);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			table.addCell(pdfname);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			/**Manisha add column old price **/
		
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(oldpricevalcell);
			
			/**End**/
			table.addCell(pdfprice);
			table.addCell(pdftax);
			table.addCell(pdfnetPay);
		}
		
		  /**   Date : 24-11-2017 BY MANISHA
		   * Description :To get the total amount on pdf..!!
		   * **/

		Phrase blkph1=new Phrase("");
		PdfPCell blkcell=new PdfPCell(blkph1);
		blkcell.setBorderWidthRight(0);
		table.addCell(blkcell);
		
		Phrase blkph2=new Phrase("");
		PdfPCell blkcell2=new PdfPCell(blkph2);
		blkcell2.setBorderWidthLeft(0);
		blkcell2.setBorderWidthRight(0);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(totcell);
		
		Phrase totalval=new Phrase(Math.round(conRenw.getNetPayable())+"",font9);
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(totalvalcell);
		
		/**End for Manisha**/
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		/**
		 * Date:06-12-2017 BY ANIL
		 */
		if(oldConDetFlag){
			parent.addCell(newContDetCell);
		}
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		 
	}

	private void footerInfo() {
		
		
//		boolean priceHikeFlag=false;
	//	
//		for(int i=0;i<conRenw.getItems().size();i++){
//			if(conRenw.getItems().get(i).getPrice()>conRenw.getItems().get(i).getOldProductPrice()){
//				priceHikeFlag=true;
//			}
//		}
	//	
//		String title1 = "";
	//	
//		if(priceHikeFlag==true){
//			
//			if(comp.getCellNumber2() != null)
//			{
//				if(comp.getLandline() != null)
//				{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost."+"\n"+"We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"   Phone :"+comp.getLandline()+"\n"+"\n";
//				}
//				else
//				{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. "+"\n"+"We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
//				}
//				
//			}
//			else
//			{
//				if(comp.getLandline() != null)
//				{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost."+"\n"+" We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+"   Phone :"+comp.getLandline()+"\n"+"\n";
//				}
//				else
//				{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost."+"\n"+" We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+"\n"+"\n";
//				}
//			}
//			
//		}
//		else
//		{
//			if(comp.getCellNumber2() != null)
//			{
//				if(comp.getLandline() != null)
//				{
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"   Phone :"+comp.getLandline()+"\n"+"\n";
//				}
//				else
//				{
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
//				}
//				
//			}
//			else
//			{
//				if(comp.getLandline() != null)
//				{
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+"   Phone :"+comp.getLandline()+"\n"+"\n";
//				}
//				else
//				{
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+"\n"+"\n";
//				}
//			}
//		}
	//	
	//	
//		Phrase msg = new Phrase(title1, font10);
//		PdfPCell msgCell = new PdfPCell(msg);
//		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
//		msgCell.setBorder(0);
	//	
	//	
//		String contratDescription="";
//		if(con.getDescription()!=null){
//			contratDescription=con.getDescription();
//		}
	//	
//		Phrase contractDesc = new Phrase(contratDescription, font10);
//		PdfPCell contractDescCell = new PdfPCell(contractDesc);
//		contractDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		contractDescCell.setBorder(0);
	//	
//		String contratRenDescription="";
//		if(conRenw.getDescription()!=null){
//			contratRenDescription=conRenw.getDescription();
//		}
	//	
//		Phrase contractRenDesc = new Phrase(contratRenDescription, font10);
//		PdfPCell contractRenDescCell = new PdfPCell(contractRenDesc);
//		contractRenDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		contractRenDescCell.setBorder(0);
	//	
	//	
	//	
//		Phrase check1 = new Phrase("Cheque should be in favour of  "+"\""+comp.getBusinessUnitName()+"\"", font10bold);
//		PdfPCell check1Cell = new PdfPCell(check1);
//		check1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		check1Cell.setBorder(0);
	//
//		String title2 = "";
//		title2 = "Thank You"+"\n"+"Your Sincerely";
	//	
//		Phrase thank = new Phrase(title2, font10);
//		PdfPCell thankCell = new PdfPCell(thank);
//		thankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		thankCell.setBorder(0);
	//	
	//	
//		Phrase compn = new Phrase("For "+comp.getBusinessUnitName(), font10bold);
//		PdfPCell compCell = new PdfPCell(compn);
//		compCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		compCell.setBorder(0);
	//	
	//	
	//	
//		Phrase sign = new Phrase("Sign. Authority", font10);
//		PdfPCell signCell = new PdfPCell(sign);
//		signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		signCell.setBorder(0);
	//	
//		PdfPTable parent=new PdfPTable(1);
//		parent.setWidthPercentage(100);
	//	
//		Paragraph blank =new Paragraph();
//	    blank.add(Chunk.NEWLINE);
//	    PdfPCell blankCell = new PdfPCell(blank);
//	    blankCell.setBorder(0);
	//    
	//	
	//	
//		if(con.getDescription()!=null){
//			parent.addCell(contractDescCell);
//		}
//		if(conRenw.getDescription()!=null){
//			parent.addCell(contractRenDescCell);
//		}
	//	
//		parent.addCell(msgCell);
//		parent.addCell(check1Cell);
//		parent.addCell(blankCell);
//		parent.addCell(thankCell);
//		parent.addCell(compCell);
//		parent.addCell(blankCell);
//		parent.addCell(blankCell);
//		parent.addCell(blankCell);
//		parent.addCell(signCell);
	//	
//		parent.setSpacingAfter(15f);
	//	
//		try {
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		
	/***
	 * Date 13-4-2018
	 * Add By jayshree	
	 */
		

		
		
		boolean priceHikeFlag=false;
		
		for(int i=0;i<conRenw.getItems().size();i++){
			if(conRenw.getItems().get(i).getPrice()>conRenw.getItems().get(i).getOldProductPrice()){
				priceHikeFlag=true;
			}
		}
		
		/*
		 * Date:10/11/2018
		 * Added by Ashwini
		 * Des:To add flag when old price and new price are same
		 */
		
		boolean priceSameFlag = false ;
		
		for(int i=0;i<conRenw.getItems().size();i++){
			if(conRenw.getItems().get(i).getPrice()==conRenw.getItems().get(i).getOldProductPrice()){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","OnlyforPecopp", conRenw.getCompanyId())){
				priceSameFlag=true;
				
			}
		}
	}
		/*
		 * end by Ashwini
		 */
		
		//By Jayshree changes to add branch contact number 
		
		
		String contactinfo="";
		String landline="";
		
			if(comp.getCellNumber1()!=null && comp.getCellNumber1()!=0){
				System.out.println("pn11");
				contactinfo=comp.getCellNumber1()+"";
			}
			if(comp.getCellNumber2()!=null && comp.getCellNumber2()!=0)
			{
				if(!contactinfo.trim().isEmpty())
					{
					contactinfo=contactinfo+" , "+comp.getCellNumber2()+"";
					}
				else{
					contactinfo=comp.getCellNumber2()+"";
					}
			System.out.println("pn33"+contactinfo);
			}
			if(comp.getLandline()!=0 && comp.getLandline()!=null)
			{
				if(!contactinfo.trim().isEmpty()){
					contactinfo=contactinfo+" , "+comp.getLandline()+"";
				}
				else{
					contactinfo=comp.getLandline()+"";
				}
			System.out.println("pn44"+contactinfo);
			}
		
		String branchcell="";
		
		if(con.getBranch()!=null){
			System.out.println("inside branch");
			for (int i = 0; i <branchList.size() ; i++) {
				System.out.println("inside for branchlist"+branchList.size());
				

				if(branchList.get(i).getCellNumber1()!=null && branchList.get(i).getCellNumber1()!=0){
					
					branchcell=branchList.get(i).getCellNumber1()+"";
					System.out.println("branchlist if"+branchcell);
				}
				if(branchList.get(i).getCellNumber2()!=null && branchList.get(i).getCellNumber2()!=0){
					if(!branchcell.trim().isEmpty())
					{
						branchcell=branchcell+","+branchList.get(i).getCellNumber2();
					}
					else
					{
						branchcell=branchList.get(i).getCellNumber2()+"";
					}
				}
				if(branchList.get(i).getLandline()!=null && branchList.get(i).getLandline()!=0){
					if(!branchcell.trim().isEmpty())
					{
						branchcell=branchcell+","+branchList.get(i).getLandline()+"";	
					}
					else
					{
					branchcell=branchList.get(i).getLandline()+"";
					
					}
				
				}	
				
			}
		
			if(branchcell.equals(" ")){
				System.out.println("branch cell not present");
				
				branchcell=contactinfo;
			}
			
		}
		
		
		
		
//		String title1 = "";
//		
//		if(priceHikeFlag==true){
//			
//			
//			if(con.getBranch()!=null){
//				
//				System.out.println("111");
//				if(pecoppflag){
//					
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//					
//				}else{
//				
//				title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//						+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on   Phone :"+branchcell+"\n"+"\n";
//			
//				
//				
//				}	
//			}
//			
//			 else if(comp.getCellNumber2() != null)
//			{
//				if(comp.getLandline() != null)
//				{
//					System.out.println("222");
//			if(pecoppflag){
//					
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//			}else{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  Phone :"+comp.getCellNumber1()+","+comp.getCellNumber2()+" , "+comp.getLandline()+"\n"+"\n";
//			   }
//			}
//				else
//				{
//					System.out.println("333");
//					if(pecoppflag){
//					
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above ";
//					}else{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on : Phone "+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
//				}
//			}	
//		}
//			else
//			{
//				if(comp.getLandline() != null)
//				{
//					System.out.println("444");
////					
//					if(pecoppflag){
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//					}else{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//						+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on : Phone "+comp.getCellNumber1()+","+comp.getLandline()+"\n"+"\n";
//					}
//			 
//				}
//				else
//				{
//					System.out.println("555");
//				
//					if(pecoppflag){
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//					}else{
//					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//						+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+"\n"+"\n";
//					}
//					}
//			}
//			
//		}
//		/*
//		 * 
//		 */
//		else if(priceSameFlag){
//			
//	if(con.getBranch()!=null){
//				
//				System.out.println("111");
//				
//				title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//						+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//			
//				
//			}
//			
//			 else if(comp.getCellNumber2() != null)
//			{
//				if(comp.getLandline() != null)
//				{
//					System.out.println("222");
//					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//				}
//				else
//				{
//					System.out.println("333");
//					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above ";
//				}
//				
//			}
//			else
//			{
//				if(comp.getLandline() != null)
//				{
//					System.out.println("444");
//					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above ";
//				}
//				else
//				{
//					System.out.println("555");
//					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//				}
//			}
//			
//	/*
//	 * end by Ashwini
//	 */
//			
//		}
//		else
//		{
//				if(con.getBranch()!=null){
//				
//				System.out.println("1010");
//				
//				if(pecoppflag){
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//				}else{
//				
//				title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//					+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+" Phone :"+branchcell+"\n"+"\n";
//				
//				}
//				
//				
//			}
//			
//				else if(comp.getCellNumber2() != null)
//			{
//				if(comp.getLandline() != null)
//				{
//					System.out.println("666");
//			
//					if(pecoppflag){
//					
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//					
//					}else{
//					
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//						+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+comp.getCellNumber2()+","+comp.getLandline()+"\n"+"\n";
//					}
//				}
//				else
//				{
//					System.out.println("777");
////					
//					if(pecoppflag){
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//					
//					}else{
//					
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
//				
//					}
//				}
//				
//			}
//			else
//			{
//				if(comp.getLandline() != null)
//				{
//					System.out.println("888");
////					
//					if(pecoppflag){
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  numbers above";
//					}else{
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on  Phone :"+comp.getCellNumber1()+","+comp.getLandline()+"\n"+"\n";
//					}
//				}
//				else
//				{
//					System.out.println("999");
////					
//					if(pecoppflag){
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call  numbers above";
//					}else{
//					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
//						+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+"\n"+"\n";
//				
//					}
//				}
//			}
//		}
//		
		
		
		String title1 = "";
		
		if(priceHikeFlag==true){
			
			
			if(con.getBranch()!=null){
				
				System.out.println("111");
				if(pecoppflag){
					
					// We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon.  
					
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
							
				}else{
				
				title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
						+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on   Phone :"+branchcell+"\n"+"\n";
				
				
				}	
			}
			
			 else if(comp.getCellNumber2() != null)
			{
				if(comp.getLandline() != null)
				{
					System.out.println("222");
			if(pecoppflag){
					
				title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
						+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
			}else{
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on  Phone :"+comp.getCellNumber1()+","+comp.getCellNumber2()+" , "+comp.getLandline()+"\n"+"\n";
			   }
			}
				else
				{
					System.out.println("333");
					if(pecoppflag){
					
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					}else{
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on : Phone "+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
				}
			}	
		}
			else
			{
				if(comp.getLandline() != null)
				{
					System.out.println("444");
//					
					if(pecoppflag){
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					}else{
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on : Phone "+comp.getCellNumber1()+","+comp.getLandline()+"\n"+"\n";
					}
			 
				}
				else
				{
					System.out.println("555");
				
					if(pecoppflag){
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					}else{
					title1 = "We seek a nominal increase our annual services charges partly to meet the hike in our operational costs. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+"\n"+"\n";
					}
					}
			}
			
		}
		/*
		 * 
		 */
		else if(priceSameFlag){
			
	if(con.getBranch()!=null){
				
				System.out.println("111");
				
				title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
						+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
			
				
			}
			
			 else if(comp.getCellNumber2() != null)
			{
				if(comp.getLandline() != null)
				{
					System.out.println("222");
					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
				}
				else
				{
					System.out.println("333");
					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
				}
				
			}
			else
			{
				if(comp.getLandline() != null)
				{
					System.out.println("444");
					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
				}
				else
				{
					System.out.println("555");
					title1 = "Price will remain same as last year. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
				}
			}
			
	/*
	 * end by Ashwini
	 */
			
		}
		else
		{
				if(con.getBranch()!=null){
				
				System.out.println("1010");
				
				if(pecoppflag){
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
				}else{
				
				title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
					+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on :"+" Phone :"+branchcell+"\n"+"\n";
				
				}
				
				
			}
			
				else if(comp.getCellNumber2() != null)
			{
				if(comp.getLandline() != null)
				{
					System.out.println("666");
			
					if(pecoppflag){
					
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					
					}else{
					
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
						+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+comp.getCellNumber2()+","+comp.getLandline()+"\n"+"\n";
					}
				}
				else
				{
					System.out.println("777");
//					
					if(pecoppflag){
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					
					}else{
					
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
				
					}
				}
				
			}
			else
			{
				if(comp.getLandline() != null)
				{
					System.out.println("888");
//					
					if(pecoppflag){
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					}else{
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on  Phone :"+comp.getCellNumber1()+","+comp.getLandline()+"\n"+"\n";
					}
				}
				else
				{
					System.out.println("999");
//					
					if(pecoppflag){
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
							+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on numbers above";
					}else{
					title1 = "We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
						+"Should you require further information on above, Please call or email us and we would gladly assist you. For any assistance, Please call on Phone :"+comp.getCellNumber1()+","+"\n"+"\n";
				
					}
				}
			}
		}
		
		
		//End By jayshree
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		
		String contratDescription="";
		if(con.getDescription()!=null){
			contratDescription=con.getDescription();
		}
		
		
		
		Phrase contractDesc = new Phrase(contratDescription, font10);
		PdfPCell contractDescCell = new PdfPCell(contractDesc);
		contractDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractDescCell.setBorder(0);
		
		String contratRenDescription="";
		if(conRenw.getDescription()!=null && !conRenw.getDescription().equals("")){
			contratRenDescription=conRenw.getDescription();
		}
		
		/**
		 * Date 11/1/2018
		 * By jayshree
		 * Des.to set only 500 characters in description
		 */
		
		Phrase contractRenDesc =null;
		
		
		if(contratRenDescription!=null &&contratRenDescription.length()>500){
			contractRenDesc = new Phrase(contratRenDescription.substring(0,499), font10);
		}else{
			contractRenDesc = new Phrase(contratRenDescription, font10);
		}
		PdfPCell contractRenDescCell = new PdfPCell(contractRenDesc);
		contractRenDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractRenDescCell.setBorder(0);
	//End by jayshree	
		
		Phrase annexture = new Phrase("Please refere annexure for product details  ", font10bold);
		PdfPCell annextureCell = new PdfPCell(annexture);
		annextureCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		annextureCell.setBorder(0);
		annextureCell.setPaddingBottom(10);
		annextureCell.setPaddingTop(10);
		
		String companyName="";
		if(branchFlag){
			if(comppayments != null){
				if(comppayments.getPaymentFavouring()!=null){
					companyName=comppayments.getPaymentFavouring();
				}else{
					companyName=comp.getBusinessUnitName();
				}
			}
			
		}else{
			companyName=comp.getBusinessUnitName();
		}
		
		Phrase check1 = new Phrase("Cheque should be in favour of  "+"\""+companyName+"\"", font10bold);
		PdfPCell check1Cell = new PdfPCell(check1);
		check1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		check1Cell.setBorder(0);

		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(2);
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    PdfPCell blankCell = new PdfPCell(blank);
	    blankCell.setBorder(0);
	    
		parent.addCell(msgCell);
		if(con.getDescription()!=null){
			parent.addCell(contractDescCell);
		}
		
		//Ashwini Patil Date:23-06-2022 Pepcopp wanted payment terms in renewal letter.
		if(conRenw.getDescription()!=null&&!conRenw.getDescription().equals("")){
			logger.log(Level.SEVERE,"in ConRenew description="+conRenw.getDescription());
			parent.addCell(contractRenDescCell);
		}else {
			
			float[] columnHalfWidth = { 1f, 3.5f };
			PdfPTable terms=new PdfPTable(2);
			terms.setWidthPercentage(100);

			try {
				terms.setWidths(columnHalfWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String paymentTermVal="";
			logger.log(Level.SEVERE,"payment terms:"+con.getPaymentTermsList().size());
			if(con.getPayTerms()!=null&&!con.getPayTerms().equals("")) {
				paymentTermVal=con.getPayTerms();
			}else {
			for(int i=0;i<con.getPaymentTermsList().size();i++) {
				paymentTermVal+=df.format(con.getPaymentTermsList().get(i).getPayTermPercent())+"% "+con.getPaymentTermsList().get(i).getPayTermComment()+"\n";
			}
			}
			
			Phrase paymentTermPhrase = new Phrase("Terms of Payment: ", font10);
			PdfPCell paymentTermCell = new PdfPCell(paymentTermPhrase);
			paymentTermCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentTermCell.setBorder(0);
			terms.addCell(paymentTermCell);
			
			Phrase paymentTermValPhrase = new Phrase(paymentTermVal, font10);
			PdfPCell paymentTermValCell = new PdfPCell(paymentTermValPhrase);
			paymentTermValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentTermValCell.setBorder(0);
			terms.addCell(paymentTermValCell);
			
			PdfPCell termTableCell = new PdfPCell(terms);
			termTableCell.setBorder(0);
			parent.addCell(termTableCell);
		}
		
		
//		if(checkOldFormat==false){
//		parent.addCell(annextureCell);
//		}
		parent.addCell(check1Cell);
		parent.addCell(blankCell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/****footer tab****/
		
		
		PdfPTable leftFooter=new PdfPTable(1);
		leftFooter.setWidthPercentage(100);
		
		String title2 = "";
		title2 = "Thank You";//By Jayshree remove the content
		
		Phrase thank = new Phrase(title2, font10);
		PdfPCell thankCell = new PdfPCell(thank);
		thankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		thankCell.setBorder(0);
		
		String compName="";
		if(branchFlag){
			if(branchDt.getCorrespondenceName()!=null){
				compName=branchDt.getCorrespondenceName();
			}else{
				compName="";
			}
		}else{
			compName=comp.getBusinessUnitName();
		}
		
		
		Phrase compn = new Phrase("For "+compName, font10bold);
		PdfPCell compCell = new PdfPCell(compn);
        compCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		compCell.setBorder(0);
		
		
		
		Phrase sign = new Phrase("Authorised Signatory", font10); // Authorised Signatory   Sign. Authority
		PdfPCell signCell = new PdfPCell(sign);
		signCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		signCell.setBorder(0);
		
		
		
		/***
		 * Date 14-04-2018 By vijay for Digital Signator
		 */

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		} catch (Exception e) {
			e.printStackTrace();
		}

//		if (imageSignCell != null) {
//			rightTable.addCell(imageSignCell);
//		} else {
//			Phrase blank1 = new Phrase(" ", font10);
//			PdfPCell blank1Cell = new PdfPCell(blank1);
//			blank1Cell.setBorder(0);
//			rightTable.addCell(blank1Cell);
//			rightTable.addCell(blank1Cell);
//			rightTable.addCell(blank1Cell);
//			rightTable.addCell(blank1Cell);
	//
//		}
		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		signParaCell.setBorder(0);
			signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		/**
		 * ends here
		 */
		
		
			
		
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
		leftFooter.addCell(thankCell);
		leftFooter.addCell(compCell);
		/** Date 14-04-2018 By vijay commneted below **/
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
		
		/** Date 14-04-2018 By vijay for Digital Signatory ***/
		if (imageSignCell != null) {
			leftFooter.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			leftFooter.addCell(blank1Cell);
			leftFooter.addCell(blank1Cell);
			leftFooter.addCell(blank1Cell);
		}
		/**
		 * ends here
		 */
		leftFooter.addCell(signCell);
		leftFooter.setSpacingAfter(15f);
		
		
		
		PdfPTable rightTab=new PdfPTable(3);
		rightTab.setWidthPercentage(100);
		
		
		try {
			/*
			 *Date: 11/10/2018
			 * Developer:Ashwini
			 * Des:to shrink right table
			 */
			if(pecoppflag){
			 float[] columnWidths = { 1f, 1f, 2f };
			 rightTab.setWidths(columnWidths);
			}else{
				
				rightTab.setWidths(new float[]{20,30,50});
				
			}
			 
			/*
			 * end by Ashwini
			 */
//			rightTab.setWidths(new float[]{20,30,50}); //commented by Ashwini
		
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase modes=new Phrase("Modes",font9bold);
		PdfPCell modesCell=new PdfPCell(modes);
		modesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(modesCell);
		
		Phrase favering=new Phrase("Favouring",font9bold);
		PdfPCell faveringCell=new PdfPCell(favering);
		faveringCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(faveringCell);
		
		Phrase details=new Phrase("Details",font9bold);
		PdfPCell detailsCell=new PdfPCell(details);
		detailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(detailsCell);
		
		Phrase chequeph=new Phrase("Cheque/"+"\n"+"Demand Draft/"+"\n"+"Pay Order ",font9);
		PdfPCell chequeCell=new PdfPCell(chequeph);
		chequeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(chequeCell);
		if(pecoppflag || pepcoppflag){
			
			Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName(),font9);
			PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
			buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			rightTab.addCell(buesinessunitCell);
			
		}else{
		
			String comName="";
			String compAdd="";
			if(branchFlag){
				if(comppayments != null){
					if(comppayments.getPaymentFavouring()!=null){
						comName=comppayments.getPaymentFavouring();
					}else{
						comName=comp.getBusinessUnitName();
					}
				}
				
			}else{
				comName=comp.getBusinessUnitName();
			}
			
			if(comp.getAddress().getCompleteAddress()!=null){
				compAdd=comp.getAddress().getCompleteAddress();
			}else{
				compAdd="";
			}
		
			
			
			
			
			
		Phrase buesinessunitph=new Phrase(comName+"\n"+compAdd,font9);
		PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
		buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(buesinessunitCell);
		}
		
//		Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName()+"\n"+comp.getAddress().getCompleteAddress(),font9);
//		PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
//		buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		rightTab.addCell(buesinessunitCell);
//	    
		/*
		 * Date:10/10/2018
		 * Added by Ashwini
		 * Developer:To add company branch Address
		 */
		if(pecoppflag){
		
		String branchaddress1 =" ";
		
		System.out.println("Branchlist::"+branchList);
		
		if(branchList!=null){
		for(Branch branch:branchList){
			System.out.println("I am inside for loop1");
			System.out.println("Branch from branchlist:"+branch.getBusinessUnitName());
			System.out.println("Branch from contractRenewal::"+conRenw.getBranch());
			System.out.println("I am inside for loop2");
			if(conRenw.getBranch().equalsIgnoreCase(branch.getBusinessUnitName())){
				branchaddress1 = branch.getAddress().getCompleteAddress();
				System.out.println("branch Address::"+branch.getAddress().getCompleteAddress());
			}
			
		}
		
		}else{
			
			branchaddress1 = comp.getAddress().getCompleteAddress();
			System.out.println("company Address::"+comp.getAddress().getCompleteAddress());
		}
		System.out.println("I am End of the for loop2");
		
		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+branchaddress1+"\n"+"2.Ensure your name and contract id is mentioned on the back of the cheque ",font9);
		PdfPCell detailsvalCell=new PdfPCell(detailsval);
		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(detailsvalCell);
		
	}else{
		String companyAddress="";
		if(comp.getAddress().getCompleteAddress()!=null){
			companyAddress=comp.getAddress().getCompleteAddress();
		}else{
			companyAddress="";
		}
			
		
		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+companyAddress+"\n"+"2.Ensure your company name is mentioned on the back of the cheque ",font9);
		PdfPCell detailsvalCell=new PdfPCell(detailsval);
		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(detailsvalCell);
	}
		
		/*
		 * end by Ashwini
		 */
		
		
		/*
		 * commented by Ashwini
		 */
		
//		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+comp.getAddress().getCompleteAddress()+"\n"+"2.Ensure Your Company Name is mentioned on the back of the cheque ",font9);
//		PdfPCell detailsvalCell=new PdfPCell(detailsval);
//		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		rightTab.addCell(detailsvalCell);

		/*
		 * end by Ashwini
		 */
		Phrase rtgsph=new Phrase("RTGS/NEFT",font9);
		PdfPCell rtgsCell=new PdfPCell(rtgsph);
		rtgsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(rtgsCell);
		
		Phrase companyname=null;
		
		
			
		if(comppayment.size()==1){
			System.out.println("first condition"+comppayment.size());
		if(comppayment.get(0).getPaymentComName()!=null){
		 companyname=new Phrase(comppayment.get(0).getPaymentComName(),font9);
		}else{
			companyname=new Phrase("",font9);
		}
		
		PdfPCell companynameCell=new PdfPCell(companyname);
		companynameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(companynameCell);
		
		Phrase bankdetail=null;
		
		if(comppayment.get(0).getPaymentAccountNo()!=null){
		 bankdetail=new Phrase("Account Number : "+comppayment.get(0).getPaymentAccountNo()+"\n"+"Bank Name : "+comppayment.get(0).getPaymentBankName()+"\n"+comppayment.get(0).getAddressInfo().getCompleteAddress()+"\n"+"IFSC/RTGS/NEFT Code : "+comppayment.get(0).getPaymentIFSCcode(),font9);
		}
		else{
			 bankdetail=new Phrase("",font9);

		}
		PdfPCell bankdetailCell=new PdfPCell(bankdetail);
		bankdetailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(bankdetailCell);
		
		}
		else if(comppayment.size()!=0){
			System.out.println("second condition"+comppayment.size());
			
			for(int p=0;p<comppayment.size();p++){
				System.out.println("second condition 222"+comppayment.size());
				if(comppayment.get(p).isPaymentDefault())
				{
					System.out.println("second condition 333"+comppayment.size());
					if(comppayment.get(p).getPaymentComName()!=null){
					 companyname=new Phrase(comppayment.get(p).getPaymentComName(),font9);
					}else{
						companyname=new Phrase("",font9);
					}
					
					PdfPCell companynameCell=new PdfPCell(companyname);
					companynameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					rightTab.addCell(companynameCell);
					
					Phrase bankdetail=null;
					
					if(comppayment.get(0).getPaymentAccountNo()!=null){
					 bankdetail=new Phrase("Account Number : "+comppayment.get(p).getPaymentAccountNo()+"\n"+"Bank Name : "+comppayment.get(p).getPaymentBankName()+"\n"+comppayment.get(p).getAddressInfo().getCompleteAddress()+"\n"+"IFSC/RTGS/NEFT Code : "+comppayment.get(p).getPaymentIFSCcode(),font9);
					}
					else{
						 bankdetail=new Phrase("",font9);

					}
					PdfPCell bankdetailCell=new PdfPCell(bankdetail);
					bankdetailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					rightTab.addCell(bankdetailCell);
					}
			}
			
			
		}
		
		
		PdfPTable footerParent=new PdfPTable(2);
		footerParent.setWidthPercentage(100);
		
		try {
//			footerParent.setWidths(new float[]{70,30});
			/*
			 * Date:11/10/2018
			 * Developer:Ashwini
			 * 
			 */
			if(pecoppflag){
			footerParent.setWidths(new float[]{40,30});
			}else{
				footerParent.setWidths(new float[]{70,30});
			}
			
			/*
			 * end by Ashwini
			 */
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(comppayment.size()!=0){
		System.out.println("companypayment111");
		PdfPCell leftCell=new PdfPCell(rightTab);
		leftCell.setBorder(0);
		footerParent.addCell(leftCell);
		
//		footerParent.setKeepTogether(true);

		PdfPCell rightCell=new PdfPCell(leftFooter);
		rightCell.setBorder(0);
		footerParent.addCell(rightCell);
		}
		else{
			System.out.println("company payment222");
			
			PdfPCell rightCell=new PdfPCell(leftFooter);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			footerParent.addCell(rightCell);
			
		}
		try {
			document.add(footerParent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}



	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=conRenw.getItems();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
			if(entity.getPercentageDiscount()==null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal*entity.getQty();
				sum=sum+priceVal;
			}
			if(entity.getPercentageDiscount()!=null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
				priceVal=priceVal*entity.getQty();
				
				sum=sum+priceVal;
			}
			

		}
		return sum;
	}

	/**
	 * Developed by : Rohan Bhagde.
	 * Reason : This method is used for calculating Total amount including tax
	 * @param newPrice
	 * @param serTax
	 * @param vatTax
	 * @return
	 */
	   /**   Date : 24-11-2017 BY MANISHA
	   * Description :To get the total amount on pdf..!!
	   * **/
	public double calculateTotalAmt(double newPrice , double serTax , double vatTax,String vatname,String sertaxname)
	{
		double totalAmt= newPrice;
		double vatTaxAmt =0;
		double setTaxAmt =0;

		
//		System.out.println("new price "+newPrice );
//		System.out.println("service tax "+serTax );
//		System.out.println("vat tax"+vatTax );
		
//		System.out.println("service tax "+sertaxname );
//		System.out.println("vat tax "+vatname );
//		System.out.println("Price "+newPrice );
		
		
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = newPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = newPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  newPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = newPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
	//	
//	    if(vatname==null && vatTax==0 && sertaxname!=null && !sertaxname.equals("") && serTax!=0) 
//		{
//	    	setTaxAmt = newPrice * (serTax/100);
//			totalAmt =newPrice+setTaxAmt;
		return totalAmt;
	}

	       /****End******/

	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			// Here if both are inclusive then first remove service tax and then on that amount
			// calculate vat.
			double removeServiceTax=(entity.getPrice()/(1+service/100));
						
			//double taxPerc=service+vat;
			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		return tax;
	}}
