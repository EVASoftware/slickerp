package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utils.Console;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class FumigationPdf{

	Fumigation fumigation;
	public  Document document;
	Company comp;
	List<ArticleType> articletype;
	ProcessConfiguration processConfig;
	boolean upcflag=false;
	boolean fumigationTitleFlag=false;
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font16bold,font9bold,font10,font10bold,font10boldul,font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	
	 float[] columnWidths={1.7f,0.2f,2.0f,1.7f,0.2f,2.0f};
	 float[] columnWidths1={1.2f,2.0f};
	 float[] columnWidths2={1f,0.2f,1f,1f,0.2f,1f,1f,0.2f,1f};
	 float[] columnWidths3={2.0f,0.8f};
	 float[] columnWidths4={1.0f,2.0f};
	 float[] columnWidths5={3.5f,0.8f,2.0f};
	 float[] columnWidths16={1.0f,0.5f,1.0f,0.5f,1.0f};
	private boolean paytermsflag=false;
	private boolean disclaimerflag=false;
	 
	public FumigationPdf() {
		
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD|Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
	}
		
	
	
	
	
	
	public void setFumigation(Long count,String preprint) {

		fumigation=ofy().load().type(Fumigation.class).id(count).now();
		
		
		if(fumigation.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",fumigation.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		articletype = new ArrayList<ArticleType>();
		
		System.out.println("size of article size() === "+comp.getArticleTypeDetails().size());
		
		
/************************************Letter Head Flag*******************************/
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Fumigation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag=true;
					}
				}
			}
		}
		
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Fumigation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("FumigationTitlePrint")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						fumigationTitleFlag=true;
					}
				}
			}
		}
		
		
		
		
		if(comp.getArticleTypeDetails().size()!=0){
			articletype.addAll(comp.getArticleTypeDetails());
		}
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}


	//*******************************Fumigation EMIAL LOGIC (ROHAN)********************************** 
	

	public void createPdfForEmail(Fumigation fumigation, Company comp2,Customer c) {
		
		this.fumigation=fumigation;
		this.comp=comp2;
//		this.cust=custEntity;
//		this.con=contractEntity;
//		this.billEntity=billingEntity;
//		this.salesProd=invoiceentity.getSalesOrderProducts();
//		this.contractTaxesLis=con.getProductTaxes();
//		this.contractChargesLis=con.getProductCharges();
//		this.billingTaxesLis=invoiceentity.getBillingTaxes();
//		this.billingChargesLis=invoiceentity.getBillingOtherCharges();
//		this.payTermsLis=contractEntity.getPaymentTermsList();
		
		if(fumigation.getCompanyId()!=null){
			this.processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceInvoicePayTerm")&&processConfig.getProcessList().get(k).isStatus()==true){
						this.paytermsflag=true;
					}
					else{
						this.paytermsflag=false;
					}
				}
			}
		}
		
/************************************Letter Head Flag*******************************/
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.upcflag=true;
					}
				}
			}
		}
		
		
		/**********************************Disclaimer Flag******************************************/
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.disclaimerflag=true;
					}
				}
			}
		}
		
//		productDetailsForEmail();
//		 articletype = new ArrayList<ArticleType>();
//		 if(cust.getArticleTypeDetails().size()!=0){
//				articletype.addAll(cust.getArticleTypeDetails());
//			}
//			if(comp.getArticleTypeDetails().size()!=0){
//				articletype.addAll(comp.getArticleTypeDetails());
//			}
//		 arrPayTerms=invoiceentity.getArrPayTerms();
//		 
//		
//		try {
//			createLogo(document,comp);
//			createCompanyAddress();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//	  try {
//			createCustInfo1();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		createProductTable();
//		termsConditionsInfo();
//		addFooter();
	
		
		
//		System.out.println("flag value ===="+PrintPaySlipOnSamePage);
//		if(PrintPaySlipOnSamePage==true){
////		createLogo1(document,comp);
//		Createblank();
//		if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//			createCompanyHedding2();
//		}else{
//			createCompanyHedding1();
//		}
//		
//		createPaymentDetails1();
//		footerInfo1();
//		
//		createPaySlipDetails1();
//		}
//		else{
////			createLogo(document,comp);
//			Createblank();
//			if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
//				createCompanyHedding4();
//			}else{
//				createCompanyHedding3();
//			}
//			
//			createPaymentDetails();
//			footerInfo();
//		}
	
		
//		System.out.println("..................................."+preprint);	
		if(upcflag==false ){
			
			System.out.println("2222222222222222222222222222222");	
			System.out.println("contract inside plane");
			
			if(comp.getLogo()!=null){
			createLogo(document,comp);
			}
			createCompanyAddress();
			}else{
			
			
				System.out.println("4444444444444444444444444444444444444444444");	
			System.out.println("inside prit no");
			
//			if(comp.getUploadHeader()!=null){
//			createCompanyNameAsHeader(document,comp);
//			}
//			
//			if(comp.getUploadFooter()!=null){
//			createCompanyNameAsFooter(document,comp);
//			}
//			createBlankforUPC();
		}
		
		myMethod("this is information you that this is my method");
		
//		createLogo(document,comp);
//		if(upcflag==false){
//		createCompanyAddress();
//		}
		CreateHeading();
		createGoodsDetails();
		createTreatmentDetails();
		createtimmber();
		createAddDeclare();
		lastRecordTable();
		
	}

	
	//*********************************CHANGES ENDS HERE *********************************

	
	
	
	
	
	public void createPdf(String preprint) {

//		 Createblank(); 
		
		System.out.println("..................................."+preprint);	
		if(upcflag==false && preprint.equals("plane")){
			
			System.out.println("2222222222222222222222222222222");	
			System.out.println("contract inside plane");
			
			if(comp.getLogo()!=null){
			createLogo(document,comp);
			}
			createCompanyAddress();
			}else{
			
			if(preprint.equals("yes")){
				
				System.out.println("333333333333333333333333333333333");	
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			
			if(preprint.equals("no")){
				System.out.println("4444444444444444444444444444444444444444444");	
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}
			
		}
		
		myMethod("this is information you that this is my method");
		
//		createLogo(document,comp);
//		if(upcflag==false){
//		createCompanyAddress();
//		}
		CreateHeading();
		createGoodsDetails();
		createTreatmentDetails();
		createtimmber();
		createAddDeclare();
		lastRecordTable();
		
	}
	
	
	
	
private void createCompanyNameAsHeader(Document doc, Company comp) {
		
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createBlankforUPC() {
		
//	String title="";
//		title="Fumigation";
	
//	Date conEnt=null;
//	if(qp instanceof Contract){
//		Contract conEntity=(Contract)qp;
//		conEnt=conEntity.getContractDate();
//	}
//	else{
//		Quotation quotEnt=(Quotation)qp;
//		conEnt=quotEnt.getCreationDate();
//	}
	
	
	
//	String countinfo="ID : "+qp.getCount()+"";
	
//	String creationdateinfo="Date: "+fmt.format(conEnt);
	
//	Phrase titlephrase =new Phrase(title,font14bold);
	Paragraph titlepdfpara=new Paragraph();
//	titlepdfpara.add(titlephrase);
//	titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//	titlepdfpara.add(Chunk.NEWLINE);
//	PdfPCell titlepdfcell=new PdfPCell();
//	titlepdfcell.addElement(titlepdfpara);
//	titlepdfcell.setBorderWidthRight(0);
//	titlepdfcell.setBorderWidthLeft(0);
//	titlepdfcell.addElement(Chunk.NEWLINE);
	
//	Phrase idphrase=new Phrase(countinfo,font14bold);
//	Paragraph idofpdfpara=new Paragraph();
//	idofpdfpara.add(idphrase);
//	idofpdfpara.setAlignment(Element.ALIGN_LEFT);
//	idofpdfpara.add(Chunk.NEWLINE);
//	PdfPCell countcell=new PdfPCell();
//	countcell.addElement(idofpdfpara);
//	countcell.setBorderWidthRight(0);
//	countcell.addElement(Chunk.NEWLINE);
	
////	Phrase dateofpdf=new Phrase(creationdateinfo,font14bold);
//	Paragraph creatndatepara=new Paragraph();
////	creatndatepara.add(dateofpdf);
//	creatndatepara.setAlignment(Element.ALIGN_RIGHT);
//	creatndatepara.add(Chunk.NEWLINE);
//	PdfPCell creationcell=new PdfPCell();
//	creationcell.addElement(creatndatepara);
//	creationcell.setBorderWidthLeft(0);
//	creationcell.addElement(Chunk.NEWLINE);
	
//	PdfPTable titlepdftable=new PdfPTable(3);
//	titlepdftable.setWidthPercentage(100);
//	titlepdftable.addCell(countcell);
//	titlepdftable.addCell(titlepdfcell);
//	titlepdftable.addCell(creationcell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
//		
//		PdfPTable parent=new PdfPTable(1);
//		parent.setWidthPercentage(100);
//		
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
//		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	
	
	

	private void lastRecordTable() {
		
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		
		Phrase msg = new Phrase(fumigation.getMoredecl(),font9bold);
		PdfPCell msgcell = new PdfPCell(msg);
		msgcell.setBorder(0);
		table.addCell(msgcell);
		
		
		Phrase holo =new Phrase("                                                    Hologram",font9);
		PdfPCell holocell = new PdfPCell(holo);
		holocell.setBorder(0);
		
		
		PdfPTable table123 = new PdfPTable(1);
		table123.addCell(holocell);
		
		PdfPCell tableCell = new PdfPCell(table123);
		
		table.addCell(tableCell);
		
		Phrase blank = new Phrase(" ",font9);
		PdfPCell blankCll = new PdfPCell(blank);
		blankCll.setBorder(0);
		table.addCell(blankCll);
		
		
		 try {
			 table.setWidths(columnWidths5);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			};
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}






	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}
	
	private void createLogo(Document doc, Company comp) {
		
		
		//********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
		}
	
	//***********************for local **************************
	
//			try
//			{
//			Image image1=Image.getInstance("images/suma.jpg");
//			image1.scalePercent(20f);
//			image1.setAbsolutePosition(40f,725f);	
//			doc.add(image1);
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
//			
	
	}
	
	public  void createCompanyAddress()
	{
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		Phrase contactinfo=null;
		if(comp.getLandline()!=0){
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Phone: "+comp.getLandline()+"    Email1: "+comp.getEmail().trim(),font10);
		}
		else{
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Email1: "+comp.getEmail().trim(),font10);
		}
		
		
		Phrase contactemail=new Phrase("Email2: "+comp.getPocEmail(),font10);
		Paragraph realmobpara1=new Paragraph();
		realmobpara1.add(contactemail);
		realmobpara1.setAlignment(Element.ALIGN_CENTER);
		
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPCell contactcell1=new PdfPCell();
		contactcell1.addElement(realmobpara1);
		contactcell1.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		companyDetails.setSpacingAfter(5f);
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.addCell(contactcell1);
		
//		String titlepdf="";
//		if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
//			titlepdf="Proforma Invoice";
//		else
//			titlepdf="Tax Invoice";
//		
//		Phrase titlephrase=new Phrase(titlepdf,font14bold);
//		Paragraph titlepdfpara=new Paragraph();
//		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//		
//		PdfPCell titlecell=new PdfPCell();
//		titlecell.addElement(titlepdfpara);
//		titlecell.setBorder(0);
//		
//		Phrase blankphrase=new Phrase("",font8);
//		PdfPCell blankCell=new PdfPCell();
//		blankCell.addElement(blankphrase);
//		blankCell.setBorder(0);
		
//		PdfPTable titlepdftable=new PdfPTable(3);
//		titlepdftable.setWidthPercentage(100);
//		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
//		titlepdftable.addCell(blankCell);
//		titlepdftable.addCell(titlecell);
//		titlepdftable.addCell(blankCell);
		
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		
//		comapnyCell.setBorderWidthTop(0);
//		comapnyCell.setBorderWidthLeft(0);
//		comapnyCell.setBorderWidthRight(0);
		
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	
	
	
	
	private void createAddDeclare() {

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String	titlepdf="ADDITIONAL DECLARATION :";
		
		Phrase titlephrase=new Phrase(titlepdf,font10boldul);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
//		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		
//		String	dotedline="- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
//				+ " - - - - - - - - - - - - - - -";
//		Phrase dotphrase =new Phrase(dotedline,font10bold);
//		PdfPCell dotcell = new PdfPCell(dotphrase);
//		dotcell.setBorder(0);
		
		

		String	titlepdf1=" ";
		if(!fumigation.getAdditionaldeclaration().equals("")){
			
			titlepdf1=fumigation.getAdditionaldeclaration();
			
		}
		
		 SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		 String invoiceDate=null;
		 String ShippingBillDate=null;
		 if(fumigation.getInvoiceDate()!=null)
			{
			 	 invoiceDate = fmt.format(fumigation.getInvoiceDate());
			}
		 else
		 {
			 	invoiceDate = " ";
		 }
		 
		 
		 if(fumigation.getShippingBillDate()!=null)
			{
			 	 ShippingBillDate = fmt.format(fumigation.getShippingBillDate());
			}
		 else
		 {
			 	ShippingBillDate = " ";
		 }
		 
		 
		String invoice =" ";
		System.out.println("11111111111111 inovice"+fumigation.getInvoiceNo());
		if(!fumigation.getInvoiceNo().equals(""))
		{
			invoice= "INVOICE NO. "+fumigation.getInvoiceNo()+" "+invoiceDate;
		}
		
		
		System.out.println("11111111111111shipping"+fumigation.getShippingBillNo());
		String shipping =" ";
		if(!fumigation.getShippingBillNo().equals(""))
		{
			shipping= "SHIPPING BILL NO. "+fumigation.getShippingBillNo()+" "+ShippingBillDate;
		}
		
		
		
		Phrase titlephrase1=new Phrase(invoice+" "+shipping+" "+titlepdf1,font9);
		Paragraph titlepdfpara1=new Paragraph();
		titlepdfpara1.add(titlephrase1);
		titlepdfpara1.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell titlecell1=new PdfPCell(titlepdfpara1);
		titlecell1.setBorder(0);
		titlecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable titlepdftable=new PdfPTable(2);
		titlepdftable.setWidthPercentage(100f);
		
//		try {
//			titlepdftable.setWidths(columnWidths4);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
		
		try {
			titlepdftable.setWidths(new float[] { 30,70 });
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(titlecell1);
		
		
		
		String note="I DECLARE THAT THESE DETAILS ARE TRUE & CORRECT AND THE FUMIGATION HAS BEEN CARRIED OUT IN ACCORDANCE WITH THE ISPM-15.";
		
		
		Phrase notephrase=new Phrase(note,font9);
		PdfPCell notecell=new PdfPCell(notephrase);
		notecell.setBorder(0);
		
		

//		String palce=" ";
//		if(!fumigation.getPlace().equals("")){
//			
//			palce=fumigation.getPlace();
//			
//		}
//		
//		String date=" ";
//		if(fumigation.getPdate()!=null){
//			
//			date=fmt.format(fumigation.getPdate());
//			
//		}
//		
//		Phrase datephrase=new Phrase("Place and Date : "+date+" / "+palce,font9);
//		PdfPCell datecell=new PdfPCell(datephrase);
//		datecell.setBorder(0);
//		
//		Phrase sigphrase=new Phrase("Signature : "+"___________________",font9);
//		PdfPCell sigcell=new PdfPCell(sigphrase);
//		sigcell.setBorder(0);
//		sigcell.setVerticalAlignment(Element.ALIGN_BASELINE);
		
		Phrase blanck=new Phrase(" ",font9);
		PdfPCell blankcell=new PdfPCell(blanck);
		blankcell.setBorder(0);
		
		PdfPTable notetable=new PdfPTable(1);
		notetable.setWidthPercentage(100f);
//		notetable.setSpacingAfter(10f);
		notetable.addCell(notecell);
//		notetable.addCell(datecell);
//		notetable.addCell(blankcell);
//		notetable.addCell(sigcell);
		
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingAfter(10f);
		
		PdfPCell titlePdfCell=new PdfPCell();
		titlePdfCell.addElement(titlepdftable);
		
//		titlePdfCell.setBorderWidthLeft(0);
//		titlePdfCell.setBorderWidthRight(0);
		titlePdfCell.setBorderWidthTop(0);
		

		PdfPCell titlePdfCell1=new PdfPCell();
		titlePdfCell1.addElement(notetable);
		
//		titlePdfCell1.setBorderWidthLeft(0);
//		titlePdfCell1.setBorderWidthRight(0);
		titlePdfCell1.setBorderWidthTop(0);
		parent.addCell(titlePdfCell);
		parent.addCell(titlePdfCell1);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}






	private void createtimmber() {

		//**********rohan changes here*************
			
				PdfPTable yesTable = new PdfPTable(5);
				yesTable.setWidthPercentage(100f);
//				yesTable.setSpacingBefore(10f);
//				yesTable.setSpacingAfter(10f);
				
				 try {
					 yesTable.setWidths(columnWidths16);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				
				Phrase yesPhrse =  new Phrase("YES",font9bold);
				PdfPCell yesCell = new PdfPCell(yesPhrse);
				yesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				yesCell.setBorder(0);
				PdfPTable smallyes = new PdfPTable(1);
				smallyes.addCell(yesCell);
				PdfPCell smalltableCell =new PdfPCell(smallyes);
				yesTable.addCell(smalltableCell);
				
				Phrase slashPhrse =  new Phrase("/",font9);
				PdfPCell slashCell = new PdfPCell(slashPhrse);
				slashCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				slashCell.setBorder(0);
				yesTable.addCell(slashCell);
				
				Phrase naPhrse =  new Phrase("NA",font9);
				PdfPCell naCell = new PdfPCell(naPhrse);
				naCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				naCell.setBorder(0);
				yesTable.addCell(naCell);
				
				yesTable.addCell(slashCell);
				
				Phrase noPhrse =  new Phrase("NO",font9);
				PdfPCell noCell = new PdfPCell(noPhrse);
				noCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				noCell.setBorder(0);
				yesTable.addCell(noCell);
				
				
				PdfPCell yesTableCell = new PdfPCell(yesTable);
				yesTableCell.setBorder(0);
				
				
				
				
				PdfPTable naTable = new PdfPTable(5);
				naTable.setWidthPercentage(100f);
				
//				naTable.setSpacingBefore(10f);
//				naTable.setSpacingAfter(10f);
				 try {
					 naTable.setWidths(columnWidths16);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				
				
				Phrase yesPhrse1 =  new Phrase("YES",font9);
				PdfPCell yesCell1 = new PdfPCell(yesPhrse1);
				yesCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				yesCell1.setBorder(0);
				naTable.addCell(yesCell1);
				
				Phrase slashPhrse1 =  new Phrase("/",font9);
				PdfPCell slashCell1 = new PdfPCell(slashPhrse1);
				slashCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				slashCell1.setBorder(0);
				naTable.addCell(slashCell1);
				
				Phrase naPhrse1 =  new Phrase("NA",font9bold);
				PdfPCell naCell1 = new PdfPCell(naPhrse1);
				naCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				naTable.addCell(naCell1);
				
				naTable.addCell(slashCell);
				
				Phrase noPhrse1 =  new Phrase("NO",font9);
				PdfPCell noCell1 = new PdfPCell(noPhrse1);
				noCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				noCell1.setBorder(0);
				naTable.addCell(noCell1);
				
				
				PdfPCell naTableCell = new PdfPCell(naTable);
				naTableCell.setBorder(0);
				
				
				PdfPTable noTable = new PdfPTable(5);
				noTable.setWidthPercentage(100f);
				
//				noTable.setSpacingBefore(10f);
//				noTable.setSpacingAfter(10f);
				 try {
					 noTable.setWidths(columnWidths16);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				
				
				Phrase yesPhrse2 =  new Phrase("YES",font9);
				PdfPCell yesCell2 = new PdfPCell(yesPhrse2);
				yesCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				yesCell2.setBorder(0);
				noTable.addCell(yesCell2);
				
				Phrase slashPhrse2 =  new Phrase("/",font9);
				PdfPCell slashCell2 = new PdfPCell(slashPhrse2);
				slashCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				slashCell2.setBorder(0);
				noTable.addCell(slashCell2);
				
				Phrase naPhrse2 =  new Phrase("NA",font9);
				PdfPCell naCell2 = new PdfPCell(naPhrse2);
				naCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				naCell2.setBorder(0);
				noTable.addCell(naCell2);
				
				noTable.addCell(slashCell);
				
				Phrase noPhrse2 =  new Phrase("NO",font9bold);
				PdfPCell noCell2 = new PdfPCell(noPhrse2);
				noCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				noTable.addCell(noCell2);
				
				
				PdfPCell noTableCell = new PdfPCell(noTable);
				noTableCell.setBorder(0);
				
				
				
				
				
				//*****************************************
		
		
		
		
		String	titlepdf="WRAPPING AND TIMBER";
		
		Phrase titlephrase=new Phrase(titlepdf,font10boldul);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
//		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
//		String	dotedline="- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
//				+ " - - - - - - - - - - - - - - -";
//		Phrase dotphrase =new Phrase(dotedline,font10bold);
//		PdfPCell dotcell = new PdfPCell(dotphrase);
//		dotcell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(titlecell);
//		titlepdftable.addCell(dotcell);
		
		
		
		String note1="Has the commodity been fumigated prior to lacquering,varnishing,painting or wrapping ?";
		String note2="Has plastic Wrapping been use in consignment ?";
		String note3="*If yes, has the consignment been fumigated prior wrapping ?";
		String note4="*Or has the plastic wrapping been slashed,opened or perforated in accordance with ";
		String note44="the wrapping and perforation standards ?";
		String note5="Is the timber in this consignment less than 200 mm thick in dimention and correctly ";
		String note55="spaced every 200 mm in height ?";
		Phrase notephrase = null;
		String answer1= null;
		String answer2= null;
		String answer3= null;
		String answer4= null;
		String answer5= null;
		
		
		if(fumigation.getNote11()!=null&&!fumigation.getNote11().equals("--SELECT--")){
			answer1=fumigation.getNote11();
//			 notephrase=new Phrase(fumigation.getNote11(),font9);
		}else{
			answer1="";
//			notephrase=new Phrase(" ",font10);
		}
		
//		 PdfPCell notecell=new PdfPCell(notephrase);
//		 notecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		 notecell.setBorder(0);
		
		 
		 Phrase notephrase1 = null;
			if(fumigation.getNote12()!=null&&!fumigation.getNote12().equals("--SELECT--")){
				answer2=fumigation.getNote12();
//				 notephrase1=new Phrase(fumigation.getNote12(),font9);
			}else{
				answer2="";
//				notephrase1=new Phrase(" ",font10);
			}
			
//			 PdfPCell notecell1=new PdfPCell(notephrase1);
//			 notecell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 notecell1.setBorder(0);
		 
			 Phrase notephrase2 = null;
				if(fumigation.getNote13()!=null&&!fumigation.getNote13().equals("--SELECT--")){
					answer3=fumigation.getNote13();
//					 notephrase2=new Phrase(fumigation.getNote13(),font9);
				}else{
					answer3="";
//					notephrase2=new Phrase(" ",font10);
				}
				
//				 PdfPCell notecell2=new PdfPCell(notephrase2);
//				 notecell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//				 notecell2.setBorder(0);
		 
				 
				 Phrase notephrase3 = null;
					if(fumigation.getNote14()!=null&&!fumigation.getNote14().equals("--SELECT--")){
						answer4 =fumigation.getNote14();
//						 notephrase3=new Phrase(fumigation.getNote14(),font9);
					}else{
						answer4="";
//						notephrase3=new Phrase(" ",font10);
					}
					
//					 PdfPCell notecell3=new PdfPCell(notephrase3);
//					 notecell3.setHorizontalAlignment(Element.ALIGN_CENTER);
//					 notecell3.setBorder(0);
					 
					 
					 
					 
					 Phrase notephrase4 = null;
						if(fumigation.getNote15()!=null&&!fumigation.getNote15().equals("--SELECT--")){
							answer5 =fumigation.getNote15();
//							 notephrase4=new Phrase(fumigation.getNote15(),font9);
						}else{
							answer5="";
//							notephrase4=new Phrase(" ",font10);
						}
						
//						 PdfPCell notecell4=new PdfPCell(notephrase4);
//						 notecell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//						 notecell4.setBorder(0);
				 
		Phrase impcountryphrase=new Phrase(note1,font9);
		Phrase impcountry1phrase=new Phrase(note2,font9);
		Phrase impcountry2phrase=new Phrase(note3,font9);
		Phrase impcountry3phrase=new Phrase(note4,font9);
		Phrase impcountry33phrase=new Phrase(note44,font9);
		Phrase impcountry4phrase=new Phrase(note5,font9);
		Phrase impcountry44phrase=new Phrase(note55,font9);
		
		
		Phrase blank= new Phrase(" ",font9);
		PdfPCell blankl = new PdfPCell(blank);
		blankl.setBorder(0);
		
		PdfPCell impcell=new PdfPCell(impcountryphrase);
		impcell.setBorder(0);
		
		PdfPCell impcell1=new PdfPCell(impcountry1phrase);
		impcell1.setBorder(0);
		
		PdfPCell impcell2=new PdfPCell(impcountry2phrase);
		impcell2.setBorder(0);
		PdfPCell impcell3=new PdfPCell(impcountry3phrase);
		impcell3.setBorder(0);
		
		PdfPCell impcell33=new PdfPCell(impcountry33phrase);
		impcell33.setBorder(0);
		
		PdfPCell impcell4=new PdfPCell(impcountry4phrase);
		impcell4.setBorder(0);
		
		PdfPCell impcell44=new PdfPCell(impcountry44phrase);
		impcell44.setBorder(0);
		
		PdfPTable imptable=new PdfPTable(2);
		imptable.setWidthPercentage(100f);
//		imptable.setSpacingAfter(5f);
		try {
			imptable.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		imptable.addCell(impcell);
		
		if(answer1!=null&&answer1.endsWith("YES")){
			imptable.addCell(yesTableCell);
			}
			else if(answer1!=null&&answer1.endsWith("NO"))
			{
				imptable.addCell(noTableCell);	
			}
			else
			{
				imptable.addCell(naTableCell);
			}	
		
//		imptable.addCell(notecell);
		
		imptable.addCell(impcell1);
		
		if(answer2!=null&&answer2.endsWith("YES")){
			imptable.addCell(yesTableCell);
			}
			else if(answer2!=null&&answer2.endsWith("NO"))
			{
				imptable.addCell(noTableCell);	
			}
			else
			{
				imptable.addCell(naTableCell);
			}
		
//		imptable.addCell(notecell1);
		imptable.addCell(impcell2);
		if(answer3!=null&&answer3.endsWith("YES")){
			imptable.addCell(yesTableCell);
			}
			else if(answer3!=null&&answer3.endsWith("NO"))
			{
				imptable.addCell(noTableCell);	
			}
			else
			{
				imptable.addCell(naTableCell);
			}
//		imptable.addCell(notecell2);
		imptable.addCell(impcell3);
		if(answer4!=null&&answer4.endsWith("YES")){
			imptable.addCell(yesTableCell);
			}
			else if(answer4!=null&&answer4.endsWith("NO"))
			{
				imptable.addCell(noTableCell);	
			}
			else
			{
				imptable.addCell(naTableCell);
			}
		imptable.addCell(impcell33);
		imptable.addCell(blankl);
		// for blank 
		
//		imptable.addCell(notecell3);
		imptable.addCell(impcell4);
		if(answer5!=null&&answer5.endsWith("YES")){
			imptable.addCell(yesTableCell);
			}
			else if(answer5!=null&&answer5.endsWith("NO"))
			{
				imptable.addCell(noTableCell);	
			}
			else
			{
				imptable.addCell(naTableCell);
			}
		imptable.addCell(impcell44);
		imptable.addCell(blankl);
//		imptable.addCell(notecell4);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell();
		titlePdfCell.addElement(titlepdftable);
		titlePdfCell.setBorderWidthBottom(0);
		
//		titlePdfCell.setBorderWidthLeft(0);
//		titlePdfCell.setBorderWidthRight(0);
//		
		
		PdfPCell titlePdfCell2=new PdfPCell(imptable);
		titlePdfCell2.setBorderWidthTop(0);
		
//		titlePdfCell2.setBorderWidthLeft(0);
//		titlePdfCell2.setBorderWidthRight(0);
		
		parent.addCell(titlePdfCell);
		parent.addCell(titlePdfCell2);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private void createTreatmentDetails() {

		
		String	titlepdf="DETAILS OF TREATEMENT";
		
		Phrase titlephrase=new Phrase(titlepdf,font10boldul);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
//		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
//		String	dotedline="- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
//				+ " - - - - - - - - - - - - - - -";
//		Phrase dotphrase =new Phrase(dotedline,font10bold);
//		PdfPCell dotcell = new PdfPCell(dotphrase);
//		dotcell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		
		 Phrase namefumihation=new Phrase("Name of Fumigation",font9);
		 PdfPCell namefumicell=new PdfPCell();
		 namefumicell.addElement(namefumihation);
		 namefumicell.setBorder(0);
		 namefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase namefumiValue=null;
			 if(!fumigation.getNameoffumigation().equals("--SELECT--")){
				 namefumiValue=new Phrase(fumigation.getNameoffumigation(),font9);
			 }
		 else
		 {
			 namefumiValue=new Phrase("",font9);
		 }
		 
		 PdfPCell namefumiValuecell=new PdfPCell();
		 namefumiValuecell.addElement(namefumiValue);
		 namefumiValuecell.setBorder(0);
		 namefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		 Phrase dateoffumi=new Phrase("Date of Fumigation",font9);
		 PdfPCell datefumicell=new PdfPCell();
		 datefumicell.addElement(dateoffumi);
		 datefumicell.setBorder(0);
		 datefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase dateoffumiValue=null;
			 if(fumigation.getDateoffumigation()!=null){
				 dateoffumiValue=new Phrase(fmt.format(fumigation.getDateoffumigation()),font9);
			 }
		 else
		 {
			 dateoffumiValue=new Phrase("",font9);
		 }
		 
		 PdfPCell datefumiValuecell=new PdfPCell();
		 datefumiValuecell.addElement(dateoffumiValue);
		 datefumiValuecell.setBorder(0);
		 datefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		 Phrase placeoffumi=new Phrase("Place of Fumigation",font9);
		 PdfPCell placefumicell=new PdfPCell();
		 placefumicell.addElement(placeoffumi);
		 placefumicell.setBorder(0);
		 placefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase placefumiValue=null;
			 if(fumigation.getPlaceoffumigation()!=null){
				 placefumiValue=new Phrase(fumigation.getPlaceoffumigation(),font9);
			 }
		 else
		 {
			 placefumiValue=new Phrase("",font9);
		 }
		 
		 PdfPCell placefumiValuecell=new PdfPCell();
		 placefumiValuecell.addElement(placefumiValue);
		 placefumiValuecell.setBorder(0);
		 placefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		 Phrase dosefumigation=new Phrase("Dosage Rate Of fumigation",font9);
		 PdfPCell dosafumicell=new PdfPCell();
		 dosafumicell.addElement(dosefumigation);
		 dosafumicell.setBorder(0);
		 dosafumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase dosefumiValue=null;
			 if(fumigation.getDoseratefumigation()!=null){
				 dosefumiValue=new Phrase(fumigation.getDoseratefumigation()+" gms/m3",font9);
			 }
		 else
		 {
			 dosefumiValue=new Phrase("",font9);
		 }
		 
		 PdfPCell dosafumiValuecell=new PdfPCell();
		 dosafumiValuecell.addElement(dosefumiValue);
		 dosafumiValuecell.setBorder(0);
		 dosafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		 Phrase durationfumi=new Phrase("Duration of Fumigation",font9);
		 PdfPCell durarationfumicell=new PdfPCell();
		 durarationfumicell.addElement(durationfumi);
		 durarationfumicell.setBorder(0);
		 durarationfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase durafumiValue=null;
			 if(fumigation.getDurartionfumigation()!=null){
				 durafumiValue=new Phrase(fumigation.getDurartionfumigation()+" hrs",font9);
			 }
		 else
		 {
			 durafumiValue=new Phrase("",font9);
		 }
		 
		 PdfPCell durafumiValuecell=new PdfPCell();
		 durafumiValuecell.addElement(durafumiValue);
		 durafumiValuecell.setBorder(0);
		 durafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		 
		 
		 
		 
		 Phrase minaietemp=new Phrase("Min. Air Temp.(in "+"\u00b0"+"C)",font9);
		 PdfPCell tempfumicell=new PdfPCell();
		 tempfumicell.addElement(minaietemp);
		 tempfumicell.setBorder(0);
		 tempfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase tempfumiValue=null;
			 if(fumigation.getMinairtemp()!=null){
				 tempfumiValue=new Phrase(fumigation.getMinairtemp()+" "+"\u00b0"+"C",font9);
			 }
		 else
		 {
			 tempfumiValue=new Phrase("",font9);
		 }
		 
		 PdfPCell tempfumiValuecell=new PdfPCell();
		 tempfumiValuecell.addElement(tempfumiValue);
		 tempfumiValuecell.setBorder(0);
		 tempfumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 
		 Phrase seperator=new Phrase(":",font9);
		 PdfPCell seperatorcell=new PdfPCell();
		 seperatorcell.addElement(seperator);
		 seperatorcell.setBorder(0);
		 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
		 PdfPTable infotable=new PdfPTable(6);
		 
		 infotable.setWidthPercentage(100f);
		 
		 try {
			infotable.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		 if(fumigation.getNameoffumigation()!=null){
			 infotable.addCell(namefumicell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(namefumiValuecell);
			 }
			 
			 if(fumigation.getDateoffumigation()!=null){
			 infotable.addCell(datefumicell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(datefumiValuecell);
			 }
			 
			 if(fumigation.getPlaceoffumigation()!=null){
			 infotable.addCell(placefumicell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(placefumiValuecell);
			 }
			 
			 if(fumigation.getDoseratefumigation()!=null){
			 infotable.addCell(dosafumicell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(dosafumiValuecell);
			 }
			
			 
			 
			 if(fumigation.getDurartionfumigation()!=null){
			 infotable.addCell(durarationfumicell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(durafumiValuecell);
			 }	
			 
			 
			 if(fumigation.getMinairtemp()!=null){
				 infotable.addCell(tempfumicell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(tempfumiValuecell);
				 }
		 
		 
			 PdfPCell infocell=new PdfPCell(infotable);
			 infocell.setBorder(0);
			 
			 
			 //*********************
				PdfPTable yesTable = new PdfPTable(5);
				yesTable.setWidthPercentage(100f);
//				yesTable.setSpacingBefore(10f);
//				yesTable.setSpacingAfter(10f);
				
				 try {
					 yesTable.setWidths(columnWidths16);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				
				Phrase yesPhrse =  new Phrase("YES",font9bold);
				PdfPCell yesCell = new PdfPCell(yesPhrse);
				yesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				yesCell.setBorder(0);
				PdfPTable smallyes = new PdfPTable(1);
				smallyes.addCell(yesCell);
				PdfPCell smalltableCell =new PdfPCell(smallyes);
				yesTable.addCell(smalltableCell);
				
				Phrase slashPhrse =  new Phrase("/",font9);
				PdfPCell slashCell = new PdfPCell(slashPhrse);
				slashCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				slashCell.setBorder(0);
				yesTable.addCell(slashCell);
				
				Phrase naPhrse =  new Phrase("NA",font9);
				PdfPCell naCell = new PdfPCell(naPhrse);
				naCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				naCell.setBorder(0);
				yesTable.addCell(naCell);
				
				yesTable.addCell(slashCell);
				
				Phrase noPhrse =  new Phrase("NO",font9);
				PdfPCell noCell = new PdfPCell(noPhrse);
				noCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				noCell.setBorder(0);
				yesTable.addCell(noCell);
				
				
				PdfPCell yesTableCell = new PdfPCell(yesTable);
				yesTableCell.setBorder(0);
				
				
				
				
				PdfPTable naTable = new PdfPTable(5);
				naTable.setWidthPercentage(100f);
				
//				naTable.setSpacingBefore(10f);
//				naTable.setSpacingAfter(10f);
				 try {
					 naTable.setWidths(columnWidths16);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				
				
				Phrase yesPhrse1 =  new Phrase("YES",font9);
				PdfPCell yesCell1 = new PdfPCell(yesPhrse1);
				yesCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				yesCell1.setBorder(0);
				naTable.addCell(yesCell1);
				
				Phrase slashPhrse1 =  new Phrase("/",font9);
				PdfPCell slashCell1 = new PdfPCell(slashPhrse1);
				slashCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				slashCell1.setBorder(0);
				naTable.addCell(slashCell1);
				
				Phrase naPhrse1 =  new Phrase("NA",font9bold);
				PdfPCell naCell1 = new PdfPCell(naPhrse1);
				naCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				naTable.addCell(naCell1);
				
				naTable.addCell(slashCell);
				
				Phrase noPhrse1 =  new Phrase("NO",font9);
				PdfPCell noCell1 = new PdfPCell(noPhrse1);
				noCell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				noCell1.setBorder(0);
				naTable.addCell(noCell1);
				
				
				PdfPCell naTableCell = new PdfPCell(naTable);
				naTableCell.setBorder(0);
				
				
				PdfPTable noTable = new PdfPTable(5);
				noTable.setWidthPercentage(100f);
				
//				noTable.setSpacingBefore(10f);
//				noTable.setSpacingAfter(10f);
				 try {
					 noTable.setWidths(columnWidths16);
					} catch (DocumentException e1) {
						e1.printStackTrace();
					}
				
				
				Phrase yesPhrse2 =  new Phrase("YES",font9);
				PdfPCell yesCell2 = new PdfPCell(yesPhrse2);
				yesCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				yesCell2.setBorder(0);
				noTable.addCell(yesCell2);
				
				Phrase slashPhrse2 =  new Phrase("/",font9);
				PdfPCell slashCell2 = new PdfPCell(slashPhrse2);
				slashCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				slashCell2.setBorder(0);
				noTable.addCell(slashCell2);
				
				Phrase naPhrse2 =  new Phrase("NA",font9);
				PdfPCell naCell2 = new PdfPCell(naPhrse2);
				naCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				naCell2.setBorder(0);
				noTable.addCell(naCell2);
				
				noTable.addCell(slashCell);
				
				Phrase noPhrse2 =  new Phrase("NO",font9bold);
				PdfPCell noCell2 = new PdfPCell(noPhrse2);
				noCell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				noTable.addCell(noCell2);
				
				
				PdfPCell noTableCell = new PdfPCell(noTable);
				noTableCell.setBorder(0);
			 
			 //*********************
				String answer1= null;
				String answer2= null;
				String answer3= null;
				String answer4= null;
				String answer5= null;
				
			 String fumigationnote="Fumigation has been performed in a container / under Gas tight enclosure / sheet";
			 String fumigationnote1="Container pressure test conducted";
			 String fumigationnote2="Container has 200 mm free air space at top of container";
			 String fumigationnote3="In transit fumigation need ventillation at port of discharge";
			 String fumigationnote4="Container/Enclosure has been ventillated to below 5ppm v/v Methyl Bromide";
			 
			 Phrase notephrase=new Phrase(fumigationnote,font9);
			 PdfPCell notecell=new PdfPCell();
			 notecell.addElement(notephrase);
			 notecell.setBorder(0);
			 
			 Phrase note123phrase=null;
			 
			 if(fumigation.getNote123()!=null&&!fumigation.getNote123().equals("--SELECT--")){
				 answer1=fumigation.getNote123().trim();
//				 note123phrase=new Phrase(fumigation.getNote123(),font9);
			 }else{
//				 note123phrase=new Phrase(" ",font9);
				 answer1="";
			 }
			 
//			 PdfPCell notecell123=new PdfPCell(note123phrase);
//			 notecell123.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 notecell123.setBorder(0);
			 
			 
			 Phrase notephrase1=new Phrase(fumigationnote1,font9);
			 PdfPCell notecell21=new PdfPCell();
			 notecell21.addElement(notephrase1);
			 notecell21.setBorder(0);
			 
			 Phrase note1phrase=null;
			 
			 if(fumigation!=null&&fumigation.getNote1()!=null&&!fumigation.getNote1().equals("--SELECT--")){
				 answer2=fumigation.getNote1();
//				 note1phrase=new Phrase(fumigation.getNote1(),font9);
			 }else{
				 answer2=null;
//				 note1phrase=new Phrase(" ",font9);
			 }
				 
			 
			 
//			 PdfPCell notecell2=new PdfPCell(note1phrase);
//			 notecell2.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 notecell2.setBorder(0);
			 
			 Phrase notephrase3=new Phrase(fumigationnote2,font9);
			 PdfPCell notecell3=new PdfPCell();
			 notecell3.addElement(notephrase3);
			 notecell3.setBorder(0);
			 
			 
			 Phrase note2phrase=null;
			 
			 if(fumigation!=null&&fumigation.getNote2()!=null&&!fumigation.getNote2().equals("--SELECT--")){
				 answer3=fumigation.getNote2().trim();
//				 note2phrase=new Phrase(fumigation.getNote2(),font9);
			 }else{
//				 note2phrase=new Phrase(" ",font9);
				 answer3="";
			 }
			 
			 
//			 PdfPCell notecell4=new PdfPCell(note2phrase);
//			 notecell4.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 notecell4.setBorder(0);
			 
			 Phrase notephrase4=new Phrase(fumigationnote3,font9);
			 PdfPCell notecell5=new PdfPCell();
			 notecell5.addElement(notephrase4);
			 notecell5.setBorder(0);
			 
			 
			 
			 Phrase note3phrase=null;
			 
			 if(fumigation!=null&&fumigation.getNote3()!=null&&!fumigation.getNote3().equals("--SELECT--")){
				 answer4=fumigation.getNote3();
//				 note3phrase=new Phrase(fumigation.getNote3(),font9);
			 }else{
//				 note3phrase=new Phrase(" ",font9);
				 answer4="";
			 }
			 
			 
//			 PdfPCell notecell6=new PdfPCell(note3phrase);
//			 notecell6.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 notecell6.setBorder(0);
			 
			 Phrase notephrase5=new Phrase(fumigationnote4,font9);
			 PdfPCell notecell7=new PdfPCell();
			 notecell7.addElement(notephrase5);
			 notecell7.setBorder(0);
			 
			 
			 Phrase note4phrase=null;
			 
			 if(fumigation!=null&&fumigation.getNote4()!=null&&!fumigation.getNote4().equals("--SELECT--")){
				 answer5=fumigation.getNote4();
//				 note4phrase=new Phrase(fumigation.getNote4(),font9);
			 }else{
//				 note4phrase=new Phrase(" ",font9);
				 answer5="";
			 }
			 
//			 PdfPCell notecell8=new PdfPCell(note4phrase);
//			 notecell8.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 notecell8.setBorder(0);
			 
			 
			 
		 PdfPTable notetable=new PdfPTable(2);
		 notetable.setWidthPercentage(100f);
		 try {
			notetable.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		 notetable.addCell(notecell);
//		 notetable.addCell(notecell123);
		 if(answer1!=null&&answer1.endsWith("YES")){
				notetable.addCell(yesTableCell);
				}
				else if(answer1!=null&&answer1.endsWith("NO"))
				{
				notetable.addCell(noTableCell);	
				}
				else
				{
					notetable.addCell(naTableCell);
				}
		 
		 
		 notetable.addCell(notecell21);
//		 notetable.addCell(notecell2);
		 if(answer2!=null&&answer2.endsWith("YES")){
				notetable.addCell(yesTableCell);
				}
				else if(answer1!=null&&answer1.endsWith("NO"))
				{
				notetable.addCell(noTableCell);	
				}
				else
				{
					notetable.addCell(naTableCell);
				}
		 
		 
		 notetable.addCell(notecell3);
//		 notetable.addCell(notecell4);
		 if(answer3!=null&&answer3.endsWith("YES")){
				notetable.addCell(yesTableCell);
				}
				else if(answer1!=null&&answer1.endsWith("NO"))
				{
				notetable.addCell(noTableCell);	
				}
				else
				{
					notetable.addCell(naTableCell);
				}
		 
		 
		 notetable.addCell(notecell5);
//		 notetable.addCell(notecell6);
		 if(answer4!=null&&answer4.endsWith("YES")){
				notetable.addCell(yesTableCell);
				}
				else if(answer1!=null&&answer1.endsWith("NO"))
				{
				notetable.addCell(noTableCell);	
				}
				else
				{
					notetable.addCell(naTableCell);
				}
		 
		 
		 notetable.addCell(notecell7);
//		 notetable.addCell(notecell8);
		 
		 if(answer5!=null&&answer5.endsWith("YES")){
				notetable.addCell(yesTableCell);
				}
				else if(answer1!=null&&answer1.endsWith("NO"))
				{
				notetable.addCell(noTableCell);	
				}
				else
				{
					notetable.addCell(naTableCell);
				}
		 
		 
		 PdfPCell notetablecell=new PdfPCell();
		 notetablecell.addElement(notetable);
		 notetablecell.setBorder(0);
		 
//		 PdfPCell titlePdfCell1=new PdfPCell();
//			titlePdfCell1.addElement(notetable);
//			titlePdfCell1.setBorder(0);
			
		 PdfPTable maintable=new PdfPTable(1);
		 maintable.setWidthPercentage(100f);
		 maintable.addCell(infocell);
		 maintable.addCell(notetablecell);
		 
		PdfPTable titlepdftable=new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(titlecell);
//		titlepdftable.addCell(dotcell);
		
		

		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell();
		titlePdfCell.addElement(titlepdftable);
		titlePdfCell.setBorderWidthBottom(0);
		
//		titlePdfCelft(0);
//		titlePdfCell.setBorderWidthRight(0);
		
		
		PdfPCell maincell=new PdfPCell();
		maincell.addElement(maintable);
		maincell.setBorderWidthTop(0);
//		maincell.setBorderWidthLeft(0);
//		maincell.setBorderWidthRight(0);
		
		parent.addCell(titlePdfCell);
		parent.addCell(maincell);
		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}






	private void createGoodsDetails() {

		String	titlepdf="DETAILS OF GOODS";
		
		Phrase titlephrase=new Phrase(titlepdf,font10boldul);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
//		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
//		String	dotedline="- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
//				+ " - - - - - - - - - - - - - - -";
//		Phrase dotphrase =new Phrase(dotedline,font10bold);
//		PdfPCell dotcell = new PdfPCell(dotphrase);
//		dotcell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(titlecell);
//		titlepdftable.addCell(dotcell);

		 PdfPCell infotablecell=null;
			 
		 Phrase descgoods=new Phrase("Description Of Goods:",font9);
		 PdfPCell descgoodscell=new PdfPCell();
		 descgoodscell.addElement(descgoods);
		 descgoodscell.setBorder(0);
		 descgoodscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase descValue=null;
			 if(fumigation.getDescriptionofgoods()!=null){
				 descValue=new Phrase(fumigation.getDescriptionofgoods(),font9);
			 }
		 else
		 {
			 descValue=new Phrase("",font9);
		 }
		 
		 PdfPCell descValuecell=new PdfPCell();
		 descValuecell.addElement(descValue);
		 descValuecell.setBorder(0);
		 descValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase qtydeclar=new Phrase("Quantity Declared:",font9);
		 PdfPCell qtydeclarecell=new PdfPCell();
		 qtydeclarecell.addElement(qtydeclar);
		 qtydeclarecell.setBorder(0);
		 qtydeclarecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase qtydeclartValue=null;
			 if((fumigation.getQuantitydeclare()!=null)&&(fumigation.getQuantitydeclareUnit()!=null)){
				
				 qtydeclartValue=new Phrase(fumigation.getQuantitydeclare()+" "+fumigation.getQuantitydeclareUnit(),font9);
		 }
		 else
		 {
			 qtydeclartValue=new Phrase("",font9);
		 }
		 
		 PdfPCell qtyValuecell=new PdfPCell();
		 qtyValuecell.addElement(qtydeclartValue);
		 qtyValuecell.setBorder(0);
		 qtyValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 //*****************************************
		 
		 
		 Phrase distMark=new Phrase("Distinguishing Marks:",font9);
		 PdfPCell distmarkcell=new PdfPCell();
		 distmarkcell.addElement(distMark);
		 distmarkcell.setBorder(0);
		 distmarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase distmarkValue=null;
			 if(fumigation.getDistiinguishingmarks()!=null){
				 distmarkValue=new Phrase(fumigation.getDistiinguishingmarks(),font9);
		 }
		 else
		 {
			 distmarkValue=new Phrase("",font9);
		 }
		
		 PdfPCell distmarkValuecell=new PdfPCell();
		 distmarkValuecell.addElement(distmarkValue);
		 distmarkValuecell.setBorder(0);
		 distmarkValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase conslisk=new Phrase("Consignment Link/Container:",font9);
		 PdfPCell conslinkcell=new PdfPCell();
		 conslinkcell.addElement(conslisk);
		 conslinkcell.setBorder(0);
		 conslinkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase conslinkValue=null;
		 
			 if(fumigation.getConsignmentlink()!=null){
				 conslinkValue=new Phrase(fumigation.getConsignmentlink(),font9);
			 }
		 else
		 {
			 conslinkValue=new Phrase("",font9);
		 }
		
		 PdfPCell conslinkValuecell=new PdfPCell();
		 conslinkValuecell.addElement(conslinkValue);
		 conslinkValuecell.setBorder(0);
		 conslinkValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 
		 
//		 Phrase containno=new Phrase("Container No.",font9);
//		 PdfPCell containernocell=new PdfPCell();
//		 containernocell.addElement(containno);
//		 containernocell.setBorder(0);
//		 containernocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
//		 Phrase containoValue=null;
//		 
//			 if(fumigation.getContainerno()!=null){
//				 containoValue=new Phrase(fumigation.getContainerno(),font9);
//			 }
//		 else
//		 {
//			 containoValue=new Phrase("",font9);
//		 }
//		
//		 PdfPCell containoValuecell=new PdfPCell();
//		 containoValuecell.addElement(containoValue);
//		 containoValuecell.setBorder(0);
//		 containoValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 
		 Phrase pcofloading=new Phrase("Port & Country Of loading",font9);
		 PdfPCell pocloadcell=new PdfPCell();
		 pocloadcell.addElement(pcofloading);
		 pocloadcell.setBorder(0);
		 pocloadcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase pocloadValue=null;
		 
			 if(fumigation.getPortncountryloading()!=null){
				 pocloadValue=new Phrase(fumigation.getPortncountryloading(),font9);
			 }
		 else
		 {
			 pocloadValue=new Phrase("",font9);
		 }
		
		 PdfPCell pocloadValuecell=new PdfPCell();
		 pocloadValuecell.addElement(pocloadValue);
		 pocloadValuecell.setBorder(0);
		 pocloadValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 Phrase nameofvessel=new Phrase("Name of Vessel/Ship",font9);
		 PdfPCell namevesscell=new PdfPCell();
		 namevesscell.addElement(nameofvessel);
		 namevesscell.setBorder(0);
		 namevesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase namevessValue=null;
		 
			 if(fumigation.getNameofvessel()!=null){
				 namevessValue=new Phrase(fumigation.getNameofvessel(),font9);
			 }
		 else
		 {
			 namevessValue=new Phrase("",font9);
		 }
		
		 PdfPCell namevessValuecell=new PdfPCell();
		 namevessValuecell.addElement(namevessValue);
		 namevessValuecell.setBorder(0);
		 namevessValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase contrydest=new Phrase("Country Of Destination",font9);
		 PdfPCell codestcell=new PdfPCell();
		 codestcell.addElement(contrydest);
		 codestcell.setBorder(0);
		 codestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase conrydestValue=null;
		 
			 if(fumigation.getCountryofdestination()!=null){
				 conrydestValue=new Phrase(fumigation.getCountryofdestination(),font9);
			 }
		 else
		 {
			 conrydestValue=new Phrase("",font9);
		 }
		
		 PdfPCell contrydestValuecell=new PdfPCell();
		 contrydestValuecell.addElement(conrydestValue);
		 contrydestValuecell.setBorder(0);
		 contrydestValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 Phrase dpentry=new Phrase("Declared Point Of Entry",font9);
		 PdfPCell dpentrycell=new PdfPCell();
		 dpentrycell.addElement(dpentry);
		 dpentrycell.setBorder(0);
		 dpentrycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase dpentryValue=null;
		 
			 if(fumigation.getDeclarepointentry()!=null){
				 dpentryValue=new Phrase(fumigation.getDeclarepointentry(),font9);
			 }
		 else
		 {
			 dpentryValue=new Phrase("",font9);
		 }
		
		 PdfPCell dpentryValuecell=new PdfPCell();
		 dpentryValuecell.addElement(dpentryValue);
		 dpentryValuecell.setBorder(0);
		 dpentryValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 //  rohan added 3 fields for sidhivinayak 
		 Phrase packing=new Phrase("Packing",font9);
		 PdfPCell packingCell=new PdfPCell();
		 packingCell.addElement(packing);
		 packingCell.setBorder(0);
		 packingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase packingValue=null;
		 
			 if(fumigation.getPackingValue()!=null){
				 packingValue=new Phrase(fumigation.getPackingValue(),font9);
			 }
		 else
		 {
			 packingValue=new Phrase("",font9);
		 }
		
		 PdfPCell packingValueCell=new PdfPCell();
		 packingValueCell.addElement(packingValue);
		 packingValueCell.setBorder(0);
		 packingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 Phrase volumeOfContainer=new Phrase("Volume of container",font9);
		 PdfPCell volumeOfContainerCell=new PdfPCell();
		 volumeOfContainerCell.addElement(volumeOfContainer);
		 volumeOfContainerCell.setBorder(0);
		 volumeOfContainerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase volumeOfContainerValue=null;
		 
			 if(fumigation.getVolumeofcontainer()!=null){
				 volumeOfContainerValue=new Phrase(fumigation.getVolumeofcontainer(),font9);
			 }
		 else
		 {
			 volumeOfContainerValue=new Phrase("",font9);
		 }
		
		 PdfPCell volumeOfContainerValueCell=new PdfPCell();
		 volumeOfContainerValueCell.addElement(volumeOfContainerValue);
		 volumeOfContainerValueCell.setBorder(0);
		 volumeOfContainerValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase totalNetweight=new Phrase("Total Net Weight",font9);
		 PdfPCell totalNetweightCell=new PdfPCell();
		 totalNetweightCell.addElement(totalNetweight);
		 totalNetweightCell.setBorder(0);
		 totalNetweightCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase totalNetweightValue=null;
		 
			 if(fumigation.getTotalNetWeight()!=null){
				 totalNetweightValue=new Phrase(fumigation.getTotalNetWeight(),font9);
			 }
		 else
		 {
			 totalNetweightValue=new Phrase("",font9);
		 }
		
		 PdfPCell totalNetweightValueCell=new PdfPCell();
		 totalNetweightValueCell.addElement(totalNetweightValue);
		 totalNetweightValueCell.setBorder(0);
		 totalNetweightValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase grossWeight=new Phrase("Gross Weight",font9);
		 PdfPCell grossWeightCell=new PdfPCell();
		 grossWeightCell.addElement(grossWeight);
		 grossWeightCell.setBorder(0);
		 grossWeightCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 Phrase grossweightValue=null;
		 
			 if(fumigation.getGrossWeight()!=null){
				 grossweightValue=new Phrase(fumigation.getGrossWeight(),font9);
			 }
		 else
		 {
			 grossweightValue=new Phrase("",font9);
		 }
		
		 PdfPCell grossWeightValueCell=new PdfPCell();
		 grossWeightValueCell.addElement(grossweightValue);
		 grossWeightValueCell.setBorder(0);
		 grossWeightValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 Phrase seperator=new Phrase(":",font9);
		 PdfPCell seperatorcell=new PdfPCell();
		 seperatorcell.addElement(seperator);
		 seperatorcell.setBorder(0);
		 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 
		 //******************rohan 
		 PdfPTable infotableCol=new PdfPTable(2);
		 infotableCol.setWidthPercentage(100f);
		 
		    try {
		    	infotableCol.setWidths(new float[]{24,76});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		 
		 if(fumigation.getDescriptionofgoods()!=null){
			 infotableCol.addCell(descgoodscell);
			 infotableCol.addCell(descValuecell);
			 }
			 
			 if(fumigation.getQuantitydeclare()!=null){
				 infotableCol.addCell(qtydeclarecell);
				 infotableCol.addCell(qtyValuecell);
			 }
			 
			 if(fumigation.getDistiinguishingmarks()!=null){
				 infotableCol.addCell(distmarkcell);
				 infotableCol.addCell(distmarkValuecell);
			 }
			 
			 if(fumigation.getConsignmentlink()!=null){
				 infotableCol.addCell(conslinkcell);
				 infotableCol.addCell(conslinkValuecell);
			 }
		 
		 
		 //****************
		 
		 PdfPTable infotable=new PdfPTable(6);
		 
		 infotable.setWidthPercentage(100f);
//		 infotable.setSpacingAfter(10f);
		 
//		 if(fumigation.getContainerno()!=null){
//		 infotable.addCell(blankCell);
//		 infotable.addCell(blankCell);
//		 infotable.addCell(blankCell);
//		 }	
		 
		 
//		 if(fumigation.getContainerno()!=null){
//			 infotable.addCell(containernocell);
//			 infotable.addCell(seperatorcell);
//			 infotable.addCell(containoValuecell);
//			 }
		 
		 
		 
		 
		 if(fumigation.getPackingValue()!=null){
			 infotable.addCell(packingCell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(packingValueCell);
			 }
		 
		 if(fumigation.getVolumeofcontainer()!=null){
			 infotable.addCell(volumeOfContainerCell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(volumeOfContainerValueCell);
			 }
		 if(fumigation.getTotalNetWeight()!=null){
			 infotable.addCell(totalNetweightCell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(totalNetweightValueCell);
			 }
		 
		 if(fumigation.getGrossWeight()!=null){
			 infotable.addCell(grossWeightCell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(grossWeightValueCell);
			 }
		 
		 if(fumigation.getPortncountryloading()!=null){
			 infotable.addCell(pocloadcell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(pocloadValuecell);
			 }
		 if(fumigation.getNameofvessel()!=null){
			 infotable.addCell(namevesscell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(namevessValuecell);
			 }
		 if(fumigation.getCountryofdestination()!=null){
			 infotable.addCell(codestcell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(contrydestValuecell);
			 }
		 if(fumigation.getDeclarepointentry()!=null){
			 infotable.addCell(dpentrycell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(dpentryValuecell);
			 }
	 
		 try {
			infotable.setWidths(columnWidths);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		} 
		 
		  infotablecell=new PdfPCell();
		  infotablecell.addElement(infotableCol);
		  infotablecell.addElement(infotable);
		  infotablecell.setBorderWidthTop(0);
		  
		  
		  
		Phrase fromaddress=new Phrase("Name and address of consigner / exporter :",font9);
		PdfPCell fromaddcell=new PdfPCell();
		fromaddcell.addElement(fromaddress);
		fromaddcell.setBorder(0);
		  
		  
		 String fromcompany;
		 if(fumigation.getFromCompanyname()!=null){
			 fromcompany=fumigation.getFromCompanyname();
		 }else{
			 fromcompany=" ";
		 }
		 
		  Phrase fromcompanypharse=new Phrase(fromcompany,font9bold);
		  PdfPCell fromcompcell=new PdfPCell();
		  fromcompcell.addElement(fromcompanypharse);
		  fromcompcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  fromcompcell.setBorder(0);
		  
		  
		  String addresslin1=" ";
		  if(fumigation.getFromaddress()!=null&&!fumigation.getFromaddress().getAddrLine1().equals("")){
			  addresslin1=fumigation.getFromaddress().getAddrLine1().trim()+" , ";
		  }
		  
		  String addressline2=" ";
		  
		  if(fumigation.getFromaddress()!=null&&!fumigation.getFromaddress().getAddrLine2().equals("")){
			  addressline2 =fumigation.getFromaddress().getAddrLine2().trim()+" , ";
		  }
		 
		  String landmark=" ";
		  if(fumigation.getFromaddress()!=null&&!fumigation.getFromaddress().getLandmark().equals("")){
			  landmark=fumigation.getFromaddress().getLandmark()+" , ";
		  }
		  
		  String locality=" ";
		  if(fumigation.getFromaddress()!=null&&!fumigation.getFromaddress().getLocality().equals("")){
			  locality=fumigation.getFromaddress().getLocality()+" , ";
		  }
		  
//		  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality,font9);
//		  PdfPCell fromaddrescell1=new PdfPCell();
//		  fromaddrescell1.addElement(addressline1);
//		  fromaddrescell1.setBorder(0);
		  
		  
		  String city=" ";
		  if(fumigation.getFromaddress()!=null&&!fumigation.getFromaddress().getCity().equals("")){
			  city=fumigation.getFromaddress().getCity()+",";
		  }
		  String state=" ";
		  if(fumigation.getFromaddress()!=null&&!fumigation.getFromaddress().getState().equals("")){
			  state=fumigation.getFromaddress().getState()+"-";
		  }
		  String pin=" ";
		  if(fumigation.getFromaddress()!=null&&fumigation.getFromaddress().getPin()!=0){
			  pin=fumigation.getFromaddress().getPin()+"";
		  }
		  String country=" ";
		  if(fumigation.getFromaddress()!=null&&fumigation.getFromaddress().getCountry()!=null){
			  country=fumigation.getFromaddress().getCountry();
		  }
		  
		  
		  
		  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality+city+state+pin+country,font9);
		  PdfPCell fromaddrescell1=new PdfPCell();
		  fromaddrescell1.addElement(addressline1);
		  fromaddrescell1.setBorder(0);
		  
//		  Phrase blank=new Phrase(" ",font9);
//		  PdfPCell blankCell=new PdfPCell();
//		  fromaddrescell1.addElement(addressline1);
//		  fromaddrescell1.setBorder(0);
		  
		  
		  PdfPTable addtable = new PdfPTable(1);
		  addtable.setWidthPercentage(100f);
		  addtable.addCell(fromaddrescell1);
		  
		  
//		  Phrase addressline3=new Phrase(city+" , "+state+" - "+pin+" , "+country,font9);
//		  PdfPCell fromaddrescell2=new PdfPCell();
//		  fromaddrescell2.addElement(addressline3);
//		  fromaddrescell2.setBorder(0);
		  
		  
		  
		  	Phrase toaddress=new Phrase("Declared Name and address Of consignee :",font9);
			PdfPCell toaddcell=new PdfPCell();
			toaddcell.addElement(toaddress);
			toaddcell.setBorder(0);
		  
		  
			
			 String tocompany;
			  
			 if(fumigation.getFromCompanyname()!=null){
				 
				 tocompany=fumigation.getToCompanyname();
			 }else{
				 tocompany=" ";
			 }
			 
			  Phrase tocompanypharse=new Phrase(tocompany,font9bold);
			  PdfPCell tocompcell=new PdfPCell();
			  tocompcell.addElement(tocompanypharse);
			  tocompcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			  tocompcell.setBorder(0);
			  
			  
			  String toaddresslin1=" ";
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getAddrLine2().equals("")){
				  toaddresslin1=fumigation.getTomaddress().getAddrLine1().trim()+" , ";
			  }
			  
			  String toaddressline2=" ";
			  
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getAddrLine2().equals("")){
				  toaddressline2 =fumigation.getTomaddress().getAddrLine2().trim()+" , ";
			  }
			 
			  String tolandmark=" ";
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getLandmark().equals("")){
				  tolandmark=fumigation.getTomaddress().getLandmark()+" , ";
			  }
			  String tolocality=" ";
			  
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getLocality().equals("")){
				  tolocality=fumigation.getTomaddress().getLocality()+" , ";
			  }
			  
			  
			  
//			  Phrase toaddressline1=new Phrase(toaddresslin1+toaddressline2+tolandmark+tolocality,font10);
//			  PdfPCell toaddrescell1=new PdfPCell();
//			  toaddrescell1.addElement(toaddressline1);
//			  toaddrescell1.setBorder(0);
			  
			  
			  String tocity=" ";
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getCity().equals("")){
				  tocity=fumigation.getTomaddress().getCity()+",";
			  }
			  String tostate=" ";
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getState().equals("")){
				  tostate=fumigation.getTomaddress().getState()+"-";
			  }
			  String topin=" ";
			  if(fumigation.getTomaddress()!=null&&fumigation.getTomaddress().getPin()!=0){
				  topin=fumigation.getTomaddress().getPin()+",";
			  }
			  String tocountry=" ";
			  if(fumigation.getTomaddress()!=null&&!fumigation.getTomaddress().getCountry().equals("")){
				  tocountry=fumigation.getTomaddress().getCountry();
			  }
			  
			  
			  
			  Phrase toaddressline1=new Phrase(toaddresslin1+toaddressline2+tolandmark+tolocality+tocity+tostate+topin+tocountry,font9);
			  PdfPCell toaddrescell1=new PdfPCell();
			  toaddrescell1.addElement(toaddressline1);
			  toaddrescell1.setBorder(0);
			  
//			  Phrase toaddressline3=new Phrase(tocity+" , "+tostate+" - "+topin+" , "+tocountry,font10);
//			  PdfPCell toaddrescell2=new PdfPCell();
//			  toaddrescell2.addElement(toaddressline3);
//			  toaddrescell2.setBorder(0);
			
			
			PdfPTable fromaddresstable=new PdfPTable(1);
			fromaddresstable.setWidthPercentage(100f);
			fromaddresstable.addCell(fromcompcell);
//			fromaddresstable.addCell(fromaddrescell1);
//			fromaddresstable.addCell(fromaddrescell2);
			
			
			
			PdfPTable toaddresstable=new PdfPTable(1);
			toaddresstable.setWidthPercentage(100f);
			toaddresstable.addCell(tocompcell);
//			toaddresstable.addCell(toaddrescell1);
//			toaddresstable.addCell(toaddrescell2);
			
			
			PdfPCell fromaddresscell=new PdfPCell(fromaddresstable);
			fromaddresscell.setBorder(0);
			
			PdfPCell toaddresscell=new PdfPCell(toaddresstable);
			toaddresscell.setBorder(0);
			
			PdfPTable addresstable=new PdfPTable(2);
			addresstable.setWidthPercentage(100f);
			
			try {
				addresstable.setWidths(columnWidths1);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			addresstable.addCell(fromaddcell);
			addresstable.addCell(fromaddresscell);
//			addresstable.addCell(toaddcell);
//			addresstable.addCell(toaddresscell);
			
//			
//			PdfPCell addresscell=new PdfPCell();
//			addresscell.addElement(addresstable);
//			addresscell.setBorder(0);
			
			
			
			
			 PdfPTable toName = new PdfPTable(2);
			 toName.setWidthPercentage(100);
			 try {
				 toName.setWidths(columnWidths1);
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
			 toName.addCell(toaddcell);
			 toName.addCell(toaddresscell);
			 

			 
			 PdfPTable toaddTable = new PdfPTable(1);
			  
			 toaddTable.setWidthPercentage(100);
			 toaddTable.addCell(toaddrescell1);
			 
			
			
			infotablecell.addElement(addresstable);
			infotablecell.addElement(addtable);
			infotablecell.addElement(toName);
			infotablecell.addElement(toaddTable);
			
			
//			infotablecell.setBorderWidthLeft(0);
//			infotablecell.setBorderWidthRight(0);
			
			
			
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell();
		titlePdfCell.addElement(titlepdftable);
		titlePdfCell.setBorderWidthBottom(0);
		titlePdfCell.setBorderWidthTop(0);
		
//		titlePdfCell.setBorderWidthLeft(0);
//		titlePdfCell.setBorderWidthRight(0);
		
		parent.addCell(titlePdfCell);
		parent.addCell(infotablecell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}






	private void CreateHeading() {
		
//		String	titlepdf="CERTIFICATE OF FUMIGATION";
		String	titlepdf="";
		if(fumigationTitleFlag==true){
			titlepdf=" ";
		}
		else{
			titlepdf="FUMIGATION CERTIFICATE";
		}
		
		
		Phrase titlephrase=new Phrase(titlepdf,font10boldul);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell(titlepdfpara);
		titlecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlecell.setBorder(0);
		
//		titlecell.setBorderWidthBottom(0);
//		titlecell.setBorderWidthLeft(0);
//		titlecell.setBorderWidthRight(0);
		
		
//		Phrase blankphrase=new Phrase("",font8);
//		PdfPCell blankCell=new PdfPCell();
//		blankCell.addElement(blankphrase);
//		blankCell.setBorder(0);
		
//		Phrase cout=new Phrase("ID : "+fumigation.getCount()+"",font10);
//		PdfPCell countCell=new PdfPCell(cout);
//		countCell.addElement(cout);
//		countCell.setBorder(0);
//		countCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		System.out.println("Creation date === "+fumigation.getCreationDate());
		
//		Phrase date=new Phrase("Date : "+fmt.format(fumigation.getCreationDate()),font10);
//		PdfPCell dateCell=new PdfPCell(date);
//		dateCell.addElement(date);
//		dateCell.setBorder(0);
//		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		PdfPTable titlepdftable=new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
//		titlepdftable.addCell(countCell);
		titlepdftable.addCell(titlecell);
//		titlepdftable.addCell(dateCell);
		
	
		
		//****************rohan remove this from following code as per new changes 
		//***"-"+fumigation.getCount()
		
//		Phrase treatnophrase=new Phrase("Registration Number :- "+fumigation.getTreatmentcardno()+"",font9);
//		PdfPCell treatcell=new PdfPCell(treatnophrase);
//		treatcell.setBorder(0);
//		Phrase dateissuephrase=new Phrase("Date of Issue :- "+fmt.format(fumigation.getDateofissue()),font9);
//		PdfPCell dateissuecell=new PdfPCell(dateissuephrase);
//		dateissuecell.setBorder(0);
//		dateissuecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		
		 logger.log(Level.SEVERE,"Hiiiiiiiiiiiiiiiiiiiiiiii non aus date of issued "+fmt.format(fumigation.getDateofissue()));
		Phrase treatnophrase=new Phrase("TREATMENT CERTIFICATE NO.: "+fumigation.getCertificateNo()+"",font10);
		PdfPCell treatcell=new PdfPCell(treatnophrase);
		treatcell.setBorder(0);
		Phrase dateissuephrase=new Phrase("DATE OF ISSUE: "+fmt.format(fumigation.getDateofissue()),font10);
		PdfPCell dateissuecell=new PdfPCell(dateissuephrase);
		dateissuecell.setBorder(0);
		dateissuecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		
		Phrase typename=null;
 		Phrase typevalue=null;
 		
// 		PdfPTable artictable=new PdfPTable(2);
// 		artictable.setWidthPercentage(100);
// 		try {
//			artictable.setWidths(columnWidths2);
//		} catch (DocumentException e2) {
//			// TODO Auto-generated catch block
//			e2.printStackTrace();
//		}
 		PdfPCell tymanecell=new PdfPCell();
 		tymanecell.setBorder(0);
 		Phrase nnn=new Phrase("");
 		PdfPCell blankcekk=new PdfPCell(nnn);
 		blankcekk.setBorder(0);
// 		System.out.println("b4   for   ===== articletype=="+this.articletype.size());
//		for(int i=0;i<this.articletype.size();i++){
//				 
//			System.out.println("in    for   ===== articletype=="+this.articletype.size());
//					
//					
//					if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().trim().equalsIgnoreCase("Fumigation")){
//						
//						
//						System.out.println("cust docu name if in == "+articletype.get(i).getDocumentName());
//						
//						 typename = new Phrase(articletype.get(i).getArticleTypeName()+"                :  "+articletype.get(i).getArticleTypeValue(),font9);
////						 typevalue = new Phrase(articletype.get(i).getArticleTypeValue(),font8);
//						 
//						 
//							tymanecell.addElement(typename);
//							tymanecell.setBorder(0);
//							
////							PdfPCell typevalcell=new PdfPCell();
////							typevalcell.addElement(typevalue);
////							typevalcell.setBorder(0);
//							
////							artictable.addCell(tymanecell);
////							artictable.addCell(typevalcell);
//							
////							artictable.addCell(blankcekk);
////							artictable.addCell(blankcekk);
//					 }else{
//						 tymanecell.addElement(nnn);
//							tymanecell.setBorder(0);
//					 }
//					
//		}
		
//		PdfPCell articcell=new PdfPCell(artictable);
// 		articcell.setBorder(0);
// 		
// 		custtable.addCell(articcell);
		
		
		
		
		
		
		PdfPTable treattable=new PdfPTable(2);
		treattable.setWidthPercentage(100);
		
		treattable.addCell(treatcell);
		treattable.addCell(dateissuecell);
		treattable.addCell(tymanecell);
		treattable.addCell(blankcekk);
		
		String impcountry="This is to certify that following regulated articles have been fumigated according to the appropriate";
		String impcountry1="procedures to conform to the current phytosanitory requirements of the importing country : ";
		
		
		Phrase impcountryphrase=new Phrase(impcountry,font9);
//		Phrase impcountry1phrase=new Phrase(impcountry1+fumigation.getImportcountry(),font9);
		Phrase impcountry1phrase=new Phrase(impcountry1,font9);
		
		PdfPCell impcell=new PdfPCell(impcountryphrase);
		impcell.setBorder(0);
		
		PdfPCell impcell1=new PdfPCell(impcountry1phrase);
		impcell1.setBorder(0);
		
		PdfPCell titlePdfCell1=new PdfPCell(treattable);
		
		titlePdfCell1.setBorder(0);
		
		
		PdfPTable imptable=new PdfPTable(1);
		imptable.setWidthPercentage(100f);
//		imptable.setSpacingAfter(5f);
		imptable.addCell(titlePdfCell1);
		imptable.addCell(impcell);
		imptable.addCell(impcell1);
		

		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		    
		    
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell();
		titlePdfCell.addElement(titlepdftable);
		titlePdfCell.setBorderWidthBottom(0);
		titlePdfCell.setBorderWidthTop(0);
		
		titlePdfCell.setBorderWidthLeft(0);
		titlePdfCell.setBorderWidthRight(0);
		
		
		PdfPCell titlePdfCell2=new PdfPCell(imptable);
		
//		titlePdfCell2.setBorderWidthTop(0);
//		titlePdfCell2.setBorderWidthBottom(0);
//		
//		titlePdfCell2.setBorderWidthLeft(0);
//		titlePdfCell2.setBorderWidthRight(0);
		
		
		parent.addCell(titlePdfCell);
		parent.addCell(titlePdfCell2);
				
		
		try {
			
//			if(upcflag==true){
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				}
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void myMethod(String result) {
		
		
		String myString= "";
		
		if(result!=null)
		{
			System.out.println("asdadadadadasdasd"+result.length());
			
			String[] dataRetrieved=result.split(" ");
			System.out.println("S1"+dataRetrieved[0]);
			System.out.println("S2"+dataRetrieved[1]);
			System.out.println("S3"+dataRetrieved[2]);
			
			System.out.println("array length ===="+dataRetrieved.length);
			if(dataRetrieved[0].trim().length()<=10){
				String firstWord = dataRetrieved[0].trim();
				System.out.println("111111111111"+firstWord);
			
			int remaining = 10 - dataRetrieved[0].trim().length();
			System.out.println("2222222222222"+remaining);
			
			
			if(dataRetrieved[1].trim().length()<=remaining){
				String second = dataRetrieved[1].trim();
				
				System.out.println("333333333333333"+second);
			
			
			int remaing2 = remaining - dataRetrieved[1].trim().length();
			if(dataRetrieved[2].trim().length()<=remaing2){
				
				String third = dataRetrieved[2].trim();
				
				System.out.println("aaaaaaaaaaaaaaa"+third);
			}
			else{
				for(int i=2;i<dataRetrieved.length;i++){
				myString=myString+dataRetrieved[i]+" ";
				}
				System.out.println("xxxxxxxxxxxxxxxxxxx"+myString);
				
			}
		}
			else
			{
				for(int i=1;i<dataRetrieved.length;i++){
					myString=myString+dataRetrieved[i]+" ";
					}
					System.out.println("xxxxxxxxxxxxxxxxxxx"+myString);
			}
			
			
			
			
		}
			
			
			
		}
	}
	
}
