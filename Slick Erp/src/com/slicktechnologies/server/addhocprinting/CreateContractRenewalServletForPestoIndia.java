package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServlet;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
public class CreateContractRenewalServletForPestoIndia extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9045859665268163757L;

	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid1 = req.getParameter("ContractId");
			stringid1 = stringid1.trim();
			int count = Integer.parseInt(stringid1);
			
			String stringid = req.getParameter("CompanyId");
			stringid = stringid.trim();
			Long companyId = Long.parseLong(stringid);
			
			ContractRenewal wo = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", count).first().now();
			
//			ContractRenewalPdf wopdf = new ContractRenewalPdf();
			PestoIndiaRenewalPdf conpdf=new PestoIndiaRenewalPdf();
			conpdf.document = new Document();
			Document document = conpdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			 document.open();
			
			conpdf.setContractRewnewal(wo);
			conpdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
