package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.util.logging.Level;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.googlecode.objectify.ObjectifyService;
import com.itextpdf.text.Document;


public class CreateOrionInvoicePdfServlet extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	  
	  public CreateOrionInvoicePdfServlet() {}
	  
	  protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException
	  {
	    response.setContentType("application/pdf");
	  		
	    try
	    {
	      String stringid = request.getParameter("Id");
	      stringid = stringid.trim();
	      Long count = Long.valueOf(Long.parseLong(stringid));
	      Invoice invoice = ((Invoice)ObjectifyService.ofy().load().type(Invoice.class).id(count.longValue()).now());
	      String invoiceRef="";
		  if(invoice.getRefNumber()!=null && !invoice.getRefNumber().equals("")){
		    	  invoiceRef = invoice.getRefNumber();
		  }else {
			  invoiceRef =invoice.getCount()+"";
		  }
		  
	      String filename="Invoice - "+invoiceRef+" - "+invoice.getPersonInfo().getFullName()+".pdf";
	      if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", invoice.getCompanyId()))
			      response.setHeader("Content-Disposition", "attachment; filename=" + filename);
	  		
	      
	      OrionServiceInvoicePdf pdf = new OrionServiceInvoicePdf();
	      pdf.document = new Document(PageSize.A4, 20, 20, 4, 5);
	      
	      
	      Document document = pdf.document;
	      PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
	      String preprintStatus = request.getParameter("preprint");
	      
	      //Ashwini Patil Date:7-07-2023
	      if (invoice!=null&&invoice.getStatus().equals(Contract.CANCELLED)) {

				writer.setPageEvent(new PdfCancelWatermark());
	      }else	if(invoice!=null&&!invoice.getStatus().equals("Approved")
					&& !invoice.getStatus().equals("Expired")) {
				writer.setPageEvent(new PdfWatermark());
		  }
	      
	      
	      document.open();
	      pdf.setInvoice(count);
	      
	      if(preprintStatus.contains("yes")){
			   
	    	  pdf.createPdf(preprintStatus);
		   }
		   else  if(preprintStatus.contains("no")){
			   
			   pdf.createPdf(preprintStatus);
		   }
		   else{
			   pdf.createPdf(preprintStatus);
		   }
	      
//	      pdf.createPdf(preprintStatus);
	      document.close();
	    } catch (DocumentException e) {
	      e.printStackTrace();
	    }
	  }

	
	



}
