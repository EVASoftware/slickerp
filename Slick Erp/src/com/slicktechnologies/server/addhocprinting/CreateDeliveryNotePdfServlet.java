package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.purchaseOrderPdf.DeliveryNotePdfUpdated;
import com.slicktechnologies.server.addhocprinting.sasha.DeliveryNoteForSasha;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class CreateDeliveryNotePdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6461094466569832561L;
	
	Logger logger = Logger.getLogger("CreateDeliveryNotePdfServlet.class");
	
	
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
//		  DeliveryNote delnoteEntity;
		  String stringid=request.getParameter("Id");
		   stringid=stringid.trim();
		   Long count =Long.parseLong(stringid);
	
	   /**
		 * Date 15-01-2018 By Vijay
		 * here i have added delivery note PDF standard for all in if condition 
		 *
		 */
		  String pdfType = request.getParameter("type"); 
		  String pdfsubtype = request.getParameter("subtype");
		 
		 if(pdfType.equals("DeliveryNote")){
			 if(pdfsubtype !=null &&pdfsubtype.equals("DELIVERYNOTEV1")){

				  DeliveryNoteForSasha dnpdfsasha = new DeliveryNoteForSasha();
				  dnpdfsasha.document = new Document(PageSize.A4,20,20,10,10);
					Document document = dnpdfsasha.document;
					PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
					document.open();
					dnpdfsasha.setDeliveryNote(count);
					dnpdfsasha.createPdf();
					document.close();
					
					
				
				  
			  }else if(pdfsubtype !=null &&pdfsubtype.equals("DeliveryNoteUpdated")){

					  DeliveryNotePdfUpdated dnpdfsasha = new DeliveryNotePdfUpdated();
					  dnpdfsasha.document = new Document(PageSize.A4,20,20,10,10);
						Document document = dnpdfsasha.document;
						PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
						document.open();
						dnpdfsasha.setDeliveryNote(count);
						dnpdfsasha.createPdf();
						document.close();
						
				  }//DeliveryNoteUpdated
			 else{
//				 logger.log(Level.SEVERE," inside else create deliver notee ");
//			   DeliveryNote delnoteEntity=ofy().load().type(DeliveryNote.class).id(count).now();
//				   
//			   DeliveryNotePdf delnotepdf=new DeliveryNotePdf();
//			   delnotepdf.document=new Document();
//			   Document document = delnotepdf.document;
//			   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
//			   
//			   if(delnoteEntity.getStatus().equals("Cancelled")){
//					writer.setPageEvent(new PdfCancelWatermark());
//					
//				}else
//			   
//			   if(!delnoteEntity.getStatus().equals("Approved")&&!delnoteEntity.getStatus().equals("Completed")){	
//					 writer.setPageEvent(new PdfWatermark());
//				}
//					
//			   
//			   
//			   document.open();
//			   
//			   delnotepdf.setDeliveryNote(count);
//			   
//			   String preprintStatus=request.getParameter("preprint");
//			   System.out.println("****************"+preprintStatus);
//			   if(preprintStatus.contains("yes")){
//				   
//				   delnotepdf.createPdf(preprintStatus);
//			   }
//			   
//			   if(preprintStatus.contains("no")){
//				   
//				   delnotepdf.createPdf(preprintStatus);
//			   }
//			   
//			   if(preprintStatus.contains("plane")){
//				   
//				   delnotepdf.createPdf(preprintStatus);
//			   }
//			   
//		//	   delnotepdf.createPdf();
//			   document.close();
				 /**
				  * @author Ashwini Patil
				  * @since 15-04-2022
				  * Standard format was not priniting any amounts.
				  * So as per discussion with Nitin sir old standard format has to be replaced with Sasha format.
				  * set Sasha pdf as standard and print 3 copies
				  *	add Sales Oder Id after Invoice Id
				  *	add Transport Mode, Vehicle Number,Date of Supply,Place Of Supply in terms and conditions box
 				  *	If values are not there dont print headings Remarks, Head Office, Company's PAN
				  * 
				  */
				 DeliveryNoteForSasha dnpdfsasha = new DeliveryNoteForSasha();
				 dnpdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				 Document document = dnpdfsasha.document;
				 PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write
				 document.open();
				 dnpdfsasha.setDeliveryNote(count);
				 dnpdfsasha.createPdf();
				 document.close();
				    
	   
		 }
		 }
		 /**
		  * Date 15-01-2018 By vijay
		  * Client :- Versatile 
		  * else if condition for delivery Note pdf based on workOrder 
		  */
		 else if(pdfType.equals("workOrderDeliveryNote")){
			 WorkOrder workorder = ofy().load().type(WorkOrder.class).id(count).now();
			 WorkOrderDeliveryNotePdf workOrderNotepdf = new WorkOrderDeliveryNotePdf();
			 workOrderNotepdf.document = new Document();
			 Document document = workOrderNotepdf.document;
			 PdfWriter pdfwriter = PdfWriter.getInstance(document, response.getOutputStream());
		   if(workorder.getStatus().equals("Cancelled")){
			   pdfwriter.setPageEvent(new PdfCancelWatermark());
			}
		   else if(!workorder.getStatus().equals("Approved")&&!workorder.getStatus().equals("Completed")){	
			   pdfwriter.setPageEvent(new PdfWatermark());
			}
		   document.open();
		   workOrderNotepdf.setWorkOrderDeliveryNote(count);
		   String preprintStatus=request.getParameter("preprint");
		   workOrderNotepdf.createPdf(preprintStatus);
		   document.close();
		   
		 }
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }
	
	
	

}
