package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;

public class CreateServicePoPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8987965744500553447L;

	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			ServicePo po = ofy().load().type(ServicePo.class).id(count).now();
			
			ServicePoPdf popdf = new ServicePoPdf();
			popdf.document = new Document();
			Document document = popdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); 
			if(po.getStatus().equals("Cancelled")){
				writer.setPageEvent(new PdfCancelWatermark());
			}else if(!po.getStatus().equals("Approved")){	
				 writer.setPageEvent(new PdfWatermark());
			}
			document.open();
			popdf.setPurchaseOrder(count);
			popdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
