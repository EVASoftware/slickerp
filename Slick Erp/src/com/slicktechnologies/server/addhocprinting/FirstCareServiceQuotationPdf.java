package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class FirstCareServiceQuotationPdf {  

    /**
     *  Customization quotation PDF for first Care.
     */
	public Document document;
	
	Company comp;
	Customer cust;
	Contract con; //Added by Ashwini
	Quotation quot;
	Lead serviceLead;
	Branch branchDt = null;
	CompanyPayment comppayment;
	boolean consolidatePrice = false;
	boolean upcflag=false;
	boolean quotationVersionOne=false;
	Phrase chunk;
	PdfPCell pdfpremise, pdfservices, pdfduration, pdffrequency, pdfprice,pdfblank,pdfttl,pdfttlval;
	PdfPCell pdfdays, pdfpercent, pdfcomment;
	PdfPCell  pdftaxname,pdftaxpercent, pdfassval, pdfamt;
	ProcessConfiguration processConfig;
	float[] colwidth = { 2.0f, 0.2f, 5.0f, 1.0f, 0.2f, 1.5f };
	float[] col1width = { 1.5f, 0.2f, 3.0f,3.0f, 1.0f, 0.2f, 3.0f};   
	float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f }; 
	
	float[] columnWidths7 = { 2.0f, 3.2f, 1.6f, 1.6f, 1.6f};
	
	Logger logger = Logger.getLogger("NameOfYourLogger");

	private Font font16boldul, font12bold, font8bold, font8, font9bold,font11,
			font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");
	
	private PdfPCell custlandcell;
	
	FirstCareServiceQuotationPdf(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font11 = new Font(Font.FontFamily.HELVETICA, 11);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}
	
	public void setfcservicequotation(Long count,String preprint) {
		
		quot = ofy().load().type(Quotation.class).id(count).now();
		
		 /*
		    * Date:14/09/2018
		    * Developer:Ashwini
		    * Des:to also add updated service Address
		    */
				
				
				if (quot.getCompanyId()!=null){
					con = ofy().load().type(Contract.class)
							.filter("count", quot.getCompanyId())
							.filter("companyId", quot.getCompanyId()).first().now();
				}
				/*
				 * End by Ashwini
				 */
				
		
		if (quot.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId())
					.filter("companyId", quot.getCompanyId()).first().now();
		
		if (quot.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId()).first().now();

		if (quot.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", quot.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (quot.getCompanyId() != null)
			quot = ofy().load().type(Quotation.class)
					.filter("companyId", quot.getCompanyId())
					.filter("count", quot.getCount()).first().now();
		else
			quot = ofy().load().type(Quotation.class)
					.filter("count", quot.getCount()).first().now();
		
		 logger.log(Level.SEVERE,"before lead load comp id is"+quot.getCompanyId());
		if (quot.getCompanyId() != null){
			 logger.log(Level.SEVERE,"inside if lead load comp id is"+quot.getCompanyId());			
			serviceLead = ofy().load().type(Lead.class)
					.filter("companyId", quot.getCompanyId())
					.filter("count", quot.getLeadCount()).first().now();
		}
		else{
			 logger.log(Level.SEVERE,"inside else lead load comp id is"+quot.getCompanyId());
			serviceLead = ofy().load().type(Lead.class)
					.filter("count", quot.getLeadCount()).first().now();
		}
		
		
		if(quot.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", quot.getCompanyId()).filter("processName", "Quotation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag=true;
						
					}
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("QuotationVersionOne")&& processConfig.getProcessList().get(k).isStatus() == true) {
						quotationVersionOne = true;
					}
				}
			}
		}
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(quot !=null && quot.getBranch() != null && quot.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",quot.getCompanyId()).filter("buisnessUnitName", quot.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", quot.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
	}
	
	
	public void createPdf(String preprint) {

		if (upcflag == false && preprint.equals("plane")) {
			//createBlankHeading();
			createLogo(document, comp);
			createCompanyHeadding();
		} else {

			if (preprint.equals("yes")) {

				System.out.println("inside prit yes");
				createBlankforUPC();
			}

			if (preprint.equals("no")) {
				System.out.println("inside prit no");

				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}

				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				createBlankforUPC();
			}

		}
		createReferenceandDateHeadding();

		System.out.println("refrence no and date has been printed");

		createCustomerDetailsHeading();
		createSubjectandMessageHeadding();
		// createQuotationHeadding();
		createProductInfoHeadding();

		// createProductTaxHeadding();
		// System.out.println("Product info has been created");

		//createTermsandPaymentHeadding();

		// craeteTandPHeadding();

		createValidityofquotationHeadding();

	}
	
	
	
	private void createBlankforUPC() {
		
		/*
		 * Commented by Ashwini
		 */
		
//			 Paragraph blank =new Paragraph();
//			    blank.add(Chunk.NEWLINE);
//			
//			
//			try {
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
		
		/*
		 *Date:30/7/2018
		 * Developer:Ashwini
		 * Des:To increase header space
	  */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" ,quot.getCompanyId())){
			 Paragraph blank =new Paragraph();
			 blank.add(Chunk.NEWLINE);
			
			
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}else{
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * End by Ashwini
		 */
		}


	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	public  void createBlankHeading() {
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createLogo(Document doc, Company comp) {
		
		//********************logo for server ********************
		
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		
	public void createCompanyHeadding() {
		
		
	
		
		String companyname ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname=" "
					+  branchDt.getCorrespondenceName();
		}else{
		 companyname = " "
				+ comp.getBusinessUnitName().trim().toUpperCase();
		}

		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font14bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);
		

		
		String custAdd1="";
		String custFullAdd1="";
		
		if(comp.getAddress()!=null){
			
			if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
			
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
				}
			}else{
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
					custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1();
				}
			}
			
			if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getPin()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}else{
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getPin()+","+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}
			
		}	
		
			Phrase addressline=new Phrase(custFullAdd1,font11);
	
			Paragraph addresspara=new Paragraph();
			addresspara.add(addressline);
			addresspara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell addresscell=new PdfPCell();
			addresscell.addElement(addresspara);
			addresscell.setBorder(0);
		   
		
		
		String contactinfo="";
		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			contactinfo =  "Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim();
		}
		//Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim(),font10);
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setFont(font11);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(addresscell);
//		table.addCell(localitycell);
		table.addCell(contactcell);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(10f);
		
		PdfPCell cell = new PdfPCell();
		cell.addElement(table);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);
		
	
	
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
		
		
//	*************************************************************************************************************	
		
	
	public void createReferenceandDateHeadding() {
		
		
//		For static dynamic entity: *************************************
	
		Phrase ref = new Phrase("Reference No",font12bold);
		Paragraph refpara = new Paragraph();
		refpara.add(ref);
		refpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell refcell = new PdfPCell(refpara);
//		refcell.addElement(refpara);
		refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase refno=null;
		if(quot.getReferenceNumber() !=null && !quot.getReferenceNumber().equals("")){
			refno = new Phrase(quot.getReferenceNumber());
		}
		else{
//			refno = new Phrase(" ");
			/** Date 10-01-2018 above one line old code commneted and below new code added by vijay 
			 * for reference no nothing then quotation id will print
			 **/
			refno = new Phrase(quot.getCount()+"",font12bold);
		}
		
		PdfPCell refnocell  = new PdfPCell(refno);
		//refnocell.addElement(refno);
		refnocell.setBorder(0);
		refnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date",font12bold);
		Paragraph datepara = new Paragraph();
		datepara.add(date);
		datepara.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell datecell = new PdfPCell(datepara);
//		datecell.addElement(datepara);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase dt = null;
		
		if(quot.getReferenceDate() !=null){
			dt = new Phrase(fmt.format(quot.getReferenceDate()));
		}
		else{
//			dt = new Phrase(" ");
			/** Date 10-01-2018 above one line old code commneted and below new code added by vijay 
			 * if reference Date is nothing then quotation Date will print
			 */
			dt = new Phrase(fmt.format(quot.getQuotationDate()),font12bold);
		}
//		if(fmt.format(quot.getReferenceDate()) !=null){
//			dt = new Phrase("" + fmt.format(quot.getReferenceDate()));	
//			
//		}
		
		PdfPCell dtcell = new PdfPCell(dt);
//		dtcell.addElement(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
//		*****************************************************************************
		Phrase col = new Phrase(":",font12bold);
		Paragraph colpara = new Paragraph();
		colpara.add(col);
		colpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell colcell = new PdfPCell(colpara);
		//colcell.addElement();
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	    ******************************************************************************	
		
		PdfPTable table1 = new PdfPTable(6);
		
		try {
			table1.setWidths(colwidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		table1.addCell(refcell);
		table1.addCell(colcell);
		table1.addCell(refnocell);
		
		table1.addCell(datecell);
		table1.addCell(colcell);
		table1.addCell(dtcell);
		
		
//		table1.addCell(refcell);
//		table1.addCell(colcell);
//		table1.addCell(refnocell);
		
		table1.setWidthPercentage(100f);
		
		PdfPCell cell1 = new PdfPCell();
		cell1.addElement(table1);
		cell1.setBorder(0);
	
		PdfPTable parent1 = new PdfPTable(1);
		
		parent1.addCell(cell1);
		parent1.setWidthPercentage(100);
		
		
		try {
			document.add(parent1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createCustomerDetailsHeading(){
		
//		For static and dynamic entity1 ***************************************************** 
		
		Phrase to = new Phrase("To,", font11);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setBorder(0);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		
		
		String customerInfo="";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			customerInfo = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				customerInfo = "M/S " + cust.getCompanyName().trim();
			} else {
				customerInfo = cust.getFullname().trim();
			}
		}

		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
				if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
				}
			}else{
				if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("") ){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(cust.getAdress().getLocality()!=null&& !cust.getAdress().getLocality().equals("")){
				if(cust.getAdress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getPin()+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getCity()+"\n"+cust.getAdress().getState()+","+cust.getAdress().getCountry();
				}
			}else{
				if(cust.getAdress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getPin()+","+cust.getAdress().getCity()+", "+cust.getAdress().getState()+", "+cust.getAdress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+"\n"+cust.getAdress().getState()+","+cust.getAdress().getCountry();
				}
			}
			

		}

		
		Phrase custAddInfo = new Phrase(customerInfo+"\n"+custFullAdd1, font10);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		PdfPCell pocCell=null;
		if(cust.getCompanyName()!=null && cust.isCompany()==true){
			
			 Phrase poc = new Phrase("POC : "+cust.getFullname(),font10);
			 pocCell = new PdfPCell(poc);
			 pocCell.setBorder(0);
		}
		
		PdfPTable table2 = new PdfPTable(1);
		table2.addCell(toCell);

		table2.addCell(custAddInfoCell);
		
		if(cust.getCompanyName()!=null && cust.isCompany()==true){
			table2.addCell(pocCell);
		}
		
		table2.setWidthPercentage(100f);		
		
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(table2);
		customerCell.setBorder(0);
		customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable parent2 = new PdfPTable(1);
		parent2.addCell(customerCell);
		parent2.setWidthPercentage(100);
		
		try {
			document.add(parent2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSubjectandMessageHeadding(){
		
//		For static an dynamic entity2 ******************************************************************
		
		Phrase sub = new Phrase("Subject  : Quotation For Pest Management Services",font12bold);
		PdfPCell subcell = new PdfPCell();
		subcell.addElement(sub);
		subcell.setBorder(0);
		subcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blank=new Phrase();
		blank.add(" ");
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
		blankCell.setBorder(0);
		
		Phrase dear = new Phrase("Dear Sir,",font10);
		PdfPCell dearCell = new PdfPCell(dear);
		dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dearCell.setBorder(0);
		
	
		Phrase blanksub=new Phrase();
		blanksub.add(" ");
		PdfPCell blanksubCell = new PdfPCell();
		blanksubCell.addElement(blank);
		blanksubCell.setBorder(0);
		
		String title1 = "";
	
		 logger.log(Level.SEVERE,"service lead"+serviceLead);
		if(serviceLead!=null){
			 logger.log(Level.SEVERE,"inside servicelead!=null");
			 System.out.println("Lead"+serviceLead);
			if(serviceLead.getCreationDate()!=null){
				 logger.log(Level.SEVERE,"inside date is present");
			title1 = "Thank you for giving us opportunity to share the scope of services. FirstCare, the trusted Pest Control Company in India with presence in the multiple markets for over 9+ years. We aims to set new standards for customer service considering the 'Quality' as the Customer's first Fundamental right."+"\n"+"\n"+"FirstCare focuses exclusively on customers that require quality services, and we have built our client base and reputation on that premise. Firstcare's processes, outstanding client services, concerns for client's business value and commitment to client delight are the key pillars of our services.";
			}
			else
			{
				title1 = "Thank you for giving us opportunity to share the scope of services. FirstCare, the trusted Pest Control Company in India with presence in the multiple markets for over 9+ years. We aims to set new standards for customer service considering the 'Quality' as the Customer's first Fundamental right."+"\n"+"\n"+"FirstCare focuses exclusively on customers that require quality services, and we have built our client base and reputation on that premise. Firstcare's processes, outstanding client services, concerns for client's business value and commitment to client delight are the key pillars of our services.";			}
		}
		else
		{
			 logger.log(Level.SEVERE,"inside service lead is null");
				title1 = "Thank you for giving us opportunity to share the scope of services. FirstCare, the trusted Pest Control Company in India with presence in the multiple markets for over 9+ years. We aims to set new standards for customer service considering the 'Quality' as the Customer's first Fundamental right."+"\n"+"\n"+"FirstCare focuses exclusively on customers that require quality services, and we have built our client base and reputation on that premise. Firstcare's processes, outstanding client services, concerns for client's business value and commitment to client delight are the key pillars of our services.";
		}
		
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setBorder(0);
		msgCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blankmsg=new Phrase();
		blankmsg.add(" ");
		PdfPCell blankmsgCell = new PdfPCell();
		blankmsgCell.addElement(blank);
		blankmsgCell.setBorder(0);
	
		
		
		PdfPTable table3 = new PdfPTable(1);
		table3.addCell(subcell);
		table3.addCell(blankCell);
		table3.addCell(dearCell);
		table3.addCell(blanksubCell);
		table3.addCell(msgCell);
		table3.addCell(blankmsgCell);
		
		table3.setWidthPercentage(100f);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.addElement(table3);
		cell2.setBorder(0);
		
		PdfPTable parent3 = new PdfPTable(1);
		parent3.addCell(cell2);
		parent3.setWidthPercentage(100);
		
		
		try {
			document.add(parent3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createValidityofquotationHeadding(){
		
		
		String paymentTerm="";
		if(quot.getPaymentTermsList()!=null&&quot.getPaymentTermsList().size()!=0){
			paymentTerm=quot.getPaymentTermsList().get(0).getPayTermComment();
			
		}
		//"Terms of Payment : 
		Phrase terms = new Phrase("Terms Of Payment : " +paymentTerm  ,font10bold);
		PdfPCell termscell = new PdfPCell(terms);
		termscell.setBorder(0);
		termscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blank=new Phrase();
		blank.add(" ");
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
		blankCell.setBorder(0);

	
		
		Phrase validity = new Phrase("Validity Of Quotation : "+"" + fmt.format(quot.getValidUntill()),font10bold);
		PdfPCell validitycell = new PdfPCell(validity);
		validitycell.setBorder(0);
		validitycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String title3= "";
//		title3 = "We hope you will find our quotation reasonable and await your acceptance of this quotation; should you require further information on the above, please call us on "  + (comp.getCellNumber1() + "\n" + "Thanks & regards,");
		title3 = "The above quotation includes a free inspection of premise to give you an idea about the current state of premises; improvement areas and half yearly audit report (Commercial Contracts)."+"\n";
				 
		Phrase str2 = new Phrase(title3,font10);
		PdfPCell str2cell = new PdfPCell(str2);
		str2cell.setBorder(0);
		str2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase blankfeel = new Phrase();
		blankfeel.add(Chunk.NEWLINE);
	    PdfPCell blank2cell = new PdfPCell(blankfeel);
	    blank2cell.setBorder(0);
		
		String title4 =" ";
		title4 = "Feel free to connect in case of any query. ";
		
		Phrase str3 = new Phrase(title4,font10);
		PdfPCell str3cell = new PdfPCell(str3);
		str3cell.setBorder(0);
		str3cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		Phrase blank1 = new Phrase();
	    blank1.add(Chunk.NEWLINE);
	    PdfPCell blank1cell = new PdfPCell(blank1);
	    blank1cell.setBorder(0);
	    
	    String title5 =" ";
	    title5 = "Thanks & regards";
	    
	    Phrase str4 = new Phrase(title5,font11);
		PdfPCell str4cell = new PdfPCell(str4);
		str4cell.setBorder(0);
		str4cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
//		Phrase pest = null;
//		if(comp.getBusinessUnitName() !=null){
//			
//			pest = new Phrase("" + comp.getBusinessUnitName(),font12bold);	
//			
//		}
//		//Team, Marketing & Customer Relations
		
		String companyname ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname=branchDt.getCorrespondenceName();
		}else{
		 companyname =comp.getBusinessUnitName().trim().toUpperCase();
		}

		Phrase companynamepara = new Phrase(companyname,font12bold);
		
//		Paragraph companynamepara = new Paragraph();
//		companynamepara.add(companyname);
//		companynamepara.setFont(font12bold);
//		companynamepara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		companynamecell.setBorder(0);
		
		Phrase tat =  new Phrase("Team, Marketing & Customer Relations", font11);
		PdfPCell tatcell= new PdfPCell(tat);
		tatcell.setBorder(0);
		tatcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blankval = new Phrase();
		blankval.add(Chunk.NEWLINE);
	    PdfPCell blankvalcell = new PdfPCell(blankval);
	    blankvalcell.setBorder(0);
		
		DocumentUpload digitalsign = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl + digitalsign.getUrl()));
			image2.scalePercent(13f);
			image2.scaleAbsoluteWidth(100f);
			

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		Phrase signatory =  new Phrase("AUTHORISED SIGNATORY",font11);
		PdfPCell signatorycell= new PdfPCell(signatory);
		signatorycell.setBorder(0);
		
		Phrase newLine =  new Phrase(Chunk.NEWLINE);
		PdfPCell newLineCell= new PdfPCell(newLine);
		newLineCell.setBorder(0);
		
		
		
//		PdfPCell pestcell = new PdfPCell(pest); 
//		pestcell.setBorder(0);
//		pestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable validitytable = new PdfPTable(1);
		validitytable.addCell(termscell);
		validitytable.addCell(blankCell);
		validitytable.addCell(validitycell);  
//		validitytable.addCell(blankvalcell);
		validitytable.addCell(str2cell);
		validitytable.addCell(blank2cell);
		validitytable.addCell(str3cell);
		validitytable.addCell(blank1cell);
		validitytable.addCell(str4cell);
//		validitytable.addCell(blank1cell);  
		validitytable.addCell(companynamecell);
		validitytable.addCell(tatcell);
		if (imageSignCell != null) {
			validitytable.addCell(imageSignCell);
		} else {
			validitytable.addCell(blankvalcell);
		}
		validitytable.addCell(newLineCell);
		validitytable.addCell(signatorycell);
		
		
		validitytable.setWidthPercentage(100f);
		
		PdfPCell cell6 = new PdfPCell(validitytable);
		cell6.setBorder(0);
		
		PdfPTable parent6 = new PdfPTable(1);
		parent6.addCell(cell6);
		parent6.setWidthPercentage(100);
		
		try {
			document.add(parent6);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	
	
	
	public void createProductInfoHeadding(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 11 ,Font.BOLD);
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);
		
		
		Phrase premise = new Phrase("Premises Under Contract", font1);
		Phrase services = new Phrase("Services", font1);
		Phrase duration = new Phrase("Period Of Contract (Days)",font1);
		Phrase frequency = new Phrase("Frequency Of Services", font1);
		Phrase price = new Phrase("Contract Charges (Rs.)", font1);
		
//		Phrase tax = new Phrase(quot.getProductTaxes().get(0).getChargeName() + "" + quot.getProductTaxes().get(0).getChargePercent() + "%",font8);
//		PdfPCell taxcell = new PdfPCell(tax);
//		taxcell.setBorder(0);
//
//		Phrase taxval = new Phrase(quot.getProductTaxes().get(0).getChargePayable() + "" ,font8);
//		PdfPCell taxvalcell = new PdfPCell(taxval);
//		taxvalcell.setBorder(0);
//		
		
		
		PdfPCell codecell = new PdfPCell(premise);
		codecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell servicescell = new PdfPCell(services);
		servicescell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell frequencycell = new PdfPCell(frequency);
		frequencycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
				
		table.addCell(codecell);
		table.addCell(servicescell);
		table.addCell(durationcell);
		table.addCell(frequencycell);
		table.addCell(pricecell);
		
		try {
			table.setWidths(columnWidths7);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

//		table.addCell(taxcell);
//		table.addCell(taxvalcell);
		
		
		double totalAmount = 0;

		for (int j = 0; j < quot.getItems().size(); j++) {
//			double taxVal = removeAllTaxes(quot.getItems().get(j).getPrduct());  //quot.getItems().get(i).
//			double calculatedPrice = products.get(j).getPrice() - taxVal;
			totalAmount = totalAmount + quot.getItems().get(j).getPrice();
		
		}

		
		
		
	for(int i=0; i<this.quot.getItems().size(); i++) {
		    
			String premises ="NA";	
		    if(quot.getItems().get(i).getPremisesDetails()!=null && !quot.getItems().get(i).getPremisesDetails().equals("")){
		    	premises=quot.getItems().get(i).getPremisesDetails();
		    }
			chunk = new Phrase(premises,font10);
			pdfpremise = new PdfPCell(chunk);
			pdfpremise.setHorizontalAlignment(Element.ALIGN_CENTER);			
			pdfpremise.setPaddingTop(3f);
			pdfpremise.setPaddingBottom(2);
			pdfpremise.setPaddingLeft(2);
			pdfpremise.setPaddingRight(2);
			
			
			chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font10);
			
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			pdfservices.setPaddingTop(3f);
			pdfservices.setPaddingBottom(2);
			pdfservices.setPaddingLeft(2);
			pdfservices.setPaddingRight(2);
			
			chunk = new Phrase(quot.getItems().get(i).getDuration() + "",font10);
			
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			pdfduration.setPaddingTop(3f);
			pdfduration.setPaddingBottom(2);
			pdfduration.setPaddingLeft(2);
			pdfduration.setPaddingRight(2);
			
			chunk = new Phrase(quot.getItems().get(i).getNumberOfServices() + "",font10);
			
			pdffrequency = new PdfPCell(chunk);
			pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
			pdffrequency.setPaddingTop(3f);
			pdffrequency.setPaddingBottom(2);
			pdffrequency.setPaddingLeft(2);
			pdffrequency.setPaddingRight(2);
			
			int pricechunk= (int) quot.getItems().get(i).getPrice();
			
			chunk = new Phrase(pricechunk+"",font10);
			
//			chunk = new Phrase(quot.getItems().get(i).getPrice() + "",font8);
			
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
			pdfprice.setPaddingTop(3f);
			pdfprice.setPaddingBottom(2);
			pdfprice.setPaddingLeft(2);
			pdfprice.setPaddingRight(2);
			
			if(quot.isConsolidatePrice()){
				if (i == 0) {
					chunk = new Phrase(df.format(totalAmount), font10);
					pdfprice = new PdfPCell(chunk);
					pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
					if(quot.getItems().size() > 1)
						pdfprice.setBorderWidthBottom(0);
					
				}else {
					chunk = new Phrase(" ", font10);
					pdfprice = new PdfPCell(chunk);
					pdfprice.setHorizontalAlignment(Element.ALIGN_RIGHT);
					if(i == quot.getItems().size()-1 ){							
						pdfprice.setBorderWidthTop(0);
					}else{
						pdfprice.setBorderWidthBottom(0);
						pdfprice.setBorderWidthTop(0);
					}
				}	
			}
			
			
			table.addCell(pdfpremise);
			table.addCell(pdfservices);
			table.addCell(pdfduration);
			table.addCell(pdffrequency);
			table.addCell(pdfprice);
		}
	
	
	Phrase blank=new Phrase();
	blank.add(" ");
	
	PdfPCell blankCell = new PdfPCell();
	blankCell.addElement(blank);
	blankCell.setBorder(0);
	
	Phrase total = new Phrase("Total",font10);
	PdfPCell totalcell = new PdfPCell(total);
	totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase totalvalue = new Phrase(df.format(quot.getTotalAmount())+"",font10);
	PdfPCell totalvalcell = new PdfPCell(totalvalue);
	totalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(totalcell);
	table.addCell(totalvalcell);
	
	for(int i=0; i<this.quot.getProductTaxes().size(); i++){
		
		if(quot.getProductTaxes().get(i).getChargePercent() !=0){
			
			
		chunk = new Phrase("" + quot.getProductTaxes().get(i).getChargeName() + " : " + quot.getProductTaxes().get(i).getChargePercent() + "%",font10);
		System.out.println("expected name: " + quot.getProductTaxes().get(i).getChargeName());
		pdftaxname = new PdfPCell(chunk);
		pdftaxname.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
	/*****************************************************************************************************/	
//		chunk = new Phrase(quot.getProductTaxes().get(i).getChargePercent()+"",font8);
//		System.out.println("expected ChargePercent:" + quot.getProductTaxes().get(i).getChargePercent());
//		pdftaxpercent = new PdfPCell(chunk);
//		pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		chunk = new Phrase("" + quot.getProductTaxes().get(i).getAssessableAmount(),font8);
//		System.out.println("expected AssessableAmount: " + quot.getProductTaxes().get(i).getAssessableAmount());
//		pdfassval = new PdfPCell(chunk);
//		pdfassval.setHorizontalAlignment(Element.ALIGN_CENTER);
		
	/***********************************************************************************************/	
		
		chunk = new Phrase(df.format(quot.getProductTaxes().get(i).getChargePayable())+ "",font10);
		System.out.println("expected ChargeAbsValue: " + quot.getProductTaxes().get(i).getChargePayable());
		pdfamt = new PdfPCell(chunk);
		pdfamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(pdftaxname);
		table.addCell(pdfamt);
		
		}
		
	}
	
	Phrase subtotal = new Phrase("Sub Total",font10);
	PdfPCell subtotalcell = new PdfPCell(subtotal);
	subtotalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase subtotalvalue = new Phrase(df.format(quot.getNetpayable())+"",font10);
	PdfPCell subtotalvalcell = new PdfPCell(subtotalvalue);
	subtotalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(subtotalcell);
	table.addCell(subtotalvalcell);
	
	
	PdfPCell productcell = new PdfPCell();
	productcell.addElement(table);
	productcell.setBorder(0);
	
	PdfPTable parent7 = new PdfPTable(1);
	parent7.addCell(productcell);
	parent7.setWidthPercentage(100);
	
	
	try {
		document.add(parent7);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	}
	
	
	
	public void createTermsandPaymentHeadding(){
		
		
		String paymentTerm="";
		if(quot.getPaymentTermsList()!=null&&quot.getPaymentTermsList().size()!=0){
			paymentTerm=quot.getPaymentTermsList().get(0).getPayTermComment();
			
		}
		//"Terms of Payment : 
		Phrase terms = new Phrase("Terms Of Payment : " +paymentTerm  ,font9bold);
		PdfPCell termscell = new PdfPCell(terms);
		termscell.setBorder(0);;
		termscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase blank=new Phrase();
		blank.add(" ");
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
		blankCell.setBorder(0);
		
		
		PdfPTable termstable = new PdfPTable(1);
		termstable.addCell(termscell);
		termstable.addCell(blankCell);
		termstable.addCell(blankCell);
		termstable.setHorizontalAlignment(Element.ALIGN_LEFT);
	    termstable.setWidthPercentage(100f);
	
	    
	    PdfPCell cell4 = new PdfPCell();
	    cell4.addElement(termstable);
	    cell4.setBorder(0);
	    
	    PdfPTable parent5 = new PdfPTable(1);
	    parent5.addCell(cell4);
	    parent5.setWidthPercentage(100);
	    parent5.setHorizontalAlignment(Element.ALIGN_LEFT);
	    try {
			document.add(parent5);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
}
	
}

