package com.slicktechnologies.server.addhocprinting.fm;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.StringUtils;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.server.addhocprinting.SalesInvoicePdf;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cnc.CNC;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class CustomizedCncInvoiceVerOne {

	Logger logger = Logger.getLogger("CustomCNCInvoice.class");

	public Document document;
	

	List<State> stateList;
	
	Invoice invoice;
	Customer cust;
	Company comp;
	ServiceProduct serviceProduct;
	CNC cnc;
	CompanyPayment companyPayment;
	Branch branchDt;
	
	CustomerBranchDetails customerBranch = null;
	
	List<ArticleType> articletype;
	ProcessConfiguration processConfig;
	
	String companyName = "";
	
	
	SimpleDateFormat fmt, fmt2;
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat df2 = new DecimalFormat("00");
	
	
	
	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 6);
	Font font5bold = new Font(Font.FontFamily.HELVETICA, 5, Font.BOLD);
	Font font5 = new Font(Font.FontFamily.HELVETICA, 5);

	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD| Font.UNDERLINE);
	
	PdfPCell coloncell = null;
	private PdfPCell imageSignCell;



	
	ServerAppUtility serverAppUtility = new ServerAppUtility();
	PdfUtility pdfUtility=new PdfUtility();
	float[] columns3 = { 25.0f, 2.0f, 78.0f};
	float[] columns2 = { 70.0f, 30.0f};
	float[] columns7 = { 12.0f, 38.0f, 10.0f, 10.0f, 10.0f, 10.0f,10.0f};

	public void setInvoice(Long count) {
		invoice = ofy().load().type(Invoice.class).id(count).now();
		if (invoice.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", invoice.getPersonInfo().getCount()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("count", invoice.getPersonInfo().getCount()).filter("companyId", invoice.getCompanyId()).first().now();

		// Load Company
		if (invoice.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", invoice.getCompanyId()).first().now();
		
		companyName = comp.getBusinessUnitName();

		if (invoice.getCompanyId() != null)
			cnc = ofy().load().type(CNC.class).filter("count", invoice.getContractCount()).filter("companyId", invoice.getCompanyId()).first().now();
		else
			cnc = ofy().load().type(CNC.class).filter("count", invoice.getContractCount()).first().now();

		if (invoice != null && invoice.getBranch() != null&& invoice.getBranch().trim().length() > 0) {
			branchDt = ofy().load().type(Branch.class).filter("companyId", invoice.getCompanyId()).filter("buisnessUnitName", invoice.getBranch()).first().now();
		}

		if (invoice.getPaymentMode() != null&& !invoice.getPaymentMode().equals("")) {
			logger.log(Level.SEVERE, "INSIDE load Companypayment");
			try {
				String paymentMode = "";
				String paymentmodevalue = "";
				if (invoice.getPaymentMode().contains("/")) {
					String[] payArr = invoice.getPaymentMode().split("/");
					paymentMode = payArr[1];

					paymentmodevalue = paymentMode.trim();
					logger.log(Level.SEVERE, "PAYMENT MODE" + paymentmodevalue);
				}
				companyPayment = ofy().load().type(CompanyPayment.class).filter("paymentBankName", paymentmodevalue).filter("companyId", invoice.getCompanyId()).first().now();

				logger.log(Level.SEVERE, "INSIDE TRY" + paymentMode);

			} catch (Exception e) {

			}
		} else if(branchDt.getPaymentMode()!=null&&!branchDt.getPaymentMode().equals("")){
			try {
				String paymentMode = "";
				String paymentmodevalue = "";
				if (branchDt.getPaymentMode().contains("/")) {
					String[] payArr = branchDt.getPaymentMode().split("/");
					paymentMode = payArr[1];

					paymentmodevalue = paymentMode.trim();
					logger.log(Level.SEVERE, "PAYMENT MODE" + paymentmodevalue);
				}
				companyPayment = ofy().load().type(CompanyPayment.class).filter("paymentBankName", paymentmodevalue).filter("companyId", invoice.getCompanyId()).first().now();

				logger.log(Level.SEVERE, "INSIDE TRY" + paymentMode);

			} catch (Exception e) {

			}
		}else {
			logger.log(Level.SEVERE, "INSIDE ELSEEEEE load Companypayment");
			companyPayment = ofy().load().type(CompanyPayment.class).filter("paymentDefault", true).filter("companyId", invoice.getCompanyId()).first().now();
		}

		stateList = ofy().load().type(State.class).filter("companyId", invoice.getCompanyId()).list();
		
		serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", invoice.getCompanyId()).filter("productName", "Management Fees").first().now();
		
		Phrase colonph = new Phrase(":", font8bold);
		coloncell = new PdfPCell();
		coloncell.addElement(colonph);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		coloncell.setBorder(0);
	}

	public void createPdf(String preprintStatus) {

		fmt = new SimpleDateFormat("dd MMM yyyy");
		fmt2 = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt2.setTimeZone(TimeZone.getTimeZone("IST"));

		if (preprintStatus.equals("plane")) {
			createHeader();
		} else {
			if (preprintStatus.equals("yes")) {
				createBlankforUPC();
			}
			if (preprintStatus.equals("no")) {
				createCompanyNameAsHeader(document, comp);
			}
		}

		invoiceDetails();
		customerDetails();
		productDetails();
		paymentDetails();
		createAmountInWords();
		signatoryDetails();
		

	}

	private void productDetails() {
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columns7);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		table.addCell(pdfUtility.getCell("HSN/SAC Code", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(pdfUtility.getCell("Particular", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(pdfUtility.getCell("No. of Staff", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(pdfUtility.getCell("Rate Per Head", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(pdfUtility.getCell("Rate Per Day", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(pdfUtility.getCell("Actual Duties", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		table.addCell(pdfUtility.getCell("Amount", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		int counter=0;
		double mangementFess=0;
		
		if(invoice.getComment()!=null&&!invoice.getComment().equals("")){
			counter++;
			PdfPCell hsnCell=pdfUtility.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell productNameCell=pdfUtility.getCell(invoice.getComment(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
			PdfPCell noOfStaffCell=pdfUtility.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell ratePerHeadCell=pdfUtility.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0);
			PdfPCell ratePerDayCell=pdfUtility.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0);
			PdfPCell actualDutiesCell=pdfUtility.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0);
			PdfPCell totalCell=pdfUtility.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0);
			
			hsnCell.setBorderWidthBottom(0);
			productNameCell.setBorderWidthBottom(0);
			noOfStaffCell.setBorderWidthBottom(0);
			ratePerHeadCell.setBorderWidthBottom(0);
			ratePerDayCell.setBorderWidthBottom(0);
			actualDutiesCell.setBorderWidthBottom(0);
			totalCell.setBorderWidthBottom(0);
			
			table.addCell(hsnCell);
			table.addCell(productNameCell);
			table.addCell(noOfStaffCell);
			table.addCell(ratePerHeadCell);
			table.addCell(ratePerDayCell);
			table.addCell(actualDutiesCell);
			table.addCell(totalCell);
		}
		
		
		Vector<String> billTypeList=new Vector<String>();
		billTypeList.add("Labour");
		billTypeList.add("Overtime");
		billTypeList.add("Public Holiday");
		billTypeList.add("Holiday");
		
		Comparator<SalesOrderProductLineItem> comp =new Comparator<SalesOrderProductLineItem>() {
			@Override
			public int compare(SalesOrderProductLineItem item1,SalesOrderProductLineItem item2) {
				return item1.getProdName().compareTo(item2.getProdName());
			}
		};
		Collections.sort(invoice.getSalesOrderProducts(),comp);
		
		
		for(String billType:billTypeList){
			for(SalesOrderProductLineItem item:invoice.getSalesOrderProducts()){
				if(item.getBillType()!=null&&billType.equals(item.getBillType())){
					counter++;
					double quantity=0;
					try{
						quantity=Double.parseDouble(item.getArea());
					}catch(Exception e){
						
					}
					
					if(quantity==0){
						continue;
					}
					String hsnCode="";
					if(item.getHsnCode()!=null){
						hsnCode=item.getHsnCode();
					}
					String productName=item.getProdName();
					String noOfStaff=item.getManpower()+"";
					String ratePerHead="";
					
					String actualDuties="";
					if(item.getArea()!=null&&!item.getArea().equals("NA")){
						actualDuties=df.format(Double.parseDouble(item.getArea()));
					}
					
					
					if(invoice.isConsolidatePrice()&&serviceProduct!=null){
						ratePerHead=df.format(item.getCtcAmount())+"";
					}else{
						ratePerHead=df.format(item.getCtcAmount()+item.getManagementFees())+"";
					}
					String ratePerDay=df.format(item.getPrice());
					
					if(invoice.isConsolidatePrice()&&serviceProduct!=null){
						if(item.getCtcAmount()!=0 && item.getManagementFees()!=0){
					  		logger.log(Level.SEVERE,"item.getManagementFees() "+item.getManagementFees());
					  		logger.log(Level.SEVERE,"item.getCtcAmount() "+item.getCtcAmount());
		
							double perdayRate = (item.getPrice()/((((item.getManagementFees()*100)/item.getCtcAmount())/100)+1));
					  		logger.log(Level.SEVERE,"perdayRate"+perdayRate);
					  		
					  		ratePerDay=df.format(perdayRate);
					  		
							double serviceCharge = (((perdayRate*quantity)*((item.getManagementFees()*100)/item.getCtcAmount()))/100);
					  		logger.log(Level.SEVERE,"serviceCharge"+serviceCharge);
					  		mangementFess+=serviceCharge;
							try{
								item.setTotalAmount(perdayRate * quantity);
							}catch(Exception e){
								
							}
						}
					}
					
					String total=df.format(item.getTotalAmount())+"";
					
					PdfPCell hsnCell=pdfUtility.getCell(hsnCode, font8, Element.ALIGN_CENTER, 0, 0, 0);
					PdfPCell productNameCell=pdfUtility.getCell(productName, font8, Element.ALIGN_LEFT, 0, 0, 0);
					PdfPCell noOfStaffCell=pdfUtility.getCell(noOfStaff, font8, Element.ALIGN_CENTER, 0, 0, 0);
					PdfPCell ratePerHeadCell=pdfUtility.getCell(ratePerHead, font8, Element.ALIGN_RIGHT, 0, 0, 0);
					PdfPCell ratePerDayCell=pdfUtility.getCell(ratePerDay, font8, Element.ALIGN_RIGHT, 0, 0, 0);
					PdfPCell actualDutiesCell=pdfUtility.getCell(actualDuties, font8, Element.ALIGN_CENTER, 0, 0, 0);
					PdfPCell totalCell=pdfUtility.getCell(total, font8, Element.ALIGN_RIGHT, 0, 0, 0);
					
					if(counter==1){
						hsnCell.setBorderWidthBottom(0);
						productNameCell.setBorderWidthBottom(0);
						noOfStaffCell.setBorderWidthBottom(0);
						ratePerHeadCell.setBorderWidthBottom(0);
						ratePerDayCell.setBorderWidthBottom(0);
						actualDutiesCell.setBorderWidthBottom(0);
						totalCell.setBorderWidthBottom(0);
					}else if(counter==27&&counter==invoice.getSalesOrderProducts().size()-1){
						hsnCell.setBorderWidthTop(0);
						productNameCell.setBorderWidthTop(0);
						noOfStaffCell.setBorderWidthTop(0);
						ratePerDayCell.setBorderWidthTop(0);
						ratePerHeadCell.setBorderWidthTop(0);
						actualDutiesCell.setBorderWidthTop(0);
						totalCell.setBorderWidthTop(0);
					}else if(counter>27&&counter==invoice.getSalesOrderProducts().size()-1){
						hsnCell.setBorderWidthTop(0);
						productNameCell.setBorderWidthTop(0);
						noOfStaffCell.setBorderWidthTop(0);
						ratePerDayCell.setBorderWidthTop(0);
						ratePerHeadCell.setBorderWidthTop(0);
						actualDutiesCell.setBorderWidthTop(0);
						totalCell.setBorderWidthTop(0);
					}else{
						hsnCell.setBorderWidthBottom(0);
						hsnCell.setBorderWidthTop(0);
						productNameCell.setBorderWidthBottom(0);
						productNameCell.setBorderWidthTop(0);
						noOfStaffCell.setBorderWidthBottom(0);
						noOfStaffCell.setBorderWidthTop(0);
						ratePerHeadCell.setBorderWidthBottom(0);
						ratePerHeadCell.setBorderWidthTop(0);
						ratePerDayCell.setBorderWidthBottom(0);
						ratePerDayCell.setBorderWidthTop(0);
						actualDutiesCell.setBorderWidthBottom(0);
						actualDutiesCell.setBorderWidthTop(0);
						totalCell.setBorderWidthBottom(0);
						totalCell.setBorderWidthTop(0);
					}
					
					table.addCell(hsnCell);
					table.addCell(productNameCell);
					table.addCell(noOfStaffCell);
					table.addCell(ratePerHeadCell);
					table.addCell(ratePerDayCell);
					table.addCell(actualDutiesCell);
					table.addCell(totalCell);
				}
			}
		}
		if(invoice.isConsolidatePrice()&&serviceProduct!=null){
			if(mangementFess!=0){
				counter++;
				
				String hsnCode="";
				if(serviceProduct.getHsnNumber()!=null){
					hsnCode=serviceProduct.getHsnNumber();
				}
				String productName=serviceProduct.getProductName();
				String noOfStaff="";
				String ratePerHead="";
				String actualDuties="";
				String ratePerDay="";
				String total=df.format(mangementFess);
				if(invoice.getManagementFeesPercent()!=0){
					productName=productName+" - "+invoice.getManagementFeesPercent()+" %";
				}
				
				
				PdfPCell hsnCell=pdfUtility.getCell(hsnCode, font8, Element.ALIGN_CENTER, 0, 0, 0);
				PdfPCell productNameCell=pdfUtility.getCell(productName, font8, Element.ALIGN_LEFT, 0, 0, 0);
				PdfPCell noOfStaffCell=pdfUtility.getCell(noOfStaff, font8, Element.ALIGN_CENTER, 0, 0, 0);
				PdfPCell ratePerHeadCell=pdfUtility.getCell(ratePerHead, font8, Element.ALIGN_RIGHT, 0, 0, 0);
				PdfPCell ratePerDayCell=pdfUtility.getCell(ratePerDay, font8, Element.ALIGN_RIGHT, 0, 0, 0);
				PdfPCell actualDutiesCell=pdfUtility.getCell(actualDuties, font8, Element.ALIGN_CENTER, 0, 0, 0);
				PdfPCell totalCell=pdfUtility.getCell(total, font8, Element.ALIGN_RIGHT, 0, 0, 0);
				
				
				if(counter==1){
					hsnCell.setBorderWidthBottom(0);
					productNameCell.setBorderWidthBottom(0);
					noOfStaffCell.setBorderWidthBottom(0);
					ratePerHeadCell.setBorderWidthBottom(0);
					ratePerDayCell.setBorderWidthBottom(0);
					actualDutiesCell.setBorderWidthBottom(0);
					totalCell.setBorderWidthBottom(0);
				}else if(counter==27&&counter==invoice.getSalesOrderProducts().size()-1){
					hsnCell.setBorderWidthTop(0);
					productNameCell.setBorderWidthTop(0);
					noOfStaffCell.setBorderWidthTop(0);
					ratePerDayCell.setBorderWidthTop(0);
					ratePerHeadCell.setBorderWidthTop(0);
					actualDutiesCell.setBorderWidthTop(0);
					totalCell.setBorderWidthTop(0);
				}else if(counter>27&&counter==invoice.getSalesOrderProducts().size()-1){
					hsnCell.setBorderWidthTop(0);
					productNameCell.setBorderWidthTop(0);
					noOfStaffCell.setBorderWidthTop(0);
					ratePerDayCell.setBorderWidthTop(0);
					ratePerHeadCell.setBorderWidthTop(0);
					actualDutiesCell.setBorderWidthTop(0);
					totalCell.setBorderWidthTop(0);
				}else{
					hsnCell.setBorderWidthBottom(0);
					hsnCell.setBorderWidthTop(0);
					productNameCell.setBorderWidthBottom(0);
					productNameCell.setBorderWidthTop(0);
					noOfStaffCell.setBorderWidthBottom(0);
					noOfStaffCell.setBorderWidthTop(0);
					ratePerHeadCell.setBorderWidthBottom(0);
					ratePerHeadCell.setBorderWidthTop(0);
					ratePerDayCell.setBorderWidthBottom(0);
					ratePerDayCell.setBorderWidthTop(0);
					actualDutiesCell.setBorderWidthBottom(0);
					actualDutiesCell.setBorderWidthTop(0);
					totalCell.setBorderWidthBottom(0);
					totalCell.setBorderWidthTop(0);
				}
				
				table.addCell(hsnCell);
				table.addCell(productNameCell);
				table.addCell(noOfStaffCell);
				table.addCell(ratePerHeadCell);
				table.addCell(ratePerDayCell);
				table.addCell(actualDutiesCell);
				table.addCell(totalCell);
			}
		}
		
		
		if(counter<27){
			
			int remainingLines=27-counter;
			logger.log(Level.SEVERE,"Ramaining Blank lines : "+remainingLines );
			
			PdfPCell blankCell=pdfUtility.getCell(" ", font9bold, Element.ALIGN_CENTER, 0, 0, 0);
			while(counter<27){
				counter++;
				if(counter==27){
					blankCell.setBorderWidthTop(0);
				}else{
					blankCell.setBorderWidthBottom(0);
					blankCell.setBorderWidthTop(0);
				}
				
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
				table.addCell(blankCell);
			}
			
			
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void paymentDetails() {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		
		try {
			table.setWidths(columns2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPTable leftTbl = new PdfPTable(1);
		leftTbl.setWidthPercentage(100);
		
		if(companyPayment!=null){
			leftTbl.addCell(pdfUtility.getCell("BANK DETAILS", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(companyPayment.getPaymentBankName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell("A/C No : "+companyPayment.getPaymentAccountNo(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell("IFSC Code : "+companyPayment.getPaymentIFSCcode(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell("Branch Address : "+companyPayment.getPaymentBranch(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell("MICR Code : "+companyPayment.getPaymentMICRcode(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		
		
		PdfPTable rightTbl = new PdfPTable(2);
		rightTbl.setWidthPercentage(100);
		
		rightTbl.addCell(pdfUtility.getCell("Net Amount", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(df.format(invoice.getFinalTotalAmt()), font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
		
		if(invoice.getBillingTaxes()!=null&&invoice.getBillingTaxes().size()!=0){
			for(ContractCharges tax:invoice.getBillingTaxes()){
				double totalCalcAmt=0;
				totalCalcAmt=tax.getTaxChargeAssesVal()*tax.getTaxChargePercent()/100;
				totalCalcAmt=Double.parseDouble(df.format(totalCalcAmt));
				tax.setPayableAmt(totalCalcAmt);
				
				rightTbl.addCell(pdfUtility.getCell(tax.getTaxChargeName(), font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				rightTbl.addCell(pdfUtility.getCell(df.format(tax.getPayableAmt()), font8, Element.ALIGN_RIGHT, 0, 0, 0)).setBorder(0);
			}
		}
		
		
//		PdfPCell amtInWordsCell=pdfUtility.getCell("RUPEES "+SalesInvoicePdf.convert(invoice.getNetPayable()).toUpperCase(), font8bold, Element.ALIGN_LEFT, 0, 0, 0);
//		amtInWordsCell.setBorderWidthRight(0);
//		leftTbl.addCell(amtInWordsCell);
//		
//		
//		PdfPCell amtInNumCell=pdfUtility.getCell("Rs. "+invoice.getNetPayable(), font8bold, Element.ALIGN_RIGHT, 0, 2, 0);
//		amtInNumCell.setBorderWidthLeft(0);
//		rightTbl.addCell(amtInNumCell);
		
		
		PdfPCell leftCell = new PdfPCell(leftTbl);
		leftCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell rightCell = new PdfPCell(rightTbl);
		rightCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(leftCell);
		table.addCell(rightCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void signatoryDetails() {
		float[] columns6 = { 13.5f, 3.5f, 33.0f, 13.5f, 3.5f, 33.0f};
		
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columns6);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		

		String panNum="";
		
		if(branchDt.getPANNumber()!=null&&!branchDt.getPANNumber().equals("")){
			panNum=branchDt.getPANNumber();
		}else{
			panNum=getValueFromCompanyArticalType("ServiceInvoice", "PAN No");
		}
		
		if(panNum!=null&&!panNum.equals("")){
			table.addCell(pdfUtility.getCell("PAN No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(panNum, font8, Element.ALIGN_LEFT, 0, 4, 0)).setBorder(0);
		}
		
		String compGstNum=serverAppUtility.getGSTINOfCompany(comp, invoice.getBranch());
		if(compGstNum!=null&&!compGstNum.equals("")){
			String compGstState = "";
			if (comp.getAddress().getState() != null) {
				compGstState=comp.getAddress().getState();
			}
			
			for (State state : stateList) {
				if (state.getStateName().equals(compGstState)) {
					if(state.getStateCode()!=null&&!state.getStateCode().equals("")){
						compGstState = compGstState+"("+state.getStateCode().trim()+")";
					}
					break;
				}
			} 
			
			table.addCell(pdfUtility.getCell("GST State", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(compGstState, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			table.addCell(pdfUtility.getCell("GST No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(compGstNum, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		String compPfNum="";
		String compEsicNum="";
		String compLwfNum="";
		String compPtNum="";
		
		 if(comp.getPfGroupNum()!=null){
			 compPfNum = comp.getPfGroupNum();
		 }
		 if(branchDt.getEsicCode()!=null){
			 compEsicNum = branchDt.getEsicCode();
		 }
		 if(branchDt.getLwfCode()!=null){
			 compLwfNum = branchDt.getLwfCode();
		 }
		 if(branchDt.getPtCode()!=null){
			 compPtNum = branchDt.getPtCode();
		 }
		 
		int counter=0; 
		if (!compPfNum.equals("")) {
			table.addCell(pdfUtility.getCell("PF No.", font8bold, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0,0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(compPfNum, font8, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			counter++;
		}
		
		if (!compEsicNum.equals("")) {
			table.addCell(pdfUtility.getCell("ESIC No.", font8bold, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0,0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(compEsicNum, font8, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			counter++;
		}
		
		if (!compLwfNum.equals("")) {
			table.addCell(pdfUtility.getCell("LWF No.", font8bold, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0,0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(compLwfNum, font8, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			counter++;
		}
		
		if (!compPtNum.equals("")) {
			table.addCell(pdfUtility.getCell("PT No.", font8bold, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" : ", font8bold, Element.ALIGN_LEFT, 0,0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(compPtNum, font8, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			counter++;
		}
		
		if(counter==1||counter==3){
			table.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" ", font8bold, Element.ALIGN_LEFT, 0,0, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(" ", font8, Element.ALIGN_LEFT,0, 0, 0)).setBorder(0);
		}
		
		if(invoice.getComment1()!=null&&!invoice.getComment1().equals("")){
			table.addCell(pdfUtility.getCell("Note:- ", font8boldul, Element.ALIGN_LEFT,0, 6, 0)).setBorder(0);
			table.addCell(pdfUtility.getCell(invoice.getComment1(), font8, Element.ALIGN_LEFT,0, 6, 0)).setBorder(0);
		}
		

		
		PdfPTable tableDigitalSign = new PdfPTable(1);
		tableDigitalSign.setWidthPercentage(100);
		
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
//		PdfPCell cellBlank = new PdfPCell(blank);
//		cellBlank.setBorder(0);
//		cellBlank.setHorizontalAlignment(Element.ALIGN_LEFT);
//		tableDigitalSign.addCell(cellBlank);

		String companyName = "For ";
		if (branchDt.getCorrespondenceName() != null && !branchDt.getCorrespondenceName().equals("")) {
			companyName += branchDt.getCorrespondenceName();
		} else {
			companyName += comp.getBusinessUnitName();
		}
		Phrase companyPhrase = new Phrase(companyName, font10bold);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setBorder(0);
		companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		tableDigitalSign.addCell(companyParaCell);

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();

		if (branchDt.getUploadDigitalSign() != null
				&& branchDt.getUploadDigitalSign().getUrl() != null
				&& !branchDt.getUploadDigitalSign().getUrl().equals("")) {
			digitalDocument = branchDt.getUploadDigitalSign();
		}

		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			image2 = Image.getInstance(new URL(hostUrl+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (imageSignCell != null) {
			tableDigitalSign.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			tableDigitalSign.addCell(blank1Cell);
			tableDigitalSign.addCell(blank1Cell);
			tableDigitalSign.addCell(blank1Cell);
			tableDigitalSign.addCell(blank1Cell);

		}
		
		Phrase signAuth;
		if (comp.getSignatoryText() != null&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
			signAuth = new Phrase("Authorised Signatory", font10bold);
		} else {
			signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
		}
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		signParaCell.setBorder(0);
		signParaCell.setPaddingLeft(30f);
		signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		tableDigitalSign.addCell(signParaCell);

		
		
		PdfPTable parent = new PdfPTable(2);
		parent.setWidthPercentage(100);
		float[] columns2 = { 65.0f, 35.0f};
		try {
			parent.setWidths(columns2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		parent.addCell(table);
		parent.addCell(tableDigitalSign);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	private void createAmountInWords() {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);

		float[] columns2 = { 80.0f, 20.0f};
		try {
			table.setWidths(columns2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPCell amtInWordsCell=pdfUtility.getCell("RUPEES "+SalesInvoicePdf.convert(invoice.getNetPayable()).toUpperCase()+" ONLY.", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		amtInWordsCell.setBorderWidthRight(0);
		table.addCell(amtInWordsCell);
		
		
		PdfPCell amtInNumCell=pdfUtility.getCell("Rs. "+df.format(invoice.getNetPayable()), font8bold, Element.ALIGN_RIGHT, 0, 0, 0);
		amtInNumCell.setBorderWidthLeft(0);
		table.addCell(amtInNumCell);

		

		try {
			document.add(table);

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void customerDetails() {
		
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
		
		PdfPTable leftTbl = new PdfPTable(3);
		leftTbl.setWidthPercentage(100);
		try {
			leftTbl.setWidths(columns3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		String custBillingAdd="";
		if(cnc.getBillingAddress()!=null){
			custBillingAdd=cnc.getBillingAddress().getCompleteAddress();
		}else{
			custBillingAdd=cust.getAdress().getCompleteAddress();
		}
		
		Phrase phCustNameLbl = new Phrase("Name & Address of Customer :", font8bold);
		Phrase phCustName = new Phrase(StringUtils.capitalize(invoice.getPersonInfo().getFullName()), font8bold);
		Phrase phCustBillAdd = new Phrase(custBillingAdd, font8);

		PdfPCell custNameLblCell = new PdfPCell(phCustNameLbl);
		custNameLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custNameLblCell.setBorder(0);
		custNameLblCell.setColspan(3);

		PdfPCell custNameCell = new PdfPCell(phCustName);
		custNameCell.setBorder(0);
		custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custNameCell.setColspan(3);

		PdfPCell custBillAddCell = new PdfPCell(phCustBillAdd);
		custBillAddCell.setBorder(0);
		custBillAddCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custBillAddCell.setColspan(3);
		
		leftTbl.addCell(custNameLblCell);
		leftTbl.addCell(custNameCell);
		leftTbl.addCell(custBillAddCell);
		
		String panNum="";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("YES")
					&& cust.getArticleTypeDetails().get(i).getDocumentName().equals("ServiceInvoice")) {
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("PAN No")) {
					panNum = cust.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
					break;
				}
			}
		}
		
		if(!panNum.equals("")){
			leftTbl.addCell(pdfUtility.getCell("PAN No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(panNum, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		
		String custGstNum = "";
		if (cnc.getCustomerGstNumber() != null&& !cnc.getCustomerGstNumber().equals("")) {
			custGstNum = cnc.getCustomerGstNumber();
		} else {
			custGstNum = serverAppUtility.getGSTINOfCustomer(cust, null,"ServiceInvoice");
		}

		if (!custGstNum.equals("")) {
			String custGstState = "";
			if (cnc.getBillingAddress().getState() != null) {
				custGstState=cnc.getBillingAddress().getState();
			}else{
				custGstState=cust.getAdress().getState();
			}
			
			for (State state : stateList) {
				if (state.getStateName().equals(custGstState)) {
					if(state.getStateCode()!=null&&!state.getStateCode().equals("")){
						custGstState = custGstState+"("+state.getStateCode().trim()+")";
					}
					break;
				}
			}
			
			
			leftTbl.addCell(pdfUtility.getCell("GST No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(custGstNum, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			
			leftTbl.addCell(pdfUtility.getCell("GST State", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdfUtility.getCell(custGstState, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}

		

		

		PdfPTable rightTbl = new PdfPTable(3);
		rightTbl.setWidthPercentage(100);
		
		try {
			rightTbl.setWidths(columns3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		String custShipingAdd="";
		if(cnc.getServiceAddress()!=null){
			custShipingAdd=cnc.getServiceAddress().getCompleteAddress();
		}else{
			custShipingAdd=cust.getSecondaryAdress().getCompleteAddress();
		}
		
		rightTbl.addCell(pdfUtility.getCell("Ship / Service Provided at :", font8bold, Element.ALIGN_LEFT, 2, 3, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(custShipingAdd, font8, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(0);
		
		rightTbl.addCell(pdfUtility.getCell("Kind Attn", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(cust.getName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(cust.getCellNumber1()!=null){
			rightTbl.addCell(pdfUtility.getCell("Contact No", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(pdfUtility.getCell(cust.getCellNumber1()+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		if(cust.getEmail()!=null&&!cust.getEmail().equals("")){
			rightTbl.addCell(pdfUtility.getCell("Email", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			rightTbl.addCell(pdfUtility.getCell(cust.getEmail()+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		PdfPCell leftCell = new PdfPCell(leftTbl);
		leftCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell rightCell = new PdfPCell(rightTbl);
		rightCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(leftCell);
		table.addCell(rightCell);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private String getValueFromCompanyArticalType(String documentType,String articalType) {

		if (comp != null) {
			for (ArticleType artical : comp.getArticleTypeDetails()) {
				if (artical.getDocumentName().trim().equals(documentType)
						&& artical.getArticleTypeName().equals(articalType)) {
					return artical.getArticleTypeValue();
				}
			}
		}
		return "";
	}

	private void invoiceDetails() {
		PdfPTable table = new PdfPTable(2);
		table.setWidthPercentage(100);
//		table.setSpacingBefore(5f);

		Phrase titlephrase = new Phrase("Tax Invoice", font12bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setVerticalAlignment(Element.ALIGN_CENTER);
		titlecell.setColspan(2);
		
		table.addCell(titlecell);

		
		PdfPTable leftTbl = new PdfPTable(3);
		leftTbl.setWidthPercentage(100);
		
		try {
			leftTbl.setWidths(columns3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase phInvoiceNo = new Phrase("Invoice No.", font8bold);
		Phrase phInvoiceDt = new Phrase("Invoice Date", font8bold);
//		
//		PdfPCell invNoCell = new PdfPCell(phInvoiceNo);
//		invNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invNoCell.setBorder(0);
//		
//		PdfPCell invDtCell = new PdfPCell(phInvoiceDt);
//		invDtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invDtCell.setBorder(0);
//		
		
		String invoiceNo = invoice.getCount()+"";
		if(invoice.getInvRefNumber()!=null){
			invoiceNo=invoice.getInvRefNumber();
		}
		String invoiceDate = fmt2.format(invoice.getInvoiceDate());
//		Phrase phInvoiceNoV = new Phrase(invoiceNo, font8);
//		Phrase phInvoiceDtV = new Phrase(invoiceDate, font8);
//
//		PdfPCell invNoCellV = new PdfPCell(phInvoiceNoV);
//		invNoCellV.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invNoCellV.setBorder(0);
//		
//		PdfPCell invDtCellV = new PdfPCell(phInvoiceDtV);
//		invDtCellV.setHorizontalAlignment(Element.ALIGN_LEFT);
//		invDtCellV.setBorder(0);
//		
//		leftTbl.addCell(invNoCell);
//		leftTbl.addCell(coloncell);
//		leftTbl.addCell(invNoCellV);
//		
//		leftTbl.addCell(invDtCell);
//		leftTbl.addCell(coloncell);
//		leftTbl.addCell(invDtCellV);
		
		
		leftTbl.addCell(pdfUtility.getCell("Invoice No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdfUtility.getCell(invoiceNo, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		leftTbl.addCell(pdfUtility.getCell("Invoice Date", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdfUtility.getCell(invoiceDate, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		
		PdfPTable rightTbl = new PdfPTable(3);
		rightTbl.setWidthPercentage(100);
		
		try {
			rightTbl.setWidths(columns3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase phWoNo = new Phrase("LOI/PO/WO No.", font8bold);
		Phrase phServicePeriod = new Phrase("Service Perioid", font8bold);
		
		PdfPCell woNoCell = new PdfPCell(phWoNo);
		woNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		woNoCell.setBorder(0);
		
		PdfPCell serPerCell = new PdfPCell(phServicePeriod);
		serPerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serPerCell.setBorder(0);
		
		String woNo = "";
		if(invoice.getWoNumber()!=null){
			woNo=invoice.getWoNumber();
		}
		String servPeriod = fmt.format(invoice.getBillingPeroidFromDate())+" to "+fmt.format(invoice.getBillingPeroidToDate());
		

//		Phrase phWoNoV = new Phrase(woNo, font8);
//		Phrase phServPeriodV = new Phrase(servPeriod, font8);
//
//		PdfPCell woNoCellV = new PdfPCell(phWoNoV);
//		woNoCellV.setHorizontalAlignment(Element.ALIGN_LEFT);
//		woNoCellV.setBorder(0);
//		
//		PdfPCell servPerCellV = new PdfPCell(phServPeriodV);
//		servPerCellV.setHorizontalAlignment(Element.ALIGN_LEFT);
//		servPerCellV.setBorder(0);
//
//		rightTbl.addCell(woNoCell);
//		rightTbl.addCell(coloncell);
//		rightTbl.addCell(woNoCellV);
//		
//		rightTbl.addCell(serPerCell);
//		rightTbl.addCell(coloncell);
//		rightTbl.addCell(servPerCellV);
		
		
		rightTbl.addCell(pdfUtility.getCell("LOI/PO/WO No.", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(woNo, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		rightTbl.addCell(pdfUtility.getCell("Service Perioid", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(":", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		rightTbl.addCell(pdfUtility.getCell(servPeriod, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		PdfPCell leftCell = new PdfPCell(leftTbl);
		leftCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell rightCell = new PdfPCell(rightTbl);
		rightCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		table.addCell(leftCell);
		table.addCell(rightCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp2) {

		DocumentUpload headerdocument = null;
		if (branchDt.getUploadHeader() != null
				&& branchDt.getUploadHeader().getUrl() != null
				&& !branchDt.getUploadHeader().getUrl().equals("")) {
			headerdocument = branchDt.getUploadHeader();
		} else {
			headerdocument = comp.getUploadHeader();
		}

		if (headerdocument == null || headerdocument.getUrl() == null|| headerdocument.getUrl().equals("")) {
			return;
		}

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ headerdocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
			createBlankforUPC();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createBlankforUPC() {

		Phrase blankphrase = new Phrase("  ", font10);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(1);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			// document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHeader() {
		DocumentUpload companyLogo = null;
		if (branchDt.getLogo() != null) {
			companyLogo = branchDt.getLogo();
		} else {
			companyLogo = comp.getLogo();
		}

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;

		try {
		
			Image image2 = Image.getInstance(new URL(hostUrl+ companyLogo.getUrl()));
			image2.scalePercent(20f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}

		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		
		

		String companyName = "";
		if (branchDt.getCorrespondenceName() != null&& !branchDt.getCorrespondenceName().equals("")) {
			companyName += branchDt.getCorrespondenceName();
		} else {
			companyName += comp.getBusinessUnitName();
		}
		Phrase phcompanyName = new Phrase(companyName, font16bold);
		Phrase phcompanyAdd = new Phrase(comp.getAddress().getCompleteAddress(), font10bold);
		Phrase phcompanyContact = new Phrase("Contact No.: "+comp.getCellNumber1()+" Email: "+comp.getEmail(), font8bold);
		
		String cinNum="";
		if(comp.getArticleTypeDetails()!=null&&comp.getArticleTypeDetails().size()!=0){
			for(ArticleType obj:comp.getArticleTypeDetails()){
				if(obj.getArticleTypeName().equalsIgnoreCase("CIN")){
					cinNum=obj.getArticleTypeValue();
					break;
				}
			}
		}
		Phrase phcompanyCin = new Phrase(cinNum, font10bold);

		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(phcompanyName);
		companyNamepara.setAlignment(Element.ALIGN_CENTER);
		companyNamepara.add(Chunk.NEWLINE);
		companyNamepara.add(phcompanyAdd);
		companyNamepara.add(Chunk.NEWLINE);
		companyNamepara.add(phcompanyContact);
		if(cinNum!=null&&!cinNum.equals("")){
			companyNamepara.add(Chunk.NEWLINE);
			companyNamepara.add(phcompanyCin);
		}

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable companyTab = new PdfPTable(1);
		companyTab.setWidthPercentage(100);
		companyTab.addCell(companyNameCell);
		companyTab.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable mainheader = null;
		if (companyLogo != null && companyLogo.getUrl() != null&& !companyLogo.getUrl().equals("")) {
			mainheader = new PdfPTable(2);
			mainheader.setWidthPercentage(100);
			try {
				mainheader.setWidths(new float[] { 20, 80 });
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
		} else {
			mainheader = new PdfPTable(1);
			mainheader.setWidthPercentage(100);
		}

		PdfPCell leftCell = new PdfPCell(logoTab);
		leftCell.setBorder(0);

		if (companyLogo != null && companyLogo.getUrl() != null&& !companyLogo.getUrl().equals("")) {
			mainheader.addCell(leftCell);
		}

		PdfPCell rightCell = new PdfPCell(companyTab);
		rightCell.setBorder(0);
		rightCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainheader.addCell(rightCell);

		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String getRoundOffValue(double value) {
		if(value != 0){
			DecimalFormat df = new DecimalFormat("##,##,##,##,##0.00");
			return df.format(value);
		}else{
			return "";
		}
	}

}
