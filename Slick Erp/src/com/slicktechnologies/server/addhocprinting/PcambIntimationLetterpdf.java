package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.ibm.icu.text.DateFormat;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambIntimationLetterpdf
{

	Company comp;
	Contract con;
	Customer cust;
//	Invoice invoiceentity;
	ArrayList<ArticleType> articletype;
	Service ser;

	Document document;

	private Font font16boldul, font12bold, font8bold, font8, font9bold, font12boldul, font10boldul, font12, font16bold,
			font10, font10bold, font14bold, font9, font7, font7bold, font9red, font9boldred, font12boldred, font16,
			font23, font20;

	private SimpleDateFormat fmt1 = new SimpleDateFormat("dd MMM yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("hh-mm-ss");

	DecimalFormat df = new DecimalFormat("0.00");

	public PcambIntimationLetterpdf() {
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD | Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font16 = new Font(Font.FontFamily.HELVETICA, 16);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.RED);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD | Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font20 = new Font(Font.FontFamily.HELVETICA, 20);

	}
	
public void setIntimationLetter (Long count) 
{
	
	// Load Service 
	
	 ser = ofy().load().type(Service.class).id(count).now(); 
		
	 // load Customer   
		if(ser.getCompanyId()==null)
		cust = ofy().load().type(Customer.class).first().now();
		else
	    cust=ofy().load().type(Customer.class).filter("companyId", ser.getCompanyId()).filter("count",ser.getCustomerId()).first().now();

		//Load Company
		if(ser.getCompanyId()==null)
		   comp=ofy().load().type(Company.class).first().now();
		else
		   comp=ofy().load().type(Company.class).filter("companyId",ser.getCompanyId()).first().now();
		
		
		//load Contract 
		if(ser.getCompanyId()==null)
			
			   con =ofy().load().type(Contract.class).first().now();
		
			else
				
		       con=ofy().load().type(Contract.class).filter("companyId",ser.getCompanyId()).first().now();
		
	 
		
		articletype = new ArrayList<ArticleType>();
			if(cust.getArticleTypeDetails().size()!=0){
				articletype.addAll(cust.getArticleTypeDetails());
			}
			if(comp.getArticleTypeDetails().size()!=0){
				articletype.addAll(comp.getArticleTypeDetails());
			}
		
		
	}
   
public void createPdf() {
	
	createHeader();
	createCustomerInfo();
	createAddressTbl();
	
}
  
private void createHeader()
{
	
	String addressline1="";
	
	if(comp.getAddress().getAddrLine2()!=null){
		addressline1=comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2() +", ";
	}
	else{
		addressline1=comp.getAddress().getAddrLine1()+", ";
	}
      

	String locality=null;
	if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
		System.out.println("inside both null condition1");
		locality= (comp.getAddress().getLandmark()+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+" - "
			      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
	}
	else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
		System.out.println("inside both null condition 2");
		locality= (comp.getAddress().getLandmark()+", "+comp.getAddress().getCity()+" - "
			      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
	}
	
	else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
		System.out.println("inside both null condition 3");
		locality= (comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+" - "
			      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
	}
	else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
		System.out.println("inside both null condition 4");
		locality=(" "+comp.getAddress().getCity()+" - "
			      +comp.getAddress().getPin()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
	
	}
	
//	 String contactinfo = "";
//	 System.out.println("landline no "+comp.getLandline());
//	 System.out.println("Cell no1 "+comp.getCellNumber1());
//	 System.out.println("Cell no2 "+comp.getCellNumber2());
//	 System.out.println("fax no "+comp.getFaxNumber());
//	 
//	if (comp.getLandline()!=0 && comp.getCellNumber2()!= 0  && comp.getFaxNumber()!=null)
//	{
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() +" / "+ comp.getLandline() +  "  Fax : " + comp.getFaxNumber());
//	}
//	else if (comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()!=null ) 
//	{
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()
//				+ "  Fax : " + comp.getFaxNumber());
//	}
//	else if(comp.getLandline() != 0  && comp.getCellNumber2() == 0 && comp.getFaxNumber()!=null ){
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline() 
//				 + "  Fax : " + comp.getFaxNumber());
//	}
//	else if(comp.getLandline() != 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null)
//	{
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()+" / "+ comp.getLandline() +  "  Fax : "  );	
//	}
//	else if(comp.getLandline() != 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null )
//	{
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline() 
//				+ "  Fax : "  );	
//	}
//	else if(comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null )
//	{
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() 
//				 + "  Fax : "  );	
//	}
//	else if(comp.getLandline() == 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null)
//	{
//		contactinfo = ("Tel. : "  + comp.getCellNumber1() + "  Fax : "  );	
//	}	
		Phrase mycomHeader=new Phrase(comp.getBusinessUnitName().toUpperCase(),font16);
		Phrase regdOffice = new Phrase("Regd Office : ",font9);
		Phrase header1 = new Phrase (addressline1 + locality,font9);
		Phrase header2 = new Phrase ("Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ",font9);
		Phrase header3 = new Phrase (" E-Mail : "+comp.getEmail().trim()+ "  Website : "+comp.getWebsite(),font9);
        
		Phrase header4 =new Phrase("CIN :",font9);
for(int i=0;i<this.articletype.size();i++)
 
 {	
	
	  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("CIN")&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("ContractRenewal")){  
	 		
         header4 =new Phrase("CIN :"+articletype.get(i).getArticleTypeValue(),font9);
     
	  }   
}
		
		Paragraph mycomPara=new Paragraph();
		
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(mycomHeader);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(regdOffice);
		mycomPara.add(header1);
		mycomPara.add(Chunk.NEWLINE);  
		mycomPara.add(header2);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(header3);
		mycomPara.add(Chunk.NEWLINE); 
		mycomPara.add(header4);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(Chunk.NEWLINE);
		
		mycomPara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell mycomCell=new PdfPCell();
		mycomCell.addElement(mycomPara);
		mycomCell.setBorder(0);
			
	    PdfPTable parentTbl=new PdfPTable(1);
		parentTbl.setWidthPercentage(100);   
		
		parentTbl.addCell(mycomCell);     

		try {
			document.add(parentTbl);  
		} catch (Exception e)
		{
			e.printStackTrace();
		}
}

private void createCustomerInfo()   
  {   
	 Date today = new Date();
	 String date = fmt1.format(today);
	 System.out.println("date is:" +date);
	
	 ////////////////////////////// 4 digit card No logic added by Ajinkya on Date: 03/06/2017  /////////////////////////
	 
//	 int fourdigitCardNo = ;
	 String strfrdigitCardNo = ""+ser.getContractCount()%10000 ; 
	 
	 if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==2){
		 strfrdigitCardNo = "00"+strfrdigitCardNo;
	 }
	 else if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==3){
		 strfrdigitCardNo = "0"+strfrdigitCardNo; 
	 }
	 System.out.println("4 digit " +strfrdigitCardNo);
	 
//////////////////////////////4 digit card No logic end Here //////////////////////////
	 
	Phrase cardNo = new Phrase ("Card No. : ",font10bold);
	String contractCardNo = ser.getProduct().getProductCode()+"/"+ strfrdigitCardNo;
	Phrase cardNoVal = new Phrase (contractCardNo,font10);          // ser.getRefNo2();  
    Phrase currDate = new Phrase( "Date : "+date ,font10);   
    Phrase Letterendphrs = new Phrase ("Yours faithfully",font10);
	
    String salutation = "";
	if (cust.getSalutation() != null && !cust.getSalutation().equals(""))
	{
		System.out.println("salutation " + cust.getSalutation());

		if (cust.getSalutation().equalsIgnoreCase("Mr.")) {
			System.out.println("In side MR salutation condition ");
			salutation = "Dear Sir,";
		} else if (cust.getSalutation().equalsIgnoreCase("Ms.")) {
			System.out.println("In side MS salutation condition ");
			salutation = "Dear Madam,";
		} else {
			System.out.println("In side  else1 salutation condition ");
			salutation = "Dear Sir / Madam,";
		}
	} else {
		System.out.println("In side  else2 salutation condition ");
		salutation = "Dear Sir / Madam,";
	}
	
	 Phrase salutationphrse = new Phrase(salutation,font10bold);
	 Phrase paragraphLine1 = new Phrase("We propose to carry out treatment of your premises on ",font10);
	
	String premises = "";
	 if (ser.getPremises()!=null && !ser.getPremises().equals(""))
	 {
		 premises = ser.getPremises();
	    System.out.println("INsidepremises"+ser.getPremises());
	 }  
	 else {
		       premises = "";
//		    System.out.println("INsidepremises"+ser.getPremises());
		 }  
	 Phrase paragraphLine2 = new Phrase(premises,font10);
	 Paragraph infoPara1 = new  Paragraph ();
	 
	 infoPara1.add(cardNo);    
	 infoPara1.add(cardNoVal);
	 infoPara1.add(Chunk.NEWLINE);
	 infoPara1.add(salutationphrse);
	 infoPara1.add(Chunk.NEWLINE);
	 infoPara1.add(paragraphLine1);
	 infoPara1.add(Chunk.NEWLINE);
	 
	 String dateNtime = " ";
	 System.out.println("outside schedule status ");
	 
	if(ser.getStatus().equals("Rescheduled"))
	{      
		 System.out.println("Inside schedule status ");
		if(ser.getListHistory().size()!=0)
		{
			 System.out.println("Inside schedule date history ");
			 int lastrecord = ser.getListHistory().size();
			 Date scheduleDate = ser.getListHistory().get(lastrecord-1).getResheduleDate();   //
			 String scheduleTime = ser.getListHistory().get(lastrecord-1).getResheduleTime();   //
			 
			  dateNtime =  fmt1.format(scheduleDate) + " At about "+scheduleTime +" ";
			 
			 System.out.println(lastrecord);
			 System.out.println(scheduleDate);
			 System.out.println(scheduleTime);
		}
		else{
			  dateNtime = "                 At about "+ "                   ";  
			
		}
	}
	else{
		 dateNtime = "                 At about "+ "                   ";
	}
	 Phrase dateNtimePhrse = new Phrase(dateNtime,font10);
//     Phrase workInphrs = new  Phrase ("WORK IN ",font10);
	 infoPara1.add(dateNtimePhrse);
	 infoPara1.add(Chunk.NEWLINE);
//	 infoPara1.add(workInphrs);
	 infoPara1.add(paragraphLine2);
	 infoPara1.add(Chunk.NEWLINE);
	 Phrase paragraphLine4 = new Phrase(ser.getProductName().toUpperCase() +"",font10);
	 Phrase paragraphLine6 = new Phrase("In case, any changes are desired , please inform us immediately  ",font10);
	 Phrase paragraphLine7 = new Phrase("Thanking You ",font10);
	 
	 PdfPCell InfoTblCell1 = new PdfPCell();
	 PdfPCell InfoTblCell2 = new PdfPCell();
	
//	 infoPara1.add(Chunk.NEWLINE);
	 infoPara1.add(paragraphLine4);
	 infoPara1.add(Chunk.NEWLINE);
	 infoPara1.add(paragraphLine6);
	 infoPara1.add(Chunk.NEWLINE);
	 infoPara1.add(paragraphLine7);
	 infoPara1.add(Chunk.NEWLINE);
	
	 InfoTblCell1.addElement(infoPara1);
	 InfoTblCell1.setBorder(0);
	 
     Paragraph infoPara2 = new  Paragraph ();
	 
	 infoPara2.add(currDate);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Chunk.NEWLINE);
	 infoPara2.add(Letterendphrs);
	 infoPara2.setAlignment(Element.ALIGN_RIGHT);  
	 
	 InfoTblCell2.addElement(infoPara2);
	 InfoTblCell2.setBorder(0);
	 
	 PdfPTable infoTbl = new PdfPTable(2);
	 infoTbl.setWidthPercentage(100);
	 try {
		 infoTbl.setWidths(new float[] {70 ,30 });
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
			  	
	 infoTbl.addCell(InfoTblCell1);
	 infoTbl.addCell(InfoTblCell2); 
	 
	 PdfPTable companypNameTbl = new PdfPTable(1);
	 companypNameTbl.setWidthPercentage(100);
	 
	 Phrase companypName = new Phrase (""+ comp.getBusinessUnitName(),font10);
	 Paragraph  compNamePara = new Paragraph ();
	 compNamePara.add(companypName);
	 compNamePara.setAlignment(Element.ALIGN_RIGHT);  
	 PdfPCell companyNameCell = new PdfPCell ();
	 
	 companyNameCell.addElement(compNamePara);
	 companyNameCell.addElement(Chunk.NEWLINE);  
	 companyNameCell.addElement(Chunk.NEWLINE);
	 companyNameCell.setBorder(0);
	 companypNameTbl.addCell(companyNameCell);
	 
	 try {
		document.add(infoTbl);
		document.add(companypNameTbl);
	} catch (DocumentException e) {
		
		e.printStackTrace();
	}
}

private void createAddressTbl() 
   {
	PdfPTable companypNameTbl = new PdfPTable(1);
	 companypNameTbl.setWidthPercentage(100);
	 
	 
		
	    String salutation1 = "" ;
		if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
			salutation1 = cust.getSalutation();
		}   
		else{  
			 salutation1 = "" ;
		}
		
		String  serCustName = salutation1 + " "+ ser.getPersonInfo().getFullName();
		
		/*
		 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
		 * Used to take customer name from Uppercase to Only first letter Uppercase
		 */
		String nameInFirstLetterUpperCase1 = getFirstLetterUpperCase(serCustName.trim());
                           ////////////////////// end here ///////////////// 
	
	
	Phrase clientNamePhrase = new Phrase ("Attn. :- "+ nameInFirstLetterUpperCase1 ,font10);
	PdfPCell clientNameCell = new PdfPCell(clientNamePhrase);
	clientNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	clientNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	clientNameCell.setBorder(0);
	companypNameTbl.addCell(clientNameCell);
	
	try {
		document.add(companypNameTbl);
	} catch (DocumentException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	PdfPTable addressTbl = new PdfPTable(2);
	addressTbl.setWidthPercentage(100);
	 try {
		 addressTbl.setWidths(new float[] {50 ,50 });
			
		} catch (Exception e1) {
			e1.printStackTrace();  
		}
	
//////////////////////////////////////////////////Added Billing  Address ///////////////////////////////////////
	String addressline1="";
		
	if(cust.getAdress().getAddrLine2().equals(null)&& cust.getAdress().getAddrLine2().equals("")){
		addressline1=cust.getAdress().getAddrLine1()+", "+cust.getAdress().getAddrLine2()+", "+"\n";
	}
	else{
		addressline1=cust.getAdress().getAddrLine1()+ ", "+"\n";
	}
   
	String locality = "";
	if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
		System.out.println("inside both not null condition1");
		locality= (cust.getAdress().getLandmark()+", "+cust.getAdress().getLocality()+", "+"\n"+cust.getAdress().getCity()+" - "
			      +cust.getAdress().getPin()+". "+"\n" +cust.getAdress().getState()+", "+cust.getAdress().getCountry()+".");
	}
	else if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
		System.out.println("inside both null condition 2");
		locality= (cust.getAdress().getLandmark()+", "+"\n"+cust.getAdress().getCity()+" - "
			      +cust.getAdress().getPin()+". "+"\n"+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
	
	else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
		System.out.println("inside both null condition 3");
		locality= (cust.getAdress().getLocality()+", "+"\n"+cust.getAdress().getCity()+" - "
			      +cust.getAdress().getPin()+". "+"\n"+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
	else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
		System.out.println("inside both null condition 4");
		locality=(cust.getAdress().getCity()+" - "+cust.getAdress().getPin()+". "+"\n"+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
    
	 Phrase billAdrsLbl = new Phrase ("Billing Address :- ",font10);
	 Phrase billAdrs = new Phrase ( addressline1 + locality,font10);
	 PdfPCell billAdrsCell = new PdfPCell();
	 
		
	    String salutation2 = "" ;
		if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
			salutation2 = cust.getSalutation();
		}   
		else{  
			 salutation2 = "" ;
		}
		
	 
	 String billAdrsclientNme = salutation2 +" "+ser.getPersonInfo().getFullName();
     String billclientName = getFirstLetterUpperCase( billAdrsclientNme.trim());
	 Phrase billAdrsclientName = new Phrase( billclientName,font10);
	
	 billAdrsCell.addElement(billAdrsLbl);
	 billAdrsCell.addElement(Chunk.NEWLINE);
	 billAdrsCell.addElement(billAdrsclientName);     // clientName  
	 billAdrsCell.addElement(billAdrs); 
	 billAdrsCell.addElement(Chunk.NEWLINE); 
	 billAdrsCell.addElement(Chunk.NEWLINE); 
	 billAdrsCell.addElement(Chunk.NEWLINE); 
	 billAdrsCell.addElement(Chunk.NEWLINE); 
	 billAdrsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 billAdrsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 
	 billAdrsCell.setBorder(0);
	 addressTbl.addCell(billAdrsCell);
	 
	 //////////////////////////////////////////////////Added service Address ///////////////////////////////////////
		String addressline="";
		
		if(ser.getAddress().getAddrLine2().equals(null)&& ser.getAddress().getAddrLine2().equals(""))
		{
			addressline=ser.getAddress().getAddrLine1()+", "+ser.getAddress().getAddrLine2()+", ";
		}
		else
		{
			addressline=ser.getAddress().getAddrLine1()+", ";
		}
	   
		String localityline=null;  
		
		if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false))
		{
			System.out.println("inside both null condition1");
			localityline= "\n" + ser.getAddress().getLandmark()+", "+ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
				      +ser.getAddress().getPin()+". "+"\n" +ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ";
		}
		else if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			localityline= "\n" + ser.getAddress().getLandmark()+", "+"\n"+ser.getAddress().getCity()+" - "
				      +ser.getAddress().getPin()+". "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ";
		}
		
		else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			localityline= "\n" + ser.getAddress().getLocality()+", "+"\n"+ser.getAddress().getCity()+" - "
				      +ser.getAddress().getPin()+". "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ";
		}   
		else if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			localityline= "\n" + ser.getAddress().getCity()+" - "
				      +ser.getAddress().getPin()+". "+"\n"+ser.getAddress().getState()+", "+ser.getAddress().getCountry()+". ";
		}
	    
		 Phrase serAdrsLbl = new Phrase ("Work At :- ",font10bold);
		 Phrase serAdrs = new Phrase (addressline + localityline,font10);
		 PdfPCell serAdrsCell = new PdfPCell();
		 
		   String salutation3 = "" ;
			if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
				salutation3 = cust.getSalutation();
			}   
			else{  
				 salutation3 = "" ;
			}
		 
		 String serAdrsclientNme = salutation3+ " " +ser.getPersonInfo().getFullName();
		 String  clientname =  getFirstLetterUpperCase( serAdrsclientNme.trim()); 
		 Phrase serAdrsclientName = new Phrase( clientname,font10);
		
		 serAdrsCell.addElement(serAdrsLbl);
		 serAdrsCell.addElement(Chunk.NEWLINE);
		 serAdrsCell.addElement(serAdrsclientName);     
		 serAdrsCell.addElement(serAdrs); 
		 serAdrsCell.addElement(Chunk.NEWLINE);   
		 serAdrsCell.addElement(Chunk.NEWLINE); 
		 serAdrsCell.addElement(Chunk.NEWLINE); 
		 serAdrsCell.addElement(Chunk.NEWLINE); 
		 serAdrsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		 serAdrsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 serAdrsCell.setBorder(0);   
		 addressTbl.addCell(serAdrsCell);
		 
		 PdfPTable noteTbl = new PdfPTable(1);
		 noteTbl.setWidthPercentage(100);
		 
		 PdfPCell noteCell = new PdfPCell();
		 Phrase note1 = new Phrase ("Kindly Note In Order To Carry Out This Service And Further Services Of the "
		                           +"Contract, Kindly Clear Any Pending Invoices For This Contract Immediately. Kindly "
		                           +"Ignore If Already Paid. ",font10bold );
		 Phrase note2 = new Phrase ( " This Is A Computerized Print Out And Does Not Require Any Signature. ",font10bold );
		 
		 noteCell.addElement(note1);
		 noteCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		 noteCell.addElement(Chunk.NEWLINE);
		 noteCell.addElement(Chunk.NEWLINE);
		 noteCell.addElement(note2);
		 noteCell.setBorder(0);
		 noteTbl.addCell(noteCell);
		 
		 try {
		document.add(addressTbl);
		document.add(noteTbl);
	} catch (DocumentException e) {
		
		e.printStackTrace();
	}
   }


//Ajinkya added this code to convert customer name in CamelCase    
//  Date : 12/4/2017

  private String getFirstLetterUpperCase(String customerFullName) {
  	
  	String customerName="";
  	String[] customerNameSpaceSpilt=customerFullName.split(" ");
  	int count=0;
  	for (String name : customerNameSpaceSpilt) {
  		String nameLowerCase=name.toLowerCase();
  		if(count==0){
  			customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
  		}else{
  			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
  		}
  		count=count+1;  
  	}
  	return customerName;    

  	
  }

}
