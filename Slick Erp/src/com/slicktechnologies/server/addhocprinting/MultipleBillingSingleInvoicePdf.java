package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.paymentinfo.billingdetails.BillingDocumentTable;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class MultipleBillingSingleInvoicePdf {
	Logger logger = Logger.getLogger("ServiceGSTInvoice.class");
	List<SalesLineItem> products;
	ArrayList<String> dataStringList;
	ArrayList<Integer> alignmentList;
	ArrayList<Chunk> chunkList;
	ArrayList<Font> fontList;
	// ArrayList<SuperProduct> stringlis;
	int firstBreakPoint = 17;
	float blankLines;

	float[] columnMoreLeftWidths = { 2f, 1f };
	float[] columnMoreLeftHeaderWidths = { 1.7f, 1.3f };

	float[] columnMoreRightWidths = { 0.8f, 2.2f };
	float[] columnMoreRightCheckBoxWidths = { 0.3f, 2.7f };
	float[] columnHalfWidth = { 1f, 1f };
	float[] columnHalfInnerWidth = { 0.65f, 1f };

	float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
	float[] columnCollon6Width = { 1.8f, 0.2f, 7.5f, 1.8f, 0.2f, 7.5f };

	float[] columnCollonGSTWidth = { 0.8f, 0.2f, 1.5f };
	float[] columnStateCodeCollonWidth = { 3.5f, 2f, 0.2f, 1f };
	float[] columnContractPeriodDateCodeCollonWidth = { 3f, 1.5f, 0.2f, 2f };
	float[] columnDateCollonWidth = { 1.5f, 0.2f, 1.2f };

	float[] column6RowCollonWidth = { 0.5f, 0.2f, 1.5f, 0.8f, 0.2f, 0.5f };
	float[] column16CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.15f, 0.25f, 0.3f };
	float[] columnrohanrrCollonWidth = { 2.8f, 0.2f, 7.7f };

	// float[] column10ProdCollonWidth = { 0.1f, 0.52f, 0.2f, 0.38f, 0.15f,
	// 0.15f,
	// 0.2f, 0.25f, 0.15f, 0.25f };
	// float[] column8ProdCollonWidth = { 0.1f, 0.92f, 0.2f, 0.38f, 0.15f, 0.2f,
	// 0.15f, 0.25f };
	float[] column7ProdCollonWidth = { 0.1f, 1.08f, 0.2f, 0.38f, 0.2f, 0.15f,
			0.25f };

	float[] column8SerProdCollonWidth = { 0.1f, 0.92f, 0.16f, 0.2f, 0.38f,
			0.2f, 0.15f, 0.25f };
	float[] column8ProdCollonWidth = { 0.1f, 1.08f, 0.2f, 0.19f, 0.19f, 0.2f,
			0.15f, 0.25f };
	float[] column9ServProdCollonWidth = { 0.1f, 1f, 0.08f, 0.2f, 0.19f, 0.19f,
			0.2f, 0.15f, 0.25f };
	float[] column4ProdCollonWidth = { 1.75f, 0.2f, 0.15f, 0.25f };

	float[] column2ProdCollonWidth = { 1.9f, 0.25f };
	float[] column3ProdCollonWidth = { 1f, 0.9f, 0.4f };

	float[] column13CollonWidth = { 0.1f, 0.4f, 0.2f, 0.15f, 0.15f, 0.3f, 0.3f,
			0.15f, 0.25f, 0.4f, 0.4f, 0.4f, 0.3f };
	// {Sr No,Services,HSN ACS,UOM,Qty,Rate,Amount,Disc,Taxable
	// amt,CGST,SGST,IGST,Total}

	/**
	 * rohan added this flag for universal pest control This is used to print
	 * vat no and other article information
	 */

	Boolean UniversalFlag = false;
	/**
	 * ends here
	 */
	/**
	 * Manisha added this to change the date for single service
	 */
	Boolean isSingleService = false;
	/** End **/

	public Document document;

	Invoice invoiceentity;
	List<BillingDocumentDetails> billingDoc;
	ProcessConfiguration processConfig;
	Customer cust;
	Company comp;
	Contract con;

	List<ContractCharges> billingTaxesLis; // ajinkya added this 03/07/2017
	List<CustomerBranchDetails> custbranchlist;
	List<CustomerBranchDetails> customerbranchlist;
	SimpleDateFormat sdf;
	DecimalFormat df = new DecimalFormat("0.00");
	boolean upcflag = false;
	boolean onlyForFriendsPestControl = false;
	boolean authOnLeft = false;
	boolean isPlaneFormat = false;
	/**
	 * Developer : Jayshree Date : 21 Nov 2017 Description : Added this to check
	 * serviceSchedulingList needed or not
	 */
	boolean serviceSchedulelistFlag = false;
	/**
	 * Rohan added this for Universal pest for printing
	 */
	Boolean multipleCompanyName = false;
	Boolean printPremiseDetails = false;

	/**
	 * ends here
	 */

	List<PaymentTerms> payTermsLis;

	List<State> stateList;

	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	/**
	 * Date 9/12/2017 Dev.By Jayshree Des.To increses the Font size Changes are
	 * done
	 */
	Font font13 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font13bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	// End By jayshree
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	Phrase blankCell = new Phrase(" ", font10);

	/* Total Amount */
	double totalAmount;
	/**
	 * Added By Rahul Verma Max Lines which can be used between products and 50
	 * characters of service Name in product Table represents on Line
	 * */

	int noOfLines = 16;
	/**
	 * This is where lines breaks
	 */
	int prouductCount = 0;
	/**
	 * Date 27-07-2017 By ANIL This flag is used to check whether to print
	 * product description on pdf or not.
	 */
	boolean productDescFlag = false;
	/**
	 * Date 28 Sept 2017 By Rahul This flag is used to check whether to print
	 * product description on pdf or not.
	 */
	boolean printAttnInPdf = false;
	boolean consolidatePrice = false;
	/* Added By Rahul Verma on Date 28 Aug 2017 */
	CompanyPayment comppayment;
	private PdfPCell imageSignCell;

	/**
	 * Date 12/1/2018 Dev By Jayshree; 1)To Check the process congigration for
	 * company email and Branch email 2)To Check the process congigration for
	 * company address and Branch Address
	 */
	boolean checkEmailId = false;
	/**
	 * Ends For Jayshree
	 */

	/**
	 * Date 15/1/2018 By jayshree dev.To check the process config for company
	 * heading detail
	 */
	boolean checkheaderLeft = false;
	boolean checkheaderRight = false;

	boolean invoiceGroupAsSignatory=false;
	// End By Jayshree

	// PdfPCell premiseTblCell = new PdfPCell();

	 
	/**
	 * @author Anil @since 01-10-2021
	 * added print bank details flag and default value for this flag will be true
	 * requirement raised by Rahul Tiwar and Nitin Sir
	 */
//	boolean printBankDetailsFlag=true;
	PdfUtility pdfUtility=new PdfUtility();
	Branch branchDt = null;
	
	public void setInvoice(Long count) {
		// Load Invoice
		invoiceentity = ofy().load().type(Invoice.class).id(count).now();
		//
		// billingDoc=invoiceentity.getArrayBillingDocument();
		// invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
		// arrPayTerms=invoiceentity.getArrPayTerms();
		// Load Customer

		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		// Load Company
		if (invoiceentity.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
       /*
        *Date:25-01-2018
        * Added by Ashwini
        * Desc:To load contract id accoding to order id present in billing information to provide contraract 
        * duratrion against service provided for particular contract id
        */
		List<Integer>countList = new ArrayList<Integer>();
		
		List<BillingDocumentDetails>billlist=invoiceentity.getArrayBillingDocument();
		
		System.out.println("billlist::::"+billlist);
        for(BillingDocumentDetails billdocDetail : billlist ){
        	System.out.println("count::"+billdocDetail.getOrderId());
        	
        	countList.add(billdocDetail.getOrderId());
        	System.out.println("countlist:::"+countList);
        }
		
        
        /*
         * end by Ashwini
         */
        
        if (invoiceentity.getCompanyId() != null)
			
	
			/*
			 * commented by Ashwini
			 */
//			con = ofy().load().type(Contract.class)
//					.filter("companyId", invoiceentity.getCompanyId()).first()
//					.now();
			
			con = ofy().load().type(Contract.class)
			.filter("count IN", countList)
			.filter("companyId", invoiceentity.getCompanyId()).first()
			.now();
		else
			con = ofy().load().type(Contract.class)
					.first()
					.now();

//		payTermsLis = con.getPaymentTermsList();

		if (invoiceentity.getCompanyId() == null)
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount()).list();
		else
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		/****************************** vijay ************************/

		System.out.println("Branch name======"
				+ invoiceentity.getCustomerBranch());
		if (invoiceentity.getCompanyId() == null)
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch()).list();
		else
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		System.out.println("Banch updated====="
				+ invoiceentity.getCustomerBranch());

		if (invoiceentity.getCompanyId() != null) {
			/**
			 * @author Anil @since 02-10-2021
			 * Payment mode from invoice was not considered 
			 * issues raised by Rahul Tiwari
			 */
			if(invoiceentity.getPaymentMode()!=null && invoiceentity.getPaymentMode().trim().length()>0){
				List<String> paymentDt = Arrays.asList(invoiceentity.getPaymentMode().trim().split("/"));
				if(paymentDt.get(0).trim().matches("[0-9]+")){
					int payId = Integer.parseInt(paymentDt.get(0).trim());
					comppayment = ofy().load().type(CompanyPayment.class)
							.filter("count", payId)
							.filter("companyId", invoiceentity.getCompanyId()).first()
							.now();
				}
			}else{
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("paymentDefault", true)
						.filter("companyId", invoiceentity.getCompanyId()).first()
						.now();
			}
		}
		/****************************** vijay ************************/

		stateList = ofy().load().type(State.class)
				.filter("companyId", invoiceentity.getCompanyId()).list();

		/************************************ Letter Head Flag *******************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("OnlyForFriendsPestControl")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						onlyForFriendsPestControl = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("AuthorityOnLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						authOnLeft = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("PrintAttnInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printAttnInPdf = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ConsolidatePrice")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						consolidatePrice = true;
					}
					// By jayshree Date 15/11/2017
					// changes are made for process configration for service
					// schedule list
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ServiceScheduleList")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						serviceSchedulelistFlag = true;
					}
					// ends for Jayshree

					/**
					 * Date 12/1/2018 Dev.By jayshree 1)Des.To check the branch
					 * and company mail id 2)Des.To check the branch and company
					 * mail Address
					 */

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}

					// End For Jayshree

					/**
					 * Date 15/1/2018 Dev.By jayshree Des.To check the company
					 * heading alingment
					 */

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtLeft")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderLeft = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HeaderAtRight")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkheaderRight = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("InvoiceGroupAsSignatory")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						invoiceGroupAsSignatory = true;
					}

					// End By Jayshree
				}
			}
		}

		branchDt = ofy().load().type(Branch.class).filter("companyId",invoiceentity.getCompanyId()).filter("buisnessUnitName", invoiceentity.getBranch()).first().now();
	}

	public void createPdf(String preprintStatus) {

		sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		// /////////////////
		if (upcflag == false && preprintStatus.equals("plane")) {
			// createLogo(document, comp);
			createHeader();
			isPlaneFormat = true;
			// createCompanyAddress();
		} else {
			isPlaneFormat = false;
			if (preprintStatus.equals("yes")) {
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			if (preprintStatus.equals("no")) {
				if (comp.getUploadHeader() != null) {
					createCompanyNameAsHeader(document, comp);
				}

				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
				createBlankforUPC();
			}
		}

		// /////////////////
		// createHeader();
		// createStaticHeader(preprintStatus);
		double discount = 0, roundOff = 0;
		discount = invoiceentity.getDiscountAmt();
		roundOff = invoiceentity.getDiscount();
		if (discount != 0) {
			noOfLines = noOfLines - 1;
		}
		if (roundOff != 0) {
			noOfLines = noOfLines - 1;
		}
		if (invoiceentity.getOtherCharges().size() > 0) {
			noOfLines = noOfLines - 1;
		}
		createInvoiceDetails();
		createCustomerDetails();
		if (con.isContractRate()) {
			createProductDetailsForRate();
		} else {
			createProductDetails();
		}

		if (con.isContractRate()) {
			createProductDetailsValForRate();
		} else {
			createProductDetailsVal();
		}
		createFooterAmountPart();
		if (discount != 0) {
			createFooterDisCountAfterPart(discount);
		}
		if (invoiceentity.getOtherCharges().size() > 0) {
			createFooterOtherChargesPart2();
		}
		createFooterTaxPart();
		if (roundOff != 0) {
			createFooterDisCountBeforeNetPayPart(roundOff);
		}
		createFooterAmountInWords_NetPayPart();
		createTermsAndCondition();
		createFooterLastPart(preprintStatus);
		createProductDescription();
		// Date 15/11/2017 By jayshree to call the method
		// createServiceSchedulelis
		if (serviceSchedulelistFlag == true) {
//			createServiceScheduleList();
		}
		// End

	}

	/**
	 * Developer : Jayshree Date: 15 Nov 2017 Description : This method is to
	 * add the service schedule list in invoice on next Page
	 * 
	 */
	private void createServiceScheduleList() {

		Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
		try {
			document.add(nextpage);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (upcflag == true) {
			Paragraph blank = new Paragraph();
			blank.add(Chunk.NEWLINE);
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		String terms = "Service Details :";
		Phrase term = new Phrase(terms, font12bold);

		Paragraph para1 = new Paragraph();
		para1.add(term);
		para1.setAlignment(Element.ALIGN_CENTER);

		String decsInfo = "";

		if (invoiceentity.getComment() != null) {
			decsInfo = invoiceentity.getComment();
		}

		Phrase desphase = new Phrase(decsInfo, font8);
		Paragraph para2 = new Paragraph();
		para2.add(desphase);

		try {
			document.add(para1);
			document.add(para2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// addServiceDetail();
		// By jayshree Date 15/11/2017
		// changes are made for to load the super product

		String desc = "";
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			ServiceProduct sup = ofy()
					.load()
					.type(ServiceProduct.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("productCode",
							invoiceentity.getSalesOrderProducts().get(i)
									.getProdCode().trim()).first().now();

			Phrase service = new Phrase("Service Product :"
					+ sup.getProductName(), font12bold);

			Paragraph servicePara = new Paragraph();
			servicePara.add(service);

			desc = sup.getComment();
			Phrase prodDesc = new Phrase(desc, font8);
			Paragraph parades = new Paragraph(prodDesc);

			PdfPTable serviceTable = new PdfPTable(4);
			serviceTable.setWidthPercentage(100);

			Phrase serno = new Phrase("Service No", font6bold);
			Phrase serDate = new Phrase("Service Date", font6bold);
			Phrase serStatus = new Phrase("Service Status", font6bold);
			Phrase custBranch = new Phrase("Customer Branch", font6bold);

			PdfPCell sernocell = new PdfPCell(serno);
			PdfPCell serDatecell = new PdfPCell(serDate);
			PdfPCell serStatuscell = new PdfPCell(serStatus);
			PdfPCell custBranchcell = new PdfPCell(custBranch);

			serviceTable.addCell(sernocell);
			serviceTable.addCell(serDatecell);
			serviceTable.addCell(serStatuscell);
			serviceTable.addCell(custBranchcell);
			serviceTable.setSpacingBefore(10f);

			Phrase chunk = null;

			System.out.println("service Schedule list size"
					+ con.getServiceScheduleList().size());

			for (int k = 0; k < con.getServiceScheduleList().size(); k++) {

				if (sup.getCount() == con.getServiceScheduleList().get(k)
						.getScheduleProdId()) {
					chunk = new Phrase(con.getServiceScheduleList().get(k)
							.getScheduleServiceNo()
							+ "", font8);
					PdfPCell srno = new PdfPCell(chunk);

					String serviceDt = sdf.format(con.getServiceScheduleList()
							.get(k).getScheduleServiceDate());
					chunk = new Phrase(serviceDt, font8);
					PdfPCell srDate = new PdfPCell(chunk);

					chunk = new Phrase("Scheduled", font8);
					PdfPCell srStatus = new PdfPCell(chunk);

					chunk = new Phrase(con.getServiceScheduleList().get(k)
							.getScheduleProBranch(), font8);
					PdfPCell srBranch = new PdfPCell(chunk);

					serviceTable.addCell(srno);
					serviceTable.addCell(srDate);
					serviceTable.addCell(srStatus);
					serviceTable.addCell(srBranch);
				}
			}

			try {
				document.add(servicePara);
				document.add(parades);
				document.add(serviceTable);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * Ends for Jayshree
	 */

	private void createProductDetailsValForRate() {

		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			rateAmountProd = rateAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice();

			amountAmountProd = amountAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmountProd = discAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmountProd = totalAssAmountProd + taxValue;
		}

		int firstBreakPoint = 5;
		float blankLines = 0;

		PdfPTable productTable = new PdfPTable(8);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column8ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int countToBeDeducted = 0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			if (noOfLines == 0) {
				prouductCount = i;
				break;
			}
			countToBeDeducted = countToBeDeducted + 1;
			noOfLines = noOfLines - 1;

			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font6);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);
			System.out.println("getProdName().trim().length()"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getProdName().trim().length());
			if (invoiceentity.getSalesOrderProducts().get(i).getProdName()
					.trim().length() > 42) {
				noOfLines = noOfLines - 1;
			}
			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(), font6);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.addElement();
			productTable.addCell(serviceNameCell);

			
			/**
			 * Date 21-01-2019 by Vijay for sales and service bill merged invoice.
			 * Des :- for checking item product or Service product if service product then will get number of HSN Code
			 * else if the product is item product then will print SAC Code
			 */
			boolean isServiceProductFlag = false;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct() instanceof ServiceProduct){
				isServiceProductFlag = true;
			}
			Phrase hsnCode = null;
			if(isServiceProductFlag){
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				} else {
					hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}
			}
			else{
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						
						hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				}
				else{
					hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}
			}
			/**
			 * ends here
			 */		

			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			productTable.addCell(hsnCodeCell);
			String startDateStr = "", endDateStr = "";
			
			/**
			 * Date 21-01-2019 below code is not using in rate contract pdf so commented the code
			 */
//			for (int j = 0; j < con.getItems().size(); j++) {
//				SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
//						"dd/MM/yyyy");
//				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//				simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
//				if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
//						.getCount() == con.getItems().get(j).getPrduct()
//						.getCount())
//						&& (invoiceentity.getSalesOrderProducts().get(i)
//								.getOrderDuration() == con.getItems().get(j)
//								.getDuration())) {
//					if(invoiceentity.getSalesOrderProducts().get(i)
//							.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName().trim())){
//					if (con.getItems().get(j).getEndDate() != null) {
//						startDateStr = simpleDateFmt.format(con.getItems()
//								.get(j).getStartDate());
//						endDateStr = simpleDateFmt.format(con.getItems().get(j)
//								.getEndDate());
//					} else {
//						Calendar c = Calendar.getInstance();
//						c.setTime(con.getItems().get(j).getStartDate());
//						c.add(Calendar.DATE, con.getItems().get(j)
//								.getDuration());
//						Date endDt = c.getTime();
//						startDateStr = simpleDateFmt.format(con.getItems()
//								.get(j).getStartDate());
//						endDateStr = simpleDateFmt.format(endDt);
//					}
//				}
//				}
//			}

			Phrase startDate_endDate = new Phrase(startDateStr + " - "
					+ endDateStr, font6);

			PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
			startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(startDate_endDateCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getArea()
					+ "", font6);
			PdfPCell qtyCell = new PdfPCell(qty);
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(qtyCell);

			Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getUnitOfMeasurement().trim(), font6);
			PdfPCell uomCell = new PdfPCell(uom);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(uomCell);
 
			Phrase rate = null;
			PdfPCell rateCell = null;
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					rate = new Phrase(df.format(rateAmountProd) + "", font6);
					rateCell = new PdfPCell(rate);
					if(invoiceentity.getSalesOrderProducts().size() > 1)
					rateCell.setBorderWidthBottom(0);
					rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					rate = new Phrase(" ", font6);

					rateCell = new PdfPCell(rate);
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						
						rateCell.setBorderWidthTop(0);
					}else{
						rateCell.setBorderWidthBottom(0);
						rateCell.setBorderWidthTop(0);
					}
					rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					// rateCell.setBorderWidthBottom(0);
					rateCell.setBorderWidthTop(0);

				}
			} else {
				rate = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getPrice())
						+ "", font6);

				rateCell = new PdfPCell(rate);

				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}
			productTable.addCell(rateCell);

			// Phrase rate = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getPrice())
			// + "", font6);
			// PdfPCell rateCell = new PdfPCell(rate);
			// rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(rateCell);
			// amountAmount=amountAmount+invoiceentity.getSalesOrderProducts().get(i)
			// .getPrice()
			// * invoiceentity.getSalesOrderProducts().get(i)
			// .getQuantity();
			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);

			// discAmount=discAmount+invoiceentity
			// .getSalesOrderProducts().get(i).getFlatDiscount();
			Phrase disc = null;
			PdfPCell discCell = null;
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					disc = new Phrase(df.format(discAmountProd) + "", font6);

					discCell = new PdfPCell(disc);
					if(invoiceentity.getSalesOrderProducts().size() > 1)
					discCell.setBorderWidthBottom(0);
					discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					disc = new Phrase(" ", font6);

					discCell = new PdfPCell(disc);
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						
						discCell.setBorderWidthTop(0);
					}else{
						discCell.setBorderWidthBottom(0);
						discCell.setBorderWidthTop(0);
					}
					discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discCell.setBorderWidthTop(0);
				}
			} else {
				disc = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getFlatDiscount())
						+ "", font6);

				discCell = new PdfPCell(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}

			productTable.addCell(discCell);

			Phrase taxableValue;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			PdfPCell taxableValueCell;/* = new PdfPCell(taxableValue); */

		//	if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					taxableValue = new Phrase(df.format(totalAssAmountProd)
							+ "", font6);
					taxableValueCell = new PdfPCell(taxableValue);
					taxableValueCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					if(invoiceentity.getSalesOrderProducts().size() > 1)
					taxableValueCell.setBorderWidthBottom(0);
				} else {
					taxableValue = new Phrase(" ", font6);
					taxableValueCell = new PdfPCell(taxableValue);
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						
						taxableValueCell.setBorderWidthTop(0);
					}else{
						taxableValueCell.setBorderWidthBottom(0);
						taxableValueCell.setBorderWidthTop(0);
					}
					taxableValueCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					// taxableValueCell.setBorderWidthBottom(0);
					taxableValueCell.setBorderWidthTop(0);
				}
			} else {
				taxableValue = new Phrase(df.format(taxValue) + "", font6);
				taxableValueCell = new PdfPCell(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;

			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());

			logger.log(Level.SEVERE, "VAT TAX ::::Config Name"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getTaxConfigName()
					+ "VAT TAx:::Tax Name"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getTaxName()
					+ "Ser TAX ::::Config Name"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getTaxConfigName()
					+ "Ser TAx:::Tax Name"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getTaxName());
			logger.log(Level.SEVERE, "VAT TAX ::::"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage()
					+ "Service Tax::::"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getPercentage());

			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount()) {
					premisesVal = con.getItems().get(j).getPremisesDetails();
				}

			}
			System.out.println("noOfLines in product" + noOfLines);
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					noOfLines = noOfLines - 1;

					Phrase blankValPhrs = new Phrase("", font8);
					PdfPCell premiseCell = new PdfPCell(blankValPhrs);
					premiseCell.setColspan(1);

					productTable.addCell(premiseCell);

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					productTable.addCell(premiseCell);
				}
			} else {

			}
			// boolean
			// vatPercentZero=invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage() != 0;
			// boolean
			// serPercentZero=invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getPercentage() != 0;
			boolean taxPresent = validateTaxes(invoiceentity
					.getSalesOrderProducts().get(i));
			// if (taxPresent) {
			// logger.log(Level.SEVERE,"Inside Tax Applicable");
			//
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			//
			// }else if
			// (invoiceentity.getSalesOrderProducts().get(i).getServiceTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			//
			// } else {
			//
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("CGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// // igstRateValCell.setBorder(0);
			// // igstRateValCell.setBorderWidthBottom(0);
			// // igstRateValCell.setBorderWidthTop(0);
			// // igstRateValCell.setBorderWidthRight(0);
			// // igstRateValCell.addElement(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// // try {
			// // document.add(productTable);
			// // } catch (DocumentException e) {
			// // // TODO Auto-generated catch block
			// // e.printStackTrace();
			// // }
			//
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("SGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			//
			// productTable.addCell(totalCell);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// }
			// }
			//
			// }
			// else {
			// logger.log(Level.SEVERE,"Inside Tax Not Applicable");
			//
			// PdfPCell cell = new PdfPCell(new Phrase("-", font8));
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// Phrase totalPhrase = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getBasePaymentAmount())
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			//
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j).getPrduct()
			// .getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase("Premise Details : "
			// + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// }
		}
		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines != 0) {
			remainingLines = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);

		if (remainingLines != 0) {
			for (int i = 0; i < remainingLines; i++) {
				System.out.println("i::::" + i);
				Phrase blankPhrase = new Phrase(" ", font10);
				PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
				blankPhraseCell.setBorder(0);
				blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
			}
		}
		PdfPCell productTableCell = null;
		int remainingLinesCheck = 0;
		if (noOfLines != 0) {
			remainingLinesCheck = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLinesCheck" + remainingLinesCheck);
		if (noOfLines == 0 && remainingLines != 0) {
			Phrase my = new Phrase("Please Refer Annexure For More Details",
					font9bold);
			productTableCell = new PdfPCell(my);

		} else {
			productTableCell = new PdfPCell(blankCell);
		}

		productTableCell.setBorder(0);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		tab.addCell(productTableCell);
		tab.setSpacingAfter(blankLines);

		// last code for both table to be added in one table

		PdfPCell tab1;
		tab1 = new PdfPCell(productTable);
		// tab1.setBorder(0);

		PdfPCell tab2 = new PdfPCell(tab);
		// tab2.setBorder(0);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		mainTable.addCell(tab2);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createProductDetailsForRate() {
		// TODO Auto-generated method stub

		PdfPTable productTable = new PdfPTable(8);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column8ProdCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase srNophrase = new Phrase("Sr No", font10bold);
		PdfPCell srNoCell = new PdfPCell(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

		/**
		 * Date 21-01-2019 by Vijay For Sales and Service bill merged the this same pdf will call
		 * so added Particulars / Services.
		 */
		Phrase servicePhrase = new Phrase("Particulars / Services", font10bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2

		Phrase hsnCode = new Phrase("HSN/SAC", font10bold);
		PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 3

		Phrase UOMphrase = new Phrase("UOM", font10bold);
		PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 4

		Phrase qtyPhrase = new Phrase("Qty", font10bold);
		PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 5

		Phrase ratePhrase = new Phrase("Rate", font10bold);
		PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 6

		Phrase amountPhrase = new Phrase("Amount", font10bold);
		PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 7

		Phrase dicphrase = new Phrase("Disc", font10bold);
		PdfPCell dicphraseCell = new PdfPCell(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 8

		Phrase taxValPhrase = new Phrase("Amount", font10bold);
		PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 9

		Phrase serviceServDate = new Phrase("Duration", font10bold);
		PdfPCell serviceServDateCell = new PdfPCell(serviceServDate);
		serviceServDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceServDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(hsnCodeCell);
		// productTable.addCell(serviceServDateCell);

		productTable.addCell(qtyPhraseCell);
		productTable.addCell(UOMphraseCell);
		productTable.addCell(ratePhraseCell);
		// productTable.addCell(amountPhraseCell);
		productTable.addCell(dicphraseCell);
		productTable.addCell(taxValPhraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterOtherChargesPart2() {
		// TODO Auto-generated method stub

		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// String amtInWordsVal = "Amount in Words : Rupees "
		// + SalesInvoicePdf.convert(invoiceentity.getNetPayable())
		// + " Only/-";
		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		otherChargesTable.addCell(amtInWordsValCell);

		Phrase otherCharges = new Phrase("Other Charges", font10bold);

		PdfPCell netPayCell = new PdfPCell(otherCharges);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		double totalOtherCharges = 0;
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			totalOtherCharges = totalOtherCharges
					+ invoiceentity.getOtherCharges().get(i).getAmount();
		}
		Phrase netPayVal = new Phrase(totalOtherCharges + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		otherChargesTable.addCell(netPayableCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountBeforeNetPayPart(double roundOff) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Round Off", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(roundOff) + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterDisCountAfterPart(double discount) {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase amtInWordsValphrase = new Phrase("", font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Discount Amt", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(discount) + "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterOtherChargesPart() {
		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");
		PdfPTable otherChargesTable = new PdfPTable(2);
		otherChargesTable.setWidthPercentage(100);
		try {
			otherChargesTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase blank = new Phrase("", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		otherChargesTable.addCell(blankCell);

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// for (int i = 0; i < invoiceentity.getBillingOtherCharges().size();
		// i++) {
		Phrase chargeName, taxes, assVal;
		PdfPCell pCell;
		// if(i==0){
		chargeName = new Phrase("Charge Name", font10bold);
		taxes = new Phrase("Taxes", font10bold);
		assVal = new Phrase("Amt", font10bold);
		pCell = new PdfPCell(chargeName);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(taxes);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(assVal);
		pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		otherCharges.addCell(pCell);
		// }else{
		chargeName = new Phrase("Transport", font10);
		taxes = new Phrase("SGST@9/CGST@9"/*
										 * invoiceentity.getBillingOtherCharges()
										 * .get(i).get()
										 */, font10);
		assVal = new Phrase("100", font10);
		pCell = new PdfPCell(chargeName);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(taxes);
		otherCharges.addCell(pCell);
		pCell = new PdfPCell(assVal);
		otherCharges.addCell(pCell);
		// }

		// }
		PdfPCell leftCell = new PdfPCell(otherCharges);
		otherChargesTable.addCell(leftCell);
		try {
			document.add(otherChargesTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createTermsAndCondition() {

		String friends = "";
		if (onlyForFriendsPestControl) {
			friends = "1.If you are not satisfied with the treatment within 15 days of treatment, free treatment will be provided. \n"
					+ "2.You will receive two reminders for each of your treatments by call, email and by SMS. Our obligation limits to reminders only. \n"
					+ "3.It is essential to avail the treatment within the due dates to validate the warranty. \n"
					+ "4.Contract period cannot be extended for any reason. \n"
					+ "5.Once the due date is over the treatment cannot be carry forwarded to extend the contract period. \n"
					+ "6.It is mandatory to avail the treatment within a period of fifteen days before or after due date.Otherwise the treatment will be considered as lapsed.\n"
					+ "THEREFOR PLEASE INSURE THE SCHEDULE MENTION HERE IS STRICTLY FOLLOWED";
		} else {
			friends = invoiceentity.getComment().trim();
		}

		int remainingLinesForTerms = 5;

		if (friends.length() > (138 * 5)) {
			friends = friends.substring(0, (138 * 5));
		}
		Phrase termNcondVal = new Phrase("Remarks: \n" + friends, font10bold);

		PdfPCell termNcondValCell = new PdfPCell(termNcondVal);
		termNcondValCell.setBorderWidthBottom(0);
		termNcondValCell.setBorderWidthTop(0);
		PdfPTable pdfTable = new PdfPTable(1);
		pdfTable.setWidthPercentage(100);
		pdfTable.addCell(termNcondValCell);

		Phrase blankPhrase = new Phrase(" ", font10bold);
		PdfPCell blank = new PdfPCell(blankPhrase);
		blank.setBorderWidthBottom(0);
		blank.setBorderWidthTop(0);
		remainingLinesForTerms = remainingLinesForTerms
				- (friends.length() / (138));
		System.out.println("remainingLinesForTerms" + remainingLinesForTerms);
		for (int i = 0; i < remainingLinesForTerms; i++) {
			pdfTable.addCell(blank);
		}
		PdfPCell pdfPcell = new PdfPCell(pdfTable);
		pdfPcell.setBorder(0);

		PdfPTable table1 = new PdfPTable(1);
		table1.setWidthPercentage(100);
		table1.addCell(pdfPcell);
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createFooterAmountInWords_NetPayPart() {

		PdfPTable amountTable = new PdfPTable(2);
		amountTable.setWidthPercentage(100);
		try {
			amountTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String amtInWordsVal = "Amount in Words : Rupees "
				+ SalesInvoicePdf.convert(invoiceentity.getNetPayable())
				+ " Only/-";
		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font10bold);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		amountTable.addCell(amtInWordsValCell);

		Phrase netPay = new Phrase("Net Payable", font10bold);

		PdfPCell netPayCell = new PdfPCell(netPay);
		netPayCell.setBorder(0);

		Phrase colon = new Phrase(":", font10bold);

		PdfPCell colonCell = new PdfPCell(colon);

		colonCell.setBorder(0);

		Phrase netPayVal = new Phrase(df.format(invoiceentity.getNetPayable())
				+ "", font10bold);
		PdfPCell netPayValCell = new PdfPCell(netPayVal);
		netPayValCell.setBorderWidthLeft(0);
		netPayValCell.setBorderWidthTop(0);
		netPayValCell.setBorderWidthBottom(0);
		netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable innerRightTable = new PdfPTable(3);
		try {
			innerRightTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		innerRightTable.setWidthPercentage(100);
		innerRightTable.addCell(netPayCell);
		innerRightTable.addCell(colonCell);
		innerRightTable.addCell(netPayValCell);

		PdfPCell netPayableCell = new PdfPCell(innerRightTable);
		// netPayableCell.setBorder(0);

		amountTable.addCell(netPayableCell);
		try {
			document.add(amountTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createFooterAmountPart() {

		double totalAssAmount = 0;
		double rateAmount = 0;
		double amountAmount = 0;
		double discAmount = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			rateAmount = rateAmount
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice();

			amountAmount = amountAmount
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmount = discAmount
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmount = totalAssAmount + taxValue;
		}
		PdfPTable productTable = new PdfPTable(4);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column4ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase totalamount = new Phrase("Total", font10bold);
		PdfPCell totalAmountCell = new PdfPCell(totalamount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountCell);

		Phrase rateValue = new Phrase(df.format(rateAmount), font10);
		PdfPCell rateValueCell = new PdfPCell(rateValue);
		rateValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productTable.addCell(rateValueCell);

		Phrase amountValue = new Phrase(df.format(amountAmount), font10);
		PdfPCell amountValueCell = new PdfPCell(amountValue);
		amountValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// productTable.addCell(amountValueCell);

		Phrase discamount = new Phrase(df.format(discAmount), font10);
		PdfPCell discamountCell = new PdfPCell(discamount);
		discamountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productTable.addCell(discamountCell);

		System.out.println("totalAssAmount:::" + totalAssAmount);
		Phrase totalValamount = new Phrase(df.format(totalAssAmount), font10);
		PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
		totalAmountValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		productTable.addCell(totalAmountValCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createtotalAmount() {

		PdfPTable productTable = new PdfPTable(2);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase totalamount = new Phrase("Total Ass Amount", font10bold);
		PdfPCell totalAmountCell = new PdfPCell(totalamount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountCell);

		Phrase totalValamount = new Phrase(df.format(0.00), font10);
		PdfPCell totalAmountValCell = new PdfPCell(totalValamount);
		totalAmountValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		productTable.addCell(totalAmountValCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	// /////////////////////////////////////////// Ajinkya Code Start Here
	// //////////////////
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,725f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	// ////////////////////////////// Code End Here
	// /////////////////////////////////

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image
		// image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(520f);
		// image1.setAbsolutePosition(40f,40f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	// ///////////////////////// Ajinkya Code end Here ///////////////

	private void createBlankforUPC() {

		Image uncheckedImg = null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(9);

		// Phrase phrAlp=new Phrase("  ALP",font9);
		// Paragraph paraAlp=new Paragraph();
		// paraAlp.setIndentationLeft(10f);
		// paraAlp.add(new Chunk(uncheckedImg, 0, 0, true));
		// paraAlp.add(phrAlp);

		// rohan added this code
		// float[] myWidth = {1,3,20,17,3,30,17,3,20,1};

		PdfPTable mytbale = new PdfPTable(3);
		mytbale.setSpacingAfter(5f);
		mytbale.setWidthPercentage(100f);
		// try {
		// mytbale.setWidths(myWidth);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		Phrase myblank = new Phrase("   ", font10);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase myblankborderZero = new Phrase(" ", font10);
		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat1Phrase = new Phrase("  Original for Receipient", font10);
		Paragraph para1 = new Paragraph();
		para1.setIndentationLeft(10f);
		para1.add(myblank);
		para1.add(new Chunk(uncheckedImg, 0, 0, true));
		para1.add(stat1Phrase);
		para1.setAlignment(Element.ALIGN_MIDDLE);

		PdfPCell stat1PhraseCell = new PdfPCell(para1);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat2Phrase = new Phrase("  Duplicate for Supplier/Transporter",
				font10);
		Paragraph para2 = new Paragraph();
		para2.setIndentationLeft(10f);
		para2.add(new Chunk(uncheckedImg, 0, 0, true));
		para2.add(stat2Phrase);
		para2.setAlignment(Element.ALIGN_CENTER);

		PdfPCell stat2PhraseCell = new PdfPCell(para2);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stat3Phrase = new Phrase("  Triplicate for Supplier", font10);
		Paragraph para3 = new Paragraph();
		para3.setIndentationLeft(10f);
		para3.add(new Chunk(uncheckedImg, 0, 0, true));
		para3.add(stat3Phrase);
		para3.setAlignment(Element.ALIGN_JUSTIFIED);

		PdfPCell stat3PhraseCell = new PdfPCell(para3);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// mytbale.addCell(myblankborderZero);
		// mytbale.addCell(myblankborderZero);
		// mytbale.addCell(stat1PhraseCell);
		// // mytbale.addCell(myblankborderZeroCell);
		// // mytbale.addCell(myblankCell);
		// mytbale.addCell(stat2PhraseCell);
		// // mytbale.addCell(myblankborderZeroCell);
		// // mytbale.addCell(myblankCell);
		// mytbale.addCell(stat3PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);

		// ends here
		String titlepdf = "";

		if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
				.getInvoiceType().trim())
				|| invoiceentity.getInvoiceType().trim()
						.equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf = "Proforma Invoice";
		else
			titlepdf = "Tax Invoice";
		
		/**
		 * @author Anil @since 01-10-2021
		 */
		titlepdf=pdfUtility.getInvoiceTitle(invoiceentity, branchDt, titlepdf);
		logger.log(Level.SEVERE,"After getInvoiceTitle Method : "+titlepdf +" | "+pdfUtility.printBankDetailsFlag);

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(10f);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		/*
		 * Commented by Ashwini
		 */
//		try {
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(mytbale);
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , invoiceentity.getCompanyId())){
			 Paragraph blank1 =new Paragraph();
			    blank1.add(Chunk.NEWLINE);
			    
			
			    
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(mytbale);
				document.add(parent);
				
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}else{
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(mytbale);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
		/*
		 * End by Ashwini
		 */

	}

	private void createLogo(Document doc, Company comp2) {

		// ********************logo for server ********************
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 750f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,785f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

		// try
		// {
		// Image image1=Image.getInstance("images/ultrapest.jpg");
		// image1.scalePercent(20f);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		// }
		// catch(Exception e)
		// {
		// e.printStackTrace();
		// }

	}

	private void createFooterLastPart(String preprintStatus) {
		// TODO Auto-generated method stub
		PdfPTable bottomTable = new PdfPTable(3);
		bottomTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			bottomTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);

		// rohan added this code for universal pest
		if (UniversalFlag) {
			if (con.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {
				if (!preprintStatus.equalsIgnoreCase("Plane")) {
					for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

						if (comp.getArticleTypeDetails().get(i)
								.getArticlePrint().equalsIgnoreCase("Yes")) {

							Phrase articalType = new Phrase(comp
									.getArticleTypeDetails().get(i)
									.getArticleTypeName()
									+ " : "
									+ comp.getArticleTypeDetails().get(i)
											.getArticleTypeValue(), font10bold);
							PdfPCell articalTypeCell = new PdfPCell();
							articalTypeCell.setBorder(0);
							articalTypeCell.addElement(articalType);
							leftTable.addCell(articalTypeCell);
						}
					}
				}
			}
		} else {
			// if (!preprintStatus.equalsIgnoreCase("Plane")) {
			// leftTable.addCell(articalTypeCell);asa
			ServerAppUtility serverApp = new ServerAppUtility();

			String gstin = "", gstinText = "";
			if (comp.getCompanyGSTType().trim()
					.equalsIgnoreCase("GST Applicable")) {
				logger.log(Level.SEVERE, "GST Applicable");
				gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
						.getBranch().trim());
				System.out.println("gstin" + gstin);

			} else {
				logger.log(Level.SEVERE, "GST Not Applicable");
				gstinText = comp.getCompanyGSTTypeText().trim();
				System.out.println("gstinText" + gstinText);
			}

			Phrase articalType2 = null;
			if (!gstin.trim().equals("")) {
				logger.log(Level.SEVERE, "GST Present");
				articalType2 = new Phrase("GSTIN" + " : " + gstin, font10bold);
			} else if (!gstinText.trim().equalsIgnoreCase("")) {
				logger.log(Level.SEVERE, "GST Not Present");
				articalType2 = new Phrase(gstinText, font10bold);
			} else {
				logger.log(Level.SEVERE, "Nothing Present");
				articalType2 = new Phrase("", font10bold);

			}
			/**
			 * Date : 12 march 2018
			 * Developer : Jayshree
			 * Description: No taxes means no gst of company
			 */
				if(invoiceentity.getBillingTaxes().size()==0){
					gstin="";
				}
				if (!gstin.equals("")) {
					PdfPCell articalType2Cell = new PdfPCell(articalType2);
					articalType2Cell.setBorder(0);
					leftTable.addCell(articalType2Cell);
	
					String stateCodeStr = serverApp.getStateOfCompany(comp,
							invoiceentity.getBranch().trim(), stateList);
					Phrase stateCode = new Phrase("State Code" + " : "
							+ stateCodeStr, font10bold);// Date 9/12/2017 By
														// Jayshree To increse the
														// fontsize by one
	
					PdfPCell stateCodeCell = new PdfPCell(stateCode);
					stateCodeCell.setBorder(0);
					leftTable.addCell(stateCodeCell);
				} 
//				else {
//					PdfPCell articalType2Cell = new PdfPCell(articalType2);
//					articalType2Cell.setBorder(0);
//					leftTable.addCell(articalType2Cell);
//				}


			PdfPTable articleTab = new PdfPTable(3);
			articleTab.setWidthPercentage(100);

			try {
				articleTab.setWidths(new float[] { 30, 5, 65 });
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

				// if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
				// .trim().equalsIgnoreCase("GSTIN")) {
				// Phrase articalType = new Phrase(comp
				// .getArticleTypeDetails().get(i)
				// .getArticleTypeName()
				// + " : "
				// + comp.getArticleTypeDetails().get(i)
				// .getArticleTypeValue(), font10bold);
				// PdfPCell articalTypeCell = new PdfPCell(articalType);
				// articalTypeCell.setBorder(0);
				// leftTable.addCell(articalTypeCell);
				//
				// }

				/**
				 * Date 13/12/2017 By Jayshree add the this
				 */

				Phrase typename;
				Phrase typevalue;
				if (comp.getArticleTypeDetails().get(i).getArticlePrint()
						.equalsIgnoreCase("YES")
						&& (comp.getArticleTypeDetails().get(i)
								.getDocumentName().equals("Invoice Details") || comp
								.getArticleTypeDetails().get(i)
								.getDocumentName().equals("ServiceInvoice"))) {

					typename = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName(), font10bold);
					typevalue = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font10bold);

					PdfPCell tymanecell = new PdfPCell();
					tymanecell.addElement(typename);
					tymanecell.setBorder(0);
					tymanecell.setHorizontalAlignment(Element.ALIGN_LEFT);

					Phrase typeblank = new Phrase(":", font10bold);
					PdfPCell typeCell = new PdfPCell(typeblank);
					typeCell.addElement(typeblank);
					typeCell.setBorder(0);
					typeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

					PdfPCell typevalcell = new PdfPCell();
					typevalcell.addElement(typevalue);
					typevalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
					typevalcell.setBorder(0);

					articleTab.addCell(tymanecell);
					articleTab.addCell(typeCell);
					articleTab.addCell(typevalcell);
				}

			}

			PdfPCell articleCell = new PdfPCell(articleTab);
			articleCell.setBorder(0);
			leftTable.addCell(articleCell);

		}

		PdfPCell leftCell = new PdfPCell();
		leftCell.addElement(leftTable);

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable innerRightTable = new PdfPTable(3);
		innerRightTable.setWidthPercentage(100);

		Phrase colon = new Phrase(" :", font10bold);
		PdfPCell colonCell = new PdfPCell();
		colonCell.setBorder(0);
		colonCell.addElement(colon);

		Phrase blank = new Phrase(" ", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);

		String companyname = "";
		if (multipleCompanyName) {
			if (con.getGroup() != null && !con.getGroup().equals("")) {
				companyname = con.getGroup().trim().toUpperCase();
			} else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}

		} else {
			companyname = comp.getBusinessUnitName().trim().toUpperCase();
		}

		if(invoiceGroupAsSignatory){
			companyname=invoiceentity.getInvoiceGroup();
			logger.log(Level.SEVERE,"invoiceentity.getInvoiceGroup()()"+invoiceentity.getInvoiceGroup());
		}
		// ends here

		// rightTable.addCell(rightUpperCell);
		Phrase companyPhrase = new Phrase("For , " + companyname, font10bold);
		PdfPCell companyParaCell = new PdfPCell(companyPhrase);
		companyParaCell.setBorder(0);
		if (authOnLeft) {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			companyParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		rightTable.addCell(companyParaCell);

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			if (authOnLeft) {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			} else {
				imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Image image1=Image.getInstance("images/digisign2copy.png");
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(100f);
		// imageCell=new PdfPCell(image1);
		// imageCell.setBorder(0);
		// if (authOnLeft) {
		// imageCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// } else {
		// imageCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// }
		// }
		// catch(Exception e)
		// {
		// e.printStackTrDayace();
		// }
		if (imageSignCell != null) {
			rightTable.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);
			rightTable.addCell(blank1Cell);

		}
		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		// Paragraph signPara = new Paragraph();
		// signPara.add(signAuth);
		// signPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell signParaCell = new PdfPCell(signAuth);
		// signParaCell.addElement();

		signParaCell.setBorder(0);
		if (authOnLeft) {
			signParaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else {
			signParaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		rightTable.addCell(signParaCell);

		PdfPCell lefttableCell = new PdfPCell(leftTable);
		// lefttableCell.addElement();
		PdfPCell righttableCell = new PdfPCell(rightTable);
		// if(authOnLeft){
		// righttableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// }else{
		// righttableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// }

		PdfPTable middletTable = new PdfPTable(1);
		middletTable.setWidthPercentage(100);
		if (pdfUtility.printBankDetailsFlag) {
			if (comppayment != null) {

				PdfPTable bankDetailsTable = new PdfPTable(1);
				bankDetailsTable.setWidthPercentage(100f);

				String favourOf = "";
				if (comppayment.getPaymentComName() != null
						&& !comppayment.getPaymentComName().equals("")) {
					favourOf = "Cheque should be in favour of '"
							+ comppayment.getPaymentComName() + "'";
				}
				Phrase favouring = new Phrase(favourOf, font8bold);
				PdfPCell favouringCell = new PdfPCell(favouring);
				favouringCell.setBorder(0);
				bankDetailsTable.addCell(favouringCell);

				Phrase heading = new Phrase("Bank Details", font8bold);
				PdfPCell headingCell = new PdfPCell(heading);
				headingCell.setBorder(0);
				bankDetailsTable.addCell(headingCell);

				float[] columnWidths3 = { 1.5f, 0.35f, 4.5f };
				PdfPTable bankDetails3Table = new PdfPTable(3);
				bankDetails3Table.setWidthPercentage(100f);
				try {
					bankDetails3Table.setWidths(columnWidths3);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				Phrase bankNamePh = new Phrase("Name", font8bold);
				PdfPCell bankNamePhCell = new PdfPCell(bankNamePh);
				bankNamePhCell.setBorder(0);
				bankDetails3Table.addCell(bankNamePhCell);

				Phrase dot = new Phrase(":", font8bold);
				PdfPCell dotCell = new PdfPCell(dot);
				dotCell.setBorder(0);
				bankDetails3Table.addCell(dotCell);

				String bankName = "";
				if (comppayment.getPaymentBankName() != null
						&& !comppayment.getPaymentBankName().equals("")) {
					bankName = comppayment.getPaymentBankName();
				}
				Phrase headingValue = new Phrase(bankName, font8);
				PdfPCell headingValueCell = new PdfPCell(headingValue);
				headingValueCell.setBorder(0);
				headingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(headingValueCell);

				// this is for branch
				Phrase bankBranch = new Phrase("Branch", font8bold);
				PdfPCell bankBranchCell = new PdfPCell(bankBranch);
				bankBranchCell.setBorder(0);
				bankDetails3Table.addCell(bankBranchCell);
				bankDetails3Table.addCell(dotCell);

				String bankBranchValue = "";
				if (comppayment.getPaymentBranch() != null
						&& !comppayment.getPaymentBranch().equals("")) {
					bankBranchValue = comppayment.getPaymentBranch();
				}
				Phrase bankBranchValuePh = new Phrase(bankBranchValue, font8);
				PdfPCell bankBranchValuePhCell = new PdfPCell(bankBranchValuePh);
				bankBranchValuePhCell.setBorder(0);
				bankBranchValuePhCell
						.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankBranchValuePhCell);

				Phrase bankAc = new Phrase("A/c No", font8bold);
				PdfPCell bankAcCell = new PdfPCell(bankAc);
				bankAcCell.setBorder(0);
				bankDetails3Table.addCell(bankAcCell);
				bankDetails3Table.addCell(dotCell);

				String bankAcNo = "";
				if (comppayment.getPaymentAccountNo() != null
						&& !comppayment.getPaymentAccountNo().equals("")) {
					bankAcNo = comppayment.getPaymentAccountNo();
				}
				Phrase bankAcNoValue = new Phrase(bankAcNo, font8);
				PdfPCell bankAcNoValueCell = new PdfPCell(bankAcNoValue);
				bankAcNoValueCell.setBorder(0);
				bankAcNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankAcNoValueCell);

				Phrase bankIFSC = new Phrase("IFS Code", font8bold);
				PdfPCell bankIFSCCell = new PdfPCell(bankIFSC);
				bankIFSCCell.setBorder(0);
				bankDetails3Table.addCell(bankIFSCCell);
				bankDetails3Table.addCell(dotCell);

				String bankIFSCNo = "";
				if (comppayment.getPaymentIFSCcode() != null
						&& !comppayment.getPaymentIFSCcode().equals("")) {
					bankIFSCNo = comppayment.getPaymentIFSCcode();
				}
				Phrase bankIFSCNoValue = new Phrase(bankIFSCNo, font8);
				PdfPCell bankIFSCNoValueCell = new PdfPCell(bankIFSCNoValue);
				bankIFSCNoValueCell.setBorder(0);
				bankIFSCNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
				bankDetails3Table.addCell(bankIFSCNoValueCell);

				PdfPCell bankDetails3TableCell = new PdfPCell(bankDetails3Table);
				bankDetails3TableCell.setBorder(0);
				bankDetailsTable.addCell(bankDetails3TableCell);

				PdfPCell bankDetailsTableCell = new PdfPCell(bankDetailsTable);
				bankDetailsTableCell.setBorder(0);
				middletTable.addCell(bankDetailsTableCell);
			}
		}

		PdfPCell middletTableCell = new PdfPCell(middletTable);

		bottomTable.addCell(lefttableCell);
		bottomTable.addCell(middletTableCell);
		bottomTable.addCell(righttableCell);

		//

		Paragraph para = new Paragraph(
				"Note : This is computer generated invoice therefore no physical signature is required.",
				font8);

		try {
			document.add(bottomTable);
			// if(comp.getUploadDigitalSign()!=null){
			if (imageSignCell != null) {
				document.add(para);
			}
			// }
			if (noOfLines == 0 && prouductCount != 0) {
				createAnnexureForRemainingProduct(prouductCount);
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createAnnexureForRemainingProduct(int count) {

		Paragraph para = new Paragraph("Annexure 1 :", font10bold);
		para.setAlignment(Element.ALIGN_LEFT);

		try {
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(Chunk.NEWLINE);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (con.isContractRate()) {
			createProductDetailsForRate();
		} else {
			createProductDetails();
		}
		if (con.isContractRate()) {
			createProductDetailsMOreThanFiveForRate(prouductCount);
		} else {
			createProductDetailsMOreThanFive(prouductCount);
		}

	}

	private void createProductDetailsMOreThanFiveForRate(int count) {

		for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			PdfPTable productTable = new PdfPTable(8);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column8ProdCollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// 1
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);
			// 2
			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(),
					font10);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.addElement();
			productTable.addCell(serviceNameCell);
			// 3
			/**
			 * Date 21-01-2019 by Vijay for sales and service bill merged invoice.
			 * Des :- for checking item product or Service product if service product then will get number of HSN Code
			 * else if the product is item product then will print SAC Code
			 */
			boolean isServiceProductFlag = false;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct() instanceof ServiceProduct){
				isServiceProductFlag = true;
			}
			Phrase hsnCode = null;
			if(isServiceProductFlag){
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				} else {
					hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}
			}
			else{
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						
						hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				}
				else{
					hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}
			}
			/**
			 * ends here
			 */ 
			
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			// hsnCodeCell.addElement();
			productTable.addCell(hsnCodeCell);

			String startDateStr = "", endDateStr = "";
			/**
			 * Date 21-01-2019 By Vijay for rate contract pdf's we are not showing the contract duration so below code commented
			 */
//			for (int j = 0; j < con.getItems().size(); j++) {
//				SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
//						"dd/MM/yyyy");
//				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//				simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
//				if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
//						.getCount() == con.getItems().get(j).getPrduct()
//						.getCount())
//						&& (invoiceentity.getSalesOrderProducts().get(i)
//								.getOrderDuration() == con.getItems().get(j)
//								.getDuration())) {
//					if(invoiceentity.getSalesOrderProducts().get(i)
//							.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName())){
//					if (con.getItems().get(j).getEndDate() != null) {
//						startDateStr = simpleDateFmt.format(con.getItems()
//								.get(j).getStartDate());
//						endDateStr = simpleDateFmt.format(con.getItems().get(j)
//								.getEndDate());
//					} else {
//						Calendar c = Calendar.getInstance();
//						c.setTime(con.getItems().get(j).getStartDate());
//						c.add(Calendar.DATE, con.getItems().get(j)
//								.getDuration());
//						Date endDt = c.getTime();
//						startDateStr = simpleDateFmt.format(con.getItems()
//								.get(j).getStartDate());
//						endDateStr = simpleDateFmt.format(endDt);
//					}
//				}
//			}
//			}
//			Phrase startDate_endDate = new Phrase(startDateStr + " - "
//					+ endDateStr, font6);
//
//			PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
//			startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			// productTable.addCell(startDate_endDateCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getArea()
					+ "", font10);
			PdfPCell qtyCell = new PdfPCell(qty);
			// qtyCell.addElement();
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(qtyCell);

			Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getUnitOfMeasurement().trim(), font10);
			PdfPCell uomCell = new PdfPCell(uom);
			uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(uomCell);

			Phrase rate = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getPrice())
					+ "", font6);
			PdfPCell rateCell = new PdfPCell(rate);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			Phrase blank = new Phrase(" ", font6);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		//	if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == count) {
					blankCell.setBorderWidthBottom(0);
					productTable.addCell(blankCell);
				} else {
					blankCell.setBorderWidthTop(0);
					productTable.addCell(blankCell);
				}
			} else {
				productTable.addCell(rateCell);
			}
			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getFlatDiscount())
					+ "", font6);
			PdfPCell discCell = new PdfPCell(disc);
			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == count) {
					blankCell.setBorderWidthBottom(0);
					productTable.addCell(blankCell);
				} else {
					blankCell.setBorderWidthTop(0);
					productTable.addCell(blankCell);
				}
			} else {
				productTable.addCell(discCell);
			}

			Phrase taxableValue;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBaseBillingAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			// totalAssAmount=totalAssAmount+taxValue;
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == count) {
					blankCell.setBorderWidthBottom(0);
					productTable.addCell(blankCell);
				} else {
					blankCell.setBorderWidthTop(0);
					productTable.addCell(blankCell);
				}
			} else {
				productTable.addCell(taxableValueCell);
			}

			PdfPCell cellIGST;
			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount()) {
					premisesVal = con.getItems().get(j).getPremisesDetails();
				}

			}
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					Phrase blankValPhrs = new Phrase(" ", font8);
					PdfPCell premiseCellBlank = new PdfPCell(blankValPhrs);
					premiseCellBlank.setColspan(1);

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font10);
					PdfPCell premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					productTable.addCell(premiseCell);
				}
			}
			try {
				document.add(productTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage() != 0
			// && invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getPercentage() != 0) {
			// logger.log(Level.SEVERE, "Inside NON Zero:::::");
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getTaxPrintName()
			// .equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else {
			//
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("CGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// // igstRateValCell.setBorder(0);
			// // igstRateValCell.setBorderWidthBottom(0);
			// // igstRateValCell.setBorderWidthTop(0);
			// // igstRateValCell.setBorderWidthRight(0);
			// // igstRateValCell.addElement(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("SGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			//
			// productTable.addCell(totalCell);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
			// }
			// } else {
			// logger.log(Level.SEVERE, "Inside Zero else:::::");
			//
			// PdfPCell cell = new PdfPCell(new Phrase("NA", font10));
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// Phrase totalPhrase = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getTotalAmount())
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			//
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
		}
	}

	private void createProductDetailsMOreThanFive(int count) {

		for (int i = count; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			PdfPTable productTable = new PdfPTable(8);
			productTable.setWidthPercentage(100);
			try {
				productTable.setWidths(column8SerProdCollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// 1
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);
			// 2
			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(),
					font10);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			// serviceNameCell.addElement();
			productTable.addCell(serviceNameCell);

			/**
			 * Date 21-01-2019 by Vijay for sales and service bill merged invoice.
			 * Des :- for checking item product or Service product if service product then will get number of services
			 * else if the product is item product then will get print Qty
			 */
			Phrase noOfServices;
			boolean isServiceProductFlag = false;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct() instanceof ServiceProduct){
				noOfServices = new Phrase(invoiceentity
						.getSalesOrderProducts().get(i).getOrderServices()
						+ "", font10);
				isServiceProductFlag = true;
			}
			else{
				noOfServices = new Phrase(invoiceentity
						.getSalesOrderProducts().get(i).getQuantity()
						+ "", font10);
			}
			
//			Phrase noOfServices = new Phrase(invoiceentity
//					.getSalesOrderProducts().get(i).getOrderServices()
//					+ "", font10);
			/**
			 * ends here
			 */
			PdfPCell noOfServicesCell = new PdfPCell(noOfServices);
			noOfServicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// serviceNameCell.addElement();
			productTable.addCell(noOfServicesCell);
			// 3
			Phrase hsnCode = null;
			/**
			 * Date 21-01-2019 by Vijay for sales and service bill merged invoice.
			 * Des :- for checking item product or Service product if service product then will get number of HSN Code
			 * else if the product is item product then will print SAC Code
			 */
			if(isServiceProductFlag){
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						
						hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				} 
				else{
					hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}
			}
			else {

				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						
						hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				}
				else{
					hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}

			}
			
			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			// hsnCodeCell.addElement();
			productTable.addCell(hsnCodeCell);

			String startDateStr = "", endDateStr = "";
			Phrase startDate_endDate = null;
			/**
			 * Date 21-01-2019 By Vijay for sales and service bill merged invoice.
			 * Des :- if product is service product then from contract it will get contract duration and will print
			 * and else if product is item product the contract duration will print N.A.
			 */
			if(isServiceProductFlag){
				for (int j = 0; j < con.getItems().size(); j++) {
					SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
							"dd/MM/yyyy");
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
					if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
							.getCount() == con.getItems().get(j).getPrduct()
							.getCount())
							&& (invoiceentity.getSalesOrderProducts().get(i)
									.getOrderDuration() == con.getItems().get(j)
									.getDuration())) {
						if(invoiceentity.getSalesOrderProducts().get(i)
								.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName().trim())){
						if (con.getItems().get(j).getEndDate() != null) {
							startDateStr = simpleDateFmt.format(con.getItems()
									.get(j).getStartDate());
							endDateStr = simpleDateFmt.format(con.getItems().get(j)
									.getEndDate());
						} else {
							Calendar c = Calendar.getInstance();
							c.setTime(con.getItems().get(j).getStartDate());
							c.add(Calendar.DATE, con.getItems().get(j)
									.getDuration());
							Date endDt = c.getTime();
							startDateStr = simpleDateFmt.format(con.getItems()
									.get(j).getStartDate());
							endDateStr = simpleDateFmt.format(endDt);
						}
						}
					}
				}
				startDate_endDate = new Phrase(startDateStr + " - "
						+ endDateStr, font6);
			}
			else{
				startDate_endDate = new Phrase("NA",font6);
			}			
			/**
			 * ends here
			 */

			PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
			startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			productTable.addCell(startDate_endDateCell);
			// Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
			// .get(i).getUnitOfMeasurement().trim(), font10);
			// PdfPCell uomCell = new PdfPCell(uom);
			// uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(uomCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getQuantity()
					+ "", font10);
			PdfPCell qtyCell = new PdfPCell(qty);
			// qtyCell.addElement();
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(qtyCell);

			Phrase rate = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getPrice())
					+ "", font6);
			PdfPCell rateCell = new PdfPCell(rate);
			rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(rateCell);

			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);

			Phrase disc = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getFlatDiscount())
					+ "", font6);
			PdfPCell discCell = new PdfPCell(disc);
			discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(discCell);

			Phrase taxableValue;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBasePaymentAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				taxableValue = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getBaseBillingAmount())
						+ "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			// totalAssAmount=totalAssAmount+taxValue;
			PdfPCell taxableValueCell = new PdfPCell(taxableValue);
			taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;
			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				if ((invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount())
						&& (invoiceentity.getSalesOrderProducts().get(i)
								.getOrderDuration() == con.getItems().get(j)
								.getDuration())) {
					if(invoiceentity.getSalesOrderProducts().get(i)
							.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName().trim())){
					premisesVal = con.getItems().get(j).getPremisesDetails();
					}
				}

			}
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {

					Phrase premisesBlank = new Phrase(" ", font10);
					PdfPCell premiseCellBlankCell = new PdfPCell(premisesBlank);
					premiseCellBlankCell.setColspan(1);
					productTable.addCell(premiseCellBlankCell);

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font10);
					PdfPCell premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					productTable.addCell(premiseCell);
				}
			}
			try {
				document.add(productTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage() != 0
			// && invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getPercentage() != 0) {
			// logger.log(Level.SEVERE, "Inside NON Zero:::::");
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getTaxPrintName()
			// .equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// } else {
			//
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("CGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// // igstRateValCell.setBorder(0);
			// // igstRateValCell.setBorderWidthBottom(0);
			// // igstRateValCell.setBorderWidthTop(0);
			// // igstRateValCell.setBorderWidthRight(0);
			// // igstRateValCell.addElement(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			//
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("SGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			//
			// productTable.addCell(totalCell);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(8);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
			// }
			// } else {
			// logger.log(Level.SEVERE, "Inside Zero else:::::");
			//
			// PdfPCell cell = new PdfPCell(new Phrase("NA", font10));
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// Phrase totalPhrase = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getTotalAmount())
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			//
			//
			// try {
			// document.add(productTable);
			// } catch (DocumentException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
		}
	}

	private void createFooterTaxPart() {
		// TODO Auto-generated method stub
		PdfPTable pdfPTaxTable = new PdfPTable(3);
		pdfPTaxTable.setWidthPercentage(100);
		float[] columnThreePartWidths = { 1f, 1f, 1f };
		try {
			pdfPTaxTable.setWidths(columnThreePartWidths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		/**
		 * rohan added this code for payment terms for invoice details
		 */

		float[] column3widths = { 2f, 2f, 6f };
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// heading
		/**
		 * Date 21-01-2019 by Vijay 
		 * Des :- if service 2 or more bills merged and made the invoice then payment terms will print
		 * and else if service and sales 2 or more bills merged then payment terms will not print
		 */
		boolean isthisSalesServiceMergedInvoice = getIsthisSalesServiceBillMerged();
		if(!isthisSalesServiceMergedInvoice){
			Phrase day = new Phrase("Day", font8bold);
			PdfPCell dayCell = new PdfPCell(day);
			dayCell.setBorder(0);

			Phrase percent = new Phrase("Percent", font8bold);
			PdfPCell percentCell = new PdfPCell(percent);
			percentCell.setBorder(0);

			Phrase comment = new Phrase("Comment", font8bold);
			PdfPCell commentCell = new PdfPCell(comment);
			commentCell.setBorder(0);

			// Phrase terms = new Phrase("Payment Terms", font8bold);
			// PdfPCell termsCell = new PdfPCell(terms);
			//
			// Phrase termsBlank = new Phrase(" ", font10bold);
			// PdfPCell termsBlankCell = new PdfPCell(termsBlank);
			// termsBlankCell.setBorder(0);
			// leftTable.addCell(termsCell);
			// leftTable.addCell(termsBlankCell);
			// leftTable.addCell(termsBlankCell);
			leftTable.addCell(dayCell);
			leftTable.addCell(percentCell);
			leftTable.addCell(commentCell);

			// Values
			for (int i = 0; i < invoiceentity.getArrPayTerms().size(); i++) {
				Phrase dayValue = new Phrase(invoiceentity.getArrPayTerms().get(i)
						.getPayTermDays()
						+ "", font8);
				PdfPCell dayValueCell = new PdfPCell(dayValue);
				dayValueCell.setBorder(0);
				leftTable.addCell(dayValueCell);

				Phrase percentValue = new Phrase(df.format(invoiceentity
						.getArrPayTerms().get(i).getPayTermPercent()), font8);
				PdfPCell percentValueCell = new PdfPCell(percentValue);
				percentValueCell.setBorder(0);
				leftTable.addCell(percentValueCell);

				Phrase commentValue = new Phrase(invoiceentity.getArrPayTerms()
						.get(i).getPayTermComment(), font8);
				PdfPCell commentValueCell = new PdfPCell(commentValue);
				commentValueCell.setBorder(0);
				leftTable.addCell(commentValueCell);
			}

			// try {
			// document.add(leftTable);
			// } catch (DocumentException e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// }
		}
	

		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		PdfPTable rightInnerTable = new PdfPTable(3);
		rightInnerTable.setWidthPercentage(100);
		try {
			rightInnerTable.setWidths(columnCollonGSTWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);

		Phrase amtB4Taxphrase = new Phrase("Total Amount Before Tax",
				font10bold);
		PdfPCell amtB4TaxCell = new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);

		Phrase amtB4TaxValphrase = new Phrase(totalAmount + "", font10bold);
		PdfPCell amtB4ValTaxCell = new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);

		double cgstTotalVal = 0, sgstTotalVal = 0, igstTotalVal = 0;
		for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {
			if (invoiceentity.getBillingTaxes().get(i).getTaxChargeName()
					.equalsIgnoreCase("IGST")) {
				igstTotalVal = igstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();
				Phrase IGSTphrase = new Phrase("IGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell IGSTphraseCell = new PdfPCell(IGSTphrase);
				IGSTphraseCell.setBorder(0);

				Phrase IGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell IGSTValphraseCell = new PdfPCell(IGSTValphrase);
				IGSTValphraseCell.setBorder(0);
				IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(IGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(IGSTValphraseCell);

			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("SGST")) {
				sgstTotalVal = sgstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase SGSTphrase = new Phrase("SGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell SGSTphraseCell = new PdfPCell(SGSTphrase);
				SGSTphraseCell.setBorder(0);
				// SGSTphraseCell.addElement(SGSTphrase);

				Phrase SGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell SGSTValphraseCell = new PdfPCell(SGSTValphrase);
				SGSTValphraseCell.setBorder(0);
				SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

				rightInnerTable.addCell(SGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(SGSTValphraseCell);
			} else if (invoiceentity.getBillingTaxes().get(i)
					.getTaxChargeName().equalsIgnoreCase("CGST")) {
				cgstTotalVal = cgstTotalVal
						+ invoiceentity.getBillingTaxes().get(i)
								.getPayableAmt();

				Phrase CGSTphrase = new Phrase("CGST @"
						+ invoiceentity.getBillingTaxes().get(i)
								.getTaxChargePercent() + " %", font10bold);
				PdfPCell CGSTphraseCell = new PdfPCell(CGSTphrase);
				CGSTphraseCell.setBorder(0);
				// CGSTphraseCell.addElement(CGSTphrase);

				Phrase CGSTValphrase = new Phrase(df.format(invoiceentity
						.getBillingTaxes().get(i).getPayableAmt())
						+ "", font10);
				PdfPCell CGSTValphraseCell = new PdfPCell(CGSTValphrase);
				CGSTValphraseCell.setBorder(0);
				CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				// CGSTValphraseCell.addElement(CGSTValphrasePara);

				rightInnerTable.addCell(CGSTphraseCell);
				rightInnerTable.addCell(colonCell);
				rightInnerTable.addCell(CGSTValphraseCell);
			}
		}

		Phrase GSTphrase = new Phrase("Total GST", font10bold);
		PdfPCell GSTphraseCell = new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
		double totalGSTValue = igstTotalVal + cgstTotalVal + cgstTotalVal;
		Phrase GSTValphrase = new Phrase(df.format(totalGSTValue) + "",
				font10bold);
		PdfPCell GSTValphraseCell = new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		rightInnerTable.addCell(GSTphraseCell);
		rightInnerTable.addCell(colonCell);
		rightInnerTable.addCell(GSTValphraseCell);

		PdfPCell innerRightCell = new PdfPCell(rightInnerTable);
		innerRightCell.setBorder(0);
		// innerRightCell.addElement();

		rightTable.addCell(innerRightCell);

		PdfPTable middleTable = new PdfPTable(1);
		middleTable.setWidthPercentage(100);

		// TODO Auto-generated method stub
		System.out.println("Inside Other Chrages");
		// PdfPTable otherChargesTable = new PdfPTable(1);
		// otherChargesTable.setWidthPercentage(100);
		// try {
		// otherChargesTable.setWidths(columnMoreLeftWidths);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// Phrase blank=new Phrase("",font10bold);
		// PdfPCell blankCell=new PdfPCell(blank);
		// otherChargesTable.addCell(blankCell);

		PdfPTable otherCharges = new PdfPTable(3);
		otherCharges.setWidthPercentage(100);
		try {
			otherCharges.setWidths(column3ProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < invoiceentity.getOtherCharges().size(); i++) {
			Phrase chargeName, taxes, assVal;
			PdfPCell pCell;
			if (i == 0) {
				chargeName = new Phrase("Charge Name", font10bold);
				taxes = new Phrase("Taxes", font10bold);
				assVal = new Phrase("Amt", font10bold);
				pCell = new PdfPCell(chargeName);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
				pCell = new PdfPCell(taxes);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
				pCell = new PdfPCell(assVal);
				pCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
				otherCharges.addCell(pCell);
			}

			chargeName = new Phrase(invoiceentity.getOtherCharges().get(i)
					.getOtherChargeName(), font10);
			String taxNames = " ";
			if (invoiceentity.getOtherCharges().get(i).getTax1()
					.getPercentage() != 0
					&& invoiceentity.getOtherCharges().get(i).getTax2()
							.getPercentage() != 0) {
				taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
						.getTaxConfigName()
						+ "/"
						+ invoiceentity.getOtherCharges().get(i).getTax2()
								.getTaxConfigName();
			} else {
				if (invoiceentity.getOtherCharges().get(i).getTax1()
						.getPercentage() != 0) {
					taxNames = invoiceentity.getOtherCharges().get(i).getTax1()
							.getTaxConfigName();
				} else if (invoiceentity.getOtherCharges().get(i).getTax2()
						.getPercentage() != 0) {
					taxNames = invoiceentity.getOtherCharges().get(i).getTax2()
							.getTaxConfigName();
				} else {
					taxNames = " ";
				}
			}
			taxes = new Phrase(taxNames /*
										 * invoiceentity. getOtherCharges()
										 * .get(i).get()
										 */, font10);
			assVal = new Phrase(invoiceentity.getOtherCharges().get(i)
					.getAmount()
					+ "", font10);
			pCell = new PdfPCell(chargeName);
			otherCharges.addCell(pCell);
			pCell = new PdfPCell(taxes);
			otherCharges.addCell(pCell);
			pCell = new PdfPCell(assVal);
			otherCharges.addCell(pCell);

		}
		// PdfPCell left2Cell=new PdfPCell(otherCharges);
		// otherChargesTable.addCell();

		PdfPCell left22Cell = new PdfPCell(otherCharges);
		middleTable.addCell(left22Cell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);
		// rightCell.addElement();
		PdfPCell middleCell = new PdfPCell(middleTable);

		PdfPCell leftCell = new PdfPCell(leftTable);
		// leftCell.setBorder(0);
		// leftCell.addElement();

		pdfPTaxTable.addCell(leftCell);
		if (invoiceentity.getOtherCharges().size() > 0) {
			pdfPTaxTable.addCell(middleCell);
		} else {
			Phrase blankPhrase = new Phrase(" ", font10);
			middleCell = new PdfPCell(blankPhrase);
			pdfPTaxTable.addCell(middleCell);
		}
		pdfPTaxTable.addCell(rightCell);

		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createProductDetailsVal() {
		double rateAmountProd = 0, amountAmountProd = 0, discAmountProd = 0, totalAssAmountProd = 0;

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			rateAmountProd = rateAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice();

			amountAmountProd = amountAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i).getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			discAmountProd = discAmountProd
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getFlatDiscount();

			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0) {
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			totalAssAmountProd = totalAssAmountProd + taxValue;
		}

		int firstBreakPoint = 5;
		float blankLines = 0;

		// if (invoiceentity.getSalesOrderProducts().size() <= firstBreakPoint)
		// {
		// int size = firstBreakPoint
		// - invoiceentity.getSalesOrderProducts().size();
		// blankLines = size * (100 / 5);
		// System.out.println("blankLines size =" + blankLines);
		// } else {
		// blankLines = 10f;d
		// }
		PdfPTable productTable = new PdfPTable(8);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column8SerProdCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int countToBeDeducted = 0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			if (noOfLines == 0) {
				// if((i+1)!=invoiceentity.getSalesOrderProducts().size())
				prouductCount = i;
				break;
			}
			countToBeDeducted = countToBeDeducted + 1;
			noOfLines = noOfLines - 1;

			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font6);
			PdfPCell srNoCell = new PdfPCell(srNo);
			srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(srNoCell);
			System.out.println("getProdName().trim().length()"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getProdName().trim().length());
			if (invoiceentity.getSalesOrderProducts().get(i).getProdName()
					.trim().length() > 42) {
				noOfLines = noOfLines - 1;
			}
			Phrase serviceName = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getProdName().trim(), font6);
			PdfPCell serviceNameCell = new PdfPCell(serviceName);
			productTable.addCell(serviceNameCell);

			/**
			 * Date 21-01-2019 by Vijay for sales and service bill merged invoice.
			 * Des :- for checking item product or Service product if service product then will get number of services
			 * else if the product is item product then will get print Qty
			 */
			Phrase noOfServices;
			boolean isServiceProductFlag = false;
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct() instanceof ServiceProduct){
				noOfServices = new Phrase(invoiceentity
						.getSalesOrderProducts().get(i).getOrderServices()
						+ "", font6);
				isServiceProductFlag = true;
				System.out.println("inside isServiceProductFlag:::::");
			}
			else{
				noOfServices = new Phrase(invoiceentity
						.getSalesOrderProducts().get(i).getQuantity()
						+ "", font6);
				
				System.out.println("not inside isServiceProductFlag::::");
			}
//			Phrase noOfServices = new Phrase(invoiceentity
//					.getSalesOrderProducts().get(i).getOrderServices()
//					+ "", font6);
			/**
			 * ends here
			 */
			
			PdfPCell noOfServicesCell = new PdfPCell(noOfServices);
			noOfServicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			productTable.addCell(noOfServicesCell);

			Phrase hsnCode = null;
			
			/**
			 * Date 21-01-2019 by Vijay for sales and service bill merged invoice.
			 * Des :- for checking item product or Service product if service product then will get number of HSN Code
			 * else if the product is item product then will print SAC Code
			 */
			if(isServiceProductFlag){
				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						
						hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				} 
				else{
					hsnCode = new Phrase(getHSNCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}
			}
			else {

				if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode() != null) {
					if (invoiceentity.getSalesOrderProducts().get(i).getHsnCode()
							.trim().length() > 0) {
						hsnCode = new Phrase(invoiceentity.getSalesOrderProducts()
								.get(i).getHsnCode().trim(), font6);
					} else {
						
						hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
								, font6);
					}
				}
				else{
					hsnCode = new Phrase(getSACCode(invoiceentity.getSalesOrderProducts().get(i).getProdCode().trim())
							, font6);
				}

			}

			PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
			hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);

			productTable.addCell(hsnCodeCell);
			String startDateStr = "", endDateStr = "";
			PdfPCell startDate_endDateCell = null;
			/**
			 * Date 21-01-2019 By Vijay for sales and service bill merged invoice.
			 * Des :- if product is service product then from contract it will get contract duration and will print
			 * and else if product is item product the contract duration will print N.A.
			 */
		
		 
			if(isServiceProductFlag){
				for (int j = 0; j < con.getItems().size(); j++) {
					SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
							"dd/MM/yyyy");
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));
					simpleDateFmt.setTimeZone(TimeZone.getTimeZone("IST"));
					if ((invoiceentity.getSalesOrderProducts().get(i).getPrduct()
							.getCount() == con.getItems().get(j).getPrduct()
							.getCount())
							&& (invoiceentity.getSalesOrderProducts().get(i)
									.getOrderDuration() == con.getItems().get(j)
									.getDuration())) {
						if(invoiceentity.getSalesOrderProducts().get(i)
									.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName().trim())){
							
							if (con.getItems().get(j).getEndDate() != null) {
								startDateStr = simpleDateFmt.format(con.getItems()
										.get(j).getStartDate());
								
								System.out.println("Start Dateash:::"+startDateStr);
								endDateStr = simpleDateFmt.format(con.getItems().get(j)
										.getEndDate());
								
								System.out.println("End  Dateash:::"+endDateStr);
								
							} else {
								Calendar c = Calendar.getInstance();
								c.setTime(con.getItems().get(j).getStartDate());
								c.add(Calendar.DATE, con.getItems().get(j)
										.getDuration());
								
								Date endDt = c.getTime();
								startDateStr = simpleDateFmt.format(con.getItems()
										.get(j).getStartDate());
								
								System.out.println("Start Date1:::"+startDateStr);
								endDateStr = simpleDateFmt.format(endDt);
								
								System.out.println("End  Date1:::"+endDateStr);
							}
							
							
						}
					}
				}

				/**
				 * Date :6/1/2018 By :Manisha Description : When no. of services is
				 * 1 then enddate is increased by 1 day..!!!
				 */
				Boolean configurationFlag = false;

				processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", invoiceentity.getCompanyId())
						.filter("processName", "Invoice")
						.filter("configStatus", true).first().now();

				if (processConfig != null) {
					for (int k = 0; k < processConfig.getProcessList().size(); k++) {
						if (processConfig.getProcessList().get(k).getProcessType()
								.trim().equalsIgnoreCase("SingleServiceDuration")
								&& processConfig.getProcessList().get(k).isStatus() == true) {
							configurationFlag = true;
							break;
						}
					}
				}

				if (configurationFlag == true) {
					for (int k = 0; k < invoiceentity.getSalesOrderProducts()
							.size(); k++) {
						if (!invoiceentity.getSalesOrderProducts().get(i)
								.getProdName().trim().equalsIgnoreCase("")
								&& invoiceentity.getSalesOrderProducts().get(i)
										.getOrderServices() == 1) {
							isSingleService = true;
						}
					}
				}

//				PdfPCell startDate_endDateCell = null;
				if (isSingleService) {
					Calendar c = Calendar.getInstance();
					SimpleDateFormat simpleDateFmt = new SimpleDateFormat(
							"dd/MM/yyyy");
					Date date = null;
					try {
						date = simpleDateFmt.parse(startDateStr);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					c.setTime(date);
					c.add(Calendar.DATE, 1);
					Date endDt = c.getTime();
					endDateStr = simpleDateFmt.format(endDt);
					Phrase startDate_endDate = new Phrase(startDateStr + " - "
							+ endDateStr, font6);
					startDate_endDateCell = new PdfPCell(startDate_endDate);
					startDate_endDateCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);
				} else {
					Phrase startDate_endDate = new Phrase(startDateStr + " - "
							+ endDateStr, font6);
					startDate_endDateCell = new PdfPCell(startDate_endDate);
					startDate_endDateCell
							.setHorizontalAlignment(Element.ALIGN_CENTER);

				}
			}
			else{
				Phrase startDate_endDate = new Phrase("NA",font6);
				startDate_endDateCell = new PdfPCell(startDate_endDate);
				startDate_endDateCell
						.setHorizontalAlignment(Element.ALIGN_CENTER);
			}
			/**
			 * ends here
			 */

			/** Commented by Manisha **/
			// Phrase startDate_endDate = new Phrase(startDateStr + " - "
			// + endDateStr, font6);
			//
			// PdfPCell startDate_endDateCell = new PdfPCell(startDate_endDate);
			// startDate_endDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			/** End of commented code **/

			/** End of Manisha **/
			productTable.addCell(startDate_endDateCell);
			
			System.out.println("startDate_endDateCell:::"+startDate_endDateCell);

			Phrase qty = new Phrase(invoiceentity.getSalesOrderProducts()
					.get(i).getQuantity()
					+ "", font6);
			PdfPCell qtyCell = new PdfPCell(qty);
			qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(qtyCell);

			// Phrase uom = new Phrase(invoiceentity.getSalesOrderProducts()
			// .get(i).getUnitOfMeasurement().trim(), font6);
			// PdfPCell uomCell = new PdfPCell(uom);
			// uomCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			// productTable.addCell(uomCell);

			Phrase rate = null;
			PdfPCell rateCell = null;
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					rate = new Phrase(df.format(rateAmountProd) + "", font6);

					rateCell = new PdfPCell(rate);
					if(invoiceentity.getSalesOrderProducts().size() > 1)
					rateCell.setBorderWidthBottom(0);
					rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					rate = new Phrase(" ", font6);

					rateCell = new PdfPCell(rate);
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						
						rateCell.setBorderWidthTop(0);
					}else{
						rateCell.setBorderWidthBottom(0);
						rateCell.setBorderWidthTop(0);
					}
					rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					//rateCell.setBorderWidthBottom(0);
					rateCell.setBorderWidthTop(0);

				}
			} else {
				rate = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getPrice())
						+ "", font6);

				rateCell = new PdfPCell(rate);
				rateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}
			productTable.addCell(rateCell);

			// amountAmount=amountAmount+invoiceentity.getSalesOrderProducts().get(i)
			// .getPrice()
			// * invoiceentity.getSalesOrderProducts().get(i)
			// .getQuantity();
			double amountValue = invoiceentity.getSalesOrderProducts().get(i)
					.getPrice()
					* invoiceentity.getSalesOrderProducts().get(i)
							.getQuantity();
			double disPercentTotalAmount = 0, disConTotalAmount = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdPercDiscount() == null
					|| invoiceentity.getSalesOrderProducts().get(i)
							.getProdPercDiscount() == 0) {
				disPercentTotalAmount = 0;
			} else {
				disPercentTotalAmount = getPercentAmount(invoiceentity
						.getSalesOrderProducts().get(i), false);
			}

			if (invoiceentity.getSalesOrderProducts().get(i).getDiscountAmt() == 0) {
				disConTotalAmount = 0;
			} else {
				disConTotalAmount = invoiceentity.getSalesOrderProducts()
						.get(i).getDiscountAmt();
			}

			totalAmount = totalAmount + amountValue;
			// Phrase amount = new Phrase(df.format(amountValue
			// - disPercentTotalAmount - disConTotalAmount)
			// + "", font6);
			// PdfPCell amountCell = new PdfPCell(amount);
			// amountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(amountCell);

			// discAmount=discAmount+invoiceentity
			// .getSalesOrderProducts().get(i).getFlatDiscount();
			Phrase disc = null;
			PdfPCell discCell = null;
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					disc = new Phrase(df.format(discAmountProd) + "", font6);

					discCell = new PdfPCell(disc);
					if(invoiceentity.getSalesOrderProducts().size() > 1)
					discCell.setBorderWidthBottom(0);
					discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					disc = new Phrase(" ", font6);

					discCell = new PdfPCell(disc);
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						
						discCell.setBorderWidthTop(0);
					}else{
						discCell.setBorderWidthBottom(0);
						discCell.setBorderWidthTop(0);
					}
					discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

					//discCell.setBorderWidthBottom(0);
					discCell.setBorderWidthTop(0);
				}
			} else {
				disc = new Phrase(df.format(invoiceentity
						.getSalesOrderProducts().get(i).getFlatDiscount())
						+ "", font6);

				discCell = new PdfPCell(disc);
				discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}

			// PdfPCell discCell = new PdfPCell(disc);
			// discCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			productTable.addCell(discCell);

			Phrase taxableValue = null;
			double taxValue = 0;
			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getPaymentPercent() != 0) {

				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBasePaymentAmount();
			} else {
				// taxableValue = new Phrase(df.format(invoiceentity
				// .getSalesOrderProducts().get(i).getBaseBillingAmount())
				// + "", font6);
				taxValue = invoiceentity.getSalesOrderProducts().get(i)
						.getBaseBillingAmount();
			}
			// taxableValue =null;/* new Phrase(df.format(taxValue)+ "",
			// font6);*/
			// totalAssAmount=totalAssAmount+taxValue;
			PdfPCell taxableValueCell = null;/*
											 * = new PdfPCell(taxableValue);
											 * taxableValueCell
											 * .setHorizontalAlignment
											 * (Element.ALIGN_RIGHT);
			
											 */
			//if (consolidatePrice) {
			/** date 06-02-2018 added by komal for consolidate price **/
			if(consolidatePrice || invoiceentity.isConsolidatePrice()){
				if (i == 0) {
					taxableValue = new Phrase(df.format(totalAssAmountProd)
							+ "", font6);
					taxableValueCell = new PdfPCell(taxableValue);
					if(invoiceentity.getSalesOrderProducts().size() > 1)
					taxableValueCell.setBorderWidthBottom(0);
					taxableValueCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
				} else {
					taxableValue = new Phrase(" ", font6);
					taxableValueCell = new PdfPCell(taxableValue);
					if(i == invoiceentity.getSalesOrderProducts().size()-1 || noOfLines == 0){
						
						taxableValueCell.setBorderWidthTop(0);
					}else{
						taxableValueCell.setBorderWidthBottom(0);
						taxableValueCell.setBorderWidthTop(0);
					}
					taxableValueCell
							.setHorizontalAlignment(Element.ALIGN_RIGHT);
					//taxableValueCell.setBorderWidthBottom(0);
					taxableValueCell.setBorderWidthTop(0);
				}
			} else {
				taxableValue = new Phrase(df.format(taxValue) + "", font6);
				taxableValueCell = new PdfPCell(taxableValue);
				taxableValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}
			productTable.addCell(taxableValueCell);

			PdfPCell cellIGST;

			// System.out.println("Print Name::::"+invoiceentity.getSalesOrderProducts().get(i).getVatTax().getTaxPrintName());

			logger.log(Level.SEVERE, "VAT TAX ::::Config Name"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getTaxConfigName()
					+ "VAT TAx:::Tax Name"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getTaxName()
					+ "Ser TAX ::::Config Name"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getTaxConfigName()
					+ "Ser TAx:::Tax Name"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getTaxName());
			logger.log(Level.SEVERE, "VAT TAX ::::"
					+ invoiceentity.getSalesOrderProducts().get(i).getVatTax()
							.getPercentage()
					+ "Service Tax::::"
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getServiceTax().getPercentage());

			
			String premisesVal = "";
			for (int j = 0; j < con.getItems().size(); j++) {
				/** Date 15-02-2018 By vijay primises issue having 2 same product primices showing 1 st product primeses into 2 nd also **/
				if (invoiceentity.getSalesOrderProducts().get(i).getProdId() == con
						.getItems().get(j).getPrduct().getCount()
						&& invoiceentity.getSalesOrderProducts().get(i)
								.getOrderDuration() == con.getItems().get(j)
								.getDuration() && invoiceentity.getSalesOrderProducts().get(i).getProductSrNumber() == con.getItems().get(j).getProductSrNo()) {
						if(invoiceentity.getSalesOrderProducts().get(i)
								.getProdName().trim().equalsIgnoreCase(con.getItems().get(j).getProductName().trim())){
						premisesVal = con.getItems().get(j).getPremisesDetails();
					}
				}

			}
			System.out.println("noOfLines in product" + noOfLines);
			if (premisesVal != null) {
				if (printPremiseDetails && !premisesVal.equals("")) {
					noOfLines = noOfLines - 1;
					Phrase blankValPhrs = new Phrase(" ", font8);
					PdfPCell premiseCell = new PdfPCell(blankValPhrs);
					premiseCell.setColspan(1);

					productTable.addCell(premiseCell);

					Phrase premisesValPhrs = new Phrase("Premise Details : "
							+ premisesVal, font8);
					premiseCell = new PdfPCell(premisesValPhrs);
					premiseCell.setColspan(7);
					productTable.addCell(premiseCell);
				}
			} else {

			}
			// boolean
			// vatPercentZero=invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage() != 0;
			// boolean
			// serPercentZero=invoiceentity.getSalesOrderProducts().get(i)
			// .getServiceTax().getPercentage() != 0;
			// boolean taxPresent = validateTaxes(invoiceentity
			// .getSalesOrderProducts().get(i));
			// if (taxPresent) {
			// logger.log(Level.SEVERE,"Inside Tax Applicable");
			//
			// if (invoiceentity.getSalesOrderProducts().get(i).getVatTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			//
			// }else if
			// (invoiceentity.getSalesOrderProducts().get(i).getServiceTax()
			// .getTaxPrintName().equalsIgnoreCase("IGST")) {
			//
			// double taxAmount = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + taxAmount;
			// totalAmount = totalAmount + indivTotalAmount;
			//
			// Phrase igstRateVal = new Phrase(df.format(taxAmount) + "",
			// font10);
			// PdfPCell igstRateValCell = new PdfPCell();
			// // igstRateValCell.setBorder(0);
			// igstRateValCell.addElement(igstRateVal);
			//
			// Phrase igstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell igstRateCell = new PdfPCell();
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setColspan(2);
			// igstRateCell.addElement(igstRate);
			//
			// /* for Cgst */
			//
			// Phrase cgst = new Phrase("-", font10);
			// PdfPCell cell = new PdfPCell(cgst);
			// // cell.addElement(cgst);
			// cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// /* for Sgst */
			// Phrase sgst = new Phrase("-", font10);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			//
			// productTable.addCell(igstRateCell);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase totalPhrase = new Phrase(df.format(indivTotalAmount)
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			//
			// productTable.addCell(premiseCell);
			// }
			//
			// } else {
			//
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("CGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// // igstRateValCell.setBorder(0);
			// // igstRateValCell.setBorderWidthBottom(0);
			// // igstRateValCell.setBorderWidthTop(0);
			// // igstRateValCell.setBorderWidthRight(0);
			// // igstRateValCell.addElement(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// // try {
			// // document.add(productTable);
			// // } catch (DocumentException e) {
			// // // TODO Auto-generated catch block
			// // e.printStackTrace();
			// // }
			//
			// } else if (invoiceentity.getSalesOrderProducts().get(i)
			// .getVatTax().getTaxPrintName()
			// .equalsIgnoreCase("SGST")) {
			//
			// double ctaxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage());
			//
			// Phrase cgstRateVal = new Phrase(df.format(ctaxValue)
			// + "", font10);
			// PdfPCell cgstRateValCell = new PdfPCell();
			// // cgstRateValCell.setBorder(0);
			// // cgstRateValCell.setBorderWidthBottom(0);
			// // cgstRateValCell.setBorderWidthTop(0);
			// // cgstRateValCell.setBorderWidthRight(0);
			// cgstRateValCell.addElement(cgstRateVal);
			//
			// Phrase cgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell cgstRateCell = new PdfPCell();
			// // cgstRateCell.setBorder(0);
			// // cgstRateCell.setBorderWidthBottom(0);
			// // cgstRateCell.setBorderWidthTop(0);
			// // cgstRateCell.setBorderWidthLeft(0);
			// cgstRateCell.addElement(cgstRate);
			// productTable.addCell(cgstRateCell);
			// productTable.addCell(cgstRateValCell);
			//
			// double staxValue = getTaxAmount(invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount(), invoiceentity
			// .getSalesOrderProducts().get(i).getVatTax()
			// .getPercentage());
			// Phrase sgstRateVal = new Phrase(df.format(staxValue)
			// + "", font10);
			// PdfPCell sgstRateValCell = new PdfPCell();
			// // sgstRateValCell.setBorder(0);
			// // sgstRateValCell.setBorderWidthBottom(0);
			// // sgstRateValCell.setBorderWidthTop(0);
			// // sgstRateValCell.setBorderWidthRight(0);
			// sgstRateValCell.addElement(sgstRateVal);
			//
			// Phrase sgstRate = new Phrase(invoiceentity
			// .getSalesOrderProducts().get(i).getServiceTax()
			// .getPercentage()
			// + "", font10);
			// PdfPCell sgstRateCell = new PdfPCell();
			// // sgstRateCell.setBorder(0);
			// // sgstRateCell.setBorderWidthBottom(0);
			// // sgstRateCell.setBorderWidthTop(0);
			// // sgstRateCell.setBorderWidthLeft(0);
			// sgstRateCell.addElement(sgstRate);
			// productTable.addCell(sgstRateCell);
			// productTable.addCell(sgstRateValCell);
			//
			// Phrase igstRateVal = new Phrase("-", font10);
			// PdfPCell igstRateValCell = new PdfPCell(igstRateVal);
			// igstRateValCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateValCell);
			//
			// Phrase igstRate = new Phrase("-", font10);
			// PdfPCell igstRateCell = new PdfPCell(igstRate);
			// // igstRateCell.setBorder(0);
			// // igstRateCell.setBorderWidthBottom(0);
			// // igstRateCell.setBorderWidthTop(0);
			// // igstRateCell.setBorderWidthLeft(0);
			// // igstRateCell.addElement(igstRate);
			// igstRateCell
			// .setHorizontalAlignment(Element.ALIGN_RIGHT);
			// productTable.addCell(igstRateCell);
			//
			// double indivTotalAmount = invoiceentity
			// .getSalesOrderProducts().get(i)
			// .getBasePaymentAmount()
			// + ctaxValue + staxValue;
			// totalAmount = totalAmount + indivTotalAmount;
			// Phrase totalPhrase = new Phrase(
			// df.format(indivTotalAmount) + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			//
			// productTable.addCell(totalCell);
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j)
			// .getPrduct().getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase(
			// "Premise Details : " + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// }
			// }
			//
			// }
			// else {
			// logger.log(Level.SEVERE,"Inside Tax Not Applicable");
			//
			// PdfPCell cell = new PdfPCell(new Phrase("-", font8));
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// productTable.addCell(cell);
			// Phrase totalPhrase = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getBasePaymentAmount())
			// + "", font10);
			// PdfPCell totalCell = new PdfPCell();
			// // totalCell.setColspan(16);
			// // totalCell.setBorder(0);
			// // totalCell.setBorderWidthBottom(0);
			// // totalCell.setBorderWidthTop(0);
			// totalCell.addElement(totalPhrase);
			// productTable.addCell(totalCell);
			//
			// String premisesVal = "";
			// for (int j = 0; j < con.getItems().size(); j++) {
			// if (invoiceentity.getSalesOrderProducts().get(i)
			// .getProdId() == con.getItems().get(j).getPrduct()
			// .getCount()) {
			// premisesVal = con.getItems().get(j)
			// .getPremisesDetails();
			// }
			//
			// }
			// if (printPremiseDetails) {
			// Phrase premisesValPhrs = new Phrase("Premise Details : "
			// + premisesVal, font8);
			// PdfPCell premiseCell = new PdfPCell();
			// premiseCell.setColspan(10);
			// premiseCell.addElement(premisesValPhrs);
			// productTable.addCell(premiseCell);
			// }
			// }
		}
		int remainingLines = 0;
		System.out.println("noOfLines outside" + noOfLines);
		System.out.println("prouductCount" + prouductCount);

		if (noOfLines != 0) {
			remainingLines = 16 - (16 - noOfLines);
		}
		System.out.println("remainingLines" + remainingLines);

		if (remainingLines != 0) {
			for (int i = 0; i < remainingLines; i++) {
				System.out.println("i::::" + i);
				Phrase blankPhrase = new Phrase(" ", font10);
				PdfPCell blankPhraseCell = new PdfPCell(blankPhrase);
				blankPhraseCell.setBorder(0);
				blankPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
				productTable.addCell(blankPhraseCell);
			}
		}
		PdfPCell productTableCell = null;
		if (noOfLines == 0) {
			Phrase my = new Phrase("Please Refer Annexure For More Details",
					font9bold);
			productTableCell = new PdfPCell(my);

		} else {
			productTableCell = new PdfPCell(blankCell);
		}

		productTableCell.setBorder(0);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);
		tab.addCell(productTableCell);
		tab.setSpacingAfter(blankLines);

		// last code for both table to be added in one table

		PdfPCell tab1;
		tab1 = new PdfPCell(productTable);
		// tab1.setBorder(0);

		PdfPCell tab2 = new PdfPCell(tab);
		// tab2.setBorder(0);

		PdfPTable mainTable = new PdfPTable(1);
		mainTable.setWidthPercentage(100f);
		mainTable.addCell(tab1);
		mainTable.addCell(tab2);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	

	private boolean validateTaxes(
			SalesOrderProductLineItem salesOrderProductLineItem) {
		// TODO Auto-generated method stub
		if (salesOrderProductLineItem.getVatTax().getPercentage() != 0) {
			return true;
		} else {
			if (salesOrderProductLineItem.getServiceTax().getPercentage() != 0) {
				return true;
			} else {
				return false;
			}
		}
	}

	private double getTaxAmount(double totalAmount2, double percentage) {
		double percAmount = totalAmount2 / 100;
		double taxAmount = percAmount * percentage;
		return taxAmount;
	}

	private void createProductDetails() {
		PdfPTable productTable = new PdfPTable(8);
		productTable.setWidthPercentage(100);
		try {
			productTable.setWidths(column8SerProdCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase srNophrase = new Phrase("Sr No", font10bold);
		PdfPCell srNoCell = new PdfPCell(srNophrase);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setRowspan(2); // 1

		Phrase servicePhrase = new Phrase("Particulars", font10bold);
		PdfPCell servicePhraseCell = new PdfPCell(servicePhrase);
		servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		servicePhraseCell.setRowspan(2);// 2

		Phrase noOfservicePhrase = new Phrase("Qty / No Of Services", font10bold);
		PdfPCell noOfservicePhraseCell = new PdfPCell(noOfservicePhrase);
		noOfservicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		noOfservicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		noOfservicePhraseCell.setRowspan(2);// 3

		Phrase hsnCode = new Phrase("HSN / SAC", font10bold);
		PdfPCell hsnCodeCell = new PdfPCell(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		hsnCodeCell.setRowspan(2);// 4

		Phrase UOMphrase = new Phrase("UOM", font10bold);
		PdfPCell UOMphraseCell = new PdfPCell(UOMphrase);
		UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		UOMphraseCell.setRowspan(2);// 5

		Phrase qtyPhrase = new Phrase("No ", font10bold);
		PdfPCell qtyPhraseCell = new PdfPCell(qtyPhrase);
		qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		qtyPhraseCell.setRowspan(2);// 6

		Phrase ratePhrase = new Phrase("Rate", font10bold);
		PdfPCell ratePhraseCell = new PdfPCell(ratePhrase);
		ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		ratePhraseCell.setRowspan(2);// 7

		Phrase amountPhrase = new Phrase("Amount", font10bold);
		PdfPCell amountPhraseCell = new PdfPCell(amountPhrase);
		amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		amountPhraseCell.setRowspan(2);// 8

		Phrase dicphrase = new Phrase("Disc", font10bold);
		PdfPCell dicphraseCell = new PdfPCell(dicphrase);
		dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dicphraseCell.setRowspan(2);// 9

		Phrase taxValPhrase = new Phrase("Billed Amount", font10bold);
		PdfPCell taxValPhraseCell = new PdfPCell(taxValPhrase);
		taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		taxValPhraseCell.setRowspan(2);// 10

		Phrase serviceServDate = new Phrase("Contract Duration", font10bold);
		PdfPCell serviceServDateCell = new PdfPCell(serviceServDate);
		serviceServDateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceServDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		productTable.addCell(srNoCell);
		productTable.addCell(servicePhraseCell);
		productTable.addCell(noOfservicePhraseCell);

		productTable.addCell(hsnCodeCell);
		productTable.addCell(serviceServDateCell);

		// productTable.addCell(qtyPhraseCell);
		// productTable.addCell(UOMphraseCell);
		productTable.addCell(ratePhraseCell);
		// productTable.addCell(amountPhraseCell);
		productTable.addCell(dicphraseCell);
		productTable.addCell(taxValPhraseCell);

		try {
			document.add(productTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private double getPercentAmount(
			SalesOrderProductLineItem salesOrderProductLineItem,
			boolean isAreaPresent) {
		// TODO Auto-generated method stub
		double percentAmount = 0;
		if (isAreaPresent) {
			percentAmount = ((salesOrderProductLineItem.getPrice()
					* Double.parseDouble(salesOrderProductLineItem.getArea()
							.trim()) * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		} else {
			percentAmount = ((salesOrderProductLineItem.getPrice() * salesOrderProductLineItem
					.getProdPercDiscount()) / 100);
		}
		return percentAmount;
	}

	private void createCustomerDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font10bold);
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan added this code for considering customer printable name as well
		// Date : 04-07-2017

		String tosir = null;
		String custName = "";
		// rohan modified this code for printing printable name

		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S " + " " + cust.getCompanyName().trim();
			} else if (cust.getSalutation() != null
					&& !cust.getSalutation().equals("")) {
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here

		Phrase nameCellVal = new Phrase(fullname, font10bold);
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////////Ajinkya Added this for Ultra pest Control

		Phrase email = new Phrase("Email", font10bold);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * By Jayshree Date 28/12/2017 to check null condition
		 */
		Phrase emailVal = null;
		if (cust.getEmail() != null) {
			emailVal = new Phrase(cust.getEmail(), font10);
		} else {
			emailVal = new Phrase("", font10);
		}
		PdfPCell emailValCell = new PdfPCell(emailVal);
		// nameCellValCell.addElement(nameCellVal);asd
		emailValCell.setBorder(0);
		emailValCell.setColspan(4);
		emailValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo = new Phrase("Phone", font10bold);
		PdfPCell mobNoCell = new PdfPCell(mobNo);
		mobNoCell.setBorder(0);
		mobNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// String contactNumber = "";
		// if (cust.getLandline() != 0) {
		// contactNumber = cust.getLandline() + "";
		// }
		// else{
		// contactNumber = "";
		// }
		// if (cust.getCellNumber1()!=0) {
		// contactNumber = contactNumber + " / " + cust.getCellNumber1();
		// } else {
		// contactNumber = contactNumber + "";
		// }
		//
		// if (cust.getCellNumber2() != 0) {
		// contactNumber = contactNumber + " / " + cust.getCellNumber2();
		// }
		// else{
		// contactNumber = contactNumber + "  " ;
		// }

//		String phone = "";
//		if (cust.getCellNumber1() != 0) {
//			phone = cust.getCellNumber1() + "";
//		} else {
//			phone = "";
//		}
//		if (cust.getCellNumber2() != 0) {
//			phone = phone + "/" + cust.getCellNumber2() + "";
//		} else {
//			phone = "";
//		}
		String phone="";
		String landline="";
		
			if(cust.getCellNumber1()!=null && cust.getCellNumber1()!=0){
				System.out.println("pn11");
			phone=cust.getCellNumber1()+"";
			}
			if(cust.getCellNumber2()!=null && cust.getCellNumber2()!=0)
			{
				if(!phone.trim().isEmpty())
					{
					phone=phone+" / "+cust.getCellNumber2()+"";
					}
				else{
					phone=cust.getCellNumber2()+"";
					}
			System.out.println("pn33"+phone);
			}
			if(cust.getLandline()!=0 && cust.getLandline()!=null)
			{
				if(!phone.trim().isEmpty()){
					phone=phone+" / "+cust.getLandline()+"";
				}
				else{
					phone=cust.getLandline()+"";
				}
			System.out.println("pn44"+phone);
			}

		Phrase mobVal = new Phrase(phone, font10);
		PdfPCell mobValCell = new PdfPCell(mobVal);
		// nameCellValCell.addElement(nameCellVal);asd
		mobValCell.setBorder(0);
		mobValCell.setColspan(4);//add Calspane by jayshree
		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase landlineph = new Phrase("LandLine", font10bold);
//		PdfPCell landlineCell = new PdfPCell(landlineph);
//		landlineCell.setBorder(0);
//		landlineCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
////		String landline = "";
////		if (cust.getLandline() != 0) {
////			landline = cust.getLandline() + "";
////		} else {
////			landline = "";
////		}
//
//		Phrase landlineVal = new Phrase(landline, font10);
//		PdfPCell landlinevalCells = new PdfPCell(landlineVal);
//		// nameCellValCell.addElement(nameCellVal);asd
//		landlinevalCells.setBorder(0);
//		landlinevalCells.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ///////////////// End Here

		Phrase address = new Phrase("Address", font10bold);// Date 9/12/2017 By
															// Jayshree increse
															// the Font size
		PdfPCell addressCell = new PdfPCell(address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * Date 07-01-2019 By Vijay
		 * Bug for different contract merge invoice not reuired to check contract address
		 * because contract id is not storing in invoice so wrong address will get print so below code commented
		 */
		String adrsValString = "";
//		if (con.getNewcustomerAddress() != null
//				&& con.getNewcustomerAddress().getAddrLine1() != null
//				&& !con.getNewcustomerAddress().getAddrLine1().equals("")
//				&& con.getNewcustomerAddress().getCountry() != null
//				&& !con.getNewcustomerAddress().getCountry().equals("")) // By Jayshree
//																// Date
//																// 28/12/2017 to
//																// check null
//																// condition
//		{
//			adrsValString = con.getNewcustomerAddress().getCompleteAddress()
//					.trim();
//		} else {
			adrsValString = cust.getAdress().getCompleteAddress().trim();
//		}

		Phrase addressVal = new Phrase(adrsValString, font13);// Date 9/12/2017
																// By Jayshree
																// increse the
																// Font size
		PdfPCell addressValCell = new PdfPCell(addressVal);
		// addressValCell.addElement(addressVal);
		addressValCell.setBorder(0);
		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String gstTinStr = "";
		if (invoiceentity.getGstinNumber() != null
				&& !invoiceentity.getGstinNumber().equals("")) {
			gstTinStr = invoiceentity.getGstinNumber().trim();

		} else {
			ServerAppUtility serverAppUtility = new ServerAppUtility();
			gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,
					invoiceentity.getCustomerBranch(), "ServiceInvoice");//Date 1/2/2018 By jayshree add new parameter
			// for (int j = 0; j < cust.getArticleTypeDetails().size(); j++) {
			// if (cust.getArticleTypeDetails().get(j).getArticleTypeName()
			// .equalsIgnoreCase("GSTIN")) {
			// gstTinStr = cust.getArticleTypeDetails().get(j)
			// .getArticleTypeValue().trim();
			// }
			// }
		}
				Phrase gstTin = new Phrase("GSTIN", font10bold);
				PdfPCell gstTinCell = new PdfPCell(gstTin);
				// gstTinCell.addElement(gstTin);
				gstTinCell.setBorder(0);
				gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
				Phrase gstTinVal = new Phrase(gstTinStr, font10);
				PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
				// gstTinValCell.addElement(gstTinVal);
				gstTinValCell.setBorder(0);
				gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * Date13/12/2017 By Jayshree To Add state code
		 */

		/**
	 * Date 1/2/2018 By jayshree 
	 * Des.remove the statecode if GSTIN is not present
	 */
	Phrase stateCode=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	stateCode = new Phrase("State Code", font10bold);//Date 9/12/2017 By Jayshree To increse the fontsize by one
	}
	else{
		stateCode = new Phrase(" ", font10bold);
	}
	PdfPCell stateCodeCell = new PdfPCell(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase colon3=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	 colon3 = new Phrase(":", font10bold);
	}
	else{
		colon3 = new Phrase(" ", font10bold);
	}
	PdfPCell colon3Cell = new PdfPCell(colon3);
	colon3Cell.setBorder(0);
	colon3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String stCo = "";
	for (int i = 0; i < stateList.size(); i++) {
		if (stateList.get(i).getStateName().trim()
				.equalsIgnoreCase(cust.getAdress().getState().trim())) {
			stCo = stateList.get(i).getStateCode().trim();
			break;
		}
	}
	
	Phrase stateCodeVal=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	 stateCodeVal = new Phrase(stCo, font10);//Date 9/12/2017 By Jayshree To increse the fontsize by one
	}else{
		stateCodeVal = new Phrase(" ", font10);
	}
	//End By Jayshree
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn = new Phrase("Attn", font10bold);
		PdfPCell attnCell = new PdfPCell(attn);
		// gstTinCell.addElement(gstTin);
		attnCell.setBorder(0);
		attnCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attnVal = new Phrase(invoiceentity.getPersonInfo().getPocName(),
				font10);
		PdfPCell attnValCell = new PdfPCell(attnVal);
		// gstTinValCell.addElement(gstTinVal);
		attnValCell.setBorder(0);
		attnValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attnCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attnValCell);
		}
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell);

		PdfPTable colonTable2 = new PdfPTable(6);
		colonTable2.setWidthPercentage(100);
		try {
			colonTable2.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f, 0.2f,
					2.0f });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable email_MobNumber = new PdfPTable(6);// Date 3/01/2017 By
														// jayshree Change colon
														// width
		email_MobNumber.setWidthPercentage(100);
		try {
			email_MobNumber.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f,
					0.2f, 2.0f });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon2 = new Phrase(":", font10bold);
		PdfPCell colon2Cell = new PdfPCell(colon2);
		colon2Cell.setBorder(0);
		colon2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		email_MobNumber.addCell(emailCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(emailValCell);
		email_MobNumber.addCell(mobNoCell);
		email_MobNumber.addCell(colon2Cell);
		email_MobNumber.addCell(mobValCell);

//		email_MobNumber.addCell(landlineCell);
//		email_MobNumber.addCell(colon2Cell);
//		email_MobNumber.addCell(landlinevalCells);
		/**
		 * Date : 12 Mar 2018
		 * Dev : Jayshree
		 * Description : Remove GST for Customer if no taxes present.
		 */
		if(invoiceentity.getBillingTaxes().size()!=0){
			
		colonTable2.addCell(gstTinCell);
		colonTable2.addCell(colon2Cell);
		colonTable2.addCell(gstTinValCell);
		/**
		 * Date 13/12/2017 By Jayshree add the cells in tab
		 */
		colonTable2.addCell(stateCodeCell);
		colonTable2.addCell(colon3Cell);//Rename cell by jayshree
		colonTable2.addCell(stateCodeValCell);
		}
		// End By Jayshree
		PdfPCell cell1 = new PdfPCell(colonTable);
		cell1.setBorder(0);

		PdfPCell email_Cell = new PdfPCell(email_MobNumber);
		email_Cell.setBorder(0);

		PdfPCell cell12 = new PdfPCell(colonTable2);
		cell12.setBorder(0);
		/**
		 * Date 13/12/2017 dev. By Jayshree Des.Comment this to remove the
		 * statecode and state
		 */
		// Phrase state = new Phrase("State", font13bold);//Date 9/12/2017 By
		// Jayshree To increse the fontsize by one
		// PdfPCell stateCell = new PdfPCell(state);
		// stateCell.setBorder(0);
		// stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// Phrase stateVal = new Phrase(cust.getAdress().getState().trim(),
		// font13);//Date 9/12/2017 By Jayshree To increse the fontsize by one
		// PdfPCell stateValCell = new PdfPCell(stateVal);
		// // stateValCell.addElement(stateVal);
		// stateValCell.setBorder(0);
		// stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// String stCo = "";
		// for (int i = 0; i < stateList.size(); i++) {
		// if (stateList.get(i).getStateName().trim()
		// .equalsIgnoreCase(cust.getAdress().getState().trim())) {
		// stCo = stateList.get(i).getStateCode().trim();
		// break;
		// }
		// }
		//
		// Phrase stateCode = new Phrase("State Code", font13bold);//Date
		// 9/12/2017 By Jayshree To increse the fontsize by one
		// PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.setBorder(0);
		// stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// Phrase stateCodeVal = new Phrase(stCo, font13);//Date 9/12/2017 By
		// Jayshree To increse the fontsize by one
		// PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		// stateCodeValCell.setBorder(0);
		// stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);

		PdfPTable pdfStateTable = new PdfPTable(4);
		pdfStateTable.setWidthPercentage(100);
		try {
			pdfStateTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// pdfStateTable.addCell(stateValCell);
		// pdfStateTable.addCell(stateCodeCell);
		// pdfStateTable.addCell(colonCell);
		// pdfStateTable.addCell(stateCodeValCell);
		// End by jayshree
		PdfPCell state4Cell = new PdfPCell(pdfStateTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell); // stateTableCell.addElement(statetable);

		PdfPCell stateCell_stateCode = new PdfPCell(colonTable);
		stateCell_stateCode.setBorder(0);

		part1Table.addCell(cell1);
		/**
		 * Date 13/12/2017 By Jayshree des. comment this to remove state and
		 * state code
		 */
		// part1Table.addCell(stateCell_stateCode);

		part1Table.addCell(email_Cell);
		part1Table.addCell(cell12);

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		/* Ends Part 1 */

		/* Part 2 Start */
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font10bold);
		PdfPCell name2Cell = new PdfPCell(name2);
		// name2Cell.addElement();
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase name2CellVal = new Phrase(fullname, font10bold);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////// Ajinkya added this
		Phrase email1 = new Phrase("Email", font10bold);
		PdfPCell email1Cell = new PdfPCell(email1);
		email1Cell.setBorder(0);
		email1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * Date 28/12/2017 By Jayshree To handle null condition
		 */
		Phrase email1Val = null;
		if (cust.getEmail() != null) {
			email1Val = new Phrase(cust.getEmail(), font10);
		} else {
			email1Val = new Phrase("", font10);
		}
		PdfPCell email1ValCell = new PdfPCell(email1Val);
		// nameCellValCell.addElement(nameCellVal);asd
		email1ValCell.setBorder(0);
		email1ValCell.setColspan(4);
		email1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase mobNo2 = new Phrase("Mobile", font10bold);
		PdfPCell mobNo2Cell = new PdfPCell(mobNo2);
		mobNo2Cell.setBorder(0);
		mobNo2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
		 * Date 28/12/2017 By jayshree To Check null condition
		 */
		// String contactNumber2 = "";
		// if (cust.getLandline() != 0) {
		// contactNumber2 = cust.getLandline() + "";
		// }
		// if (!contactNumber2.equals("")) {
		// contactNumber2 = contactNumber2 + " / " + cust.getCellNumber1();
		// } else {
		// contactNumber2 = cust.getCellNumber1() + "";
		// }
		//
		// if (cust.getCellNumber2() != 0) {
		// contactNumber2 = contactNumber2 + " / " + cust.getCellNumber2();
		// }

//		String phone2 = "";
//		if (cust.getCellNumber1() != 0) {
//			phone2 = cust.getCellNumber1() + "";
//		} else {
//			phone2 = "";
//		}
//		if (cust.getCellNumber2() != 0) {
//			phone2 = phone2 + "/" + cust.getCellNumber2() + "";
//		} else {
//			phone2 = "";
//		}
		String phone2="";
//		String landline="";
		
			if(cust.getCellNumber1()!=null && cust.getCellNumber1()!=0){
				System.out.println("pn11");
			phone2=cust.getCellNumber1()+"";
			}
			if(cust.getCellNumber2()!=null && cust.getCellNumber2()!=0)
			{
				if(!phone2.trim().isEmpty())
					{
					phone2=phone2+" / "+cust.getCellNumber2()+"";
					}
				else{
					phone2=cust.getCellNumber2()+"";
					}
			System.out.println("pn33"+phone2);
			}
			if(cust.getLandline()!=0 && cust.getLandline()!=null)
			{
				if(!phone2.trim().isEmpty()){
					phone2=phone2+" / "+cust.getLandline()+"";
				}
				else{
					phone2=cust.getLandline()+"";
				}
			System.out.println("pn44"+phone2);
			}
		

		Phrase mobNo2Val = new Phrase(phone2, font10);
		PdfPCell mobNo2ValCell = new PdfPCell(mobNo2Val);
		// nameCellValCell.addElement(nameCellVal);asd
		mobNo2ValCell.setBorder(0);
		mobNo2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase landlineph2 = new Phrase("LandLine", font10bold);
		PdfPCell landlineCell2 = new PdfPCell(landlineph2);
		landlineCell2.setBorder(0);
		landlineCell2.setHorizontalAlignment(Element.ALIGN_LEFT);

		String landline2 = "";
		if (cust.getLandline() != 0) {
			landline2 = cust.getLandline() + "";
		} else {
			landline2 = "";
		}

		Phrase landlineVal2 = new Phrase(landline2, font10);
		PdfPCell landlinevalCells2 = new PdfPCell(landlineVal2);
		// nameCellValCell.addElement(nameCellVal);asd
		landlinevalCells2.setBorder(0);
		landlinevalCells2.setHorizontalAlignment(Element.ALIGN_LEFT);

		// //////////////////

		Phrase address2 = new Phrase("Address", font10bold);// Date 9/12/2017 By
															// Jayshree increse
															// the Font size
		PdfPCell address2Cell = new PdfPCell(address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ///////////////////////// Ajinkya added branch code for customer
		// Branch On Date : 15/07/2017 //////////////////////////////
		// String adrsValString1 = "";
		/**
		 * Date :17-11-2017 Below Code Commented By Manisha
		 */
		// //////////////////////////////
		// String customerPoc = null;
		// if (!cust.getSecondaryAdress().getAddrLine1().equals("")
		// && customerbranchlist.size() == 0) {
		// System.out.println("Inside service Addrss");
		//
		// // old code commented by vijay new code added below
		// // adrsValString1 = cust.getSecondaryAdress().getCompleteAddress()
		// // .trim();
		//
		// /**
		// * Date 13-09-2017 added by vijay for customer address first it will
		// * check from contract if in contract customer address is available
		// * then it will print else it will get customer address from
		// * customer
		// *
		// */
		//
		// if (con.getCustomerServiceAddress() != null
		// && con.getCustomerServiceAddress().getAddrLine1() != null
		// && !con.getCustomerServiceAddress().getAddrLine1()
		// .equals("")
		// && con.getCustomerServiceAddress().getCountry() != null
		// && !con.getCustomerServiceAddress().getCountry().equals("")) {
		// adrsValString1 = con.getCustomerServiceAddress()
		// .getCompleteAddress().trim();
		// } else {
		// adrsValString1 = cust.getSecondaryAdress().getCompleteAddress()
		// .trim();
		// }
		// customerPoc = invoiceentity.getPersonInfo().getPocName();
		// } else {
		// System.out.println("Inside Customer branch  "
		// + customerbranchlist.size());
		// for (int i = 0; i < customerbranchlist.size(); i++) {
		// // if()
		// adrsValString1 = customerbranchlist.get(i).getAddress()
		// .getCompleteAddress();
		// customerPoc = customerbranchlist.get(i).getPocName();
		// }
		//
		// }
		/** ends **/

		/**
		 * Date :17-11-2017 By :Manisha To get the service address on pdf..!!
		 * 
		 */

		String adrsValString1 = "";
		String customerPoc = null;

		Address serviceAddress = null;

		if (invoiceentity.getCustomerBranch() != null
				&& !invoiceentity.getCustomerBranch().equals("")) {
			CustomerBranchDetails branch = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("companyId", comp.getCompanyId())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch()).filter("cinfo.count", invoiceentity.getPersonInfo().getCount()).first().now();
			/** Date 18-01-2018 By vijay above query added customer id filter for getting specific customer branch **/
			if (branch != null) {
				serviceAddress = branch.getAddress();
				System.out.println("heeeeeeeelllllllooo" + serviceAddress);

				adrsValString1 = branch.getAddress().getCompleteAddress();

			}

		} else {

			/**
			 * Date 07-01-2019 By Vijay
			 * Bug for different contract merge invoice not reuired to check contract address
			 * because contract id is not storing in invoice so wrong address will get print so below code commented
			 */
//			if (con.getCustomerServiceAddress() != null) {
//				serviceAddress = con.getCustomerServiceAddress();
//				adrsValString1 = con.getCustomerServiceAddress()
//						.getCompleteAddress();
//
//			} else {
				serviceAddress = cust.getSecondaryAdress();
				adrsValString1 = cust.getSecondaryAdress().getCompleteAddress();

//			}
		}

		if (!cust.getSecondaryAdress().getAddrLine1().equals("")
				&& customerbranchlist.size() == 0) {
			customerPoc = invoiceentity.getPersonInfo().getPocName();
		}

		System.out.println("Inside Customer branch  "
				+ customerbranchlist.size());
		for (int i = 0; i < customerbranchlist.size(); i++) {

			customerPoc = customerbranchlist.get(i).getPocName();
		}

		/** ends for Manisha **/

		Phrase address2Val = new Phrase(adrsValString1, font13);// Date
																// 9/12/2017 By
																// Jayshree
																// increse the
																// Font size
		PdfPCell address2ValCell = new PdfPCell(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String gstTin2Str = "";
		if (invoiceentity.getGstinNumber() != null
				&& !invoiceentity.getGstinNumber().equalsIgnoreCase("")) {
			gstTin2Str = invoiceentity.getGstinNumber().trim();
		} else {
			ServerAppUtility serverAppUlAppUtility = new ServerAppUtility();

			gstTin2Str = serverAppUlAppUtility.getGSTINOfCustomer(cust,
					invoiceentity.getCustomerBranch(),"ServiceInvoice");

		}

		Phrase gstTin2 = new Phrase("GSTIN", font10bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font10);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2 = new Phrase("Attn", font10bold);
		PdfPCell attn2Cell = new PdfPCell(attn2);
		attn2Cell.setBorder(0);
		attn2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase attn2Val = null;
		if (custName != null) {
			attn2Val = new Phrase(customerPoc, font10);
		} else {
			attn2Val = new Phrase("", font10);
		}
		PdfPCell attn2ValCell = new PdfPCell(attn2Val);
		attn2ValCell.setBorder(0);
		attn2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(nameCellValCell);
		if (printAttnInPdf) {
			colonTable.addCell(attn2Cell);
			colonTable.addCell(colonCell);
			colonTable.addCell(attn2ValCell);
		}
		colonTable.addCell(address2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);

		PdfPTable colonTable22 = new PdfPTable(3);
		colonTable22.setWidthPercentage(100);
		try {
			colonTable22.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable email_MobNumber2 = new PdfPTable(6);
		email_MobNumber2.setWidthPercentage(100);
		try {
			email_MobNumber2.setWidths(new float[] { 1.9f, 0.2f, 3.7f, 2.0f,
					0.2f, 2.0f });
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		email_MobNumber2.addCell(emailCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(emailValCell);

		email_MobNumber2.addCell(mobNoCell);
		email_MobNumber2.addCell(colonCell);
		email_MobNumber2.addCell(mobValCell);

//		email_MobNumber2.addCell(landlineCell2);
//		email_MobNumber2.addCell(colonCell);
//		email_MobNumber2.addCell(landlinevalCells2);

		// colonTable22.addCell(gstTin2Cell);
		// colonTable22.addCell(colonCell);
		// colonTable22.addCell(gstTin2ValCell);

		PdfPCell cell2 = new PdfPCell(colonTable);
		cell2.setBorder(0);

		PdfPCell email_MobNumber2Cell = new PdfPCell(email_MobNumber2);
		email_MobNumber2Cell.setBorder(0);

		PdfPCell cell22 = new PdfPCell(colonTable22);
		cell22.setBorder(0);

		Phrase state2 = new Phrase("State", font13bold);// Date 9/12/2017 By
														// Jayshree To increse
														// the fontsize by one
		PdfPCell state2Cell = new PdfPCell(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stateName = "";
		if (!cust.getSecondaryAdress().getAddrLine1().equals("")
				&& customerbranchlist.size() == 0) {
			stateName = cust.getSecondaryAdress().getState().trim();
		} else {
			for (int i = 0; i < customerbranchlist.size(); i++) {
				// if()
				stateName = customerbranchlist.get(i).getAddress().getState()
						.trim();
			}

		}
		Phrase state2Val = new Phrase(stateName, font13);// Date 9/12/2017 By
															// Jayshree To
															// increse the
															// fontsize by one
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// stateValCell.addElement(stateVal);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String st2Co = "";
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim()
					.equalsIgnoreCase(stateName.trim())) {
				st2Co = stateList.get(i).getStateCode().trim();
				break;
			}
		}

		Phrase state2Code = new Phrase("State Code", font13bold);// Date
																	// 9/12/2017
																	// By
																	// Jayshree
																	// To
																	// increse
																	// the
																	// fontsize
																	// by one
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(st2Co, font13);// Date 9/12/2017 By
															// Jayshree To
															// increse the
															// fontsize by one
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);

		// dc
		// PdfPTable pdfState2Table = new PdfPTable(4);
		// pdfState2Table.setWidthPercentage(100);
		// try {
		// pdfState2Table.setWidths(columnStateCodeCollonWidth);
		// } catch (DocumentException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		// pdfState2Table.addCell(state2ValCell);
		// pdfState2Table.addCell(state2CodeCell);
		// pdfState2Table.addCell(colonCell);
		// pdfState2Table.addCell(state2CodeValCell);
		//
		// PdfPCell state4Cell2 = new PdfPCell(pdfState2Table);
		// state4Cell2.setBorder(0);
		// state4Cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// colonTable.addCell(state4Cell2); //
		// stateTableCell.addElement(statetable);

		PdfPCell stateCell_stateCode2 = new PdfPCell(colonTable);
		stateCell_stateCode2.setBorder(0);

		part2Table.addCell(cell2);
		/**
		 * Date 13/12/2017 Dev.By Jayshree Des.To remove the state code Comment
		 * this
		 */
		// part2Table.addCell(stateCell_stateCode2);
		// End By Jayshree
		part2Table.addCell(email_MobNumber2Cell);
		part2Table.addCell(cell22);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);
		/* Part 2 Ends */
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
		mainTable.addCell(blankCell);
		mainTable.addCell(blankCell);

		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createInvoiceDetails() {
		// TODO Auto-generated method stub
		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = { 3.5f, 0.2f, 6.8f };
		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon = new Phrase(":", font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		/**
	 * Date 1/2/2018
	 * By Jayshree
	 * Des.to change the value of reverse chage if GSTIN is not Presen
	 */
	String gstTinStr = "";
	if (invoiceentity.getGstinNumber() != null
			&& !invoiceentity.getGstinNumber().equals("")) {
		
		gstTinStr = invoiceentity.getGstinNumber().trim();
		System.out.println("Inside invoiceentity.getGstinNumber()"+gstTinStr);

	} else {
		ServerAppUtility serverAppUtility = new ServerAppUtility();
		gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,
				invoiceentity.getCustomerBranch(), "ServiceInvoice");
	}
	
	Phrase reverseCharge = new Phrase("Reverse Charge (Y/N) ", font10bold);
	PdfPCell reverseChargeCell = new PdfPCell(reverseCharge);
	reverseChargeCell.setBorder(0);
	reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase reverseChargeVal=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
	reverseChargeVal = new Phrase("NO", font10);
	}
	else{
	reverseChargeVal = new Phrase("YES", font10);
	}
	
	//End By Jayshree
		PdfPCell reverseChargeValCell = new PdfPCell(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNo = new Phrase("Invoice No", font10bold);
		PdfPCell invoiceNoCell = new PdfPCell(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceNoVal = new Phrase(invoiceentity.getCount() + "", font10);
		PdfPCell invoiceNoValCell = new PdfPCell(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase workOrder_PoNo = new Phrase("Ref No/WO", font10bold);
		PdfPCell workOrder_PoNoCell = new PdfPCell(workOrder_PoNo);
		workOrder_PoNoCell.setBorder(0);
		workOrder_PoNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase workOrder_PoNoVal = new Phrase(
				invoiceentity.getRefNumber() + "", font10);
		PdfPCell workOrder_PoNoValCell = new PdfPCell(workOrder_PoNoVal);
		workOrder_PoNoValCell.setBorder(0);
		workOrder_PoNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceNoValCell);//add By jayshree date 30-3-2018
		Phrase invoiceDate = new Phrase("Date", font10bold);
		PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase invoiceDateVal = new Phrase(sdf.format(invoiceentity
				.getInvoiceDate()), font10);
		PdfPCell invoiceDateValCell = new PdfPCell(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable invcolonTable = new PdfPTable(3);
		invcolonTable.setWidthPercentage(100);

		try {
			invcolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable pdfInvDataTable = new PdfPTable(4);
		pdfInvDataTable.setWidthPercentage(100);
		try {
			pdfInvDataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		pdfInvDataTable.addCell(invoiceNoValCell);
//		pdfInvDataTable.addCell(invoiceDateCell);
//		pdfInvDataTable.addCell(colonCell);
//		pdfInvDataTable.addCell(invoiceDateValCell);

		PdfPCell pdfPInvoiceDataCell = new PdfPCell(pdfInvDataTable);
		pdfPInvoiceDataCell.setBorder(0);
		pdfPInvoiceDataCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		invcolonTable.addCell(pdfPInvoiceDataCell); // stateTableCell.addElement(statetable);

		PdfPCell invoiceDataCell = new PdfPCell(pdfInvDataTable);
		invoiceDataCell.setBorder(0);

//		colonTable.addCell(invoiceDataCell);//Comment by jayshree 30-3-2018

		colonTable.addCell(workOrder_PoNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(workOrder_PoNoValCell);
		colonTable.setSpacingAfter(10f);

		PdfPCell pdfCell = new PdfPCell(colonTable);
		pdfCell.setBorder(0);
		// pdfCell.addElement();

		part1Table.addCell(pdfCell);

		Phrase state = new Phrase("State", font13bold);// Date 9/12/2017 By
														// Jayshree To increse
														// the fontsize by one
		PdfPCell stateCell = new PdfPCell(state);
		// stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateVal = new Phrase(comp.getAddress().getState().trim(),
				font13);// Date 9/12/2017 By Jayshree To increse the fontsize by
						// one
		PdfPCell stateValCell = new PdfPCell(stateVal);
		// stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCode = new Phrase("State Code", font13bold);// Date
																// 9/12/2017 By
																// Jayshree To
																// increse the
																// fontsize by
																// one
		PdfPCell stateCodeCell = new PdfPCell(stateCode);
		// stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase stateCodeVal = new Phrase(" ", font13);// Date 9/12/2017 By
														// Jayshree To increse
														// the fontsize by one
		PdfPCell stateCodeValCell = new PdfPCell(stateCodeVal);
		// stateCodeValCell.addElement();
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);

		// float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		statetable = new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);

		PdfPCell stateTableCell = new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);

		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		Phrase contractId = new Phrase("Contract No", font10bold);
		PdfPCell contractIdCell = new PdfPCell(contractId);
		// contractIdCell.addElement(contractId);
		contractIdCell.setBorder(0);
		contractIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase contractIdVal = new Phrase(
				invoiceentity.getContractCount() + "", font10);
		PdfPCell contractIdValCell = new PdfPCell(contractIdVal);
		// contractIdValCell.addElement(contractIdVal);
		contractIdValCell.setBorder(0);
		contractIdValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// Phrase contractDate = new Phrase("Contract Date", font10bold);
		// PdfPCell contractDateCell = new PdfPCell(contractDate);
		// // contractIdCell.addElement(contractId);
		// contractDateCell.setBorder(0);
		// contractDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		//
		//
		// PdfPTable billperiodtable = new PdfPTable(2);
		// billperiodtable.setWidthPercentage(100);
		//
		// PdfPTable billFromcolonTable = new PdfPTable(3);
		// billFromcolonTable.setWidthPercentage(100);
		// try {
		// billFromcolonTable.setWidths(columnDateCollonWidth);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// billFromcolonTable.addCell(periodstartDateCell);
		// billFromcolonTable.addCell(colonCell);
		// billFromcolonTable.addCell(periodstartDateValCell);
		//
		// PdfPTable billTocolonTable = new PdfPTable(3);
		// billTocolonTable.setWidthPercentage(100);
		// try {
		// billTocolonTable.setWidths(columnDateCollonWidth);
		// } catch (DocumentException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// billTocolonTable.addCell(periodendDateCell);
		// billTocolonTable.addCell(colonCell);
		// billTocolonTable.addCell(periodendDateValCell);

		// PdfPCell fromDate = new PdfPCell(billFromcolonTable);
		// fromDate.setBorder(0);
		// // fromDate.addElement(billFromcolonTable);
		//
		// billperiodtable.addCell(fromDate);
		// fromDate = new PdfPCell(billTocolonTable);
		// fromDate.setBorder(0);
		// // fromDate.addElement();
		// billperiodtable.addCell(fromDate);
		/**
		 *
		 */
		PdfPTable periodtable = new PdfPTable(2);
		periodtable.setWidthPercentage(100);

		PdfPTable concolonTable = new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		concolonTable.addCell(contractIdCell);//comment by jayshree 30-3-2018
//		concolonTable.addCell(colonCell);

		Phrase contractDate = new Phrase("Date", font10bold);
		PdfPCell contractDateCell = new PdfPCell(contractDate);
		contractDateCell.setBorder(0);
		contractDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		Phrase contractDateVal = new Phrase(sdf.format(con.getContractDate()),
//				font10);
//		PdfPCell contractDateValCell = new PdfPCell(contractDateVal);
//		contractDateValCell.setBorder(0);
//		contractDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable contractColTable = new PdfPTable(3);
		contractColTable.setWidthPercentage(100);

		try {
			contractColTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		PdfPTable pdfConDataTable = new PdfPTable(4);
		pdfConDataTable.setWidthPercentage(100);
		try {
			pdfConDataTable.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfConDataTable.addCell(contractIdValCell);
		pdfConDataTable.addCell(contractDateCell);
		pdfConDataTable.addCell(colonCell);
//		pdfConDataTable.addCell(contractDateValCell);

		PdfPCell pdfPConDataCell = new PdfPCell(pdfConDataTable);
		pdfPConDataCell.setBorder(0);
		pdfPConDataCell.setHorizontalAlignment(Element.ALIGN_LEFT);

//		concolonTable.addCell(pdfPConDataCell); // stateTableCell.addElement(statetable);

		PdfPCell contractDataCell = new PdfPCell(concolonTable);
		contractDataCell.setBorder(0);

		concolonTable.addCell(contractDataCell);

		Phrase startDate = new Phrase("Contract From", font10bold);
		PdfPCell startDateCell = new PdfPCell(startDate);
		startDateCell.setBorder(0);
		startDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase startDateVal = new Phrase(sdf.format(invoiceentity
				.getContractStartDate()), font10);
		PdfPCell startDateValCell = new PdfPCell(startDateVal);
		// stateValCell.addElement(stateVal);
		startDateValCell.setBorder(0);
		startDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDate = new Phrase("To", font10bold);
		PdfPCell endDateCell = new PdfPCell(endDate);
		endDateCell.setBorder(0);
		endDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase endDateVal = new Phrase(sdf.format(invoiceentity
				.getContractEndDate()), font10);
		PdfPCell endDateValCell = new PdfPCell(endDateVal);
		endDateValCell.setBorder(0);
		endDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(startDateCell);
		colonTable.addCell(colonCell);
		PdfPTable pdfContractPeriodTable = new PdfPTable(4);
		pdfContractPeriodTable.setWidthPercentage(100);
		try {
			pdfContractPeriodTable
					.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfContractPeriodTable.addCell(startDateValCell);
		pdfContractPeriodTable.addCell(endDateCell);
		pdfContractPeriodTable.addCell(colonCell);
		pdfContractPeriodTable.addCell(endDateValCell);

		PdfPCell state4Cell = new PdfPCell(pdfContractPeriodTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell); // stateTableCell.addElement(statetable);

		PdfPCell periodTableCell = new PdfPCell(colonTable);
		periodTableCell.setBorder(0);
		/**
		 * Billing Period
		 */
		Phrase periodstartDate = new Phrase("Billing From", font10bold);
		PdfPCell periodstartDateCell = new PdfPCell(periodstartDate);
		// periodstartDateCell.addElement(periodstartDate);
		periodstartDateCell.setBorder(0);
		periodstartDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan adde this code
		Phrase periodstartDateVal = null;
		if (invoiceentity.getBillingPeroidFromDate() != null) {
			periodstartDateVal = new Phrase(sdf.format(invoiceentity
					.getBillingPeroidFromDate()), font10);
		} else {
			periodstartDateVal = new Phrase(" ", font10);
		}

		PdfPCell periodstartDateValCell = new PdfPCell(periodstartDateVal);
		// periodstartDateValCell.addElement(periodstartDateVal);
		periodstartDateValCell.setBorder(0);
		periodstartDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase periodendDate = new Phrase("To", font10bold);
		PdfPCell periodendDateCell = new PdfPCell(periodendDate);
		// periodendDateCell.addElement(periodendDate);
		periodendDateCell.setBorder(0);
		periodendDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// rohan adde this code
		Phrase periodendDateVal = null;
		if (invoiceentity.getBillingPeroidToDate() != null) {
			periodendDateVal = new Phrase(sdf.format(invoiceentity
					.getBillingPeroidToDate()), font10);
		} else {
			periodendDateVal = new Phrase(" ", font10);
		}

		PdfPCell periodendDateValCell = new PdfPCell(periodendDateVal);
		// periodendDateValCell.addElement(periodendDateVal);
		periodendDateValCell.setBorder(0);
		periodendDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(periodstartDateCell);
		colonTable.addCell(colonCell);
		PdfPTable pdfBillingPeriodTable = new PdfPTable(4);
		pdfBillingPeriodTable.setWidthPercentage(100);
		try {
			pdfBillingPeriodTable
					.setWidths(columnContractPeriodDateCodeCollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		pdfBillingPeriodTable.addCell(periodstartDateValCell);
		pdfBillingPeriodTable.addCell(periodendDateCell);
		pdfBillingPeriodTable.addCell(colonCell);
		pdfBillingPeriodTable.addCell(periodendDateValCell);

		state4Cell = new PdfPCell(pdfBillingPeriodTable);
		state4Cell.setBorder(0);
		state4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(state4Cell);

		// periodTableCell.addElement();

		PdfPCell concolonTableCell = new PdfPCell(concolonTable);
		concolonTableCell.setBorder(0);
		// concolonTableCell.addElement();

		PdfPCell billperiodtableCell = new PdfPCell(colonTable);
		billperiodtableCell.setBorder(0);
		// billperiodtableCell.addElement();

		part2Table.addCell(concolonTableCell);
		if (con.isContractRate()) {
			part2Table.addCell(periodTableCell);
		}else{
			System.out.println("ash :::::inside else");
		   part2Table.addCell(billperiodtableCell);
		}
		PdfPCell part1Cell = new PdfPCell(part1Table);
		part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement();

		mainTable.addCell(part1Cell);

		part1Cell = new PdfPCell(part2Table);
		// part1Cell.setBorderWidthRight(0);
		// part1Cell.addElement();
		mainTable.addCell(part1Cell);

		// mainTable.addCell(blankCell);
		// mainTable.addCell(blankCell);
		Phrase billingAddress = new Phrase("Billing Address", font8bold);
		// Paragraph billingpara=new Paragraph();
		// billingpara.add(billingAddress);
		// billingpara.setAlignment(Element.ALIGN_CENTER);
		// billingpara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell billAdressCell = new PdfPCell(billingAddress);
		// billAdressCell.addElement(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// billAdressCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);

		mainTable.addCell(billAdressCell);
		Phrase serviceaddress = new Phrase("Service Address", font8bold);
		// Paragraph servicepara=new Paragraph();
		// servicepara.add(serviceaddress);
		// servicepara.setAlignment(Element.ALIGN_CENTER);
		// servicepara.setAlignment(Element.ALIGN_MIDDLE);
		PdfPCell serviceCell = new PdfPCell(serviceaddress);
		// serviceCell.addElement(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		// serviceCell.setHorizontalAlignment(Element.ALIGN_MIDDLE);
		mainTable.addCell(serviceCell);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createProductDescription() {

		/**
		 * Date : 27-07-2017 By ANIL
		 */
		if (!productDescFlag) {
			return;
		}
		/**
		 * End
		 */

		PdfPTable prodDescriptionTbl = new PdfPTable(2);
		prodDescriptionTbl.setWidthPercentage(100);

		try {
			prodDescriptionTbl.setWidths(new float[] { 40, 60 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase prodDescriptionVal = new Phrase("", font8);
		String prodDescriptionValue = "";
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			Phrase productIdLbl = new Phrase("Product Id : "
					+ invoiceentity.getSalesOrderProducts().get(i).getProdId(),
					font10bold);
			PdfPCell productIdLblCell = new PdfPCell(productIdLbl);
			productIdLblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			productIdLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productIdLblCell.setBorder(0);
			prodDescriptionTbl.addCell(productIdLblCell);

			String prodNameValue = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();

			Phrase prodNameLbl = new Phrase("Product Name : " + prodNameValue,
					font10bold);
			PdfPCell prodlblCell = new PdfPCell(prodNameLbl);
			prodlblCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodlblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			prodlblCell.setBorder(0);

			prodDescriptionTbl.addCell(prodlblCell);

			prodDescriptionValue = "";

			if (invoiceentity.getSalesOrderProducts().get(i).getProdDesc1() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc1()
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getProdDesc2();
			} else if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdDesc1() == null
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc2();
			} else if (invoiceentity.getSalesOrderProducts().get(i)
					.getProdDesc1() != null
					&& !invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc1().equals("")
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2() == null
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getProdDesc2().equals("")) {
				prodDescriptionValue = invoiceentity.getSalesOrderProducts()
						.get(i).getProdDesc1();
			} else {
				prodDescriptionValue = "";

			}

			prodDescriptionVal = new Phrase("" + prodDescriptionValue, font8);
		}
		Paragraph value = new Paragraph(prodDescriptionVal);
		value.setAlignment(Element.ALIGN_LEFT);

		try {
			/**
			 * Date :27-07-2017 By ANIL If no product description is found no
			 * table will be printed on pdf
			 */
			if (!prodDescriptionValue.equals("")) {
				document.add(Chunk.NEXTPAGE);
				document.add(prodDescriptionTbl);
				document.add(value);
			}
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Added description code End Here
	 */

	// private void createStaticHeader(String preprintStatus) {
	// // TODO Auto-generated method stub
	// PdfPTable mainTable=new PdfPTable(2);
	// mainTable.setWidthPercentage(100);
	// try {
	// mainTable.setWidths(columnMoreLeftHeaderWidths);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// // rohan added this code for printing INVOICE heading when process config
	// is off
	// Phrase pdfHeading=null;
	// if(preprintStatus.equalsIgnoreCase("Plane")){
	// if(invoiceentity.getInvoiceType().equalsIgnoreCase("Proforma Invoice")){
	// pdfHeading=new Phrase("Proform Invoice",font14bold);
	// }
	// else{
	// pdfHeading=new Phrase("Tax Invoice",font14bold);
	// }
	//
	// }
	// else{
	// pdfHeading=new Phrase(" ",font14bold);
	// }
	//
	// Paragraph invPara=new Paragraph();
	// invPara.add(pdfHeading);
	// invPara.setAlignment(Element.ALIGN_RIGHT);
	// PdfPCell pdfHeadingCell=new PdfPCell();
	// pdfHeadingCell.addElement(invPara);
	// pdfHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// mainTable.addCell(pdfHeadingCell);
	//
	// PdfPTable partialTable=new PdfPTable(2);
	// partialTable.setWidthPercentage(100);
	// try {
	// partialTable.setWidths(columnMoreRightCheckBoxWidths);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// Phrase blankPhrase=new Phrase(" ",font10);
	// PdfPCell blankCell=new PdfPCell(blankPhrase);
	//
	// // blankCell.addElement(blankPhrase);
	// // blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// /*Just to create little spacing between boxes*/
	// // PdfPTable pdfTable=new PdfPTable(1);
	// // pdfTable.addCell(blankCell);
	// // pdfTable.setWidthPercentage(100);
	// //
	// // PdfPCell blank2Cell=new PdfPCell();
	// // blank2Cell.addElement(pdfTable);
	// // blank2Cell.setBorder(0);
	// // blank2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//
	// Phrase stat1Phrase=new Phrase("Original for Receipient",font10);
	// PdfPCell stat1PhraseCell=new PdfPCell(stat1Phrase);
	// // stat1PhraseCell.addElement(stat1Phrase);
	// stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase stat2Phrase=new
	// Phrase("Duplicate for Supplier/Transporter",font10);
	// PdfPCell stat2PhraseCell=new PdfPCell(stat2Phrase);
	// // stat2PhraseCell.addElement(stat2Phrase);
	// stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// Phrase stat3Phrase=new Phrase("Triplicate for Supplier",font10);
	// PdfPCell stat3PhraseCell=new PdfPCell(stat3Phrase);
	// // stat3PhraseCell.addElement(stat3Phrase);
	// stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat1PhraseCell);
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat2PhraseCell);
	// partialTable.addCell(blankCell);
	// partialTable.addCell(stat3PhraseCell);
	//
	// PdfPCell pdfPCell=new PdfPCell();
	// pdfPCell.addElement(partialTable);
	// // pdfPCell.setBorder(0);
	// pdfPCell.setBorderWidthLeft(0);
	// pdfPCell.setBorderWidthBottom(0);
	// pdfPCell.setBorderWidthTop(0);
	//
	// mainTable.addCell(pdfPCell);
	// try {
	// document.add(mainTable);
	// } catch (DocumentException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	private void createHeader() {

		// Date 20/11/2017 By Jayshree
		// to add the logo in Table
		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}
		/**
		 * Ends For Jayshree
		 */

		/**
		 * Developer : Jayshree Date : 21 Nov 2017 Description : Logo position
		 * adjustment
		 */
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		/**
		 * Ends for Jayshree
		 */
		Phrase companyName = null;
		if (comp != null) {
			companyName = new Phrase(comp.getBusinessUnitName().trim(),
					font16bold);
		}

		Paragraph companyNamepara = new Paragraph();
		companyNamepara.add(companyName);
		/**
		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
		 * alignment
		 */

		if (checkheaderLeft == true) {
			companyNamepara.setAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyNamepara.setAlignment(Element.ALIGN_RIGHT);
		} else {
			companyNamepara.setAlignment(Element.ALIGN_CENTER);
		}

		// companyNamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyNameCell = new PdfPCell();
		companyNameCell.addElement(companyNamepara);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyAddr = null;
		if (comp != null) {
			companyAddr = new Phrase(comp.getAddress().getCompleteAddress()
					.trim(), font12);
		}
		Paragraph companyAddrpara = new Paragraph();
		companyAddrpara.add(companyAddr);
		/**
		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
		 * alignment
		 */

		if (checkheaderLeft == true) {
			companyAddrpara.setAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyAddrpara.setAlignment(Element.ALIGN_RIGHT);
		} else {
			companyAddrpara.setAlignment(Element.ALIGN_CENTER);
		}

		// companyAddrpara.setAlignment(Element.ALIGN_CENTER);
		/**
		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
		 * alignment
		 */
		PdfPCell companyAddrCell = new PdfPCell(companyAddrpara);
		// companyAddrCell.addElement();
		companyAddrCell.setBorder(0);
		if (checkheaderLeft == true) {
			companyAddrCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyAddrCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		/**
		 * Ends
		 */

		// companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase companyGSTTIN = null;
		String gstinValue = "";
		if (UniversalFlag) {
			if (con.getGroup().equalsIgnoreCase(
					"Universal Pest Control Pvt. Ltd.")) {

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = comp.getArticleTypeDetails().get(i)
								.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
						break;
					}
				}

				for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
					if (!comp.getArticleTypeDetails().get(i)
							.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstinValue = gstinValue
								+ ","
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeName()
								+ " : "
								+ comp.getArticleTypeDetails().get(i)
										.getArticleTypeValue().trim();
					}
				}
			}
		} else {

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
					break;
				}
			}

			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				if (!comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = gstinValue
							+ ","
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeName()
							+ " : "
							+ comp.getArticleTypeDetails().get(i)
									.getArticleTypeValue().trim();
				}
			}
		}

		if (!gstinValue.equals("")) {
			companyGSTTIN = new Phrase(gstinValue, font12bold);
		}

		Paragraph companyGSTTINpara = new Paragraph();
		companyGSTTINpara.add(companyGSTTIN);
		companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companyGSTTINCell = new PdfPCell();
		companyGSTTINCell.addElement(companyGSTTINpara);
		companyGSTTINCell.setBorder(0);
		companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		/**
		 * Date 12/1/2018 Dev.Jayshree Des.To add the company Email And Branch
		 * Email
		 */
		String branchmail = "";
		ServerAppUtility serverApp = new ServerAppUtility();

		if (checkEmailId == true) {
			branchmail = serverApp.getBranchEmail(comp,
					invoiceentity.getBranch());
			System.out.println("server method " + branchmail);

		} else {
			branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
			System.out.println("server method 22" + branchmail);
		}

		String email = null;
		if (branchmail != null) {
			email = "Email : " + branchmail;
		} else {
			email = "Email : ";
		}

		/**
		 * By Jayshree Date 28/12/2017 to handle null condition
		 */

		String website = "";
		if (comp.getWebsite() == null || comp.getWebsite().equals("")) {
			website = " ";
		} else {

			website = "Website : " + comp.getWebsite();
		}
		/**
		 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
		 * add the LandLine no.
		 */

		/**
		 * Comment by jayshree the below code
		 */
		// if (comp.getCellNumber2() != 0) {
		// number = "Phone " + comp.getCellNumber1() + ","
		// + comp.getCellNumber2();
		// } else if (comp.getCellNumber1() != 0) {
		// number = "Phone " + comp.getCellNumber1();
		// } else {
		// number = "Phone ";
		// }
		// String landline = "";
		// if (comp.getLandline() != 0) {
		// System.out.println("comp.getLandline()" + comp.getLandline());
		// landline = "Landline  " + comp.getLandline();
		// } else {
		// landline = "";
		// }
		/**
		 * Above code is Commented bY Jayshree
		 */
		/**
		 * Date 3/1/2018 By Jayshree Des.Change the title Mobile to Phone And
		 * add the LandLine no.
		 */
		String number = "";
		String landline = "";

		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			System.out.println("pn11");
			number = comp.getCellNumber1() + "";
		}
		if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
			if (!number.trim().isEmpty()) {
				number = number + " , " + comp.getCellNumber2() + "";
			} else {
				number = comp.getCellNumber2() + "";
			}
			System.out.println("pn33" + number);
		}
		if (comp.getLandline() != 0 && comp.getLandline() != null) {
			if (!number.trim().isEmpty()) {
				number = number + " , " + comp.getLandline() + "";
			} else {
				number = comp.getLandline() + "";
			}
			System.out.println("pn44" + number);
		}

		PdfPCell companyEmailandwebCell = null;
		if (number != null && !number.trim().isEmpty()) {
			companyEmailandwebCell = new PdfPCell(new Phrase(email + " "
					+ "Phone " + number, font11));
		} else {
			companyEmailandwebCell = new PdfPCell(new Phrase(email, font11));
		}

		// PdfPCell companyEmailandwebCell = new PdfPCell(new Phrase(email + " "
		// + number + " " + landline, font11));
		/* End By Jayshree */
		// companyGSTTINCell.addElement(companyGSTTINpara);
		companyEmailandwebCell.setBorder(0);
		if (checkheaderLeft == true) {
			companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		// companyEmailandwebCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		// Date 16/11/2017 By jayshree add the webside in header section

		PdfPCell companymob = new PdfPCell(new Phrase("" + website, font11));
		// companyGSTTINCell.addElement(companyGSTTINpara);
		companymob.setBorder(0);
		/**
		 * Date 15/1/2018 dev.By jayshree Des. To set the company Heading info
		 * alignment
		 */
		if (checkheaderLeft == true) {
			companymob.setHorizontalAlignment(Element.ALIGN_LEFT);
		} else if (checkheaderRight == true) {
			companymob.setHorizontalAlignment(Element.ALIGN_RIGHT);
		} else {
			companymob.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		/**
		 * Ends
		 */
		// companymob.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pdfPTable = new PdfPTable(1);
		// pdfPTable.setWidthPercentage(100);
		pdfPTable.addCell(companyNameCell);
		pdfPTable.addCell(companyAddrCell);
		pdfPTable.addCell(companyEmailandwebCell);
		pdfPTable.addCell(companymob);

		/**
		 * Developer:Jayshree Date 21/11/2017 Description:changes are done to
		 * add the logo and website at proper position
		 */
		PdfPTable mainheader = new PdfPTable(2);
		mainheader.setWidthPercentage(100);

		try {
			mainheader.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		if (imageSignCell != null) {
			PdfPCell leftCell = new PdfPCell(logoTab);
			leftCell.setBorder(0);
			mainheader.addCell(leftCell);

			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			mainheader.addCell(rightCell);
		} else {
			PdfPCell rightCell = new PdfPCell(pdfPTable);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			mainheader.addCell(rightCell);
		}

		try {
			document.add(mainheader);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// rohan added this code
		float[] myWidth = { 1, 3, 20, 17, 3, 30, 17, 3, 20, 1 };
		PdfPTable mytbale = new PdfPTable(10);
		mytbale.setWidthPercentage(100f);
		mytbale.setSpacingAfter(5f);
		mytbale.setSpacingBefore(5f);

		try {
			mytbale.setWidths(myWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Phrase myblank = new Phrase(" ", font10);
		PdfPCell myblankCell = new PdfPCell(myblank);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase myblankborderZero = new Phrase(" ", font10);
		PdfPCell myblankborderZeroCell = new PdfPCell(myblankborderZero);
		// stat1PhraseCell.addElement(stat1Phrase);
		myblankborderZeroCell.setBorder(0);
		myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase stat1Phrase = new Phrase("Original for Receipient", font10);
		PdfPCell stat1PhraseCell = new PdfPCell(stat1Phrase);
		stat1PhraseCell.setBorder(0);
		stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stat1PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase stat2Phrase = new Phrase("Duplicate for Supplier/Transporter",
				font10);
		PdfPCell stat2PhraseCell = new PdfPCell(stat2Phrase);
		stat2PhraseCell.setBorder(0);
		stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stat2PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase stat3Phrase = new Phrase("Triplicate for Supplier", font10);
		PdfPCell stat3PhraseCell = new PdfPCell(stat3Phrase);
		stat3PhraseCell.setBorder(0);
		stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stat3PhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		// mytbale.addCell(stat1PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		// mytbale.addCell(stat2PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);
		// mytbale.addCell(myblankCell);
		// mytbale.addCell(stat3PhraseCell);
		// mytbale.addCell(myblankborderZeroCell);

		PdfPTable tab = new PdfPTable(1);
		tab.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(mytbale);
		tab.addCell(cell);
		// try {
		// // document.add(tab);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		// ends here
		String titlepdf = "";

		if (AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity
				.getInvoiceType().trim())
				|| invoiceentity.getInvoiceType().trim()
						.equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf = "Proforma Invoice";
		else
			titlepdf = "Tax Invoice";
		/**
		 * @author Anil @since 01-10-2021
		 */
		titlepdf=pdfUtility.getInvoiceTitle(invoiceentity, branchDt, titlepdf);
		logger.log(Level.SEVERE,"After getInvoiceTitle Method : "+titlepdf +" | "+pdfUtility.printBankDetailsFlag);

		Phrase titlephrase = new Phrase(titlepdf, font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);

		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);

		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	/** 23-10-2017 sagar sore **/
	public void createPdfForEmailGST(Invoice invoiceDetails,
			Company companyEntity, Customer custEntity,
			Contract contractEntity, BillingDocument billingEntity,
			List<CustomerBranchDetails> custbranchlist,
			ServiceGSTInvoice invpdf, Document document)// By Jayshree
	{

		Invoice invoiceentity = invoiceDetails;
		long count = invoiceentity.getId();
		logger.log(Level.SEVERE, " Count: " + count);
		SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));

		boolean flag = false;
		try {
			flag = invoiceentity.getInvoiceDate().after(
					sdf.parse("30 Jun 2017"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("invoice Date " + invoiceentity.getInvoiceDate());

		System.out.println("SINGLE CONTRACT INVOICE");

		String preprintStatus = "plane";
		System.out.println("ppppppppppppppppppooooooooooo" + count);
		invpdf.setInvoice(count);
		invpdf.createPdf(preprintStatus);// By jayshree
		document.close();

	}

	/**
	 * Date 21-01-2019 By Vijay
	 * Des :- for HSN Code getting from service product master common method
	 * @param productCode
	 * @return
	 */
	private String getHSNCode(String productCode) {
		String hsnCode = "";
		ServiceProduct serviceProduct = ofy()
				.load()
				.type(ServiceProduct.class)
				.filter("companyId", comp.getCompanyId())
				.filter("productCode", productCode)
				.first().now();
		
		if (serviceProduct!=null && serviceProduct.getHsnNumber() != null) {
			hsnCode = serviceProduct.getHsnNumber();
		}
		return hsnCode;
	}
	
	/**
	 * Date 21-01-2019 By Vijay
	 * Des :- for SAC Code getting from service product master common method
	 * @param productCode
	 * @return
	 */
	private String getSACCode(String productCode) {
		
		String sacCode = "";
		ItemProduct	itemproduct = ofy().load().type(ItemProduct.class)
				.filter("companyId", comp.getCompanyId())
				.filter("productCode",productCode)
				.first().now();
		
		if (itemproduct!=null && itemproduct.getHsnNumber() != null) {
			sacCode = itemproduct.getHsnNumber();
		}
		return sacCode;
	}
	
	/**
	 * Date 21-01-2019 By Vijay
	 * Des :- checking sales and service bills merged invoice or not
	 * @param productCode
	 * @return
	 */
	private boolean getIsthisSalesServiceBillMerged() {
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {
			if(invoiceentity.getSalesOrderProducts().get(i).getPrduct() instanceof ItemProduct){
				return true;
			}
		}
		return false;
	}	
	/**
	 * ends here
	 */
}
