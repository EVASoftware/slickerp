package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

@SuppressWarnings("serial")
public class CreateServicePdfServlet extends HttpServlet {

 @Override
 protected void doGet(HttpServletRequest request,
   HttpServletResponse response) throws ServletException, IOException {
  
	 response.setContentType("application/pdf");
  

	 String compId = request.getParameter("compId");
		long companyId=Long.parseLong(compId);
		
		Company company =ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		
		/**
		 * Date 15-05-2018 
		 * Developer: Vijay
		 * Des :- below process config for IMP And Fumigation service pdf wthout customer cell no and customer address
		 * 
		 */
		 if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","EnableServicePdfWithoutaddressAndCellNumber", companyId)){
			 try {
				   ServicePdfForIMP pdf=new ServicePdfForIMP();
				   pdf.document=new Document(PageSize.A4.rotate());//Date16/11/2017 by jayshree changes 
						//are made to print the services in landscape mode
				   Document document = pdf.document;
				   PdfWriter.getInstance(document, response.getOutputStream());
				   document.open();
				   pdf.createPdf();
				   document.close();
				   ServicePdfForIMP.servicearaylist=null;
				  } 
				  catch (DocumentException e) {
					  e.printStackTrace();
				  }
				  
		 }else{
			 
			 try {
				   ServicePdf pdf=new ServicePdf();
				   pdf.document=new Document(PageSize.A4.rotate());//Date16/11/2017 by jayshree changes 
						//are made to print the services in landscape mode
				   Document document = pdf.document;
				   PdfWriter writer =PdfWriter.getInstance(document, response.getOutputStream());
				   
				   
				   //Ashwini Patil Date:18-03-2024 added preprint logic
				   String preprintStatus=request.getParameter("preprint");
				   if(company!=null&&company.getUploadHeader()!=null && company.getUploadFooter()!=null){
			 		   document.setPageSize(PageSize.A4);
				 	   document.setMargins(30, 30, 120, 100);
				 	   document.setMarginMirroring(false);
				 	   
			 		  HeaderFooterPageEvent event = new HeaderFooterPageEvent(company,"",preprintStatus);
				 	   writer.setPageEvent(event);
			 	   }
				   
				   document.open();
				   
				   if(preprintStatus.contains("yes")){
					   
					   pdf.createPdf(preprintStatus);
				   }
				   else  if(preprintStatus.contains("no")){
					   
					   pdf.createPdf(preprintStatus);
				   }
				   else{
					   pdf.createPdf(preprintStatus);
				   }
				   
				   document.close();
				   ServicePdf.servicearaylist=null;
				  } 
				  catch (DocumentException e) {
					  e.printStackTrace();
				  }
			 
		 }
   }

}
