package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CreateWorkOrderPdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1263156673455548917L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			WorkOrder wo = ofy().load().type(WorkOrder.class).id(count).now();
			
			WorkOrderPdf wopdf = new WorkOrderPdf();
			
			wopdf.document = new Document();
			Document document = wopdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			if(wo.getStatus().equals("Cancelled")){
				writer.setPageEvent(new PdfCancelWatermark());
			}else															
			if(!wo.getStatus().equals("Approved")){	
				 writer.setPageEvent(new PdfWatermark());
			} 
																		
			
			 
			 document.open();
			
			wopdf.setWorkOrder(count);
			wopdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	
}
