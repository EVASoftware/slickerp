package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;

public class HygeiaPurchaseOrderPdf {

	Logger logger = Logger.getLogger("Name of logger");

	ProcessConfiguration processConfig;
	boolean hygeiaPest = false;

	public Document document;

	Company comp;
	Vendor vendor;
	Phrase chunk, total;
	String str, sum;
	ItemProduct ip;
	List<ItemProduct> itemList;
	PurchaseOrder po;

	Phrase blank;
	Phrase col;

	double ttl, vatAmt;
	long roundAmt;

	PdfPCell pdfsr, pdfdesc, pdfitem, pdfqty, pdfrate, pdfamt, pdfvatAmt,
			pdfvatAmt1, pdfvatAmt2;
	// double ttlAmt,tQty,tRate;

	float[] colWidth = { 20f, 80f };

	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul, font9boldul;

	public HygeiaPurchaseOrderPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setpdfpurchaseorder(Long count) {

		po = ofy().load().type(PurchaseOrder.class).id(count).now();

		if (po.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", po.getCompanyId()).first().now();

		if (po.getCompanyId() == null)
			vendor = ofy().load().type(Vendor.class).first().now();
		else
			vendor = ofy()
					.load()
					.type(Vendor.class)
					.filter("companyId",
							po.getVendorDetails().get(0).getCompanyId())
					.filter("count", po.getVendorDetails().get(0).getVendorId())
					.first().now();

		itemList = new ArrayList<ItemProduct>();
		for (int i = 0; i < po.getProductDetails().size(); i++) {
			List<ItemProduct> iplist = ofy()
					.load()
					.type(ItemProduct.class)
					.filter("companyId", po.getCompanyId())
					.filter("count",
							po.getProductDetails().get(i).getProductID())
					.list();

			if (iplist.size() != 0) {
				itemList.addAll(iplist);
			}
		}
		System.out.println("Size of ITEM LIST=============" + itemList.size());

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		// ***********Process config code*******************
		if (po.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", po.getCompanyId())
					.filter("processName", "PurchaseOrder")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						hygeiaPest = true;
					}
				}
			}
		}

		String amountInWord = ServiceInvoicePdf.convert(po.getGtotal());

	}

	public void createPdf() {

		createLogo(document, comp);
		createBlankHeading();
		createTitle();
		createCustAndCompanyInfo();
		createKindAttendentInfo();
		createProductTable();
		createTotalAmtTable();
		createProductTaxTable();
		createRoundValueTable();
		createGrandTotalTable();
		createInWordsDetail();
		createTermsAndConditions();
		createSignatureInfo();

	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createBlankHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createTitle() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase purchase = new Phrase("PURCHASE ORDER", font12bold);
		PdfPCell percell = new PdfPCell(purchase);
		percell.setBorder(0);
		percell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pertable = new PdfPTable(1);
		pertable.setWidthPercentage(100);
		pertable.addCell(percell);
		pertable.setSpacingAfter(2f);
		// pertable.addCell(blcell);

		PdfPCell cell = new PdfPCell(pertable);
		// cell.setBorder(0);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustAndCompanyInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// PdfPTable table = new PdfPTable(3);
		//
		// try {
		// table.setWidths(new float[] { 45, 20, 30 });
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
		// table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);

		Phrase customer = null;
		if (po.getVendorDetails().get(0).getVendorName() != null) {
			customer = new Phrase("To, " + "\n" + "\n"
					+ po.getVendorDetails().get(0).getVendorName(), font10bold);
		} else {
			customer = new Phrase(" ", font10bold);
		}
		PdfPCell customerCell = new PdfPCell(customer);
		customerCell.setBorder(0);
		customerCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd = "";
		String custFullAdd = "";

		if (vendor.getPrimaryAddress() != null
				|| !vendor.getPrimaryAddress().equals("")) {
			System.out.println("INSIDE VENDOR ADDRESS..............");

			if (vendor.getPrimaryAddress().getAddrLine2() != null
					|| !vendor.getPrimaryAddress().getAddrLine2().equals("")) {
				if (vendor.getPrimaryAddress().getLandmark() != null
						|| !vendor.getPrimaryAddress().getLandmark().equals("")) {
					custAdd = vendor.getPrimaryAddress().getAddrLine1() + ","
							+ vendor.getPrimaryAddress().getAddrLine2() + ","
							+ vendor.getPrimaryAddress().getLandmark();
				} else {
					custAdd = vendor.getPrimaryAddress().getAddrLine1() + ","
							+ vendor.getPrimaryAddress().getAddrLine2();
				}
			} else {
				if (vendor.getPrimaryAddress().getLandmark() != null
						|| !vendor.getPrimaryAddress().getLandmark().equals("")) {
					custAdd = vendor.getPrimaryAddress().getAddrLine1() + ","
							+ vendor.getPrimaryAddress().getLandmark();
				} else {
					custAdd = vendor.getPrimaryAddress().getAddrLine1();
				}
			}

			if (vendor.getPrimaryAddress().getLocality() != null
					|| !vendor.getPrimaryAddress().getLocality().equals("")) {

				custFullAdd = custAdd + "," + "\n"
						+ vendor.getPrimaryAddress().getCountry() + ","
						+ vendor.getPrimaryAddress().getState() + ","
						+ vendor.getPrimaryAddress().getCity() + " "
						+ vendor.getPrimaryAddress().getPin();
			}

		}

		Phrase custAddInfo = new Phrase(custFullAdd, font9bold);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setBorder(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(customerCell);
		leftTable.addCell(custAddInfoCell);
		leftTable.addCell(blcell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// // *************center table************
		// PdfPTable centerTable = new PdfPTable(1);
		// centerTable.setWidthPercentage(100);
		//
		// centerTable.addCell(blcell);
		// centerTable.addCell(blcell);
		// centerTable.addCell(blcell);
		// centerTable.addCell(blcell);
		// centerTable.addCell(blcell);
		//
		// PdfPCell centerCell = new PdfPCell(centerTable);
		// centerCell.setBorder(0);

		// *************right table************
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100);

		/********************/
		PdfPTable infoTable = new PdfPTable(2);
		infoTable.setWidthPercentage(100f);

		Phrase date = new Phrase("DATE : ", font9bold);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setBorder(0);
		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		infoTable.addCell(dateCell);

		Phrase dt = null;
		if (po.getPODate() != null) {
			dt = new Phrase("" + fmt.format(po.getPODate()), font9);
		} else {
			dt = new Phrase("" + font9);
		}
		PdfPCell dtCell = new PdfPCell(dt);
		// dtCell.setBorder(0);
		dtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infoTable.addCell(dtCell);

		infoTable.addCell(blcell);
		infoTable.addCell(blcell);

		Phrase pono = new Phrase("PO. NO. : ", font9bold);
		PdfPCell ponoCell = new PdfPCell(pono);
		ponoCell.setBorder(0);
		ponoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		infoTable.addCell(ponoCell);

		Phrase pno = null;
		if (po.getCount() != 0) {
			pno = new Phrase("" + po.getCount(), font9);
		} else {
			pno = new Phrase("" + font9);
		}
		PdfPCell pnoCell = new PdfPCell(pno);
		// dtCell.setBorder(0);
		pnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infoTable.addCell(pnoCell);

		infoTable.addCell(blcell);
		infoTable.addCell(blcell);

		Phrase refno = new Phrase("REF. NO. : ", font9bold);
		PdfPCell refnoCell = new PdfPCell(refno);
		refnoCell.setBorder(0);
		refnoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		infoTable.addCell(refnoCell);

		Phrase rno = null;
		if (po.getRefOrderNO() != null) {
			rno = new Phrase("" + po.getRefOrderNO(), font9);
		} else {
			rno = new Phrase("" + font9);
		}
		PdfPCell rnoCell = new PdfPCell(rno);
		// dtCell.setBorder(0);
		rnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infoTable.addCell(rnoCell);

		infoTable.addCell(blcell);
		infoTable.addCell(blcell);

		PdfPCell infoCell = new PdfPCell(infoTable);
		infoCell.setBorder(0);

		/*************** company info ********************/
		Phrase abc = new Phrase("SHIP TO : ", font10bold);
		PdfPCell abcCell = new PdfPCell(abc);
		abcCell.setBorder(0);
		abcCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("" + comp.getBusinessUnitName(), font10bold);
		} else {
			company = new Phrase(" ", font10bold);
		}
		PdfPCell compcell = new PdfPCell(company);
		compcell.setBorder(0);
		compcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ********address*******
		String compAdd1 = "";
		String compFullAdd1 = "";

		if (comp.getAddress() != null) {

			if (!comp.getAddress().getAddrLine2().equals("")) {
				if (!comp.getAddress().getLandmark().equals("")) {
					compAdd1 = comp.getAddress().getAddrLine1() + "\n"
							+ comp.getAddress().getAddrLine2() + "\n"
							+ comp.getAddress().getLandmark();
				} else {
					compAdd1 = comp.getAddress().getAddrLine1() + "\n"
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (!comp.getAddress().getLandmark().equals("")) {
					compAdd1 = comp.getAddress().getAddrLine1() + "\n"
							+ comp.getAddress().getLandmark() + ",";
				} else {
					compAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (!comp.getAddress().getLocality().equals("")) {
				compFullAdd1 = compAdd1 + comp.getAddress().getCountry() + ","
						+ comp.getAddress().getState() + ","
						+ comp.getAddress().getCity() + "\n"
						+ comp.getAddress().getLocality() + " "
						+ comp.getAddress().getPin();

			} else {
				compFullAdd1 = compAdd1 + "," + comp.getAddress().getCountry()
						+ "," + comp.getAddress().getState() + ","
						+ comp.getAddress().getCity() + " "
						+ comp.getAddress().getPin();
			}
		}

		Phrase compAddInfo1 = new Phrase("" + compFullAdd1, font9bold);
		PdfPCell compAddInfoCell = new PdfPCell(compAddInfo1);
		compAddInfoCell.setBorder(0);
		compAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase contact = null;
		if (comp.getLandline() != 0) {
			contact = new Phrase("Phone No. " + comp.getLandline(), font9bold);
		} else {
			contact = new Phrase("", font9bold);
		}
		PdfPCell contactCell = new PdfPCell(contact);
		contactCell.setBorder(0);
		contactCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		rightTable.addCell(infoCell);
		rightTable.addCell(abcCell);
		rightTable.addCell(compcell);
		rightTable.addCell(compAddInfoCell);
		rightTable.addCell(contactCell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		// table.addCell(centerCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);
		// cell.setBorderWidthLeft(0);
		// cell.setBorderWidthRight(0);
		// cell.setBorderWidthTop(0);
		// cell.setBorderWidthBottom(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createKindAttendentInfo() {

		PdfPTable attenTable = new PdfPTable(1);
		attenTable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase atten = null;
		if (vendor.getPointOfContact().getFullname() != null) {
			atten = new Phrase("Kind Attn. :  " + "Mr. "
					+ vendor.getPointOfContact().getFullname(), font10bold);
		}
		PdfPCell attencell = new PdfPCell(atten);
		attencell.setBorder(0);
		attencell.setHorizontalAlignment(Element.ALIGN_LEFT);

		attenTable.addCell(attencell);
		attenTable.addCell(blcell);
		attenTable.addCell(blcell);

		PdfPCell cell = new PdfPCell(attenTable);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createProductTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable table = new PdfPTable(6);

		try {
			table.setWidths(new float[] { 10, 45, 10, 10, 10, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase srno = new Phrase("SR.No.", font1);
		Phrase name = new Phrase("DISCRIPTION", font1);
		Phrase item = new Phrase("ITEM", font1);
		Phrase qty = new Phrase("QTY", font1);
		Phrase rate = new Phrase("RATE", font1);
		Phrase amt = new Phrase("AMOUNT", font1);

		PdfPCell srnocell = new PdfPCell(srno);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell namecell = new PdfPCell(name);
		namecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell itemcell = new PdfPCell(item);
		itemcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell qtycell = new PdfPCell(qty);
		qtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell ratecell = new PdfPCell(rate);
		ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell amtcell = new PdfPCell(amt);
		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		table.addCell(srnocell);
		table.addCell(namecell);
		table.addCell(itemcell);
		table.addCell(qtycell);
		table.addCell(ratecell);
		table.addCell(amtcell);

		for (int i = 0; i < po.getProductDetails().size(); i++) {

			chunk = new Phrase(i + 1 + "", font9);
			pdfsr = new PdfPCell(chunk);
			pdfsr.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfsr);
		}

		for (int i = 0; i < po.getProductDetails().size(); i++) {
			if (itemList.get(i).getComment() != null) {
				chunk = new Phrase(itemList.get(i).getComment() + "", font9);
			} else {
				chunk = new Phrase("", font9);
			}
			pdfdesc = new PdfPCell(chunk);
			pdfdesc.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfdesc);

		}

		for (int i = 0; i < po.getProductDetails().size(); i++) {
			if (po.getProductDetails().get(i).getProductName() != null) {
				chunk = new Phrase(po.getProductDetails().get(i)
						.getProductName()
						+ "", font9);
			} else {
				chunk = new Phrase("", font9);
			}
			pdfitem = new PdfPCell(chunk);
			pdfitem.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfitem);
		}

		for (int i = 0; i < po.getProductDetails().size(); i++) {

			if (po.getProductDetails().get(i).getProductQuantity() != 0) {
				chunk = new Phrase(po.getProductDetails().get(i)
						.getProductQuantity()
						+ "", font9);
			} else {
				chunk = new Phrase("", font9);
			}
			pdfqty = new PdfPCell(chunk);
			pdfqty.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfqty);
		}

		for (int i = 0; i < po.getProductDetails().size(); i++) {
			if (po.getProductDetails().get(i).getProdPrice() != 0) {
				chunk = new Phrase(po.getProductDetails().get(i).getProdPrice()
						+ "", font9);
			} else {
				chunk = new Phrase("", font9);
			}

			pdfrate = new PdfPCell(chunk);
			pdfrate.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfrate);
		}

		for (int i = 0; i < po.getProductDetails().size(); i++) {

			// if (po.getProductDetails().get(i).getTotal() != 0) {
			// chunk = new Phrase(po.getProductDetails().get(i).getTotal()
			// + "", font8);
			// } else {
			// chunk = new Phrase("", font8);
			// }
			/*****************************/

			double tQty = po.getProductDetails().get(i).getProductQuantity();
			double tRate = po.getProductDetails().get(i).getProdPrice();
			double ttlAmt = tQty * tRate;

			Phrase total = new Phrase(Math.round(ttlAmt) + "", font9);
			pdfamt = new PdfPCell(total);
			pdfamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			table.addCell(pdfamt);
		}

		PdfPCell prodcell = new PdfPCell(table);
		prodcell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(prodcell);
		// parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createTotalAmtTable() {
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);

		// ********total table***************
		PdfPTable ttltable = new PdfPTable(6);

		try {
			ttltable.setWidths(new float[] { 10, 45, 10, 10, 10, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		ttltable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		ttltable.addCell(blcell);

		Phrase bl1 = new Phrase("Total ", font9bold);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		ttltable.addCell(bl1cell);

		Phrase bl2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(bl2);
		ttltable.addCell(bl2cell);

		Phrase bl3 = new Phrase(" ");
		PdfPCell bl3cell = new PdfPCell(bl3);
		ttltable.addCell(bl3cell);

		Phrase bl4 = new Phrase(" ");
		PdfPCell bl4cell = new PdfPCell(bl4);
		ttltable.addCell(bl4cell);

		for (int i = 0; i < po.getProductDetails().size(); i++) {

			double tQty = po.getProductDetails().get(i).getProductQuantity();
			double tRate = po.getProductDetails().get(i).getProdPrice();
			double ttlAmt = tQty * tRate;

			Phrase total = new Phrase(Math.round(ttlAmt) + "", font9bold);
			pdfamt = new PdfPCell(total);
			pdfamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			ttltable.addCell(pdfamt);
		}

		PdfPCell cell = new PdfPCell(ttltable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createProductTaxTable() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);

		// ********vat table**************
		PdfPTable vattable = new PdfPTable(6);

		try {
			vattable.setWidths(new float[] { 10, 45, 10, 10, 10, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		vattable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		vattable.addCell(blcell);

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);

		for (int i = 0; i < po.getProductTaxes().size(); i++) {

			if (!po.getProductTaxes().get(i).getChargeName().equals("VAT")
					&& po.getProductTaxes().get(i).getChargePercent() > 0) {
				System.out.println("inside if condition ::::::::");

				String str = po.getProductTaxes().get(i).getChargeName()
						+ " @ "
						+ po.getProductTaxes().get(i).getChargePercent() + "%";

				Phrase strp = new Phrase(str, font9bold);
				PdfPCell strcell = new PdfPCell(strp);
				strcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(strcell);

			} else {

				if (!po.getProductTaxes().get(i).getChargeName()
						.equals("SERVICE TAX")
						&& po.getProductTaxes().get(i).getChargePercent() > 0) {
					System.out.println("inside else condition ::::::::");

					String str = po.getProductTaxes().get(i).getChargeName()
							+ " @ "
							+ po.getProductTaxes().get(i).getChargePercent()
							+ "%";

					Phrase strp = new Phrase(str, font9bold);
					PdfPCell strcell = new PdfPCell(strp);
					strcell.setHorizontalAlignment(Element.ALIGN_CENTER);
					table.addCell(strcell);

				}

			}

		}

		PdfPCell tableCell = new PdfPCell(table);
		vattable.addCell(tableCell);

		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1);
		vattable.addCell(bl1cell);

		Phrase bl2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(bl2);
		vattable.addCell(bl2cell);

		Phrase bl3 = new Phrase(" ");
		PdfPCell bl3cell = new PdfPCell(bl3);
		vattable.addCell(bl3cell);

		for (int i = 0; i < po.getProductTaxes().size(); i++) {

			for (int j = 0; j < po.getProductDetails().size(); j++) {

				if (!po.getProductTaxes().get(i).getChargeName().equals("VAT")
						&& po.getProductTaxes().get(i).getChargePercent() > 0) {

					double tQty = po.getProductDetails().get(j)
							.getProductQuantity();
					double tRate = po.getProductDetails().get(j).getProdPrice();
					double ttlAmt = tQty * tRate;

					// double vatAmt = 5 * ttlAmt / 100;
					double vatAmt = po.getProductTaxes().get(i)
							.getChargePercent()
							* ttlAmt / 100;

					Phrase vatAmtp = new Phrase(vatAmt + "", font9bold);
					pdfvatAmt = new PdfPCell(vatAmtp);
					pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);

					vattable.addCell(pdfvatAmt);

				} else if (!po.getProductTaxes().get(i).getChargeName()
						.equals("VAT")
						&& po.getProductTaxes().get(i).getChargePercent() == 5) {

					double tQty = po.getProductDetails().get(j)
							.getProductQuantity();
					double tRate = po.getProductDetails().get(j).getProdPrice();
					double ttlAmt = tQty * tRate;

					double vatAmt = 5 * ttlAmt / 100;
					// double vatAmt = po.getProductTaxes().get(i)
					// .getChargePercent()
					// * ttlAmt / 100;

					Phrase vatAmtp = new Phrase(vatAmt + "", font9bold);
					pdfvatAmt = new PdfPCell(vatAmtp);
					pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);

					vattable.addCell(pdfvatAmt);

				} else if (!po.getProductTaxes().get(i).getChargeName()
						.equals("SERVICE TAX")
						&& po.getProductTaxes().get(i).getChargePercent() > 0) {

					System.out.println("inside else condition ::::::::");

					double tQty = po.getProductDetails().get(j)
							.getProductQuantity();
					double tRate = po.getProductDetails().get(j).getProdPrice();
					double ttlAmt = tQty * tRate;

					// double vatAmt = 5 * ttlAmt / 100;
					double vatAmt = po.getProductTaxes().get(i)
							.getChargePercent()
							* ttlAmt / 100;

					Phrase vatAmtp = new Phrase(vatAmt + "", font9bold);
					pdfvatAmt = new PdfPCell(vatAmtp);
					pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);

					vattable.addCell(pdfvatAmt);

				} else if (!po.getProductTaxes().get(i).getChargeName()
						.equals("SERVICE TAX")
						&& po.getProductTaxes().get(i).getChargePercent() == 14.5) {

					double tQty = po.getProductDetails().get(j)
							.getProductQuantity();
					double tRate = po.getProductDetails().get(j).getProdPrice();
					double ttlAmt = tQty * tRate;

					// double vatAmt = po.getProductTaxes().get(i)
					// .getChargePercent()
					// * ttlAmt / 100;

					double vatAmt = 14 * ttlAmt / 100;
					Phrase vatAmtp = new Phrase(Math.round(vatAmt) + "", font9);

					pdfvatAmt = new PdfPCell(vatAmtp);
					pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
					vattable.addCell(pdfvatAmt);

					double vatAmt1 = 0.5 * ttlAmt / 100;
					Phrase vatAmt1p = new Phrase(Math.round(vatAmt1) + "",
							font9);

					pdfvatAmt1 = new PdfPCell(vatAmt1p);
					pdfvatAmt1.setHorizontalAlignment(Element.ALIGN_RIGHT);
					vattable.addCell(pdfvatAmt1);

				} else if (!po.getProductTaxes().get(i).getChargeName()
						.equals("SERVICE TAX")
						&& po.getProductTaxes().get(i).getChargePercent() == 15) {

					double tQty = po.getProductDetails().get(j)
							.getProductQuantity();
					double tRate = po.getProductDetails().get(j).getProdPrice();
					double ttlAmt = tQty * tRate;

					double vatAmt = 14 * ttlAmt / 100;
					Phrase vatAmtp = new Phrase(Math.round(vatAmt) + "", font9);

					pdfvatAmt = new PdfPCell(vatAmtp);
					pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
					vattable.addCell(pdfvatAmt);

					double vatAmt1 = 0.5 * ttlAmt / 100;
					Phrase vatAmt1p = new Phrase(Math.round(vatAmt1) + "",
							font9);

					pdfvatAmt1 = new PdfPCell(vatAmt1p);
					pdfvatAmt1.setHorizontalAlignment(Element.ALIGN_RIGHT);
					vattable.addCell(pdfvatAmt1);

					double vatAmt2 = 0.5 * ttlAmt / 100;
					Phrase vatAmt2p = new Phrase(Math.round(vatAmt2) + "",
							font9);

					pdfvatAmt2 = new PdfPCell(vatAmt2p);
					pdfvatAmt2.setHorizontalAlignment(Element.ALIGN_RIGHT);
					vattable.addCell(pdfvatAmt2);

				}

			}

		}

		// PdfPCell table1Cell = new PdfPCell(table);
		// vattable.addCell(table1Cell);

		PdfPCell cell = new PdfPCell(vattable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// private void createRoundValueTable() {
	//
	// Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	//
	// // ********rount up table***************
	// PdfPTable roundtable = new PdfPTable(6);
	//
	// try {
	// roundtable.setWidths(new float[] { 10, 45, 10, 10, 10, 15 });
	// } catch (DocumentException e1) {
	// e1.printStackTrace();
	// }
	// roundtable.setWidthPercentage(100);
	//
	// Phrase bl4 = new Phrase(" ");
	// PdfPCell bl4cell = new PdfPCell(bl4);
	// roundtable.addCell(bl4cell);
	//
	// Phrase bl5 = new Phrase("Round Up", font9bold);
	// PdfPCell bl5cell = new PdfPCell(bl5);
	// bl5cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// roundtable.addCell(bl5cell);
	//
	// Phrase bl6 = new Phrase(" ");
	// PdfPCell bl6cell = new PdfPCell(bl6);
	// roundtable.addCell(bl6cell);
	//
	// Phrase bl7 = new Phrase(" ");
	// PdfPCell bl7cell = new PdfPCell(bl7);
	// roundtable.addCell(bl7cell);
	//
	// Phrase bl8 = new Phrase(" ");
	// PdfPCell bl8cell = new PdfPCell(bl8);
	// roundtable.addCell(bl8cell);
	//
	// // PdfPTable t1 = new PdfPTable(1);
	//
	// for (int i = 0; i < po.getProductTaxes().size(); i++) {
	//
	// for (int j = 0; j < po.getProductDetails().size(); j++) {
	//
	// if (!po.getProductTaxes().get(i).getChargeName().equals("VAT")
	// && po.getProductTaxes().get(i).getChargePercent() > 0) {
	//
	// double tQty = po.getProductDetails().get(j)
	// .getProductQuantity();
	// double tRate = po.getProductDetails().get(j).getProdPrice();
	// double ttlAmt = tQty * tRate;
	//
	// // double vatAmt = 5 * ttlAmt / 100;
	// double vatAmt = po.getProductTaxes().get(i)
	// .getChargePercent()
	// * ttlAmt / 100;
	//
	// Phrase vatAmtp = new Phrase(Math.round(vatAmt) + "",
	// font9bold);
	// pdfvatAmt = new PdfPCell(vatAmtp);
	// pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//
	// roundtable.addCell(pdfvatAmt);
	//
	// } else {
	// if (!po.getProductTaxes().get(i).getChargeName().equals("SERVICE TAX")
	// && po.getProductTaxes().get(i).getChargePercent() > 0) {
	//
	// double tQty = po.getProductDetails().get(j)
	// .getProductQuantity();
	// double tRate = po.getProductDetails().get(j)
	// .getProdPrice();
	// double ttlAmt = tQty * tRate;
	//
	// // double vatAmt = 5 * ttlAmt / 100;
	// double vatAmt = po.getProductTaxes().get(i)
	// .getChargePercent()
	// * ttlAmt / 100;
	//
	// Phrase vatAmtp = new Phrase(Math.round(vatAmt) + "",
	// font9bold);
	// pdfvatAmt = new PdfPCell(vatAmtp);
	// pdfvatAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//
	// roundtable.addCell(pdfvatAmt);
	// }
	//
	// }
	// }
	//
	// }
	//
	// PdfPCell cell = new PdfPCell(roundtable);
	// cell.setBorder(0);
	//
	// PdfPTable parenttable = new PdfPTable(1);
	// parenttable.addCell(cell);
	// parenttable.setWidthPercentage(100);
	//
	// try {
	// document.add(parenttable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// }

	// *********patch for roundup value****************
	private void createRoundValueTable() {

		PdfPTable totaltable = new PdfPTable(6);

		try {
			totaltable.setWidths(new float[] { 10, 45, 10, 10, 10, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		totaltable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase bl9 = new Phrase(" ");
		PdfPCell bl9cell = new PdfPCell(bl9);
		totaltable.addCell(bl9cell);

		Phrase bl10 = new Phrase("Round Up", font9bold);
		PdfPCell bl10cell = new PdfPCell(bl10);
		bl10cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totaltable.addCell(bl10cell);

		Phrase bl11 = new Phrase(" ");
		PdfPCell bl11cell = new PdfPCell(bl11);
		totaltable.addCell(bl11cell);

		Phrase bl12 = new Phrase(" ");
		PdfPCell bl12cell = new PdfPCell(bl12);
		totaltable.addCell(bl12cell);

		Phrase bl13 = new Phrase(" ");
		PdfPCell bl13cell = new PdfPCell(bl13);
		totaltable.addCell(bl13cell);

		Phrase grandTotal = null;
		if (po.getGtotal() != 0) {
			grandTotal = new Phrase("" + Math.round(po.getGtotal()), font9bold);
		} else {
			grandTotal = new Phrase("", font9bold);
		}
		PdfPCell grandCell = new PdfPCell(grandTotal);
		grandCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totaltable.addCell(grandCell);

		PdfPCell cell = new PdfPCell(totaltable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createGrandTotalTable() {

		PdfPTable totaltable = new PdfPTable(6);

		try {
			totaltable.setWidths(new float[] { 10, 45, 10, 10, 10, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		totaltable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase bl9 = new Phrase(" ");
		PdfPCell bl9cell = new PdfPCell(bl9);
		totaltable.addCell(bl9cell);

		Phrase bl10 = new Phrase("Grand Total", font9bold);
		PdfPCell bl10cell = new PdfPCell(bl10);
		bl10cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		totaltable.addCell(bl10cell);

		Phrase bl11 = new Phrase(" ");
		PdfPCell bl11cell = new PdfPCell(bl11);
		totaltable.addCell(bl11cell);

		Phrase bl12 = new Phrase(" ");
		PdfPCell bl12cell = new PdfPCell(bl12);
		totaltable.addCell(bl12cell);

		Phrase bl13 = new Phrase(" ");
		PdfPCell bl13cell = new PdfPCell(bl13);
		totaltable.addCell(bl13cell);

		Phrase grandTotal = null;
		if (po.getGtotal() != 0) {
			grandTotal = new Phrase("" + Math.round(po.getGtotal()), font9bold);
		} else {
			grandTotal = new Phrase("", font9bold);
		}
		PdfPCell grandCell = new PdfPCell(grandTotal);
		grandCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totaltable.addCell(grandCell);

		PdfPCell cell = new PdfPCell(totaltable);
		cell.setBorder(0);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createInWordsDetail() {

		PdfPTable rstable = new PdfPTable(2);

		try {
			rstable.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		rstable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase rs = new Phrase("Amount in words  :", font10bold);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String amountInWord = ServiceInvoicePdf.convert(Math.round(po
				.getGtotal()));
		Phrase bl1 = new Phrase("" + amountInWord + " Only ", font10bold);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorder(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		rstable.addCell(rscell);
		rstable.addCell(bl1cell);

		PdfPCell cell = new PdfPCell(rstable);
		cell.setBorder(0);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(cell);
		amttable.addCell(blcell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createTermsAndConditions() {

		PdfPTable termsTable = new PdfPTable(1);
		termsTable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase terms = null;
		if (po.getDescription() != null) {
			terms = new Phrase("Terms & Conditions :  " + po.getDescription(),
					font10bold);
		}
		PdfPCell termscell = new PdfPCell(terms);
		termscell.setBorder(0);
		termscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		termsTable.addCell(termscell);
		termsTable.addCell(blcell);
		termsTable.addCell(blcell);
		termsTable.addCell(blcell);

		PdfPCell cell = new PdfPCell(termsTable);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createSignatureInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);

		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// *************righ table************
		PdfPTable righTable = new PdfPTable(1);
		righTable.setWidthPercentage(100);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("For " + comp.getBusinessUnitName(),
					font10bold);
		} else {
			company = new Phrase(" ", font10bold);
		}

		Phrase sign = new Phrase("Authorized Signatory ", font10bold);

		PdfPCell compcell = new PdfPCell(company);
		compcell.setBorder(0);
		compcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell signcell = new PdfPCell(sign);
		signcell.setBorder(0);
		signcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		righTable.addCell(compcell);
		righTable.addCell(blcell);
		righTable.addCell(blcell);
		righTable.addCell(signcell);

		PdfPCell rightCell = new PdfPCell(righTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		/******************************** rough part *************************************************/
		// String sum = "";
		// double ttl = po.getTotalAmount();
		// double vatAmt = 5 * po.getTotalAmount() / 100;
		// long roundAmt = (Math.round(vatAmt));
		//
		// sum = sum + ttl + roundAmt;
		// this.sum = sum;
		// System.out.println("The round value::::::::::::::: " + sum);
		//
		// Phrase grandTotal = new Phrase(sum, font10bold);
		// PdfPCell grandCell = new PdfPCell(grandTotal);
		// grandCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// totaltable.addCell(grandCell);

	}

}
