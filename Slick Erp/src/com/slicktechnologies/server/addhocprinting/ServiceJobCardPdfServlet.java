package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class ServiceJobCardPdfServlet extends HttpServlet{
	/**
	 * 
	 */
	private static final long serialVersionUID = -489075798223467102L;

	public static ArrayList<Service> serviceList = new ArrayList<Service>();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		  String stringCompanyId=req.getParameter("companyId");
		  Long companyId=0l;
		  if(stringCompanyId!=null){
		  companyId =Long.parseLong(stringCompanyId);
		  }else{
			  companyId= Long.parseLong("5348024557502464");
		  }
		/**
		 * Rahul Verma added on 24 Aug 2018 
		 * Because while printing from android due to warm up issue. Print is not working
		 */
		RegisterServiceImpl serviceImpl=new RegisterServiceImpl();
		serviceImpl.getClass();
		/**
		 * Ends for Rahul Verma
		 */
		String preprintStatus = req.getParameter("preprint"); //preprint
		resp.setContentType("application/pdf");
		SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
			 
		
		try {
			
			//Ashwini Patil Date:02-05-2023				
			String id = req.getParameter("Id");
			id = id.trim();
			Long c = Long.parseLong(id);
			Service service = ofy().load().type(Service.class).id(c).now();
			if(service!=null){
				String filename="Service - "+service.getCount()+" - "+service.getPersonInfo().getFullName()+" - "+fmt.format(service.getServiceDate())+".pdf";
				if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Company", "EnablePrintPreview", service.getCompanyId()))
					resp.setHeader("Content-Disposition", "attachment; filename=" + filename);
				System.out.println("filename="+filename);
			}
			
			
			
          /**Date 16-1-2020 by AMOL added a Process Configuration "PrintServiceJobCardPdf",**/
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PrintServiceJobCardPdf",companyId)){
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			ServiceJobCardPdf serjobcard = new ServiceJobCardPdf();
			
			serjobcard.document = new Document();
			Document document = serjobcard.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream());

			document.open();
			serjobcard.setpdfserjob(count);
			serjobcard.createPdf();
			document.close();
			} else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","OnlyForPecopp",companyId)){
				
				String stringid = req.getParameter("Id");
				stringid = stringid.trim();
				Long count = Long.parseLong(stringid);
				
				 String srNumber=req.getParameter("SRNumber");

				MultipleServicesRecordVersionOnePdf multipleServicesRecordPdf = new MultipleServicesRecordVersionOnePdf();
				//ServiceJobCardPdf serjobcard = new ServiceJobCardPdf();
				
				multipleServicesRecordPdf.document=new Document();
				Document document = multipleServicesRecordPdf.document;
				 PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); 
				 document.open();
				  multipleServicesRecordPdf.loadAll(count, companyId);
				  multipleServicesRecordPdf.createPdf(preprintStatus);
			   	  //document.close();
				document.close();
				
			}else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_ServiceReportV3",companyId)){
				
				String stringid = req.getParameter("Id");
				stringid = stringid.trim();
				Long count = Long.parseLong(stringid);
				
				 String srNumber=req.getParameter("SRNumber");

				 CustomerServiceRecordVersionOnePdf customerServicePdf = new CustomerServiceRecordVersionOnePdf();
				//ServiceJobCardPdf serjobcard = new ServiceJobCardPdf();
				
				 customerServicePdf.document=new Document();
				Document document = customerServicePdf.document;
				 PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); 
				 document.open();
				 customerServicePdf.setpdfCustomerServRecord(count);
				// customerServicePdf.loadAll(count, companyId);
				 customerServicePdf.createPdf();
			   	  //document.close();
				document.close();
				
			}  //
			else if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service",AppConstants.PC_SRFORMATVERSION1,companyId)){
				
				

				try {
					String stringid = req.getParameter("Id");
					stringid=stringid.trim();
					Long count =Long.parseLong(stringid);
				    
					Service serviceEntity = ofy().load().type(Service.class).id(count).now();
					ServerAppUtility serverUtility = new ServerAppUtility();
					Company companyEntity = serverUtility.loadCompany(serviceEntity.getCompanyId());
							
					SRFormatVersion1 srformatv1 = new SRFormatVersion1();
					srformatv1.document = new Document(); 
			 	    Document document = srformatv1.document;
			 	    PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write the pdf in response
			 	    
			 	    Service serviceentity=ofy().load().type(Service.class).id(count).now();

			 	    
			 	    document.setPageSize(PageSize.A4);
			 	  	document.setMargins(20, 20, 120, 100);
			 	  	document.setMarginMirroring(false);
					
//			 	  	 if(preprintStatus.equals("no")){
			 	  	   HeaderFooterPageEvent event = new HeaderFooterPageEvent(companyEntity,serviceentity.getBranch(),preprintStatus);
			 	  	   writer.setPageEvent(event);
//			 	    }
			 	  
			 	 
			 	    document.open();
			 		   
			 	   srformatv1.setService(count);
			 	   srformatv1.createPdf(preprintStatus);
			 	   
			 	    
			 	    document.close();
			 	    

				}catch (Exception e) {
					e.printStackTrace();
				}
			}
			else {
				
				/***Date 16-1-2020 by AMOL
				 *  added a new Pdf CustomerServicerecordPdf For  Default Service Voucher Pdf 
				 * ***/
				
				String stringid = req.getParameter("Id");
				stringid = stringid.trim();
				Long count = Long.parseLong(stringid);

				CustomerServiceRecordPdf customerServiceRecordpdf = new CustomerServiceRecordPdf();
				customerServiceRecordpdf.document = new Document();
				Document document = customerServiceRecordpdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());

				document.open();
				customerServiceRecordpdf.setpdfCustomerServRecord(count);
				customerServiceRecordpdf.createPdf();
				document.close();
			}
          
          
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
