package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class OmPestContractRenewalPdf {

	public Document document;
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat monthFmt = new SimpleDateFormat("MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");
	
	ContractRenewal conRenw;
	Contract con;
	Invoice invoice;
	Customer cust;
	Company comp;
	double subTotal = 0;
	double taxvalue =0;
	double total=0;
	Logger logger = Logger.getLogger("NameOfYourLogger");
	Phrase chunk;
	PdfPCell pdfno, pdftreatment, pdfpest, pdffrequency, pdfpremise, pdfprice;
	PdfPCell  pdfsubttl,pdfttlamt;

	float[] colwidth = { 1.0f, 0.1f, 7.0f, 1.5f, 0.1f, 1.5f };
	float[] tblcowidth = { 1.5f, 0.2f, 7.f };
	
	float[] tblcolwidth12 = { 0.3f, 1.0f, 1.0f, 1.0f, 1.0f ,1.0f};
	
	public OmPestContractRenewalPdf()
    {
	new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	font8 = new Font(Font.FontFamily.HELVETICA, 8);
	font9 = new Font(Font.FontFamily.HELVETICA, 9);
	font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	font12 = new Font(Font.FontFamily.HELVETICA, 12);
	font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
	font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
	font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
}

	public void setContractRewnewal(ContractRenewal contract) {
		
		conRenw =contract;
		
		if (conRenw.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", conRenw.getCompanyId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", conRenw.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getCustomerId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", conRenw.getContractId()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getContractId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			invoice = ofy().load().type(Invoice.class).filter("contractCount", conRenw.getContractId()).first().now();
		else
			invoice = ofy().load().type(Invoice.class).filter("companyId", conRenw.getCompanyId()).filter("contractCount", conRenw.getContractId()).first().now();
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		monthFmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
	}

	public void createPdf(String status){
		
		System.out.println("status value "+status);
		if(status.equals("yes"))
		{
			createBlank1Heading();
			createLogo(document,comp);
		}
		else if(status.equals("no"))
		{
			createBlank1Heading();
		    	if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
		}
		
		
		createTitleHeading();
		createReferenceHeading();
		
		if(con.getGroup().equalsIgnoreCase("One Time"))
		{
			createCustomerDetails();
		}
		else if(con.getGroup().equalsIgnoreCase("Yearly"))
		{
			createCustomerDetailsHeading();
		}
		else
		{
			createCustomerDetailsHeading();
		}
		
		createQuotationTable();
		createOPCSHeading();
		createNoteHeading();
	}
	

private void createCustomerDetails() {
	//    ******************  rohan added this for new contract renewal period  ****************
	
 	Calendar cal = Calendar.getInstance();
 	cal.setTime(con.getEndDate());
 	cal.add(Calendar.YEAR, 1);

 	
 Date dt = DateUtility.getStartDateofMonth(cal.getTime());
 System.out.println("modified date "+dt);
 	Date newStDt = dt;
 	cal.setTime(newStDt);
 	cal.add(Calendar.DATE , 364);
 	
 	System.out.println("Contract perid st dt"+dt+ "end dt "+cal.getTime());
//     *******************************changes ends here *************************************

Phrase to = new Phrase("To",font8);
PdfPCell toCell = new PdfPCell(to);
toCell.setBorder(0);
toCell.setHorizontalAlignment(Element.ALIGN_LEFT);

String custAdd1="";
String custFullAdd1="";


Phrase designation = null;
PdfPCell designationCell = null;
if(cust.getCustCorresponence()!=null)
{
	designation = new Phrase(cust.getCustCorresponence(),font8);
	
	designationCell = new PdfPCell(designation);
	designationCell.setBorder(0);
}
//else
//{
//	designation = new Phrase(" " ,font8);
//	
//	designationCell = new PdfPCell(designation);
//	designationCell.setBorder(0);
//}
Phrase custName = null;

if(cust.isCompany()==true)
{
	custName = new Phrase(cust.getCompanyName(),font9);
}
else
{
	custName = new Phrase(cust.getFullname(),font9);
}
PdfPCell custNameCell =  new PdfPCell(custName);
custNameCell.setBorder(0);


if(cust.getAdress()!=null){
	
	if(!cust.getAdress().getAddrLine2().equals("")){
		if(!cust.getAdress().getLandmark().equals("")){
			custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
		}else{
			custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
		}
	}else{
		if(!cust.getAdress().getLandmark().equals("")){
			custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
		}else{
			custAdd1=cust.getAdress().getAddrLine1();
		}
	}
	
	if(!cust.getAdress().getLocality().equals("")){
		custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
				
	}else{
		custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
	}
}

Phrase mobno = null;
if (cust.getLandline() != null && cust.getLandline()!=0) {
	mobno = new Phrase("Tel No. : " + cust.getCellNumber1()+" ,022-"+cust.getLandline(), font9);
}
else
{
	mobno = new Phrase("Tel No. : " + cust.getCellNumber1(),font9);
}

PdfPCell mobcell = new PdfPCell(mobno);
mobcell.setBorder(0);
mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);

//Phrase poc = new Phrase("POC: "+dir.getCustomerFullName(),font10);
//PdfPCell poccell = new PdfPCell(poc);
//poccell.setBorder(0);
//poccell.setHorizontalAlignment(Element.ALIGN_LEFT);



//**************All calculation for product table **********8


for(int i=0; i<this.conRenw.getItems().size(); i++){
	
	subTotal = subTotal +(this.conRenw.getItems().get(i).getPrice()*this.conRenw.getItems().get(i).getQty());
	
}


if((con.getItems().get(0).getServiceTax().getPercentage() !=0)){

	taxvalue = subTotal*(con.getItems().get(0).getServiceTax().getPercentage()/100);	
}
 total= subTotal+taxvalue;


//**************changes ends here **************
Phrase pname = null;
PdfPCell pnamecell =null;
if(cust.isCompany()==true){
		pname = new Phrase("CTC: " + cust.getFullname(), font9);
	}
	
	pnamecell = new PdfPCell(pname);
	pnamecell.setBorder(0);
	pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	

PdfPCell cellNo2=null;
if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
{
	Phrase phNo2 = new Phrase("Cell No. : "+cust.getCellNumber2(),font9); 
	cellNo2 = new PdfPCell(phNo2);
	cellNo2.setBorder(0);
}

Phrase sub = new Phrase("Subject : Renewal of Contract for Pest Management Services.",font9);
 PdfPCell subcell = new PdfPCell(sub);
subcell.setBorder(0);
subcell.setHorizontalAlignment(Element.ALIGN_LEFT);

Phrase custAddInfo = new Phrase(custFullAdd1, font8);
PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
custAddInfoCell.setBorder(0);

Phrase blank = new Phrase(" ", font8);
PdfPCell blankCell = new PdfPCell(blank);
blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
blankCell.setBorder(0);

Phrase dear = new Phrase("Dear Sir / Madam"+","+"\n",font9);
PdfPCell dearcell = new PdfPCell(dear);
dearcell.setBorder(0);
dearcell.setHorizontalAlignment(Element.ALIGN_LEFT);


//  **************this code is used to findout last service of that contract ***********
List<Service> serList =new ArrayList<Service>();
if (conRenw.getCompanyId() == null)
	serList = ofy().load().type(Service.class).filter("contractCount", conRenw.getContractId()).filter("status", Service.SERVICESTATUSCOMPLETED).list();
else
	serList = ofy().load().type(Service.class).filter("companyId", conRenw.getCompanyId()).filter("contractCount", conRenw.getContractId()).filter("status", Service.SERVICESTATUSCOMPLETED).list();

System.out.println("Service List size "+serList.size());


Collections.sort(serList , new Comparator<Service>() {

	@Override
	public int compare(Service arg0, Service arg1) {
		
		Integer serNo1 = arg0.getServiceSerialNo();
		Integer serNo2 = arg1.getServiceSerialNo();
		
		return serNo2.compareTo(serNo1);
	}
});

//*********************************code ends here **************************************

//   rohan added this 
Phrase msg = null;
if(serList.size()!=0){
 msg = new Phrase("We have done pest control service "+fmt.format(serList.get(0).getServiceDate())+",In order to enjoy an uninterrupted service for pest-free environment, "
    		+ "we recommend you to renew the contract at the earliest. Our renewal charges will be "+total+" in terms and conditions for 12 months contract (for period "+fmt.format(dt)+"to"+fmt.format(cal.getTime())+") would be mentioned as below: ",font9);
}
else
{
	msg = new Phrase("We have done pest control service,In order to enjoy an uninterrupted service for pest-free environment, "
    		+ "we recommend you to renew the contract at the earliest. Our renewal charges will be "+total+" in terms and conditions for 12 months contract (for period "+fmt.format(dt)+"to"+fmt.format(cal.getTime())+") would be mentioned as below: ",font9);
}

PdfPCell msgcell = new PdfPCell(msg);
msgcell.setBorder(0);
msgcell.setHorizontalAlignment(Element.ALIGN_LEFT);

//Phrase opcs =null;
//for(int i=0;i<conRenw.getItems().size();i++)
//{
//	opcs = new Phrase(conRenw.getItems().get(i).getProductName(),font8bold);
//	PdfPCell opcsCell = new PdfPCell(opcs);
//	opcsCell.setBorder(0);
//	opcsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//}


PdfPTable cdtable = new PdfPTable(1);
cdtable.addCell(toCell);
if(cust.getCustCorresponence()!=null)
{
cdtable.addCell(designationCell);
}
cdtable.addCell(custNameCell);
cdtable.addCell(custAddInfoCell);

if(cust.isCompany()==true){
cdtable.addCell(pnamecell);
}
cdtable.addCell(mobcell);
if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
{
cdtable.addCell(cellNo2);
}
cdtable.addCell(blankCell);
cdtable.addCell(subcell);
cdtable.addCell(blankCell);
cdtable.addCell(dearcell);
cdtable.addCell(msgcell);
//cdtable.addCell(opcsCell);
cdtable.setSpacingAfter(10f);


cdtable.setWidthPercentage(100f);

PdfPCell cell4 = new PdfPCell(cdtable);
cell4.setBorder(0);

PdfPTable table3 = new PdfPTable(1);
table3.addCell(cell4);
table3.setWidthPercentage(100);


try {
	document.add(table3);
} catch (DocumentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

	}

private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//			Image image1=Image.getInstance("images/OM Pest Letter Head.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	public void createBlank1Heading() {
		
		System.out.println("Inside create blank heading");
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
	    
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    
	    
	    PdfPCell blcell = new PdfPCell(blank);
	    blcell.setBorder(0);
	    blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);
		
//		PdfPCell cell1 = new PdfPCell();
//		cell1.addElement(bltable);
////		cell1.setBorder(0);
//		cell1.setBorderWidthTop(0);
//		cell1.setBorderWidthLeft(0);
//		cell1.setBorderWidthRight(0);
//		
//		PdfPTable table = new PdfPTable(1);
//		table.addCell(cell1);
//		table.setWidthPercentage(100);
	
	    
		try {
			document.add(bltable);
			System.out.println("BLANK CELL ADDED IN DOCUMENT...");
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	}
	
	    private void createLogo(Document doc, Company comp) {
			//********************logo for server ********************
				DocumentUpload document =comp.getLogo();
				//patch
				String hostUrl; 
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				if (environment.equals("Production")) {
				    String applicationId = System.getProperty("com.google.appengine.application.id");
				    String version = System.getProperty("com.google.appengine.application.version");
				    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
				} else {
				    hostUrl = "http://localhost:8888";
			}
				
				try {
					Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
					image2.scalePercent(20f);
					image2.setAbsolutePosition(40f,765f);	
					doc.add(image2);
				} catch (Exception e) {
					e.printStackTrace();
			}			
	}

public void createTitleHeading(){
		
		System.out.println("inside create tilte heading");
		
		String title = "Renewal Letter";
		Phrase quotation = new Phrase(title,font16boldul);
		Paragraph qpara = new Paragraph(quotation);
		qpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell qcell = new PdfPCell();
		qcell.addElement(qpara);
		qcell.setBorder(0);
		qcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase blk = new Phrase(" ");
		PdfPCell blkcell = new PdfPCell(blk);
		blkcell.setBorder(0);
		blkcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable qtable = new PdfPTable(1);
		qtable.addCell(qcell);
		qtable.addCell(blkcell);
		qtable.setWidthPercentage(100f);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.addElement(qtable);
		cell2.setBorder(0);
		
		PdfPTable table1 = new PdfPTable(1);
		table1.addCell(cell2);
		table1.setWidthPercentage(100);
	
		try {
			document.add(table1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}


public void createReferenceHeading(){
	
	logger.log(Level.SEVERE,"Ref number form comtract"+con.getRefNo());
	Phrase ref = new Phrase("Ref. no.",font9);
	PdfPCell refcell = new PdfPCell(ref);
	refcell.setBorder(0);
	refcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase r = null;
	
	if(con.getRefNo() !=null){
	r = new Phrase(con.getRefNo()+"",font9);	
	}
	
	PdfPCell rcell = new PdfPCell(r);
	rcell.setBorder(0);
	rcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase column = new Phrase(":",font9);
	PdfPCell colcell = new PdfPCell(column);
	colcell.setBorder(0);
	colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase date = new Phrase("Date",font9);
	PdfPCell datecell = new PdfPCell(date);
	datecell.setBorder(0);
	datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	logger.log(Level.SEVERE,"Ref date form comtract"+con.getRefDate());
	Phrase dt = null;
	if(con.getRefDate() !=null){
		dt = new Phrase(fmt.format(con.getRefDate()),font9);
	}
	
	PdfPCell dtcell = new PdfPCell(dt);
	dtcell.setBorder(0);
	dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase blank = new Phrase(" ");
	PdfPCell blankcell = new PdfPCell(blank); 
	blankcell.setBorder(0);
	blankcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	PdfPTable rdttable = new PdfPTable(6);
	rdttable.addCell(refcell);
	rdttable.addCell(colcell);
	rdttable.addCell(rcell);
	rdttable.addCell(datecell);
	rdttable.addCell(colcell);
	rdttable.addCell(dtcell);
//	rdttable.addCell(blankcell);
	rdttable.setWidthPercentage(100f);
	
	try {
		 rdttable.setWidths(colwidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	PdfPCell cell3 = new PdfPCell(rdttable); 		
	cell3.setBorder(0);
	
	PdfPTable table2 = new PdfPTable(1);
	table2.addCell(cell3);
	table2.setWidthPercentage(100);
	
	
	//***************for invoice details ***************************
	
	Phrase custid= new Phrase("Customer ID : "+invoice.getCount()+" Date : "+fmt.format(invoice.getInvoiceDate()),font9);
	PdfPCell custidCell = new PdfPCell(custid);
	custidCell.setBorder(0);
	
	PdfPTable table  = new PdfPTable(1);
	table.setWidthPercentage(100f);
	table.addCell(custidCell);
	
	table.setSpacingAfter(20f);
	
	
	try {
		document.add(table2);
		document.add(table);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}



public void createCustomerDetailsHeading(){
	
	//    ******************  rohan added this for new contract renewal period  ****************
	
	 	Calendar cal = Calendar.getInstance();
	 	cal.setTime(con.getEndDate());
	 	cal.add(Calendar.YEAR, 1);
	
	 	
	 Date dt = DateUtility.getStartDateofMonth(cal.getTime());
	 System.out.println("modified date "+dt);
	 	Date newStDt = dt;
	 	cal.setTime(newStDt);
	 	cal.add(Calendar.DATE , 364);
	 	
	 	System.out.println("Contract perid st dt"+dt+ "end dt "+cal.getTime());
	//     *******************************changes ends here *************************************
	
	Phrase to = new Phrase("To",font9);
	PdfPCell toCell = new PdfPCell(to);
	toCell.setBorder(0);
	toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String custAdd1="";
	String custFullAdd1="";
	
	
	Phrase designation = null;
	PdfPCell designationCell = null;
	if(cust.getCustCorresponence()!=null)
	{
		designation = new Phrase(cust.getCustCorresponence(),font9);
		
		designationCell = new PdfPCell(designation);
		designationCell.setBorder(0);
	}
//	else
//	{
//		designation = new Phrase(" " ,font8);
//		
//		designationCell = new PdfPCell(designation);
//		designationCell.setBorder(0);
//	}
	Phrase custName = null;
	
	if(cust.isCompany()==true)
	{
		custName = new Phrase(cust.getCompanyName(),font9);
	}
	else
	{
		custName = new Phrase(cust.getFullname(),font9);
	}
	PdfPCell custNameCell =  new PdfPCell(custName);
	custNameCell.setBorder(0);
	
	
	if(cust.getAdress()!=null){
		
		if(!cust.getAdress().getAddrLine2().equals("")){
			if(!cust.getAdress().getLandmark().equals("")){
				custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
			}else{
				custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
			}
		}else{
			if(!cust.getAdress().getLandmark().equals("")){
				custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
			}else{
				custAdd1=cust.getAdress().getAddrLine1();
			}
		}
		
		if(!cust.getAdress().getLocality().equals("")){
			custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
					
		}else{
			custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+"-"+cust.getAdress().getPin()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();
		}
	}
	
	Phrase mobno = null;
	if (cust.getLandline() != null && cust.getLandline()!=0) {
		mobno = new Phrase("Tel No. : " + cust.getCellNumber1()+" ,022-"+cust.getLandline(), font9);
	}
	else
	{
		mobno = new Phrase("Tel No. : " + cust.getCellNumber1(),font9);
	}
	
	PdfPCell mobcell = new PdfPCell(mobno);
	mobcell.setBorder(0);
	mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
//	Phrase poc = new Phrase("POC: "+dir.getCustomerFullName(),font10);
//	PdfPCell poccell = new PdfPCell(poc);
//	poccell.setBorder(0);
//	poccell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	
	//**************All calculation for product table **********8
	
	
	for(int i=0; i<this.conRenw.getItems().size(); i++){
		
		subTotal = subTotal +(this.conRenw.getItems().get(i).getPrice()*this.conRenw.getItems().get(i).getQty());
		
	}
	
	
	if((con.getItems().get(0).getServiceTax().getPercentage() !=0)){

		taxvalue = subTotal*(con.getItems().get(0).getServiceTax().getPercentage()/100);	
	}
	 total= subTotal+taxvalue;
	
	
	//**************changes ends here **************
	Phrase pname = null;
	PdfPCell pnamecell =null;
	if(cust.isCompany()==true){
			pname = new Phrase("CTC: " + cust.getFullname(), font9);
		}
		
		pnamecell = new PdfPCell(pname);
		pnamecell.setBorder(0);
		pnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	
	PdfPCell cellNo2=null;
	if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
	{
		Phrase phNo2 = new Phrase("Cell No. : "+cust.getCellNumber2(),font9); 
		cellNo2 = new PdfPCell(phNo2);
		cellNo2.setBorder(0);
	}
	
	Phrase sub = new Phrase("Subject : Renewal of Contract for Pest Management Services.",font9);
	 PdfPCell subcell = new PdfPCell(sub);
	subcell.setBorder(0);
	subcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase custAddInfo = new Phrase(custFullAdd1, font9);
	PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
	custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	custAddInfoCell.setBorder(0);
	
	Phrase blank = new Phrase(" ", font9);
	PdfPCell blankCell = new PdfPCell(blank);
	blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	blankCell.setBorder(0);
	
	Phrase dear = new Phrase("Dear Sir / Madam"+","+"\n",font9);
	PdfPCell dearcell = new PdfPCell(dear);
	dearcell.setBorder(0);
	dearcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	//   rohan added this 
	Phrase msg = new Phrase("It is our Privilege to have been a service to you over the past year. We truly appreciate and value our association and trust you have found "
	    		+ "our services exemplary and to your complete satisfaction."+"\n"
	    		+"Your current contract for Pest Management Services concludes on "+monthFmt.format(con.getEndDate())+". In order to enjoy an uninterrupted service for pest-free environment, "
	    		+ "we recommend you to renew the contract at the earliest. Our renewal charges will be "+total+" in terms and conditions for 12th months contract (for period "+fmt.format(dt)+" to "+fmt.format(cal.getTime())+") would be mentioned as below: ",font9);
	
	
	PdfPCell msgcell = new PdfPCell(msg);
	msgcell.setBorder(0);
	msgcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	Phrase opcs = new Phrase("Manifest of Om Pest Control Services",font9);
	PdfPCell opcsCell = new PdfPCell(opcs);
	opcsCell.setBorder(0);
	opcsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable cdtable = new PdfPTable(1);
	cdtable.addCell(toCell);
	if(cust.getCustCorresponence()!=null)
	{
	cdtable.addCell(designationCell);
	}
	cdtable.addCell(custNameCell);
	cdtable.addCell(custAddInfoCell);

	if(cust.isCompany()==true){
	cdtable.addCell(pnamecell);
	}
	cdtable.addCell(mobcell);
	if (cust.getCellNumber2() != null && cust.getCellNumber2() != 0)
	{
	cdtable.addCell(cellNo2);
	}
	cdtable.addCell(blankCell);
	cdtable.addCell(subcell);
	cdtable.addCell(blankCell);
	cdtable.addCell(dearcell);
	cdtable.addCell(msgcell);
	cdtable.addCell(opcsCell);
//	cdtable.setSpacingAfter(10f);
	
	
	cdtable.setWidthPercentage(100f);

	PdfPCell cell4 = new PdfPCell(cdtable);
	cell4.setBorder(0);
	
	PdfPTable table3 = new PdfPTable(1);
	table3.addCell(cell4);
	table3.setWidthPercentage(100);
	
	
	try {
		document.add(table3);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}


public void createQuotationTable(){
	
	Font font1 = new Font(Font.FontFamily.HELVETICA, 9 ,Font.BOLD);
	PdfPTable table = new PdfPTable(6);
	table.setWidthPercentage(100);
	try {
		table.setWidths(tblcolwidth12);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	Phrase no = new Phrase("No.",font1);
	Phrase treatment = new Phrase("Treatment", font1);
	Phrase pest = new Phrase("Pest Covered",font1);
	Phrase frequency = new Phrase("Frequency per annum(Services)", font1);
	Phrase premise = new Phrase("Premises under contract", font1);
	Phrase price = new Phrase("AMC Charges(Rs).", font1);
	
	
	PdfPCell nocell = new PdfPCell(no);
	nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPCell  treatcell = new PdfPCell( treatment);
	treatcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPCell pestcell = new PdfPCell(pest);
	pestcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPCell freqcell = new PdfPCell(frequency);
	freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPCell precell = new PdfPCell(premise);
	precell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPCell pricecell = new PdfPCell(price);
	pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	
	table.addCell(nocell);
	table.addCell(treatcell);
	table.addCell(pestcell);
	table.addCell(freqcell);
	table.addCell(precell);
	table.addCell(pricecell);

	
	for(int i=0; i<this.conRenw.getItems().size(); i++){
		
	chunk = new Phrase(i+1 +"",font9);	
	pdfno = new PdfPCell(chunk);
	pdfno.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	chunk = new Phrase(conRenw.getItems().get(i).getProductName()+"",font9);
	System.out.println("chunk 1: "+conRenw.getItems().get(i).getProductName());
	pdftreatment = new PdfPCell(chunk);
	pdftreatment.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	chunk = new Phrase(conRenw.getItems().get(i).getComment()+"",font9);
	System.out.println("chunk 2 :"+conRenw.getItems().get(i).getComment());
	pdfpest = new PdfPCell(chunk);
	pdfpest.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	chunk = new Phrase(conRenw.getItems().get(i).getNumberOfServices()+"",font9);	
	System.out.println("chunk 3 :"+conRenw.getItems().get(i).getNumberOfServices());
	pdffrequency = new PdfPCell(chunk);
	pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	if(conRenw.getItems().get(i).getPremisesDetails()!=null){
		chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails(),font9);
		}
	else
	{
		chunk = new Phrase(" ",font8);
	}
		pdfpremise = new PdfPCell(chunk);
		pdfpremise.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	chunk =  new Phrase((conRenw.getItems().get(i).getPrice()*conRenw.getItems().get(i).getQty())+"",font9);
	System.out.println("chunk 4 :"+conRenw.getItems().get(i).getPrice());
	pdfprice = new PdfPCell(chunk);
	pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	
	table.addCell(pdfno);
	table.addCell(pdftreatment);
	table.addCell(pdfpest);
	table.addCell(pdffrequency);
	table.addCell(pdfpremise);
	table.addCell(pdfprice);
	
}
	
	Phrase blank=new Phrase(" ",font9);
	PdfPCell blankCell = new PdfPCell();
	blankCell.addElement(blank);
//	blankCell.setBorder(0);
	blankCell.setBorderWidthRight(0);
	blankCell.setBorderWidthTop(0);
	blankCell.setBorderWidthLeft(0);
	
	
	Phrase blank1=new Phrase(" ",font9);
	PdfPCell blank1Cell = new PdfPCell();
	blank1Cell.addElement(blank1);
//	blankCell.setBorder(0);
//	blank1Cell.setBorderWidthRight(0);
	blank1Cell.setBorderWidthTop(0);
	
	
	//   rohan commented this code 
//	double subTotal = 0;
//	for(int i=0; i<this.conRenw.getItems().size(); i++){
//		
//		subTotal = subTotal +(this.conRenw.getItems().get(i).getPrice()*this.conRenw.getItems().get(i).getQty());
//		
//	}
	
	
	
	String name=null;
	double servalue=0;
	for(int i=0;i<con.getProductTaxes().size();i++){
	if(!con.getProductTaxes().get(i).getChargeName().equalsIgnoreCase("vat")){
	name = con.getProductTaxes().get(i).getChargeName();
	servalue =con.getProductTaxes().get(i).getChargePercent();
	System.out.println("charge %"+servalue);
	System.out.println("charge name "+name);
	}
	}
	Phrase stax = new Phrase(name+"@"+servalue+"%",font9bold);
	PdfPCell staxcell = new PdfPCell(stax);
	staxcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	staxcell.setBorderWidthLeft(0);
	staxcell.setBorderWidthTop(0);
	
	
//	String name = dir.getItems().get(0).getServiceTax().getTaxName();
//	double servalue =dir.getItems().get(0).getServiceTax().getPercentage();
	Phrase st = new Phrase("Sub Total",font9bold);
	PdfPCell stcell = new PdfPCell(st);
	stcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	stcell.setBorderWidthLeft(0);
	stcell.setBorderWidthTop(0);
	
	
	Phrase st1 = new Phrase(df.format(subTotal),font9bold);
	PdfPCell st1cell = new PdfPCell(st1);
	st1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase tamt = new Phrase("Total Amount",font9bold);
	PdfPCell tamtcell = new PdfPCell(tamt);
	tamtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	tamtcell.setBorderWidthLeft(0);
	tamtcell.setBorderWidthTop(0);

	table.addCell(blank1Cell);
	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(stcell);
	table.addCell(st1cell);
	
		PdfPCell pdftaxpercent=null;
		double taxvalue123 =0;
		if((con.getItems().get(0).getServiceTax().getPercentage() !=0)){

			 taxvalue123 = subTotal*(con.getItems().get(0).getServiceTax().getPercentage()/100);	
				
			chunk = new Phrase(df.format(taxvalue123),font9bold);
			pdftaxpercent = new PdfPCell(chunk);
			pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		}
		else
		{
			chunk = new Phrase(" ",font9bold);
			pdftaxpercent = new PdfPCell(chunk);
			pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		
		
		
		table.addCell(blank1Cell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(staxcell);
		table.addCell(pdftaxpercent);
		
	
		
		
		chunk = new Phrase(df.format(total),font9bold);
		pdfttlamt = new PdfPCell(chunk);
		pdfttlamt.setHorizontalAlignment(Element.ALIGN_CENTER);

		//  rohan added this code 
		String amountInWord = ServiceInvoicePdf.convert(total);
		
		String amountInWordWithLowerCase="";
		String[] spliturl=amountInWord.split(" ");
		
		System.out.println("RRR   spliturl.length"+spliturl.length);
		for(int i=1; i < spliturl.length;i++)
		{
			amountInWordWithLowerCase = amountInWordWithLowerCase+spliturl[i].toLowerCase()+" ";
		}
		
		
		
		Phrase inwords = new Phrase("(In words) Rs. "+spliturl[0]+" "+amountInWordWithLowerCase + "only.", font9bold);
		PdfPCell inwordscell = new PdfPCell(inwords);
		inwordscell.setBorderWidthTop(0);
		inwordscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		table.addCell(blank1Cell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(tamtcell);
		table.addCell(pdfttlamt);
		
	PdfPTable xx = new PdfPTable(1);
	xx.setWidthPercentage(100f);
	xx.addCell(inwordscell);
	
	try {
		document.add(table);
		document.add(xx);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}

public void createOPCSHeading(){
	
	Phrase bl = new Phrase(" ",font9);
	PdfPCell blcell = new PdfPCell(bl);
	blcell.setBorder(0);
	
	Phrase opcs = new Phrase("OPCS Assurance",font9bold);
	PdfPCell opcscell = new PdfPCell(opcs); 
	opcscell.setBorder(0);
	opcscell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase colon = new Phrase(":",font9);
	PdfPCell colcell = new PdfPCell(colon); 
	colcell.setBorder(0);
	colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase line = new Phrase("In case of pest resurgence during the contarct period, interim calls, if any, would be attended to, without any extra cost. ",font9);
	PdfPCell linecell = new PdfPCell(line);
	linecell.setBorder(0);
	linecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable otable = new PdfPTable(3);
	otable.addCell(opcscell);
	otable.addCell(colcell);
	otable.addCell(linecell);
	otable.setWidthPercentage(100f);
	otable.setSpacingBefore(10f);
	try {
		otable.setWidths(tblcowidth);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
//	PdfPCell ocell = new PdfPCell(otable);
//	ocell.setBorder(0);
	
//	PdfPTable table4 = new PdfPTable(1);
//	table4.addCell(ocell);
//	table4.setWidthPercentage(100);
	
	try {
		document.add(otable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


public void createNoteHeading(){
	
	PdfPCell descCell=null;
	PdfPCell bnoteCell=null;
	if(conRenw.getDescription()!=null){
		
		Phrase bnote = new  Phrase("Notes : ", font9bold);
		bnoteCell = new PdfPCell(bnote);
		bnoteCell.setBorder(0);
		
		Phrase description =new Phrase(conRenw.getDescription(),font9);
		descCell = new PdfPCell(description);
		descCell.setBorder(0);
	}
	
//	Phrase notes = new Phrase("Notes "+"-",font10bold);
//	PdfPCell notecell = new PdfPCell(notes);
//	notecell.setBorder(0);
//	notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	
//	Phrase abc = new Phrase("    -     Full Payment in Advance."+"\n"+"    -     Our treatment are Eco Friendly, Nontoxic & Odourless."+"\n"+"    -     No need to keep your premise close or vacant before or after service."+"\n"+"    -     Service details, SOP's, MSDS and Service schedule will be submitted after signing the contract."+"\n"+"    -     We are members of Pest Managemnet Association"+"\n"+"    -     An ISO 9001-2000 company.",font10);
//	PdfPCell abcCell = new PdfPCell(abc);
//	abcCell.setBorder(0);
//	abcCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase abc1 = new Phrase("We reiterate our commitment to ensure your complete satisfaction and look forward to hearing from you soon. Assuring you of our best service and attention at all times."+"\n"+"\n"+"\n"+"\n"+"Thanks & Regards, ",font9);
	PdfPCell abc1cell = new PdfPCell(abc1);
	abc1cell.setBorder(0);
	abc1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase abc2 = new Phrase("For Om Pest Control Services "+"\n"+"\n"+"\n"+"Authorised Signatory. "+"\n"+"Enclosure: Pest Fact Sheet",font9bold);
	PdfPCell abc2cell = new PdfPCell(abc2);
	abc2cell.setBorder(0);
	abc2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	Phrase spacing =new Phrase(Chunk.NEWLINE);
	PdfPCell spacinngCell = new PdfPCell(spacing);
	spacinngCell.setBorder(0);
	
	
	PdfPTable notetable = new PdfPTable(1);
//	notetable.addCell(notecell);
//	notetable.addCell(abcCell);
	if(conRenw.getDescription()!=null){
		
		notetable.addCell(bnoteCell);
		notetable.addCell(descCell);
	}
	notetable.addCell(spacinngCell);
	notetable.addCell(abc1cell);
	notetable.addCell(abc2cell);
	notetable.setWidthPercentage(100f);
	
	PdfPCell cell6 = new PdfPCell(notetable);
	cell6.setBorder(0);
	
	PdfPTable table5 = new PdfPTable(1);
	table5.addCell(cell6);
	table5.setWidthPercentage(100);
	
	try {
		document.add(table5);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
	
}
