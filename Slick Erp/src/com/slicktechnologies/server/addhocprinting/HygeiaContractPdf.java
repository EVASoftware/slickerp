package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.TimeZone;
import java.util.logging.Logger;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class HygeiaContractPdf {


	Logger logger = Logger.getLogger("Name of logger");

	ProcessConfiguration processConfig;
	boolean hygeiaPest = false;

	public Document document;

	Company comp;
	Customer cust;
	Invoice invoice;
	Branch branch;
	Phrase chunk;
	Contract con;
	Phrase col;
	List<Service> serviceList;
	String str;

	PdfPCell pdfserv, pdffreq, pdfperiod, pdfamt, pdfttl, pdftaxAmt,
			pdftaxAmt1, pdftaxAmt2;

	float[] colWidth = { 0.5f, 0.4f };
	float[] colWidth1 = { 0.3f, 0.1f, 0.3f };
	float[] colWidth2 = { 0.3f, 1.1f };

	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul, font9boldul, font4, font1;

	public HygeiaContractPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font4 = new Font(Font.FontFamily.HELVETICA, 4, Font.NORMAL);
		font1 = new Font(Font.FontFamily.HELVETICA, 1, Font.NORMAL);

	}

	public void setpdfcon(Long count) {

		con = ofy().load().type(Contract.class).id(count).now();

		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first().now();

		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("companyId", con.getCompanyId())
					.filter("count", con.getCustomerId()).first().now();

		// ********load service list**********
		serviceList = new ArrayList<Service>();
		List<Service> servlist = ofy().load().type(Service.class)
				.filter("companyId", con.getCompanyId())
				.filter("contractCount", con.getCount()).list();
		if (servlist.size() != 0) {
			serviceList.addAll(servlist);
		}
		System.out.println("SIZE OF SERVICE LIST"
				+ serviceList.addAll(servlist));

		// if (con.getCompanyId() == null)
		// service = ofy().load().type(Service.class)
		// .filter("contractCount", con.getCount()).first().now();
		// else
		// service = ofy().load().type(Service.class)
		// .filter("companyId", con.getCompanyId())
		// .filter("contractCount", con.getCount()).first().now();

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		// *********process config code*******************
		if (con.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", con.getCompanyId())
					.filter("processName", "Contract")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						hygeiaPest = true;
					}
				}
			}
		}

		String amountInWord = ServiceInvoicePdf.convert(Math.round(con
				.getNetpayable()));

	}

	public void createPdf() {

		// myMethod();
		createLogo(document, comp);
		createBlankHeading();
		createHeaderInfo();
		createCompAndCustAddress();
		createHardCodeInfo();
		createProductInfo();
		createLine();
		createCategoryAndAddress();
		// createLine1();
		createConInfoTable();
		createHardCode1Info();
		createBottomInfo();
		createBottomInfo1();
		createBottomInfo2();

	}

	// private void myMethod() {
	//
	// Paragraph para = new Paragraph("My Pdf........");
	// try {
	// document.add(para);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// }

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createBlankHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHeaderInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// ********left table*********
		PdfPTable leftTable = new PdfPTable(5);
		leftTable.setWidthPercentage(100f);

		Phrase no = new Phrase("Book No.", font10);
		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorderWidthRight(0);
		blcell.setBorderWidthLeft(0);
		blcell.setBorderWidthTop(0);

		Phrase dt = new Phrase("   Date", font10);
		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthTop(0);

		Phrase nr = new Phrase("NR", font10bold);
		PdfPCell nrcell = new PdfPCell(nr);
		nrcell.setBorder(0);
		nrcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(nocell);
		leftTable.addCell(blcell);
		leftTable.addCell(dtcell);
		leftTable.addCell(bl1cell);
		leftTable.addCell(nrcell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// ********right table*********
		PdfPTable rightTable = new PdfPTable(4);
		rightTable.setWidthPercentage(100f);

		Phrase code = new Phrase("Code No.", font10);
		PdfPCell codecell = new PdfPCell(code);
		codecell.setBorderWidthRight(0);
		codecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bl2 = new Phrase(" ", font10);
		PdfPCell bl2cell = new PdfPCell(bl2);
		bl2cell.setBorderWidthLeft(0);

		Phrase conno = new Phrase("Order -cum-Contract No", font10);
		PdfPCell connocell = new PdfPCell(conno);
		connocell.setBorder(0);
		connocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase no1 = null;
		if (con.getCount() != 0) {
			no1 = new Phrase(" " + con.getCount(), font10bold);
		} else {
			no1 = new Phrase(" ", font10bold);
		}
		PdfPCell no1cell = new PdfPCell(no1);
		no1cell.setBorder(0);
		no1cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		rightTable.addCell(codecell);
		rightTable.addCell(bl2cell);
		rightTable.addCell(connocell);
		rightTable.addCell(no1cell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.setSpacingAfter(3f);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompAndCustAddress() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 45, 55 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("To, " + "\n" + "\n"
					+ comp.getBusinessUnitName().toUpperCase(), font10bold);
		} else {
			company = new Phrase("", font10bold);
		}
		PdfPCell companyCell = new PdfPCell(company);
		companyCell.setBorder(0);
		companyCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(companyCell);

		// Phrase company1 = null;
		// if (comp.getBusinessUnitName() != null) {
		// company1 = new Phrase(
		// "" + comp.getBusinessUnitName().toUpperCase(), font10bold);
		// } else {
		// company1 = new Phrase("", font10bold);
		// }
		// PdfPCell company1Cell = new PdfPCell(company1);
		// company1Cell.setBorder(0);
		// company1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// leftTable.addCell(company1Cell);

		String compAdd1 = "";
		String compFullAdd1 = "";

		if (comp.getAddress() != null || !comp.getAddress().equals("")) {
			System.out.println("INSIDE COMPANY ADDRESS..............");

			if (comp.getAddress().getAddrLine2() != null
					|| !comp.getAddress().getAddrLine2().equals("")) {
				if (comp.getAddress().getLandmark() != null
						|| !comp.getAddress().getLandmark().equals("")) {
					compAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2() + ","
							+ comp.getAddress().getLandmark();
				} else {
					compAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getAddrLine2();
				}
			} else {
				if (comp.getAddress().getLandmark() != null
						|| !comp.getAddress().getLandmark().equals("")) {
					compAdd1 = comp.getAddress().getAddrLine1() + ","
							+ comp.getAddress().getLandmark() + ",";
				} else {
					compAdd1 = comp.getAddress().getAddrLine1();
				}
			}

			if (comp.getAddress().getLocality() != null
					|| !comp.getAddress().getLocality().equals("")) {

				compFullAdd1 = compAdd1 + "\n" + comp.getAddress().getCountry()
						+ "," + comp.getAddress().getState() + ","
						+ comp.getAddress().getCity() + " "
						+ comp.getAddress().getPin();
			}

		}
		Phrase compAddInfo1 = new Phrase("REGD. OFFICE : " + compFullAdd1,
				font9);
		PdfPCell compAddInfoCell = new PdfPCell(compAddInfo1);
		compAddInfoCell.setBorder(0);
		compAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(compAddInfoCell);

		Phrase contact = null;
		if (comp.getLandline() != null) {
			contact = new Phrase("Ph no. : " + comp.getLandline() + " / "
					+ comp.getLandline() + " / " + comp.getLandline(), font9);
		} else {
			contact = new Phrase("", font9);
		}
		PdfPCell contactCell = new PdfPCell(contact);
		contactCell.setBorder(0);
		contactCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(contactCell);

		Phrase web = null;
		if (comp.getWebsite() != null) {
			web = new Phrase("Visit us at " + comp.getWebsite(), font9);
		} else {
			web = new Phrase("", font9);
		}
		PdfPCell webCell = new PdfPCell(web);
		webCell.setBorder(0);
		webCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(webCell);

		/***************** Branch Addess ****************/
		Phrase branch = new Phrase("Nearest Branch : ", font9bold);
		PdfPCell branchCell = new PdfPCell(branch);
		// branchCell.setBorder(0);
		branchCell.setBorderWidthBottom(0);
		branchCell.setBorderWidthRight(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(branchCell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);

		String branchAdd1 = "";
		String branchFullAdd1 = "";

		// if (branch.getAddress() != null || !branch.getAddress().equals("")) {
		// System.out.println("INSIDE CUSTOMER ADDRESS..............");
		//
		// if (branch.getAddress().getAddrLine2() != null
		// || !branch.getAddress().getAddrLine2().equals("")) {
		// if (branch.getAddress().getLandmark() != null
		// || !branch.getAddress().getLandmark().equals("")) {
		// branchAdd1 = branch.getAddress().getAddrLine1() + ","
		// + branch.getAddress().getAddrLine2() + ","
		// + branch.getAddress().getLandmark();
		// } else {
		// branchAdd1 = branch.getAddress().getAddrLine1() + ","
		// + branch.getAddress().getAddrLine2();
		// }
		// } else {
		// if (branch.getAddress().getLandmark() != null
		// || !branch.getAddress().getLandmark().equals("")) {
		// branchAdd1 = branch.getAddress().getAddrLine1() + ","
		// + branch.getAddress().getLandmark() + ",";
		// } else {
		// branchAdd1 = branch.getAddress().getAddrLine1();
		// }
		// }
		//
		// if (branch.getAddress().getLocality() != null
		// || !branch.getAddress().getLocality().equals("")) {
		//
		// branchFullAdd1 = compAdd1 + "\n"
		// + branch.getAddress().getCountry() + ","
		// + branch.getAddress().getState() + ","
		// + branch.getAddress().getCity() + " "
		// + branch.getAddress().getPin();
		// }
		//
		// }
		// Phrase compAddInfo1 = new Phrase("" + compFullAdd1, font9);
		// PdfPCell compAddInfoCell = new PdfPCell(compAddInfo1);
		// compAddInfoCell.setBorder(0);
		// compAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// leftTable.addCell(compAddInfoCell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		// leftCell.setBorder(0);

		// ********right table*********
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		Phrase from = new Phrase("From, ", font10bold);
		PdfPCell fromCell = new PdfPCell(from);
		fromCell.setBorder(0);
		fromCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(fromCell);
		rightTable.addCell(blcell);

		Phrase name = null;
		if (cust.isCompany() == true) {
			name = new Phrase("" + cust.getCompanyName(), font9);
		} else {
			name = new Phrase("Name : " + cust.getFullname(), font9);
		}
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(nameCell);

		Phrase address = new Phrase("Address : (For Billing)", font9);
		PdfPCell addressCell = new PdfPCell(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(addressCell);

		/******** Customer address *********************/
		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null || !cust.getAdress().equals("")) {
			System.out.println("INSIDE CUSTOMER ADDRESS..............");

			if (cust.getAdress().getAddrLine2() != null
					|| !cust.getAdress().getAddrLine2().equals("")) {
				if (cust.getAdress().getLandmark() != null
						|| !cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (cust.getAdress().getLandmark() != null
						|| !cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getLandmark() + ",";
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null
					|| !cust.getAdress().getLocality().equals("")) {

				custFullAdd1 = custAdd1 + "," + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity();
			}

		}
		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setBorder(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(custAddInfoCell);

		Phrase pin = null;
		if (cust.getAdress().getPin() != 0 && cust.getCellNumber1() != null) {
			pin = new Phrase(
					"PIN Code : " + cust.getAdress().getPin()
							+ "                 " + "Mobile : "
							+ cust.getCellNumber1(), font9);
		} else {
			pin = new Phrase("", font9);
		}
		PdfPCell pinCell = new PdfPCell(pin);
		pinCell.setBorder(0);
		pinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(pinCell);

		Phrase email = null;
		if (cust.getEmail() != null) {
			email = new Phrase("Email : " + cust.getEmail(), font9);
		} else {
			email = new Phrase("", font9);
		}
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(emailCell);

		rightTable.addCell(blcell);
		rightTable.addCell(blcell);

		Phrase tel = null;
		if (cust.getLandline() != null && cust.getCellNumber2() != null) {
			tel = new Phrase("Tel :  (Resi)  022-" + cust.getLandline()
					+ "                 " + "(Off)  " + cust.getCellNumber2(),
					font9);
		} else {
			tel = new Phrase("", font9);
		}
		PdfPCell telCell = new PdfPCell(tel);
		telCell.setBorder(0);
		telCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(telCell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		// rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		// cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.setSpacingAfter(4f);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCodeInfo() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		String str = "";
		str = "with reference to the discussion with your representative, we are pleased to place our oredr-cum-contract on you for "
				+ "Pest Management Service/s as per the following particulars and on the terms and conditions mentioned overleaf : ";

		Phrase strp = new Phrase(str, font9);
		PdfPCell strpcell = new PdfPCell(strp);
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable strpTable = new PdfPTable(1);
		strpTable.setWidthPercentage(100);

		strpTable.addCell(strpcell);
		strpTable.setSpacingAfter(5f);

		try {
			document.add(strpTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createProductInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 70, 30 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// ********left table*********
		PdfPTable leftTable = new PdfPTable(4);

		try {
			leftTable.setWidths(new float[] { 85, 35, 65, 80 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		leftTable.setWidthPercentage(100f);

		Phrase no = new Phrase("A) TYPES OF SERVICES :", font9bold);
		Phrase nr = new Phrase("TERMS", font9bold);
		Phrase freq = new Phrase("FREQUENCY OF SERVICES", font9bold);
		Phrase premises = new Phrase("AREA OF PREMISES TO BE TREATED",
				font9bold);

		PdfPCell nocell = new PdfPCell(no);
		// nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell nrcell = new PdfPCell(nr);
		// nrcell.setBorder(0);
		nrcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell freqcell = new PdfPCell(freq);
		// freqcell.setBorder(0);
		freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell precell = new PdfPCell(premises);
		// precell.setBorder(0);
		precell.setHorizontalAlignment(Element.ALIGN_CENTER);

		leftTable.addCell(nocell);
		leftTable.addCell(nrcell);
		leftTable.addCell(freqcell);
		leftTable.addCell(precell);

		Phrase serv = null;
		PdfPCell servcCell = null;

		Phrase terms = null;
		PdfPCell termsCell = null;

		Phrase freq1 = null;
		PdfPCell freq1Cell = null;

		Phrase pre1 = null;
		PdfPCell pre1Cell = null;

		for (int i = 0; i < con.getItems().size(); i++) {

			serv = new Phrase(con.getItems().get(i).getProductName() + "",
					font9);
			servcCell = new PdfPCell(serv);
			servcCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			leftTable.addCell(servcCell);

			if (con.getItems().get(i).getDuration() != 0) {
				terms = new Phrase(con.getItems().get(i).getDuration() + "",
						font9);
			} else {
				terms = new Phrase("", font9);
			}
			termsCell = new PdfPCell(terms);
			termsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			leftTable.addCell(termsCell);

			if (con.getItems().get(i).getNumberOfServices() != 0) {
				freq1 = new Phrase(con.getItems().get(i).getNumberOfServices()
						+ "", font9);
			} else {
				freq1 = new Phrase("", font9);
			}
			freq1Cell = new PdfPCell(freq1);
			freq1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			leftTable.addCell(freq1Cell);

			if (con.getItems().get(i).getPremisesDetails() != null) {
				pre1 = new Phrase(con.getItems().get(i).getPremisesDetails()
						+ "", font9);
			}
			pre1Cell = new PdfPCell(pre1);
			pre1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			leftTable.addCell(pre1Cell);

		}

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// ********right table*********
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		// Phrase dt = new Phrase("Date : ", font10);
		// Phrase time = new Phrase("Time : ", font10);
		//
		// PdfPCell dtcell = new PdfPCell(dt);
		// dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// PdfPCell timecell = new PdfPCell(time);
		// timecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//
		// rightTable.addCell(dtcell);
		// rightTable.addCell(timecell);

		Phrase dt1 = null;
		PdfPCell dt1cell = null;

		/****************************/
		// if (con.getCreationDate() != null) {
		// dt1 = new Phrase("Date : " + fmt.format(con.getCreationDate()),
		// font9);
		// } else {
		// dt1 = new Phrase(" ", font10bold);
		// }

		//    rohan added this code for sorting service list by service date 
		
		
		
		for (int i = 0; i < serviceList.size(); i++) {

			
			if(serviceList.get(i).getServiceSerialNo()==1)
			{
				dt1 = new Phrase("Date : "+ fmt.format(serviceList.get(i).getServiceDate()),font9);
				break;
				
			}
		}
		
		
		
		dt1cell = new PdfPCell(dt1);
		dt1cell.setBorderWidthBottom(0);
		dt1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(dt1cell);

		Phrase blk1 = new Phrase(" ", font9bold);
		PdfPCell blk1cell = new PdfPCell(blk1);
		blk1cell.setBorderWidthTop(0);
		rightTable.addCell(blk1cell);

		Phrase tm = null;
		PdfPCell tmcell = null;

		for (int i = 0; i < serviceList.size(); i++) {

			if (!serviceList.get(i).getServiceTime().equals("Flexible")) {
				tm = new Phrase(
						"Time : " + serviceList.get(i).getServiceTime(), font9);
			} else {
				tm = new Phrase(" ", font10bold);
			}
		}
		tmcell = new PdfPCell(tm);
		tmcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(tmcell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.setSpacingAfter(4f);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createLine() {

		Phrase line = new Phrase("B) PREMISES TO BE TREATED : ", font9bold);
		PdfPCell linecell = new PdfPCell(line);
		linecell.setBorder(0);
		linecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable lineTable = new PdfPTable(1);
		lineTable.setWidthPercentage(100);
		lineTable.addCell(linecell);
		lineTable.setSpacingAfter(7f);

		try {
			document.add(lineTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCategoryAndAddress() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 20, 80 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// ********left table**************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		Phrase category = null;
		if (con.getCategory() != null) {
			category = new Phrase("" + con.getCategory(), font9);
		}
		PdfPCell categorycell = new PdfPCell(category);
		// categorycell.setBorder(0);
		categorycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(categorycell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// ********right table*********
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getSecondaryAdress() != null
				|| !cust.getSecondaryAdress().equals("")) {
			System.out.println("INSIDE CUSTOMER ADDRESS..............");

			if (cust.getSecondaryAdress().getAddrLine2() != null
					|| !cust.getSecondaryAdress().getAddrLine2().equals("")) {
				if (cust.getSecondaryAdress().getLandmark() != null
						|| !cust.getSecondaryAdress().getLandmark().equals("")) {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getAddrLine2() + ","
							+ cust.getSecondaryAdress().getLandmark();
				} else {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getAddrLine2();
				}
			} else {
				if (cust.getSecondaryAdress().getLandmark() != null
						|| !cust.getSecondaryAdress().getLandmark().equals("")) {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getLandmark() + ",";
				} else {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1();
				}
			}

			if (cust.getSecondaryAdress().getLocality() != null
					|| !cust.getSecondaryAdress().getLocality().equals("")) {

				custFullAdd1 = custAdd1
						+ cust.getSecondaryAdress().getCountry() + ","
						+ cust.getSecondaryAdress().getState() + ","
						+ cust.getSecondaryAdress().getCity() + " "
						+ cust.getSecondaryAdress().getPin();
			}

		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		// custAddInfoCell.setBorder(0);
		custAddInfoCell.setBorderWidthBottom(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(custAddInfoCell);

		Phrase contact = null;
		if (cust.getCellNumber1() != null) {
			contact = new Phrase("Tel No. : 022-" + cust.getLandline(), font9);
		} else {
			contact = new Phrase("", font9);
		}
		PdfPCell contactCell = new PdfPCell(contact);
		// contactCell.setBorder(0);
		contactCell.setBorderWidthTop(0);
		contactCell.setBorderWidthBottom(0);
		contactCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(contactCell);

		Phrase poc = null;
		if (cust.isCompany() == true) {
			poc = new Phrase("Contact Person : " + cust.getFullname(), font9);
		} else {
			poc = new Phrase("", font9);
		}
		PdfPCell pocCell = new PdfPCell(poc);
		// pocCell.setBorder(0);
		pocCell.setBorderWidthTop(0);
		pocCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTable.addCell(pocCell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.setSpacingAfter(5f);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// private void createLine1() {
	//
	// Phrase line = new Phrase("C) AREA OF PREMISES TO BE TREATED : ",
	// font9bold);
	// PdfPCell linecell = new PdfPCell(line);
	// linecell.setBorder(0);
	// linecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//
	// PdfPTable lineTable = new PdfPTable(1);
	// lineTable.setWidthPercentage(100);
	// lineTable.addCell(linecell);
	// lineTable.setSpacingAfter(5f);
	//
	// try {
	// document.add(lineTable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// }

	private void createConInfoTable() {

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 25, 75 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// ********left table*********
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		Phrase no = new Phrase("C) TERMS OF PAYMENT ", font9bold);
		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase time = new Phrase("D) CONTRACT PERIOD ", font9bold);
		PdfPCell timecell = new PdfPCell(time);
		timecell.setBorder(0);
		timecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase nr = new Phrase("E) BILLING INSTRUCTIONS", font9bold);
		PdfPCell nrcell = new PdfPCell(nr);
		nrcell.setBorder(0);
		nrcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(nocell);
		leftTable.addCell(timecell);
		leftTable.addCell(nrcell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// ********right table*********
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		Phrase pay = null;
		for (int i = 0; i < con.getPaymentTermsList().size(); i++) {
			if (con.getPaymentTermsList().get(i).getPayTermPercent() != null
					&& con.getPaymentTermsList().get(i).getPayTermComment() != null) {
				pay = new Phrase(Math.round(con.getPaymentTermsList().get(i)
						.getPayTermPercent())
						+ " % "
						+ "     "
						+ con.getPaymentTermsList().get(i).getPayTermComment(),
						font10);
			} else {
				pay = new Phrase("");
			}
		}
		PdfPCell paycell = new PdfPCell(pay);
		paycell.setBorder(0);
		paycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dates = null;
		if (con.getStartDate() != null && con.getEndDate() != null) {
			dates = new Phrase("" + fmt.format(con.getStartDate()) + "   to   "
					+ "" + fmt.format(con.getEndDate()), font10);
		}
		PdfPCell dtcell = new PdfPCell(dates);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase billing = null;
		if (con.getDescription() != null) {
			billing = new Phrase(" " + con.getDescription(), font10);
		} else {
			billing = new Phrase(" ", font10);
		}
		PdfPCell billingcell = new PdfPCell(billing);
		billingcell.setBorder(0);
		billingcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		rightTable.addCell(paycell);
		rightTable.addCell(dtcell);
		rightTable.addCell(billingcell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.setSpacingAfter(5f);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createHardCode1Info() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 15, 85 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase charge = new Phrase("F) SERVICE CHARGES : ", font9bold);
		PdfPCell chargecell = new PdfPCell(charge);
		chargecell.setBorder(0);
		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String amountInWord = ServiceInvoicePdf.convert(Math.round(con
				.getNetpayable()));
		String str = "We hereby agree to pay your charges of Rs. "
				+ Math.round(con.getNetpayable())
				+ " /- "
				+ "(Rupees "
				+ amountInWord
				+ " Only) "
				+ "& Service Tax wherever applicable, for this contract prior to commencement of initial treatment, for management of pests as specified "
				+ "herein and / or in the schedule to the extent necessary to reasonably free the said premises from their presence. "
				+ "I / We have understood your contract terms and requirements for the treatments, as mentioned overleaf. "
				+ "\n"
				+ "(Please sign and return a copy of this Order-cum-Contract in acceptance)"
				+ "                        Yours truely, " + "\n"
				+ "We accept the Order-cum-Contract with thanks ";

		Phrase strp = new Phrase(str, font9);
		PdfPCell strpcell = new PdfPCell(strp);
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		table.addCell(chargecell);
		table.addCell(strpcell);
		table.setSpacingAfter(5f);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createBottomInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 60, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("For " + comp.getBusinessUnitName(),
					font10bold);
		} else {
			company = new Phrase("", font10bold);
		}
		PdfPCell compCell = new PdfPCell(company);
		compCell.setBorder(0);
		compCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// ********emp table***************
		PdfPTable empTable = new PdfPTable(2);

		try {
			empTable.setWidths(new float[] { 45, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		empTable.setWidthPercentage(100f);

		Phrase emp = new Phrase(
				"(Name & Designation of Signatory)  Empolyee Code   : ", font9);
		PdfPCell empcell = new PdfPCell(emp);
		empcell.setBorder(0);
		empcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		empTable.addCell(empcell);

		Phrase bl1 = new Phrase(" ", font9);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		empTable.addCell(bl1cell);

		PdfPCell empCell = new PdfPCell(empTable);
		empCell.setBorder(0);

		leftTable.addCell(compCell);
		leftTable.addCell(blcell);
		leftTable.addCell(blcell);
		leftTable.addCell(empCell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// *************righ table************
		PdfPTable righTable = new PdfPTable(1);
		righTable.setWidthPercentage(100);

		Phrase sign = new Phrase("(Customer Signature & Office Stamp) ", font9);
		PdfPCell signcell = new PdfPCell(sign);
		signcell.setBorder(0);
		signcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable panTable = new PdfPTable(2);

		try {
			panTable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		panTable.setWidthPercentage(100f);

		Phrase pan = null;
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
				pan = new Phrase(" "
						+ cust.getArticleTypeDetails().get(i)
								.getArticleTypeName()+" :", font8);
			}
		}
		PdfPCell pancell = new PdfPCell(pan);
		pancell.setBorder(0);
		pancell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		panTable.addCell(pancell);

		PdfPTable pTable = new PdfPTable(1);
		// PdfPTable pTable = new PdfPTable(10);
		// try {
		// pTable.setWidths(new float[] { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 });
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
		pTable.setWidthPercentage(100f);

		Phrase blk = null;

		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
				blk = new Phrase(""
						+ cust.getArticleTypeDetails().get(i)
								.getArticleTypeValue(), font8);
			}
		}

		// Phrase blk1 = new Phrase(" ", font1);
		// Phrase bl2 = new Phrase(" ", font1);
		// Phrase bl3 = new Phrase(" ", font1);
		// Phrase bl4 = new Phrase(" ", font1);
		// Phrase bl5 = new Phrase(" ", font1);
		// Phrase bl6 = new Phrase(" ", font1);
		// Phrase bl7 = new Phrase(" ", font1);
		// Phrase bl8 = new Phrase(" ", font1);
		// Phrase bl9 = new Phrase(" ", font1);

		PdfPCell blkCell = new PdfPCell(blk);
		blkCell.setBorder(0);
		blkCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// PdfPCell blk1Cell = new PdfPCell(blk1);
		// PdfPCell bl2Cell = new PdfPCell(bl2);
		// PdfPCell bl3Cell = new PdfPCell(bl3);
		// PdfPCell bl4Cell = new PdfPCell(bl4);
		// PdfPCell bl5Cell = new PdfPCell(bl5);
		// PdfPCell bl6Cell = new PdfPCell(bl6);
		// PdfPCell bl7Cell = new PdfPCell(bl7);
		// PdfPCell bl8Cell = new PdfPCell(bl8);
		// PdfPCell bl9Cell = new PdfPCell(bl9);

		pTable.addCell(blkCell);
		// pTable.addCell(blk1Cell);
		// pTable.addCell(bl2Cell);
		// pTable.addCell(bl3Cell);
		// pTable.addCell(bl4Cell);
		// pTable.addCell(bl5Cell);
		// pTable.addCell(bl6Cell);
		// pTable.addCell(bl7Cell);
		// pTable.addCell(bl8Cell);
		// pTable.addCell(bl9Cell);

		PdfPCell pCell = new PdfPCell(pTable);
		pCell.setBorder(0);
		pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		panTable.addCell(pCell);

		PdfPCell abcCell = new PdfPCell(panTable);
		abcCell.setBorder(0);

		Phrase date = new Phrase("Date : ", font9);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		righTable.addCell(blcell);
		righTable.addCell(signcell);
		righTable.addCell(abcCell);
		righTable.addCell(datecell);

		PdfPCell rightCell = new PdfPCell(righTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createBottomInfo1() {

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);

		Phrase payment = new Phrase("G) PAYMENT DETAILS : "
				+ "                                  For Office Use Only ",
				font10bold);
		PdfPCell paycell = new PdfPCell(payment);
		paycell.setBorder(0);
		paycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		// *********upper table**************
		PdfPTable upTable = new PdfPTable(4);

		try {
			upTable.setWidths(new float[] { 30, 10, 20, 40 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		upTable.setWidthPercentage(100f);

		Phrase cash = new Phrase("CHEQUE NO. / CASH", font10);
		Phrase date = new Phrase("DATE", font10);
		Phrase amt = new Phrase("AMOUNT (Rs.)", font10);
		Phrase bank = new Phrase("NAME OF THE BANK AND BRANCH", font10);

		PdfPCell cashCell = new PdfPCell(cash);
		cashCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell amtCell = new PdfPCell(amt);
		amtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell bankCell = new PdfPCell(bank);
		bankCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		upTable.addCell(cashCell);
		upTable.addCell(dateCell);
		upTable.addCell(amtCell);
		upTable.addCell(bankCell);

		PdfPCell cell = new PdfPCell(upTable);
		cell.setBorder(0);

		table.addCell(paycell);
		table.addCell(cell);

		PdfPCell tcell = new PdfPCell(table);
		// tcell.setBorder(0);
		tcell.setBorderWidthLeft(0);
		tcell.setBorderWidthRight(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(tcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createBottomInfo2() {

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);

		// *******lower table*********
		PdfPTable lowTable = new PdfPTable(8);
		lowTable.setWidthPercentage(100f);

		Phrase branch = new Phrase("Branch", font10);
		Phrase bill = new Phrase("Bill No.", font10);
		Phrase amount = new Phrase("Amount (Rs.)", font10);
		Phrase dt = new Phrase("Date", font10);
		Phrase noted = new Phrase("Billing Instr. Noted", font10);
		Phrase sheet = new Phrase("Prog. Sheet Made", font10);
		Phrase card = new Phrase("Office Log Card Made", font10);
		Phrase initial = new Phrase("Initial of Manager", font10);

		PdfPCell branchCell = new PdfPCell(branch);
		branchCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell billCell = new PdfPCell(bill);
		billCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell amt1Cell = new PdfPCell(amount);
		amt1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell dtCell = new PdfPCell(dt);
		dtCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell noteCell = new PdfPCell(noted);
		noteCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell sheetCell = new PdfPCell(sheet);
		sheetCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell cardCell = new PdfPCell(card);
		cardCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell initialCell = new PdfPCell(initial);
		initialCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		lowTable.addCell(branchCell);
		lowTable.addCell(billCell);
		lowTable.addCell(amt1Cell);
		lowTable.addCell(dtCell);
		lowTable.addCell(noteCell);
		lowTable.addCell(sheetCell);
		lowTable.addCell(cardCell);
		lowTable.addCell(initialCell);

		PdfPCell cell1 = new PdfPCell(lowTable);
		cell1.setBorder(0);

		table.addCell(cell1);
		table.setSpacingBefore(3f);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
