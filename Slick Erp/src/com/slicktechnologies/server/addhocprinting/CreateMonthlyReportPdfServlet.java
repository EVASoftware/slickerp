package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateMonthlyReportPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3941160456464348259L;

	Logger logger = Logger.getLogger("NameOfYourLogger");
	
	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
		  
		  MonthlyReportPdf mypdf = new MonthlyReportPdf();
		  	mypdf.document = new Document(PageSize.A4.rotate());
			Document document = mypdf.document;
			PdfWriter.getInstance(document, response.getOutputStream()); 
			document.open();

			String stringid = request.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			
			String preprintStatus=request.getParameter("date");
			System.out.println("in serv let "+preprintStatus);
			String[] dataRetrieved=preprintStatus.split("[$]");
						
			String fromDt=dataRetrieved[0];
			String toDt=dataRetrieved[1];
			
			
			 logger.log(Level.SEVERE,"form date before :::"+fromDt);	
			 logger.log(Level.SEVERE,"to date before :::"+toDt);	
			
			System.out.println("from date "+fromDt);
			System.out.println("to date "+toDt);
			
			SimpleDateFormat fmt12345 =  new SimpleDateFormat("dd-MM-yyyy");
			fmt12345.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
//			 SimpleDateFormat fmt123 = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
			 
			 Date fromDate =null;
			 try {
				 fromDate = fmt12345.parse(fromDt);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 System.out.println("fromDate Date after parsing"+fromDate);
			 logger.log(Level.SEVERE,"fromDate Date after parsing"+fromDate);	
			 
			 Date toDate =null;
			 try {
				 toDate = fmt12345.parse(toDt);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 System.out.println("to date Date after parsing"+toDate);
			 logger.log(Level.SEVERE,"to date Date after parsing"+toDate);	
			 
//			 try {
//				System.out.println("toDate Date after parsing"+fmt12345.parse(toDate.toString()));
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			 try {
//				logger.log(Level.SEVERE,"toDate Date after parsing"+fmt12345.parse(toDate.toString()));
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}	
			
			PdfWriter.getInstance(document, response.getOutputStream()); 
			document.open();
			
			mypdf.createMontlyReport(count,fromDate,toDate);
			mypdf.createPdf();
			document.close();

	  }
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }

}
}
