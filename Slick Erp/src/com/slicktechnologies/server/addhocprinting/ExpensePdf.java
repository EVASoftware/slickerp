package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
//import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Expense;
import com.slicktechnologies.shared.common.businessprocesslayer.MultipleExpenseMngt;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class ExpensePdf {


	Logger logger = Logger.getLogger("ExpensePdf.class");

	public Document document;

	Company comp;
	Customer cust;
	Expense exp;
	
	float round = 0.00f;
	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	float[] colWidth = { 70f, 15f, 15f };
	float[] columnWidth = { (float) 33.33, (float) 33.33, (float) 33.33 };
	float[] col1Width = { 10f, 90f };
	float[] col2Width = { 25f, 75f };

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul;

	public ExpensePdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	}

	public void setpdfexpense(Long count) {

			exp = ofy().load().type(Expense.class).id(count).now();
		
	
		if (exp.getCompanyId() != null){
			comp = ofy().load().type(Company.class).filter("companyId", exp.getCompanyId()).first().now();
		}
		else{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		
		if (exp.getCompanyId() != null){
			cust = ofy().load().type(Customer.class).filter("companyId", exp.getCompanyId()).filter("count", exp.getUserId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", exp.getUserId()).first().now();
		}
		 fmt.setTimeZone(TimeZone.getTimeZone("IST"));

	}

	public void createPdf() {

		createVoucherHeading();
		createExpenseDetails();
		createEmployeeDetail();
		createAccountDetail();
		createAmountDetail();
		createBlankDetail();
		createInWordsTable();
		createSignature();

		String amountInWord = ServiceInvoicePdf.convert(exp.getAmount());

	}

	public void createVoucherHeading() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(blank1);
		bl1cell.setBorder(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase head = new Phrase("Expense Voucher", font14boldul);
		PdfPCell headcell = new PdfPCell(head);
		headcell.setBorder(0);
		headcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable headtable = new PdfPTable(3);
		headtable.addCell(blcell);
		headtable.addCell(headcell);
		headtable.addCell(bl1cell);
		headtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(headtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);

//		try {
//			table.setWidths(columnWidth);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createExpenseDetails() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase no = null;
		if (exp.getCount() != 0) {
			no = new Phrase("No. : " + exp.getCount(), font10bold);
		}

		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase date = null;
		if (exp.getExpenseDate() != null) {
			logger.log(Level.SEVERE,
					"Expense date ::::::::::::::::::" + exp.getExpenseDate());
			
			System.out.println("Expense date ::::::::::::::::::"
					+ exp.getExpenseDate());

			date = new Phrase("Date : " + fmt.format(exp.getExpenseDate()),
					font10bold);

			System.out.println("Date Format::::::::::::::::::::"
					+ fmt.format(exp.getExpenseDate()));
			logger.log(
					Level.SEVERE,
					"Date Format::::::::::::::::::::"
							+ fmt.format(exp.getExpenseDate()));
		}

		PdfPCell dtcell = new PdfPCell(date);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable detailtable = new PdfPTable(3);
		detailtable.addCell(nocell);
		detailtable.addCell(blcell);
		detailtable.addCell(dtcell);
		detailtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(detailtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);

		// try {
		// table.setWidths(columnWidth);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createAccountDetail() {

		Phrase blank = new Phrase(" " + exp.getAccount(), font10bold);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorderWidthLeft(0);
		blcell.setBorderWidthRight(0);
		blcell.setBorderWidthTop(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase acc = new Phrase("A/c. : ", font10bold);
		PdfPCell accountcell = new PdfPCell(acc);
		accountcell.setBorder(0);
		accountcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank1 = null;
		if (exp.getGroup() != null) {
			blank1 = new Phrase(" ");
		}

		PdfPCell bl1cell = new PdfPCell(blank1);
		bl1cell.setBorder(0);

		PdfPTable acctable = new PdfPTable(2);
		acctable.addCell(accountcell);
		acctable.addCell(blcell);

		try {
			acctable.setWidths(col1Width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		acctable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(acctable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createInWordsTable() {

		String amountInWord = ServiceInvoicePdf.convert(exp.getAmount());

		Phrase blank = new Phrase(" " + amountInWord + " Only ", font10bold);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorderWidthLeft(0);
		blcell.setBorderWidthRight(0);
		blcell.setBorderWidthTop(0);
		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase acc = new Phrase("Rupees in Words : ", font10bold);
		PdfPCell accountcell = new PdfPCell(acc);
		accountcell.setBorder(0);
		accountcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(blank1);
		bl1cell.setBorder(0);

		PdfPTable acctable = new PdfPTable(2);
		acctable.addCell(accountcell);
		acctable.addCell(blcell);

		try {
			acctable.setWidths(col2Width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		acctable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(acctable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);
		table.addCell(bl1cell);

		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createSignature() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase sign1 = new Phrase("Receiver's Signature ", font10bold);
		PdfPCell sign1cell = new PdfPCell(sign1);
		sign1cell.setBorder(0);
		sign1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase blank2 = new Phrase(" ");
		PdfPCell bl2cell = new PdfPCell(blank2);
		bl2cell.setBorder(0);
		bl2cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase sign2 = new Phrase("Signature ", font10bold);
		PdfPCell sign2cell = new PdfPCell(sign2);
		sign2cell.setBorder(0);
		sign2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPTable detailtable = new PdfPTable(3);
		detailtable.addCell(sign1cell);
		detailtable.addCell(blcell);
		detailtable.addCell(sign2cell);

		try {
			detailtable.setWidths(columnWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		detailtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(detailtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(bl2cell);
		table.addCell(bl2cell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createAmountDetail() {

		// Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		// PdfPTable table = new PdfPTable(3);
		//
		// try {
		// table.setWidths(colWidth);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
		// table.setWidthPercentage(100);
		//
		// Phrase par = new Phrase("Particulars", font1);
		// Phrase rs = new Phrase("Rs.", font1);
		// Phrase ps = new Phrase("Ps.", font1);
		//
		// PdfPCell parcell = new PdfPCell(par);
		// parcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// PdfPCell rscell = new PdfPCell(rs);
		// rscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// PdfPCell pscell = new PdfPCell(ps);
		// pscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		//
		// table.addCell(parcell);
		// table.addCell(rscell);
		// table.addCell(pscell);
		//
		// for(int i=0; i<exp.){
		//
		// }
		/**************************************************/

		Phrase par = new Phrase("Particular", font10bold);
		PdfPCell parcell = new PdfPCell(par);
		parcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(parcell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/*******************************************************************/

		Phrase rs = new Phrase("Rs.", font10bold);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable centertable = new PdfPTable(1);
		centertable.addCell(rscell);

		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setHorizontalAlignment(Element.ALIGN_CENTER);
		/*********************************************************************/

		Phrase ps = new Phrase("Ps.", font10bold);
		PdfPCell pscell = new PdfPCell(ps);
		pscell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(pscell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);

		try {
			table.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createBlankDetail() {
		
		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		
		//***********to get particulars**************
		Phrase blank1 = null;
		if (exp.getDescription() != null) {
			blank1 = new Phrase(" " + exp.getDescription(), font10bold);
		}
		PdfPCell blank1cell = new PdfPCell(blank1);
		blank1cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable lefttable = new PdfPTable(1);
		lefttable.addCell(blank1cell);

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		//***********to get amount in Rs.**************
		Phrase blank2 = null;
		if (exp.getAmount() != null) {
			blank2 = new Phrase(" " + df.format(exp.getAmount()), font10bold);
		}
		PdfPCell blank2cell = new PdfPCell(blank2);
		blank2cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable centertable = new PdfPTable(1);
		centertable.addCell(blank2cell);

		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		//***********to get amount in Paise**************
		Phrase blank3 = new Phrase(" " + round, font10bold);
		PdfPCell blank3cell = new PdfPCell(blank3);
		blank3cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable righttable = new PdfPTable(1);
		righttable.addCell(blank3cell);

		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);

		try {
			table.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		table.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(table);

		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);

		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void createEmployeeDetail() {

		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase name = null;
		if (exp.getEmployee() != null) {
			name = new Phrase("Person Responsible : " + exp.getEmployee(),
					font10bold);
		}

		PdfPCell namecell = new PdfPCell(name);
		namecell.setBorder(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable nametable = new PdfPTable(1);
		nametable.addCell(namecell);
		nametable.addCell(blcell);
		nametable.addCell(blcell);
		nametable.setWidthPercentage(100);

		try {
			document.add(nametable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
