package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.servlet.http.HttpServlet;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class QuotationPdfFormatVersion2 extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8445287761820373363L;

	
	Font font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	Font font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	Font font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	Font font14 = new Font(Font.FontFamily.HELVETICA, 14);
	Font font7 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	Font font9 = new Font(Font.FontFamily.HELVETICA, 9);
	Font font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	Font font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	Font font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	Font font8 = new Font(Font.FontFamily.HELVETICA, 8);
	Font font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	Font font12 = new Font(Font.FontFamily.HELVETICA, 12);
	Font font11 = new Font(Font.FontFamily.HELVETICA, 11);
	Font font10 = new Font(Font.FontFamily.HELVETICA, 10);
	Font font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	Font font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);

	Font font6 = new Font(Font.FontFamily.HELVETICA, 7);
	Font font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
	
	Font font8boldul = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD| Font.UNDERLINE);
	Font titlefont=new Font(Font.FontFamily.HELVETICA,14,Font.BOLD);
	Font font12Ul = new Font(Font.FontFamily.HELVETICA, 12, Font.UNDERLINE);


	public Document document;
	Quotation quotation;
	Company comp;
	Customer cust;
	Branch branchEntity;
	Employee employee;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat decimalformat = new DecimalFormat("0.00");
	
	PdfPCell cellBlank;
	
//	float[] column8SerProdCollonWidth = { 0.67f, 0.68f, 0.35f, 0.28f,
//			0.50f, 0.50f };
	
	float[] column8SerProdCollonWidth = { 0.72f, 0.73f, 0.35f, 0.38f,
			0.80f};
	boolean branchAsCompanyFlag = false;
	
	PdfUtility pdfUtility = new PdfUtility();

	public void setservicequotation(Long count) {

		quotation=ofy().load().type(Quotation.class).id(count).now();
		
		if (quotation.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		
		if (quotation.getCompanyId() != null)
		{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).first().now();
		}
		
		branchEntity = ofy().load().type(Branch.class).filter("buisnessUnitName", quotation.getBranch()).filter("companyId", quotation.getCompanyId()).first().now();
		employee = ofy().load().type(Employee.class).filter("fullname", quotation.getEmployee()).filter("companyId", quotation.getCompanyId()).first().now();
				
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		Phrase phblank = new Phrase(" ",font6);
//		Paragraph blank = new Paragraph();
//		blank.add(Chunk.NEWLINE);
		cellBlank=new PdfPCell(phblank);
		cellBlank.setBorder(0);
		
		branchAsCompanyFlag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId());
		
		try {
			BaseFont verdanafont = BaseFont.createFont("verdana.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
			font12 = new Font(verdanafont, 11);
			font12bold = new Font(verdanafont, 11, Font.BOLD);

		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void createPdf(String preprintStatus) {
		
		
		createCustomerInfo();
		createFixedData();
		createProductDetails(preprintStatus);
		createLastPart();
		
	}



	private void createLastPart() {
		
		PdfPTable lastPartTable = new PdfPTable(1);
		lastPartTable.setWidthPercentage(100);
		lastPartTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		lastPartTable.addCell(cellBlank);
		
//		String paragraph1 = comp.getBusinessUnitName()+" is a certified Pest Professional of �Bayer Network Expert Alliance�";
//		lastPartTable.addCell(pdfUtility.getCell(paragraph1,font11bold, Element.ALIGN_CENTER, 0, 0, 0,0));
//		lastPartTable.addCell(cellBlank);
		
		String paragraph2 = "�All OF OUR TECHNICIANS MOVE ON MOTORCYCLE TO GIVE YOU FASTER SERVICE.�";
		lastPartTable.addCell(pdfUtility.getCell(paragraph2,font10bold, Element.ALIGN_CENTER, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);
		
		
		PdfPTable noteTable = new PdfPTable(2);
		noteTable.setWidthPercentage(100);
		
		String licenceNo = getlicenseNo(comp);
		String paragraph3 = "We are certified by the department of agriculture under the insecticide act issued by the Government of Maharashtra. Our license no is "+licenceNo+". A photocopy is also attached.";

		float[] notetablecolumnwidth = { 0.06f, 0.70f };
		try {
			noteTable.setWidths(notetablecolumnwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		noteTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		noteTable.addCell(pdfUtility.getCell("Note : - ",font10bold, Element.ALIGN_LEFT, 0, 0, 0,0));
		noteTable.addCell(pdfUtility.getCell(paragraph3,font12, Element.ALIGN_LEFT, 0, 0, 0,0));

		PdfPCell noteCell = new PdfPCell(noteTable);
		noteCell.setBorder(0);
		
		lastPartTable.addCell(noteCell);
		lastPartTable.addCell(cellBlank);

		String reviews = ServerAppUtility.getForProcessConfigurartionIsActiveOrNot("GetReviewsForQuotationPdf", quotation.getCompanyId());
		String feedbackUrl = ServerAppUtility.getForProcessConfigurartionIsActiveOrNot("GetReviewsLinkForQuotationPdf", quotation.getCompanyId());;
		if(reviews==null){
			reviews = "";
		}
		if(feedbackUrl==null){
			feedbackUrl = "";
		}
		
		String paragraph4 = "We have more than "+reviews+" Reviews on Google. Please see what are Clients are saying about us. "+feedbackUrl;
		lastPartTable.addCell(pdfUtility.getCell(paragraph4,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);


		String paymentTerms ="";
		if(quotation.getPayTerms()!=null && !quotation.getPayTerms().equals("")){
			paymentTerms = quotation.getPayTerms();
		}
		else{
			paymentTerms = quotation.getPaymentTermsList().get(0).getPayTermComment();
		}
		
		PdfPTable paymenttermstable = new PdfPTable(2);
		paymenttermstable.setWidthPercentage(100);
		
		float[] columnwidth = { 0.20f, 0.70f };
		try {
			paymenttermstable.setWidths(columnwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		paymenttermstable.setHorizontalAlignment(Element.ALIGN_LEFT);
		paymenttermstable.addCell(pdfUtility.getCell("TERMS OF PAYMENT: ",font10bold, Element.ALIGN_LEFT, 0, 0, 0,0));
		paymenttermstable.addCell(pdfUtility.getCell(paymentTerms,font12, Element.ALIGN_LEFT, 0, 0, 0,0));

		PdfPCell paymenttermcell = new PdfPCell(paymenttermstable);
		paymenttermcell.setBorder(0);
		
		lastPartTable.addCell(paymenttermcell);
		lastPartTable.addCell(cellBlank);

		String para5 = "Payment to be made via cheque/Neft/Rtgs/Gpay.";
		lastPartTable.addCell(pdfUtility.getCell(para5,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);

		String paragraph5 = "All Payments should be made payable to "+comp.getBusinessUnitName();
		lastPartTable.addCell(pdfUtility.getCell(paragraph5,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);

		String para6 = "GST will be charged as per applicable rate.";
		lastPartTable.addCell(pdfUtility.getCell(para6,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);

		String salespersonName ="";
		if(quotation.getEmployee()!=null && !quotation.getEmployee().equals("")){
			salespersonName = quotation.getEmployee();
		}
		String salesperonCellNumber = "";
		if(employee!=null && employee.getCellNumber1()!=0){
			salesperonCellNumber = employee.getCellNumber1()+"";
		}
		String paragraph6 ="";
		if(!salesperonCellNumber.equals("")){
			paragraph6 = "Please feel free  to contact Mr. "+salespersonName+" on mobile no. "+salesperonCellNumber+"/ 9920735713 in case you need further information.";
		}
		else{
			paragraph6 = "Please feel free  to contact Mr. "+salespersonName+" on mobile no. 9920735713 in case you need further information.";

		}
		lastPartTable.addCell(pdfUtility.getCell(paragraph6,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);
		
		
		String paragraph7 = "We look forward in getting your valuable order.  Kindly call us of any clarifications.";
		lastPartTable.addCell(pdfUtility.getCell(paragraph7,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(cellBlank);

		
		PdfPTable validitytable = new PdfPTable(2);
		validitytable.setWidthPercentage(100);
		
		float[] validityColumnWidth = { 0.10f, 0.70f };
		try {
			validitytable.setWidths(validityColumnWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int validityduration = 90;
		if(quotation.getValidUntill()!=null){
			validityduration = DateUtility.getNumberOfDaysBetweenDates(quotation.getValidUntill(), quotation.getQuotationDate());
		}
		String validity = validityduration+""+" Days from the date of quotation given";
		
		validitytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		validitytable.addCell(pdfUtility.getCell("VALIDITY : - ",font10bold, Element.ALIGN_LEFT, 0, 0, 0,0));
		validitytable.addCell(pdfUtility.getCell(validity,font12, Element.ALIGN_LEFT, 0, 0, 0,0));

		PdfPCell validitytableCell = new PdfPCell(validitytable);
		validitytableCell.setBorder(0);
		
		lastPartTable.addCell(validitytableCell);
		lastPartTable.addCell(cellBlank);
		lastPartTable.addCell(cellBlank);

		lastPartTable.addCell(pdfUtility.getCell("Thanking You,",font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		lastPartTable.addCell(pdfUtility.getCell(comp.getBusinessUnitName(),font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		
		lastPartTable.addCell(cellBlank);
		lastPartTable.addCell(cellBlank);

		lastPartTable.addCell(pdfUtility.getCell(salespersonName,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		if(employee!=null && employee.getDesignation()!=null && !employee.getDesignation().equals("")){
			lastPartTable.addCell(pdfUtility.getCell(employee.getDesignation(),font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		}
		if(salesperonCellNumber!=null && !salesperonCellNumber.equals("")){
			String MobNo = "Mob No. "+salesperonCellNumber;
			lastPartTable.addCell(pdfUtility.getCell(MobNo,font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		}
		lastPartTable.addCell(pdfUtility.getCell(comp.getEmail(),font12, Element.ALIGN_LEFT, 0, 0, 0,0));

		try {
			document.add(lastPartTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private String getlicenseNo(Company comp) {

		if(comp!=null && comp.getArticleTypeDetails()!=null && comp.getArticleTypeDetails().size()>0){
			for(ArticleType articleType : comp.getArticleTypeDetails()){
				if(articleType.getDocumentName().equals(AppConstants.QUOTATION) && articleType.getArticleTypeName().equalsIgnoreCase("License No") &&
						 articleType.getArticleTypeValue()!=null && !articleType.getArticleTypeValue().equals("") && articleType.getArticlePrint().equalsIgnoreCase("Yes")){
					return articleType.getArticleTypeValue();
				}
			}
		}
		return "";
	}


	private void createProductDetails(String preprintStatus) {
		
		try {
			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEXTPAGE);
//			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable producttable = new PdfPTable(5);
		producttable.setWidthPercentage(100);
		try {
			producttable.setWidths(column8SerProdCollonWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		producttable.setHorizontalAlignment(Element.ALIGN_CENTER);
		producttable.setHeaderRows(1);
		
//		PdfPTable producttableheader = new PdfPTable(6);
//		producttableheader.setWidthPercentage(100);
//		try {
//			producttableheader.setWidths(column8SerProdCollonWidth);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		producttableheader.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase typeofTreatment = new Phrase("Type Of Treatment", font12bold);
		PdfPCell typeofTreatmentCell = new PdfPCell(typeofTreatment);
		typeofTreatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		typeofTreatmentCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		typeofTreatmentCell.setPaddingTop(5f);
//		typeofTreatmentCell.setPaddingBottom(5f);

		
		Phrase particularsOfPremises = new Phrase("Particulars Of Premises", font12bold);
		PdfPCell particularsOfPremisesCell = new PdfPCell(particularsOfPremises);
		particularsOfPremisesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		particularsOfPremisesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		particularsOfPremisesCell.setPaddingTop(5f);
//		particularsOfPremisesCell.setPaddingBottom(5f);
		
		Phrase periodOfContract = new Phrase("Period Of Contract", font12bold);
		PdfPCell periodOfContractCell = new PdfPCell(periodOfContract);
		periodOfContractCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		periodOfContractCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodOfContractCell.setPaddingTop(5f);
		periodOfContractCell.setPaddingBottom(7f);

		Phrase frequencyOfServices = new Phrase("No.Of Services", font12bold);
		PdfPCell frequencyOfServicesCell = new PdfPCell(frequencyOfServices);
		frequencyOfServicesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		frequencyOfServicesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);

		Phrase cost = new Phrase("Cost", font12bold);
		PdfPCell costCell = new PdfPCell(cost);
		costCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		costCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase phtotalAmount = new Phrase("Total Amount", font12bold);
		PdfPCell totalAmountCell = new PdfPCell(phtotalAmount);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalAmountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		
		producttable.addCell(typeofTreatmentCell);
		producttable.addCell(particularsOfPremisesCell);
		producttable.addCell(periodOfContractCell);
		producttable.addCell(frequencyOfServicesCell);
		producttable.addCell(costCell);
//		producttable.addCell(totalAmountCell);

		try {
			document.add(producttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
//		PdfPTable producttable = new PdfPTable(6);
//		producttable.setWidthPercentage(100);
//		try {
//			producttable.setWidths(column8SerProdCollonWidth);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//		producttable.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		producttable.setHeaderRows(2);

		for(int i=0;i<quotation.getItems().size();i++){
			
			
			Phrase productName = new Phrase(quotation.getItems().get(i).getProductName(), font12);
			PdfPCell productNameCell = new PdfPCell(productName);
			productNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(productNameCell);
			
			
			Phrase coverageArea = new Phrase(quotation.getItems().get(i).getPremisesDetails(), font12);
			PdfPCell coverageAreaCell = new PdfPCell(coverageArea);
			coverageAreaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(coverageAreaCell);
			
			
			Phrase phRemark = new Phrase(quotation.getItems().get(i).getRemark()+"", font12);
			PdfPCell phRemarkCell = new PdfPCell(phRemark);
			phRemarkCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(phRemarkCell);
			
			Phrase phserviceFrequency = new Phrase(quotation.getItems().get(i).getNumberOfServices()+"", font12);
			PdfPCell phserviceFrequencyCell = new PdfPCell(phserviceFrequency);
			phserviceFrequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			producttable.addCell(phserviceFrequencyCell);
			
			double totalAmount = getTotalAmount(quotation.getItems().get(i));
			double finalTotalAmount =  totalAmount;
			String strTotalAmount ="Rs. "+decimalformat.format(totalAmount); //+" /-";
			String CGST = "", SGST="",IGST="";
			double CGSTAmount=0, SGSTAmount=0,IGSTAmount=0;
			if(quotation.getItems().get(i).getVatTax().getPercentage()>0 && quotation.getItems().get(i).getServiceTax().getPercentage()>0){
				CGST = quotation.getItems().get(i).getVatTax().getTaxPrintName()+" "+quotation.getItems().get(i).getVatTax().getPercentage()+"%";
				SGST = quotation.getItems().get(i).getServiceTax().getTaxPrintName()+" "+quotation.getItems().get(i).getServiceTax().getPercentage()+"%";
				finalTotalAmount += (totalAmount*quotation.getItems().get(i).getVatTax().getPercentage())/100;
				finalTotalAmount += (totalAmount*quotation.getItems().get(i).getServiceTax().getPercentage())/100;
				CGSTAmount =  (totalAmount*quotation.getItems().get(i).getVatTax().getPercentage())/100;
				SGSTAmount = (totalAmount*quotation.getItems().get(i).getServiceTax().getPercentage())/100;
			}
			else if(quotation.getItems().get(i).getVatTax().getPercentage()>0 || quotation.getItems().get(i).getServiceTax().getPercentage()>0){
				if(quotation.getItems().get(i).getVatTax().getPercentage()>0 ){
					IGST = quotation.getItems().get(i).getVatTax().getTaxPrintName()+" "+quotation.getItems().get(i).getVatTax().getPercentage()+"%";
					finalTotalAmount += (totalAmount*quotation.getItems().get(i).getVatTax().getPercentage())/100;
					IGSTAmount = (totalAmount*quotation.getItems().get(i).getVatTax().getPercentage())/100;
				}
				else if(quotation.getItems().get(i).getServiceTax().getPercentage()>0){
					IGST = quotation.getItems().get(i).getServiceTax().getTaxPrintName()+" "+quotation.getItems().get(i).getServiceTax().getPercentage()+"%";
					finalTotalAmount += (totalAmount*quotation.getItems().get(i).getServiceTax().getPercentage())/100;
					IGSTAmount = (totalAmount*quotation.getItems().get(i).getServiceTax().getPercentage())/100;
				}
			}
			
			

			PdfPTable QutationIdDateTable = new PdfPTable(2);
			QutationIdDateTable.setWidthPercentage(100);
			QutationIdDateTable.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			QutationIdDateTable.addCell(pdfUtility.getCell(strTotalAmount, font12, Element.ALIGN_RIGHT, 0, 2, 0,0));
			if(CGSTAmount!=0 && SGSTAmount!=0){
				QutationIdDateTable.addCell(pdfUtility.getCell(CGST, font12, Element.ALIGN_LEFT, 0, 0, 0,0));
				QutationIdDateTable.addCell(pdfUtility.getCell(CGSTAmount+"", font12, Element.ALIGN_RIGHT, 0, 0, 0,0));
//				QutationIdDateTable.addCell(pdfUtility.getCell(SGST, font12, Element.ALIGN_LEFT, 0, 0, 0,0));
//				QutationIdDateTable.addCell(pdfUtility.getCell(SGSTAmount+"", font12, Element.ALIGN_RIGHT, 0, 0, 0,0));
				QutationIdDateTable.addCell(pdfUtility.getPdfCell(SGST+"", font12, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
				QutationIdDateTable.addCell(pdfUtility.getPdfCell(SGSTAmount+"", font12, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0));


			}
			else if(IGSTAmount!=0){
//				QutationIdDateTable.addCell(pdfUtility.getCell(IGST, font12, Element.ALIGN_LEFT, 0, 0, 0,0));
//				QutationIdDateTable.addCell(pdfUtility.getCell(IGSTAmount+"", font12, Element.ALIGN_RIGHT, 0, 0, 0,0));
				
				QutationIdDateTable.addCell(pdfUtility.getPdfCell(IGST+"", font12, Element.ALIGN_LEFT, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
				QutationIdDateTable.addCell(pdfUtility.getPdfCell(IGSTAmount+"", font12, Element.ALIGN_RIGHT, Element.ALIGN_MIDDLE, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0));

			}
			
//			QutationIdDateTable.addCell(pdfUtility.getCell("__________________", font12, Element.ALIGN_RIGHT, 0, 2, 0,0));

			QutationIdDateTable.addCell(pdfUtility.getCell("Total Rs. "+finalTotalAmount+"/-"+"", font12, Element.ALIGN_RIGHT, 0, 2, 0,0));

			PdfPCell totalamountCell = new PdfPCell(QutationIdDateTable);
			totalamountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			producttable.addCell(totalamountCell);
			
		}
		
		
		try {
			document.add(producttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}


	public double getTotalAmount(SalesLineItem object) {
		
		double total = 0;
		SuperProduct product = object.getPrduct();
		double tax = removeAllTaxes(product);
		double origPrice = object.getPrice() - tax;
		
		if (object.isServiceRate()&&(object.getArea()==null||object.getArea().equals("")||object.getArea().equalsIgnoreCase("NA"))) {
			origPrice=origPrice*object.getNumberOfServices();
		}

		if ((object.getPercentageDiscount() == null && object.getPercentageDiscount() == 0)&& (object.getDiscountAmt() == 0)) {

			System.out.println("inside both 0 condition");
			// total=origPrice;

			// new code
			/********************
			 * Square Area Calculation code added in if condition and
			 * without square area calculation code in else block
			 ***************************/
			System.out.println("Get value from area =="+ object.getArea());
			System.out.println("total amount before area calculation =="+ total);
			if (!object.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(object.getArea());
				total = origPrice * area;
				System.out.println(" Final TOTAL if no discount per & no discount Amt ==="+ total);
			} else {
				total = origPrice;
			}
		}
		else if ((object.getPercentageDiscount() != null)&& (object.getDiscountAmt() != 0)) {

			System.out.println("inside both not null condition");

			// total=origPrice-(origPrice*object.getPercentageDiscount()/100);
			// total=total-object.getDiscountAmt();
			// total=total*object.getQty();

			if (!object.getArea().equalsIgnoreCase("NA")) {
				double area = Double.parseDouble(object.getArea());
				total = origPrice * area;
//				System.out.println("total before discount per ===="+ total);
				total = total- (total * object.getPercentageDiscount() / 100);
//				System.out.println("after discount per total === "+ total);
				total = total - object.getDiscountAmt();
//				System.out.println("after discount AMT total === "+ total);
//				System.out.println(" Final TOTAL   discount per &  discount Amt ==="+ total);
			} else {
//				System.out.println(" normal === total " + total);
				total = origPrice;
				total = total- (total * object.getPercentageDiscount() / 100);
				total = total - object.getDiscountAmt();
			}

		} else {
			System.out.println("inside oneof the null condition");

			if (object.getPercentageDiscount() != null) {
				// System.out.println("inside getPercentageDiscount oneof the null condition");
				// total=origPrice-(origPrice*object.getPercentageDiscount()/100);

				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
//					System.out.println("total before discount per ===="+ total);
					total = total- (total * object.getPercentageDiscount() / 100);
//					System.out.println("after discount per total === "+ total);
				} else {
//					System.out.println("old code");
					total = origPrice;
					total = total- (total * object.getPercentageDiscount() / 100);
				}
			} else {
				// System.out.println("inside getDiscountAmt oneof the null condition");
				// total=origPrice-object.getDiscountAmt();

				if (!object.getArea().equalsIgnoreCase("NA")) {
					double area = Double.parseDouble(object.getArea());
					total = origPrice * area;
//					System.out.println("total before discount amt ===="+ total);
					total = total - object.getDiscountAmt();
//					System.out.println("after discount amt total === "+ total);

				} else {
					total = total - object.getDiscountAmt();
				}
			}

			// total=total*object.getQty();

		}

		return total;
	}


	private double removeAllTaxes(SuperProduct entity) {


		double vat = 0, service = 0;
		double tax = 0, retrVat = 0, retrServ = 0;
		if (entity instanceof ServiceProduct) {
			ServiceProduct prod = (ServiceProduct) entity;
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
		}

		if (entity instanceof ItemProduct) {
			ItemProduct prod = (ItemProduct) entity;
			if (prod.getVatTax() != null
					&& prod.getVatTax().isInclusive() == true) {
				vat = prod.getVatTax().getPercentage();
			}
			if (prod.getServiceTax() != null
					&& prod.getServiceTax().isInclusive() == true) {
				service = prod.getServiceTax().getPercentage();
			}
		}

		if (vat != 0 && service == 0) {
			retrVat = entity.getPrice() / (1 + (vat / 100));
			retrVat = entity.getPrice() - retrVat;
		}
		if (service != 0 && vat == 0) {
			retrServ = entity.getPrice() / (1 + service / 100);
			retrServ = entity.getPrice() - retrServ;
		}
		if (service != 0 && vat != 0) {
			/**
			 * Date 13-06-2018
			 * By Vijay 
			 * Des :- above old code commented and new code added below
			 */
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			/**
			 * ends here
			 */
		}
		tax = retrVat + retrServ;
		return tax;
	
		
	}


	private void createFixedData() {

		PdfPTable subjecttable = new PdfPTable(1);
		subjecttable.setWidthPercentage(100);
		subjecttable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		subjecttable.addCell(cellBlank);
		subjecttable.addCell(pdfUtility.getCell("Dear Sir,", font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		subjecttable.addCell(cellBlank);

		String salesPersonName = "";
		if(quotation.getEmployee()!=null && !quotation.getEmployee().equals("")){
			salesPersonName = quotation.getEmployee();
		}
		String empDesignation = "";
		if(employee!=null && employee.getDesignation()!=null){
			empDesignation = employee.getDesignation();
		}
		
		String firstParagraph = "We thank you for the courtesy shown to our "+empDesignation+" Mr. "+salesPersonName +" inspecting your entire premises. We give below the details of the treatment which need to be carried out.";
		subjecttable.addCell(pdfUtility.getCell(firstParagraph, font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		subjecttable.addCell(cellBlank);

		String title = "About Ultra Pest Control:";
		subjecttable.addCell(pdfUtility.getCell(title, font10bold, Element.ALIGN_LEFT, 0, 0, 0,0));
		subjecttable.addCell(cellBlank);
		
		String secondParagraph = "Founded in 1997, Ultra Pest Control has more than 25 years of experience in pest control services and are leaders in the industry, specializing in protection against common pests, including cockroaches, termites, rodents, and insects. Our aim is to give simple, honest and more personalised services and solutions to our clients. Armed with a fleet of over 100 expertly-trained pest professionals, we attribute our success to the longstanding tradition of excellence in providing quality pest control solutions for Societies & Compound Organisations.";
		subjecttable.addCell(pdfUtility.getCell(secondParagraph, font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		subjecttable.addCell(cellBlank);

		try {
			document.add(subjecttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void createCustomerInfo() {
		
		
		PdfPTable QutationIdDateTable = new PdfPTable(5);
		QutationIdDateTable.setWidthPercentage(100);
		QutationIdDateTable.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		float[] QutationIdDateTableColumnWidth = { 0.5f, 1f, 2f, 0.5f,1f };
		try {
			QutationIdDateTable.setWidths(QutationIdDateTableColumnWidth);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		QutationIdDateTable.addCell(pdfUtility.getCell("Qtn. No.", font12bold, Element.ALIGN_LEFT, 0, 0, 0,0));
		QutationIdDateTable.addCell(pdfUtility.getCell(quotation.getCount()+"", font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		QutationIdDateTable.addCell(pdfUtility.getCell(" ", font12bold, Element.ALIGN_CENTER, 0, 0, 0,0));
		QutationIdDateTable.addCell(pdfUtility.getCell("Date : ", font12bold, Element.ALIGN_CENTER, 0, 0, 0,0));
		QutationIdDateTable.addCell(pdfUtility.getCell(fmt.format(quotation.getQuotationDate()), font12, Element.ALIGN_LEFT, 0, 0, 0,0));

		
		try {
			document.add(QutationIdDateTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable customerInfotable = new PdfPTable(1);
		customerInfotable.setWidthPercentage(100);
		customerInfotable.setHorizontalAlignment(Element.ALIGN_CENTER);

		String customerName="";
		if(cust.isCompany()){
			customerName = cust.getCompanyName();
		}
		else{
			customerName = cust.getFullname();
		}
		
		customerInfotable.addCell(cellBlank);
		customerInfotable.addCell(pdfUtility.getCell("To, ", font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		customerInfotable.addCell(cellBlank);
		customerInfotable.addCell(pdfUtility.getCell(customerName, font12, Element.ALIGN_LEFT, 0, 0, 0,0));

		
		try {
			document.add(customerInfotable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		float[] columnWidths = { 1f, 2f };
		
		PdfPTable customerInfotable2 = new PdfPTable(2);
		customerInfotable2.setWidthPercentage(100);
		try {
			customerInfotable2.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		customerInfotable2.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerInfotable2.addCell(pdfUtility.getCell(cust.getSecondaryAdress().getCompleteAddress(), font12, Element.ALIGN_LEFT, 0, 0, 0,0));
		customerInfotable2.addCell(cellBlank);
		
		try {
			document.add(customerInfotable2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}


	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();
		if(branchAsCompanyFlag && branchEntity!=null && branchEntity.getUploadHeader()!=null){
			document = branchEntity.getUploadHeader();
		}
		
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		if(branchAsCompanyFlag && branchEntity!=null && branchEntity.getUploadFooter()!=null){
			document = branchEntity.getUploadFooter();
		}
		
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
