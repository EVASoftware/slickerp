package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class MaterialIssueSheetPdf {
	Service service;
	Company company;
	Long serialNo;
	
	public Document document;
	private Font font16boldul, font12bold, font8bold,font9bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,fontG8,
	font10Wbold,font12Wbold,font9Wbold;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	BaseColor darkGreyColor = WebColors.getRGBColor("#696969");
	BaseColor lightGreyColor = WebColors.getRGBColor("#D3D3D3");
	
	float[] column4Widths = {1.0f,2.0f,2.0f,5.0f};
	float[] column6Widths = {1.5f,4.0f,1.5f,1.0f,1.0f,1.0f};
	float[] column8Widths = {1.5f,3.5f,1.5f,3.5f};
	
	public MaterialIssueSheetPdf(){
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9,Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		fontG8 = new Font(Font.FontFamily.HELVETICA, 8,Font.NORMAL,BaseColor.LIGHT_GRAY);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10Wbold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD,BaseColor.WHITE);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		font9Wbold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.WHITE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setMIS(Long id){
		Service service = ofy().load().type(Service.class).id(id).now();
		this.service=service;
		
		if(service!=null){
			Company company=ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
			this.company=company;
		}
		
		if(service!=null){
			NumberGeneration numberGen=ofy().load().type(NumberGeneration.class).filter("companyId", service.getCompanyId()).filter("processName", "MIS").filter("status", true).first().now();
			if(numberGen!=null){
				serialNo=numberGen.getNumber();
				numberGen.setNumber(serialNo+1);
				ofy().save().entity(numberGen).now();
			}
		}
		
		
	}
	
	public void createPdf(){
		if (company.getUploadHeader() != null) {
			createCompanyNameAsHeader(document, company);
		}
		if (company.getUploadFooter() != null) {
			createCompanyNameAsFooter(document, company);
		}
		createBlankforUPC();
		createMisHeadingTable();
		createMisDetailTable();
		createSignatureTable();
	}
	
	
	private void createSignatureTable() {
		PdfPTable table1=new PdfPTable(4);
		table1.setWidthPercentage(100);
		table1.setSpacingBefore(25f);
		table1.setSpacingAfter(20f);
		try {
			table1.setWidths(column8Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase ph1=new Phrase("Issued By ",font9bold);
		Phrase ph2=new Phrase("  Signature ",font9bold);
		Phrase ph3=new Phrase("Received By ",font9bold);
		Phrase ph4=new Phrase("Returned By ",font9bold);
		Phrase blank=new Phrase(" ");
		
		//Issued By
		PdfPCell cell1=new PdfPCell(ph1);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell1.setBorder(0);
		table1.addCell(cell1);
		
		//Blank
		PdfPCell cell2=new PdfPCell(blank);
		cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell2.setBorderWidthLeft(0);
		cell2.setBorderWidthRight(0);
		cell2.setBorderWidthTop(0);
		table1.addCell(cell2);
		
		//Signature
		PdfPCell cell3=new PdfPCell(ph2);
		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell3.setBorder(0);
		table1.addCell(cell3);
		
		//Blank
		PdfPCell cell4=new PdfPCell(blank);
		cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4.setBorderWidthLeft(0);
		cell4.setBorderWidthRight(0);
		cell4.setBorderWidthTop(0);
		table1.addCell(cell4);
		
		
		//Received By
		PdfPCell cell9=new PdfPCell(ph3);
		cell9.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell9.setBorder(0);
		table1.addCell(cell9);
		
		//Blank
		PdfPCell cell10=new PdfPCell(blank);
		cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell10.setBorderWidthLeft(0);
		cell10.setBorderWidthRight(0);
		cell10.setBorderWidthTop(0);
		table1.addCell(cell10);
		
		//Signature
		PdfPCell cell15=new PdfPCell(ph2);
		cell15.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell15.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell15.setBorder(0);
		table1.addCell(cell15);
		
		//Blank
		PdfPCell cell16=new PdfPCell(blank);
		cell16.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell16.setBorderWidthLeft(0);
		cell16.setBorderWidthRight(0);
		cell16.setBorderWidthTop(0);
		table1.addCell(cell16);
		
		PdfPCell blankcell=new PdfPCell(blank);
		blankcell.setBorder(0);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		//Returned By
		PdfPCell cell5=new PdfPCell(ph4);
		cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell5.setBorder(0);
		table1.addCell(cell5);
		
		//Blank
		PdfPCell cell6=new PdfPCell(blank);
		cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell6.setBorderWidthLeft(0);
		cell6.setBorderWidthRight(0);
		cell6.setBorderWidthTop(0);
		table1.addCell(cell6);
		
		//Signature
		PdfPCell cell7=new PdfPCell(ph2);
		cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell7.setBorder(0);
		table1.addCell(cell7);
		
		//Blank
		PdfPCell cell8=new PdfPCell(blank);
		cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell8.setBorderWidthLeft(0);
		cell8.setBorderWidthRight(0);
		cell8.setBorderWidthTop(0);
		table1.addCell(cell8);
		
		//Received By
		PdfPCell cell11=new PdfPCell(ph3);
		cell11.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell11.setBorder(0);
		table1.addCell(cell11);
		
		//Blank
		PdfPCell cell12=new PdfPCell(blank);
		cell12.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell12.setBorderWidthLeft(0);
		cell12.setBorderWidthRight(0);
		cell12.setBorderWidthTop(0);
		table1.addCell(cell12);
		
		//Signature
		PdfPCell cell13=new PdfPCell(ph2);
		cell13.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell13.setBorder(0);
		table1.addCell(cell13);
		
		//Blank
		PdfPCell cell14=new PdfPCell(blank);
		cell14.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell14.setBorderWidthLeft(0);
		cell14.setBorderWidthRight(0);
		cell14.setBorderWidthTop(0);
		table1.addCell(cell14);
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	private void createMisDetailTable() {
		PdfPTable table1=new PdfPTable(6);
		table1.setWidthPercentage(100);
		table1.setSpacingBefore(25f);
		try {
			table1.setWidths(column6Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase ph1=new Phrase("Service Id",font10bold);
		Phrase ph2=new Phrase("Material",font10bold);
		Phrase ph3=new Phrase("Units",font10bold);
		
		Phrase ph4=new Phrase("Quantity",font10bold);
		Phrase ph4_1=new Phrase("Issued",font9Wbold);
		Phrase ph4_2=new Phrase("Used",font9Wbold);
		Phrase ph4_3=new Phrase("Returned",font9Wbold);
		
		
		
		PdfPCell cell1=new PdfPCell(ph1);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell1.setRowspan(2);
		cell1.setBackgroundColor(lightGreyColor);
		table1.addCell(cell1);
		
		PdfPCell cell2=new PdfPCell(ph2);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell2.setRowspan(2);
		cell2.setBackgroundColor(lightGreyColor);
		table1.addCell(cell2);
		
		PdfPCell cell3=new PdfPCell(ph3);
		cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell3.setRowspan(2);
		cell3.setBackgroundColor(lightGreyColor);
		table1.addCell(cell3);
		
		PdfPCell cell4=new PdfPCell(ph4);
		cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4.setColspan(3);
		cell4.setBackgroundColor(lightGreyColor);
		table1.addCell(cell4);
		
		PdfPCell cell4_1=new PdfPCell(ph4_1);
		cell4_1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell4_1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4_1.setBackgroundColor(darkGreyColor);
		table1.addCell(cell4_1);
		
		PdfPCell cell4_2=new PdfPCell(ph4_2);
		cell4_2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell4_2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4_2.setBackgroundColor(darkGreyColor);
		table1.addCell(cell4_2);
		
		PdfPCell cell4_3=new PdfPCell(ph4_3);
		cell4_3.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell4_3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4_3.setBackgroundColor(darkGreyColor);
		table1.addCell(cell4_3);
		
		Phrase blank=new Phrase(" ");
		PdfPCell blankcell=new PdfPCell();
		blankcell.addElement(blank);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		table1.addCell(blankcell);
		
		table1.addCell(blankcell);
		table1.addCell(blankcell);
//		table1.addCell(blankcell);
//		table1.addCell(blankcell);
//		table1.addCell(blankcell);
//		table1.addCell(blankcell);
		
		
		
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createMisHeadingTable() {
		PdfPTable topHeadingTable=new PdfPTable(1);
		topHeadingTable.setWidthPercentage(100);
		
		Phrase headingPhrase=new Phrase("Material Issue Sheet",font12Wbold);
		PdfPCell headingCell=new PdfPCell(headingPhrase);
		headingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		headingCell.setBackgroundColor(darkGreyColor);
		headingCell.setFixedHeight(25f);
		
		topHeadingTable.addCell(headingCell);
		
		try {
			document.add(topHeadingTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable table1=new PdfPTable(4);
		table1.setWidthPercentage(100);
		table1.setSpacingBefore(15f);
		try {
			table1.setWidths(column4Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase ph1=new Phrase("Serial No. ",font9);
		Phrase ph2=new Phrase("Date ",font9);
		
		Phrase ph3=null;
		if(serialNo!=null){
			ph3=new Phrase(" "+serialNo,font9);
		}else{
			ph3=new Phrase(" ");
		}
		Phrase ph4=new Phrase(" "+fmt.format(new Date()),font9);
		
		Phrase ph5=new Phrase("    Branch ",font9);
		Phrase ph6=new Phrase(" "+service.getBranch(),font9);
		Phrase ph7=new Phrase("    Name Of Associates ",font9);
		Phrase ph8=new Phrase(" ");
		
		
		// 1st row
		
		PdfPCell cell1=new PdfPCell(ph1);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell1.setBorder(0);
		table1.addCell(cell1);
		
		PdfPCell cell3=new PdfPCell(ph3);
		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell3.setBorderWidthLeft(0);
		cell3.setBorderWidthRight(0);
		cell3.setBorderWidthTop(0);
		table1.addCell(cell3);
		
		PdfPCell cell5=new PdfPCell(ph5);
		cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell5.setBorder(0);
		table1.addCell(cell5);
		
		PdfPCell cell6=new PdfPCell(ph6);
		cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell6.setBorderWidthLeft(0);
		cell6.setBorderWidthRight(0);
		cell6.setBorderWidthTop(0);
		table1.addCell(cell6);
		
		
		// 2nd row
		
		PdfPCell cell2=new PdfPCell(ph2);
		cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell2.setBorder(0);
		table1.addCell(cell2);
		
		PdfPCell cell4=new PdfPCell(ph4);
		cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell4.setBorderWidthLeft(0);
		cell4.setBorderWidthRight(0);
		cell4.setBorderWidthTop(0);
		table1.addCell(cell4);
		
		PdfPCell cell7=new PdfPCell(ph7);
		cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell7.setBorder(0);
		table1.addCell(cell7);
		
		PdfPCell cell8=new PdfPCell(ph8);
		cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell8.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell8.setBorderWidthLeft(0);
		cell8.setBorderWidthRight(0);
		cell8.setBorderWidthTop(0);
		table1.addCell(cell8);
		
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
	}

	private void createBlankforUPC() {
		Phrase blankphrase = new Phrase("", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
