package com.slicktechnologies.server.addhocprinting.nbhc;
import java.text.DateFormat;
import com.slicktechnologies.shared.common.personlayer.Employee;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Logger;

import com.google.apphosting.api.ApiProxy.LogRecord.Level;
import com.google.gwt.layout.client.Layout.Alignment;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.addhocprinting.ServiceGSTInvoice;
import com.slicktechnologies.server.addhocprinting.sasha.EmploymentCardPdf;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.MaterialProduct;
import com.slicktechnologies.shared.common.inventory.ProductInventoryViewDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ServiceSchedulePdf {

	Logger logger = Logger.getLogger("ServiceSchedulePdf");
	public Document document;
	HashSet<Integer> invoiceIdSet;

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12Wbold, font12boldul, font10boldul, font12, font16bold,
			font10, font10bold, font10ul, font9boldul, font14bold, font9,
			font7, font7bold, font9red, font9boldred, font12boldred, font23;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a zzz");
	SimpleDateFormat sdf;
	List<Service> serviceList;
	int tecnicianId;
	String technicianTeam;
	Date fromDATE;
	Date toDATE;
	int servicesrn;
	Company comp;
	Invoice invoiceentity;
	EmploymentCardPdf employmentCardPdf = new EmploymentCardPdf();
	List<ServiceProject> serviceLoadList;
	Map<Integer , ServiceProject> serviceProjectMap = new HashMap<Integer , ServiceProject>();
	TreeMap<String , ArrayList<Service>> dateTechnicianMap = new TreeMap<String , ArrayList<Service>>();
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	Map<String , ProductGroupList> technicianTotalMaterialMap = new TreeMap<String ,ProductGroupList>();
	Map<String ,Double> availableQuntityMap = new HashMap<String , Double>();
	public ServiceSchedulePdf() {
		super();

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL
				| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,
				BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,
				BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	
		dateTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	}

	public void loadData(String team, Integer technicianid, Date fromDate,
			Date toDate, long companyId,String status) {
		// TODO Auto-generated method stub
		serviceList=new ArrayList<Service>();

		tecnicianId = technicianid;
		technicianTeam = team;
		fromDATE = fromDate;
		toDATE = toDate;
		System.out.println("team" + team);
		logger.log(java.util.logging.Level.SEVERE , "technician id" + technicianid);

		List<Service> tempserviceList=null;
		 if (technicianid != 0) {
			Employee emp =  ofy().load().type(Employee.class)
					.filter("companyId", companyId)
					.filter("count", tecnicianId).first().now();
					
				if(emp != null){	
					tempserviceList= ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("serviceDate >=", fromDate)
					.filter("serviceDate <=", toDate)
					.filter("employee", emp.getFullName()).list();
				}else{
					tempserviceList= ofy().load().type(Service.class)
							.filter("companyId", companyId)
							.filter("serviceDate >=", fromDate)
							.filter("serviceDate <=", toDate)
							.filter("technicians.empCount", technicianid).list();
				}
				
			
			logger.log(java.util.logging.Level.SEVERE , "serviceprolist size ==" + tempserviceList.size());
			System.out.println("companyid inside if" + companyId);
			System.out.println("frmdate inside if" + fromDate);
			System.out.println("todate inside if" + toDate);
			System.out.println("technician id if" + technicianid);
		}else{
			tempserviceList= ofy().load().type(Service.class)
					.filter("companyId", companyId)
					.filter("serviceDate >=", fromDate)
					.filter("serviceDate <=", toDate).list();
			System.out
					.println("serviceprolist size ==" + tempserviceList.size());
			System.out.println("companyid inside if" + companyId);
			System.out.println("frmdate inside if" + fromDate);
			System.out.println("todate inside if" + toDate);
			System.out.println("technician id if" + technicianid);
		}
		 if(tempserviceList!=null)
				logger.log(java.util.logging.Level.SEVERE , "tempserviceList size="+tempserviceList.size());
			
		 if(status!=null&&!status.equals("")&&tempserviceList!=null) {
			 logger.log(java.util.logging.Level.SEVERE , "Service status filter="+status);
				for(Service s:tempserviceList) {
					if(s.getStatus().equals(status))
						serviceList.add(s);
				}
		}else
				serviceList=tempserviceList;
		 
		comp = ofy().load().type(Company.class).filter("companyId", companyId)
				.first().now();

		

	}

	public void createpdf() {
		invoiceIdSet = new HashSet<Integer>();
		createHeaderTable();
	//	createTitleTable();
		createSortedservices();
		createSummaryDetails();

	}


	private void createHeaderTable() {

		PdfPTable headerTab = new PdfPTable(1);
		headerTab.setWidthPercentage(100);
		headerTab.setSpacingAfter(10);

		Phrase comName = new Phrase(comp.getBusinessUnitName(), font16bold);
		PdfPCell ComNameCell = new PdfPCell(comName);
		ComNameCell.setBorder(0);
		ComNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTab.addCell(ComNameCell);

		try {
			document.add(headerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}

	private void createTitleTable() {
		PdfPTable titleTab = new PdfPTable(9);
		titleTab.setWidthPercentage(100);
		//titleTab.setSpacingBefore(5);
		try {
			titleTab.setWidths(new float[] { 10 , 10 ,10 ,10,10,10,10,10 ,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		titleTab.addCell(employmentCardPdf.getCell("Time", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Service Id", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Product Name", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Material", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Planned Qty", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Required Qty", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Consumed Qty", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Status", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		titleTab.addCell(employmentCardPdf.getCell("Remark", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		try {
			document.add(titleTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	private void createSortedservices() 
	{
		if(serviceList!=null)
			logger.log(java.util.logging.Level.SEVERE , "serviceList size="+serviceList.size());
		HashSet<Integer> serviceId=new HashSet<Integer>();
		for (int i = 0; i < serviceList.size(); i++) 
		{
			serviceId.add(serviceList.get(i).getCount());
		}
		System.out.println("size of serviceId: "+serviceId.size());
		if(serviceId.size() > 0) {
			List<ServiceProject> serviceProjectList=ofy().load().type(ServiceProject.class)
					.filter("companyId",comp.getCompanyId())
					.filter("serviceId IN", serviceId).list();
			
			System.out.println("inside creat sorted"+serviceProjectList.size());
			
			for(ServiceProject serProj : serviceProjectList ){
				serviceProjectMap.put(serProj.getserviceId(), serProj);
			}
		
			List<Service> sortedServiceList =new ArrayList<Service>();
			sortedServiceList.addAll(getSortedList(serviceList));
			
			ArrayList<Service> list = new ArrayList<Service>();
			String key = "";
			for(Service service : sortedServiceList){
				
				ServiceProject proj = serviceProjectMap.get(service.getCount());
				if(proj != null){
					if(proj.getProdDetailsList() != null && proj.getProdDetailsList().size() >0){
						for(ProductGroupList group : proj.getProdDetailsList()){
				
						}
						
					}
					if(proj.getTechnicians().size() > 0){
				//	
					EmployeeInfo info = proj.getTechnicians().get(0);
						try {
							key =new java.sql.Date(dateFormat.parse(dateFormat.format(service.getServiceDate())).getTime())+"/"+info.getFullName();
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(dateTechnicianMap.containsKey(key)){
							list = new ArrayList<Service>();
							list = dateTechnicianMap.get(key);
							list.add(service);
							dateTechnicianMap.put(key, list);
						}else{
							list = new ArrayList<Service>();
							list.add(service);
							dateTechnicianMap.put(key, list);
						}
				//	}
					}else{
						try {
							key = new java.sql.Date(dateFormat.parse(dateFormat.format(service.getServiceDate())).getTime())+"/"+service.getEmployee();
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if(dateTechnicianMap.containsKey(key)){
							list = new ArrayList<Service>();
							list = dateTechnicianMap.get(key);
							list.add(service);
							dateTechnicianMap.put(key, list);
						}else{
							list = new ArrayList<Service>();
							list.add(service);
							dateTechnicianMap.put(key, list);
						}
					}
				}
			}
	
			int initialposition = 0;
			createServiceDetailTable(initialposition,sortedServiceList);
		} 
	}		
		
	
	private List<Service> getSortedList(List<Service> sortedServiceList)
	{
		DateFormat fmt1 = new SimpleDateFormat("dd/MM/yyyy");
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		for(int i=0;i<sortedServiceList.size();i++){
			
			if(sortedServiceList.get(i).getServiceTime().equalsIgnoreCase("Flexible")){
				
				Date serviceDate=DateUtility.getDateWithTimeZone("IST", sortedServiceList.get(i).getServiceDate());
				Calendar cal=Calendar.getInstance();
				cal.setTime(serviceDate);
				cal.add(Calendar.DATE, 0);
				
				Date servicedate=null;
				
				try {
					servicedate=fmt1.parse(fmt1.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,23);
					cal.set(Calendar.MINUTE,00);
					cal.set(Calendar.SECOND,00);
					servicedate=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				sortedServiceList.get(i).setServiceDate(servicedate);
			}
			else{
						
				String serviceTime[] = sortedServiceList.get(i).getServiceTime().split(":"); 
				int hh=0;
				int mm =0;
				int ss=0;
				if(sortedServiceList.get(i).getServiceTime().contains("AM")){
					if(!serviceTime[0].contains("--")){
						  hh = Integer.parseInt(serviceTime[0].trim());
					}
					if(!serviceTime[1].contains("--")){
						if(serviceTime[1].contains("AM")){
							String strAM[] = serviceTime[1].split("AM");
							mm = Integer.parseInt(strAM[0].trim());
						}
					}
					
				}
				
				if(sortedServiceList.get(i).getServiceTime().contains("PM")){
					 
					if(!serviceTime[0].contains("12")){
						hh = 12;	
					}
					if(!serviceTime[0].contains("--")){
						  hh += Integer.parseInt(serviceTime[0].trim());
					}		
					if(!serviceTime[1].contains("--")){
						if(serviceTime[1].contains("PM")){
							String strPM[] = serviceTime[1].split("PM");
							mm = Integer.parseInt(strPM[0].trim());
						}
					}
					
				}
				
				Date serviceDate=DateUtility.getDateWithTimeZone("IST", sortedServiceList.get(i).getServiceDate());
				Calendar cal=Calendar.getInstance();
				cal.setTime(serviceDate);
				cal.add(Calendar.DATE, 0);
				
				Date servicedate=null;
				
				try {
					servicedate=fmt1.parse(fmt1.format(cal.getTime()));
					cal.set(Calendar.HOUR_OF_DAY,hh);
					cal.set(Calendar.MINUTE,mm);
					cal.set(Calendar.SECOND,ss);
					servicedate=cal.getTime();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				sortedServiceList.get(i).setServiceDate(servicedate);
			}
		}
	
		
		Comparator<Service> compServicelist = new Comparator<Service>() {
			
			@Override
			public int compare(Service s1, Service s2) {
				
				Date serviceDate = s1.getServiceDate();
				Date serviceDate2 = s2.getServiceDate();
				
				return serviceDate.compareTo(serviceDate2);
			}
		};
		Collections.sort(sortedServiceList,compServicelist);
		
		return sortedServiceList;	
	}
	
	private void createServiceDetailTable(int position,
			List<Service> sortedServiceList) {
		int noOfLine = 0;
		int lineNo = 0;
		int linecount = 0;
		System.out.println("servicelist size===outfor"
				+ serviceList.size());

		List<Service> serviceList = new ArrayList<Service>();

		Contract contract=null; 
		Boolean serviceMoreThanPage = false;
		System.out.println("intial position value==" + position);
		
		for(Map.Entry<String, ArrayList<Service>> entry : dateTechnicianMap.entrySet()){
			PdfPTable headerTable = new PdfPTable(1);
			headerTable.setWidthPercentage(100);
			headerTable.setSpacingBefore(10);
			headerTable.setSpacingAfter(10);
			logger.log(java.util.logging.Level.SEVERE , "Technician key :" + entry.getKey() +" "+ "Technician service : "+entry.getValue());
			serviceList = getSortedList(entry.getValue());
			String technician = "Service Engineer Name : ";
			String serviceDate = "Service Date : " ;
			String[] arr = entry.getKey().split("/");
			try{
			if(arr[1] != null){
				technician += arr[1];				
			}
			}catch(Exception e){
				logger.log(java.util.logging.Level.SEVERE   , "INSIDE CATCH");
				technician += serviceList.get(0).getEmployee();
			}
			if(serviceList.get(0).getServiceDate() != null){
				serviceDate += fmt2.format(serviceList.get(0).getServiceDate());
			}
			PdfPCell technicianAndDateCell = employmentCardPdf.getCell(serviceDate+"  "+technician, font10bold, Element.ALIGN_LEFT, 0, 0 , 0);
			technicianAndDateCell.setBorder(0);			
			headerTable.addCell(technicianAndDateCell);
			
			try {
				document.add(headerTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPTable serviceValueTable = new PdfPTable(9);
			serviceValueTable.setWidthPercentage(100);
			//serviceValueTable.setSpacingBefore(5);
			try {
				serviceValueTable.setWidths(new float[] { 10 , 10 ,10 ,10,10,10,10,10 ,20});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			createTitleTable();
			int listSize = serviceList.size();
			Map<Integer , MaterialMovementNote> mmnMap = new HashMap<Integer, MaterialMovementNote>();
			for(Service service : serviceList){
				if((service.isServiceScheduled()) && (service.getMmn() != null )){
					mmnMap.put(service.getMmn().getCount() , service.getMmn());
				}	
				
			}
			for(Map.Entry<Integer , MaterialMovementNote> val : mmnMap.entrySet()){
			//	if(service.getMmn() != null && (service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) || service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE))){
					MaterialMovementNote mmn = val.getValue();
					logger.log(java.util.logging.Level.SEVERE , " MMN :" + mmn + " "+val.getKey());
					for(MaterialProduct mp :mmn.getSubProductTableMmn()){
					 String key = mp.getMaterialProductName()+"-"+mmn.getTransToStorBin();
					 double qty = 0;
					 logger.log(java.util.logging.Level.SEVERE , " KEY :" + key);
					if(availableQuntityMap.containsKey(key)){
						qty = availableQuntityMap.get(key);
						qty += mp.getMaterialProductRequiredQuantity();
						availableQuntityMap.put(key, qty);
					}else{
						qty = mp.getMaterialProductRequiredQuantity();							
						availableQuntityMap.put(key, qty);
					}	
				  }	
			//	}
			}
			for(Service service : serviceList){
				int rowSpan = 0;
				
				if(service != null){
//					if(service.getMmn() != null && (service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) || service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE))){
//						MaterialMovementNote mmn = service.getMmn();
//						logger.log(java.util.logging.Level.SEVERE , " MMN :" + mmn);
//						for(MaterialProduct mp :mmn.getSubProductTableMmn()){
//						 String key = mp.getMaterialProductName()+"-"+mmn.getTransToStorBin();
//						 double qty = 0;
//						 logger.log(java.util.logging.Level.SEVERE , " KEY :" + key);
//						if(availableQuntityMap.containsKey(key)){
//							qty = availableQuntityMap.get(key);
//							qty += mp.getMaterialProductRequiredQuantity();
//							availableQuntityMap.put(key, qty);
//						}else{
//							qty = mp.getMaterialProductRequiredQuantity();							
//							availableQuntityMap.put(key, qty);
//						}	
//					  }	
//					}
					ServiceProject serProject = serviceProjectMap.get(service.getCount());
					if(serProject != null && serProject.getProdDetailsList() != null){
						rowSpan = serProject.getProdDetailsList().size();
					}
					if(service.getServiceTime() != null && !service.getServiceTime().equalsIgnoreCase("")){
						serviceValueTable.addCell(employmentCardPdf.getCell(service.getServiceTime(), font8, Element.ALIGN_CENTER, rowSpan, 0, 0));
					}else{
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER,rowSpan, 0, 0));
					}
						
					serviceValueTable.addCell(employmentCardPdf.getCell(service.getCount()+" / "+service.getPersonInfo().getFullName(), font8, Element.ALIGN_CENTER, rowSpan, 0, 0));
					if(service.getProductName() != null && !service.getProductName().equalsIgnoreCase("")){
						serviceValueTable.addCell(employmentCardPdf.getCell(service.getProductName(), font8, Element.ALIGN_CENTER, rowSpan, 0, 0));
					}else{
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER,rowSpan, 0, 0));
					}
					
					if(serProject != null && serProject.getProdDetailsList() != null && serProject.getProdDetailsList().size() >0){
						for(ProductGroupList group : serProject.getProdDetailsList()){
							String name = "";
							try{
								name = arr[1];
							}catch(Exception e){
								name = "";
							}
							String key = group.getName()+"-"+name;
							logger.log(java.util.logging.Level.SEVERE , " GROUP :" + key+" quantity="+group.getQuantity()+" return quantity="+group.getReturnQuantity());
//							if((service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) || service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE))){ //Ashwini Patil Date:24-04-2024 commented this as totals were not matching with actual data
							if(technicianTotalMaterialMap.containsKey(key)){
								ProductGroupList productGroupList = technicianTotalMaterialMap.get(key);
								productGroupList.setProActualQty(productGroupList.getProActualQty() + group.getProActualQty());
//								productGroupList.setPlannedQty(productGroupList.getPlannedQty() + group.getPlannedQty());
								productGroupList.setPlannedQty(productGroupList.getPlannedQty() + group.getQuantity());//Ashwini Patil Date:24-04-2024
								productGroupList.setReturnQuantity(productGroupList.getReturnQuantity() + group.getReturnQuantity());
								technicianTotalMaterialMap.put(key, productGroupList);
							}else{
								ProductGroupList productGroupList = new ProductGroupList();
								productGroupList.setName(group.getName());
								productGroupList.setCreatedBy(name);
								productGroupList.setProActualQty(group.getProActualQty());
//								productGroupList.setPlannedQty(group.getPlannedQty());
								productGroupList.setPlannedQty(group.getQuantity());//Ashwini Patil Date:24-04-2024
								productGroupList.setReturnQuantity(group.getReturnQuantity());
								productGroupList.setUnit(group.getUnit());
								productGroupList.setWarehouse(group.getWarehouse());
								productGroupList.setStorageLocation(group.getStorageLocation());
								productGroupList.setStorageBin(group.getStorageBin());
								
								technicianTotalMaterialMap.put(key, productGroupList);
							}
//							}
							if(group.getName() != null && !group.getName().equalsIgnoreCase("")){
								serviceValueTable.addCell(employmentCardPdf.getCell(group.getName(), font8, Element.ALIGN_CENTER, 0, 0, 0));
							}else{
								serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
							}
							//Ashwini Patil Date:24-04-2024 group.getplannedquantity changed to group.getQuantity
							
							serviceValueTable.addCell(employmentCardPdf.getCell(group.getQuantity()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
							serviceValueTable.addCell(employmentCardPdf.getCell(group.getProActualQty()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
							serviceValueTable.addCell(employmentCardPdf.getCell(group.getReturnQuantity()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
							break;
						}
					}else{
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
					}
					
					noOfLine = rowSpan + 1;
					
					if(service.getStatus() != null && !service.getStatus().equalsIgnoreCase("")){
						serviceValueTable.addCell(employmentCardPdf.getCell(service.getStatus(), font8, Element.ALIGN_CENTER,rowSpan, 0, 0));
					}else{
						serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER,rowSpan, 0, 0));
					}
							
					serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, rowSpan, 0, 0));
					if(serProject != null && serProject.getProdDetailsList() != null && serProject.getProdDetailsList().size() >1){
						for(int i =1 ; i<serProject.getProdDetailsList().size() ;i++ ){
							ProductGroupList group =  serProject.getProdDetailsList().get(i);
							String key = group.getName()+"-"+arr[1];
							logger.log(java.util.logging.Level.SEVERE , " GROUP :" + key);
							if((service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSSCHEDULE) || service.getStatus().equalsIgnoreCase(Service.SERVICESTATUSRESCHEDULE))){
							if(technicianTotalMaterialMap.containsKey(key)){
								ProductGroupList productGroupList = technicianTotalMaterialMap.get(key);
								productGroupList.setProActualQty(productGroupList.getProActualQty() + group.getProActualQty());
//								productGroupList.setPlannedQty(productGroupList.getPlannedQty() + group.getPlannedQty());
								productGroupList.setPlannedQty(productGroupList.getPlannedQty() + group.getQuantity());
								
								productGroupList.setReturnQuantity(productGroupList.getReturnQuantity() + group.getReturnQuantity());
								technicianTotalMaterialMap.put(key, productGroupList);
							}else{
								ProductGroupList productGroupList = new ProductGroupList();
								productGroupList.setName(group.getName());
								productGroupList.setCreatedBy(arr[1]);
								productGroupList.setProActualQty(group.getProActualQty());
//								productGroupList.setPlannedQty(group.getPlannedQty());
								productGroupList.setPlannedQty(group.getQuantity());
								productGroupList.setReturnQuantity(group.getReturnQuantity());
								productGroupList.setUnit(group.getUnit());
								productGroupList.setWarehouse(group.getWarehouse());
								productGroupList.setStorageLocation(group.getStorageLocation());
								productGroupList.setStorageBin(group.getStorageBin());
								
								technicianTotalMaterialMap.put(key, productGroupList);
							}
							}
							if(group.getName() != null && !group.getName().equalsIgnoreCase("")){
								serviceValueTable.addCell(employmentCardPdf.getCell(group.getName(), font8, Element.ALIGN_CENTER, 0, 0, 0));
							}else{
								serviceValueTable.addCell(employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
							}
							
							serviceValueTable.addCell(employmentCardPdf.getCell(group.getQuantity()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
							serviceValueTable.addCell(employmentCardPdf.getCell(group.getProActualQty()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
							serviceValueTable.addCell(employmentCardPdf.getCell(group.getReturnQuantity()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
						}
					}
					
				}
					
				noOfLine++;
			}
			try {
				PdfPCell myCell = employmentCardPdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 9, 0);
				myCell.setBorder(Rectangle.BOTTOM);
				serviceValueTable.addCell(myCell);
				document.add(serviceValueTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
//		for (int i = position; i < sortedServiceList.size(); i++) {
//
//			System.out.println("servicelist size==" + sortedServiceList.size());
//			System.out.println("intial position value222==" + i);
//
//			
//			Service service = new Service();
//			service = sortedServiceList.get(i);
//			if(service != null){
//				
//			}
//
//
//			noOfLine++;
//			System.out.println("linecount11==" + noOfLine);
//		}
		System.out.println("linecount22==" + noOfLine);
		if (serviceMoreThanPage) {
			
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			createHeaderTable();
			createTitleTable();
			createServiceDetailTable(lineNo, sortedServiceList);

		}
	}
	private void createSummaryDetails(){
		
		
		PdfPTable headerTab = new PdfPTable(1);
		headerTab.setWidthPercentage(100);
		headerTab.setSpacingAfter(10);

		Phrase comName = new Phrase("Technicianwise Material Details :  "+fmt2.format(fromDATE) +" - "+ fmt2.format(toDATE), font12bold);
		PdfPCell ComNameCell = new PdfPCell(comName);
		ComNameCell.setBorder(0);
		ComNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerTab.addCell(ComNameCell);

		try {
			document.add(headerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	//	technicianTotalMaterialMap
		PdfPTable technicianMaterialSummaryTable = new PdfPTable(7);
		technicianMaterialSummaryTable.setWidthPercentage(100);
		//titleTab.setSpacingBefore(5);
		try {
			technicianMaterialSummaryTable.setWidths(new float[] { 20 ,20 ,12,12,12,12,12});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Service Engineer", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Material", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Planned Qty", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Required Qty", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Consumed Qty", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Stock to be transferred", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
		technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("Unit", font10bold, Element.ALIGN_CENTER, 0, 0, 0));
	
		for(Map.Entry<String, ProductGroupList> entry : technicianTotalMaterialMap.entrySet()){
			logger.log(java.util.logging.Level.SEVERE  , entry.getKey() + " "+availableQuntityMap + " "+availableQuntityMap.size());
			ProductGroupList group = entry.getValue();
			technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(group.getCreatedBy(), font10, Element.ALIGN_CENTER, 0, 0, 0));
			technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(group.getName(), font10, Element.ALIGN_CENTER, 0, 0, 0));
			technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(group.getPlannedQty()+"", font10, Element.ALIGN_CENTER, 0, 0, 0));
			technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(group.getProActualQty()+"", font10, Element.ALIGN_CENTER, 0, 0, 0));
			technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(group.getReturnQuantity()+"", font10, Element.ALIGN_CENTER, 0, 0, 0));
			logger.log(java.util.logging.Level.SEVERE  , availableQuntityMap.get(group.getName()+"-"+group.getStorageBin())+"");
			if(availableQuntityMap.containsKey(group.getName()+"-"+group.getStorageBin())){
				technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(df.format(availableQuntityMap.get(group.getName()+"-"+group.getStorageBin())), font10, Element.ALIGN_CENTER, 0, 0, 0));
			}else{
				technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell("0", font10, Element.ALIGN_CENTER, 0, 0, 0));
			}
			
			if(group.getUnit() != null && !group.getUnit().equalsIgnoreCase("")){
				technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(group.getUnit(), font10, Element.ALIGN_CENTER, 0, 0, 0));
			}else{
				technicianMaterialSummaryTable.addCell(employmentCardPdf.getCell(" ", font10, Element.ALIGN_CENTER, 0, 0, 0));
			}
			}
		try {
			document.add(technicianMaterialSummaryTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
}
