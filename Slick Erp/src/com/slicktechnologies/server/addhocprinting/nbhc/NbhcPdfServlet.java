package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;

public class NbhcPdfServlet  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1054371258604903857L;

	/**
	 * 
	 */
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			String pdfType = req.getParameter("type");
			String stringId = req.getParameter("Id");
			String loitype=req.getParameter("loiType");//Add by Jayshree
			stringId = stringId.trim();
			Long count = Long.parseLong(stringId);
			
			if(pdfType.equals("FAR")){
				
				FumigationAssessmentReportPdf pdf = new FumigationAssessmentReportPdf();
				pdf.document = new Document();
				Document document = pdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				pdf.setFAReport(count);
				pdf.createPdf();
				document.close();
				
			}else if(pdfType.equals("FMR")){
				
				FumigationMonitoringReportPdf fmrPdf = new FumigationMonitoringReportPdf();
				fmrPdf.document = new Document();
				Document document = fmrPdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				document.open();
				fmrPdf.setFMR(count);
				fmrPdf.createPdf();
				document.close();
				
			}else if(pdfType.equals("CHR")){
				
				CommodityHealthReportPdf pdf = new CommodityHealthReportPdf();
				pdf.document = new Document();
				Document document = pdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				pdf.setCHReport(count);
				pdf.createPdf();
				document.close();
				
			}else if(pdfType.equals("MIS")){
				
				MaterialIssueSheetPdf misPdf = new MaterialIssueSheetPdf();
				misPdf.document = new Document();
				Document document = misPdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				document.open();
				misPdf.setMIS(count);
				misPdf.createPdf();
				document.close();
				
			}else if(pdfType.equals("LOI")){
				System.out.println("Inside LOI");
				/**
				 * Date 16/12/2017
				 * By jayshree
				 * des.to print the quotation with data and without data conditions are cheched
				 */
				/**
				 * nidhi
				 */
				String prePrint=req.getParameter("prePrint");
				if(loitype.equalsIgnoreCase("yes")){/*
					System.out.println("Inside LOI yes");
				
				NBHCLoiPdf pdf = new NBHCLoiPdf();
				pdf.document = new Document(PageSize.A4.rotate(),20,20,30,30);
				Document document=pdf.document;
				PdfWriter.getInstance(document,resp.getOutputStream());
				if(!document.isOpen())
					document.open();
				pdf.setQuotationLoi(count);
				pdf.createPdf();
				document.close();
				*/

					System.out.println("Inside LOI yes");
				
					NBHCLoiPdf pdf = new NBHCLoiPdf();
					pdf.document = new Document();
					Document document = pdf.document;
					PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
					document.open();
					pdf.setQuotationLoi(count);
					pdf.createPdf(prePrint);
					document.close();
					
				}
				else if(loitype.equalsIgnoreCase("no")){
					System.out.println("Inside LOI no");
				NBHCloiPdfWithoutData pdfnodata=new NBHCloiPdfWithoutData();
				pdfnodata.document=new Document(PageSize.A4,35,35,0,0);
				Document document=pdfnodata.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				pdfnodata.setQuotationLoiBlankdata(count);
				pdfnodata.createPdf(prePrint);
				document.close();
				}
				//End By jayshree
			}else if(pdfType.equals("Gatepass")){
				
				NBHCGatePassPdf pdf = new NBHCGatePassPdf();
				pdf.document = new Document();
				Document document = pdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				pdf.setMMN(count);
				pdf.createPdf();
				document.close();
				
			}else if(pdfType.equals("IA")){
				
				InvoiceAcknowledgementPdf pdf = new InvoiceAcknowledgementPdf();
				pdf.document = new Document();
				Document document = pdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				pdf.setInvoiceAck(count);
				pdf.createPdf();
				document.close();
				
			}else if (pdfType.equals("ALPFUM")){
				System.out.print("heellloo");
				ALPFUMPdf aluminium = new ALPFUMPdf();
				aluminium.document = new Document();
				Document document = aluminium.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				aluminium.LoadaluminiumData(count);
				aluminium.createALPFUMPdf();
				document.close();
			}else if (pdfType.equals("MethyleBromide")){
				System.out.print("hiiiiihello");
				MethyleBromideFumigation methyle = new MethyleBromideFumigation();
				methyle.document = new Document();
				Document document = methyle.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				document.open();
				methyle.LoadData(count);
				methyle.createMethyleBromideFumigationPdf();
				document.close();
			}
			
			
			
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
