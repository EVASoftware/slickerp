package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class NBHCloiPdfWithoutData {

	public Document document;
	Quotation quotation;
	Customer cust;
	Employee emp;
	Branch branch;
	
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,
	font14bold;
	
	BaseColor mycolor = WebColors.getRGBColor("#C0C0C0");
	BaseColor mycolor1 = WebColors.getRGBColor("#F8F8F8");
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	/**
	 * nidhi
	 */
	Company company;
	
	public NBHCloiPdfWithoutData() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setQuotationLoiBlankdata(Long count) {
		
		quotation=ofy().load().type(Quotation.class).id(count).now();
		
		if(quotation!=null){
			Company company=ofy().load().type(Company.class).filter("companyId", quotation.getCompanyId()).first().now();
			this.company=company;
		}
		
		//   load customer 
		if(quotation.getCompanyId()!=null)
		      cust=ofy().load().type(Customer.class).filter("count",quotation.getCustomerId()).
		        filter("companyId",quotation.getCompanyId()).first().now();
			if(quotation.getCompanyId()==null)
			     cust=ofy().load().type(Customer.class).filter("count",quotation.getCustomerId()).first().now();
			
		//  load employee	
			
			if(quotation.getCompanyId()!=null)
			      emp=ofy().load().type(Employee.class).filter("fullname",quotation.getEmployee().trim()).
			        filter("companyId",quotation.getCompanyId()).first().now();
				if(quotation.getCompanyId()==null)
					emp=ofy().load().type(Employee.class).filter("fullname",quotation.getEmployee().trim()).first().now();
	
				
			//  load branch	
				
				if(quotation.getCompanyId()!=null)
					branch=ofy().load().type(Branch.class).filter("buisnessUnitName",quotation.getBranch()).
				        filter("companyId",quotation.getCompanyId()).first().now();
					if(quotation.getCompanyId()==null)
						branch=ofy().load().type(Branch.class).filter("buisnessUnitName",quotation.getBranch()).first().now();		
				
					
		
	}

	public void createPdf(String prePrint) {
		
		if (company.getUploadHeader() != null && prePrint.equalsIgnoreCase("no")) {
			createCompanyNameAsHeader(document, company);
		}
		if (company.getUploadFooter() != null  && prePrint.equalsIgnoreCase("no")) {
			createCompanyNameAsFooter(document, company);
		}
		createBlankforUPC();
		CreateHeading();
		CompanyAndCustomerAddress();
		CreateProductDetails();
		ContactPersonDetails();
	}

	

	

	

	private void CreateHeading() {
		
		Phrase heading = new Phrase("Letter Of Intent for Pest Management/Fumigation Services " ,font10bold );
		
		PdfPCell headingCell = new PdfPCell(heading);
		headingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingCell.setVerticalAlignment(Element.ALIGN_CENTER);
		headingCell.setBorder(0);
		headingCell.setBackgroundColor(mycolor);
		headingCell.setFixedHeight(20f);
		
		Paragraph serialNo = new Paragraph("Serial No.",font8);
		serialNo.setAlignment(Element.ALIGN_LEFT);
		
		Phrase serialno=new Phrase(serialNo); 
		PdfPCell serialCell=new PdfPCell(serialno);
		serialCell.setBackgroundColor(mycolor1);
		serialCell.setBorder(0);
//		serialCell.setVerticalAlignment(Element.ALIGN_CENTER);
		serialCell.setFixedHeight(15);
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.addCell(headingCell);
		table.addCell(serialCell);
		table.setSpacingAfter(5f);
	
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
		
	
	private void CompanyAndCustomerAddress() {
		
		
		
		
		PdfPTable customertable = new PdfPTable(1);
		customertable.setWidthPercentage(100f);
		
		
		for (int i = 0; i <=3; i++) {
			
			Phrase custBlankPh=new Phrase(" ");
			PdfPCell custBlank=new PdfPCell(custBlankPh);
			custBlank.setBorderWidthLeft(0);
			custBlank.setBorderWidthRight(0);
			custBlank.setBorderWidthTop(0);
			customertable.addCell(custBlank);
		}
		
		
		PdfPTable nbhcTable = new PdfPTable(1);
		nbhcTable.setWidthPercentage(100f);
		
		for (int j = 0; j <=3; j++) {
			
			Phrase comBlankPh=new Phrase(" ");
			PdfPCell comBlank=new PdfPCell(comBlankPh);
			comBlank.setBorderWidthLeft(0);
			comBlank.setBorderWidthRight(0);
			comBlank.setBorderWidthTop(0);
			nbhcTable.addCell(comBlank);
		}
		
		
		float[] columnWidths4={0.5f,4.3f,1.2f,4.3f};
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(5f);
		table.setSpacingAfter(10f);
		
		try {
			table.setWidths(columnWidths4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
//		Phrase totalBlank = new Phrase(" ");
//		PdfPCell totalBlankCell= new PdfPCell(totalBlank);
//		totalBlankCell.setBorder(0);
		
		
		
		Phrase to = new Phrase("To :",font8);
		PdfPCell toCell= new PdfPCell(to);
		toCell.setBorder(0);
		table.addCell(toCell);
		
		
		PdfPCell nbhcTableCell = new PdfPCell(nbhcTable);
		nbhcTableCell.setBorder(0);
		nbhcTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(nbhcTableCell);
		
		
		Phrase from = new Phrase("From :",font8);
		PdfPCell fromCell= new PdfPCell(from);
		fromCell.setBorder(0);
		fromCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(fromCell);
		
		PdfPCell customerTableCell =new PdfPCell(customertable);
		customerTableCell.setBorder(0);
		customerTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(customerTableCell);
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
		
	}
	
	
	
	private void CreateProductDetails() {
		
		PdfPTable tablepresmise= new PdfPTable(1);
		tablepresmise.setWidthPercentage(100f);
		
		Phrase quotNote = new Phrase("We are pleased to avail your pest management / fumigation services as per quote dated _____________ and request you to provide services from _________________ to ______________",font8);
		PdfPCell quotNoteCell = new PdfPCell(quotNote);
		quotNoteCell.setBorder(0);
		quotNoteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		tablepresmise.addCell(quotNoteCell);
		
		Phrase bllank1 = new Phrase(" ",font9bold);
		PdfPCell bllank1Cell = new PdfPCell(bllank1);
		bllank1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bllank1Cell.setBorder(0);
		tablepresmise.addCell(bllank1Cell);
		tablepresmise.addCell(bllank1Cell);
		
		
		
		Phrase note = new Phrase("Site Address and Area in(Sq.ft)for Services:",font9bold);
		PdfPCell noteCell = new PdfPCell(note);
		noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		noteCell.setBorder(0);
		tablepresmise.addCell(noteCell);
		
		
		Phrase premise = new Phrase(" ",font8);
		PdfPCell premiseCell = new PdfPCell(premise);
		premiseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		premiseCell.setBorderWidthLeft(0);
		premiseCell.setBorderWidthRight(0);
		premiseCell.setBorderWidthTop(0);
		tablepresmise.addCell(premiseCell);
		tablepresmise.addCell(premiseCell);
		tablepresmise.addCell(premiseCell);
		
		
		try {
			document.add(tablepresmise);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		float[] columnWidths3={5f,1.5f,3.5f};
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(15f);
		table.setSpacingBefore(15f);
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase productNameHeading = new Phrase("Service",font8bold);
		PdfPCell productNameHeadingCell = new PdfPCell(productNameHeading);
		productNameHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productNameHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(productNameHeadingCell);
	
		
		Phrase frequencyHeading = new Phrase("Frequency",font8bold);
		PdfPCell frequencyHeadingCell = new PdfPCell(frequencyHeading);
		frequencyHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		frequencyHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(frequencyHeadingCell);
		
		Phrase changesHeading = new Phrase("Service Charges"+"\n"+"(excluding Tax)",font8bold);
		PdfPCell changesHeadingCell = new PdfPCell(changesHeading);
		changesHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		changesHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(changesHeadingCell);
		
		
		for (int i = 1; i <=7*3; i++) {
			
		Phrase prodblank=new Phrase(" ");
		PdfPCell prodBlankCell=new PdfPCell(prodblank);
		table.addCell(prodBlankCell);
		
		}
		
		Phrase totalHeading = new Phrase("TOTAL",font9bold);
		PdfPCell totalHeadingCell = new PdfPCell(totalHeading);
		totalHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(totalHeadingCell);
		
		Phrase blankcolour = new Phrase(" ",font9bold);
		PdfPCell blankcolourCell = new PdfPCell(blankcolour);
		blankcolourCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankcolourCell.setBackgroundColor(mycolor1);
		table.addCell(blankcolourCell);
		
		
		
		Phrase totalValue = new Phrase("",font9bold);
		PdfPCell totalValueCell = new PdfPCell(totalValue);
		totalValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalValueCell.setBackgroundColor(mycolor1);
		table.addCell(totalValueCell);
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable noteTable = new PdfPTable(1);
		noteTable.setWidthPercentage(100f);
		
		Phrase note1 = new Phrase("We shall make payment in advance / on completion of services within 7 days of receipt of invoices. "
				+"\n"+ "Invoice for services may be raised on our address above or as below:",font9bold);
		
		PdfPCell note1Cell = new PdfPCell(note1);
		note1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		note1Cell.setBorder(0);
		noteTable.addCell(note1Cell);
		
		
		
		Phrase blankFrom3sides = new Phrase(" ",font8);
		PdfPCell blankFrom3sidesCell = new PdfPCell(blankFrom3sides);
		blankFrom3sidesCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankFrom3sidesCell.setBorderWidthLeft(0);
		blankFrom3sidesCell.setBorderWidthRight(0);
		blankFrom3sidesCell.setBorderWidthTop(0);
		noteTable.addCell(blankFrom3sidesCell);
		noteTable.addCell(blankFrom3sidesCell);
		noteTable.addCell(blankFrom3sidesCell);
		
		
		try {
			document.add(noteTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	private void ContactPersonDetails() {
		
		Paragraph contact = new Paragraph("Our contact person for your services shall be :",font9bold);
		contact.setAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(contact);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		float[] columnWidths3={1.2f,0.2f,3.6f,1f,1.2f,0.2f,3.6f};//4.5f,1f,4.5f
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase name=new Phrase("Name ",font8);//1
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.addElement(name);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCell.setBorder(0);
		table.addCell(nameCell);
		
		
		Phrase namecolon=new Phrase(":",font8);//2
		PdfPCell namecolonCell = new PdfPCell(namecolon);
		namecolonCell.addElement(namecolon);
		namecolonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		namecolonCell.setBorder(0);
		table.addCell(namecolonCell);
		
		Phrase nameBlank=new Phrase(" ",font8);//3
		PdfPCell nameBlankCell = new PdfPCell(nameBlank);
		nameBlankCell.addElement(nameBlank);
		nameBlankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameBlankCell.setBorderWidthLeft(0);
		nameBlankCell.setBorderWidthRight(0);
		nameBlankCell.setBorderWidthTop(0);
		table.addCell(nameBlankCell);
		
		
		Phrase nameBlank2=new Phrase(" ",font8);//4
		PdfPCell nameBlankCell2 = new PdfPCell(nameBlank2);
		nameBlankCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameBlankCell2.setBorder(0);
//		nameBlankCell.setBorderWidthRight(0);
//		nameBlankCell.setBorderWidthTop(0);
		table.addCell(nameBlankCell2);
		
		
		Phrase designationph=new Phrase("Designation",font8);//5
		PdfPCell designationCell = new PdfPCell(designationph);
		designationCell.addElement(designationph);
		designationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		designationCell.setBorder(0);
		table.addCell(designationCell);
		
		table.addCell(namecolonCell);//6
		
		table.addCell(nameBlankCell);//7
		
//		table.addCell(nameBlankCell2);
		
		Phrase telph=new Phrase("Tel ",font8);//1
		PdfPCell telCell = new PdfPCell(telph);
		telCell.addElement(telph);
		telCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		telCell.setBorder(0);
		table.addCell(telCell);
		
		table.addCell(namecolonCell);//2
		
		table.addCell(nameBlankCell);//3
		
		table.addCell(nameBlankCell2);//4
		
		
		Phrase emailph=new Phrase("Email ",font8);//5
		PdfPCell emailCell = new PdfPCell(emailph);
		emailCell.addElement(emailph);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailCell.setBorder(0);
		table.addCell(emailCell);
		
		table.addCell(namecolonCell);//6
		
		table.addCell(nameBlankCell);//7
		
//		table.addCell(nameBlankCell2);
		
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		
		Phrase confirm = new Phrase("We confirm having read and understood terms and conditions of services.",font8);
		PdfPCell confirmCell = new PdfPCell(confirm);
		confirmCell.addElement(confirm);
		confirmCell.setBorder(0);
		confirmCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		confirmCell.setColspan(3);
		table.addCell(confirmCell);
		
		table.addCell(nameBlankCell2);
		
		Phrase received = new Phrase("Received and accepted.",font8);
		PdfPCell receivedCell = new PdfPCell(received);
		receivedCell.addElement(received);
		receivedCell.setBorder(0);
		receivedCell.setColspan(3);
		receivedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(receivedCell);
		
		Phrase sincerely = new Phrase("Your sincerely,",font8);
		PdfPCell sincerelyCell = new PdfPCell(sincerely);
		sincerelyCell.addElement(sincerely);
		sincerelyCell.setBorder(0);
		sincerelyCell.setColspan(7);
		sincerelyCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(sincerelyCell);
		
		
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		
		
		Phrase sign = new Phrase("Signature",font8);//1
		PdfPCell signCell = new PdfPCell(sign);
		signCell.addElement(sign);
		signCell.setBorder(0);
		signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(signCell);
		
		table.addCell(namecolonCell);//2
		table.addCell(nameBlankCell2);//3
	
		table.addCell(nameBlankCell2);//4
		
		
		table.addCell(signCell);//5
		table.addCell(namecolonCell);//6
		table.addCell(nameBlankCell2);//7
		
		
		table.addCell(nameCell);//right name1
		table.addCell(namecolonCell);//2
		table.addCell(nameBlankCell);//3
		table.addCell(nameBlankCell2);//4
		
		

		Phrase representph = new Phrase("NBHC Representative    :  _________________________",font8);//5
		PdfPCell representCell = new PdfPCell(representph);
		representCell.addElement(representph);
		representCell.setBorder(0);
		representCell.setColspan(3);
		representCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(representCell);
		
//		table.addCell(namecolonCell);//2
//		table.addCell(nameBlankCell);//7
		
		Phrase custCell = new Phrase("Contact No.",font8);//1
		PdfPCell custCellCell = new PdfPCell(custCell);
		custCellCell.addElement(custCell);
		custCellCell.setBorder(0);
		custCellCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(custCellCell);
		
		
		table.addCell(namecolonCell);//2
		table.addCell(nameBlankCell);//3
		table.addCell(nameBlankCell2);//4
		
		
		table.addCell(custCellCell);//right contact5
		table.addCell(namecolonCell);//6
		table.addCell(nameBlankCell);//7
		
		
		Phrase emailph2 = new Phrase("Email",font8);//1
		PdfPCell emailCell2 = new PdfPCell(emailph2);
		emailCell2.addElement(emailph2);
		emailCell2.setBorder(0);
		emailCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(emailCell2);
		
		
		table.addCell(namecolonCell);//2
		table.addCell(nameBlankCell);//3
		table.addCell(nameBlankCell2);//4
		
		
		table.addCell(emailCell2);//5
		table.addCell(namecolonCell);//6
		table.addCell(nameBlankCell);//7
		
		
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		table.addCell(nameBlankCell2);
		
		
		Phrase stamp = new Phrase("Date & Stamp :",font8);
		PdfPCell stampCell = new PdfPCell(stamp);
		stampCell.addElement(stamp);
		stampCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stampCell.setBorder(0);
		stampCell.setColspan(7);
		table.addCell(stampCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			Image image2 =Image.getInstance("images/NBHC_Header.jpg");
			image2.scalePercent(10f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(37f, 770f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			Image image2 =Image.getInstance("images/NBHC_Footer.png");
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(37f, 10f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private void createBlankforUPC() {
		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
