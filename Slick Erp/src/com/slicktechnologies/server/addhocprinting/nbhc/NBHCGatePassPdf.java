package com.slicktechnologies.server.addhocprinting.nbhc;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.MaterialMovementNote;
import com.slicktechnologies.shared.common.inventory.WareHouse;

public class NBHCGatePassPdf {

	
	
	public Document document;
	
	MaterialMovementNote mmn;
	Company comp;
	
	private Font font16boldul, font12bold, font8bold, font8,font9, font9bold,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,
	font14bold;
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	/**
	 * Date : 17-03-2017 By ANIL
	 */
	String fromWhName="";
	String toWhName="";
	String fromWhAddress="";
	String toWhAddress="";
	
	
	public NBHCGatePassPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	
	public void setMMN(Long count) {
		
		mmn=ofy().load().type(MaterialMovementNote.class).id(count).now();
		
		if(mmn.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",mmn.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		if(mmn.getMmnTransactionType().equalsIgnoreCase("TransferOut")){
			toWhName=mmn.getTransToWareHouse();
			WareHouse transferToWh=ofy().load().type(WareHouse.class).filter("companyId", mmn.getCompanyId())
					.filter("buisnessUnitName", mmn.getTransToWareHouse()).filter("status", true).first().now();
			if(transferToWh!=null){
				if(transferToWh.getAddress().getAddrLine1()!=null){
					toWhAddress=toWhAddress+transferToWh.getAddress().getAddrLine1();
				}
				if(transferToWh.getAddress().getAddrLine2()!=null){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getAddrLine2();
				}
				if(transferToWh.getAddress().getLandmark()!=null){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getLandmark();
				}
				if(transferToWh.getAddress().getLocality()!=null){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getLocality();
				}
				if(transferToWh.getAddress().getCity()!=null){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getCity();
				}
				if(transferToWh.getAddress().getPin()!=0){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getPin();
				}
				if(transferToWh.getAddress().getState()!=null){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getState();
				}
				if(transferToWh.getAddress().getCountry()!=null){
					toWhAddress=toWhAddress.trim()+" "+transferToWh.getAddress().getCountry();
				}
			}
			
			fromWhName="";
			if(mmn.getSubProductTableMmn()!=null&&mmn.getSubProductTableMmn().size()!=0){
				fromWhName=mmn.getSubProductTableMmn().get(0).getMaterialProductWarehouse();
			}
			
			WareHouse transferFromWh=ofy().load().type(WareHouse.class).filter("companyId", mmn.getCompanyId())
					.filter("buisnessUnitName", fromWhName).filter("status", true).first().now();
			if(transferFromWh!=null){
				if(transferFromWh.getAddress().getAddrLine1()!=null){
					fromWhAddress=fromWhAddress+transferFromWh.getAddress().getAddrLine1();
				}
				if(transferFromWh.getAddress().getAddrLine2()!=null){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getAddrLine2();
				}
				if(transferFromWh.getAddress().getLandmark()!=null){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getLandmark();
				}
				if(transferFromWh.getAddress().getLocality()!=null){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getLocality();
				}
				if(transferFromWh.getAddress().getCity()!=null){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getCity();
				}
				if(transferFromWh.getAddress().getPin()!=0){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getPin();
				}
				if(transferFromWh.getAddress().getState()!=null){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getState();
				}
				if(transferFromWh.getAddress().getCountry()!=null){
					fromWhAddress=fromWhAddress.trim()+" "+transferFromWh.getAddress().getCountry();
				}
			}
		}
	   
	}
	
	
	public void createPdf() {
		
		
		if(comp.getUploadHeader()!=null){
		createCompanyNameAsHeader(document,comp);
		}
		
		if(comp.getUploadFooter()!=null){
		createCompanyNameAsFooter(document,comp);
		}
		createBlankforUPC();
		
		CreateHeadder();
		createAdddressDetails();
		createMMNProductTable();
		createFooterTable();
	}
	
	
	private void createBlankforUPC() {

		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}


	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		}


	private void createAdddressDetails() {
		PdfPTable table =new PdfPTable(2);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);
		table.setSpacingAfter(20f);
		
		PdfPTable table1 =new PdfPTable(2);
		table1.setWidthPercentage(100f);
		table1.setSpacingBefore(20f);
//		table1.setSpacingAfter(20f);
		
		Phrase phFrom=new Phrase("From : ");
		Phrase phTo=new Phrase("To : ");
		
		Phrase phFromWhName=new Phrase(fromWhName,font9);
		Phrase phToWhName=new Phrase(toWhName,font9);
		
		Phrase phFromWhAddress=new Phrase(fromWhAddress,font9);
		Phrase phToWhAddress=new Phrase(toWhAddress,font9);
		
		
		PdfPCell cell1=new PdfPCell(phFrom);
		cell1.setBorder(0);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table1.addCell(cell1);
		
		PdfPCell cell2=new PdfPCell(phTo);
		cell2.setBorder(0);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table1.addCell(cell2);
		
		PdfPCell cell3=new PdfPCell(phFromWhName);
		cell3.setBorder(0);
		cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell3);
		
		PdfPCell cell4=new PdfPCell(phToWhName);
		cell4.setBorder(0);
		cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell4);
		
		PdfPCell cell5=new PdfPCell(phFromWhAddress);
		cell5.setBorder(0);
		cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell5);
		
		PdfPCell cell6=new PdfPCell(phToWhAddress);
		cell6.setBorder(0);
		cell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(cell6);
		
		try {
			document.add(table1);
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
	}


	private void createMMNProductTable() {
	
		Paragraph para = new Paragraph("We are sending you following materials and request you to confirm receipt.",font10);
		para.setAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(para);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		float[] columnWidths5={1.0f,4.0f,1.0f,2.0f,2.0f};
		
		PdfPTable table = new PdfPTable(5);
		try {
			table.setWidths(columnWidths5);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		table.setWidthPercentage(100f);
		table.setSpacingAfter(20f);
		table.setSpacingBefore(20f);
		
		Phrase srNo = new Phrase("Sr No",font10bold);
		PdfPCell srNoCell = new PdfPCell(srNo);
		table.addCell(srNoCell);
		
		Phrase items = new Phrase("Item",font10bold);
		PdfPCell itemsCell = new PdfPCell(items);
		itemsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(itemsCell);
		
		Phrase units = new Phrase("Units",font10bold);
		PdfPCell unitsCell = new PdfPCell(units);
		unitsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(unitsCell);
		
		Phrase returnable = new Phrase("Returnable",font10bold);
		PdfPCell returnableCell = new PdfPCell(returnable);
		returnableCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(returnableCell);
		
		Phrase  nonreturn= new Phrase("Non-Returnable",font10bold);
		PdfPCell nonreturnCell = new PdfPCell(nonreturn);
		nonreturnCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(nonreturnCell);
		
		
		for (int i = 0; i < mmn.getSubProductTableMmn().size(); i++) {
			
			Phrase srNoValue= new Phrase(i+1+"",font10);
			PdfPCell srNoValueCell = new PdfPCell(srNoValue);
			srNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(srNoValueCell);
			
			Phrase itemValue= new Phrase(mmn.getSubProductTableMmn().get(i).getMaterialProductName(),font10);
			PdfPCell itemValueCell =new PdfPCell(itemValue);
			itemValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(itemValueCell);
			
			Phrase unitsreq= new Phrase(mmn.getSubProductTableMmn().get(i).getMaterialProductRequiredQuantity()+"",font8);
			PdfPCell unitsreqCell =new PdfPCell(unitsreq);
			unitsreqCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(unitsreqCell);
			
			Phrase blank= new Phrase(" ",font8);
			PdfPCell blankCell =new PdfPCell(blank);
			table.addCell(blankCell);
			table.addCell(blankCell);
			
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
			table.addCell(blankCell);
		}
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Paragraph mypara = new Paragraph("These materials are meant for",font10);
		mypara.setAlignment(Element.ALIGN_LEFT);
		try {
			document.add(mypara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		float[] columnWidths4={7.0f,3.0f};
		PdfPTable myTable = new PdfPTable(2);
		myTable.setWidthPercentage(100);
		try {
			myTable.setWidths(columnWidths4);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		myTable.setSpacingBefore(5f);
		myTable.setSpacingAfter(20f);
		
		Phrase sideblank = new Phrase(" ");
		PdfPCell sideblankCell =new PdfPCell(sideblank);
		sideblankCell.setBorderWidthLeft(0);
		sideblankCell.setBorderWidthTop(0);
		sideblankCell.setBorderWidthRight(0);
		
		myTable.addCell(sideblankCell);
		
		Phrase note = new Phrase("and shall be taken back at end ",font10);
		PdfPCell noteCell = new PdfPCell(note);
		noteCell.setBorder(0);
		noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		myTable.addCell(noteCell);
		
		Phrase note1 =  new Phrase("of period when they have been used.",font10);
		PdfPCell note1Cell = new PdfPCell(note1);
		note1Cell.setBorder(0);
		note1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		myTable.addCell(note1Cell);
		
		Phrase blank =  new Phrase(" ",font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		myTable.addCell(blankCell);
		
		try {
			document.add(myTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void CreateHeadder() {
		
		Paragraph headingPara =new Paragraph("Gate Pass",font12bold);
		headingPara.setAlignment(Element.ALIGN_CENTER);
		
		try {
			document.add(headingPara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(15f);
		
		Phrase mmnId = new Phrase("MMN Id :"+mmn.getCount(),font10);
		PdfPCell mmnIdCell = new PdfPCell(mmnId);
		mmnIdCell.setBorder(0);
		
		Phrase blank = new Phrase(" ",font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);

		
		Phrase mmnDate = new Phrase("MMN Date :"+sdf.format(mmn.getMmnDate()),font10);
		PdfPCell mmnDateCell = new PdfPCell(mmnDate);
		mmnDateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		mmnDateCell.setBorder(0);

		table.addCell(mmnIdCell);
		table.addCell(blankCell);
		table.addCell(mmnDateCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void createFooterTable()
	{
		PdfPTable representiveTbl = new PdfPTable (2);
		representiveTbl.setWidthPercentage(100);
		
		PdfPCell nbhcRepresentiveCell = new PdfPCell ();
		Phrase nbhcrepresentrive1 = new Phrase ("Signature and Date",font10);
		Phrase nbhcrepresentrive2 = new Phrase ("Name : "+mmn.getApproverName(),font10);
		nbhcRepresentiveCell.addElement(nbhcrepresentrive1);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		nbhcRepresentiveCell.addElement(nbhcrepresentrive2);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		

		PdfPCell custRepresentiveCell = new PdfPCell ();
		Phrase custRepresentive1 = new Phrase ("Signature and Date",font10);
		Phrase custRepresentive2 = new Phrase ("Name :"+mmn.getEmployee(),font10);
		custRepresentiveCell.addElement(custRepresentive1);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
		custRepresentiveCell.addElement(custRepresentive2);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
		
		
		PdfPCell nbhcRepresentiveLblCell = new PdfPCell ();
		Phrase nbhcrepresentriveLbl = new Phrase ("Issued By ",font10);
		Paragraph nbhcRepPara = new Paragraph();
		nbhcRepPara.add(nbhcrepresentriveLbl);
		nbhcRepPara.setAlignment(Element.ALIGN_CENTER);
		nbhcRepresentiveLblCell.addElement(nbhcRepPara);
		
		PdfPCell custRepresentiveLblCell = new PdfPCell ();
		Phrase custRepresentiveLbl = new Phrase ("Received By",font10);
		Paragraph custRepPara = new Paragraph();
		custRepPara.add(custRepresentiveLbl);
		custRepPara.setAlignment(Element.ALIGN_CENTER);
		custRepresentiveLblCell.addElement(custRepPara);
		
		representiveTbl.addCell(nbhcRepresentiveCell);
		representiveTbl.addCell(custRepresentiveCell); 
		representiveTbl.addCell(nbhcRepresentiveLblCell);
		representiveTbl.addCell(custRepresentiveLblCell);
		
		
		try {
			document.add(representiveTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}



	
}