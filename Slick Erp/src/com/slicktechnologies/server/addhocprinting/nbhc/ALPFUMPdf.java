package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class ALPFUMPdf {
	public Document document;
	Contract con;
	Customer cust;
	Company comp;
	Fumigation fumigation;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,font12Wbold,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,font10ul,font9boldul,
	font14bold,font9,font7,font7bold,font9red,font9boldred,font12boldred,font23;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat fmt2=new SimpleDateFormat("dd.MM.yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	public ALPFUMPdf() {
		super();
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
				}
	
	public void LoadaluminiumData(Long count) {
		// TODO Auto-generated method stub
		
	
		 fumigation = ofy().load().type(Fumigation.class).id(count).now();
		
		comp =ofy().load().type(Company.class).filter("companyId", fumigation.getCompanyId()).first().now();
		
	}

	
 public void createALPFUMPdf(){
	 
	 createALPFUMSamplePdf();
 }

private void createALPFUMSamplePdf() {
	// TODO Auto-generated method stub
//	System.out.print("hiii");
//	 if (comp.getUploadHeader() != null) {
//	 createCompanyNameAsHeader(document, comp);
//	 }
//	 if (comp.getUploadFooter() != null) {
//	 createCompanyNameAsFooter(document, comp);
//	 }
	// TODO Auto-generated method stub
	
	createBlankforUPC();
	createHeaderTable();
	createDetailTable();
}





private void createHeaderTable() {
	// TODO Auto-generated method stub
	PdfPTable table=new PdfPTable(1);
	table.setWidthPercentage(100f);
	table.setSpacingBefore(50f);
	
	Phrase headPh=new Phrase("Fumigation Certificate",font10bold);
	PdfPCell headCell=new PdfPCell(headPh);
	//headCell.addElement(headPh);
	//headCell.setBorder(0);
	headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headCell.setFixedHeight(18f);
	headCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	table.addCell(headCell);
	
	
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		PdfPTable companyName=new PdfPTable(2);
		companyName.setWidthPercentage(100f);
		
		try {
			companyName.setWidths(new float[] { 50,50});
		 } catch (DocumentException e1) {
			e1.printStackTrace();
		 }
		
	

	//Phrase companyNamePhrase=new Phrase("National Bulk Handling Corporation Pvt Ltd.",font10bold);
	
	Phrase companyNamePhrase=null;
	if(comp.getBusinessUnitName()!=null){
		companyNamePhrase=new Phrase(comp.getBusinessUnitName(),font10bold);
	}else{
		companyNamePhrase=new Phrase(" ",font10bold);
	}
	PdfPCell companyNameCell=new PdfPCell(companyNamePhrase);
	companyNameCell.setBorderWidthBottom(0);
	companyNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	companyNameCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	companyNameCell.setPaddingLeft(6f);
	companyName.addCell(companyNameCell);
	
	Phrase treatmentCertificationnumberphrase=new Phrase("Treatment Certificate Number ",font10bold);
	PdfPCell treatmentCertificationnumberCell=new PdfPCell(treatmentCertificationnumberphrase);
	//treatmentCertificationnumberCell.setRowspan(2);
	treatmentCertificationnumberCell.setBorderWidthBottom(0);
	treatmentCertificationnumberCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	treatmentCertificationnumberCell.setVerticalAlignment(Element.ALIGN_CENTER);
	treatmentCertificationnumberCell.setPaddingLeft(6f);
	companyName.addCell(treatmentCertificationnumberCell);
	
	
	//Phrase companyAddPhrase=new Phrase("Ground Floor ,Plot No .10,                                                "
	//+ "Sector 26,Vashi,                                                                                   "
	//+ "Navi Mumbai-400705                                                            ",font10);
	
	Phrase companyAddPhrase=null;
	if(comp.getAddress()!=null){
		companyAddPhrase=new Phrase(comp.getAddress().getCompleteAddress(),font10);
	}else{
		companyAddPhrase=new Phrase(" ",font10);
	}
	PdfPCell companyAddCell=new PdfPCell(companyAddPhrase);
	//companyAddCell.setBorderWidthLeft();
	//companyAddCell.setBorderWidthRight(1);
	companyAddCell.setBorderWidthTop(0);
	companyAddCell.setBorderWidthBottom(0);
	companyAddCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	companyAddCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	companyAddCell.setPaddingLeft(6f);
	companyName.addCell(companyAddCell);
	
	
	//Phrase treatmentCertificationnumberValuephrase=new Phrase("",font10bold);
	
	Phrase treatmentCertificationnumberValuephrase=null;
	if(fumigation.getCertificateNo()!=null){
		treatmentCertificationnumberValuephrase=new Phrase(fumigation.getCertificateNo(),font10);
	}else{
		treatmentCertificationnumberValuephrase=new Phrase(" ",font10);
	}
	PdfPCell treatmentCertificationnumberValueCell=new PdfPCell(treatmentCertificationnumberValuephrase);
	//treatmentCertificationnumberCell.setRowspan(2);
	treatmentCertificationnumberValueCell.setBorderWidthTop(0);
	//treatmentCertificationnumberCell.setBorder(0);
	treatmentCertificationnumberValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	treatmentCertificationnumberValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
	treatmentCertificationnumberValueCell.setPaddingLeft(6f);
	companyName.addCell(treatmentCertificationnumberValueCell);
	
	
	
	
	
	
	Phrase datePQRSPhrase=new Phrase("(Dte PQRS Reg.No.72/ALP Dt.31.07.2012)",font10bold);
	PdfPCell datePQRSCell=new PdfPCell(datePQRSPhrase);
	//headCell.addElement(headPh);
	//headCell.setBorder(0);
	datePQRSCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//datePQRSCell.setFixedHeight(18f);
	datePQRSCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	//datePQRSCell.setBorderWidthLeft(1);
	//datePQRSCell.setBorderWidthBottom(1);
	//datePQRSCell.setBorderWidthRight(0);
	datePQRSCell.setBorderWidthTop(0);
	datePQRSCell.setPaddingLeft(6f);
	companyName.addCell(datePQRSCell);
	
	
	//Phrase dateOfIssue=new Phrase("Date Of Issue: 17.12.2016",font10bold);
	Phrase dateOfIssue=null;
	if(fumigation.getDateofissue()!=null){
		dateOfIssue=new Phrase("Date Of Issue:"+fmt2.format(fumigation.getDateofissue()),font10bold);
	}else{
		dateOfIssue=new Phrase(" Date Of Issue:",font10);
	}
	PdfPCell dateOfIssueCell=new PdfPCell(dateOfIssue);
	//headCell.addElement(headPh);
	//headCell.setBorder(0);
	dateOfIssueCell.setBorderWidthTop(0);
	dateOfIssueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dateOfIssueCell.setFixedHeight(18f);
	dateOfIssueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	dateOfIssueCell.setPaddingLeft(6f);
	companyName.addCell(dateOfIssueCell);
	
	
	
	
	
	
	
	
	try {
		document.add(companyName);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
private void createDetailTable() {
	// TODO Auto-generated method stub
	PdfPTable detailTable=new PdfPTable(2);
	detailTable.setWidthPercentage(100f);
	
	try {
		detailTable.setWidths(new float[] { 50,50});
	 } catch (DocumentException e1) {
		e1.printStackTrace();
	 }
	
	
	Phrase infoPhrase=new Phrase("This is to certify that the goods described below were treated in accordance with the fumigation treatment requirment of importing country (CAPE TOWN,SOUTH AFRICA)and declared that the consignment has been verified free of impervious surfaces /layers such as plastic wrapping or laminated plastic films,lacquered or painted surfaces,Aluminium foil,tarred or waxed paper etc.that may adversely effect the penetration "
	+ "of the fumigant,prior to fumigation.   ",font8);
	
	
	PdfPCell infoPhraseCell=new PdfPCell(infoPhrase);
	//headCell.addElement(headPh);
	//headCell.setBorder(0);
	infoPhraseCell.setColspan(2);
	infoPhraseCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	//infoPhraseCell.setFixedHeight(18f);
	infoPhraseCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	infoPhraseCell.setPaddingLeft(6f);
	detailTable.addCell(infoPhraseCell);
	
	
	Phrase DetailOfTreatmentPhrase=new Phrase("Details Of Treatment",font10bold);
	PdfPCell DetailOfTreatmentCell=new PdfPCell(DetailOfTreatmentPhrase);
	DetailOfTreatmentCell.setColspan(2);
	DetailOfTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	DetailOfTreatmentCell.setFixedHeight(20f);
	DetailOfTreatmentCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	DetailOfTreatmentCell.setPaddingLeft(6f);
	detailTable.addCell(DetailOfTreatmentCell);
	
	
	Phrase nameOfFumigantPhrase=new Phrase("Name of Fumigant",font10);
	
	PdfPCell nameOfFumigantCell=new PdfPCell(nameOfFumigantPhrase);
	nameOfFumigantCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameOfFumigantCell.setFixedHeight(18f);
	nameOfFumigantCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameOfFumigantCell.setPaddingLeft(6f);
	detailTable.addCell(nameOfFumigantCell);
	
	
	//Phrase nameOfFumigantValuePhrase=new Phrase("Aluminium Phosphide",font10);
	Phrase nameOfFumigantValuePhrase=null;
	if(fumigation.getNameoffumigation()!=null){
		nameOfFumigantValuePhrase=new Phrase(fumigation.getNameoffumigation(),font10);
	}else{
		nameOfFumigantValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell nameOfFumigantvalueCell=new PdfPCell(nameOfFumigantValuePhrase);
	nameOfFumigantvalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameOfFumigantvalueCell.setFixedHeight(18f);
	nameOfFumigantvalueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameOfFumigantvalueCell.setPaddingLeft(6f);
	detailTable.addCell(nameOfFumigantvalueCell);
	
	
	Phrase dateOfFumigantPhrase=new Phrase("Date of Fumigation",font10);
	PdfPCell dateOfFumigantCell=new PdfPCell(dateOfFumigantPhrase);
	dateOfFumigantCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dateOfFumigantCell.setFixedHeight(18f);
	dateOfFumigantCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	dateOfFumigantCell.setPaddingLeft(6f);
	detailTable.addCell(dateOfFumigantCell);
	
	
	//Phrase dateOfFumigantValuePhrase=new Phrase("09/12/2016",font10);
	Phrase dateOfFumigantValuePhrase=null;
	if(fumigation.getDateoffumigation()!=null){
		dateOfFumigantValuePhrase=new Phrase(fmt.format(fumigation.getDateoffumigation()),font10);
	}
	else{
		dateOfFumigantValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell dateOfFumigantValueCell=new PdfPCell(dateOfFumigantValuePhrase);
	dateOfFumigantValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dateOfFumigantValueCell.setFixedHeight(18f);
	dateOfFumigantValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	dateOfFumigantValueCell.setPaddingLeft(6f);
	detailTable.addCell(dateOfFumigantValueCell);
	
	
	Phrase placeOfFumigantPhrase=new Phrase("Place of Fumigation",font10);
	PdfPCell placeOfFumigantCell=new PdfPCell(placeOfFumigantPhrase);
	placeOfFumigantCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	placeOfFumigantCell.setFixedHeight(18f);
	placeOfFumigantCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	placeOfFumigantCell.setPaddingLeft(6f);
	detailTable.addCell(placeOfFumigantCell);
	
	//Phrase placeOfFumigantValuePhrase=new Phrase("VIRGINIA MHAPE ,NAVI MUMBAI",font10);
	
	Phrase placeOfFumigantValuePhrase=null;
	if(fumigation.getPlaceoffumigation()!=null){
		placeOfFumigantValuePhrase=new Phrase(fumigation.getPlaceoffumigation(),font10);
	}
	else{
		placeOfFumigantValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell placeOfFumigantValueCell=new PdfPCell(placeOfFumigantValuePhrase);
	placeOfFumigantValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	placeOfFumigantValueCell.setFixedHeight(18f);
	placeOfFumigantValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	placeOfFumigantValueCell.setPaddingLeft(6f);
	detailTable.addCell(placeOfFumigantValueCell);
	
	
	Phrase dosegeOfFumigantPhrase=new Phrase("Dosage of Fumigant",font10);
	PdfPCell dosegeOfFumigantCell=new PdfPCell(dosegeOfFumigantPhrase);
	dosegeOfFumigantCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dosegeOfFumigantCell.setFixedHeight(18f);
	dosegeOfFumigantCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	dosegeOfFumigantCell.setPaddingLeft(6f);
	detailTable.addCell(dosegeOfFumigantCell);
	
	
	//Phrase dosageOfFumigantValuePhrase=new Phrase("9  Gms/MT",font10);
	Phrase dosageOfFumigantValuePhrase=null;
	if(fumigation.getDoseratefumigation()!=null){
		dosageOfFumigantValuePhrase=new Phrase(fumigation.getDoseratefumigation(),font10);
	}
	else{
		dosageOfFumigantValuePhrase=new Phrase(" ",font10);
	}
	
	PdfPCell dosageOfFumigantValueCell=new PdfPCell(dosageOfFumigantValuePhrase);
	dosageOfFumigantValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dosageOfFumigantValueCell.setFixedHeight(18f);
	dosageOfFumigantValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	dosageOfFumigantValueCell.setPaddingLeft(6f);
	detailTable.addCell(dosageOfFumigantValueCell);
	
	Phrase durationOfFumigantPhrase=new Phrase("Duration of Fumigation (in days) ",font10);
	PdfPCell durationOfFumigantCell=new PdfPCell(durationOfFumigantPhrase);
	durationOfFumigantCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	durationOfFumigantCell.setFixedHeight(18f);
	durationOfFumigantCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	durationOfFumigantCell.setPaddingLeft(6f);
	detailTable.addCell(durationOfFumigantCell);
	
	
	//Phrase durationOfFumigantValuePhrase=new Phrase("72 HRS",font10);
	Phrase durationOfFumigantValuePhrase=null;
	if(fumigation.getDurartionfumigation()!=null){
		durationOfFumigantValuePhrase=new Phrase(fumigation.getDurartionfumigation(),font10);
	}
	else{
		durationOfFumigantValuePhrase=new Phrase(" ",font10);
	}
	
	PdfPCell durationOfFumigantValueCell=new PdfPCell(durationOfFumigantValuePhrase);
	durationOfFumigantValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	durationOfFumigantValueCell.setFixedHeight(18f);
	durationOfFumigantValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	durationOfFumigantValueCell.setPaddingLeft(6f);
	detailTable.addCell(durationOfFumigantValueCell);
	
	
	Phrase averageAmbientPhrase=new Phrase("Average ambient humidity during fumigation(%) ",font10);
	PdfPCell averageAmbientCell=new PdfPCell(averageAmbientPhrase);
	averageAmbientCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	averageAmbientCell.setFixedHeight(18f);
	averageAmbientCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	averageAmbientCell.setPaddingLeft(6f);
	detailTable.addCell(averageAmbientCell);
	
	
	//Phrase averageAmbientValuePhrase=new Phrase(" ",font10);
	Phrase averageAmbientValuePhrase=null;
	if(fumigation.getMinairtemp()!=null){
		averageAmbientValuePhrase=new Phrase(fumigation.getMinairtemp(),font10);
	}
	else{
		averageAmbientValuePhrase=new Phrase(" ",font10);
	}
	
	PdfPCell averageAmbientValueCell=new PdfPCell(averageAmbientValuePhrase);
	averageAmbientValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	averageAmbientValueCell.setFixedHeight(18f);
	averageAmbientValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	averageAmbientValueCell.setPaddingLeft(6f);
	detailTable.addCell(averageAmbientValueCell);
	
	Phrase fumigationPerformedPhrase=new Phrase("Fumigation Performed under gastight sheets ",font10);
	PdfPCell fumigationPerformedCell=new PdfPCell(fumigationPerformedPhrase);
	fumigationPerformedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	fumigationPerformedCell.setFixedHeight(18f);
	fumigationPerformedCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	fumigationPerformedCell.setPaddingLeft(6f);
	detailTable.addCell(fumigationPerformedCell);
	
	
	//Phrase fumigationPerformedValuePhrase=new Phrase("",font10);
	Phrase fumigationPerformedValuePhrase=null;
	if(fumigation.getNote1()!=null){
		fumigationPerformedValuePhrase=new Phrase(fumigation.getNote1(),font10);
	}
	else{
		fumigationPerformedValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell fumigationPerformedValueCell=new PdfPCell(fumigationPerformedValuePhrase);
	fumigationPerformedValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	fumigationPerformedValueCell.setFixedHeight(18f);
	fumigationPerformedValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	fumigationPerformedValueCell.setPaddingLeft(6f);
	detailTable.addCell(fumigationPerformedValueCell);
	
	
	Phrase descriptionOfGoodPhrase=new Phrase("Description Of Goods",font10bold);
	PdfPCell descriptionOfGoodCell=new PdfPCell(descriptionOfGoodPhrase);
	descriptionOfGoodCell.setColspan(2);
	descriptionOfGoodCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	descriptionOfGoodCell.setFixedHeight(20f);
	descriptionOfGoodCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	descriptionOfGoodCell.setPaddingLeft(6f);
	detailTable.addCell(descriptionOfGoodCell);
	
	Phrase containerNumberPhrase=new Phrase("Container Number (or numerical link )/Seal Number",font10);
	PdfPCell  containerNumberCell=new PdfPCell(containerNumberPhrase);
	containerNumberCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	containerNumberCell.setFixedHeight(18f);
	containerNumberCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	containerNumberCell.setPaddingLeft(6f);
	detailTable.addCell(containerNumberCell);
	
	
	//Phrase containerNumberValuePhrase=new Phrase("MSKU 3779836",font10);
	
	Phrase containerNumberValuePhrase=null;
	if(fumigation.getConsignmentlink()!=null){
		containerNumberValuePhrase=new Phrase(fumigation.getConsignmentlink(),font10);
	}
	else{
		containerNumberValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell  containerNumberValueCell=new PdfPCell(containerNumberValuePhrase);
	containerNumberValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	containerNumberValueCell.setFixedHeight(18f);
	containerNumberValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	containerNumberValueCell.setPaddingLeft(6f);
	detailTable.addCell(containerNumberValueCell);
	
	
	Phrase nameOfExporterPhrase=new Phrase("Name & Address Of exporter",font10);
	PdfPCell  nameOfExporterCell=new PdfPCell(nameOfExporterPhrase);
	nameOfExporterCell.setRowspan(2);
	nameOfExporterCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameOfExporterCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameOfExporterCell.setPaddingLeft(6f);
	detailTable.addCell(nameOfExporterCell);
	
	
	//Phrase nameOfExporterValuePhrase=new Phrase("FALZANI EXPORTS PVT LTD                                         "
			//+ " "
	//+ "NIRMAL BUILDING ,21ST FLOOR.                                                       "
	//+ " "
  //  + "NARIMAN POINT,MUMBAI-400021,INDIA                                                   ",font10);
	Phrase nameOfExporterValuePhrase=null;
	if(fumigation.getFromCompanyname()!=null)
	{
		nameOfExporterValuePhrase=new Phrase(fumigation.getFromCompanyname(),font9);
	}
	else{
		nameOfExporterValuePhrase=new Phrase(" ",font9);
	}
	PdfPCell  nameOfExporterValueCell=new PdfPCell(nameOfExporterValuePhrase);
	nameOfExporterValueCell.setBorderWidthBottom(0);
	nameOfExporterValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameOfExporterValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameOfExporterValueCell.setPaddingLeft(6f);
	//nameOfExporterValueCell.setPaddingBottom(5);
	detailTable.addCell(nameOfExporterValueCell);
	
	//Phrase addOfExporterValuePhrase2=new Phrase("add")
	Phrase 	addOfExporterValuePhrase=null;
	if(fumigation.getFromaddress()!=null){
			addOfExporterValuePhrase=new Phrase(fumigation.getFromaddress().getCompleteAddress(),font10);
	}else{
			addOfExporterValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell  addOfExporterValueCell2=new PdfPCell(addOfExporterValuePhrase);
	addOfExporterValueCell2.setBorderWidthTop(0);
	addOfExporterValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
	addOfExporterValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	addOfExporterValueCell2.setPaddingLeft(6f);
	addOfExporterValueCell2.setPaddingBottom(5f);
	detailTable.addCell(addOfExporterValueCell2);
	
	Phrase nameOfConsigneePhrase=new Phrase("Name & Address Of consignee",font10);
	PdfPCell  nameOfConsigneeCell=new PdfPCell(nameOfConsigneePhrase);
	nameOfConsigneeCell.setRowspan(2);
	nameOfConsigneeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameOfConsigneeCell.setFixedHeight(18f);
	nameOfConsigneeCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameOfConsigneeCell.setPaddingLeft(6f);
	detailTable.addCell(nameOfConsigneeCell);
	
	
	//Phrase nameOfConsigneeValuePhrase=new Phrase("                                                  ",font10);
	Phrase nameOfConsigneeValuePhrase=null;
	if(fumigation.getToCompanyname()!=null)
	{
		nameOfConsigneeValuePhrase=new Phrase(fumigation.getToCompanyname(),font9);
	}
	else{
		nameOfConsigneeValuePhrase=new Phrase(" ",font9);
	}
	PdfPCell  nameOfConsigneeValueCell=new PdfPCell(nameOfConsigneeValuePhrase);
	nameOfConsigneeValueCell.setBorderWidthBottom(0);
	nameOfConsigneeValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameOfConsigneeValueCell.setFixedHeight(18f);
	nameOfConsigneeValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameOfConsigneeValueCell.setPaddingLeft(6f);
	detailTable.addCell(nameOfConsigneeValueCell);
	

	Phrase addOfConsigneeValuePhrase=null;
	if(fumigation.getTomaddress()!=null){
		addOfConsigneeValuePhrase=new Phrase(fumigation.getTomaddress().getCompleteAddress(),font10);
	}else{
		addOfConsigneeValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell  addOfConsigneeValueCell=new PdfPCell(addOfConsigneeValuePhrase);
	addOfConsigneeValueCell.setBorderWidthTop(0);
	addOfConsigneeValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	addOfConsigneeValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	addOfConsigneeValueCell.setPaddingLeft(6f);
	addOfConsigneeValueCell.setPaddingBottom(5f);
	detailTable.addCell(addOfConsigneeValueCell);
	
	Phrase typeOfCargoPhrase=new Phrase("Type and Description of cargo",font10);
	PdfPCell typeOfCargoCell=new PdfPCell(typeOfCargoPhrase);
	typeOfCargoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	typeOfCargoCell.setFixedHeight(18f);
	typeOfCargoCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	typeOfCargoCell.setPaddingLeft(6f);
	detailTable.addCell(typeOfCargoCell);
	
	
	Phrase typeOfCargoValuePhrase=new Phrase("",font10);
	PdfPCell typeOfCargoValueCell=new PdfPCell(typeOfCargoValuePhrase);
	typeOfCargoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	typeOfCargoValueCell.setFixedHeight(18f);
	typeOfCargoValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	typeOfCargoValueCell.setPaddingLeft(6f);
	detailTable.addCell(typeOfCargoValueCell);
	
	Phrase QuantityPhrase=new Phrase("Quantity (MTs)/No of packages /No of pieces",font10);
	PdfPCell  QuantityCell=new PdfPCell(QuantityPhrase);
	QuantityCell.setRowspan(3);
	QuantityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	QuantityCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	QuantityCell.setPaddingLeft(6f);
	detailTable.addCell(QuantityCell);
	
	
	//Phrase QuantityValuePhrase=new Phrase(" ",font10);
	Phrase QuantityValuePhrase=null;
	if(fumigation.getQuantitydeclare()!=null)
	{
		QuantityValuePhrase=new Phrase(fumigation.getQuantitydeclare(),font10);
	}
	else
	{
		QuantityValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell  QuantityValueCell=new PdfPCell(QuantityValuePhrase);
	QuantityValueCell.setBorderWidthBottom(0);
	QuantityValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	QuantityValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	QuantityValueCell.setPaddingLeft(6f);
	detailTable.addCell(QuantityValueCell);
	
	
	Phrase totalNetPhrase=null;
	if(fumigation.getTotalNetWeight()!=null)
	{
		totalNetPhrase=new Phrase("TOTAL NET WT.: "+fumigation.getTotalNetWeight(),font10);
	}
	else{
		totalNetPhrase=new Phrase(" ",font10);
	}
	PdfPCell  totalNeCell=new PdfPCell(totalNetPhrase);
	totalNeCell.setBorderWidthBottom(0);
	totalNeCell.setBorderWidthTop(0);
	totalNeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	totalNeCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	totalNeCell.setPaddingLeft(6f);
	detailTable.addCell(totalNeCell);
	
	Phrase grossWet=null;
	if(fumigation.getGrossWeight()!=null)
	{
		grossWet=new Phrase("TOTAL GROSS WT.: "+fumigation.getGrossWeight(),font10);
	}
	else{
		grossWet=new Phrase(" ",font10);
	}
	PdfPCell  grossWetCell=new PdfPCell(grossWet);
	//QuantityValueCell.setRowspan(3);
	grossWetCell.setBorderWidthTop(0);
	grossWetCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//QuantityValueCell.setFixedHeight(18f);
	grossWetCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	grossWetCell.setPaddingLeft(6f);
	grossWetCell.setPaddingBottom(5);
	detailTable.addCell(grossWetCell);
	
	Phrase descriptionofPackingPhrase=new Phrase("Description of packaging material",font10);
	PdfPCell descriptionofPackingCell=new PdfPCell(descriptionofPackingPhrase);
	descriptionofPackingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	descriptionofPackingCell.setFixedHeight(18f);
	descriptionofPackingCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	descriptionofPackingCell.setPaddingLeft(6f);
	detailTable.addCell(descriptionofPackingCell);
	
	
	//Phrase descriptionofPackingValuePhrase=new Phrase("  ",font10);
	Phrase descriptionofPackingValuePhrase=null;
	if(fumigation.getPackingValue()!=null)
	{
		descriptionofPackingValuePhrase=new Phrase(fumigation.getPackingValue(),font10);
	}
	else
	{
		descriptionofPackingValuePhrase=new Phrase(" ",font10);
	}
	
	PdfPCell descriptionofPackingValueCell=new PdfPCell(descriptionofPackingValuePhrase);
	descriptionofPackingValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	descriptionofPackingValueCell.setFixedHeight(18f);
	descriptionofPackingValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	descriptionofPackingValueCell.setPaddingLeft(6f);
	detailTable.addCell(descriptionofPackingValueCell);
	
	Phrase shippingMarkPhrase=new Phrase("Shipping mark or brand",font10);
	PdfPCell  shippingMarkCell=new PdfPCell(shippingMarkPhrase);
	shippingMarkCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	shippingMarkCell.setFixedHeight(18f);
	shippingMarkCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	shippingMarkCell.setPaddingLeft(6f);
	detailTable.addCell(shippingMarkCell);
	
	//Phrase shippingMarkValuePhrase=new Phrase("",font10);
	Phrase shippingMarkValuePhrase=null;
	if(fumigation.getDistiinguishingmarks()!=null)
	{
		shippingMarkValuePhrase=new Phrase(fumigation.getDistiinguishingmarks(),font10);
	}
	else
	{
		shippingMarkValuePhrase=new Phrase(" ",font10);
	}
	PdfPCell  shippingMarkValueCell=new PdfPCell(shippingMarkValuePhrase);
	shippingMarkValueCell.setFixedHeight(18f);
	shippingMarkValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	shippingMarkValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	shippingMarkValueCell.setPaddingLeft(6f);
	detailTable.addCell(shippingMarkValueCell);
	
	
	Phrase nameSignPhrase=new Phrase("Name & Signature of Accredited Fumigation                                                      "
			+ "Operator with seal& date /Accredition Number                                                                        ",font10);
	PdfPCell nameSignCell=new PdfPCell(nameSignPhrase);
	nameSignCell.setRowspan(3);
	nameSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameSignCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameSignCell.setPaddingLeft(6f);
	detailTable.addCell(nameSignCell);
	
	
	Phrase nameSignValuePhrase=new Phrase(" ",font10bold);
	PdfPCell nameSignValueCell=new PdfPCell(nameSignValuePhrase);
	nameSignValueCell.setRowspan(3);
	nameSignValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	nameSignValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	nameSignValueCell.setPaddingLeft(6f);
	detailTable.addCell(nameSignValueCell);
	
	
	Phrase endorsedByspecifiedOfficerPhrase=new Phrase("Endorsed by Specified Officer of Dte of PPQS                                                  "
			+ "(Applicable only in case of non-accredited                                                                  fumigation agency ) ",font10);
	PdfPCell endorsedByspecifiedOfficerCell=new PdfPCell(endorsedByspecifiedOfficerPhrase);
	endorsedByspecifiedOfficerCell.setRowspan(3);
	endorsedByspecifiedOfficerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	endorsedByspecifiedOfficerCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	endorsedByspecifiedOfficerCell.setPaddingLeft(6f);
	detailTable.addCell(endorsedByspecifiedOfficerCell);
	
	
	Phrase endorsedByspecifiedOfficerValuePhrase=new Phrase(" ",font10);
	PdfPCell endorsedByspecifiedOfficerValueCell=new PdfPCell(endorsedByspecifiedOfficerValuePhrase);
	endorsedByspecifiedOfficerValueCell.setRowspan(3);
	endorsedByspecifiedOfficerValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	endorsedByspecifiedOfficerValueCell.setFixedHeight(18f);
	endorsedByspecifiedOfficerValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	endorsedByspecifiedOfficerValueCell.setPaddingLeft(6f);
	detailTable.addCell(endorsedByspecifiedOfficerValueCell);
	
	
	try {
		document.add(detailTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	PdfPTable footerTable=new PdfPTable(2);
	detailTable.setWidthPercentage(100f);
	
	try {
		footerTable.setWidths(new float[] { 20,90});
	 } catch (DocumentException e1) {
		e1.printStackTrace();
	 }
	
	Phrase additionalDeclarationPhrase=new Phrase("Additional Declaration ",font10bold);
	PdfPCell additionalDeclarationCell=new PdfPCell(additionalDeclarationPhrase);
	additionalDeclarationCell.setBorder(0);
	additionalDeclarationCell.setPaddingLeft(5);
	additionalDeclarationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	additionalDeclarationCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	footerTable.addCell(additionalDeclarationCell);
	
	//Phrase additionalDeclarationValuePhrase=new Phrase("      Additional Declaration -",font10);
	
	Phrase additionalDeclarationValuePhrase=null;
	if(fumigation.getAdditionaldeclaration()!=null)
	{
		additionalDeclarationValuePhrase=new Phrase(fumigation.getAdditionaldeclaration(),font10);
	}
	else{
		additionalDeclarationValuePhrase=new Phrase(" ",font10bold);
	}
	PdfPCell additionalDeclarationValueCell=new PdfPCell(additionalDeclarationValuePhrase);
	additionalDeclarationValueCell.setBorder(0);
	additionalDeclarationValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	additionalDeclarationValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	footerTable.addCell(additionalDeclarationValueCell);
	
	
	
	Phrase additionalDeclarationPhrase2=new Phrase("Ref No. ",font10);
	PdfPCell additionalDeclarationCell2=new PdfPCell(additionalDeclarationPhrase2);
	additionalDeclarationCell2.setBorder(0);
	additionalDeclarationCell2.setPaddingLeft(5);
	additionalDeclarationCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
	additionalDeclarationCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	footerTable.addCell(additionalDeclarationCell2);
	
	
	//Phrase additionalDeclarationValuePhrase2=new Phrase("      Ref No. ",font10);
	
	Phrase additionalDeclarationValuePhrase2=null;
	if(fumigation.getInvoiceNo()!=null)
	{
		additionalDeclarationValuePhrase2=new Phrase("Custom Invoice No.  " +fumigation.getInvoiceNo(),font10bold);
	}
	else{
		additionalDeclarationValuePhrase2=new Phrase(" ",font10bold);
	}
	PdfPCell additionalDeclarationValueCell2=new PdfPCell(additionalDeclarationValuePhrase2);
	additionalDeclarationValueCell2.setBorder(0);
	additionalDeclarationValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
	additionalDeclarationValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
	footerTable.addCell(additionalDeclarationValueCell2);
	
	try {
		document.add(footerTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}



private void createCompanyNameAsHeader(Document doc, Company comp) {
	System.out.print("hiiiiiii");

	DocumentUpload document = comp.getUploadHeader();

	// patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System.getProperty("com.google.appengine.application.id");
		String version = System.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 725f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

private void createCompanyNameAsFooter(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadFooter();

	// patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System.getProperty("com.google.appengine.application.id");
		String version = System.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 40f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

private void createBlankforUPC() {
	System.out.print("jayshree");
	Phrase blankphrase = new Phrase("", font9);
	PdfPCell blankCell = new PdfPCell();
	blankCell.addElement(blankphrase);
	blankCell.setBorder(0);

	PdfPTable titlepdftable = new PdfPTable(3);
	titlepdftable.setWidthPercentage(100);
	titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
	titlepdftable.addCell(blankCell);
	titlepdftable.addCell(blankCell);

	Paragraph blank = new Paragraph();
	blank.add(Chunk.NEWLINE);

	PdfPTable parent = new PdfPTable(1);
	parent.setWidthPercentage(100);

	PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	titlePdfCell.setBorder(0);
	parent.addCell(titlePdfCell);

	try {
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(parent);
	} catch (DocumentException e) {
		e.printStackTrace();
	}

}
}



