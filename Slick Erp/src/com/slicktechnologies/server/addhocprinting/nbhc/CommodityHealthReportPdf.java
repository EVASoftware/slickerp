package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class CommodityHealthReportPdf {
	
	public Document document;
	Contract con;
	Company comp;
	Customer cust;
	Long serialNo;

	private Font font16boldul, font12bold, font8bold, font8, font9bold,font12Wbold,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,font10ul,font9boldul,
	font14bold,font9,font7,font7bold,font9red,font9boldred,font12boldred,font23;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	DecimalFormat df=new DecimalFormat("0.00");
	
	BaseColor darkGreyColor = WebColors.getRGBColor("#696969");
	BaseColor lightGreyColor = WebColors.getRGBColor("#D3D3D3");
	
	
	
	public CommodityHealthReportPdf() {
		super();
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		
	}
	
	public void setCHReport(Long count)
	{
		// Load Contract
		con = ofy().load().type(Contract.class).id(count).now();

		// load Company
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();

//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}	

	
public void createPdf() {
	
	if (comp.getUploadHeader() != null) {
		createCompanyNameAsHeader(document, comp);
	}
	if (comp.getUploadFooter() != null) {
		createCompanyNameAsFooter(document, comp);
	}
	createBlankforUPC();
	createCustomerDeatails();
    CreateCHRTbl();
    createRemarks();
    createSignature();
    createFooterTable();
   
}

private void createFooterTable() {
	
	PdfPTable table =new PdfPTable(1);
	table.setWidthPercentage(100f);
	table.setSpacingBefore(10f);
	
	Phrase insectCode1 = new Phrase ("Code for Insects : ",font9bold);
	
	PdfPCell codeInsect1 = new PdfPCell();
	codeInsect1.addElement(insectCode1);
	codeInsect1.setBorder(0);
	
	table.addCell(codeInsect1);
	
	try {
		document.add(table);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	PdfPTable footerTbl = new PdfPTable(6);
	footerTbl.setWidthPercentage(100);
	
	Phrase insectCode2 = new Phrase ("LGB - Lesser Grain Borer;",font8);
	Phrase insectCode3 = new Phrase ("CB - Cigaretter Beetle;",font8);
	Phrase insectCode4 = new Phrase ("KB - Khapra Beetle;",font8);
	Phrase insectCode5 = new Phrase ("TB - Tribolium;",font8);
	Phrase insectCode6 = new Phrase ("SI - Sitophilus;",font8);
	Phrase insectCode7 = new Phrase ("FGB-Grain Borer;",font8);
	Phrase insectCode8 = new Phrase ("BR - Bruchids;",font8);
	Phrase insectCode9 = new Phrase ("TE - Tenebroides;",font8);
	Phrase insectCode10 = new Phrase ("PS - Psocids;",font8);
	Phrase insectCode11 = new Phrase ("GM - Grain Moth;",font8);
	Phrase insectCode12 = new Phrase ("RM - Rice Moth;",font8);
	
	PdfPCell codeInsect2 = new PdfPCell();
	codeInsect2.addElement(insectCode2);
	codeInsect2.setBorder(0);
	
	PdfPCell codeInsect3 = new PdfPCell();
	codeInsect3.addElement(insectCode3);
	codeInsect3.setBorder(0);
	
	PdfPCell codeInsect4 = new PdfPCell();
	codeInsect4.addElement(insectCode4);
	codeInsect4.setBorder(0);
	
	PdfPCell codeInsect5 = new PdfPCell();
	codeInsect5.addElement(insectCode5);
	codeInsect5.setBorder(0);
	
	PdfPCell codeInsect6 = new PdfPCell();
	codeInsect6.addElement(insectCode6);
	codeInsect6.setBorder(0);
	
	PdfPCell codeInsect7 = new PdfPCell();
	codeInsect7.addElement(insectCode7);
	codeInsect7.setBorderWidthLeft(0);
	codeInsect7.setBorderWidthRight(0);
	codeInsect7.setBorderWidthTop(0);
	
	PdfPCell codeInsect8 = new PdfPCell();
	codeInsect8.addElement(insectCode8);  
	codeInsect8.setBorderWidthLeft(0);
	codeInsect8.setBorderWidthRight(0);
	codeInsect8.setBorderWidthTop(0);
	
	PdfPCell codeInsect9 = new PdfPCell();
	codeInsect9.addElement(insectCode9);
	codeInsect9.setBorderWidthLeft(0);
	codeInsect9.setBorderWidthRight(0);
	codeInsect9.setBorderWidthTop(0);
	
	PdfPCell codeInsect10 = new PdfPCell();
	codeInsect10.addElement(insectCode10);
	codeInsect10.setBorderWidthLeft(0);
	codeInsect10.setBorderWidthRight(0);
	codeInsect10.setBorderWidthTop(0);
	
	PdfPCell codeInsect11 = new PdfPCell();
	codeInsect11.addElement(insectCode11);
	codeInsect11.setBorderWidthLeft(0);
	codeInsect11.setBorderWidthRight(0);
	codeInsect11.setBorderWidthTop(0);

	
	PdfPCell codeInsect12 = new PdfPCell();
	codeInsect12.addElement(insectCode12);
	codeInsect12.setBorderWidthLeft(0);
	codeInsect12.setBorderWidthRight(0);
	codeInsect12.setBorderWidthTop(0);
	
	Phrase blnk =new Phrase("  ",font9);
	PdfPCell BlnkCell= new PdfPCell(blnk);
	BlnkCell.setBorder(0);
	  
//	footerTbl.addCell(BlnkCell);
//	footerTbl.addCell(BlnkCell);
//	footerTbl.addCell(BlnkCell);
//	footerTbl.addCell(BlnkCell);
//	footerTbl.addCell(BlnkCell);
	
	footerTbl.addCell(codeInsect2);
	footerTbl.addCell(codeInsect3);
	footerTbl.addCell(codeInsect4);
	footerTbl.addCell(codeInsect5);
	footerTbl.addCell(codeInsect6);
	footerTbl.addCell(BlnkCell);
	
	footerTbl.addCell(codeInsect7);
	footerTbl.addCell(codeInsect8);
	footerTbl.addCell(codeInsect9);
	footerTbl.addCell(codeInsect10);
	footerTbl.addCell(codeInsect11);
	footerTbl.addCell(codeInsect12);
	
	try {
		 footerTbl.setWidths(new float[] { 19,18,16,16,16,15 });
		document.add(footerTbl);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

private void createCompanyNameAsHeader(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadHeader();

	// patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System.getProperty("com.google.appengine.application.id");
		String version = System.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 725f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

private void createCompanyNameAsFooter(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadFooter();

	// patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System.getProperty("com.google.appengine.application.id");
		String version = System.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 40f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

private void createBlankforUPC() {
	Phrase blankphrase = new Phrase("", font9);
	PdfPCell blankCell = new PdfPCell();
	blankCell.addElement(blankphrase);
	blankCell.setBorder(0);

	PdfPTable titlepdftable = new PdfPTable(3);
	titlepdftable.setWidthPercentage(100);
	titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
	titlepdftable.addCell(blankCell);
	titlepdftable.addCell(blankCell);

	Paragraph blank = new Paragraph();
	blank.add(Chunk.NEWLINE);

	PdfPTable parent = new PdfPTable(1);
	parent.setWidthPercentage(100);

	PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	titlePdfCell.setBorder(0);
	parent.addCell(titlePdfCell);

	try {
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(parent);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}

private void createCustomerDeatails()
{
	 PdfPTable LabelTbl  =  new PdfPTable (1);
	 LabelTbl.setWidthPercentage(100);
//	 LabelTbl.setSpacingBefore(20f);
	 
	 Phrase commodityLbl = new Phrase("Commodity Health Report ",font12Wbold); 
	 
	 PdfPCell commodityLblCell = new PdfPCell(commodityLbl);
	 commodityLblCell.setBackgroundColor(darkGreyColor);
	 commodityLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 commodityLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 commodityLblCell.setFixedHeight(25f); 
	 LabelTbl.addCell(commodityLblCell); 
	 
	 try {
			document.add(LabelTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	 
	 PdfPTable customerDetailsTbl  =  new PdfPTable (6);
	 customerDetailsTbl.setWidthPercentage(100);
	 customerDetailsTbl.setSpacingBefore(4f);
	 try {
		 customerDetailsTbl.setWidths(new float[] { 10, 25, 15, 15,15, 20 });
	 } catch (DocumentException e1) {
		e1.printStackTrace();
	 }
	 // 1st ROW
	 
	 Phrase serialNoPh = new Phrase ("Serial No.",font9);
	 PdfPCell serialNoCell = new PdfPCell(serialNoPh);
//	 serialNoCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 serialNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 serialNoCell.setBorder(0);
//	 customerDetailsTbl.addCell(serialNoCell);
	 
	 Phrase serialNoVal = null;
//	 if (serialNo != null) {
	 serialNoVal = new Phrase(" " + con.getCount(), font9);
//	 } else {
//		serialNoVal = new Phrase(" ", font9);
//	 }
	 PdfPCell serialNoValCell = new PdfPCell(serialNoVal);
	 serialNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 serialNoValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 serialNoValCell.setBorderWidthLeft(0);
	 serialNoValCell.setBorderWidthRight(0);
	 serialNoValCell.setBorderWidthTop(0);
//	 customerDetailsTbl.addCell(serialNoValCell);  
	 
	 Phrase date = new Phrase("Date :" , font9);
	 PdfPCell dateCell = new PdfPCell(date);
	 dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 dateCell.setBorder(0);
//	 customerDetailsTbl.addCell(dateCell);
	 
	 Phrase DateVal= new Phrase(" "+fmt.format(new Date()),font9);
	 PdfPCell dateValCell = new PdfPCell(DateVal);
//	 dateValCell.addElement(DateVal);
	 dateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 dateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 dateValCell.setBorderWidthLeft(0);
	 dateValCell.setBorderWidthRight(0);
	 dateValCell.setBorderWidthTop(0);
//	 customerDetailsTbl.addCell(dateValCell);
	 
	 Phrase blnks = new Phrase("                             ",font9);
	 PdfPCell blnksCell = new PdfPCell();
	 blnksCell.addElement(blnks);
	 blnksCell.setBorder(0);
	 
	 
	 customerDetailsTbl.addCell(serialNoCell);
	 customerDetailsTbl.addCell(serialNoValCell);
	 customerDetailsTbl.addCell(blnksCell);
	 customerDetailsTbl.addCell(blnksCell);
	 customerDetailsTbl.addCell(dateCell);
	 customerDetailsTbl.addCell(dateValCell);
	 
	 try {
			document.add(customerDetailsTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	 

	 	 
	 //2nd ROW
	 
	 PdfPTable table1=new PdfPTable(2);
	 table1.setWidthPercentage(100f);
	 table1.setSpacingBefore(4f);
	 
	 try {
		 table1.setWidths(new float[] { 35, 65 });
	 } catch (DocumentException e1) {
		e1.printStackTrace();
	 }
	 
	 
	 Phrase wareHouse = new Phrase("Warehouse / Godown / Chamber / Code :" , font9);
	 PdfPCell wareHouseCell = new PdfPCell();
	 wareHouseCell.addElement(wareHouse);
	 wareHouseCell.setBorder(0);
	 table1.addCell(wareHouseCell);	 
	 
	 
	
	 Phrase wareHouseVal= new Phrase("",font9);
	 PdfPCell wareHouseValCell = new PdfPCell(wareHouseVal);
	 
	
	 wareHouseValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 wareHouseValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 wareHouseValCell.setBorderWidthLeft(0);
	 wareHouseValCell.setBorderWidthRight(0);
	 wareHouseValCell.setBorderWidthTop(0);
	 table1.addCell(wareHouseValCell);	 
	 
	 try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	 
	 //3rd ROW
	
	 PdfPTable table2=new PdfPTable(2);
	 table2.setWidthPercentage(100f);
	 table2.setSpacingBefore(4f);
	 
	 try {
		 table2.setWidths(new float[] { 10, 90 });
	 } catch (DocumentException e1) {
		e1.printStackTrace();
	 }
	 
	 Phrase location = new Phrase("Location :" , font9);
	 PdfPCell locationCell = new PdfPCell(location);
	 locationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 locationCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 locationCell.setBorder(0);
	 table2.addCell(locationCell);	
	 
	 Phrase locationVal= new Phrase(""+con.getBranch(),font9);
	 PdfPCell locationValCell = new PdfPCell(locationVal);
	 locationValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 locationValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 locationValCell.setBorderWidthLeft(0);
	 locationValCell.setBorderWidthRight(0);
	 locationValCell.setBorderWidthTop(0);
	 table2.addCell(locationValCell);	
	 
	 try {
			document.add(table2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	 
	 //4th ROW
	 
	 PdfPTable table3=new PdfPTable(2);
	 table3.setWidthPercentage(100f);
	 table3.setSpacingBefore(4f);
	 table3.setSpacingAfter(8f);
	 
	 try {
		 table3.setWidths(new float[] { 13, 87 });
	 } catch (DocumentException e1) {
		e1.printStackTrace();
	 }
	
	 Phrase custName = new Phrase("Customer(s) :" , font9);
	 PdfPCell custNameCell = new PdfPCell(custName);
	 custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 custNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE ) ;
	 custNameCell .setBorder(0);
	 table3.addCell(custNameCell);
	 
	 Phrase custNameVal= new Phrase(""+con.getCinfo().getFullName(),font9);
	 PdfPCell custNameValCell = new PdfPCell(custNameVal);
	 custNameValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 custNameValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	 custNameValCell.setBorderWidthLeft(0);
	 custNameValCell.setBorderWidthRight(0);
	 custNameValCell.setBorderWidthTop(0);
	 table3.addCell(custNameValCell);
	 
	 
	 try {
			document.add(table3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
    	 
}
private void CreateCHRTbl() {
	PdfPTable table=new PdfPTable(1);
	table.setWidthPercentage(100f);
	table.setSpacingAfter(4f);
	
	Phrase infestation = new Phrase("Details of Insect Infestation :" , font8bold);
	PdfPCell infestationCell = new PdfPCell(infestation);
	infestationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	infestationCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	infestationCell.setBorder(0);
	table.addCell(infestationCell);
	
	try {
		document.add(table);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	PdfPTable chrTbl = new PdfPTable (7); 
	chrTbl.setWidthPercentage(100);
	try {
		chrTbl.setWidths(new float[] { 14,14,14,15,15,14,14 });
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	 
	Phrase commodity = new Phrase ("Commodity",font9);
	PdfPCell commodityCell = new PdfPCell (commodity);
	commodityCell.setBackgroundColor(lightGreyColor);
	commodityCell.setRowspan(2);
	commodityCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	commodityCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	chrTbl.addCell(commodityCell);
	
	
	
	Phrase stackNo = new Phrase ("Stack No. ",font9);
	PdfPCell stackNoCell = new PdfPCell (stackNo);
	stackNoCell.setBackgroundColor(lightGreyColor);
	stackNoCell.setRowspan(2);
	stackNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	stackNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	chrTbl.addCell(stackNoCell);
	
	
	Phrase qntyInMT = new Phrase ("Quantity In MT ",font9);
	PdfPCell qntyInMTCell = new PdfPCell (qntyInMT);
	qntyInMTCell.setBackgroundColor(lightGreyColor);
	qntyInMTCell.setRowspan(2);
	qntyInMTCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	qntyInMTCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	chrTbl.addCell(qntyInMTCell);
	
	
	Phrase noOfBagsSampled = new Phrase ("Number Of Bags Sampled ",font9);
	PdfPCell noOfBagsSampledCell = new PdfPCell (noOfBagsSampled);
	noOfBagsSampledCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	noOfBagsSampledCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	noOfBagsSampledCell.setBackgroundColor(lightGreyColor);
	noOfBagsSampledCell.setRowspan(2);
	chrTbl.addCell(noOfBagsSampledCell);
	
	
	Phrase noOfAdultInsect1 = new Phrase ("Number of Adult Insects ",font9);
	PdfPCell noOfAdultInsect1Cell = new PdfPCell (noOfAdultInsect1);
	noOfAdultInsect1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	noOfAdultInsect1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	noOfAdultInsect1Cell.setColspan(2);
	noOfAdultInsect1Cell.setBackgroundColor(lightGreyColor);
	chrTbl.addCell(noOfAdultInsect1Cell);
	
	Phrase noOfAdultInsect2 = new Phrase ("Number of Adult Insects ",font9); 
	PdfPCell noOfAdultInsect2Cell = new PdfPCell (noOfAdultInsect2);
	noOfAdultInsect2Cell.setBackgroundColor(lightGreyColor);
	noOfAdultInsect2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	noOfAdultInsect2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	noOfAdultInsect2Cell.setColspan(2);
	chrTbl.addCell(noOfAdultInsect2Cell);
	
	
	
	Phrase typeOfInsectsFound = new Phrase ("Type of Insects Found ",font9); 
	PdfPCell typeOfInsectsFoundCell = new PdfPCell (typeOfInsectsFound);
	typeOfInsectsFoundCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	typeOfInsectsFoundCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	typeOfInsectsFoundCell.setRowspan(2);
	typeOfInsectsFoundCell.setBackgroundColor(lightGreyColor);
	chrTbl.addCell(typeOfInsectsFoundCell); 
	
	Phrase noOfAdultInsectVal1 = new Phrase ("Live",font9);   
	PdfPCell noOfAdultInsectval1Cell = new PdfPCell (noOfAdultInsectVal1);
	noOfAdultInsectval1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	noOfAdultInsectval1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	chrTbl.addCell(noOfAdultInsectval1Cell); 
	
	
	Phrase noOfAdultInsectVal2 = new Phrase ("Dead",font9);  
	PdfPCell noOfAdultInsectval2Cell = new PdfPCell (noOfAdultInsectVal2);
	noOfAdultInsectval2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	noOfAdultInsectval2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	chrTbl.addCell(noOfAdultInsectval2Cell); 
	
	
	
	for (int i =0 ; i<15 ; i++ )
	{
	  PdfPCell commodityValCell = new PdfPCell();	
	 Phrase commodityVal = new Phrase (" ",font9);
	 commodityValCell.addElement(commodityVal);
	 chrTbl.addCell(commodityValCell);	
	
	}
	
	for (int i =0 ; i<15 ; i++ )
	{
	  PdfPCell stackNoValCell = new PdfPCell();	
	 Phrase stackNoVal = new Phrase (" ",font9);
	 stackNoValCell.addElement(stackNoVal);
	 chrTbl.addCell(stackNoValCell);
	 
	}  
	
	for (int i =0 ; i<15 ; i++ )
	{
	  PdfPCell qntyInMTValCell = new PdfPCell();	
	  Phrase qntyInMTVal = new Phrase (" ",font9);
	  qntyInMTValCell.addElement(qntyInMTVal);
	  chrTbl.addCell(qntyInMTValCell);	
	}
	
	
	for (int i =0 ; i<15 ; i++ )
	{
	  PdfPCell noOfBagsSampledValCell = new PdfPCell();	
	 Phrase noOfBagsSampledVal = new Phrase (" ",font9);
	 noOfBagsSampledValCell.addElement(noOfBagsSampledVal);
	 chrTbl.addCell(noOfBagsSampledValCell);
	 
	}
	
	for (int i =0 ; i<15 ; i++ )
	{
	  PdfPCell noOfAdultInsect1ValCell = new PdfPCell();	
	 Phrase noOfAdultInsect1Val = new Phrase (" ",font9);
	 noOfAdultInsect1ValCell.addElement(noOfAdultInsect1Val);
	 chrTbl.addCell(noOfAdultInsect1ValCell);	
	
	}
	
	for (int i =0 ; i<15 ; i++ )
	{
	  PdfPCell noOfAdultInsect2ValCell = new PdfPCell();	
	 Phrase noOfAdultInsect2Val = new Phrase (" ",font9);
	 noOfAdultInsect2ValCell.addElement(noOfAdultInsect2Val);
	 chrTbl.addCell(noOfAdultInsect2ValCell);	
	 
	}
	
	for (int i =0 ; i<15 ; i++ )  
	{
	  PdfPCell typeOfInsectsFoundValCell = new PdfPCell();	
	  Phrase typeOfInsectsFoundVal = new Phrase (" ",font9);
	  typeOfInsectsFoundValCell.addElement(typeOfInsectsFoundVal);
	  chrTbl.addCell(typeOfInsectsFoundValCell);	
	
	}
	try {
		document.add(chrTbl);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
}

private void createRemarks() {
	
	PdfPTable remarkTbl = new PdfPTable(1);
	remarkTbl.setWidthPercentage(100);
	
	PdfPCell remarkCell = new PdfPCell ();   
	Phrase remark1 = new Phrase("Remarks : _______________________________________________________________________________________________ ",font9);
	Phrase remark2 = new Phrase("_______________________________________________________________________________________________________ ",font9);
	
                      
	Paragraph remarkPara = new Paragraph();
//	remarkPara.add(Chunk.NEWLINE);
	remarkPara.add(remark1);
	remarkPara.add(Chunk.NEWLINE);
	remarkPara.add(remark2);
	remarkPara.add(Chunk.NEWLINE);    
//	remarkPara.add(Chunk.NEWLINE);
	remarkCell.addElement(remarkPara);  
	remarkCell.setBorder(0);
	remarkTbl.addCell(remarkCell);
	remarkTbl.setSpacingAfter(15f);
	
//	PdfPCell remarkValCell = new PdfPCell ();
//	Phrase remarkVal = new Phrase(" ",font9);
//	remarkValCell.addElement(remarkVal);
//	remarkValCell.setBorder(0);
//	remarkTbl.addCell(remarkValCell);
	
	try {
		document.add(remarkTbl);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
	private void createSignature() {
	
		
		PdfPTable representiveTbl = new PdfPTable (2);
		representiveTbl.setWidthPercentage(100);
		
		PdfPCell nbhcRepresentiveCell = new PdfPCell ();
		Phrase nbhcrepresentrive1 = new Phrase ("  Signature and Date",font9);
		Phrase nbhcrepresentrive2 = new Phrase ("  Name",font9);
		nbhcRepresentiveCell.addElement(nbhcrepresentrive1);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
//		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		nbhcRepresentiveCell.addElement(nbhcrepresentrive2);
//		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		nbhcRepresentiveCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		

		PdfPCell custRepresentiveCell = new PdfPCell ();
		Phrase custRepresentive1 = new Phrase ("  Signature and Date",font9);
		Phrase custRepresentive2 = new Phrase ("  Name",font9);
		custRepresentiveCell.addElement(custRepresentive1);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
//		custRepresentiveCell.addElement(Chunk.NEWLINE);
		custRepresentiveCell.addElement(custRepresentive2);
//		custRepresentiveCell.addElement(Chunk.NEWLINE);
		custRepresentiveCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		
		Phrase nbhcrepresentriveLbl = new Phrase (" NBHC Representive ",font9);
		PdfPCell nbhcRepresentiveLblCell = new PdfPCell (nbhcrepresentriveLbl);
		nbhcRepresentiveLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		nbhcRepresentiveLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		nbhcRepresentiveLblCell.setBackgroundColor(lightGreyColor);
		nbhcRepresentiveLblCell.setFixedHeight(20f);
		
		Phrase custRepresentiveLbl = new Phrase (" Customer Representive ",font9);
		PdfPCell custRepresentiveLblCell = new PdfPCell (custRepresentiveLbl);
		custRepresentiveLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		custRepresentiveLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		custRepresentiveLblCell.setBackgroundColor(lightGreyColor);
		custRepresentiveLblCell.setFixedHeight(20f);
		
		representiveTbl.addCell(nbhcRepresentiveCell);
		representiveTbl.addCell(custRepresentiveCell); 
		representiveTbl.addCell(nbhcRepresentiveLblCell);
		representiveTbl.addCell(custRepresentiveLblCell);
		
		
		try {
			document.add(representiveTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


}
