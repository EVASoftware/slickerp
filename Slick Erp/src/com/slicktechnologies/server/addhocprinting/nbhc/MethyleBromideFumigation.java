package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class MethyleBromideFumigation {
	public Document document;
	
	Customer cust;
	Company comp;
	Fumigation fumigation;
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,font12Wbold,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,font10ul,font9boldul,
	font14bold,font9,font7,font7bold,font9red,font9boldred,font12boldred,font23;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
	 SimpleDateFormat fmt2 = new  SimpleDateFormat("dd.MM.yyyy");
	
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	public  MethyleBromideFumigation() {
		super();
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
	}
	
	
	public void LoadData(Long count) {
		fumigation=ofy().load().type(Fumigation.class).id(count).now();
		
		comp =ofy().load().type(Company.class).filter("companyId", fumigation.getCompanyId()).first().now();
		
	}


	public void createMethyleBromideFumigationPdf() {
		if (comp.getUploadHeader() != null) {
			createCompanyNameAsHeader(document, comp);
		}
		if (comp.getUploadFooter() != null) {
			createCompanyNameAsFooter(document, comp);
		}
		createBlankforUPC();
		createMethyleHeaderTable();
		createfumigationDetailTable();
	}
	
	private void createMethyleHeaderTable() {
		// TODO Auto-generated method stub
		
		PdfPTable headertable=new PdfPTable(1);
		headertable.setWidthPercentage(100f);
		headertable.setSpacingBefore(50f);
		
		Phrase headPh=new Phrase("Fumigation Certificate",font10bold);
		PdfPCell headCell=new PdfPCell(headPh);
		//headCell.addElement(headPh);
		//headCell.setBorder(0);
		headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headCell.setFixedHeight(18f);
		headCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		headertable.addCell(headCell);
		
		
			try {
				document.add(headertable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PdfPTable companyName=new PdfPTable(2);
			companyName.setWidthPercentage(100f);
			
			try {
				companyName.setWidths(new float[] { 50,50});
			 } catch (DocumentException e1) {
				e1.printStackTrace();
			 }
			
			
//			//Phrase companyNamePhrase=new Phrase("National Bulk Handling Corporation Pvt Ltd.",font10bold);
			Phrase companyNamePhrase=null;
			if(comp.getBusinessUnitName()!=null){
				companyNamePhrase=new Phrase(comp.getBusinessUnitName(),font10bold);
			}else{
				companyNamePhrase=new Phrase(" ",font10bold);
			}
			
			PdfPCell companyNameCell=new PdfPCell(companyNamePhrase);
			companyNameCell.setBorderWidthBottom(0);
			companyNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			companyNameCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			companyNameCell.setPaddingLeft(6f);
			companyName.addCell(companyNameCell);
			
			Phrase treatmentCertificationnumberphrase=new Phrase("Treatment Certificate Number ",font10bold);
			PdfPCell treatmentCertificationnumberCell=new PdfPCell(treatmentCertificationnumberphrase);
			treatmentCertificationnumberCell.setBorderWidthBottom(0);
			treatmentCertificationnumberCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treatmentCertificationnumberCell.setVerticalAlignment(Element.ALIGN_CENTER);
			treatmentCertificationnumberCell.setPaddingLeft(6f);
			companyName.addCell(treatmentCertificationnumberCell);
			
			
//			Phrase companyAddPhrase=new Phrase("Ground Floor ,Plot No .10,                                                "
//					+ "Sector 26,Vashi,                                                                                   "
//					+ "Navi Mumbai-400705                                                            "
//					+ "Maharastra  India",font10);
			
			
			Phrase companyAddPhrase=null;
			if(comp.getAddress()!=null){
				companyAddPhrase=new Phrase(comp.getAddress().getCompleteAddress(),font10);
			}else{
				companyAddPhrase=new Phrase(" ",font10);
			}
			PdfPCell companyAddCell=new PdfPCell(companyAddPhrase);
			companyAddCell.setBorderWidthTop(0);
			companyAddCell.setBorderWidthBottom(0);
			companyAddCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			companyAddCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			companyAddCell.setPaddingLeft(6f);
			companyName.addCell(companyAddCell);
			
			
			Phrase treatmentCertificationnumberValuephrase=null;
			if(fumigation.getCertificateNo()!=null){
				treatmentCertificationnumberValuephrase=new Phrase(fumigation.getCertificateNo(),font10bold);
			}else{
				treatmentCertificationnumberValuephrase=new Phrase(" ",font10);
			}
			PdfPCell treatmentCertificationnumberValueCell=new PdfPCell(treatmentCertificationnumberValuephrase);
			treatmentCertificationnumberValueCell.setBorderWidthTop(0);
			treatmentCertificationnumberValueCell.setBorderWidthBottom(0);
			treatmentCertificationnumberValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treatmentCertificationnumberValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
			treatmentCertificationnumberValueCell.setPaddingLeft(6f);
			companyName.addCell(treatmentCertificationnumberValueCell);
			
			Phrase datePQRSPhrase=new Phrase("(Dte PQRS Reg.No.169/MB Dt.23.11.2006)",font10bold);
			PdfPCell datePQRSCell=new PdfPCell(datePQRSPhrase);
			//headCell.addElement(headPh);
			//headCell.setBorder(0);
			datePQRSCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			//datePQRSCell.setFixedHeight(18f);
			datePQRSCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			//datePQRSCell.setBorderWidthLeft(1);
			//datePQRSCell.setBorderWidthBottom(1);
			//datePQRSCell.setBorderWidthRight(0);
			datePQRSCell.setBorderWidthTop(0);
			datePQRSCell.setPaddingLeft(6f);
			companyName.addCell(datePQRSCell);
			
			
			
			
			
			//Phrase dateOfIssue=new Phrase("Date Of Issue: 17.12.2016",font10bold);
			
			Phrase dateOfIssuevalue=null;
			if(fumigation.getDateofissue()!=null){
				dateOfIssuevalue=new Phrase("Date Of Issue: " +fmt2.format(fumigation.getDateofissue()),font10bold);
			}else{
				dateOfIssuevalue=new Phrase("Date Of Issue: ",font10);
			}
			
			PdfPCell dateOfIssuevaluCell=new PdfPCell(dateOfIssuevalue);
			//headCell.addElement(headPh);
			//headCell.setBorder(0);
			dateOfIssuevaluCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			dateOfIssuevaluCell.setFixedHeight(18f);
			dateOfIssuevaluCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
			dateOfIssuevaluCell.setPaddingLeft(6f);
			companyName.addCell(dateOfIssuevalue);
			
			
			try {
				document.add(companyName);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	private void createfumigationDetailTable() {
		// TODO Auto-generated method stub
		
		PdfPTable fumigationdetailTable=new PdfPTable(2);
		fumigationdetailTable.setWidthPercentage(100f);
		
		try {
			fumigationdetailTable.setWidths(new float[] { 50,50});
		 } catch (DocumentException e1) {
			e1.printStackTrace();
		 }
		
		
		Phrase infoPhrase=new Phrase("This is to certify that the goods described below were treated in accordance with the fumigation "
		+ "treatment requirment of importing country (CHICAGO,USA)and declared that the consignment has been verified free of"
		+ "impervious surfaces/layers such as plastic wrapping or laminated plastic films,lacquered or painted surfaces,Aluminium foil,tarred or "
		+ "waxed paper etc.that may adversely effect the penetration "
		+ "of the fumigant,prior to fumigation.   ",font8);
		
		
		PdfPCell infoPhraseCell=new PdfPCell(infoPhrase);
		//headCell.addElement(headPh);
		//headCell.setBorder(0);
		infoPhraseCell.setColspan(2);
		infoPhraseCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		//infoPhraseCell.setFixedHeight(18f);
		infoPhraseCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		infoPhraseCell.setPaddingLeft(6f);
		fumigationdetailTable.addCell(infoPhraseCell);
		
		
		Phrase DetailOfFumigationTreatmentPhrase=new Phrase("Details Of Treatment",font10bold);
		PdfPCell DetailOfFumigationTreatmentCell=new PdfPCell(DetailOfFumigationTreatmentPhrase);
		DetailOfFumigationTreatmentCell.setColspan(2);
		DetailOfFumigationTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		DetailOfFumigationTreatmentCell.setFixedHeight(20f);
		DetailOfFumigationTreatmentCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		DetailOfFumigationTreatmentCell.setPaddingLeft(6f);
		fumigationdetailTable.addCell(DetailOfFumigationTreatmentCell);
		
		Phrase nameOfFumigantPhrase2=new Phrase("Name of Fumigant",font10);
		PdfPCell nameOfFumigantCell2=new PdfPCell(nameOfFumigantPhrase2);
		nameOfFumigantCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameOfFumigantCell2.setFixedHeight(18f);
		nameOfFumigantCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		nameOfFumigantCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(nameOfFumigantCell2);
		
		
		//Phrase nameOfFumigantValuePhrase2=new Phrase(fumigation.getNameoffumigation(),font10);
		Phrase nameOfFumigantValuePhrase2=null;
		if(fumigation.getNameoffumigation()!=null){
			nameOfFumigantValuePhrase2=new Phrase(fumigation.getNameoffumigation(),font10);
		}else{
			nameOfFumigantValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell nameOfFumigantvalueCell2=new PdfPCell(nameOfFumigantValuePhrase2);
		nameOfFumigantvalueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameOfFumigantvalueCell2.setFixedHeight(18f);
		nameOfFumigantvalueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		nameOfFumigantvalueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(nameOfFumigantvalueCell2);
		
		
		Phrase dateOfFumigantPhrase2=new Phrase("Date of Fumigation",font10);
		PdfPCell dateOfFumigantCell2=new PdfPCell(dateOfFumigantPhrase2);
		dateOfFumigantCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateOfFumigantCell2.setFixedHeight(18f);
		dateOfFumigantCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		dateOfFumigantCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(dateOfFumigantCell2);
		
		

		//Phrase dateOfFumigantValuePhrase2=new Phrase(" 12/12/2016",font10);
		Phrase dateOfFumigantValuePhrase2=null;
		if(fumigation.getDateoffumigation()!=null){
			dateOfFumigantValuePhrase2=new Phrase( fmt.format(fumigation.getDateoffumigation()),font10);
		}else{
			dateOfFumigantValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell dateOfFumigantValueCell2=new PdfPCell(dateOfFumigantValuePhrase2);
		dateOfFumigantValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateOfFumigantValueCell2.setFixedHeight(18f);
		dateOfFumigantValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		dateOfFumigantValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(dateOfFumigantValueCell2);
		
		
		Phrase placeOfFumigantPhrase2=new Phrase("Place of Fumigation",font10);
		PdfPCell placeOfFumigantCell2=new PdfPCell(placeOfFumigantPhrase2);
		placeOfFumigantCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		placeOfFumigantCell2.setFixedHeight(18f);
		placeOfFumigantCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		placeOfFumigantCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(placeOfFumigantCell2);
		
		
		//Phrase placeOfFumigantValuePhrase2=new Phrase("VIRGINIA MHAPE ,NAVI MUMBAI",font10);
		Phrase placeOfFumigantValuePhrase2=null;
		if(fumigation.getPlaceoffumigation()!=null){
			placeOfFumigantValuePhrase2=new Phrase(fumigation.getPlaceoffumigation(),font10);
		}else{
			placeOfFumigantValuePhrase2=new Phrase(" ",font10);
		}
		
		PdfPCell placeOfFumigantValueCell2=new PdfPCell(placeOfFumigantValuePhrase2);
		placeOfFumigantValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		placeOfFumigantValueCell2.setFixedHeight(18f);
		placeOfFumigantValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		placeOfFumigantValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(placeOfFumigantValueCell2);
		
		
		Phrase dosegeOfFumigantPhrase2=new Phrase("Dosage of Fumigant(g/m3)",font10);
		PdfPCell dosegeOfFumigantCell2=new PdfPCell(dosegeOfFumigantPhrase2);
		dosegeOfFumigantCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		dosegeOfFumigantCell2.setFixedHeight(18f);
		dosegeOfFumigantCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		dosegeOfFumigantCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(dosegeOfFumigantCell2);
		
		//Phrase dosageOfFumigantValuePhrase2=new Phrase("56g/m3",font10);
		
		Phrase dosageOfFumigantValuePhrase2=null;
		if(fumigation.getDoseratefumigation()!=null){
			dosageOfFumigantValuePhrase2=new Phrase(fumigation.getDoseratefumigation(),font10);
		}else{
			dosageOfFumigantValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell dosageOfFumigantValueCell2=new PdfPCell(dosageOfFumigantValuePhrase2);
		dosageOfFumigantValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		dosageOfFumigantValueCell2.setFixedHeight(18f);
		dosageOfFumigantValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		dosageOfFumigantValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(dosageOfFumigantValueCell2);
		
		Phrase durationOfFumigantPhrase2=new Phrase("Duration of Fumigation (hours) ",font10);
		PdfPCell durationOfFumigantCell2=new PdfPCell(durationOfFumigantPhrase2);
		durationOfFumigantCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		durationOfFumigantCell2.setFixedHeight(18f);
		durationOfFumigantCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		durationOfFumigantCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(durationOfFumigantCell2);
		
		//Phrase durationOfFumigantValuePhrase2=new Phrase("24 HRS",font10);
		
		Phrase durationOfFumigantValuePhrase2=null;
		if(fumigation.getDurartionfumigation()!=null){
			durationOfFumigantValuePhrase2=new Phrase(fumigation.getDurartionfumigation(),font10);
		}else{
			durationOfFumigantValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell durationOfFumigantValueCell2=new PdfPCell(durationOfFumigantValuePhrase2);
		durationOfFumigantValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		durationOfFumigantValueCell2.setFixedHeight(18f);
		durationOfFumigantValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		durationOfFumigantValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(durationOfFumigantValueCell2);

		
		Phrase averageAmbientPhrase2=new Phrase("Average ambient temperature during fumigation(DC) ",font10);
		PdfPCell averageAmbientCell2=new PdfPCell(averageAmbientPhrase2);
		averageAmbientCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		averageAmbientCell2.setFixedHeight(18f);
		averageAmbientCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		averageAmbientCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(averageAmbientCell2);
		
		
		//Phrase averageAmbientValuePhrase2=new Phrase("16 Degree Celsius",font10);
		
		Phrase averageAmbientValuePhrase2=null;
		if(fumigation.getMinairtemp()!=null){
		averageAmbientValuePhrase2=new Phrase(fumigation.getMinairtemp(),font10);
		}
		else{
		averageAmbientValuePhrase2=new Phrase(" ",font10);
		}
		
		PdfPCell averageAmbientValueCell2=new PdfPCell(averageAmbientValuePhrase2);
		averageAmbientValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		averageAmbientValueCell2.setFixedHeight(18f);
		averageAmbientValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		averageAmbientValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(averageAmbientValueCell2);
		
		
		Phrase fumigationPerformedPhrase2=new Phrase("Fumigation Performed under gastight sheets ",font10);
		PdfPCell fumigationPerformedCell2=new PdfPCell(fumigationPerformedPhrase2);
		fumigationPerformedCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		fumigationPerformedCell2.setFixedHeight(18f);
		fumigationPerformedCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		fumigationPerformedCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(fumigationPerformedCell2);
		
		

		//Phrase fumigationPerformedValuePhrase2=new Phrase("",font10);

		Phrase fumigationPerformedValuePhrase2=null;
		if(fumigation.getNote1()!=null){
			fumigationPerformedValuePhrase2=new Phrase(fumigation.getNote1(),font9);
		}else{
			fumigationPerformedValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell fumigationPerformedValueCell2=new PdfPCell(fumigationPerformedValuePhrase2);
		fumigationPerformedValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		fumigationPerformedValueCell2.setFixedHeight(18f);
		fumigationPerformedValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		fumigationPerformedValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(fumigationPerformedValueCell2);
		
		Phrase gasTightsheetPhrase2=new Phrase("If Containers are not fumigated under gas-tight sheets,                                "
				+ "pressure decay value (from 200-100 pascal's)in Seconds  ",font10);
		PdfPCell gasTightsheetCell2=new PdfPCell(gasTightsheetPhrase2);
		gasTightsheetCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//fumigationPerformedCell2.setFixedHeight(18f);
		gasTightsheetCell2.setRowspan(2);
		gasTightsheetCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		gasTightsheetCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(gasTightsheetCell2);

		
		Phrase gasTightsheetValuePhrase2=new Phrase(" ",font10);
		
//		Phrase gasTightsheetValuePhrase2=null;
//		if(fumigation.getNote1()!=null){
//			gasTightsheetValuePhrase2=new Phrase(fumigation.getNote1(),font9);
//		}else{
//			gasTightsheetValuePhrase2=new Phrase(" ",font10);
//		}
		PdfPCell gasTightsheetValueCell2=new PdfPCell(gasTightsheetValuePhrase2);
		gasTightsheetValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//fumigationPerformedCell2.setFixedHeight(18f);
		gasTightsheetValueCell2.setRowspan(2);
		gasTightsheetValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		gasTightsheetValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(gasTightsheetValueCell2);
		
		
		Phrase descriptionOfGoodPhrase2=new Phrase("Description Of Goods",font10bold);
		PdfPCell descriptionOfGoodCell2=new PdfPCell(descriptionOfGoodPhrase2);
		descriptionOfGoodCell2.setColspan(2);
		descriptionOfGoodCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptionOfGoodCell2.setFixedHeight(20f);
		descriptionOfGoodCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		descriptionOfGoodCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(descriptionOfGoodCell2);
		
		
		Phrase containerNumberPhrase2=new Phrase("Container Number (or numerical link )/Seal Number",font10);
		PdfPCell  containerNumberCell2=new PdfPCell(containerNumberPhrase2);
		containerNumberCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		containerNumberCell2.setFixedHeight(18f);
		containerNumberCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		containerNumberCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(containerNumberCell2);
		
		
		//Phrase containerNumberValuePhrase2=new Phrase("N/A",font10);
		
		Phrase containerNumberValuePhrase2=null;
		if(fumigation.getConsignmentlink()!=null){
			containerNumberValuePhrase2=new Phrase(fumigation.getConsignmentlink(),font10);
		}else{
			containerNumberValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell  containerNumberValueCell2=new PdfPCell(containerNumberValuePhrase2);
		containerNumberValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		containerNumberValueCell2.setFixedHeight(18f);
		containerNumberValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		containerNumberValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(containerNumberValueCell2);
		
		Phrase nameOfExporterPhrase2=new Phrase("Name & Address Of exporter",font10);
		PdfPCell  nameOfExporterCell2=new PdfPCell(nameOfExporterPhrase2);
		nameOfExporterCell2.setRowspan(2);
		nameOfExporterCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//nameOfExporterCell.setFixedHeight(18f);
		nameOfExporterCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		nameOfExporterCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(nameOfExporterCell2);
		
//		Phrase nameOfExporterValuePhrase2=new Phrase("FALZANI EXPORTS PVT LTD                                         "
//				+ " "
//		+ "NIRMAL BUILDING ,21ST FLOOR.                                                       "
//		+ " "
//	    + "NARIMAN POINT,MUMBAI-400021,INDIA                                                   ",font10);
	    
	    Phrase nameOfExporterValuePhrase2=null;
		if(fumigation.getFromCompanyname()!=null){
			nameOfExporterValuePhrase2=new Phrase(fumigation.getFromCompanyname(),font9);
		}else{
			nameOfExporterValuePhrase2=new Phrase(" ",font9);
		}
		PdfPCell  nameOfExporterValueCell2=new PdfPCell(nameOfExporterValuePhrase2);
		//nameOfExporterValueCell2.setRowspan(3);
		nameOfExporterValueCell2.setBorderWidthBottom(0);
		nameOfExporterValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//nameOfExporterValueCell.setFixedHeight(18f);
		nameOfExporterValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		nameOfExporterValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(nameOfExporterValueCell2);
		
		//Phrase addOfExporterValuePhrase2=new Phrase("add")
		Phrase addOfExporterValuePhrase2=null;
		if(fumigation.getFromaddress()!=null){
			addOfExporterValuePhrase2=new Phrase(fumigation.getFromaddress().getCompleteAddress(),font10);
		}else{
			addOfExporterValuePhrase2=new Phrase(" ",font9);
		}
		PdfPCell  addOfExporterValueCell2=new PdfPCell(addOfExporterValuePhrase2);
		addOfExporterValueCell2.setBorderWidthTop(0);
		//nameOfExporterValueCell2.setRowspan(2);
		addOfExporterValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//nameOfExporterValueCell.setFixedHeight(18f);
		addOfExporterValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		addOfExporterValueCell2.setPaddingLeft(6f);
		addOfExporterValueCell2.setPaddingBottom(5);
		fumigationdetailTable.addCell(addOfExporterValueCell2);
		
		
		Phrase nameOfConsigneePhrase2=new Phrase("Name & Address Of consignee",font10);
		PdfPCell  nameOfConsigneeCell2=new PdfPCell(nameOfConsigneePhrase2);
		nameOfConsigneeCell2.setRowspan(2);
		nameOfConsigneeCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameOfConsigneeCell2.setFixedHeight(18f);
		nameOfConsigneeCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		nameOfConsigneeCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(nameOfConsigneeCell2);
		
		
		//Phrase nameOfConsigneeValuePhrase2=new Phrase("                                                  ",font10);
		 Phrase nameOfConsigneeValuePhrase2=null;
			if(fumigation.getToCompanyname()!=null ){
				nameOfConsigneeValuePhrase2=new Phrase(fumigation.getToCompanyname(),font9);
				
			}else{
				nameOfConsigneeValuePhrase2=new Phrase(" ",font9);
			}
		PdfPCell  nameOfConsigneeValueCell2=new PdfPCell(nameOfConsigneeValuePhrase2);
		//nameOfConsigneeValueCell2.setRowspan(3);
		nameOfConsigneeValueCell2.setBorderWidthBottom(0);
		nameOfConsigneeValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameOfConsigneeValueCell2.setFixedHeight(18f);
		nameOfConsigneeValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		nameOfConsigneeValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(nameOfConsigneeValueCell2);
		
		
		Phrase addOfConsigneeValuePhrase2=null;
		if(fumigation.getTomaddress()!=null){
			addOfConsigneeValuePhrase2=new Phrase(fumigation.getTomaddress().getCompleteAddress(),font10);
		}else{
			addOfConsigneeValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell  addOfConsigneeValueCell2=new PdfPCell(addOfConsigneeValuePhrase2);
		addOfConsigneeValueCell2.setBorderWidthTop(0);
		//addOfConsigneeValueCell2.setRowspan(2);
		addOfConsigneeValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//nameOfExporterValueCell.setFixedHeight(18f);
		addOfConsigneeValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		addOfConsigneeValueCell2.setPaddingLeft(6f);
		addOfConsigneeValueCell2.setPaddingBottom(5);
		fumigationdetailTable.addCell(addOfConsigneeValueCell2);
		
		
		Phrase typeOfCargoPhrase2=new Phrase("Type and Description of cargo",font10);
		PdfPCell typeOfCargoCell2=new PdfPCell(typeOfCargoPhrase2);
		typeOfCargoCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		typeOfCargoCell2.setFixedHeight(18f);
		typeOfCargoCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		typeOfCargoCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(typeOfCargoCell2);
		
		
		Phrase typeOfCargoValuePhrase2=new Phrase("",font10);
		PdfPCell typeOfCargoValueCell2=new PdfPCell(typeOfCargoValuePhrase2);
		typeOfCargoValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		typeOfCargoValueCell2.setFixedHeight(18f);
		typeOfCargoValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		typeOfCargoValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(typeOfCargoValueCell2);
		
		
		Phrase QuantityPhrase=new Phrase("Quantity (MTs)/No of packages /No of pieces",font10);
		PdfPCell  QuantityCell=new PdfPCell(QuantityPhrase);
		QuantityCell.setRowspan(3);
		QuantityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		//QuantityCell.setFixedHeight(18f);
		QuantityCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		QuantityCell.setPaddingLeft(6f);
		fumigationdetailTable.addCell(QuantityCell);
		
		
		//Phrase QuantityValuePhrase2=new Phrase("10PALLET",font10);
		Phrase QuantityValuePhrase2=null;
		if(fumigation.getQuantitydeclare()!=null){
			QuantityValuePhrase2=new Phrase(fumigation.getQuantitydeclare(),font10);
		}else{
			QuantityValuePhrase2=new Phrase(" ",font10);
		}
		
		PdfPCell  QuantityValueCell2=new PdfPCell(QuantityValuePhrase2);
		QuantityValueCell2.setRowspan(3);
		QuantityValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		QuantityValueCell2.setFixedHeight(18f);
		QuantityValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		QuantityValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(QuantityValueCell2);
		
		
		Phrase descriptionofPackingPhrase2=new Phrase("Description of packaging material",font10);
		PdfPCell descriptionofPackingCell2=new PdfPCell(descriptionofPackingPhrase2);
		descriptionofPackingCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptionofPackingCell2.setFixedHeight(18f);
		descriptionofPackingCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		descriptionofPackingCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(descriptionofPackingCell2);
		
		
		//Phrase descriptionofPackingValuePhrase2=new Phrase("AS PER B/L",font10);
		Phrase descriptionofPackingValuePhrase2=null;
		if(fumigation.getPackingValue()!=null){
			descriptionofPackingValuePhrase2=new Phrase(fumigation.getPackingValue(),font10);
		}else{
			descriptionofPackingValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell descriptionofPackingValueCell2=new PdfPCell(descriptionofPackingValuePhrase2);
		descriptionofPackingValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		descriptionofPackingValueCell2.setFixedHeight(18f);
		descriptionofPackingValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		descriptionofPackingValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(descriptionofPackingValueCell2);
		
		
		Phrase shippingMarkPhrase2=new Phrase("Shipping mark or brand",font10);
		PdfPCell  shippingMarkCell2=new PdfPCell(shippingMarkPhrase2);
		shippingMarkCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		shippingMarkCell2.setFixedHeight(18f);
		shippingMarkCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		shippingMarkCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(shippingMarkCell2);
		
		//Phrase shippingMarkValuePhrase2=new Phrase("AS PER B/L",font10);
		
		Phrase shippingMarkValuePhrase2=null;
		if(fumigation.getDistiinguishingmarks()!=null){
			shippingMarkValuePhrase2=new Phrase(fumigation.getDistiinguishingmarks(),font10);
		}else{
			shippingMarkValuePhrase2=new Phrase(" ",font10);
		}
		PdfPCell  shippingMarkValueCell2=new PdfPCell(shippingMarkValuePhrase2);
		shippingMarkValueCell2.setFixedHeight(18f);
		shippingMarkValueCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		shippingMarkValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		shippingMarkValueCell2.setPaddingLeft(6f);
		fumigationdetailTable.addCell(shippingMarkValueCell2);
		
		
		Phrase nameSignPhrase2=new Phrase("Name & Signature of Accredited Fumigation                                                      "
				+ "Operator with seal & date /Accredition Number                                                                        ",font10);
		PdfPCell nameSignCell2=new PdfPCell(nameSignPhrase2);
		nameSignCell2.setRowspan(6);
		nameSignCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//nameSignCell.setFixedHeight(18f);
		nameSignCell2.setVerticalAlignment(Element.ALIGN_CENTER);
		nameSignCell2.setPaddingLeft(6f);
		nameSignCell2.setPaddingTop(40f);
		nameSignCell2.setPaddingBottom(40f);
		fumigationdetailTable.addCell(nameSignCell2);
		
	
		Phrase nameSignValuePhrase2=new Phrase(" ");
//		Phrase nameSignValuePhrase2=null;
	//if(fumigation.!=null){
//			nameSignValuePhrase2=new Phrase(fumigation.getShippingBillNo(),font10);
//		}else{
//			nameSignValuePhrase2=new Phrase(" ",font10);
//		}
		PdfPCell nameSignValueCell2=new PdfPCell(nameSignValuePhrase2);
		nameSignValueCell2.setRowspan(6);
		nameSignValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//nameSignValueCell.setFixedHeight(18f);
		nameSignValueCell2.setVerticalAlignment(Element.ALIGN_CENTER);
		nameSignValueCell2.setPaddingLeft(20f);
		nameSignValueCell2.setPaddingRight(20f);
		nameSignCell2.setPaddingTop(40f);
		//nameSignCell2.setPaddingBottom(40f);
		fumigationdetailTable.addCell(nameSignValueCell2);
		
		
		Phrase additionalDeclarationPhrase=new Phrase("ADITIONAL DECLARATION: ",font10bold);
		PdfPCell additionalDeclarationCell=new PdfPCell(additionalDeclarationPhrase);
		//additionalDeclarationCell.setBorder(0);
		//additionalDeclarationCell.setColspan(2);
		additionalDeclarationCell.setPaddingLeft(6f);
		additionalDeclarationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		additionalDeclarationCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		additionalDeclarationCell.setFixedHeight(18f);
		fumigationdetailTable.addCell(additionalDeclarationCell);
		
		
		
		//Phrase additionalDeclarationValuePhrase=new Phrase("As per CUSTOM INVOICE NO.FEI-1617-174",font10);Phrase additionalDeclarationPhrase=null;
		Phrase additionalDeclarationValuePhrase=null;
		if(fumigation.getInvoiceNo()!=null){
			additionalDeclarationValuePhrase=new Phrase("As per CUSTOME INVOICE NO." +fumigation.getInvoiceNo(),font10);
		}else{
			additionalDeclarationValuePhrase=new Phrase(" ",font10);
		}
		
		PdfPCell additionalDeclarationValueCell=new PdfPCell(additionalDeclarationValuePhrase);
		//additionalDeclarationCell.setBorder(0);
		//additionalDeclarationCell.setColspan(2);
		additionalDeclarationValueCell.setPaddingLeft(6f);
		additionalDeclarationValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		additionalDeclarationValueCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		additionalDeclarationValueCell.setFixedHeight(18f);
		fumigationdetailTable.addCell(additionalDeclarationValueCell);
		
		
		Phrase additionalDeclarationPhrase2=new Phrase("As per ISPM15 the certificate is valid only if the goods described above are shipped within 21 days from the date of fumigation",font10);
		PdfPCell additionalDeclarationCell2=new PdfPCell(additionalDeclarationPhrase2);
		additionalDeclarationCell2.setBorder(0);
		additionalDeclarationCell2.setColspan(2);
		additionalDeclarationCell2.setPaddingLeft(20f);
		additionalDeclarationCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		additionalDeclarationCell2.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		fumigationdetailTable.addCell(additionalDeclarationCell2);
		
		try {
			document.add(fumigationdetailTable);
//			document.add(Chunk.NEXTPAGE);
//			timepass();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	
	
	
	
	
	
	
//	private void timepass() {
//		// TODO Auto-generated method stub
//		PdfPTable name=new PdfPTable(1);
//		name.setWidthPercentage(100f);
//		
//		java.util.List<String> namelist= new ArrayList<String>();
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		namelist.add("Rohan");
//		
//		
//		
//		for(int i=0;i<namelist.size();i++)
//		{
//			Phrase namePhrase=new Phrase(namelist.get(i),font9);
//			PdfPCell nameCell=new PdfPCell(namePhrase);
//			name.addCell(nameCell);
//		}
//		
//		try {
//			document.add(name);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//

	private void createCompanyNameAsHeader(Document doc, Company comp) {
		System.out.print("hiiiiiii");

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createBlankforUPC() {
		System.out.print("jayshree");
		Phrase blankphrase = new Phrase("", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
//		document.add(blank);
//			document.add(blank);
			document.add(blank);
			document.add(blank);
		    document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
