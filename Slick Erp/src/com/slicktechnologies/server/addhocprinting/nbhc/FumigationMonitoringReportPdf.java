package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Image;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;

public class FumigationMonitoringReportPdf {
	
	Contract contract;
	Company company;
	
	public Document document;
	private Font font16boldul, font12bold, font8bold,font9bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,fontG8,font10Wbold;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	BaseColor darkGreyColor = WebColors.getRGBColor("#696969");
	BaseColor lightGreyColor = WebColors.getRGBColor("#D3D3D3");
	
	float[] column6Widths = {0.8f,1.2f,0.7f,2.8f,0.5f,2.0f};
	float[] column2Widths = {0.9f,9.1f};
	float[] column4Widths = {1.5f,4.0f,1.5f,3.0f};
	float[] column7Widths = {1.3f,2.5f,0.9f,1.2f,1.1f,2.0f,1.0f};
	float[] column14Widths = {0.5f,1.1f,1.0f,1.0f,0.9f,0.5f,0.8f,0.5f,0.5f,0.5f,0.5f,0.5f,0.8f,0.9f};
	
	
	public FumigationMonitoringReportPdf() {
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9,Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		fontG8 = new Font(Font.FontFamily.HELVETICA, 8,Font.NORMAL,BaseColor.LIGHT_GRAY);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setFMR(Long id){
		Contract contract = ofy().load().type(Contract.class).id(id).now();
		this.contract=contract;
		
		if(contract!=null){
			Company company=ofy().load().type(Company.class).filter("companyId", contract.getCompanyId()).first().now();
			this.company=company;
		}
	}
	
	public void createPdf(){
		if (company.getUploadHeader() != null) {
			createCompanyNameAsHeader(document, company);
		}
		if (company.getUploadFooter() != null) {
			createCompanyNameAsFooter(document, company);
		}
		createBlankforUPC();
		createFmrHeadingTable();
		createFumigantDetailsTable();
		createFumigantMonitorReadingTable();
		
	}
	
	
	
	
	
	private void createFumigantMonitorReadingTable() {
		Phrase phrFmr=new Phrase("Fumigant Monitor Readings: ",font9bold);
		PdfPCell cellFmr=new PdfPCell(phrFmr);
		cellFmr.setBorder(0);
		
		Phrase phrFmrsr=new Phrase("Fumigant Monitor Serial No.:_________________",font9bold);
		PdfPCell cellFmrsr=new PdfPCell(phrFmrsr);
		cellFmrsr.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cellFmrsr.setBorder(0);
		
		PdfPTable table=new PdfPTable(2);
		table.setWidthPercentage(100);
		table.addCell(cellFmr);
		table.addCell(cellFmrsr);
		table.setSpacingBefore(30f);
		
		PdfPTable table1=new PdfPTable(14);
		table1.setWidthPercentage(100);
		try {
			table1.setWidths(column14Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table1.setSpacingBefore(10f);
		
		Phrase ph1=new Phrase("Serial No.",font8bold);
		Phrase ph2=new Phrase("Commodity",font8bold);
		Phrase ph3=new Phrase("Stack / Silo No.",font8bold);
		Phrase ph4=new Phrase("Date",font8bold);
		Phrase ph5=new Phrase("Temp (Celsius)",font8bold);
		Phrase ph6=new Phrase("RH (%)",font8bold);
		Phrase ph7=new Phrase("Time (In Hrs)",font8bold);
		Phrase ph8=new Phrase("Reading in ppm",font8bold);
		Phrase ph8_1=new Phrase(" 1 ",font8bold);
		Phrase ph8_2=new Phrase(" 2 ",font8bold);
		Phrase ph8_3=new Phrase(" 3 ",font8bold);
		Phrase ph8_4=new Phrase(" 4 ",font8bold);
		Phrase ph8_5=new Phrase(" 5 ",font8bold);
		Phrase ph9=new Phrase("Signatures",font8bold);
		Phrase ph9_1=new Phrase("NBHC",font8bold);
		Phrase ph9_2=new Phrase("Customer",font8bold);
		
		
		PdfPCell cell1=new PdfPCell(ph1);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_CENTER);
		cell1.setBackgroundColor(lightGreyColor);
		cell1.setRowspan(2);
		table1.addCell(cell1);
		
		PdfPCell cell2=new PdfPCell(ph2);
		cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell2.setVerticalAlignment(Element.ALIGN_CENTER);
		cell2.setBackgroundColor(lightGreyColor);
		cell2.setRowspan(2);
		table1.addCell(cell2);
		
		PdfPCell cell3=new PdfPCell(ph3);
		cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell3.setVerticalAlignment(Element.ALIGN_CENTER);
		cell3.setBackgroundColor(lightGreyColor);
		cell3.setRowspan(2);
		table1.addCell(cell3);
		
		PdfPCell cell4=new PdfPCell(ph4);
		cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell4.setVerticalAlignment(Element.ALIGN_CENTER);
		cell4.setBackgroundColor(lightGreyColor);
		cell4.setRowspan(2);
		table1.addCell(cell4);
		
		PdfPCell cell5=new PdfPCell(ph5);
		cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell5.setVerticalAlignment(Element.ALIGN_CENTER);
		cell5.setBackgroundColor(lightGreyColor);
		cell5.setRowspan(2);
		table1.addCell(cell5);
		
		PdfPCell cell6=new PdfPCell(ph6);
		cell6.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell6.setVerticalAlignment(Element.ALIGN_CENTER);
		cell6.setBackgroundColor(lightGreyColor);
		cell6.setRowspan(2);
		table1.addCell(cell6);
		
		PdfPCell cell7=new PdfPCell(ph7);
		cell7.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell7.setVerticalAlignment(Element.ALIGN_CENTER);
		cell7.setBackgroundColor(lightGreyColor);
		cell7.setRowspan(2);
		table1.addCell(cell7);
		
		PdfPCell cell8=new PdfPCell(ph8);
		cell8.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell8.setVerticalAlignment(Element.ALIGN_CENTER);
		cell8.setBackgroundColor(lightGreyColor);
		cell8.setColspan(5);
		table1.addCell(cell8);
		
		PdfPCell cell9=new PdfPCell(ph9);
		cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell9.setVerticalAlignment(Element.ALIGN_CENTER);
		cell9.setBackgroundColor(lightGreyColor);
		cell9.setColspan(2);
		table1.addCell(cell9);
		
		PdfPCell cell8_1=new PdfPCell(ph8_1);
		cell8_1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell8_1.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell8_1);
		
		PdfPCell cell8_2=new PdfPCell(ph8_2);
		cell8_2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell8_2.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell8_2);
		
		PdfPCell cell8_3=new PdfPCell(ph8_3);
		cell8_3.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell8_3.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell8_3);
		
		PdfPCell cell8_4=new PdfPCell(ph8_4);
		cell8_4.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell8_4.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell8_4);
		
		PdfPCell cell8_5=new PdfPCell(ph8_5);
		cell8_5.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell8_5.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell8_5);
		
		
		
		PdfPCell cell9_1=new PdfPCell(ph9_1);
		cell9_1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell9_1.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell9_1);
		
		PdfPCell cell9_2=new PdfPCell(ph9_2);
		cell9_2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell9_2.setVerticalAlignment(Element.ALIGN_CENTER);
		table1.addCell(cell9_2);
		
		Phrase blank=new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		
		try {
			document.add(table);
			document.add(table1);
			System.out.println("TABLE ADDED..!!!");
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createFumigantDetailsTable() {
		Phrase phrFum=new Phrase("Fumigant Details: ",font9bold);
		PdfPCell cellFum=new PdfPCell(phrFum);
		cellFum.setBorder(0);
		
		PdfPTable table=new PdfPTable(1);
		table.setWidthPercentage(100);
		table.addCell(cellFum);
		table.setSpacingBefore(10f);
		
		Image uncheckedImg=null;
		try {
			uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
		} catch (BadElementException | IOException e3) {
			e3.printStackTrace();
		}
		uncheckedImg.scalePercent(7);
		
		PdfPTable table1=new PdfPTable(7);
		table1.setWidthPercentage(100);
		try {
			table1.setWidths(column7Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table1.setSpacingBefore(10f);
		
		Phrase phrase1=new Phrase("Fumigant Used",font8bold);
		Phrase phrase2=new Phrase("Pack Type",font8bold);
		Phrase phrase3=new Phrase("No. Used",font8bold);
		Phrase phrase4=new Phrase("Qty. Used",font8bold);
		Phrase phrase5=new Phrase("Manufacturer",font8bold);
		Phrase phrase6=new Phrase("Date Of Manufacturer",font8bold);
		Phrase phrase7=new Phrase("Batch No.",font8bold);
		
		PdfPCell cell1=new PdfPCell(phrase1);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_CENTER);
		cell1.setBackgroundColor(lightGreyColor);
		cell1.setFixedHeight(25f);
		table1.addCell(cell1);
		
		PdfPCell cell2=new PdfPCell(phrase2);
		cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell2.setVerticalAlignment(Element.ALIGN_CENTER);
		cell2.setBackgroundColor(lightGreyColor);
		table1.addCell(cell2);
		
		PdfPCell cell3=new PdfPCell(phrase3);
		cell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell3.setVerticalAlignment(Element.ALIGN_CENTER);
		cell3.setBackgroundColor(lightGreyColor);
		table1.addCell(cell3);
		
		PdfPCell cell4=new PdfPCell(phrase4);
		cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell4.setVerticalAlignment(Element.ALIGN_CENTER);
		cell4.setBackgroundColor(lightGreyColor);
		table1.addCell(cell4);
		
		PdfPCell cell5=new PdfPCell(phrase5);
		cell5.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell5.setVerticalAlignment(Element.ALIGN_CENTER);
		cell5.setBackgroundColor(lightGreyColor);
		table1.addCell(cell5);
		
		PdfPCell cell6=new PdfPCell(phrase6);
		cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell6.setVerticalAlignment(Element.ALIGN_CENTER);
		cell6.setBackgroundColor(lightGreyColor);
		table1.addCell(cell6);
		
		PdfPCell cell7=new PdfPCell(phrase7);
		cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell7.setVerticalAlignment(Element.ALIGN_CENTER);
		cell7.setBackgroundColor(lightGreyColor);
		table1.addCell(cell7);
		
		
		//// 1st Row
		
		Phrase phrAlp=new Phrase("  ALP",font9);
		Paragraph paraAlp=new Paragraph();
		paraAlp.setIndentationLeft(10f);
		paraAlp.add(new Chunk(uncheckedImg, 0, 0, true));
		paraAlp.add(phrAlp);
		
		PdfPCell cellAlp=new PdfPCell();
		cellAlp.addElement(paraAlp);
		cellAlp.setBorderWidthBottom(0);
		
		
		
		Phrase phr3gm=new Phrase("  3gm Tablets",font9);
		Paragraph para3gm=new Paragraph();
		para3gm.setIndentationLeft(10f);
		para3gm.add(new Chunk(uncheckedImg, 0, 0, true));
		para3gm.add(phr3gm);
//		paraAlp.add(new Chunk("3gm Tablets", font8));
		
		PdfPCell cell3gm=new PdfPCell();
		cell3gm.addElement(para3gm);
		
		
		Phrase phr__gm=new Phrase("______gm",font9);
		PdfPCell cell_gm=new PdfPCell(phr__gm);
//		cell_gm.addElement(phr__gm);
		cell_gm.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell_gm.setVerticalAlignment(Element.ALIGN_CENTER);
		
		
		Phrase blank=new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
//		blankCell.setBackgroundColor(darkGreyColor);
//		blankCell.setBorder(1);
		
		Phrase blank1=new Phrase(" ");
		PdfPCell blankCell1=new PdfPCell();
		blankCell1.addElement(blank1);
		
		Phrase blank2=new Phrase(" ");
		PdfPCell blankCell2=new PdfPCell();
		blankCell2.addElement(blank2);
		
		Phrase blank3=new Phrase(" ");
		PdfPCell blankCell3=new PdfPCell();
		blankCell3.addElement(blank3);
		
		Phrase blank4=new Phrase(" ");
		PdfPCell blankCell4=new PdfPCell();
		blankCell4.addElement(blank4);
		
		Phrase blank5=new Phrase(" ");
		PdfPCell blankCell5=new PdfPCell();
		blankCell5.addElement(blank5);
		
		Phrase blank6=new Phrase(" ");
		PdfPCell blankCell6=new PdfPCell();
		blankCell6.addElement(blank6);
		
		Phrase blank7=new Phrase(" ");
		PdfPCell blankCell7=new PdfPCell();
		blankCell7.addElement(blank7);
		
		PdfPCell blankCellWNBT=new PdfPCell();
		blankCellWNBT.addElement(blank);
		blankCellWNBT.setBorderWidthBottom(0);
		
		table1.addCell(cellAlp);
		table1.addCell(cell3gm);
		table1.addCell(blankCell);
		table1.addCell(cell_gm);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		
//		table1.addCell(blankCell1);
//		table1.addCell(blankCell2);
//		table1.addCell(blankCell3);
//		table1.addCell(blankCell4);
//		table1.addCell(blankCell5);
//		table1.addCell(blankCell6);
//		table1.addCell(blankCell7);
		System.out.println("BLANK OKKKKKK");
		
		//// 2nd row
		
		
		
		PdfPCell blankCellWNB=new PdfPCell();
		blankCellWNB.addElement(blank);
		blankCellWNB.setBorderWidthBottom(0);
		blankCellWNB.setBorderWidthTop(0);
		
		Phrase phr10gm=new Phrase("  10gm Pouch",font9);
		Paragraph para10gm=new Paragraph();
		para10gm.setIndentationLeft(10f);
		para10gm.add(new Chunk(uncheckedImg, 0, 0, true));
		para10gm.add(phr10gm);
		
		PdfPCell cell10gm=new PdfPCell();
		cell10gm.addElement(para10gm);
		
		
		table1.addCell(blankCellWNB);
		table1.addCell(cell10gm);
		table1.addCell(blankCell);
		table1.addCell(cell_gm);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		
		/// 3rd row 
		
		PdfPCell blankCellWNT=new PdfPCell(blank);
		blankCellWNT.setBorderWidthTop(0);
		
		Phrase phr34gm=new Phrase("  34gm Pouch",font9);
		Paragraph para34gm=new Paragraph();
		para34gm.setIndentationLeft(10f);
		para34gm.add(new Chunk(uncheckedImg, 0, 0, true));
		para34gm.add(phr34gm);
		
		PdfPCell cell34gm=new PdfPCell();
		cell34gm.addElement(para34gm);
		
		table1.addCell(blankCellWNT);
		table1.addCell(cell34gm);
		table1.addCell(blankCell);
		table1.addCell(cell_gm);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		// 4th row
		
		Phrase phrMbr=new Phrase("  Mbr",font9);
		Paragraph paraMbr=new Paragraph();
		paraMbr.setIndentationLeft(10f);
		paraMbr.add(new Chunk(uncheckedImg, 0, 0, true));
		paraMbr.add(phrMbr);
		
		PdfPCell cellMbr=new PdfPCell();
		cellMbr.addElement(paraMbr);
		cellMbr.setBorderWidthBottom(0);
		
		Phrase phr1lb=new Phrase("  1 lb Can",font9);
		Paragraph para1lb=new Paragraph();
		para1lb.setIndentationLeft(10f);
		para1lb.add(new Chunk(uncheckedImg, 0, 0, true));
		para1lb.add(phr1lb);
		
		PdfPCell cell1lb=new PdfPCell();
		cell1lb.addElement(para1lb);
		
		table1.addCell(cellMbr);
		table1.addCell(cell1lb);
		table1.addCell(blankCell);
		table1.addCell(cell_gm);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		// 5 th row
		
		Phrase phr1_5lb=new Phrase("  1.5 lb Can",font9);
		Paragraph para1_5lb=new Paragraph();
		para1_5lb.setIndentationLeft(10f);
		para1_5lb.add(new Chunk(uncheckedImg, 0, 0, true));
		para1_5lb.add(phr1_5lb);
		
		PdfPCell cell1_5lb=new PdfPCell();
		cell1_5lb.addElement(para1_5lb);
		
		table1.addCell(blankCellWNB);
		table1.addCell(cell1_5lb);
		table1.addCell(blankCell);
		table1.addCell(cell_gm);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		//6th row
		
		Phrase phrCyl=new Phrase("  Cylinder",font9);
		Paragraph paraCyl=new Paragraph();
		paraCyl.setIndentationLeft(10f);
		paraCyl.add(new Chunk(uncheckedImg, 0, 0, true));
		paraCyl.add(phrCyl);
		
		PdfPCell cellCyl=new PdfPCell();
		cellCyl.addElement(paraCyl);
		
		table1.addCell(blankCellWNT);
		table1.addCell(cellCyl);
		table1.addCell(blankCell);
		table1.addCell(cell_gm);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		table1.addCell(blankCell);
		
		try {
			document.add(table);
			document.add(table1);
			System.out.println("TABLE ADDED..!!!");
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
	}

	private void createFmrHeadingTable() {
		PdfPTable topHeadingTable=new PdfPTable(1);
		topHeadingTable.setWidthPercentage(100);
		
		Phrase headingPhrase=new Phrase("Fumigant Monitoring Report",font10Wbold);
		PdfPCell headingCell=new PdfPCell(headingPhrase);
		headingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingCell.setVerticalAlignment(Element.ALIGN_CENTER);
		headingCell.setBackgroundColor(darkGreyColor);
		headingCell.setFixedHeight(25f);
		
		topHeadingTable.addCell(headingCell);
		
		try {
			document.add(topHeadingTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable table1=new PdfPTable(6);
		table1.setWidthPercentage(100);
		try {
			table1.setWidths(column6Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table1.setSpacingBefore(15f);
		
		Phrase phrase1=new Phrase("Serial No. ",font9);
		Phrase phrase2=new Phrase(" "+contract.getCount()+"",font9);
		Phrase phrase3=new Phrase(" Branch ",font9);
		Phrase phrase4=new Phrase(" "+contract.getBranch()+"",font9);
		Phrase phrase5=new Phrase(" Date ",font9);
		Phrase phrase6=new Phrase(" "+fmt.format(contract.getContractDate()),font9);
		
		PdfPCell cell1=new PdfPCell(phrase1);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_CENTER);
		cell1.setBorder(0);
		table1.addCell(cell1);
		
		PdfPCell cell2=new PdfPCell(phrase2);
		cell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell2.setVerticalAlignment(Element.ALIGN_CENTER);
		cell2.setBorderWidthLeft(0);
		cell2.setBorderWidthRight(0);
		cell2.setBorderWidthTop(0);
		table1.addCell(cell2);
		
		PdfPCell cell3=new PdfPCell(phrase3);
		cell3.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell3.setVerticalAlignment(Element.ALIGN_CENTER);
		cell3.setBorder(0);
		table1.addCell(cell3);
		
		PdfPCell cell4=new PdfPCell(phrase4);
		cell4.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell4.setVerticalAlignment(Element.ALIGN_CENTER);
		cell4.setBorderWidthLeft(0);
		cell4.setBorderWidthRight(0);
		cell4.setBorderWidthTop(0);
		table1.addCell(cell4);
		
		PdfPCell cell5=new PdfPCell(phrase5);
		cell5.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell5.setVerticalAlignment(Element.ALIGN_CENTER);
		cell5.setBorder(0);
		table1.addCell(cell5);
		
		PdfPCell cell6=new PdfPCell(phrase6);
		cell6.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell6.setVerticalAlignment(Element.ALIGN_CENTER);
		cell6.setBorderWidthLeft(0);
		cell6.setBorderWidthRight(0);
		cell6.setBorderWidthTop(0);
		table1.addCell(cell6);
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		PdfPTable table2=new PdfPTable(2);
		table2.setWidthPercentage(100);
		try {
			table2.setWidths(column2Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table2.setSpacingBefore(15f);
		
		Phrase phrase7=new Phrase("Customer ",font9);
		Phrase phrase8=new Phrase(" "+contract.getCinfo().getFullName()+"",font9);
		
		PdfPCell cell7=new PdfPCell(phrase7);
		cell7.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell7.setVerticalAlignment(Element.ALIGN_CENTER);
		cell7.setBorder(0);
		table2.addCell(cell7);
		
		PdfPCell cell8=new PdfPCell(phrase8);
		cell8.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell8.setVerticalAlignment(Element.ALIGN_CENTER);
		cell8.setBorderWidthLeft(0);
		cell8.setBorderWidthRight(0);
		cell8.setBorderWidthTop(0);
		table2.addCell(cell8);
		
		try {
			document.add(table2);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPTable table3=new PdfPTable(4);
		table3.setWidthPercentage(100);
		try {
			table3.setWidths(column4Widths);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		table3.setSpacingBefore(15f);
		
		Phrase phrase9=new Phrase("Warehouse/Code ",font9);
		Phrase phrase10=new Phrase(" Compartment ",font9);
		
		PdfPCell cell9=new PdfPCell(phrase9);
		cell9.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell9.setVerticalAlignment(Element.ALIGN_CENTER);
		cell9.setBorder(0);
		table3.addCell(cell9);
		
		PdfPCell blankCell=new PdfPCell();
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell.setVerticalAlignment(Element.ALIGN_CENTER);
		blankCell.setBorderWidthLeft(0);
		blankCell.setBorderWidthRight(0);
		blankCell.setBorderWidthTop(0);
		table3.addCell(blankCell);
		
		PdfPCell cell10=new PdfPCell(phrase10);
		cell10.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell10.setVerticalAlignment(Element.ALIGN_CENTER);
		cell10.setBorder(0);
		table3.addCell(cell10);
		
		table3.addCell(blankCell);
		table3.setSpacingAfter(15f);
		
		
		
		try {
			document.add(table3);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createBlankforUPC() {
		Phrase blankphrase = new Phrase("", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
