package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;


public class InvoiceAcknowledgementPdf {
	
	Company company;
	Long serialNo;

	Document document;

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font10boldul, font12, font16bold, font10, font10bold,
			font10boldred, font11bold, font14bold, font9, font7, font7bold,
			font9red, font9boldred, font12boldred, font16, font23, font20,font12Wbold;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df =new DecimalFormat("0.00");
	
	BaseColor darkGreyColor = WebColors.getRGBColor("#696969");
  
public InvoiceAcknowledgementPdf() {
	font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
	font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	font16 = new Font(Font.FontFamily.HELVETICA, 16);
	font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	font8 = new Font(Font.FontFamily.HELVETICA, 8);
	font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
	font12 = new Font(Font.FontFamily.HELVETICA, 12);
	font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
	font10 = new Font(Font.FontFamily.HELVETICA, 10);
	font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
	font9 = new Font(Font.FontFamily.HELVETICA, 9);
	font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
	font7 = new Font(Font.FontFamily.HELVETICA, 7);
	font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
	font23= new Font(Font.FontFamily.HELVETICA, 23);
	font20= new Font(Font.FontFamily.HELVETICA, 20);
	font11bold = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD);
	font10boldred = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD,BaseColor.RED);
	font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
	
	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
}  

	public void setInvoiceAck(Long count) {
		
		Company company=ofy().load().type(Company.class).filter("companyId", count).first().now();
		this.company=company;
		
		NumberGeneration numberGen=ofy().load().type(NumberGeneration.class).filter("companyId", count).filter("processName", "IA").filter("status", true).first().now();
		if(numberGen!=null){
			serialNo=numberGen.getNumber();
			numberGen.setNumber(serialNo+1);
			ofy().save().entity(numberGen).now();
		}
	} 
	
	public void createPdf() {
		if (company.getUploadHeader() != null) {
			createCompanyNameAsHeader(document, company);
		}
		if (company.getUploadFooter() != null) {
			createCompanyNameAsFooter(document, company);
		}
		createBlankforUPC();
		
		createHeader();
		createInvoiceListTbl();
		createFooter();
		
		
	}

	private void createHeader() {
		
		PdfPTable headerTbl = new PdfPTable (1);
		headerTbl.setWidthPercentage(100);
		
		
		Phrase header = new Phrase (" INVOICE ACKNOWLEDGMENT ",font12Wbold);
		PdfPCell headerCell = new PdfPCell (header);
//		headerCell.addElement(header);
		headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerCell.setBackgroundColor(darkGreyColor);
		headerCell.setFixedHeight(25f);
		headerTbl.addCell(headerCell);
		
		try {
			document.add(headerTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
  
	private void createInvoiceListTbl() {
		PdfPTable custDetailsTbl = new PdfPTable(4);
		custDetailsTbl.setWidthPercentage(100);
		try {
			custDetailsTbl.setWidths(new float[] { 10 , 50 , 20 , 10 });
		} catch (DocumentException e3) {
			e3.printStackTrace();
		}
		custDetailsTbl.setSpacingBefore(20f);
		
		Phrase srNo = new Phrase("Serial No :",font9);
		Phrase srNoVal =null;
		if(serialNo!=null){
			srNoVal = new Phrase(""+serialNo,font9);
		}else{
			srNoVal = new Phrase("",font9);
		}
		Phrase date = new Phrase("Date :",font9);
		Phrase dateVal = new Phrase(" "+fmt.format(new Date()),font9);
		
		Phrase custName = new Phrase("Customer Name : _______________________________________________________________",font9);
		Phrase custNameVal = new Phrase("",font9);
		
		Phrase blnks = new Phrase(" ",font9);
		
		PdfPCell srNoCell = new PdfPCell(srNo);
//		srNoCell.addElement(srNo);
		srNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoCell.setBorder(0);
		
		PdfPCell srNoValCell = new PdfPCell(srNoVal);
//		srNoValCell.addElement(srNoVal);
		srNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		srNoValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		srNoValCell.setBorder(0);
		
		PdfPCell dateCell = new PdfPCell(date);
//		Paragraph datePara = new Paragraph();
//		datePara.add(date);
//		datePara.setAlignment(Element.ALIGN_RIGHT);
//		dateCell.addElement(datePara);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		dateCell.setBorder(0);
		
		PdfPCell dateValCell = new PdfPCell(dateVal);
//		dateValCell.addElement(dateVal);
		dateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dateValCell.setBorder(0);
		
		PdfPCell custNameCell = new PdfPCell (custName);
//		custNameCell.addElement(custName);
		custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		custNameCell.setBorder(0);
		
		PdfPTable table=new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(20f);
		table.setSpacingBefore(10f);
		table.addCell(custNameCell);
		
//		PdfPCell custNameValCell = new PdfPCell ();
//		custNameValCell.addElement(custNameVal);
//		custNameValCell.setBorder(0);
//		
//		PdfPCell blnksCell = new PdfPCell ();
//		blnksCell.addElement(blnks);
//		blnksCell.setBorder(0);
		
		custDetailsTbl.addCell(srNoCell);
		custDetailsTbl.addCell(srNoValCell);
		custDetailsTbl.addCell(dateCell);
		custDetailsTbl.addCell(dateValCell);
		

//		custDetailsTbl.addCell(srNoCell);
//		custDetailsTbl.addCell(srNoValCell);
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(dateCell);
//		custDetailsTbl.addCell(dateValCell);
//		
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(blnksCell);
//		
//		custDetailsTbl.addCell(custNameCell);
//		custDetailsTbl.addCell(custNameValCell);
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(blnksCell);
//		custDetailsTbl.addCell(blnksCell);
		
		try {
			document.add(custDetailsTbl);
			document.add(table);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		
		PdfPTable instructionTbl = new PdfPTable(1);
		instructionTbl.setWidthPercentage(100);
		
		PdfPCell instructionCell = new PdfPCell ();
		Phrase instruction = new Phrase ("We have received following invoices towards NBHC's pest management services.",font8bold);
		Paragraph instructPara = new Paragraph();
		instructPara.add(instruction);
		instructPara.add(Chunk.NEWLINE);
		instructPara.add(Chunk.NEWLINE);
		instructionCell.addElement(instructPara);
		instructionCell.setBorder(0);
		instructionTbl.addCell(instructionCell);
		
		PdfPTable invoiceListTbl = new PdfPTable(4);
		invoiceListTbl.setWidthPercentage(100);
		
		PdfPCell invoiceNoCell = new PdfPCell();
		Phrase invoiceNo = new Phrase("Invoice Number ",font8bold);
		Paragraph invoiceNoPara = new  Paragraph();
		invoiceNoPara.add(invoiceNo);
		invoiceNoPara.setAlignment(Element.ALIGN_CENTER);
		invoiceNoCell.addElement(invoiceNoPara);
		
		
		PdfPCell invoiceDateCell = new PdfPCell();
		Phrase invoiceDate = new Phrase("Invoice Date ",font8bold);
		Paragraph invoiceDatePara = new  Paragraph();
		invoiceDatePara.add(invoiceDate);
		invoiceDatePara.setAlignment(Element.ALIGN_CENTER);
		invoiceDateCell.addElement(invoiceDatePara);
		
		PdfPCell invoiceAmountCell = new PdfPCell();
		Phrase invoiceAmount = new Phrase("Invoice Amount ",font8bold);
		Paragraph invoiceAmountPara = new  Paragraph();
		invoiceAmountPara.add(invoiceAmount);
		invoiceAmountPara.setAlignment(Element.ALIGN_CENTER);
		invoiceAmountCell.addElement(invoiceAmountPara);
		
		
		PdfPCell LinkedServiceRecordCell = new PdfPCell();
		Phrase LinkedServiceRecord = new Phrase("Linked Service records submitted",font8bold);
		Paragraph LinkedServiceRecordPara = new  Paragraph();
		LinkedServiceRecordPara.add(LinkedServiceRecord);
		LinkedServiceRecordPara.setAlignment(Element.ALIGN_CENTER);
		LinkedServiceRecordCell.addElement(LinkedServiceRecordPara);
		
		
		PdfPCell blnkCell  = new PdfPCell();
		Phrase blnk = new Phrase("  ",font9);
		Paragraph blnkPara = new  Paragraph();
		blnkPara.add(blnk);
		blnkPara.setAlignment(Element.ALIGN_CENTER);
		blnkCell.addElement(blnkPara);
		
		
		invoiceListTbl.addCell(invoiceNoCell);
		invoiceListTbl.addCell(invoiceDateCell);
		invoiceListTbl.addCell(invoiceAmountCell);
		invoiceListTbl.addCell(LinkedServiceRecordCell);
		
		for(int i = 0 ; i<40 ;i++)
			
		{
			invoiceListTbl.addCell(blnkCell);
		}
		
		try {
			invoiceListTbl.setWidths(new float[] { 20 , 15 , 20 , 45 });

		} catch (DocumentException e3) {
			e3.printStackTrace();
		}
		
		
		try {
			document.add(instructionTbl);
			document.add(invoiceListTbl);
		} 
		catch (DocumentException e)
		{
			
			e.printStackTrace();
			
		}
	}

	private void createFooter() {
		
		PdfPTable noteTbl = new PdfPTable(1);
		noteTbl.setWidthPercentage(100);
		
		PdfPCell noteCell = new PdfPCell ();
		Phrase note = new Phrase ("We confirm having avalled your services as shown in records of services and will pay your service changes within 7 days of receipt of invoices",font8bold);
		noteCell.addElement(note);
		noteCell.addElement(Chunk.NEWLINE);
		noteCell.setBorder(0);
		
		noteTbl.addCell(noteCell);
        
		PdfPTable footerTbl	 = new PdfPTable(4); 
		footerTbl.setWidthPercentage(100);
		
		PdfPCell dateCell = new PdfPCell();
		Phrase date = new Phrase(" Date : ",font9);
		Paragraph datePara = new  Paragraph();
		datePara.add(date);
		dateCell.addElement(datePara);
		dateCell.setBorder(0);
		
		PdfPCell dateValCell = new PdfPCell();
		Phrase dateVal = new Phrase(" ",font9);
		Paragraph dateValPara = new  Paragraph();
		dateValPara.add(dateVal);
		dateValCell.addElement(dateValPara);
		dateValCell.setBorder(0);

		PdfPCell signatureCell = new PdfPCell();
		Phrase signature = new Phrase(" Signature : ",font9);
		Paragraph signaturePara = new  Paragraph();
		signaturePara.add(signature);
		signaturePara.setAlignment(Element.ALIGN_RIGHT);
		signatureCell.addElement(signaturePara);
		signatureCell.setBorder(0);
		
		PdfPCell signatureValCell = new PdfPCell();
		Phrase signatureVal = new Phrase(" ",font9);
		Paragraph signatureValPara = new  Paragraph();
		signatureValPara.add(signatureVal);
		signatureValCell.addElement(signatureValPara);
		signatureValCell.setBorder(0);
		
		

		PdfPCell placeCell = new PdfPCell();
		Phrase place = new Phrase(" Place : ",font9);
		Paragraph placePara = new  Paragraph();
		placePara.add(place);
		placeCell.addElement(placePara);
		placeCell.setBorder(0);
		
		PdfPCell placeValCell = new PdfPCell();
		Phrase placeVal = new Phrase("",font9);
		Paragraph placeValPara = new  Paragraph();
		placeValPara.add(placeVal);
		placeValCell.addElement(placeValPara);
		placeValCell.setBorder(0);
		
		
		
		

		PdfPCell nameCell = new PdfPCell();
		Phrase name = new Phrase(" Name : ",font9);
		Paragraph namePara = new  Paragraph();
		namePara.add(name);
		namePara.setAlignment(Element.ALIGN_RIGHT);
		nameCell.addElement(namePara);
		nameCell.setBorder(0);
		
		PdfPCell nameValCell = new PdfPCell();
		Phrase nameVal = new Phrase(" ",font9);
		Paragraph nameValPara = new  Paragraph();
		nameValPara.add(nameVal);
		nameValCell.addElement(nameValPara);
		nameValCell.setBorder(0);
		

		PdfPCell stampCell = new PdfPCell();
		Phrase stamp = new Phrase(" Stamp : ",font9);
		Paragraph stampPara = new  Paragraph();
		stampPara.add(stamp);
		stampCell.addElement(stampPara);
		stampCell.setBorder(0);
		
		PdfPCell stampValCell = new PdfPCell();
		Phrase stampVal = new Phrase("",font9);
		Paragraph stampValPara = new  Paragraph();
		stampValPara.add(stampVal);
		stampValCell.addElement(stampValPara);
		stampValCell.setBorder(0);
		
		
		

		PdfPCell designationCell = new PdfPCell();
		Phrase designation = new Phrase(" Designation : ",font9);
		Paragraph designationPara = new  Paragraph();
		designationPara.add(designation);
		designationPara.setAlignment(Element.ALIGN_RIGHT);
		designationCell.addElement(designationPara);
		designationCell.setBorder(0);
		
		PdfPCell designationValCell = new PdfPCell();
		Phrase designationVal = new Phrase("",font9);
		Paragraph designationValPara = new  Paragraph();
		designationValPara.add(designationVal);
		designationValCell.addElement(designationValPara);
		designationValCell.setBorder(0);
		
		
		footerTbl.addCell(dateCell);
		footerTbl.addCell(dateValCell);
		footerTbl.addCell(signatureCell);
		footerTbl.addCell(signatureValCell);
		footerTbl.addCell(placeCell);
		footerTbl.addCell(placeValCell);
		footerTbl.addCell(nameCell);
		footerTbl.addCell(nameValCell);
		footerTbl.addCell(stampCell);
		footerTbl.addCell(stampValCell);
		footerTbl.addCell(designationCell);
		footerTbl.addCell(designationValCell);
		
		
		try {
			
			footerTbl.setWidths(new float[] { 25 , 25 , 25 , 25 });

		} catch (DocumentException e3) {
			e3.printStackTrace();
		}
		
		
		
	
		try {
			
			document.add(noteTbl);
			document.add(footerTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createBlankforUPC() {
		Phrase blankphrase = new Phrase("", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	

}
