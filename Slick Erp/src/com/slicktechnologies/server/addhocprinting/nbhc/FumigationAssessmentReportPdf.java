package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.Element;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;

public class FumigationAssessmentReportPdf {

	public Document document;
	
	   
		Customer cust;  
		Company comp;
		Contract con;
		List<ArticleType> articletype;
		Long serialNo;
		
		private Font font16boldul, font12bold, font8bold, font8, font9bold,font9boldul,
		font12boldul, font10boldul, font12, font16bold, font10, font10bold,font12Wbold,
		font14bold,font9,font7,font7bold,font9red,font9boldred,font12boldred;
		
		private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		
		DecimalFormat df=new DecimalFormat("0.00");
		
		BaseColor darkGreyColor = WebColors.getRGBColor("#696969");
		BaseColor lightGreyColor = WebColors.getRGBColor("#D3D3D3");

		public FumigationAssessmentReportPdf() {
			super();
			font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
			font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
			font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
			font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
			font8 = new Font(Font.FontFamily.HELVETICA, 8);
			font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
			font12 = new Font(Font.FontFamily.HELVETICA, 12);
			font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
			font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.WHITE);
			font10 = new Font(Font.FontFamily.HELVETICA, 10);
			font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
			font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
			font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
			font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);  
			font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
			font9 = new Font(Font.FontFamily.HELVETICA, 9);
			font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
			font7 = new Font(Font.FontFamily.HELVETICA, 7);
			font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
			
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			
		}

	public void setFAReport(Long count) {

		// Load Contract
		con = ofy().load().type(Contract.class).id(count).now();

		// load Company
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();

//		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
		
		
	}

	public void createPdf() {

		if (comp.getUploadHeader() != null) {
			createCompanyNameAsHeader(document, comp);
		}
		if (comp.getUploadFooter() != null) {
			createCompanyNameAsFooter(document, comp);
		}
		createBlankforUPC();
		createFumigationDetails();
		createFARTbl();
		createSummeryDetails();
		createrepresentiveTbl();

	}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createBlankforUPC() {
		Phrase blankphrase = new Phrase("", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	 private void createFumigationDetails() 
	 {
		 
		PdfPTable fumigationDetailsTbl = new PdfPTable(1);
		fumigationDetailsTbl.setWidthPercentage(100);

		Phrase fumigationLbl = new Phrase("Fumigation Assessment Report ",font12Wbold);

		PdfPCell fumigationLblCell = new PdfPCell(fumigationLbl);
		fumigationLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		fumigationLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		fumigationLblCell.setBackgroundColor(darkGreyColor);
		fumigationLblCell.setFixedHeight(25f);
		fumigationDetailsTbl.addCell(fumigationLblCell);

		PdfPTable fumigationDetailsInfoTbl = new PdfPTable(6);
		fumigationDetailsInfoTbl.setWidthPercentage(100);
		
		try {
			fumigationDetailsInfoTbl.setWidths(new float[] { 10, 25, 15, 15,15, 20 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		PdfPCell fumigationDetailsCell = new PdfPCell();

		Phrase serialNoPh = new Phrase("Serial No.", font9);
		PdfPCell serialNoCell = new PdfPCell(serialNoPh);
		// serialNoCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		serialNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		serialNoCell.setBorder(0);

		
		// first row
		Phrase serialNoVal = null;
		serialNoVal = new Phrase(" " + con.getCount(), font9);

		PdfPCell serialNoValCell = new PdfPCell(serialNoVal);
		serialNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serialNoValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		serialNoValCell.setBorderWidthLeft(0);
		serialNoValCell.setBorderWidthRight(0);
		serialNoValCell.setBorderWidthTop(0);

		Phrase date = new Phrase(" Date   ", font9);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dateCell.setBorder(0);

		Phrase dateVal = new Phrase(" " + fmt.format(new Date()), font9);
		PdfPCell dateValCell = new PdfPCell(dateVal);
		dateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		dateValCell.setBorderWidthLeft(0);
		dateValCell.setBorderWidthRight(0);
		dateValCell.setBorderWidthTop(0);
		

		// 2nd row
		Phrase customer = new Phrase("Customer ", font9);
		PdfPCell customerCell = new PdfPCell(customer);
		// customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		customerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerCell.setBorder(0);

		Phrase customerVal = new Phrase("" + con.getCustomerFullName(), font9);
		PdfPCell customerValCell = new PdfPCell(customerVal);
		customerValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerValCell.setBorderWidthLeft(0);
		customerValCell.setBorderWidthRight(0);
		customerValCell.setBorderWidthTop(0);
		
		PdfPTable table1=new PdfPTable(2);
		table1.setWidthPercentage(100f);
		try {
			table1.setWidths(new float[] { 10,90});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		table1.addCell(customerCell);
		table1.addCell(customerValCell);
		

		// 3rd row
		Phrase wareHousecode = new Phrase("Warehouse/Code _______________________________________", font9);
		PdfPCell wareHousecodeCell = new PdfPCell(wareHousecode);
		wareHousecodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		wareHousecodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		wareHousecodeCell.setBorder(0);

		Phrase wareHousecodeVal = new Phrase("                                      ",font9);
		PdfPCell wareHousecodeValCell = new PdfPCell(wareHousecodeVal);
		wareHousecodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		wareHousecodeValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		wareHousecodeValCell.setBorderWidthLeft(0);
		wareHousecodeValCell.setBorderWidthRight(0);
		wareHousecodeValCell.setBorderWidthTop(0);

		Phrase branch = new Phrase("CCPM Branch ___________________", font9);
		PdfPCell branchCell = new PdfPCell(branch);
		branchCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		branchCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		branchCell.setBorder(0);

		Phrase branchVal = new Phrase(" " + con.getBranch(), font9);
		PdfPCell branchValCell = new PdfPCell(branchVal);
		branchValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		branchValCell.setBorderWidthLeft(0);
		branchValCell.setBorderWidthRight(0);
		branchValCell.setBorderWidthTop(0);
		
		PdfPTable table2=new PdfPTable(2);
		table2.setWidthPercentage(100f);
		try {
			table2.setWidths(new float[] { 60,40});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		table2.addCell(wareHousecodeCell);
		table2.addCell(branchCell);
		

		Phrase blnks = new Phrase("                             ",font9);
		PdfPCell blnksCell = new PdfPCell();
		blnksCell.addElement(blnks);
		blnksCell.setBorder(0);

		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);

		fumigationDetailsInfoTbl.addCell(serialNoCell);
		fumigationDetailsInfoTbl.addCell(serialNoValCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(blnksCell);
		fumigationDetailsInfoTbl.addCell(dateCell);
		fumigationDetailsInfoTbl.addCell(dateValCell);
//
//		fumigationDetailsInfoTbl.addCell(customerCell);
//		fumigationDetailsInfoTbl.addCell(customerValCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//
//		fumigationDetailsInfoTbl.addCell(wareHousecodeCell);
//		fumigationDetailsInfoTbl.addCell(wareHousecodeValCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(branchCell);
//		fumigationDetailsInfoTbl.addCell(branchValCell);
//
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);
//		fumigationDetailsInfoTbl.addCell(blnksCell);

		fumigationDetailsCell.setBorder(0);
		fumigationDetailsTbl.addCell(fumigationDetailsCell);

		fumigationDetailsInfoTbl.setSpacingAfter(10f);
		table1.setSpacingAfter(10f);
		table2.setSpacingAfter(10f);
		try {
			document.add(fumigationDetailsTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		try {
			document.add(fumigationDetailsInfoTbl);
			document.add(table1);
			document.add(table2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createFARTbl() {
		PdfPTable farTbl = new PdfPTable (3);
		farTbl.setWidthPercentage(100);
		
		try {
			farTbl.setWidths(new float [] {35,20,45});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase itemLbl = new  Phrase (" Item ",font9bold);
		
		PdfPCell itemLblCell = new PdfPCell(itemLbl);
		itemLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		itemLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		itemLblCell.setFixedHeight(25f); 
		itemLblCell.setBackgroundColor(lightGreyColor);
		
		Phrase ysno = new  Phrase (" Yes / No / NA ",font9bold);
		
		PdfPCell yesNoLblCell = new PdfPCell(ysno);
		yesNoLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		yesNoLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		yesNoLblCell.setFixedHeight(25f); 
		yesNoLblCell.setBackgroundColor(lightGreyColor);
		
		Phrase comments = new  Phrase (" Comments ",font9bold);
		PdfPCell commentsLblCell = new PdfPCell(comments);
		commentsLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		commentsLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		commentsLblCell.setFixedHeight(25f); 
		commentsLblCell.setBackgroundColor(lightGreyColor);
		
		Phrase itemdetails1 = new Phrase("Space Availability around Stacks ",font9);
		PdfPCell item1Cell = new PdfPCell(itemdetails1);
		item1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);;
		item1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails2 = new Phrase("Moisture Content of Commodity ",font9);
		PdfPCell item2Cell = new PdfPCell(itemdetails2);
		item2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);;
		item2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails3 = new Phrase("Possibility of Rain Water Entry ",font9);
		
		PdfPCell item3Cell = new PdfPCell(itemdetails3);
		item3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);;
		item3Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails4 = new Phrase("Infested Stack in Neighbouring Warehouses(WH) ",font9);
		
		PdfPCell item4Cell = new PdfPCell(itemdetails4);
		item4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item4Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails5 = new Phrase("Infest stacks of the other customers in same WH",font9);
		
		PdfPCell item5Cell = new PdfPCell(itemdetails5);
		item5Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item5Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails6 = new Phrase("Floor Condition-Free of Cracks  ",font9);
		
		PdfPCell item6Cell = new PdfPCell(itemdetails6);
		item6Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item6Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails7 = new Phrase("Presence of domestic animals in and around WH ",font9);
		
		PdfPCell item7Cell = new PdfPCell(itemdetails7);
		item7Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item7Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails8 = new Phrase("Restricted access to WH ",font9);
		
		
		PdfPCell item8Cell = new PdfPCell(itemdetails8);
		item8Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item8Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails9 = new Phrase("Human Habitation in and around WH",font9);
		
		PdfPCell item9Cell = new PdfPCell(itemdetails9);
		item9Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item9Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails10 = new Phrase("Adequate ventilation for dispersing Fumigant ",font9);
		
		PdfPCell item10Cell = new PdfPCell(itemdetails10);
		item10Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item10Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails11 = new Phrase("Adequacy of fumigation cover size for covering stacks ",font9);
		PdfPCell item11Cell = new PdfPCell(itemdetails11);
		item11Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		item11Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
		Phrase itemdetails12 = new Phrase("Wheater Stacks completed for initiating fumigation ?",font9);
		
		PdfPCell item12Cell = new PdfPCell(itemdetails12);
		item12Cell.setHorizontalAlignment(Element.ALIGN_LEFT);;
		item12Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
//		PdfPCell itemdetails1Cell = new PdfPCell();
//		itemCell.addElement(itemdetails2);
		Phrase blanks = new Phrase (" ", font9);
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.addElement(blanks);
		
		
	////////////////////////////////	Adding Cell in Table     /////////////////////////// 
		farTbl.addCell(itemLblCell);	
		farTbl.addCell(yesNoLblCell);	
		farTbl.addCell(commentsLblCell);	
		
		farTbl.addCell(item1Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);
		
		farTbl.addCell(item2Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item3Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item4Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item5Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item6Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item7Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item8Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item9Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item10Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);	
		
		farTbl.addCell(item11Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);
		
		farTbl.addCell(item12Cell);	
		farTbl.addCell(blnkCell);	
		farTbl.addCell(blnkCell);
		
		try {
			document.add(farTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}    
	 
private void createSummeryDetails() {
		
		PdfPTable summeryTbl = new PdfPTable (1); 
		summeryTbl.setWidthPercentage(100);
		
	Phrase summery     = new Phrase ("Summery _______________________________________________________________________________________________",font9);
	Phrase summeryVal1 = new Phrase ("_______________________________________________________________________________________________________",font9);
	Phrase summeryVal2 = new Phrase ("_______________________________________________________________________________________________________",font9);
	PdfPCell summeryCell = new PdfPCell (); 
	summeryCell.setBorder(0);
	Paragraph summeryPara = new Paragraph();
	summeryPara.add(Chunk.NEWLINE);
	summeryPara.add(summery);  
	summeryPara.add(Chunk.NEWLINE);
	summeryPara.add(summeryVal1);
	summeryPara.add(Chunk.NEWLINE);
	summeryPara.add(summeryVal2);
	summeryPara.add(Chunk.NEWLINE);
	summeryPara.add(Chunk.NEWLINE);
	summeryCell.addElement(summeryPara);
	summeryTbl.addCell(summeryCell);
	
	try {
		document.add(summeryTbl);
	} catch (DocumentException e) {
		
		e.printStackTrace();
		
     	}
	}

	private void createrepresentiveTbl()
	{
		
		PdfPTable representiveTbl = new PdfPTable (2);
		representiveTbl.setWidthPercentage(100);
		
		PdfPCell nbhcRepresentiveCell = new PdfPCell ();
		Phrase nbhcrepresentrive1 = new Phrase ("  Signature and Date",font9);
		Phrase nbhcrepresentrive2 = new Phrase ("  Name",font9);
		nbhcRepresentiveCell.addElement(nbhcrepresentrive1);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		nbhcRepresentiveCell.addElement(nbhcrepresentrive2);
		nbhcRepresentiveCell.addElement(Chunk.NEWLINE);
		

		PdfPCell custRepresentiveCell = new PdfPCell ();
		Phrase custRepresentive1 = new Phrase ("  Signature and Date",font9);
		Phrase custRepresentive2 = new Phrase ("  Name",font9);
		custRepresentiveCell.addElement(custRepresentive1);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
		custRepresentiveCell.addElement(custRepresentive2);
		custRepresentiveCell.addElement(Chunk.NEWLINE);
		
		
		
		Phrase nbhcrepresentriveLbl = new Phrase (" NBHC Representive ",font9);
		PdfPCell nbhcRepresentiveLblCell = new PdfPCell (nbhcrepresentriveLbl);
		nbhcRepresentiveLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		nbhcRepresentiveLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		nbhcRepresentiveLblCell.setBackgroundColor(lightGreyColor);
		nbhcRepresentiveLblCell.setFixedHeight(25f);
		
		Phrase custRepresentiveLbl = new Phrase (" Customer Representive ",font9);
		PdfPCell custRepresentiveLblCell = new PdfPCell (custRepresentiveLbl);
		custRepresentiveLblCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		custRepresentiveLblCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		custRepresentiveLblCell.setBackgroundColor(lightGreyColor);
		custRepresentiveLblCell.setFixedHeight(25f);
		
		representiveTbl.addCell(nbhcRepresentiveCell);
		representiveTbl.addCell(custRepresentiveCell); 
		representiveTbl.addCell(nbhcRepresentiveLblCell);
		representiveTbl.addCell(custRepresentiveLblCell);
		
		
		try {
			document.add(representiveTbl);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
	      



}
