package com.slicktechnologies.server.addhocprinting.nbhc;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
public class NBHCContractRenewalPdf {

	ContractRenewal conRenw;
	Company comp;
	Customer cust;
	Contract con;
	Employee emp;

	public Document document;

	private Font font16boldul, font12bold, font8bold, font8, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9;

	Phrase chunk;
	PdfPCell pdfcode,pdfname,pdfduration,pdfservices,pdfprice,pdftax,pdfnetPay,pdfunit,pdfstartdate,pdfenddate;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	public NBHCContractRenewalPdf() {
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
	}

	public void setContractRewnewal(ContractRenewal conRenewal){
		conRenw=conRenewal;
		
		if (conRenw.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", conRenw.getCompanyId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", conRenw.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getCustomerId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", conRenw.getContractId()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getContractId()).first().now();

		if (conRenw.getCompanyId() == null)
			emp = ofy().load().type(Employee.class).filter("fullname", conRenw.getSalesPerson()).first().now();
		else
			emp = ofy().load().type(Employee.class).filter("fullname", conRenw.getSalesPerson()).filter("companyId", conRenw.getCompanyId()).first().now();

	}



	public void createPdf(String preprintStatus) {
		if(preprintStatus.contains("plane"))
		{
			Createblank();
			createLogo(document,comp);
			createCompanyAddress();
		}
		else if(preprintStatus.contains("yes"))
		{
			createBlankforUPC();
		}
		else if(preprintStatus.contains("no"))
		{
			createBlankforUPC();
			  if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
		}
		
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg();
		createProductInfo();
		footerInfo();
	}


	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createBlankforUPC() {
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPdfAttachment(Contract con,Company company,ContractRenewal conRen,Customer cust){
		this.con=con;
		comp=company;
		conRenw=conRen;
		this.cust=cust;
		Createblank();
		createLogo(document,comp);
		createCompanyAddress();
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg();
		createProductInfo();
		footerInfo();
	}



	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	public  void createCompanyAddress()
	{
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		/*
		 *  nidhi
		 *  15-07-2017
		 *   mobile number remove from display only telephone number will display
		 */
		Phrase contactinfo=new Phrase(" Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim()
				+ " , "+ comp.getWebsite().trim(),font10);
		
		/*
		 *  end
		 */
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
	//	
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		comapnyCell.setBorderWidthTop(0);
		comapnyCell.setBorderWidthLeft(0);
		comapnyCell.setBorderWidthRight(0);
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}




	private void createHeading() {
		String title1 = "";
		title1 = "Renewal Letter";

		Phrase titlephrase = new Phrase(title1, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorder(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		
		/*
		 *   nidhi
		 *   space removed between "customer id and : "
		 *   15-07-2017  
		 */
		Phrase custInfo = new Phrase("Customer Id:", font10);
		
		/*
		 *  end
		 */
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		
		Phrase custInfovalue = new Phrase(conRenw.getCustomerId()+"", font10);
		PdfPCell custInfovalueCell = new PdfPCell(custInfovalue);
		custInfovalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfovalueCell.setBorder(0);
		
		
		
		Phrase date = new Phrase("Date :", font10);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setBorder(0);
		
		
		Phrase datevalue = new Phrase(fmt.format(conRenw.getDate())+"", font10);
		PdfPCell datevalueCell = new PdfPCell(datevalue);
		datevalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datevalueCell.setBorder(0);
		
		
		
		
		
		
		float[] relativeWidths1 = {1.3f,5.5f,1f,2f};
		
		PdfPTable headTbl=new PdfPTable(4);
		headTbl.setWidthPercentage(100f);
		try {
			headTbl.setWidths(relativeWidths1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		headTbl.addCell(custInfoCell);
		headTbl.addCell(custInfovalueCell);
		headTbl.addCell(dateCell);
		headTbl.addCell(datevalueCell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell1=new PdfPCell(titlepdfcell);
		titlePdfCell1.setBorder(0);
		
		PdfPCell headIfo=new PdfPCell(headTbl);
		headIfo.setBorder(0);
		
		parent.addCell(titlePdfCell1);
		parent.addCell(headIfo);
		
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createCustomerDetails() {
		
		Phrase to = new Phrase("To,", font10);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		toCell.setBorder(0);
		
		Phrase custInfo ;
		if(cust.isCompany()){
			custInfo = new Phrase(cust.getCompanyName(), font10);
		}else{
			custInfo = new Phrase(cust.getFullname(), font10);
		}
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			if(cust.getAdress().getAddrLine2()!=null){
				if(cust.getAdress().getLandmark()!=null){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
				}
			}else{
				if(cust.getAdress().getLandmark()!=null){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(cust.getAdress().getLocality()!=null){
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+" "+cust.getAdress().getPin();
			}
		}
		
		
		Phrase custAddInfo = new Phrase(custFullAdd1, font10);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		
		PdfPTable customerTable=new PdfPTable(1);
		customerTable.setWidthPercentage(100);
		
		customerTable.addCell(toCell);
		customerTable.addCell(custInfoCell);
		customerTable.addCell(custAddInfoCell);
		
		
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(customerTable);
		customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		customerCell.setBorder(0);
		
		
		
		PdfPTable headTbl=new PdfPTable(2);
		headTbl.setWidthPercentage(100f);
		
		headTbl.addCell(customerCell);
		headTbl.addCell(blankCell);
		
		PdfPCell parentCell = new PdfPCell();
		parentCell.addElement(headTbl);
		parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		parentCell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(parentCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
	}

	private void createSubjectAndMsg() {
		
		Phrase dear = new Phrase("Dear Sir/Madam,", font10bold);
		PdfPCell dearCell = new PdfPCell(dear);
		dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dearCell.setBorder(0);
		
		/*
		 *  nidhi
		 *   15-07-2017
		 *   end date to ending date text change done by nidhi
		 */
		
		Phrase sub = new Phrase("Subject : Renewal of Contract ending on "+fmt.format(con.getEndDate())+".                                                                  "+"\n"+"\n", font10);
		/*
		 *  end
		 */
		
		PdfPCell subCell = new PdfPCell(sub);
		subCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subCell.setBorder(0);
		
		String title1 = "";
		/*
		 *  ndihi
		 *  	15-07-2017
		 *  	
		 */
		title1 = "It has been our privilege to serve you over the past year and we truly appreciate & value your patronage. \nWe trust you have found our services exemplery & to your complete satisfaction. Your current pest management contract for following services expires on "+fmt.format(con.getEndDate())+" and we request you to renew the same. Renewal charges are mentioned below.";
		
		/*
		 *   end
		 */
		
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(dearCell);
		parent.addCell(subCell);
		parent.addCell(msgCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createProductInfo() {
//		 double totalExcludingTax=calculateTotalExcludingTax();
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		float[] relativeWidths = {2.2f,1.2f,1.2f,0.5f,1f,1f,2.4f};
		
		PdfPTable table = new PdfPTable(7);
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		/*
		 *  nidhi
		 *    15-07-2017
		 *    variable added for add unit field
		 */
		Phrase unit = new Phrase("Unit", font1);
		/*
		 *  end
		 */
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		Phrase startdate = new Phrase("START DATE",font1);
		Phrase enddate = new Phrase("END DATE", font1);
		Phrase price = new Phrase("PRICE INR (EXCLUDING TAXES)", font1);
		
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/*
		 *  nidhi
		 *  	15-07-2017
		 *   unit cell declare for table
		 */
		
		PdfPCell cellunit = new PdfPCell(unit);
		cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellStartdate = new PdfPCell(startdate);
		cellStartdate.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellenddate = new PdfPCell(enddate);
		cellenddate.setHorizontalAlignment(Element.ALIGN_CENTER);
		/*
		 *  end
		 */
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellname);
		table.addCell(startdate);
		table.addCell(enddate);
		table.addCell(cellunit);
		table.addCell(cellduration);
		table.addCell(cellservices);
		table.addCell(cellprice);
		
		
		for(int i=0;i<conRenw.getItems().size();i++){
			
			////
			if(conRenw.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			
			////
			if(con.getStartDate()!=null){
				chunk = new Phrase(fmt.format(con.getStartDate()) + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfstartdate = new PdfPCell(chunk);
			pdfstartdate.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getEndDate()!=null){
				chunk = new Phrase(fmt.format(con.getEndDate()) + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfenddate = new PdfPCell(chunk);
			pdfenddate.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(conRenw.getItems().get(i).getQty()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getQty()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfunit = new PdfPCell(chunk);
			pdfunit.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			 /////
			if(conRenw.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			if(conRenw.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(conRenw.getItems().get(i).getPrice()+"", font9);
			}else{
				 chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);

			
			table.addCell(pdfname);
			table.addCell(pdfstartdate);
			table.addCell(pdfenddate);
			table.addCell(pdfunit);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			table.addCell(pdfprice);
		}
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		 
	}

	private void footerInfo() {
		
		
		boolean priceHikeFlag=false;
		
		for(int i=0;i<conRenw.getItems().size();i++){
			if(conRenw.getItems().get(i).getPrice()>conRenw.getItems().get(i).getOldProductPrice()){
				priceHikeFlag=true;
			}
		}
		
		String title1 = "";
		
		if(priceHikeFlag==true){
			
			if(comp.getCellNumber2() != null)
			{
				if(comp.getLandline() != null)
				{
					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"   Phone :"+comp.getLandline()+"\n"+"\n";
				}
				else
				{
					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+comp.getCellNumber2()+"\n"+"\n";
				}
				
			}
			else
			{
				if(comp.getLandline() != null)
				{
					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+"   Phone :"+comp.getLandline()+"\n"+"\n";
				}
				else
				{
					title1 = "Kindly note that we have been compelled to increase our annual services charges due to increase in our operational cost. We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above.Please do give us a call and we would gladly assist you. For any assistance, Please call on :"+comp.getCellNumber1()+","+"\n"+"\n";
				}
			}
			
		}
		else
		{
					title1 = "We reiterate our commitment to meet your requirements and look forward to hearing from you soon."+"\n"
							+"Should you require further information on above.Please let us know and we would gladly assist you.";
		}
		
		
		
		String ConformNote = "Please issue us a signed order for confirmation of our services."; 
		
		
		Phrase Confmsg = new Phrase(ConformNote, font10);
		PdfPCell ConfmsgCell = new PdfPCell(Confmsg);
		ConfmsgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		ConfmsgCell.setBorder(0);
		
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		
		String contratDescription="";
		if(con.getDescription()!=null){
			contratDescription=con.getDescription();
		}
		
		Phrase contractDesc = new Phrase(contratDescription, font10);
		PdfPCell contractDescCell = new PdfPCell(contractDesc);
		contractDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractDescCell.setBorder(0);
		
		String contratRenDescription="";
		if(conRenw.getDescription()!=null){
			contratRenDescription=conRenw.getDescription();
		}
		
		Phrase contractRenDesc = new Phrase(contratRenDescription, font10);
		PdfPCell contractRenDescCell = new PdfPCell(contractRenDesc);
		contractRenDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractRenDescCell.setBorder(0);
		
		

		String title2 = "";
		title2 = "Thank You,"+"\n"+"Yours Sincerely,";
		
		Phrase thank = new Phrase(title2, font10);
		PdfPCell thankCell = new PdfPCell(thank);
		thankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		thankCell.setBorder(0);
		
		
		Phrase compn = new Phrase("For "+comp.getBusinessUnitName(), font10bold);
		PdfPCell compCell = new PdfPCell(compn);
		compCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compCell.setBorder(0);
		
		
		float[] relativeWidths = {1f,0.2f,4f};
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell salesname = new PdfPCell(new Phrase("Sales Person Name:", font10));
		salesname.setHorizontalAlignment(Element.ALIGN_LEFT);
		salesname.setBorder(0);
		
		PdfPCell nameCell = new PdfPCell(new Phrase(conRenw.getSalesPerson(), font10));
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCell.setBorder(0);
		
		
		PdfPCell emailname = new PdfPCell(new Phrase("Email:", font10));
		emailname.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailname.setBorder(0);
		
		PdfPCell emailCell = new PdfPCell(new Phrase(emp.getEmail(), font10));
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailCell.setBorder(0);
		
		
		PdfPCell mblNum = new PdfPCell(new Phrase("Mobile No.:", font10));
		mblNum.setHorizontalAlignment(Element.ALIGN_LEFT);
		mblNum.setBorder(0);
		
		PdfPCell mblNumCell = new PdfPCell(new Phrase(emp.getCellNumber1()+"", font10));
		mblNumCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		mblNumCell.setBorder(0);
		
		PdfPCell columnCell = new PdfPCell(new Phrase(":", font10));
		columnCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		columnCell.setBorder(0);
		
		table.addCell(salesname);
		table.addCell(columnCell);
		table.addCell(nameCell);
		
		table.addCell(emailname);
		table.addCell(columnCell);
		table.addCell(emailCell);
		
		table.addCell(mblNum);
		table.addCell(columnCell);
		table.addCell(mblNumCell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    PdfPCell blankCell = new PdfPCell(blank);
	    blankCell.setBorder(0);
	    
	    parent.addCell(ConfmsgCell);
	    
	    parent.addCell(blankCell);
	    
		parent.addCell(msgCell);
		if(con.getDescription()!=null){
			parent.addCell(contractDescCell);
		}
		if(conRenw.getDescription()!=null){
			parent.addCell(contractRenDescCell);
		}
		parent.addCell(blankCell);
		parent.addCell(thankCell);
		parent.addCell(compCell);
		parent.addCell(blankCell);
		parent.addCell(blankCell);
		parent.addCell(blankCell);
		
		PdfPCell parentCell = new PdfPCell();
		parentCell.addElement(table);
		parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		parentCell.setBorder(0);
		
		PdfPTable parent1=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(parentCell);
		parent.setSpacingAfter(15f);
		
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
			document.add(parent1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}



	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=conRenw.getItems();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
			if(entity.getPercentageDiscount()==null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal*entity.getQty();
				sum=sum+priceVal;
			}
			if(entity.getPercentageDiscount()!=null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
				priceVal=priceVal*entity.getQty();
				
				sum=sum+priceVal;
			}
			

		}
		return sum;
	}

	/**
	 * Developed by : Rohan Bhagde.
	 * Reason : This method is used for calculating Total amount including tax
	 * @param newPrice
	 * @param serTax
	 * @param vatTax
	 * @return
	 */
	public double calculateTotalAmt(double newPrice , double serTax , double vatTax)
	{
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		System.out.println("new price "+newPrice );
		System.out.println("service tax "+serTax );
		System.out.println("vat tax"+vatTax );
		if(vatTax!=0)
		{
			vatTaxAmt = newPrice * (vatTax / 100);
		}
		totalAmt = newPrice +vatTaxAmt;
		if(serTax !=0 )
		{
			setTaxAmt = totalAmt * (serTax / 100);
		}
		
		totalAmt = totalAmt +setTaxAmt;
		System.out.println("new price "+totalAmt );
		return totalAmt;
	}



	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			// Here if both are inclusive then first remove service tax and then on that amount
			// calculate vat.
			double removeServiceTax=(entity.getPrice()/(1+service/100));
						
			//double taxPerc=service+vat;
			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		return tax;
	}
}
