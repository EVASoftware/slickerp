package com.slicktechnologies.server.addhocprinting.nbhc;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.personlayer.Employee;

public class NBHCLoiPdf {

	public Document document;
	Quotation quotation;
	Customer cust;
	Employee emp;
	Branch branch;
	
	
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,
	font14bold;
	
	BaseColor mycolor = WebColors.getRGBColor("#C0C0C0");
	BaseColor mycolor1 = WebColors.getRGBColor("#F8F8F8");
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	/**
	 * nidhi
	 */
	Company company;
	public NBHCLoiPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void setQuotationLoi(Long count) {
		
		quotation=ofy().load().type(Quotation.class).id(count).now();
		
		if(quotation!=null){
			Company company=ofy().load().type(Company.class).filter("companyId", quotation.getCompanyId()).first().now();
			this.company=company;
		}
		
		//   load customer 
		if(quotation.getCompanyId()!=null)
		      cust=ofy().load().type(Customer.class).filter("count",quotation.getCustomerId()).
		        filter("companyId",quotation.getCompanyId()).first().now();
			if(quotation.getCompanyId()==null)
			     cust=ofy().load().type(Customer.class).filter("count",quotation.getCustomerId()).first().now();
			
		//  load employee	
			
			if(quotation.getCompanyId()!=null)
			      emp=ofy().load().type(Employee.class).filter("fullname",quotation.getEmployee().trim()).
			        filter("companyId",quotation.getCompanyId()).first().now();
				if(quotation.getCompanyId()==null)
					emp=ofy().load().type(Employee.class).filter("fullname",quotation.getEmployee().trim()).first().now();
	
				
			//  load branch	
				
				if(quotation.getCompanyId()!=null)
					branch=ofy().load().type(Branch.class).filter("buisnessUnitName",quotation.getBranch()).
				        filter("companyId",quotation.getCompanyId()).first().now();
					if(quotation.getCompanyId()==null)
						branch=ofy().load().type(Branch.class).filter("buisnessUnitName",quotation.getBranch()).first().now();		
				
					
	}

	public void createPdf(String prePrint) {
		if (company.getUploadHeader() != null && prePrint.equalsIgnoreCase("no")) {
			createCompanyNameAsHeader(document, company);
		}
		if (company.getUploadFooter() != null && prePrint.equalsIgnoreCase("no")) {
			createCompanyNameAsFooter(document, company);
		}
		createBlankforUPC();
		CreateHeading();
		CompanyAndCustomerAddress();
		CreateProductDetails();
		ContactPersonDetails();
	}

	private void CompanyAndCustomerAddress() {
		
		Phrase heading = new Phrase("Serial No. :"+quotation.getCount(),font8);
		PdfPCell headingCell = new PdfPCell(heading);
		headingCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		headingCell.setBorder(0);
//		headingCell.setBackgroundColor(mycolor);
//		headingCell.setFixedHeight(20f);
		
		PdfPTable table1 = new PdfPTable(1);
		table1.setWidthPercentage(100f);
		table1.addCell(headingCell);
//		table.setSpacingAfter(5f);
		
		try {
			document.add(table1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		PdfPTable customertable = new PdfPTable(1);
		customertable.setWidthPercentage(100f);
		
		
		
		String custName="";
		
		//  rohan modified this code for printabel name 
		// date : 8/11/2016
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
	    Phrase fullname =new Phrase(custName,font8);
	   /* if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
	    	fullname.add(custName);
		}
	    else
	    {
	    	fullname.add(custName);
	    }
	    fullname.setFont(font8);*/
	    
		PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(fullname);
		custnamecell.setBorder(0);
		//custnamecell.setBorder(1);
		custnamecell.setBackgroundColor(mycolor1);
		customertable.addCell(custnamecell);
		
			 
		
		
		Phrase customeradress= new Phrase(cust.getAdress().getAddrLine1(),font8);
		Phrase customeradress2=null;
		if(cust.getAdress().getAddrLine2()!=null){
		   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font8);
		}
		
		PdfPCell custaddress1=new PdfPCell();
		custaddress1.addElement(customeradress);
		custaddress1.setBorder(0);
		customertable.addCell(custaddress1);
		
		PdfPCell custaddress2=new PdfPCell();
		if(cust.getAdress().getAddrLine2()!=null){
			custaddress2.addElement(customeradress2);
			custaddress2.setBorder(0);
			}
		customertable.addCell(custaddress2);
		
		String custlandmark2="";
		Phrase landmar2=null;
		
		String  localit2="";
		Phrase custlocality2= null;
		
		if(cust.getAdress().getLandmark()!=null)
		{
			custlandmark2 = cust.getAdress().getLandmark();
			landmar2=new Phrase(custlandmark2,font8);
		}
		
		
		if(cust.getAdress().getLocality()!=null){
			localit2=cust.getAdress().getLocality();
			custlocality2=new Phrase(localit2,font8);
		}
		
		Phrase cityState2=new Phrase(cust.getAdress().getCity()
				+" - "+cust.getAdress().getPin()
//				+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
				,font8);
		
		PdfPCell custlandcell2=new PdfPCell();
		custlandcell2.addElement(landmar2);
		custlandcell2.setBorder(0);
		customertable.addCell(custlandcell2);
		
		PdfPCell custlocalitycell2=new PdfPCell();
		custlocalitycell2.addElement(custlocality2);
		custlocalitycell2.setBorder(0);
		customertable.addCell(custlocalitycell2);
		
		PdfPCell custcitycell2=new PdfPCell();
		custcitycell2.addElement(cityState2);
		custcitycell2.setBorder(0); 
		customertable.addCell(custcitycell2);
//		custcitycell2.addElement(Chunk.NEWLINE);
		 
		 //   rohan modified and added cust landline to customer details 
		Phrase custcontact = null;
		if(cust.getLandline()!= null && cust.getLandline()!= 0)
		{
		  custcontact= new Phrase("Mobile: : "+cust.getCellNumber1()+"   Phone: "+cust.getLandline(),font8); 
		}
		else
		{
		  custcontact= new Phrase("Mobile: : "+cust.getCellNumber1(),font8); 
		}
		 
		 
		PdfPCell custcontactcell=new PdfPCell();
		custcontactcell.addElement(custcontact);
		custcontactcell.setBorder(0);
		customertable.addCell(custcontactcell);
		
		Phrase custemail=new Phrase("email: "+cust.getEmail(),font8);
		PdfPCell custemailcell=new PdfPCell();
		custemailcell.addElement(custemail);
		custemailcell.setBorder(0);
		customertable.addCell(custemailcell);
		
		
		
		//   nbhc name and address 
			
			
			
			
		PdfPTable nbhcTable = new PdfPTable(1);
		nbhcTable.setWidthPercentage(100f);
		
		
		Phrase name= new Phrase("NBCH Pvt. Ltd. ("+(quotation.getBranch())+")",font8);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0); 
		nbhcTable.addCell(nameCell);
		
		
		
		Phrase branchadress= new Phrase(branch.getAddress().getAddrLine1(),font8);
		Phrase branchadress2=null;
		if(branch.getAddress().getAddrLine2()!=null){
			branchadress2= new Phrase(branch.getAddress().getAddrLine2(),font8);
		}
		
		PdfPCell branchadress1=new PdfPCell();
		branchadress1.addElement(branchadress);
		branchadress1.setBorder(0);
		nbhcTable.addCell(branchadress1);
		
		PdfPCell branchAddd2=new PdfPCell();
		if(branch.getAddress().getAddrLine2()!=null){
			branchAddd2.addElement(branchadress2);
			branchAddd2.setBorder(0);
			}
		nbhcTable.addCell(branchAddd2);
		
		String branchlandmark2="";
		Phrase branchlandmar2=null;
		
		String  branchlocalit2="";
		Phrase branchlocality2= null;
		
		if(cust.getAdress().getLandmark()!=null)
		{
			branchlandmark2 = branch.getAddress().getLandmark();
			branchlandmar2=new Phrase(branchlandmark2,font8);
		}
		
		
		if(cust.getAdress().getLocality()!=null){
			branchlocalit2=branch.getAddress().getLocality();
			branchlocality2=new Phrase(branchlocalit2,font8);
			}
		
		Phrase branchcityState2=new Phrase(branch.getAddress().getCity()
				+" - "+branch.getAddress().getPin()
//					+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
				,font8);
		
		PdfPCell branchlandcell2=new PdfPCell();
		branchlandcell2.addElement(branchlandmar2);
		branchlandcell2.setBorder(0);
		nbhcTable.addCell(branchlandcell2);
		
		PdfPCell branchlocalitycell2=new PdfPCell();
		branchlocalitycell2.addElement(branchlocality2);
		branchlocalitycell2.setBorder(0);
		nbhcTable.addCell(branchlocalitycell2);
		
		PdfPCell branchcitycell2=new PdfPCell();
		branchcitycell2.addElement(branchcityState2);
		branchcitycell2.setBorder(0); 
		nbhcTable.addCell(branchcitycell2);
//			custcitycell2.addElement(Chunk.NEWLINE);
				 
		 //   rohan modified and added cust landline to customer details 
		 Phrase branchcontact = null;
		  if(cust.getLandline()!= null)
		  {
			  branchcontact= new Phrase("Mobile: : "+branch.getCellNumber1()+"   Phone: "+branch.getLandline(),font8); 
		  }
		  else
		  {
			  branchcontact= new Phrase("Mobile: : "+branch.getCellNumber1(),font8); 
		  }
		 
		 
		PdfPCell branchcontactCell=new PdfPCell();
		branchcontactCell.addElement(branchcontact);
		branchcontactCell.setBorder(0);
		nbhcTable.addCell(branchcontactCell);
		
		Phrase branchemail=new Phrase("email: "+branch.getEmail(),font8);
		PdfPCell branchemailCell=new PdfPCell();
		branchemailCell.addElement(branchemail);
		branchemailCell.setBorder(0);
		nbhcTable.addCell(branchemailCell);
			
		
		
		//  neds here 
		
		float[] columnWidths4={0.5f,4.5f,0.8f,4.5f};
		PdfPTable table = new PdfPTable(4);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(10f);
		table.setSpacingAfter(10f);
		
		try {
			table.setWidths(columnWidths4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase totalBlank = new Phrase(" ");
		PdfPCell totalBlankCell= new PdfPCell(totalBlank);
		totalBlankCell.setBorder(0);
		
		//   nbhc name 
		
		Phrase to = new Phrase("To :",font8);
		PdfPCell toCell= new PdfPCell(to);
		toCell.setBorder(0);
		table.addCell(toCell);
		
		
		PdfPCell nbhcTableCell = new PdfPCell(nbhcTable);
		nbhcTableCell.setBorder(0);
		nbhcTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(nbhcTableCell);
		
		
		Phrase from = new Phrase("From :",font8);
		PdfPCell fromCell= new PdfPCell(from);
		fromCell.setBorder(0);
		table.addCell(fromCell);
		
		
		PdfPCell customerTableCell =new PdfPCell(customertable);
		customerTableCell.setBorder(0);
		customerTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(customerTableCell);
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
			
	}

	private void CreateProductDetails() {
		
		//  quotation note 
		
		PdfPTable tablepresmise= new PdfPTable(1);
		tablepresmise.setWidthPercentage(100f);
		
		Phrase quotNote = new Phrase("We are pleased to avail your pest management / fumigation services as per quote dated "
				+sdf.format(quotation.getQuotationDate())+ " and request you to provide services from _________________ to ______________",font8);
		PdfPCell quotNoteCell = new PdfPCell(quotNote);
		quotNoteCell.setBorder(0);
		quotNoteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		tablepresmise.addCell(quotNoteCell);
		
		Phrase bllank1 = new Phrase(" ",font9bold);
		PdfPCell bllank1Cell = new PdfPCell(bllank1);
		bllank1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bllank1Cell.setBorder(0);
		tablepresmise.addCell(bllank1Cell);
//		tablepresmise.addCell(bllank1Cell);
		
		
		//  premises details 
		
		
		
		Phrase note = new Phrase("Site Address and Area in(Sq.ft)for Services:",font9bold);
		PdfPCell noteCell = new PdfPCell(note);
		noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		noteCell.setBorder(0);
		tablepresmise.addCell(noteCell);
		
		if(quotation.getPremisesDesc()!= null  && !quotation.getPremisesDesc().equals(""))
		{
			Phrase premise = new Phrase(quotation.getPremisesDesc(),font8);
			PdfPCell premiseCell = new PdfPCell(premise);
			premiseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			premiseCell.setBorder(0);
			tablepresmise.addCell(premiseCell);
		}
		else
		{
			Phrase premise = new Phrase(" ",font8);
			PdfPCell premiseCell = new PdfPCell(premise);
			premiseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			premiseCell.setBorderWidthLeft(0);
			premiseCell.setBorderWidthRight(0);
			premiseCell.setBorderWidthTop(0);
			tablepresmise.addCell(premiseCell);
			tablepresmise.addCell(premiseCell);
			tablepresmise.addCell(premiseCell);
		}
		
		try {
			document.add(tablepresmise);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		//  product details 
		
		float[] columnWidths3={5f,1.5f,3.5f};
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(15f);
		table.setSpacingBefore(15f);
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase productNameHeading = new Phrase("Service",font8bold);
		PdfPCell productNameHeadingCell = new PdfPCell(productNameHeading);
		productNameHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		productNameHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(productNameHeadingCell);
	
		
		Phrase frequencyHeading = new Phrase("Frequency",font8bold);
		PdfPCell frequencyHeadingCell = new PdfPCell(frequencyHeading);
		frequencyHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		frequencyHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(frequencyHeadingCell);
		
		Phrase changesHeading = new Phrase("Service Charges(excluding Service Tax)",font8bold);
		PdfPCell changesHeadingCell = new PdfPCell(changesHeading);
		changesHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		changesHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(changesHeadingCell);
		
		double total=0;
		
		/**
		 * Date : 19-07-2017 BY Anil
		 * Calculating service total price
		 */
		ContractServiceImplementor contractImpl=new ContractServiceImplementor();
		for (int i = 0; i < quotation.getItems().size(); i++) {
			
			Phrase productName = new Phrase(i+1+"."+quotation.getItems().get(i).getProductName(),font8);
			PdfPCell productNameCell = new PdfPCell(productName);
			productNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			table.addCell(productNameCell);
			
			Phrase frequency = new Phrase(quotation.getItems().get(i).getNumberOfServices()+"",font8);
			PdfPCell frequencyCell = new PdfPCell(frequency);
			frequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(frequencyCell);
			
//			Phrase price = new Phrase(quotation.getItems().get(i).getPrice()+"",font8);
			/**
			 * 
			 */
			Phrase price = new Phrase(df.format(contractImpl.getTotalPriceOfService(quotation.getItems().get(i)))+"",font8);
			PdfPCell priceCell = new PdfPCell(price);
			priceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(priceCell);
			
			
			total = total + contractImpl.getTotalPriceOfService(quotation.getItems().get(i));
		}
		
		Phrase blank = new Phrase(" ",font8bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase totalHeading = new Phrase("TOTAL",font8bold);
		PdfPCell totalHeadingCell = new PdfPCell(totalHeading);
		totalHeadingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalHeadingCell.setBackgroundColor(mycolor1);
		table.addCell(totalHeadingCell);
		
		Phrase blankcolour = new Phrase(" ",font8bold);
		PdfPCell blankcolourCell = new PdfPCell(blankcolour);
		blankcolourCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankcolourCell.setBackgroundColor(mycolor1);
		table.addCell(blankcolourCell);
		
		Phrase totalValue = new Phrase(df.format(total)+"",font8bold);
		PdfPCell totalValueCell = new PdfPCell(totalValue);
		totalValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalValueCell.setBackgroundColor(mycolor1);
		table.addCell(totalValueCell);
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//   below product table details 
		PdfPTable noteTable = new PdfPTable(1);
		noteTable.setWidthPercentage(100f);
		
		Phrase note1 = new Phrase("We shall make payment in advance / on completion of services within 7 days of receipt of invoices. "
				+"\n"+ "Invoice for services may be raised on our address above or as below:",font9bold);
		
		PdfPCell note1Cell = new PdfPCell(note1);
		note1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		note1Cell.setBorder(0);
		noteTable.addCell(note1Cell);
		
		
		
		Phrase blankFrom3sides = new Phrase(" ",font8);
		PdfPCell blankFrom3sidesCell = new PdfPCell(blankFrom3sides);
		blankFrom3sidesCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankFrom3sidesCell.setBorderWidthLeft(0);
		blankFrom3sidesCell.setBorderWidthRight(0);
		blankFrom3sidesCell.setBorderWidthTop(0);
		noteTable.addCell(blankFrom3sidesCell);
		noteTable.addCell(blankFrom3sidesCell);
		noteTable.addCell(blankFrom3sidesCell);
		
		
		try {
			document.add(noteTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void ContactPersonDetails() {
		
		Paragraph contact = new Paragraph("Our contact person for your services shall be :",font9bold);
		contact.setAlignment(Element.ALIGN_LEFT);
		
		try {
			document.add(contact);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		float[] columnWidths3={4.5f,1f,4.5f};
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		try {
			table.setWidths(columnWidths3);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase name=new Phrase("Name :"+emp.getFullname(),font8);
		PdfPCell nameCell = new PdfPCell(name);
		nameCell.setBorder(0);
		table.addCell(nameCell);
		
		
		Phrase blank=new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setBorder(0);
		table.addCell(blankCell);
		
		
		Phrase designation =null;
		if(emp.getDesignation()!= null  && !emp.getDesignation().equals("")){
			 designation=new Phrase("Designation :"+emp.getDesignation(),font8);
		}
		else
		{
			 designation=new Phrase(" ",font8);
		}
		
		PdfPCell designationCell = new PdfPCell(designation);
		designationCell.setBorder(0);
		table.addCell(nameCell);
		
		
		Phrase tel=new Phrase("Tel :"+emp.getCellNumber1(),font8);
		PdfPCell telCell = new PdfPCell(tel);
		telCell.setBorder(0);
		table.addCell(telCell);
		
		table.addCell(blankCell);
		
		Phrase email=new Phrase("E-mail :"+emp.getEmail(),font8);
		PdfPCell emailCell = new PdfPCell(email);
		emailCell.setBorder(0);
		table.addCell(emailCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		
		Phrase confirm = new Phrase("We confirm having read and understood terms and conditions of services.",font8);
		PdfPCell confirmCell = new PdfPCell(confirm);
		confirmCell.setBorder(0);
		table.addCell(confirmCell);
		
		table.addCell(blankCell);
		
		Phrase received = new Phrase("Received and accepted.",font8);
		PdfPCell receivedCell = new PdfPCell(received);
		receivedCell.setBorder(0);
		table.addCell(receivedCell);
		
		
		Phrase sincerely = new Phrase("Your sincerely,",font8);
		PdfPCell sincerelyCell = new PdfPCell(sincerely);
		sincerelyCell.setBorder(0);
		table.addCell(sincerelyCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		
		Phrase sign = new Phrase("Signature : ",font8);
		PdfPCell signCell = new PdfPCell(sign);
		signCell.setBorder(0);
		signCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(signCell);
		table.addCell(blankCell);
		table.addCell(signCell);
		
		Phrase custName = new Phrase("Name :"+cust.getFullname(),font8);
		PdfPCell custNameCell = new PdfPCell(custName);
		custNameCell.setBorder(0);
		custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(custNameCell);
		table.addCell(blankCell);
		Phrase NBHCRepresentative = new Phrase("NBHC Representative :"+emp.getFullname(),font8);
		PdfPCell NBHCRepresentativeCell = new PdfPCell(NBHCRepresentative);
		NBHCRepresentativeCell.setBorder(0);
		NBHCRepresentativeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(NBHCRepresentativeCell);
		
		
		Phrase custCell = new Phrase("Contact No. :"+cust.getCellNumber1(),font8);
		PdfPCell custCellCell = new PdfPCell(custCell);
		custCellCell.setBorder(0);
		custCellCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(custCellCell);
		table.addCell(blankCell);
		Phrase NBHCReprePh = new Phrase("Contact No. :"+emp.getCellNumber1(),font8);
		PdfPCell NBHCReprePhCell = new PdfPCell(NBHCReprePh);
		NBHCReprePhCell.setBorder(0);
		NBHCReprePhCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(NBHCReprePhCell);
		
		Phrase custemail = new Phrase("E-mail :"+cust.getEmail(),font8);
		PdfPCell custemailCell = new PdfPCell(custemail);
		custemailCell.setBorder(0);
		custemailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(custemailCell);
		table.addCell(blankCell);
		Phrase NBHCRepreemail = new Phrase("E-mail :"+emp.getEmail(),font8);
		PdfPCell NBHCRepreemailCell = new PdfPCell(NBHCRepreemail);
		NBHCRepreemailCell.setBorder(0);
		NBHCRepreemailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(NBHCRepreemailCell);
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		Phrase stamp = new Phrase("Date & Stamp :",font8);
		PdfPCell stampCell = new PdfPCell(stamp);
		stampCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stampCell.setBorder(0);
		table.addCell(stampCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void CreateHeading() {
		
		Phrase heading = new Phrase("Letter Of Intent for Pest Management / Fumigation Services" ,font12bold );
		PdfPCell headingCell = new PdfPCell(heading);
		headingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headingCell.setBorder(0);
		headingCell.setBackgroundColor(mycolor);
		headingCell.setFixedHeight(20f);
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.addCell(headingCell);
		table.setSpacingAfter(10f);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			Image image2 =Image.getInstance("images/NBHC_Header.jpg");
			image2.scalePercent(10f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 770f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
//			Image image2 =Image.getInstance("images/NBHC_Footer.png");
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 10f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private void createBlankforUPC() {
		Phrase blankphrase = new Phrase("", font8);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
//			document.add(blank);
//			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
