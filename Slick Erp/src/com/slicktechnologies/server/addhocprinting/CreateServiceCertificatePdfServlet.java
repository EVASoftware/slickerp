package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
public class CreateServiceCertificatePdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5111342912465884836L;

	Logger logger = Logger.getLogger("NameOfYourLogger");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			 String stringid = req.getParameter("Id");
			 stringid = stringid.trim();
			 Long count = Long.parseLong(stringid);
			 
			 System.out.println("ID"+count);
			
			 String stringdate = req.getParameter("date");
			 
			 String[] dataRetrieved=stringdate.split("[$]");
				
			  String date=dataRetrieved[0];
			 
			 System.out.println("VALID UNTILL DATE ::: "+date);
			 logger.log(Level.SEVERE, "valid until date form popup"+date);
			 ServiceCertificatePdf servicepdf = new ServiceCertificatePdf();

			servicepdf.document = new Document();
			Document document = servicepdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream()); // write

			document.open();
			
			servicepdf.setService(count,date);
			 String preprintStatus=req.getParameter("preprint");
			   if(preprintStatus.contains("yes")){
				   
				   servicepdf.createPdf(date,preprintStatus);
			   }
			   else  if(preprintStatus.contains("no")){
				   
				   servicepdf.createPdf(date,preprintStatus);
			   }
			   else{
				   servicepdf.createPdf(date,preprintStatus);
			   }
			
			
			
			
		
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
