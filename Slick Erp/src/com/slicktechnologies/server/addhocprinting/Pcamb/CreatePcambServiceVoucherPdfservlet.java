package com.slicktechnologies.server.addhocprinting.Pcamb;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gargoylesoftware.htmlunit.Page;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.Pcamb.CreatePcambQuotationServlet.Rotate;


public class CreatePcambServiceVoucherPdfservlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -327755999184484008L;

	
	
	
	 public class Rotate extends PdfPageEventHelper {
	        protected PdfNumber rotation = PdfPage.PORTRAIT;
	        public void setRotation(PdfNumber rotation) {
	            this.rotation = rotation;
	        }
	        public void onEndPage(PdfWriter writer, Document document) {
	            writer.addPageDictEntry(PdfName.ROTATE, rotation);
	        }
	    }
	

	protected  void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException ,IOException 
	  {
		    response.setContentType("application/pdf");
	  
	  try{
		  
		  
		  
		   String stringid = request.getParameter("Id");
			stringid = stringid.trim();            
			Long count = Long.parseLong(stringid);
			
			PcambServiceVoucherPdf pdf = new PcambServiceVoucherPdf();
			
			pdf.document= new Document(PageSize.A4);
//			pdf.document= new Document(PageSize.A5.rotate(),25,25,25,25);
			  Document document = pdf.document;
			  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
			  Rotate rotation = new Rotate();
		      writer.setPageEvent(rotation);
			 
			  document.open();
			  pdf.setContractserviceVoucher(count);
			  pdf.createPdf(); 
			  rotation.setRotation(PdfPage.PORTRAIT);
			  document.close();			    
	  }catch (DocumentException e) {
		  e.printStackTrace();
	  }
	}
}
