package com.slicktechnologies.server.addhocprinting.Pcamb;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambServiceVoucherPdf {
	public Document document;
	Company comp;
	Contract con;
	Customer cust;
	Service ser;
	Invoice invoiceentity;
	PdfPTable voucherDetails1;
	List<CustomerBranchDetails> custbranchlist;
    String	val;
    int j;
    boolean maintainflag=false;
	/**
	 * Date : 18-12-2017 BY ANIL
	 */
	List<ContactPersonIdentification> custContactList;
	
	private Font font16boldul, font12bold, font8bold, font9bold, font8, font9,
			font12boldul, font10boldul, font12, font16bold, font10, font10bold,
			font9boldul, font10ul, font14bold, font7, font7bold, font9red,
			font9boldred, font12boldred, font16, font23, font20;

	private SimpleDateFormat fmt1 = new SimpleDateFormat("dd MMM yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	Logger logger = Logger.getLogger("PcambServiceVoucherPdf.class");
	Address serAdd = new Address();
	/**
	 * nidhi
	 * 23-06-2018
	 * 
	 */
	CustomerBranchDetails custBranch = null;
	private SimpleDateFormat fmt2 = new SimpleDateFormat("MM-yyyy");
	public PcambServiceVoucherPdf() {
		BaseFont base = null;
		try {
			base = BaseFont.createFont("fonts\\VERDANA.TTF", BaseFont.WINANSI,BaseFont.EMBEDDED);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		font16boldul = new Font(base, 16, Font.BOLD | Font.UNDERLINE);
		font16bold = new Font(base, 16, Font.BOLD);
		font16 = new Font(base, 16);
		font12bold = new Font(base, 12, Font.BOLD);
		font8bold = new Font(base, 8, Font.BOLD);
		font8 = new Font(base, 8);
		font12boldul = new Font(base, 12, Font.BOLD | Font.UNDERLINE);
		font12 = new Font(base, 12);
		font12boldred = new Font(base, 12, Font.BOLD, BaseColor.RED);
		font10 = new Font(base, 10);
		font10bold = new Font(base, 10, Font.BOLD);
		font14bold = new Font(base, 14, Font.BOLD);
		font10boldul = new Font(base, 10, Font.BOLD | Font.UNDERLINE);
		font9bold = new Font(base, 9, Font.BOLD);
		font9boldred = new Font(base, 9, Font.BOLD, BaseColor.RED);

		// font9 = new Font(base, 9);
		font9 = new Font(base, 9);
		font10ul = new Font(base, 10, Font.UNDERLINE);
		font9red = new Font(base, 9, Font.NORMAL, BaseColor.RED);
		font7 = new Font(base, 7);
		font7bold = new Font(base, 7, Font.BOLD);
		font23 = new Font(base, 23);
		font20 = new Font(base, 20);
		font9boldul = new Font(base, 9, Font.BOLD | Font.UNDERLINE);
	}

	public void setContractserviceVoucher(Long count) {

		// Load Contract
		con = ofy().load().type(Contract.class).id(count).now();

		// load Company
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", con.getCompanyId()).first().now();
		// System.out.println("company loaded "+comp.getCellNumber2());
		// Load Invoice

		if (con.getCompanyId() == null)
			invoiceentity = ofy().load().type(Invoice.class).first().now();
		else
			invoiceentity = ofy().load().type(Invoice.class)
					.filter("companyId", con.getCompanyId()).first().now();

		// load Customer
		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("companyId", con.getCompanyId())
					.filter("count", con.getCustomerId()).first().now(); 

		
		    //load customer branch
		
				if(con.getCompanyId() == null){
					custbranchlist = ofy().load().type(CustomerBranchDetails.class).list();
				}else
				{
					custbranchlist =ofy().load().type(CustomerBranchDetails.class).filter("companyId", con.getCompanyId()).filter("cinfo.count", con.getCustomerId()).list();
				}
				
				System.out.println(custbranchlist);
				
		// load Service

		if (con.getCompanyId() == null)
			ser = ofy().load().type(Service.class).first().now();

		else

			ser = ofy().load().type(Service.class)
					.filter("companyId", con.getCompanyId())
					.filter("contractCount", con.getCount()).first().now();

		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt2.setTimeZone(TimeZone.getTimeZone("IST"));
		/**
		 * Date : 18-12-2017 BY ANIL
		 * Loading Customer's contact persons list
		 */
		if(cust!=null){
			custContactList=ofy().load().type(ContactPersonIdentification.class).filter("companyId", cust.getCompanyId()).filter("entype","Customer").filter("personInfo.count", cust.getCount()).list();
			if(custContactList!=null&&custContactList.size()!=0){
				Comparator<ContactPersonIdentification> idComparator = new Comparator<ContactPersonIdentification>() {  
					@Override  
					public int compare(ContactPersonIdentification o1, ContactPersonIdentification o2) { 
						Integer count1=o1.getCount();
						Integer count2=o2.getCount();
						return count1.compareTo(count2);  
					}  
				};  
				Collections.sort(custContactList,idComparator);
			}
		}
	}

	public void createPdf() {/*

		for (int i = 0; i < con.getItems().size(); i++) {
			*//**
			 * nidhi
			 * 13-06-2018
			 * 
			 *//*
			if(con.getItems().get(i).getBranchSchedulingInfo() != null){
				val = con.getItems().get(i).getBranchSchedulingInfo();
				String arr[] =val.split("#");
				System.out.println("LLLLLLLLL5555"+val);
				
				if(custbranchlist.size()!=0)
				{
				        for(int k=1 ;k<arr.length;k=k+3)
				        {
				            if(maintainflag==false)
				                {
				                   if( arr[k].equalsIgnoreCase("Yes"))
				                   {
					                 CreateHeader1();  
					                 voucherDetails(con.getItems().get(i).getProductName());
					                 CreateHeader2();
					                 voucherDetails(con.getItems().get(i).getProductName());
					                 maintainflag=true;
					                 try {
											document.add(Chunk.NEXTPAGE);
										 } catch (DocumentException e) {
											e.printStackTrace();
								            }
				                   }
				              }
				         else{
					          for( j=0; j<custbranchlist.size(); j++)
					           {
					    	      if(k<arr.length){
								      if(arr[k].equalsIgnoreCase("yes"))
								      {
									   CreateHeader1();  
									   voucherDetails(con.getItems().get(i).getProductName());
									   CreateHeader2();
									   voucherDetails(con.getItems().get(i).getProductName());
									   try {
											document.add(Chunk.NEXTPAGE);
										 } catch (DocumentException e) {
											e.printStackTrace();
								            }
									   
									  }
						           }
					          }
			              }


//				
//			    System.out.println("LLLLLLLLL5555"+val);
//			
//					if(k<arr.length){
//						
//					  if( arr[k].equalsIgnoreCase("Yes")){
//						  
//					   CreateHeader1();  
//					   voucherDetails(con.getItems().get(i).getProductName());
//					   CreateHeader2();
//					   voucherDetails(con.getItems().get(i).getProductName());
//						k=k+3;
//						 maintainflag=true;
//						
//						
//						 try {
//								document.add(Chunk.NEXTPAGE);
//							} catch (DocumentException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//					              }
//					     }
//				     }
//				  }
		       }
	     }
				
			else{  
				    CreateHeader1();
					voucherDetails(con.getItems().get(i).getProductName());
					
					CreateHeader2();
					voucherDetails(con.getItems().get(i).getProductName());
				}
			}else{
				
			}
			
			
			
			 try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
		            }
			
		 }
	*/
		
	
	
	
HashMap<Integer,String> proDetail = new HashMap<Integer,String>();
		
		HashSet<String> branchSet = new HashSet<String>();
		
		for (int i=0 ; i< con.getItems().size(); i++)
		{
			
			Set<Integer> ser =  con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet();
			
			for(Integer seq : ser){
				ArrayList<BranchWiseScheduling> branch =  con.getItems().get(i).getCustomerBranchSchedulingInfo().get(seq);
				for(int k =0 ;k<branch.size();k++){
					if(!branch.get(k).getBranchName().equalsIgnoreCase("Service Address")
							&& !branch.get(k).getBranchName().equalsIgnoreCase("Select")  && branch.get(k).isCheck())
					{
						branchSet.add(branch.get(k).getBranchName());
					}
				}
				
			}
		}
		
//		List<CustomerBranchDetails> custBranchList = new ArrayList<CustomerBranchDetails>();
		List<CustomerBranchDetails> custBranchList = null; //vijay

		ArrayList <String> branchList = new  ArrayList<String>();
		if(branchSet.size()>0){
			branchList.addAll(branchSet);
			
			custBranchList = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName IN",  branchList).filter("cinfo.count", con.getCinfo().getCount())
			   .filter("companyId",con.getCompanyId()).list();
			
		}
		
		ArrayList<BranchWiseScheduling> custDt = new ArrayList<BranchWiseScheduling>();
		if(custBranchList != null){
			
			for (int i=0 ; i< con.getItems().size(); i++)
			 {
				if( con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet().size()>0){
					
					for(Map.Entry<Integer, ArrayList<BranchWiseScheduling>> mapList : con.getItems().get(i).getCustomerBranchSchedulingInfo().entrySet()){
						
						custDt = mapList.getValue();
						for(BranchWiseScheduling brandDt  : custDt){
							if((brandDt.getBranchName().equalsIgnoreCase("Service Address")
							|| brandDt.getBranchName().equalsIgnoreCase("Select")) &&   brandDt.isCheck()){
									
								/**
								 * Date 28-06-2018 By Vijay
								 * Des :- if contract doest not contain customer service address then service adddress will print from customer
								 */
								if(con.getCustomerServiceAddress()==null){
										serAdd = cust.getSecondaryAdress();
									}else{
										serAdd =  con.getCustomerServiceAddress();
								}
								/**
								 * ends here
								 */
											
								custBranch = null;
									CreateHeader1();  
					                 voucherDetails(con.getItems().get(i).getProductName(),"");
					                 CreateHeader2();
					                 voucherDetails(con.getItems().get(i).getProductName(),"");
					                 maintainflag=true;
								try {
									document.add(Chunk.NEXTPAGE);
								} catch (DocumentException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else{
								if(!brandDt.getBranchName().equalsIgnoreCase("Service Address")
										&& !brandDt.getBranchName().equalsIgnoreCase("Select") &&   brandDt.isCheck()){
										for(CustomerBranchDetails custBranchDt : custBranchList){
											if(custBranchDt.getBusinessUnitName().equals(brandDt.getBranchName())){
												custBranch = custBranchDt;
												
												if(custBranchDt.getAddress()!=null){
													serAdd = custBranchDt.getAddress();
												}else{
													serAdd =  con.getCustomerServiceAddress();
												}
												  CreateHeader1();  
									                 voucherDetails(con.getItems().get(i).getProductName(),custBranchDt.getBusinessUnitName()+"\n");
									                 CreateHeader2();
									                 voucherDetails(con.getItems().get(i).getProductName(),custBranchDt.getBusinessUnitName()+"\n");
//									                 maintainflag=true;
												try {
													document.add(Chunk.NEXTPAGE);
												} catch (DocumentException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												break;
											}
										}
								}
							
							}
						}
						
					}
				}
			 }
			
		}else{
			
			for (int i=0 ; i< con.getItems().size(); i++)
			 {
				{
					/**
					 * Date 28-06-2018 By Vijay
					 * Des :- if contract doest not contain customer service address then service adddress will print from customer
					 */
					if(con.getCustomerServiceAddress()!=null){
						serAdd =  con.getCustomerServiceAddress();
					}else{ 
						if(cust!=null)
						serAdd = cust.getSecondaryAdress();
					}
					/**
					 * ends here
					 */
				}
				  	 CreateHeader1();  
	                 voucherDetails(con.getItems().get(i).getProductName(),"");
	                 CreateHeader2();
	                 voucherDetails(con.getItems().get(i).getProductName(),"");
//	                 maintainflag=true;
	        	
	        	try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	
			 }
		}
	
	}
	

	private void CreateHeader1()	{
		
		
		// System.out.println("landline: " +comp.getLandline());
		// System.out.println("Cell no 2: " +comp.getCellNumber2());
		// System.out.println("fax No: " +comp.getFaxNumber());

	
		Phrase myHeader1 = new Phrase(""+ comp.getBusinessUnitName().toUpperCase(), font10bold);
		
		Phrase contactInfo = new Phrase("Tel.:(91-22) 2266 1091, 2262 5376 Fax :2266 0810 ", font10ul);
		// ///////////////////////////// END HERE
		// ////////////////////////////////
		
		Phrase Pcamb_label=new Phrase("\n"+"PCAMB Copy",font10bold);
		

		Paragraph headerPara = new Paragraph();

		
//		headerPara.add(Chunk.NEWLINE);
//		headerPara.add(Chunk.NEWLINE);
		headerPara.add(Chunk.NEWLINE);


		
		PdfPCell hcell1=new PdfPCell(myHeader1);
		hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		hcell1.setBorder(0);
		
		
		PdfPCell hcell2=new PdfPCell(contactInfo);
		hcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		hcell2.setBorder(0);
		
		PdfPCell hcell3=new PdfPCell(Pcamb_label);
		hcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		hcell3.setBorder(0);

		Phrase blnkphrse = new Phrase("", font10);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnkphrse);
		blnkCell.setBorder(0);

		Phrase blnk1phrse = new Phrase("", font10);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1phrse);
		blnk1Cell.setBorder(0);

		PdfPTable headertbl = new PdfPTable(1);
		headertbl.setWidthPercentage(100);
		headertbl.setSpacingAfter(20);
		
//		PdfPCell myheadercell=new PdfPCell(myHeader1);
//		myheadercell.setBorder(0);
		
		
		
		headertbl.addCell(hcell1);
		headertbl.addCell(hcell2);
		headertbl.addCell(hcell3);
		
		PdfPCell myheader = new PdfPCell(myHeader1);
	
		myheader.setBorder(0);
		
		PdfPCell mycont = new PdfPCell(contactInfo);
	
		mycont.setBorder(0);

		
		try {
			headertbl.setWidths(new float[] { 10f});
			document.add(headertbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private void CreateHeader2(){

		
		Phrase myHeader1 = new Phrase(""
				+ comp.getBusinessUnitName().toUpperCase(), font10bold);
	
		Phrase contactInfo = new Phrase(
				"Tel.:(91-22) 2266 1091, 2262 5376 Fax :2266 0810 ", font10ul);
		
		Phrase Client_label=new Phrase("\n"+"Client Copy",font10bold);
	

		Paragraph headerPara = new Paragraph();

		
//	
		headerPara.add(Chunk.NEWLINE);

		
		PdfPCell hcell1=new PdfPCell(myHeader1);
		hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		hcell1.setBorder(0);
		
		
		PdfPCell hcell2=new PdfPCell(contactInfo);
		hcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
		hcell2.setBorder(0);


		PdfPCell hcell3=new PdfPCell(Client_label);
		hcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
		hcell3.setBorder(0);
		
		Phrase blnkphrse = new Phrase("", font10);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnkphrse);
		blnkCell.setBorder(0);

		Phrase blnk1phrse = new Phrase("", font10);
		PdfPCell blnk1Cell = new PdfPCell();
		blnk1Cell.addElement(blnk1phrse);
		blnk1Cell.setBorder(0);

		PdfPTable headertbl = new PdfPTable(1);
		headertbl.setWidthPercentage(100);
		headertbl.setSpacingAfter(20);
		
//		PdfPCell myheadercell=new PdfPCell(myHeader1);
//		myheadercell.setBorder(0);
		
		
		
		headertbl.addCell(hcell1);
		headertbl.addCell(hcell2);
		headertbl.addCell(hcell3);
		
		PdfPCell myheader = new PdfPCell(myHeader1);
	
		myheader.setBorder(0);
		
		PdfPCell mycont = new PdfPCell(contactInfo);
	
		mycont.setBorder(0);

		
		try {
			headertbl.setWidths(new float[] { 10f});
			document.add(headertbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	
	}

	private void voucherDetails(String productName,String branchName) {

		voucherDetails1 = new PdfPTable(4);
		voucherDetails1.setWidthPercentage(100);
		try {

//			voucherDetails1.setWidths(new float[] { 3f,0.2f, 5f, 0.8f});
			voucherDetails1.setWidths(new float[] {2.6f,0.2f, 4.6f, 1.2f});			

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		Phrase blnkphrse = new Phrase("", font10);
		PdfPCell blnkCell = new PdfPCell();
		blnkCell.addElement(blnkphrse);
		blnkCell.setBorder(0);

		String salutation = "";
		if (cust.getSalutation() != null && !cust.getSalutation().equals("")) {
			salutation = cust.getSalutation();
		} else {
			salutation = "";
		}
		String custNamestr = "";

		/**
		 * Date 28-jun-2018 By Vijay
		 * Des :- as per Sonu always print customer Name
		 */
//		if(custBranch != null){
//			custNamestr = custBranch.getBusinessUnitName();
//			
//		}else{
			if (!salutation.equals("")) {
				if (ser != null) {
					custNamestr = salutation + " "+ ser.getPersonInfo().getFullName();
				} else {
					custNamestr = salutation + " " + con.getCustomerFullName();
				}
			} else {
				if (ser != null) {
					custNamestr = ser.getPersonInfo().getFullName();
				} else {
					custNamestr = con.getCustomerFullName();
				}
			}
//		}
		
		/*
		 * nameInFirstLetterUpperCase defined by Ajinkya suggested by Rahul V
		 * Used to take customer name from Uppercase to Only first letter
		 * Uppercase
		 */
		logger.log(Level.SEVERE, "Customer Name" + custNamestr.trim());
		String nameInFirstLetterUpperCase = getFirstLetterUpperCase(custNamestr.trim());

		// //////////////////// end here /////////////////

		Phrase custNamePhrse = new Phrase(nameInFirstLetterUpperCase, font10);
		PdfPCell custNameCell = new PdfPCell(custNamePhrse);
//		custNameCell.addElement();
		custNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custNameCell.setBorder(0);

		Calendar calobj = Calendar.getInstance();
		Phrase datePhrs = new Phrase("DATE : " + fmt1.format(calobj.getTime()), font10);
		PdfPCell dateCell = new PdfPCell(datePhrs);
		dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dateCell.setBorder(0);

		String strfrdigitCardNo = ""+ con.getCount()%10000 ; 
		 
		 if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==2){
			 strfrdigitCardNo = "00"+strfrdigitCardNo;
		 }
		 else if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==3){
			 strfrdigitCardNo = "0"+strfrdigitCardNo; 
		 }
		 System.out.println("4 digit " +strfrdigitCardNo);
		
		Phrase voucherPhrs = new Phrase("VOUCHER NO : ", font10);
		/*if (ser != null) {
			voucherPhrs = new Phrase("VOUCHER NO : " + ser.getContractCount(),font9);
		} else */{
			voucherPhrs = new Phrase("VOUCHER NO : " + con.getGroup()+"/"+fmt2.format(con.getStartDate())+"/"+strfrdigitCardNo,font9);
		}
		PdfPCell voucherCell = new PdfPCell(voucherPhrs);
		voucherCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		voucherCell.setBorder(0);

//		voucherDetails1.addCell(blnkCell);
//		voucherDetails1.addCell(blnkCell);
		
		voucherDetails1.addCell(custNameCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(dateCell);
		voucherDetails1.addCell(blnkCell);

//		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(voucherCell);
		voucherDetails1.addCell(blnkCell);
		

		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		voucherDetails1.addCell(blnkCell);
		
		try {
			document.add(voucherDetails1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		PdfPTable premisesDetails = new PdfPTable(4);
		premisesDetails.setWidthPercentage(100);

	
		Phrase premisesLbl = new Phrase("Premises to be Treated", font10);
		PdfPCell PremiseslblCell = new PdfPCell(premisesLbl);
		
		PremiseslblCell.setBorder(0);
		
		Phrase col=new Phrase(":");
		PdfPCell colcell=new PdfPCell(col);
		colcell.setBorder(0);

		// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		
//		logger.log(Level.SEVERE,"BBBBranchSchedulingInfo"+val);
		String addressline = "";
		String localityline = "";
		Phrase serAdrs = new Phrase("");
		
//		String arr[] =val.split("#");
		
		
		
//		if(maintainflag==false)
		{
//		        if (ser != null) {
//			      if (ser.getAddress().getAddrLine2().equals(null)
//					&& ser.getAddress().getAddrLine2().equals("")) {
//				addressline = ser.getAddress().getAddrLine1() + ", " + "\n"
//						+ ser.getAddress().getAddrLine2() + ", " + "\n";
//			     } else {
//				  addressline = ser.getAddress().getAddrLine1() + ", " + "\n";
//			   }
//
//			   if ((!ser.getAddress().getLandmark().equals(""))
//					&& (ser.getAddress().getLocality().equals("") == false)) {
//				System.out.println("inside both null condition1");
//				localityline = (ser.getAddress().getLandmark() + ", "
//						+ ser.getAddress().getLocality() + ", " + "\n"
//						+ ser.getAddress().getCity() + "- "
//						+ ser.getAddress().getPin() + ", " + "\n"
//						+ ser.getAddress().getState() + ", "
//						+ ser.getAddress().getCountry() + ". ");
//			} else if ((!ser.getAddress().getLandmark().equals(""))
//					&& (ser.getAddress().getLocality().equals("") == true)) {
//				System.out.println("inside both null condition 2");
//				localityline = (ser.getAddress().getLandmark() + ", " + "\n"
//						+ ser.getAddress().getCity() + " - "
//						+ ser.getAddress().getPin() + ", " + "\n"
//						+ ser.getAddress().getState() + ", "
//						+ ser.getAddress().getCountry() + ". ");
//			}
//
//			else if ((ser.getAddress().getLandmark().equals(""))
//					&& (ser.getAddress().getLocality().equals("") == false)) {
//				System.out.println("inside both null condition 3");
//				localityline = (ser.getAddress().getLocality() + ", " + "\n"
//						+ ser.getAddress().getCity() + " - "
//						+ ser.getAddress().getPin() + ", " + "\n"
//						+ ser.getAddress().getState() + ", "
//						+ ser.getAddress().getCountry() + ". ");
//			} else if ((ser.getAddress().getLandmark().equals(""))
//					&& (ser.getAddress().getLocality().equals("") == true)) {
//				System.out.println("inside both null condition 4");
//				localityline = (ser.getAddress().getCity() + ", "
//						+ ser.getAddress().getPin() + ", " + "\n"
//						+ ser.getAddress().getState() + ", "
//						+ ser.getAddress().getCountry() + ". ");
//				
//			}
//		  }
			
			
			
			if (serAdd != null) {/*
			      if (cust.getAdress().getAddrLine2().equals(null)
					&& cust.getAdress().getAddrLine2().equals("")) {
				addressline = cust.getAdress().getAddrLine1() + ", " + "\n"
						+ cust.getAdress().getAddrLine2() + ", " + "\n";
			     } else {
				  addressline = cust.getAdress().getAddrLine1() + ", " + "\n";
			   }

			   if ((!cust.getAdress().getLandmark().equals(""))
					&& (cust.getAdress().getLocality().equals("") == false)) {
				System.out.println("inside both null condition1");
				localityline = (cust.getAdress().getLandmark()+ ", "
						+ cust.getAdress().getLocality()+ ", " + "\n"
						+ cust.getAdress().getCity() + "- "
						+ cust.getAdress().getPin() + ", " + "\n"
						+ cust.getAdress().getState() + ", "
						+ cust.getAdress().getCountry() + ". ");
			} else if ((!cust.getAdress().getLandmark().equals(""))
					&& (cust.getAdress().getLocality().equals("") == true)) {
				System.out.println("inside both null condition 2");
				localityline = (cust.getAdress().getLandmark() + ", " + "\n"
						+ cust.getAdress().getCity() + " - "
						+ cust.getAdress().getPin() + ", " + "\n"
						+ cust.getAdress().getState() + ", "
						+ cust.getAdress().getCountry()  + ". ");
			}

			else if ((cust.getAdress().getLandmark().equals(""))
					&& (cust.getAdress().getLocality().equals("") == false)) {
				System.out.println("inside both null condition 3");
				localityline = (cust.getAdress().getLocality() + ", " + "\n"
						+ cust.getAdress().getCity()  + " - "
						+ cust.getAdress().getPin() + ", " + "\n"
						+ cust.getAdress().getState() + ", "
						+ cust.getAdress().getCountry() + ". ");
			} else if ((cust.getAdress().getLandmark().equals(""))
					&& (cust.getAdress().getLocality().equals("") == true)) {
				System.out.println("inside both null condition 4");
				localityline = (cust.getAdress().getCity() + ", "
						+ cust.getAdress().getPin() + ", " + "\n"
						+ cust.getAdress().getState() + ", "
						+ cust.getAdress().getCountry() + ". ");
			}
		  */
				

			      if (serAdd.getAddrLine2().equals(null)
					&& serAdd.getAddrLine2().equals("")) {
				addressline = serAdd.getAddrLine1() + ", " + "\n"
						+ serAdd.getAddrLine2() + ", " + "\n";
			     } else {
				  addressline = serAdd.getAddrLine1() + ", " + "\n";
			   }

			   if ((!serAdd.getLandmark().equals(""))
					&& (serAdd.getLocality().equals("") == false)) {
				System.out.println("inside both null condition1");
				localityline = (serAdd.getLandmark()+ ", "
						+ serAdd.getLocality()+ ", " + "\n"
						+ serAdd.getCity() + "- "
						+ serAdd.getPin() + ", " + "\n"
						+ serAdd.getState() + ", "
						+ serAdd.getCountry() + ". ");
			} else if ((!serAdd.getLandmark().equals(""))
					&& (serAdd.getLocality().equals("") == true)) {
				System.out.println("inside both null condition 2");
				localityline = (serAdd.getLandmark() + ", " + "\n"
						+ serAdd.getCity() + " - "
						+ serAdd.getPin() + ", " + "\n"
						+ serAdd.getState() + ", "
						+ serAdd.getCountry()  + ". ");
			}

			else if ((serAdd.getLandmark().equals(""))
					&& (serAdd.getLocality().equals("") == false)) {
				System.out.println("inside both null condition 3");
				localityline = (serAdd.getLocality() + ", " + "\n"
						+ serAdd.getCity()  + " - "
						+ serAdd.getPin() + ", " + "\n"
						+ serAdd.getState() + ", "
						+ serAdd.getCountry() + ". ");
			} else if ((serAdd.getLandmark().equals(""))
					&& (serAdd.getLocality().equals("") == true)) {
				System.out.println("inside both null condition 4");
				localityline = (serAdd.getCity() + ", "
						+ serAdd.getPin() + ", " + "\n"
						+ serAdd.getState() + ", "
						+ serAdd.getCountry() + ". ");
			}
		  }
			
			
			
//		        addressline = cust.getAdress().getAddrLine1() + ", " + "\n"+cust.getAdress().getAddrLine2()+","
//		        		+"\n"+cust.getAdress().getLocality()+ ", " + "\n"+cust.getAdress().getCity()+ ", " + "\n"+
//		        		cust.getAdress().getPin()+ ", " + "\n"+cust.getAdress().getState()+ ", " + "\n"+
//		        		cust.getAdress().getCountry();
		        
		        serAdrs = new Phrase(branchName+addressline + localityline, font10);
//		        serAdrs = new Phrase(addressline, font10);
		}
		/*else{			

			addressline =serAdd.getAddrLine1()
					+","+"\n"+serAdd.getAddrLine2()+","+"\n"+
					serAdd.getLandmark()+custbranchlist.get(j).getAddress().getCity()+","+"\n"
					+serAdd.getState()+","+"\n"+
					serAdd.getCountry()+","+"\n";
			
			serAdrs = new Phrase( custbranchlist.get(j)+"\n"+addressline, font10);
//			j=j+1;
			
		}*/
		
         
			
			// ///////////////////////////////////////////////////////////////////////////////////////////////

			String salutation1 = "";
			if (cust.getSalutation() != null
					&& !cust.getSalutation().equals("")) {
				salutation1 = cust.getSalutation();
			} else {
				salutation1 = "";
			}

			
			/**
			 * Date 28-jun-2018 By vijay added if condition
			 * Des :- If customer branches selected in contract at product level then customer branche Name will print
			 * else customer name will print old code
			 */
			String serCustName = "";
			
			if(custBranch != null){
				serCustName = custBranch.getBusinessUnitName();
			}else{
				if (!salutation.equals("") && salutation != null) {

					if (ser != null) {
						serCustName = salutation1 + " " + ser.getCustomerName();
					} else {
						serCustName = salutation1 + " " + con.getCustomerFullName();
					}
				} else {
					if (ser != null) {
						serCustName = ser.getCustomerName();
					} else {
						serCustName = con.getCustomerFullName();
					}
				}
			}	
			/**
			 * ends here
			 */
			
			/*
			 * nameInFirstLetterUpperCase defined by Ajinkya suggested by Rahul
			 * V Used to take customer name from Uppercase to Only first letter
			 * Uppercase
			 */
			String nameInFirstLetterUpperCase1 = getFirstLetterUpperCase(serCustName
					.trim()); // 2
			// //////////////////// end here /////////////////
			
			String name= nameInFirstLetterUpperCase1.substring(0,3);
			if(name.equals("M S"))
			{
				nameInFirstLetterUpperCase1.replace(name,"");
			}

			Phrase custName = new Phrase(nameInFirstLetterUpperCase1, font10);
			
//			premisesDetails.addCell(premisesValCell);
//			premisesDetails.addCell(blnkCell);
			
			PdfPCell premisesValCell = new PdfPCell(custName);
			premisesValCell.setBorder(0);
			PdfPCell addrerssCell = new PdfPCell(serAdrs);
			addrerssCell.setBorder(0);

//			premisesDetails.addCell(blnkCell);
			premisesDetails.addCell(PremiseslblCell);
			premisesDetails.addCell(colcell);
			premisesDetails.addCell(premisesValCell);
			premisesDetails.addCell(blnkCell);
			premisesDetails.addCell(blnkCell);
			premisesDetails.addCell(blnkCell);
			premisesDetails.addCell(addrerssCell);
			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(blnkCell);
			
			
//		    Phrase newadd=new Phrase(custName +"\n"+serAdrs,font10);
//			PdfPCell custcell=new PdfPCell(newadd);
////			PdfPCell adresscell=new PdfPCell(serAdrs);
			
//
//			premisesValCell.addElement(custName);
//			premisesValCell.addElement(serAdrs);
//			premisesValCell.setBorder(0);
//
////			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(PremiseslblCell);
//			premisesDetails.addCell(colcell);
//			premisesDetails.addCell(premisesValCell);
//			premisesDetails.addCell(blnkCell);
////			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(blnkCell);

		
            
            //*******Manisha*********//////////////
            
//            
//            else {
//			// /////////////////////////////////////////////////////////
//
//			// //////////////////// rohan suggested that take this address from
//			// customer //////////////////////
//
//			// take service addrsss from customer or contract
//			if (cust.getSecondaryAdress().getAddrLine2() != null
//					&& !cust.getSecondaryAdress().getAddrLine2().equals("")) {
//				addressline = cust.getSecondaryAdress().getAddrLine1() + ", "
//						+ cust.getSecondaryAdress().getAddrLine2() + ", "
//						+ "\n";
//			} else {
//				addressline = cust.getSecondaryAdress().getAddrLine1() + ", "
//						+ "\n";
//			}
//
//			if ((!cust.getSecondaryAdress().getLandmark().equals(""))
//					&& (cust.getSecondaryAdress().getLocality().equals("") == false)) {
//				System.out.println("inside both null condition1");
//				localityline = (cust.getSecondaryAdress().getLandmark() + ", "
//						+ cust.getSecondaryAdress().getLocality() + ", " + "\n"
//						+ cust.getSecondaryAdress().getCity() + " - "
//						+ cust.getSecondaryAdress().getPin() + ", " + "\n"
//						+ cust.getSecondaryAdress().getState() + ", "
//						+ cust.getSecondaryAdress().getCountry() + ".");
//			} else if ((!cust.getSecondaryAdress().getLandmark().equals(""))
//					&& (cust.getSecondaryAdress().getLocality().equals("") == true)) {
//				System.out.println("inside both null condition 2");
//				localityline = (cust.getSecondaryAdress().getLandmark() + ", "
//						+ "\n" + cust.getSecondaryAdress().getCity() + " - "
//						+ cust.getSecondaryAdress().getPin() + ", " + "\n"
//						+ cust.getSecondaryAdress().getState() + ", "
//						+ cust.getSecondaryAdress().getCountry() + ". ");
//			}
//
//			else if ((cust.getSecondaryAdress().getLandmark().equals(""))
//					&& (cust.getSecondaryAdress().getLocality().equals("") == false)) {
//				System.out.println("inside both null condition 3");
//				localityline = (cust.getSecondaryAdress().getLocality() + ", "
//						+ "\n" + cust.getSecondaryAdress().getCity() + " - "
//						+ cust.getSecondaryAdress().getPin() + ", " + "\n"
//						+ cust.getSecondaryAdress().getState() + ", "
//						+ cust.getSecondaryAdress().getCountry() + ". ");
//			} else if ((cust.getSecondaryAdress().getLandmark().equals(""))
//					&& (cust.getSecondaryAdress().getLocality().equals("") == true)) {
//				System.out.println("inside both null condition 4");
//				localityline = (cust.getSecondaryAdress().getCity() + ", "
//						+ cust.getSecondaryAdress().getPin() + ", " + "\n"
//						+ cust.getSecondaryAdress().getState() + ", "
//						+ cust.getSecondaryAdress().getCountry() + ". ");
//			}
//
//			serAdrs = new Phrase(addressline + localityline, font10);
//			// /////////////////////////////////////////////////////////
//
//			String salutation1 = "";
//
//			if (cust.getSalutation() != null
//					&& !cust.getSalutation().equals("")) {
//				salutation1 = cust.getSalutation();
//			} else {
//				salutation1 = "";
//			}
//
//			String serCustName = "";
//			if (!salutation.equals("") && salutation != null) {
//
//				if (ser != null) {
//					serCustName = salutation1 + " " + ser.getCustomerName();
//				} else {
//					serCustName = salutation1 + " " + con.getCustomerFullName();
//				}
//			} else {
//				if (ser != null) {
//					serCustName = ser.getCustomerName();
//				} else {
//					serCustName = con.getCustomerFullName();
//				}
//			}
//			/*
//			 * nameInFirstLetterUpperCase defined by Ajinkya suggested by Rahul
//			 * V Used to take customer name from Uppercase to Only first letter
//			 * Uppercase
//			 */
//			String nameInFirstLetterUpperCase1 = getFirstLetterUpperCase(serCustName
//					.trim()); // 1
//			// //////////////////// end here /////////////////
//
//			Phrase custName = new Phrase(nameInFirstLetterUpperCase1, font10);
//
//			
//			
//			
////		    Phrase newadd=new Phrase(custName +" \n"+serAdrs,font10);
////			PdfPCell custcell=new PdfPCell(newadd);
//////			PdfPCell adresscell=new PdfPCell(serAdrs);
////			
////
//////			premisesValCell.addElement(custName);
//////			premisesValCell.addElement(serAdrs);
////			premisesValCell.setBorder(0);
////
//////			premisesDetails.addCell(blnkCell);
////			premisesDetails.addCell(PremiseslblCell);
////			premisesDetails.addCell(colcell);
////			premisesDetails.addCell(premisesValCell);
////			premisesDetails.addCell(blnkCell);
//			PdfPCell premisesValCell = new PdfPCell(custName);
//			premisesValCell.setBorder(0);
//			PdfPCell addrerssCell = new PdfPCell(serAdrs);
//			addrerssCell.setBorder(0);
//
////			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(PremiseslblCell);
//			premisesDetails.addCell(colcell);
//			premisesDetails.addCell(premisesValCell);
//			premisesDetails.addCell(blnkCell);
//			
//			
//			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(blnkCell);
//			premisesDetails.addCell(addrerssCell);
//			premisesDetails.addCell(blnkCell);
////			premisesDetails.addCell(blnkCell);
////			premisesDetails.addCell(blnkCell);
//
//		}
		try {
			premisesDetails.setWidths(new float[] { 2.6f,0.2f, 4.6f, 1.2f });
			document.add(premisesDetails);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	

		PdfPTable serviceDetalsTbl = new PdfPTable(4);
		serviceDetalsTbl.setWidthPercentage(100);

		try {
			serviceDetalsTbl.setWidths(new float[] { 2.6f,0.2f, 4.6f, 1.2f });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase dateTime = new Phrase("Date & Time of services", font10);
		
		
		
		Phrase ph1=new Phrase(":");
		
		 PdfPCell col_cell=new PdfPCell(ph1);
		  
		    col_cell.setBorder(0);
		    
		 
		/**
		 * Date : 22-12-2017 BY ANIL
		 * earlier service date was pick up from service entity which was the random data out of number of service 
		 * now we will print service date as todays date and pick time from contract's service scheduling list   
		 */
		 Phrase dateTimeVal =null;

		 if(ser!=null){
		 dateTimeVal = new Phrase("" + fmt1.format(ser.getServiceDate())+ "   " + ser.getServiceTime(), font10);
		 }
		 else{
			 dateTimeVal = new Phrase("" + fmt1.format(con.getServiceScheduleList().get(0).getScheduleServiceDate())+ "   " + 
		                                 con.getServiceScheduleList().get(0).getScheduleServiceTime(), font10); 
		 }
		Phrase typeOfTreatment = new Phrase("Type Of Treatment", font10);
//		Phrase typeOfTreatmentVal = new Phrase("" + productName, font10);
		Phrase typeOfTreatmentVal = new Phrase("" + con.getItems().get(0).getProductCategory(), font10);
		Phrase clientTelNo = new Phrase("Contact Person 1:-", font10);
		/**
		 * Date : 19-12-2017 BY ANIL
		 */
		
//		 String salutation1 = "" ;
//			if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
//				salutation1 = cust.getSalutation();
//			}  
//			else{
//				 salutation1 = "" ;
//			}
//		
//		///////////////
//			String serCustName = "";	
//		if(ser!=null){	
//			if (ser.getCustomerName() == comp.getBusinessUnitName()) {
//				serCustName = ser.getCustomerName();
//				System.out.println("Service Cust name " + ser.getCustomerName());
//			} else {
//				serCustName = salutation1 + " "+ ser.getPersonInfo().getPocName();
//				System.out.println("Service Cust POC "+ ser.getPersonInfo().getPocName());
//			}
//		}else{
//			if (con.getCustomerFullName() == comp.getBusinessUnitName()) {
//				serCustName = con.getCustomerFullName();
//				System.out.println("Service Cust name "+ con.getCustomerFullName());
//			} else {
//				serCustName = salutation1 + " " + con.getCinfo().getPocName();
//				System.out.println("Service Cust POC "+ con.getCinfo().getPocName());
//			}
//		}
		
		String serCustName1="";
		if(custBranch != null){
			if(custBranch.getPocName()!=null && custBranch.getPocName().length()>0){
				serCustName1 = custBranch.getPocName();
			}
		}else{

			if(custContactList != null && custContactList.size()>0){
					serCustName1= custContactList.get(0).getName();
			}else{
				
				/** Date 29-06-2018 added by vijay for customer name will print if contact person not exist ***/
				if(cust.isCompany()){
					serCustName1 = cust.getCompanyName()+" / "+ cust.getCellNumber1();;
				}else{
					serCustName1 = cust.getFullname()+" / "+ cust.getCellNumber1();;
				}
				/** Ends here ***/
			}
			
	     
		}
		String nameInFirstLetterUpperCase11=getFirstLetterUpperCase(serCustName1.trim());
		String cellNo = "";
		
		if(custBranch != null){
//			cellNo = nameInFirstLetterUpperCase11+" / "+ custBranch.getPocCell();
			/** Date 28-jun-2018 added by Vijay if cell no exist then it will print ***/
			cellNo = nameInFirstLetterUpperCase11;
			if(custBranch.getPocCell()!=0)
				cellNo = cellNo +" / "+ custBranch.getPocCell();
			/**
			 * ends here
			 */
		}else{
//			cellNo = nameInFirstLetterUpperCase11+" / "+ cust.getCellNumber1();
			
			cellNo= nameInFirstLetterUpperCase11;
			
			if(custContactList != null && custContactList.size()>0){
//				cellNo= nameInFirstLetterUpperCase11+" / "+ custContactList.get(0).getCell();
			/** Date 28-jun-2018 added by Vijay if cell no exist then it will print ***/
			if(custContactList.get(0).getCell()!=0){
				cellNo = cellNo+" / "+ custContactList.get(0).getCell();
			}
			/**
			 * ends here
			 */
			}
		}
		Phrase clientTelNoVal = new Phrase(cellNo, font10);
		
		/**
		 * End
		 */
		
		
		Phrase remark = new Phrase("Remark", font10);
		String remarkVal = "";
		for (int i = 0; i < con.getItems().size(); i++) {
			if (con.getItems().get(i).getProductName() == productName) {
				remarkVal = con.getItems().get(i).getRemark();
			}
		}
		
		
		
		Phrase remarkValPhrase = new Phrase(remarkVal, font10);
		Phrase blnkcell=new Phrase("  ",font10);

		PdfPCell dateTimeCell = new PdfPCell(dateTime);
		dateTimeCell.setBorder(0);
		
		
		

	    PdfPCell col_cell1=new PdfPCell(ph1);
	   
	    col_cell1.setBorder(0);
//	    
		
		PdfPCell dateTimeValCell = new PdfPCell(dateTimeVal);
//		dateTimeValCell.addElement(dateTimeVal);
		dateTimeValCell.setBorder(0);

		PdfPCell typeOfTreatmentCell = new PdfPCell(typeOfTreatment);
//		typeOfTreatmentCell.addElement(typeOfTreatment);
		typeOfTreatmentCell.setBorder(0);
		
		
		    

		PdfPCell typeOfTreatmentValCell = new PdfPCell(typeOfTreatmentVal);
//		typeOfTreatmentValCell.addElement(typeOfTreatmentVal);
		typeOfTreatmentValCell.setBorder(0);

		PdfPCell clientTelNoCell = new PdfPCell(clientTelNo);
//		clientTelNoCell.addElement(clientTelNo);
		clientTelNoCell.setBorder(0);

//		  PdfPCell col_cell2=new PdfPCell();
//		    col_cell2.addElement(ph1);
//		    col_cell2.setBorder(0);
		    
		PdfPCell clientTelNoValCell = new PdfPCell(clientTelNoVal);
//		clientTelNoValCell.addElement();
		clientTelNoValCell.setBorder(0);
		
		

		PdfPCell remarkCell = new PdfPCell(remark);
//		remarkCell.addElement(remark);
		remarkCell.setBorder(0);
		
	

		PdfPCell remarkValCell = new PdfPCell(remarkValPhrase);
//		remarkValCell.addElement(remarkValPhrase);
		remarkValCell.setBorder(0);

//		serviceDetalsTbl.addCell(blnkCell);
		serviceDetalsTbl.addCell(dateTimeCell);
	
		serviceDetalsTbl.addCell(col_cell1);
		serviceDetalsTbl.addCell(dateTimeValCell);
		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);
////
//     	serviceDetalsTbl.addCell(blnkCell);
		serviceDetalsTbl.addCell(typeOfTreatmentCell);
		serviceDetalsTbl.addCell(col_cell1);
		serviceDetalsTbl.addCell(typeOfTreatmentValCell);
		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);
//
//		serviceDetalsTbl.addCell(blnkCell);
		serviceDetalsTbl.addCell(clientTelNoCell);
		serviceDetalsTbl.addCell(col_cell1);
		serviceDetalsTbl.addCell(clientTelNoValCell);
		serviceDetalsTbl.addCell(blnkCell);
		
		/**
		 * Date : 19-12-2017 BY ANIL
		 * adding contact person 2 details
		 */
		if(custContactList!=null&&custContactList.size()>1 && custBranch==null ){
			Phrase contactPerson2 = new Phrase("Contact Person 2:-", font10);  
			PdfPCell contactPersonCell2 = new PdfPCell(contactPerson2);
//			contactPersonCell2.addElement(contactPerson2);
			contactPersonCell2.setBorder(0);
			serviceDetalsTbl.addCell(contactPersonCell2);
			serviceDetalsTbl.addCell(col_cell1);
			
			
			String custPocName2="";
			custPocName2=custPocName2+getFirstLetterUpperCase(custContactList.get(1).getName())+" / "+custContactList.get(1).getCell();
			
			Phrase custPocNamePh2 = new Phrase(custPocName2, font10);  
			PdfPCell custPocNameCell2 = new PdfPCell(custPocNamePh2);
//			custPocNameCell2.addElement(custPocNamePh2);
			custPocNameCell2.setBorder(0);
			serviceDetalsTbl.addCell(custPocNameCell2);
			serviceDetalsTbl.addCell(blnkCell);
		}		
//		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);

//		serviceDetalsTbl.addCell(blnkCell);
		serviceDetalsTbl.addCell(remarkCell);
		serviceDetalsTbl.addCell(col_cell1);
		serviceDetalsTbl.addCell(remarkValCell);
		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);
//		serviceDetalsTbl.addCell(blnkCell);

		PdfPTable sigTbl = new PdfPTable(4);
		sigTbl.setWidthPercentage(100);
		sigTbl.setSpacingBefore(20);
        sigTbl.setSpacingAfter(60);
		
		try {
			sigTbl.setWidths(new float[] { 4f,0.2f, 2, 3f });
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase signature = new Phrase("Signature of the Occupant ",
				font9bold);

		PdfPCell signatureCell = new PdfPCell(signature);
		signatureCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		signatureCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		signatureCell.setBorder(0);
		int i=80;
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		
		
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(blnkCell);
		sigTbl.addCell(signatureCell);
		
		
	

		try {
			document.add(serviceDetalsTbl);
			document.add(sigTbl);
		} catch (DocumentException e) {

			e.printStackTrace();
		}
		
	

	}

	// Ajinkya added this code to convert customer name in CamelCase
	// Date : 12/4/2017

	private String getFirstLetterUpperCase(String customerFullName) {System.out.println("customerFullName "+customerFullName);
	String customerName="";
	String[] customerNameSpaceSpilt=customerFullName.split(" ");
	System.out.println("customerNameSpaceSpilt"+ customerNameSpaceSpilt.length);
  	System.out.println("customerNameSpaceSpilt size "+ customerNameSpaceSpilt.length);
  	logger.log(Level.SEVERE,"customerFullName size "+customerFullName.length());
  	logger.log(Level.SEVERE,"customerNameSpaceSpilt size "+ customerNameSpaceSpilt.length);
	int count=0;
	for (String name : customerNameSpaceSpilt)
	{
		String nameLowerCase=name.toLowerCase().trim();
	
		System.out.println("nameLowerCase"+ nameLowerCase);   
		if(nameLowerCase!=null && !name.equals(""))
		{
			
		
		if(count==0)
		{
			customerName=customerName.concat(name).toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			
			System.out.println("customerName"+customerName);
			logger.log(Level.SEVERE,"customerName"+customerName);
		}
		else
		{
			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			System.out.println("customerName"+customerName);
		}
		count=count+1;  
		}
	}
	return customerName; 

	}

}
