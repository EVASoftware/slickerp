package com.slicktechnologies.server.addhocprinting.Pcamb;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambQuotationGipcPdf {

    public Document document;
	Customer cust;
	Company comp;
	Contract con;
	List<Service> ser;
	Quotation quot; 
	Invoice invo;
	List<ArticleType> articletype;
	//public Image img;
	private Font font16boldul, font12bold, font8bold, font8, font9bold,font9boldul,
	font12boldul, font10boldul, font12, font16bold, font10, font10bold,
	font14bold,font9,font7,font7bold,font9red,font9boldred,font12boldred;
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	DecimalFormat df=new DecimalFormat("0.00");
	
	public PcambQuotationGipcPdf() {
		super();
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
		
	}

public void setQuotationGipc(Long count) {
	
	//Load Quotation
	quot=ofy().load().type(Quotation.class).id(count).now();
	
	// Load Invoice
	invo =ofy().load().type(Invoice.class).id(count).now();
	
	// load Customer
	if(quot.getCompanyId()==null)
	cust = ofy().load().type(Customer.class).filter("count",quot.getCinfo().getCount()).first().now();
	else
    cust=ofy().load().type(Customer.class).filter("companyId", quot.getCompanyId()).filter("count",quot.getCinfo().getCount()).first().now();

	//Load Company
	if(quot.getCompanyId()==null)
	   comp=ofy().load().type(Company.class).first().now();
	else
	   comp=ofy().load().type(Company.class).filter("companyId",quot.getCompanyId()).first().now();
	//Load Contract
	if(quot.getCompanyId()!=null)
		con=ofy().load().type(Contract.class).filter("companyId",quot.getCompanyId()).filter("count",quot.getContractCount()).first().now();
	else
		con=ofy().load().type(Contract.class).filter("count", quot.getContractCount()).first().now();
	
	       
	//Load Article type Details  
	 articletype = new ArrayList<ArticleType>();
		if(cust.getArticleTypeDetails().size()!=0){
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if(comp.getArticleTypeDetails().size()!=0){
			articletype.addAll(comp.getArticleTypeDetails());
		}
}

	public void createPdf() {
		
		//3rd page 
		createLogo(document,comp);
		createGIPCHeader();
		createGIPCTermiteObservationtbl();
		
	}
	
/////////////////////// - - Method for GIPC checklist - - /////////////////////////////////
	
// 3rd page start

	private void createLogo(Document doc, Company comp) {
		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
private void createGIPCHeader() {

PdfPCell headerCell=new PdfPCell();

Phrase mycomHeader1=new Phrase(comp.getBusinessUnitName().toUpperCase(),font16bold);
Paragraph headerPara1=new Paragraph();
headerPara1.add(mycomHeader1);
headerPara1.add(Chunk.NEWLINE);
//headerPara1.add(mycomHeader2);
//headerPara1.add(Chunk.NEWLINE);
headerPara1.setAlignment(Element.ALIGN_CENTER);
headerCell.addElement(headerPara1);

Phrase mycomHeader2=new Phrase(" GIPC INSPECTION CHECKLIST",font12bold);
Paragraph headerPara2=new Paragraph();
headerPara2.add(mycomHeader2);
headerPara2.add(Chunk.NEWLINE);
headerPara2.setAlignment(Element.ALIGN_CENTER);
headerCell.addElement(headerPara2);      

Phrase mycomHeader3=new Phrase("GIPC  ",font10bold);

String addressline1="";

if(cust.getAdress().getAddrLine2().equals(null)&& cust.getAdress().getAddrLine2().equals("")){
	addressline1=cust.getAdress().getAddrLine1()+"  "+cust.getAdress().getAddrLine2();
}
else{
	addressline1=cust.getAdress().getAddrLine1()+ " , ";
}

String locality = "";
if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
	System.out.println("inside both null condition1");
	locality= (cust.getAdress().getLandmark()+" , "+cust.getAdress().getLocality()+" , "+"\n"+cust.getAdress().getCity()+" , "
		      +cust.getAdress().getPin()+" , "+"\n" +cust.getAdress().getState()+" , "+cust.getAdress().getCountry());
}
else if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
	System.out.println("inside both null condition 2");
	locality= (cust.getAdress().getLandmark()+" , "+"\n"+cust.getAdress().getCity()+" , "
		      +cust.getAdress().getPin()+" , "+"\n"+cust.getAdress().getState()+" , "+cust.getAdress().getCountry()+" . ");
}

else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
	System.out.println("inside both null condition 3");
	locality= (cust.getAdress().getLocality()+" , "+"\n"+cust.getAdress().getCity()+" , "
		      +cust.getAdress().getPin()+"\n"+" , "+cust.getAdress().getState()+" , "+cust.getAdress().getCountry());
}
else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
	System.out.println("inside both null condition 4");
	locality=(cust.getAdress().getCity()+" , "+cust.getAdress().getPin()+" , "+"\n"+cust.getAdress().getState()+" , "+cust.getAdress().getCountry());
}

Phrase mycomHeader4 = new Phrase ("Client Name: "+     "_____________________________________",font9);
Phrase mycomHeader5 = new Phrase ("Client Address: "+  "___________________________________",font9);  
Phrase mycomHeader6 = new Phrase ("                         "+ "___________________________________",font9);
Phrase mycomHeader7 = new Phrase("FLAT /BUNGALOW /OFFICE / SOCIETY AREA _________________ SQ.FT & FLOOR NO:_________________",font10bold);                                         
Paragraph headerPara3=new Paragraph();
headerPara3.add(mycomHeader3);   
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(mycomHeader4);
headerPara3.add(Chunk.NEWLINE);     
headerPara3.add(mycomHeader5);
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(mycomHeader6);
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(mycomHeader7);
headerPara3.add(Chunk.NEWLINE);
headerPara3.add(Chunk.NEWLINE);
headerPara3.setAlignment(Element.ALIGN_LEFT);
headerCell.addElement(headerPara3);

//image logo code added here // 
//try
//{
//Image image1=Image.getInstance("images/PCAMB.png");
//image1.scalePercent(10f);
//image1.scaleAbsoluteWidth(120f);
//image1.setAbsolutePosition(442f,758f);	  
//document.add(image1);
//}
//catch(Exception e)  
//{
//	e.printStackTrace();
//}

// image logo code end here // 

PdfPTable parentTbl=new PdfPTable(1);
parentTbl.setWidthPercentage(100);    
parentTbl.addCell(headerCell);

try {
	
     document.add(parentTbl);
     
} 
catch (Exception e1) 
{
       e1.printStackTrace();
}
}

private void createGIPCTermiteObservationtbl() {

PdfPTable gipcObsTbl=new PdfPTable(6);
gipcObsTbl.setWidthPercentage(100);  

// tableheader 6 cols

Paragraph p1= new Paragraph();
Phrase srno=new Phrase("SR NO.",font9bold);
p1.add(srno);
p1.setAlignment(Element.ALIGN_CENTER);
PdfPCell srnoCell =new PdfPCell();    
srnoCell.addElement(p1);

Paragraph p2= new Paragraph();
Phrase questions=new Phrase("QUESTIONS",font9bold);
p2.add(questions);
p2.setAlignment(Element.ALIGN_CENTER);
PdfPCell questionsCell =new PdfPCell();
questionsCell.addElement(p2);

Paragraph p3= new Paragraph();
Phrase tmobs=new Phrase(" OBSERVATION      YES  /   NO ",font9bold);
p3.add(tmobs);
p3.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobsCell =new PdfPCell();
tmobsCell.addElement(p3);

Paragraph p4= new Paragraph();
Phrase suggestion=new Phrase("INFERENCE / SUGGESTION ",font9bold);
p4.add(suggestion);
p4.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestionCell =new PdfPCell();
suggestionCell.addElement(p4);  

Paragraph p5= new Paragraph();
Phrase applicableTo=new Phrase("APPLICABLE TO ",font9bold);
p5.add(applicableTo);
p5.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableToCell =new PdfPCell();
applicableToCell.addElement(p5);  

Paragraph p6= new Paragraph();
Phrase remarks=new Phrase("REMARKS / OTHER DETAILS",font9bold);
p6.add(remarks);
p6.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarksCell =new PdfPCell();   
remarksCell.addElement(p6);



//row 1 values table with 7cols

PdfPTable gipcObsValueTbl=new PdfPTable(7);
gipcObsValueTbl.setWidthPercentage(100);
	
Paragraph srno1p= new Paragraph();
Phrase srno1=new Phrase("1",font9);
srno1p.add(srno1);
srno1p.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno1Cell =new PdfPCell();    
srno1Cell.addElement(srno1p);

Paragraph p7= new Paragraph();
Phrase questions1=new Phrase("Check for cracks and crevices",font9);
p7.add(questions1);
p7.add(Chunk.NEWLINE);
p7.add(Chunk.NEWLINE);
p7.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions1Cell =new PdfPCell();
questions1Cell.addElement(p7);

Paragraph p81= new Paragraph();
Phrase tmobs1 =new Phrase(" ",font9bold);
p81.add(tmobs1);
p81.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs11Cell =new PdfPCell();
tmobs11Cell.addElement(p81);

Paragraph p82= new Paragraph();
Phrase tmobs12=new Phrase(" ",font9bold);
p82.add(tmobs12);
p82.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs12Cell =new PdfPCell();
tmobs12Cell.addElement(p82);

Paragraph p9= new Paragraph();
Phrase suggestion1=new Phrase("Low / Moderate / High",font9);
p9.add(suggestion1);
p9.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion1Cell =new PdfPCell();
suggestion1Cell.addElement(p9);

Paragraph applcableTo1Para= new Paragraph();
Phrase applcableTo1=new Phrase(" Check for cracks and crevices ",font9);
applcableTo1Para.add(applcableTo1);
applcableTo1Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo1Cell =new PdfPCell();
applicableTo1Cell.addElement(applcableTo1Para);

Paragraph p10= new Paragraph();
Phrase remarks1=new Phrase(" ",font9bold);
p10.add(remarks1);
p10.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks1Cell =new PdfPCell();
remarks1Cell.addElement(p10);

//row 2

Paragraph p11= new Paragraph();
Phrase srno2=new Phrase("2",font9bold);
p11.add(srno2);
p11.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno2Cell =new PdfPCell();    
srno2Cell.addElement(p11);

Paragraph p12= new Paragraph();
Phrase questions2=new Phrase("Droppings, Excreta, Oothica's (Eggs)",font9);
p12.add(questions2);
p12.add(Chunk.NEWLINE);
p12.add(Chunk.NEWLINE);

p12.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions2Cell =new PdfPCell();
questions2Cell.addElement(p12);

Paragraph p13= new Paragraph();
Phrase tmobs21 =new Phrase(" ",font9bold);
p13.add(tmobs21);
p13.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs21Cell =new PdfPCell();
tmobs21Cell.addElement(p13);        

Paragraph p14= new Paragraph();
Phrase tmobs22=new Phrase(" ",font9bold);
p14.add(tmobs22);
p14.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs22Cell =new PdfPCell();
tmobs22Cell.addElement(p14);

Paragraph p15= new Paragraph();
Phrase suggestion2=new Phrase(" Low / Moderate / High ",font9);
p15.add(suggestion2);
p15.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion2Cell =new PdfPCell();
suggestion2Cell.addElement(p15);

Paragraph applcableTo2Para= new Paragraph();
Phrase applcableTo2=new Phrase(" Droppings, Excreta, Oothica ",font9);
applcableTo2Para.add(applcableTo2);
applcableTo2Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo2Cell =new PdfPCell();
applicableTo2Cell.addElement(applcableTo2Para);

Paragraph p16= new Paragraph();
Phrase remarks2=new Phrase(" ",font9bold);
p16.add(remarks2);
p16.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks2Cell =new PdfPCell();
remarks2Cell.addElement(p16);

//Empty phrase	
Paragraph emptyPara11= new Paragraph();
Phrase emptyphrs11=new Phrase("",font9boldul);
emptyPara11.add(emptyphrs11);
emptyPara11.add(Chunk.NEWLINE);
emptyPara11.add(Chunk.NEWLINE);
emptyPara11.setAlignment(Element.ALIGN_LEFT);
PdfPCell emptyphrs11Cell =new PdfPCell();
emptyphrs11Cell.addElement(emptyPara11);

Paragraph emptyPara12= new Paragraph();
Phrase emptyphrs12=new Phrase("Insect identification",font9boldul);
emptyPara12.add(emptyphrs12);
emptyPara12.add(Chunk.NEWLINE);
emptyPara12.add(Chunk.NEWLINE);
emptyPara12.setAlignment(Element.ALIGN_LEFT);
PdfPCell emptyphrs12Cell =new PdfPCell();
emptyphrs12Cell.addElement(emptyPara12);

Paragraph emptyPara13= new Paragraph();
Phrase emptyphrs13=new Phrase(" ",font9);
emptyPara13.add(emptyphrs13);
emptyPara13.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs13Cell =new PdfPCell();    
emptyphrs13Cell.addElement(emptyPara13);

Paragraph emptyPara14= new Paragraph();
Phrase emptyphrs14=new Phrase("If yes, mention areas",font9);
emptyPara14.add(emptyphrs14);
emptyPara14.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs14Cell =new PdfPCell();    
emptyphrs14Cell.addElement(emptyPara14);

Paragraph emptyPara15= new Paragraph();
Phrase emptyphrs15=new Phrase("Cockroaches / Silver Fish / red Ants / Black Ants",font9);
emptyPara15.add(emptyphrs15);
emptyPara15.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs15Cell =new PdfPCell();    
emptyphrs15Cell.addElement(emptyPara15);

Paragraph emptyPara16= new Paragraph();
Phrase emptyphrs16=new Phrase(" Cockroaches, Silver Fish, red Ants, Black Ants  ",font9bold);
emptyPara16.add(emptyphrs16);
emptyPara16.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs16Cell =new PdfPCell();    
emptyphrs16Cell.addElement(emptyPara16);

Paragraph emptyPara17= new Paragraph();
Phrase emptyphrs17=new Phrase(" ",font9bold);
emptyPara17.add(emptyphrs17);
emptyPara17.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs17Cell =new PdfPCell();    
emptyphrs17Cell.addElement(emptyPara17);



//row3
Paragraph p17= new Paragraph();
Phrase srno3=new Phrase("3",font9bold);
p17.add(srno3);
p17.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno3Cell =new PdfPCell();    
srno3Cell.addElement(p17);  

Paragraph p18= new Paragraph();
Phrase questions3=new Phrase("A. Species identification",font9);
p18.add(questions3);
p18.add(Chunk.NEWLINE);
p18.add(Chunk.NEWLINE);

p18.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions3Cell =new PdfPCell();
questions3Cell.addElement(p18);

Paragraph p19= new Paragraph();
Phrase tmobs31 =new Phrase(" ",font9bold);
p19.add(tmobs31);
p19.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs31Cell =new PdfPCell();
tmobs31Cell.addElement(p19);

Paragraph p20= new Paragraph();
Phrase tmobs32=new Phrase(" ",font9);
p20.add(tmobs32);
p20.setAlignment(Element.ALIGN_LEFT);
PdfPCell tmobs32Cell =new PdfPCell();
tmobs32Cell.addElement(p20);

Paragraph p21= new Paragraph();
Phrase suggestion3=new Phrase("Cockroaches - American /German /Oriental; Silver Fish; red Ants; Black Ants",font9);
p21.add(suggestion3);
p21.add(Chunk.NEWLINE);
p21.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion3Cell =new PdfPCell();
suggestion3Cell.addElement(p21);

Paragraph applcableTo3Para= new Paragraph();
Phrase applcableTo3=new Phrase(" Species identification ",font9);
applcableTo3Para.add(applcableTo3);
applcableTo3Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo3Cell =new PdfPCell();
applicableTo3Cell.addElement(applcableTo3Para);


Paragraph p22= new Paragraph();
Phrase remarks3=new Phrase(" ",font9bold);
p22.add(remarks3);
p22.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks3Cell =new PdfPCell();
remarks3Cell.addElement(p22);



//row4
Paragraph p23= new Paragraph();
Phrase srno4=new Phrase("4",font9bold);
p23.add(srno4);
p23.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno4Cell =new PdfPCell();    
srno4Cell.addElement(p23);

Paragraph p24= new Paragraph();
Phrase questions4=new Phrase("Level of infestation (population)",font9);
p24.add(questions4);
p24.add(Chunk.NEWLINE);
p24.add(Chunk.NEWLINE);

p24.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions4Cell =new PdfPCell();
questions4Cell.addElement(p24);

Paragraph p25= new Paragraph();
Phrase tmobs41 =new Phrase(" ",font9bold);
p25.add(tmobs41);
p25.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs41Cell =new PdfPCell();
tmobs41Cell.addElement(p25);

Paragraph p26= new Paragraph();
Phrase tmobs42=new Phrase(" ",font9bold);
p26.add(tmobs42);
p26.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs42Cell =new PdfPCell();
tmobs42Cell.addElement(p26);

Paragraph p27= new Paragraph();
Phrase suggestion4=new Phrase("Low / Moderate / High",font9);
p27.add(suggestion4);
p27.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion4Cell =new PdfPCell();
suggestion4Cell.addElement(p27);

Paragraph applcableTo4Para= new Paragraph();
Phrase applcableTo4=new Phrase("",font9);
applcableTo4Para.add(applcableTo4);
applcableTo4Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo4Cell =new PdfPCell();
applicableTo4Cell.addElement(applcableTo4Para);


Paragraph p28= new Paragraph();
Phrase remarks4=new Phrase(" ",font9bold);
p28.add(remarks4);
p28.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks4Cell =new PdfPCell();
remarks4Cell.addElement(p28);


//row5
Paragraph p29= new Paragraph();
Phrase srno5=new Phrase("5",font9bold);
p29.add(srno5);
p29.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno5Cell =new PdfPCell();    
srno5Cell.addElement(p29);

Paragraph p30= new Paragraph();
Phrase questions5=new Phrase("Stages of infestation",font9);
p30.add(questions5);
p30.add(Chunk.NEWLINE);
p30.add(Chunk.NEWLINE);
p30.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions5Cell =new PdfPCell();
questions5Cell.addElement(p30);

Paragraph p31= new Paragraph();
Phrase tmobs51 =new Phrase(" ",font9);
p31.add(tmobs51);
p31.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs51Cell =new PdfPCell();
tmobs51Cell.addElement(p31);

Paragraph p32= new Paragraph();
Phrase tmobs52=new Phrase(" ",font9bold);
p32.add(tmobs52);
p32.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs52Cell =new PdfPCell();
tmobs52Cell.addElement(p32);

Paragraph p33= new Paragraph();
Phrase suggestion5=new Phrase("Young One / Adult /Both",font9);                   
p33.add(suggestion5);
p33.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion5Cell =new PdfPCell();
suggestion5Cell.addElement(p33);

Paragraph applcableTo5Para= new Paragraph();
Phrase applcableTo5=new Phrase("",font9);
applcableTo5Para.add(applcableTo5);
applcableTo5Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo5Cell =new PdfPCell();
applicableTo5Cell.addElement(applcableTo5Para);


Paragraph p34= new Paragraph();
Phrase remarks5=new Phrase(" ",font9bold);
p34.add(remarks5);
p34.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks5Cell =new PdfPCell();
remarks5Cell.addElement(p34);

//row6
Paragraph p35= new Paragraph();
Phrase srno6=new Phrase("6",font9bold);
p35.add(srno6);
p35.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno6Cell =new PdfPCell();    
srno6Cell.addElement(p35);

Paragraph p36= new Paragraph();
Phrase questions6=new Phrase("Sanitation Level",font9);
p36.add(questions6);
p36.add(Chunk.NEWLINE);
p36.add(Chunk.NEWLINE);

p36.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions6Cell =new PdfPCell();
questions6Cell.addElement(p36);

Paragraph p37= new Paragraph();
Phrase tmobs61 =new Phrase(" ",font9);
p37.add(tmobs61);
p37.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs61Cell =new PdfPCell();
tmobs61Cell.addElement(p37);

Paragraph p38= new Paragraph();
Phrase tmobs62=new Phrase(" ",font9bold);
p38.add(tmobs62);
p38.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs62Cell =new PdfPCell();
tmobs62Cell.addElement(p38);

Paragraph p39= new Paragraph();
Phrase suggestion6=new Phrase("Low / Moderate / High",font9);
p39.add(suggestion6);
p39.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion6Cell =new PdfPCell();
suggestion6Cell.addElement(p39);

Paragraph applcableTo6Para= new Paragraph();
Phrase applcableTo6=new Phrase("",font9);
applcableTo6Para.add(applcableTo6);
applcableTo6Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo6Cell =new PdfPCell();
applicableTo6Cell.addElement(applcableTo6Para);


Paragraph p40= new Paragraph();
Phrase remarks6=new Phrase(" ",font9bold);
p40.add(remarks6);
p40.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks6Cell =new PdfPCell();
remarks6Cell.addElement(p40);



//Empty phrase	
Paragraph emptyPara21= new Paragraph();
Phrase emptyphrs21=new Phrase("",font9boldul);
emptyPara21.add(emptyphrs21);
emptyPara21.add(Chunk.NEWLINE);
emptyPara21.add(Chunk.NEWLINE);
emptyPara21.setAlignment(Element.ALIGN_LEFT);
PdfPCell emptyphrs21Cell =new PdfPCell();
emptyphrs21Cell.addElement(emptyPara21);


Paragraph emptyPara22= new Paragraph();
Phrase emptyphrs22=new Phrase("Insect identification",font9boldul);
emptyPara22.add(emptyphrs22);
emptyPara22.add(Chunk.NEWLINE);
emptyPara22.add(Chunk.NEWLINE);
emptyPara22.setAlignment(Element.ALIGN_LEFT);
PdfPCell emptyphrs22Cell =new PdfPCell();
emptyphrs22Cell.addElement(emptyPara22);

Paragraph emptyPara23= new Paragraph();
Phrase emptyphrs23=new Phrase(" ",font9);
emptyPara23.add(emptyphrs23);
emptyPara23.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs23Cell =new PdfPCell();    
emptyphrs23Cell.addElement(emptyPara23);

Paragraph emptyPara24= new Paragraph();
Phrase emptyphrs24=new Phrase(" ",font9bold);
emptyPara24.add(emptyphrs24);
emptyPara24.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs24Cell =new PdfPCell();    
emptyphrs24Cell.addElement(emptyPara24);

Paragraph emptyPara25= new Paragraph();
Phrase emptyphrs25=new Phrase(" ",font9bold);
emptyPara25.add(emptyphrs25);
emptyPara25.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs25Cell =new PdfPCell();    
emptyphrs25Cell.addElement(emptyPara25);

Paragraph emptyPara26= new Paragraph();
Phrase emptyphrs26=new Phrase(" ",font9bold);
emptyPara26.add(emptyphrs26);
emptyPara26.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs26Cell =new PdfPCell();    
emptyphrs26Cell.addElement(emptyPara26);


Paragraph emptyPara27= new Paragraph();
Phrase emptyphrs27=new Phrase(" ",font9bold);
emptyPara27.add(emptyphrs27);
emptyPara27.setAlignment(Element.ALIGN_CENTER);
PdfPCell emptyphrs27Cell =new PdfPCell();    
emptyphrs27Cell.addElement(emptyPara27);
		



//row7
Paragraph p41= new Paragraph();
Phrase srno7=new Phrase("7",font9bold);
p41.add(srno7);

p41.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno7Cell =new PdfPCell();    
srno7Cell.addElement(p41);

Paragraph p42= new Paragraph();
Phrase questions7=new Phrase("A. Checking of drainage outlets (nani traps)",font9);
p42.add(questions7);
p42.add(Chunk.NEWLINE);
p42.add(Chunk.NEWLINE);

p42.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions7Cell =new PdfPCell();
questions7Cell.addElement(p42);

Paragraph p43= new Paragraph();
Phrase tmobs71 =new Phrase("",font9);
p43.add(tmobs71);
p43.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs71Cell =new PdfPCell();
tmobs71Cell.addElement(p43);

Paragraph p44= new Paragraph();
Phrase tmobs72=new Phrase(" ",font9bold);
p44.add(tmobs72);
p44.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs72Cell =new PdfPCell();
tmobs72Cell.addElement(p44);

Paragraph p45= new Paragraph();
Phrase suggestion7=new Phrase("Low / Moderate / High",font9);
p45.add(suggestion7);
p45.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion7Cell =new PdfPCell();
suggestion7Cell.addElement(p45);

Paragraph applcableTo7Para= new Paragraph();
Phrase applcableTo7=new Phrase("",font9);
applcableTo7Para.add(applcableTo7);
applcableTo7Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo7Cell =new PdfPCell();
applicableTo7Cell.addElement(applcableTo7Para);


Paragraph p46= new Paragraph();
Phrase remarks7=new Phrase(" ",font9bold);
p46.add(remarks7);
p46.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks7Cell =new PdfPCell();
remarks7Cell.addElement(p46);


//row8
Paragraph p47= new Paragraph();
Phrase srno8=new Phrase("8",font9bold);
p47.add(srno8);
p47.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno8Cell =new PdfPCell();    
srno8Cell.addElement(p47);

Paragraph p48= new Paragraph();
Phrase questions8=new Phrase("B. Dark, damp and protected corners ",font9);
p48.add(questions8);
p48.add(Chunk.NEWLINE);
p48.add(Chunk.NEWLINE);
p48.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions8Cell =new PdfPCell();
questions8Cell.addElement(p48);

Paragraph p49= new Paragraph();
Phrase tmobs81 =new Phrase(" ",font9bold);
p49.add(tmobs81);
p49.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs81Cell =new PdfPCell();
tmobs81Cell.addElement(p49);

Paragraph p50= new Paragraph();
Phrase tmobs82=new Phrase(" ",font9bold);
p50.add(tmobs82);
p50.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs82Cell =new PdfPCell();
tmobs82Cell.addElement(p50);

Paragraph p51= new Paragraph();
Phrase suggestion8=new Phrase("Low / Moderate / High",font9);
p51.add(suggestion8);
p51.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion8Cell =new PdfPCell();
suggestion8Cell.addElement(p51);

Paragraph applcableTo8Para= new Paragraph();
Phrase applcableTo8=new Phrase("",font9);
applcableTo8Para.add(applcableTo8);
applcableTo8Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo8Cell =new PdfPCell();
applicableTo8Cell.addElement(applcableTo8Para);


Paragraph p52= new Paragraph();
Phrase remarks8=new Phrase(" ",font9bold);
p52.add(remarks8);
p52.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks8Cell =new PdfPCell();
remarks8Cell.addElement(p52);

//row9
Paragraph p53= new Paragraph();
Phrase srno9=new Phrase("9",font9bold);
p53.add(srno9);
p53.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno9Cell =new PdfPCell();    
srno9Cell.addElement(p53);

Paragraph p54= new Paragraph();
Phrase questions9=new Phrase("C. Checking for storase units, stoarase areas(materials)",font9);  // Storase Storase
p54.add(questions9);
p54.add(Chunk.NEWLINE);
p54.add(Chunk.NEWLINE);
p54.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions9Cell =new PdfPCell();
questions9Cell.addElement(p54);

Paragraph p55= new Paragraph();
Phrase tmobs91 =new Phrase(" ",font9bold);
p55.add(tmobs91);
p55.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs91Cell =new PdfPCell();
tmobs91Cell.addElement(p55);

Paragraph p56= new Paragraph();
Phrase tmobs92=new Phrase(" ",font9bold);
p56.add(tmobs92);
p56.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs92Cell =new PdfPCell();
tmobs92Cell.addElement(p56);

Paragraph p57= new Paragraph();
Phrase suggestion9=new Phrase("Low /Moderate /High",font9);
p57.add(suggestion9);
p57.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion9Cell =new PdfPCell();
suggestion9Cell.addElement(p57);


Paragraph applcableTo9Para= new Paragraph();
Phrase applcableTo9=new Phrase("",font9);
applcableTo9Para.add(applcableTo9);
applcableTo9Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo9Cell =new PdfPCell();
applicableTo9Cell.addElement(applcableTo9Para);


Paragraph p58= new Paragraph();
Phrase remarks9=new Phrase(" ",font9bold);
p58.add(remarks9);
p58.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks9Cell =new PdfPCell();
remarks9Cell.addElement(p58);


//row10
Paragraph p59= new Paragraph();
Phrase srno10=new Phrase("10",font9bold);
p59.add(srno10);
p59.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno10Cell =new PdfPCell();    
srno10Cell.addElement(p59);

Paragraph p60= new Paragraph();
Phrase questions10=new Phrase("D.Checking of food operating and catering Areas - Kitchen, Pantry, Cafeteria  ",font9);
p60.add(questions10);
p60.add(Chunk.NEWLINE);
p60.add(Chunk.NEWLINE);
p60.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions10Cell =new PdfPCell();
questions10Cell.addElement(p60);

Paragraph p61= new Paragraph();
Phrase tmobs101 =new Phrase(" ",font9bold);
p61.add(tmobs101);
p61.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs101Cell =new PdfPCell();
tmobs101Cell.addElement(p61);

Paragraph p62= new Paragraph();
Phrase tmobs102=new Phrase(" ",font9bold);
p62.add(tmobs102);
p61.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs102Cell =new PdfPCell();
tmobs102Cell.addElement(p62);

Paragraph p63= new Paragraph();
Phrase suggestion10=new Phrase("Low /Moderate /High ",font9);
p63.add(suggestion10);
p63.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion10Cell =new PdfPCell();
suggestion10Cell.addElement(p63);

Paragraph applcableTo10Para= new Paragraph();
Phrase applcableTo10=new Phrase("",font9);
applcableTo10Para.add(applcableTo10);
applcableTo10Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo10Cell =new PdfPCell();
applicableTo10Cell.addElement(applcableTo10Para);


Paragraph p64= new Paragraph();
Phrase remarks10=new Phrase(" ",font9bold);
p64.add(remarks10);
p64.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks10Cell =new PdfPCell();
remarks10Cell.addElement(p64);

//row11
Paragraph p65= new Paragraph();
Phrase srno11=new Phrase("11",font9bold);
p65.add(srno11);
p65.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno11Cell =new PdfPCell();    
srno11Cell.addElement(p65);

Paragraph p66= new Paragraph();
Phrase questions11=new Phrase("E. Checking for cacks, crevices, broken tiles and gaps etc.",font9);
p66.add(questions11);
p66.add(Chunk.NEWLINE);
p66.add(Chunk.NEWLINE);
p66.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions11Cell =new PdfPCell();
questions11Cell.addElement(p66);

Paragraph p67= new Paragraph();
Phrase tmobs111 =new Phrase(" ",font9bold);
p67.add(tmobs111);
p67.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs111Cell =new PdfPCell();
tmobs111Cell.addElement(p67);

Paragraph p68= new Paragraph();
Phrase tmobs112=new Phrase(" ",font9bold);
p68.add(tmobs112);
p68.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs112Cell =new PdfPCell();
tmobs112Cell.addElement(p68);

Paragraph p69= new Paragraph();
Phrase suggestion11=new Phrase("Low /Moderate /High",font9);
p69.add(suggestion11);
p69.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion11Cell =new PdfPCell();
suggestion11Cell.addElement(p69);

Paragraph applcableTo11Para= new Paragraph();
Phrase applcableTo11=new Phrase("",font9);
applcableTo11Para.add(applcableTo11);
applcableTo11Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo11Cell =new PdfPCell();
applicableTo11Cell.addElement(applcableTo11Para);


Paragraph p70= new Paragraph();
Phrase remarks11=new Phrase(" ",font9bold);
p70.add(remarks11);
p70.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks11Cell =new PdfPCell();
remarks11Cell.addElement(p70);

//row12
Paragraph p71= new Paragraph();
Phrase srno12=new Phrase("12",font9bold);
p71.add(srno12);
p71.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno12Cell =new PdfPCell();    
srno12Cell.addElement(p71);

Paragraph p72= new Paragraph();
Phrase questions12=new Phrase("F. Checking below wash basin and cabinets",font9);
p72.add(questions12);
p72.add(Chunk.NEWLINE);
p72.add(Chunk.NEWLINE);
p72.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions12Cell =new PdfPCell();
questions12Cell.addElement(p72);

Paragraph p73= new Paragraph();
Phrase tmobs121 =new Phrase(" ",font9bold);
p73.add(tmobs121);
p73.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs121Cell =new PdfPCell();
tmobs121Cell.addElement(p73);

Paragraph p74= new Paragraph();
Phrase tmobs122=new Phrase(" ",font9bold);
p74.add(tmobs122);
p74.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs122Cell =new PdfPCell();
tmobs122Cell.addElement(p74);

Paragraph p75= new Paragraph();
Phrase suggestion12=new Phrase("Low /Moderate /High",font9);
p75.add(suggestion12);
p75.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion12Cell =new PdfPCell();
suggestion12Cell.addElement(p75);

Paragraph applcableTo12Para= new Paragraph();
Phrase applcableTo12=new Phrase("",font9);
applcableTo12Para.add(applcableTo12);
applcableTo12Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo12Cell =new PdfPCell();
applicableTo12Cell.addElement(applcableTo12Para);


Paragraph p76= new Paragraph();
Phrase remarks12=new Phrase(" ",font9bold);
p76.add(remarks12);
p76.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks12Cell =new PdfPCell();
remarks12Cell.addElement(p76);


//row13
Paragraph p77= new Paragraph();
Phrase srno13=new Phrase("13",font9bold);
p77.add(srno13);
p77.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno13Cell =new PdfPCell();    
srno13Cell.addElement(p77);

Paragraph p78= new Paragraph();
Phrase questions13=new Phrase("G. Gas cylinder, cold drinks crates, Corrugated boxes, store racks ",font9);
p78.add(questions13);
p78.add(Chunk.NEWLINE);
p78.add(Chunk.NEWLINE);
p78.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions13Cell =new PdfPCell();
questions13Cell.addElement(p78);

Paragraph p79= new Paragraph();
Phrase tmobs131 =new Phrase(" ",font9bold);
p79.add(tmobs131);
p79.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs131Cell =new PdfPCell();
tmobs91Cell.addElement(p79);

Paragraph p80= new Paragraph();
Phrase tmobs132=new Phrase(" ",font9bold);
p80.add(tmobs132);
p80.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs132Cell =new PdfPCell();  
tmobs92Cell.addElement(p80);

Paragraph p811= new Paragraph();
Phrase suggestion13=new Phrase("Low /Moderate /High",font9);
p81.add(suggestion13);
p811.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion13Cell =new PdfPCell();
suggestion13Cell.addElement(p811);

Paragraph applcableTo13Para= new Paragraph();
Phrase applcableTo13=new Phrase("",font9);
applcableTo13Para.add(applcableTo13);
applcableTo13Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo13Cell =new PdfPCell();
applicableTo13Cell.addElement(applcableTo13Para);


Paragraph p822= new Paragraph();
Phrase remarks13=new Phrase(" ",font9bold);
p822.add(remarks13);
p822.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks13Cell =new PdfPCell();
remarks13Cell.addElement(p822);

Paragraph p83= new Paragraph();
Phrase srno14=new Phrase("14",font9bold);
p83.add(srno14);
p83.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno14Cell =new PdfPCell();    
srno14Cell.addElement(p83);

Paragraph p84= new Paragraph();
Phrase questions14=new Phrase("H.Kitchen/ Pantry Device - fridge, cold storage , microwave, grinding machine, cooking range ",font9);
p84.add(questions14);
p84.add(Chunk.NEWLINE);
p84.add(Chunk.NEWLINE);
p84.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions14Cell =new PdfPCell();
questions14Cell.addElement(p84);

Paragraph p85= new Paragraph();
Phrase tmobs141 =new Phrase(" ",font9bold);
p85.add(tmobs141);
p85.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs141Cell =new PdfPCell();
tmobs141Cell.addElement(p85);

Paragraph p86= new Paragraph();
Phrase tmobs142=new Phrase(" ",font9bold);
p86.add(tmobs142);
p86.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs142Cell =new PdfPCell();
tmobs142Cell.addElement(p86);

Paragraph p87= new Paragraph();
Phrase suggestion14=new Phrase("Low /Moderate /High",font9);
p87.add(Chunk.NEWLINE);
p87.add(suggestion14);
p87.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion14Cell =new PdfPCell();
suggestion14Cell.addElement(p87);

Paragraph applcableTo14Para= new Paragraph();
Phrase applcableTo14=new Phrase("",font9);
applcableTo14Para.add(applcableTo14);
applcableTo14Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo14Cell =new PdfPCell();
applicableTo14Cell.addElement(applcableTo14Para);


Paragraph p88= new Paragraph();
Phrase remarks14=new Phrase(" ",font9bold);
p88.add(remarks14);
p88.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks14Cell =new PdfPCell();
remarks14Cell.addElement(p88);

Paragraph p89= new Paragraph();
Phrase srno15=new Phrase("15",font9bold);
p89.add(srno15);
p89.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno15Cell =new PdfPCell();    
srno15Cell.addElement(p89);

Paragraph p90= new Paragraph();
Phrase questions15=new Phrase("I. Toilets and Bathrooms ",font9);
p90.add(questions15);
p90.add(Chunk.NEWLINE);
p90.add(Chunk.NEWLINE);
p90.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions15Cell =new PdfPCell();
questions15Cell.addElement(p90);

Paragraph p91= new Paragraph();
Phrase tmobs151 =new Phrase(" ",font9bold);
p91.add(tmobs151);
p91.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs151Cell =new PdfPCell();
tmobs151Cell.addElement(p91);

Paragraph p92= new Paragraph();
Phrase tmobs152=new Phrase(" ",font9bold);
p92.add(tmobs152);
p92.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs152Cell =new PdfPCell();
tmobs152Cell.addElement(p92);

Paragraph p93= new Paragraph();
Phrase suggestion15=new Phrase("Low /Moderate /High",font9);
p93.add(suggestion15);
p93.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion15Cell =new PdfPCell();
suggestion15Cell.addElement(p93);

Paragraph applcableTo15Para= new Paragraph();
Phrase applcableTo15=new Phrase("",font9);
applcableTo15Para.add(applcableTo15);
applcableTo15Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo15Cell =new PdfPCell();
applicableTo15Cell.addElement(applcableTo15Para);


Paragraph p94= new Paragraph();
Phrase remarks15=new Phrase(" ",font9bold);
p94.add(remarks15);
p94.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks15Cell =new PdfPCell();
remarks15Cell.addElement(p94);


Paragraph p95= new Paragraph();
Phrase srno16=new Phrase("16",font9bold);
p95.add(srno16);
p95.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno16Cell =new PdfPCell();
srno16Cell.addElement(p95);


Paragraph p96= new Paragraph();
Phrase questions16=new Phrase("J. Loft area, above false ceiling area and below false flooring ",font9);
p96.add(questions16);
p96.add(Chunk.NEWLINE);
p96.add(Chunk.NEWLINE);
p96.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions16Cell =new PdfPCell();
questions16Cell.addElement(p96);   


Paragraph p97= new Paragraph();
Phrase tmobs161=new Phrase(" ",font9bold);
p97.add(tmobs161);
p97.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs161Cell =new PdfPCell();
tmobs161Cell.addElement(p97);

Paragraph p98= new Paragraph();
Phrase tmobs162=new Phrase(" ",font9bold);
p98.add(tmobs162);
p98.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs162Cell =new PdfPCell();
tmobs162Cell.addElement(p98);

Paragraph p99= new Paragraph();
Phrase suggestion16=new Phrase("Low /Moderate /High",font9);
p99.add(suggestion16);
p99.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion16Cell =new PdfPCell();
suggestion16Cell.addElement(p99);

Paragraph applcableTo16Para= new Paragraph();
Phrase applcableTo16=new Phrase("",font9);
applcableTo16Para.add(applcableTo16);
applcableTo16Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo16Cell =new PdfPCell();
applicableTo16Cell.addElement(applcableTo16Para);

Paragraph p100 = new Paragraph();
Phrase remarks16 = new Phrase(" ",font9bold);
p100.add(remarks16);
p100.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks16Cell =new PdfPCell();
remarks16Cell.addElement(p100);

Paragraph p101 = new Paragraph();
Phrase srno17 = new Phrase("17",font9bold);
p101.add(srno17);
p101.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno17Cell =new PdfPCell();
srno17Cell.addElement(p101);

Paragraph p102 = new Paragraph();
Phrase questions17 = new Phrase("K.Checking of furniture",font9);
p102.add(questions17);
p102.add(Chunk.NEWLINE);
p102.add(Chunk.NEWLINE);
p102.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions17Cell =new PdfPCell();
questions17Cell.addElement(p102);

Paragraph p103 = new Paragraph();
Phrase tmobs171 = new Phrase(" ",font9bold);
p103.add(tmobs171);
p103.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs171Cell =new PdfPCell();
tmobs171Cell.addElement(p103);

Paragraph p104 = new Paragraph();
Phrase tmobs172 = new Phrase(" ",font9bold);
p104.add(tmobs172);
p104.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs172Cell =new PdfPCell();
tmobs172Cell.addElement(p104);

Paragraph p105 = new Paragraph();
Phrase suggestion17 = new Phrase("Low /Moderate /High",font9);
p105.add(suggestion17);
p105.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion17Cell =new PdfPCell();
suggestion17Cell.addElement(p105);

Paragraph applcableTo17Para= new Paragraph();
Phrase applcableTo17=new Phrase("",font9);
applcableTo17Para.add(applcableTo17);
applcableTo17Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo17Cell =new PdfPCell();
applicableTo17Cell.addElement(applcableTo17Para);

Paragraph p106 = new Paragraph();
Phrase remarks17 = new Phrase(" ",font9bold);
p106.add(remarks17);
p106.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks17Cell =new PdfPCell();
remarks17Cell.addElement(p106);    


Paragraph p107 = new Paragraph();
Phrase srno18 = new Phrase("18",font9bold);
p107.add(srno18);
p107.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno18Cell =new PdfPCell();
srno18Cell.addElement(p107);

Paragraph p108 = new Paragraph();
Phrase questions18 = new Phrase("I.Checking of ducts and shafts ",font9);
p108.add(questions18);
p108.add(Chunk.NEWLINE);
p108.add(Chunk.NEWLINE);

p108.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions18Cell =new PdfPCell();
questions18Cell.addElement(p108);

Paragraph p109 = new Paragraph();
Phrase tmobs181 = new Phrase(" ",font9bold);
p109.add(tmobs181);
p109.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs181Cell =new PdfPCell();
tmobs181Cell.addElement(p109);

Paragraph p110 = new Paragraph();
Phrase tmobs182 = new Phrase(" ",font9bold);
p110.add(tmobs182);
p110.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs182Cell =new PdfPCell();
tmobs182Cell.addElement(p110);

Paragraph p111 = new Paragraph();
Phrase suggestion18 = new Phrase("Low /Moderate /High",font9);
p111.add(suggestion18);
p111.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion18Cell =new PdfPCell();
suggestion18Cell.addElement(p111);

Paragraph applcableTo18Para= new Paragraph();
Phrase applcableTo18=new Phrase("",font9);
applcableTo18Para.add(applcableTo18);
applcableTo18Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo18Cell =new PdfPCell();
applicableTo18Cell.addElement(applcableTo18Para);

Paragraph p112 = new Paragraph();
Phrase remarks18 = new Phrase(" ",font9bold);
p112.add(remarks18);
p112.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks18Cell =new PdfPCell();
remarks18Cell.addElement(p112);


Paragraph p113 = new Paragraph();
Phrase srno19 = new Phrase("19",font9bold);
p113.add(srno19);
p113.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno19Cell =new PdfPCell();
srno19Cell.addElement(p113);

Paragraph p114 = new Paragraph();
Phrase questions19 = new Phrase("M. Checking of lift wells and lifts",font9);
p114.add(questions19);
p114.add(Chunk.NEWLINE);
p114.add(Chunk.NEWLINE);
p114.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions19Cell =new PdfPCell();
questions19Cell.addElement(p114);

Paragraph p115 = new Paragraph();
Phrase tmobs191 = new Phrase(" ",font9bold);
p115.add(tmobs191);
p115.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs191Cell =new PdfPCell();
tmobs191Cell.addElement(p115);

Paragraph p116 = new Paragraph();
Phrase tmobs192 = new Phrase(" ",font9bold);
p116.add(tmobs192);
p116.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs192Cell =new PdfPCell();
tmobs192Cell.addElement(p116);

Paragraph p117 = new Paragraph();
Phrase suggestion19 = new Phrase("Low /Moderate /High",font9);
p117.add(suggestion19);
p117.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion19Cell =new PdfPCell();
suggestion19Cell.addElement(p117);

Paragraph applcableTo19Para= new Paragraph();
Phrase applcableTo19=new Phrase("",font9);
applcableTo19Para.add(applcableTo19);
applcableTo19Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo19Cell =new PdfPCell();
applicableTo19Cell.addElement(applcableTo19Para);

Paragraph p118 = new Paragraph();
Phrase remarks19 = new Phrase(" ",font9bold);
p118.add(remarks19);
p118.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks19Cell =new PdfPCell();
remarks19Cell.addElement(p118);


Paragraph p119 = new Paragraph();
Phrase srno20 = new Phrase("20",font9bold);
p119.add(srno20);
p119.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno20Cell =new PdfPCell();
srno20Cell.addElement(p119);

Paragraph p120 = new Paragraph();
Phrase questions20 = new Phrase("N. Manholes, Underground, Drains,Gutters,Nallas",font9);
p120.add(questions20);
p120.add(Chunk.NEWLINE);
p120.add(Chunk.NEWLINE);

p120.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions20Cell =new PdfPCell();
questions20Cell.addElement(p120);

Paragraph p121 = new Paragraph();
Phrase tmobs201 = new Phrase(" ",font9bold);
p121.add(tmobs201);
p121.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs201Cell =new PdfPCell();
tmobs171Cell.addElement(p121);

Paragraph p122 = new Paragraph();
Phrase tmobs202 = new Phrase(" ",font9bold);
p122.add(tmobs202);
p122.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs202Cell =new PdfPCell();
tmobs202Cell.addElement(p122);

Paragraph p123 = new Paragraph();
Phrase suggestion20 = new Phrase(" Low/Moderate/ High ",font9);
p123.add(suggestion20);
//p123.add(Chunk.NEWLINE);
//p123.add(Chunk.NEWLINE);
p123.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion20Cell =new PdfPCell();
suggestion20Cell.addElement(p123);

Paragraph applcableTo20Para= new Paragraph();
Phrase applcableTo20=new Phrase("",font9);
applcableTo20Para.add(applcableTo20);
applcableTo20Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo20Cell =new PdfPCell();
applicableTo20Cell.addElement(applcableTo20Para);

Paragraph p124 = new Paragraph();
Phrase remarks20 = new Phrase(" ",font9bold);
p124.add(remarks20);
p124.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks20Cell =new PdfPCell();
remarks20Cell.addElement(p124);


Paragraph p125 = new Paragraph();
Phrase srno21 = new Phrase("21",font9bold);
p125.add(srno21);
p125.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno21Cell =new PdfPCell();
srno21Cell.addElement(p125);

Paragraph p126 = new Paragraph();
Phrase questions21 = new Phrase("Checking of Parking area, Basement ",font9);
p126.add(questions21);
p126.add(Chunk.NEWLINE);
p126.add(Chunk.NEWLINE);
p126.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions21Cell =new PdfPCell();
questions21Cell.addElement(p126);

Paragraph p127= new Paragraph();
Phrase tmobs211 = new Phrase(" ",font9bold);
p127.add(tmobs211);
p127.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs211Cell =new PdfPCell();
tmobs171Cell.addElement(p127);

Paragraph p128 = new Paragraph();
Phrase tmobs212 = new Phrase(" ",font9bold);
p128.add(tmobs212);
p128.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs212Cell =new PdfPCell();
tmobs212Cell.addElement(p128);

Paragraph p129 = new Paragraph();
Phrase suggestion21 = new Phrase("Low /Moderate /High",font9);
p129.add(suggestion21);
p129.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion21Cell =new PdfPCell();
suggestion21Cell.addElement(p129);

Paragraph applcableTo21Para= new Paragraph();
Phrase applcableTo21=new Phrase("",font9);
applcableTo21Para.add(applcableTo21);
applcableTo21Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo21Cell =new PdfPCell();
applicableTo21Cell.addElement(applcableTo21Para);

Paragraph p130 = new Paragraph();
Phrase remarks21 = new Phrase(" ",font9bold);
p130.add(remarks21);
p130.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks21Cell =new PdfPCell();
remarks21Cell.addElement(p130);


Paragraph p131 = new Paragraph();
Phrase srno22 = new Phrase("22",font9bold);
p131.add(srno22);
p131.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno22Cell =new PdfPCell();
srno22Cell.addElement(p131);

Paragraph p132 = new Paragraph();
Phrase questions22 = new Phrase("P.checking pump room, meter room and sump room",font9);
p132.add(questions22);
p132.add(Chunk.NEWLINE);
p132.add(Chunk.NEWLINE);
p132.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions22Cell =new PdfPCell();
questions22Cell.addElement(p132);

Paragraph p133 = new Paragraph();
Phrase tmobs221 = new Phrase(" ",font9bold);
p133.add(tmobs221);
p133.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs221Cell =new PdfPCell();
tmobs221Cell.addElement(p133);

Paragraph p134 = new Paragraph();
Phrase tmobs222 = new Phrase(" ",font9bold);
p134.add(tmobs222);
p134.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs222Cell =new PdfPCell();
tmobs222Cell.addElement(p134);

Paragraph p135 = new Paragraph();
Phrase suggestion22 = new Phrase("Low /Moderate /High",font9);
p135.add(suggestion22);
p135.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion22Cell =new PdfPCell();
suggestion22Cell.addElement(p135);

Paragraph applcableTo22Para= new Paragraph();
Phrase applcableTo22=new Phrase("",font9);
applcableTo22Para.add(applcableTo22);
applcableTo22Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo22Cell =new PdfPCell();
applicableTo22Cell.addElement(applcableTo22Para);

Paragraph p136 = new Paragraph();
Phrase remarks22 = new Phrase(" ",font9bold);
p136.add(remarks22);
p136.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks22Cell =new PdfPCell();
remarks22Cell.addElement(p136);


Paragraph p137 = new Paragraph();
Phrase srno23 = new Phrase("23",font9bold);
p137.add(srno23);
p137.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno23Cell =new PdfPCell();
srno23Cell.addElement(p137);

Paragraph p138 = new Paragraph();
Phrase questions23 = new Phrase("Q. Checking of work stations/ Modular furniture",font9);
p138.add(questions23);
p138.add(Chunk.NEWLINE);
p138.add(Chunk.NEWLINE);
p138.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions23Cell =new PdfPCell();
questions23Cell.addElement(p138);

Paragraph p139 = new Paragraph();
Phrase tmobs231 = new Phrase(" ",font9bold);
p139.add(tmobs231);
p139.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs231Cell =new PdfPCell();
tmobs231Cell.addElement(p139);

Paragraph p140 = new Paragraph();
Phrase tmobs232 = new Phrase(" ",font9bold);
p140.add(tmobs232);
p140.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs232Cell =new PdfPCell();
tmobs232Cell.addElement(p140);

Paragraph p141 = new Paragraph();
Phrase suggestion23 = new Phrase("Low /Moderate /High",font9);
p141.add(suggestion23);
p141.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion23Cell =new PdfPCell();
suggestion23Cell.addElement(p141);

Paragraph applcableTo23Para= new Paragraph();
Phrase applcableTo23=new Phrase("",font9);
applcableTo23Para.add(applcableTo23);
applcableTo23Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo23Cell =new PdfPCell();
applicableTo23Cell.addElement(applcableTo23Para);

Paragraph p142 = new Paragraph();
Phrase remarks23 = new Phrase(" ",font9bold);
p142.add(remarks23);
p142.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks23Cell =new PdfPCell();
remarks23Cell.addElement(p142);

Paragraph p143 = new Paragraph();
Phrase srno24 = new Phrase("24",font9bold);
p143.add(srno24);
p143.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno24Cell =new PdfPCell();
srno24Cell.addElement(p143);

Paragraph p144 = new Paragraph();
Phrase questions24 = new Phrase("R. Checking of Lockers",font9);
p144.add(questions24);
p144.add(Chunk.NEWLINE);
p144.add(Chunk.NEWLINE);
p144.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions24Cell =new PdfPCell();
questions24Cell.addElement(p144);

Paragraph p145 = new Paragraph();
Phrase tmobs241 = new Phrase(" ",font9bold);
p145.add(tmobs241);
p145.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs241Cell =new PdfPCell();
tmobs241Cell.addElement(p145);

Paragraph p146 = new Paragraph();
Phrase tmobs242 = new Phrase(" ",font9bold);
p146.add(tmobs242);
p146.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs242Cell =new PdfPCell();
tmobs242Cell.addElement(p146);

Paragraph p147 = new Paragraph();
Phrase suggestion24 = new Phrase("Low /Moderate /High",font9);
p147.add(suggestion24);
p147.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion24Cell =new PdfPCell();
suggestion24Cell.addElement(p147);

Paragraph applcableTo24Para= new Paragraph();
Phrase applcableTo24=new Phrase("",font9);
applcableTo24Para.add(applcableTo24);
applcableTo24Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo24Cell =new PdfPCell();
applicableTo24Cell.addElement(applcableTo24Para);

Paragraph p148 = new Paragraph();
Phrase remarks24 = new Phrase(" ",font9bold);
p148.add(remarks24);
p148.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks24Cell =new PdfPCell();
remarks24Cell.addElement(p148);


Paragraph p149 = new Paragraph();
Phrase srno25 = new Phrase("25",font9bold);
p149.add(srno25);
p149.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno25Cell =new PdfPCell();
srno25Cell.addElement(p149);

Paragraph p150 = new Paragraph();
Phrase questions25 = new Phrase("S. Garbage disposal area",font9);
p150.add(questions25);
p150.add(Chunk.NEWLINE);
p150.add(Chunk.NEWLINE);

p150.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions25Cell =new PdfPCell();
questions25Cell.addElement(p150);

Paragraph p151 = new Paragraph();
Phrase tmobs251 = new Phrase(" ",font9bold);
p151.add(tmobs251);
p151.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs251Cell =new PdfPCell();
tmobs251Cell.addElement(p151);

Paragraph p152 = new Paragraph();
Phrase tmobs252 = new Phrase(" ",font9bold);
p152.add(tmobs252);
p152.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs252Cell =new PdfPCell();
tmobs252Cell.addElement(p152);

Paragraph p153 = new Paragraph();
Phrase suggestion25 = new Phrase("Low /Moderate /High",font9);
p153.add(suggestion25);
p153.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion25Cell =new PdfPCell();
suggestion25Cell.addElement(p153);

Paragraph applcableTo25Para= new Paragraph();
Phrase applcableTo25=new Phrase("",font9);
applcableTo25Para.add(applcableTo25);
applcableTo25Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo25Cell =new PdfPCell();
applicableTo25Cell.addElement(applcableTo25Para);

Paragraph p154 = new Paragraph();
Phrase remarks25 = new Phrase(" ",font9bold);
p154.add(remarks25);
p154.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks25Cell =new PdfPCell();
remarks25Cell.addElement(p154);

//row 26
Paragraph p155 = new Paragraph();
Phrase srno26 = new Phrase("26",font9bold);
p155.add(srno26);
p155.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno26Cell =new PdfPCell();
srno26Cell.addElement(p155);

Paragraph p156 = new Paragraph();
Phrase questions26 = new Phrase("T. Checking of garden area or Flower pots etc.",font9);
p156.add(questions26);
p156.add(Chunk.NEWLINE);
p156.add(Chunk.NEWLINE);
p156.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions26Cell =new PdfPCell();
questions26Cell.addElement(p156);

Paragraph p157 = new Paragraph();
Phrase tmobs261 = new Phrase(" ",font9bold);
p157.add(tmobs261);
p157.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs261Cell =new PdfPCell();
tmobs261Cell.addElement(p157);

Paragraph p158 = new Paragraph();
Phrase tmobs262 = new Phrase(" ",font9bold);
p158.add(tmobs262);
p158.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs262Cell =new PdfPCell();
tmobs262Cell.addElement(p158);

Paragraph p159 = new Paragraph();
Phrase suggestion26 = new Phrase("Low /Moderate /High",font9);
p159.add(suggestion26);
p159.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion26Cell =new PdfPCell();
suggestion26Cell.addElement(p159);

Paragraph applcableTo26Para= new Paragraph();
Phrase applcableTo26=new Phrase("",font9);
applcableTo26Para.add(applcableTo26);
applcableTo26Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo26Cell =new PdfPCell();
applicableTo26Cell.addElement(applcableTo26Para);

Paragraph p160 = new Paragraph();
Phrase remarks26 = new Phrase(" ",font9bold);
p160.add(remarks26);
p160.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks26Cell =new PdfPCell();
remarks26Cell.addElement(p160);


Paragraph p161 = new Paragraph();
Phrase srno27 = new Phrase("27",font9bold);
p161.add(srno27);
p161.setAlignment(Element.ALIGN_CENTER);
PdfPCell srno27Cell =new PdfPCell();
srno27Cell.addElement(p161);

Paragraph p162 = new Paragraph();
Phrase questions27 = new Phrase("U. Checking of service areas - staircase area",font9);
p162.add(questions27);
p162.add(Chunk.NEWLINE);
p162.add(Chunk.NEWLINE);
p162.setAlignment(Element.ALIGN_LEFT);
PdfPCell questions27Cell =new PdfPCell();
questions27Cell.addElement(p162);

Paragraph p163 = new Paragraph();
Phrase tmobs271 = new Phrase(" ",font9bold);
p163.add(tmobs271);
p163.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs271Cell =new PdfPCell();
tmobs271Cell.addElement(p163);

Paragraph p164 = new Paragraph();
Phrase tmobs272 = new Phrase(" ",font9bold);
p164.add(tmobs272);
p164.setAlignment(Element.ALIGN_CENTER);
PdfPCell tmobs272Cell =new PdfPCell();
tmobs272Cell.addElement(p164);

Paragraph p165 = new Paragraph();
Phrase suggestion27 = new Phrase("Low /Moderate /High",font9);
p165.add(suggestion27);
p165.setAlignment(Element.ALIGN_CENTER);
PdfPCell suggestion27Cell =new PdfPCell();
suggestion27Cell.addElement(p165);

Paragraph applcableTo27Para= new Paragraph();
Phrase applcableTo27=new Phrase("",font9);
applcableTo27Para.add(applcableTo27);
applcableTo27Para.setAlignment(Element.ALIGN_CENTER);
PdfPCell applicableTo27Cell =new PdfPCell();
applicableTo27Cell.addElement(applcableTo27Para);

Paragraph p166 = new Paragraph();
Phrase remarks27 = new Phrase(" ",font9bold);
p166.add(remarks27);
p166.setAlignment(Element.ALIGN_CENTER);
PdfPCell remarks27Cell =new PdfPCell();
remarks27Cell.addElement(p166);







// adding all Cell

gipcObsTbl.addCell(srnoCell);
gipcObsTbl.addCell(questionsCell);      
gipcObsTbl.addCell(tmobsCell);
gipcObsTbl.addCell(suggestionCell);
gipcObsTbl.addCell(applicableToCell);
gipcObsTbl.addCell(remarksCell);

gipcObsValueTbl.addCell(srno1Cell);
gipcObsValueTbl.addCell(questions1Cell);
gipcObsValueTbl.addCell(tmobs11Cell);
gipcObsValueTbl.addCell(tmobs12Cell);
gipcObsValueTbl.addCell(suggestion1Cell);
gipcObsValueTbl.addCell(applicableTo1Cell);
gipcObsValueTbl.addCell(remarks1Cell);

gipcObsValueTbl.addCell(srno2Cell);
gipcObsValueTbl.addCell(questions2Cell);      
gipcObsValueTbl.addCell(tmobs21Cell);
gipcObsValueTbl.addCell(tmobs22Cell);
gipcObsValueTbl.addCell(suggestion2Cell);
gipcObsValueTbl.addCell(applicableTo2Cell);
gipcObsValueTbl.addCell(remarks2Cell);

gipcObsValueTbl.addCell(emptyphrs11Cell);
gipcObsValueTbl.addCell(emptyphrs12Cell);      
gipcObsValueTbl.addCell(emptyphrs13Cell);
gipcObsValueTbl.addCell(emptyphrs14Cell);
gipcObsValueTbl.addCell(emptyphrs15Cell);
gipcObsValueTbl.addCell(emptyphrs16Cell);
gipcObsValueTbl.addCell(emptyphrs17Cell);




gipcObsValueTbl.addCell(srno3Cell);
gipcObsValueTbl.addCell(questions3Cell);      
gipcObsValueTbl.addCell(tmobs31Cell);
gipcObsValueTbl.addCell(tmobs32Cell);
gipcObsValueTbl.addCell(suggestion3Cell);
gipcObsValueTbl.addCell(applicableTo3Cell);
gipcObsValueTbl.addCell(remarks3Cell);


gipcObsValueTbl.addCell(srno4Cell);
gipcObsValueTbl.addCell(questions4Cell);      
gipcObsValueTbl.addCell(tmobs41Cell);
gipcObsValueTbl.addCell(tmobs42Cell);
gipcObsValueTbl.addCell(suggestion4Cell);
gipcObsValueTbl.addCell(applicableTo4Cell);
gipcObsValueTbl.addCell(remarks4Cell);

gipcObsValueTbl.addCell(srno5Cell);
gipcObsValueTbl.addCell(questions5Cell);      
gipcObsValueTbl.addCell(tmobs51Cell);
gipcObsValueTbl.addCell(tmobs52Cell);
gipcObsValueTbl.addCell(suggestion5Cell);
gipcObsValueTbl.addCell(applicableTo5Cell);
gipcObsValueTbl.addCell(remarks5Cell);

gipcObsValueTbl.addCell(srno6Cell);
gipcObsValueTbl.addCell(questions6Cell);      
gipcObsValueTbl.addCell(tmobs61Cell);
gipcObsValueTbl.addCell(tmobs62Cell);
gipcObsValueTbl.addCell(suggestion6Cell);
gipcObsValueTbl.addCell(applicableTo6Cell);
gipcObsValueTbl.addCell(remarks6Cell);

gipcObsValueTbl.addCell(emptyphrs21Cell);
gipcObsValueTbl.addCell(emptyphrs22Cell);      
gipcObsValueTbl.addCell(emptyphrs23Cell);
gipcObsValueTbl.addCell(emptyphrs24Cell);
gipcObsValueTbl.addCell(emptyphrs25Cell);
gipcObsValueTbl.addCell(emptyphrs26Cell);
gipcObsValueTbl.addCell(emptyphrs27Cell);

gipcObsValueTbl.addCell(srno7Cell);
gipcObsValueTbl.addCell(questions7Cell);      
gipcObsValueTbl.addCell(tmobs71Cell);
gipcObsValueTbl.addCell(tmobs72Cell);
gipcObsValueTbl.addCell(suggestion7Cell);
gipcObsValueTbl.addCell(applicableTo7Cell);
gipcObsValueTbl.addCell(remarks7Cell);



gipcObsValueTbl.addCell(srno8Cell);
gipcObsValueTbl.addCell(questions8Cell);      
gipcObsValueTbl.addCell(tmobs81Cell);
gipcObsValueTbl.addCell(tmobs82Cell);
gipcObsValueTbl.addCell(suggestion8Cell);
gipcObsValueTbl.addCell(applicableTo8Cell);
gipcObsValueTbl.addCell(remarks8Cell);


gipcObsValueTbl.addCell(srno9Cell);
gipcObsValueTbl.addCell(questions9Cell);      
gipcObsValueTbl.addCell(tmobs91Cell);
gipcObsValueTbl.addCell(tmobs92Cell);
gipcObsValueTbl.addCell(suggestion9Cell);
gipcObsValueTbl.addCell(applicableTo9Cell);
gipcObsValueTbl.addCell(remarks9Cell);


gipcObsValueTbl.addCell(srno10Cell);
gipcObsValueTbl.addCell(questions10Cell);      
gipcObsValueTbl.addCell(tmobs101Cell);
gipcObsValueTbl.addCell(tmobs102Cell);
gipcObsValueTbl.addCell(suggestion10Cell);
gipcObsValueTbl.addCell(applicableTo10Cell);
gipcObsValueTbl.addCell(remarks10Cell);


gipcObsValueTbl.addCell(srno11Cell);
gipcObsValueTbl.addCell(questions11Cell);      
gipcObsValueTbl.addCell(tmobs111Cell);
gipcObsValueTbl.addCell(tmobs112Cell);
gipcObsValueTbl.addCell(suggestion11Cell);
gipcObsValueTbl.addCell(applicableTo11Cell);
gipcObsValueTbl.addCell(remarks11Cell);


gipcObsValueTbl.addCell(srno12Cell);
gipcObsValueTbl.addCell(questions12Cell);      
gipcObsValueTbl.addCell(tmobs121Cell);
gipcObsValueTbl.addCell(tmobs122Cell);
gipcObsValueTbl.addCell(suggestion12Cell);
gipcObsValueTbl.addCell(applicableTo12Cell);
gipcObsValueTbl.addCell(remarks12Cell);


gipcObsValueTbl.addCell(srno13Cell);
gipcObsValueTbl.addCell(questions13Cell);      
gipcObsValueTbl.addCell(tmobs131Cell);
gipcObsValueTbl.addCell(tmobs132Cell);
gipcObsValueTbl.addCell(suggestion13Cell);
gipcObsValueTbl.addCell(applicableTo13Cell);
gipcObsValueTbl.addCell(remarks13Cell);


gipcObsValueTbl.addCell(srno14Cell);
gipcObsValueTbl.addCell(questions14Cell);      
gipcObsValueTbl.addCell(tmobs141Cell);
gipcObsValueTbl.addCell(tmobs142Cell);
gipcObsValueTbl.addCell(suggestion14Cell);
gipcObsValueTbl.addCell(applicableTo14Cell);
gipcObsValueTbl.addCell(remarks14Cell);

gipcObsValueTbl.addCell(srno15Cell);
gipcObsValueTbl.addCell(questions15Cell);      
gipcObsValueTbl.addCell(tmobs151Cell);
gipcObsValueTbl.addCell(tmobs152Cell);
gipcObsValueTbl.addCell(suggestion15Cell);
gipcObsValueTbl.addCell(applicableTo15Cell);
gipcObsValueTbl.addCell(remarks15Cell);

gipcObsValueTbl.addCell(srno16Cell);
gipcObsValueTbl.addCell(questions16Cell);      
gipcObsValueTbl.addCell(tmobs161Cell);
gipcObsValueTbl.addCell(tmobs162Cell);
gipcObsValueTbl.addCell(suggestion16Cell);
gipcObsValueTbl.addCell(applicableTo16Cell);
gipcObsValueTbl.addCell(remarks16Cell);


gipcObsValueTbl.addCell(srno17Cell);
gipcObsValueTbl.addCell(questions17Cell);      
gipcObsValueTbl.addCell(tmobs171Cell);
gipcObsValueTbl.addCell(tmobs172Cell);
gipcObsValueTbl.addCell(suggestion17Cell);
gipcObsValueTbl.addCell(applicableTo17Cell);
gipcObsValueTbl.addCell(remarks17Cell);

gipcObsValueTbl.addCell(srno18Cell);
gipcObsValueTbl.addCell(questions18Cell);      
gipcObsValueTbl.addCell(tmobs181Cell);
gipcObsValueTbl.addCell(tmobs182Cell);
gipcObsValueTbl.addCell(suggestion18Cell);
gipcObsValueTbl.addCell(applicableTo18Cell);
gipcObsValueTbl.addCell(remarks18Cell);

gipcObsValueTbl.addCell(srno19Cell);
gipcObsValueTbl.addCell(questions19Cell);      
gipcObsValueTbl.addCell(tmobs191Cell);
gipcObsValueTbl.addCell(tmobs192Cell);
gipcObsValueTbl.addCell(suggestion19Cell);
gipcObsValueTbl.addCell(applicableTo19Cell);
gipcObsValueTbl.addCell(remarks19Cell);

gipcObsValueTbl.addCell(srno20Cell);
gipcObsValueTbl.addCell(questions20Cell);      
gipcObsValueTbl.addCell(tmobs201Cell);
gipcObsValueTbl.addCell(tmobs202Cell);
gipcObsValueTbl.addCell(suggestion20Cell);
gipcObsValueTbl.addCell(applicableTo20Cell);
gipcObsValueTbl.addCell(remarks20Cell);


gipcObsValueTbl.addCell(srno21Cell);
gipcObsValueTbl.addCell(questions21Cell);      
gipcObsValueTbl.addCell(tmobs211Cell);
gipcObsValueTbl.addCell(tmobs212Cell);
gipcObsValueTbl.addCell(suggestion21Cell);
gipcObsValueTbl.addCell(applicableTo21Cell);
gipcObsValueTbl.addCell(remarks21Cell);

gipcObsValueTbl.addCell(srno22Cell);
gipcObsValueTbl.addCell(questions22Cell);      
gipcObsValueTbl.addCell(tmobs221Cell);
gipcObsValueTbl.addCell(tmobs222Cell);
gipcObsValueTbl.addCell(suggestion22Cell);
gipcObsValueTbl.addCell(applicableTo22Cell);
gipcObsValueTbl.addCell(remarks22Cell);

gipcObsValueTbl.addCell(srno23Cell);
gipcObsValueTbl.addCell(questions23Cell);      
gipcObsValueTbl.addCell(tmobs231Cell);
gipcObsValueTbl.addCell(tmobs232Cell);
gipcObsValueTbl.addCell(suggestion23Cell);
gipcObsValueTbl.addCell(applicableTo23Cell);
gipcObsValueTbl.addCell(remarks23Cell);


gipcObsValueTbl.addCell(srno24Cell);
gipcObsValueTbl.addCell(questions24Cell);      
gipcObsValueTbl.addCell(tmobs241Cell);
gipcObsValueTbl.addCell(tmobs242Cell);
gipcObsValueTbl.addCell(suggestion24Cell);
gipcObsValueTbl.addCell(applicableTo24Cell);
gipcObsValueTbl.addCell(remarks24Cell);

gipcObsValueTbl.addCell(srno25Cell);
gipcObsValueTbl.addCell(questions25Cell);      
gipcObsValueTbl.addCell(tmobs251Cell);
gipcObsValueTbl.addCell(tmobs252Cell);
gipcObsValueTbl.addCell(suggestion25Cell);
gipcObsValueTbl.addCell(applicableTo25Cell);
gipcObsValueTbl.addCell(remarks25Cell);

gipcObsValueTbl.addCell(srno26Cell);
gipcObsValueTbl.addCell(questions26Cell);      
gipcObsValueTbl.addCell(tmobs261Cell);
gipcObsValueTbl.addCell(tmobs262Cell);
gipcObsValueTbl.addCell(suggestion26Cell);
gipcObsValueTbl.addCell(applicableTo26Cell);
gipcObsValueTbl.addCell(remarks26Cell);

gipcObsValueTbl.addCell(srno27Cell);
gipcObsValueTbl.addCell(questions27Cell);      
gipcObsValueTbl.addCell(tmobs271Cell);
gipcObsValueTbl.addCell(tmobs272Cell);
gipcObsValueTbl.addCell(suggestion27Cell);
gipcObsValueTbl.addCell(applicableTo27Cell);
gipcObsValueTbl.addCell(remarks27Cell);


try {
gipcObsTbl.setWidths(new float[] {04,25,16,22,15,18 });
	document.add(gipcObsTbl);
	  
} catch (Exception e1) {
	e1.printStackTrace();
}


	 try {
		gipcObsValueTbl.setWidths(new float[] { 04,25,8,8,22,15,18 });
			document.add(gipcObsValueTbl);
			document.add(Chunk.NEXTPAGE);
		} catch (Exception e1) {  
			e1.printStackTrace();
		}


    }

}
