package com.slicktechnologies.server.addhocprinting.Pcamb;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class PcambrodentCountMonthlyReportpdfServlet  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4516554169717616610L;
	
	protected  void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException ,IOException {
			 //super.doGet(request, response);
		    response.setContentType("application/pdf");
		    
	
	  try{
		   String stringid = request.getParameter("Id");
			stringid = stringid.trim();  
			Long count = Long.parseLong(stringid);
			
			PcambrodentCountMonthlyReportpdf pdf= new  PcambrodentCountMonthlyReportpdf();
			
			
			pdf.document= new Document();
			  Document document = pdf.document;
			  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
			 
			  document.open();
			  pdf.setContract(count);
			  pdf.createPdf();
			  document.close();			    
	  }catch (DocumentException e) {
		  e.printStackTrace();
	  }
	}

}
