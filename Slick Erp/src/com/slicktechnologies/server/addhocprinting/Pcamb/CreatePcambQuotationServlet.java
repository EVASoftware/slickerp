package com.slicktechnologies.server.addhocprinting.Pcamb;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;


public class CreatePcambQuotationServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5983000961404259213L;
	
	 public class Rotate extends PdfPageEventHelper {
	        protected PdfNumber rotation = PdfPage.PORTRAIT;
	        public void setRotation(PdfNumber rotation) {
	            this.rotation = rotation;
	        }
	        public void onEndPage(PdfWriter writer, Document document) {
	            writer.addPageDictEntry(PdfName.ROTATE, rotation);
	        }
	    }

	

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//super.doGet(request, response);
		response.setContentType("application/pdf");
	


	
	String stringid = request.getParameter("Id");
	stringid = stringid.trim();
	Long count = Long.parseLong(stringid);
	
	
	String type = request.getParameter("type");
	type =type.trim();
	
	 

	
	if(type.equalsIgnoreCase("WoodBorer"))
	{
		 try{
			 PcambQuotationWoodBorerPdf pdf= new  PcambQuotationWoodBorerPdf();
				
				pdf.document= new Document(PageSize.A4);
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				  Rotate rotation = new Rotate();
			      writer.setPageEvent(rotation);
				 
				  document.open();
				  pdf.setWdQuotation(count);
				  pdf.createPdf();
				  rotation.setRotation(PdfPage.PORTRAIT);
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("BedBugs"))
	{
		 try{
			 PcambQuotationBedBugsPdf pdf= new  PcambQuotationBedBugsPdf();
				
				pdf.document= new Document(PageSize.A4);
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				  Rotate rotation = new Rotate();
			      writer.setPageEvent(rotation);
				 
				  document.open();
				  pdf.setBbQuotation(count);
				  rotation.setRotation(PdfPage.PORTRAIT);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
		 
	}
	else if(type.equalsIgnoreCase("FlyMosquito"))
	{
		 try{
			 PcambQuotationFlyMosquitoPdf pdf= new  PcambQuotationFlyMosquitoPdf();
				
				pdf.document= new Document(PageSize.A4);
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				  Rotate rotation = new Rotate();
			      writer.setPageEvent(rotation);
				  document.open();
				  pdf.setInvoice(count);
				  rotation.setRotation(PdfPage.PORTRAIT);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("Rodent"))
	{
		 try{
			 PcambQuotationRodentPdf pdf= new  PcambQuotationRodentPdf();
				
				pdf.document= new Document(PageSize.A4);
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				  Rotate rotation = new Rotate();
			      writer.setPageEvent(rotation);
				  document.open();
				  pdf.setQuotationRodent(count);
				  rotation.setRotation(PdfPage.PORTRAIT);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("Termite"))
	{
		 try{
			 PcambQuotationTermitePdf pdf= new  PcambQuotationTermitePdf();
				
				pdf.document= new Document(PageSize.A4);
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				  Rotate rotation = new Rotate();
			      writer.setPageEvent(rotation);
				  document.open();
				  pdf.setQuotationTermite(count);
				  rotation.setRotation(PdfPage.PORTRAIT);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	else if(type.equalsIgnoreCase("Gipc"))
	{
		 try{
			 PcambQuotationGipcPdf pdf= new  PcambQuotationGipcPdf();
				
				pdf.document= new Document(PageSize.A4);
				  Document document = pdf.document;
				  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream());
				  Rotate rotation = new Rotate();
			      writer.setPageEvent(rotation);
				  document.open();
				  pdf.setQuotationGipc(count);
				  rotation.setRotation(PdfPage.PORTRAIT);
				  pdf.createPdf();
				  document.close();	
		    }catch (DocumentException e)
		 	{
			  e.printStackTrace();
			}
	}
	
	}
}
