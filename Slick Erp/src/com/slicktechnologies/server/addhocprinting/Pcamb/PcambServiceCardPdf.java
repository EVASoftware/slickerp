package com.slicktechnologies.server.addhocprinting.Pcamb;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class PcambServiceCardPdf 
{

	public Document document;
	Contract con;
	Company comp;
	Customer cust;
	Service ser;
	
	Address serAdd = new Address();
	CustomerBranchDetails custBranch = null;
	ServiceSchedule sersch;
	Invoice invoiceentity;
	List<ArticleType> articletype;
	public int j ;
	
	/**
	 * Date : 18-12-2017 BY ANIL
	 */
	List<ContactPersonIdentification> custContactList;

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font9boldred, font9, font9red, font7, font7bold, font12boldul,
			font10boldul, font12, font16bold, font10, font10bold,
			font12boldred, font14bold, font11bold;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd MMM yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("MM-yyyy");
	
	Logger logger = Logger.getLogger("PcambServiceCardPdf.class");
	
//	private SimpleDateFormat fmt1 = new SimpleDateFormat("hh-mm-ss"); 
	
	
	int bedbug = 0, termite = 0, woodborer = 0, flycontrol = 0, rodent = 0,gel=0,
			gipc = 0, other = 0 , dt =0 ;

	public PcambServiceCardPdf() {
		super();
		BaseFont base = null;
		try {
			logger.log(Level.SEVERE,"BASE FONT SET");
			base = BaseFont.createFont("fonts\\VERDANA.TTF", BaseFont.WINANSI,BaseFont.EMBEDDED);
		} catch (DocumentException e) {
			logger.log(Level.SEVERE,"DOC EXCPN "+e.getMessage());
			// TODO Auto-generated catch block0
			e.printStackTrace();
		} catch (IOException e) {
			logger.log(Level.SEVERE,"IO EXCPN "+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		font16boldul = new Font(base, 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(base, 16, Font.BOLD);
		font12bold = new Font(base, 12, Font.BOLD);
		font8bold = new Font(base, 8, Font.BOLD);
		font8 = new Font(base, 8);
		font12boldul = new Font(base, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(base, 12);
		font12boldred = new Font(base, 12, Font.BOLD,
				BaseColor.RED);
		font10 = new Font(base, 10);
		font10bold = new Font(base, 10, Font.BOLD);
		font14bold = new Font(base, 14, Font.BOLD);
		font10boldul = new Font(base, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(base, 9, Font.BOLD);
		font9boldred = new Font(base, 9, Font.BOLD,
				BaseColor.RED);
		font9 = new Font(base, 9);
		font9red = new Font(base, 9, Font.NORMAL,
				BaseColor.RED);
		font7 = new Font(base, 7);
		font7bold = new Font(base, 7, Font.BOLD);
		font11bold = new Font(base, 11, Font.BOLD);

	}

	public void setContract(Long count) {	// load service
		
		con = ofy().load().type(Contract.class).id(count).now();

	// load Service
		
		if (con.getCompanyId() == null)
			ser = ofy().load().type(Service.class).first().now();
		else			
		    ser = ofy().load().type(Service.class).filter("companyId", con.getCompanyId()).filter("contractCount", con.getCount()).first().now();
		
		// load Company
		
		if (con.getCompanyId() == null)

			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();


		// load Customer
		if(con.getCompanyId()==null)
			cust = ofy().load().type(Customer.class).filter("count", con.getCustomerId()).first().now();
			else
		    cust=ofy().load().type(Customer.class).filter("companyId", con.getCompanyId()).filter("count", con.getCustomerId()).first().now();

		if(con.getCompanyId() ==null)
			sersch = ofy().load().type(ServiceSchedule.class).first().now();
		else
			sersch =  ofy().load().type(ServiceSchedule.class).filter("companyId", con.getCompanyId()).filter("contractCount", con.getCount()).first().now();
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt2.setTimeZone(TimeZone.getTimeZone("IST"));
		/**
		 * Date : 18-12-2017 BY ANIL
		 * Loading Customer's contact persons list
		 */
		if(cust!=null){
			custContactList=ofy().load().type(ContactPersonIdentification.class).filter("companyId", cust.getCompanyId()).filter("entype","Customer").filter("personInfo.count", cust.getCount()).list();
			if(custContactList!=null&&custContactList.size()!=0){
				Comparator<ContactPersonIdentification> idComparator = new Comparator<ContactPersonIdentification>() {  
					@Override  
					public int compare(ContactPersonIdentification o1, ContactPersonIdentification o2) { 
						Integer count1=o1.getCount();
						Integer count2=o2.getCount();
						return count1.compareTo(count2);  
					}  
				};  
				Collections.sort(custContactList,idComparator);
			}
		}
		
}
   
	public void createPdf() {
		
		HashMap<Integer,String> proDetail = new HashMap<Integer,String>();
		
		HashSet<String> branchSet = new HashSet<String>();
		
		for (int i=0 ; i< con.getItems().size(); i++)
		{
			
			Set<Integer> ser =  con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet();
			
			for(Integer seq : ser){
				ArrayList<BranchWiseScheduling> branch =  con.getItems().get(i).getCustomerBranchSchedulingInfo().get(seq);
				for(int k =0 ;k<branch.size();k++){
					if(!branch.get(k).getBranchName().equalsIgnoreCase("Service Address")
							&& !branch.get(k).getBranchName().equalsIgnoreCase("Select")  && branch.get(k).isCheck())
					{
						branchSet.add(branch.get(k).getBranchName());
					}
				}
				
			}
		}
		
		List<CustomerBranchDetails> custBranchList = new ArrayList<CustomerBranchDetails>();
		ArrayList <String> branchList = new  ArrayList<String>();
		if(branchSet.size()>0){
			branchList.addAll(branchSet);
			
			custBranchList = ofy().load().type(CustomerBranchDetails.class).filter("buisnessUnitName IN",  branchList).filter("cinfo.count", con.getCinfo().getCount())
			   .filter("companyId",con.getCompanyId()).list();
			
		}
		
		ArrayList<BranchWiseScheduling> custDt = new ArrayList<BranchWiseScheduling>();
		if(custBranchList != null){
			for (int i=0 ; i< con.getItems().size(); i++)
			 {
				if( con.getItems().get(i).getCustomerBranchSchedulingInfo().keySet().size()>0){
					
					for(Map.Entry<Integer, ArrayList<BranchWiseScheduling>> mapList : con.getItems().get(i).getCustomerBranchSchedulingInfo().entrySet()){
						
						custDt = mapList.getValue();
						for(BranchWiseScheduling brandDt  : custDt){
							if((brandDt.getBranchName().equalsIgnoreCase("Service Address")
							|| brandDt.getBranchName().equalsIgnoreCase("Select")) &&   brandDt.isCheck()){
										
//										serAdd = cust.getSecondaryAdress();
								
								/**
								 * Date 28-06-2018 By Vijay
								 * Des :- if contract doest not contain customer service address then service adddress will print from customer
								 */
								if(con.getCustomerServiceAddress()==null){
										serAdd = cust.getSecondaryAdress();
									}else{
										serAdd =  con.getCustomerServiceAddress();
								}
								/**
								 * ends here
								 */
						
								/**
								 * nidhi
								 * 23-06-2018
								 *))
								 */
								custBranch = null;
								/*if(custBranchDt.getAddress()!=null){
									serAdd = custBranchDt.getAddress();
								}else{
									serAdd = ser.getAddress();
								}*/
								createServiceCaredDetails(con.getItems().get(i).getProductCode());
					
								try {
									document.add(Chunk.NEXTPAGE);
								} catch (DocumentException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}else{
								if(!brandDt.getBranchName().equalsIgnoreCase("Service Address")
										&& !brandDt.getBranchName().equalsIgnoreCase("Select") &&   brandDt.isCheck()){
										for(CustomerBranchDetails custBranchDt : custBranchList){
											if(custBranchDt.getBusinessUnitName().equals(brandDt.getBranchName())){
												custBranch = custBranchDt;
												if(custBranchDt.getAddress()!=null){
													serAdd = custBranchDt.getAddress();
												}else{
													serAdd = cust.getSecondaryAdress();
												}
												createServiceCaredDetails(con.getItems().get(i).getProductCode());
												
												try {
													document.add(Chunk.NEXTPAGE);
												} catch (DocumentException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												break;
											}
										}
								}
							
							}
						}
						
					}
				}
			 }
			
		}else{
			for (int i=0 ; i< con.getItems().size(); i++)
			 {
				{
//					serAdd = cust.getSecondaryAdress();
					
					/**
					 * Date 28-06-2018 By Vijay
					 * Des :- if contract doest not contain customer service address then service adddress will print from customer
					 */
					if(con.getCustomerServiceAddress()!=null){
						serAdd =  con.getCustomerServiceAddress();
					}else{ 
						if(cust!=null)
						serAdd = cust.getSecondaryAdress();
					}
					/**
					 * ends here
					 */
				}
	        	createServiceCaredDetails(con.getItems().get(i).getProductCode());
	        	
	        	
	        	try {
					document.add(Chunk.NEXTPAGE);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        	
			 }
		}
        
		
	}

	private void createServiceCaredDetails(String productCode) {  //  
		
        System.out.println("String productCode"+ productCode);
        
//		Phrase myHeader1 = new Phrase(""+ comp.getBusinessUnitName().toUpperCase(), font11bold);
//		Phrase myHeader2 = new Phrase("SERVICE CARD", font11bold);
//		
//		PdfPCell header1Cell = new PdfPCell(myHeader1);
//		header1Cell.setBorder(0);
//		header1Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
////		header1Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		
//		PdfPCell header2Cell = new PdfPCell(myHeader2);
//		header2Cell.setBorder(0);
//		header2Cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		header2Cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		
//		PdfPTable headertbl = new PdfPTable(1);
//		headertbl.setWidthPercentage(100);
//		headertbl.addCell(header1Cell);
//		headertbl.addCell(header2Cell);

		// ///////////////////////// sevice details with customer info table added ////////////////////

		PdfPTable serviceDetailstbl = new PdfPTable(2);
		serviceDetailstbl.setWidthPercentage(100);
		/**
		 * Date : 18-12-2017 By ANIL
		 */
		serviceDetailstbl.setSpacingBefore(30f);
		/**
		 * End
		 */
		try {
			serviceDetailstbl.setWidths(new float[] { 45, 55 });
		} catch (DocumentException e1) {

			e1.printStackTrace();
		}

		Phrase cardNo = new Phrase("CARD NO:-", font9bold);
		PdfPCell cardNoCell = new PdfPCell();
		cardNoCell.addElement(cardNo);
		cardNoCell.setBorder(0);
		serviceDetailstbl.addCell(cardNoCell);
		
		 ////////////////////////////// 4 digit card No logic added by Ajinkya on Date: 03/06/2017  /////////////////////////
		 
		 String strfrdigitCardNo = ""+ con.getCount()%10000 ; 
		 
		 if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==2){
			 strfrdigitCardNo = "00"+strfrdigitCardNo;
		 }
		 else if(strfrdigitCardNo.length()!=0 &&strfrdigitCardNo.length()==3){
			 strfrdigitCardNo = "0"+strfrdigitCardNo; 
		 }
		 System.out.println("4 digit " +strfrdigitCardNo);
		 
	//////////////////////////////4 digit card No logic end Here //////////////////////////
		

		 		String	 code = productCode;
				String crdNo = code +" / "+strfrdigitCardNo ;
				Phrase cardNoValue = new Phrase(crdNo, font9);
				
				PdfPCell cardNoValueCell = new PdfPCell();	
				cardNoValueCell.addElement(cardNoValue);
			    cardNoValueCell.setBorder(0);
				serviceDetailstbl.addCell(cardNoValueCell);
		
		Phrase contractNo = new Phrase("CONTRACT NO:-", font9bold);
		PdfPCell contractNoCell = new PdfPCell();
		contractNoCell.addElement(contractNo);
		contractNoCell.setBorder(0);
		serviceDetailstbl.addCell(contractNoCell);

//		Phrase contractNoValue = new Phrase(strfrdigitCardNo +"/"+con.getGroup()+"/"+fmt1.format(con.getStartDate())+"/"+con.getCount(), font9); // con.getCount(); check     
		Phrase contractNoValue = new Phrase(con.getGroup()+"/"+fmt2.format(con.getStartDate())+"/"+strfrdigitCardNo, font9); 
		PdfPCell contractNoValueCell = new PdfPCell();
		contractNoValueCell.addElement(contractNoValue);
		contractNoValueCell.setBorder(0);
		serviceDetailstbl.addCell(contractNoValueCell);     

		// 3
		Phrase occupant = new Phrase("OCCUPANT:- ", font9bold);   
		PdfPCell occupantCell = new PdfPCell();
		occupantCell.addElement(occupant);
		occupantCell.setBorder(0);
		serviceDetailstbl.addCell(occupantCell);
		
		PdfPCell occupantValueCell = new PdfPCell();
		
		  ///////////////////////////
			
		    String salutation1 = "" ;
		    
			if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
				salutation1 = cust.getSalutation();
			}
			else{  
				 salutation1 = "" ;
			}
			
			 String occupantValue1 = " " ;
			 //////////////////////////////////////////////
			
		if(ser!=null )
		{
			if(custBranch != null){
				   if ( custBranch.getBusinessUnitName()!=null && !custBranch.getBusinessUnitName().equals("") )  
		           {
				  
			         occupantValue1 = custBranch.getBusinessUnitName() +"\n";
		           }  
		  
		         else if (ser.getCustomerName()!= null && !ser.getCustomerName().equals("") && !ser.getCustomerName().equals(cust.getName())){
		
		        	 occupantValue1 = salutation1 + ser.getCustomerName()+"\n";
                 }
		         else
		         {
		        	 occupantValue1 = " "+ser.getCustomerName() +"\n";
                 }
			}else{
				   if ( ser.getCustomerName()!=null && !ser.getCustomerName().equals("") && !ser.getCustomerName().equals(comp.getBusinessUnitName()))  
		           {
				  
			         occupantValue1 = salutation1+" "+ cust.getName() +"\n";
		           }  
		  
		         else if (ser.getCustomerName()!= null && !ser.getCustomerName().equals("") && !ser.getCustomerName().equals(cust.getName())){
		
		         occupantValue1 = salutation1 + ser.getCustomerName()+"\n";
                 }
		         else
		         {
        	      occupantValue1 = " "+ser.getCustomerName() +"\n";
                 }
			}
			
		      
		}
		else
		{
			if(custBranch != null){
				   if ( custBranch.getBusinessUnitName()!=null && !custBranch.getBusinessUnitName().equals("") )  
		           {
				  
			         occupantValue1 = custBranch.getBusinessUnitName() +"\n";
		           }  
		  
		         else if (ser.getCustomerName()!= null && !ser.getCustomerName().equals("") && !ser.getCustomerName().equals(cust.getName())){
		
		        	 occupantValue1 = salutation1 + ser.getCustomerName()+"\n";
              }
		         else
		         {
		        	 occupantValue1 = " "+ser.getCustomerName() +"\n";
              }
			}else{

				  if ( con.getCustomerFullName()!=null && !con.getCustomerFullName().equals("") && !con.getCustomerFullName().equals(comp.getBusinessUnitName()))  
		           {
				  
			         occupantValue1 = salutation1+" "+ cust.getName() +"\n";
		           }  
		  
		           else if (con.getCustomerFullName()!= null && !con.getCustomerFullName().equals("") && !con.getCustomerFullName().equals(cust.getName())){
		
		              occupantValue1 = salutation1 + con.getCustomerFullName()+"\n";
	               }
		          else
		          {
	   	            occupantValue1 = " "+con.getCustomerFullName() +"\n";
	              }
			}
			
		}
		  
		  
			/*
			 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by Rahul V 
			 * Used to take customer name from Uppercase to Only first letter Uppercase
			 */
			String nameInFirstLetterUpperCase=getFirstLetterUpperCase(occupantValue1.trim());
	                           ////////////////////// end here ///////////////// 
		
			 String addressline = "" ;
			  String localityline = "" ;
	   /////////////////////////////////
		   if(serAdd!=null){
		  
		  if(serAdd.getAddrLine2() != null && !serAdd.getAddrLine2().equals(""))   
			{
				addressline=serAdd.getAddrLine1()+", "+serAdd.getAddrLine2()+", "+"\n";
			}
			else
			{
				addressline= serAdd.getAddrLine1()+", "+"\n";
			}
			
			if((!serAdd.getLandmark().equals(""))&&(serAdd.getLocality().equals("")==false)){
				System.out.println("inside both null condition1");
				localityline= (serAdd.getLandmark()+", "+serAdd.getLocality()+", "+"\n"+serAdd.getCity()+" - "
					      +serAdd.getPin()+". ");
			}
			else if((!serAdd.getLandmark().equals(""))&&(serAdd.getLocality().equals("")==true)){
				System.out.println("inside both null condition 2");
				localityline= ""+ (serAdd.getLandmark()+", "+"\n"+ serAdd.getCity()+" - "
					           +serAdd.getPin()+". ");
			}
			
			else if((serAdd.getLandmark().equals(""))&&(serAdd.getLocality().equals("")==false)){
				System.out.println("inside both null condition 3");
				localityline= ""+(serAdd.getLocality()+", "+"\n"+serAdd.getCity()+" - "
					      +serAdd.getPin()+". ");
			}
			else if((serAdd.getLandmark().equals(""))&&(serAdd.getLocality().equals("")==true)){
				System.out.println("inside both null condition 4");
				localityline= ""+(serAdd.getCity()+" - "+serAdd.getPin()+". ");
			}
			 Phrase occupantVal = new Phrase(nameInFirstLetterUpperCase+"\n"+ addressline + localityline , font9); //
			 occupantValueCell.addElement(occupantVal);
		     occupantValueCell.setBorder(0);
			 serviceDetailstbl.addCell(occupantValueCell);
	}
	else
	{

		////////////////////// rohan suggested that take this address from customer ////////////////////// 
	
//		take service addrsss from customer or contract
		if(con.getCustomerServiceAddress().getAddrLine2() != null && !con.getCustomerServiceAddress().getAddrLine2().equals(""))   
		{
			addressline=con.getCustomerServiceAddress().getAddrLine1()+", "+con.getCustomerServiceAddress().getAddrLine2()+", "+"\n";
		}
		else
		{
			addressline= cust.getSecondaryAdress().getAddrLine1()+", "+"\n";
		}
		
		if((!con.getCustomerServiceAddress().getLandmark().equals(""))&&(con.getCustomerServiceAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			localityline= (con.getCustomerServiceAddress().getLandmark()+", "+con.getCustomerServiceAddress().getLocality()+", "+"\n"+con.getCustomerServiceAddress().getCity()+" - "
				      +cust.getSecondaryAdress().getPin()+". "+"\n" +cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		else if((!con.getCustomerServiceAddress().getLandmark().equals(""))&&(con.getCustomerServiceAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			localityline= (con.getCustomerServiceAddress().getLandmark()+", "+"\n"+ con.getCustomerServiceAddress().getCity()+" - "
				      +con.getCustomerServiceAddress().getPin()+". "+"\n"+con.getCustomerServiceAddress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		
		else if((con.getCustomerServiceAddress().getLandmark().equals(""))&&(con.getCustomerServiceAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			localityline= (con.getCustomerServiceAddress().getLocality()+", "+"\n"+con.getCustomerServiceAddress().getCity()+" - "
				      +con.getCustomerServiceAddress().getPin()+". "+"\n"+con.getCustomerServiceAddress().getState()+", "+con.getCustomerServiceAddress().getCountry()+". ");
		}
		else if((con.getCustomerServiceAddress().getLandmark().equals(""))&&(con.getCustomerServiceAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			localityline=(con.getCustomerServiceAddress().getCity()+" - "+con.getCustomerServiceAddress().getPin()+". "+"\n"+cust.getSecondaryAdress().getState()+", "+cust.getSecondaryAdress().getCountry()+". ");
		}
		
		 Phrase occupantVal = new Phrase(nameInFirstLetterUpperCase + addressline + localityline , font9); //
		 occupantValueCell.addElement(occupantVal);
	     occupantValueCell.setBorder(0);
		 serviceDetailstbl.addCell(occupantValueCell);
		
	}
		  /////////////////////////////
// 4
		Phrase tel = new Phrase("TEL:-", font9bold);
		PdfPCell telCell = new PdfPCell();
		telCell.addElement(tel);
		telCell.setBorder(0);
		serviceDetailstbl.addCell(telCell);
            
		PdfPCell telValueCell = new PdfPCell();
		Phrase telValue = new Phrase("",font9);
		if(custBranch!=null){
			if( custBranch.getLandline()!= null && custBranch.getLandline()!=0)  
			{
				
				System.out.println("inside tel Value");
				if(custBranch.getCellNumber1()==0){
					telValue = new Phrase(custBranch.getLandline()+"", font9);
				}
				else if(custBranch.getLandline()==0)
				{
				telValue = new Phrase( custBranch.getCellNumber1() + " ", font9);
				}
				else if(custBranch.getCellNumber1()!=0 && custBranch.getLandline()!=0)
				{
				telValue = new Phrase( custBranch.getCellNumber1() + " / " + custBranch.getLandline(), font9);
				}
			}
			else {
				System.out.println("inside cell Value");
				 telValue = new Phrase( cust.getCellNumber1()+"", font9);
			}
		}else{
			if( cust.getLandline()!= null && cust.getLandline()!=0)  
			{
				System.out.println("inside tel Value");
				if(cust.getCellNumber1()==0){
					telValue = new Phrase(cust.getLandline()+"", font9);
				}
				else if(cust.getLandline()==0)
				{
				telValue = new Phrase( cust.getCellNumber1() + " ", font9);
				}
				else if(cust.getCellNumber1()!=0 && cust.getLandline()!=0)
				{
				telValue = new Phrase( cust.getCellNumber1() + " / " + cust.getLandline(), font9);
				}
			}
			
			else {
				
				System.out.println("inside cell Value");
				 telValue = new Phrase( cust.getCellNumber1()+"", font9);
				
			}
		}
		
		
		
		telValueCell.addElement(telValue); 
		telValueCell.setBorder(0);   
		serviceDetailstbl.addCell(telValueCell);
		
		// 5
		Phrase fax = new Phrase("FAX:-", font9bold);
		PdfPCell faxCell = new PdfPCell();
		faxCell.addElement(fax);
		faxCell.setBorder(0);
		serviceDetailstbl.addCell(faxCell); 
		
		PdfPCell faxValueCell = new PdfPCell();
		Phrase faxValue = new Phrase("",font9);
			
		if(custBranch!=null){

			if( custBranch.getFaxNumber()!= null && custBranch.getFaxNumber()!=0)  
			{
	        	 faxValue = new Phrase("" + custBranch.getFaxNumber(), font9);
			}
			else {
				 faxValue = new Phrase(" ", font9);
	        	
			}
		}else{

			if( cust.getFaxNumber()!= null && cust.getFaxNumber()!=0)  
			{
	        	 faxValue = new Phrase("" + cust.getFaxNumber(), font9);
			}
			else {
				 faxValue = new Phrase(" ", font9);
	        	
			}
		}
		faxValueCell.addElement(faxValue);
		faxValueCell.setBorder(0);
		serviceDetailstbl.addCell(faxValueCell);

// 6
		Phrase email = new Phrase("E-MAIL :-", font9bold);
		PdfPCell emailCell = new PdfPCell();
		emailCell.addElement(email);
		emailCell.setBorder(0);
		serviceDetailstbl.addCell(emailCell);
		
		if(custBranch!=null){

		      if(custBranch.getEmail()!= null && !custBranch.getEmail().equals("")&&!custBranch.getEmail().equalsIgnoreCase("abc@gmail.com")&&!custBranch.getEmail().equalsIgnoreCase("xyz@gmail.com"))
		      {
		    	  System.out.println("true mail ");
		    	  Phrase emailValue = new Phrase("" + custBranch.getEmail(), font9);
		  		PdfPCell emailValueCell = new PdfPCell();
		  		emailValueCell.addElement(emailValue);
		  		emailValueCell.setBorder(0);  
		  		serviceDetailstbl.addCell(emailValueCell);
		    	  
		      }
		      
		      else
		      {
		    	  System.out.println("else null email printed");  
		    	  
		    	    Phrase emailValue = new Phrase(" ", font9);
		    		PdfPCell emailValueCell = new PdfPCell();
		    		emailValueCell.addElement(emailValue);
		    		emailValueCell.setBorder(0);  
		    		serviceDetailstbl.addCell(emailValueCell);
		    	  
		      }
		}else{

		      if(cust.getEmail()!= null && !cust.getEmail().equals("")&&!cust.getEmail().equalsIgnoreCase("abc@gmail.com")&&!cust.getEmail().equalsIgnoreCase("xyz@gmail.com"))
		      {
		    	  System.out.println("true mail ");
		    	  Phrase emailValue = new Phrase("" + cust.getEmail(), font9);
		  		PdfPCell emailValueCell = new PdfPCell();
		  		emailValueCell.addElement(emailValue);
		  		emailValueCell.setBorder(0);  
		  		serviceDetailstbl.addCell(emailValueCell);
		    	  
		      }
		      else
		      {
		    	  System.out.println("else null email printed");  
		    	  
		    	    Phrase emailValue = new Phrase(" ", font9);
		    		PdfPCell emailValueCell = new PdfPCell();
		    		emailValueCell.addElement(emailValue);
		    		emailValueCell.setBorder(0);  
		    		serviceDetailstbl.addCell(emailValueCell);
		    	  
		      }
		}
// 7
		Phrase contractExp = new Phrase("CONTRACT EXPIRES:-", font9bold);  
		PdfPCell contractExpCell = new PdfPCell();
		contractExpCell.addElement(contractExp);
		contractExpCell.setBorder(0);
		serviceDetailstbl.addCell(contractExpCell);
    System.out.println("contrac texpires: without format "+ con.getEndDate());
    System.out.println("contrac texpires: with format "+fmt.format(con.getEndDate()) );

    Phrase contractExpValue = new Phrase(""+ fmt.format(con.getEndDate()), font9);   
		PdfPCell contractExpValueCell = new PdfPCell();
		contractExpValueCell.addElement(contractExpValue);
		contractExpValueCell.setBorder(0);  
		serviceDetailstbl.addCell(contractExpValueCell);

// 8
		Phrase freq = new Phrase("FREQUENCY:-", font9bold);
		PdfPCell freqCell = new PdfPCell(freq);
		freqCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		freqCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqCell.setBorder(0);
		serviceDetailstbl.addCell(freqCell);  
		     
        
  	  
		 String ferqVal= "" ;
		 Phrase ferqValphrase =new Phrase("",font9);

  	     for(int i=0;i<con.getItems().size();i++)
  	     {
  	    	 if( con.getItems().get(i).getProductCode()== productCode){
  		     ferqVal=con.getItems().get(i).getProfrequency();
  		     if(ferqVal!=null && !ferqVal.equals(""))   
             { 
  		    	ferqValphrase = new Phrase(ferqVal, font9);
             }
           else  
             {
        	   ferqValphrase = new Phrase(" ", font9);
             }
  	      }
  	    
      }
		PdfPCell freqValueCell = new PdfPCell(ferqValphrase);
		freqValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		freqValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		freqValueCell.setBorder(0);
		serviceDetailstbl.addCell(freqValueCell);

// 9
		Phrase daytime = new Phrase("DAY & TIME :-", font9bold);
		PdfPCell daytimeCell = new PdfPCell(daytime);
		daytimeCell.setBorder(0);
		serviceDetailstbl.addCell(daytimeCell);
		
		////////////////////////////  week code  strat //////////////////////////
			
		// rohan added this code for week in service schedule  on date : 25-04-2017
		String week = getWeekFromService();
	
		Phrase daytimeValue= null;
		String serviceDayTime="";
		if(ser!=null)
		{
//	        daytimeValue = new Phrase(" " + ser.getServiceDateDay()!= null ? ser.getServiceDateDay() : "" +" " + week != null ? week : "" +" WEEK "+ ser.getServiceTime(), font9);
		
			/**
			 * Date 04-07-2018 By Vijay
			 * Des :- above old code commneted and below new code added for service Day and time
			 */
			if(ser.getServiceDateDay()!= null){
				serviceDayTime = ser.getServiceDateDay();
			}
			if(week != null){
				serviceDayTime = serviceDayTime + " "+week+" WEEK "+ ser.getServiceTime();
			}
			/**
			 * ends here
			 */
		}
		else{
//			daytimeValue = new Phrase(" "+ con.getServiceScheduleList().get(0).getScheduleServiceDay() != null ? con.getServiceScheduleList().get(0).getScheduleServiceDay() : "" +" "
//					+week +" WEEK "+con.getServiceScheduleList().get(0).getScheduleServiceTime(), font9);
			
			/**
			 * Date 04-07-2018 By Vijay
			 * Des :- above old code commneted and below new code added for service Day and time
			 */
			if(con.getServiceScheduleList().get(0).getScheduleServiceDay() != null){
				serviceDayTime = con.getServiceScheduleList().get(0).getScheduleServiceDay();
			}
			if(week != null){
				serviceDayTime = serviceDayTime + " "+week+" WEEK "+con.getServiceScheduleList().get(0).getScheduleServiceTime();
			}
			/**
			 * ends here
			 */
		}
		
		daytimeValue = new Phrase(serviceDayTime,font9);

		
		PdfPCell daytimeValueCell = new PdfPCell(daytimeValue);
		daytimeValueCell.setBorder(0);
		serviceDetailstbl.addCell(daytimeValueCell);
		
		// 10
		PdfPTable contactPresonTbl = new PdfPTable(2);
		contactPresonTbl.setWidthPercentage(100);

		/**
		 * nidhi
		 * 
		 */
		
		
		
		String custPocName1="";
		if(custBranch!=null){
				if(custBranch.getPocName()!=null && custBranch.getPocName().trim().length()>0 && custBranch.getPocCell()==0){
					custPocName1=custPocName1+getFirstLetterUpperCase(custBranch.getPocName());
				}else if(custBranch.getPocName()!=null && custBranch.getPocName().trim().length()>0) {
					custPocName1=custPocName1+getFirstLetterUpperCase(custBranch.getPocName())+" / "+custBranch.getPocCell();
				}
		}else{
			if(custContactList!=null&&custContactList.size()!=0){
				
				custPocName1="";
				if(custContactList.get(0).getCell()==0){
					custPocName1=custPocName1+getFirstLetterUpperCase(custContactList.get(0).getName());
				}else{
					custPocName1=custPocName1+getFirstLetterUpperCase(custContactList.get(0).getName())+" / "+custContactList.get(0).getCell();
				}
			
			}else{
				
				if(cust!=null&&cust.getSalutation()!=null&&!cust.getSalutation().equals("")){
					if(cust.getCellNumber1()==0){
						custPocName1=custPocName1+cust.getSalutation()+" "+getFirstLetterUpperCase(cust.getFullname());
					}else{
						custPocName1=custPocName1+cust.getSalutation()+" "+getFirstLetterUpperCase(cust.getFullname())+" / "+cust.getCellNumber1();
					}
				}else{
					custPocName1=getFirstLetterUpperCase(cust.getFullname())+" / "+cust.getCellNumber1();
				}
			}
			
		}
		
		//below if condition Or condition added by vijay as per sonu in case of branch it show the label and value (value may blank or anything)
		if(custPocName1.trim().length()>0 || custBranch!=null){
			Phrase contactPerson = new Phrase("CONTACT PERSON 1:-", font9bold);  
			PdfPCell contactPersonCell = new PdfPCell();
			contactPersonCell.addElement(contactPerson);
			contactPersonCell.setBorder(0);
			serviceDetailstbl.addCell(contactPersonCell);
			

			Phrase custPocNamePh1 = new Phrase(custPocName1, font9);  
			PdfPCell custPocNameCell1 = new PdfPCell();
			custPocNameCell1.addElement(custPocNamePh1);
			custPocNameCell1.setBorder(0);
			serviceDetailstbl.addCell(custPocNameCell1);
		
			
			if(custContactList!=null&&custContactList.size()>1 && custBranch == null){
				/**
				 * nidhi
				 * 
				 */
				Phrase contactPerson2 = new Phrase("CONTACT PERSON 2:-", font9bold);  
				PdfPCell contactPersonCell2 = new PdfPCell();
				contactPersonCell2.addElement(contactPerson2);
				contactPersonCell2.setBorder(0);
//				contactPresonTbl.addCell(contactPersonCell);
				serviceDetailstbl.addCell(contactPersonCell2);
				
				
				String custPocName2="";
				if(custContactList.get(1).getCell()==0){
				custPocName2=custPocName2+getFirstLetterUpperCase(custContactList.get(1).getName());
				}else{
				custPocName2=custPocName2+getFirstLetterUpperCase(custContactList.get(1).getName())+" / "+custContactList.get(1).getCell();
				}
				Phrase custPocNamePh2 = new Phrase(custPocName2, font9);  
				PdfPCell custPocNameCell2 = new PdfPCell();
				custPocNameCell2.addElement(custPocNamePh2);
				custPocNameCell2.setBorder(0);
				serviceDetailstbl.addCell(custPocNameCell2);
			}
		}
	
		
		
		
		
		/****
		 * 
		 * 
		 * 
		 */
//		////////////////
//		   String salutation = "" ;
//			if (cust.getSalutation()!= null && !cust.getSalutation().equals("")){
//				salutation = cust.getSalutation();
//			}  
//			else{
//				 salutation = "" ;
//			}
//		
//		///////////////
//			String serCustName = "";	
//		if(ser!=null)
//		{	
//		             if (ser.getCustomerName() == comp.getBusinessUnitName())
//		                {
//			              serCustName =  ser.getCustomerName() ;
//			              System.out.println("Service Cust name "+ser.getCustomerName());
//		                }
//		             else 
//		                {
//			              serCustName =  salutation  +" "+ ser.getPersonInfo().getPocName() ;
//			              System.out.println("Service Cust POC "+ser.getPersonInfo().getPocName());
//		                }
//		}
//		else 
//		{
//			   if (con.getCustomerFullName() == comp.getBusinessUnitName())
//               {
//	              serCustName =  con.getCustomerFullName() ;
//	              System.out.println("Service Cust name "+con.getCustomerFullName());
//               }
//            else 
//               {
//	              serCustName =  salutation  +" "+ con.getCinfo().getPocName() ;
//	              System.out.println("Service Cust POC "+con.getCinfo().getPocName());
//               }
//			
//		}
//        
//		String nameInFirstLetterUpperCase1=getFirstLetterUpperCase(serCustName.trim());
//		Phrase contactPersonValue = new Phrase( nameInFirstLetterUpperCase1, font9);  
//		PdfPCell contactPersonValueCell = new PdfPCell();
//		contactPersonValueCell.addElement(contactPersonValue);
//		contactPersonValueCell.setBorder(0);
//		contactPresonTbl.addCell(contactPersonValueCell);
//		
//		
//		try 
//		{
//			contactPresonTbl.setWidths(new float[] { 50, 50 });
//
//		}
//		catch (DocumentException e3)
//		{
//			e3.printStackTrace();
//		}
//
//
//		PdfPCell contactPresonTblCell = new PdfPCell();
//		contactPresonTblCell.addElement(contactPresonTbl);
//		contactPresonTblCell.setBorder(0);
//		serviceDetailstbl.addCell(contactPresonTblCell);
//
//		PdfPTable contactPresonMobNoTbl = new PdfPTable(2);
//		contactPresonMobNoTbl.setWidthPercentage(100);
//
//		Phrase contactMobNo = new Phrase("Mobile No:", font9bold);
//		PdfPCell contactMobNoCell = new PdfPCell();
//		contactMobNoCell.addElement(contactMobNo);
//		contactMobNoCell.setBorder(0);
//		contactPresonMobNoTbl.addCell(contactMobNoCell);
//
//		if (ser.getCustomerName() == comp.getBusinessUnitName()) {
//
//			Phrase contactMobNoVal = new Phrase("" + ser.getCellNumber(), font9);
//			PdfPCell contactMobNoValCell = new PdfPCell();
//			contactMobNoValCell.addElement(contactMobNoVal);
//			contactMobNoValCell.setBorder(0);
//			contactPresonMobNoTbl.addCell(contactMobNoValCell);
//		}
//		else 
//		   {
//			Phrase contactMobNoVal = new Phrase("" + comp.getPocCell(), font9);
//			PdfPCell contactMobNoValCell = new PdfPCell();
//			contactMobNoValCell.addElement(contactMobNoVal);
//			contactMobNoValCell.setBorder(0);
//			contactPresonMobNoTbl.addCell(contactMobNoValCell);
//		   }
//
//		try {
//			contactPresonMobNoTbl.setWidths(new float[] { 50, 50 });
//
//     		} catch (DocumentException e3) {
//			e3.printStackTrace();
//		    }
//
//		PdfPCell contactPresonMobNoTblCell = new PdfPCell();
//		contactPresonMobNoTblCell.addElement(contactPresonMobNoTbl);
//		contactPresonMobNoTblCell.setBorder(0);
//		serviceDetailstbl.addCell(contactPresonMobNoTblCell);
		
		
		/****
		 * 
		 */

		// 11
		Phrase premise = new Phrase("TYPE OF PREMISE:-", font9bold);
		PdfPCell premiseCell = new PdfPCell();
		premiseCell.addElement(premise);
		premiseCell.setBorder(0);
		serviceDetailstbl.addCell(premiseCell);
		
		PdfPCell premiseValueCell = new PdfPCell();
		String premises = "";
		////////////////////////////// Premises value ////////////////////
		Phrase premiseValue = new Phrase("  " , font9);
		
		for (int i = 0; i < con.getItems().size();i++)
		{
         if( con.getItems().get(i).getProductCode()== productCode)
            {
	          premises =  con.getItems().get(i).getPremisesDetails();
	          
	        if(premises!=null && !premises.equals(""))   
               { 
		        premiseValue = new Phrase(premises, font9);
               }
	        else  
	           {
	    	    premiseValue = new Phrase(" ", font9);
	           }
            }
}
        ////////////////////////////// End Here //////////////////////////		
		
		premiseValueCell.addElement(premiseValue);
		premiseValueCell.setBorder(0);    
		serviceDetailstbl.addCell(premiseValueCell);
//		
		// 12
		
		Phrase remarks = new Phrase("REMARKS :-", font9bold);
		PdfPCell remarksCell = new PdfPCell();
		remarksCell.addElement(remarks);
		remarksCell.setBorder(0);
		serviceDetailstbl.addCell(remarksCell);
		
		
		PdfPCell remarksValueCell = new PdfPCell();
		Phrase remarksValue = new Phrase(" ", font8);
      
		for (int i = 0; i < con.getItems().size(); i++) {
			if (con.getItems().get(i).getProductCode() == productCode) {
				String remark = con.getItems().get(i).getRemark();
				if (!remark.equals(null) && !remark.equals("")) {
					remarksValue = new Phrase(remark, font8);
				} else {
					remarksValue = new Phrase(" ", font8);
				}
			}
	    }
    	remarksValueCell.addElement(remarksValue);   
		remarksValueCell.setBorder(0);
		serviceDetailstbl.addCell(remarksValueCell);
		
		PdfPTable pdTbl = new PdfPTable(1);
		pdTbl.setWidthPercentage(100);

		PdfPCell headerTblCell = new PdfPCell();
//		headerTblCell.addElement(headertbl);
		headerTblCell.setBorder(0);

		PdfPCell serviceDetailstblCell = new PdfPCell();
		serviceDetailstblCell.addElement(serviceDetailstbl);
		serviceDetailstblCell.setBorder(0);

		pdTbl.addCell(headerTblCell);
		pdTbl.addCell(serviceDetailstblCell);

		PdfPCell pdTblCell = new PdfPCell();
		pdTblCell.addElement(pdTbl);
		pdTblCell.setBorder(0);


		PdfPTable emptyTbl = new PdfPTable(4);
		emptyTbl.setWidthPercentage(100);

		try {
			emptyTbl.setWidths(new float[] { 25, 25, 25, 25 });
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}

		for (int i = 0; i < 16; i++) {
			Phrase emptyphrse = new Phrase("   ", font9);
			PdfPCell empty1Cell = new PdfPCell();
			empty1Cell.addElement(emptyphrse);
			empty1Cell.setBorderWidthLeft(0);
			emptyTbl.addCell(empty1Cell);

			PdfPCell empty2Cell = new PdfPCell();
			empty2Cell.addElement(emptyphrse);
			emptyTbl.addCell(empty2Cell);

			PdfPCell empty3Cell = new PdfPCell();
			empty3Cell.addElement(emptyphrse);
			emptyTbl.addCell(empty3Cell);

			PdfPCell empty4Cell = new PdfPCell();
			empty4Cell.addElement(emptyphrse);
			empty4Cell.setBorderWidthRight(0);
			emptyTbl.addCell(empty4Cell);

		}

		PdfPCell srviceCardTblCell = new PdfPCell();

//		srviceCardTblCell.addElement(titleTbl);
//		srviceCardTblCell.addElement(emptyTbl);
		srviceCardTblCell.setBorder(0);

		PdfPTable parentTbl = new PdfPTable(2);
		parentTbl.setWidthPercentage(100);
		parentTbl.addCell(srviceCardTblCell);
		parentTbl.addCell(pdTblCell);

		try {
			parentTbl.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {

			e1.printStackTrace();
		}

		try {

			document.add(parentTbl);
			// document.add(Chunk.NEXTPAGE);

		} catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	// //////////////////////////////////////2nd Page Added by Ajinkya  29/12/2016 ////////////////////////////////////////////////////////

	  //Ajinkya added this code to convert customer name in CamelCase    
	//  Date : 12/4/2017

private String getFirstLetterUpperCase(String customerFullName)

{
	System.out.println("customerFullName "+customerFullName);
	String customerName="";
	String[] customerNameSpaceSpilt=customerFullName.split(" ");
	System.out.println("customerNameSpaceSpilt"+ customerNameSpaceSpilt.length);
  	System.out.println("customerNameSpaceSpilt size "+ customerNameSpaceSpilt.length);
  	logger.log(Level.SEVERE,"customerFullName size "+customerFullName.length());
  	logger.log(Level.SEVERE,"customerNameSpaceSpilt size "+ customerNameSpaceSpilt.length);
	int count=0;
	for (String name : customerNameSpaceSpilt)
	{
		String nameLowerCase=name.toLowerCase().trim();
	
		System.out.println("nameLowerCase"+ nameLowerCase);   
		if(nameLowerCase!=null && !name.equals(""))
		{
			
		
		if(count==0)
		{
			customerName=customerName.concat(name).toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			
			System.out.println("customerName"+customerName);
			logger.log(Level.SEVERE,"customerName"+customerName);
		}
		else
		{
			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
			System.out.println("customerName"+customerName);
		}
		count=count+1;  
		}
	}
	return customerName;    
}

	private void createServiceCaredRemarksForTMTbl() 
	{
		PdfPTable blnkTable = new  PdfPTable(1);
		blnkTable.setWidthPercentage(100);
		
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.setBorder(0);
		blnkTable.addCell(blnkCell);
		
		
		try {
			document.add(blnkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
//	{
//
//		Phrase srdate = new Phrase("DATE", font9);
//		Paragraph srdatePara = new Paragraph();
//		srdatePara.add(srdate);
//		srdatePara.add(Chunk.NEWLINE);
//		srdatePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell srDateCell = new PdfPCell();
//		srDateCell.addElement(srdatePara);
//		srDateCell.setBorderWidthLeft(0);
//
//		PdfPTable detailstbl = new PdfPTable(1);
//		detailstbl.setWidthPercentage(100);
//
//		Phrase title = new Phrase(" ", font9);
//		Paragraph titlePara = new Paragraph();
//		titlePara.add(title);
//		titlePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell titleCell = new PdfPCell();
//		titleCell.addElement(titlePara);
//		titleCell.setBorderWidthRight(0);
//		titleCell.setBorderWidthTop(0);
//		titleCell.setBorderWidthLeft(0);
//
//		Phrase no1 = new Phrase("7 NO.", font9);
//		Paragraph no1Para = new Paragraph();
//		no1Para.add(no1);
//		no1Para.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell no1Cell = new PdfPCell();
//		no1Cell.addElement(no1Para);
//		no1Cell.setBorderWidthBottom(0);
//		no1Cell.setBorderWidthTop(0);
//		no1Cell.setBorderWidthLeft(0);
//
//		Phrase no2 = new Phrase("18 NO.", font9);
//		Paragraph no2Para = new Paragraph();
//		no2Para.add(no2);
//		no2Para.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell no2Cell = new PdfPCell();
//		no2Cell.addElement(no2Para);
//		no2Cell.setBorderWidthBottom(0);
//		no2Cell.setBorderWidthTop(0);
//		// no2Cell.setBorderWidthRight(0);
//
//		Phrase nphrase = new Phrase("N", font9);
//		Paragraph nphrasePara = new Paragraph();
//		nphrasePara.add(nphrase);
//		nphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nphraseCell = new PdfPCell();
//		nphraseCell.addElement(nphrasePara);
//		nphraseCell.setBorderWidthBottom(0);
//		nphraseCell.setBorderWidthTop(0);
//		// nphraseCell.setBorderWidthRight(0);
//
//		Phrase hphrase = new Phrase("H", font9);
//		Paragraph hphrasePara = new Paragraph();
//		hphrasePara.add(hphrase);
//		hphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell hphraseCell = new PdfPCell();
//		hphraseCell.addElement(hphrasePara);
//		hphraseCell.setBorderWidthBottom(0);
//		hphraseCell.setBorderWidthTop(0);
//		hphraseCell.setBorderWidthRight(0);
//		// outTimeCell.setBorder(0);
//
//		PdfPTable NoTitleCellTbl = new PdfPTable(4);
//		NoTitleCellTbl.setWidthPercentage(100);
//
//		try {
//			NoTitleCellTbl.setWidths(new float[] { 25, 25, 25, 25 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		NoTitleCellTbl.addCell(no1Cell);
//		NoTitleCellTbl.addCell(no2Cell);
//		NoTitleCellTbl.addCell(nphraseCell);
//		NoTitleCellTbl.addCell(hphraseCell);
//
//		PdfPCell noValueCell = new PdfPCell();
//
//		noValueCell.addElement(NoTitleCellTbl);
//		noValueCell.setBorder(0);
//
//		detailstbl.addCell(titleCell);
//		detailstbl.addCell(noValueCell);
//
//		PdfPCell noCell = new PdfPCell();
//		noCell.addElement(detailstbl);
//
//		Phrase nameofworker = new Phrase("NAME OF WORKER", font9);
//		Paragraph nameofworkerPara = new Paragraph();
//		nameofworkerPara.add(nameofworker);
//		nameofworkerPara.add(Chunk.NEWLINE);
//		nameofworkerPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nameofworkerCell = new PdfPCell();
//		nameofworkerCell.addElement(nameofworkerPara);
//
//		Phrase remarks = new Phrase("REMARKS", font9);
//		Paragraph remarksPara = new Paragraph();
//		remarksPara.add(remarks);
//		remarksPara.add(Chunk.NEWLINE);
//		remarksPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell remarksCell = new PdfPCell();
//		remarksCell.addElement(remarksPara);
//		remarksCell.setBorderWidthRight(0);
//
//		PdfPTable titleTbl = new PdfPTable(4);
//		titleTbl.setWidthPercentage(100);
//
//		try {
//			titleTbl.setWidths(new float[] { 18, 32, 20, 30 });
//
//		} catch (DocumentException e1) {
//
//			e1.printStackTrace();
//		}
//
//		titleTbl.addCell(srDateCell);
//		titleTbl.addCell(noCell);
//		titleTbl.addCell(nameofworkerCell);
//		titleTbl.addCell(remarksCell);
//
//		PdfPTable emptyTbl = new PdfPTable(7);
//		emptyTbl.setWidthPercentage(100);
//
//		try {
//			emptyTbl.setWidths(new float[] { 18, 8, 8, 8, 8, 20, 30 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		for (int i = 0; i < 16; i++) {
//			Phrase emptyphrse = new Phrase("   ", font9);
//			PdfPCell empty1Cell = new PdfPCell();
//			empty1Cell.addElement(emptyphrse);
//			empty1Cell.setBorderWidthLeft(0);
//			emptyTbl.addCell(empty1Cell);
//
//			PdfPCell empty2Cell = new PdfPCell();
//			empty2Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty2Cell);
//
//			PdfPCell empty3Cell = new PdfPCell();
//			empty3Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty3Cell);
//
//			PdfPCell empty4Cell = new PdfPCell();
//			empty4Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty4Cell);
//
//			PdfPCell empty5Cell = new PdfPCell();
//			empty5Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty5Cell);
//
//			PdfPCell empty6Cell = new PdfPCell();
//			empty6Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty6Cell);
//
//			PdfPCell empty7Cell = new PdfPCell();
//			empty7Cell.addElement(emptyphrse);
//			empty7Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty7Cell);
//
//		}
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//			document.add(titleTbl);
//			document.add(emptyTbl);
//			document.add(Chunk.NEXTPAGE);
//
//		} catch (DocumentException e) {
//
//			e.printStackTrace();
//		}
//
//	}
//}
	private void createServiceCaredRemarksForRCTbl()
	{
		PdfPTable blnkTable = new  PdfPTable(1);
		blnkTable.setWidthPercentage(100);
		
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.setBorder(0);
		blnkTable.addCell(blnkCell);
		
		try {
			document.add(blnkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	{
//		
//		
//
//		Phrase srdate = new Phrase("DATE", font9);
//		Paragraph srdatePara = new Paragraph();
//		srdatePara.add(srdate);
//		srdatePara.add(Chunk.NEWLINE);
//		srdatePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell srDateCell = new PdfPCell();
//		srDateCell.addElement(srdatePara);
//		srDateCell.setBorderWidthLeft(0);
//
//		PdfPTable detailstbl = new PdfPTable(1);
//		detailstbl.setWidthPercentage(100);
//
//		Phrase title = new Phrase(" ", font9);
//		Paragraph titlePara = new Paragraph();
//		titlePara.add(title);
//		titlePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell titleCell = new PdfPCell();
//		titleCell.addElement(titlePara);
//		titleCell.setBorderWidthRight(0);
//		titleCell.setBorderWidthTop(0);
//		titleCell.setBorderWidthLeft(0);
//
//		Phrase traps = new Phrase("Traps", font9);
//		PdfPCell trapsCell = new PdfPCell(traps);
//		trapsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		trapsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		trapsCell.setBorderWidthBottom(0);
//		trapsCell.setBorderWidthTop(0);
////		trapsCell.setBorderWidthLeft(0);
//
//		Phrase baits = new Phrase("Baits", font9);
//		
//		PdfPCell baitsCell = new PdfPCell(baits);
//		baitsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		baitsCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		baitsCell.setBorderWidthBottom(0);
//		baitsCell.setBorderWidthTop(0);
//		// .setBorderWidthRight(0);
//
//		Phrase cakes = new Phrase("Cakes", font9);
//		PdfPCell cakesCell = new PdfPCell(cakes);
//		cakesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cakesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		cakesCell.setBorderWidthBottom(0);
//		cakesCell.setBorderWidthTop(0);
//		// .setBorderWidthRight(0);
//
//		Phrase glueBrd = new Phrase("Glue Board", font9);
//		
//		PdfPCell glueBrdCell = new PdfPCell();
//		glueBrdCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		cakesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		glueBrdCell.setBorderWidthBottom(0);
//		glueBrdCell.setBorderWidthTop(0);
////		glueBrdCell.setBorderWidthRight(0);
//		// .setBorder(0);
//		
//		Phrase blnkphrase = new Phrase("  ", font9);
//		PdfPCell blnkphraseCell = new PdfPCell(blnkphrase);
//		blnkphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		blnkphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		blnkphraseCell.setBorderWidthBottom(0);
//		blnkphraseCell.setBorderWidthTop(0);
//		blnkphraseCell.setBorderWidthRight(0);
//		// .setBorder(0);
//
//		PdfPTable NoTitleCellTbl = new PdfPTable(5);
//		NoTitleCellTbl.setWidthPercentage(100);
//
//		try
//		{
//			NoTitleCellTbl.setWidths(new float[] { 20, 20, 20, 20 ,20});
//		}
//		catch (DocumentException e2)
//		{
//			e2.printStackTrace();
//		}
//
//		NoTitleCellTbl.addCell(trapsCell);
//		NoTitleCellTbl.addCell(baitsCell);
//		NoTitleCellTbl.addCell(cakesCell);
//		NoTitleCellTbl.addCell(glueBrdCell);
//		NoTitleCellTbl.addCell(blnkphraseCell);
//
//		PdfPCell noValueCell = new PdfPCell();
//		noValueCell.addElement(NoTitleCellTbl);
//		noValueCell.setBorder(0);
//
//		detailstbl.addCell(titleCell);
//		detailstbl.addCell(noValueCell);
//
//		PdfPCell noCell = new PdfPCell();
//		noCell.addElement(detailstbl);
//
//		Phrase nameofworker = new Phrase("NAME OF WORKER", font9);
//		Paragraph nameofworkerPara = new Paragraph();
//		nameofworkerPara.add(nameofworker);
//		nameofworkerPara.add(Chunk.NEWLINE);
//		nameofworkerPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nameofworkerCell = new PdfPCell();
//		nameofworkerCell.addElement(nameofworkerPara);
//
//		Phrase remarks = new Phrase("REMARKS", font9);
//		Paragraph remarksPara = new Paragraph();
//		remarksPara.add(remarks);
//		remarksPara.add(Chunk.NEWLINE);
//		remarksPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell remarksCell = new PdfPCell();
//		remarksCell.addElement(remarksPara);
//		remarksCell.setBorderWidthRight(0);
//
//		PdfPTable titleTbl = new PdfPTable(4);
//		titleTbl.setWidthPercentage(100);
//
//		try {
//			titleTbl.setWidths(new float[] { 15, 35, 20, 30 });
//
//		} catch (DocumentException e1) {
//
//			e1.printStackTrace();
//		}
//                                                  
//		titleTbl.addCell(srDateCell);
//		titleTbl.addCell(noCell);
//		titleTbl.addCell(nameofworkerCell);
//		titleTbl.addCell(remarksCell);
//
//		PdfPTable emptyTbl = new PdfPTable(8);
//		emptyTbl.setWidthPercentage(100);
//
//		try {
//			emptyTbl.setWidths(new float[] { 15, 7, 7, 7, 7,7, 20, 30 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		for (int i = 0; i < 16; i++) {
//			Phrase emptyphrse = new Phrase("   ", font9);
//			PdfPCell empty1Cell = new PdfPCell();
//			empty1Cell.addElement(emptyphrse);
//			empty1Cell.setBorderWidthLeft(0);
//			emptyTbl.addCell(empty1Cell);
//
//			PdfPCell empty2Cell = new PdfPCell();
//			empty2Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty2Cell);
//
//			PdfPCell empty3Cell = new PdfPCell();
//			empty3Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty3Cell);
//
//			PdfPCell empty4Cell = new PdfPCell();
//			empty4Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty4Cell);
//
//			PdfPCell empty5Cell = new PdfPCell();
//			empty5Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty5Cell);
//
//			PdfPCell empty6Cell = new PdfPCell();
//			empty6Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty6Cell);
//
//			PdfPCell empty7Cell = new PdfPCell();
//			empty7Cell.addElement(emptyphrse);
//			empty7Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty7Cell);
//			
//			PdfPCell empty8Cell = new PdfPCell();
//			empty8Cell.addElement(emptyphrse);
//			empty8Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty8Cell);
//
//
//		}
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//			document.add(titleTbl);
//			document.add(emptyTbl);
//			document.add(Chunk.NEXTPAGE);
//
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	
//		
//	}
//}
	private void createServiceCaredRemarksForGIPCAndGelTbl()
	{
		PdfPTable blnkTable = new  PdfPTable(1);
		blnkTable.setWidthPercentage(100);
		
		PdfPCell blnkCell = new PdfPCell ();
		blnkCell.setBorder(0);
		blnkTable.addCell(blnkCell);
		
		try {
			document.add(blnkTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	{
//
//		
//		Phrase srdate = new Phrase("DATE", font9);
//		Paragraph srdatePara = new Paragraph();
//		srdatePara.add(srdate);
//		srdatePara.add(Chunk.NEWLINE);
//		srdatePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell srDateCell = new PdfPCell();
//		srDateCell.addElement(srdatePara);
//		srDateCell.setBorderWidthLeft(0);
//
//		PdfPTable detailstbl = new PdfPTable(1);
//		detailstbl.setWidthPercentage(100);
//
//		Phrase title = new Phrase(" ", font9);
//		Paragraph titlePara = new Paragraph();
//		titlePara.add(title);
//		titlePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell titleCell = new PdfPCell();
//		titleCell.addElement(titlePara);
//		titleCell.setBorderWidthRight(0);
//		titleCell.setBorderWidthTop(0);
//		titleCell.setBorderWidthLeft(0);
//
//		Phrase two = new Phrase("2", font9);    
//		Paragraph twoPara = new Paragraph();
//		twoPara.add(two);
//		twoPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell twoCell = new PdfPCell();
//		twoCell.addElement(twoPara);
//		twoCell.setBorderWidthBottom(0);
//		twoCell.setBorderWidthTop(0);
//		twoCell.setBorderWidthLeft(0);
//
//		Phrase eight = new Phrase("8", font9);
//		Paragraph eightPara = new Paragraph();
//		eightPara.add(eight);
//		eightPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell eightCell = new PdfPCell();
//		eightCell.addElement(eightPara);
//		eightCell.setBorderWidthBottom(0);
//		eightCell.setBorderWidthTop(0);
//		// no2Cell.setBorderWidthRight(0);
//
//		Phrase nphrase = new Phrase("N", font9);
//		Paragraph nphrasePara = new Paragraph();
//		nphrasePara.add(nphrase);   
//		nphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nphraseCell = new PdfPCell();
//		nphraseCell.addElement(nphrasePara);
//		nphraseCell.setBorderWidthBottom(0);
//		nphraseCell.setBorderWidthTop(0);
//		// nphraseCell.setBorderWidthRight(0);
//
//		Phrase kmphrase = new Phrase("K/M", font9);
//		Paragraph kmphrasePara = new Paragraph();
//		kmphrasePara.add(kmphrase);
//		kmphrasePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell kmphraseCell = new PdfPCell();
//		kmphraseCell.addElement(kmphrasePara);
//		kmphraseCell.setBorderWidthBottom(0);
//		kmphraseCell.setBorderWidthTop(0);
//		kmphraseCell.setBorderWidthRight(0);
//		// outTimeCell.setBorder(0);
//		
//		Phrase paste = new Phrase("Paste", font9);
//		Paragraph pastePara = new Paragraph();
//		pastePara.add(paste);
//		pastePara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell pasteCell = new PdfPCell();
//		pasteCell.addElement(pastePara);
//		pasteCell.setBorderWidthBottom(0);
//		pasteCell.setBorderWidthTop(0);
//		pasteCell.setBorderWidthRight(0);
//		// outTimeCell.setBorder(0);
//
//		PdfPTable NoTitleCellTbl = new PdfPTable(5);
//		NoTitleCellTbl.setWidthPercentage(100);
//
//		try {
//			NoTitleCellTbl.setWidths(new float[] { 20, 20, 20, 20, 20 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		NoTitleCellTbl.addCell(twoCell);
//		NoTitleCellTbl.addCell(eightCell);
//		NoTitleCellTbl.addCell(nphraseCell);
//		NoTitleCellTbl.addCell(kmphraseCell);
//		NoTitleCellTbl.addCell(pasteCell);
//
//		PdfPCell noValueCell = new PdfPCell();
//
//		noValueCell.addElement(NoTitleCellTbl);
//		noValueCell.setBorder(0);
//
//		detailstbl.addCell(titleCell);
//		detailstbl.addCell(noValueCell);
//
//		PdfPCell noCell = new PdfPCell();
//		noCell.addElement(detailstbl);
//
//		Phrase nameofworker = new Phrase("NAME OF WORKER", font9);
//		Paragraph nameofworkerPara = new Paragraph();
//		nameofworkerPara.add(nameofworker);
//		nameofworkerPara.add(Chunk.NEWLINE);
//		nameofworkerPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell nameofworkerCell = new PdfPCell();
//		nameofworkerCell.addElement(nameofworkerPara);
//
//		Phrase remarks = new Phrase("REMARKS", font9);
//		Paragraph remarksPara = new Paragraph();
//		remarksPara.add(remarks);
//		remarksPara.add(Chunk.NEWLINE);
//		remarksPara.setAlignment(Element.ALIGN_CENTER);
//		PdfPCell remarksCell = new PdfPCell();
//		remarksCell.addElement(remarksPara);
//		remarksCell.setBorderWidthRight(0);
//
//		PdfPTable titleTbl = new PdfPTable(4);
//		titleTbl.setWidthPercentage(100);
//
//		try {
//			titleTbl.setWidths(new float[] { 15, 35, 20, 30 });
//
//		} catch (DocumentException e1) {
//
//			e1.printStackTrace();
//		}
//		                                          
//		titleTbl.addCell(srDateCell);
//		titleTbl.addCell(noCell);
//		titleTbl.addCell(nameofworkerCell);
//		titleTbl.addCell(remarksCell);
//
//		PdfPTable emptyTbl = new PdfPTable(8);
//		emptyTbl.setWidthPercentage(100);
//
//		try {
//			emptyTbl.setWidths(new float[] { 15, 7, 7, 7, 7,7, 20, 30 });
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
//
//		for (int i = 0; i < 16; i++) {
//			Phrase emptyphrse = new Phrase("   ", font9);
//			PdfPCell empty1Cell = new PdfPCell();
//			empty1Cell.addElement(emptyphrse);
//			empty1Cell.setBorderWidthLeft(0);
//			emptyTbl.addCell(empty1Cell);
//
//			PdfPCell empty2Cell = new PdfPCell();
//			empty2Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty2Cell);
//
//			PdfPCell empty3Cell = new PdfPCell();
//			empty3Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty3Cell);
//
//			PdfPCell empty4Cell = new PdfPCell();
//			empty4Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty4Cell);
//
//			PdfPCell empty5Cell = new PdfPCell();
//			empty5Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty5Cell);
//
//			PdfPCell empty6Cell = new PdfPCell();
//			empty6Cell.addElement(emptyphrse);
//			emptyTbl.addCell(empty6Cell);
//
//			PdfPCell empty7Cell = new PdfPCell();
//			empty7Cell.addElement(emptyphrse);
//			empty7Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty7Cell);
//			
//			PdfPCell empty8Cell = new PdfPCell();
//			empty8Cell.addElement(emptyphrse);
//			empty8Cell.setBorderWidthRight(0);
//			emptyTbl.addCell(empty8Cell);
//
//		}
//
//		try {
//			document.add(Chunk.NEXTPAGE);
//			document.add(titleTbl);
//			document.add(emptyTbl);
//			document.add(Chunk.NEXTPAGE);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//	}
//}
	
	private String getWeekFromService() 
	{
		
		
		String week ="";
		
		if(ser==null){
			if(con.getServiceScheduleList().get(0).getWeekNo()==1){
				week=con.getServiceScheduleList().get(0).getWeekNo()+" ST";	
			}else if(con.getServiceScheduleList().get(0).getWeekNo()==2){
				week=con.getServiceScheduleList().get(0).getWeekNo()+" ND";
			}else if (con.getServiceScheduleList().get(0).getWeekNo()==3) {
				week=con.getServiceScheduleList().get(0).getWeekNo()+" RD";
			}
			else{
				week=con.getServiceScheduleList().get(0).getWeekNo()+" TH";
			}
			return week;
		}
		else{
		if(ser.getServiceWeekNo()==1){
			week=ser.getServiceWeekNo()+" ST";
		}else if(ser.getServiceWeekNo()==2){
			week=ser.getServiceWeekNo()+" ND";
		}else if(ser.getServiceWeekNo()==3){
			week=ser.getServiceWeekNo()+" RD";
		}else{
			week=ser.getServiceWeekNo()+" TH";
		}
		
		return week;
		
		}
	}
	
}
