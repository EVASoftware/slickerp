package com.slicktechnologies.server.addhocprinting.Pcamb;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class PcambContractRenewalLetterpdf {
	Company comp;
	Customer cust;
	Contract con;
	Service ser;
	ServerAppUtility serapp = new ServerAppUtility();
	public Document document;
	ContractRenewal conRenw;
	List<ContractCharges> billingTaxesLis;
	Invoice invoiceentity;
	List<ArticleType> articletype2;
	private Font font16boldul, font12bold, font8bold, font9boldul, font8,
			font16bold, font12boldul, font12, font14bold, font10, font10ul,// font9,
			font10bold, font10boldul, font9, font9bold, font16, font9ul;

	Phrase chunk;
	PdfPCell pdfcode, pdfname, pdfduration, pdfservices, pdfprice, pdftax,
			pdfnetPay;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd MMM yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	DecimalFormat commadf = new DecimalFormat("#,###.00");
	BaseFont base = null;

	Logger logger = Logger.getLogger("Contract Renewal PDf");
	
	/**
	 * Date : 19-12-2017 By Anil
	 * 
	 */
	boolean descriptionFlag=false;
	int noOfLines=10;
	boolean annexureFlag=false;
	int productSrNo=0;
	/**
	 * End
	 */

	public PcambContractRenewalLetterpdf() {
		super();

		try {
			logger.log(Level.SEVERE,"BASE FONT SET");
			base = BaseFont.createFont("fonts\\VERDANA.TTF",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
		} catch (DocumentException e) {
			logger.log(Level.SEVERE,"DOC EXCPN "+e.getMessage());
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.log(Level.SEVERE,"IO EXCPN "+e.getMessage());
			e.printStackTrace();
		}
		font16 = new Font(base, 16);
		font16bold = new Font(base, 16, Font.BOLD);
		font16boldul = new Font(base, 16, Font.BOLD | Font.UNDERLINE);
		new Font(base, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(base, 12, Font.BOLD);
		font8bold = new Font(base, 8, Font.BOLD);
		font8 = new Font(base, 8);
		font9 = new Font(base, 9);
		font12boldul = new Font(base, 12, Font.BOLD | Font.UNDERLINE);
		font12 = new Font(base, 12);
		font14bold = new Font(base, 14, Font.BOLD);
		font10 = new Font(base, 10, Font.NORMAL);
		font10bold = new Font(base, 10, Font.BOLD);
		font10boldul = new Font(base, 10, Font.BOLD | Font.UNDERLINE);
		font10ul = new Font(base, 10, Font.UNDERLINE);
		font9bold = new Font(base, 9, Font.BOLD);
		font9boldul = new Font(base, 9, Font.BOLD | Font.UNDERLINE);
		font9ul = new Font(base, 9, Font.BOLD | Font.UNDERLINE);

	}

	public void setContractRewnewal(ContractRenewal conRenewal) {
		conRenw = conRenewal;

		if (conRenw.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", conRenw.getCompanyId()).first().now();

		if (conRenw.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", conRenw.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("companyId", conRenw.getCompanyId())
					.filter("count", conRenw.getCustomerId()).first().now();

		if (conRenw.getCompanyId() == null)
			con = ofy().load().type(Contract.class)
					.filter("count", conRenw.getContractId()).first().now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", conRenw.getCompanyId())
					.filter("count", conRenw.getContractId()).first().now();

		// load Service

		System.out.println("contract id- " + conRenw.getContractId());
		System.out.println("company id-" + conRenw.getCompanyId());

		if (conRenw.getCompanyId() == null)
			ser = ofy().load().type(Service.class).first().now();
		else
			ser = ofy().load().type(Service.class)
					.filter("companyId", conRenw.getCompanyId())
					.filter("contractCount", conRenw.getContractId()).first()
					.now();

		if (conRenw.getCompanyId() == null)

			invoiceentity = ofy().load().type(Invoice.class).first().now();
		else {
			invoiceentity = ofy().load().type(Invoice.class)
					.filter("companyId", conRenw.getCompanyId()).first().now();
		}

		billingTaxesLis = invoiceentity.getBillingTaxes();

		// load article type

		articletype2 = new ArrayList<ArticleType>();
		if (comp.getArticleTypeDetails().size() != 0) {
			articletype2.addAll(comp.getArticleTypeDetails());
		}
	}

	public void createPdf() {

		createHeader();
		createToPara();
		createRenewalLetterPara();
		if(annexureFlag){
			generateNewProductTblWithAnnexure(productSrNo);
		}

	}

	private void generateNewProductTblWithAnnexure(int productSrNo2) {
		PdfPTable annexTbl=new PdfPTable(1);
		annexTbl.setWidthPercentage(100f);
		Phrase annexDetPh = new Phrase("Annexure Details : ", font10);
		PdfPCell annecDetCell = new PdfPCell();
		annecDetCell.addElement(annexDetPh);
		annecDetCell.setBorder(0);
		annexTbl.addCell(annecDetCell);
		
		try {
			document.add(Chunk.NEXTPAGE);
			document.add(annexTbl);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		noOfLines=28;
		annexureFlag=false;
		
		PdfPTable productTable = new PdfPTable(4);
		productTable.setWidthPercentage(100);

		try {
			productTable.setWidths(new float[] { 28, 16, 28, 28 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		//
		
//		String string = "\u20B9";
//	    byte[] utf8;
//		try {
//			logger.log(Level.SEVERE,"UTF FONT");
//			utf8 = string.getBytes("UTF-8");
//			string = new String(utf8, "UTF-8");
//		} catch (UnsupportedEncodingException e1) {
//			// TODO Auto-generated catch block
//			logger.log(Level.SEVERE,"EXCEPTION ");
//			e1.printStackTrace();
//		}

	    
		Phrase treatment = new Phrase(" Treatment ", font10);
		Paragraph treatmentPara = new Paragraph();
		treatmentPara.add(treatment);
		treatmentPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell treatmentCell = new PdfPCell();
		treatmentCell.addElement(treatmentPara);
		productTable.addCell(treatmentCell);

		Phrase frequency = new Phrase(" Frequency", font10);
		Paragraph frequencyPara = new Paragraph();
		frequencyPara.add(frequency);
		frequencyPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell frequencytCell = new PdfPCell();
		frequencytCell.addElement(frequencyPara);
		productTable.addCell(frequencytCell);

		Phrase prevChrages = new Phrase(" Previous Charges [INR]", font10);
		Paragraph prevChragesPara = new Paragraph();
		prevChragesPara.add(prevChrages);
		prevChragesPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell prevChragesCell = new PdfPCell();
		prevChragesCell.addElement(prevChragesPara);
		productTable.addCell(prevChragesCell);

		Phrase renewCharges = new Phrase(" Renewal Charges [INR]", font10);
		Paragraph renewChargesPara = new Paragraph();
		renewChargesPara.add(renewCharges);
		renewChargesPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell renewChargesCell = new PdfPCell();
		renewChargesCell.addElement(renewChargesPara);
		productTable.addCell(renewChargesCell);

		for (int i = productSrNo2; i < conRenw.getItems().size(); i++) {
			/**
			 * Date : 19-12-2017 By ANIL
			 */
			if(noOfLines<=0){
				annexureFlag=true;
				productSrNo=i;
				break;
			}
			int length=conRenw.getItems().get(i).getProductName().length();
			if(length>24){
				noOfLines=noOfLines-1;
			}
			noOfLines=noOfLines-1;
			/**
			 * End
			 */
			PdfPCell treatmentValCell = new PdfPCell();
			Paragraph treatmentValPara = new Paragraph();

			Phrase treatmentVal = new Phrase(" ", font10);

			/**
			 * Date : 22-12-2017 By ANIL 
			 * remark should not be print along with treatment name
			 */
//			if (conRenw.getItems().get(i).getRemark() != null) {
//				treatmentVal = new Phrase(conRenw.getItems().get(i).getProductName()+ " " + conRenw.getItems().get(i).getRemark(), font10);
//				treatmentValPara.add(treatmentVal);
//				treatmentValPara.add(Chunk.NEWLINE);
//				treatmentValCell.addElement(treatmentValPara);
//			} else if (conRenw.getItems().get(i).getRemark() == null) {
				treatmentVal = new Phrase(conRenw.getItems().get(i).getProductName(), font10);
				treatmentValPara.add(treatmentVal);
				treatmentValPara.add(Chunk.NEWLINE);
				treatmentValCell.addElement(treatmentValPara);
//			}
			treatmentValPara.setAlignment(Element.ALIGN_LEFT);
			productTable.addCell(treatmentValCell);

			PdfPCell frequencyValCell = new PdfPCell();
			Paragraph frequencyValPara = new Paragraph();
			String ferqVal = " ";
			if(con.getItems().get(i).getProfrequency()!=null){
				ferqVal=con.getItems().get(i).getProfrequency();
			}else{
				ferqVal="";
			}
			Phrase frequencyVal = new Phrase(ferqVal, font10);
			frequencyValPara.add(frequencyVal);
			frequencyValPara.add(Chunk.NEWLINE);
			frequencyValPara.setAlignment(Element.ALIGN_CENTER);
			frequencyValCell.addElement(frequencyValPara);
			productTable.addCell(frequencyValCell);

			Phrase oldprice = new Phrase(df.format(conRenw.getItems().get(i).getOldProductPrice())+ " ", font9);
			PdfPCell oldpricecell = new PdfPCell(oldprice);
			oldpricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(oldpricecell);

			double newprice;
			newprice = conRenw.getItems().get(i).getPrice();
			Phrase newchargeval = new Phrase(df.format(newprice) + " ", font9);
			PdfPCell newchargevaluecell = new PdfPCell(newchargeval);
			newchargevaluecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			productTable.addCell(newchargevaluecell);

		}
		try {
			document.add(productTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		if(annexureFlag){
			generateNewProductTblWithAnnexure(productSrNo);
		}
	}

	private void createRenewalLetterPara()
	{
		String custAdd1 = "";
		String custFullAdd1 = "";
		String pinNo = "";
		String pinNostr = "";
		// ///////////// Added by Ajinkya
		if (ser != null) {
			if (ser.getAddress() != null && ser.getAddress().getPin() != 0) {
				pinNo = "" + ser.getAddress().getPin();
				System.out.println("pinNo " + pinNo);
				if (pinNo.length() > 3) {
					pinNostr = pinNo.substring(0, 3) + " "
							+ pinNo.substring(3, 6);
					System.out.println("Substring " + pinNostr);
				} else {
					pinNostr = pinNo;
				}
				// end here
			}

			System.out.println("ServiceAddress"+ ser.getAddress().getAddrLine2());
			if (ser.getAddress().getAddrLine2() != null
					&& !ser.getAddress().getAddrLine2().equals("")) {
				custAdd1 = ser.getAddress().getAddrLine1() + ", "
						+ ser.getAddress().getAddrLine2() + ", ";
			} else {
				custAdd1 = ser.getAddress().getAddrLine1() + ", ";
			}

			if ((ser.getAddress().getLandmark() != null)
					&& (!ser.getAddress().getLandmark().equals(""))
					&& (ser.getAddress().getLocality().equals("") == false)) {
				System.out.println("inside both null condition1");
				custFullAdd1 = (ser.getAddress().getLandmark() + ", "
						+ ser.getAddress().getLocality() + ", "
						+ ser.getAddress().getCity() + " - " + pinNostr + ". "
						+ ser.getAddress().getState() + ", "
						+ ser.getAddress().getCountry() + ". ");
			} else if ((ser.getAddress().getLandmark() != null)
					&& (!ser.getAddress().getLandmark().equals(""))
					&& (ser.getAddress().getLocality().equals("") == true)) {
				System.out.println("inside both null condition 2");
				custFullAdd1 = (ser.getAddress().getLandmark() + ", "
						+ ser.getAddress().getCity() + " - " + pinNostr + ". "
						+ ser.getAddress().getState() + ", "
						+ ser.getAddress().getCountry() + ". ");
			} else if ((ser.getAddress().getLandmark() != null)
					&& (ser.getAddress().getLandmark().equals(""))
					&& (ser.getAddress().getLocality().equals("") == false)) {
				System.out.println("inside both null condition 3");
				custFullAdd1 = (ser.getAddress().getLocality() + ", "
						+ ser.getAddress().getCity() + " - " + pinNostr + ". "
						+ ser.getAddress().getState() + ", "
						+ ser.getAddress().getCountry() + ". ");
			} else if ((ser.getAddress().getLandmark() != null)
					&& (ser.getAddress().getLandmark().equals(""))
					&& (ser.getAddress().getLocality().equals("") == true)) {
				System.out.println("inside both null condition 4");
				custFullAdd1 = (ser.getAddress().getCity() + " - " + pinNostr
						+ ". " + ser.getAddress().getState() + ", "
						+ ser.getAddress().getCountry() + ". ");
			}

		}
		// ////////////////////// calc days to month //////////////////////////

		Paragraph refPara = new Paragraph();
		refPara.setAlignment(Element.ALIGN_JUSTIFIED);
		Phrase refLbl = new Phrase("Ref: ", font10boldul);
		refPara.add(refLbl);

		Phrase Sub1 = new Phrase(
				"Renewal Of Annual Pest Management Solution Order Of Your Office Premises Of "
						+ custAdd1 + custFullAdd1 + "For The Period From "
						+ fmt1.format(con.getStartDate()) + " To "
						+ fmt1.format(con.getEndDate()), font10ul);
		refPara.add(Sub1);
		refPara.add(Chunk.NEWLINE);
//		refPara.add(Chunk.NEWLINE);

		// Phrase Sub2 = new Phrase (custAdd1 +custFullAdd1,font10ul);
		// refPara.add(Sub2);

		// Phrase Sub3 = new Phrase (" +". ",font10ul);
		// refPara.add(Sub3);

//		refPara.add(Chunk.NEWLINE);
//		refPara.add(Chunk.NEWLINE);
		String sacvalue = " ";
		for (int i = 0; i < this.articletype2.size(); i++) {
			System.out.println(articletype2.get(i).getArticleTypeName());
			System.out.println(articletype2.get(i).getArticlePrint());
			System.out.println(articletype2.get(i).getDocumentName());
			if (articletype2.get(i).getArticleTypeName().equalsIgnoreCase("SAC")
					&& articletype2.get(i).getArticlePrint().equalsIgnoreCase("YES")
					&& articletype2.get(i).getDocumentName().equalsIgnoreCase("ContractRenewal")) {
				sacvalue = articletype2.get(i).getArticleTypeValue();
			}
		}
		String gstval = serapp.getGSTINOfCompany(comp, conRenw.getBranch());

		Phrase sub4 = new Phrase("GSTIN:" + gstval + "--" + "SAC:" + sacvalue,font9boldul);
		refPara.add(sub4);

		
		refPara.add(Chunk.NEWLINE);

		// rohan added this code as per salutation in customer here we will add
		// dear sir / madam
		// Date : 13/2/2017 used by Ajinkya

		String salutation = "";
		if (cust.getSalutation() != null && !cust.getSalutation().equals("")) {
			if (cust.getSalutation().equalsIgnoreCase("Mr.")) {
				salutation = "Dear Sir,";
			} else if (cust.getSalutation().equalsIgnoreCase("Ms.")) {
				salutation = "Dear Madam,";
			} else {
				salutation = "Dear Sir / Madam,";
			}
		} else {
			salutation = "Dear Sir / Madam,";
		}

		// ///////////////////////////////////////////////////
		Date startDate1 = con.getStartDate();
		Date endDate1 = con.getEndDate();

		Calendar startCalendar1 = new GregorianCalendar();
		startCalendar1.setTime(startDate1);
		Calendar endCalendar1 = new GregorianCalendar();
		endCalendar1.setTime(endDate1);

		int diffYear1 = endCalendar1.get(Calendar.YEAR)
				- startCalendar1.get(Calendar.YEAR);
		int diffMonth1 = diffYear1 * 12 + endCalendar1.get(Calendar.MONTH)
				- startCalendar1.get(Calendar.MONTH);
		//
		// ////////////////////// calc days to month //////////////////////
		//
		logger.log(Level.SEVERE, "month" + diffMonth1);
		String months = diffMonth1 + " months";
		// //////////////////////////////////////////////////

		Phrase Start = new Phrase(salutation, font10);
		refPara.add(Start);
		refPara.add(Chunk.NEWLINE);
//		refPara.add(Chunk.NEWLINE);

		Phrase sentence1 = new Phrase(
				"This has reference to the above subject matter, we wish to confirm as under.",
				font10);
		refPara.add(sentence1);
		refPara.add(Chunk.NEWLINE);
		// refPara.add(Chunk.NEWLINE);

		Phrase sentence2 = new Phrase(
				"Our previous order of Comprehensive Pest management solution for your above mentioned "
						+ "premises is expiring from "
						+ fmt.format(con.getEndDate()) + ". ", font10);
		refPara.add(sentence2);
		refPara.add(Chunk.NEWLINE);
		// refPara.add(Chunk.NEWLINE);

//		Phrase sentence3 = new Phrase(
//				"Owing to the rising costs of delivering services, particularly high cost of manpower and insecticides costs, we are compelled to",
//				font10);
//		Phrase sentence4 = new Phrase(
//				"marginally increase our annual charges to compensate for these rising costs. ",
//				font10);
//		Phrase sentence5 = new Phrase(
//				"We would like to continue our services for further " + months
//						+ " from " + fmt1.format(con.getStartDate()) + " To "
//						+ fmt1.format(con.getEndDate())
//						+ ",as per the details given below : ", font10);
//
//		boolean priceHikeFlag = false;
//
//		for (int i = 0; i < conRenw.getItems().size(); i++) {
//			if (conRenw.getItems().get(i).getPrice() > conRenw.getItems()
//					.get(i).getOldProductPrice()) {
//				priceHikeFlag = true;
//			}
//		}
//
//		if (priceHikeFlag == true) {
//			System.out.println("inside price Flag ");
//			refPara.add(sentence3);
//			refPara.add(Chunk.NEWLINE);
//			refPara.add(sentence4);
//			refPara.add(Chunk.NEWLINE);
////			refPara.add(Chunk.NEWLINE);
//
//		}
//
//		refPara.add(sentence5);
////		refPara.add(Chunk.NEWLINE);
//		refPara.add(Chunk.NEWLINE);

		//

		/**
		 * Date : 19-12-2017 BY ANIL
		 * If description text is present then we will make the value of descrptionFlag to true
		 * which will be used for page break
		 */
		if(conRenw.getDescription()!=null&&!conRenw.getDescription().equals("")){
			descriptionFlag=true;
			noOfLines=noOfLines-5;
		}
		/**
		 * End
		 */
		Phrase sentence3 = new Phrase(conRenw.getDescription(), font10);
		refPara.add(sentence3);
		
		PdfPCell refParaCell = new PdfPCell();
		refParaCell.addElement(refPara);
		refParaCell.setBorder(0);

		PdfPTable refParaTable = new PdfPTable(1);
		refParaTable.setWidthPercentage(100);
		refParaTable.addCell(refParaCell);

		try {
			document.add(refParaTable);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		// }
		// }
		// /////////////////////////////////////////////////////////////////////////////////////////
		PdfPTable RenwalChragesDetailsTable = new PdfPTable(4);
		RenwalChragesDetailsTable.setWidthPercentage(100);

		try {
			RenwalChragesDetailsTable.setWidths(new float[] { 28, 16, 28, 28 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		//
		Phrase treatment = new Phrase(" Treatment ", font10);
		Paragraph treatmentPara = new Paragraph();
		treatmentPara.add(treatment);
		treatmentPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell treatmentCell = new PdfPCell();
		treatmentCell.addElement(treatmentPara);
		RenwalChragesDetailsTable.addCell(treatmentCell);

		Phrase frequency = new Phrase(" Frequency", font10);
		Paragraph frequencyPara = new Paragraph();
		frequencyPara.add(frequency);
		frequencyPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell frequencytCell = new PdfPCell();
		frequencytCell.addElement(frequencyPara);
		RenwalChragesDetailsTable.addCell(frequencytCell);

		Phrase prevChrages = new Phrase(" Previous Charges [INR]", font10);
		Paragraph prevChragesPara = new Paragraph();
		prevChragesPara.add(prevChrages);
		prevChragesPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell prevChragesCell = new PdfPCell();
		prevChragesCell.addElement(prevChragesPara);
		RenwalChragesDetailsTable.addCell(prevChragesCell);

		Phrase renewCharges = new Phrase(" Renewal Charges [INR]", font10);
		Paragraph renewChargesPara = new Paragraph();
		renewChargesPara.add(renewCharges);
		renewChargesPara.setAlignment(Element.ALIGN_CENTER);
		PdfPCell renewChargesCell = new PdfPCell();
		renewChargesCell.addElement(renewChargesPara);
		RenwalChragesDetailsTable.addCell(renewChargesCell);

		// //////////////

		// for(int j=0;j<conRenw.getItems().size();j++){
		for (int i = 0; i < conRenw.getItems().size(); i++) {
			
			/**
			 * Date : 19-12-2017 By ANIL
			 */
			if(noOfLines<=0){
				annexureFlag=true;
				productSrNo=i;
				break;
			}
			int length=conRenw.getItems().get(i).getProductName().length();
			if(length>24){
				noOfLines=noOfLines-1;
			}
			noOfLines=noOfLines-1;
			/**
			 * End
			 */
			PdfPCell treatmentValCell = new PdfPCell();
			Paragraph treatmentValPara = new Paragraph();

			Phrase treatmentVal = new Phrase(" ", font10);
			/**
			 * Date : 22-12-2017 By ANIL 
			 * remark should not be print along with treatment name
			 */
//			if (conRenw.getItems().get(i).getRemark() != null) {
//				System.out.println("rmrk not null" + i);
//				treatmentVal = new Phrase(conRenw.getItems().get(i).getProductName()+ " " + conRenw.getItems().get(i).getRemark(), font10);
//				treatmentValPara.add(treatmentVal);
//				treatmentValPara.add(Chunk.NEWLINE);
//				treatmentValCell.addElement(treatmentValPara);
//			} else if (conRenw.getItems().get(i).getRemark() == null) {
				System.out.println("rmrk null" + i);
				treatmentVal = new Phrase(conRenw.getItems().get(i).getProductName(), font10);
				treatmentValPara.add(treatmentVal);
				treatmentValPara.add(Chunk.NEWLINE);
				treatmentValCell.addElement(treatmentValPara);
//			}
			treatmentValPara.setAlignment(Element.ALIGN_LEFT);
			RenwalChragesDetailsTable.addCell(treatmentValCell);
//			

			PdfPCell frequencyValCell = new PdfPCell();
			Paragraph frequencyValPara = new Paragraph();
			String ferqVal = " ";
			if(con.getItems().get(i).getProfrequency()!=null){
				ferqVal=con.getItems().get(i).getProfrequency();
			}else{
				ferqVal="";
			}
			Phrase frequencyVal = new Phrase(ferqVal, font10);
			frequencyValPara.add(frequencyVal);
			frequencyValPara.add(Chunk.NEWLINE);
			frequencyValPara.setAlignment(Element.ALIGN_CENTER);
			frequencyValCell.addElement(frequencyValPara);
			RenwalChragesDetailsTable.addCell(frequencyValCell);

			Phrase oldprice = new Phrase(df.format(conRenw.getItems().get(i).getOldProductPrice())+ " ", font9);
			PdfPCell oldpricecell = new PdfPCell(oldprice);
			oldpricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			RenwalChragesDetailsTable.addCell(oldpricecell);

			double newprice;
			newprice = conRenw.getItems().get(i).getPrice();
			Phrase newchargeval = new Phrase(df.format(newprice) + " ", font9);
			PdfPCell newchargevaluecell = new PdfPCell(newchargeval);
			newchargevaluecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			RenwalChragesDetailsTable.addCell(newchargevaluecell);

		}
		
		/**
		 * Date:19-12-2017 BY ANIL
		 * Modifying table structure
		 */
		PdfPTable pretaxcalculation = new PdfPTable(1);
//		PdfPTable pretaxcalculation = new PdfPTable(3);
		pretaxcalculation.setWidthPercentage(100);

//		try {
//			pretaxcalculation.setWidths(new float[] {34, 20, 30});
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}

		for (int i = 0; i < con.getProductTaxes().size(); i++) {

			System.out.println(con.getProductTaxes());

//			Phrase plus_ph = new Phrase("", font9);
//			PdfPCell plus_cell = new PdfPCell(plus_ph);
//			plus_cell.setBorder(0);
//
//			Phrase charge_phrase = new Phrase(con.getProductTaxes().get(i).getChargeName()+ "@", font9);
//			PdfPCell charge_cell = new PdfPCell(charge_phrase);
//			charge_cell.setBorder(0);
//			
//
//			Phrase per_phrase = new Phrase(con.getProductTaxes().get(i).getChargePercent()+ "% ", font9);
//			PdfPCell per_vell = new PdfPCell(per_phrase);
//            per_vell.setBorder(0);
//            
//			Phrase taxamtph = new Phrase(" \u20B9 "+ df.format(con.getProductTaxes().get(i).getChargePayable())+ " ",font9);
//			PdfPCell taxcell = new PdfPCell(taxamtph);
//			taxcell.setBorder(0);
			
			/**
			 * Date : 19-12-2017 by ANIL
			 **/
			String taxDet=con.getProductTaxes().get(i).getChargeName()+"@"+con.getProductTaxes().get(i).getChargePercent()+ "% "
							+" \u20B9 "+ df.format(con.getProductTaxes().get(i).getChargePayable());
			Phrase taxDetPh = new Phrase(taxDet, font9);
			PdfPCell taxDetCell = new PdfPCell(taxDetPh);
			taxDetCell.setBorder(0);
			taxDetCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

//			pretaxcalculation.addCell(charge_cell);
//			pretaxcalculation.addCell(per_vell);
//			pretaxcalculation.addCell(taxcell);
			pretaxcalculation.addCell(taxDetCell);

		}

	

		/**
		 * Date : 19-12-2017 BY ANIL
		 * Below is the method for calculating taxes on item
		 */
		
		ArrayList<ProductOtherCharges> taxList=new ArrayList<ProductOtherCharges>();
		for(SalesLineItem item:conRenw.getItems()){
			double productNewPrice=0,taxPrice=0;
			taxPrice=removeAllTaxes(item.getPrduct());
			productNewPrice=(item.getPrice()-taxPrice);
			
			if(item.getVatTax().getPercentage()!=0){
				if(item.getVatTax().getTaxPrintName()!=null && !item.getVatTax().getTaxPrintName().equals(""))
				{
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(item.getVatTax().getTaxPrintName());
					pocentity.setChargePercent(item.getVatTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(item.getVatTax().getPercentage(),item.getVatTax().getTaxPrintName(),taxList);
					if(indexValue!=-1){
						pocentity.setAssessableAmount(productNewPrice+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(productNewPrice);
					}
					taxList.add(pocentity);
				}
				else{
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("VAT");
					pocentity.setChargePercent(item.getVatTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(item.getVatTax().getPercentage(),"VAT",taxList);
					
					if(indexValue!=-1){
						pocentity.setAssessableAmount(productNewPrice+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(productNewPrice);
					}
					taxList.add(pocentity);
				}
			}
			
			if(item.getServiceTax().getPercentage()!=0){
				if(item.getServiceTax().getTaxPrintName()!=null && !item.getServiceTax().getTaxPrintName().equals("")){
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName(item.getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(item.getServiceTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(item.getServiceTax().getPercentage(),item.getServiceTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						pocentity.setAssessableAmount(productNewPrice+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(productNewPrice);
					}
					taxList.add(pocentity);
				}
				else{
					ProductOtherCharges pocentity=new ProductOtherCharges();
					pocentity.setChargeName("Service");
					pocentity.setChargePercent(item.getServiceTax().getPercentage());
//					pocentity.setIndexCheck(i+1);
					int indexValue=checkTaxPercent(item.getServiceTax().getPercentage(),"Service",taxList);
					if(indexValue!=-1){
						double assessValue=0;
						if(item.getVatTax().getPercentage()!=0){
							assessValue=productNewPrice+(productNewPrice*item.getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessValue+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
						if(item.getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(productNewPrice+taxList.get(indexValue).getAssessableAmount());
							taxList.remove(indexValue);
						}
					}
					if(indexValue==-1){
						double assessVal=0;
						if(item.getVatTax().getPercentage()!=0){
							assessVal=productNewPrice+(productNewPrice*item.getVatTax().getPercentage()/100);
							pocentity.setAssessableAmount(assessVal);
						}
						if(item.getVatTax().getPercentage()==0){
							pocentity.setAssessableAmount(productNewPrice);
						}
					}
					taxList.add(pocentity);
				}
			}
			
		}
		
		PdfPTable gsttable = new PdfPTable(1);
		gsttable.setWidthPercentage(100);
		for (int i = 0; i <taxList.size(); i++) {
			
			String taxDet=taxList.get(i).getChargeName()+"@"+taxList.get(i).getChargePercent()+ "% "
							+" \u20B9 "+ df.format(taxList.get(i).getAssessableAmount()*taxList.get(i).getChargePercent()/100);
			Phrase taxDetPh = new Phrase(taxDet, font9);
			PdfPCell taxDetCell = new PdfPCell(taxDetPh);
			taxDetCell.setBorder(0);
			taxDetCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			gsttable.addCell(taxDetCell);

		}
		Phrase blnk = new Phrase(" ");
		RenwalChragesDetailsTable.addCell(blnk);
		RenwalChragesDetailsTable.addCell(blnk);
		RenwalChragesDetailsTable.addCell(pretaxcalculation);
		RenwalChragesDetailsTable.addCell(gsttable);
		
		
		PdfPTable nettable=new PdfPTable(2);
		nettable.setWidthPercentage(100);

		try {
			nettable.setWidths(new float[] {50,50});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
//		 Phrase gstphrase3 = new Phrase("Net Amount"+"                    " +  conRenw.getNetPayable(), font9);
		 Phrase gstphrase3 = new Phrase("Net Amount", font9);
		 PdfPCell gstphrase3cell=new PdfPCell(gstphrase3);
		 gstphrase3cell.setBorder(0);
		 
		 Phrase gstphrase3val=new Phrase(" \u20B9 "+commadf.format(conRenw.getNetPayable())+" ",font9);
		 PdfPCell gstphrase3cellval=new PdfPCell(gstphrase3val);
		 gstphrase3cellval.setBorder(0);
		 gstphrase3cellval.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 
		 nettable.addCell(gstphrase3cell);
		 nettable.addCell(gstphrase3cellval);
		 
		Double d = new Double(con.getNetpayable());
		int netamount = d.intValue();

		PdfPTable oldnetamt = new PdfPTable(2);
		oldnetamt.setWidthPercentage(100);

		try {
			oldnetamt.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase oldnetph = new Phrase("Net Amount", font9);
		PdfPCell oldnetphcell = new PdfPCell(oldnetph);
		oldnetphcell.setBorder(0);

		Phrase oldnetphval = new Phrase(" \u20B9 "+commadf.format(netamount) + "", font9);
		PdfPCell oldnetphcellval = new PdfPCell(oldnetphval);
		oldnetphcellval.setBorder(0);
		oldnetphcellval.setHorizontalAlignment(Element.ALIGN_RIGHT);

		/**
		 * Date : 15-12-2017 BY ANIl
		 */
		// oldnetamt.addCell(oldnetph);
		// oldnetamt.addCell(oldnetphval);

		oldnetamt.addCell(oldnetphcell);
		oldnetamt.addCell(oldnetphcellval);
		/**
		 * End
		 */


		/**
		 * Date : 19-12-2017 BY ANIL
		 */
		RenwalChragesDetailsTable.addCell(blnk);
		RenwalChragesDetailsTable.addCell(blnk);
		RenwalChragesDetailsTable.addCell(oldnetamt);
		RenwalChragesDetailsTable.addCell(nettable);

		Phrase annexPh = new Phrase("Refer Annexure for more details.", font8);
		PdfPCell annexCell = new PdfPCell();
		annexCell.addElement(annexPh);
		annexCell.setBorder(0);
		annexCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPTable annexTbl=new PdfPTable(1);
		annexTbl.setWidthPercentage(100f);
		annexTbl.addCell(annexCell);
		
		

		try {
			document.add(RenwalChragesDetailsTable);
			if(annexureFlag){
				document.add(annexTbl);
			}
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		// /////////////////////////////////////// end here
		Phrase Noteline1 = new Phrase("Kindly note,as per the notification of the Government,the Goods & services Tax(GST) @18%,has been enforced from 1st July 2017",font10bold);
		Phrase paraline1 = new Phrase("Other terms and conditions will be remaining the same. We request your kind consideration and acceptance of the above", font10);
		Phrase paraline2 = new Phrase("Look forward to your confirmation at the earliest, in order for us to process and forward the contract documents to you. ", font10);
		Phrase paraline3 = new Phrase("Thanking you, ", font10);
		Phrase paraline4 = new Phrase("Yours faithfully, ", font10);
		Phrase paraline5 = new Phrase("" + comp.getBusinessUnitName(),font9bold);
		Phrase paraline6 = new Phrase("-----------------------------", font10);
		Phrase paraline7 = new Phrase("Authorised Signatory ", font10);
		// Phrase paraline8 = new Phrase ("Ss/-",font9);

		Paragraph notePara = new Paragraph();
		notePara.add(Noteline1);
		notePara.add(Chunk.NEWLINE);
		PdfPCell noteCell = new PdfPCell();
		noteCell.addElement(notePara);
		noteCell.setBorder(0);
		PdfPTable noteTable = new PdfPTable(1);
		noteTable.setWidthPercentage(100);
		noteTable.addCell(noteCell);

		try {
			document.add(noteTable);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Paragraph linePara = new Paragraph();
		linePara.setAlignment(Element.ALIGN_JUSTIFIED);
		linePara.add(paraline1);
		linePara.add(Chunk.NEWLINE);
		linePara.add(paraline2);
		linePara.add(Chunk.NEWLINE);
		linePara.add(paraline3);
		linePara.add(Chunk.NEWLINE);
		linePara.add(Chunk.NEWLINE);
		linePara.add(paraline4);
		linePara.add(Chunk.NEWLINE);
		linePara.add(paraline5);
		linePara.add(Chunk.NEWLINE);
		linePara.add(Chunk.NEWLINE);
		linePara.add(Chunk.NEWLINE);
		linePara.add(paraline6);
		linePara.add(Chunk.NEWLINE);
		linePara.add(paraline7);
		linePara.add(Chunk.NEWLINE);

		PdfPCell lineParaCell = new PdfPCell();
		lineParaCell.addElement(linePara);
		lineParaCell.setBorder(0);

		PdfPTable lineParaTable = new PdfPTable(1);
		lineParaTable.setWidthPercentage(100);
		lineParaTable.addCell(lineParaCell);

		try {
			document.add(lineParaTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	

	private double getTaxAmount(List<SalesLineItem> list) {
		// TODO Auto-generated method stub
		return 0;
	}

	private void createHeader() {

		String addressline1 = "";

		/**
		 * Pincode No with space added On Date : 24/06/2017
		 */

		String pinNo = "" + comp.getAddress().getPin();
		System.out.println("pinNo " + pinNo);
		String pinNostr = pinNo.substring(0, 3) + " " + pinNo.substring(3, 6);
		System.out.println("Substring " + pinNostr);

		/**
		 * End Here
		 */

		if (comp.getAddress().getAddrLine2() != null) {
			addressline1 = comp.getAddress().getAddrLine1() + ", "
					+ comp.getAddress().getAddrLine2() + ", ";
		} else {
			addressline1 = comp.getAddress().getAddrLine1() + ", ";
		}

		String locality = null;
		if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition1");
			locality = (comp.getAddress().getLandmark() + ", "
					+ comp.getAddress().getLocality() + ", "
					+ comp.getAddress().getCity() + " - " + pinNostr + ". "+"\n"
					+ comp.getAddress().getState() + ", " + comp.getAddress()
					.getCountry());
		} else if ((!comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 2");
			locality = (comp.getAddress().getLandmark() + ", "
					+ comp.getAddress().getCity() + " - " + pinNostr + ". "+"\n"
					+ comp.getAddress().getState() + ", "
					+ comp.getAddress().getCountry() + ".");
		}

		else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition 3");
			locality = (comp.getAddress().getLocality() + ", "
					+ comp.getAddress().getCity() + " - " + pinNostr + ". "+"\n"
					+ comp.getAddress().getState() + ", "
					+ comp.getAddress().getCountry() + ". ");
		} else if ((comp.getAddress().getLandmark().equals(""))
				&& (comp.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 4");
			locality = (comp.getAddress().getCity() + " - " + pinNostr + ". "+"\n"
					+ comp.getAddress().getState() + ", "
					+ comp.getAddress().getCountry() + ".");
		}

		// String contactinfo = "" ;
		// System.out.println("landline no "+comp.getLandline());
		// System.out.println("Cell no1 "+comp.getCellNumber1());
		// System.out.println("Cell no2 "+comp.getCellNumber2());
		// System.out.println("fax no "+comp.getFaxNumber());

		// if (comp.getLandline()!=0 && comp.getCellNumber2()!= 0 &&
		// comp.getFaxNumber()!=null)
		// {
		// contactinfo = ("Tel. : " + comp.getCellNumber1() +" / "+
		// comp.getCellNumber2() +" / "+ comp.getLandline() + "  Fax : " +
		// comp.getFaxNumber());
		// }
		// else if (comp.getLandline() == 0 && comp.getCellNumber2() != 0 &&
		// comp.getFaxNumber()!=null )
		// {
		// contactinfo = ("Tel. : " + comp.getCellNumber1() +" / "+
		// comp.getCellNumber2()
		// + "  Fax : " + comp.getFaxNumber());
		// }
		// else if(comp.getLandline() != 0 && comp.getCellNumber2() == 0 &&
		// comp.getFaxNumber()!=null ){
		// contactinfo = ("Tel. : " + comp.getCellNumber1() +" / "+
		// comp.getLandline()
		// + "  Fax : " + comp.getFaxNumber());
		// }
		// else if(comp.getLandline() != 0 && comp.getCellNumber2() != 0 &&
		// comp.getFaxNumber()==null)
		// {
		// contactinfo = ("Tel. : " + comp.getCellNumber1() +" / "+
		// comp.getCellNumber2()+" / "+ comp.getLandline() + "  Fax : " );
		// }
		// else if(comp.getLandline() != 0 && comp.getCellNumber2() == 0 &&
		// comp.getFaxNumber()==null )
		// {
		// contactinfo = ("Tel. : " + comp.getCellNumber1() +" / "+
		// comp.getLandline()
		// + "  Fax : " );
		// }
		// else if(comp.getLandline() == 0 && comp.getCellNumber2() != 0 &&
		// comp.getFaxNumber()==null )
		// {
		// contactinfo = ("Tel. : " + comp.getCellNumber1() +" / "+
		// comp.getCellNumber2()
		// + "  Fax : " );
		// }
		// else if(comp.getLandline() == 0 && comp.getCellNumber2() == 0 &&
		// comp.getFaxNumber()==null)
		// {
		// contactinfo = ("Tel. : " + comp.getCellNumber1() + "  Fax : " );
		// }

		Phrase mycomHeader = new Phrase(comp.getBusinessUnitName()
				.toUpperCase(), font16);
		Phrase officeLbl = new Phrase("Regd. Office: ", font9bold);
		Phrase header1 = new Phrase(addressline1 + locality+"Tel.: +(91-22) 22625376 / 23514380 / 912222661091 Fax : +(91-22) 2266 0810 ", font10);
		// Added this by Ajinkya on Date 20/05/2017
//		Phrase contactInfo = new Phrase(
//				"Tel.: +(91-22) 22625376 / 23514380 / 912222661091 Fax : +(91-22) 2266 0810 ",
//				font10);
		Phrase header3 = new Phrase("E-Mail : " + comp.getEmail().trim()
				+ "  Website : " + comp.getWebsite(), font10);

		Paragraph mycomPara = new Paragraph();

		mycomPara.add(mycomHeader);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(officeLbl);
		mycomPara.add(header1);
		mycomPara.add(Chunk.NEWLINE);
//		mycomPara.add(contactInfo);
//		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(header3);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.add(Chunk.NEWLINE);
		mycomPara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell mycomCell = new PdfPCell();
		mycomCell.addElement(mycomPara);
		mycomCell.setBorder(0);

		PdfPTable parentTbl = new PdfPTable(1);
		parentTbl.setWidthPercentage(100);

		parentTbl.addCell(mycomCell);

		try {
			document.add(parentTbl);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createToPara() {
		Phrase renewalDt = new Phrase("" + fmt.format(conRenw.getDate()),font10);
		PdfPCell renewalDtCell = new PdfPCell(renewalDt);
		renewalDtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		renewalDtCell.setBorder(0);

		String customerFullName = "";

		if (cust.isCompany()) {
			customerFullName = cust.getCompanyName();
		} else {
			if (cust.getSalutation() != null
					&& !cust.getSalutation().equals("")) {
				customerFullName = cust.getSalutation() + " "
						+ cust.getFullname();
			} else {
				customerFullName = cust.getFullname();
			}

		}

		// ///////////////////////////////////

		/**
		 * nameInFirstLetterUpperCase defined by Ajinkya suggested by rahul Used
		 * to take customer name from Uppercase to Only first letter Uppercase
		 */
		String nameInFirstLetterUpperCase = getFirstLetterUpperCase(customerFullName
				.trim());
		Phrase custInfo = new Phrase(nameInFirstLetterUpperCase, font10);
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);

		// ///////////////////////////////////
		// ///////////// Added by Ajinkya
		/**
		 * Date :15-12-20-17 BY ANIL
		 */
		String pinNostr="";
		if(cust.getAdress().getPin()!=0){
			String pinNo = "" + cust.getAdress().getPin();
	
			// System.out.println("pinNo "+pinNo);
			pinNostr = pinNo.substring(0, 3) + " " + pinNo.substring(3, 6);
			System.out.println("Substring " + pinNostr);
		}
		// end here

		String addressline = "";

		

		if (ser.getAddress() != null && ser.getAddress().getAddrLine2() != null
				&& !ser.getAddress().getAddrLine2().equals("")) {
			addressline = ser.getAddress().getAddrLine1() + ", "
					+ ser.getAddress().getAddrLine2() + ", ";
		} else {
			addressline = ser.getAddress().getAddrLine1() + ", ";
		}

		String localityline1 = "";
		String localityline2 = "";
		String localityline3 = "";

		if ((ser.getAddress().getLandmark() != null && !ser.getAddress()
				.getLandmark().equals(""))
				&& (ser.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition1");
			localityline1 = ser.getAddress().getLandmark() + ", "
					+ ser.getAddress().getLocality() + ", ";
			localityline2 = ser.getAddress().getCity() + " - " + pinNostr
					+ ". ";
			localityline3 = ser.getAddress().getState() + ", "
					+ ser.getAddress().getCountry() + ". ";
		} else if ((!ser.getAddress().getLandmark().equals("") && (ser
				.getAddress().getLandmark() != null))
				&& (ser.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 2");
			localityline1 = ser.getAddress().getLandmark() + ", ";
			localityline2 = ser.getAddress().getCity() + " - " + pinNostr
					+ ". ";
			localityline3 = ser.getAddress().getState() + ", "
					+ ser.getAddress().getCountry() + ". ";
		}

		else if ((ser.getAddress().getLandmark() == null)
				&& (ser.getAddress().getLandmark().equals(""))
				&& (ser.getAddress().getLocality().equals("") == false)) {
			System.out.println("inside both null condition 3");
			localityline1 = ser.getAddress().getLocality() + ", ";
			localityline2 = ser.getAddress().getCity() + " - " + pinNostr
					+ ". ";
			localityline3 = ser.getAddress().getState() + ", "
					+ ser.getAddress().getCountry() + ". ";
		} else if ((ser.getAddress().getLandmark() == null)
				&& (ser.getAddress().getLandmark().equals(""))
				&& (ser.getAddress().getLocality().equals("") == true)) {
			System.out.println("inside both null condition 4");
			localityline1 = ser.getAddress().getCity() + " - " + pinNostr
					+ ". ";
			localityline2 = ser.getAddress().getState() + ", "
					+ ser.getAddress().getCountry() + ". ";
			localityline3 = " ";
		}

		// ///////////////////////////////////

		Phrase custAddInfo1 = new Phrase(addressline, font10);
		Phrase custAddInfo2 = new Phrase(localityline1, font10);
		Phrase custAddInfo3 = new Phrase(localityline2, font10);
		Phrase custAddInfo4 = new Phrase(localityline3, font10);

		// Paragraph adrsPara = new Paragraph ();
		// adrsPara.add(custAddInfo1);
		// adrsPara.add(Chunk.NEWLINE);
		// // adrsPara.add(Chunk.NEWLINE);
		// adrsPara.add(custAddInfo2);
		// adrsPara.add(Chunk.NEWLINE);
		// // adrsPara.add(Chunk.NEWLINE);
		// adrsPara.add(custAddInfo3);
		// adrsPara.add(Chunk.NEWLINE);
		// // adrsPara.add(Chunk.NEWLINE);
		// adrsPara.add(custAddInfo4);
		// adrsPara.setAlignment(Element.ALIGN_LEFT);

		PdfPCell custAddInfo1Cell = new PdfPCell(custAddInfo1);
		PdfPCell custAddInfo2Cell = new PdfPCell(custAddInfo2);
		PdfPCell custAddInfo3Cell = new PdfPCell(custAddInfo3);
		PdfPCell custAddInfo4Cell = new PdfPCell(custAddInfo4);

		custAddInfo1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfo2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfo3Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfo4Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		custAddInfo1Cell.setBorder(0);
		custAddInfo2Cell.setBorder(0);
		custAddInfo3Cell.setBorder(0);
		custAddInfo4Cell.setBorder(0);

		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(renewalDtCell);
		parentTable.addCell(blankCell);
		parentTable.addCell(custInfoCell);
		parentTable.addCell(custAddInfo1Cell);
		parentTable.addCell(custAddInfo2Cell);
		parentTable.addCell(custAddInfo3Cell);
		parentTable.addCell(custAddInfo4Cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	// Ajinkya added this code to convert customer name in CamelCase
	// Date : 12/4/2017

	private String getFirstLetterUpperCase(String customerFullName) {

		String customerName = "";
		String[] customerNameSpaceSpilt = customerFullName.split(" ");
		int count = 0;
		for (String name : customerNameSpaceSpilt) {
			String nameLowerCase = name.toLowerCase();
			if (count == 0) {
				customerName = customerName
						+ name.toUpperCase().substring(0, 1)
						+ nameLowerCase.substring(1);
			} else {
				customerName = customerName + " "
						+ name.toUpperCase().substring(0, 1)
						+ nameLowerCase.substring(1);
			}
			count = count + 1;
		}
		return customerName;

	}
	
	
	public double removeAllTaxes(SuperProduct entity){
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct){
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct){
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			if(entity instanceof ItemProduct){
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals("")){
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}else{
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			if(entity instanceof ServiceProduct){
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals("")){
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}else{
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	}
	
	public int checkTaxPercent(double taxValue,String taxName,ArrayList<ProductOtherCharges> taxesList){
		for(int i=0;i<taxesList.size();i++){
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
			}
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
			
		}
		return -1;
	}
	
	

	// private void createRenewalLetterPara() {
	// String custAdd1="";
	// String custFullAdd1="";
	//
	// if(ser.getAddress()!=null){
	//
	// if(ser.getAddress().getAddrLine2().equals(null)&&
	// ser.getAddress().getAddrLine2().equals("")){
	//
	// custAdd1=ser.getAddress().getAddrLine1().toUpperCase()+" , "+ser.getAddress().getAddrLine2().toUpperCase()+" , ";
	// }
	// else
	// {
	// custAdd1=ser.getAddress().getAddrLine1().toUpperCase()+" , ";
	// }
	//
	// if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false))
	// {
	// System.out.println("inside both null condition1");
	// custFullAdd1 =
	// (ser.getAddress().getLandmark().toUpperCase()+" , "+ser.getAddress().getLocality().toUpperCase()+" , "
	// +ser.getAddress().getCity().toUpperCase()+" , "
	// +ser.getAddress().getPin()+" , "+ser.getAddress().getState().toUpperCase()+" , "+ser.getAddress().getCountry().toUpperCase()+" . ");
	// }
	// else
	// if((!ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
	// System.out.println("inside both null condition 2");
	// custFullAdd1=
	// (ser.getAddress().getLandmark().toUpperCase()+" , "+ser.getAddress().getCity().toUpperCase()+" , "
	// +ser.getAddress().getPin()+" , "+ser.getAddress().getState().toUpperCase()+" , "+ser.getAddress().getCountry().toUpperCase()+" . ");
	// }
	// else
	// if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==false)){
	// System.out.println("inside both null condition 3");
	// custFullAdd1=
	// (ser.getAddress().getLocality().toUpperCase()+" , "+ser.getAddress().getCity().toUpperCase()+" , "
	// +ser.getAddress().getPin()+" , "+ser.getAddress().getState().toUpperCase()+" , "+ser.getAddress().getCountry().toUpperCase()+" . ");
	// }
	// else
	// if((ser.getAddress().getLandmark().equals(""))&&(ser.getAddress().getLocality().equals("")==true)){
	// System.out.println("inside both null condition 4");
	// custFullAdd1=(ser.getAddress().getCity().toUpperCase()+" , "
	// +ser.getAddress().getPin()+" , "+ser.getAddress().getState().toUpperCase()+" , "+ser.getAddress().getCountry().toUpperCase()+" . ");
	// }
	//
	//
	// ////////////////////// calc days to month //////////////////////////
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// ////////////////////// calc days to month //////////////////////
	//
	// logger.log(Level.SEVERE,"month"+diffMonth);
	// String months = diffMonth+" months";
	//
	// Phrase Sub1 = new Phrase
	// ("Ref:RENEWAL OF ANNUAL PEST MANAGEMENT SOLUTION ORDER OF YOUR OFFICE PREMISES OF "+"\n",font9ul);
	// Phrase Sub2 = new Phrase (custAdd1 +custFullAdd1,font9ul);
	// Phrase Sub3 = new Phrase
	// (" FOR THE PERIOD FROM "+fmt.format(con.getStartDate())+" To "+fmt.format(con.getEndDate())+". ",font9ul);
	//
	//
	// // rohan added this code as per salutation in customer here we will add
	// dear sir / madam
	// // Date : 13/2/2017
	// String salutation = "";
	// if(cust.getSalutation()!= null && !cust.getSalutation().equals(""))
	// {
	// if(cust.getSalutation().equalsIgnoreCase("Mr."))
	// {
	// salutation = "Dear Sir ,";
	// }
	// else if(cust.getSalutation().equalsIgnoreCase("Ms."))
	// {
	// salutation = "Dear Madam ,";
	// }
	// else
	// {
	// salutation ="Dear Sir / Madam ,";
	// }
	// }
	// else
	// {
	// salutation ="Dear Sir / Madam ,";
	// }
	//
	// Phrase Start = new Phrase (salutation,font9);
	//
	// Phrase sentence1 = new Phrase
	// ("This has reference  to the above subject matter, we wish to confirm as under.",font9);
	// Phrase sentence2 = new Phrase
	// ("Our previous order of comprehensive Pest management solution for your above mentioned "
	// + "premises is expired from "+ fmt.format(con.getEndDate()) +
	// " . ",font9);
	// Phrase sentence3 = new Phrase
	// ("Owing to the rising costs of delivering services, particularly high cost of manpower and Insecticides costs, we are compelled to",font9);
	// Phrase sentence4 = new Phrase
	// (" marginally increase our annual charges to compensate for rising cost. ",font9);
	// Phrase sentence5 = new Phrase
	// ("We would like to continue our services for further "+months+" from "+fmt.format(con.getStartDate())+" To "+fmt.format(con.getEndDate())+" , as per the details given below :",font9);
	//
	// boolean priceHikeFlag=false;
	//
	// for(int i=0;i<conRenw.getItems().size();i++){
	// if(conRenw.getItems().get(i).getPrice()>conRenw.getItems().get(i).getOldProductPrice())
	// {
	// priceHikeFlag=true;
	// }
	// }
	//
	// Paragraph refPara = new Paragraph();
	// refPara.add(Sub1);
	// refPara.add(Sub2);
	// refPara.add(Sub3);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Start);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(sentence1);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(sentence2);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Chunk.NEWLINE);
	//
	// if(priceHikeFlag==true){
	//
	// refPara.add(sentence3);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(sentence4);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Chunk.NEWLINE);
	//
	// }
	//
	// refPara.add(sentence5);
	// refPara.add(Chunk.NEWLINE);
	// refPara.add(Chunk.NEWLINE);
	//
	//
	// PdfPCell refParaCell = new PdfPCell ();
	// refParaCell.addElement(refPara);
	// refParaCell.setBorder(0);
	//
	// PdfPTable refParaTable = new PdfPTable (1);
	// refParaTable.setWidthPercentage(100);
	// refParaTable.addCell(refParaCell);
	//
	//
	// try {
	// document.add(refParaTable);
	// } catch (DocumentException e1) {
	// e1.printStackTrace();
	// }
	// }
	//
	// PdfPTable RenwalChragesDetailsTable = new PdfPTable (4);
	// RenwalChragesDetailsTable.setWidthPercentage(100);
	//
	// try {
	// RenwalChragesDetailsTable.setWidths(new float []{20,20,30,30});
	// }
	// catch (DocumentException e1)
	// {
	// e1.printStackTrace();
	// }
	//
	// Phrase treatment = new Phrase("Treatment ",font10);
	// Paragraph treatmentPara = new Paragraph();
	// treatmentPara.add(treatment);
	// treatmentPara.setAlignment(Element.ALIGN_CENTER);
	// PdfPCell treatmentCell = new PdfPCell();
	// treatmentCell.addElement(treatmentPara);
	// RenwalChragesDetailsTable.addCell(treatmentCell);
	//
	// Phrase frequency = new Phrase("Frequency",font10);
	// Paragraph frequencyPara = new Paragraph();
	// frequencyPara.add(frequency);
	// frequencyPara.setAlignment(Element.ALIGN_CENTER);
	// PdfPCell frequencytCell = new PdfPCell();
	// frequencytCell.addElement(frequencyPara);
	// RenwalChragesDetailsTable.addCell(frequencytCell);
	//
	// Phrase prevChrages = new Phrase(" Previous Charges ",font10);
	// Paragraph prevChragesPara = new Paragraph();
	// prevChragesPara.add(prevChrages);
	// prevChragesPara.setAlignment(Element.ALIGN_CENTER);
	// PdfPCell prevChragesCell = new PdfPCell();
	// prevChragesCell.addElement(prevChragesPara);
	// RenwalChragesDetailsTable.addCell(prevChragesCell);
	//
	// Phrase renewCharges = new Phrase(" Renewal Charges ",font10);
	// Paragraph renewChargesPara = new Paragraph();
	// renewChargesPara.add(renewCharges);
	// renewChargesPara.setAlignment(Element.ALIGN_CENTER);
	// PdfPCell renewChargesCell = new PdfPCell();
	// renewChargesCell.addElement(renewChargesPara);
	// RenwalChragesDetailsTable.addCell(renewChargesCell);
	//
	//
	// PdfPCell treatmentValCell = new PdfPCell();
	// Paragraph treatmentValPara = new Paragraph();
	// for(int i=0;i<conRenw.getItems().size();i++){
	//
	// Phrase treatmentVal = new Phrase(" ",font9);
	//
	// if(conRenw.getItems().get(i).getProductName()!= null &&
	// conRenw.getItems().get(i).getRemark()!= null){
	// treatmentVal = new
	// Phrase(conRenw.getItems().get(i).getProductName()+" "+conRenw.getItems().get(i).getRemark(),
	// font10);
	// }
	// else if(conRenw.getItems().get(i).getProductName()!= null &&
	// conRenw.getItems().get(i).getRemark()== null)
	// {
	// treatmentVal = new
	// Phrase(conRenw.getItems().get(i).getProductName()+" "+conRenw.getItems().get(i).getRemark(),
	// font10);
	// }
	// else{
	// treatmentVal = new Phrase("");
	// }
	//
	// treatmentValPara.add(treatmentVal);
	// treatmentValPara.add(Chunk.NEWLINE);
	//
	// treatmentValPara.setAlignment(Element.ALIGN_CENTER);
	//
	// // treatmentValCell.setBorder(0);
	// }
	// treatmentValCell.addElement(treatmentValPara);
	// RenwalChragesDetailsTable.addCell(treatmentValCell);
	//
	// PdfPCell frequencyValCell = new PdfPCell();
	// Paragraph frequencyValPara = new Paragraph();
	// String ferqVal= null ;
	// for(int i=0;i<conRenw.getItems().size();i++){
	//
	// if(conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC")
	// )
	// {
	// ferqVal= " Treatment " ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB00")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT00")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC00")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT00")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY00")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO00")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT00")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA00")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT00")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT00")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT00")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP00")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC00")
	// )
	// {
	// ferqVal= "Assinment" ;
	//
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB365")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC365")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT365")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY365")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO365")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT365")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA365")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT365")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT365")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT365")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP365")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC365")
	// )
	// {
	// ferqVal= " Daily" ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB52")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT52")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC52")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT52")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY52")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO52")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT52")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA52")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT52")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT52")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT52")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP52")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC52")
	// )
	// {
	// ferqVal= " Weeekly " ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB36")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT36")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC36")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT36")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY36")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO36")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT36")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA36")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT36")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT36")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT36")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP36")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC36")
	// )
	// {
	// ferqVal= " Twice the Month " ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB24")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT24")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC24")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT24")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY24")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO24")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT24")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA24")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT24")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT24")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT24")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP24")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC24")
	// )
	// {
	// ferqVal= " Four Nightly " ;
	// }
	//
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB12")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT12")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC12")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT12")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY12")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO12")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT12")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA12")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT12")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT12")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT12")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP12")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC12")
	// )
	// {
	// ferqVal= " Monthly" ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB06")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT06")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC06")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT06")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY06")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO06")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT06")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA06")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT06")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT06")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT06")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP06")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC06")
	// )
	// {
	// ferqVal= " Alternate Month" ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB04")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT04")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC04")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT04")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY04")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO04")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT04")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA04")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT04")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT04")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT04")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP04")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC04")
	// )
	// {
	// ferqVal= " Quarterly " ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB03")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT03")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC03")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT03")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY03")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO03")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT03")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA03")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT03")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT03")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT03")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP03")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC03")
	// )
	// {
	// ferqVal= " Four Monthtly " ;
	// }
	//
	// if(
	// conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WB02")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BT02")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GIPC02")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GT02")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FLY02")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("MO02")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("GFT02")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("WA02")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("DT02")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("FT02")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("BMT02")||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("SP02")
	// ||conRenw.getItems().get(i).getProductCode().equalsIgnoreCase("RC02")
	// )
	// {
	// ferqVal= " Six Monthly " ;
	// }
	// Phrase frequencyVal = new Phrase(" "+ferqVal,font10);
	// frequencyValPara.add(frequencyVal);
	// frequencyValPara.add(Chunk.NEWLINE);
	// frequencyValPara.setAlignment(Element.ALIGN_CENTER);
	//
	// }
	// frequencyValCell.addElement(frequencyValPara);
	//
	// RenwalChragesDetailsTable.addCell(frequencyValCell);
	//
	//
	// /////////////////////////////// code of contract renewal pdf code for tax
	// ////////////////////////
	// Phrase servicetax= null;
	// Phrase servicetax1= null;
	// int flag=0;
	// int vat=0;
	// int st=0;
	// for(int i=0; i<conRenw.getItems().size();i++){
	//
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==0)){
	// servicetax = new Phrase("VAT %",font9);
	// vat=vat+1;
	// System.out.println("phrase value===="+servicetax.toString());
	// }
	// else
	// if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()==0))
	// {
	// servicetax = new Phrase("ST %",font9);
	// st=st+1;
	// }
	// else
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0)){
	// servicetax1 = new Phrase("VAT / ST %",font9);
	// flag=flag+1;
	// System.out.println("flag value;;;;;"+flag);
	// }
	// else{
	//
	// servicetax = new Phrase("TAX %",font9);
	// }
	//
	// }
	//
	// /////////////////////////////// code of contract renewal pdf code for tax
	// end Here ////////////////////////
	//
	// /////////////////////////////// Add tax for Prev value cal
	// ////////////////////////////////////////////////////
	// //
	// PdfPCell prevChragesValCell = new PdfPCell();
	// Paragraph prevChragesValPara = new Paragraph();
	//
	// ////////////// add prev Tax calc /////////
	//
	// for(int i=0;i<conRenw.getItems().size();i++){
	//
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==15))
	// {
	// System.out.println("3 inside st 15");
	//
	// double preVal1 = conRenw.getItems().get(i).getOldProductPrice();
	// double tax1 = 14 * conRenw.getItems().get(i).getOldProductPrice()/100;
	// double tax2 = 0.5 * conRenw.getItems().get(i).getOldProductPrice()/100;
	// double tax3 = 0.5 * conRenw.getItems().get(i).getOldProductPrice()/100;
	// double tax = tax1 + tax2 + tax3 ;
	// double total = tax + preVal1 ;
	//
	// System.out.println("st @15 "+preVal1);
	// System.out.println(tax1);
	// System.out.println(tax2);
	// System.out.println(tax3);
	// System.out.println(tax);
	// System.out.println(total);
	//
	// String str = "service Tax @ 15 %"+ df.format(tax);
	//
	// Phrase prevChragesOLDVal1 = new Phrase(""+preVal1, font10);
	// Phrase words1 = new Phrase(" plus "+ str +" Rs ",font10);
	// Phrase prevChragesOLDVal2 = new Phrase(""+df.format(total),font10);
	// Phrase words2 = new Phrase(" i.e ",font10);
	// Phrase words3 = new Phrase(" p.a ",font10);
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// prevChragesValPara.add(prevChragesOLDVal1);
	// prevChragesValPara.add(words1);
	// prevChragesValPara.add(prevChragesOLDVal2);
	// prevChragesValPara.add(words2);
	// if( diffMonth == 12){
	// prevChragesValPara.add(words3);
	// }
	// prevChragesValPara.add(Chunk.NEWLINE);
	// prevChragesValPara.setAlignment(Element.ALIGN_CENTER);
	// prevChragesValCell.addElement(prevChragesValPara);
	//
	// }
	//
	//
	// else
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==14.5))
	// {
	// System.out.println("inside st 14.5");
	//
	// double preVal1 = conRenw.getItems().get(i).getOldProductPrice();
	// double tax = 14 * conRenw.getItems().get(i).getOldProductPrice()/100;
	// double tax2 = 0.5 * conRenw.getItems().get(i).getOldProductPrice()/100;
	// double total = tax + preVal1 + tax2;
	// double totaltax = tax + tax2 ;
	// String str = "service Tax @ 14.5 % "+ df.format(totaltax);
	//
	// Phrase prevChragesOLDVal1 = new Phrase(""+preVal1, font10);
	// Phrase words1 = new Phrase(" plus "+ str + " Rs ",font10);
	// Phrase prevChragesOLDVal2 = new Phrase(""+df.format(total),font10);
	// Phrase words2 = new Phrase(" i.e ",font10);
	// Phrase words3 = new Phrase(" p.a ",font10);
	//
	// System.out.println("st@14.5"+ preVal1);
	// System.out.println(tax);
	// System.out.println(total);
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// prevChragesValPara.add(prevChragesOLDVal1);
	// prevChragesValPara.add(words1);
	// prevChragesValPara.add(prevChragesOLDVal2);
	// prevChragesValPara.add(words2);
	// if( diffMonth == 12){
	// prevChragesValPara.add(words3);
	// }
	// prevChragesValPara.add(Chunk.NEWLINE);
	// prevChragesValPara.setAlignment(Element.ALIGN_CENTER);
	// prevChragesValCell.addElement(prevChragesValPara);
	//
	// }
	//
	// else
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()!=0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==0))
	// {
	// System.out.println("inside vat");
	//
	// double amount = conRenw.getItems().get(i).getOldProductPrice();
	// double vat1 =
	// conRenw.getItems().get(i).getVatTax().getPercentage()*conRenw.getItems().get(i).getPrice()/100;
	// double toalAmount = amount + vat1 ;
	// String str =
	// " VAT "+conRenw.getItems().get(i).getVatTax().getPercentage()+" @ % " +
	// df.format(vat1) ;
	//
	//
	// Phrase prevChragesOLDVal1 = new Phrase(""+ amount, font10);
	// Phrase words1 = new Phrase(" plus "+ str + " Rs ",font10);
	// Phrase prevChragesOLDVal2 = new Phrase(""+df.format(toalAmount),font10);
	// Phrase words2 = new Phrase(" i.e ",font10);
	// Phrase words3 = new Phrase(" p.a ",font10);
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// prevChragesValPara.add(prevChragesOLDVal1);
	// prevChragesValPara.add(words1);
	// prevChragesValPara.add(prevChragesOLDVal2);
	// prevChragesValPara.add(words2);
	// if( diffMonth == 12){
	// prevChragesValPara.add(words3);
	// }
	// prevChragesValPara.add(Chunk.NEWLINE);
	// prevChragesValPara.setAlignment(Element.ALIGN_CENTER);
	// prevChragesValCell.addElement(prevChragesValPara);
	//
	// }
	// }
	// RenwalChragesDetailsTable.addCell(prevChragesValCell);
	//
	// // ////////////// add prev Tax1 calc end here/////////
	//
	// // prevChragesValPara.setAlignment(Element.ALIGN_CENTER);
	// // prevChragesValCell.addElement(prevChragesValPara);
	// // RenwalChragesDetailsTable.addCell(prevChragesValCell);
	//
	// ////////////////////////////////////////////////////////////////////////////////////////////
	// PdfPCell renewChargesValCell = new PdfPCell();
	// Paragraph renewChargesValPara = new Paragraph();
	//
	// /////////////////////////////// Add tax for renew value cal
	// ////////////////////////////////////////////////////
	//
	// for(int i=0;i<conRenw.getItems().size();i++){
	//
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==15))
	// {
	// System.out.println("3 inside st 15");
	//
	// double renewVal1 = conRenw.getItems().get(i).getPrice();
	// double tax1 = 14 * conRenw.getItems().get(i).getPrice()/100;
	// double tax2 = 0.5 * conRenw.getItems().get(i).getPrice()/100;
	// double tax3 = 0.5 * conRenw.getItems().get(i).getPrice()/100;
	// double tax = tax1 + tax2 + tax3 ;
	// double total = tax + renewVal1 ;
	//
	// System.out.println("st @15 "+renewVal1);
	// System.out.println(tax1);
	// System.out.println(tax2);
	// System.out.println(tax3);
	// System.out.println(tax);
	// System.out.println(total);
	//
	// String str = "service Tax @ 15 %"+ df.format(tax);
	//
	// Phrase renewChragesVal1 = new Phrase(""+renewVal1, font10);
	// Phrase words1 = new Phrase(" plus "+ str +" Rs ",font10);
	// Phrase renewChragesVal2 = new Phrase(""+df.format(total),font10);
	// Phrase words2 = new Phrase(" i.e ",font10);
	// Phrase words3 = new Phrase(" p.a ",font10);
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// renewChargesValPara.add(renewChragesVal1);
	// renewChargesValPara.add(words1);
	// renewChargesValPara.add(renewChragesVal2);
	// renewChargesValPara.add(words2);
	//
	// if( diffMonth == 12){
	// renewChargesValPara.add(words3);
	// }
	// renewChargesValPara.add(Chunk.NEWLINE);
	// renewChargesValPara.setAlignment(Element.ALIGN_LEFT);
	// renewChargesValCell.addElement(renewChargesValPara);
	// }
	// else
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==14.5))
	// {
	// System.out.println("inside st 14.5");
	//
	// double renewVal1 = conRenw.getItems().get(i).getPrice();
	// double tax = 14 * conRenw.getItems().get(i).getPrice()/100;
	// double tax2 = 0.5 * conRenw.getItems().get(i).getPrice()/100;
	// double total = tax + renewVal1 + tax2;
	// double totaltax = tax + tax2 ;
	// String str = "service Tax @ 14.5 % "+ df.format(totaltax);
	//
	// Phrase renewChragesVal1 = new Phrase(""+renewVal1, font10);
	// Phrase words1 = new Phrase(" plus "+ str + " Rs ",font10);
	// Phrase renewChragesVal2 = new Phrase(""+df.format(total),font10);
	// Phrase words2 = new Phrase(" i.e ",font10);
	// Phrase words3 = new Phrase(" p.a ",font10);
	//
	// System.out.println("st@14.5"+ renewVal1);
	// System.out.println(tax);
	// System.out.println(total);
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// renewChargesValPara.add(renewChragesVal1);
	// renewChargesValPara.add(words1);
	// renewChargesValPara.add(renewChragesVal2);
	// renewChargesValPara.add(words2);
	// if( diffMonth == 12){
	// renewChargesValPara.add(words3);
	// }
	// renewChargesValPara.add(Chunk.NEWLINE);
	// renewChargesValPara.setAlignment(Element.ALIGN_LEFT);
	// renewChargesValCell.addElement(renewChargesValPara);
	// }
	//
	// else
	// if((conRenw.getItems().get(i).getVatTax().getPercentage()!=0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()==0))
	// {
	// System.out.println("inside vat");
	//
	// double amount = conRenw.getItems().get(i).getPrice();
	// double vat1 =
	// conRenw.getItems().get(i).getVatTax().getPercentage()*conRenw.getItems().get(i).getPrice()/100;
	// double toalAmount = amount + vat1 ;
	// String str =
	// " VAT "+conRenw.getItems().get(i).getVatTax().getPercentage()+" @ % " +
	// df.format(vat1) ;
	//
	// Phrase renewChragesVal1 = new Phrase(""+ amount, font10);
	// Phrase words1 = new Phrase(" plus "+ str + " Rs ",font10);
	// Phrase renewChragesVal2 = new Phrase(""+df.format(toalAmount),font10);
	// Phrase words2 = new Phrase(" i.e ",font10);
	// Phrase words3 = new Phrase(" p.a ",font10);
	//
	// Date startDate = con.getStartDate();
	// Date endDate = con.getEndDate();
	//
	// Calendar startCalendar = new GregorianCalendar();
	// startCalendar.setTime(startDate);
	// Calendar endCalendar = new GregorianCalendar();
	// endCalendar.setTime(endDate);
	//
	// int diffYear = endCalendar.get(Calendar.YEAR) -
	// startCalendar.get(Calendar.YEAR);
	// int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) -
	// startCalendar.get(Calendar.MONTH);
	//
	// renewChargesValPara.add(renewChragesVal1);
	// renewChargesValPara.add(words1);
	// renewChargesValPara.add(renewChragesVal2);
	// renewChargesValPara.add(words2);
	// if( diffMonth == 12){
	// renewChargesValPara.add(words3);
	// }
	// renewChargesValPara.add(Chunk.NEWLINE);
	// renewChargesValPara.setAlignment(Element.ALIGN_LEFT);
	// renewChargesValCell.addElement(renewChargesValPara);
	// }
	// }
	// //////////////////////////////// calc end Here
	// /////////////////////////////////////////////
	// RenwalChragesDetailsTable.addCell(renewChargesValCell);
	//
	// try {
	// document.add(RenwalChragesDetailsTable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	// ///////////////////////////////////////// end here
	// ////////////////////////////////////////////
	//
	// Phrase Noteline1= new Phrase
	// ("Kindly note Government of India has announced revision of service Tax rate from "
	// +
	// "14.05 % to 15 % from 1st June 2016. (Service Tax @ 14.% , Swachh Bharat Cess @0.5 % "
	// + " & Krishi Kalyan Cess @ 0.5 %).",font9bold);
	//
	// Phrase paraline1 = new Phrase
	// ("Other terms and conditions will be remaining the same. We request your kind consideration and"
	// + " acceptance of the above",font9);
	// Phrase paraline2 = new Phrase
	// ("Look forward to your confirmation at the earliest, in order for us to process and forward the contract "
	// + "documents to you .",font9);
	// Phrase paraline3 = new Phrase ("Thanking you , ",font9);
	// Phrase paraline4 = new Phrase ("Yours faithfully , ",font9);
	// Phrase paraline5 = new Phrase (""+comp.getBusinessUnitName(),font9);
	// Phrase paraline6 = new Phrase ("------------------------------",font9);
	// Phrase paraline7 = new Phrase ("AUTHORISED SIGNATORY",font9);
	// Phrase paraline8 = new Phrase ("Ss/-",font9);
	//
	// Paragraph notePara = new Paragraph();
	// notePara.add(Noteline1);
	// notePara.add(Chunk.NEWLINE);
	// PdfPCell noteCell =new PdfPCell();
	// noteCell.addElement(notePara);
	// noteCell.setBorder(0);
	// PdfPTable noteTable = new PdfPTable(1);
	// noteTable.setWidthPercentage(100);
	// noteTable.addCell(noteCell);
	//
	// try {
	// document.add(noteTable);
	// } catch (DocumentException e) {
	// e.printStackTrace();
	// }
	//
	// Paragraph linePara = new Paragraph();
	// linePara.add(paraline1);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline2);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline3);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline4);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline5);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline6);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline7);
	// linePara.add(Chunk.NEWLINE);
	// linePara.add(paraline8);
	// linePara.add(Chunk.NEWLINE);
	//
	// PdfPCell lineParaCell =new PdfPCell();
	// lineParaCell.addElement(linePara);
	// lineParaCell.setBorder(0);
	//
	// PdfPTable lineParaTable = new PdfPTable(1);
	// lineParaTable.setWidthPercentage(100);
	// lineParaTable.addCell(lineParaCell);
	//
	// try {
	// document.add(lineParaTable);
	// e.printStackTrace();
	// }
	//
	// }

}
