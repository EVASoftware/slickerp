package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class PcambCoveringLetterpdf {
	
	ContractRenewal conRenw;
	Company comp;
	Contract con;
	Customer cust;
	Service ser;
	Invoice invoiceentity;
	ArrayList<ArticleType> articletype; 
	
	//	BillingDocument billEntity;
	String invoiceOrderType="";     

	 Document document;  
  
		private Font font16boldul, font12bold, font8bold, font9bold, font8, font9,
		font12boldul, font10boldul, font12, font16bold, font10, font10bold,
		font14bold,font7,font7bold,font9red,font9boldred,font12boldred,font16,font23,font20;
		
		 SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
		 SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-dd-yyyy");
		
		 DecimalFormat df=new DecimalFormat("0.00");

		 public PcambCoveringLetterpdf() 
		 {
			super();
			font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
			font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
			font16 = new Font(Font.FontFamily.HELVETICA, 16);
			font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
			font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
			font8 = new Font(Font.FontFamily.HELVETICA, 8);
			font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
			font12 = new Font(Font.FontFamily.HELVETICA, 12);
			font12boldred = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,BaseColor.RED);
			font10 = new Font(Font.FontFamily.HELVETICA, 10);
			font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
			font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
			font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
			font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
			font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,BaseColor.RED);
			font9 = new Font(Font.FontFamily.HELVETICA, 9);
			font9red = new Font(Font.FontFamily.HELVETICA, 9,Font.NORMAL, BaseColor.RED);
			font7 = new Font(Font.FontFamily.HELVETICA, 7);
			font7bold = new Font(Font.FontFamily.HELVETICA, 7,Font.BOLD);
			font23= new Font(Font.FontFamily.HELVETICA, 23);
			font20= new Font(Font.FontFamily.HELVETICA, 20);
			
		}
		
		public void   setCoveringLetter(ContractRenewal conRenewal){
		conRenw= conRenewal;
		
		if (conRenw.getCompanyId() == null)
			{comp = ofy().load().type(Company.class).first().now();
		    System.out.println("Comp Id"+conRenw.getCompanyId());
			}
		else{
			comp = ofy().load().type(Company.class).filter("companyId", conRenw.getCompanyId()).first().now();
			System.out.println("Comp Id"+conRenw.getCompanyId());
		    }
		
		if (conRenw.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", conRenw.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getCustomerId()).first().now();
		
		if (conRenw.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", conRenw.getContractId()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getContractId()).first().now();
        
		//Load Article type Details
		
		
		articletype = new ArrayList<ArticleType>();
			if(cust.getArticleTypeDetails().size()!=0){
				articletype.addAll(cust.getArticleTypeDetails());
			}
			if(comp.getArticleTypeDetails().size()!=0){
				articletype.addAll(comp.getArticleTypeDetails());
			}
 }
	
public void createPdf() {
			
			createHeader();
		    createToInfo();
			createCoveringParagraph();
		
}
		
private void createHeader()

{

String addressline1="";

if(comp.getAddress().getAddrLine2()!=null){
addressline1=comp.getAddress().getAddrLine1()+", "+comp.getAddress().getAddrLine2()+", ";
}
else{
addressline1=comp.getAddress().getAddrLine1()+", ";
}

String locality=null;
if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
System.out.println("inside both null condition1");
locality= (comp.getAddress().getLandmark()+", "+comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+" - "
	      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
}
else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
System.out.println("inside both null condition 2");
locality= (comp.getAddress().getLandmark()+", "+comp.getAddress().getCity()+" - "
	      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
}

else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
System.out.println("inside both null condition 3");
locality= (comp.getAddress().getLocality()+", "+comp.getAddress().getCity()+" - "
	      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
}
else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
System.out.println("inside both null condition 4");
locality=(comp.getAddress().getCity()+" - "
	      +comp.getAddress().getPin()+". "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+". ");
}

//String contactinfo = "" ;
//System.out.println("landline no "+comp.getLandline());
//System.out.println("Cell no1 "+comp.getCellNumber1());
//System.out.println("Cell no2 "+comp.getCellNumber2());
//System.out.println("fax no "+comp.getFaxNumber());

//if (comp.getLandline()!=0 && comp.getCellNumber2()!= 0  && comp.getFaxNumber()!=null)
//{
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() +" / "+ comp.getLandline() + " SERVICE DEPT.: 2351 4360"
//			 + "  Fax : " + comp.getFaxNumber());
//}
//else if (comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()!=null ) 
//{
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()  
//			+ "  Fax : " + comp.getFaxNumber());
//}
//else if(comp.getLandline() != 0  && comp.getCellNumber2() == 0 && comp.getFaxNumber()!=null ){
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline() 
//			 + "  Fax : " + comp.getFaxNumber());
//}
//else if(comp.getLandline() != 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null)
//{
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2()+" / "+ comp.getLandline() + " SERVICE DEPT.: 2351 4360"
//			+ "  Fax : "  );	
//}
//else if(comp.getLandline() != 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null )
//{
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getLandline() 
//			+ "  Fax : "  );	
//}
//else if(comp.getLandline() == 0 && comp.getCellNumber2() != 0 && comp.getFaxNumber()==null )
//{
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() +" / "+ comp.getCellNumber2() 
//			 + "  Fax : "  );	
//}
//else if(comp.getLandline() == 0 && comp.getCellNumber2() == 0 && comp.getFaxNumber()==null)
//{
//	contactinfo = ("Tel. : "  + comp.getCellNumber1() + "  Fax : "  );	
//}	

Phrase mycomHeader=new Phrase(comp.getBusinessUnitName().toUpperCase(),font12);
Phrase header1 = new Phrase ("Regd Office: "+ addressline1 + locality ,font10);
Phrase header2 = new Phrase ("Tel.: (91-22) 2266 1091, 2262 5376 Fax : (91-22) 2266 0810 ",font10);
Phrase header3 = new Phrase ("E-Mail : "+comp.getEmail().trim()+ " Website : "+comp.getWebsite(),font10);

Paragraph mycomPara=new Paragraph();
mycomPara.add(mycomHeader);  
mycomPara.add(Chunk.NEWLINE);
mycomPara.add(header1);
mycomPara.add(Chunk.NEWLINE);    
mycomPara.add(header2);
mycomPara.add(Chunk.NEWLINE);
mycomPara.add(header3);
mycomPara.add(Chunk.NEWLINE); 
mycomPara.setMultipliedLeading((float) 1.3);

for(int i=0;i<this.articletype.size();i++)
 
 {	
	  if(articletype.get(i).getArticleTypeName().equalsIgnoreCase("CIN")&& articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("Service")){  
	 		
      Phrase header4 =new Phrase("CIN :"+articletype.get(i).getArticleTypeValue(),font10);
      mycomPara.add(header4);
	  }   
}

mycomPara.add(Chunk.NEWLINE);
mycomPara.setAlignment(Element.ALIGN_CENTER);

PdfPCell mycomCell=new PdfPCell();
mycomCell.addElement(mycomPara);
mycomCell.setBorder(0);
	
PdfPTable parentTbl=new PdfPTable(1);
parentTbl.setWidthPercentage(100);      
parentTbl.addCell(mycomCell);     

try {
	  document.add(parentTbl);  
    } 
catch (Exception e) 
    {
	e.printStackTrace();
    }

}

private void createToInfo() 
{
	
	Date date = new Date();
	Calendar calobj = Calendar.getInstance(); 
	Phrase renewalDt = new Phrase (""+fmt1.format(date),font10);
	PdfPCell renewalDtCell = new PdfPCell(renewalDt);
	renewalDtCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	renewalDtCell.setBorder(0);
	
	String customerFullName="";  
	
	if(cust.isCompany())
	  {
		customerFullName=cust.getCompanyName();
	  }
	else
	  {
		
	   if(cust.getSalutation()!= null && ! cust.getSalutation().equals(""))
		  {
			customerFullName=cust.getSalutation()+" "+cust.getFullname();
		  }
	   else
		  {
			customerFullName=cust.getFullname();
		  }
	  }
	
	/*
	 * nameInFirstLetterUpperCase defined by Ajinkya  suggested by rahul 
	 * Used to take customer name from Uppercase to Only first letter Uppercase
	 */
	String nameInFirstLetterUpperCase=getFirstLetterUpperCase(customerFullName.trim());
	Phrase custInfo = new Phrase(nameInFirstLetterUpperCase, font10);
	PdfPCell custInfoCell = new PdfPCell(custInfo);
	custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	custInfoCell.setBorder(0);
	
	/////////////////////////////////////////////// address code   ////////////////////////////
	String addressline1="";
	
	if(cust.getAdress().getAddrLine2().equals(null)&& cust.getAdress().getAddrLine2().equals(""))
	{
		addressline1 = cust.getAdress().getAddrLine1()+", "+cust.getAdress().getAddrLine2()+", "+"\n" ;
	}
	else {
		addressline1 = cust.getAdress().getAddrLine1()+", "+"\n" ;
	}
   
	String locality = "";
	if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
		System.out.println("inside both null condition1");
		locality= (cust.getAdress().getLandmark()+", "+cust.getAdress().getLocality()+" , "+"\n"+cust.getAdress().getCity()+" - "
			      +cust.getAdress().getPin()+". "+"\n" +cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
	else if((!cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
		System.out.println("inside both null condition 2");
		locality= (cust.getAdress().getLandmark()+", "+"\n"+cust.getAdress().getCity()+" - "
			      +cust.getAdress().getPin()+". "+"\n"+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
	
	else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==false)){
		System.out.println("inside both null condition 3");
		locality= (cust.getAdress().getLocality()+", "+"\n"+cust.getAdress().getCity()+" - "
			      +cust.getAdress().getPin()+". "+"\n"+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
	else if((cust.getAdress().getLandmark().equals(""))&&(cust.getAdress().getLocality().equals("")==true)){
		System.out.println("inside both null condition 4");
		locality=(cust.getAdress().getCity()+" - "+cust.getAdress().getPin()+". "+"\n"+cust.getAdress().getState()+", "+cust.getAdress().getCountry()+". ");
	}
	
	/////////////////////////////////////////////// address code End Here  ////////////////////////////	
	
	Phrase custAddInfo = new Phrase(addressline1 + locality, font10);
	Paragraph adrsPara = new Paragraph ();
	adrsPara.add(custAddInfo);
	adrsPara.setAlignment(Element.ALIGN_LEFT);
	PdfPCell custAddInfoCell = new PdfPCell();
	custAddInfoCell.addElement(adrsPara);
//	custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	custAddInfoCell.setBorder(0);
	
	
	Phrase blank = new Phrase(" ", font10);
	PdfPCell blankCell = new PdfPCell(blank);
	blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	blankCell.setFixedHeight(5);
	blankCell.setBorder(0);
	
	PdfPTable  parentTable = new PdfPTable(1);
	parentTable.setWidthPercentage(100);
	parentTable.addCell(renewalDtCell);
	parentTable.addCell(blankCell);
	parentTable.addCell(custInfoCell);
	parentTable.addCell(custAddInfoCell);
	
	try {
		document.add(parentTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
    //Ajinkya added this code to convert customer name in CamelCase    
	//  Date : 12/4/2017

private String getFirstLetterUpperCase(String customerFullName)
   {
	
	String customerName="";
	String[] customerNameSpaceSpilt=customerFullName.split(" ");
	int count=0;
	for (String name : customerNameSpaceSpilt) {
		String nameLowerCase=name.toLowerCase();
		if(count==0){
			customerName=customerName+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);
		}else{
			customerName=customerName+" "+name.toUpperCase().substring(0, 1)+nameLowerCase.substring(1);	
		}
		count=count+1;
	}
	return customerName;  
}

private void createCoveringParagraph() 
 {

	//  rohan added this code as per salutation in customer here we will add dear sir / madam  
	//  Date : 13/2/2017	
	String salutation = "";
	if(cust.getSalutation()!= null && !cust.getSalutation().equals(""))
	{
		if(cust.getSalutation().equalsIgnoreCase("Mr."))
		{
			salutation = "Dear Sir ,";
		}
		else if(cust.getSalutation().equalsIgnoreCase("Ms."))
		{
			salutation = "Dear Madam ,";
		}
		else
		{
			salutation ="Dear Sir / Madam ,";
		}
	}	
	else 
	{
		salutation ="Dear Sir / Madam ,";
	}
	
Phrase startWords = new Phrase (salutation,font10);

Paragraph coveringLetterPara = new Paragraph();
//coveringLetterPara.add(Chunk.NEWLINE);  
coveringLetterPara.add(startWords);
coveringLetterPara.add(Chunk.NEWLINE);  
coveringLetterPara.add(Chunk.NEWLINE);


Phrase coveringletterphrase1 = new Phrase ("",font10);
for(int i=0;i<conRenw.getItems().size();i++)
{
	String productCode = conRenw.getItems().get(i).getProductCode(); 
    coveringletterphrase1 = new Phrase ("We are enclosing herewith our order form no."+" ("+productCode +"/"+con.getCount()+" )",font10);  /// contract count / only count 
}
coveringLetterPara.add(coveringletterphrase1);
coveringLetterPara.add(Chunk.NEWLINE);

Phrase coveringletterphrase2 = new Phrase (" dt. " +fmt.format(conRenw.getDate())+ "  for Rs. "+con.getTotalAmount()+"  all inclusive for the period,"
+fmt.format(conRenw.getDate())+ " to "+ fmt.format(conRenw.getContractEndDate())+" .",font10);
Phrase coveringletterphrase3 = new Phrase ("As per the latest notification of the Government, Service Tax has been increased ",font10bold);
Phrase coveringletterphrase4 = new Phrase ("from 14.5 to 15 % from 1st june 2016. (Service Tax @ 14.% , Swachh Bharat Cess @ 0.5 % & Krishi Kalyan Cess @ 0.5 % ) ",font9bold);
Phrase coveringletterphrase5 = new Phrase ("Please send us the copy duly signed, along with your cheque,retaining the original for your record. ",font10);
Phrase coveringletterphrase6 = new Phrase ("It is important that we receive the signed copy of the Contract. ",font10);
Phrase coveringletterphrase7 = new Phrase ("We request your co-operation in matter. ",font10);
Phrase coveringletterphrase8 = new Phrase ("Thanking you, ",font10);
Phrase coveringletterphrase9 = new Phrase ("Yours Faithfully,",font10);
Phrase coveringletterphrase10 = new Phrase ("For",font10);
Phrase coveringletterphrase11 = new Phrase ("  "+comp.getBusinessUnitName(),font10bold);
Phrase coveringletterphrase12 = new Phrase ("-----------------------------------",font10);
Phrase coveringletterphrase13 = new Phrase (" AUTHORIZED SIGNATORY  ",font10);
Phrase coveringletterphrase14 = new Phrase ("SS/-",font10);

coveringLetterPara.add(coveringletterphrase2);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase3);
coveringLetterPara.add(coveringletterphrase4);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase5);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase6);
coveringLetterPara.add(coveringletterphrase7);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase8);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase9);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase10);
coveringLetterPara.add(coveringletterphrase11);  
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.setMultipliedLeading((float) 1.3);
coveringLetterPara.add(coveringletterphrase12);
coveringLetterPara.add(Chunk.NEWLINE);   
coveringLetterPara.add(coveringletterphrase13);
coveringLetterPara.add(Chunk.NEWLINE);
coveringLetterPara.add(coveringletterphrase14);
coveringLetterPara.add(Chunk.NEWLINE);

PdfPCell coveringLetterCell = new PdfPCell ( );
coveringLetterCell.addElement(coveringLetterPara);
coveringLetterCell.setBorder(0);

PdfPTable coveringletterTbl = new PdfPTable(1);
coveringletterTbl.setWidthPercentage(100);
coveringletterTbl.addCell(coveringLetterCell);

try {
	document.add(coveringletterTbl);  
} catch (DocumentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
}
}
