package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocdownload.CsvWriter;

public class CreatePayslipStatementPdfServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4909519057829089215L;

	/**
	 * 
	 */

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
//		super.doGet(req, resp);
		Logger logger=Logger.getLogger("Pay Pdf Logger");
		resp.setContentType("application/pdf");
		try {
			String stringId = req.getParameter("Id");
			stringId = stringId.trim();
			Long count = Long.parseLong(stringId);
			
			boolean isInternal=false;
			String salRegTyp = req.getParameter("Type");
			if(salRegTyp.equalsIgnoreCase("External")){
				isInternal=false;
			}else{
				isInternal=true;
			}

			PayslipStatementPdf payStmt = new PayslipStatementPdf();
			
			payStmt.document =new Document(PageSize.A4.rotate(),0,0,0,0);
			Document document = payStmt.document;
			PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());// write the pdf in response
			document.open();
//			CsvWriter csv=new CsvWriter();
			payStmt.setPayslip(count,CsvWriter.paySlipList,isInternal);
			payStmt.createPdf();
			
			document.close();

		} catch (DocumentException e) {
			e.printStackTrace();
		}
		  
	}
	
	
	

}
