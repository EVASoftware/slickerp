package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.util.IOUtils;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.customerbranchdetails.servicelocation.Area;
import com.slicktechnologies.client.views.device.CatchTraps;
import com.slicktechnologies.client.views.device.PestTreatmentDetails;
import com.slicktechnologies.client.views.device.TrackTableDetails;
import com.slicktechnologies.server.addhocdownload.XlsxWriter;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchServiceLocation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.EmployeeInfo;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productgroup.ProductGroupList;
import com.slicktechnologies.shared.common.servicerelated.ServiceProject;

public class CustomerServiceRecordPdf {
	
	SimpleDateFormat sdf1;
	
	Logger logger=Logger.getLogger("CustomerServiceRecordPdf.class");
	public Document document;
   
	Company comp;
	Customer cust;
	Service service;
	CustomerBranchDetails customerBranchDetails;//Ashwini Patil Date:5-01-2023
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private Font font16boldul, font12bold, font8bold, font8, font12boldul,
	font12, font16bold, font10, font10bold, font14bold, font14, font9 ,font9bold;
	Branch branchDt;
	boolean checkBranchdata = false;
	/**
	 * @author Anil
	 * @since 26-06-2020
	 * No need to load customer branch entity for just printing customer branch name 
	 * it is already stored in service entity
	 */
//	CustomerBranchDetails custbranch;
	private PdfPCell imageSignCell;
	boolean companyAsaletter=false;
	
	/**
	 * @author Anil
	 * @since 24-06-2020
	 */
	PdfUtility pdf=new PdfUtility();
	int maxRow=6; //3
	ServiceProject project;
	int maxRowForCheckList=5; //5
	boolean printPhotoOnSRCopyFlag;
	boolean printMaterialOnSRCopyFlag;
	boolean printFindingsOnSRCopyFlag;
	boolean printChecklistOnSRCopyFlag;
	boolean printCustomerRemarkFlag;
	boolean printTechnicianRemarkFlag;
	boolean defaultFooterFlag=true;
	boolean photoFlag=true;
	boolean PC_DoNotPrintMaterialOnSummarySR=false; //Ashwini Patil Date: 23-06-2022 As per pecopp requirement they dont want to print material on summary SR copy
	int clCount;
	
	boolean printPestTreatmentFlag;
	
	
	/**
	 * @author Anil @since 16-09-2021
	 * printing pest treatment as finding using process "PC_SR_FINDINGS"
	 */
	
	boolean PC_SR_FINDINGS=false;
	
	/**
	 * @author Anil @since 28-10-2021
	 * Below process configuration is used to print Service record copy for UDS (wash room Hygiene)
	 * By Nitin Sir and Nithila
	 */
	boolean PC_GenericSrCopy=false;
	
	//Ashwini Patil
	String fromDate="",toDate="";
	String branch;
	List<Service> totalServicesList;
	String reportFormat="";
	String reportName="";
	boolean completedFlag=false,cancelledFlag=false,otherFlag=false;
	int rowCounter=0, counter=0;
	XSSFCellStyle boldStyle,style,leftStyle;
	boolean isCalledFromContract=false; //To print contactId From CreateHeader method
	boolean onlyForPsipl=false; //Ashwini  Patil Date:4-10-2024 for psipl
	boolean onlyForPestOCop=false; //Ashwini  Patil Date:12-12-2024 for pest o cop label changes
	/**
	 * @author Ashwini Patil
	 * @since 6-01-2025 
	 * When completed services are in large amount size of pdf document becomes more than 25MB and SR email is not getting send.
	 * Header and footer images are right now getting inserted on every page which is creating size issue.
	 * As per Nitin sir's instruction, adding header in the first copy and footer in the last copy.
	 */
	boolean printHeaderFlag=true;
	boolean printFooterFlag=false;
	
	public CustomerServiceRecordPdf(){	
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 10);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL);
		font9 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font9bold=new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		totalServicesList=new ArrayList<Service>();
	}
	
	//Ashwini Patil to retrive user selections from MultipleSRCopyPrintPopup
	public CustomerServiceRecordPdf(Date fromDate1,Date toDate1,String branch1,List<Service> totalServicesList1,String reportName,String reportFormat,boolean completedFlag,boolean cancelledFlag,boolean otherFlag) {
				
				this.reportFormat=reportFormat;
				this.reportName=reportName;
				this.completedFlag=completedFlag;
				this.cancelledFlag=cancelledFlag;
				this.otherFlag=otherFlag;
				sdf1 = new SimpleDateFormat("dd/MM/yyyy");
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				sdf1.setTimeZone(TimeZone.getTimeZone("IST"));
		
				logger.log(Level.SEVERE,"in constructor fromDate1="+fromDate1+" toDate1= "+toDate1);
				
				if(fromDate1!=null)
					fromDate=sdf1.format(fromDate1);
				if(toDate1!=null)
					toDate=sdf1.format(toDate1);
				
				logger.log(Level.SEVERE,"in constructor fromDate="+fromDate+" toDate= "+toDate);
				branch=branch1;
				totalServicesList=new ArrayList<Service>();
				totalServicesList=totalServicesList1;
				
				isCalledFromContract=true;				
				
				
				font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
						| Font.UNDERLINE);
				font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
				new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
				font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
				font8bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
				font8 = new Font(Font.FontFamily.HELVETICA, 10);
				font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
						| Font.UNDERLINE);
				font12 = new Font(Font.FontFamily.HELVETICA, 12);
				font10 = new Font(Font.FontFamily.HELVETICA, 10);
				font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
				font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
				font14 = new Font(Font.FontFamily.HELVETICA, 14, Font.NORMAL);
				font9 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
				font9bold=new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
				
				fmt.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}
	
	public void createPdf() {
		logger.log(Level.SEVERE,"INSIDE CREATE PDF CUSTOMERSERICERECORD PDF");
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead", comp.getCompanyId())){
			companyAsaletter=true;
		}
		
		
		if(companyAsaletter){
			if (comp.getUploadHeader() != null&&printHeaderFlag) {
				createCompanyNameAsHeader(document, comp);
				printHeaderFlag=false;
				blankCells();
			}
		}else{
			createHeader();
		}
		createaAddress();
		/**
		 * @author Anil
		 * @since 24-06-2020
		 */
		if(defaultFooterFlag){
			logger.log(Level.SEVERE,"INSIDE Default footer");
			createFooter();
		}else{
			logger.log(Level.SEVERE,"printMaterialOnSRCopyFlag "+printMaterialOnSRCopyFlag);
			logger.log(Level.SEVERE,"printFindingsOnSRCopyFlag "+printFindingsOnSRCopyFlag);
			logger.log(Level.SEVERE,"printPhotoOnSRCopyFlag "+printPhotoOnSRCopyFlag);
			logger.log(Level.SEVERE,"photoFlag "+photoFlag);
			logger.log(Level.SEVERE,"printTechnicianRemarkFlag "+printTechnicianRemarkFlag);
			logger.log(Level.SEVERE,"printCustomerRemarkFlag "+printCustomerRemarkFlag);
			logger.log(Level.SEVERE,"printChecklistOnSRCopyFlag "+printChecklistOnSRCopyFlag);
			
			if(printPestTreatmentFlag){
				try {
					document.add(createPestTreatmentTable());
				}catch(Exception e){
					e.printStackTrace();	
				}
			}
			if(printMaterialOnSRCopyFlag||printFindingsOnSRCopyFlag){
				if(printMaterialOnSRCopyFlag&&printFindingsOnSRCopyFlag){
					try {
						document.add(createFindingAndMaterialTable());
					}catch(Exception e){
						e.printStackTrace();	
					}
				}else if(printMaterialOnSRCopyFlag){
					try {
						document.add(createMaterialTable());
					}catch(Exception e){
						e.printStackTrace();	
					}
				}else if(printFindingsOnSRCopyFlag){
					try {
						document.add(createFindingTable());
					}catch(Exception e){
						e.printStackTrace();	
					}
				}
			}
			
			/**
			 * @author Anil @since 16-09-2021
			 */
			if(PC_SR_FINDINGS){
				try {
					document.add(createPestTreatmentAsFindingTable());
				}catch(Exception e){
					e.printStackTrace();	
				}
			}
			
			if(printPhotoOnSRCopyFlag&&photoFlag){
				try {
					document.add(createPhotosTable());
				}catch(Exception e){
					e.printStackTrace();	
				}
			}
			
			try {
				document.add(createFooterTable());
			}catch(Exception e){
				e.printStackTrace();	
			}
			
		}
		
		
		if(companyAsaletter){
			if (comp.getUploadFooter() != null&&printFooterFlag) {
				createCompanyNameAsFooter(document, comp);
				printFooterFlag=false;
			}
		}
	}


	private void blankCells() {
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
//			document.add(blank);
//			document.add(blank);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp2) {
		DocumentUpload document = comp.getUploadFooter();
		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsHeader(Document doc, Company comp2) {
		DocumentUpload document = comp.getUploadHeader();
		// patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}

	public void setpdfCustomerServRecord(Long count) {
		// loading service
		service = ofy().load().type(Service.class).id(count).now();

		// loading customer
		if (service.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", service.getPersonInfo().getCount())
					.filter("companyId", service.getCompanyId()).first().now();

		
		
		if(service.getServiceBranch()!=null&&!service.getServiceBranch().equals("")) {
			logger.log(Level.SEVERE,"service branch is not null");
			if(!service.getServiceBranch().equals("Service Address")) {
				logger.log(Level.SEVERE,"loading customer branch");
				customerBranchDetails=ofy().load().type(CustomerBranchDetails.class)
						.filter("cinfo.count",service.getPersonInfo().getCount())
						.filter("buisnessUnitName", service.getServiceBranch()).first().now();
				
			}				
		}
		
		// loading company
		if (service.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", service.getCompanyId()).first().now();

		if (service.getCompanyId() != null) {

		}
		branchDt = ofy().load().type(Branch.class)
				.filter("companyId", service.getCompanyId())
				.filter("buisnessUnitName", service.getBranch()).first().now();
		
		
		//Ashwini Patil Date:3-03-2023
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			if(branchDt!=null) {
				comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
			}
		}
		
		/**
		 * @author Anil
		 * @since 26-06-2020
		 * No need to load customer branch entity for just printing customer branch name 
		 * it is already stored in service entity
		 */
//        custbranch=ofy().load().type(CustomerBranchDetails.class)
//				.filter("companyId", comp.getCompanyId()).first().now();
          
          
		if (service != null) {
			project = ofy().load().type(ServiceProject.class)
					.filter("companyId", service.getCompanyId())
					.filter("contractId", service.getContractCount())
					.filter("serviceId", service.getCount()).first().now();
		}

		
		
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", service.getCompanyId()).filter("processName", "Service").filter("configStatus", true).list();
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintPhotoOnSRCopy")&&ptDetails.isStatus()==true){
						printPhotoOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintMaterialOnSRCopy")&&ptDetails.isStatus()==true){
						printMaterialOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintFindingsOnSRCopy")&&ptDetails.isStatus()==true){
						printFindingsOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintChecklistOnSRCopy")&&ptDetails.isStatus()==true){
						printChecklistOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintCustomerRemark")&&ptDetails.isStatus()==true){
						printCustomerRemarkFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintTechnicianRemark")&&ptDetails.isStatus()==true){
						printTechnicianRemarkFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintPestTreatmentOnSRCopy")&&ptDetails.isStatus()==true){
						printPestTreatmentFlag=true;
						printMaterialOnSRCopyFlag=false;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PC_SR_FINDINGS")&&ptDetails.isStatus()==true){
						PC_SR_FINDINGS=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PC_GenericSrCopy")&&ptDetails.isStatus()==true){
						PC_GenericSrCopy=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("OnlyForPsipl")&&ptDetails.isStatus()==true){
						onlyForPsipl=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("onlyForPestOCop")&&ptDetails.isStatus()==true){
						onlyForPestOCop=true;
					}
					
				}
			}
		}
		
		if(printPhotoOnSRCopyFlag||printMaterialOnSRCopyFlag||printFindingsOnSRCopyFlag||printChecklistOnSRCopyFlag||printCustomerRemarkFlag||printTechnicianRemarkFlag||printPestTreatmentFlag||PC_SR_FINDINGS){
			defaultFooterFlag=false;
		}
		
		if((service.getServiceImage1()==null||service.getServiceImage1().getUrl().equals(""))
				&&(service.getServiceImage2()==null||service.getServiceImage2().getUrl().equals(""))
				&&(service.getServiceImage3()==null||service.getServiceImage3().getUrl().equals(""))
				&&(service.getServiceImage4()==null||service.getServiceImage4().getUrl().equals(""))){
			photoFlag=false;
		}
	}
	
	private void createaAddress() {
		
		PdfPTable mainTable=new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable billingaddessTable=new PdfPTable(1);
		billingaddessTable.setWidthPercentage(100);
//		billingaddessTable.setSpacingAfter(5);
		
		
		Phrase serviceph=new Phrase("This is to certify that "+service.getProductName()+" Service was provided at the premises of ",font8bold);
		PdfPCell servicecell=new PdfPCell(serviceph);
		servicecell.setBorder(0);
		servicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(servicecell);
		
		String custBranch="";
		String customerBranchName ="";
		/**
		 * @author Anil
		 * @since 26-06-2020
		 * No need to load customer branch entity for just printing customer branch name 
		 * it is already stored in service entity
		 */
//		if(custbranch!=null&&custbranch.getBranch()!=null){
//			custBranch=custbranch.getBranch();
//		}else{
//			custBranch="";
//		}
		
		if(service.getServiceBranch()!=null&&!service.getServiceBranch().equals("")&&!service.getServiceBranch().equals("Service Address")){
			custBranch=service.getServiceBranch();
			customerBranchName = service.getServiceBranch();
		}
		/**
		 * @author Vijay Date :- 18-11-2020
		 * Des :- Updated code if customer branch is blank then mapping customer name and
		 * also added location to show premises details if exist
		 */
		 
		 /**
		 * @author Priyanka Date :- 07-08-2021
		 * Des :- Change label customer branch to Name / Company Name / Branch
		 * 
		 */
//		boolean flag = false;
//		if(custBranch.equals("")) {
//			//custBranch = service.getCustomerName();
//			//flag = true;
//		}
		
		/**
		 * @author Anil @since 28-10-2021
		 * For UDS label should be location
		 */
		String updatedBranch="Branch : "+custBranch;
		if(PC_GenericSrCopy){
			updatedBranch="Location : "+custBranch;
		}
		Phrase custbranchPh = null;
		if(!custBranch.equals("")) {
//			custbranch=new Phrase("Branch : "+custBranch,font8);
			custbranchPh=new Phrase(updatedBranch,font8);
			PdfPCell custbranchcell=new PdfPCell(custbranchPh);
			custbranchcell.setBorder(0);
	//		custnamecell.setPaddingTop(8);
			custbranchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			billingaddessTable.addCell(custbranchcell);
		}
		
		Phrase custInfo ;
		if(cust.isCompany()){
			custInfo = new Phrase("Company Name : "+cust.getCompanyName(), font8);
		}else{
			custInfo = new Phrase("Name : "+cust.getFullname(), font8);
		}
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		billingaddessTable.addCell(custInfoCell);
		
		String primices = "";
		if(service.getPremises()!=null && !service.getPremises().equals("")) {
			primices = service.getPremises();
		}
		if(!primices.equals("")) {
		Phrase location=new Phrase("Location : "+primices,font8);
		PdfPCell locationcell=new PdfPCell(location);
		locationcell.setBorder(0);
		locationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(locationcell);
		}
		
		
		
		
//		Phrase custname=new Phrase(service.getCustomerName(),font8);
//		PdfPCell custnamecell=new PdfPCell(custname);
//		custnamecell.setBorder(0);
////		custnamecell.setPaddingTop(8);
//		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		billingaddessTable.addCell(custnamecell);
		
		
		
		
		Phrase custadd=null;
		if(service.getAddress()!=null&&service.getAddress().getCompleteAddress()!=null) //Ashwini Patil Date:17-10-2023
			custadd=new Phrase(service.getAddress().getCompleteAddress(),font8);
		else
			custadd=new Phrase("",font8);
		
		PdfPCell custaddcell=new PdfPCell(custadd);
		custaddcell.setBorder(0);
		custaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(custaddcell);
		
		Phrase custCellNo=new Phrase("Phone : "+cust.getCellNumber1(),font8);
		PdfPCell custCellNocell=new PdfPCell(custCellNo);
		custCellNocell.setBorder(0);
		custCellNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billingaddessTable.addCell(custCellNocell);
		
		
		
		
		PdfPTable serviceDetailTable=new PdfPTable(2);
		serviceDetailTable.setWidthPercentage(100);
//		serviceDetailTable.setSpacingAfter(5);
		try {
			serviceDetailTable.setWidths(new float[]{50,50});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase serviceId=new Phrase("Service Id : "+service.getCount(),font8);
		PdfPCell serviceIdcell=new PdfPCell(serviceId);
		serviceIdcell.setBorder(0);
		serviceIdcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceIdcell);
		
		Phrase serviceNo=new Phrase("Service No : "+service.getServiceSerialNo(),font8);
		PdfPCell serviceNocell=new PdfPCell(serviceNo);
		serviceNocell.setBorder(0);
		serviceNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceNocell);
		
		
		Phrase serviceBranch=new Phrase("Branch : "+service.getBranch(),font8);
		PdfPCell serviceBranchcell=new PdfPCell(serviceBranch);
		serviceBranchcell.setBorder(0);
//		contractIdcell.setColspan(2);
		serviceBranchcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(serviceBranchcell);
		/**
		 * @author Ashwini Patil
		 * @since 24-05-2022
		 * Adding contract id and contract duration below service ID as per requirement given by Nitin sir
		 */
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Phrase contractId=new Phrase("Contract Id : "+service.getContractCount(),font8);
		PdfPCell contractIdcell=new PdfPCell(contractId);
		contractIdcell.setBorder(0);
//		contractIdcell.setColspan(2);
		contractIdcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(contractIdcell);
		
		if(onlyForPsipl) {
			if(service.getRefNo()!=null&&!service.getRefNo().equals("")&&!service.getRefNo().equals("0")) {
				Phrase refno=new Phrase("Ref. No. : "+service.getRefNo(),font8);
				PdfPCell refnocell=new PdfPCell(refno);
				refnocell.setBorder(0);
				refnocell.setColspan(2);
				refnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(refnocell);			
			}
		}
		
		Phrase duration=null;
		if(onlyForPsipl)
			duration=new Phrase("Contract Period : "+sdf.format(service.getContractStartDate()) +" To "+sdf.format(service.getContractEndDate()),font8);
		else
			duration=new Phrase("Duration : "+sdf.format(service.getContractStartDate()) +" To "+sdf.format(service.getContractEndDate()),font8);
		
		PdfPCell durationcell=new PdfPCell(duration);
		durationcell.setBorder(0);
		durationcell.setColspan(2);
		durationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(durationcell);
		
		/** @sheetal:02-02-2022
		 *   Des : Printing Premise on SR Copy if exist**/
		if(service.getPremises()!=null && !service.getPremises().equals("")){
			/**@sheetal:22-02-2022
			 *  Des : If value entered in premise is numeric label will be quantity, requirement by UDS water***/
			String string=service.getPremises();
			if(isNumeric(string)){
				Phrase premises=new Phrase("Quantity : "+service.getPremises(),font8);
				PdfPCell premisescell=new PdfPCell(premises);
				premisescell.setBorder(0);
				premisescell.setColspan(2);
				premisescell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(premisescell);
				System.out.println("Inside numeric quantity");
			}else{
			Phrase premises=new Phrase("Premise : "+service.getPremises(),font8);
			PdfPCell premisescell=new PdfPCell(premises);
			premisescell.setBorder(0);
			premisescell.setColspan(2);
			premisescell.setHorizontalAlignment(Element.ALIGN_LEFT);
			serviceDetailTable.addCell(premisescell);
			}/**end**/
		}
		
		

		
		Phrase blank=new Phrase("",font8bold);
		PdfPCell blankcell=new PdfPCell(blank);
		blankcell.setBorder(0);
		blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		serviceDetailTable.addCell(blankcell);
		
		/**Added by sheetal:31-01-2022
		 * Des:If team of technicians are selected then names of all 
		       technicians will print in SR copy, requirement raised by poonam**/
		if(service.getTechnicians()!=null && service.getTechnicians().size()>1){
			
				String stremployeName = "";
				for(int i =0;i< service.getTechnicians().size();i++){
					stremployeName = stremployeName + service.getTechnicians().get(i).getFullName();
					if(i!=service.getTechnicians().size()-1){
						stremployeName +=",";
					}
				}
			
			logger.log(Level.SEVERE,"team names "+stremployeName);
			Phrase technicianName=new Phrase("Technician Name : "+stremployeName,font8);
			PdfPCell technicianNamecell=new PdfPCell(technicianName);
			technicianNamecell.setBorder(0);
			technicianNamecell.setColspan(2);
			technicianNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			serviceDetailTable.addCell(technicianNamecell);
		}/**end**/
		else{		
			Phrase technicianName;
			if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_HideCellNumber_SRCopy",comp.getCompanyId())) 
				technicianName=new Phrase("Technician Name : "+service.getEmployee()+" - "+comp.getCellNumber1(),font8);
			else
				technicianName=new Phrase("Technician Name : "+service.getEmployee(),font8);
		 
		PdfPCell technicianNamecell=new PdfPCell(technicianName);
		technicianNamecell.setBorder(0);
		technicianNamecell.setColspan(2);
		technicianNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		serviceDetailTable.addCell(technicianNamecell);
		logger.log(Level.SEVERE,"technician name "+technicianNamecell);
	
		}
		/**
		 * @author Anil @since 28-10-2021
		 * For UDS and ALL instead of showing technician number we need to show company number
		 * raised by Nitin Sir
		 */
       /*
        * Added by sheetal: 10-11-2021
        * Des: Mobile number should remove from SR copy for specific process configuration
        */
//		if(!ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service","PC_HideCellNumber_SRCopy",comp.getCompanyId())){
////		Phrase mobNo=new Phrase("Mobile No : "+service.getCellNumber(),font8);
//		Phrase mobNo=new Phrase("Mobile No : "+comp.getCellNumber1(),font8);
//		PdfPCell mobNocell=new PdfPCell(mobNo);
//		mobNocell.setBorder(0);
//		mobNocell.setColspan(2);
//		mobNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		serviceDetailTable.addCell(mobNocell);
//		}
//		String serviceDuration="";
		String serviceDuration=getServiceDuration();
//		logger.log(Level.SEVERE, "Serv Duration 1111 "+serviceDuration);
		Phrase serviceDurationPhrase=new Phrase("Service Duration : "+serviceDuration,font8);
		PdfPCell serviceDurationcell=new PdfPCell(serviceDurationPhrase);
		serviceDurationcell.setBorder(0);
		serviceDurationcell.setColspan(2);
		serviceDurationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/**
		 * @author Anil @since 16-09-2021
		 * Removing service duration for Envocare 
		 * raised by Swati and Nitin Sir
		 */
//		serviceDetailTable.addCell(serviceDurationcell);//Ashwini Patil Date:1-07-2022
		 logger.log(Level.SEVERE, "Service Duration :" + serviceDuration);
		/** @sheetal:02-02-2022
		 *   Des : Printing Start time of the service if exist,requirement by UDS water**/
		

		if(service.getTrackServiceTabledetails()!=null&&service.getTrackServiceTabledetails().size()!=0){
			String startedTime="";
			SimpleDateFormat serverDateSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			SimpleDateFormat serverTimeSdf = new SimpleDateFormat("HH:mm");
			for(TrackTableDetails tractTable:service.getTrackServiceTabledetails()){
				if(tractTable.getDate_time()!=null&&tractTable.getStatus().equalsIgnoreCase("Reported")){//Started
				 try{
					 Date Time = serverDateSdf.parse(tractTable.getDate_time());
					 startedTime=serverTimeSdf.format(Time);
					 logger.log(Level.SEVERE, "Parsed Time" + startedTime);
					 
				}catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				  
					}
				
			}
		
			Phrase startTime=new Phrase("Start Time : "+startedTime,font8);
			PdfPCell startTimecell=new PdfPCell(startTime);
			logger.log(Level.SEVERE, "Start Time" + startTime);
			startTimecell.setBorder(0);
			startTimecell.setColspan(2);
			startTimecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			serviceDetailTable.addCell(startTimecell);
		}
		
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

		String serviceCompleteDate="";
		if(service.getCompletedDate_time()!=null){
//			serviceCompleteDate=service.getCompletedDate_time();
			try {
				Date completionDate = dateformat.parse(service.getCompletedDate_time());
				serviceCompleteDate = dateformat.format(completionDate);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}else{
			serviceCompleteDate="";
		}
		/**
		 * @author Vijay Chougule Date 02-10-2020 
		 * Des :- updated code time should be print from completion Time
		 * if you took time from date then getting issue time in other country for eg umas client
		 */
		String completedTime = service.getCompletedTime();
		logger.log(Level.SEVERE, "serviceCompleteDate "+serviceCompleteDate);
		logger.log(Level.SEVERE, "completionTime "+completedTime);

//		String completionTime=getServiceCompleteTime();
		logger.log(Level.SEVERE, "completionTime ");
		
		if(service.getStatus().equals(Service.SERVICESTATUSCOMPLETED)||service.getStatus().equals(Service.SERVICESTATUSTECHCOMPLETED)) {
			if(!serviceCompleteDate.equals("")) {
				Phrase completionDate=new Phrase("Completion Date & Time : "+serviceCompleteDate+" "+completedTime,font8);
				PdfPCell completionDatecell=new PdfPCell(completionDate);
				completionDatecell.setBorder(0);
				completionDatecell.setColspan(2);
				completionDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(completionDatecell);
			}else if(service.getServiceCompletionDate()!=null){
				Phrase completionDate=new Phrase("Completion Date : "+dateformat.format(service.getServiceCompletionDate()),font8);
				PdfPCell completionDatecell=new PdfPCell(completionDate);
				completionDatecell.setBorder(0);
				completionDatecell.setColspan(2);
				completionDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(completionDatecell);
			}
		}else if(service.getStatus().equals(Service.SERVICESTATUSCANCELLED)) {
			if(service.getCancellationDate()!=null) {
				Phrase completionDate=new Phrase("Cancellation Date : "+dateformat.format(service.getCancellationDate()),font8);
				PdfPCell completionDatecell=new PdfPCell(completionDate);
				completionDatecell.setBorder(0);
				completionDatecell.setColspan(2);
				completionDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(completionDatecell);
			}
		}else {
			if(service.getServiceTime()!=null) {
				Phrase completionDate=new Phrase("Service Date & Time : "+dateformat.format(service.getServiceDate())+" "+service.getServiceTime(),font8);
				PdfPCell completionDatecell=new PdfPCell(completionDate);
				completionDatecell.setBorder(0);
				completionDatecell.setColspan(2);
				completionDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(completionDatecell);
			}else {
				Phrase completionDate=new Phrase("Service Date: "+dateformat.format(service.getServiceDate()),font8);
				PdfPCell completionDatecell=new PdfPCell(completionDate);
				completionDatecell.setBorder(0);
				completionDatecell.setColspan(2);
				completionDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				serviceDetailTable.addCell(completionDatecell);
			}
		}
		
		
		
		/**
		 * @author Anil @since 16-09-2021
		 * Changing left side table content as per the envocare raised by Swati and Nitin Sir
		 */
		PdfPTable leftTbl=new PdfPTable(3);
		leftTbl.setWidthPercentage(100);
		
		leftTbl.addCell(pdf.getCell("Client Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(" : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		
		String custName="";
		if(cust!=null){
			logger.log(Level.SEVERE,"customer not null "+cust.getCount());
			if(cust.getServiceAddressName()!=null&&!cust.getServiceAddressName().equals("")) {
				custName=cust.getServiceAddressName();
			}else if (cust.getCustPrintableName() != null
					&& !cust.getCustPrintableName().equals("")) {
				custName = cust.getCustPrintableName().trim();
			}
			else if (cust.isCompany() == true && cust.getCompanyName() != null) {
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, comp.getCompanyId())){
					custName = cust.getCompanyName().trim();
				}
				else{
					custName = "M/S " + " " + cust.getCompanyName().trim();
				}
			}else if (cust.getSalutation() != null
					&& !cust.getSalutation().equals("")) {
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}
		if(custName!=null&!custName.equals("")) {
			leftTbl.addCell(pdf.getCell(custName, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);			
		}else {
			leftTbl.addCell(pdf.getCell(service.getPersonInfo().getFullName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		/**
		 * if service is having customer branch name then showing customer branch name also
		 */
		
		String branchname="";
		if(customerBranchDetails!=null)
		{	logger.log(Level.SEVERE,"customerBranchDetails not null");
			if(customerBranchDetails.getServiceAddressName()!=null&&!customerBranchDetails.getServiceAddressName().equals(""))
				branchname=customerBranchDetails.getServiceAddressName();
			else
				branchname=customerBranchDetails.getBusinessUnitName();
		}else {
			if(!customerBranchName.equals("")){			
				branchname=customerBranchName;
			}		
		}
		if(branchname!=null&&!branchname.equals("")) {
		leftTbl.addCell(pdf.getCell("Branch", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(" : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(branchname, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}

		String serviceAddress="";
		if(service.getAddress()!=null){
			serviceAddress=service.getAddress().getCompleteAddress();
		}
		leftTbl.addCell(pdf.getCell("Address", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(" : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(serviceAddress, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		leftTbl.addCell(pdf.getCell("Phone No.", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(" : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(service.getPersonInfo().getCellNumber()+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		if(service.getPersonInfo().getEmail()!=null&&!service.getPersonInfo().getEmail().equals("")){
			leftTbl.addCell(pdf.getCell("Email Id", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdf.getCell(" : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
			leftTbl.addCell(pdf.getCell(service.getPersonInfo().getEmail(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		}
		
		leftTbl.addCell(pdf.getCell("Service Name", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(" : ", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		leftTbl.addCell(pdf.getCell(service.getProductName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
		
		try {
			leftTbl.setWidths(new float[]{30,5,65});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		PdfPCell addcell=new PdfPCell(billingaddessTable);
//		addcell.setFixedHeight(90);
		PdfPCell addcell=new PdfPCell(leftTbl);
		
		PdfPCell prodcell=new PdfPCell(serviceDetailTable);
		
		
		//Ashwini Patil Date:14-06-2022 Lengthy addresses were not getting prited due to fiexd height.
		int horizontalTableCount=0;
		if(printPestTreatmentFlag||printMaterialOnSRCopyFlag)
			horizontalTableCount++;
		if(printFindingsOnSRCopyFlag)
			horizontalTableCount++;
		if(PC_SR_FINDINGS)
			horizontalTableCount++;
		if(printPhotoOnSRCopyFlag&&photoFlag)
			horizontalTableCount++;
//		if(printMaterialOnSRCopyFlag||printFindingsOnSRCopyFlag||printPhotoOnSRCopyFlag||photoFlag||printChecklistOnSRCopyFlag||printPestTreatmentFlag) {
		if(horizontalTableCount==4) {
			addcell.setFixedHeight(90);
			prodcell.setFixedHeight(90);
		}
	
		if(companyAsaletter){
			if(onlyForPsipl) {
				mainTable.addCell(pdf.getCell("Service Report", font14bold, Element.ALIGN_CENTER, 0, 2, 0)).setBorder(0);
				PdfPCell blankcel=pdf.getCell(" ", font14bold, Element.ALIGN_CENTER, 0, 2, 0);
				blankcel.setBorder(0);
				blankcel.setPaddingBottom(2);
				mainTable.addCell(blankcel);
			}else
				mainTable.addCell(pdf.getCell("Service Report", font10bold, Element.ALIGN_CENTER, 0, 2, 0)).setBorder(0);
			
		}
		
		mainTable.addCell(addcell);
		mainTable.addCell(prodcell);
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    
	

	private String getServiceDuration() {
		String serviceDuration = "";
		SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		Date reportedTime = null;
		if (service.getReportedDate_time() != null) {
			logger.log(Level.SEVERE, "Reported Timee " + service.getReportedDate_time());
			try {
				logger.log(Level.SEVERE, "reportedTime in iff " +  serverTimeSdf.parse(service.getReportedDate_time()));
				reportedTime = serverTimeSdf.parse(service.getReportedDate_time());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "reportedTime in iff " + reportedTime);
		}
		else {
			reportedTime = null;
		}

		Date CompletedTime = null;
		if (service.gettCompletedDate_time() != null) {
			logger.log(Level.SEVERE, "Completion Time" + service.gettCompletedDate_time());
			try {
				logger.log(Level.SEVERE, "CompletedTime in ifff " +  serverTimeSdf.parse(service.gettCompletedDate_time()));
				CompletedTime = serverTimeSdf.parse(service
						.gettCompletedDate_time());
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} else {
			CompletedTime = null;
		}

		long timeDiff = 0;
		if (CompletedTime != null && reportedTime != null) {
			timeDiff = CompletedTime.getTime() - reportedTime.getTime();
			logger.log(Level.SEVERE, "timeDiff 111 " + timeDiff);
		} else {
			timeDiff = 0;
		}
		long diffSec = timeDiff / 1000;
		
	    long hrs = timeDiff / (60 * 60 * 1000);
	    long min = diffSec / 60;
	    long sec = diffSec % 60;
	    
	    /**
	     * @author Anil @since 28-10-2021
	     * In Min section it was printing total minute 
	     * like 3:188:12 but once hours section come then minute can not ne 3 digit
	     * raised by UDS by Nithila
	     */
	    min = min / 60;
	    
	    logger.log(Level.SEVERE,"Updated Service Duration :  "+ hrs+":"+ min+":"+sec);
	    
	    String durMin="",durHrs="",durSec="";
	    
	    String hrsinString=String.valueOf(hrs);
	    int hrslength=hrsinString.length();
	    
	    if(hrslength==1){
	    	durHrs="0"+hrs;
	    }else{
	    	durHrs=hrs+"";
	    }
	    
	    String minInString=String.valueOf(min);
	    int minLength =minInString.length();
	    
	    if(minLength==1){
	    	durMin="0"+min;
	    }else{
	    	durMin=min+"";
	    }
	    
	    String secInString =String.valueOf(sec);
	    int seclength= secInString.length();
	    
	    if(seclength==1){
	    	durSec="0"+sec;
	    }else{
	    	durSec=sec+"";
	    }
	    
		serviceDuration = (durHrs+ ":"+durMin+ ":"+durSec);
		
		logger.log(Level.SEVERE, "Service Duration111 " + serviceDuration);
		return serviceDuration;

	}

	private String getServiceCompleteTime(){
		String servCompltionTime="";
		SimpleDateFormat serverTimeSdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SS");
		
		if(service.getTrackServiceTabledetails()!=null&&service.getTrackServiceTabledetails().size()!=0){
			for(TrackTableDetails tractTable:service.getTrackServiceTabledetails()){
				if(tractTable.getDate_time()!=null&&tractTable.getStatus().equalsIgnoreCase("TCompleted")){
				servCompltionTime=tractTable.getDate_time();
				}else{
				servCompltionTime="";
				}
			}
		}
		logger.log(Level.SEVERE, "Service Completion Time "+servCompltionTime);
		return  servCompltionTime;
	}




	private void createHeader() {
		
		/**
		 * @author Vijay Chougule Date:- 24-07-2020
		 * Des :- As per the requirement here must be print the Branch Address as per Nitin Sir
		 * if branch address is not defined then it will print Company address.
		 */
		if(branchDt!=null) {
		if(branchDt.getAddress()!=null){
			if(!branchDt.getAddress().getCompleteAddress().equals("")){
				checkBranchdata = true;
			}
			else{
				checkBranchdata = false;

			}
		}
		}else
			checkBranchdata = false;
		
		
		PdfPTable headerTAB=new PdfPTable(3);
		headerTAB.setWidthPercentage(100);
		try {
			headerTAB.setWidths(new float[]{20,60,20});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable headertable=new PdfPTable(1);
		headertable.setWidthPercentage(100);
		headertable.setSpacingAfter(10);
		
		//Set Company Name
		String businessunit=null;
//		if(checkBranchdata) {
//			businessunit=branchDt.getBusinessUnitName();
//		}
//		else {
			businessunit=comp.getBusinessUnitName();
//		}
		
		Phrase companyNameph = new Phrase(businessunit.toUpperCase(),
				font14bold);
		PdfPCell companyNameCell = new PdfPCell(companyNameph);
		companyNameCell.setBorder(0);
		companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyNameCell);
		
		//Set Address of Company
		String address = "";
		if(checkBranchdata) {
			address=branchDt.getAddress().getCompleteAddress();
		} else {
			address=comp.getAddress().getCompleteAddress();
		}
		
		Phrase companyAddph = new Phrase(address, font9);
		
		PdfPCell companyAddCell = new PdfPCell(companyAddph);
		companyAddCell.setBorder(0);
		companyAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyAddCell);

		//Email ID
		String emailid="";
		if(checkBranchdata) {
			emailid = branchDt.getEmail();
		} else {
				emailid = comp.getEmail();
				System.out.println("server method " + emailid);
			} 
		
		Phrase companyemailph = new Phrase("E-Mail : "+emailid, font9);
		PdfPCell companyemailCell = new PdfPCell(companyemailph);
		companyemailCell.setBorder(0);
		companyemailCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyemailCell);
		
		
		//WebSite
		String website="";
		if(comp.getWebsite()==null || comp.getWebsite().equals(""))
		{
			website="";
		}
		else
		{
			
			website="Website : "+comp.getWebsite();
		}
		Phrase companyWebph = new Phrase(website, font9);
		PdfPCell companyWebCell = new PdfPCell(companyWebph);
		companyWebCell.setBorder(0);
		companyWebCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companyWebCell);
		
		
		//Contact
       String contactinfo="";
		if(checkBranchdata) {
			if (branchDt.getCellNumber1() != null && branchDt.getCellNumber1() != 0) {
				System.out.println("pn11");
				contactinfo =  branchDt.getCellNumber1() + "";
			}
			if (branchDt.getCellNumber2() != null && branchDt.getCellNumber2() != 0) {
				if (!contactinfo.trim().isEmpty()) {
					contactinfo = contactinfo + " / " + branchDt.getCellNumber2() + "";
				} else {
					contactinfo = branchDt.getCellNumber2()
							+ "";
				}
				System.out.println("pn33" + contactinfo);
			}
			if (branchDt.getLandline() != 0 && branchDt.getLandline() != null) {
				if (!contactinfo.trim().isEmpty()) {
					contactinfo = contactinfo + " / " 
							+ branchDt.getLandline() + "";
				} else {
					contactinfo =  branchDt.getLandline() + "";
				}
				System.out.println("pn44" + contactinfo);
			}
		} else {
			if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
				contactinfo = comp.getCountryCode() + comp.getCellNumber1() + "";
			}
			if (comp.getCellNumber2() != null && comp.getCellNumber2() != 0) {
				if (!contactinfo.trim().isEmpty()) {
					contactinfo = contactinfo + " / " + comp.getCountryCode()
							+ comp.getCellNumber2() + "";
				} else {
					contactinfo = comp.getCountryCode() + comp.getCellNumber2()
							+ "";
				}
				System.out.println("pn33" + contactinfo);
			}
			if (comp.getLandline() != 0 && comp.getLandline() != null) {
				if (!contactinfo.trim().isEmpty()) {
					contactinfo = contactinfo + " / " + comp.getStateCode()
							+ comp.getLandline() + "";
				} else {
					contactinfo = comp.getStateCode() + comp.getLandline() + "";
				}
				System.out.println("pn44" + contactinfo);
			}
		}
		Phrase companymobph = new Phrase("Phone :  "+contactinfo,font9);
		PdfPCell companymobCell = new PdfPCell(companymobph);
		companymobCell.setBorder(0);
		companymobCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headertable.addCell(companymobCell);
		
		PdfPTable logoTable = new PdfPTable(2);
		logoTable.setSpacingAfter(5);
		logoTable.setWidthPercentage(100f);
		try {
			logoTable.setWidths(new float[]{8,92});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		DocumentUpload logodocument ;
		//Ashwini Patil Date:24-12-2024 commented checkbranchdata condition as its logic is wrong. it is picking logo from branch even if branchascompany is inactive 
//		if(checkBranchdata) {
//			logger.log(Level.SEVERE, "in if(checkBranchdata) "+service.getBranch());
//			
//			logodocument = branchDt.getLogo();
//		} else {
			logodocument =comp.getLogo();
//		}
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2=null;
		if(logodocument!=null){
		try {
			image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,765f);	
//			doc.add(image2);
			
			imageSignCell = new PdfPCell();
			imageSignCell.addElement(image2);
			imageSignCell.setImage(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}
		}
		
		Phrase blank =new Phrase(" ");
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);
		if(imageSignCell != null)
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(imageSignCell);
		}
		else
		{
			logoTable.addCell(blankCell);
			logoTable.addCell(blankCell);
		}

		PdfPTable titletab = new PdfPTable(1);
		titletab.setWidthPercentage(100f);
		titletab.setSpacingAfter(5);
		
		Phrase title =new Phrase("",font9bold);
		PdfPCell titlecell=new PdfPCell(title);
		titlecell.setBorder(0);
		titlecell.setPaddingRight(3);
		titlecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		titletab.addCell(titlecell);
		
		
		PdfPCell logocell=new PdfPCell(logoTable);
		logocell.setBorderWidthRight(0);
		
		PdfPCell headerCell=new PdfPCell(headertable);
		headerCell.setBorderWidthLeft(0);
		headerCell.setBorderWidthRight(0);
		
		PdfPCell titlcell=new PdfPCell(titletab);
		titlcell.setBorderWidthLeft(0);
		
		headerTAB.addCell(logocell);
		headerTAB.addCell(headerCell);
		headerTAB.addCell(titlcell);
		
		
		
		
		
		
		try {
			document.add(headerTAB);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable headertab2=new PdfPTable(3);
		headertab2.setWidthPercentage(100);
		try {
			headertab2.setWidths(new float[]{33,33,33});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//By: Ashwini Patil To Print Contract Id in header on MultipleSrCopy pdf
		if(isCalledFromContract)
		{
			Phrase custid=new Phrase("Contract ID : "+service.getContractCount(),font8);
			PdfPCell custidcell=new PdfPCell(custid);
			custidcell.setBorderWidthRight(0);
			custidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custidcell.setPaddingBottom(5);
			headertab2.addCell(custidcell);
			
		}
		else {
			Phrase custid=new Phrase("Customer ID : "+service.getCustomerId(),font8);
			PdfPCell custidcell=new PdfPCell(custid);
			custidcell.setBorderWidthRight(0);
			custidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custidcell.setPaddingBottom(5);
			headertab2.addCell(custidcell);
		}
		Phrase servRecord;
		if(reportName.equals("checklist")) {
			servRecord=new Phrase("Checklist Report",font8bold);
			
		}else {
			if(onlyForPestOCop)
				servRecord=new Phrase("SERVICE REPORT",font8bold);
			else
				servRecord=new Phrase("SERVICE RECORD",font8bold);
			
		}
		PdfPCell servRecordcell=new PdfPCell(servRecord);
		servRecordcell.setBorderWidthRight(0);
		servRecordcell.setBorderWidthLeft(0);
		servRecordcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		servRecordcell.setPaddingBottom(5);
		headertab2.addCell(servRecordcell);		
		
		//By: Ashwini Patil To Print Contract duration in header on MultipleSrCopy pdf		
		if(isCalledFromContract)
		{
			Contract con = ofy().load().type(Contract.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", service.getContractCount()).first().now();
			Phrase date=new Phrase("Contract Duration: "+fmt.format(con.getStartDate())+" - "+fmt.format(con.getEndDate()),font8);
			PdfPCell datecell=new PdfPCell(date);
			datecell.setBorderWidthLeft(0);
			datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			datecell.setPaddingBottom(5);
			headertab2.addCell(datecell);			
		}else {
			Phrase date=new Phrase("Date : "+fmt.format(service.getServiceDate()),font8);
			PdfPCell datecell=new PdfPCell(date);
			datecell.setBorderWidthLeft(0);
			datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			datecell.setPaddingBottom(5);
			headertab2.addCell(datecell);		
		}
		
		
		try {
			document.add(headertab2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

    private void createFooter() {
    	
    	PdfPTable feedbackTable=new PdfPTable(1);
    	feedbackTable.setWidthPercentage(100);
    	
    	String feed ="";
    	if(service.getCustomerFeedback()!=null){
    		feed=service.getCustomerFeedback();
    	}else{
    		feed="";
    	}
    	
		Phrase feedback=new Phrase("Feedback : "+feed,font8);
		PdfPCell feedbackcell=new PdfPCell(feedback);
//		feedbackcell.setBorderWidthLeft(0);
		feedbackcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		feedbackcell.setPaddingBottom(5);
		feedbackTable.addCell(feedbackcell);
    	
		
		
		DocumentUpload digitalDocument = service.getCustomerSignature();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		imageSignCell = null;
		Image image2 = null;
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			
//			image2 = Image.getInstance(new URL(hostUrl+ digitalDocument.getUrl()));
			try{
				image2 = Image
						.getInstance(new URL(hostUrl + service.getCustomerSignature().getUrl()));
				logger.log(Level.SEVERE,"screen" + image2+"");
				}catch(Exception e){
						
						image2 = Image
								.getInstance(new URL(service.getCustomerSignature().getUrl())); 
						logger.log(Level.SEVERE, "Android " +image2+"");
				}
			
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
//			imageSignCell.setBorder(0);
			imageSignCell.setBorderWidthBottom(0);
			imageSignCell.setBorderWidthTop(0);
//			imageSignCell.setFixedHeight(15);
			imageSignCell.setPaddingTop(2);
			imageSignCell.setPaddingRight(2);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (imageSignCell != null) {
			feedbackTable.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorderWidthBottom(0);
			blank1Cell.setBorderWidthTop(0);
			feedbackTable.addCell(blank1Cell);
			feedbackTable.addCell(blank1Cell);
			feedbackTable.addCell(blank1Cell);
			feedbackTable.addCell(blank1Cell);

		}
		
		
		
		
		
		
		
		
		
		
		String signPart="";
		if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")&&!service.getCustomerSignNumber().equals("")&&service.getCustomerSignNumber()!=null){
			logger.log(Level.SEVERE,"INSIDE IF 1");
			signPart=service.getCustomerSignName()+"/"+service.getCustomerSignNumber();
		}else if(service.getCustomerSignName()!=null&&service.getCustomerSignNumber().equals("")){
			logger.log(Level.SEVERE,"INSIDE IF 2");
			signPart=service.getCustomerSignName();
		}else if(service.getCustomerSignNumber()!=null&&service.getCustomerSignName().equals("")){
			logger.log(Level.SEVERE,"INSIDE IF 3");
			signPart=service.getCustomerSignNumber();
		}
			else{
			logger.log(Level.SEVERE,"INSIDE ELSE");
			signPart="";
		}
		
		Phrase signPerson=new Phrase(signPart,font8);
		PdfPCell signPersoncell=new PdfPCell(signPerson);
//		signPersoncell.setBorder(0);
		signPersoncell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		signPersoncell.setPaddingTop(2);
		signPersoncell.setPaddingBottom(3);
		signPersoncell.setPaddingRight(10);
		signPersoncell.setBorderWidthBottom(0);
		signPersoncell.setBorderWidthTop(0);
		feedbackTable.addCell(signPersoncell);
		
     /** @Sheetal:01-02-2022
		 Des : Renaming Authorised Signatory as Customer Signature, requirement raised by UDS Water **/

//		Phrase auth=new Phrase("Authorised Signatory",font8bold);
		Phrase auth=new Phrase("Customer Signature",font8bold);
		PdfPCell authcell=new PdfPCell(auth);
		authcell.setBorderWidthTop(0);
		authcell.setPaddingTop(3);
		authcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		authcell.setPaddingRight(10);
		feedbackTable.addCell(authcell);
		
    	
		Phrase remark=new Phrase("Remark : "+service.getServiceCompleteRemark(),font8bold);
		PdfPCell remarkcell=new PdfPCell(remark);
//		remarkcell.setBorderWidthLeft(0);
		remarkcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		remarkcell.setPaddingBottom(5);
		feedbackTable.addCell(remarkcell);
    	
		String contractDate="";
		if(service.getContractEndDate()!=null){
			contractDate=(fmt.format(service.getContractEndDate()));
		}else{
			contractDate="";
		}
		
		Phrase conExpDate=new Phrase("Your Contract is Expiring on : "+contractDate,font8);
		PdfPCell conExpDatecell=new PdfPCell(conExpDate);
//		conExpDatecell.setBorderWidthLeft(0);
		conExpDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		conExpDatecell.setPaddingBottom(5);
		feedbackTable.addCell(conExpDatecell);
		
		try {
		document.add(feedbackTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    	
    	
	}
    
    
    
    
    
    /**
     * @author Anil
     * @since 24-06-2020
     */
    public PdfPTable createFindingTable(){
    	PdfPTable tbl = new PdfPTable(3);
		tbl.setWidthPercentage(100);
		try {
			/**
			 * @author Priyanka Bhagwat Date:- 4-11-2020
			 * Des :- As per the requirement Count column in service finding section in SR copy should be small.
			 */
			tbl.setWidths(new float[]{39.38f,20.33f,39.38f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		
		/**
		 * @author Anil @since 28-10-2021
		 * For UDS we need to change some labels for finding tables 
		 * Also need to hide Finding heading
		 */
		
		if(PC_GenericSrCopy){
			tbl.addCell(pdf.getCell("Product Item", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdf.getCell("Numbers", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			tbl.addCell(pdf.getCell("Location found", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		}else{
			if(onlyForPsipl)
				tbl.addCell(pdf.getCell("Infestation Level", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
			else
				tbl.addCell(pdf.getCell("FINDINGS", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
			
			if(onlyForPsipl)
				tbl.addCell(pdf.getCell("Pest", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			else if(onlyForPestOCop)
				tbl.addCell(pdf.getCell("Description", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			else
				tbl.addCell(pdf.getCell("Insect Type", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			if(onlyForPsipl)	
				tbl.addCell(pdf.getCell("Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			else
				tbl.addCell(pdf.getCell("Numbers", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
			
			tbl.addCell(pdf.getCell("Location found", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		}
		
		if(service.getCatchtrapList()!=null&&service.getCatchtrapList().size()!=0){
			
			int totalCount=service.getCatchtrapList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			logger.log(Level.SEVERE,"createFindingTable totalCount "+totalCount+" remainingRow "+remainingRow);
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					CatchTraps finding=service.getCatchtrapList().get(i);
					tbl.addCell(pdf.getCell(finding.getPestName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
					if(finding.getCatchLevel()!=null&&!finding.getCatchLevel().equals("")) //Ashwini Patil Date:14-10-2024 for psipl
						tbl.addCell(pdf.getCell(finding.getCatchLevel(), font8, Element.ALIGN_RIGHT, 0, 0, 0));
					else
						tbl.addCell(pdf.getCell(finding.getCount()+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					
					tbl.addCell(pdf.getCell(finding.getLocation(), font8, Element.ALIGN_LEFT, 0, 0, 0));
				}
				
				for(int i=0;i<remainingRow;i++){
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
				}
			}else{
				for(int i=0;i<maxRow;i++){
					CatchTraps finding=service.getCatchtrapList().get(i);
					tbl.addCell(pdf.getCell(finding.getPestName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
					if(finding.getCatchLevel()!=null&&!finding.getCatchLevel().equals("")) //Ashwini Patil Date:14-10-2024 for psipl
						tbl.addCell(pdf.getCell(finding.getCatchLevel(), font8, Element.ALIGN_RIGHT, 0, 0, 0));
					
					else
						tbl.addCell(pdf.getCell(finding.getCount()+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdf.getCell(finding.getLocation(), font8, Element.ALIGN_LEFT, 0, 0, 0));
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0));
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
			}
		}
		
		return tbl;
    }
    
    public PdfPTable createMaterialTable(){
    	PdfPTable tbl = new PdfPTable(3);
		tbl.setWidthPercentage(100);
		
		try {
			tbl.setWidths(new float[]{53.33f,23.33f,23.33f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Chemical Used", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
		else
			tbl.addCell(pdf.getCell("MATERIAL", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
		
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Product Name", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else
			tbl.addCell(pdf.getCell("Name", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		tbl.addCell(pdf.getCell("Quantity", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Unit", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		
		if(project!=null&&project.getProdDetailsList()!=null&&project.getProdDetailsList().size()!=0){
			
			int totalCount=project.getProdDetailsList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			logger.log(Level.SEVERE,"createMaterialTable totalCount "+totalCount+" remainingRow "+remainingRow);
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					tbl.addCell(pdf.getCell(material.getName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						tbl.addCell(pdf.getCell((material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					}else{
						tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					}
//					tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdf.getCell(material.getUnit()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
				}
				
				for(int i=0;i<remainingRow;i++){
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
				}
			}else{
				for(int i=0;i<maxRow;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					tbl.addCell(pdf.getCell(material.getName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						tbl.addCell(pdf.getCell((material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					}else{
						tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					}
//					tbl.addCell(pdf.getCell((material.getQuantity()-material.getReturnQuantity())+"", font8, Element.ALIGN_RIGHT, 0, 0, 0));
					tbl.addCell(pdf.getCell(material.getUnit()+"", font8, Element.ALIGN_CENTER, 0, 0, 0));
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_RIGHT, 0, 0, 0));
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_CENTER, 0, 0, 0));
			}
		}
		
		return tbl;
    }
    
    public PdfPTable createPhotosTable(){
    	PdfPTable tbl = new PdfPTable(4);
		tbl.setWidthPercentage(100);
		
		tbl.addCell(pdf.getCell("PHOTOS", font9bold, Element.ALIGN_CENTER, 0, 4, 0)).setPaddingBottom(5);
		
		/**
		 * @author Priyanka Bhagwat Date:- 4-11-2020
		 * Des :- As per the requirement Before & After the titles to be removed from photo section of SR copy. 
		 */
		
		tbl.addCell(pdf.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
		tbl.addCell(pdf.getCell(" ", font8bold, Element.ALIGN_CENTER, 0, 2, 0));

		if(service.getServiceImage1()!=null&&!service.getServiceImage1().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage1(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		
		if(service.getServiceImage2()!=null&&!service.getServiceImage2().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage2(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		
		if(service.getServiceImage3()!=null&&!service.getServiceImage3().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage3(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		
		if(service.getServiceImage4()!=null&&!service.getServiceImage4().getUrl().equals("")){
			tbl.addCell(pdf.getPhotoCell(service.getServiceImage4(), 20f, 120, 0, 0,0));
		}else{
			tbl.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
		}
		return tbl;
    }
    
    /**
     * @author Anil
     * @since 25-06-2020
     */
    public PdfPTable createFindingAndMaterialTable(){
    	PdfPTable tbl = new PdfPTable(6);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[]{16.66f,10.66f,22.66f,28.66f,10.66f,10.66f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Infestation Level", font9bold, Element.ALIGN_CENTER, 0, 3, 0));
		else
			tbl.addCell(pdf.getCell("Findings", font9bold, Element.ALIGN_CENTER, 0, 3, 0));
		
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Chemical Used", font9bold, Element.ALIGN_CENTER, 0, 3, 0));
		else
			tbl.addCell(pdf.getCell("Material", font9bold, Element.ALIGN_CENTER, 0, 3, 0));
		
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Pest", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else
			tbl.addCell(pdf.getCell("Insects", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Level", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else
			tbl.addCell(pdf.getCell("Numbers", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		tbl.addCell(pdf.getCell("Location", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Product Name", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else
			tbl.addCell(pdf.getCell("Name", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		tbl.addCell(pdf.getCell("Quantity", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Unit", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		HashMap<Integer,Vector<String>> dataMap=new HashMap<Integer,Vector<String>>();
		Integer row=1;
		if(service.getCatchtrapList()!=null&&service.getCatchtrapList().size()!=0){
			
			int totalCount=service.getCatchtrapList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					CatchTraps finding=service.getCatchtrapList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(finding.getPestName());
					if(finding.getCatchLevel()!=null&&!finding.getCatchLevel().equals("")) //Ashwini Patil Date:14-10-2024 for psipl
						vecLis.add(finding.getCatchLevel()+"");
					else
						vecLis.add(finding.getCount()+"");
					vecLis.add(finding.getLocation());
					dataMap.put(row, vecLis);
					row++;
				}
				
				for(int i=0;i<remainingRow;i++){
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(" ");
					vecLis.add(" ");
					vecLis.add(" ");
					dataMap.put(row, vecLis);
					row++;
				}
			}else{
				for(int i=0;i<maxRow;i++){
					CatchTraps finding=service.getCatchtrapList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(finding.getPestName());
					if(finding.getCatchLevel()!=null&&!finding.getCatchLevel().equals("")) //Ashwini Patil Date:14-10-2024 for psipl
						vecLis.add(finding.getCatchLevel()+"");
					else
						vecLis.add(finding.getCount()+"");
					vecLis.add(finding.getLocation());
					dataMap.put(row, vecLis);
					row++;
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				Vector<String> vecLis=new Vector<String>();
				vecLis.add(" ");
				vecLis.add(" ");
				vecLis.add(" ");
				dataMap.put(row, vecLis);
				row++;
			}
		}
		
		
		row=1;
		if(project!=null&&project.getProdDetailsList()!=null&&project.getProdDetailsList().size()!=0){
			
			int totalCount=project.getProdDetailsList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			logger.log(Level.SEVERE,"createMaterialTable totalCount "+totalCount+" remainingRow "+remainingRow);
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					dataMap.get(row).add(material.getName());
					
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						dataMap.get(row).add((material.getReturnQuantity())+" "+material.getUnit());
					}else{
						dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+"");
					}
//					dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+"");
					dataMap.get(row).add(material.getUnit());
					row++;
				}
				
				for(int i=0;i<remainingRow;i++){
					dataMap.get(row).add(" ");
					dataMap.get(row).add(" ");
					dataMap.get(row).add(" ");
					row++;
				}
			}else{
				for(int i=0;i<maxRow;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					dataMap.get(row).add(material.getName());
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						dataMap.get(row).add((material.getReturnQuantity())+" "+material.getUnit());
					}else{
						dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+" ");
					}
//					dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+"");
					dataMap.get(row).add(material.getUnit());
					row++;
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				dataMap.get(row).add(" ");
				dataMap.get(row).add(" ");
				dataMap.get(row).add(" ");
				row++;
			}
		}
		
		
		for (Map.Entry<Integer,Vector<String>> entry : dataMap.entrySet()){  
			logger.log(Level.SEVERE,"Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
            Vector<String> list =entry.getValue();
            tbl.addCell(pdf.getCell(list.get(0), font8, Element.ALIGN_LEFT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(1), font8, Element.ALIGN_RIGHT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(2), font8, Element.ALIGN_LEFT, 0, 0, 0));
			
			tbl.addCell(pdf.getCell(list.get(3), font8, Element.ALIGN_LEFT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(4), font8, Element.ALIGN_RIGHT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(5), font8, Element.ALIGN_CENTER, 0, 0, 0));
		}
		return tbl;
    
    }
 
    public PdfPTable createFooterTable(){
    	
    	/**
    	 * Technician and Customer Remark
    	 */
    	String techRemark =" ";
    	if(service.getTechnicianRemark()!=null&&!service.getTechnicianRemark().equals("")){
    		techRemark=service.getTechnicianRemark();
    	}else{
    		techRemark=" ";
    	}
    	String custRemark ="";
    	if(service.getCustomerFeedback()!=null&&!service.getCustomerFeedback().equals("")){
    		custRemark="Rating : "+service.getCustomerFeedback()+"\n"; //Zoho rating or Pedio Rating mapped here.
    	}else{
    		custRemark="";
    	}
    	
    	if(service.getServiceCompleteRemark()!=null&&!service.getServiceCompleteRemark().equals("")){
    		if(!custRemark.equals("")){
    			
    			custRemark=custRemark+"Remark : "+service.getServiceCompleteRemark();
    		}else{
    			custRemark=service.getServiceCompleteRemark();
    		}
    	}
    	
    	PdfPCell techTopCell=null;
    	if(onlyForPsipl)
    		techTopCell=pdf.getCell("Observation", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
    	else
    		techTopCell=pdf.getCell("Technician Remark", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		
    	techTopCell.setBorderWidthBottom(0);
		PdfPCell techBottomCell=pdf.getCell(techRemark,font8, Element.ALIGN_LEFT, 4, 0, 0);
		techBottomCell.setBorderWidthTop(0);
		if(printTechnicianRemarkFlag&&printCustomerRemarkFlag){
//			techBottomCell.setRowspan(5);
			techBottomCell.setRowspan(2);
		}else{
//			techBottomCell.setRowspan(11);
			techBottomCell.setRowspan(5);
		}
		PdfPCell custTopCell=null;
		if(onlyForPsipl)
			custTopCell=pdf.getCell("Customer Remark", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		else
			custTopCell=pdf.getCell("Customer Remark & Rating", font8bold, Element.ALIGN_LEFT, 0, 0, 0);
		
		custTopCell.setBorderWidthBottom(0);
		PdfPCell custBottomCell=pdf.getCell(custRemark, font8, Element.ALIGN_LEFT, 4, 0, 0);
		custBottomCell.setBorderWidthTop(0);
		if(printTechnicianRemarkFlag&&printCustomerRemarkFlag){
//			custBottomCell.setRowspan(5);
			custBottomCell.setRowspan(2);
			logger.log(Level.SEVERE,"custBottomCell.setRowspan(2)");
		}else{
//			custBottomCell.setRowspan(11);
			custBottomCell.setRowspan(5);
			logger.log(Level.SEVERE,"custBottomCell.setRowspan(5)");
		}
		
		/**
		 * Check List Table
		 */
		
		PdfPCell chkLisHeadCell=pdf.getCell("Checklist", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
		
		
		
		/**
		 * Signature Table
		 */
		/**
		 * @author Anil
		 * @since 06-08-2020
		 */
		String signPart="";	
		if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")&&service.getCustomerSignNumber()!=null&&!service.getCustomerSignNumber().equals("")){
			logger.log(Level.SEVERE,"INSIDE IF 1 "+service.getCustomerSignName());
			signPart="("+service.getCustomerSignName()+"/"+service.getCustomerSignNumber()+")";
		}else if(service.getCustomerSignName()!=null&&!service.getCustomerSignName().equals("")){
			signPart="("+service.getCustomerSignName()+")";
		}else if(service.getCustomerSignNumber()!=null&&!service.getCustomerSignNumber().equals("")){
			signPart="("+service.getCustomerSignNumber()+")";
		}else{
			signPart="";
		}
		
		if(!signPart.equals("")){
			signPart=signPart+"\n";
		}
		/** @Sheetal:01-02-2022
		 Des : Renaming Authorised Signatory as Customer Signature, requirement raised by UDS Water **/
		
//	    Phrase auth=new Phrase(signPart+"Authorised Signatory",font8bold);
		Phrase auth=new Phrase(signPart+"Customer Signature",font8bold);
		PdfPCell authcell=new PdfPCell(auth);
		authcell.setBorderWidthTop(0);
//		authcell.setPaddingRight(40);
		authcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell imageSignCell = null;
		Image image2 = null;
		
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System.getProperty("com.google.appengine.application.id");
			String version = System.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		try {
			if(service.getCustomerSignature()!=null&&!service.getCustomerSignature().getUrl().equals("")){
				try {
					image2 = Image.getInstance(new URL(hostUrl+ service.getCustomerSignature().getUrl()));
					logger.log(Level.SEVERE, "screen" + image2 + "");
				} catch (Exception e) {
					image2 = Image.getInstance(new URL(service.getCustomerSignature().getUrl()));
					logger.log(Level.SEVERE, "Android " + image2 + "");
				}
			}

			if(image2!=null){
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(100f);
				imageSignCell = new PdfPCell(image2);
			}else{
				imageSignCell = new PdfPCell(new Phrase(" "));
//				authcell.setPaddingTop(110);
//				authcell.setPaddingTop(70);
			}
			
			imageSignCell.setBorderWidthBottom(0);
//			imageSignCell.setPaddingTop(100);
			imageSignCell.setPaddingTop(50);
			imageSignCell.setPaddingRight(35);
//			imageSignCell.setPaddingBottom(20);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			imageSignCell.setRowspan(11);
			imageSignCell.setRowspan(5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
    	
    	PdfPTable footerTbl=null;
		if((printTechnicianRemarkFlag||printCustomerRemarkFlag)&&printChecklistOnSRCopyFlag){
			footerTbl = new PdfPTable(3);
			footerTbl.setWidthPercentage(100);
			try {
				footerTbl.setWidths(new float[]{33.33f,33.33f,33.33f});
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
		}else if(printChecklistOnSRCopyFlag){
			footerTbl = new PdfPTable(2);
			footerTbl.setWidthPercentage(100);
			try {
				footerTbl.setWidths(new float[]{66.66f,33.33f});
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
		}else if(printTechnicianRemarkFlag||printCustomerRemarkFlag){
			footerTbl = new PdfPTable(2);
			footerTbl.setWidthPercentage(100);
			try {
				footerTbl.setWidths(new float[]{66.66f,33.33f});
			} catch (DocumentException e2) {
				e2.printStackTrace();
			}
		}else{
			footerTbl = new PdfPTable(1);
			footerTbl.setWidthPercentage(100);
		}
		
		PdfPCell blankCell=new PdfPCell(new Phrase(" "));
		
		if((printTechnicianRemarkFlag||printCustomerRemarkFlag)&&printChecklistOnSRCopyFlag){
			
			if(printTechnicianRemarkFlag&&printCustomerRemarkFlag){
				logger.log(Level.SEVERE,"in first footer tab condition");
				footerTbl.addCell(techTopCell);				// 1    1st col
				footerTbl.addCell(chkLisHeadCell);			// 1	2nd col
				footerTbl.addCell(imageSignCell);			// 5	3rd col
				
				footerTbl.addCell(techBottomCell);			// 2	1st col  3
				addCheckListDataToTbl(footerTbl,2);			// 2	2nd col  3
															//				 5
				
				footerTbl.addCell(custTopCell);				// 1	1st col  4
				addCheckListDataToTbl(footerTbl,1);			// 1	2nd col  4
															//               5
				
				footerTbl.addCell(custBottomCell);			// 2	1st col	 6
				addCheckListDataToTbl(footerTbl,2);			// 1	2nd col	 5
															//				 5
							
//				addCheckListDataToTbl(footerTbl,1);			
//				footerTbl.addCell(blankCell);				// 1    2nd col	 6	
//				authcell.setPaddingTop(0);
				footerTbl.addCell(authcell);				// 1	3rd col  6
				
				
			}else if(printTechnicianRemarkFlag){
				footerTbl.addCell(techTopCell);				// 1  1st 	1
				footerTbl.addCell(chkLisHeadCell);			// 1  2nd 	1
				footerTbl.addCell(imageSignCell);			// 5  		5
				
				footerTbl.addCell(techBottomCell);			// 5  1st	6
				addCheckListDataToTbl(footerTbl,0);			// 5  2nd	6
															//			5
//				footerTbl.addCell(blankCell);				// 1   2nd  6
				footerTbl.addCell(authcell);				// 1   3rd  6
			}else{
				footerTbl.addCell(custTopCell);
				footerTbl.addCell(chkLisHeadCell);
				footerTbl.addCell(imageSignCell);
				
				footerTbl.addCell(custBottomCell);
				addCheckListDataToTbl(footerTbl,0);
				
//				footerTbl.addCell(blankCell);
				footerTbl.addCell(authcell);
			}
			
		}else if(printTechnicianRemarkFlag||printCustomerRemarkFlag){
			
			if(printTechnicianRemarkFlag&&printCustomerRemarkFlag){
				footerTbl.addCell(techTopCell);			//1st col 1   1
				footerTbl.addCell(imageSignCell);		//2nd col 2   5
				footerTbl.addCell(techBottomCell);		//1st col 2	  3
//				techBottomCell.setPaddingTop(55);
//				techBottomCell.setPaddingTop(40);
				footerTbl.addCell(custTopCell);			//1st col 1	  4   
				footerTbl.addCell(custBottomCell);		//1st col 2   6
//				authcell.setPaddingTop(110);
				footerTbl.addCell(authcell);			//2nd col 1   6
				
			}else if(printTechnicianRemarkFlag){
				footerTbl.addCell(techTopCell);			//1st col 1		1
				footerTbl.addCell(imageSignCell);		//2nd col 5		5
				
				footerTbl.addCell(techBottomCell);		//1st col 5		6
//				authcell.setPaddingTop(110);
				footerTbl.addCell(authcell);			//2nd col 1     6
			}else{
				footerTbl.addCell(custTopCell);			//1st col 1		1
				footerTbl.addCell(imageSignCell);		//2nd col 5		6
				
				footerTbl.addCell(custBottomCell);		//1st col 5     6
//				authcell.setPaddingTop(110);
				footerTbl.addCell(authcell);			//2nd col 1		6
			}
			
		}else if(printChecklistOnSRCopyFlag){
			footerTbl.addCell(chkLisHeadCell); 		// 1st col 1	1
			footerTbl.addCell(imageSignCell);  		// 2nd col 5	5
			addCheckListDataToTbl(footerTbl,0);  	// 1st col 5	6
//			footerTbl.addCell(new Phrase(" "));		
			footerTbl.addCell(authcell);	   		// 2nd col 1	6
			
		}else{
			footerTbl.addCell(imageSignCell);
//			authcell.setPaddingTop(110);
			footerTbl.addCell(authcell);
		}
		
		
		return footerTbl;
    }
    
    
    public PdfPTable addCheckListDataToTbl(PdfPTable tbl, int limit){
    	int ctr=0;
    	if(service.getCheckList()!=null&&service.getCheckList().size()!=0){
			int totalCount=service.getCheckList().size();
			int remainingRow=0;
			if(totalCount<maxRowForCheckList){
				remainingRow=maxRowForCheckList-totalCount;
			}
			if(remainingRow>0){
				boolean updateFlag=false;
				for(int i=clCount;i<totalCount;i++){
					CatchTraps finding=service.getCheckList().get(i);
					tbl.addCell(pdf.getCell(finding.getPestName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
					if(limit>0&&limit==(ctr+1)){
						clCount=i+1;
						return tbl;
					}
					clCount++;
					ctr++;
				}
				for(int i=0;i<remainingRow;i++){
					tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
					if(limit>0&&limit==(ctr+1)){
						clCount=clCount+1;//i+1 Ashwini Patil Date:20-1-2023 When checklist contains 2 items. last item was getting printed twice.
						return tbl;
					}
					clCount++;
					ctr++;
				}
			}else{
				for(int i=clCount;i<maxRowForCheckList;i++){
					CatchTraps finding=service.getCheckList().get(i);
					tbl.addCell(pdf.getCell(finding.getPestName(), font8, Element.ALIGN_LEFT, 0, 0, 0));
					if(limit>0&&limit==(ctr+1)){
						clCount=i+1;
						return tbl;
					}
					clCount++;
					ctr++;
				}
			}
		}else{
			for(int i=clCount;i<maxRowForCheckList;i++){
				tbl.addCell(pdf.getCell(" ", font8, Element.ALIGN_LEFT, 0, 0, 0));
				if(limit>0&&limit==(ctr+1)){
					clCount=i+1;
					return tbl;
				}
				clCount++;
				ctr++;
			}
		}
    	return tbl;
    }
    
    
    
    public PdfPTable createPestTreatmentTable(){
//    	servRecordcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		servRecordcell.setPaddingBottom(5);
    	
    	/*
    	 * Ashwini Patil
    	 * Date: 11-12-2023
    	 * Commented 5 column code and adding3 column code as material getting printed twise on srcopy if printPestTreatmentFlagis active as well as printMaterialOnSRCopyFlag is active
    	 * if client do not want to print material, they disable printMaterialOnSRCopyFlag but still material gets displayedin pesttreatment table
    	 * so as pernitin sir's instruction, removing material from pesttreatment table
    	 * 
    	 */
//    	PdfPTable tbl = new PdfPTable(5);
//		tbl.setWidthPercentage(100);
//		try {
//			tbl.setWidths(new float[]{24f,18f,18f,24f,16f});
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
    	
    	PdfPTable tbl = new PdfPTable(3);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[]{34f,33f,33f});
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("SERVICE DETAILS", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
		else
			tbl.addCell(pdf.getCell("PEST TREATMENTS", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
		
		tbl.addCell(pdf.getCell("Treatment Areas", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Service Type", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else
			tbl.addCell(pdf.getCell("Method of application", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
		if(onlyForPsipl)
			tbl.addCell(pdf.getCell("Target Pest", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else if(onlyForPestOCop)
			tbl.addCell(pdf.getCell("Service Category", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		else
			tbl.addCell(pdf.getCell("Pest Target", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		
			
//		tbl.addCell(pdf.getCell("Material", font8bold, Element.ALIGN_CENTER, 0, 2, 0));
		
		HashMap<Integer,Vector<String>> dataMap=new HashMap<Integer,Vector<String>>();
		Integer row=1;
		if(service.getPestTreatmentList()!=null&&service.getPestTreatmentList().size()!=0){
			
			int totalCount=service.getPestTreatmentList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(pest.getTreatmentArea());
					vecLis.add(pest.getMethodOfApplication());
					vecLis.add(pest.getPestTarget());
					dataMap.put(row, vecLis);
					row++;
				}
				
				for(int i=0;i<remainingRow;i++){
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(" ");
					vecLis.add(" ");
					vecLis.add(" ");
					dataMap.put(row, vecLis);
					row++;
				}
			}else{
				for(int i=0;i<maxRow;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(pest.getTreatmentArea());
					vecLis.add(pest.getMethodOfApplication());
					vecLis.add(pest.getPestTarget());
					dataMap.put(row, vecLis);
					row++;
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				Vector<String> vecLis=new Vector<String>();
				vecLis.add(" ");
				vecLis.add(" ");
				vecLis.add(" ");
				dataMap.put(row, vecLis);
				row++;
			}
		}
		
		
		row=1;
		if(project!=null&&project.getProdDetailsList()!=null&&project.getProdDetailsList().size()!=0){
			
			int totalCount=project.getProdDetailsList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			logger.log(Level.SEVERE,"createMaterialTable totalCount "+totalCount+" remainingRow "+remainingRow);
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					dataMap.get(row).add(material.getName());
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						dataMap.get(row).add((material.getReturnQuantity())+" "+material.getUnit());
					}else{
						dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+" "+material.getUnit());
					}
//					dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+" "+material.getUnit());
					row++;
				}
				
				for(int i=0;i<remainingRow;i++){
					dataMap.get(row).add(" ");
					dataMap.get(row).add(" ");
					row++;
				}
			}else{
				for(int i=0;i<maxRow;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					dataMap.get(row).add(material.getName());
					/**
					 * @author Anil
					 * @since 25-04-2020
					 * If this process configuration is active then we will display return quantity as consumed quantity
					 * Issue faced by ideal asar raised by Vaishnavi
					 */
					if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "MINBasedOnConsumedQuantity",project.getCompanyId())){
						dataMap.get(row).add((material.getReturnQuantity())+" "+material.getUnit());
					}else{
						dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+" "+material.getUnit());
					}
//					dataMap.get(row).add((material.getQuantity()-material.getReturnQuantity())+" "+material.getUnit());
					row++;
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				dataMap.get(row).add(" ");
				dataMap.get(row).add(" ");
				row++;
			}
		}
		
		
		for (Map.Entry<Integer,Vector<String>> entry : dataMap.entrySet()){  
			logger.log(Level.SEVERE,"Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
            Vector<String> list =entry.getValue();
            tbl.addCell(pdf.getCell(list.get(0), font8, Element.ALIGN_LEFT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(1), font8, Element.ALIGN_LEFT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(2), font8, Element.ALIGN_LEFT, 0, 0, 0));
			
//			tbl.addCell(pdf.getCell(list.get(3), font8, Element.ALIGN_LEFT, 0, 0, 0));
//			tbl.addCell(pdf.getCell(list.get(4), font8, Element.ALIGN_RIGHT, 0, 0, 0));
		}
		return tbl;
    
    }
    
    
    public PdfPTable createPestTreatmentAsFindingTable(){
    	PdfPTable tbl = new PdfPTable(3);
		tbl.setWidthPercentage(100);
		
//		try {
//			tbl.setWidths(new float[]{24f,18f,18f,24f,16f});
//		} catch (DocumentException e2) {
//			e2.printStackTrace();
//		}
		
//		tbl.addCell(pdf.getCell("FINDINGS", font9bold, Element.ALIGN_CENTER, 0, 3, 0)).setPaddingBottom(5);
		
		tbl.addCell(pdf.getCell("Pest Type", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Mode of Treatment", font8bold, Element.ALIGN_CENTER, 0, 0, 0));
		tbl.addCell(pdf.getCell("Treatment Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0));//old label "Infected Area". Ashwini Patil changed label as per Ankita Pest requirement and Niti sir's instruction
		
		
		HashMap<Integer,Vector<String>> dataMap=new HashMap<Integer,Vector<String>>();
		Integer row=1;
		if(service.getPestTreatmentList()!=null&&service.getPestTreatmentList().size()!=0){
			
			int totalCount=service.getPestTreatmentList().size();
			int remainingRow=0;
			if(totalCount<maxRow){
				remainingRow=maxRow-totalCount;
			}
			if(remainingRow>0){
				for(int i=0;i<totalCount;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(pest.getPestTarget());
					vecLis.add(pest.getMethodOfApplication());
					vecLis.add(pest.getTreatmentArea());
					dataMap.put(row, vecLis);
					row++;
				}
				
				for(int i=0;i<remainingRow;i++){
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(" ");
					vecLis.add(" ");
					vecLis.add(" ");
					dataMap.put(row, vecLis);
					row++;
				}
			}else{
				for(int i=0;i<maxRow;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					Vector<String> vecLis=new Vector<String>();
					vecLis.add(pest.getPestTarget());
					vecLis.add(pest.getMethodOfApplication());
					vecLis.add(pest.getTreatmentArea());
					
					dataMap.put(row, vecLis);
					row++;
				}
			}
		}else{
			for(int i=0;i<maxRow;i++){
				Vector<String> vecLis=new Vector<String>();
				vecLis.add(" ");
				vecLis.add(" ");
				vecLis.add(" ");
				dataMap.put(row, vecLis);
				row++;
			}
		}
		
		
		
		
		if(dataMap.size()>0) { //Ashwini Patil Date:8-08-2022
		for (Map.Entry<Integer,Vector<String>> entry : dataMap.entrySet()){  
			logger.log(Level.SEVERE,"Key = " + entry.getKey() +  ", Value = " + entry.getValue()); 
            Vector<String> list =entry.getValue();
            tbl.addCell(pdf.getCell(list.get(0), font8, Element.ALIGN_LEFT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(1), font8, Element.ALIGN_LEFT, 0, 0, 0));
			tbl.addCell(pdf.getCell(list.get(2), font8, Element.ALIGN_LEFT, 0, 0, 0));
			
//			tbl.addCell(pdf.getCell(list.get(3), font8, Element.ALIGN_LEFT, 0, 0, 0));
//			tbl.addCell(pdf.getCell(list.get(4), font8, Element.ALIGN_RIGHT, 0, 0, 0));
		}
    }
		return tbl;
    
    }
    
    public void loadMultipleSRCopies(List<Service> serviceList){
//    	logger.log(Level.SEVERE,"in loadMultipleSRCopies serviceList size="+serviceList.size()+" totalServicesList size="+totalServicesList.size());
		if(serviceList.size()>0) {
			logger.log(Level.SEVERE,"loadBasicData() with completed services");
    	loadBasicData(serviceList.get(0).getCompanyId(),serviceList.get(0).getPersonInfo().getCount(),serviceList.get(0).getBranch());
    	
    	//Ashwini Patil Date:1-07-2024 added completion date sort on request of vectron
    	try {
    		Comparator<Service> completionDateComp=new Comparator<Service>() {
    			@Override
    			public int compare(Service arg0, Service arg1) {
    				
    				return arg0.getServiceCompletionDate().compareTo(arg1.getServiceCompletionDate());
    			}
    		};
    		Collections.sort(serviceList, completionDateComp);
    	}catch(Exception e) {
    		
    	}
    	 int serviceNo=1;
    			
    	for(Service service:serviceList){
    		if(serviceNo==serviceList.size())
			{
				printFooterFlag=true;			
			}
    		this.service=service;
    		clCount=0;
    		maxRow=6; //3
    		maxRowForCheckList=5; //5
    		photoFlag=true;
    		if (this.service != null) {
    			project = ofy().load().type(ServiceProject.class)
    					.filter("companyId", service.getCompanyId())
    					.filter("contractId", service.getContractCount())
    					.filter("serviceId", service.getCount()).first().now();
    		}
    		
    		if((service.getServiceImage1()==null||service.getServiceImage1().getUrl().equals(""))
    				&&(service.getServiceImage2()==null||service.getServiceImage2().getUrl().equals(""))
    				&&(service.getServiceImage3()==null||service.getServiceImage3().getUrl().equals(""))
    				&&(service.getServiceImage4()==null||service.getServiceImage4().getUrl().equals(""))){
    			photoFlag=false;
    		}
    		
    		if(!reportFormat.equalsIgnoreCase("pdf")) {
    			createPdf();
        		
    			Phrase nextpage = new Phrase(Chunk.NEXTPAGE);
    			try {
    				document.add(nextpage);
    			} catch (DocumentException e1) {
    				e1.printStackTrace();
    			}
    		}
    			
    		serviceNo++;   	}
		}else if(totalServicesList.size()>0)
		{logger.log(Level.SEVERE,"loadBasicData() with total services");
			this.service=totalServicesList.get(0);
			loadBasicData(totalServicesList.get(0).getCompanyId(),totalServicesList.get(0).getPersonInfo().getCount(),totalServicesList.get(0).getBranch());	    	
		}else {
			logger.log(Level.SEVERE,"loadBasicData() cannot be executed as service list size is zero");
		}
    		
		if(reportFormat.equalsIgnoreCase("pdf"))
    		createnewPdf(serviceList);
				
		
}
    //Ashwini Patil
    public void loadMultipleSRCopies(List<Service> serviceList, XSSFWorkbook workbook){
    if(serviceList.size()>0) {	
		loadBasicData(serviceList.get(0).getCompanyId(),serviceList.get(0).getPersonInfo().getCount(),serviceList.get(0).getBranch());
    	for(Service service:serviceList){
    		this.service=service;
    		photoFlag=true;
    		if (this.service != null) {
    			project = ofy().load().type(ServiceProject.class)
    					.filter("companyId", service.getCompanyId())
    					.filter("contractId", service.getContractCount())
    					.filter("serviceId", service.getCount()).first().now();
    		}
    		
    		if((service.getServiceImage1()==null||service.getServiceImage1().getUrl().equals(""))
    				&&(service.getServiceImage2()==null||service.getServiceImage2().getUrl().equals(""))
    				&&(service.getServiceImage3()==null||service.getServiceImage3().getUrl().equals(""))
    				&&(service.getServiceImage4()==null||service.getServiceImage4().getUrl().equals(""))){
    			photoFlag=false;
    		}
    		
    	}
    }else if(totalServicesList.size()>0) {
    	loadBasicData(totalServicesList.get(0).getCompanyId(),totalServicesList.get(0).getPersonInfo().getCount(),totalServicesList.get(0).getBranch());
    	
    }else {
    	logger.log(Level.SEVERE,"loadBasicData() cannot be executed as service list size is zero");   	
    }
//    		----------------------------------------
    if(totalServicesList.size()>0) {
    	List<CustomerBranchDetails> custBranches= ofy().load().type(CustomerBranchDetails.class)
    			.filter("companyId", serviceList.get(0).getCompanyId())
    			.filter("cinfo.count", serviceList.get(0).getCustomerId()).list();		
    	
    	logger.log(Level.SEVERE,"No. of Customer Branches ="+custBranches.size());
    	List<Service> serviceAddressServiceList=new ArrayList<Service>();
    	
    	for(CustomerBranchDetails c:custBranches) {
    		List<Service> branchWiseServiceList=new ArrayList<Service>();	
    		logger.log(Level.SEVERE,"branchWiseServiceList size of"+c.getBusinessUnitName()+"before iteration="+branchWiseServiceList.size());
    		for(Service s:totalServicesList) {
    			if(s.getServiceBranch().equals(c.getBusinessUnitName())) {
    				branchWiseServiceList.add(s);
    			}		
    		}
    		logger.log(Level.SEVERE,"branchWiseServiceList size of"+c.getBusinessUnitName()+"after iteration="+branchWiseServiceList.size());			
    		if(branchWiseServiceList.size()>0) {
    			logger.log(Level.SEVERE,"Printing details of branch ="+branchWiseServiceList.get(0).getServiceBranch());
    			List<CustomerBranchServiceLocation> locList=new ArrayList<CustomerBranchServiceLocation>();
    			locList=ofy().load().type(CustomerBranchServiceLocation.class)
    					.filter("companyId", c.getCompanyId())
    					.filter("custBranchId", c.getCount()).list();
    			
    			int areaCount=0;
    			if(locList!=null&&locList.size()>0) {
    				logger.log(Level.SEVERE,"locList.size ="+locList.size());
    				for(CustomerBranchServiceLocation loc:locList)
    				{
//    					areaCount+=loc.getAreaList().size();
    					if(loc.getAreaList2()!=null && loc.getAreaList2().size()>0){
        					areaCount+=loc.getAreaList2().size();
    					}
    					else if(loc.getAreaList()!=null && loc.getAreaList().size()>0){
        					areaCount+=loc.getAreaList().size();
    					}
    				}
    			}
    			createServiceDetailsExcel(branchWiseServiceList, workbook,c.getBusinessUnitName(),areaCount);

    			branchWiseServiceList.clear();
    		}
    		
    	}
    	
    	for(Service s:totalServicesList) {			
    		if(s.getServiceBranch().equals("Service Address"))
    				serviceAddressServiceList.add(s);											
    	}
    	 	
    	if(serviceAddressServiceList.size()>0) {
    		logger.log(Level.SEVERE,"Printing details of service address");
    		
    		createServiceDetailsExcel(serviceAddressServiceList, workbook,"Service Address",1);
    	}
        
    }else {
    	logger.log(Level.SEVERE,"No records found");
    	boldStyle = (XSSFCellStyle) workbook.createCellStyle();
        XSSFFont boldFont = (XSSFFont) workbook.createFont();
        boldFont.setFontHeightInPoints((short) 14);
        boldFont.setFontName("Times New Roman");
        boldFont.setBold(true);
        boldStyle.setFont(boldFont);
        boldStyle.setBorderBottom(BorderStyle.THIN);
        boldStyle.setBorderLeft(BorderStyle.THIN);
        boldStyle.setBorderRight(BorderStyle.THIN);
        boldStyle.setBorderTop(BorderStyle.THIN);
        boldStyle.setWrapText(true);
          	
        
        leftStyle = (XSSFCellStyle) workbook.createCellStyle();
        leftStyle.setFont(boldFont);
        leftStyle.setBorderBottom(BorderStyle.THIN);
        leftStyle.setBorderLeft(BorderStyle.THIN);
        leftStyle.setBorderRight(BorderStyle.THIN);
        leftStyle.setBorderTop(BorderStyle.THIN);
        leftStyle.setWrapText(true);
        leftStyle.setAlignment(HorizontalAlignment.LEFT);
        leftStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        
        XSSFSheet sheet = (XSSFSheet) workbook.createSheet("Sheet1");
        sheet.setDefaultColumnWidth(17);
        sheet.setColumnWidth(0,5);
        
        Row row=sheet.createRow(0);
        createCell(row, 0, "No records found", leftStyle);
        sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 6));   	
    }
//    ------------------------------------------------
//    		createServiceDetailsExcel(totalServicesList, workbook);
	logger.log(Level.SEVERE,"End of loadMultipleSRCopies method");
}

    /**
     * @author Ashwini Patil 
     * this method is called when user selects pdf format from MultipleSRCopyPopup
     */
    public void createnewPdf(List<Service> completedServiceList){
    	List<Service> serviceList=new ArrayList<Service>();
    	serviceList=totalServicesList;
    	logger.log(Level.SEVERE,"in createnewPdf service list size="+serviceList.size());
    	
    	if(serviceList.size()>0) {//Ashwini Patil Date:8-02-2022
    		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "CompanyAsLetterHead", comp.getCompanyId())){
    			companyAsaletter=true;
    		}
    		
    		
    		if(companyAsaletter){
    			if (comp.getUploadHeader() != null) {
    				createCompanyNameAsHeader(document, comp);
    				blankCells();
    			}
    			PdfPTable headertab=new PdfPTable(3);
    			headertab.setWidthPercentage(100);
    			try {
    				headertab.setWidths(new float[]{33,33,33});
    			} catch (DocumentException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			
    			//By: Ashwini Patil To Print Contract Id in header on MultipleSrCopy pdf
    			PdfPCell cell=pdf.getCell("Contract ID : "+serviceList.get(0).getContractCount(), font8, Element.ALIGN_LEFT, 0, 0, 0);
    			cell.setPaddingBottom(5);
    			cell.setBorderWidthRight(0);
    			headertab.addCell(cell);
    			cell=pdf.getCell("SERVICE RECORD", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
    			cell.setPaddingBottom(5);
    			cell.setBorderWidthRight(0);
    			cell.setBorderWidthLeft(0);
    			headertab.addCell(cell);
    			
    			Contract con = ofy().load().type(Contract.class)
    					.filter("companyId", service.getCompanyId())
    					.filter("count", service.getContractCount()).first().now();
    			String duration="Contract Duration: "+fmt.format(con.getStartDate())+" - "+fmt.format(con.getEndDate());
    			
    			cell=pdf.getCell(duration, font8, Element.ALIGN_RIGHT, 0, 0, 0);
    			cell.setPaddingBottom(5);
    			cell.setBorderWidthLeft(0);
    			headertab.addCell(cell);
        		
    			try {
    				document.add(headertab);
    			} catch (DocumentException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			
    		}else{
    			createHeader();
    		}
    		
    		List<CustomerBranchDetails> custBranches= ofy().load().type(CustomerBranchDetails.class)
    				.filter("companyId", serviceList.get(0).getCompanyId())
    				.filter("cinfo.count", serviceList.get(0).getCustomerId()).list();		
    		
    		logger.log(Level.SEVERE,"No. of Customer Branches ="+custBranches.size());
    		List<Service> serviceAddressServiceList=new ArrayList<Service>();
    		
    		for(CustomerBranchDetails c:custBranches) {
    			List<Service> branchWiseServiceList=new ArrayList<Service>();	
    			logger.log(Level.SEVERE,"branchWiseServiceList size of"+c.getBusinessUnitName()+"before iteration="+branchWiseServiceList.size());
    			for(Service s:serviceList) {
    				if(s.getServiceBranch().equals(c.getBusinessUnitName())) {
    					branchWiseServiceList.add(s);
    				}		
    			}
    			logger.log(Level.SEVERE,"branchWiseServiceList size of"+c.getBusinessUnitName()+"after iteration="+branchWiseServiceList.size());			
    			if(branchWiseServiceList.size()>0) {
    				logger.log(Level.SEVERE,"Printing details of branch ="+branchWiseServiceList.get(0).getServiceBranch());
    				List<CustomerBranchServiceLocation> locList=new ArrayList<CustomerBranchServiceLocation>();
        			locList=ofy().load().type(CustomerBranchServiceLocation.class)
        					.filter("companyId", c.getCompanyId())
        					.filter("custBranchId", c.getCount()).list();
        			
        			int areaCount=0;
        			if(locList!=null&&locList.size()>0) {
        				logger.log(Level.SEVERE,"locList.size ="+locList.size());
        				for(CustomerBranchServiceLocation loc:locList)
        				{
//        					if(loc.getAreaList()!=null)
//        						areaCount+=loc.getAreaList().size();
        					
        					if(loc.getAreaList2()!=null && loc.getAreaList2().size()>0){
            					areaCount+=loc.getAreaList2().size();
        					}
        					else if(loc.getAreaList()!=null && loc.getAreaList().size()>0){
            					areaCount+=loc.getAreaList().size();
        					}
        				}
        			}
    				createServiceDetailsTable(branchWiseServiceList,areaCount);
    				document.newPage();
    				branchWiseServiceList.clear();
    			}
    			
    		}
    		
    		for(Service s:serviceList) {			
    			if(s.getServiceBranch().equals("Service Address"))
    					serviceAddressServiceList.add(s);											
    		}
        	 	
    		if(serviceAddressServiceList.size()>0) {
    			logger.log(Level.SEVERE,"Printing details of service address");
    			
    			createServiceDetailsTable(serviceAddressServiceList,1);
    		}
		
    	}else {
    		PdfPTable errorTable=new PdfPTable(1);
    		errorTable.setWidthPercentage(100);
    		errorTable.addCell(pdf.getCell("No Records Found", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        	try {
    			document.add(errorTable);
    		} catch (DocumentException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    	}
    		
		//Printing service details table
//		createServiceDetailsTable(serviceList);
	
    }
    
    //Ashwini Patil
    public String getChecklist(Service s)
    {

    	String chklist="";
    	if(s.getCheckList()!=null&&s.getCheckList().size()!=0){
			int totalCount=s.getCheckList().size();	
			
				for(int i=0;i<totalCount;i++){
					CatchTraps finding=s.getCheckList().get(i);
					chklist=chklist+finding.getPestName()+"\n";
				}
    	}
    	return chklist;
    
    }
    //Ashwini Patil
    public String getMaterial(Service s) {
    String materialDetails="";
    if (service != null) {
		project = ofy().load().type(ServiceProject.class)
				.filter("companyId", s.getCompanyId())
				.filter("contractId", s.getContractCount())
				.filter("serviceId", s.getCount()).first().now();
	}
	if(project!=null&&project.getProdDetailsList()!=null&&project.getProdDetailsList().size()!=0){
			
			int totalCount=project.getProdDetailsList().size();
			
				for(int i=0;i<totalCount;i++){
					ProductGroupList material=project.getProdDetailsList().get(i);
					double qty=material.getQuantity()-material.getReturnQuantity();
					materialDetails=materialDetails+material.getName()+" / "+qty+" / "+material.getUnit()+"\n";
				}				
			}
	return materialDetails;
		
    }
	
	//Ashwini Patil
    public String getFindings(Service s) {
    	String findingDetails="";
		if(s.getCatchtrapList()!=null&&s.getCatchtrapList().size()!=0){
			
			int totalCount=s.getCatchtrapList().size();
			
				for(int i=0;i<totalCount;i++){
					CatchTraps finding=s.getCatchtrapList().get(i);
					if(finding.getCatchLevel()!=null&&!finding.getCatchLevel().equals("")) //Ashwini Patil Date:14-10-2024 for psipl
						findingDetails=findingDetails+finding.getPestName()+" / "+finding.getCatchLevel()+" / "+finding.getLocation()+"\n";
					else
						findingDetails=findingDetails+finding.getPestName()+" / "+finding.getCount()+" / "+finding.getLocation()+"\n";
				}			
				
		}
		return findingDetails;
    }
    
	//Ashwini Patil
    public String getTreatment(Service s)
    {
    	String treatmentDetails="";
    	try {
		if(s.getPestTreatmentList()!=null&&s.getPestTreatmentList().size()!=0){
			
			int totalCount=s.getPestTreatmentList().size();
			int remainingRow=0;
			
			for(int i=0;i<totalCount;i++){
					PestTreatmentDetails pest=service.getPestTreatmentList().get(i);
					treatmentDetails=treatmentDetails+pest.getTreatmentArea()+" / "+pest.getMethodOfApplication()+" / "+pest.getPestTarget()+"\n";				
			}
		}
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
		return treatmentDetails;
    }
    
    //Ashwini Patil
    public void createServiceDetailsTable(List<Service> serviceList,int areaCount) {
    	float[] columnWidth = { 0.8f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f};
		
    	PdfPTable mainTable=new PdfPTable(12);
		mainTable.setWidthPercentage(100);
		mainTable.setHeaderRows(1);
		try {
			mainTable.setWidths(columnWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		
		
		int totalServices=0,incompleteServices=0,completeServices=0,cancelledServices=0;
		List<Service> completedServiceList=new ArrayList<Service>();
		List<Service> cancelledServiceList=new ArrayList<Service>();
		List<Service> otherServiceList=new ArrayList<Service>();
		totalServices=serviceList.size();
		
		for(Service s:serviceList) {
			if(s.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)) {
				completeServices=completeServices+1;
				completedServiceList.add(s);
			}else if(s.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCLOSED)||s.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)) {
				cancelledServices=cancelledServices+1;
				cancelledServiceList.add(s);
			}else {				
				incompleteServices=incompleteServices+1;
				otherServiceList.add(s);
			}			
		}
		
		//Pecopp require productwise sorting
		Comparator<Service> prodNameComp=new Comparator<Service>() {
			@Override
			public int compare(Service arg0, Service arg1) {
				return arg0.getProductName().toUpperCase().compareTo(arg1.getProductName().toUpperCase());
			}
		};
		
		int hp=0;
		if(completedFlag) {
			createHeadingForSummarySRPdf(serviceList,mainTable,totalServices,incompleteServices,areaCount,completeServices,cancelledServices);			    		
			hp+=1;			
			if(completedServiceList.size()>1)
				Collections.sort(completedServiceList, prodNameComp);//21-09-2022
			
			printServiceRecords(completedServiceList, mainTable);			
		}
		if(cancelledFlag) {
			if(hp==0) {
				createHeadingForSummarySRPdf(serviceList,mainTable,totalServices,incompleteServices,areaCount,completeServices,cancelledServices);			    		
				hp+=1;
			}	
			if(cancelledServiceList.size()>1)
				Collections.sort(cancelledServiceList, prodNameComp);//21-09-2022
			printServiceRecords(cancelledServiceList, mainTable);
		}
		if(otherFlag) {
			if(hp==0) {
				createHeadingForSummarySRPdf(serviceList,mainTable,totalServices,incompleteServices,areaCount,completeServices,cancelledServices);			    		
				hp+=1;
			}	
			if(otherServiceList.size()>1)
				Collections.sort(otherServiceList, prodNameComp);//21-09-2022
			printServiceRecords(otherServiceList, mainTable);
		}
		
		
		
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    public void createHeadingForSummarySRPdf(List<Service> serviceList,PdfPTable mainTable,int totalServices,int incompleteServices,int areaCount,int completeServices,int cancelledServices) {
    	float[] columnWidth = { 0.8f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f, 3.5f};		
    	PdfPTable infoTable=new PdfPTable(12);
    	infoTable.setWidthPercentage(100);
		try {
			infoTable.setWidths(columnWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    	String info="";
    	String serviceDuration="";
    	logger.log(Level.SEVERE,"fromDate="+fromDate+" todate="+toDate);
    	if(!fromDate.equals("")&&!toDate.equals("")) {
    		
    		serviceDuration="Service Duration : "+fromDate+" - "+toDate;
    	}
    	logger.log(Level.SEVERE,"service duration="+serviceDuration);
    	info=info+serviceList.get(0).getPersonInfo().getFullName()+" - "+serviceList.get(0).getServiceBranch()+"\n"+serviceList.get(0).getAddress().getCompleteAddress()+"\n"+serviceDuration;
    	
    	infoTable.addCell(pdf.getCell(info, font8, Element.ALIGN_LEFT, 2, 7, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell("Total Areas", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell("Total Services", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell(totalServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell("In Progress", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell(incompleteServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell(areaCount+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell("Completed", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell(completeServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell("Cancelled or On Hold", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
		infoTable.addCell(pdf.getCell(cancelledServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		
		try {
			document.add(infoTable);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		mainTable.addCell(pdf.getCell("S.N.", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("ID / Date", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);//"ID / Date / Geo location name"
		mainTable.addCell(pdf.getCell("Service Location", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("Area", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);		
		mainTable.addCell(pdf.getCell("Technician(s)", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("Service", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("Status", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		if(PC_DoNotPrintMaterialOnSummarySR) {
			mainTable.addCell(pdf.getCell("Findings", font8bold, Element.ALIGN_CENTER, 0, 2, 0)).setBorder(Rectangle.BOX);
		}else
			mainTable.addCell(pdf.getCell("Material/Findings", font8bold, Element.ALIGN_CENTER, 0, 2, 0)).setBorder(Rectangle.BOX);
		
		mainTable.addCell(pdf.getCell("Check List/Treatment Type", font8bold, Element.ALIGN_CENTER, 0, 2, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("Client Name, Number & Rating", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		
    }
    
	
	//Ashwini Patil
    public void printServiceRecords(List<Service> serviceList,PdfPTable mainTable) {
		
		for(Service service:serviceList){

			counter++;
			String id="";
			String serviceLocation="";
			String area="";
			String technician="";
			String servicename="";
			String status="";
			String material="";
			String findings="";
			String checklist="";
			String treatment=""; 
			String clientNameNumber="";
			String date="";
			String geoLocation="";
			Image photo1,photo2,photo3,photo4;			
			String remark="";
			String rating="";
			String signature;
			

			DocumentUpload image1 = service.getServiceImage1();
			DocumentUpload image2 = service.getServiceImage2();
			DocumentUpload image3 = service.getServiceImage3();
			DocumentUpload image4 = service.getServiceImage4();
			DocumentUpload customerSignature=service.getCustomerSignature();
			
			if(service.getStatus().equalsIgnoreCase(service.SERVICESTATUSCOMPLETED)) {
				geoLocation=service.getCompletedLocationName();
			}
			
			id=service.getCount()+"";
//			serviceLocation=service.getPremises();
			if(service.getServiceLocation()!=null&&!service.getServiceLocation().equals(""))
				serviceLocation=service.getServiceLocation();
			if(service.getServiceLocationArea()!=null&&!service.getServiceLocationArea().equals(""))
				area=service.getServiceLocationArea();
			technician=getTechnician(service);
//			List<EmployeeInfo> technicians=service.getTechnicians();
//			for(EmployeeInfo e:technicians) {
//				technician=technician+e.getFullName()+"\n";
//			}
			//technician=service.getEmployee();
			servicename=service.getProductName();
			status=service.getStatus();
			material=getMaterial(service);
			findings=getFindings(service);
			checklist=getChecklist(service);
			treatment=getTreatment(service);
			
			if(service.getCustomerSignName()!=null){
				clientNameNumber=service.getCustomerSignName();
			}
			if(service.getCustomerSignNumber()!=null){
				clientNameNumber=clientNameNumber+"\n"+service.getCustomerSignNumber()+"\n"+service.getServiceCompleteRemark();
			}
			if(service.getServiceCompletionDate()!=null){
				date=sdf1.format(service.getServiceCompletionDate());
				date+="\n"+service.getCompletedTime();
			}
			if(service.getStatus().equalsIgnoreCase(service.SERVICESTATUSCANCELLED)||service.getStatus().equalsIgnoreCase(service.SERVICESTATUSCLOSED))
				remark=service.getTechnicianRemark();
			else
				remark=service.getRemark();
	
			mainTable.addCell(pdf.getCell(counter+"", font8, Element.ALIGN_CENTER, 2, 0, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(id, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(serviceLocation, font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(area, font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);			
			mainTable.addCell(pdf.getCell(technician, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(servicename, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(status, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			if(PC_DoNotPrintMaterialOnSummarySR) {
				mainTable.addCell(pdf.getCell(findings, font8, Element.ALIGN_LEFT, 2, 2, 0)).setBorder(Rectangle.BOX);
			}else {
				mainTable.addCell(pdf.getCell(material, font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(Rectangle.BOX);
			}
			mainTable.addCell(pdf.getCell(checklist, font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(Rectangle.BOX);			
			mainTable.addCell(pdf.getCell(clientNameNumber, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(date, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);//"\n"+geoLocation
		
			
			if(service.getServiceImage1()!=null&&!service.getServiceImage1().getUrl().equals("")){
				logger.log(Level.SEVERE,"image1 url="+service.getServiceImage1().getUrl());
				mainTable.addCell(pdf.getPhotoCell(service.getServiceImage1(), 20f, 120, 0, 0,0));
			}else{
				mainTable.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
			}
			
			if(service.getServiceImage2()!=null&&!service.getServiceImage2().getUrl().equals("")){
				mainTable.addCell(pdf.getPhotoCell(service.getServiceImage2(), 20f, 120, 0, 0,0));
			}else{
				mainTable.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
			}
			
			if(service.getServiceImage3()!=null&&!service.getServiceImage3().getUrl().equals("")){
				mainTable.addCell(pdf.getPhotoCell(service.getServiceImage3(), 20f, 120, 0, 0,0));
			}else{
				mainTable.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
			}
			
			if(service.getServiceImage4()!=null&&!service.getServiceImage4().getUrl().equals("")){
				mainTable.addCell(pdf.getPhotoCell(service.getServiceImage4(), 20f, 120, 0, 0,0));
			}else{
				mainTable.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
			}
			

			mainTable.addCell(pdf.getCell(remark, font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
			if(!PC_DoNotPrintMaterialOnSummarySR) {
				mainTable.addCell(pdf.getCell(findings, font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(Rectangle.BOX);
			}
//			mainTable.addCell(pdf.getCell(findings, font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(Rectangle.BOX);
			mainTable.addCell(pdf.getCell(treatment, font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(Rectangle.BOX);
			
			if(service.getCustomerSignature()!=null&&!service.getCustomerSignature().getUrl().equals("")){
				mainTable.addCell(pdf.getPhotoCell(service.getCustomerSignature(), 20f, 120, 0, 0,0));
			}else{
				mainTable.addCell(pdf.getCell("", font8, Element.ALIGN_LEFT, 0, 0, 120));
			}

		}
		
    }
    
    
    //Ashwini Patil to generate excel workbook of multiple SR copies
    public void createServiceDetailsExcel(List<Service> serviceList,XSSFWorkbook workbook, String sheetName, int areaCount) {
    	logger.log(Level.SEVERE,"Inside createServiceDetailsExcel");
    	
    	boldStyle = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont boldFont = (XSSFFont) workbook.createFont();
	    boldFont.setFontHeightInPoints((short) 14);
	    boldFont.setFontName("Times New Roman");
	    boldFont.setBold(true);
	    boldStyle.setFont(boldFont);
	    boldStyle.setBorderBottom(BorderStyle.THIN);
	    boldStyle.setBorderLeft(BorderStyle.THIN);
	    boldStyle.setBorderRight(BorderStyle.THIN);
	    boldStyle.setBorderTop(BorderStyle.THIN);
	    boldStyle.setWrapText(true);
	   
    	style = (XSSFCellStyle) workbook.createCellStyle();
	    XSSFFont font = (XSSFFont) workbook.createFont();
	    font.setFontHeightInPoints((short) 14);
	    font.setFontName("Times New Roman");
	    font.setBold(false);
	    style.setFont(font);
	    style.setBorderBottom(BorderStyle.THIN);
	    style.setBorderLeft(BorderStyle.THIN);
	    style.setBorderRight(BorderStyle.THIN);
	    style.setBorderTop(BorderStyle.THIN);
	    style.setWrapText(true);
	    style.setAlignment(HorizontalAlignment.CENTER);
	    style.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    leftStyle = (XSSFCellStyle) workbook.createCellStyle();
	    leftStyle.setFont(font);
	    leftStyle.setBorderBottom(BorderStyle.THIN);
	    leftStyle.setBorderLeft(BorderStyle.THIN);
	    leftStyle.setBorderRight(BorderStyle.THIN);
	    leftStyle.setBorderTop(BorderStyle.THIN);
	    leftStyle.setWrapText(true);
	    leftStyle.setAlignment(HorizontalAlignment.LEFT);
	    leftStyle.setVerticalAlignment(VerticalAlignment.CENTER);
	    
	    
	   	    
    	String info="";
		int totalServices=0,incompleteServices=0,completeServices=0,cancelledServices=0;
		totalServices=serviceList.size();
		List<Service> completedServiceList=new ArrayList<Service>();
		List<Service> cancelledServiceList=new ArrayList<Service>();
		List<Service> otherServiceList=new ArrayList<Service>();
		rowCounter=3;
		
		for(Service s:serviceList) {
			if(s.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)) {
				completeServices=completeServices+1;
				completedServiceList.add(s);
			}else if(s.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCLOSED)||s.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)) {
				cancelledServices=cancelledServices+1;
				cancelledServiceList.add(s);
			}else {				
				incompleteServices=incompleteServices+1;
				otherServiceList.add(s);
			}			
		}
		
		XSSFSheet sheet = (XSSFSheet) workbook.createSheet(sheetName.replace("/", "-"));
	    sheet.setDefaultColumnWidth(17);
	    sheet.setColumnWidth(0,5);
	    String serviceDuration="";
    	if(!fromDate.equals("")&&!toDate.equals("")) {
    		serviceDuration="\nService Duration : "+fromDate+" - "+toDate;
    	}
		info=info+serviceList.get(0).getPersonInfo().getFullName()+" - "+serviceList.get(0).getServiceBranch()+"\n"+serviceList.get(0).getAddress().getCompleteAddress()+serviceDuration+"\nContract ID:"+serviceList.get(0).getContractCount();
		Row row=sheet.createRow(0);
	    createCell(row, 0, info, leftStyle);
	    sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 6));
	    createCell(row, 7, "Total Areas", leftStyle);
	    createCell(row, 8, "Total Services", leftStyle);
	    createCell(row, 9, totalServices+"", style);
	    createCell(row, 10, "In Progress", leftStyle);
	    createCell(row, 11, incompleteServices+"", style);
	    row=sheet.createRow(1);
	    createCell(row, 7, areaCount+"", style);
	    createCell(row, 8, "Completed", leftStyle);
	    createCell(row, 9, completeServices+"", style);
	    createCell(row, 10, "Cancelled or on Hold", leftStyle);
	    createCell(row, 11, cancelledServices+"", style); 
	    
	    boldStyle.setAlignment(HorizontalAlignment.CENTER);
	    row=sheet.createRow(2);
	    createCell(row, 0, "S.N.", boldStyle);
	    createCell(row, 1, "ID / Date", boldStyle);//"ID / Date / Geo location name"
	    createCell(row, 2, "Service Location", boldStyle);
	    createCell(row, 3, "Area", boldStyle);
	    createCell(row, 4, "Technician(s)", boldStyle);
	    createCell(row, 5, "Service", boldStyle);
	    createCell(row, 6, "Status", boldStyle);
	    if(PC_DoNotPrintMaterialOnSummarySR)
	    	createCell(row, 7, "Findings", boldStyle);
	    else
	    	createCell(row, 7, "Material/Findings", boldStyle);
	    sheet.addMergedRegion(new CellRangeAddress(2, 2, 7, 8));
	    createCell(row, 9, "Check List/Treatment Type", boldStyle);
	    sheet.addMergedRegion(new CellRangeAddress(2, 2, 9, 10));
	    createCell(row, 11, "Client Name, Number & Rating", boldStyle);

	  //Pecopp require productwise sorting
	  		Comparator<Service> prodNameComp=new Comparator<Service>() {
	  			@Override
	  			public int compare(Service arg0, Service arg1) {
	  				return arg0.getProductName().toUpperCase().compareTo(arg1.getProductName().toUpperCase());
	  			}
	  		};
	    if(completedFlag) {
	    	if(completedServiceList.size()>1)
	    		Collections.sort(completedServiceList, prodNameComp);//21-09-2022			
	    	printServiceRecordsInWorkbook(completedServiceList,workbook,sheet);
	    }		
	    if(cancelledFlag) {
	    	if(cancelledServiceList.size()>1)
	    		Collections.sort(cancelledServiceList, prodNameComp);//21-09-2022
			printServiceRecordsInWorkbook(cancelledServiceList,workbook,sheet);
	    }
		if(otherFlag) {
			if(otherServiceList.size()>1)
				Collections.sort(otherServiceList, prodNameComp);//21-09-2022
			printServiceRecordsInWorkbook(otherServiceList,workbook,sheet);
		}
	       
    }
    
    //Ashwini Patil to generate rows for every service in excel workbook
    public void printServiceRecordsInWorkbook(List<Service> serviceList,XSSFWorkbook workbook,XSSFSheet sheet) {
	    
		for(Service service:serviceList){
			Row row=sheet.createRow(rowCounter);
			counter++;
			String id="";
			String serviceLocation="";
			String area="";
			String technician="";
			String servicename="";
			String status="";
			String material="";
			String findings="";
			String checklist="";
			String treatment=""; 
			String clientNameNumber="";
			String date="";
			Image photo1,photo2,photo3,photo4;			
			String remark="";
			String rating="";
			String signature;
			String geoLocation="";
			if(service.getStatus().equalsIgnoreCase(service.SERVICESTATUSCOMPLETED)) {
				geoLocation=service.getCompletedLocationName();
			}

			DocumentUpload image1 = service.getServiceImage1();
			DocumentUpload image2 = service.getServiceImage2();
			DocumentUpload image3 = service.getServiceImage3();
			DocumentUpload image4 = service.getServiceImage4();
			DocumentUpload customerSignature=service.getCustomerSignature();
			
			id=service.getCount()+"";
//			serviceLocation=service.getPremises();
			if(service.getServiceLocation()!=null&&!service.getServiceLocation().equals(""))
				serviceLocation=service.getServiceLocation();
			if(service.getServiceLocationArea()!=null&&!service.getServiceLocationArea().equals(""))
				area=service.getServiceLocationArea();
			technician=getTechnician(service);
//			List<EmployeeInfo> technicians=service.getTechnicians();
//			for(EmployeeInfo e:technicians) {
//				technician=technician+e.getFullName()+"\n";
//			}

			servicename=service.getProductName();
			status=service.getStatus();
			material=getMaterial(service);
			findings=getFindings(service);
			checklist=getChecklist(service);
			treatment=getTreatment(service);
			
			if(service.getCustomerSignName()!=null){
				clientNameNumber=service.getCustomerSignName();
			}
			if(service.getCustomerSignNumber()!=null){
				clientNameNumber=clientNameNumber+"\n"+service.getCustomerSignNumber()+"\n"+service.getServiceCompleteRemark();
			}
			if(service.getServiceCompletionDate()!=null){
				date=sdf1.format(service.getServiceCompletionDate());
				date+="\n"+service.getCompletedTime() ;//+"\n"+geoLocation
			}
			if(service.getStatus().equalsIgnoreCase(service.SERVICESTATUSCANCELLED)||service.getStatus().equalsIgnoreCase(service.SERVICESTATUSCLOSED))
				remark=service.getTechnicianRemark();
			else
				remark=service.getRemark();
	
			createCell(row, 0, counter+"", style);
			sheet.addMergedRegion(new CellRangeAddress(rowCounter, rowCounter+1, 0, 0));
			createCell(row, 1,id , style);
			createCell(row, 2,serviceLocation , style);
			createCell(row, 3,area , style);
			createCell(row, 4,technician , style);
			createCell(row, 5,servicename , style);
			createCell(row, 6,status , style);
			if(PC_DoNotPrintMaterialOnSummarySR) {
				createCell(row, 7,findings, leftStyle);
				sheet.addMergedRegion(new CellRangeAddress(rowCounter, rowCounter+1, 7, 8));
			}else {
				createCell(row, 7,material, leftStyle);
				sheet.addMergedRegion(new CellRangeAddress(rowCounter, rowCounter, 7, 8));
			}
			createCell(row, 9,checklist , leftStyle);
			sheet.addMergedRegion(new CellRangeAddress(rowCounter, rowCounter, 9, 10));
			createCell(row, 11,clientNameNumber , style);
			
			rowCounter++;
			row=sheet.createRow(rowCounter);
			row.setHeightInPoints(80);
			createCell(row,1,date , style);
			
			XlsxWriter writer=new XlsxWriter();
			byte[] bytes;
			if(service.getServiceImage1()!=null&&!service.getServiceImage1().getUrl().equals("")){
				logger.log(Level.SEVERE,"image1 url="+service.getServiceImage1().getUrl());
				try {
				bytes=getBolbDataToByteArray(image1);
				createImageCell(workbook,sheet,bytes,row,rowCounter,2);
				} catch (Exception e) {
				// TODO Auto-generated catch block

		    		logger.log(Level.SEVERE,"error:"+e.getMessage());
				e.printStackTrace();
				}
			}else
				createCell(row, 2, "", style);
			
			if(service.getServiceImage2()!=null&&!service.getServiceImage2().getUrl().equals("")){
				logger.log(Level.SEVERE,"image2 url="+service.getServiceImage2().getUrl());
				
				try {
					bytes=getBolbDataToByteArray(image2);
					createImageCell(workbook,sheet,bytes,row,rowCounter,3);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			}else
				createCell(row, 3, "", style);
			
			if(service.getServiceImage3()!=null&&!service.getServiceImage3().getUrl().equals("")){
				logger.log(Level.SEVERE,"image3 url="+service.getServiceImage3().getUrl());
				try {
					bytes=getBolbDataToByteArray(image3);
					createImageCell(workbook,sheet,bytes,row,rowCounter,4);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
			else
				createCell(row, 4, "", style);
			
			if(service.getServiceImage4()!=null&&!service.getServiceImage4().getUrl().equals("")){
				logger.log(Level.SEVERE,"image4 url="+service.getServiceImage4().getUrl());
				try {
					bytes=getBolbDataToByteArray(image4);
					createImageCell(workbook,sheet,bytes,row,rowCounter,5);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
			else
				createCell(row, 5, "", style);
			
	
			createCell(row,6, remark, style);
			if(!PC_DoNotPrintMaterialOnSummarySR) {
				createCell(row,7,findings, leftStyle);
				sheet.addMergedRegion(new CellRangeAddress(rowCounter, rowCounter, 7, 8));
			}
			createCell(row,9,treatment, leftStyle);
			sheet.addMergedRegion(new CellRangeAddress(rowCounter, rowCounter, 9, 10));
			
			if(service.getCustomerSignature()!=null&&!service.getCustomerSignature().getUrl().equals("")){
				logger.log(Level.SEVERE,"CustomerSignature url="+service.getCustomerSignature().getUrl());
				try {
					bytes=getBolbDataToByteArray(customerSignature);
					createImageCell(workbook,sheet,bytes,row,rowCounter,11);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}					
			}
			else
				createCell(row, 11, "", style);
			rowCounter++;
		}
    }
	
	//Ashwini Patil
    public void createCell(Row row, int column,String value,XSSFCellStyle style) {
    	Cell cell=row.createCell(column);
    	cell.setCellValue(value);
    	cell.setCellStyle(style);
    }
	
	//Ashwini Patil
    private void createImageCell(XSSFWorkbook workbook,XSSFSheet sheet,byte[] bytes,Row row, int rowCount, int colCount){
		// TODO Auto-generated method stub
    	try {
    	logger.log(Level.SEVERE,"inside createImageCell");
    	logger.log(Level.SEVERE,"ROW : "+rowCount+" COL : "+colCount);
		System.out.println("ROW : "+rowCount+" COL : "+colCount);
		  
		 //Adds a picture to the workbook
		   int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
		  
		   //Returns an object that handles instantiating concrete classes
		   CreationHelper helper = workbook.getCreationHelper();
		   //Creates the top-level drawing patriarch.
		   Drawing drawing = sheet.createDrawingPatriarch();

		   //Create an anchor that is attached to the worksheet
		   ClientAnchor anchor = helper.createClientAnchor();
		   
		   anchor.setCol1(colCount); //Column A
		   anchor.setRow1(rowCount); //Row 0
		   anchor.setCol2(colCount+1); //Column B
		   anchor.setRow2(rowCount+1); //Row 1

		   //Creates a picture
		   Picture pict = drawing.createPicture(anchor, pictureIdx);
		   logger.log(Level.SEVERE,"Picture created");
		  
		   Cell cell = row.createCell(colCount);


    	}
    	catch(Exception e)
    	{
    		logger.log(Level.SEVERE,"error:"+e.getMessage());
    		e.printStackTrace();
    		
    	}
		
	}
    
    public byte[] getBolbDataToByteArray(DocumentUpload uploadedData) {
		BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
		String urlData = "";
		String fileName = "";
		byte[] arrayBytes = null;
		if (uploadedData != null&& uploadedData.getUrl() != null) {
			urlData = uploadedData.getUrl();
			logger.log(Level.SEVERE, "Doc URL :::: " + urlData);
			fileName = uploadedData.getName();
			logger.log(Level.SEVERE, "Document Name :::: " + fileName);
			String[] res = urlData.split("blobkey=", 0);
			logger.log(Level.SEVERE, "Splitted Url :::: " + res);
			String blob = res[1].trim();
			logger.log(Level.SEVERE, "Splitted Url Assigned to string  :::: "+ blob);
			BlobKey blobKey = null;
			try {
				blobKey = new BlobKey(blob);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			logger.log(Level.SEVERE, "BlobKey  :::: " + blobKey);

			// This method read bytes from blobstore
			long inxStart = 0;
			long inxEnd = 1024;
			boolean flag = false;
			byte[] bytes = null;
			do {
				try {
					bytes = blobstoreService.fetchData(blobKey, inxStart,							inxEnd);
					byteStream.write(bytes);
					if (bytes.length < 1024){
						flag = true;
					}
					inxStart = inxEnd + 1;
					inxEnd += 1025;
				} catch (Exception e) {
					flag = true;
				}

			} while (!flag);
		}
		arrayBytes=byteStream.toByteArray();
		logger.log(Level.SEVERE, " ARRAY BYTE : "+arrayBytes);
		return arrayBytes;
	}
    

	private void loadBasicData(long companyId,int customerId,String branch) {
		cust = ofy().load().type(Customer.class).filter("count", customerId).filter("companyId", companyId).first().now();
		comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		branchDt = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", branch).first().now();
		
		//Ashwini Patil Date:3-03-2023
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			if(branchDt!=null) {
						comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
			}
		}
		
		
		List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("processName", "Service").filter("configStatus", true).list();
		if(processConfigList!=null){
			for(ProcessConfiguration processConf:processConfigList){
				for(ProcessTypeDetails ptDetails:processConf.getProcessList()){
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintPhotoOnSRCopy")&&ptDetails.isStatus()==true){
						printPhotoOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintMaterialOnSRCopy")&&ptDetails.isStatus()==true){
						printMaterialOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintFindingsOnSRCopy")&&ptDetails.isStatus()==true){
						printFindingsOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintChecklistOnSRCopy")&&ptDetails.isStatus()==true){
						printChecklistOnSRCopyFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintCustomerRemark")&&ptDetails.isStatus()==true){
						printCustomerRemarkFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintTechnicianRemark")&&ptDetails.isStatus()==true){
						printTechnicianRemarkFlag=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PrintPestTreatmentOnSRCopy")&&ptDetails.isStatus()==true){
						printPestTreatmentFlag=true;
						printMaterialOnSRCopyFlag=false;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PC_SR_FINDINGS")&&ptDetails.isStatus()==true){
					
						PC_SR_FINDINGS=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PC_GenericSrCopy")&&ptDetails.isStatus()==true){
						PC_GenericSrCopy=true;
					}
					if(ptDetails.getProcessType().equalsIgnoreCase("PC_DoNotPrintMaterialOnSummarySR")&&ptDetails.isStatus()==true){
						PC_DoNotPrintMaterialOnSummarySR=true;
					}
				}
			}
		}
		
		if(printPhotoOnSRCopyFlag||printMaterialOnSRCopyFlag||printFindingsOnSRCopyFlag||printChecklistOnSRCopyFlag||printCustomerRemarkFlag||printTechnicianRemarkFlag||printPestTreatmentFlag||PC_SR_FINDINGS){
			defaultFooterFlag=false;
		}
	
	}
	/** Added by Sheetal : 22-02-2022
	 *    Des : This method will check if value entered is numeric or not **/
	private boolean isNumeric(String string) {
		int intValue;
		try{
			intValue = Integer.parseInt(string);
			return true;
		}catch(NumberFormatException e){
			System.out.println("Input String cannot be parsed to Integer");
		}
		return false;
	}
	
	public String getTechnician(Service service) {
		String stremployeName = "";
		try {
		if(service.getTechnicians()!=null && service.getTechnicians().size()>1){			
			for(int i =0;i< service.getTechnicians().size();i++){
				stremployeName = stremployeName + service.getTechnicians().get(i).getFullName();
				if(i!=service.getTechnicians().size()-1){
					stremployeName +="\n";
				}
			}
		}
		else{	
			if(service.getEmployee()!=null)
				stremployeName=service.getEmployee();
		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return stremployeName;
	}
	
	//Ashwini Patil Date:8-08-2022
	public void loadChecklistReport(List<Service> serviceList) {
//		float[] columnWidth = { 2f, 2f, 1f, 1f, 1f, 1f, 3f};
		float[] columnWidth = { 2f, 1f,1f, 1f, 1f, 1f, 1f, 3f};

		if(serviceList.size()>0) {
		if (serviceList.get(0).getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", serviceList.get(0).getCompanyId()).first().now();

		service=serviceList.get(0);
		if(companyAsaletter){
			if (comp.getUploadHeader() != null) {
				createCompanyNameAsHeader(document, comp);
				blankCells();
			}
			PdfPTable headertab=new PdfPTable(3);
			headertab.setWidthPercentage(100);
			try {
				headertab.setWidths(new float[]{33,33,33});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//By: Ashwini Patil To Print Contract Id in header on MultipleSrCopy pdf
			PdfPCell cell=pdf.getCell("Contract ID : "+serviceList.get(0).getContractCount(), font8, Element.ALIGN_LEFT, 0, 0, 0);
			cell.setPaddingBottom(5);
			cell.setBorderWidthRight(0);
			headertab.addCell(cell);
			cell=pdf.getCell("Checklist Report", font8bold, Element.ALIGN_CENTER, 0, 0, 0);
			cell.setPaddingBottom(5);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthLeft(0);
			headertab.addCell(cell);
			
			Contract con = ofy().load().type(Contract.class)
					.filter("companyId", service.getCompanyId())
					.filter("count", service.getContractCount()).first().now();
			String duration="Contract Duration: "+fmt.format(con.getStartDate())+" - "+fmt.format(con.getEndDate());
			
			cell=pdf.getCell(duration, font8, Element.ALIGN_RIGHT, 0, 0, 0);
			cell.setPaddingBottom(5);
			cell.setBorderWidthLeft(0);
			headertab.addCell(cell);
    		
			try {
				document.add(headertab);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			createHeader();
		}

		List<CustomerBranchDetails> custBranches= ofy().load().type(CustomerBranchDetails.class)
				.filter("companyId", serviceList.get(0).getCompanyId())
				.filter("cinfo.count", serviceList.get(0).getCustomerId()).list();		
		
		logger.log(Level.SEVERE,"No. of Customer Branches ="+custBranches.size());
		
		List<Integer> branchIDList=new ArrayList<Integer>();
		
		for(CustomerBranchDetails c:custBranches) {
			branchIDList.add(c.getCount());
		}
		
		//write condition for branch size 0
		
		List<CustomerBranchServiceLocation> locList=new ArrayList<CustomerBranchServiceLocation>();
		locList=ofy().load().type(CustomerBranchServiceLocation.class)
				.filter("companyId", custBranches.get(0).getCompanyId())
				.filter("custBranchId IN",branchIDList).filter("status",true).list(); //custBranches.get(b).getCount()).list();
		
		logger.log(Level.SEVERE,"No. of service locations ="+locList.size());
		
		List<Service> serviceAddressServiceList=new ArrayList<Service>();
		
		int grandTotalServices=0;
		int grandCompleteServices=0;
		int grandCancelledServices=0;
		int grandIncompleteServices=0;
		int branchCount=custBranches.size();
		for(int b=0;b<branchCount;b++) {
			List<Service> branchWiseServiceList=new ArrayList<Service>();	
			logger.log(Level.SEVERE,"branchWiseServiceList size of"+custBranches.get(b).getBusinessUnitName()+"before iteration="+branchWiseServiceList.size());
			for(Service s:serviceList) {
				if(s.getServiceBranch().equals(custBranches.get(b).getBusinessUnitName())) {
					branchWiseServiceList.add(s);
				}		
			}
			logger.log(Level.SEVERE,"branchWiseServiceList size of"+custBranches.get(b).getBusinessUnitName()+"after iteration="+branchWiseServiceList.size());			
//			if(branchWiseServiceList.size()>0) {
				
				int branchWiseTotalServices=0;
				int branchWiseCompleteServices=0;
				int branchWiseCancelledServices=0;
				int branchWiseIncompleteServices=0;
				
		    	PdfPTable mainTable=new PdfPTable(8);
				mainTable.setWidthPercentage(100);
				mainTable.setHeaderRows(1);
				try {
					mainTable.setWidths(columnWidth);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
				
				String serviceDuration="";
		    	if(!fromDate.equals("")&&!toDate.equals("")) {
		    		serviceDuration="Service Duration : "+fromDate+" - "+toDate;
		    	}		    	
		    	
		    	String headerInfo="";		    	
		    	headerInfo+="Customer Branch : "+custBranches.get(b).getBusinessUnitName();
		    	headerInfo+="\n"+serviceDuration;
		    	
		    	logger.log(Level.SEVERE,"headerInfo="+headerInfo);
				
				mainTable.addCell(pdf.getCell(headerInfo, font8, Element.ALIGN_LEFT, 0, 8, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Service Location", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Area", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Frequency", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Planned Services", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Completed Services", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Cancelled Services", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("In Process", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
				mainTable.addCell(pdf.getCell("Description", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
		
			
				logger.log(Level.SEVERE,"Printing details of branch ="+custBranches.get(b).getBusinessUnitName());
				
    			int areaCount=0;
    			
    			List<CustomerBranchServiceLocation> branchwiseLocList=new ArrayList<CustomerBranchServiceLocation>();
    			for(CustomerBranchServiceLocation loc:locList) {
    				if(loc.getCustBranchId()==custBranches.get(b).getCount())
    					branchwiseLocList.add(loc);
    			}
    			
    			logger.log(Level.SEVERE,"branchwiseLocList size ="+branchwiseLocList.size());
				
    			
    			if(branchwiseLocList!=null&&branchwiseLocList.size()>0) {
    				logger.log(Level.SEVERE,"locList.size ="+branchwiseLocList.size());
    				int locCount=branchwiseLocList.size();
    				for(int l=0;l<locCount;l++)
    				{
//    					if(locList.get(l).getCustBranchId()==custBranches.get(b).getCount()) {
    					logger.log(Level.SEVERE,"loc ="+branchwiseLocList.get(l).getServiceLocation());
    					List<Service> locationWiseServiceList=new ArrayList<Service>();	
    						for(Service bs:branchWiseServiceList) {
    							if(bs.getServiceLocation().equals(branchwiseLocList.get(l).getServiceLocation())) {
    								locationWiseServiceList.add(bs);
    							}		
    						}
    					logger.log(Level.SEVERE,"locationWiseServiceList size ="+locationWiseServiceList.size());
    					
//    					List<String> areaList=new ArrayList<String>();
    					List<Area> areaList=new ArrayList<Area>();
    					areaCount=0;
    					if(branchwiseLocList.get(l).getAreaList2()!=null && branchwiseLocList.get(l).getAreaList2().size()>0){
    						areaList = branchwiseLocList.get(l).getAreaList2();
    					}
    					else{
    						if(branchwiseLocList.get(l).getAreaList()!=null){
//        						areaList=branchwiseLocList.get(l).getAreaList();    					
//        						areaCount=branchwiseLocList.get(l).getAreaList().size();
    							areaList= getArea(branchwiseLocList.get(l).getAreaList());    					
        						
        					}
    					}
    					logger.log(Level.SEVERE,"areaList.size ="+areaCount);
        				
    					if(areaList.size()==0) {
    						mainTable.addCell(pdf.getCell(branchwiseLocList.get(l).getServiceLocation(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("No area found", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("-", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("-", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("-", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("-", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("-", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
    						mainTable.addCell(pdf.getCell("-", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);

    					}else {
    						
    						boolean plannedServiceFlag = false;
    						for(int i=0;i<areaList.size();i++) { 
    							if(areaList.get(i).getPlannedservices()>0 || !areaList.get(i).getFrequency().equals("")){
    								plannedServiceFlag = true;
    								break;
    							}
    						}

    						
    						for(int i=0;i<areaList.size();i++) { 
        						int totalServices=0;
        						int completeServices=0;
        						int cancelledServices=0;
        						int incompleteServices=0;
        						String description="";
        						logger.log(Level.SEVERE,"area "+i+"="+areaList.get(i));
        						
        						for(Service ls:locationWiseServiceList) {
        							
        							if(ls.getServiceLocationArea().equals(areaList.get(i).getArea())) {
        								if(ls.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCOMPLETED)) {
            								completeServices=completeServices+1;
            								description+=ls.getCount()+" - "+sdf1.format(ls.getServiceCompletionDate())+"\n";//+" - "+ls.getCompletedLocationName()
            							}else if(ls.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCLOSED)||ls.getStatus().equalsIgnoreCase(Service.SERVICESTATUSCANCELLED)) {
            								cancelledServices=cancelledServices+1;        								
            							}else {				
            								incompleteServices=incompleteServices+1;        								
            							}	
        							}
        						}
        						totalServices=completeServices+cancelledServices+incompleteServices;
        						
        						mainTable.addCell(pdf.getCell(branchwiseLocList.get(l).getServiceLocation(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
        						
        						PdfPTable areaTable = new PdfPTable(1);
        						areaTable.setWidthPercentage(100);
        						areaTable.setHorizontalAlignment(Element.ALIGN_CENTER);
        						
        						if(plannedServiceFlag){
        							logger.log(Level.SEVERE, "inside planned services "+plannedServiceFlag);
//        							areaTable.addCell(pdf.getCell(areaList.get(i).getArea()+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
//        							String plannedServices ="";
//        							if(areaList.get(i).getPlannedservices()>0){
//        								plannedServices = "Planned Serivices - "+areaList.get(i).getPlannedservices()+"";
//            						}
//        							if(areaList.get(i).getFrequency()!=null && !areaList.get(i).getFrequency().equals("")){
//        								plannedServices += " Frequency - "+areaList.get(i).getFrequency();
//        							}
//        							if(!plannedServices.equals(""))
//    								areaTable.addCell(pdf.getCell(plannedServices, font8, Element.ALIGN_LEFT, 0, 0, 0));
//
//            						PdfPCell areaCell = new PdfPCell(areaTable);
//            						areaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//            						mainTable.addCell(areaCell);
            						
            						/**
            						 * @author Vijay Since 10-10-2022
            						 * As per nitin sir total service = planned services
            						 * inprocess = planned service - completed - cancelled
            						 */
            						if(areaList.get(i).getPlannedservices()>0){
            							totalServices = (int) areaList.get(i).getPlannedservices();
            							incompleteServices = totalServices - completeServices - cancelledServices;
            						}
            						
            						mainTable.addCell(pdf.getCell(areaList.get(i).getArea()+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);

        						}
        						else{
            						mainTable.addCell(pdf.getCell(areaList.get(i).getArea()+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
        						}
        						mainTable.addCell(pdf.getCell(areaList.get(i).getFrequency()+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);

        						mainTable.addCell(pdf.getCell(totalServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(completeServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(cancelledServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(incompleteServices+"", font8, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(description, font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);
        						
        						branchWiseTotalServices+=totalServices;
        						branchWiseCompleteServices+=completeServices;
        						branchWiseCancelledServices+=cancelledServices;
        						branchWiseIncompleteServices+=incompleteServices;
        						
        						
        					}//end of area loop
    					}
    					
    						if(l==locCount-1) {
    							logger.log(Level.SEVERE,"In last location record");
        						
    							mainTable.addCell(pdf.getCell("Total", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(branchWiseTotalServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(branchWiseCompleteServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(branchWiseCancelledServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell(branchWiseIncompleteServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
        						mainTable.addCell(pdf.getCell("", font8bold, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);

        						grandTotalServices+=branchWiseTotalServices;
        						grandCompleteServices+=branchWiseCompleteServices;
        						grandCancelledServices+=branchWiseCancelledServices;
        						grandIncompleteServices+=branchWiseIncompleteServices;
        						
        						
    						}
    					
    				}//end of loc loop
    			}//end of loc if
    			else {
    				mainTable.addCell(pdf.getCell("No service location and area found", font8, Element.ALIGN_LEFT, 0, 8, 0)).setBorder(Rectangle.BOX);			
    			}
    			
    			try {
					document.add(mainTable);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    			if(b!=branchCount-1)
    				document.newPage();
				branchWiseServiceList.clear();
//			} //end of branchlist if
			
			
		} //end of branch loop
		
		
		PdfPTable mainTable=new PdfPTable(8);
		mainTable.setWidthPercentage(100);
		
		try {
			mainTable.setWidths(columnWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mainTable.addCell(pdf.getCell("\nGrand Total", font8bold, Element.ALIGN_LEFT, 0, 3, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("\n"+grandTotalServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("\n"+grandCompleteServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("\n"+grandCancelledServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("\n"+grandIncompleteServices+"", font8bold, Element.ALIGN_CENTER, 0, 0, 0)).setBorder(Rectangle.BOX);
		mainTable.addCell(pdf.getCell("\n"+"", font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(Rectangle.BOX);								
		try {
			document.add(mainTable);
			logger.log(Level.SEVERE,"Grand total added");
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	else {
		PdfPTable mainTable=new PdfPTable(1);
		mainTable.setWidthPercentage(100);
		
		mainTable.addCell(pdf.getCell("No services found", font8, Element.ALIGN_LEFT, 0, 2, 0)).setBorder(Rectangle.BOX);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	}

	private List<Area> getArea(ArrayList<String> areaList) {
		List<Area> arealist = new ArrayList<Area>();
		for(String areaName : areaList){
			Area area = new Area();
			area.setArea(areaName);
			area.setFrequency("");
			area.setPlannedservices(0);
			arealist.add(area);
		}
		return arealist;
	}

}
