package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.friends.FrendsServiceCardPdf;
import com.slicktechnologies.shared.Contract;

public class ServiceVoucherServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6378425103971073218L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			resp.setContentType("application/pdf");
			
			try {
				String stringid = req.getParameter("Id").trim();
				long count = Long.parseLong(stringid);
				

				ServiceVoucherPdf voucherpdf = new ServiceVoucherPdf();
				voucherpdf.document = new Document();
				Document document = voucherpdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,
						resp.getOutputStream());

				

				document.open();
				voucherpdf.loadServiceVoucher( count);
				voucherpdf.createPdf();
				document.close();

			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}
	
}
