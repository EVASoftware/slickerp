package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.google.appengine.api.search.query.QueryParser.phrase_return;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPRow;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.Pfm2afm;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.complain.ComplainList;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.SalesQuotation;
import com.slicktechnologies.shared.common.workorder.WorkOrder;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
public class JobReportcumReceiptPdf {


	public Document document;
	Logger logger = Logger.getLogger("Name of logger");
	boolean isCompany = false;
	ProcessConfiguration processConfig;
	boolean forJobReportcumReceiptFlag = false;
	
	Company comp; 
	Customer cust;
	Complain complain;
	String str;
//	String sum;
	double sum,chargeAmt,recAmt,amount;
	
	Phrase chunk;
	PdfPCell pdfsrno1,pdfdate,pdfamt,pdfremark;

	float[] colWidth = {50f,50f};
	float[] colWidth1 = {0.2f,0.1f,0.3f};
	float[] colWidth2 = {0.3f,0.2f,0.3f,0.3f};
	float[] colWidth3 = {0.1f,0.6f};
	float[] colWidth4 = {0.2f,0.4f,0.1f,0.5f};
	float[] colWidth5 = {0.3f,0.7f};
	float[] colWidth6 = {0.1f,0.7f,0.1f};
	float[] colWidth7 = {0.4f,0.3f,0.2f};
	float[] colWidth8 = {0.2f,0.3f,0.1f};
	float[] colWidth9 = {0.2f,0.3f,0.3f};
	float[] colWidth10 = {70f,30f};
	float[] colWidth11 = {0.3f,1.1f};
	
	
	float round = 0.00f;
	DecimalFormat df = new DecimalFormat("0.00");
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");
	
	private Font font16boldul, font12bold, font8bold, font8, font16bold,
	font12boldul, font12, font14bold, font10, font10bold, font10boldul,
	font9, font9bold, font14boldul;
	
	public JobReportcumReceiptPdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);

	}
	
	public void setpdfjob(Long count) {
		
		complain = ofy().load().type(Complain.class).id(count).now();

		if (complain.getCompanyId() != null) {
			comp = ofy().load().type(Company.class)
					.filter("companyId", complain.getCompanyId()).first().now();
		}

		if (complain.getCompanyId() != null) {
			cust = ofy().load().type(Customer.class)
					.filter("companyId", complain.getCompanyId())
					.filter("count", complain.getPersoninfo().getCount())
					.first().now();
		}
		
		if (complain.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", complain.getCompanyId())
					.filter("processName", "Complain")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("JobReportcumReceiptForRealGuard")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						forJobReportcumReceiptFlag = true;
					}
				}
			}
		}
		
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		}
	
	public void createPdf() {
	createHeading();
	createCustInfo();
	createHeadingTable();
	createTable();
//	createTable1();
	createTable1();
//	createExtraChargesTable();
	createTable2();
	createInwordsTable();
	createHardCodedInfo();
	createHard1CodedInfo();
	createHard2CodedInfo();
	createFeedbackInfo();
	
	
	String amountInWord = ServiceInvoicePdf.convert(sum);
	
	}


	public void createHeading(){
		
		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);	
		
		String str;
		str = "JOB-REPORT CUM RECEIPT.";
		 
		Phrase strp = new Phrase(str,font12boldul);
		PdfPCell strpcell = new PdfPCell(strp);
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPTable strtable = new PdfPTable(1);
		strtable.addCell(strpcell);
		strtable.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(strtable);
		cell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.addCell(blankcell);
		table.addCell(cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createCustInfo() {
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);	
		
		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		
		//***********left table starts***************
		String str = "";
		str = "CUSTOMER NAME";	
		
		Phrase company = null;
		if(cust.isCompany()==true){
			company = new Phrase(" "+cust.getCompanyName(),font10bold);	
		}
		else{
//			company = new Phrase(""+str+","+" \n"+cust.getFullname(),font10bold);
			company = new Phrase(""+str+" : "+cust.getFullname(),font10bold);
		}
		PdfPCell companycell = new PdfPCell(company);	
		companycell.setBorder(0);
		companycell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		//********Customer Address**************
		String custAdd1 = "";
		String custFullAdd1 = "";
		
		if (cust.getAdress() != null) {
			logger.log(Level.SEVERE,"111111111111111111");
			if (!cust.getAdress().getAddrLine2().equals("")) {
				logger.log(Level.SEVERE,"22222222222222222");
				logger.log(Level.SEVERE,"hiii"+cust.getAdress().getAddrLine2());
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE,"333333333333333");
					custAdd1 = cust.getAdress().getAddrLine1() 
							+ "," + "\n" 
							+ cust.getAdress().getAddrLine2() 
							+ "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE,"4444444444444");
					custAdd1 = cust.getAdress().getAddrLine1()
							+ "," + "\n"
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				logger.log(Level.SEVERE,"5555555555555");
				if (!cust.getAdress().getLandmark().equals("")) {
					logger.log(Level.SEVERE,"66666666666666666");
					custAdd1 = cust.getAdress().getAddrLine1() + "," + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					logger.log(Level.SEVERE,"77777777777777777");
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null) {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + "\n"
						+ cust.getAdress().getLocality() + " "+ "\n" +"Pin : "
						+ cust.getAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}
		}
		
		Phrase custAddInfo = new Phrase("ADDRESS : "+custFullAdd1, font9bold);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		Phrase contactno = null;
		if (cust.getLandline() !=null && cust.getCellNumber1() != null) {
			contactno = new Phrase("CONTACT NO. : "+cust.getLandline()+" / "+cust.getCellNumber1(),font9bold);
		}
		PdfPCell contactnocell = new PdfPCell(contactno);
		contactnocell.setBorder(0);
		contactnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable lefttable = new PdfPTable(1);
		
		lefttable.addCell(companycell);
		lefttable.addCell(blcell);
		lefttable.addCell(custAddInfoCell);
		lefttable.addCell(blcell);
		lefttable.addCell(contactnocell);
		
		lefttable.setWidthPercentage(100f);
		
		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);
		
		
		//***********right table starts***************
		PdfPTable righttable = new PdfPTable(3);
		
		Phrase receipt = new Phrase("RECEIPT NO.",font9bold);
		PdfPCell receiptcell = new PdfPCell(receipt);
		receiptcell.setBorder(0);
		receiptcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase ticket = null;
		logger.log(Level.SEVERE,"RECEIPT NO"+complain.getCount());
		if(complain.getCount() !=0){
//			logger.log(Level.SEVERE,"RECEIPT NO"+complain.getTicketId());
			ticket = new Phrase(" "+complain.getCount(),font9bold);
		}
		PdfPCell ticketcell = new PdfPCell(ticket);
		ticketcell.setBorder(0);
		ticketcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase complentDate = new Phrase("COMPLAINT DATE",font9bold);
		PdfPCell complentDatecell = new PdfPCell(complentDate);
		complentDatecell.setBorder(0);
		complentDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase dt = null;
		if(complain.getDate() !=null){
			dt = new Phrase(" "+fmt.format(complain.getComplainDate()),font9bold);
		}
		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase attDate = new Phrase("ATTENDING DATE",font9bold);
		PdfPCell attDatecell = new PdfPCell(attDate);
		attDatecell.setBorder(0);
		attDatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase dt1 = null;
		if(complain.getDueDate() !=null){
			dt1 = new Phrase(" "+fmt.format(complain.getDueDate()),font9bold);
		}
		PdfPCell dt1cell = new PdfPCell(dt1);
		dt1cell.setBorder(0);
		dt1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase brandname = new Phrase("BRAND NAME",font9bold);
		PdfPCell brandnamecell = new PdfPCell(brandname);
		brandnamecell.setBorder(0);
		brandnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase brand = null;
		if(complain.getBrandName() !=null){
			brand = new Phrase(" "+complain.getBrandName(),font9bold);
		}
		PdfPCell brandcell = new PdfPCell(brand);
		brandcell.setBorder(0);
		brandcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase techname = new Phrase("TECHNICIAN",font9bold);
		PdfPCell technamecell = new PdfPCell(techname);
		technamecell.setBorder(0);
		technamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase technician = null;
		if(complain.getAssignto() !=null){
			technician = new Phrase(" "+complain.getAssignto(),font9bold);
		}
		PdfPCell techniciancell = new PdfPCell(technician);
		techniciancell.setBorder(0);
		techniciancell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		righttable.addCell(receiptcell);
		righttable.addCell(colcell);
		righttable.addCell(ticketcell);
		
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		
		righttable.addCell(complentDatecell);
		righttable.addCell(colcell);
		righttable.addCell(dtcell);
		
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		
		righttable.addCell(attDatecell);
		righttable.addCell(colcell);
		righttable.addCell(dt1cell);
		
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		
		righttable.addCell(brandnamecell);
		righttable.addCell(colcell);
		righttable.addCell(brandcell);
		
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		
		righttable.addCell(technamecell);
		righttable.addCell(colcell);
		righttable.addCell(techniciancell);
		
		try {
			righttable.setWidths(colWidth1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		righttable.setWidthPercentage(100f);
		
		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);
		
		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);
		
		try {
			table.setWidths(colWidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createInwordsTable() {
		
		PdfPTable rstable = new PdfPTable(2);
		
		try {
			rstable.setWidths(colWidth11);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		rstable.setWidthPercentage(100f);
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);

		Phrase rs = new Phrase("RUPEES IN WORDS :-", font9bold);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String amountInWord = ServiceInvoicePdf.convert(sum);
		Phrase bl1 = new Phrase(""+ amountInWord + " Only ", font10bold);
		PdfPCell bl1cell = new PdfPCell(bl1); 
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		rstable.addCell(rscell);
		rstable.addCell(bl1cell);
		
		PdfPCell cell = new PdfPCell(rstable);
		cell.setBorder(0);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(cell);
		amttable.addCell(blcell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createHardCodedInfo() {
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);
		
		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1); 
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		
		String str = "";
		str = "TECHNICIAN SIGN & STAMP";	
		
		String str1 = "";
		str1 = "  CUSTOMER SIGN & STAMP";
		
		Phrase strp = new Phrase(str,font10bold);	
		PdfPCell strpcell = new PdfPCell(strp);	
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase str1p = new Phrase(str1,font10bold);	
		PdfPCell str1pcell = new PdfPCell(str1p);	
		str1pcell.setBorder(0);
		str1pcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		PdfPTable stamptable = new PdfPTable(4);
		stamptable.addCell(strpcell);
		stamptable.addCell(bl1cell);
		stamptable.addCell(str1pcell);
		stamptable.addCell(bl1cell);
		
		try {
			stamptable.setWidths(colWidth2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		stamptable.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(stamptable);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e){
			e.printStackTrace();
		}
	}
		
	private void createHard1CodedInfo() {
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);
		
		Phrase bl1 = new Phrase(" ");
		PdfPCell bl1cell = new PdfPCell(bl1); 
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);
		
		String str = "";
		str = "FOR REAL-GUARD";	
		
		Phrase strp = new Phrase(str,font9);	
		PdfPCell strpcell = new PdfPCell(strp);	
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String str1 = "";
		str1 = "    FOR";	
		
		Phrase str1p = new Phrase(str1,font9);	
		PdfPCell str1pcell = new PdfPCell(str1p);	
		str1pcell.setBorder(0);
		str1pcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		PdfPTable stamptable = new PdfPTable(4);
		stamptable.addCell(strpcell);
		stamptable.addCell(bl1cell);
		stamptable.addCell(str1pcell);
		stamptable.addCell(bl1cell);
		
		try {
			stamptable.setWidths(colWidth4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		stamptable.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(stamptable);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e){
			e.printStackTrace();
		}
	}	
		
	
	private void createHard2CodedInfo() {
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);
		
		String str = "";
		str = "NOTE : ";	
		
		Phrase strp = new Phrase(str,font10boldul);	
		PdfPCell strpcell = new PdfPCell(strp);	
		strpcell.setBorder(0);
		strpcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String str1 = "";
		str1 = "* BATTERY NOT COVERED IN WARRANTY PERIOD, SO PLEASE REPLACE THE BATTERY BEFORE"+"\n"+"   MAKING THE COMPLAINT."+"\n"
				+"\n"+"* AFTER MAKING THE COMPLAINT MAKE A CALL TO COMPANY.   ";	
		
		Phrase str1p = null;
		if(comp.getCellNumber1() !=null && comp.getCellNumber2() !=null){
			str1p = new Phrase(""+str1+comp.getCellNumber1()+" / "+comp.getCellNumber2(),font9bold);
		}
		PdfPCell str1pcell = new PdfPCell(str1p);	
		str1pcell.setBorder(0);
		str1pcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		PdfPTable notetable = new PdfPTable(2);
		notetable.addCell(strpcell);
		notetable.addCell(str1pcell);
		
		try {
			notetable.setWidths(colWidth3);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		notetable.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(notetable);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e){
			e.printStackTrace();
		}	
	}	
		
	private void createFeedbackInfo() {
		
	Phrase feedback = new Phrase("CUSTOMER FEEDBACK : ",font10boldul);	
	PdfPCell feedbackcell = new PdfPCell(feedback);	
	feedbackcell.setBorder(0);
	feedbackcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	System.out.println("FEEDBACK::::::::::"+complain.getCustomerFeedback());
	Phrase bl1 = null;
	if(complain.getCustomerFeedback() !=null){
		bl1 = new Phrase(" "+complain.getCustomerFeedback(),font10bold);	
	}else{
		bl1 = new Phrase(" ",font10bold);
	}
	 
	PdfPCell bl1cell = new PdfPCell(bl1); 
	bl1cell.setBorderWidthLeft(0);
	bl1cell.setBorderWidthRight(0);
	bl1cell.setBorderWidthTop(0);	
		
	PdfPTable feedbacktable = new PdfPTable(2);	
	feedbacktable.addCell(feedbackcell);
	feedbacktable.addCell(bl1cell);
	
	try {
		feedbacktable.setWidths(colWidth5);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	feedbacktable.setWidthPercentage(100);
	
	PdfPCell cell = new PdfPCell(feedbacktable);
	cell.setBorder(0);
	
	PdfPTable parenttable = new PdfPTable(1);
	parenttable.addCell(cell);
//	parenttable.addCell(blcell);
	parenttable.setWidthPercentage(100);
	
	try {
		document.add(parenttable);
	} catch (DocumentException e){
		e.printStackTrace();
		}	
	}	
	
	private void createTable() {
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable lefttable = new PdfPTable(1);
		lefttable.setWidthPercentage(100f);	
		
		Phrase srno = new Phrase("SR.NO.", font1);
		PdfPCell srnocell = new PdfPCell(srno);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		lefttable.addCell(srnocell);
		
//		System.out.println("inside ComplainList ::::::::::::::"+complain.getComplainList().size());
//		for (int i=0;i<complain.getComplainList().size();i++) {
//			chunk = new Phrase(complain.getComplainList().get(i).getSrNo() + "", font8);
//			pdfsrno1 = new PdfPCell(chunk);
//			pdfsrno1.setHorizontalAlignment(Element.ALIGN_CENTER);
//			lefttable.addCell(pdfsrno1);
//			}

		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);
		
		
	//***********center table****************	
		PdfPTable centertable = new PdfPTable(1);
		centertable.setWidthPercentage(100f);	
		
		Phrase desc = new Phrase("DESCRIPTION", font1);
		PdfPCell desccell = new PdfPCell(desc);
		desccell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		centertable.addCell(desccell);
		
		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setBorder(0);

		
	//***********right table****************	
		PdfPTable righttable = new PdfPTable(1);
		righttable.setWidthPercentage(100f);		
		
		Phrase amt = new Phrase("AMOUNT", font1);
		PdfPCell amtcell = new PdfPCell(amt);
		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		righttable.addCell(amtcell);
		
//		for (int i = 0; i < this.complain.getComplainList().size(); i++) {
//
//			chunk = new Phrase(complain.getComplainList().get(i).get + "", font8);
//			pdfamt = new PdfPCell(chunk);
//			pdfamt.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			righttable.addCell(pdfamt);
//		}
		
		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);
		
		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);
		
		try {
			table.setWidths(colWidth6);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
//		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e){
			e.printStackTrace();
			}	
		}	
	
	
	/**************************************************************/	
//	private void createTable1() {
//		
//		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
//		PdfPTable left1table = new PdfPTable(1);
//		left1table.setWidthPercentage(100f);	
//		
//		Phrase srno = new Phrase(" ", font1);
//		PdfPCell srnocell = new PdfPCell(srno);
//		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		left1table.addCell(srnocell);
//		
//		PdfPCell left1cell = new PdfPCell(left1table);
//		left1cell.setBorder(0);
//
//		
//		/****************************Description tbale***********************************/
//		PdfPTable desctable = new PdfPTable(1);
//		desctable.setWidthPercentage(100f);	
//		
//		Phrase bl = new Phrase(" ");
//		PdfPCell blcell = new PdfPCell(bl); 
//		blcell.setBorder(0);
//		
//		//**********left table of Description tbale****************
//		PdfPTable lefttable = new PdfPTable(2);
//		lefttable.setWidthPercentage(100f);	
//		
//		Phrase spacification = new Phrase("TECHNICAL SPECIFICATION : ", font1);
//		PdfPCell spaccell = new PdfPCell(spacification);
//		spaccell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase bl1 = new Phrase(" ");
//		PdfPCell bl1cell = new PdfPCell(bl1); 
//		bl1cell.setBorderWidthLeft(0);
//		bl1cell.setBorderWidthRight(0);
//		bl1cell.setBorderWidthTop(0);	
//		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				
//		Phrase ser = new Phrase("SAFE REPAIRE AND SERVICES", font1);
//		PdfPCell sercell = new PdfPCell(ser);
//		sercell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase any = new Phrase("ANY OTHER PART", font1);
//		PdfPCell anycell = new PdfPCell(any);
//		anycell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		Phrase charges = new Phrase("LABOUR CHARGES ONLY", font9bold);
//		PdfPCell chargescell = new PdfPCell(charges);
//		chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		
//		lefttable.addCell(spaccell);
//		lefttable.addCell(bl1cell);
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		lefttable.addCell(sercell);
//		lefttable.addCell(bl1cell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(anycell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		
//		lefttable.addCell(charges);
//		lefttable.addCell(blcell);
//		
//		PdfPCell leftcell = new PdfPCell(lefttable);
//		leftcell.setBorder(0);
//		
//		
//		//**********center table of Description tbale****************
//		PdfPTable centertable = new PdfPTable(1);
//		centertable.setWidthPercentage(100f);
//		
//		Phrase part = new Phrase("REPLACED FAULTY PART :- ", font1);
//		PdfPCell partell = new PdfPCell(part);
//		partell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	
//		centertable.addCell(partell);
//		
//		PdfPCell centercell = new PdfPCell(centertable);
//		centercell.setBorder(0);
//				
//		
//		//***********right tableof Description tbale****************	
//		PdfPTable righttable = new PdfPTable(1);
//		righttable.setWidthPercentage(100f);		
//				
//		String str="";
//		str = "* BATTERY"+"\n"
//				+ "* CIRCUIT KEYBOARD"+"\n"
//				+ "* BATTERY ADAPTER"+"\n"
//				+ "* SOLONOID"+"\n"
//				+ "* CIRCUIT MAIN BOARD";
//		
//		Phrase parts = new Phrase(str, font1);
//		PdfPCell partsell = new PdfPCell(parts);
//		partsell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
////		righttable.addCell(bl3cell);
//		righttable.addCell(partsell);
//				
//		PdfPCell rightcell = new PdfPCell(righttable);
//		rightcell.setBorder(0);	
//		
//		desctable.addCell(leftcell);
//		desctable.addCell(centercell);
//		desctable.addCell(rightcell);
//		
////		try {
////			desctable.setWidths(colWidth7);
////		} catch (DocumentException e1) {
////			e1.printStackTrace();
////		}
//		desctable.setWidthPercentage(100f);
//		
//		PdfPCell desccell = new PdfPCell(desctable);
//		desccell.setBorder(0);
//		
//		//*********************************
//		PdfPTable right1table = new PdfPTable(1);
//		right1table.setWidthPercentage(100f);		
//		
//		Phrase amt = new Phrase(" ", font1);
//		PdfPCell amtcell = new PdfPCell(amt);
//		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		
//		right1table.addCell(amtcell);
//		
//		PdfPCell right1cell = new PdfPCell(right1table);
//		right1cell.setBorder(0);	
//		
//		
//		PdfPTable table = new PdfPTable(3);
//		table.addCell(left1cell);
//		table.addCell(desccell);
//		table.addCell(right1cell);
//		
//		try {
//			table.setWidths(colWidth6);
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
//		table.setWidthPercentage(100);
//		
//		PdfPCell cell = new PdfPCell(table);
//		cell.setBorder(0);
//		
//		PdfPTable parenttable = new PdfPTable(1);
//		parenttable.addCell(cell);
//		parenttable.addCell(blcell);
//		parenttable.setWidthPercentage(100);
//		
//		try {
//			document.add(parenttable);
//		} catch (DocumentException e){
//			e.printStackTrace();
//			}	
//		
//	}
	/**************************************************************/
	
	
	private void createTable1() {
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank); 
//		blankcell.setBorder(0);
		blankcell.setBorderWidthTop(0);
		blankcell.setBorderWidthBottom(0);
		
		
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable left1table = new PdfPTable(1);
		left1table.setWidthPercentage(100f);	
		
		Phrase srno = new Phrase(" ", font1);
		PdfPCell srnocell = new PdfPCell(srno);
//		srnocell.setBorderWidthBottom(0);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		left1table.addCell(srnocell);
		
		PdfPCell left1cell = new PdfPCell(left1table);
		left1cell.setBorder(0);	
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);
		
		
		/*****************************Description table****************************************/
		
		//*************upper table*****************
		PdfPTable uppertable = new PdfPTable(3);
		
		try {
			uppertable.setWidths(colWidth8);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		uppertable.setWidthPercentage(100f);	
		
		Phrase spacification = new Phrase("TECHNICAL SPECIFICATION : ", font1);
		PdfPCell spaccell = new PdfPCell(spacification);
		spaccell.setBorder(0);
		spaccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase bl1 = new Phrase(" "+complain.getDestription(),font9bold);
		PdfPCell bl1cell = new PdfPCell(bl1); 
		bl1cell.setBorderWidthLeft(0);
		bl1cell.setBorderWidthRight(0);
		bl1cell.setBorderWidthTop(0);	
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
				
		Phrase ser = new Phrase("SAFE REPAIRE AND SERVICES.", font1);
		PdfPCell sercell = new PdfPCell(ser);
		sercell.setBorder(0);
		sercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		uppertable.addCell(blcell);
		uppertable.addCell(blcell);
		uppertable.addCell(blcell);
		
		uppertable.addCell(spaccell);
		uppertable.addCell(bl1cell);
		uppertable.addCell(blcell);
		
		uppertable.addCell(sercell);
		uppertable.addCell(blcell);
		uppertable.addCell(blcell);
		
		PdfPCell cell1 = new PdfPCell(uppertable);
		cell1.setBorder(0);
		
		
		//*************middle table*****************
		PdfPTable middletable = new PdfPTable(1);
		middletable.setWidthPercentage(100f);	
		
		Phrase part = new Phrase("REPLACED FAULTY PART :- ", font9bold);
		PdfPCell partcell = new PdfPCell(part);
		partcell.setBorder(0);
		partcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		//********************parts table************************
		PdfPTable blanktable = new PdfPTable(1);
		blanktable.addCell(blcell);
		blanktable.addCell(blcell);
		blanktable.addCell(blcell);
		blanktable.addCell(blcell);
		blanktable.addCell(blcell);
		blanktable.setWidthPercentage(100f);
		
		PdfPCell blkcell = new PdfPCell(blanktable);
		blkcell.setBorder(0);
		
		Phrase part1 = new Phrase("* BATTERY", font1);
		Phrase part2 = new Phrase("* CIRCUIT KEYBOARD", font1);
		Phrase part3 = new Phrase("* BATTERY ADAPTER", font1);
		Phrase part4 = new Phrase("* SOLONOID", font1);
		Phrase part5 = new Phrase("* CIRCUIT MAIN BOARD", font1);
	
		PdfPCell part1cell = new PdfPCell(part1);
		part1cell.setBorder(0);
		part1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell part2cell = new PdfPCell(part2);
		part2cell.setBorder(0);
		part2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell part3cell = new PdfPCell(part3);
		part3cell.setBorder(0);
		part3cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell part4cell = new PdfPCell(part4);
		part4cell.setBorder(0);
		part4cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell part5cell = new PdfPCell(part5);
		part5cell.setBorder(0);
		part5cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable partstable = new PdfPTable(1);
		partstable.addCell(part1cell);
		partstable.addCell(part2cell);
		partstable.addCell(part3cell);
		partstable.addCell(part4cell);		
		partstable.addCell(part5cell);
		partstable.setWidthPercentage(100f);
		
		PdfPCell partscell = new PdfPCell(partstable);
		partscell.setBorder(0);
//		partscell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		
		PdfPTable table = new PdfPTable(2);
		table.addCell(blkcell);
		table.addCell(partscell);
		
		try {
			table.setWidths(colWidth10);
		} catch (DocumentException e1) {
		e1.printStackTrace();
			}
		table.setWidthPercentage(100);
		
		PdfPCell tablecell = new PdfPCell(table);
		tablecell.setBorder(0);
		
		middletable.addCell(partcell);
		middletable.addCell(tablecell);
		
		PdfPCell cell2 = new PdfPCell(middletable);
		cell2.setBorder(0);
		
		
		//*************lower table*****************
		PdfPTable lowertable = new PdfPTable(3);
		
		try {
		lowertable.setWidths(colWidth9);
		} catch (DocumentException e1) {
		e1.printStackTrace();
		}
		lowertable.setWidthPercentage(100f);	
		
		Phrase any = new Phrase(" * ANY OTHER PART  :-", font1);
		PdfPCell anycell = new PdfPCell(any);
		anycell.setBorder(0);
		anycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		lowertable.addCell(anycell);
		
		System.out.println("Complain size:::::::::::::::"+complain.getComplainList().size());
		for(int i=0;i<this.complain.getComplainList().size();i++){
			
			chunk = new Phrase(complain.getComplainList().get(i).getRemark()+"",font9bold);
			pdfremark=new PdfPCell(chunk);
			pdfremark.setBorderWidthLeft(0);
			pdfremark.setBorderWidthRight(0);
			pdfremark.setBorderWidthTop(0);
			pdfremark.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			lowertable.addCell(pdfremark);
			
		}
		
//		Phrase bl2 = new Phrase(" "+complain.getRemark(),font9bold);
//		PdfPCell bl2cell = new PdfPCell(bl2); 
//		bl2cell.setBorderWidthLeft(0);
//		bl2cell.setBorderWidthRight(0);
//		bl2cell.setBorderWidthTop(0);	
//		bl2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//		lowertable.addCell(anycell);
//		lowertable.addCell(bl2cell);
		lowertable.addCell(blcell);
		
		
		PdfPCell cell3 = new PdfPCell(lowertable);
		cell3.setBorder(0);
		
		
		//***************charge table********************
		PdfPTable chargestable = new PdfPTable(1);
		chargestable.setWidthPercentage(100f);
		
		Phrase charges = new Phrase("LABOUR CHARGES ONLY", font9bold);
		PdfPCell chargescell = new PdfPCell(charges);
//		chargescell.setBorderWidthTop(0);
		chargescell.setBorder(0);
		chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);

		chargestable.addCell(chargescell);

		PdfPCell cell4 = new PdfPCell(chargestable);
		cell4.setBorder(0);
	
		
		PdfPTable desctable = new PdfPTable(1);
		desctable.addCell(cell1);
		desctable.addCell(cell2);
//		desctable.addCell(table);
		desctable.addCell(cell3);
		desctable.addCell(blcell);
		desctable.addCell(cell4);
		desctable.setWidthPercentage(100f);
		
		PdfPCell desccell = new PdfPCell(desctable);
		desccell.setBorder(0);
		
		
		//*********************************
		PdfPTable right1table = new PdfPTable(1);
		right1table.setWidthPercentage(100f);	
		
		Phrase amt = null;
		if(complain.getAmountReceived()!=null){
			System.out.println("Amount::::::::::"+complain.getAmountReceived());
			amt = new Phrase(" "+complain.getAmountReceived(), font1);
		}
		PdfPCell amtcell = new PdfPCell(amt);
		amtcell.setBorderWidthBottom(0);
		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		/*******************rough part***********************/
		Phrase labour = null;
		if(complain.getExtraCharges()!=null){
			System.out.println("Charges::::::::::"+complain.getExtraCharges());
			labour = new Phrase(" "+complain.getExtraCharges(), font1);
		}
		PdfPCell labourcell = new PdfPCell(labour);
		labourcell.setBorderWidthTop(0);
		labourcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		right1table.addCell(amtcell);
		
		
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);
		right1table.addCell(blankcell);


		right1table.addCell(labourcell);
		
		
		PdfPCell right1cell = new PdfPCell(right1table);
		right1cell.setBorder(0);	
		
		
		PdfPTable table1 = new PdfPTable(3);
		table1.addCell(left1cell);
		table1.addCell(desccell);
		table1.addCell(right1cell);
		
		try {
			table1.setWidths(colWidth6);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table1.setWidthPercentage(100);
		
		PdfPCell cell = new PdfPCell(table1);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
//		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
			} catch (DocumentException e){
			e.printStackTrace();
			}	
	}
	
	
	private void createHeadingTable() {
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);
		
		Phrase rec_inv = new Phrase("RECEIPT / INVOICE", font9bold);
		PdfPCell rec_invcell = new PdfPCell(rec_inv);
		rec_invcell.setBorder(0);
		rec_invcell.setHorizontalAlignment(Element.ALIGN_CENTER);	
		
		PdfPTable rec_invtable = new PdfPTable(1);
		rec_invtable.addCell(rec_invcell);
		rec_invtable.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(rec_invtable);
		cell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.setWidthPercentage(100);
		
		try {
			document.add(table);
			} catch (DocumentException e){
			e.printStackTrace();
			}	
		}
	
	
/**************************************************************/	
//	private void createExtraChargesTable() {
//		
//		Phrase bl = new Phrase(" ");
//		PdfPCell blcell = new PdfPCell(bl);
//		blcell.setBorder(0);
//		
//		//*****************left table*********************
//				Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
//				PdfPTable lefttable = new PdfPTable(1);
//				lefttable.setWidthPercentage(100f);	
//		
//				Phrase srno = new Phrase(" ", font1);
//				PdfPCell srnocell = new PdfPCell(srno);
//				srnocell.setBorderWidthTop(0);
//				srnocell.setBorderWidthBottom(0);
////				srnocell.setBorderWidth(0);
//				srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				
//				lefttable.addCell(srnocell);
//				
//				PdfPCell leftcell = new PdfPCell(lefttable);
//				leftcell.setBorder(0);	
//				
//		
//		//***************charge table********************	
//				PdfPTable chargestable = new PdfPTable(1);
//				chargestable.setWidthPercentage(100f);
//				
//				Phrase charges = new Phrase("LABOUR CHARGES ONLY", font9bold);
//				PdfPCell chargescell = new PdfPCell(charges);
////				chargescell.setBorderWidthTop(0);
//				chargescell.setBorder(0);
//				chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
//
//				chargestable.addCell(chargescell);
//
//				PdfPCell cell4 = new PdfPCell(chargestable);
//				cell4.setBorder(0);
//				
//				
//		//***************right table********************
//				PdfPTable righttable = new PdfPTable(1);
//				righttable.setWidthPercentage(100f);		
//				
//				Phrase labour = null;
//				if(complain.getExtraCharges()!=null){
//					System.out.println("Charges::::::::::"+complain.getExtraCharges());
//					labour = new Phrase(" "+complain.getExtraCharges(), font1);
//				}
//				PdfPCell labourcell = new PdfPCell(labour);
////				labourcell.setBorderWidthTop(0);
//				labourcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//				
//				righttable.addCell(labourcell);
//				
//				PdfPCell rightcell = new PdfPCell(righttable);
//				rightcell.setBorder(0);		
//		
//				
//				PdfPTable table = new PdfPTable(3);
//				table.addCell(leftcell);
//				table.addCell(cell4);
//				table.addCell(rightcell);
//				
//				try {
//					table.setWidths(colWidth6);
//				} catch (DocumentException e1) {
//					e1.printStackTrace();
//				}
//				table.setWidthPercentage(100);
//				
//				PdfPCell cell = new PdfPCell(table);
//				cell.setBorder(0);
//				
//				PdfPTable parenttable = new PdfPTable(1);
//				parenttable.addCell(cell);
//				parenttable.setWidthPercentage(100);
//				
//				try {
//					document.add(parenttable);
//					} catch (DocumentException e){
//					e.printStackTrace();
//				}
//			}
	/**************************************************************/	
	
	
	private void createTable2(){
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank); 
		blankcell.setBorder(0);
		
		//*****************left table*********************
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		PdfPTable lefttable = new PdfPTable(1);
		lefttable.setWidthPercentage(100f);	
		
		Phrase srno = new Phrase(" ", font1);
		PdfPCell srnocell = new PdfPCell(srno);
		srnocell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		lefttable.addCell(srnocell);
		
		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);	
		
		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl); 
		blcell.setBorder(0);	
		
		//**********center table****************
		PdfPTable centertable = new PdfPTable(1);
		centertable.setWidthPercentage(100f);
		
		Phrase part = new Phrase("TOTAL", font1);
		PdfPCell partell = new PdfPCell(part);
		partell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
		centertable.addCell(partell);
		
		PdfPCell centercell = new PdfPCell(centertable);
		centercell.setBorder(0);
		
		//**********right table****************
		PdfPTable righttable = new PdfPTable(1);
		righttable.setWidthPercentage(100f);	
	
		/********************************************/		
		double amount = 0;
		
		if(complain.getExtraCharges()!=null && complain.getAmountReceived()!=null){
			System.out.println(" extra charges and total::::::::::"+complain.getExtraCharges()+complain.getAmountReceived());
			
			chargeAmt = complain.getExtraCharges();
			recAmt = complain.getAmountReceived();
			
			amount = amount+chargeAmt+recAmt;
		}
		Phrase amt = new Phrase(""+amount,font1);
		PdfPCell amtcell = new PdfPCell(amt);
		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		double sum = 0;
		sum = sum+amount;
		this.sum = sum;
		
		/************************************************/		
		
//		Phrase	amt = null;
//		if(complain.getAmountReceived()!=null){
//			System.out.println("total::::::::::"+complain.getAmountReceived());
//			amt = new Phrase(" "+complain.getAmountReceived(),font1);
//		}
//		PdfPCell amtcell = new PdfPCell(amt);
//		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		righttable.addCell(amtcell);
		
		PdfPCell rightcell = new PdfPCell(righttable);
		rightcell.setBorder(0);	
		
		
		PdfPTable table = new PdfPTable(3);
		table.addCell(leftcell);
		table.addCell(centercell);
		table.addCell(rightcell);
		
		try {
			table.setWidths(colWidth6);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.addCell(blcell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
			} catch (DocumentException e){
			e.printStackTrace();
		}
	}
}
