package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class reddysPestControlInvoice {

	public Document document;
	
	ProcessConfiguration processConfig;
	boolean forReddyPestControlFlag = false;
	boolean CompanyNameLogoflag = false;
	
	Company comp;
	Customer cust;
	Invoice invoiceentity;
	Contract con;
	Service service;
	
	String str;

	Phrase chunk;
	Phrase blank;
	Phrase colon;
	
	PdfPCell pdfsno, pdfdesc, pdfqty, pdfrate, pdfamt;
	PdfPCell pdftaxAmt,pdftaxAmt1,pdftaxAmt2,pdfgrandTotal;
	double grandTotal,sum;
	
	
	float[] columnWidths2 = {1.8f,0.5f,2.5f};
//	float[] colWidth = {0.8f, 6f, 4.1f, 1.9f, 2f};
	
	//***********tp table***************
	float[] colWidth = {0.8f,4f,2f,2.5f};
	
	//*********for 2 working table************
//	float[] colWidth = {0.7f,4f,5f};
//	float[] colWidth1 = {0.1f,0.2f,0.5f};
//	float[] colWidth2 = {0.9f,4f,3.7f,4f};
	
	float[]	colWidth3 = {0.1f,0.2f};

	float round = 0.00f;
	DecimalFormat df = new DecimalFormat("0.00");
	
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul;

	public reddysPestControlInvoice() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setpdfrinvoice(Long count) {

		invoiceentity = ofy().load().type(Invoice.class).id(count).now();

		if (invoiceentity.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		if (invoiceentity.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getCustomerId())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getCustomerId()).first()
					.now();
		
		if (invoiceentity.getCompanyId() == null)
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount()).first().now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("count", invoiceentity.getContractCount()).first().now();
		
		if (invoiceentity.getCompanyId() == null)
			service = ofy().load().type(Service.class)
					.filter("contractCount", invoiceentity.getContractCount()).first()
					.now();
		else
			service = ofy().load().type(Service.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("contractCount", invoiceentity.getContractCount()).first()
					.now();
		
		
		//************process config code*************************
		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("RInvoiceChanges")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						forReddyPestControlFlag = true;
					}
				}
			}
		}
		
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

	}

	
	public void createPdf() {
		
		createLogo123(document, comp);

		createBlank();
		createHeading();
		createHeading1();
		createCustomerDetails();
		createTableInfo();
//		createTableInfo1();
//		createTableInfo2();
		createInwordsTable();
		createBottom1Detail();
		
		
		
		String amountInWord = ServiceInvoicePdf.convert(con.getNetpayable());

	}

	public void createBlank() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
//		bltable.addCell(blcell);
		bltable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(bltable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

/**********************************COMPANY LOGO****************************************************/	
	private void createLogo123(Document doc, Company comp) {

		// ********************logo for server ********************

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl+ document.getUrl()));
			
//			Image image2 = Image.getInstance("images/rpc.jpg");
			image2.scalePercent(25f);
			image2.scaleAbsoluteWidth(120f);
			image2.setAbsolutePosition(40f, 725);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
/***********************************************************************/
	
	public void createHeading() {
		
		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase head = new Phrase("Invoice", font14boldul);
		PdfPCell headcell = new PdfPCell(head);
		headcell.setBorder(0);
		headcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable headtable = new PdfPTable(1);
//		headtable.addCell(blcell);
		headtable.addCell(headcell);
		headtable.setWidthPercentage(100f);

		PdfPCell cell = new PdfPCell(headtable);
		cell.setBorder(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createHeading1() {
		
		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		
	Phrase name = null;
	if(comp.getBusinessUnitName() !=null){
		name = new Phrase(" "+comp.getBusinessUnitName(),font14bold);
	}
	PdfPCell namecell = new PdfPCell(name);	
	namecell.setBorder(0);
	namecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	//********address************
	
//	Phrase compaddress = new Phrase("Address : ", font10bold);
//	PdfPCell compaddcell = new PdfPCell(compaddress);
//	compaddcell.setBorder(0);
//	compaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	String compAdd1 = "";
	String compFullAdd1 = "";

	if (comp.getAddress() != null) {

		if (comp.getAddress().getAddrLine2() != null) {
			if (comp.getAddress().getLandmark() != null) {
				compAdd1 = comp.getAddress().getAddrLine1() + ","
						+ comp.getAddress().getAddrLine2() + "\n"
						+ comp.getAddress().getLandmark();
			} else {
				compAdd1 = comp.getAddress().getAddrLine1() + ","
						+ comp.getAddress().getAddrLine2();
			}
		} else {
			if (comp.getAddress().getLandmark() != null) {
				compAdd1 = comp.getAddress().getAddrLine1() + ","
						+ comp.getAddress().getLandmark();
			} else {
				compAdd1 = comp.getAddress().getAddrLine1();
			}
		}

		if (comp.getAddress().getLocality() != null) {
			compFullAdd1 = compAdd1 + "\n" + comp.getAddress().getCountry()
					+ "," + comp.getAddress().getState() + ","
					+ comp.getAddress().getCity() + "\n"
					+ comp.getAddress().getLocality() + " "
					+ comp.getAddress().getPin();

		} else {
			compFullAdd1 = compAdd1 + "\n" + comp.getAddress().getCountry()
					+ "," + comp.getAddress().getState() + ","
					+ comp.getAddress().getCity() + " "
					+ comp.getAddress().getPin();
		}
	}

	Phrase compAddInfo = new Phrase("Address : "+compFullAdd1, font9);
	PdfPCell compAddInfoCell = new PdfPCell(compAddInfo);
	compAddInfoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	compAddInfoCell.setBorder(0);
		
	//*****contact & email & website details*****	
	Phrase info = null;
	if (comp.getLandline()!=null && comp.getCellNumber1() !=null  && comp.getEmail() !=null) {

		info = new Phrase("TEL : " + comp.getLandline()+" ; "+comp.getCellNumber1()+" ; "+"E-Mail : "+comp.getEmail(),font9);
	}
	PdfPCell infocell = new PdfPCell(info);
	infocell.setBorder(0);
	infocell.setHorizontalAlignment(Element.ALIGN_CENTER);	
		
	Phrase web = null;
	if(comp.getWebsite() !=null){
	web = new Phrase(" "+comp.getWebsite(),font12boldul);	
	}
	PdfPCell webcell = new PdfPCell(web);
	webcell.setBorder(0);
	webcell.setHorizontalAlignment(Element.ALIGN_CENTER);		
		
	PdfPTable detailtable = new PdfPTable(1);	
	
	detailtable.addCell(namecell);	
//	detailtable.addCell(compaddcell);
	detailtable.addCell(compAddInfoCell);
	detailtable.addCell(infocell);
	detailtable.addCell(webcell);
	detailtable.addCell(blcell);
	detailtable.setWidthPercentage(100f);

	PdfPCell cell = new PdfPCell(detailtable);	
		
	PdfPTable table = new PdfPTable(1);	
	table.addCell(cell);	
	table.setWidthPercentage(100);	
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
		
	}
	
	public void createCustomerDetails() {
		
		Phrase blank = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);
		
		//************leftside info*************
//		Phrase custname = new Phrase("CUSTOMER : ", font10bold);
//		PdfPCell custnamecell = new PdfPCell(custname);
//		custnamecell.setBorder(0);
//		custnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase cname = null;
		if(cust.getCompanyName() !=null)
		{
			cname = new Phrase(" "+cust.getCompanyName(), font9);
		} 
		else {
				cname = new Phrase("M/S" + cust.getFullname(), font9);
		}
		PdfPCell cnamecell = new PdfPCell(cname);
		cnamecell.setBorder(0);
		cnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase custaddress = new Phrase("ADDRESS : ", font9);
		PdfPCell custaddcell = new PdfPCell(custaddress);
		custaddcell.setBorder(0);
		custaddcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null) {

			if (cust.getAdress().getAddrLine2() != null) {
				if (cust.getAdress().getLandmark() != null) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2() + "\n"
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (cust.getAdress().getLandmark() != null) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null) {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + "\n"
						+ cust.getAdress().getLocality() + " "
						+ cust.getAdress().getPin();

			} else {
				custFullAdd1 = custAdd1 + "\n" + cust.getAdress().getCountry()
						+ "," + cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}
		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);

		Phrase mobno = null;
		if (cust.getCellNumber1() != null) {

			mobno = new Phrase("CONTACT NO. : " + cust.getCellNumber1(), font9);
		}
		PdfPCell mobcell = new PdfPCell(mobno);
		mobcell.setBorder(0);
		mobcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable lefttable = new PdfPTable(1);
		
//		lefttable.addCell(custnamecell);
		
		lefttable.addCell(cnamecell);
		lefttable.addCell(custaddcell);
		lefttable.addCell(custAddInfoCell);
		lefttable.addCell(mobcell);
		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
//		lefttable.addCell(blcell);
		
		lefttable.setWidthPercentage(100f);
		
		PdfPCell leftcell = new PdfPCell(lefttable);
		leftcell.setBorder(0);

		
		
		//***********rightside info*************
		Phrase colon = new Phrase(":",font9bold);
		PdfPCell colcell = new PdfPCell(colon);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase inv = new Phrase("INVOICE NO",font9bold);
		PdfPCell invcell = new PdfPCell(inv);
		invcell.setBorder(0);
		invcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		Phrase no = null;
		
		System.out.println("INVOICE NO :::::::::::::::::::::::::::;"+invoiceentity.getCount());
		if(invoiceentity.getCount() !=0){
		no = new Phrase(" "+invoiceentity.getCount(), font9);
		System.out.println("INVOICE NO1 :::::::::::::::::::::::::::;"+invoiceentity.getCount());
		}
		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		
		Phrase date = new Phrase("DATE",font9);
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase dt = null;
		if(invoiceentity.getInvoiceDate() !=null){
			dt = new Phrase(" "+fmt.format(invoiceentity.getInvoiceDate()), font9);
		}
		PdfPCell dtcell = new PdfPCell(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		
//		Phrase ser = new Phrase("SERVICE TYPE",font9);
//		PdfPCell sercell = new PdfPCell(ser);
//		sercell.setBorder(0);
//		sercell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase type = null;
//		if(con.getDescription() !=null){
//			type = new Phrase(" "+con.getDescription(), font9);
//		}
//		PdfPCell typecell = new PdfPCell(type);
//		typecell.setBorder(0);
//		typecell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		Phrase exc = new Phrase("EXECUTION DATE",font9);
		PdfPCell exccell = new PdfPCell(exc);
		exccell.setBorder(0);
		exccell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Date excDate = getExecutionDate();
		if(excDate !=null){
			str = fmt.format(excDate);
		}else{
			str = fmt1.format(invoiceentity.getInvoiceDate());
		}
		
		Phrase str1 = new Phrase(str,font9);
		PdfPCell strcell = new PdfPCell(str1);
		strcell.setBorder(0);
		strcell.setHorizontalAlignment(Element.ALIGN_LEFT);	
		
		
		//*********article table***************
		
		PdfPTable righttable = new PdfPTable(3);
		
		righttable.addCell(invcell);
		righttable.addCell(colcell);
		righttable.addCell(nocell);
		
		righttable.addCell(datecell);
		righttable.addCell(colcell);
		righttable.addCell(dtcell);
		
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			Phrase pan = null;
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
				pan = new Phrase(""
						+ comp.getArticleTypeDetails().get(i)
								.getArticleTypeName(), font9);
			}
			PdfPCell pancell = new PdfPCell(pan);
			pancell.setBorder(0);
			pancell.setHorizontalAlignment(Element.ALIGN_LEFT);

			Phrase pan1 = null;
			if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
				pan1 = new Phrase(""
						+ comp.getArticleTypeDetails().get(i)
								.getArticleTypeValue(), font9);
			}
			PdfPCell pan1cell = new PdfPCell(pan1);
			pan1cell.setBorder(0);
			pan1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

			righttable.addCell(pancell);
			righttable.addCell(colcell);
			righttable.addCell(pan1cell);

		}
		
//		righttable.addCell(sercell);
//		righttable.addCell(colcell);
//		righttable.addCell(typecell);
		
		righttable.addCell(exccell);
		righttable.addCell(colcell);
		righttable.addCell(strcell);
		
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		righttable.addCell(blcell);
		
		try {
			righttable.setWidths(columnWidths2);
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
		righttable.setWidthPercentage(100f);
		
		PdfPCell rightcell = new PdfPCell(righttable);
//		rightcell.setBorder(0);
		
		PdfPTable table = new PdfPTable(2);
		table.addCell(leftcell);
		table.addCell(rightcell);
		try {
			table.setWidths(new float[] { 55, 45 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);
		
		PdfPCell cell = new PdfPCell(table);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);
		
		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	public void createInwordsTable() {

		String amountInWord = ServiceInvoicePdf.convert(con.getNetpayable());
		Phrase rs = new Phrase("Amount in Words : Rs.  " + amountInWord + " Only ", font10bold);
		PdfPCell rscell = new PdfPCell(rs);
		// rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(rscell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}	
	
	
public void createBottom1Detail(){
	
	Phrase blank = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(blank);
	blcell.setBorder(0);
	
	PdfPTable lefttable = new PdfPTable(1);
	
	lefttable.addCell(blcell);
	lefttable.addCell(blcell);
	lefttable.addCell(blcell);
	lefttable.addCell(blcell);
	lefttable.addCell(blcell);
	lefttable.addCell(blcell);

	
	lefttable.setWidthPercentage(100f);
	
	PdfPCell leftcell = new PdfPCell(lefttable);
	leftcell.setBorder(0);
	
	//******************************
	Phrase company = null;
	if(comp.getBusinessUnitName() !=null){
		company = new Phrase("For "+comp.getBusinessUnitName(),font12bold);
	}
	PdfPCell compcell = new PdfPCell(company);
	compcell.setBorder(0);
	compcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase autho = new Phrase("Authorized signatory",font10bold);
	PdfPCell authocell = new PdfPCell(autho);
	authocell.setBorder(0);
	authocell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPTable righttable = new PdfPTable(1);
	
	righttable.addCell(compcell);
	righttable.addCell(blcell);
	righttable.addCell(blcell);
	righttable.addCell(blcell);
	righttable.addCell(blcell);
	righttable.addCell(authocell);
	
	righttable.setWidthPercentage(100f);
	
	PdfPCell rightcell = new PdfPCell(righttable);
	rightcell.setBorder(0);
	
	PdfPTable table = new PdfPTable(2);
	table.addCell(leftcell);
	table.addCell(rightcell);
	try {
		table.setWidths(new float[] { 50, 50 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	table.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(table);
	
	PdfPTable parenttable = new PdfPTable(1);
	parenttable.addCell(cell);
	parenttable.setWidthPercentage(100);
	
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}


public void createTableInfo(){
	
	Phrase blank = new Phrase(" ");
	PdfPCell blcell = new PdfPCell(blank);
	blcell.setBorder(0);
	
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	
	//*********left table***********
	PdfPTable lefttable = new PdfPTable(1);
	lefttable.setWidthPercentage(100f);
	
	Phrase serialno = new Phrase("SR. NO.", font1);
	PdfPCell nocell = new PdfPCell(serialno);
	nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	lefttable.addCell(nocell);
	
	
	for (int i=0; i < this.invoiceentity.getSalesOrderProducts().size(); i++){
		
		chunk = new Phrase(i + 1 + "", font8);
		pdfsno = new PdfPCell(chunk);
		pdfsno.setBorderWidthTop(0);
		pdfsno.setBorderWidthBottom(0);
		pdfsno.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		lefttable.addCell(pdfsno);
	}
	PdfPCell leftcell = new PdfPCell(lefttable);
	leftcell.setBorder(0);
	leftcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
//*********center table***********
	PdfPTable centertable = new PdfPTable(1);
	centertable.setWidthPercentage(100f);
	
	Phrase desc = new Phrase("DESCRIPTION", font1);
	PdfPCell desccell = new PdfPCell(desc);
	desccell.setHorizontalAlignment(Element.ALIGN_CENTER);

	 
	//   rohan made this change for editing desc in reddy pest invoice taken from invoice desc. 
	if(invoiceentity.getComment()!= null)
	{
		chunk = new Phrase(invoiceentity.getComment(), font8);
	}
	else
	{
		chunk = new Phrase("Being Our charges for pest control services carried out at your premises for the month of "+str+"", font8);
	}
	
	pdfdesc = new PdfPCell(chunk);
	pdfdesc.setBorderWidthBottom(0);
	pdfdesc.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	centertable.addCell(desccell);
	centertable.addCell(pdfdesc);
	
	
	//    patch added by rohan  
	
	if(this.invoiceentity.getSalesOrderProducts().size() > 2){
		for (int i=2; i < this.invoiceentity.getSalesOrderProducts().size(); i++){
		
		chunk = new Phrase(" ", font8);
		pdfsno = new PdfPCell(chunk);
		pdfsno.setBorderWidthTop(0);
		pdfsno.setBorderWidthBottom(0);
		pdfsno.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		centertable.addCell(pdfsno);
	}
	
	}
	
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	centertable.addCell(blcell);
	
	Phrase total = new Phrase("TOTAL", font1);
	PdfPCell totalcell = new PdfPCell(total);
	totalcell.setBorder(0);
	totalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase add = new Phrase("ADD:",font9bold);
	PdfPCell addcell = new PdfPCell(add);
	addcell.setBorder(0);
	addcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	
	centertable.addCell(totalcell);
//	centertable.addCell(blcell);
	centertable.addCell(addcell);
//	centertable.addCell(blcell);
	
	
	
//*********for service,swach bharat and cess taxes(static part)*****************
	for (int i=0; i<con.getProductTaxes().size(); i++) {
	System.out.println("Product taxes:::::::::"+con.getProductTaxes().get(i).getChargeName());
	
	System.out.println("Product taxes in %:::::::::"+con.getProductTaxes().get(i).getChargePercent());
if(con.getProductTaxes().get(i).getChargeName().equals("Service Tax") && 
		con.getProductTaxes().get(i).getChargePercent()==15	){
	System.out.println("inside condition ::::::::");
	
		String str = "Service Tax "+ " @ "+" 14%";
	
		Phrase strp = new Phrase(str,font8);
		PdfPCell strcell =new PdfPCell(strp);
		strcell.setBorder(0);
		strcell.setHorizontalAlignment(Element.ALIGN_CENTER);	
		centertable.addCell(strcell);
		
		String str1 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
		
		Phrase str1p = new Phrase(str1,font8);
		PdfPCell str1cell =new PdfPCell(str1p);
		str1cell.setBorder(0);
		str1cell.setHorizontalAlignment(Element.ALIGN_CENTER);	
		centertable.addCell(str1cell);
		
		String str2 = "Cess"+ " @ "+" 0.5%";
		
		Phrase str2p = new Phrase(str2,font8);
		PdfPCell str2cell =new PdfPCell(str2p);
		str2cell.setBorder(0);
		str2cell.setHorizontalAlignment(Element.ALIGN_CENTER);	
		centertable.addCell(str2cell);
			
		}

	if(con.getProductTaxes().get(i).getChargePercent()!=15){
		Phrase str2p = new Phrase(con.getProductTaxes().get(i).getChargeName()+"@"+con.getProductTaxes().get(i).getChargePercent(),font8);
		PdfPCell str2cell =new PdfPCell(str2p);
		str2cell.setBorder(0);
		str2cell.setHorizontalAlignment(Element.ALIGN_CENTER);	
		centertable.addCell(str2cell);
	}
	}
	
	centertable.addCell(blcell);
	
	
//***********for Grand total************************	
	Phrase grandt = new Phrase("G. TOTAL", font10bold);
	PdfPCell grandtcell = new PdfPCell(grandt);
	grandtcell.setBorder(0);
	grandtcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	centertable.addCell(grandtcell);
	
	
	PdfPCell centercell = new PdfPCell(centertable);
	centercell.setBorder(0);
	centercell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	//*********right table***********
	PdfPTable righttable = new PdfPTable(2);
	
	try {
		righttable.setWidths(colWidth3);
		} catch (DocumentException e1) {
		e1.printStackTrace();
		}
	righttable.setWidthPercentage(100f);
	
	Phrase quantity = new Phrase("QTY", font1);
	Phrase rate = new Phrase("RATE", font1);

	PdfPCell qtycell = new PdfPCell(quantity);
	qtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

	PdfPCell ratecell = new PdfPCell(rate);
	ratecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	

	righttable.addCell(qtycell);
	righttable.addCell(ratecell);
	
	for (int i=0; i < this.invoiceentity.getSalesOrderProducts().size(); i++){
		
		chunk = new Phrase(invoiceentity.getSalesOrderProducts().get(i).getQuantity()+"", font8);
		pdfqty = new PdfPCell(chunk);
		pdfqty.setBorderWidthTop(0);
		pdfqty.setBorderWidthBottom(0);
		pdfqty.setHorizontalAlignment(Element.ALIGN_CENTER);

		chunk = new Phrase(df.format(invoiceentity.getSalesOrderProducts().get(i).getPrice())+"", font8);
		pdfrate = new PdfPCell(chunk);
		pdfrate.setBorderWidthTop(0);
		pdfrate.setBorderWidthBottom(0);
		pdfrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		

		righttable.addCell(pdfqty);
		righttable.addCell(pdfrate);
	}
	
	PdfPCell rightcell = new PdfPCell(righttable);
	rightcell.setBorder(0);
	rightcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	
//***********for Amount table********************************
	Phrase bl = new Phrase(" ");
	PdfPCell bcell = new PdfPCell(bl);
	bcell.setBorderWidthTop(0);
	bcell.setBorderWidthBottom(0);
	
	PdfPTable amttable = new PdfPTable(1);
	amttable.setWidthPercentage(100f);
	
	Phrase price = new Phrase("AMOUNT INR", font1);
	
	PdfPCell pricecell = new PdfPCell(price);
	pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	amttable.addCell(pricecell);
	
for (int i=0; i < this.invoiceentity.getSalesOrderProducts().size(); i++){
		
		chunk = new Phrase(df.format(invoiceentity.getSalesOrderProducts().get(i).getTotalAmount())+"", font8);
		pdfamt = new PdfPCell(chunk);
		pdfamt.setBorderWidthTop(0);
		pdfamt.setBorderWidthBottom(0);
		pdfamt.setHorizontalAlignment(Element.ALIGN_CENTER);	
		
		amttable.addCell(pdfamt);
	}

	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
	amttable.addCell(bcell);
//	amttable.addCell(bcell);
	
	
//	for (int i=0; i < this.invoiceentity.getSalesOrderProducts().size(); i++){
		
		chunk = new Phrase(df.format(con.getTotalAmount()), font8);
		pdfamt = new PdfPCell(chunk);
		pdfamt.setBorderWidthTop(0);
		pdfamt.setBorderWidthBottom(0);
		pdfamt.setHorizontalAlignment(Element.ALIGN_CENTER);	
	
		amttable.addCell(pdfamt);
//	}
	
	amttable.addCell(bcell);
//	amttable.addCell(bcell);
//	amttable.addCell(bcell);
	
//	for (int j=0; j < this.invoiceentity.getSalesOrderProducts().size(); ++){
		
		for (int i=0; i<con.getProductTaxes().size(); i++) {
			
		if(con.getProductTaxes().get(i).getChargeName().equals("Service Tax") && 
				con.getProductTaxes().get(i).getChargePercent()==15	){
			
		
		double taxAmt = 14* con.getTotalAmount() / 100;
		Phrase taxAmtp = new Phrase(taxAmt+"",font8);
		
		pdftaxAmt = new PdfPCell(taxAmtp);
		pdftaxAmt.setBorderWidthTop(0);
		pdftaxAmt.setBorderWidthBottom(0);
		pdftaxAmt.setHorizontalAlignment(Element.ALIGN_CENTER);	
		
		amttable.addCell(pdftaxAmt);
		
		//////////////
		
		double taxAmt1 = 0.5* con.getTotalAmount() / 100;
		Phrase taxAmt1p = new Phrase(taxAmt1+"",font8);
		
		pdftaxAmt1 = new PdfPCell(taxAmt1p);
		pdftaxAmt1.setBorderWidthTop(0);
		pdftaxAmt1.setBorderWidthBottom(0);
		pdftaxAmt1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		amttable.addCell(pdftaxAmt1);
		
		//////////////
		
		double taxAmt2 = 0.5* con.getTotalAmount() / 100;
		Phrase taxAmt2p = new Phrase(taxAmt2+"",font8);
		
		pdftaxAmt2 = new PdfPCell(taxAmt2p);
		pdftaxAmt2.setBorderWidthTop(0);
		pdftaxAmt2.setBorderWidthBottom(0);
		pdftaxAmt2.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		amttable.addCell(pdftaxAmt2);
		
		}
		
		
		if(con.getProductTaxes().get(i).getChargePercent()!=15){
		Phrase ph = new Phrase(df.format(con.getProductTaxes().get(i).getChargePayable()),font8);
		pdftaxAmt = new PdfPCell(ph);
		pdftaxAmt.setBorderWidthTop(0);
		pdftaxAmt.setBorderWidthBottom(0);
		pdftaxAmt.setHorizontalAlignment(Element.ALIGN_CENTER);	
		
		amttable.addCell(pdftaxAmt);
		}
		
			
	}
		 //   rohan added blank 
		amttable.addCell(bcell);
		
		double totalAmt = con.getNetpayable();
		System.out.println("GrandTotal::::::::::::::::::::::;;"+sum);
		
		Phrase grandTotalp = new Phrase(totalAmt +"",font8);
		PdfPCell blkcell = new PdfPCell(grandTotalp);
		blkcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amttable.addCell(blkcell);
	
		
		PdfPCell amtcell = new PdfPCell(amttable);
		amtcell.setBorder(0);	
	
	
	PdfPTable table = new PdfPTable(4);
	table.addCell(leftcell);
	table.addCell(centercell);
	table.addCell(rightcell);
	table.addCell(amtcell);
	
	try {
	table.setWidths(colWidth);
	} catch (DocumentException e1) {
	e1.printStackTrace();
	}
	table.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(table);
	cell.setBorder(0);
	
	PdfPTable parenttable = new PdfPTable(1);
	parenttable.addCell(cell);
	parenttable.setWidthPercentage(100);
	
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}
	

public Date getExecutionDate(){
	
	Date invoiceDate;
	 
	ArrayList<Service> serList= new ArrayList<Service>();
	
	List<Service> list = ofy().load().type(Service.class)
			.filter("companyId", invoiceentity.getCompanyId())
			.filter("contractCount", invoiceentity.getContractCount()).list();
	
	Date startDateoftheMonth = DateUtility.getStartDateofMonth(invoiceentity.getInvoiceDate());
	
	for(Service ser:list){
		
		if(ser.getServiceDate().after(startDateoftheMonth) && ser.getServiceDate().before(invoiceentity.getInvoiceDate())){
			
			serList.add(ser);
		}
	}
	
	if(serList.size() ==0){
		
		invoiceDate = null;	
		
	}else if(serList.size()>1)
	{
		invoiceDate = null;		
	}
	else{
		invoiceDate = serList.get(0).getServiceDate();
	}
	
return invoiceDate;
	
}
	
}
