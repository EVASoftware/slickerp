package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.contractrenewal.ContractRenewalProduct;
import com.slicktechnologies.server.utility.BahtText;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;

public class ContractRenewalPdfversionOne {
	
	Logger logger = Logger.getLogger("ContractRenewalPdfversionOne.class");
	ContractRenewal conRenw;
	Company comp;
	Customer cust;
	Branch branchDt = null;
	Contract con;
	ContactPersonIdentification conDetails;
	public Document document;
	int noOfLine=12;  //10  //16
	int productcount=0;
	int vat;
	int st;
	boolean checkEmailId=false;
	boolean checkOldFormat=false;
	List<Branch> branchList=new ArrayList<Branch>();//By Jayshree
	CompanyPayment comppayment;
	private Font font16boldul, font12bold, font8bold, font8,font7bold, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold;

	Phrase chunk;
	PdfPCell pdfcode,pdfname,pdfsacCode,pdfduration,pdfservices,pdfprice,pdftax,pdfnetPay;
    ContractRenewalProduct conProd;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt3 = new SimpleDateFormat("MMM-yyyy");
	DecimalFormat df = new DecimalFormat("#");

	boolean oldConDetFlag=false;
	
	boolean companyAsaletter=false;
	DecimalFormat df2 = new DecimalFormat("0");
	
	 float[] colwidth = { 0.8f, 0.2f, 5.0f, 2.0f, 0.2f, 1.7f };
	
	 List<CustomerBranchDetails> custbranchlist;
	 List<CustomerBranchDetails> customerbranchlist;
	 CustomerBranchDetails customerBranch=null;
	 
	 List<CompanyPayment>comppay=new ArrayList<CompanyPayment>();
	 boolean thaiFontFlag=false;//checkFormat
	 boolean checkFormat=false;//checkFormat
	 
	 ServerAppUtility serverapputility = new ServerAppUtility();
	 
	 public ContractRenewalPdfversionOne() {
			font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
			font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
			new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
			font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
			font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
			font8 = new Font(Font.FontFamily.HELVETICA, 8);
		   font7bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//added by ashwini
			font9 = new Font(Font.FontFamily.HELVETICA, 9);
			font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
			font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
			font12 = new Font(Font.FontFamily.HELVETICA, 12);
			font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
			font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
			font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
			font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
			
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		}
	 
		public void setContractRewnewal(ContractRenewal conRenewal, String preprintStatus){
			conRenw=conRenewal;
			
			
			if (conRenw.getCompanyId() != null) {
				
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", conRenw.getCompanyId())
						.filter("processName", "Company")
						.filter("configStatus", true).first().now();
				
				if(processConfig!=null){
					for(ProcessTypeDetails obj:processConfig.getProcessList()){
						if(obj.getProcessType().equalsIgnoreCase("UseThaiFontForPdf")&&obj.isStatus()==true){
							thaiFontFlag=true;
							break;
						}
					}
				}
			}
			try {
				logger.log(Level.SEVERE, "Initializing Thai Font updated .....");
				
				if(thaiFontFlag){
					BaseFont regularFont;
					BaseFont boldFont;
					if(preprintStatus!=null&&!preprintStatus.equals("")&&preprintStatus.equals("Thai")){
//						regularFont=BaseFont.createFont("angsa.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
//						boldFont=BaseFont.createFont("angsab.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
						
						//Ashwini Patil Date:31-03-2022 Changed font as per innovative requirement
						regularFont=BaseFont.createFont("Tahoma Regular font.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
						boldFont=BaseFont.createFont("TAHOMAB0.TTF",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
											
						
						font16boldul = new Font(boldFont, 19,Font.BOLD);
						font12bold = new Font(boldFont, 15,Font.BOLD);
						font8bold = new Font(boldFont, 10,Font.BOLD);
						font8 = new Font(regularFont, 10);
						font9 = new Font(regularFont, 10);
						font12boldul = new Font(boldFont, 15,Font.BOLD);
						font12 = new Font(regularFont, 15);
						font10bold = new Font(boldFont, 13,Font.BOLD);
						font14bold = new Font(boldFont, 17,Font.BOLD);
						font9bold = new Font(boldFont, 10,Font.BOLD);
						font16bold = new Font(boldFont, 18,Font.BOLD);
						font10 = new Font(regularFont, 12);
						font7bold = new Font(boldFont, 9,Font.BOLD);
					}else{
						regularFont=BaseFont.createFont("Sarabun-Regular.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
						boldFont=BaseFont.createFont("Sarabun-Bold.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
						
						font16boldul = new Font(boldFont, 17);
						font12bold = new Font(boldFont, 13);
						font8bold = new Font(boldFont, 8);
						font8 = new Font(regularFont, 8);
						font9 = new Font(regularFont, 8);
						font12boldul = new Font(boldFont, 13);
						font12 = new Font(regularFont, 13);
						//font11bold = new Font(boldFont, 12);
						font10bold = new Font(boldFont, 11);
						//font7 = new Font(regularFont, 8, Font.NORMAL);
						font14bold = new Font(boldFont, 15);
						font9bold = new Font(boldFont, 8);
						//font6bold = new Font(boldFont, 7);
						
						font16bold = new Font(boldFont, 16);
						//font11 = new Font(regularFont, 10);
						font10 = new Font(regularFont, 10);
						//font13 = new Font(regularFont, 9);
						font7bold = new Font(boldFont, 7);
					}
					
					
					
					
					
				}
				
				
			} catch (DocumentException e1) {
				e1.printStackTrace();
				logger.log(Level.SEVERE, "Exception 1 Thai Font.....");
			} catch (IOException e1) {
				e1.printStackTrace();
				logger.log(Level.SEVERE, "Exception 2 Thai Font.....");
			}catch (Exception e1) {
				e1.printStackTrace();
				logger.log(Level.SEVERE, "Exception 3 Thai Font.....");
			}
			
			
			if (conRenw.getCompanyId() == null)
				comp = ofy().load().type(Company.class).first().now();
			else
				comp = ofy().load().type(Company.class).filter("companyId", conRenw.getCompanyId()).first().now();
			
			if (conRenw.getCompanyId() == null)
				cust = ofy().load().type(Customer.class).filter("count", conRenw.getCustomerId()).first().now();
			else
				cust = ofy().load().type(Customer.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getCustomerId()).first().now();
			
			if (conRenw.getCompanyId() == null)
				con = ofy().load().type(Contract.class).filter("count", conRenw.getContractId()).first().now();
			else
				con = ofy().load().type(Contract.class).filter("companyId", conRenw.getCompanyId()).filter("count", conRenw.getContractId()).first().now();
			
			
			branchList=ofy().load().type(Branch.class).filter("companyId",conRenw.getCompanyId()).filter("buisnessUnitName",conRenw.getBranch()).list();

			
			comppay = ofy().load().type(CompanyPayment.class)
					.filter("companyId", conRenw.getCompanyId()).filter(" paymentStatus", true).list();
			
			if (conRenw.getCompanyId() != null) {
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("paymentDefault", true)
						.filter("companyId", conRenw.getCompanyId()).first().now();
			}
			
			
			conDetails=ofy().load().type(ContactPersonIdentification.class)
					.filter("companyId", con.getCompanyId()).filter("name", con.getPocName()).first().now();
			
			
			if (con.getCompanyId() == null)
				custbranchlist = ofy()
						.load()
						.type(CustomerBranchDetails.class)
						.filter("cinfo.count",
								con.getCinfo().getCount()).list();
			else
				custbranchlist = ofy()
						.load()
						.type(CustomerBranchDetails.class)
						.filter("cinfo.count",
								con.getCinfo().getCount())
						.filter("companyId", con.getCompanyId()).list();

			

	
			if (con.getCompanyId() == null)
				customerbranchlist = ofy()
						.load()
						.type(CustomerBranchDetails.class)
						.filter("cinfo.count",
								con.getCinfo().getCount())
						.filter("buisnessUnitName",
								con.getNewCustomerBranch()).list();
			else
				customerbranchlist = ofy()
						.load()
						.type(CustomerBranchDetails.class)
						.filter("cinfo.count",
								con.getCinfo().getCount())
						.filter("buisnessUnitName",
								con.getNewCustomerBranch())
						.filter("companyId", con.getCompanyId()).list();

			System.out.println("Banch updated====="+ con.getNewCustomerBranch());
			
			
			
			ArrayList<String> custbranchlist=getCustomerBranchList(con.getItems());
			
			if(custbranchlist!=null&&custbranchlist.size()==1&& custbranchlist.contains("Service Address")==false){
					logger.log(Level.SEVERE,"In Side AList1:");
					customerBranch= ofy().load().type(CustomerBranchDetails.class)
								.filter("cinfo.count",con.getCinfo().getCount())
								.filter("companyId", con.getCompanyId())
								.filter("buisnessUnitName", custbranchlist.get(0)).first().now();
					
					logger.log(Level.SEVERE,"AList1:" +customerBranch);
					logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());
				}
			

			
			if(conRenw.getCompanyId()!=null){
				ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
						.filter("companyId", conRenw.getCompanyId()).filter("processName", "ContractRenewal").filter("configStatus", true).first().now();
				if (processConfig != null) {
					for (int k = 0; k < processConfig.getProcessList().size(); k++) {
						if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowOldContractDetails")
								&& processConfig.getProcessList().get(k).isStatus() == true) {
							oldConDetFlag=true;
						}
						
						if (processConfig.getProcessList().get(k).getProcessType()
								.trim().equalsIgnoreCase("ActiveBranchEmailId")
								&& processConfig.getProcessList().get(k).isStatus() == true) {
							checkEmailId = true;
						}
						
						
					}
				}
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
					
					logger.log(Level.SEVERE,"Process active --");
					if(conRenw !=null && conRenw.getBranch() != null && conRenw.getBranch().trim().length() >0){
						
						branchDt = ofy().load().type(Branch.class).filter("companyId",conRenw.getCompanyId()).filter("buisnessUnitName", conRenw.getBranch()).first().now();
						
					
						if(branchDt!=null){
							logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
							
							if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
								
							
							List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
							
							if(paymentDt.get(0).trim().matches("[0-9]+")){
								
								
								
								int payId = Integer.parseInt(paymentDt.get(0).trim());
								
								comppayment = ofy().load().type(CompanyPayment.class)
										.filter("count", payId)
										.filter("companyId", con.getCompanyId()).first()
										.now();
								
								
								if(comppayment != null){
									
								}
								
							}
							}
							
							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
						}
					}
				}
			
			}

			
							
			
		}
		
		public void createPdf(String LanguangeStatus){
			
			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "Pc_LetterHead", comp.getCompanyId())){
				companyAsaletter=true;
			}
			
			if(LanguangeStatus.contains("English")){
				
				
				if(companyAsaletter){
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document , comp);
						createBlankforUPC();
					}
				}else{
					createCompanyHeadding();
				}		
			
				createReferenceandDateHeadding();
				createCustomerDetails();
				createSubjectAndMsg();
				createFooterDetailTab();
				
				if(companyAsaletter){
					if (comp.getUploadFooter() != null) {
						createCompanyNameAsFooter(document, comp);
					}
				}
			}
			
			
			if(LanguangeStatus.contains("Thai")){
				if(companyAsaletter){
					if (comp.getUploadHeader() != null) {
						createCompanyNameAsHeader(document , comp);
						createBlankforUPC();
					}
				}else{
					createCompanyHeadding();
				}
			
				createReferenceandDateHeaddingForThai();
				createCustomerDetailsForThai();
				createSubjectAndMsgForThai();
				createFooterDetailTabForThai();
			
			if(companyAsaletter){
				if (comp.getUploadFooter() != null) {
					createCompanyNameAsFooter(document, comp);
				}
			}
		  }
		
		}
	 
	 
		private ArrayList<String> getCustomerBranchList(List<SalesLineItem> itemList) {
			HashSet<String> branchHs=new HashSet<String>();
			for(SalesLineItem itemObj:itemList){
				if(itemObj.getCustomerBranchSchedulingInfo()!=null){
					ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
					for(BranchWiseScheduling obj:branchSchedulingList){
						if(obj.isCheck()==true){
							branchHs.add(obj.getBranchName());
						}
					}
				}
			}
			
			if(branchHs!=null&&branchHs.size()!=0){
				ArrayList<String> branchList=new ArrayList<String>(branchHs);
				logger.log(Level.SEVERE,"In Side AList3:"+branchList.size());
				return branchList;
			}
			
			return null;
		}
		
	
		public void createCompanyHeadding() {
			
			
			
			
			String companyname ="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyname=" "
						+  branchDt.getCorrespondenceName();
			}else{
			 companyname = " "
					+ comp.getBusinessUnitName().trim().toUpperCase();
			}

			Paragraph companynamepara = new Paragraph();
			companynamepara.add(companyname);
			companynamepara.setFont(font12bold);
			companynamepara.setAlignment(Element.ALIGN_CENTER);

			PdfPCell companynamecell = new PdfPCell();
			companynamecell.addElement(companynamepara);
			companynamecell.setBorder(0);
			

			
			String custAdd1="";
			String custFullAdd1="";
			
			if(comp.getAddress()!=null){
				
				if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
				
					if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
						custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
					}else{
						custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
					}
				}else{
					if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
						custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
					}else{
						custAdd1=comp.getAddress().getAddrLine1();
					}
				}
				
				if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
					if(comp.getAddress().getPin()!=0){
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getPin()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}else{
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}
				}else{
					if(comp.getAddress().getPin()!=0){
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getPin()+","+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry();
					}else{
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}
				}
				
			}	
			
				Phrase addressline=new Phrase(custFullAdd1,font10);
		
				Paragraph addresspara=new Paragraph();
				addresspara.add(addressline);
				addresspara.setAlignment(Element.ALIGN_CENTER);
				
				PdfPCell addresscell=new PdfPCell();
				addresscell.addElement(addresspara);
				addresscell.setBorder(0);
			   
			
			
			String contactinfo="";
			if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
//				contactinfo =  "Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim();
				/**
				 * @author Vijay Date :- 07-12-2021
				 * Des :- Adding country code(from country master) in cell number.
				 */
				String companycellNumber = serverapputility.getMobileNoWithCountryCode(comp.getCellNumber1()+"", comp.getAddress().getCountry(), comp.getCompanyId());
				contactinfo =  "Mobile: "+companycellNumber +" Email: "+comp.getEmail().trim();

			}
			//Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim(),font10);
			
			Paragraph realmobpara=new Paragraph();
			realmobpara.add(contactinfo);
			realmobpara.setFont(font10);
			realmobpara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell contactcell=new PdfPCell();
			contactcell.addElement(realmobpara);
			contactcell.setBorder(0);
			
			PdfPTable table = new PdfPTable(1);
			table.addCell(companynamecell);
			table.addCell(addresscell);
//			table.addCell(localitycell);
			table.addCell(contactcell);
			table.setWidthPercentage(100f);
			table.setSpacingAfter(10f);
			
			PdfPCell cell = new PdfPCell();
			cell.addElement(table);
			cell.setBorderWidthLeft(0);
			cell.setBorderWidthRight(0);
			cell.setBorderWidthTop(0);
			
			PdfPTable parenttable = new PdfPTable(1);
			parenttable.addCell(cell);
			parenttable.setWidthPercentage(100);
			
		
		
		try {
			document.add(parenttable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
		
	
		
		
		public void createReferenceandDateHeadding() {
			
			
			Phrase date = new Phrase("Date",font12bold);
			Paragraph datepara = new Paragraph();
			datepara.add(date);
			datepara.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell datecell = new PdfPCell(datepara);
//			datecell.addElement(datepara);
			datecell.setBorder(0);
			datecell.setHorizontalAlignment(Element.ALIGN_LEFT);// //
			

			
			Phrase dt = null;
			
			if(con.getContractDate()!=null){
				dt = new Phrase(fmt.format(con.getContractDate()),font12);
			}
			
			
			PdfPCell dtcell = new PdfPCell(dt);
//			dtcell.addElement(dt);
			dtcell.setBorder(0);
			dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
//			*****************************************************************************
		
			Phrase ref = new Phrase("Reference No",font12bold);
			Paragraph refpara = new Paragraph();
			refpara.add(ref);
			refpara.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell refcell = new PdfPCell(refpara);
//			refcell.addElement(refpara);
			refcell.setBorder(0);
			refcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			
			Phrase refno=new Phrase(con.getCount()+"",font12);
			PdfPCell refnocell  = new PdfPCell(refno);
			//refnocell.addElement(refno);
			refnocell.setBorder(0);
			refnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			Phrase col = new Phrase(":",font12bold);
			Paragraph colpara = new Paragraph();
			colpara.add(col);
			colpara.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell colcell = new PdfPCell(colpara);
			//colcell.addElement();
			colcell.setBorder(0);
			colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		    ******************************************************************************	
			
			PdfPTable table1 = new PdfPTable(6);
			
			try {
				table1.setWidths(colwidth);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			
			table1.addCell(datecell);
			table1.addCell(colcell);
			table1.addCell(dtcell);
			
			table1.addCell(refcell);
			table1.addCell(colcell);
			table1.addCell(refnocell);
			

			
			table1.setWidthPercentage(100f);
			
			PdfPCell cell1 = new PdfPCell();
			cell1.addElement(table1);
			cell1.setBorder(0);
		
			PdfPTable parent1 = new PdfPTable(1);
			
			parent1.addCell(cell1);
			parent1.setWidthPercentage(100);
			
			
			try {
				document.add(parent1);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void createReferenceandDateHeaddingForThai() {
			
			
			Phrase date = new Phrase("วันที่",font12);
			Paragraph datepara = new Paragraph();
			datepara.add(date);
			datepara.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell datecell = new PdfPCell(datepara);
//			datecell.addElement(datepara);
			datecell.setBorder(0);
			datecell.setHorizontalAlignment(Element.ALIGN_LEFT);// //
			

			
			Phrase dt = null;
			
			if(con.getContractDate()!=null){
				dt = new Phrase(fmt.format(con.getContractDate()),font12);
			}
			
			
			PdfPCell dtcell = new PdfPCell(dt);
//			dtcell.addElement(dt);
			dtcell.setBorder(0);
			dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
//			*****************************************************************************
		
			Phrase ref = new Phrase("Reference No",font12);
			Paragraph refpara = new Paragraph();
			refpara.add(ref);
			refpara.setAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell refcell = new PdfPCell(refpara);
//			refcell.addElement(refpara);
			refcell.setBorder(0);
			refcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			
			Phrase refno=new Phrase(con.getCount()+"",font12);
			PdfPCell refnocell  = new PdfPCell(refno);
			//refnocell.addElement(refno);
			refnocell.setBorder(0);
			refnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			Phrase col = new Phrase(":",font12);
			Paragraph colpara = new Paragraph();
			colpara.add(col);
			colpara.setAlignment(Element.ALIGN_LEFT);
			
			PdfPCell colcell = new PdfPCell(colpara);
			//colcell.addElement();
			colcell.setBorder(0);
			colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		    ******************************************************************************	
			
			PdfPTable table1 = new PdfPTable(6);
			
			try {
				table1.setWidths(colwidth);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			
			table1.addCell(datecell);
			table1.addCell(colcell);
			table1.addCell(dtcell);
			
			table1.addCell(refcell);
			table1.addCell(colcell);
			table1.addCell(refnocell);
			

			
			table1.setWidthPercentage(100f);
			
			PdfPCell cell1 = new PdfPCell();
			cell1.addElement(table1);
			cell1.setBorder(0);
		
			PdfPTable parent1 = new PdfPTable(1);
			
			parent1.addCell(cell1);
			parent1.setWidthPercentage(100);
			
			
			try {
				document.add(parent1);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		private void createCustomerDetails() {
			
			Phrase to = new Phrase("To,", font10);
			PdfPCell toCell = new PdfPCell(to);
			toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			toCell.setBorder(0);
			
			String roleName="";
			 if(customerBranch!=null)
			 {
				roleName=customerBranch.getPocName(); 
			 }else{
				 roleName=cust.getFullname();
			 }
	    	
			String[] nameArray = roleName.split("\\s+");
			if(nameArray.length>0)
			{
				roleName=nameArray[0];
			}
				
				
			String output = roleName.substring(0,1).toUpperCase() + roleName.substring(1).toLowerCase();
			
			Phrase role = new Phrase("Kind Attention : "+output, font10);
			PdfPCell roleCell = new PdfPCell(role);
			roleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			roleCell.setBorder(0);
			
			
			Phrase custInfo = null ;
			if(cust.isCompany()){
				custInfo = new Phrase("Company Name : "+cust.getCompanyName(), font10);
			}
			PdfPCell custInfoCell = new PdfPCell(custInfo);
			custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custInfoCell.setBorder(0);
			
			
			
			
			String custAdd1="";
			String custFullAdd1="";
			
			if(cust.getAdress()!=null){
				
				if(cust.getAdress().getAddrLine2()!=null){
					if(cust.getAdress().getLandmark()!=null){
						custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
					}else{
						custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
					}
				}else{
					if(cust.getAdress().getLandmark()!=null){
						custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
					}else{
						custAdd1=cust.getAdress().getAddrLine1();
					}
				}
				
				
				
				String pin="";
				if(cust.getAdress().getPin()!=0){
					pin=cust.getAdress().getPin()+"";
				}
				else{
					pin="";
				}
				if(cust.getAdress().getLocality()!=null){
//					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
					
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+cust.getAdress().getLocality()+"\n"+pin;

				}else{
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+pin;
				}
			}
			String custAddress="";
			
			 if(customerBranch!=null)
			 {
				custAddress=customerBranch.getBillingAddress().getCompleteAddress().trim();
				logger.log(Level.SEVERE,"billing address of customer branch " +custAddress);
			
			}
			else{
				custAddress = cust.getAdress().getCompleteAddress().trim();
				logger.log(Level.SEVERE,"billing address of customer " +custAddress);
			}
			
			Phrase custAddInfo = new Phrase("Address : "+custAddress, font10);
			PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
			custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custAddInfoCell.setBorder(0);
			
			
			Phrase blank = new Phrase(" ", font10);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			blankCell.setBorder(0);
			
			
//			String cell="";
//			if(cust.getCellNumber1()!=null){
//				cell="Cell : "+cust.getCellNumber1()+"";
//			}else{
//				cell=" ";
//			}
//			
//			Phrase cellNo = new Phrase(cell, font10);
//			PdfPCell cellNoCell = new PdfPCell(cellNo);
//			cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cellNoCell.setBorder(0);
//			
//			String email="";
//			if(cust.getEmail()!=null){
//				email=cust.getEmail();
//			}else{
//				email="";
//			}
//			Phrase emailPr = new Phrase("E-mail : "+email, font10);
//			PdfPCell emailCell = new PdfPCell(emailPr);
//			emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			emailCell.setBorder(0);
			
			PdfPTable customerTable=new PdfPTable(1);
			customerTable.setWidthPercentage(100);
			
			customerTable.addCell(toCell);
			customerTable.addCell(roleCell);
			customerTable.addCell(custInfoCell);
			customerTable.addCell(custAddInfoCell);
//			if(cust.getCellNumber1()!=0)
//			{
//			customerTable.addCell(cellNoCell);
//			}
//			if(email!=null&&!email.equals(""))
//			{	
//			customerTable.addCell(emailCell);
//			}
			PdfPCell customerCell = new PdfPCell();
			customerCell.addElement(customerTable);
			customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			customerCell.setBorder(0);
			
			
			
			PdfPTable headTbl=new PdfPTable(2);
			headTbl.setWidthPercentage(100f);
			
			headTbl.addCell(customerCell);
			headTbl.addCell(blankCell);
			
			PdfPCell parentCell = new PdfPCell();
			parentCell.addElement(headTbl);
			parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			parentCell.setBorder(0);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			parent.addCell(parentCell);
			parent.setSpacingAfter(15f);
			
			try {
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
				
		}
		
		private void createCustomerDetailsForThai() {
			
			Phrase to = new Phrase("To,", font10);
			PdfPCell toCell = new PdfPCell(to);
			toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			toCell.setBorder(0);
			
						
			String roleName="";
			 if(customerBranch!=null)
			 {
				roleName=customerBranch.getPocName(); 
			 }else{
				 roleName=cust.getFullname();
			 }
	    	
			String[] nameArray = roleName.split("\\s+");
			if(nameArray.length>0)
			{
				roleName=nameArray[0];
			}
				
				
			String output = roleName.substring(0,1).toUpperCase() + roleName.substring(1).toLowerCase();
			
			Phrase role = new Phrase("เรียน : "+output, font10);
			PdfPCell roleCell = new PdfPCell(role);
			roleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			roleCell.setBorder(0);
			
			
			Phrase custInfo = null ;
			if(cust.isCompany()){
				custInfo = new Phrase("ชื่อบริษัท  : "+cust.getCompanyName(), font10);
			}
			PdfPCell custInfoCell = new PdfPCell(custInfo);
			custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custInfoCell.setBorder(0);
			
			
			
			
			String custAdd1="";
			String custFullAdd1="";
			
			if(cust.getAdress()!=null){
				
				if(cust.getAdress().getAddrLine2()!=null){
					if(cust.getAdress().getLandmark()!=null){
						custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
					}else{
						custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
					}
				}else{
					if(cust.getAdress().getLandmark()!=null){
						custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
					}else{
						custAdd1=cust.getAdress().getAddrLine1();
					}
				}
				
				
				
				String pin="";
				if(cust.getAdress().getPin()!=0){
					pin=cust.getAdress().getPin()+"";
				}
				else{
					pin="";
				}
				if(cust.getAdress().getLocality()!=null){
//					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
					
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+cust.getAdress().getLocality()+"\n"+pin;

				}else{
					custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+pin;
				}
			}
			
			String custAddress="";
			
			 if(customerBranch!=null)
			 {
				
				custAddress=customerBranch.getBillingAddress().getCompleteAddress().trim();
				logger.log(Level.SEVERE,"billing address of customer branch " +custAddress);
			
			}
			else{
				custAddress = cust.getAdress().getCompleteAddress().trim();
				logger.log(Level.SEVERE,"billing address of customer " +custAddress);
			}
			Phrase custAddInfo = new Phrase("ที่อยู่ : "+custAddress, font10);
			PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
			custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			custAddInfoCell.setBorder(0);
			
			
			Phrase blank = new Phrase(" ", font10);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			blankCell.setBorder(0);
			
			
//			String cell="";
//			if(cust.getCellNumber1()!=null){
//				cell="Cell : "+cust.getCellNumber1()+"";
//			}else{
//				cell=" ";
//			}
//			
//			Phrase cellNo = new Phrase(cell, font10);
//			PdfPCell cellNoCell = new PdfPCell(cellNo);
//			cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			cellNoCell.setBorder(0);
//			
//			String email="";
//			if(cust.getEmail()!=null){
//				email=cust.getEmail();
//			}else{
//				email="";
//			}
//			Phrase emailPr = new Phrase("E-mail : "+email, font10);
//			PdfPCell emailCell = new PdfPCell(emailPr);
//			emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			emailCell.setBorder(0);
			
			PdfPTable customerTable=new PdfPTable(1);
			customerTable.setWidthPercentage(100);
			
			customerTable.addCell(toCell);
			customerTable.addCell(roleCell);
			customerTable.addCell(custInfoCell);
			customerTable.addCell(custAddInfoCell);
//			if(cust.getCellNumber1()!=0)
//			{
//			customerTable.addCell(cellNoCell);
//			}
//			if(email!=null&&!email.equals(""))
//			{	
//			customerTable.addCell(emailCell);
//			}
			PdfPCell customerCell = new PdfPCell();
			customerCell.addElement(customerTable);
			customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			customerCell.setBorder(0);
			
			
			
			PdfPTable headTbl=new PdfPTable(2);
			headTbl.setWidthPercentage(100f);
			
			headTbl.addCell(customerCell);
			headTbl.addCell(blankCell);
			
			PdfPCell parentCell = new PdfPCell();
			parentCell.addElement(headTbl);
			parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			parentCell.setBorder(0);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			parent.addCell(parentCell);
			parent.setSpacingAfter(15f);
			
			try {
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
				
		}
		
		private void createSubjectAndMsg() {
			
			Phrase sub=null;
		    sub = new Phrase("Subject : Renewal of Pest Management Contract ", font10);
			//logger.log(Level.SEVERE,"old contract Date"+oldContract.getEndDate());
			
			PdfPCell subCell = new PdfPCell(sub);
			subCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			subCell.setBorder(0);
			subCell.setPaddingBottom(5);
			
			Phrase blank = new Phrase("", font10bold);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			blankCell.setBorder(0);
			
			
			Paragraph blankparaOne = new Paragraph();
			blankparaOne.add(Chunk.NEWLINE);
			PdfPCell blankparaOneCell = new PdfPCell(blankparaOne);
			blankparaOneCell.setBorder(0);
			
			
			
			String firstName="";
			
     		
			 if(customerBranch!=null)
			 {
				 firstName = customerBranch.getPocName();
			}else{
			firstName=cust.getFullname();
			String[] nameArray = firstName.split("\\s+");
			if(nameArray.length>0){
				firstName=nameArray[0];
				}
			}
			
			String output = firstName.substring(0,1).toUpperCase() + firstName.substring(1).toLowerCase();


			
			Phrase dear = new Phrase("Dear Khun"+" "+output+",", font10);
			PdfPCell dearCell = new PdfPCell(dear);
			dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			dearCell.setBorder(0);
			
			Phrase blankph = new Phrase(" ", font8);
			PdfPCell blankcell = new PdfPCell(blankph);
			blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			blankcell.setBorder(0);
			
			
			//int maxServDuration=maxDuration();
			//logger.log(Level.SEVERE,"no of max service days "+maxServDuration);
			
			//String noOfServDays=numberOfDaysToMonthAndDay(maxServDuration);
			String title1 = "";
			Date renewDate = null;
			try{
			renewDate=new Date(con.getEndDate().getTime());
			Calendar c = Calendar.getInstance();
			c.setTime(renewDate);
			c.add(Calendar.DATE,  1);
			renewDate=c.getTime();
			}catch(Exception e){
				e.printStackTrace();
			}
			Date startDate = null;
			Date endDate = null;
			int k=0;
			int duration=0;
			try{
//			for(SalesLineItem item:con.getItems()){
//				if(item.getDuration()>duration){
//					duration=item.getDuration();
//				}
//			}
//	
			duration=maxDuration();
			startDate=DateUtility.addDaysToDate(con.getEndDate(), 1);
			endDate=DateUtility.addDaysToDate(startDate, duration);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			int i = 0;
			double sum=0;
			
			if(conRenw.getItems().get(i).getPrice()!=0){
				sum=conRenw.getItems().get(i).getPrice();
			}	
			
			String amtInWordsVal="";
			if(sum!=0){
			amtInWordsVal =SalesInvoicePdf.convert(sum)+" Only";
			}
			
			
			
			
			title1="Since your contract of pest management service will expire on date "+fmt.format(con.getEndDate())+","+"therefore we would like to request you to confirm an extension of the contract for another period effective from "+fmt.format(startDate)+" "+"to "+fmt.format(endDate)+" "+"with baht "+df2.format(sum)+"/- "+"("+amtInWordsVal+") "+"excluding VAT as applicable.The work specifications and conditions of this renewal contract are as same as existing contract no "+con.getCount()+".";
			
			Phrase msg = new Phrase(title1, font10);
			PdfPCell msgCell = new PdfPCell(msg);
			msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			msgCell.setBorder(0);
			
			String title2 = "";
			
			title2="We thank you very much in advance for your kind consideration and would like to request you to sign below for confirmation of contract renewal and return this letter to us. We shall send you a full contract after receipt of this letter for both signatures.";
			
			Phrase msg1 = new Phrase(title2, font10);
			PdfPCell msgCell1 = new PdfPCell(msg1);
			msgCell1.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			msgCell1.setBorder(0);
			
			String contact="";
			if(branchDt!=null) {
				if(branchDt.getCellNumber1()!=null)
						contact=branchDt.getCellNumber1()+"";
//				
//				if(branchDt.getCellNumber2()!=0&&branchDt.getCellNumber2()!=null) {
//					contact=contact+" / "+branchDt.getCellNumber2()+"";
//				}
				
				/**
				 * @author Vijay Date :- 07-12-2021
				 * Des :- Adding country code(from country master) in cell number.
				 */
				contact = serverapputility.getMobileNoWithCountryCode(contact, branchDt.getAddress().getCountry(), comp.getCompanyId());

				if(branchDt.getCellNumber2()!=0&&branchDt.getCellNumber2()!=null) {
//					contact=contact+" / "+branchDt.getCellNumber2()+"";
					String cellnumber2 = branchDt.getCellNumber2()+"";
					cellnumber2 = serverapputility.getMobileNoWithCountryCode(cellnumber2, branchDt.getAddress().getCountry(), comp.getCompanyId());
					contact=contact+" / "+cellnumber2+"";
				}
				
				if(branchDt.getLandline()!=0 && branchDt.getLandline()!=null) {
					contact=contact+" / "+branchDt.getLandline()+"";
				}

			
			}else if(comp!=null) {
				if(comp.getCellNumber1()!=null)
					contact=comp.getCellNumber1()+"";
				contact = serverapputility.getMobileNoWithCountryCode(contact, comp.getAddress().getCountry(), comp.getCompanyId());

				if(comp.getCellNumber2()!=0&&comp.getCellNumber2()!=null) {
//					contact=contact+" / "+branchDt.getCellNumber2()+"";
					String cellnumber2 = comp.getCellNumber2()+"";
					cellnumber2 = serverapputility.getMobileNoWithCountryCode(cellnumber2, comp.getAddress().getCountry(), comp.getCompanyId());
					contact=contact+" / "+cellnumber2+"";
				}
				
				
				if(comp.getLandline()!=0 && comp.getLandline()!=null) {
					contact=contact+" / "+comp.getLandline()+"";
				}			
			}
			
			
			
			String title3 = "";
			
			title3="We look forward to serving you our best again. Should you have any comment or suggestions please do not hesitate to contact us at "+contact+".";
			
			Phrase msg2 = new Phrase(title3, font10);
			PdfPCell msgCell3 = new PdfPCell(msg2);
			msgCell3.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
			msgCell3.setBorder(0);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			parent.addCell(subCell);
			parent.addCell(blankCell);
			parent.addCell(dearCell);
			parent.addCell(blankCell);
			parent.addCell(msgCell);
			parent.addCell(blankCell);
			parent.addCell(blankCell);
		    parent.addCell(blankparaOneCell);
			parent.addCell(msgCell1);
			parent.addCell(blankCell);
			parent.addCell(blankCell);
			parent.addCell(blankparaOneCell);
			parent.addCell(msgCell3);
			parent.addCell(blankCell);
			parent.addCell(blankparaOneCell);
			parent.setSpacingAfter(15f);
			
			try {
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
		
		private void createSubjectAndMsgForThai() {
			
			Phrase sub=null;
		    sub = new Phrase("Subject : การต่อสัญญาบริการกำจัดแมลง ", font10);
			//logger.log(Level.SEVERE,"old contract Date"+oldContract.getEndDate());
			
			PdfPCell subCell = new PdfPCell(sub);
			subCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			subCell.setBorder(0);
			subCell.setPaddingBottom(5);
			
			Phrase blank = new Phrase("", font10bold);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			blankCell.setBorder(0);
			
			Paragraph blankparaOne = new Paragraph();
			blankparaOne.add(Chunk.NEWLINE);
			PdfPCell blankparaOneCell = new PdfPCell(blankparaOne);
			blankparaOneCell.setBorder(0);
			
			String firstName="";
			
     		
			if(customerBranch!=null) {

			firstName = customerBranch.getPocName();
			}else{
			firstName=cust.getFullname();
			String[] nameArray = firstName.split("\\s+");
			if(nameArray.length>0){
				firstName=nameArray[0];
				}
			}
			
			String output = firstName.substring(0,1).toUpperCase() + firstName.substring(1).toLowerCase();


			
			Phrase dear = new Phrase("Dear Khun"+" "+output+",", font10);
			PdfPCell dearCell = new PdfPCell(dear);
			dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			dearCell.setBorder(0);
			
			Phrase blankph = new Phrase(" ", font8);
			PdfPCell blankcell = new PdfPCell(blankph);
			blankcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			blankcell.setBorder(0);
			//int maxServDuration=maxDuration();
			//logger.log(Level.SEVERE,"no of max service days "+maxServDuration);
			
			//String noOfServDays=numberOfDaysToMonthAndDay(maxServDuration);
			String title1 = "";
			Date renewDate = null;
			try{
			renewDate=new Date(con.getEndDate().getTime());
			Calendar c = Calendar.getInstance();
			c.setTime(renewDate);
			c.add(Calendar.DATE,  1);
			renewDate=c.getTime();
			}catch(Exception e){
				e.printStackTrace();
			}
			Date startDate = null;
			Date endDate = null;
			int k=0;
			int duration=0;
			try{
//				if(conProd.getDuaration()>duration) {
//					duration=conProd.getDuaration();
//				}
				duration=maxDuration();
				logger.log(Level.SEVERE,"no of max service days "+duration);
			startDate=DateUtility.addDaysToDate(con.getEndDate(), 1);
			endDate=DateUtility.addDaysToDate(startDate, duration);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			int i = 0;
			double sum=0;
			
			if(conRenw.getItems().get(i).getPrice()!=0){
				sum=conRenw.getItems().get(i).getPrice();
			}	
			
			String amtInWordsVal="";
			if(sum!=0){
//			amtInWordsVal =SalesInvoicePdf.convert(sum)+" Only";
//			amtInWordsVal = EnglishNumberToWords.convert(sum)+" Only";

			BahtText bahtText=new BahtText();
			DecimalFormat df1 = new DecimalFormat("#,###.00");
			amtInWordsVal =	bahtText.getBath(df1.format(sum));
			}
			
			
			
			
//			title1="Since your contract of pest management service will expire on date"
//					+ " "+fmt3.format(con.getEndDate())+","+"therefore we would like to request you "
//							+ "to confirm an extension of the contract for another"
//							+ " period effective from "+fmt3.format(startDate)+" "+"to "+fmt3.format(endDate)+""
//									+ " "+"with baht "+df2.format(sum)+"/- "+"("+amtInWordsVal+") "+"excluding "
//											+ "VAT as applicable.The work specifications and conditions of this renewal "
//											+ "contract are as same as existing contract no "+con.getCount()+".";
			
			//title1="Ã Â¸â„¢Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸â€¡Ã Â¸Ë†Ã Â¸Â²Ã Â¸ï¿½Ã Â¸ÂªÃ Â¸Â±Ã Â¸ï¿½Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â£Ã Â¸ï¿½Ã Â¸Â³Ã Â¸Ë†Ã Â¸Â±Ã Â¸â€�Ã Â¹ï¿½Ã Â¸Â¡Ã Â¸Â¥Ã Â¸â€¡Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡Ã Â¸â€”Ã Â¹Ë†Ã Â¸Â²Ã Â¸â„¢Ã Â¸Ë†Ã Â¸Â°Ã Â¸Â«Ã Â¸Â¡Ã Â¸â€�Ã Â¸Â­Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â¸Ã Â¹Æ’Ã Â¸â„¢Ã Â¸Â§Ã Â¸Â±Ã Â¸â„¢Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë† "+fmt3.format(con.getEndDate())+","+"Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€”Ã Â¸Â¯ Ã Â¸Ë†Ã Â¸Â¶Ã Â¸â€¡Ã Â¹Æ’Ã Â¸â€žÃ Â¸Â£Ã Â¹Ë†Ã Â¸â€šÃ Â¸Â­Ã Â¹Æ’Ã Â¸Â«Ã Â¹â€°Ã Â¸â€”Ã Â¹Ë†Ã Â¸Â²Ã Â¸â„¢Ã Â¸Â¢Ã Â¸Â·Ã Â¸â„¢Ã Â¸Â¢Ã Â¸Â±Ã Â¸â„¢Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â£Ã Â¸â€¢Ã Â¹Ë†Ã Â¸Â­Ã Â¸ÂªÃ Â¸Â±Ã Â¸ï¿½Ã Â¸ï¿½Ã Â¸Â²Ã Â¹â€šÃ Â¸â€�Ã Â¸Â¢Ã Â¸Ë†Ã Â¸Â°Ã Â¸Â¡Ã Â¸ÂµÃ Â¸Å“Ã Â¸Â¥Ã Â¸â€¢Ã Â¸Â±Ã Â¹â€°Ã Â¸â€¡Ã Â¹ï¿½Ã Â¸â€¢Ã Â¹Ë†"+fmt3.format(startDate)+""+" Ã Â¸â€“Ã Â¸Â¶Ã Â¸â€¡"+fmt3.format(endDate)+""+ " "+" Ã Â¸Ë†Ã Â¸Â³Ã Â¸â„¢Ã Â¸Â§Ã Â¸â„¢Ã Â¹â‚¬Ã Â¸â€¡Ã Â¸Â´Ã Â¸â„¢  "+df2.format(sum)+"/- "+"("+amtInWordsVal+") "+"  Ã Â¹â€žÃ Â¸Â¡Ã Â¹Ë†Ã Â¸Â£Ã Â¸Â§Ã Â¸Â¡Ã Â¸ Ã Â¸Â²Ã Â¸Â©Ã Â¸ÂµÃ Â¸Â¡Ã Â¸Â¹Ã Â¸Â¥Ã Â¸â€žÃ Â¹Ë†Ã Â¸Â²Ã Â¹â‚¬Ã Â¸Å¾Ã Â¸Â´Ã Â¹Ë†Ã Â¸Â¡ Ã Â¸Â£Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â¥Ã Â¸Â°Ã Â¹â‚¬Ã Â¸Â­Ã Â¸ÂµÃ Â¸Â¢Ã Â¸â€�Ã Â¹ï¿½Ã Â¸Â¥Ã Â¸Â°Ã Â¹â‚¬Ã Â¸â€¡Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸â„¢Ã Â¹â€žÃ Â¸â€šÃ Â¸ÂªÃ Â¸Â³Ã Â¸Â«Ã Â¸Â£Ã Â¸Â±Ã Â¸Å¡Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â£Ã Â¸â€¢Ã Â¹Ë†Ã Â¸Â­Ã Â¸ÂªÃ Â¸Â±Ã Â¸ï¿½Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â±Ã Â¸â€¡Ã Â¸â€žÃ Â¸â€¡Ã Â¹â‚¬Ã Â¸Â«Ã Â¸Â¡Ã Â¸Â·Ã Â¸Â­Ã Â¸â„¢Ã Â¹â‚¬Ã Â¸â€�Ã Â¸Â´Ã Â¸Â¡Ã Â¸â€¢Ã Â¸Â²Ã Â¸Â¡Ã Â¸ÂªÃ Â¸Â±Ã Â¸ï¿½Ã Â¸ï¿½Ã Â¸Â²Ã Â¹â‚¬Ã Â¸Â¥Ã Â¸â€šÃ Â¸â€”Ã Â¸ÂµÃ Â¹Ë† "+con.getCount()+".";
			title1="นื่องจากสัญญาบริการกำจัดแมลงของท่านจะหมดอายุในวันที่ "+fmt.format(con.getEndDate())+","+"บริษัทฯ จึงใคร่ขอให้ท่านยืนยันการต่อสัญญาโดยจะมีผลตั้งแต่"+fmt.format(startDate)+""+" ถึง"+fmt.format(endDate)+""+ " "+" จำนวนเงิน  "+df2.format(sum)+"/- "+"("+amtInWordsVal+") "+"  ไม่รวมภาษีมูลค่าเพิ่ม รายละเอียดและเงื่อนไขสำหรับการต่อสัญญายังคงเหมือนเดิมตามสัญญาเลขที่ "+con.getCount()+".";
			Phrase msg = new Phrase(title1, font10);
			PdfPCell msgCell = new PdfPCell(msg);
			msgCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			msgCell.setBorder(0);
			
			String title2 = " ";
			
			//title2="We thank you very much in advance for your kind consideration and would like to request you to sign below for confirmation of contract renewal and return this letter to us. We shall send you a full contract after receipt of this letter for both signatures.";
			//title2="Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€”Ã Â¸Â¯ Ã Â¸â€šÃ Â¸Â­Ã Â¸â€šÃ Â¸Â­Ã Â¸Å¡Ã Â¸Å¾Ã Â¸Â£Ã Â¸Â°Ã Â¸â€žÃ Â¸Â¸Ã Â¸â€œÃ Â¹â‚¬Ã Â¸â€ºÃ Â¹â€¡Ã Â¸â„¢Ã Â¸Â­Ã Â¸Â¢Ã Â¹Ë†Ã Â¸Â²Ã Â¸â€¡Ã Â¸ÂªÃ Â¸Â¹Ã Â¸â€¡Ã Â¸Â¥Ã Â¹Ë†Ã Â¸Â§Ã Â¸â€¡Ã Â¸Â«Ã Â¸â„¢Ã Â¹â€°Ã Â¸Â²Ã Â¸ÂªÃ Â¸Â³Ã Â¸Â«Ã Â¸Â£Ã Â¸Â±Ã Â¸Å¡Ã Â¸â€žÃ Â¸Â§Ã Â¸Â²Ã Â¸Â¡Ã Â¹â€žÃ Â¸Â§Ã Â¹â€°Ã Â¸Â§Ã Â¸Â²Ã Â¸â€¡Ã Â¹Æ’Ã Â¸Ë†Ã Â¹Æ’Ã Â¸â„¢Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â£Ã Â¹Æ’Ã Â¸Å Ã Â¹â€°Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â£Ã Â¸â€šÃ Â¸Â­Ã Â¸â€¡Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€”Ã Â¸Â¯ Ã Â¹ï¿½Ã Â¸Â¥Ã Â¸Â°Ã Â¸â€šÃ Â¸Â­Ã Â¹Æ’Ã Â¸Â«Ã Â¹â€°Ã Â¸â€”Ã Â¹Ë†Ã Â¸Â²Ã Â¸â„¢Ã Â¸Â¥Ã Â¸â€¡Ã Â¸â„¢Ã Â¸Â²Ã Â¸Â¡Ã Â¸â€�Ã Â¹â€°Ã Â¸Â²Ã Â¸â„¢Ã Â¸Â¥Ã Â¹Ë†Ã Â¸Â²Ã Â¸â€¡Ã Â¹â‚¬Ã Â¸Å¾Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸Â¢Ã Â¸Â·Ã Â¸â„¢Ã Â¸Â¢Ã Â¸Â±Ã Â¸â„¢Ã Â¸ï¿½Ã Â¸Â²Ã Â¸Â£Ã Â¸â€¢Ã Â¹Ë†Ã Â¸Â­Ã Â¸ÂªÃ Â¸Â±Ã Â¸ï¿½Ã Â¸ï¿½Ã Â¸Â²Ã Â¹ï¿½Ã Â¸Â¥Ã Â¸Â°Ã Â¸ÂªÃ Â¹Ë†Ã Â¸â€¡Ã Â¸â€žÃ Â¸Â·Ã Â¸â„¢Ã Â¸Ë†Ã Â¸â€�Ã Â¸Â«Ã Â¸Â¡Ã Â¸Â²Ã Â¸Â¢Ã Â¸â„¢Ã Â¸ÂµÃ Â¹â€°Ã Â¸ï¿½Ã Â¸Â¥Ã Â¸Â±Ã Â¸Å¡Ã Â¸Â¡Ã Â¸Â²Ã Â¸â€”Ã Â¸ÂµÃ Â¹Ë†Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€”Ã Â¸Â¯ Ã Â¹â€šÃ Â¸â€�Ã Â¸Â¢Ã Â¸Å¡Ã Â¸Â£Ã Â¸Â´Ã Â¸Â©Ã Â¸Â±Ã Â¸â€”Ã Â¸Â¯ Ã Â¸Ë†Ã Â¸Â°Ã Â¸Ë†Ã Â¸Â±Ã Â¸â€�Ã Â¸â€”Ã Â¸Â³Ã Â¸ÂªÃ Â¸Â±Ã Â¸ï¿½Ã Â¸ï¿½Ã Â¸Â²Ã Â¹â‚¬Ã Â¸Å¾Ã Â¸Â·Ã Â¹Ë†Ã Â¸Â­Ã Â¸Â¥Ã Â¸â€¡Ã Â¸â„¢Ã Â¸Â²Ã Â¸Â¡Ã Â¸â€”Ã Â¸Â±Ã Â¹â€°Ã Â¸â€¡Ã Â¸ÂªÃ Â¸Â­Ã Â¸â€¡Ã Â¸ï¿½Ã Â¹Ë†Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â«Ã Â¸Â¥Ã Â¸Â±Ã Â¸â€¡Ã Â¸Ë†Ã Â¸Â²Ã Â¸ï¿½Ã Â¹â€žÃ Â¸â€�Ã Â¹â€°Ã Â¸Â£Ã Â¸Â±Ã Â¸Å¡Ã Â¸Ë†Ã Â¸â€�Ã Â¸Â«Ã Â¸Â¡Ã Â¸Â²Ã Â¸Â¢Ã Â¸Â¢Ã Â¸Â·Ã Â¸â„¢Ã Â¸Â¢Ã Â¸Â±Ã Â¸â„¢";
			//title2 = "บริษัทฯขอขอบพระคุณเป็นอย่างสูงล่วงหน้าสำหรับความไว้วางใจในการใช้บริการของบริษัทฯ และขอให้ท่านลงนามด้านล่างเพื่อยืนยันการต่อสัญญาและส่งคืนจดหมายนี้กลับมาที่บริษัทฯ โดยบริษัทฯจะจัดทำสัญญาเพื่อลงนามทั้งสองฝ่ายหลังจากได้รับจดหมายยืนยัน";
			StringBuilder sb = new StringBuilder();
			final String BLANK_SPACE=" ";
			sb.append("บริษัทฯขอขอบพระคุณเป็นอย่างสูงล่วงหน้าสำหรับความไว้วางใจในการใช้บริการของบริษัทฯ");
		//	sb.append("\n");
			sb.append("และขอให้ท่านลงนามด้านล่างเพื่อยืนยันการต่อสัญญาและส่งคืนจดหมายนี้กลับมาที่บริษัทฯ");
			sb.append(BLANK_SPACE);
			sb.append("โดยบริษัทฯจะจัดทำสัญญาเพื่อลงนามทั้งสองฝ่ายหลังจากได้รับจดหมายยืนยัน");
			sb.append(".");
			title2=sb.toString();
			Phrase msg1= new Phrase(title2, font10);
			PdfPCell msgCell1 = new PdfPCell(msg1);
			msgCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			msgCell1.setBorder(0);
			String contact="";
			if(branchDt!=null) {
				if(branchDt.getCellNumber1()!=null)
					contact=branchDt.getCellNumber1()+"";
				/**
				 * @author Vijay Date :- 07-12-2021
				 * Des :- Adding country code(from country master) in cell number.
				 */
				contact = serverapputility.getMobileNoWithCountryCode(contact, branchDt.getAddress().getCountry(), comp.getCompanyId());

				if(branchDt.getCellNumber2()!=0&&branchDt.getCellNumber2()!=null) {
//					contact=contact+" / "+branchDt.getCellNumber2()+"";
					String cellnumber2 = branchDt.getCellNumber2()+"";
					cellnumber2 = serverapputility.getMobileNoWithCountryCode(cellnumber2, branchDt.getAddress().getCountry(), comp.getCompanyId());
					contact=contact+" / "+cellnumber2+"";
				}
				
				
				if(branchDt.getLandline()!=0 && branchDt.getLandline()!=null) {
					contact=contact+" / "+branchDt.getLandline()+"";
				}			
			}else if(comp!=null) {
				if(comp.getCellNumber1()!=null)
					contact=comp.getCellNumber1()+"";
				contact = serverapputility.getMobileNoWithCountryCode(contact, comp.getAddress().getCountry(), comp.getCompanyId());

				if(comp.getCellNumber2()!=0&&comp.getCellNumber2()!=null) {
//					contact=contact+" / "+branchDt.getCellNumber2()+"";
					String cellnumber2 = comp.getCellNumber2()+"";
					cellnumber2 = serverapputility.getMobileNoWithCountryCode(cellnumber2, comp.getAddress().getCountry(), comp.getCompanyId());
					contact=contact+" / "+cellnumber2+"";
				}
				
				
				if(comp.getLandline()!=0 && comp.getLandline()!=null) {
					contact=contact+" / "+comp.getLandline()+"";
				}			
			}
			
			
			String title3 = "";
			
			title3="บริษัทฯ หวังว่าจะได้รับโอกาสให้บริการท่านอย่างดีที่สุดอีกครั้ง หากท่านมีข้อแนะนำหรือความเห็นเพิ่มเติมสามารถติดต่อกลับมาที่บริษัทฯ เบอร์  "+contact+".";
			Phrase msg2 = new Phrase(title3, font10);
			PdfPCell msgCell3 = new PdfPCell(msg2);
			msgCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
			msgCell3.setBorder(0);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			parent.addCell(subCell);
			parent.addCell(blankCell);
			parent.addCell(dearCell);
			parent.addCell(blankCell);
			parent.addCell(msgCell);
			parent.addCell(blankCell);
			parent.addCell(blankCell);
		    parent.addCell(blankparaOneCell);
			parent.addCell(msgCell1);
			parent.addCell(blankCell);
			parent.addCell(blankCell);
			parent.addCell(blankparaOneCell);
			parent.addCell(msgCell3);
			parent.addCell(blankCell);
			parent.addCell(blankparaOneCell);
			parent.setSpacingAfter(15f);
			
			try {
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
		
		public int maxDuration() {
			 ArrayList<Integer> servDurList= new ArrayList<Integer>();
			   
//			 if(conRenw.getItems().get(k).getDuration()!=0) {
//					duration=conRenw.getItems().get(k).getDuration();
			 
			   for(int i=0;i<conRenw.getItems().size();i++){
				   servDurList.add(conRenw.getItems().get(i).getDuration());
			   }
			   Collections.sort(servDurList); 
			   return servDurList.get(servDurList.size() - 1);
		}

		private void createFooterDetailTab() {
			PdfPTable footerTab= new PdfPTable(2);
			footerTab.setWidthPercentage(100);
			
			try {
				footerTab.setWidths(new float[]{40,60});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPTable leftTab= new PdfPTable(1);
			footerTab.setWidthPercentage(100);
			
			Phrase invoiceto = new Phrase(" Yours faithfully ,", font10);
			PdfPCell invoicetocell = new PdfPCell(invoiceto);
			invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicetocell.setBorder(0);
			leftTab.addCell(invoicetocell);
			
			Phrase blankPhrase = new Phrase(" ", font10);
			PdfPCell blank = new PdfPCell(blankPhrase);
			blank.setBorder(0);
			leftTab.addCell(blank);
			
			Phrase name = new Phrase("Name : ", font10); //Position
			PdfPCell namecell = new PdfPCell(name);
			namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			namecell.setBorder(0);
			leftTab.addCell(namecell);
			
			Phrase position = new Phrase("Position : ", font10); //Position
			PdfPCell positioncell = new PdfPCell(position);
			positioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
			positioncell.setBorder(0);
			leftTab.addCell(positioncell);
			
			Phrase Date = new Phrase("Date : ", font10); //Position
			PdfPCell Datecell = new PdfPCell(Date);
			Datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			Datecell.setBorder(0);
			leftTab.addCell(Datecell);
			
			PdfPTable rightTab= new PdfPTable(1);
			footerTab.setWidthPercentage(100);
			
			Phrase invoiceto1 = new Phrase(" Confirmation to Renewal of Contract ", font10);
			PdfPCell invoicetocell1 = new PdfPCell(invoiceto1);
			invoicetocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicetocell1.setBorder(0);
			rightTab.addCell(invoicetocell1);
			
			Phrase blankPhrase1 = new Phrase("Authorized Sign : ", font10);
			PdfPCell blank1 = new PdfPCell(blankPhrase1);
			blank1.setBorder(0);
			rightTab.addCell(blank1);
			
			Phrase name1 = new Phrase("Name : ", font10); //Position
			PdfPCell namecell1 = new PdfPCell(name1);
			namecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			namecell1.setBorder(0);
			rightTab.addCell(namecell1);
			
			Phrase position1 = new Phrase("Position : ", font10); //Position
			PdfPCell positioncell1 = new PdfPCell(position1);
			positioncell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			positioncell1.setBorder(0);
			rightTab.addCell(positioncell1);
			
			Phrase Date1 = new Phrase("Date : ", font10); //Position
			PdfPCell Datecell1 = new PdfPCell(Date1);
			Datecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			Datecell1.setBorder(0);
			rightTab.addCell(Datecell1);
			
			
			PdfPCell leftCell = new PdfPCell(leftTab);
			PdfPCell rightCell = new PdfPCell(rightTab);
			
			
			footerTab.addCell(leftCell);
			footerTab.addCell(rightCell);
		
		try {
			document.add(footerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			
	}	
		
		private void createFooterDetailTabForThai() {
			PdfPTable footerTab= new PdfPTable(2);
			footerTab.setWidthPercentage(100);
			
			try {
				footerTab.setWidths(new float[]{40,60});
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			PdfPTable leftTab= new PdfPTable(1);
			footerTab.setWidthPercentage(100);
			
			Phrase invoiceto = new Phrase(" Yours faithfully ,", font10);
			PdfPCell invoicetocell = new PdfPCell(invoiceto);
			invoicetocell.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicetocell.setBorder(0);
			leftTab.addCell(invoicetocell);
			
			Phrase blankPhrase = new Phrase(" ", font10);
			PdfPCell blank = new PdfPCell(blankPhrase);
			blank.setBorder(0);
			leftTab.addCell(blank);
			
			Phrase name = new Phrase("Name : ", font10); //Position
			PdfPCell namecell = new PdfPCell(name);
			namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			namecell.setBorder(0);
			leftTab.addCell(namecell);
			
			Phrase position = new Phrase("Position : ", font10); //Position
			PdfPCell positioncell = new PdfPCell(position);
			positioncell.setHorizontalAlignment(Element.ALIGN_LEFT);
			positioncell.setBorder(0);
			leftTab.addCell(positioncell);
			
			Phrase Date = new Phrase("Date : ", font10); //Position
			PdfPCell Datecell = new PdfPCell(Date);
			Datecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			Datecell.setBorder(0);
			leftTab.addCell(Datecell);
			
			PdfPTable rightTab= new PdfPTable(1);
			footerTab.setWidthPercentage(100);
			
			Phrase invoiceto1 = new Phrase(" ยืนยันการต่อสัญญา ", font10);
			PdfPCell invoicetocell1 = new PdfPCell(invoiceto1);
			invoicetocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			invoicetocell1.setBorder(0);
			rightTab.addCell(invoicetocell1);
			
			Phrase blankPhrase1 = new Phrase("ลายมือชื่อผู้มีอำนาจ : ", font10);
			PdfPCell blank1 = new PdfPCell(blankPhrase1);
			blank1.setBorder(0);
			rightTab.addCell(blank1);
			
			Phrase name1 = new Phrase("ชื่อ : ", font10); //Position
			PdfPCell namecell1 = new PdfPCell(name1);
			namecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			namecell1.setBorder(0);
			rightTab.addCell(namecell1);
			
			Phrase position1 = new Phrase("ตำแหน่ง : ", font10); //Position
			PdfPCell positioncell1 = new PdfPCell(position1);
			positioncell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			positioncell1.setBorder(0);
			rightTab.addCell(positioncell1);
			
			
			PdfPCell leftCell = new PdfPCell(leftTab);
			PdfPCell rightCell = new PdfPCell(rightTab);
			
			
			footerTab.addCell(leftCell);
			footerTab.addCell(rightCell);
		
		try {
			document.add(footerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			
	}
		
		private void createCompanyNameAsHeader(Document doc, Company comp) {
			
			DocumentUpload document =comp.getUploadHeader();

			//patch
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(520f);
				image2.setAbsolutePosition(40f,725f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
//			
//			try
//			{
//			Image image1=Image.getInstance("images/header.jpg");
//			image1.scalePercent(15f);
//			image1.scaleAbsoluteWidth(520f);
//			image1.setAbsolutePosition(40f,725f);	
//			doc.add(image1);
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
			}
		
		private void createCompanyNameAsFooter(Document doc, Company comp) {
			
			
			DocumentUpload document =comp.getUploadFooter();

			//patch
			String hostUrl;
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(15f);
				image2.scaleAbsoluteWidth(520f);
				image2.setAbsolutePosition(40f,10f); //40f	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
//			try
//			{
//			Image image1=Image.getInstance("images/pestomatic _letterhead-1.jpg");
//			image1.scalePercent(15f);
//			image1.scaleAbsoluteWidth(520f);
//			image1.setAbsolutePosition(40f,40f);	
//			doc.add(image1);
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
			}

		private void createBlankforUPC() {
			
			Phrase blankphrase=new Phrase("",font8);
			PdfPCell blankCell=new PdfPCell();
			blankCell.addElement(blankphrase);
			blankCell.setBorder(0);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
			titlepdftable.addCell(blankCell);
			titlepdftable.addCell(blankCell);
			
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
			parent.addCell(titlePdfCell);
			
			
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				//document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}

		}

}
