package com.slicktechnologies.server.addhocprinting;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.Pfm2afm;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceCharges;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.ProductDetailsPO;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ServiceInvoicePdf{
	
	ArrayList<Service> service;
	//List<SalesLineItem> products;
	List<SalesOrderProductLineItem> salesProd;
	List<ContractCharges> billingTaxesLis;
	List<ContractCharges> billingChargesLis;
	List<BillingDocumentDetails> billingDoc;
	List<PaymentTerms> payTermsLis;
	List<ArticleType> articletype;
	Customer cust;
	ProcessConfiguration processConfig;
	List<ProductOtherCharges> contractTaxesLis;
	List<ProductOtherCharges> contractChargesLis;
	List<PaymentTerms> payTerms= new ArrayList<PaymentTerms>();
	Boolean paytermsflag=false;
	
	
	/**
	 * rohan added this flag for oversales 
	 * This is used for to remove service address column and authority and add 
	 * "This is 
	 */
		Boolean autorityFlag=false;
	/**
	 * 
	 */
		
	/**
	 * rohan added this flag for universal pest control 
	 * This is used to print vat no and other article information  	
	 */
		
		Boolean UniversalFlag = false;
	/**
	 *   ends here 
	 */
		
	/**
	 * rohan added this code for printing establishment year in service invoice pdf
	 * Date :25/2/2017	
	 */
		
		String establishmentYr="";		
	/**
	 * ends here  	
	 */
		
	/**
	 * Rohan added this for Universal pest 	
	 */
		Boolean multipleCompanyName = false;
		/**
		 * ends here 
		 */
		
	
	Boolean ServiceAndBillingAdressSamePrintOnes = false;
	SuperProduct sup;
	ArrayList<SuperProduct>stringlis= new ArrayList<SuperProduct>();
	List<CustomerBranchDetails> custbranchlist;
	List<InvoiceCharges> conbillingTaxesLis = new ArrayList<InvoiceCharges>();
	List<InvoiceCharges> conbillingChargesLis= new ArrayList<InvoiceCharges>();
	PdfPTable chargetaxtableToNewPage = new PdfPTable(2);
	PdfPTable netPayableTable = new PdfPTable(2);
	
	List<CustomerBranchDetails> customerbranchlist;
	//******************rohan taken variables ************8
		int both=0;
		int discAmt=0;
		int discPer=0;
		int nothing=0;
	
	String functionalYear=null;
	//SalesOrder salesOrderEntity;
	Company comp;
	Contract con;
	public  Document document;
	Invoice invoiceentity;
	List<PaymentTerms> arrPayTerms;
	String invComment="";
	BillingDocument billEntity;
	
	String invoiceOrderType="";
	private  Font font16boldul,font12bold,font8bold,font8,font9bold,font12boldul,font10boldul,font12,font16bold,font10,font10bold,font14bold;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	double total=0;
	
	Phrase chunk;
	int eva=100;
	int firstBreakPoint = 5;
	int BreakPoint = 5;
	int count=0;
	float blankLines;
	int flag=0;
	int flag1=0;
	float[] columnWidths = {0.8f, 3.5f, 2.0f,1.0f,1.1f,0.9f,0.8f,1.2f,1.1f,1.0f};
	float[] columnWidths8 = {0.8f, 3.5f, 1.5f, 1.1f,0.9f,0.8f,1.2f,1.1f};
	float[] columnWidths12 = {1.7f,0.3f,2.5f,1.7f,0.3f,2.5f};
	
	float[] columnWidths123 = {0.8f,0.2f,3.5f};
	Logger logger = Logger.getLogger("NameOfYourLogger");
	boolean upcflag=false;
	boolean disclaimerflag=false;
	PdfPTable remainigInfotable;
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	
	final static String disclaimerText="I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";
	
	public 	ServiceInvoicePdf()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10boldul= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD|Font.UNDERLINE);
		font9bold= new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
	}
	
	public void setInvoice(long count)
	{
		//Load Invoice
			invoiceentity=ofy().load().type(Invoice.class).id(count).now();
			
			billingDoc=invoiceentity.getArrayBillingDocument();
			invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
			arrPayTerms=invoiceentity.getArrPayTerms();
		//Load Customer
		if(invoiceentity.getCompanyId()==null)
			cust=ofy().load().type(Customer.class).filter("count",invoiceentity.getPersonInfo().getCount()).first().now();
		else
			cust=ofy().load().type(Customer.class).filter("count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).first().now();
			
		//Load Company
		if(invoiceentity.getCompanyId()==null)
		   comp=ofy().load().type(Company.class).first().now();
		else
		   comp=ofy().load().type(Company.class).filter("companyId",invoiceentity.getCompanyId()).first().now();
		
		if(invoiceentity.getCompanyId()!=null)
			con=ofy().load().type(Contract.class).filter("count",invoiceentity.getContractCount()).filter("companyId",invoiceentity.getCompanyId()).first().now();
		else
			con=ofy().load().type(Contract.class).filter("count", invoiceentity.getContractCount()).first().now();
		
		if(invoiceentity.getCompanyId()==null)
			custbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).list();
		else
			custbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).list();
		
		
		/****************************** vijay ************************/
		
		System.out.println("Branch name======"+invoiceentity.getCustomerBranch());
		if(invoiceentity.getCompanyId()==null)
			customerbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).list();
		else
			customerbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).filter("companyId", invoiceentity.getCompanyId()).list();
		
		System.out.println("Banch updated====="+invoiceentity.getCustomerBranch());
		/****************************** vijay ************************/
		
		
		/*********************Display Payment Terms Flag*****************************/
		
		if(invoiceentity.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceInvoicePayTerm")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						paytermsflag=true;
					}
					
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("REMOVEAUTHORITYTERM")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						autorityFlag=true;
					}
					
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("OnlyForUniversal")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						UniversalFlag=true;
					}
				}
			}
		}
		
		
		
/*********************if service Address and Billing Address is same display ones (Vcare)*****************************/
		
		if(invoiceentity.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceAndBillingAdressSamePrintOne")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						ServiceAndBillingAdressSamePrintOnes=true;
					}
					
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintMultipleCompanyNamesFromInvoiceGroup")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						multipleCompanyName=true;
					}
				}
			}
		}
		
		
		/************************************Letter Head Flag*******************************/
		
		if(invoiceentity.getCompanyId()!=null)
		{
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag =true;
					}
				}
			}
		 }
		
		
		 Date todaysDate = invoiceentity.getInvoiceDate();
		 logger.log(Level.SEVERE,"system date :::"+todaysDate);		 
		getFunnctionalYear(todaysDate);
		/**********************************Disclaimer Flag******************************************/
		
		if(invoiceentity.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						disclaimerflag=true;
					}
				}
			}
		}
		
//		if(invoiceentity.getCompanyId()!=null)
//			billEntity=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("invoiceCount", invoiceentity.getCount()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
//		else
//			billEntity=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("invoiceCount", invoiceentity.getCount()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
//
//		System.out.println("billEntity"+billEntity);
		 salesProd=invoiceentity.getSalesOrderProducts();
		 System.out.println("AAAAAAAAAA sales prod size"+salesProd.size());
		 billingTaxesLis=invoiceentity.getBillingTaxes();
		 billingChargesLis=invoiceentity.getBillingOtherCharges();
		 payTermsLis=con.getPaymentTermsList();
		 
		 
		 /************************************discount Flag*******************************/
			
			if(invoiceentity.getCompanyId()!=null){
				for(int i=0;i<salesProd.size();i++){
					if((salesProd.get(i).getDiscountAmt()>0)&&(salesProd.get(i).getProdPercDiscount()>0)){
								
						both=both+1;
					}
					else if(salesProd.get(i).getDiscountAmt()>0)
					{
						discAmt=discAmt+1;
					}
					else if(salesProd.get(i).getProdPercDiscount()>0)
					{
						discPer=discPer+1;
					}
					else
					{
						nothing = nothing +1;
					}
				}
			System.out.println("both"+both+" discAmt"+discAmt +" disper"+discPer+" nothing"+nothing);	
			}	
		 
		 
		 
		 if(invoiceentity.getArrayBillingDocument().size()>1)
			{
				for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
					BillingDocument bill =new BillingDocument();
					if(invoiceentity.getCompanyId()!=null){
						bill=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
					}else{
						bill=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
					}
					payTerms.addAll(bill.getArrPayTerms());
					
			}
		 
			}else
			{
				payTerms.addAll(invoiceentity.getArrPayTerms());
			}
		 
		 
		 contractTaxesLis=con.getProductTaxes();
		 contractChargesLis= con.getProductCharges();
		 
		 //************************rohan changes here *******************
		 
		 if(invoiceentity.getArrayBillingDocument().size()!=0){
//			 BillingDocument billDocumentEntity =null;
			 for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++)
			 {
				 
//				 if(invoiceentity.getCompanyId()!=null)
//					{
//						billDocumentEntity=ofy().load().type(BillingDocument.class).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("companyId",billEntity.getCompanyId()).filter("contractCount", billEntity.getContractCount()).filter("typeOfOrder", billEntity.getTypeOfOrder().trim()).first().now();
//					}
//					else
//					{	
//						billDocumentEntity=ofy().load().type(BillingDocument.class).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("contractCount", billEntity.getContractCount()).filter("invoiceCount", billEntity.getInvoiceCount()).filter("typeOfOrder", billEntity.getTypeOfOrder().trim()).first().now();	
//					}
				
				 
				 InvoiceCharges taxes = new InvoiceCharges();
				 taxes.setChargesList(invoiceentity.getBillingTaxes());
				 taxes.setPayPercent(invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
				 conbillingTaxesLis.add(taxes);
				 
				 InvoiceCharges charges = new InvoiceCharges();
				 charges.setPayPercent(invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
				 charges.setOtherChargesList(invoiceentity.getBillingOtherCharges());
				 conbillingChargesLis.add(charges);
			 }
			 
		 }
		 //**********************changes ends here ***********************
		 
		 articletype = new ArrayList<ArticleType>();
			if(cust.getArticleTypeDetails().size()!=0){
				articletype.addAll(cust.getArticleTypeDetails());
			}
			if(comp.getArticleTypeDetails().size()!=0){
				articletype.addAll(comp.getArticleTypeDetails());
			}
		 
			
			//  rohan bhagde added this code for printing establishment year 
			 
			for(int i=0;i<this.articletype.size();i++){
				  if(articletype.get(i).getArticlePrint().equalsIgnoreCase("NO") && articletype.get(i).getDocumentName().equalsIgnoreCase("ServiceInvoice") && articletype.get(i).getArticleTypeName().equalsIgnoreCase("ESTABLISHMENT YEAR")){  
					
					  establishmentYr = articletype.get(i).getArticleTypeValue();
					  break;
				  }
			}
			//   ends here 
			
			arrPayTerms=invoiceentity.getArrPayTerms();
		 
		 
		 if(cust!=null){
			 logger.log(Level.SEVERE,"Customer Loaded");
		 }
		 else{
			 logger.log(Level.SEVERE,"Customer Not Loaded");
		 }
		 
		 if(con!=null){
			 logger.log(Level.SEVERE,"con Loaded");
		 }
		 else{
			 logger.log(Level.SEVERE,"con Not Loaded");
		 }
		 
		 
		 
		logger.log(Level.SEVERE,"All initilizations over");
		
		
		
		logger.log(Level.SEVERE,"set invoice 1.....");
		 try {
			invComment=getInvComment();
			logger.log(Level.SEVERE,"set invoice 2.....");
		} catch (Exception e) {
			logger.log(Level.SEVERE,"set invoice 3.....");
			e.printStackTrace();
		}
		 logger.log(Level.SEVERE,"set invoice 4.....");
		 
		 
		 for(int i=0;i<con.getItems().size();i++){
				
				if(invoiceentity.getCompanyId()!=null){
					sup=ofy().load().type(SuperProduct.class).filter("companyId",invoiceentity.getCompanyId()).
					filter("count", con.getItems().get(i).getPrduct().getCount()).first().now();
				
					
					SuperProduct superprod= new SuperProduct();
					
					superprod.setCount(sup.getCount());
					
					System.out.println("Product name "+sup.getProductName());
					System.out.println("Product description "+sup.getComment()+" "+sup.getCommentdesc());
					superprod.setProductName(sup.getProductName());
					
					superprod.setComment(sup.getComment()+" "+sup.getCommentdesc()+" "+sup.getCommentdesc1()+" "+sup.getCommentdesc2());
					
					stringlis.add(superprod);
				
				}
				}
		 
		 	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			}

	

	public  void createPdf(String preprint)
	{
		try {
			
			if(upcflag==false && preprint.equals("plane")){
				createLogo(document,comp);
				createCompanyAddress();
			}else{
				
				if(preprint.equals("yes")){
					System.out.println("inside prit yes");
					createBlankforUPC();
				}
				if(preprint.equals("no")){
				    if(comp.getUploadHeader()!=null){
				    	createCompanyNameAsHeader(document,comp);
					}
					
					if(comp.getUploadFooter()!=null){
						createCompanyNameAsFooter(document,comp);
					}
					createBlankforUPC();
				}
			}
			createCustInfo1();
			
			if((both >0) || ((discAmt >0)&&(discPer >0)))
			{
				createProductDetailWithDiscount();
			}
			else if(nothing >0)
			{
				createProductDetailWithOutDisc();
			}
			else
			{
				createProductTable();
			}
			if(isMultipleBillingOfSameContract()==true)
			{
				termsConditionsInfoForMultipleBilling();
			}
			else
			{
				termsConditionsInfo123();
			}
			addFooter();
	  } catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	

	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	
private void createBlankforUPC() {
	String titlepdf="";
	boolean flag = false;
	
	for (int i = 0; i < billingTaxesLis.size(); i++) {
		if(billingTaxesLis.get(i).getTaxChargePercent() > 0){
			flag= true;
			break;
		}
	}
	System.out.println("inside rohan "+flag);
	if(!flag){
		titlepdf="Invoice";
	}
	else{
		
		if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
			titlepdf="Proforma Invoice";
		else
			titlepdf="Tax Invoice";
	}
		Phrase titlephrase=new Phrase(titlepdf,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		/*
		 * Commented by Ashwini
		 */
		
//		
//		try {
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(parent);
//		} catch (DocumentException e) {
//			e.printStackTrace();
//		}
		
	/*
	 * Date:30/07/2018
	 * Developer:Ashwini	
	 */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , invoiceentity.getCompanyId())){
			 Paragraph blank1 =new Paragraph();
			    blank1.add(Chunk.NEWLINE);
			    
			
			    
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(parent);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}else{
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		}
		/*
		 * End by Ashwini
		 */
	}

	private void termsConditionsInfo123() {

		  Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
		  Phrase blankval=new Phrase(" ",font8);
		  Phrase payphrase=new Phrase("Payment Terms :",font10bold);
			payphrase.add(Chunk.NEWLINE);
			payphrase.add(Chunk.NEWLINE);
			PdfPTable table = new PdfPTable(3);
			table.setWidthPercentage(100);
//			table.setSpacingAfter(50f);
			
			
			PdfPCell tablecell1 = new PdfPCell();
			 if(this.payTermsLis.size()>5){
				 tablecell1= new PdfPCell(table);
//				 tablecell1.setBorder(0);
			        }else{
			        	tablecell1= new PdfPCell(table);
			        	tablecell1.setBorder(0);
			        }
			
			
			
			
			Phrase paytermdays = new Phrase("Days",font1);
			Phrase paytermpercent = new Phrase("Percent",font1);
	      Phrase paytermcomment = new Phrase("Comment",font1);
	    
	      PdfPCell headingpayterms = new PdfPCell(payphrase);
	      headingpayterms.setBorder(0);
	      headingpayterms.setColspan(3);
		    PdfPCell celldays = new PdfPCell(paytermdays);
		    celldays.setBorder(0);
		    celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPCell cellpercent = new PdfPCell(paytermpercent);
			cellpercent.setBorder(0);
			cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		    PdfPCell cellcomment = new PdfPCell(paytermcomment);
		    cellcomment.setBorder(0);
		    cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table.addCell(headingpayterms);
	      table.addCell(celldays);
	      table.addCell(cellpercent);
	      table.addCell(cellcomment);
		  
	      
	      if( this.payTermsLis.size()<= BreakPoint){
				int size =  BreakPoint - this.payTermsLis.size();
				      blankLines = size*(80/5);
				  	System.out.println("blankLines size ="+blankLines);
				System.out.println("blankLines size ="+blankLines);
					}
					else{
						blankLines = 0;
					}
					table.setSpacingAfter(blankLines);	
	      
	      System.out.println("this.F.size()"+this.payTermsLis.size());
	      
	      PdfPCell table1cell = new PdfPCell();
	      
	      if(paytermsflag==true){
	      
	      for(int i=0;i<this.payTermsLis.size();i++)
	      {
	      	System.out.println(this.payTermsLis.size()+"  in for1");
//	     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//	     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//	     	 pdfdayscell.setBorder(0);
	      	
	      	
	      	
	    	boolean validateEqualPayTerm=findEqualPayTerm(payTermsLis.get(i).getPayTermDays());
			
	    		
	    		System.out.println(" pay percent in contract == "+payTermsLis.get(i).getPayTermPercent());
	    		
		    	
	    		if(validateEqualPayTerm==true){
		      		
		      		Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8bold);
			      	PdfPCell pdfdayscell = new PdfPCell(chunk);
			      	pdfdayscell.setBorder(0);
			     	  
			     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
			     	 	
			     	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8bold);
			     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
			     	 pdfpercentcell.setBorder(0);
			     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			     	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8bold);
			     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
			     	 pdfcommentcell.setBorder(0);
			     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			     	
			     	table.addCell(pdfdayscell);
			     	table.addCell(pdfpercentcell);
			     	table.addCell(pdfcommentcell);
	    		
	    	
	      		
	      	}else{
	      		
	      		System.out.println("ELSE-----");
	      		
	      		Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
		      	PdfPCell pdfdayscell = new PdfPCell(chunk);
		      	pdfdayscell.setBorder(0);
		     	  
		     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	 	
		     	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8);
		     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
		     	 pdfpercentcell.setBorder(0);
		     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
		     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
		     	 pdfcommentcell.setBorder(0);
		     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	
		     	table.addCell(pdfdayscell);
		     	table.addCell(pdfpercentcell);
		     	table.addCell(pdfcommentcell);
	      		
	      	}
	      	
	     	
	     	count=i;
	    	 ///
	    	 if(count==4|| count==BreakPoint){
	 				
	 				flag1=BreakPoint;
	 				break;
	 	}
	     	
	     	
	     	}
		  
	    ///2nd table for pay terms start
	      System.out.println(this.payTermsLis.size()+"  out");
	      
//	      if(this.payTermsLis.size()>4){
	      	
	     
	      
	      
			PdfPTable table1 = new PdfPTable(3);
			table1.setWidthPercentage(100);
			
			
			
			 if(this.payTermsLis.size()>5){
				 table1cell= new PdfPCell(table1);
//				 table1cell.setBorder(0);
			        }else{
			        	table1cell= new PdfPCell(blankval);
			        	table1cell.setBorder(0);
			        }
			
			Phrase paytermdays1 = new Phrase("Days",font1);
			Phrase paytermpercent1 = new Phrase("Percent",font1);
	      Phrase paytermcomment1 = new Phrase("Comment",font1);
	    
	      PdfPCell celldays1;
	      PdfPCell cellpercent1;
	      PdfPCell cellcomment1;
	      
	      System.out.println(this.payTermsLis.size()+" ...........b4 if");
	      if(this.payTermsLis.size()>5){
	      	System.out.println(this.payTermsLis.size()+" ...........af if");
	       celldays1= new PdfPCell(paytermdays1);
		    celldays1.setBorder(0);
		    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
	      }else{
	      	celldays1= new PdfPCell(blankval);
	  	    celldays1.setBorder(0);
	      	
	      }
	      
	      if(this.payTermsLis.size()>5){
	      
			 cellpercent1 = new PdfPCell(paytermpercent1);
			cellpercent1.setBorder(0);
			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
	      }else{
	      	cellpercent1 = new PdfPCell(blankval);
	  		cellpercent1.setBorder(0);
	      	
	      }
	      
	      if(this.payTermsLis.size()>5){
		    cellcomment1= new PdfPCell(paytermcomment1);
		    cellcomment1.setBorder(0);
		    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
	      }else{
	      	cellcomment1= new PdfPCell(blankval);
	  	    cellcomment1.setBorder(0);
	      }
		
		
	      table1.addCell(celldays1);
	      table1.addCell(cellpercent1);
	      table1.addCell(cellcomment1);
	     
	   
	      
	      
	      
	      for(int i=5;i<this.payTermsLis.size();i++)
	      {
	      	System.out.println(this.payTermsLis.size()+"  in for");
	      	
	      	Phrase chunk = null;
//	     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//	     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//	     	 pdfdayscell.setBorder(0);
	    	if(payTermsLis.get(i).getPayTermDays()!=null){
	     	  chunk = new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
	    	}else{
	    		chunk = new Phrase(" ",font8);
	    	}
	     	 	
	     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
	     	 	pdfdayscell.setBorder(0);
	     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 if(payTermsLis.get(i).getPayTermPercent()!=0){ 
	     	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8);
	     	 }else{
	     		chunk = new Phrase(" ",font8);
	     	 }
	     	 
	     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
	     	 pdfpercentcell.setBorder(0);
	     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	if(payTermsLis.get(i).getPayTermComment()!=null){
	     	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
	     	}else{
	     		chunk = new Phrase(" ",font8);
	     	}
	     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
	     	 pdfcommentcell.setBorder(0);
	     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	
	     	table1.addCell(pdfdayscell);
	     	table1.addCell(pdfpercentcell);
	     	table1.addCell(pdfcommentcell);
	     	
	    	 if(i==9|| count==BreakPoint){
	 				
	 				break;
	    	 }
	     	
	     	}
	      
	      
	      
	      ///2nd table for pay terms end
	      
//	      PdfPCell table1cell = new PdfPCell();
	      table1cell.addElement(table1);
	      
	      
	
	      }else{
		      
		      for(int i=0;i<this.payTerms.size();i++)
		      {
		      	System.out.println(this.payTermsLis.size()+"  in for1");
//		     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//		     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//		     	 pdfdayscell.setBorder(0);
		    	
		     	 	Phrase chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
		     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
		     	 	pdfdayscell.setBorder(0);
		     	  
		     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	 	
		     	 chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
		     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
		     	 pdfpercentcell.setBorder(0);
		     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	 chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
		     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
		     	 pdfcommentcell.setBorder(0);
		     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     
		     	table.addCell(pdfdayscell);
		     	table.addCell(pdfpercentcell);
		     	table.addCell(pdfcommentcell);
		     	
		     	
		     	
		     	count=i;
		    	 ///
		    	 if(count==4|| count==BreakPoint){
		 				
		 				flag1=BreakPoint;
		 				break;
		 	}
		     	
		     	
		     	}
			  
		    ///2nd table for pay terms start
		      System.out.println(this.payTerms.size()+"  out");
		      
//		      if(this.payTermsLis.size()>4){
		      	
		     
		      
		      
				PdfPTable table1 = new PdfPTable(3);
				table1.setWidthPercentage(100);
				
				
				
				 if(this.payTerms.size()>5){
					 table1cell= new PdfPCell(table1);
//					 table1cell.setBorder(0);
				        }else{
				        	table1cell= new PdfPCell(blankval);
				        	table1cell.setBorder(0);
				        }
				
				Phrase paytermdays1 = new Phrase("Days",font1);
				Phrase paytermpercent1 = new Phrase("Percent",font1);
		      Phrase paytermcomment1 = new Phrase("Comment",font1);
		    
		      PdfPCell celldays1;
		      PdfPCell cellpercent1;
		      PdfPCell cellcomment1;
		      
		      System.out.println(this.payTerms.size()+" ...........b4 if");
		      if(this.payTermsLis.size()>5){
		      	System.out.println(this.payTermsLis.size()+" ...........af if");
		       celldays1= new PdfPCell(paytermdays1);
			    celldays1.setBorder(0);
			    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
		      }else{
		      	celldays1= new PdfPCell(blankval);
		  	    celldays1.setBorder(0);
		      	
		      }
		      
		      if(this.payTerms.size()>5){
		      
				 cellpercent1 = new PdfPCell(paytermpercent1);
				cellpercent1.setBorder(0);
				cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
		      }else{
		      	cellpercent1 = new PdfPCell(blankval);
		  		cellpercent1.setBorder(0);
		      	
		      }
		      
		      if(this.payTerms.size()>5){
			    cellcomment1= new PdfPCell(paytermcomment1);
			    cellcomment1.setBorder(0);
			    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
		      }else{
		      	cellcomment1= new PdfPCell(blankval);
		  	    cellcomment1.setBorder(0);
		      }
			
			
		      table1.addCell(celldays1);
		      table1.addCell(cellpercent1);
		      table1.addCell(cellcomment1);
		     
		   
		      
		      
		      
		      for(int i=5;i<this.payTerms.size();i++)
		      {
		      	System.out.println(this.payTerms.size()+"  in for");
		      	
		      	Phrase chunk = null;
//		     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//		     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//		     	 pdfdayscell.setBorder(0);
		    	if(payTerms.get(i).getPayTermDays()!=null){
		     	  chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
		    	}else{
		    		chunk = new Phrase(" ",font8);
		    	}
		     	 	
		     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
		     	 	pdfdayscell.setBorder(0);
		     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	 if(payTerms.get(i).getPayTermPercent()!=0){ 
		     	 chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
		     	 }else{
		     		chunk = new Phrase(" ",font8);
		     	 }
		     	 
		     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
		     	 pdfpercentcell.setBorder(0);
		     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	if(payTerms.get(i).getPayTermComment()!=null){
		     	 chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
		     	}else{
		     		chunk = new Phrase(" ",font8);
		     	}
		     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
		     	 pdfcommentcell.setBorder(0);
		     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		     	
		     	table1.addCell(pdfdayscell);
		     	table1.addCell(pdfpercentcell);
		     	table1.addCell(pdfcommentcell);
		     	
		    	 if(i==9|| count==BreakPoint){
		 				
		 				break;
		    	 }
		     	}
		      
		      
		      
		      ///2nd table for pay terms end
		      
//		      PdfPCell table1cell = new PdfPCell();
		      table1cell.addElement(table1);
	      } 
	      
	      PdfPTable termstable1 = new PdfPTable(2);
	      termstable1.setWidthPercentage(100);
	      termstable1.addCell(headingpayterms);
	      termstable1.addCell(tablecell1);
	      termstable1.addCell(table1cell);
	      
	  
	      
	      
		   PdfPTable chargetaxtable = new PdfPTable(2);
		   chargetaxtable.setWidthPercentage(100);
		   chargetaxtableToNewPage.setWidthPercentage(100);
		   
		   Phrase totalAmtPhrase = new Phrase("Order Amount   " + "", font1);
			PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
			totalAmtCell.setBorder(0);
			double totalAmt = 0;
				totalAmt = con.getTotalAmount();

			Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
			PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
			realtotalAmtCell.setBorder(0);
			realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			chargetaxtable.addCell(totalAmtCell);
			chargetaxtable.addCell(realtotalAmtCell);
		   
//			int breakpoint=5;
//			int cnt=this.billingTaxesLis.size()+this.billingChargesLis.size();
		   
			List<String> myList1 = new ArrayList<>();
			List<String> myList2 = new ArrayList<>();
			List<String> myList3 = new ArrayList<>();
			List<String> myList4 = new ArrayList<>();
			
	//*****************rohan changes here on 5/6/15 for part payment *****************
				 
//			 for(int i=0;i<this.contractTaxesLis.size();i++)
//		      {
//		    	  
//		   	  if(contractTaxesLis.get(i).getChargePercent()!=0){
//		   	  
//		   		  if(contractTaxesLis.get(i).getChargeName().equals("VAT")||contractTaxesLis.get(i).getChargeName().equals("CST")){
//					
//					System.out.println("1st loop"+contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent());
//					
//					String str = contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent();
//					double taxAmt1 = contractTaxesLis.get(i).getChargePercent()
//							* contractTaxesLis.get(i).getAssessableAmount() / 100;
//					
//					 myList1.add(str);
//					 myList2.add(df.format(taxAmt1));
//					 
//					 System.out.println("Size of mylist1 is () === "+myList1.size());
//				}
//				
//				if(contractTaxesLis.get(i).getChargeName().equals("Service Tax")){
//					
//					System.out.println("2nd loop == "+contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent());
//					
//					String str = contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent();
//					double taxAmt1 = contractTaxesLis.get(i).getChargePercent()
//							* contractTaxesLis.get(i).getAssessableAmount() / 100;
//					String ss=" ";
//					 myList3.add(str);
//					 myList4.add(df.format(taxAmt1));
//					 myList4.add(ss);
//					 System.out.println("Size of mylist2 is () === "+myList2.size());
//				}
//		   	  }
//		    }
				 
//				 PdfPCell pdfservicecell=null;
//					
//					PdfPTable other1table=new PdfPTable(1);
//					other1table.setWidthPercentage(100);
//					for(int j=0;j<myList1.size();j++){
//						chunk = new Phrase(myList1.get(j),font8);
//						 pdfservicecell = new PdfPCell(chunk);
//						pdfservicecell.setBorder(0);
//						pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//						other1table.addCell(pdfservicecell);
//						
//					}
					
					
					
//					PdfPCell pdfservicecell1=null;
//					
//					PdfPTable other2table=new PdfPTable(1);
//					other2table.setWidthPercentage(100);
//					for(int j=0;j<myList2.size();j++){
//						chunk = new Phrase(myList2.get(j),font8);
//						 pdfservicecell1 = new PdfPCell(chunk);
//						pdfservicecell1.setBorder(0);
//						pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						other2table.addCell(pdfservicecell1);
//						
//					}
					
//					PdfPCell chargescell=null;
//					for(int j=0;j<myList3.size();j++){
//						chunk = new Phrase(myList3.get(j),font8);
//						chargescell = new PdfPCell(chunk);
//						chargescell.setBorder(0);
//						chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
//						other1table.addCell(chargescell);
//						
//					}
					
//					PdfPCell chargescell1=null;
//					for(int j=0;j<myList4.size();j++){
//						chunk = new Phrase(myList4.get(j),font8);
//						chargescell1 = new PdfPCell(chunk);
//						chargescell1.setBorder(0);
//						chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						other2table.addCell(chargescell1);
//						
//					}
					
					
					
//					PdfPCell othercell = new PdfPCell();
//					othercell.addElement(other1table);
//					othercell.setBorder(0);
//					chargetaxtable.addCell(othercell);
//					
//					PdfPCell othercell1 = new PdfPCell();
//					othercell1.addElement(other2table);
//					othercell1.setBorder(0);
//					chargetaxtable.addCell(othercell1);
				 
				 
				 
				 
//			      PdfPCell chargecell = null;
//			      PdfPCell chargeamtcell=null;
//			      PdfPCell otherchargecell=null;
//			      
//				 for(int i=0;i<this.invoiceentity.getBillingOtherCharges().size();i++)
//			      {
//			   	   Phrase chunk = null;
//			   	Phrase chunk1 = new Phrase("",font8);
//			   	   double chargeAmt=0;
//			   	   PdfPCell pdfchargeamtcell = null;
//			   	   PdfPCell pdfchargecell = null;
//			   	   if(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent()!=0){
//			   		   System.out.println(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+".......charge name"+"@"+contractChargesLis.get(i).getChargePercent());
//			   		   chunk = new Phrase(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+" @ "+contractChargesLis.get(i).getChargePercent(),font1);
//			   		   pdfchargecell=new PdfPCell(chunk);
//			   		pdfchargecell.setBorder(0);
//			   		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			   		   chargeAmt=this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent()*this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAssesVal()/100;
//			   		   chunk=new Phrase(df.format(chargeAmt),font8);
//			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			  	      	   pdfchargeamtcell.setBorder(0);
//			   	   }else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   	   }
//				      
//			   	   ///start
//			   	   
//			   	PdfPCell pdfchargeamtcell1 = null;
//				   PdfPCell pdfchargecell1 = null;
//			   	   
//			   	   if(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAbsVal()!=0){
//			   		   chunk = new Phrase(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+"",font1);
//			   		pdfchargecell1=new PdfPCell(chunk);
//			   		chargeAmt=this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAbsVal();
//			   		   chunk=new Phrase(chargeAmt+"",font8);
//			   		pdfchargeamtcell1 = new PdfPCell(chunk);
//			   		pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			   		pdfchargeamtcell1.setBorder(0);
//			   	    }else{
//			   		pdfchargecell1=new PdfPCell(chunk1);
//			   		pdfchargeamtcell1 = new PdfPCell(chunk1);
//			   	    }
//			 	      pdfchargecell.setBorder(0);
//			 	      pdfchargeamtcell.setBorder(0);
//				      pdfchargecell1.setBorder(0);
//				      pdfchargeamtcell1.setBorder(0);
//				      
//				       chargetaxtable.addCell(pdfchargecell1);
//				       chargetaxtable.addCell(pdfchargeamtcell1);
//			 	       chargetaxtable.addCell(pdfchargecell);
//			 	       chargetaxtable.addCell(pdfchargeamtcell);
//			 	       
//			      }  
				 
				
				
				//*******************************changes  here*********************
//				Phrase netname = new Phrase("Grand Total",font8);
//				PdfPCell netnamecell = new PdfPCell(netname);
//				netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
////				namephasevaluecell.addElement(namePhasevalue);
//				netnamecell.setBorder(0);
//				chargetaxtable.addCell(netnamecell);
//			 	
//			 
//			 	double netpayable=0;
//			 	netpayable = con.getNetpayable();
//				String netpayableamt=netpayable+"";
//				System.out.println("total============"+netpayableamt);
//				Phrase netpay = new Phrase(netpayableamt,font8);
//				PdfPCell netpaycell = new PdfPCell(netpay);
//				netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
////				namephasevaluecell.addElement(namePhasevalue);
//				netpaycell.setBorder(0);
//				chargetaxtable.addCell(netpaycell);
			 
			 
				Phrase blankSpace = new Phrase(" ",font8);
				PdfPCell blankSpacecell = new PdfPCell(blankSpace);
				blankSpacecell.setBorder(0);
				chargetaxtable.addCell(blankSpacecell);
				chargetaxtable.addCell(blankSpacecell);
				
				
//				if(payTerms.get(0).getPayTermPercent()!=100){
//				Phrase annexure = new Phrase("Reffer annexure for more details",font8);
//				PdfPCell annexurecell = new PdfPCell(annexure);
//				annexurecell.setHorizontalAlignment(Element.ALIGN_LEFT);
////				namephasevaluecell.addElement(namePhasevalue);
//				annexurecell.setBorder(0);
//				chargetaxtable.addCell(annexurecell);
//				chargetaxtable.addCell(blankSpacecell);
//				}
				
				double flatDics=0;
				
				System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAA "+invoiceentity.getSalesOrderProducts().size());
				
				 for(int i=0;i<invoiceentity.getSalesOrderProducts().size();i++){
						if(invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount()!=0)
						{
							flatDics =flatDics+ invoiceentity.getSalesOrderProducts().get(i).getFlatDiscount();
						}
					
				 }
				
				 double pay=0;
				 for(int i=0;i<payTerms.size();i++){
						
					 pay=payTerms.get(i).getPayTermPercent();
				 }
				 
					//*******************************88888 
				 if(pay == 100)
				 {
					 
					 if(flatDics!=0)
					 {
						 
						  	Phrase totAMt = new Phrase("Total" + "", font1);
							PdfPCell totAMtCell = new PdfPCell(totAMt);
							totAMtCell.setBorder(0);
							double totalAmt123 = 0;
							totalAmt123 = con.getTotalAmount();

							Phrase realtotalAmt123 = new Phrase(df.format(totalAmt123), font1);
							PdfPCell realtotalAmtCell123 = new PdfPCell(realtotalAmt123);
							realtotalAmtCell123.setBorder(0);
							realtotalAmtCell123.setHorizontalAlignment(Element.ALIGN_RIGHT);
							chargetaxtable.addCell(totAMtCell);
							chargetaxtable.addCell(realtotalAmtCell123);
						 
						 Phrase  amtdisc123=new Phrase("Flat Disc",font8);
						 PdfPCell amtdisc123Cell = new PdfPCell(amtdisc123);
						 amtdisc123Cell.setBorder(0);
						 chargetaxtable.addCell(amtdisc123Cell);
						 
						 Phrase  amtdiscValue123=new Phrase(flatDics+"",font8);
						 PdfPCell amtdiscValue123Cell = new PdfPCell(amtdiscValue123);
						 amtdiscValue123Cell.setBorder(0);
						 amtdiscValue123Cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						 chargetaxtable.addCell(amtdiscValue123Cell);
						 
						 
					 double amtDisc = con.getTotalAmount() - flatDics;
					 Phrase  amtdisc=new Phrase("Disc Amt",font8);
					 PdfPCell amtdiscCell = new PdfPCell(amtdisc);
					 amtdiscCell.setBorder(0);
					 chargetaxtable.addCell(amtdiscCell);
					 
					 Phrase  amtdiscValue=new Phrase(amtDisc+"",font8);
					 PdfPCell amtdiscValueCell = new PdfPCell(amtdiscValue);
					 amtdiscValueCell.setBorder(0);
					 amtdiscValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					 chargetaxtable.addCell(amtdiscValueCell);
					 
					 }
					 
					 
					 
					 System.out.println("in side rohan bhagde ................");
					 
					 for(int k=0;k<this.conbillingTaxesLis.size();k++)
				     {
				   	  
				   	  for(int i=0;i<this.conbillingTaxesLis.get(k).getChargesList().size();i++)
				   	  {
				   		  
				   		  ///   code for charges other than service tax and 14.5
				   		  if((conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST"))&& (conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0.0)){
								
								System.out.println("1st loop"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
								
								String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
								double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
										* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
								
//								 myList11.add(str);
//								 myList22.add(df.format(taxAmt1));
								 
								Phrase strp = new Phrase(str,font8);
								PdfPCell strcell =new PdfPCell(strp);
								strcell.setBorder(0);
								strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
								chargetaxtable.addCell(strcell);
								
								
								Phrase taxAmt1p = new Phrase(df.format(taxAmt1),font8);
								PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
								taxAmt1cell.setBorder(0);
								taxAmt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								chargetaxtable.addCell(taxAmt1cell);
								
							}
							
			
							
				   		  else if((conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0.0)&&(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==15)){	
								
								 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
								
								System.out.println("2nd loop 5678== "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
								
								String str = "Service Tax "+ " @ "+" 14%";
								double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
								String blidnk=" ";
								
								Phrase strp = new Phrase(str,font8);
								PdfPCell strcell =new PdfPCell(strp);
								strcell.setBorder(0);
								strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
								chargetaxtable.addCell(strcell);
								
								
								Phrase taxAmt1p = new Phrase(df.format(taxAmt1),font8);
								PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
								taxAmt1cell.setBorder(0);
								taxAmt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								chargetaxtable.addCell(taxAmt1cell);
								
								 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
								 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
								 
								 	Phrase str123p = new Phrase(str123,font8);
									PdfPCell str123cell =new PdfPCell(str123p);
									str123cell.setBorder(0);
									str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
									chargetaxtable.addCell(str123cell);
									
									
									Phrase taxAmtp = new Phrase(df.format(taxAmt),font8);
									PdfPCell taxAmtcell =new PdfPCell(taxAmtp);
									taxAmtcell.setBorder(0);
									taxAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
									chargetaxtable.addCell(taxAmtcell);
								 
									 String kkc = "Krishi Kalyan Cess"+ " @ "+" 0.5%";
									 double kkctaxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
									 
									 	Phrase kkcPhrase = new Phrase(kkc,font8);
										PdfPCell kkcCell =new PdfPCell(kkcPhrase);
										kkcCell.setBorder(0);
										kkcCell.setHorizontalAlignment(Element.ALIGN_LEFT);
										chargetaxtable.addCell(kkcCell);
										
										
										Phrase taxAmtkkc = new Phrase(df.format(kkctaxAmt),font8);
										PdfPCell taxAmtkkcCell =new PdfPCell(taxAmtkkc);
										taxAmtkkcCell.setBorder(0);
										taxAmtkkcCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
										chargetaxtable.addCell(taxAmtkkcCell);	
									
							}
				   		  
							else if((conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0.0)&& (conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==14.5)){
							
							 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
							
							System.out.println("2nd loop 890== "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
							
							String str = "Service Tax "+ " @ "+" 14%";
							double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							String blidnk=" ";
							
							Phrase strp = new Phrase(str,font8);
							PdfPCell strcell =new PdfPCell(strp);
							strcell.setBorder(0);
							strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
							chargetaxtable.addCell(strcell);
							
							
							Phrase taxAmt1p = new Phrase(df.format(taxAmt1),font8);
							PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
							taxAmt1cell.setBorder(0);
							taxAmt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							chargetaxtable.addCell(taxAmt1cell);
							 
							 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
							 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							 
							 	Phrase str123p = new Phrase(str123,font8);
								PdfPCell str123cell =new PdfPCell(str123p);
								str123cell.setBorder(0);
								str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
								chargetaxtable.addCell(str123cell);
								
								
								Phrase taxAmtp = new Phrase(df.format(taxAmt),font8);
								PdfPCell taxAmtcell =new PdfPCell(taxAmtp);
								taxAmtcell.setBorder(0);
								taxAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								chargetaxtable.addCell(taxAmtcell);
							 
							 
						}
				   		
				   		else if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0.0){
				   			
							 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
							
							System.out.println("2nd loop 1234 == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
							
							String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
							double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
									* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							
							Phrase strp = new Phrase(str,font8);
							PdfPCell strcell =new PdfPCell(strp);
							strcell.setBorder(0);
							strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
							chargetaxtable.addCell(strcell);
							
							
							Phrase taxAmt1p = new Phrase(df.format(taxAmt1),font8);
							PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
							taxAmt1cell.setBorder(0);
							taxAmt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							chargetaxtable.addCell(taxAmt1cell);
							
						} 
				   	  }
				   	  
				   	  }
				 }
				 
				 if(pay != 100){
			 
			 
			
	//***************************changes ends here ******************************************			
		
		//**********************rohan changes here on 26/5/15 for part payments********************************		
		System.out.println("billEntity.getArrPayTerms().size==========="+invoiceentity.getArrayBillingDocument().size());
		
		System.out.println("contract count========="+invoiceentity.getContractCount());
		System.out.println("order type======"+invoiceentity.getTypeOfOrder().trim());
		
		double disccAmtValue=0;
		
		if(invoiceentity.getArrayBillingDocument().size()>1)
		{
			 for(int k=0;k<this.payTerms.size();k++)
		      {
				 	List<String> myList11 = new ArrayList<>();
					List<String> myList22 = new ArrayList<>();
					List<String> myList33 = new ArrayList<>();
					List<String> myList44 = new ArrayList<>();
				 
				 System.out.println("11111111111111111111111111"+payTerms.get(k).getPayTermPercent());
				 System.out.println("22222222222222222222222222"+conbillingTaxesLis.get(k).getPayPercent());
				 
				 if(payTerms.get(k).getPayTermPercent().equals(billingTaxesLis.get(k).getPaypercent())){
					 
				 for(int z=0;z<invoiceentity.getSalesOrderProducts().size();z++){	 
				 
				 if(payTerms.get(k).getPayTermPercent().equals(conbillingTaxesLis.get(k).getPayPercent())){
					 
				 String payterm=this.conbillingTaxesLis.get(k).getPayPercent()+"";
				 chunk= new Phrase("Pay Terms if"+payterm+"%",font8bold);
				 PdfPCell paytermCell = new PdfPCell(chunk);
				 paytermCell.setBorder(0);
				 chargetaxtable.addCell(paytermCell);
				 
				 double total =0;
					
				total = ((con.getTotalAmount()*payTerms.get(k).getPayTermPercent())/100);
				
				Phrase totalphase = new Phrase(df.format(total),font8bold);
				PdfPCell totalcell = new PdfPCell(totalphase);
				totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				totalcell.setBorder(0);
				chargetaxtable.addCell(totalcell);
				
					System.out.println("******************************************************");
				 }
					else{
				
						double BillPay=100*(payTerms.get(k).getPayTermPercent()/100)*(invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent()/100);
					
					 Phrase billPayPhrase= new Phrase("Pay Term else"+BillPay+"%",font8bold);
					 PdfPCell BillPayCell = new PdfPCell(billPayPhrase);
					 BillPayCell.setBorder(0);
					 chargetaxtable.addCell(BillPayCell);
					
					
					
					System.out.println("inside condition ***********************");
					double paymentAsperPercent= (invoiceentity.getSalesOrderProducts().get(z).getBaseBillingAmount()*invoiceentity.getSalesOrderProducts().get(z).getPaymentPercent())/100;
				
					Phrase paymentAsperPercentPhrase = new Phrase(df.format(paymentAsperPercent),font8bold);
					PdfPCell paymentAsperPercentCell = new PdfPCell(paymentAsperPercentPhrase);
					paymentAsperPercentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
					paymentAsperPercentCell.setBorder(0);
					chargetaxtable.addCell(paymentAsperPercentCell);
				}
				 
				 //*****************rohan added this code for disc Amt ************8
				 if(invoiceentity.getSalesOrderProducts().get(z).getDiscountAmt()!=0)
				 {
				 Phrase discAmt= new Phrase("Flat Disc",font8);
				 PdfPCell discAmtCell = new PdfPCell(discAmt);
				 discAmtCell.setBorder(0);
				 chargetaxtable.addCell(discAmtCell);
				 
				 Phrase discAmtValue= new Phrase(invoiceentity.getSalesOrderProducts().get(z).getDiscountAmt()+"",font8);
				 PdfPCell discAmtValueCell = new PdfPCell(discAmtValue);
				 discAmtValueCell.setBorder(0);
				 discAmtValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				 chargetaxtable.addCell(discAmtValueCell);
				 
				 
				 Phrase AmtDisc= new Phrase("Disc Amt",font8);
				 PdfPCell AmtDiscCell = new PdfPCell(AmtDisc);
				 AmtDiscCell.setBorder(0);
				 chargetaxtable.addCell(AmtDiscCell);
				 
				 
				 Phrase amtDiscValue= new Phrase(invoiceentity.getSalesOrderProducts().get(z).getDiscountAmt()+"",font8);
				 PdfPCell amtDiscValueCell = new PdfPCell(amtDiscValue);
				 amtDiscValueCell.setBorder(0);
				 amtDiscValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				 chargetaxtable.addCell(amtDiscValueCell);
				 				 
				}
			}
				 
				  System.out.println("billingTaxesLis size"+billingTaxesLis.size());
				  System.out.println("iiiiiiii   value    "+k); 
		    	  System.out.println("billingTaxesLis.get(k).getChargesList().get(i) list "+conbillingTaxesLis.get(k).getChargesList().size());
		    	 for(int i=0;i<this.conbillingTaxesLis.get(k).getChargesList().size();i++){
		    	  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0){
		    	  
		    		  System.out.println("billingTaxesLis.get(i).getTaxChargeName()"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
		    		  
		    		  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST")){
							
							System.out.println("1st loop"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
							
							String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
							double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
									* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							
							 myList11.add(str);
							 myList22.add(df.format(taxAmt1));
							 
							 System.out.println("Size of mylist1 is () === "+myList1.size());
						}
						
						if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")){
							
							 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
							
							System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
							
							String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
							double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
									* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							String blidnk=" ";
							 myList33.add(str);
							 myList44.add(df.format(taxAmt1));
							 myList44.add(blidnk);
							 
							 System.out.println("Size of mylist2 is () === "+myList2.size());
						}
		    	  }
		    	 }
			 
			 
			 PdfPCell pdfservicecell123=null;
				
				PdfPTable other1table123=new PdfPTable(1);
				other1table123.setWidthPercentage(100);
				for(int j=0;j<myList11.size();j++){
					chunk = new Phrase(myList11.get(j),font8);
					pdfservicecell123 = new PdfPCell(chunk);
					pdfservicecell123.setBorder(0);
					pdfservicecell123.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table123.addCell(pdfservicecell123);
					
				}
				
				PdfPCell pdfservicecell1123=null;
				
				PdfPTable other2table123=new PdfPTable(1);
				other2table123.setWidthPercentage(100);
				for(int j=0;j<myList22.size();j++){
					chunk = new Phrase(myList22.get(j),font8);
					pdfservicecell1123 = new PdfPCell(chunk);
					pdfservicecell1123.setBorder(0);
					pdfservicecell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table123.addCell(pdfservicecell1123);
					
				}
				
				PdfPCell chargescell123=null;
				for(int j=0;j<myList33.size();j++){
					chunk = new Phrase(myList33.get(j),font8);
					chargescell123 = new PdfPCell(chunk);
					chargescell123.setBorder(0);
					chargescell123.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table123.addCell(chargescell123);
					
				}
				
				PdfPCell chargescell1123=null;
				for(int j=0;j<myList44.size();j++){
					chunk = new Phrase(myList44.get(j),font8);
					chargescell1123 = new PdfPCell(chunk);
					chargescell1123.setBorder(0);
					chargescell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table123.addCell(chargescell1123);
					
				}
				
				
				
				PdfPCell othercell123 = new PdfPCell();
				othercell123.addElement(other1table123);
				othercell123.setBorder(0);
				chargetaxtable.addCell(othercell123);
				
				PdfPCell othercell1123 = new PdfPCell();
				othercell1123.addElement(other2table123);
				othercell1123.setBorder(0);
				chargetaxtable.addCell(othercell1123);
//		    	 }
			 //*******ROHAN CHANGES HERE FOR PART PAYMENT ON 5/6/15******************
//		      }
				
			 //**********************CHANGES ENDS HERE********************************
			 
			 
//			 PdfPCell chargecell22 = null;
//		      PdfPCell chargeamtcell22=null;
//		      PdfPCell otherchargecell22=null;
//			 if(payTerms.get(k).getPayTermPercent().equals(billingChargesLis.get(0).getPayPercent())){
			 for(int j=0;j<this.billingChargesLis.size();j++)
		      {
				 
		   	   Phrase chunk = null;
		   	   Phrase chunk1 = new Phrase("",font8);
		   	   double chargeAmt=0;
		   	   PdfPCell pdfchargeamtcell = null;
		   	   PdfPCell pdfchargecell = null;
		   	
		   	 System.out.println("88888888888888888888888888"+payTerms.get(k).getPayTermPercent());
			 System.out.println("99999999999999999999999999"+conbillingChargesLis.get(j).getPayPercent());
			 
			 if(payTerms.get(k).getPayTermPercent().equals(conbillingChargesLis.get(j).getPayPercent())){
		   	   
		   	   
		   	 for(int i=0;i<this.conbillingChargesLis.get(j).getOtherChargesList().size();i++){
		    	  
		   	   if(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargePercent()!=0){
		   		   chunk = new Phrase(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeName()+" @ "+conbillingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargePercent(),font1);
		   		   pdfchargecell=new PdfPCell(chunk);
		   		pdfchargecell.setBorder(0);
		   		chargetaxtable.addCell(pdfchargecell);
		 	     
//		   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
		   		 chargeAmt=conbillingChargesLis.get(j).getOtherChargesList().get(i).getPayableAmt();
		   		   chunk=new Phrase(df.format(chargeAmt),font8);
		  	      	   pdfchargeamtcell = new PdfPCell(chunk);
		  	      	 pdfchargeamtcell.setBorder(0);
		  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  	      chargetaxtable.addCell(pdfchargeamtcell);
		  	      	
		  	      	  
		   	   }else{
		   		pdfchargecell=new PdfPCell(chunk1);
		   		pdfchargecell.setBorder(0);
		   		pdfchargeamtcell = new PdfPCell(chunk1);
		   		pdfchargeamtcell.setBorder(0);
		   		chargetaxtable.addCell(pdfchargecell);
		   		chargetaxtable.addCell(pdfchargeamtcell);
		   	   }
		   	   if(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
		   		   chunk = new Phrase(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
		   		   pdfchargecell=new PdfPCell(chunk);
		   		pdfchargecell.setBorder(0);
		   		chargetaxtable.addCell(pdfchargecell);
		   		   chargeAmt=conbillingChargesLis.get(j).getOtherChargesList().get(i).getPayableAmt();
		   		   chunk=new Phrase(chargeAmt+"",font8);
		  	      	   pdfchargeamtcell = new PdfPCell(chunk);
		  	      	  pdfchargeamtcell.setBorder(0);
		  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  	      	chargetaxtable.addCell(pdfchargeamtcell);
		   	   }else{
		   		pdfchargecell=new PdfPCell(chunk1);
		   		pdfchargecell.setBorder(0);
		   		pdfchargeamtcell = new PdfPCell(chunk1);
		   		pdfchargeamtcell.setBorder(0);
		   		chargetaxtable.addCell(pdfchargecell);
		   		chargetaxtable.addCell(pdfchargeamtcell);
		   	   }
		   	   
			      }
			 }
		      }
				 }
				 
				 Phrase chunk1 = new Phrase(" ",font8);
				 PdfPCell pdfchargecell=new PdfPCell(chunk1);
			   		pdfchargecell.setBorder(0);
			   		PdfPCell pdfchargeamtcell = new PdfPCell(chunk1);
			   		pdfchargeamtcell.setBorder(0);
			   		chargetaxtable.addCell(pdfchargecell);
			   		chargetaxtable.addCell(pdfchargeamtcell);
		      }
		}
		else
		{
			System.out.println("indise else condition");
			
			
			double total =0;
			System.out.println("rohan1234 "+invoiceentity.getBillingTaxes().size());	 
			for(int x=0;x<invoiceentity.getSalesOrderProducts().size();x++){	
				if(invoiceentity.getSalesOrderProducts().get(x).getFlatDiscount()!=0)
				{
					disccAmtValue = disccAmtValue + invoiceentity.getSalesOrderProducts().get(x).getFlatDiscount();	
				}
		}	
		
			for(int z=0;z<invoiceentity.getBillingTaxes().size();z++){
			System.out.println("rohan for the 1 st time "+invoiceentity.getBillingTaxes().get(z).getTaxChargeAssesVal());
			total = total+(invoiceentity.getBillingTaxes().get(z).getTaxChargeAssesVal());
			}
//		if(invoiceentity.getSalesOrderProducts().get(x).getPaymentPercent()==100){	
			
		System.out.println("Pay Percent "+invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
		
		double payterms= 100*(invoiceentity.getArrPayTerms().get(0).getPayTermPercent()/100);
		Phrase namePhase = new Phrase("Pay Terms "+payterms+"%",font8);
		PdfPCell namephasecell = new PdfPCell(namePhase);
		namephasecell.addElement(namePhase);
		namephasecell.setBorder(0);
		chargetaxtable.addCell(namephasecell);
		
		
		
		
		Phrase totalphase = new Phrase(df.format(total),font8);
		PdfPCell totalcell = new PdfPCell(totalphase);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalcell.setBorder(0);
		chargetaxtable.addCell(totalcell);
		
		//*********************rohan make changes here for payment %
			System.out.println("************************************************789789789******");
//				}
//			else
//			{
//			System.out.println("inside condition ***********************");
//			double paymentAsperPercent= (invoiceentity.getSalesOrderProducts().get(x).getBaseBillingAmount()*invoiceentity.getSalesOrderProducts().get(x).getPaymentPercent())/100;
//			double payment= 100*(invoiceentity.getArrPayTerms().get(0).getPayTermPercent()/100)*(invoiceentity.getSalesOrderProducts().get(x).getPaymentPercent()/100);
//			 Phrase billPayPhrase= new Phrase("Pay Term "+payment+"%",font8);
//			 PdfPCell BillPayCell = new PdfPCell(billPayPhrase);
//			 BillPayCell.setBorder(0);
//			 chargetaxtable.addCell(BillPayCell);
//			
//			Phrase paymentAsperPercentPhrase = new Phrase(df.format(paymentAsperPercent),font8);
//			PdfPCell paymentAsperPercentCell = new PdfPCell(paymentAsperPercentPhrase);
//			paymentAsperPercentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			paymentAsperPercentCell.setBorder(0);
//			chargetaxtable.addCell(paymentAsperPercentCell);
//		}
		 
		//***************changes ends here *********************** 
		
			
			 //*****************rohan added this code for disc Amt ************8
			 
			if(disccAmtValue!=0)
			{
			 Phrase discAmt= new Phrase("Flat Disc",font8);
			 PdfPCell discAmtCell = new PdfPCell(discAmt);
			 discAmtCell.setBorder(0);
			 chargetaxtable.addCell(discAmtCell);
			 
			 Phrase discAmtValue= new Phrase(disccAmtValue+"",font8);
			 PdfPCell discAmtValueCell = new PdfPCell(discAmtValue);
			 discAmtValueCell.setBorder(0);
			 discAmtValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 chargetaxtable.addCell(discAmtValueCell);
			} 				 
			//*****************rohan ***********************************
			
		
		//*********************rohan changes here ****************************
		List<String> myList11 = new ArrayList<>();
		List<String> myList22 = new ArrayList<>();
		List<String> myList33 = new ArrayList<>();
		List<String> myList44 = new ArrayList<>();
		//****************************rohan changes on 16/6/15*****************
		 for(int k=0;k<this.conbillingTaxesLis.size();k++)
	     {
	   	  
			 
			 System.out.println("billingTaxesLis size"+conbillingTaxesLis.size());
			  System.out.println("iiiiiiii   value    "+k); 
	   	  
	   	  for(int i=0;i<this.conbillingTaxesLis.get(k).getChargesList().size();i++){
	   	  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0){
	   	  
	   		  System.out.println("billingTaxesLis.get(i).getChargesList() size"+conbillingTaxesLis.get(k).getChargesList().size());
	   		  
	   		  System.out.println("billingTaxesLis.get(i).getTaxChargeName()"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
	   		  
	   		  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST")){
						
						System.out.println("1st loop"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
						double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
								* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						
						 myList11.add(str);
						 myList22.add(df.format(taxAmt1));
						 
						 System.out.println("Size of mylist1 is () === "+myList1.size());
					}
					
					
					
	   		  else if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==14.5){
						
						 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
						
						System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = "Service Tax "+ " @ "+" 14%";
						double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						String blidnk=" ";
						 
						
						myList33.add(str);
						 myList44.add(df.format(taxAmt1));
						 myList44.add(blidnk);
						 
						 
						 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
						 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						 
						 myList33.add(str123);
						 myList44.add(df.format(taxAmt));
						 myList44.add(blidnk);
						 System.out.println("Size of mylist2 is () === "+myList2.size());
					}
					else if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==15){	
						

						
						 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
						
						System.out.println("2nd loop ohan in 15 == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = "Service Tax "+ " @ "+" 14%";
						double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						String blidnk=" ";
						
						 myList33.add(str);
						 myList44.add(df.format(taxAmt1));
						 myList44.add(blidnk);
						
						 
						 
						 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
						 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						 
						 	 myList33.add(str123);
							 myList44.add(df.format(taxAmt));
							 myList44.add(blidnk);
						 
						 
							 String kkc = "Krishi Kalyan Cess"+ " @ "+" 0.5%";
							 double kkctaxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							 
							 myList33.add(kkc);
							 myList44.add(df.format(kkctaxAmt));
							 myList44.add(blidnk);
					}
					else if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent() != 14.5 && conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent() != 15){
						
						 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
						
						System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
						double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
								* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						 String blidnk=" ";
						 myList33.add(str);
						 myList44.add(df.format(taxAmt1));
						 myList44.add(blidnk);
						 
						 System.out.println("Size of mylist2 is () === "+myList2.size());
					}
					
	   	  }
	    	}
	     }
		 
		 PdfPCell pdfservicecell123=null;
			
			PdfPTable other1table123=new PdfPTable(1);
			other1table123.setWidthPercentage(100);
			for(int j=0;j<myList11.size();j++){
				chunk = new Phrase(myList11.get(j),font8);
				pdfservicecell123 = new PdfPCell(chunk);
				pdfservicecell123.setBorder(0);
				pdfservicecell123.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table123.addCell(pdfservicecell123);
				
			}
			
			PdfPCell pdfservicecell1123=null;
			
			PdfPTable other2table123=new PdfPTable(1);
			other2table123.setWidthPercentage(100);
			for(int j=0;j<myList22.size();j++){
				chunk = new Phrase(myList22.get(j),font8);
				pdfservicecell1123 = new PdfPCell(chunk);
				pdfservicecell1123.setBorder(0);
				pdfservicecell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table123.addCell(pdfservicecell1123);
				
			}
			
			PdfPCell chargescell123=null;
			for(int j=0;j<myList33.size();j++){
				chunk = new Phrase(myList33.get(j),font8);
				chargescell123 = new PdfPCell(chunk);
				chargescell123.setBorder(0);
				chargescell123.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table123.addCell(chargescell123);
				
			}
			
			PdfPCell chargescell1123=null;
			for(int j=0;j<myList44.size();j++){
				chunk = new Phrase(myList44.get(j),font8);
				chargescell1123 = new PdfPCell(chunk);
				chargescell1123.setBorder(0);
				chargescell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table123.addCell(chargescell1123);
				
			}
			
			
			
			PdfPCell othercell123 = new PdfPCell();
			othercell123.addElement(other1table123);
			othercell123.setBorder(0);
			chargetaxtable.addCell(othercell123);
			
			PdfPCell othercell1123 = new PdfPCell();
			othercell1123.addElement(other2table123);
			othercell1123.setBorder(0);
			chargetaxtable.addCell(othercell1123);
		 
		 //*******ROHAN CHANGES HERE FOR PART PAYMENT ON 5/6/15******************
			
			
			
		 //**********************CHANGES ENDS HERE********************************
		 
		 
//		 PdfPCell chargecell22 = null;
//	     PdfPCell chargeamtcell22=null;
//	     PdfPCell otherchargecell22=null;
			System.out.println(billingChargesLis.size());
	     System.out.println("rohan bhagde====================== "+billingChargesLis.size());
		 for(int k=0;k<this.conbillingChargesLis.size();k++)
	     {
	  	   Phrase chunk = null;
	  	   Phrase chunk1 = new Phrase("",font8);
	  	   double chargeAmt=0;
	  	   PdfPCell pdfchargeamtcell = null;
	  	   PdfPCell pdfchargecell = null;
	  	   
	  	   
	  	   for(int i=0;i<this.conbillingChargesLis.get(k).getOtherChargesList().size();i++){
	  	   
	  	   if(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargePercent()!=0){
	  		   chunk = new Phrase(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeName()+" @ "+conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargePercent(),font1);
	  		   pdfchargecell=new PdfPCell(chunk);
	  		pdfchargecell.setBorder(0);
	  		chargetaxtable.addCell(pdfchargecell);
		     
//	  		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
	  		 chargeAmt=conbillingChargesLis.get(k).getOtherChargesList().get(i).getPayableAmt();
	  		   chunk=new Phrase(df.format(chargeAmt),font8);
	 	      	   pdfchargeamtcell = new PdfPCell(chunk);
	 	      	 pdfchargeamtcell.setBorder(0);
	 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 	      	chargetaxtable.addCell(pdfchargeamtcell);
	 	      	
	 	      	  
	  	   }else{
	  		pdfchargecell=new PdfPCell(chunk1);
	  		pdfchargecell.setBorder(0);
	  		pdfchargeamtcell = new PdfPCell(chunk1);
	  		pdfchargeamtcell.setBorder(0);
	  		chargetaxtable.addCell(pdfchargecell);
	  		chargetaxtable.addCell(pdfchargeamtcell);
	  	   }
	  	   if(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
	  		   chunk = new Phrase(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
	  		   pdfchargecell=new PdfPCell(chunk);
	  		pdfchargecell.setBorder(0);
	  		chargetaxtable.addCell(pdfchargecell);
	  		   chargeAmt=conbillingChargesLis.get(k).getOtherChargesList().get(i).getPayableAmt();
	  		   chunk=new Phrase(chargeAmt+"",font8);
	 	      	   pdfchargeamtcell = new PdfPCell(chunk);
	 	      	  pdfchargeamtcell.setBorder(0);
	 	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 	      	chargetaxtable.addCell(pdfchargeamtcell);
	  	   }else{
	  		pdfchargecell=new PdfPCell(chunk1);
	  		pdfchargecell.setBorder(0);
	  		pdfchargeamtcell = new PdfPCell(chunk1);
	  		pdfchargeamtcell.setBorder(0);
	  		chargetaxtable.addCell(pdfchargecell);
	  		chargetaxtable.addCell(pdfchargeamtcell);
	  	   }
		      
		      }
	     }
		
		}			
	}
	    
	    
	    PdfPTable billamtable = new PdfPTable(2);
	    billamtable.setWidthPercentage(100);
	    billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
		       
		      for(int i=0;i<this.billingChargesLis.size();i++){
		       
		      	if(i==billingChargesLis.size()-1){
		      		chunk = new Phrase("Net Payable",font1);
			        PdfPCell pdfinvamtcell = new PdfPCell(chunk);
			        pdfinvamtcell.setBorder(0);
			      	Double invAmt=invoiceentity.getInvoiceAmount();
			      	
			      	int netpayble=(int) invAmt.doubleValue();
			      	
			      	chunk=new Phrase(netpayble+"",font8);
			        PdfPCell pdfnetpayamt = new PdfPCell(chunk);
			        pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			        pdfnetpayamt.setBorder(0);
			        
//			        chargetaxtable.addCell(pdfinvamtcell);
//			        chargetaxtable.addCell(pdfnetpayamt);
			        
			        billamtable.addCell(pdfinvamtcell);
			        billamtable.addCell(pdfnetpayamt);
			        
			        netPayableTable.addCell(pdfinvamtcell);
			        netPayableTable.addCell(pdfnetpayamt);
		      	}
	   	}
	    
	    if(this.billingChargesLis.size()==0){
	  	  
	  	  Phrase chunkinvAmttitle = new Phrase("Net Payable",font1);
	  	  PdfPCell invAmtTitleCell=new PdfPCell(chunkinvAmttitle);
	  	  invAmtTitleCell.setBorder(0);
	  	  Double invoiceAmount=invoiceentity.getInvoiceAmount();
	  	  
	  	  int netpayble=(int) invoiceAmount.doubleValue();
	  	  
		      Phrase chunkinvamt=new Phrase(netpayble+"",font8);
		      PdfPCell pdfinvamt = new PdfPCell(chunkinvamt);
		      pdfinvamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		      pdfinvamt.setBorder(0);
		        
//		      chargetaxtable.addCell(invAmtTitleCell);
//		      chargetaxtable.addCell(pdfinvamt);
		      
		      billamtable.addCell(invAmtTitleCell);
		        billamtable.addCell(pdfinvamt);
		        
		        
		        netPayableTable.addCell(invAmtTitleCell);
		        netPayableTable.addCell(pdfinvamt);
	    }
	    
	    PdfPCell cagrecell=new PdfPCell();
	    cagrecell.addElement(chargetaxtable);
	    cagrecell.setBorder(0);
	    
	    PdfPTable mainbilltable = new PdfPTable(1);
	    mainbilltable.setWidthPercentage(100);
	    mainbilltable.setHorizontalAlignment(Element.ALIGN_RIGHT);
	    mainbilltable.addCell(cagrecell);
//	    mainbilltable.addCell(billamtable);
	    
	    
	    
	    PdfPTable taxinfotable=new PdfPTable(1);
//	    taxinfotable.setWidthPercentage(100);
//	    taxinfotable.addCell(mainbilltable);
	    
	    PdfPTable parenttaxtable=new PdfPTable(2);
	    parenttaxtable.setWidthPercentage(100);
	    try {
			parenttaxtable.setWidths(new float[]{65,35});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	    
		  PdfPCell amtWordsTblcell1 = new PdfPCell();
		  PdfPCell taxdatacell = new PdfPCell();
			
		  amtWordsTblcell1.addElement(termstable1);
		  taxdatacell.addElement(mainbilltable);
		  
		  parenttaxtable.addCell(amtWordsTblcell1);
		  parenttaxtable.addCell(taxdatacell);
		
			try {
				document.add(parenttaxtable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
			  String amtInWords="Rupees:  "+SalesInvoicePdf.convert(invoiceentity.getInvoiceAmount());
			   System.out.println("amt"+amtInWords);
			   Phrase amtwords=new Phrase(amtInWords+" Only.",font1);
			   PdfPCell amtWordsCell=new PdfPCell();
			   amtWordsCell.addElement(amtwords);
			   amtWordsCell.setBorder(0);
			   
			   
			   PdfPTable amountInWordsTable=new PdfPTable(1);
			   amountInWordsTable.setWidthPercentage(100);
//			   amountInWordsTable.setHorizontalAlignment(Element.ALIGN_TOP);
			   amountInWordsTable.addCell(amtWordsCell);
				
			  
			   PdfPTable termpayTable=new PdfPTable(1);
			   termpayTable.setWidthPercentage(100);
//			   termpayTable.addCell(headingpayterms);
			   termpayTable.addCell(termstable1);
//			   termpayTable.addCell(amountInWordsTable);
			
			   PdfPTable parenttaxtable1=new PdfPTable(2);
			      parenttaxtable1.setWidthPercentage(100);
			      try {
					parenttaxtable1.setWidths(new float[]{65,35});
				} catch (DocumentException e1) {
					e1.printStackTrace();
				}
			      
				  PdfPCell amtWordsTblcell = new PdfPCell();
				  PdfPCell taxdatacell1 = new PdfPCell();
					
				  amtWordsTblcell.addElement(amountInWordsTable);
				  taxdatacell1.addElement(billamtable);
				  parenttaxtable1.addCell(amtWordsTblcell);
				  parenttaxtable1.addCell(taxdatacell1);
				
					try {
						document.add(parenttaxtable1);
						
					} catch (DocumentException e) {
						e.printStackTrace();
					}
					
	}

	public  void createPdfForEmail(Invoice invoiceDetails,Company companyEntity,Customer custEntity,Contract contractEntity,BillingDocument billingEntity,List<CustomerBranchDetails> custbranchlist) 
	{
		this.invoiceentity=invoiceDetails;
		this.comp=companyEntity;
		this.cust=custEntity;
		this.con=contractEntity;
		this.billEntity=billingEntity;
		this.salesProd=invoiceentity.getSalesOrderProducts();
		this.contractTaxesLis=con.getProductTaxes();
		this.contractChargesLis=con.getProductCharges();
		this.billingTaxesLis=invoiceentity.getBillingTaxes();
		this.billingChargesLis=invoiceentity.getBillingOtherCharges();
		this.payTermsLis=contractEntity.getPaymentTermsList();
		billingDoc=invoiceentity.getArrayBillingDocument();
		this.custbranchlist=custbranchlist;
	
		
	if(invoiceentity.getCompanyId()==null)
		custbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).list();
	else
		custbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("companyId", invoiceentity.getCompanyId()).list();
	
	
	/****************************** vijay ************************/
	
	System.out.println("Branch name======"+invoiceentity.getCustomerBranch());
	if(invoiceentity.getCompanyId()==null)
		customerbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).list();
	else
		customerbranchlist=ofy().load().type(CustomerBranchDetails.class).filter("cinfo.count",invoiceentity.getPersonInfo().getCount()).filter("buisnessUnitName", invoiceentity.getCustomerBranch()).filter("companyId", invoiceentity.getCompanyId()).list();
	
	System.out.println("Banch updated====="+invoiceentity.getCustomerBranch());
	/****************************** vijay ************************/
	
	
	/*********************Display Payment Terms Flag*****************************/
	
	if(invoiceentity.getCompanyId()!=null){
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceInvoicePayTerm")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					paytermsflag=true;
				}
			}
		}
	}
	
	
	
/*********************if service Address and Billing Address is same display ones (Vcare)*****************************/
	
	if(invoiceentity.getCompanyId()!=null){
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceAndBillingAdressSamePrintOne")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					ServiceAndBillingAdressSamePrintOnes=true;
				}
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintMultipleCompanyNamesFromInvoiceGroup")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					multipleCompanyName=true;
				}
			}
		}
	}
	
	
	/************************************Letter Head Flag*******************************/
	
	if(invoiceentity.getCompanyId()!=null)
	{
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					upcflag=true;
				}
			}
		}
	 }
	
	
	 Date todaysDate = invoiceentity.getInvoiceDate();
	 logger.log(Level.SEVERE,"system date :::"+todaysDate);		 
	getFunnctionalYear(todaysDate);
	/**********************************Disclaimer Flag******************************************/
	
	if(invoiceentity.getCompanyId()!=null){
		processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", invoiceentity.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
		if(processConfig!=null){
			for(int k=0;k<processConfig.getProcessList().size();k++)
			{
				if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")&&processConfig.getProcessList().get(k).isStatus()==true)
				{
					disclaimerflag=true;
				}
			}
		}
	}
	
	 salesProd=invoiceentity.getSalesOrderProducts();
	 System.out.println("AAAAAAAAAA sales prod size"+salesProd.size());
	 billingTaxesLis=invoiceentity.getBillingTaxes();
	 billingChargesLis=invoiceentity.getBillingOtherCharges();
	 payTermsLis=con.getPaymentTermsList();
	 
	 
	 /************************************discount Flag*******************************/
		
		if(invoiceentity.getCompanyId()!=null){
			for(int i=0;i<salesProd.size();i++){
				if((salesProd.get(i).getDiscountAmt()>0)&&(salesProd.get(i).getProdPercDiscount()>0)){
							
					both=both+1;
				}
				else if(salesProd.get(i).getDiscountAmt()>0)
				{
					discAmt=discAmt+1;
				}
				else if(salesProd.get(i).getProdPercDiscount()>0)
				{
					discPer=discPer+1;
				}
				else
				{
					nothing = nothing +1;
				}
				
				
			}
			
		System.out.println("both"+both+" discAmt"+discAmt +" disper"+discPer+" nothing"+nothing);	
		}	
	 
	 
	 
	 if(invoiceentity.getArrayBillingDocument().size()>1)
		{
			for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
				BillingDocument bill =new BillingDocument();
				if(invoiceentity.getCompanyId()!=null){
					bill=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
				}else{
					bill=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
				}
				payTerms.addAll(bill.getArrPayTerms());
				
			}
	 
		}else
		{
			payTerms.addAll(invoiceentity.getArrPayTerms());
		}
	 
	 
	 contractTaxesLis=con.getProductTaxes();
	 contractChargesLis= con.getProductCharges();
	 
	 //************************rohan changes here *******************
	 
	 if(invoiceentity.getArrayBillingDocument().size()!=0){
		 for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++)
		 {
			 
			 InvoiceCharges taxes = new InvoiceCharges();
			 taxes.setChargesList(invoiceentity.getBillingTaxes());
			 taxes.setPayPercent(invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
			 conbillingTaxesLis.add(taxes);
			 
			 InvoiceCharges charges = new InvoiceCharges();
			 charges.setPayPercent(invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
			 charges.setOtherChargesList(invoiceentity.getBillingOtherCharges());
			 conbillingChargesLis.add(charges);
		 }
		 
	 }
	 //**********************changes ends here ***********************
	 
	 	articletype = new ArrayList<ArticleType>();
		if(cust.getArticleTypeDetails().size()!=0){
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if(comp.getArticleTypeDetails().size()!=0){
			articletype.addAll(comp.getArticleTypeDetails());
		}
	 arrPayTerms=invoiceentity.getArrPayTerms();
	 
	 
	 if(cust!=null){
		 logger.log(Level.SEVERE,"Customer Loaded");
	 }
	 else{
		 logger.log(Level.SEVERE,"Customer Not Loaded");
	 }
	 
	 if(con!=null){
		 logger.log(Level.SEVERE,"con Loaded");
	 }
	 else{
		 logger.log(Level.SEVERE,"con Not Loaded");
	 }
	 
	 
	 
	logger.log(Level.SEVERE,"All initilizations over");
	
	
	
	logger.log(Level.SEVERE,"set invoice 1.....");
	 try {
		invComment=getInvComment();
		logger.log(Level.SEVERE,"set invoice 2.....");
	} catch (Exception e) {
		logger.log(Level.SEVERE,"set invoice 3.....");
		e.printStackTrace();
	}
	 logger.log(Level.SEVERE,"set invoice 4.....");
	 
	 
	 for(int i=0;i<con.getItems().size();i++){
			
			if(invoiceentity.getCompanyId()!=null){
				sup=ofy().load().type(SuperProduct.class).filter("companyId",invoiceentity.getCompanyId()).
				filter("count", con.getItems().get(i).getPrduct().getCount()).first().now();
			
				
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				
				System.out.println("Product name "+sup.getProductName());
				System.out.println("Product description "+sup.getComment()+" "+sup.getCommentdesc());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
				
				stringlis.add(superprod);
			
			}
			}
	 
	 	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		
//		try {
//			createLogo(document,comp);
//			createCompanyAddress();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//	  try {
//			createCustInfo1();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	  
//	  if((both >0) || ((discAmt >0)&&(discPer >0)))
//		{
//			createProductDetailWithDiscount();
//		}
//		else
//		{
//			createProductTable();
//		}
//		if(isMultipleBillingOfSameContract()==true)
//		{
//			termsConditionsInfoForMultipleBilling();
//		}
//		else
//		{
//			termsConditionsInfo();
//		}
//	  
//		
//		addFooter();
		 
		 /**
		  * Rohan : Commented all the previous methods for email and added this new code 
		  * This pdf is same as printed on 
		  */
		 
				
				    if(comp.getUploadHeader()!=null && comp.getUploadHeader().getUrl()!=null && !comp.getUploadHeader().getUrl().equals("")){
				    	createBlankforUPC();
				    	createCompanyNameAsHeader(document,comp);
				    	 logger.log(Level.SEVERE,"inside if .....");
				    	try {
							createCustInfo1();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				    else 
				    {
				    	 logger.log(Level.SEVERE,"inside else .....");
				    	createLogo(document,comp);
						try {
							createCompanyAddress();
							createCustInfo1();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
				    }
					
					if(comp.getUploadFooter()!=null){
						createCompanyNameAsFooter(document,comp);
					}
				
				
		
			
			if((both >0) || ((discAmt >0)&&(discPer >0)))
			{
				createProductDetailWithDiscount();
			}
			else if(nothing >0)
			{
				createProductDetailWithOutDisc();
			}
			else
			{
				createProductTable();
			}
			if(isMultipleBillingOfSameContract()==true)
			{
				termsConditionsInfoForMultipleBilling();
			}
			else
			{
				termsConditionsInfo123();
			}
			addFooter();
	
	
	}

private void createLogo(Document doc, Company comp) {
		

//		********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
		}
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	
	
//	try
//	{
//		Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	
	}
		
	public  void createCompanyAddress() throws Exception
	{
		logger.log(Level.SEVERE,"Creating Company Info");
		 Phrase companyName= null;
		 
		 if(multipleCompanyName)
		 {
			 
			 System.out.println("in side conditions="+con.getGroup()+"="+multipleCompanyName);
			 if(con.getGroup()!=null && ! con.getGroup().equals(""))
			 {
				 companyName = new Phrase(con.getGroup(),font16bold);
			 }
			 else
			 {
				 companyName = new Phrase(comp.getBusinessUnitName(),font16bold);
			 }
			
		 }
		 else
		 {
			 companyName = new Phrase(comp.getBusinessUnitName(),font16bold);
		 }
		 
		 
	   Phrase p =new Phrase(companyName);
	   PdfPCell pCell = new PdfPCell(p);
	   pCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	   pCell.setBorder(0);
	  
	   
	   
	   /**
	    * rohan modified this code as i wanted to add estblishment year in pdf 
	    * Date :25/2/2017
	    * For Shraddha pest control
	    */
	   float[] mycolumnWidths = {1.5f,7f,1.5f};
	   
	   PdfPTable table = new PdfPTable(3);
	   table.setWidthPercentage(100f);
	   table.setWidths(mycolumnWidths);
	   
	   Phrase myblank = new Phrase("",font8);
	   PdfPCell myblankCell= new PdfPCell(myblank);
	   myblankCell.setBorder(0);
	   myblankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	   
	   table.addCell(myblankCell);
	   table.addCell(myblankCell);
	   table.addCell(myblankCell);
	   table.addCell(myblankCell);
	   table.addCell(myblankCell);
	   table.addCell(myblankCell);
	   
	   table.addCell(myblankCell);
	   table.addCell(pCell);
	   if(!establishmentYr.equalsIgnoreCase("")){
		   Phrase establishmentYear = new Phrase(establishmentYr,font10bold);
		   PdfPCell establishmentYearCell= new PdfPCell(establishmentYear);
		   establishmentYearCell.setBorder(0);
		   table.addCell(establishmentYearCell);
		   establishmentYearCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		   establishmentYearCell.setBorder(0);
	   }
	   else{
		   table.addCell(myblankCell);
	   }
	   
	   /**
	    * ends here 
	    */
	   PdfPCell companyNameCell=new PdfPCell();
	   companyNameCell.addElement(table);
	   companyNameCell.setBorder(0);
	   
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+"  "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		Phrase branchPhrase=new Phrase("Branch : "+con.getBranch());
		PdfPCell branchCell=new PdfPCell(branchPhrase);
		branchCell.setBorder(0);
		branchCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		Phrase contactinfo=null;
		if(comp.getLandline()!=0)
		{
			
			if(comp.getCellNumber2()!=0)
			{
				contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+","+comp.getCellNumber2()+"    Phone: "+comp.getLandline()+"    email: "+comp.getEmail().trim(),font10);
			}
			else
			{
				contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Phone: "+comp.getLandline()+"    email: "+comp.getEmail().trim(),font10);
			}
			
			
		}
		else
		{
			if(comp.getCellNumber2()!=0)
			{
				contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+","+comp.getCellNumber2()+"    email: "+comp.getEmail().trim(),font10);
			}
			else
			{
				contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    email: "+comp.getEmail().trim(),font10);
			}
			
			
		}
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(branchCell);
		companyDetails.addCell(contactcell);
		
		
		//   rohan added this code 
		String titlepdf="";
		boolean flag = false;
		
		for (int i = 0; i < billingTaxesLis.size(); i++) {
			if(billingTaxesLis.get(i).getTaxChargePercent() > 0){
				flag= true;
				break;
			}
		}
		System.out.println("inside rohan "+flag);
		if(!flag){
			titlepdf="Invoice";
		}
		else{
			
			if(AppConstants.CREATEPROFORMAINVOICE.equals(invoiceentity.getInvoiceType().trim())||invoiceentity.getInvoiceType().trim().equals(AppConstants.CREATEPROFORMAINVOICE))
				titlepdf="Proforma Invoice";
			else
				titlepdf="Tax Invoice";
		}
		
		Phrase titlephrase=new Phrase(titlepdf,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(comapnyCell);
		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		logger.log(Level.SEVERE,"End of Creating Company Info");
	}
	
	
	////do
	
	public void createCustInfo1() throws Exception
	{
		logger.log(Level.SEVERE,"Creating customer  Info");
//		String tosir="To, M/S";
		
		//************************changes made by rohan	for M/s **************** 
//		String tosir= null;
//		if(cust.isCompany()==true){
//			
//		 tosir="To, M/S";
//		}
//		else{
//			tosir="To, ";
//		}
//		//*******************changes ends here ********************sss*************
//		
//		Phrase clientname=null;
//		
//		if(cust.isCompany()==true){
//			clientname=new Phrase(tosir+"  "+cust.getCompanyName().trim(),font10bold);
//		}
//		else{
//			clientname=new Phrase(tosir+"  "+cust.getFullname().trim(),font10bold);
//		}
		
		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
			if(cust.isCompany()==true){
				
				
			 tosir="To, M/S";
			}
			else{
				tosir="To,";
			}
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
		Phrase clientname=null;
		String fullname= "";
		 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		   	{
		       	fullname = custName;
		   	}
		       else
		       {
		       	fullname = tosir+"   "+custName;
		       }
		 
		 clientname = new Phrase(fullname,font10bold);
		PdfPCell custnamecell=new PdfPCell();
		custnamecell.addElement(clientname);
		custnamecell.setBorder(0);
		
		logger.log(Level.SEVERE,"1111111111111111111111111111111111111111111111111");
	Phrase custaddress1=new Phrase("                   "+cust.getAdress().getAddrLine1().trim(),font8);
	
	Phrase custaddress2=null;
	if(!cust.getAdress().getAddrLine2().equals("")||cust.getAdress().getAddrLine2()!=null){
		custaddress2=new Phrase("                   "+cust.getAdress().getAddrLine2().trim(),font8);
	}else{
		custaddress2=new Phrase("",font10);
	}
	Phrase landmark=null;
	
	if(!cust.getAdress().getLandmark().equals("")){
		landmark=new Phrase("                   "+cust.getAdress().getLandmark(),font8);
	}else{
		landmark=new Phrase("",font10);
	}
	Phrase locality=null;
	if(!cust.getAdress().getLocality().equals("")){
		locality=new Phrase("                   "+cust.getAdress().getLocality(),font8);
	}else{
		locality=new Phrase("",font8);
	}
		Phrase city=new Phrase("                   "+cust.getAdress().getCity().trim()+" - "+cust.getAdress().getPin(),font8);
	
		
		Phrase contactcust = null;
	if(cust.getLandline()!=null)
	{
		 contactcust=new Phrase("mobile: "+cust.getCellNumber1()+"    Phone: "+cust.getLandline()+"    Email:  "+cust.getEmail().trim()+" ",font8);
	}
	else
	{
		 contactcust=new Phrase("mobile: "+cust.getCellNumber1()+"    Email:  "+cust.getEmail().trim()+" ",font8);
	}
		
	logger.log(Level.SEVERE,"222222222222222222222222222222222222222222222222222");
	PdfPCell custaddr1cell=new PdfPCell();
	custaddr1cell.addElement(custaddress1);
	custaddr1cell.setBorder(0);
	
	PdfPCell custaddr2cell=null;
	if(custaddress2!=null){
		custaddr2cell=new PdfPCell();
		custaddr2cell.addElement(custaddress2);
		custaddr2cell.setBorder(0);
	}
	
	PdfPCell custlocalitycell=new PdfPCell();
	custlocalitycell.addElement(locality);
	custlocalitycell.setBorder(0);
	
	PdfPCell lanmarkcell=new PdfPCell();
	lanmarkcell.addElement(landmark);
	lanmarkcell.setBorder(0);
	
	PdfPCell citycell=new PdfPCell();
	citycell.addElement(city);
	citycell.setBorder(0);
	
	PdfPCell custcontactcell=new PdfPCell();
	custcontactcell.addElement(contactcust);
	//custcontactcell.addElement(Chunk.NEWLINE);
	custcontactcell.setBorder(0);
	logger.log(Level.SEVERE,"3333333333333333333333333333333333333333333333");
	PdfPTable custtable=new PdfPTable(1);
	custtable.setWidthPercentage(100);
	custtable.addCell(custnamecell);
	custtable.addCell(custaddr1cell);
	if(!cust.getAdress().getAddrLine2().equals("")){
		custtable.addCell(custaddr2cell);
	}
	if(!cust.getAdress().getLandmark().equals("")){
		custtable.addCell(lanmarkcell);
	}
	if(!cust.getAdress().getLocality().equals("")){
		custtable.addCell(custlocalitycell);
	}
	custtable.addCell(citycell);
	custtable.addCell(custcontactcell);
	
	//*****************rohan added this code for customer corresponence *****************
	
	if(cust.getCustCorresponence()!=null&&!cust.getCustCorresponence().equals(""))
	{
		Phrase custCorrespondence = new Phrase("Kind Attn: "+cust.getCustCorresponence(),font8);
		PdfPCell custCorrespondenceCell = new PdfPCell(custCorrespondence);
		custCorrespondenceCell.setBorder(0);
		custtable.addCell(custCorrespondenceCell);
	}
	
	
	//**********************change ends here *************************************8
	
	logger.log(Level.SEVERE,"4444444444444444444444444444444444444444444444444444444");
	
	Phrase billnotitle=new Phrase("Bill No",font8);
	
	PdfPTable fiscal =new PdfPTable(3);
	fiscal.setWidthPercentage(100f);
	fiscal.setWidths(columnWidths123);
	
	Phrase realbillno=new Phrase(invoiceentity.getCount()+" / "+functionalYear,font8); 
	logger.log(Level.SEVERE,"AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	
	
	Phrase datetitle=new Phrase("Bill Date",font8);
	Phrase realdate=new Phrase(fmt.format(invoiceentity.getInvoiceDate())+"",font8);
	
	
	Phrase billnotitle1=new Phrase("Contract Id",font8);
	Phrase realbillno1=new Phrase(invoiceentity.getContractCount()+"",font8); 
	Phrase datetitle1=new Phrase(" ",font8);
	
	//************rohan remove this invoice creation date for back datedrecords (ultra pest change) 
	Phrase realdate1=new Phrase(" ",font8);
	
	
	
	//Phrase ponotitle=new Phrase("PO No.         :",font8);
	//Phrase realpono=new Phrase("  ",font8);
	//Phrase podatetitle=new Phrase("PO Date       :",font8);
	//Phrase realpodate=new Phrase(" ",font8);o
	logger.log(Level.SEVERE,"BBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
	Phrase stdate=new Phrase("Start Date",font8);
	Phrase sdate=new Phrase(fmt.format(con.getStartDate())+"",font8);
	logger.log(Level.SEVERE,"CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
	Phrase enddate=new Phrase("End Date",font8);
	Phrase edate=new Phrase(fmt.format(con.getEndDate())+"",font8);
	
	//Phrase duration=new Phrase("Duration       :",font8);
	//Phrase dration=new Phrase(con.get,font8);
	//
	//Phrase noser=new Phrase("No of Services       :",font8);
	//Phrase ser=new Phrase(con.get,font8);
	//
	logger.log(Level.SEVERE,"DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
	Phrase refno=new Phrase("Ref No",font8);
	
	
	Phrase ref=null;
	String refNO="";
//	System.out.println("ref no====="+refNO);
	
	if(!con.getRefNo().equals("")){
		refNO=con.getRefNo();
	ref=new Phrase(refNO+"",font8);
	}
	else{
		 ref=new Phrase("",font8);
	}
	logger.log(Level.SEVERE,"EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
	
	/**
	 * InvoiceGroup Add 29/6/15
	 */
	
	Phrase invoicegroupphrase1=null;
	Phrase invoicegroupphrase3=null;
	if(!invoiceentity.getInvoiceGroup().equals("")){
		invoicegroupphrase1=new Phrase("Invoice Group",font8);
		invoicegroupphrase3=new Phrase(invoiceentity.getInvoiceGroup(),font8);
	}else{
		invoicegroupphrase1=new Phrase("",font8);
		invoicegroupphrase3=new Phrase("",font8);
	}
	
	
	System.out.println("invoicce group == "+invoiceentity.getInvoiceGroup());
	PdfPCell invoicegroupcell1=new PdfPCell();
	invoicegroupcell1.setBorder(0);
	invoicegroupcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	invoicegroupcell1.addElement(invoicegroupphrase1);
	PdfPCell invoicegroupcell3=new PdfPCell();
	invoicegroupcell3.setBorder(0);
	invoicegroupcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	invoicegroupcell3.addElement(invoicegroupphrase3);
	
	
	
	Phrase columnPhrase=new Phrase(":",font8);
	PdfPCell columnCell = new PdfPCell();
	columnCell.setBorder(0);
	columnCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	columnCell.addElement(columnPhrase);
	logger.log(Level.SEVERE,"55555555555555555555555555555555555555555555555555555555555555");
	
	PdfPCell stdatecell = new PdfPCell();
	stdatecell.addElement(stdate);
	stdatecell.setBorder(0);
	
	PdfPCell sdatecell = new PdfPCell();
	sdatecell.addElement(sdate);
	sdatecell.setBorder(0);
	
	PdfPCell enddatecell = new PdfPCell();
	enddatecell.addElement(enddate);
	enddatecell.setBorder(0);
	
	PdfPCell edatecell = new PdfPCell();
	edatecell.addElement(edate);
	edatecell.setBorder(0);
	
	PdfPCell refnocell = new PdfPCell();
	refnocell.addElement(refno);
	refnocell.setBorder(0);
	
	PdfPCell refcell = new PdfPCell();
	refcell.addElement(ref);
	refcell.setBorder(0);
	
	PdfPCell billnotitlecell=new PdfPCell();
	billnotitlecell.addElement(billnotitle);
	billnotitlecell.setBorder(0);
	
	PdfPCell billnocell=new PdfPCell();
	billnocell.addElement(realbillno);
	billnocell.setBorder(0);
	fiscal.addCell(billnotitlecell);
	fiscal.addCell(columnCell);
	fiscal.addCell(billnocell);
	
	
	PdfPCell datetitlecell=new PdfPCell();
	datetitlecell.addElement(datetitle);
	datetitlecell.setBorder(0);
	
	PdfPCell datecell=new PdfPCell();
	datecell.addElement(realdate);
	datecell.setBorder(0);
	
	PdfPCell ponotitlecell=new PdfPCell();
	ponotitlecell.addElement(billnotitle1);
	ponotitlecell.setBorder(0);
	
	PdfPCell ponocell=new PdfPCell();
	ponocell.addElement(realbillno1);
	ponocell.setBorder(0);
	
	PdfPCell podatetitlecell=new PdfPCell();
	podatetitlecell.addElement(datetitle1);
	podatetitlecell.setBorder(0);
	
	PdfPCell podatecell=new PdfPCell();
	podatecell.addElement(realdate1);
	podatecell.setBorder(0);
	
	/**
	 *   rohan added this code for billing period information 
	 *   Date : 12-4-2017
	 */
	
	PdfPCell billingFromDtName = null;
	PdfPCell billingFromDtValue= null;
	PdfPCell billingToDtName = null;
	PdfPCell billingToDtValue = null;
	
	if(invoiceentity.getBillingPeroidFromDate()!= null && invoiceentity.getBillingPeroidToDate()!= null){
		
		Phrase name = new Phrase("Bill From",font8);
		billingFromDtName = new PdfPCell();
		billingFromDtName.addElement(name);
		billingFromDtName.setBorder(0);
		billingFromDtName.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase billPeriod = new Phrase(fmt.format(invoiceentity.getBillingPeroidFromDate()),font8);
		billingFromDtValue = new PdfPCell();
		billingFromDtValue.addElement(billPeriod);
		billingFromDtValue.setBorder(0);
		billingFromDtValue.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		
		Phrase name1 = new Phrase("Bill To",font8);
		billingToDtName = new PdfPCell();
		billingToDtName.addElement(name1);
		billingToDtName.setBorder(0);
		billingToDtName.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase billPeriod1 = new Phrase(fmt.format(invoiceentity.getBillingPeroidToDate()),font8);
		billingToDtValue = new PdfPCell();
		billingToDtValue.addElement(billPeriod1);
		billingToDtValue.setBorder(0);
		billingToDtValue.setHorizontalAlignment(Element.ALIGN_LEFT);
	}
	
	/**
	 * ends here
	 */
	
	
	logger.log(Level.SEVERE,"6666666666666666666666666666666666666666666666666666666");
	PdfPTable invoiceinfotable=new PdfPTable(6);
	invoiceinfotable.setWidths(columnWidths12);
	invoiceinfotable.setWidthPercentage(100);
	
	//   rohan adedd this for billing period information
	if(invoiceentity.getBillingPeroidFromDate()!= null && invoiceentity.getBillingPeroidToDate()!= null){
		invoiceinfotable.addCell(billingFromDtName);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(billingFromDtValue);
		
		invoiceinfotable.addCell(billingToDtName);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(billingToDtValue);
	}
	//  ends here 
	invoiceinfotable.addCell(datetitlecell);
	invoiceinfotable.addCell(columnCell);
	invoiceinfotable.addCell(datecell);
	
	invoiceinfotable.addCell(ponotitlecell);
	invoiceinfotable.addCell(columnCell);
	invoiceinfotable.addCell(ponocell);
	
	
	invoiceinfotable.addCell(stdatecell);
	invoiceinfotable.addCell(columnCell);
	invoiceinfotable.addCell(sdatecell);
	invoiceinfotable.addCell(enddatecell);
	invoiceinfotable.addCell(columnCell);
	invoiceinfotable.addCell(edatecell);

	if(!con.getRefNo().equals("")){
	invoiceinfotable.addCell(refnocell);
	invoiceinfotable.addCell(columnCell);
	invoiceinfotable.addCell(refcell);
	}
	
	/**
	 * add on table invoicegroup 29/6/15
	 */
	if(!invoiceentity.getInvoiceGroup().equals("")){
		invoiceinfotable.addCell(invoicegroupcell1);
		invoiceinfotable.addCell(columnCell);
		invoiceinfotable.addCell(invoicegroupcell3);
		PdfPCell blankcell=new PdfPCell();
		blankcell.setBorder(0);
	}
	
	Phrase titleBranch=new Phrase("Branch",font8);
	PdfPCell titlebranchcell=new PdfPCell();
	titlebranchcell.addElement(titleBranch);
	titlebranchcell.setBorder(0);
	
	Phrase branchCustPhrase=new Phrase(cust.getBranch(),font8);
	PdfPCell branchCustCell=new PdfPCell();
	branchCustCell.addElement(branchCustPhrase);
	branchCustCell.setBorder(0);
	invoiceinfotable.addCell(titlebranchcell);
	invoiceinfotable.addCell(columnCell);
	invoiceinfotable.addCell(branchCustCell);
	
	PdfPTable pfifparenttable=new PdfPTable(1);
	pfifparenttable.setWidthPercentage(100f);
	
	
	
	PdfPCell fiscalYear = new PdfPCell();
	fiscalYear.addElement(fiscal);
	fiscalYear.setBorder(0);
	
	PdfPCell invoiccell=new PdfPCell();
	invoiccell.addElement(invoiceinfotable);
	invoiccell.setBorder(0);
	
	pfifparenttable.addCell(fiscalYear);
	pfifparenttable.addCell(invoiccell);
	
	
		
	if(!cust.getSecondaryAdress().getAddrLine1().equals("")&&customerbranchlist.size()==0){
		if(ServiceAndBillingAdressSamePrintOnes==true && cust.getAdress().getCompleteAddress().trim().endsWith(cust.getSecondaryAdress().getCompleteAddress().trim())){
			 logger.log(Level.SEVERE,"RRR in side flag true and address same  :::");		
		}
		else if(ServiceAndBillingAdressSamePrintOnes==true && ! cust.getAdress().getCompleteAddress().trim().endsWith(cust.getSecondaryAdress().getCompleteAddress().trim()))
		{
			 logger.log(Level.SEVERE,"RRR in side flag true and address not same  :::");	
			Phrase addtitle=new Phrase("Service Address ",font8bold);
			
			Phrase serviceaddress1=new Phrase("     "+cust.getSecondaryAdress().getAddrLine1().trim(),font8);
			
			Phrase serviceaddress2=null;
			if(!cust.getSecondaryAdress().getAddrLine2().equals("")||cust.getSecondaryAdress().getAddrLine2()!=null){
				serviceaddress2=new Phrase("     "+cust.getSecondaryAdress().getAddrLine2().trim(),font8);
			}else{
				serviceaddress2=new Phrase("",font10);
			}
			Phrase sevicelandmark=null;
			
			if(!cust.getSecondaryAdress().getLandmark().equals("")){
				sevicelandmark=new Phrase("     "+cust.getSecondaryAdress().getLandmark(),font8);
			}else{
				sevicelandmark=new Phrase("",font10);
			}
			Phrase servicelocality=null;
			if(!cust.getSecondaryAdress().getLocality().equals("")){
				servicelocality=new Phrase("     "+cust.getSecondaryAdress().getLocality(),font8);
			}else{
				servicelocality=new Phrase("",font8);
			}
				Phrase servicecity=new Phrase("     "+cust.getSecondaryAdress().getCity().trim()+" - "+cust.getSecondaryAdress().getPin(),font8);
			
			
			PdfPCell addtiltlecell=new PdfPCell();
			addtiltlecell.addElement(addtitle);
			addtiltlecell.setBorder(0);
			
				
			PdfPCell serviceaddr1cell=new PdfPCell();
			serviceaddr1cell.addElement(serviceaddress1);
			serviceaddr1cell.setBorder(0);
			
			PdfPCell serviceadde2cell=new PdfPCell();
			serviceadde2cell.addElement(serviceaddress2);
			serviceadde2cell.setBorder(0);
			
			PdfPCell servicelocalitycell=new PdfPCell();
			servicelocalitycell.addElement(servicelocality);
			servicelocalitycell.setBorder(0);
			
			PdfPCell servicelanmarkcell=new PdfPCell();
			servicelanmarkcell.addElement(sevicelandmark);
			servicelanmarkcell.setBorder(0);
			
			PdfPCell servicecitycell=new PdfPCell();
			servicecitycell.addElement(servicecity);
			servicecitycell.setBorder(0);
			
			PdfPTable servicetable=new PdfPTable(1);
			servicetable.setWidthPercentage(100f);
			servicetable.addCell(addtiltlecell);
			servicetable.addCell(serviceaddr1cell);
			servicetable.addCell(serviceadde2cell);
			servicetable.addCell(servicelanmarkcell);
			servicetable.addCell(servicelocalitycell);
			servicetable.addCell(servicecitycell);
			
			PdfPCell sevicecell=new PdfPCell();
			sevicecell.addElement(servicetable);
			sevicecell.setBorder(0);
			
			pfifparenttable.addCell(servicetable);
		}
		else if(ServiceAndBillingAdressSamePrintOnes==false)
		{
			 logger.log(Level.SEVERE,"RRR in side flag false :::");
			Phrase addtitle=new Phrase("Service Address ",font8bold);
			
			Phrase serviceaddress1=new Phrase("     "+cust.getSecondaryAdress().getAddrLine1().trim(),font8);
			
			Phrase serviceaddress2=null;
			if(!cust.getSecondaryAdress().getAddrLine2().equals("")||cust.getSecondaryAdress().getAddrLine2()!=null){
				serviceaddress2=new Phrase("     "+cust.getSecondaryAdress().getAddrLine2().trim(),font8);
			}else{
				serviceaddress2=new Phrase("",font10);
			}
			Phrase sevicelandmark=null;
			
			if(!cust.getSecondaryAdress().getLandmark().equals("")){
				sevicelandmark=new Phrase("     "+cust.getSecondaryAdress().getLandmark(),font8);
			}else{
				sevicelandmark=new Phrase("",font10);
			}
			Phrase servicelocality=null;
			if(!cust.getSecondaryAdress().getLocality().equals("")){
				servicelocality=new Phrase("     "+cust.getSecondaryAdress().getLocality(),font8);
			}else{
				servicelocality=new Phrase("",font8);
			}
				Phrase servicecity=new Phrase("     "+cust.getSecondaryAdress().getCity().trim()+" - "+cust.getSecondaryAdress().getPin(),font8);
			
			
			PdfPCell addtiltlecell=new PdfPCell();
			addtiltlecell.addElement(addtitle);
			addtiltlecell.setBorder(0);
			
				
			PdfPCell serviceaddr1cell=new PdfPCell();
			serviceaddr1cell.addElement(serviceaddress1);
			serviceaddr1cell.setBorder(0);
			
			PdfPCell serviceadde2cell=new PdfPCell();
			serviceadde2cell.addElement(serviceaddress2);
			serviceadde2cell.setBorder(0);
			
			PdfPCell servicelocalitycell=new PdfPCell();
			servicelocalitycell.addElement(servicelocality);
			servicelocalitycell.setBorder(0);
			
			PdfPCell servicelanmarkcell=new PdfPCell();
			servicelanmarkcell.addElement(sevicelandmark);
			servicelanmarkcell.setBorder(0);
			
			PdfPCell servicecitycell=new PdfPCell();
			servicecitycell.addElement(servicecity);
			servicecitycell.setBorder(0);
			
			PdfPTable servicetable=new PdfPTable(1);
			servicetable.setWidthPercentage(100f);
			servicetable.addCell(addtiltlecell);
			servicetable.addCell(serviceaddr1cell);
			servicetable.addCell(serviceadde2cell);
			servicetable.addCell(servicelanmarkcell);
			servicetable.addCell(servicelocalitycell);
			servicetable.addCell(servicecitycell);
			
			PdfPCell sevicecell=new PdfPCell();
			sevicecell.addElement(servicetable);
			sevicecell.setBorder(0);
			
			pfifparenttable.addCell(servicetable);
		}
	}
	else
	{
		/********************* when customer branch added to customer and save the that customer invoice with branch then this branch address will added in place of service address**********/
		
		for(int i=0; i<customerbranchlist.size();i++){
			
				Phrase addtitle=new Phrase("Branch Address ",font8bold);
				
				Phrase serviceaddress1=new Phrase("     "+customerbranchlist.get(i).getAddress().getAddrLine1().trim(),font8);
				
				Phrase serviceaddress2=null;
				if(!customerbranchlist.get(i).getAddress().getAddrLine2().equals("")||customerbranchlist.get(i).getAddress().getAddrLine2()!=null){
					serviceaddress2=new Phrase("     "+customerbranchlist.get(i).getAddress().getAddrLine2().trim(),font8);
					System.out.println("");
				}else{
					serviceaddress2=new Phrase("",font10);
				}
				
				Phrase sevicelandmark=null;
				if(!customerbranchlist.get(i).getAddress().getLandmark().equals("")){
					sevicelandmark=new Phrase("     "+customerbranchlist.get(i).getAddress().getLandmark(),font8);
				}else{
					sevicelandmark=new Phrase("",font10);
				}
				
				
				Phrase servicelocality=null;
				if(!customerbranchlist.get(i).getAddress().getLocality().equals("")){
					servicelocality=new Phrase("     "+customerbranchlist.get(i).getAddress().getLocality(),font8);
				}else{
					servicelocality=new Phrase("",font8);
				}
				
				
				Phrase servicecity=new Phrase("     "+customerbranchlist.get(i).getAddress().getCity().trim()+" - "+customerbranchlist.get(i).getAddress().getPin(),font8);
				
				
				PdfPCell addtiltlecell=new PdfPCell();
				addtiltlecell.addElement(addtitle);
				addtiltlecell.setBorder(0);
				
					
				PdfPCell serviceaddr1cell=new PdfPCell();
				serviceaddr1cell.addElement(serviceaddress1);
				serviceaddr1cell.setBorder(0);
				
				PdfPCell serviceadde2cell=new PdfPCell();
				serviceadde2cell.addElement(serviceaddress2);
				serviceadde2cell.setBorder(0);
				
				PdfPCell servicelocalitycell=new PdfPCell();
				servicelocalitycell.addElement(servicelocality);
				servicelocalitycell.setBorder(0);
				
				PdfPCell servicelanmarkcell=new PdfPCell();
				servicelanmarkcell.addElement(sevicelandmark);
				servicelanmarkcell.setBorder(0);
				
				PdfPCell servicecitycell=new PdfPCell();
				servicecitycell.addElement(servicecity);
				servicecitycell.setBorder(0);
				
				PdfPTable servicetable=new PdfPTable(1);
				servicetable.setWidthPercentage(100f);
				servicetable.addCell(addtiltlecell);
				servicetable.addCell(serviceaddr1cell);
				servicetable.addCell(serviceadde2cell);
				servicetable.addCell(servicelanmarkcell);
				servicetable.addCell(servicelocalitycell);
				servicetable.addCell(servicecitycell);
				
				PdfPCell sevicecell=new PdfPCell();
				sevicecell.addElement(servicetable);
				sevicecell.setBorder(0);
				
				pfifparenttable.addCell(servicetable);
				
		}
		
	}
	
	
	
	//if(con.getRefDate()!=null){
	//invoiceinfotable.addCell(refdatecell);
	//invoiceinfotable.addCell(columnCell);
	//invoiceinfotable.addCell(refdtcell);
	//}
	PdfPTable parenttable= new PdfPTable(2);
	parenttable.setWidthPercentage(100);
	try {
		parenttable.setWidths(new float[]{50,50});
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	PdfPCell custinfocell = new PdfPCell();
	PdfPCell  invoiceinfocell = new PdfPCell();
	
	custinfocell.addElement(custtable);
	invoiceinfocell.addElement(pfifparenttable);
	parenttable.addCell(custinfocell);
	parenttable.addCell(invoiceinfocell);
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	logger.log(Level.SEVERE,"End of Creating Company Info");
	}
	
	///do
	
	private void createProductDetailWithDiscount() {
		
		 logger.log(Level.SEVERE,"create product Info");
			Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			Phrase productdetails= new Phrase("Product Details",font1);
			Paragraph para = new Paragraph();
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(productdetails);
			para.setAlignment(Element.ALIGN_CENTER);
			para.add(Chunk.NEWLINE);
				
			PdfPTable table = new PdfPTable(10);
			table.setWidthPercentage(100);
			
			Phrase category = new Phrase("SR. NO. ",font1);
	      Phrase productname = new Phrase("ITEM DETAILS",font1);
	      Phrase qty = new Phrase("NO OF BRANCHES",font1);
	      Phrase area = new Phrase("AREA",font1);
	      Phrase unit = new Phrase("SERVICES",font1);
	      Phrase rate= new Phrase ("RATE",font1);
	      
	      Phrase percDisc = new Phrase("DISC %",font1);
	      Phrase discAmt = new Phrase("DISC AMT",font1);
	      
	//      Paragraph tax= new Paragraph();
		       //*************chnges mukesh on 22/4/2015******************** 
	      
	      Phrase servicetax= null;
	      Phrase servicetax1= null;
	      int flag=0;
	      int vat=0;
	      int st=0;
	      for(int i=0; i<salesProd.size();i++){
	      	
	      if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()==0)){
	       servicetax = new Phrase("VAT %",font1);
	       vat=vat+1;
	       System.out.println("phrase value===="+servicetax.toString());
	      }
	      else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()==0))
	      {
	      	  servicetax = new Phrase("ST %",font1);
	      	  st=st+1;
	      }
	       else if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()>0)){
	      	  servicetax1 = new Phrase("VAT / ST %",font1);
	      	  flag=flag+1;
	      	  System.out.println("flag value;;;;;"+flag);
	      }
	       else{
	      	  servicetax = new Phrase("TAX %",font1);
	       }
	      }
	      
	      
	      Phrase total = new Phrase("AMOUNT",font1);
			
	      
			 PdfPCell cellcategory = new PdfPCell(category);
			 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellproductname = new PdfPCell(productname);
	       cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellqty = new PdfPCell(qty);
	       cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell areaCell = new PdfPCell(area);
	       areaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellunit = new PdfPCell(unit);
	       cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellrate = new PdfPCell(rate);
	       cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell percDisccell = new PdfPCell(percDisc);
	       percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell celldiscAmt = new PdfPCell(discAmt);
	       celldiscAmt.setHorizontalAlignment(Element.ALIGN_CENTER);
	       
	       
	       PdfPCell cellservicetax1= null;
	       PdfPCell cellservicetax= null;
	       PdfPCell cellservicetax2= null;
	       Phrase servicetax2=null;
	       
	       if(flag>0){
	        cellservicetax1 = new PdfPCell(servicetax1);
	       cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
	       }
	       else if(vat>0 && st>0)
	       {
	      	 System.out.println("in side condition");
	      	 servicetax2 = new Phrase("VAT / ST",font1);
	      	 cellservicetax2 = new PdfPCell(servicetax2);
	           cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
	       }
	       else 
	       {
	      	   cellservicetax = new PdfPCell(servicetax);
	           cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	       }
	       
	       PdfPCell celltotal= new PdfPCell(total);
	       celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	       
	       table.addCell(cellcategory);
	       table.addCell(cellproductname);
	       table.addCell(cellqty);
	       table.addCell(areaCell);
	       table.addCell(cellunit);
	       table.addCell(cellrate);
	       table.addCell(percDisccell);
	       table.addCell(celldiscAmt);
	       if(flag>0){
	           table.addCell(cellservicetax1);
	           }else if(vat>0 && st>0){
	          	  table.addCell(cellservicetax2);
	           }else
	           {
	          	 table.addCell(cellservicetax);
	           }
	       table.addCell(celltotal);
	       
	       try {
				table.setWidths(columnWidths);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
	//       ///change dummy this.products.size() to eva
	//       
	//      if( eva<= firstBreakPoint){
	//			int size =  firstBreakPoint - eva;
	//		    blankLines = size*(185/19);
	//		}
	//		else{
	//			blankLines = 0;
	//		}
	//		table.setSpacingAfter(blankLines);	 
	       
	       
	
	    	 if( this.salesProd.size()<= firstBreakPoint){
				int size =  firstBreakPoint - this.salesProd.size();
				      blankLines = size*(70/5);
				  	System.out.println("blankLines size ="+blankLines);
				System.out.println("blankLines size ="+blankLines);
					}
					else{
						blankLines = 10f;
					}
					table.setSpacingAfter(blankLines);	
					Phrase rephrse=null;
	       
					
					
					 PdfPCell noServiceCell =null;
			if(isMultipleBillingOfSameContract()==true)
			{
				if(con.isContractRate())
				{
					
					  for(int i=0;i<this.salesProd.size();i++)
				       {
			      	 Phrase chunk=new Phrase((i+1)+"",font8);
			      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
			      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 
			      	 
			      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
			      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
			      	 
			      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
			      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
			      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 
			      	 
			      	 chunk = new Phrase(salesProd.get(i).getArea()+"",font8);
			      	 PdfPCell pdfareaCell = new PdfPCell(chunk);
			      	 pdfareaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 
			      	 
			      	for(int j =0; j<con.getItems().size();j++){
				      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
				      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
				      	noServiceCell = new PdfPCell(chunk);
				      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				      	 }
				      	 }
			      	 
			      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
			      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
			      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	 
			      	 
			      	chunk = new Phrase(df.format(salesProd.get(i).getProdPercDiscount()),font8);
			      	 PdfPCell pdfdisPercell = new PdfPCell(chunk);
			      	pdfdisPercell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	 
			      
			      	chunk = new Phrase(df.format(salesProd.get(i).getDiscountAmt()),font8);
			      	 PdfPCell pdfDisAmtcell = new PdfPCell(chunk);
			      	pdfDisAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	 
			      	 PdfPCell pdfservice =null;
			      	 Phrase vatno=null;
			      	 Phrase stno=null;
			      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
			      	 {
			      	 
			      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
			      		 if(st==salesProd.size()){
			      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
			      		 }
			      		 else{
			      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
			      		 }
			      		 }
			      	    else
			      		 chunk = new Phrase("N.A"+"",font8);
			      		 if(st==salesProd.size()){
			      	  pdfservice = new PdfPCell(chunk);
			      		 }else{
			      			 pdfservice = new PdfPCell(stno);
			      		 }
			      	  
			      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 }
			      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
			           {
			      		 
			      		 if(salesProd.get(i).getVatTax()!=null){
			      			 if(vat==salesProd.size()){
			      				 System.out.println("rohan=="+vat);
			      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
			      			 }else{
			      				 System.out.println("mukesh");
			           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
			      		 }
			      		 }
			           	    else{
			           		 chunk = new Phrase("N.A"+"",font8);
			           	    }
			      		 if(vat==salesProd.size()){
			           	  pdfservice = new PdfPCell(chunk);
			      		 }else{ pdfservice = new PdfPCell(vatno);}
			           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 }
			      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
			      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
			      			 
			            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
			            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
			            	 else
			            		 chunk = new Phrase("N.A"+"",font8);
			            	  pdfservice = new PdfPCell(chunk);
			            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      		 
			      	 }else{
			      		 
			      		 chunk = new Phrase("N.A"+"",font8);
			            	  pdfservice = new PdfPCell(chunk);
			            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      		 
			      	 }
			      	 
				 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
				 PdfPCell pdftotalproduct = new PdfPCell(chunk);
				 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
				 
				 
				 table.addCell(pdfcategcell);
				 table.addCell(pdfnamecell);
				 table.addCell(pdfqtycell);
				 table.addCell(pdfareaCell);
				 table.addCell(noServiceCell);
				 table.addCell(pdfspricecell);
				 table.addCell(pdfdisPercell);
				 table.addCell(pdfDisAmtcell);
				 table.addCell(pdfservice);
				 table.addCell(pdftotalproduct);
				 try {
					table.setWidths(columnWidths);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				 
				 count=i;
					
					
			   	if(count==this.salesProd.size()|| count==firstBreakPoint){
								
			   		
			   		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
			   		
			   		
								flag=firstBreakPoint;
								break;
					}
					
					System.out.println("flag value" + flag);
					if(firstBreakPoint==flag){
						
						termsConditionsInfo();
						addFooter();
						
						
				}

				}
					
				}else{
				
				System.out.println("inside disc amt column method return true ");
			       for(int i=0;i<this.salesProd.size()/2;i++)
			       {
			      	 Phrase chunk=new Phrase((i+1)+"",font8);
			      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
			      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 
			      	 
			      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
			      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
			      	 
			      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
			      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
			      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 
			      	 for(int j =0; j<con.getItems().size();j++){
			      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
			      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
			      	noServiceCell = new PdfPCell(chunk);
			      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 }
			      	 }
			      	 
			      	 
			      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
			      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
			      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	 
			//      	 if(salesProd.get(i).getServiceTax()!=null)
			//       	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
			//       	 else
			//       		 chunk = new Phrase("N.A"+"",font8);
			//       	 
			//       	 PdfPCell pdfservice = new PdfPCell(chunk);
			//       	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
			//       	 if(salesProd.get(i).getVatTax()!=null)
			//       	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
			//       	 else
			//       		 chunk = new Phrase("N.A"+"",font8);
			//       	 PdfPCell pdfvattax = new PdfPCell(chunk);
			       	 
			       	 if(salesProd.get(i).getProdPercDiscount()!=0)
			        	    chunk = new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8);
			        	 else
			        		 chunk = new Phrase("0"+"",font8);
			        	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
			        	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
			       	
			        	
			        	
			        	System.out.println("DISC AMT"+salesProd.get(i).getDiscountAmt());
			        	 if(salesProd.get(i).getDiscountAmt()!=0)
				        	    chunk = new Phrase(salesProd.get(i).getDiscountAmt()+"",font8);
				        	 else
				        		 chunk = new Phrase("0"+"",font8);
				        	 PdfPCell pdfdiscAMt = new PdfPCell(chunk);
				        	 pdfdiscAMt.setHorizontalAlignment(Element.ALIGN_RIGHT);	
			        	
			      	 
			      	 PdfPCell pdfservice =null;
			      	 Phrase vatno=null;
			      	 Phrase stno=null;
			      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
			      	 {
			      	 
			      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
			      		 if(st==salesProd.size()){
			      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
			      		 }
			      		 else{
			      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
			      		 }
			      		 }
			      	    else
			      		 chunk = new Phrase("N.A"+"",font8);
			      		 if(st==salesProd.size()){
			      	  pdfservice = new PdfPCell(chunk);
			      		 }else{
			      			 pdfservice = new PdfPCell(stno);
			      		 }
			      	  
			      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 }
			      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
			           {
			      		 
			      		 if(salesProd.get(i).getVatTax()!=null){
			      			 if(vat==salesProd.size()){
			      				 System.out.println("rohan=="+vat);
			      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
			      			 }else{
			      				 System.out.println("mukesh");
			           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
			      		 }
			      		 }
			           	    else{
			           		 chunk = new Phrase("N.A"+"",font8);
			           	    }
			      		 if(vat==salesProd.size()){
			           	  pdfservice = new PdfPCell(chunk);
			      		 }else{ pdfservice = new PdfPCell(vatno);}
			           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 }
			      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
			      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
			      			 
			            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
			            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
			            	 else
			            		 chunk = new Phrase("N.A"+"",font8);
			            	  pdfservice = new PdfPCell(chunk);
			            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      		 
			      	 }else{
			      		 
			      		 chunk = new Phrase("N.A"+"",font8);
			            	  pdfservice = new PdfPCell(chunk);
			            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
			      		 
			      	 }
			      	 
			 PdfPCell pdftotalproduct = new PdfPCell(chunk);
			 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
			 
			 table.addCell(pdfcategcell);
			 table.addCell(pdfnamecell);
			 table.addCell(pdfqtycell);
			 table.addCell(noServiceCell);
			 table.addCell(pdfspricecell);
			 table.addCell(pdfperdiscount);
			 table.addCell(pdfdiscAMt);
			 table.addCell(pdfservice);
//			 table.addCell(pdfservice1);
			 table.addCell(pdftotalproduct);
			 try {
					table.setWidths(columnWidths);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			 
			 
			 count=i;
					
					
		     	if(count==this.salesProd.size()|| count==firstBreakPoint){
								
		     		
		     		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
		     		
		     		
								flag=firstBreakPoint;
								break;
					}
						
					
					
					System.out.println("flag value" + flag);
					if(firstBreakPoint==flag){
						
						termsConditionsInfo();
						addFooter();
						
						
						}
			       }
				}
			}				
			else
			{	
					
	       for(int i=0;i<this.salesProd.size();i++)
	       {
	      	 Phrase chunk=new Phrase((i+1)+"",font8);
	      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
	      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
	      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
	      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
	      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	     	for(int j =0; j<con.getItems().size();j++){
		      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
		      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
		      	noServiceCell = new PdfPCell(chunk);
		      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 }
		      	 }
	      	 
	      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
	      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
	      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	 
	//      	 if(salesProd.get(i).getServiceTax()!=null)
	//       	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//       	 else
	//       		 chunk = new Phrase("N.A"+"",font8);
	//       	 
	//       	 PdfPCell pdfservice = new PdfPCell(chunk);
	//       	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//       	 if(salesProd.get(i).getVatTax()!=null)
	//       	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	//       	 else
	//       		 chunk = new Phrase("N.A"+"",font8);
	//       	 PdfPCell pdfvattax = new PdfPCell(chunk);
	       	 
	       	 if(salesProd.get(i).getProdPercDiscount()!=0)
	        	    chunk = new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8);
	        	 else
	        		 chunk = new Phrase("0"+"",font8);
	        	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
	        	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
	       	
	        	
	        	
	        	System.out.println("DISC AMT"+salesProd.get(i).getDiscountAmt());
	        	 if(salesProd.get(i).getDiscountAmt()!=0)
		        	    chunk = new Phrase(salesProd.get(i).getDiscountAmt()+"",font8);
		        	 else
		        		 chunk = new Phrase("0"+"",font8);
		        	 PdfPCell pdfdiscAMt = new PdfPCell(chunk);
		        	 pdfdiscAMt.setHorizontalAlignment(Element.ALIGN_RIGHT);	
	        	
	      	 
	      	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==salesProd.size()){
	      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 else{
	      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font8);
	      		 if(st==salesProd.size()){
	      	  pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(salesProd.get(i).getVatTax()!=null){
	      			 if(vat==salesProd.size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font8);
	           	    }
	      		 if(vat==salesProd.size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
	      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
	      	 
	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 
	 
	 table.addCell(pdfcategcell);
	 table.addCell(pdfnamecell);
	 table.addCell(pdfqtycell);
	 table.addCell(noServiceCell);
	 table.addCell(pdfspricecell);
	 table.addCell(pdfperdiscount);
	 table.addCell(pdfdiscAMt);
	 table.addCell(pdfservice);
//	 table.addCell(pdfservice1);
	 table.addCell(pdftotalproduct);
	 try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	 
	 count=i;
			
			
     	if(count==this.salesProd.size()|| count==firstBreakPoint){
						
     		
     		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
     		
     		
						flag=firstBreakPoint;
						break;
			}
				
			
			
			System.out.println("flag value" + flag);
			if(firstBreakPoint==flag){
				
				termsConditionsInfo();
				addFooter();
				
		}
	 

	  }
	}		
				
				PdfPTable parentTableProd=new PdfPTable(1);
	         parentTableProd.setWidthPercentage(100);
	         
	         PdfPCell prodtablecell=new PdfPCell();
	         prodtablecell.addElement(table);
	         
	      prodtablecell.addElement(rephrse);
	         parentTableProd.addCell(prodtablecell);
	         
	         
	         try {
				document.add(parentTableProd);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	      logger.log(Level.SEVERE,"ensds product info Info");
	}
	
	private void createProductDetailWithOutDisc() {
		

	  	logger.log(Level.SEVERE,"create product Info");
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
		Phrase productdetails= new Phrase("Product Details",font1);
		Paragraph para = new Paragraph();
		para.add(Chunk.NEWLINE);
		para.add(Chunk.NEWLINE);
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
		para.add(Chunk.NEWLINE);
			
		PdfPTable table = null;
			 float[] columnWidths = {0.7f, 3.1f, 1.0f,0.8f,1.0f,0.8f,0.8f,0.9f};
			 table = new PdfPTable(8);
				try {
					table.setWidths(columnWidths);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
	
		table.setWidthPercentage(100);
		
	  Phrase category = new Phrase("SR. NO. ",font1);
      Phrase productname = new Phrase("ITEM DETAILS",font1);
      Phrase qty = new Phrase("NO OF BRANCHES",font1);
      Phrase area = new Phrase("AREA",font1);
      Phrase unit = new Phrase("SERVICES",font1);
      Phrase rate= new Phrase ("RATE",font1);
      
      
      Phrase servicetax= null;
      Phrase servicetax1= null;
      int flag=0;
      int vat=0;
      int st=0;
      for(int i=0; i<salesProd.size();i++){
      	
      if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()==0)){
       servicetax = new Phrase("VAT %",font1);
       vat=vat+1;
       System.out.println("phrase value===="+servicetax.toString());
      }
      else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()==0))
      {
      	  servicetax = new Phrase("ST %",font1);
      	  st=st+1;
      }
       else if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()>0)){
      	  servicetax1 = new Phrase("VAT / ST %",font1);
      	  flag=flag+1;
      	  System.out.println("flag value;;;;;"+flag);
      }
       else{
      	 
      	  servicetax = new Phrase("TAX %",font1);
       }
      }
      
      
      Phrase total = new Phrase("AMOUNT",font1);
		
      
		 PdfPCell cellcategory = new PdfPCell(category);
		 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
       PdfPCell cellproductname = new PdfPCell(productname);
       cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
       PdfPCell cellqty = new PdfPCell(qty);
       cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
       PdfPCell areaCell = new PdfPCell(area);
       areaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
       
       PdfPCell cellunit = new PdfPCell(unit);
       cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
       PdfPCell cellrate = new PdfPCell(rate);
       cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
       
       
       PdfPCell cellservicetax1= null;
       PdfPCell cellservicetax= null;
       PdfPCell cellservicetax2= null;
       Phrase servicetax2=null;
       
       if(flag>0){
        cellservicetax1 = new PdfPCell(servicetax1);
       cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
       }
       else if(vat>0 && st>0)
       {
      	 System.out.println("in side condition");
      	 servicetax2 = new Phrase("VAT / ST",font1);
      	 cellservicetax2 = new PdfPCell(servicetax2);
           cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
       }
       else {
      	  cellservicetax = new PdfPCell(servicetax);
           cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 
       }
       
       PdfPCell celltotal= new PdfPCell(total);
       celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
       
       table.addCell(cellcategory);
       table.addCell(cellproductname);
       table.addCell(cellqty);
       table.addCell(areaCell);
       table.addCell(cellunit);
       table.addCell(cellrate);
       
       if(flag>0){
           table.addCell(cellservicetax1);
           }else if(vat>0 && st>0){
          	  table.addCell(cellservicetax2);
           }else
           {
          	 table.addCell(cellservicetax);
           }
       table.addCell(celltotal);
       
       try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
//       ///change dummy this.products.size() to eva
//       
//      if( eva<= firstBreakPoint){
//			int size =  firstBreakPoint - eva;
//		    blankLines = size*(185/19);
//		}
//		else{
//			blankLines = 0;
//		}
//		table.setSpacingAfter(blankLines);	 
       
       

    	 if( this.salesProd.size()<= firstBreakPoint){
			int size =  firstBreakPoint - this.salesProd.size();
			      blankLines = size*(70/5);
			  	System.out.println("blankLines size ="+blankLines);
			System.out.println("blankLines size ="+blankLines);
				}
				else{
					blankLines = 10f;
				}
				table.setSpacingAfter(blankLines);	
				Phrase rephrse=null;

			PdfPCell noServiceCell = null;
	if(isMultipleBillingOfSameContract()==true)
	{
		System.out.println("inside disc % column method return true ");
		if(con.isContractRate())
		{
			
			  for(int i=0;i<this.salesProd.size();i++)
		       {
	      	 Phrase chunk=new Phrase((i+1)+"",font8);
	      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
	      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
	      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
	      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
	      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getArea()+"",font8);
	      	 PdfPCell pdfareaCell = new PdfPCell(chunk);
	      	 pdfareaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	      	
	      	for(int j =0; j<con.getItems().size();j++){
		      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
		      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
		      	 noServiceCell = new PdfPCell(chunk);
		      	 noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		       }
		    }
	      	 
	      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
	      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
	      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	 
	      	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==salesProd.size()){
	      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 else{
	      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font8);
	      		 if(st==salesProd.size()){
	      	  pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(salesProd.get(i).getVatTax()!=null){
	      			 if(vat==salesProd.size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font8);
	           	    }
	      		 if(vat==salesProd.size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
	      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
	      	 
		 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
		 PdfPCell pdftotalproduct = new PdfPCell(chunk);
		 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
		 
		 
		 table.addCell(pdfcategcell);
		 table.addCell(pdfnamecell);
		 table.addCell(pdfqtycell);
		 table.addCell(pdfareaCell);
		 table.addCell(noServiceCell);
		 table.addCell(pdfspricecell);
		 table.addCell(pdfservice);
		 table.addCell(pdftotalproduct);
		 try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
		 count=i;
			
			
	   	if(count==this.salesProd.size()|| count==firstBreakPoint){
						
	   		
	   		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
	   		
	   		
						flag=firstBreakPoint;
						break;
			}
			
			System.out.println("flag value" + flag);
			if(firstBreakPoint==flag){
				
				termsConditionsInfo();
				addFooter();
				
				
		}

		}
			
		}else{
		  for(int i=0;i<this.salesProd.size()/2;i++)
	       {
      	 Phrase chunk=new Phrase((i+1)+"",font8);
      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 
      	 
      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
      	 
      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 
      	 chunk = new Phrase(salesProd.get(i).getArea()+"",font8);
     	 PdfPCell pdfareaCell = new PdfPCell(chunk);
     	 pdfareaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	
      	 
      	for(int j =0; j<con.getItems().size();j++){
	      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
	      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
	      	noServiceCell = new PdfPCell(chunk);
	      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 }
      	 
      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	 
      	 PdfPCell pdfservice =null;
      	 Phrase vatno=null;
      	 Phrase stno=null;
      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
      	 {
      	 
      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
      		 if(st==salesProd.size()){
      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
      		 }
      		 else{
      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
      		 }
      		 }
      	    else
      		 chunk = new Phrase("N.A"+"",font8);
      		 if(st==salesProd.size()){
      	  pdfservice = new PdfPCell(chunk);
      		 }else{
      			 pdfservice = new PdfPCell(stno);
      		 }
      	  
      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 }
      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
           {
      		 
      		 if(salesProd.get(i).getVatTax()!=null){
      			 if(vat==salesProd.size()){
      				 System.out.println("rohan=="+vat);
      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
      			 }else{
      				 System.out.println("mukesh");
           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
      		 }
      		 }
           	    else{
           		 chunk = new Phrase("N.A"+"",font8);
           	    }
      		 if(vat==salesProd.size()){
           	  pdfservice = new PdfPCell(chunk);
      		 }else{ pdfservice = new PdfPCell(vatno);}
           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 }
      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
      			 
            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
            	 else
            		 chunk = new Phrase("N.A"+"",font8);
            	  pdfservice = new PdfPCell(chunk);
            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      		 
      	 }else{
      		 
      		 chunk = new Phrase("N.A"+"",font8);
            	  pdfservice = new PdfPCell(chunk);
            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      		 
      	 }
      	 
	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 
	 
	 table.addCell(pdfcategcell);
	 table.addCell(pdfnamecell);
	 table.addCell(pdfqtycell);
	 table.addCell(pdfareaCell);
	 table.addCell(noServiceCell);
	 table.addCell(pdfspricecell);
	 table.addCell(pdfservice);
	 table.addCell(pdftotalproduct);
	 try {
		table.setWidths(columnWidths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 
	 count=i;
		
		
   	if(count==this.salesProd.size()|| count==firstBreakPoint){
					
   		
   		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
   		
   		
					flag=firstBreakPoint;
					break;
		}
		
		System.out.println("flag value" + flag);
		if(firstBreakPoint==flag){
			
			termsConditionsInfo();
			addFooter();
			
			
	}

	}
	}
	}
	else
	{			
       for(int i=0;i<this.salesProd.size();i++)
       {
      	 Phrase chunk=new Phrase((i+1)+"",font8);
      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 
      	 
      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
      	 
      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 
      	chunk = new Phrase(salesProd.get(i).getArea()+"",font8);
     	PdfPCell pdfareaCell = new PdfPCell(chunk);
     	pdfareaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 
      	
      	 
   	 	for(int j =0; j<con.getItems().size();j++){
	      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
	      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
	      	noServiceCell = new PdfPCell(chunk);
	      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 }
      	 
      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
      	 
//      	 if(salesProd.get(i).getServiceTax()!=null)
//       	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//       	 else
//       		 chunk = new Phrase("N.A"+"",font8);
//       	 
//       	 PdfPCell pdfservice = new PdfPCell(chunk);
//       	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
//       	 if(salesProd.get(i).getVatTax()!=null)
//       	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
//       	 else
//       		 chunk = new Phrase("N.A"+"",font8);
//       	 PdfPCell pdfvattax = new PdfPCell(chunk);
//       	 asfdg
//      	 
//      	 if(discPer >0)
//    	 {
//    	 if(salesProd.get(i).getProdPercDiscount()!=0){
//    		chunk=new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8); 
//    	 }
//    	 else{
//    		 chunk=new Phrase("0"+"",font8);
//    	 }
//    	 }
//    	 else
//    	 {
//    		 if(salesProd.get(i).getDiscountAmt()!=0){
// 	    		chunk=new Phrase(salesProd.get(i).getDiscountAmt()+"",font8); 
// 	    	 }
// 	    	 else{
// 	    		 chunk=new Phrase("0"+"",font8);
// 	    	 }
//    	 }
//      	 
//        	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
//        	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
       	
      	 
      	 PdfPCell pdfservice =null;
      	 Phrase vatno=null;
      	 Phrase stno=null;
      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
      	 {
      	 
      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
      		 if(st==salesProd.size()){
      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
      		 }
      		 else{
      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
      		 }
      		 }
      	    else
      		 chunk = new Phrase("N.A"+"",font8);
      		 if(st==salesProd.size()){
      	  pdfservice = new PdfPCell(chunk);
      		 }else{
      			 pdfservice = new PdfPCell(stno);
      		 }
      	  
      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 }
      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
           {
      		 
      		 if(salesProd.get(i).getVatTax()!=null){
      			 if(vat==salesProd.size()){
      				 System.out.println("rohan=="+vat);
      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
      			 }else{
      				 System.out.println("mukesh");
           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
      		 }
      		 }
           	    else{
           		 chunk = new Phrase("N.A"+"",font8);
           	    }
      		 if(vat==salesProd.size()){
           	  pdfservice = new PdfPCell(chunk);
      		 }else{ pdfservice = new PdfPCell(vatno);}
           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      	 }
      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
      			 
            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
            	 else
            		 chunk = new Phrase("N.A"+"",font8);
            	  pdfservice = new PdfPCell(chunk);
            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      		 
      	 }else{
      		 
      		 chunk = new Phrase("N.A"+"",font8);
            	  pdfservice = new PdfPCell(chunk);
            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
      		 
      	 }
      	 
//       	if(qp instanceof SalesOrder){
//       		SalesOrder salesq=(SalesOrder)qp;
//       		
//       		if(salesq.getCformstatus().trim().equals(AppConstants.YES)||salesq.getCformstatus().trim().equals(AppConstants.NO))
//       		{
//       			cstFlag=1;
//       			cstval=salesq.getCstpercent();
//       		}
//       	}
       	
//       	if(qp instanceof SalesOrder){
//       		SalesOrder salesorder=(SalesOrder)qp;
//       		
//       		if(salesorder.getCformstatus().trim().equals(AppConstants.YES)||salesorder.getCformstatus().trim().equals(AppConstants.NO))
//       		{
//       			cstFlag=1;
//       			cstval=salesorder.getCstpercent();
//       		}
//       	}
      	 
      	 
//      	 System.out.println("one");
//      	 
//      	  if((salesProd.get(i).getVatTax().getPercentage()>0)&& (salesProd.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
//            	   
//      		  System.out.println("two");
//      		  if((salesProd.get(i).getVatTax()!=null)&&(salesProd.get(i).getServiceTax()!=null))
//              	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//              	 else
//              		 chunk = new Phrase("0"+"",font8);
//         		  pdfservice1 = new PdfPCell(chunk);
//         		 
//         		  
//         		  pdfservice1 = new PdfPCell(chunk);
//              	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//      		  
//      		  
//      		  
//      	  }
//           else if((salesProd.get(i).getServiceTax().getPercentage()>0) && (salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
//          	
//          	 System.out.println("three");
//          	 if(salesProd.get(i).getServiceTax()!=null)
//          		
//           	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//          	 
//           	 else{
//           		 chunk = new Phrase("0"+"",font8);
//           	 }
//           	  pdfservice1 = new PdfPCell(chunk);
//           	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//           	 
//           }
//           else if((salesProd.get(i).getServiceTax().getPercentage()>0) &&(salesProd.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
//          	 System.out.println("four");
//          	 chunk = new Phrase(cstval+""+"/"+
//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//          		 
//          		 pdfservice1 = new PdfPCell(chunk);
//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//          	 
//          	 
//          	 
//	  
//           }
//           else if(cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)
//          		 &&(salesProd.get(i).getVatTax().getPercentage()==0)){
//          	 
//          	 System.out.println("five");
//          		 chunk = new Phrase(cstval+"",font8);
//          		 
//          		 pdfservice1 = new PdfPCell(chunk);
//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//          	 
//           } 
//           else if(cstFlag==1&&salesProd.get(i).getServiceTax().getPercentage()>0){
//        	  
//          	 System.out.println("six");
//          	 chunk = new Phrase(cstval+""+"/"+
//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//          		 
//          		 pdfservice1 = new PdfPCell(chunk);
//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//          	 
//           } 
//           else if((cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))){
//        	   
//          	 System.out.println("seven");
//          	 chunk = new Phrase(cstval+"/"+"0",font8);
//    			  
////    		 chunk = new Phrase("0"+"",font8);
//    	  pdfservice1 = new PdfPCell(chunk);
//    	  
//     	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//          	 
//           }else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
//          	 System.out.println("four");
//          	 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+"/"+
//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
//          		 
//          		 pdfservice1 = new PdfPCell(chunk);
//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//          	 
//          	 
//          	 
//	  
//           }
//        	
        	
	
	/////
	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
	 
	 
	 
	 
	 table.addCell(pdfcategcell);
	 table.addCell(pdfnamecell);
	 table.addCell(pdfqtycell);
	 table.addCell(pdfareaCell);
	 table.addCell(noServiceCell);
	 table.addCell(pdfspricecell);
//	 table.addCell(pdfperdiscount);
	 table.addCell(pdfservice);
//	 table.addCell(pdfservice1);
	 table.addCell(pdftotalproduct);
	 try {
		table.setWidths(columnWidths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 
	 count=i;
		
		
    	if(count==this.salesProd.size()|| count==firstBreakPoint){
					
    		
    		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
    		
    		
					flag=firstBreakPoint;
					break;
		}
			
		
		
		System.out.println("flag value" + flag);
		if(firstBreakPoint==flag){
			
			termsConditionsInfo();
			addFooter();
			
			
			}

}
}		
			
			PdfPTable parentTableProd=new PdfPTable(1);
         parentTableProd.setWidthPercentage(100);
         
         PdfPCell prodtablecell=new PdfPCell();
         prodtablecell.addElement(table);
         
      prodtablecell.addElement(rephrse);
         parentTableProd.addCell(prodtablecell);
         
         
         try {
			document.add(parentTableProd);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
      logger.log(Level.SEVERE,"ensds product info Info");

	}
	
	public  void createProductTable()
	{
		  	logger.log(Level.SEVERE,"create product Info");
			Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			Phrase productdetails= new Phrase("Product Details",font1);
			Paragraph para = new Paragraph();
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(productdetails);
			para.setAlignment(Element.ALIGN_CENTER);
			para.add(Chunk.NEWLINE);
				
			PdfPTable table = null;
			 if(discPer >0)
			{
				float[] columnWidths = {0.7f, 3.5f, 0.7f,0.9f, 0.9f,0.7f,0.7f,0.9f,1f};
				table = new PdfPTable(9);
				try {
					table.setWidths(columnWidths);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
			}
			 else if(discAmt >0)
			{
				 float[] columnWidths = {0.7f, 3.5f, 0.7f,0.9f, 0.9f,0.7f,0.7f,0.9f,1f};
					table = new PdfPTable(9);
					try {
						table.setWidths(columnWidths);
					} catch (DocumentException e) {
						e.printStackTrace();
					}
			}
//			else
//			{
//				 float[] columnWidths ={0.7f, 3.5f, 0.7f, 0.9f,0.7f,0.7f,0.9f,1f};
//				 table = new PdfPTable(7);
//					try {
//						table.setWidths(columnWidths);
//					} catch (DocumentException e) {
//						e.printStackTrace();
//					}
//			}
		
			table.setWidthPercentage(100);
			
		  Phrase category = new Phrase("SR. NO. ",font1);
	      Phrase productname = new Phrase("ITEM DETAILS",font1);
	      Phrase qty = new Phrase("NO OF BRANCHES",font1);
	      Phrase area = new Phrase("AREA",font1);
	      Phrase unit = new Phrase("SERVICES",font1);
	      Phrase rate= new Phrase ("RATE",font1);
	      
	      Phrase percDisc=null;
	  
	       if(discPer >0)
	        {
	    	   percDisc = new Phrase("DISC %",font1);
		    }
		    else if(discAmt >0)
		    {
		    	percDisc=new Phrase("DISC",font1);
		    }
	//      Paragraph tax= new Paragraph();
		       //*************chnges mukesh on 22/4/2015******************** 
	      
	      Phrase servicetax= null;
	      Phrase servicetax1= null;
	      int flag=0;
	      int vat=0;
	      int st=0;
	      for(int i=0; i<salesProd.size();i++){
	      	
	      if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()==0)){
	       servicetax = new Phrase("VAT %",font1);
	       vat=vat+1;
	       System.out.println("phrase value===="+servicetax.toString());
	      }
	      else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()==0))
	      {
	      	  servicetax = new Phrase("ST %",font1);
	      	  st=st+1;
	      }
	       else if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()>0)){
	      	  servicetax1 = new Phrase("VAT / ST %",font1);
	      	  flag=flag+1;
	      	  System.out.println("flag value;;;;;"+flag);
	      }
	       else{
	      	 
	      	  servicetax = new Phrase("TAX %",font1);
	       }
	      }
	      
	      
	      Phrase total = new Phrase("AMOUNT",font1);
			
	      
			 PdfPCell cellcategory = new PdfPCell(category);
			 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellproductname = new PdfPCell(productname);
	       cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellqty = new PdfPCell(qty);
	       cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
	       
	       PdfPCell areaCell = new PdfPCell(area);
	       areaCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	       
	       PdfPCell cellunit = new PdfPCell(unit);
	       cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell cellrate = new PdfPCell(rate);
	       cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
	       PdfPCell percDisccell = new PdfPCell(percDisc);
	       percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
	//       PdfPCell cellservicetax = new PdfPCell(tax);
	//       cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	       
	       
	       PdfPCell cellservicetax1= null;
	       PdfPCell cellservicetax= null;
	       PdfPCell cellservicetax2= null;
	       Phrase servicetax2=null;
	       
	       if(flag>0){
	        cellservicetax1 = new PdfPCell(servicetax1);
	       cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
	       }
	       else if(vat>0 && st>0)
	       {
	      	 System.out.println("in side condition");
	      	 servicetax2 = new Phrase("VAT / ST",font1);
	      	 cellservicetax2 = new PdfPCell(servicetax2);
	           cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
	       }
	       else {
	      	  cellservicetax = new PdfPCell(servicetax);
	           cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	       }
	       
	       PdfPCell celltotal= new PdfPCell(total);
	       celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	       
	       table.addCell(cellcategory);
	       table.addCell(cellproductname);
	       table.addCell(cellqty);
	       table.addCell(areaCell);
	       table.addCell(cellunit);
	       table.addCell(cellrate);
	       table.addCell(percDisccell);
	       
	       if(flag>0){
	           table.addCell(cellservicetax1);
	           }else if(vat>0 && st>0){
	          	  table.addCell(cellservicetax2);
	           }else
	           {
	          	 table.addCell(cellservicetax);
	           }
	       table.addCell(celltotal);
	       
	       try {
				table.setWidths(columnWidths);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
	//       ///change dummy this.products.size() to eva
	//       
	//      if( eva<= firstBreakPoint){
	//			int size =  firstBreakPoint - eva;
	//		    blankLines = size*(185/19);
	//		}
	//		else{
	//			blankLines = 0;
	//		}
	//		table.setSpacingAfter(blankLines);	 
	       
	       
	
	    	 if( this.salesProd.size()<= firstBreakPoint){
				int size =  firstBreakPoint - this.salesProd.size();
				      blankLines = size*(70/5);
				  	System.out.println("blankLines size ="+blankLines);
				System.out.println("blankLines size ="+blankLines);
					}
					else{
						blankLines = 10f;
					}
					table.setSpacingAfter(blankLines);	
					Phrase rephrse=null;

				PdfPCell noServiceCell =null;	
					
		if(isMultipleBillingOfSameContract()==true)
		{
			System.out.println("inside disc % column method return true ");
			
			if(con.isContractRate())
			{
				
				  for(int i=0;i<this.salesProd.size();i++)
			       {
		      	 Phrase chunk=new Phrase((i+1)+"",font8);
		      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
		      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 
		      	 
		      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
		      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
		      	 
		      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
		      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
		      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 
		      	 chunk = new Phrase(salesProd.get(i).getArea()+"",font8);
		      	 PdfPCell pdfareacell = new PdfPCell(chunk);
		      	pdfareacell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 
		      	for(int j =0; j<con.getItems().size();j++){
			      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
			      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
			      	noServiceCell = new PdfPCell(chunk);
			      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			      	 }
			      }
		      	 
		      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
		      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
		      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		      	 
		      	 PdfPCell pdfdisPercell=null;
		      	 PdfPCell pdfdisAmtcell=null;
		      	if(discPer >0)
		        {
		      		chunk = new Phrase(df.format(salesProd.get(i).getProdPercDiscount()),font8);
			      	pdfdisPercell = new PdfPCell(chunk);
			      	pdfdisPercell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			    }
			    else if(discAmt >0)
			    {
			    	chunk = new Phrase(df.format(salesProd.get(i).getDiscountAmt()),font8);
			        pdfdisAmtcell = new PdfPCell(chunk);
			      	pdfdisAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			    }
		      	 
		      	 
		      	 PdfPCell pdfservice =null;
		      	 Phrase vatno=null;
		      	 Phrase stno=null;
		      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
		      	 {
		      	 
		      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
		      		 if(st==salesProd.size()){
		      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
		      		 }
		      		 else{
		      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
		      		 }
		      		 }
		      	    else
		      		 chunk = new Phrase("N.A"+"",font8);
		      		 if(st==salesProd.size()){
		      	  pdfservice = new PdfPCell(chunk);
		      		 }else{
		      			 pdfservice = new PdfPCell(stno);
		      		 }
		      	  
		      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 }
		      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
		           {
		      		 
		      		 if(salesProd.get(i).getVatTax()!=null){
		      			 if(vat==salesProd.size()){
		      				 System.out.println("rohan=="+vat);
		      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
		      			 }else{
		      				 System.out.println("mukesh");
		           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
		      		 }
		      		 }
		           	    else{
		           		 chunk = new Phrase("N.A"+"",font8);
		           	    }
		      		 if(vat==salesProd.size()){
		           	  pdfservice = new PdfPCell(chunk);
		      		 }else{ pdfservice = new PdfPCell(vatno);}
		           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 }
		      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
		      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
		      			 
		            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
		            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
		            	 else
		            		 chunk = new Phrase("N.A"+"",font8);
		            	  pdfservice = new PdfPCell(chunk);
		            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		      		 
		      	 }else{
		      		 
		      		 chunk = new Phrase("N.A"+"",font8);
		            	  pdfservice = new PdfPCell(chunk);
		            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		      		 
		      	 }
		      	 
			 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
			 PdfPCell pdftotalproduct = new PdfPCell(chunk);
			 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
			 
			 table.addCell(pdfcategcell);
			 table.addCell(pdfnamecell);
			 table.addCell(pdfqtycell);
			 table.addCell(pdfareacell);
			 table.addCell(noServiceCell);
			 table.addCell(pdfspricecell);
			 
		 	if(discPer >0)
	        {
			 table.addCell(pdfdisPercell);
		    }
		    else if(discAmt >0)
		    {
		    	 table.addCell(pdfdisAmtcell);
		    }
			 table.addCell(pdfservice);
			 table.addCell(pdftotalproduct);
//			 try {
//				table.setWidths(columnWidths);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			 
			 
			 count=i;
				
				
		   	if(count==this.salesProd.size()|| count==firstBreakPoint){
							
		   		
		   		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
		   		
		   		
							flag=firstBreakPoint;
							break;
				}
				
				System.out.println("flag value" + flag);
				if(firstBreakPoint==flag){
					
					termsConditionsInfo();
					addFooter();
					
					
			}

			}
				
			}else{
			
			for(int i=0;i<this.salesProd.size()/2;i++)
		       {
	      	 Phrase chunk=new Phrase((i+1)+"",font8);
	      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
	      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
	      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
	      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
	      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

	      	 	for(int j =0; j<con.getItems().size();j++){
		      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
		      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
		      	noServiceCell = new PdfPCell(chunk);
		      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 }
		      	 }
	      	 
	      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
	      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
	      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	 
	//      	 if(salesProd.get(i).getServiceTax()!=null)
	//       	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//       	 else
	//       		 chunk = new Phrase("N.A"+"",font8);
	//       	 
	//       	 PdfPCell pdfservice = new PdfPCell(chunk);
	//       	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//       	 if(salesProd.get(i).getVatTax()!=null)
	//       	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	//       	 else
	//       		 chunk = new Phrase("N.A"+"",font8);
	//       	 PdfPCell pdfvattax = new PdfPCell(chunk);
	       	 
	      	 
	      	 if(discPer >0)
	    	 {
	    	 if(salesProd.get(i).getProdPercDiscount()!=0){
	    		chunk=new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8); 
	    	 }
	    	 else{
	    		 chunk=new Phrase("0"+"",font8);
	    	 }
	    	 }
	    	 else
	    	 {
	    		 if(salesProd.get(i).getDiscountAmt()!=0){
	 	    		chunk=new Phrase(salesProd.get(i).getDiscountAmt()+"",font8); 
	 	    	 }
	 	    	 else{
	 	    		 chunk=new Phrase("0"+"",font8);
	 	    	 }
	    	 }
	      	 
	        	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
	        	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
	       	
	      	 
	      	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==salesProd.size()){
	      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 else{
	      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font8);
	      		 if(st==salesProd.size()){
	      	  pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(salesProd.get(i).getVatTax()!=null){
	      			 if(vat==salesProd.size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font8);
	           	    }
	      		 if(vat==salesProd.size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
	      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
	      	 
	//       	if(qp instanceof SalesOrder){
	//       		SalesOrder salesq=(SalesOrder)qp;
	//       		
	//       		if(salesq.getCformstatus().trim().equals(AppConstants.YES)||salesq.getCformstatus().trim().equals(AppConstants.NO))
	//       		{
	//       			cstFlag=1;
	//       			cstval=salesq.getCstpercent();
	//       		}
	//       	}
	       	
	//       	if(qp instanceof SalesOrder){
	//       		SalesOrder salesorder=(SalesOrder)qp;
	//       		
	//       		if(salesorder.getCformstatus().trim().equals(AppConstants.YES)||salesorder.getCformstatus().trim().equals(AppConstants.NO))
	//       		{
	//       			cstFlag=1;
	//       			cstval=salesorder.getCstpercent();
	//       		}
	//       	}
	      	 
	      	 
	//      	 System.out.println("one");
	//      	 
	//      	  if((salesProd.get(i).getVatTax().getPercentage()>0)&& (salesProd.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
	//            	   
	//      		  System.out.println("two");
	//      		  if((salesProd.get(i).getVatTax()!=null)&&(salesProd.get(i).getServiceTax()!=null))
	//              	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//              	 else
	//              		 chunk = new Phrase("0"+"",font8);
	//         		  pdfservice1 = new PdfPCell(chunk);
	//         		 
	//         		  
	//         		  pdfservice1 = new PdfPCell(chunk);
	//              	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//      		  
	//      		  
	//      		  
	//      	  }
	//           else if((salesProd.get(i).getServiceTax().getPercentage()>0) && (salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
	//          	
	//          	 System.out.println("three");
	//          	 if(salesProd.get(i).getServiceTax()!=null)
	//          		
	//           	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          	 
	//           	 else{
	//           		 chunk = new Phrase("0"+"",font8);
	//           	 }
	//           	  pdfservice1 = new PdfPCell(chunk);
	//           	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//           	 
	//           }
	//           else if((salesProd.get(i).getServiceTax().getPercentage()>0) &&(salesProd.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
	//          	 System.out.println("four");
	//          	 chunk = new Phrase(cstval+""+"/"+
	//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//          	 
	//          	 
	//	  
	//           }
	//           else if(cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)
	//          		 &&(salesProd.get(i).getVatTax().getPercentage()==0)){
	//          	 
	//          	 System.out.println("five");
	//          		 chunk = new Phrase(cstval+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//           } 
	//           else if(cstFlag==1&&salesProd.get(i).getServiceTax().getPercentage()>0){
	//        	  
	//          	 System.out.println("six");
	//          	 chunk = new Phrase(cstval+""+"/"+
	//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//           } 
	//           else if((cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))){
	//        	   
	//          	 System.out.println("seven");
	//          	 chunk = new Phrase(cstval+"/"+"0",font8);
	//    			  
	////    		 chunk = new Phrase("0"+"",font8);
	//    	  pdfservice1 = new PdfPCell(chunk);
	//    	  
	//     	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//           }else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
	//          	 System.out.println("four");
	//          	 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+"/"+
	//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//          	 
	//          	 
	//	  
	//           }
	//        	
	        	
   	
   	/////
 	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
 	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
 	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
 	 
 	 
 	 table.addCell(pdfcategcell);
 	 table.addCell(pdfnamecell);
 	 table.addCell(pdfqtycell);
 	 table.addCell(noServiceCell);
 	 table.addCell(pdfspricecell);
 	 table.addCell(pdfperdiscount);
 	 table.addCell(pdfservice);
// 	 table.addCell(pdfservice1);
 	 table.addCell(pdftotalproduct);
 	 try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	 
 	 
 	 count=i;
			
			
       	if(count==this.salesProd.size()|| count==firstBreakPoint){
						
       		
       		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
       		
       		
						flag=firstBreakPoint;
						break;
			}
				
			
			
			System.out.println("flag value" + flag);
			if(firstBreakPoint==flag){
				
				termsConditionsInfo();
				addFooter();
				
				
				}

		}
		}
		}
		else
		{			
	       for(int i=0;i<this.salesProd.size();i++)
	       {
	      	 Phrase chunk=new Phrase((i+1)+"",font8);
	      	 PdfPCell pdfcategcell = new PdfPCell(chunk);
	      	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
	      	 PdfPCell pdfnamecell = new PdfPCell(chunk);
	      	 
	      	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
	      	 PdfPCell pdfqtycell = new PdfPCell(chunk);
	      	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

	      	 //  rohan addedthos area field in this method 
     	     chunk = new Phrase(salesProd.get(i).getArea()+"",font8);
	      	 PdfPCell pdfAreacell = new PdfPCell(chunk);
	      	pdfAreacell.setHorizontalAlignment(Element.ALIGN_CENTER);

	      	 
	      	 for(int j =0; j<con.getItems().size();j++){
		      	 if(salesProd.get(i).getProdCode().equalsIgnoreCase(con.getItems().get(j).getProductCode())){
		      	 chunk = new Phrase(con.getItems().get(j).getNumberOfServices()+"",font8);
		      	noServiceCell = new PdfPCell(chunk);
		      	noServiceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		      	 }
		      	 }
	      	 
	      	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
	      	 PdfPCell pdfspricecell = new PdfPCell(chunk);
	      	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	      	 
	//      	 if(salesProd.get(i).getServiceTax()!=null)
	//       	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//       	 else
	//       		 chunk = new Phrase("N.A"+"",font8);
	//       	 
	//       	 PdfPCell pdfservice = new PdfPCell(chunk);
	//       	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//       	 if(salesProd.get(i).getVatTax()!=null)
	//       	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	//       	 else
	//       		 chunk = new Phrase("N.A"+"",font8);
	//       	 PdfPCell pdfvattax = new PdfPCell(chunk);
	       	 
	      	 
	      	 if(discPer >0)
	    	 {
	    	 if(salesProd.get(i).getProdPercDiscount()!=0){
	    		chunk=new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8); 
	    	 }
	    	 else{
	    		 chunk=new Phrase("0"+"",font8);
	    	 }
	    	 }
	    	 else
	    	 {
	    		 if(salesProd.get(i).getDiscountAmt()!=0){
	 	    		chunk=new Phrase(salesProd.get(i).getDiscountAmt()+"",font8); 
	 	    	 }
	 	    	 else{
	 	    		 chunk=new Phrase("0"+"",font8);
	 	    	 }
	    	 }
	      	 
	        	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
	        	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
	       	
	      	 
	      	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==salesProd.size()){
	      			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 else{
	      			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font8);
	      		 if(st==salesProd.size()){
	      	  pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(salesProd.get(i).getVatTax()!=null){
	      			 if(vat==salesProd.size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font8);
	           	    }
	      		 if(vat==salesProd.size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
	      		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font8);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
	      	 
	//       	if(qp instanceof SalesOrder){
	//       		SalesOrder salesq=(SalesOrder)qp;
	//       		
	//       		if(salesq.getCformstatus().trim().equals(AppConstants.YES)||salesq.getCformstatus().trim().equals(AppConstants.NO))
	//       		{
	//       			cstFlag=1;
	//       			cstval=salesq.getCstpercent();
	//       		}
	//       	}
	       	
	//       	if(qp instanceof SalesOrder){
	//       		SalesOrder salesorder=(SalesOrder)qp;
	//       		
	//       		if(salesorder.getCformstatus().trim().equals(AppConstants.YES)||salesorder.getCformstatus().trim().equals(AppConstants.NO))
	//       		{
	//       			cstFlag=1;
	//       			cstval=salesorder.getCstpercent();
	//       		}
	//       	}
	      	 
	      	 
	//      	 System.out.println("one");
	//      	 
	//      	  if((salesProd.get(i).getVatTax().getPercentage()>0)&& (salesProd.get(i).getServiceTax().getPercentage()>0)&&(cstFlag==0)){
	//            	   
	//      		  System.out.println("two");
	//      		  if((salesProd.get(i).getVatTax()!=null)&&(salesProd.get(i).getServiceTax()!=null))
	//              	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//              	 else
	//              		 chunk = new Phrase("0"+"",font8);
	//         		  pdfservice1 = new PdfPCell(chunk);
	//         		 
	//         		  
	//         		  pdfservice1 = new PdfPCell(chunk);
	//              	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//      		  
	//      		  
	//      		  
	//      	  }
	//           else if((salesProd.get(i).getServiceTax().getPercentage()>0) && (salesProd.get(i).getVatTax().getPercentage()==0&&cstFlag==0)){
	//          	
	//          	 System.out.println("three");
	//          	 if(salesProd.get(i).getServiceTax()!=null)
	//          		
	//           	    chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          	 
	//           	 else{
	//           		 chunk = new Phrase("0"+"",font8);
	//           	 }
	//           	  pdfservice1 = new PdfPCell(chunk);
	//           	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//           	 
	//           }
	//           else if((salesProd.get(i).getServiceTax().getPercentage()>0) &&(salesProd.get(i).getVatTax().getPercentage()>0)&&cstFlag==1){
	//          	 System.out.println("four");
	//          	 chunk = new Phrase(cstval+""+"/"+
	//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//          	 
	//          	 
	//	  
	//           }
	//           else if(cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)
	//          		 &&(salesProd.get(i).getVatTax().getPercentage()==0)){
	//          	 
	//          	 System.out.println("five");
	//          		 chunk = new Phrase(cstval+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//           } 
	//           else if(cstFlag==1&&salesProd.get(i).getServiceTax().getPercentage()>0){
	//        	  
	//          	 System.out.println("six");
	//          	 chunk = new Phrase(cstval+""+"/"+
	//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//           } 
	//           else if((cstFlag==1&&(salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))){
	//        	   
	//          	 System.out.println("seven");
	//          	 chunk = new Phrase(cstval+"/"+"0",font8);
	//    			  
	////    		 chunk = new Phrase("0"+"",font8);
	//    	  pdfservice1 = new PdfPCell(chunk);
	//    	  
	//     	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//           }else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0)&&(cstFlag==0)){
	//          	 System.out.println("four");
	//          	 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+"/"+
	//          			 salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	//          		 
	//          		 pdfservice1 = new PdfPCell(chunk);
	//               	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	//          	 
	//          	 
	//          	 
	//	  
	//           }
	//        	
	        	
    	
    	/////
  	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
  	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
  	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
  	 
  	 
  	 table.addCell(pdfcategcell);
  	 table.addCell(pdfnamecell);
  	 table.addCell(pdfqtycell);
  	 table.addCell(pdfAreacell);
  	 table.addCell(noServiceCell);
  	 table.addCell(pdfspricecell);
  	 table.addCell(pdfperdiscount);
  	 table.addCell(pdfservice);
//  	 table.addCell(pdfservice1);
  	 table.addCell(pdftotalproduct);
  	 try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  	 
  	 
  	 count=i;
			
			
        	if(count==this.salesProd.size()|| count==firstBreakPoint){
						
        		
        		 rephrse=new Phrase("Refer Annexure 1 for additional products ",font8);
        		
        		
						flag=firstBreakPoint;
						break;
			}
				
			
			
			System.out.println("flag value" + flag);
			if(firstBreakPoint==flag){
				
				termsConditionsInfo();
				addFooter();
				
				
				}
  	 
//  	 
//  	 count= i;
//  	 
//  	 ///change this.salesProd.size() to eva.
//		 
//		if(count==eva|| count==firstBreakPoint){
//					
//					flag=firstBreakPoint;
//					break;
//		}
//		if(firstBreakPoint==flag){
//			
//			termsConditionsInfo();
//			addFooter();
//			}
//   	}
  	////
   
//   Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
//	Phrase category = new Phrase("Category ", font1);
//	Phrase qty = new Phrase("Quantity", font1);
//	Phrase servicetax = new Phrase("Service Tax", font1);
//	Phrase vat = new Phrase("VAT", font1);
//	Phrase percdisc = new Phrase("% Discount", font1);
//
//	PdfPCell cellcategory = new PdfPCell(category);
//	PdfPCell cellqty = new PdfPCell(qty);
//	PdfPCell cellperdisc = new PdfPCell(percdisc);
//	PdfPCell cellvat = new PdfPCell(vat);
//	PdfPCell cellservicetax = new PdfPCell(servicetax);
//
//	table.addCell(cellcategory);
//	table.addCell(cellqty);
//	table.addCell(cellperdisc);
//	table.addCell(cellvat);
//	table.addCell(cellservicetax);
	
	
//	 if( eva<= firstBreakPoint){
//		int size =  firstBreakPoint - eva;
//		      blankLines = size*(185/18);
//		  	System.out.println("blankLines size ="+blankLines);
//		System.out.println("blankLines size ="+blankLines);
//			}
//			else{
//				blankLines = 0;
//			}
//			table.setSpacingAfter(blankLines);	
//	
//	
//	
//				
//				for(int i =0;i<eva;i++){
//				if(getdata().get(i).getProductCategory()!=null){
//				Phrase chunk = new Phrase(getdata().get(i).getProductCategory(),font8);
//				
//				}else{
//				}
//				PdfPCell pdfcategcell = new PdfPCell(chunk);
//	
//				
//				
//				
//				if(getdata().get(i).getProductQuantity()!=0){
//				chunk = new Phrase(getdata().get(i).getProductQuantity() + "", font8);
//				}
//				else{
//					chunk = new Phrase("", font8);
//				}
//				PdfPCell pdfqtycell = new PdfPCell(chunk);
//				
//				
//				
//				if(getdata().get(i).getTax()!=0){
//					chunk = new Phrase(getdata().get(i).getTax() + "", font8);
//				}else{
//					chunk = new Phrase("", font8);
//				}
//				PdfPCell pdfservice = new PdfPCell(chunk);
//	
//				
//				
//				
//				if(getdata().get(i).getVat()!=0){
//				
//					chunk = new Phrase(getdata().get(i).getVat() + "", font8);
//				}else{
//					chunk = new Phrase( "", font8);
//				}
//				PdfPCell pdfvattax = new PdfPCell(chunk);
//				
//				
//				
//				
//				if(getdata().get(i).getDiscount()!=0)
//				{
//					chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//			
//					}else{
//						chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
//					}	
//				
//				PdfPCell pdfperdiscount = new PdfPCell(chunk);
//	
//					table.addCell(pdfcategcell);
//					table.addCell(pdfqtycell);
//					table.addCell(pdfperdiscount);
//					table.addCell(pdfvattax);
//					table.addCell(pdfservice);
					
					
//					count=i;
//					
//					
//		        	if(count==eva|| count==firstBreakPoint){
//								
//								flag=firstBreakPoint;
//								break;
//					}
//						
//					
//					
//					System.out.println("flag value" + flag);
//					if(firstBreakPoint==flag){
//						
//						termsConditionsInfo();
//						addFooter();
//						
//						
//						}
   
   
   ////
   
}
	}		
				
				PdfPTable parentTableProd=new PdfPTable(1);
	         parentTableProd.setWidthPercentage(100);
	         
	         PdfPCell prodtablecell=new PdfPCell();
	         prodtablecell.addElement(table);
	         
	      prodtablecell.addElement(rephrse);
	         parentTableProd.addCell(prodtablecell);
	         
	         
	         try {
				document.add(parentTableProd);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	      logger.log(Level.SEVERE,"ensds product info Info");
}	
	

public void termsConditionsInfo()
{
logger.log(Level.SEVERE,"terms Conditions Info");
Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
////
Phrase blankval=new Phrase(" ",font8);
Phrase payphrase=new Phrase("Payment Terms :",font10bold);
payphrase.add(Chunk.NEWLINE);
PdfPTable table = new PdfPTable(3);
table.setWidthPercentage(100);
//table.setSpacingAfter(50f);

PdfPCell tablecell1 = new PdfPCell();
 if(this.payTerms.size()>4){
	 tablecell1= new PdfPCell(table);
//	 tablecell1.setBorder(0);
        }else{
        	tablecell1= new PdfPCell(table);
        	tablecell1.setBorder(0);
        }




Phrase paytermdays = new Phrase("Days",font1);
Phrase paytermpercent = new Phrase("Percent",font1);
Phrase paytermcomment = new Phrase("Comment",font1);

PdfPCell headingpayterms = new PdfPCell(payphrase);
headingpayterms.setBorder(0);
headingpayterms.setColspan(3);
PdfPCell celldays = new PdfPCell(paytermdays);
celldays.setBorder(0);
celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
PdfPCell cellpercent = new PdfPCell(paytermpercent);
cellpercent.setBorder(0);
cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
PdfPCell cellcomment = new PdfPCell(paytermcomment);
cellcomment.setBorder(0);
cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
//table.addCell(headingpayterms);
table.addCell(celldays);
table.addCell(cellpercent);
table.addCell(cellcomment);


if( this.payTerms.size()<= BreakPoint){
	int size =  BreakPoint - this.payTerms.size();
	      blankLines = size*(80/5);
	  	System.out.println("blankLines size ="+blankLines);
	System.out.println("blankLines size ="+blankLines);
		}
		else{
			blankLines = 0;
		}
		table.setSpacingAfter(blankLines);	


for(int i=0;i<this.payTerms.size();i++)
{
System.out.println(this.payTerms.size()+"  in for1");
//Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//PdfPCell pdfdayscell = new PdfPCell(chunk);
//pdfdayscell.setBorder(0);

Phrase chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
PdfPCell pdfdayscell = new PdfPCell(chunk);
pdfdayscell.setBorder(0);

pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);

chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
PdfPCell pdfpercentcell = new PdfPCell(chunk);
pdfpercentcell.setBorder(0);
pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
PdfPCell pdfcommentcell = new PdfPCell(chunk);
pdfcommentcell.setBorder(0);
pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

table.addCell(pdfdayscell);
table.addCell(pdfpercentcell);
table.addCell(pdfcommentcell);



count=i;
///
if(count==4|| count==BreakPoint){
		
		flag1=BreakPoint;
		break;
}


}

///2nd table for pay terms start
System.out.println(this.payTerms.size()+"  out");

//if(this.payTermsLis.size()>4){




PdfPTable table1 = new PdfPTable(3);
table1.setWidthPercentage(100);

PdfPCell table1cell = new PdfPCell();

 if(this.payTerms.size()>4){
	 table1cell= new PdfPCell(table1);
//	 table1cell.setBorder(0);
        }else{
        	table1cell= new PdfPCell(blankval);
        	table1cell.setBorder(0);
        }

Phrase paytermdays1 = new Phrase("Days",font1);
Phrase paytermpercent1 = new Phrase("Percent",font1);
Phrase paytermcomment1 = new Phrase("Comment",font1);

PdfPCell celldays1;
PdfPCell cellpercent1;
PdfPCell cellcomment1;

System.out.println(this.payTerms.size()+" ...........b4 if");
if(this.payTerms.size()>4){
System.out.println(this.payTerms.size()+" ...........af if");
celldays1= new PdfPCell(paytermdays1);
celldays1.setBorder(0);
celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
}else{
celldays1= new PdfPCell(blankval);
celldays1.setBorder(0);

}

if(this.payTerms.size()>4){

 cellpercent1 = new PdfPCell(paytermpercent1);
cellpercent1.setBorder(0);
cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
}else{
cellpercent1 = new PdfPCell(blankval);
cellpercent1.setBorder(0);

}

if(this.payTerms.size()>4){
cellcomment1= new PdfPCell(paytermcomment1);
cellcomment1.setBorder(0);
cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
}else{
cellcomment1= new PdfPCell(blankval);
cellcomment1.setBorder(0);
}
table1.addCell(celldays1);
table1.addCell(cellpercent1);
table1.addCell(cellcomment1);




for(int i=5;i<this.payTerms.size();i++)
{
System.out.println(this.payTerms.size()+"  in for");

Phrase chunk = null;
if(payTerms.get(i).getPayTermDays()!=null){
chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
}else{
chunk = new Phrase(" ",font8);
}

PdfPCell pdfdayscell = new PdfPCell(chunk);
pdfdayscell.setBorder(0);
pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
if(payTerms.get(i).getPayTermPercent()!=0){ 
chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
}else{
chunk = new Phrase(" ",font8);
}

PdfPCell pdfpercentcell = new PdfPCell(chunk);
pdfpercentcell.setBorder(0);
pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
if(payTerms.get(i).getPayTermComment()!=null){
chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
}else{
chunk = new Phrase(" ",font8);
}
PdfPCell pdfcommentcell = new PdfPCell(chunk);
pdfcommentcell.setBorder(0);
pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);

table1.addCell(pdfdayscell);
table1.addCell(pdfpercentcell);
table1.addCell(pdfcommentcell);

//count=i;
///
if(i==9|| count==BreakPoint){
		
//		flag1=BreakPoint;
		break;
}

}



///2nd table for pay terms end

table1cell.addElement(table1);


PdfPTable termstable1 = new PdfPTable(2);
termstable1.setWidthPercentage(100);
termstable1.addCell(headingpayterms);
termstable1.addCell(tablecell1);
termstable1.addCell(table1cell);




PdfPTable chargetaxtable = new PdfPTable(2);

chargetaxtable.setWidthPercentage(100);
chargetaxtable.setSpacingAfter(10f);




Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
totalAmtCell.setBorder(0);
double totalAmt = 0;
	
totalAmt = con.getTotalAmount();

Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
realtotalAmtCell.setBorder(0);
realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
chargetaxtable.addCell(totalAmtCell);
chargetaxtable.addCell(realtotalAmtCell);






int breakpoint=5;
int cnt=this.billingTaxesLis.size()+this.billingChargesLis.size();
System.out.println("cnt value;;;;;;;"+cnt);

List<String> myList1 = new ArrayList<>();
List<String> myList2 = new ArrayList<>();
 List<String> myList3 = new ArrayList<>();
	List<String> myList4 = new ArrayList<>();
	 List<String> myList5 = new ArrayList<>();
		List<String> myList6 = new ArrayList<>();
		
		
		 List<String> myList11 = new ArrayList<>();
			List<String> myList22 = new ArrayList<>();
			 List<String> myList33 = new ArrayList<>();
				List<String> myList44 = new ArrayList<>();
				 List<String> myList55 = new ArrayList<>();
					List<String> myList66 = new ArrayList<>();
		
//*****************rohan changes on 29/5/15 for charges from contract********************			
		
		 if(cnt>breakpoint){
			   
			   for(int i=0;i<this.contractTaxesLis.size();i++)
			      {
				   	   double taxAmt=0;
				   	double taxAmt1=0;
				   	double taxAmt2=0;
				   	double taxAmt3=0;
					double taxAmt4=0;
			    	  
			    	  if(contractTaxesLis.get(i).getChargePercent()!=0){
			    		  
			    		  
			    		  if((contractTaxesLis.get(i).getChargeName().equals("VAT")))
					  	      	
			    		  {  
			    			  
			    			  
			    			String name=contractTaxesLis.get(i).getChargeName()+" @ "+contractTaxesLis.get(i).getChargePercent();
			    			taxAmt=contractTaxesLis.get(i).getChargePercent()*contractTaxesLis.get(i).getAssessableAmount()/100;
			    			myList11.add(name);
			    			myList22.add(df.format(taxAmt));
			    			  
			    			  
			    		  }
			    		  
			    		  if((contractTaxesLis.get(i).getChargeName().equals("Service Tax"))&&(contractTaxesLis.get(i).getChargePercent()!=12.36))
					  	      	
			    		  {  
			    		  
			    		  String name=contractTaxesLis.get(i).getChargeName()+" @ "+contractTaxesLis.get(i).getChargePercent();
			    		  taxAmt1=contractTaxesLis.get(i).getChargePercent()*contractTaxesLis.get(i).getAssessableAmount()/100;
			    		  
			    		  myList33.add(name);
			    		  myList44.add(df.format(taxAmt1));
			    		  
			    		  
			    		  }
			    		  
			    		  
			    		  if((contractTaxesLis.get(i).getChargeName().equals("Service Tax"))&&(contractTaxesLis.get(i).getChargePercent()==12.36))
			  	      	{
			  	      		
			  	      		
			    			  
			    		String name="Service Tax "+"@ 12.00";	  
			    		taxAmt2=(contractTaxesLis.get(i).getAssessableAmount()*12)/100;
			    		String name1="Education Cess "+"@ 2.00";
			    		taxAmt3=(taxAmt2*2.00)/100;
			    		String name2="High Education Cess "+"@ 1.00";
			    		taxAmt4=(taxAmt2*1.00)/100;
			    			 
			    		String str=" ";
			    		
			    		myList55.add(name);
			    		myList55.add(name1);
			    		myList55.add(name2);
			    		
			    		myList66.add(df.format(taxAmt2));
			    		myList66.add(df.format(taxAmt3));
			    		myList66.add(str);
			    		myList66.add(df.format(taxAmt4));
			    		
			  	      	}
			    	  }
			     	}
			   
			   
			   
			   
			   
			   
			   PdfPCell pdfservicecell=null;
				
				PdfPTable other1table=new PdfPTable(1);
				other1table.setWidthPercentage(100);
				for(int j=0;j<myList11.size();j++){
					chunk = new Phrase(myList11.get(j),font8);
					 pdfservicecell = new PdfPCell(chunk);
					pdfservicecell.setBorder(0);
					pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table.addCell(pdfservicecell);
					
				}
				
				PdfPCell pdfservicecell1=null;
				
				PdfPTable other2table=new PdfPTable(1);
				other2table.setWidthPercentage(100);
				for(int j=0;j<myList22.size();j++){
					chunk = new Phrase(myList22.get(j),font8);
					 pdfservicecell1 = new PdfPCell(chunk);
					pdfservicecell1.setBorder(0);
					pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table.addCell(pdfservicecell1);
					
				}
				
				 
				   PdfPCell pdfservice=null;
					
					other1table.setWidthPercentage(100);
					for(int j=0;j<myList33.size();j++){
						chunk = new Phrase(myList33.get(j),font8);
						pdfservice = new PdfPCell(chunk);
						pdfservice.setBorder(0);
						pdfservice.setHorizontalAlignment(Element.ALIGN_LEFT);
						other1table.addCell(pdfservice);
						
					}
					
					PdfPCell pdfservic1=null;
					
					other2table.setWidthPercentage(100);
					for(int j=0;j<myList44.size();j++){
						chunk = new Phrase(myList44.get(j),font8);
						pdfservic1 = new PdfPCell(chunk);
						pdfservic1.setBorder(0);
						pdfservic1.setHorizontalAlignment(Element.ALIGN_RIGHT);
						other2table.addCell(pdfservic1);
						
					}
				
					 
					   PdfPCell pdfservice1=null;
						
						other1table.setWidthPercentage(100);
						for(int j=0;j<myList55.size();j++){
							chunk = new Phrase(myList55.get(j),font8);
							pdfservice1 = new PdfPCell(chunk);
							pdfservice1.setBorder(0);
							pdfservice1.setHorizontalAlignment(Element.ALIGN_LEFT);
							other1table.addCell(pdfservice1);
							
						}
						
						PdfPCell pdfservic2=null;
						
						other2table.setWidthPercentage(100);
						for(int j=0;j<myList66.size();j++){
							chunk = new Phrase(myList66.get(j),font8);
							pdfservic2 = new PdfPCell(chunk);
							pdfservic2.setBorder(0);
							pdfservic2.setHorizontalAlignment(Element.ALIGN_RIGHT);
							other2table.addCell(pdfservic2);
							
						}
				
				PdfPCell othercell = new PdfPCell();
				othercell.addElement(other1table);
				othercell.setBorder(0);
				chargetaxtable.addCell(othercell);
				
				PdfPCell othercell1 = new PdfPCell();
				othercell1.addElement(other2table);
				othercell1.setBorder(0);
				chargetaxtable.addCell(othercell1);
			   
			   
		//*****************		
//				
//				   Phrase contAmtNmae=new Phrase("Contract Amount",font8);
//				   
//				   
//				   double contAmt = 0;
//				   for(int j=1;j<myList2.size();j++){
//					   
//					   double val1=Double.parseDouble(myList2.get(0));
//					   logger.log(Level.SEVERE,"Val One"+val1);
//					   double val2=Double.parseDouble(myList2.get(j));
//					   logger.log(Level.SEVERE,"Val Two"+val2);
////					   contAmt=Double.parseDouble(myList2.get(0))+Double.parseDouble(myList2.get(j));
//					   contAmt=val1+val2;
//				   }
//				   
//				 double  contAmt1=contAmt+totalAmt;
//				   
//				   
//				Phrase contAmtValue=new Phrase(df.format(contAmt1),font8);
	//
//				PdfPCell conamtcell=new PdfPCell(contAmtNmae);
//				conamtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				conamtcell.setBorder(0);
//				
//				PdfPCell conamtcell1=new PdfPCell(contAmtValue);
//				conamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				conamtcell1.setBorder(0);
//				
//				
//				chargetaxtable.addCell(conamtcell);  
//				chargetaxtable.addCell(conamtcell1);  
				   
				
			   

			      PdfPCell chargecell = null;
			      PdfPCell chargeamtcell=null;
			      PdfPCell otherchargecell=null;
			   
			    for(int i=0;i<this.contractChargesLis.size();i++)
			      {
			   	   Phrase chunk = null;
			   	Phrase chunk1 = new Phrase("",font8);
			   	   double chargeAmt=0;
			   	   PdfPCell pdfchargeamtcell = null;
			   	   PdfPCell pdfchargecell = null;
			   	   if(contractChargesLis.get(i).getChargePercent()!=0){
			   		   chunk = new Phrase(contractChargesLis.get(i).getChargeName()+" @ "+contractChargesLis.get(i).getChargePercent(),font1);
			   		   pdfchargecell=new PdfPCell(chunk);
			   		   chargeAmt=contractChargesLis.get(i).getChargePercent()*contractChargesLis.get(i).getChargeAbsValue()/100;
			   		   chunk=new Phrase(df.format(chargeAmt),font8);
			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  	      	   pdfchargeamtcell.setBorder(0);
			   	   }
//			   	   else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   	   }
			   	   if(contractChargesLis.get(i).getChargeAbsValue()!=0){
			   		   chunk = new Phrase(contractChargesLis.get(i).getChargeName()+"",font1);
			   		   pdfchargecell=new PdfPCell(chunk);
			   		   chargeAmt=contractChargesLis.get(i).getChargeAbsValue();
			   		   chunk=new Phrase(chargeAmt+"",font8);
			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
			  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  	      	   pdfchargeamtcell.setBorder(0);
			   	   }
			   	   
			   	 total=total+chargeAmt;
//			   	   else{
//			   		pdfchargecell=new PdfPCell(chunk1);
//			   		pdfchargeamtcell = new PdfPCell(chunk1);
//			   	   }
			 	       pdfchargecell.setBorder(0);
			 	      pdfchargeamtcell.setBorder(0);
			 	      
			 	     if(total!=0){
				   		   chunk = new Phrase("Other charges total",font1);
				   		   chargecell=new PdfPCell(chunk);
				   		   chargecell.setBorder(0);
				   		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				   		  
				   		chunk=new Phrase(df.format(total),font8);
				  	      	   chargeamtcell = new PdfPCell(chunk);
				  	      	chargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				  	      	   chargeamtcell.setBorder(0);
				   	   }
				     
				     chunk = new Phrase("Refer other charge details",font1);
				     otherchargecell=new PdfPCell(chunk);
				     otherchargecell.setBorder(0);
				     otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			      }
			      chargetaxtable.addCell(chargecell);
			      chargetaxtable.addCell(chargeamtcell);
			      chargetaxtable.addCell(otherchargecell);
			 	      
			   
		   }
		   else if(cnt<= breakpoint)
		   {
			   
			   for(int i=0;i<this.contractTaxesLis.size();i++)
			      {
			    	  Phrase chunk = null;
				   	   double taxAmt=0;
				   	double taxAmt1=0;
				   	double taxAmt2=0;
				   	double taxAmt3=0;
					double taxAmt4=0;
				   	   PdfPCell pdfservicecell = null;
				   	   PdfPCell pdftaxamtcell = null;
				   	 PdfPCell pdftaxamt1cell = null;
				   	 PdfPCell pdftaxamt2cell = null;
				   	 PdfPCell pdftaxamt3cell = null;
			    	  
			    	  if(contractTaxesLis.get(i).getChargePercent()!=0){
			    		  
			    		  
			    		  if((contractTaxesLis.get(i).getChargeName().equals("VAT")))
					  	      	
			    		  {  
//				    	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//				         pdfservicecell = new PdfPCell(chunk);
//				        pdfservicecell.setBorder(0);
//				        chargetaxtable.addCell(pdfservicecell);
//			    	  
//				        
//				        chunk=new Phrase(df.format(taxAmt),font1);
//				        pdftaxamtcell = new PdfPCell(chunk);
//				      	pdftaxamtcell.setBorder(0);
//				      	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				      	chargetaxtable.addCell(pdftaxamtcell);
			    			  
			    			  
			    			String name=contractTaxesLis.get(i).getChargeName()+" @ "+contractTaxesLis.get(i).getChargePercent();
			    			taxAmt=contractTaxesLis.get(i).getChargePercent()*contractTaxesLis.get(i).getAssessableAmount()/100;
			    			myList11.add(name);
			    			myList22.add(df.format(taxAmt));
			    			  
			    			  
			    		  }
			    		  
			    		  if((contractTaxesLis.get(i).getChargeName().equals("Service Tax"))&&(contractTaxesLis.get(i).getChargePercent()!=12.36))
					  	      	
			    		  {  
//				    	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//				         pdfservicecell = new PdfPCell(chunk);
//				        pdfservicecell.setBorder(0);
//				        chargetaxtable.addCell(pdfservicecell);
//			    	  
//				        taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
//				        chunk=new Phrase(df.format(taxAmt),font1);
//				        pdftaxamtcell = new PdfPCell(chunk);
//				      	pdftaxamtcell.setBorder(0);
//				      	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				      	chargetaxtable.addCell(pdftaxamtcell);
			    		  
			    		  String name=contractTaxesLis.get(i).getChargeName()+" @ "+contractTaxesLis.get(i).getChargePercent();
			    		  taxAmt1=contractTaxesLis.get(i).getChargePercent()*contractTaxesLis.get(i).getAssessableAmount()/100;
			    		  
			    		  myList11.add(name);
			    		  myList22.add(df.format(taxAmt1));
			    		  
			    		  
			    		  }
			    		  
			    		  
			    		  if((contractTaxesLis.get(i).getChargeName().equals("Service Tax"))&&(contractTaxesLis.get(i).getChargePercent()==12.36))
			  	      	{
			  	      		
//			  	      		Phrase chunk1 = new Phrase("Service Tax "+"@ 12.00",font1);
//			  	      		PdfPCell pdfservcell = new PdfPCell(chunk1);
//			  	      		pdfservcell.setBorder(0);
//			  	      	pdfservcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			  	       	chargetaxtable.addCell(pdfservcell);
//			  	       	
//			  	      		
//			  	      	taxAmt1=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
//			  	        chunk=new Phrase(df.format(taxAmt1),font1);
//				        pdftaxamt1cell = new PdfPCell(chunk);
//				        pdftaxamt1cell.setBorder(0);
//				        pdftaxamt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				      	chargetaxtable.addCell(pdftaxamt1cell);
//			  	      		
//			  	      		
//			  	      		Phrase chunk2 = new Phrase("Education Cess "+"@ 2.00",font1);
//			  	      		PdfPCell pdfEducesscell = new PdfPCell(chunk2);
//			  	      		pdfEducesscell.setBorder(0);
//			  	      	pdfEducesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			  	       	chargetaxtable.addCell(pdfEducesscell);
//			  	      	
//			  	      	taxAmt2=(taxAmt1*2.00)/100;
//			  	        chunk=new Phrase(df.format(taxAmt2),font1);
//				        pdftaxamt2cell = new PdfPCell(chunk);
//				        pdftaxamt2cell.setBorder(0);
//				        pdftaxamt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				      	chargetaxtable.addCell(pdftaxamt2cell);	
//			  	      		
//			  	      		
//			  	      		Phrase chunk3 = new Phrase("High Education Cess "+"@ 1.00",font1);
//			  	      		PdfPCell pdfHighcesscell = new PdfPCell(chunk3);
//			  	      		pdfHighcesscell.setBorder(0);
//			  	      	pdfHighcesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			  	       	chargetaxtable.addCell(pdfHighcesscell);
//			  	      		
//			  	      	taxAmt3=(taxAmt1*1.00)/100;
//			  	        chunk=new Phrase(df.format(taxAmt3),font1);
//				        pdftaxamt3cell = new PdfPCell(chunk);
//				        pdftaxamt3cell.setBorder(0);
//				        pdftaxamt3cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				      	chargetaxtable.addCell(pdftaxamt3cell);		
			  	      		
			    			  String space=" ";
			    		String name="Service Tax "+"@ 12.00";	  
			    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
			    		String name1="Education Cess "+"@ 2.00";
			    		taxAmt3=(taxAmt2*2.00)/100;
			    		String name2="High Education Cess "+"@ 1.00";
			    		taxAmt4=(taxAmt2*1.00)/100;
			    			  
			    		myList11.add(name);
			    		myList11.add(name1);
			    		myList11.add(name2);
			    		
			    		myList22.add(df.format(taxAmt2));
			    		myList22.add(df.format(taxAmt3));
			    		myList22.add(space);
			    		myList22.add(df.format(taxAmt4));
			    		
			  	      	}
			    	  
			    	
			    	  }
			     	}
			   
			   PdfPCell pdfservicecell=null;
				
				PdfPTable other1table=new PdfPTable(1);
				other1table.setWidthPercentage(100);
				for(int j=0;j<myList11.size();j++){
					chunk = new Phrase(myList11.get(j),font8);
					 pdfservicecell = new PdfPCell(chunk);
					pdfservicecell.setBorder(0);
					pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table.addCell(pdfservicecell);
					
				}
				
				PdfPCell pdfservicecell1=null;
				
				PdfPTable other2table=new PdfPTable(1);
				other2table.setWidthPercentage(100);
				for(int j=0;j<myList22.size();j++){
					chunk = new Phrase(myList22.get(j),font8);
					 pdfservicecell1 = new PdfPCell(chunk);
					pdfservicecell1.setBorder(0);
					pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table.addCell(pdfservicecell1);
					
				}
				
				PdfPCell othercell = new PdfPCell();
				othercell.addElement(other1table);
				othercell.setBorder(0);
				chargetaxtable.addCell(othercell);
				
				PdfPCell othercell1 = new PdfPCell();
				othercell1.addElement(other2table);
				othercell1.setBorder(0);
				chargetaxtable.addCell(othercell1);
			   
			   ///
				
				

				
				/**
				 * 
				 */
				/**
				 * 
				 */
				/**
				 * 
				 */
				
//				Phrase contAmtNmae=new Phrase("Contract Amount",font8); 
//				   
//				   Double contAmt = null;
//				   for(int j=1;j<myList2.size();j++){
//					   double val1=Double.parseDouble(myList2.get(0));
//					   logger.log(Level.SEVERE,"Val One"+val1);
//					   double val2=Double.parseDouble(myList2.get(j));
//					   logger.log(Level.SEVERE,"Val Two"+val2);
////					   contAmt=Double.parseDouble(myList2.get(0))+Double.parseDouble(myList2.get(j));
//					   contAmt=val1+val2;
//				   }
//				   
//				   contAmt=contAmt+totalAmt;
//				   
//				   
//				Phrase contAmtValue=new Phrase(df.format(contAmt),font8);
	//
//				PdfPCell conamtcell=new PdfPCell(contAmtNmae);
//				conamtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				conamtcell.setBorder(0);
//				
//				PdfPCell conamtcell1=new PdfPCell(contAmtValue);
//				conamtcell1.setBorder(0);
//				conamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				
//				chargetaxtable.addCell(conamtcell);  
//				chargetaxtable.addCell(conamtcell1);  
				   
			   ///
			   
//				double pay = 0;
//				if(this.arrPayTerms.size()!=0){
//					
//					for(int i=0;i<this.arrPayTerms.size();i++){
//						
//						 pay=arrPayTerms.get(i).getPayTermPercent();
//					
//					}
//				}
//				
//				
//				
//				double amt=(contAmt*pay)/100;
//				
//				Phrase name=new Phrase("Amount",font8);
//			   PdfPCell namecell=new PdfPCell(name);
//			   namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			   namecell.setBorder(0);
//			   
//			   Phrase amout=new Phrase(df.format(amt),font8);
//			   PdfPCell namecell1=new PdfPCell(amout);
//			   namecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			   namecell1.setBorder(0);
//			   
//			   chargetaxtable.addCell(namecell);  
//			   chargetaxtable.addCell(namecell1);  
			   
			   System.out.println("rrrrrrrrrrrrrrrrrrrrrrrrr");
			   System.out.println("contract other charges list size===="+this.contractChargesLis.size());
			    for(int i=0;i<this.contractChargesLis.size();i++)
			      {
			   	   Phrase chunk = null;
			   	Phrase chunk1 = new Phrase("",font8);
			   	   double chargeAmt=0;
			   	   PdfPCell pdfchargeamtcell = null;
			   	   PdfPCell pdfchargecell = null;
			   	   if(contractChargesLis.get(i).getChargePercent()!=0){
			   		   
			   		   chunk = new Phrase(contractChargesLis.get(i).getChargeName()+" @ "+contractChargesLis.get(i).getChargePercent(),font1);
			   		   pdfchargecell=new PdfPCell(chunk);
			   		   chargeAmt=contractChargesLis.get(i).getChargePercent()*contractChargesLis.get(i).getAssessableAmount()/100;
			   		   chunk=new Phrase(df.format(chargeAmt),font8);
			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  	      	   pdfchargeamtcell.setBorder(0);
			  	      	   
			  	      	 pdfchargecell.setBorder(0);
				 	      pdfchargeamtcell.setBorder(0);
				 	       chargetaxtable.addCell(pdfchargecell);
				 	       chargetaxtable.addCell(pdfchargeamtcell);
			   	  
			   	   }else{
			   		   
			   		pdfchargecell=new PdfPCell(chunk1);
			   		pdfchargeamtcell = new PdfPCell(chunk1);
			   	 pdfchargecell.setBorder(0);
		 	      pdfchargeamtcell.setBorder(0);
		 	       chargetaxtable.addCell(pdfchargecell);
		 	       chargetaxtable.addCell(pdfchargeamtcell);
			   	   
			   	   }
			   	 
			   	 
		 	       
			   	   if(contractChargesLis.get(i).getChargeAbsValue()!=0){
			   		   chunk = new Phrase(contractChargesLis.get(i).getChargeName()+"",font1);
			   		   pdfchargecell=new PdfPCell(chunk);
			   		   chargeAmt=contractChargesLis.get(i).getChargeAbsValue();
			   		   chunk=new Phrase(chargeAmt+"",font8);
			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
			  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			  	      	   pdfchargeamtcell.setBorder(0);
			  	      	 pdfchargecell.setBorder(0);
				 	      pdfchargeamtcell.setBorder(0);
				 	       chargetaxtable.addCell(pdfchargecell);
				 	       chargetaxtable.addCell(pdfchargeamtcell);
			   	   }else{
			   		pdfchargecell=new PdfPCell(chunk1);
			   		pdfchargeamtcell = new PdfPCell(chunk1);
			   	 pdfchargecell.setBorder(0);
		 	      pdfchargeamtcell.setBorder(0);
		 	       chargetaxtable.addCell(pdfchargecell);
		 	       chargetaxtable.addCell(pdfchargeamtcell);
			   	   }
			 	      
			      }
			   
		   }
		
		 
			Phrase netname = new Phrase("Grand Total",font8);
			PdfPCell netnamecell = new PdfPCell(netname);
			netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			namephasevaluecell.addElement(namePhasevalue);
			netnamecell.setBorder(0);
			chargetaxtable.addCell(netnamecell);
		 	
		 
		 	double netpayable=0;
		 	netpayable = con.getNetpayable();
			String netpayableamt=netpayable+"";
			System.out.println("total============"+netpayableamt);
			Phrase netpay = new Phrase(netpayableamt,font8);
			PdfPCell netpaycell = new PdfPCell(netpay);
			netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			namephasevaluecell.addElement(namePhasevalue);
			netpaycell.setBorder(0);
			chargetaxtable.addCell(netpaycell);
			
			
			Phrase blankSpace = new Phrase(" ",font8);
			PdfPCell blankSpacecell = new PdfPCell(blankSpace);
			blankSpacecell.setBorder(0);
			chargetaxtable.addCell(blankSpacecell);
			chargetaxtable.addCell(blankSpacecell);
			
			
			
		 
		 double pay=0;
		 for(int i=0;i<payTerms.size();i++){
				
			 pay=payTerms.get(i).getPayTermPercent();
		 }
		 
		 
			//*******************************88888 
			 if(pay == 100)
			 {
				 
				 System.out.println("in side roha bhagde ................");
				 
				 for(int k=0;k<this.conbillingTaxesLis.size();k++)
			     {
			   	  
			   	  for(int i=0;i<this.conbillingTaxesLis.get(k).getChargesList().size();i++)
			   	  {
			   		if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==14.5){
						
						 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
						
						System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = "Service Tax "+ " @ "+" 14%";
						double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						String blidnk=" ";
						
						Phrase strp = new Phrase(str,font8);
						PdfPCell strcell =new PdfPCell(strp);
						strcell.setBorder(0);
						chargetaxtable.addCell(strcell);
						
						
						Phrase taxAmt1p = new Phrase(taxAmt1+"",font8);
						PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
						taxAmt1cell.setBorder(0);
						chargetaxtable.addCell(taxAmt1cell);
						
//						myList33.add(str);
//						 myList44.add(df.format(taxAmt1));
//						 myList44.add(blidnk);
						 
						 
						 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
						 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						 
						 	Phrase str123p = new Phrase(str123,font8);
							PdfPCell str123cell =new PdfPCell(str123p);
							str123cell.setBorder(0);
							chargetaxtable.addCell(str123cell);
							
							
							Phrase taxAmtp = new Phrase(taxAmt+"",font8);
							PdfPCell taxAmtcell =new PdfPCell(taxAmtp);
							taxAmtcell.setBorder(0);
							chargetaxtable.addCell(taxAmtcell);
						 
						 
//						 myList33.add(str123);
//						 myList44.add(df.format(taxAmt));
//						 myList44.add(blidnk);
//						 System.out.println("Size of mylist2 is () === "+myList2.size());
					}
			   		
			   		
			   		
			   		if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==15){	
						

						
						 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
						
						System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
						
						String str = "Service Tax "+ " @ "+" 14%";
						double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						String blidnk=" ";
						
						Phrase strp = new Phrase(str,font8);
						PdfPCell strcell =new PdfPCell(strp);
						strcell.setBorder(0);
						strcell.setHorizontalAlignment(Element.ALIGN_LEFT);
						chargetaxtable.addCell(strcell);
						
						
						Phrase taxAmt1p = new Phrase(df.format(taxAmt1),font8);
						PdfPCell taxAmt1cell =new PdfPCell(taxAmt1p);
						taxAmt1cell.setBorder(0);
						taxAmt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
						chargetaxtable.addCell(taxAmt1cell);
						
//						myList33.add(str);
//						 myList44.add(df.format(taxAmt1));
//						 myList44.add(blidnk);
						 
						 
						 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
						 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
						 
						 	Phrase str123p = new Phrase(str123,font8);
							PdfPCell str123cell =new PdfPCell(str123p);
							str123cell.setBorder(0);
							str123cell.setHorizontalAlignment(Element.ALIGN_LEFT);
							chargetaxtable.addCell(str123cell);
							
							
							Phrase taxAmtp = new Phrase(df.format(taxAmt),font8);
							PdfPCell taxAmtcell =new PdfPCell(taxAmtp);
							taxAmtcell.setBorder(0);
							taxAmtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
							chargetaxtable.addCell(taxAmtcell);
						 
						 
							 String kkc = "Krishi Kalyan Cess"+ " @ "+" 0.5%";
							 double kkctaxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
							 
							 	Phrase kkcPhrase = new Phrase(kkc,font8);
								PdfPCell kkcCell =new PdfPCell(kkcPhrase);
								kkcCell.setBorder(0);
								kkcCell.setHorizontalAlignment(Element.ALIGN_LEFT);
								chargetaxtable.addCell(str123cell);
								
								
								Phrase taxAmtkkc = new Phrase(df.format(kkctaxAmt),font8);
								PdfPCell taxAmtkkcCell =new PdfPCell(taxAmtkkc);
								taxAmtkkcCell.setBorder(0);
								taxAmtkkcCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
								chargetaxtable.addCell(taxAmtkkcCell);	
							
							
//						 myList33.add(str123);
//						 myList44.add(df.format(taxAmt));
//						 myList44.add(blidnk);
//					
					}
			   		
			   	  }
			   }
			 }
			 //********************************
		 
		 
		 
		
		 int cutAmt=contractChargesLis.size()+contractTaxesLis.size();
		 System.out.println("cutAmt value ===="+cutAmt);
		 if(cutAmt<=4){
			 
			
		 if(pay != 100){
		
//***************************changes ends here ******************************************			
		
		//**********************rohan changes here on 26/5/15 for part payments********************************		
		System.out.println("billEntity.getArrPayTerms().size90==========="+invoiceentity.getArrayBillingDocument().size());
		
		System.out.println("contract count========="+invoiceentity.getContractCount());
		System.out.println("order type======"+invoiceentity.getTypeOfOrder().trim());
		
		
		
		if(invoiceentity.getArrayBillingDocument().size()>1)
		{
			for(int i=0;i<payTerms.size();i++){
				System.out.println("pay term size ======"+payTerms.size());
			System.out.println("Pay Percent "+payTerms.get(i).getPayTermPercent());
			Phrase namePhase = new Phrase("Pay Terms "+payTerms.get(i).getPayTermPercent()+"%",font8);
			PdfPCell namephasecell = new PdfPCell(namePhase);
			namephasecell.addElement(namePhase);
			namephasecell.setBorder(0);
			chargetaxtable.addCell(namephasecell);
			total = ((con.getTotalAmount()*payTerms.get(i).getPayTermPercent())/100);
			String amt=total+"";
			System.out.println("total============"+amt);
			Phrase namePhasevalue = new Phrase(amt,font8);
			PdfPCell namephasevaluecell = new PdfPCell(namePhasevalue);
			namephasevaluecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			namephasevaluecell.addElement(namePhasevalue);
			namephasevaluecell.setBorder(0);
			chargetaxtable.addCell(namephasevaluecell);
			
			}
		}
		else
		{
			System.out.println("indise else condition");
			
		Phrase namePhase = new Phrase("Pay Terms "+invoiceentity.getArrPayTerms().get(0).getPayTermPercent()+"%",font8);
		PdfPCell namephasecell = new PdfPCell(namePhase);
		namephasecell.addElement(namePhase);
		namephasecell.setBorder(0);
		chargetaxtable.addCell(namephasecell);
		double total =0;
		total = ((con.getTotalAmount()*invoiceentity.getArrPayTerms().get(0).getPayTermPercent())/100);
		
		Phrase totalphase = new Phrase(total+"",font8);
		PdfPCell totalcell = new PdfPCell(totalphase);
		totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalcell.setBorder(0);
		chargetaxtable.addCell(totalcell);
		
		}			
//*******************************changes endes here******************************			
		
		
		
if(cnt>breakpoint){
   
   for(int i=0;i<this.billingTaxesLis.size();i++)
      {
	   	   double taxAmt=0;
	   	double taxAmt1=0;
	   	double taxAmt2=0;
	   	double taxAmt3=0;
		double taxAmt4=0;
    	  
    	  if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
    		  
    		  
    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("VAT")))
		  	      	
    		  {  
    			  
    			  
    			String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
    			taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
    			myList1.add(name);
    			myList2.add(df.format(taxAmt));
    			  
    			  
    		  }
    			if(billingTaxesLis.get(i).getTaxChargeName().equalsIgnoreCase("Service Tax")&& billingTaxesLis.get(i).getTaxChargePercent()==15){	
					
					
					 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+billingTaxesLis.get(i).getTaxChargeName());
					
					System.out.println("2nd loop == "+billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent());
					
					String str = "Service Tax "+ " @ "+" 14%";
					double sertaxAmt1 = 14* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
					String blidnk=" ";
					
					 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
					 double sertaxAmt = 0.5* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
					 
						 String kkc = "Krishi Kalyan Cess"+ " @ "+" 0.5%";
						 double kkctaxAmt = 0.5* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
						 
							myList5.add(str);
				    		myList5.add(str123);
				    		myList5.add(kkc); 
				    		
				    		myList6.add(df.format(sertaxAmt1));
				    		myList6.add(df.format(sertaxAmt));
				    		myList6.add(blidnk);
				    		myList6.add(df.format(kkctaxAmt));
						
			}
    		else if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==12.36))
  	      	{
    		String name="Service Tax "+"@ 12.00";	  
    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
    		String name1="Education Cess "+"@ 2.00";
    		taxAmt3=(taxAmt2*2.00)/100;
    		String name2="High Education Cess "+"@ 1.00";
    		taxAmt4=(taxAmt2*1.00)/100;
    			 
    		String str=" ";
    		
    		myList5.add(name);
    		myList5.add(name1);
    		myList5.add(name2);
    		
    		myList6.add(df.format(taxAmt2));
    		myList6.add(df.format(taxAmt3));
    		myList6.add(str);
    		myList6.add(df.format(taxAmt4));
    		
  	      	}
    		
    	else if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax")))
  		  {  
  		  String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
  		  taxAmt1=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
  		  
  		  myList3.add(name);
  		  myList4.add(df.format(taxAmt1));
  		  
  		  }
    		
    	  }
     	}
   
   
   PdfPCell pdfservicecell=null;
	
	PdfPTable other1table=new PdfPTable(1);
	other1table.setWidthPercentage(100);
	for(int j=0;j<myList1.size();j++){
		chunk = new Phrase(myList1.get(j),font8);
		 pdfservicecell = new PdfPCell(chunk);
		pdfservicecell.setBorder(0);
		pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		other1table.addCell(pdfservicecell);
		
	}
	
	PdfPCell pdfservicecell1=null;
	
	PdfPTable other2table=new PdfPTable(1);
	other2table.setWidthPercentage(100);
	for(int j=0;j<myList2.size();j++){
		chunk = new Phrase(myList2.get(j),font8);
		 pdfservicecell1 = new PdfPCell(chunk);
		pdfservicecell1.setBorder(0);
		pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		other2table.addCell(pdfservicecell1);
		
	}
	
	 
	   PdfPCell pdfservice=null;
		
		other1table.setWidthPercentage(100);
		for(int j=0;j<myList3.size();j++){
			chunk = new Phrase(myList3.get(j),font8);
			pdfservice = new PdfPCell(chunk);
			pdfservice.setBorder(0);
			pdfservice.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(pdfservice);
			
		}
		
		PdfPCell pdfservic1=null;
		
		other2table.setWidthPercentage(100);
		for(int j=0;j<myList4.size();j++){
			chunk = new Phrase(myList4.get(j),font8);
			pdfservic1 = new PdfPCell(chunk);
			pdfservic1.setBorder(0);
			pdfservic1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(pdfservic1);
			
		}
	
		 
		   PdfPCell pdfservice1=null;
			
			other1table.setWidthPercentage(100);
			for(int j=0;j<myList5.size();j++){
				chunk = new Phrase(myList5.get(j),font8);
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setBorder(0);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservice1);
				
			}
			
			PdfPCell pdfservic2=null;
			
			other2table.setWidthPercentage(100);
			for(int j=0;j<myList6.size();j++){
				chunk = new Phrase(myList6.get(j),font8);
				pdfservic2 = new PdfPCell(chunk);
				pdfservic2.setBorder(0);
				pdfservic2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservic2);
				
			}

			
			
	PdfPCell othercell = new PdfPCell();
	othercell.addElement(other1table);
	othercell.setBorder(0);
	chargetaxtable.addCell(othercell);
	
	PdfPCell othercell1 = new PdfPCell();
	othercell1.addElement(other2table);
	othercell1.setBorder(0);
	chargetaxtable.addCell(othercell1);
   

      PdfPCell chargecell = null;
      PdfPCell chargeamtcell=null;
      PdfPCell otherchargecell=null;
   System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
   System.out.println("billing charges list size==="+this.billingChargesLis.size());
    for(int i=0;i<this.billingChargesLis.size();i++)
      {
   	   Phrase chunk = null;
   	Phrase chunk1 = new Phrase("",font8);
   	   double chargeAmt=0;
   	   PdfPCell pdfchargeamtcell = null;
   	   PdfPCell pdfchargecell = null;
   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
   		   pdfchargecell=new PdfPCell(chunk);
   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
   		   chunk=new Phrase(df.format(chargeAmt),font8);
  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  	      	   pdfchargeamtcell.setBorder(0);
   	   }
//   	   else{
//   		pdfchargecell=new PdfPCell(chunk1);
//   		pdfchargeamtcell = new PdfPCell(chunk1);
//   	   }
   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
   		   pdfchargecell=new PdfPCell(chunk);
   		   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
   		   chunk=new Phrase(chargeAmt+"",font8);
  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  	      	   pdfchargeamtcell.setBorder(0);
   	   }
	     
	     chunk = new Phrase("Refer other charge details",font1);
	     otherchargecell=new PdfPCell(chunk);
	     otherchargecell.setBorder(0);
	     otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	     chargetaxtable.addCell(otherchargecell);
      }
    
   
}
else if(cnt<= breakpoint)
{
   
   for(int i=0;i<this.billingTaxesLis.size();i++)
      {
	   	double taxAmt=0;
	   	double taxAmt1=0;
	   	double taxAmt2=0;
	   	double taxAmt3=0;
		double taxAmt4=0;
    	  
    	  if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
    		  
    		  
    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("VAT")))
		  	      	
    		  {  
//	    	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//	         pdfservicecell = new PdfPCell(chunk);
//	        pdfservicecell.setBorder(0);
//	        chargetaxtable.addCell(pdfservicecell);
//    	  
//	        
//	        chunk=new Phrase(df.format(taxAmt),font1);
//	        pdftaxamtcell = new PdfPCell(chunk);
//	      	pdftaxamtcell.setBorder(0);
//	      	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	      	chargetaxtable.addCell(pdftaxamtcell);
    			  
    			  
    			String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
    			taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
    			myList1.add(name);
    			myList2.add(df.format(taxAmt));
    			  
    			  
    		  }
    		  
    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()!=12.36))
		  	      	
    		  {  
//	    	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//	         pdfservicecell = new PdfPCell(chunk);
//	        pdfservicecell.setBorder(0);
//	        chargetaxtable.addCell(pdfservicecell);
//    	  
//	        taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
//	        chunk=new Phrase(df.format(taxAmt),font1);
//	        pdftaxamtcell = new PdfPCell(chunk);
//	      	pdftaxamtcell.setBorder(0);
//	      	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	      	chargetaxtable.addCell(pdftaxamtcell);
    		  
    		  String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
    		  taxAmt1=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
    		  
    		  myList1.add(name);
    		  myList2.add(df.format(taxAmt1));
    		  
    		  
    		  }
    		  
    		  
    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==12.36))
  	      	{
  	      		
//  	      		Phrase chunk1 = new Phrase("Service Tax "+"@ 12.00",font1);
//  	      		PdfPCell pdfservcell = new PdfPCell(chunk1);
//  	      		pdfservcell.setBorder(0);
//  	      	pdfservcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//  	       	chargetaxtable.addCell(pdfservcell);
//  	       	
//  	      		
//  	      	taxAmt1=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
//  	        chunk=new Phrase(df.format(taxAmt1),font1);
//	        pdftaxamt1cell = new PdfPCell(chunk);
//	        pdftaxamt1cell.setBorder(0);
//	        pdftaxamt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	      	chargetaxtable.addCell(pdftaxamt1cell);
//  	      		
//  	      		
//  	      		Phrase chunk2 = new Phrase("Education Cess "+"@ 2.00",font1);
//  	      		PdfPCell pdfEducesscell = new PdfPCell(chunk2);
//  	      		pdfEducesscell.setBorder(0);
//  	      	pdfEducesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//  	       	chargetaxtable.addCell(pdfEducesscell);
//  	      	
//  	      	taxAmt2=(taxAmt1*2.00)/100;
//  	        chunk=new Phrase(df.format(taxAmt2),font1);
//	        pdftaxamt2cell = new PdfPCell(chunk);
//	        pdftaxamt2cell.setBorder(0);
//	        pdftaxamt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	      	chargetaxtable.addCell(pdftaxamt2cell);	
//  	      		
//  	      		
//  	      		Phrase chunk3 = new Phrase("High Education Cess "+"@ 1.00",font1);
//  	      		PdfPCell pdfHighcesscell = new PdfPCell(chunk3);
//  	      		pdfHighcesscell.setBorder(0);
//  	      	pdfHighcesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//  	       	chargetaxtable.addCell(pdfHighcesscell);
//  	      		
//  	      	taxAmt3=(taxAmt1*1.00)/100;
//  	        chunk=new Phrase(df.format(taxAmt3),font1);
//	        pdftaxamt3cell = new PdfPCell(chunk);
//	        pdftaxamt3cell.setBorder(0);
//	        pdftaxamt3cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	      	chargetaxtable.addCell(pdftaxamt3cell);		
  	      		
    			  String space=" ";
    		String name="Service Tax "+"@ 12.00";	  
    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
    		String name1="Education Cess "+"@ 2.00";
    		taxAmt3=(taxAmt2*2.00)/100;
    		String name2="High Education Cess "+"@ 1.00";
    		taxAmt4=(taxAmt2*1.00)/100;
    			  
    		myList1.add(name);
    		myList1.add(name1);
    		myList1.add(name2);
    		
    		myList2.add(df.format(taxAmt2));
    		myList2.add(df.format(taxAmt3));
    		myList2.add(space);
    		myList2.add(df.format(taxAmt4));
    		
  	      	}
    	  
    	
    	  }
     	}
   
   PdfPCell pdfservicecell=null;
	
	PdfPTable other1table=new PdfPTable(1);
	other1table.setWidthPercentage(100);
	for(int j=0;j<myList1.size();j++){
		chunk = new Phrase(myList1.get(j),font8);
		 pdfservicecell = new PdfPCell(chunk);
		pdfservicecell.setBorder(0);
		pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		other1table.addCell(pdfservicecell);
		
	}
	
	PdfPCell pdfservicecell1=null;
	
	PdfPTable other2table=new PdfPTable(1);
	other2table.setWidthPercentage(100);
	for(int j=0;j<myList2.size();j++){
		chunk = new Phrase(myList2.get(j),font8);
		 pdfservicecell1 = new PdfPCell(chunk);
		pdfservicecell1.setBorder(0);
		pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		other2table.addCell(pdfservicecell1);
		
	}
	
	PdfPCell othercell = new PdfPCell();
	othercell.addElement(other1table);
	othercell.setBorder(0);
	chargetaxtable.addCell(othercell);
	
	PdfPCell othercell1 = new PdfPCell();
	othercell1.addElement(other2table);
	othercell1.setBorder(0);
	chargetaxtable.addCell(othercell1);
   
   ///
	
	

	
	/**
	 * 
	 */
	/**
	 * 
	 */
	/**
	 * 
	 */
	
//	Phrase contAmtNmae=new Phrase("Contract Amount",font8); 
//	   
//	   Double contAmt = null;
//	   for(int j=1;j<myList2.size();j++){
//		   double val1=Double.parseDouble(myList2.get(0));
//		   logger.log(Level.SEVERE,"Val One"+val1);
//		   double val2=Double.parseDouble(myList2.get(j));
//		   logger.log(Level.SEVERE,"Val Two"+val2);
////		   contAmt=Double.parseDouble(myList2.get(0))+Double.parseDouble(myList2.get(j));
//		   contAmt=val1+val2;
//	   }
//	   
//	   contAmt=contAmt+totalAmt;
//	   
//	   
//	Phrase contAmtValue=new Phrase(df.format(contAmt),font8);
//
//	PdfPCell conamtcell=new PdfPCell(contAmtNmae);
//	conamtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	conamtcell.setBorder(0);
//	
//	PdfPCell conamtcell1=new PdfPCell(contAmtValue);
//	conamtcell1.setBorder(0);
//	conamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	
//	chargetaxtable.addCell(conamtcell);  
//	chargetaxtable.addCell(conamtcell1);  
	   
   ///
   
//	double pay = 0;
//	if(this.arrPayTerms.size()!=0){
//		
//		for(int i=0;i<this.arrPayTerms.size();i++){
//			
//			 pay=arrPayTerms.get(i).getPayTermPercent();
//		
//		}
//	}
//	
//	
//	
//	double amt=(contAmt*pay)/100;
//	
//	Phrase name=new Phrase("Amount",font8);
//   PdfPCell namecell=new PdfPCell(name);
//   namecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//   namecell.setBorder(0);
//   
//   Phrase amout=new Phrase(df.format(amt),font8);
//   PdfPCell namecell1=new PdfPCell(amout);
//   namecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//   namecell1.setBorder(0);
//   
//   chargetaxtable.addCell(namecell);  
//   chargetaxtable.addCell(namecell1);  
   
   
   
    for(int i=0;i<this.billingChargesLis.size();i++)
      {
   	   Phrase chunk = null;
   	Phrase chunk1 = new Phrase("",font8);
   	   double chargeAmt=0;
   	   PdfPCell pdfchargeamtcell = null;
   	   PdfPCell pdfchargecell = null;
   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
   		   pdfchargecell=new PdfPCell(chunk);
//   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
   		 chargeAmt=billingChargesLis.get(i).getPayableAmt();
   		   chunk=new Phrase(df.format(chargeAmt),font8);
  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  	      	   pdfchargeamtcell.setBorder(0);
  	      	 pdfchargecell.setBorder(0);
	 	      pdfchargeamtcell.setBorder(0);
	 	       chargetaxtable.addCell(pdfchargecell);
	 	       chargetaxtable.addCell(pdfchargeamtcell);
   	   }else{
   		pdfchargecell=new PdfPCell(chunk1);
   		pdfchargeamtcell = new PdfPCell(chunk1);
   	 pdfchargecell.setBorder(0);
      pdfchargeamtcell.setBorder(0);
       chargetaxtable.addCell(pdfchargecell);
       chargetaxtable.addCell(pdfchargeamtcell);
   	   }
   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
   		   pdfchargecell=new PdfPCell(chunk);
//   		   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
   		chargeAmt=billingChargesLis.get(i).getPayableAmt();
   		   chunk=new Phrase(chargeAmt+"",font8);
  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  	      	   pdfchargeamtcell.setBorder(0);
  	      	 pdfchargecell.setBorder(0);
	 	      pdfchargeamtcell.setBorder(0);
	 	       chargetaxtable.addCell(pdfchargecell);
	 	       chargetaxtable.addCell(pdfchargeamtcell);
   	   }else{
   		pdfchargecell=new PdfPCell(chunk1);
   		pdfchargeamtcell = new PdfPCell(chunk1);
   	 pdfchargecell.setBorder(0);
      pdfchargeamtcell.setBorder(0);
       chargetaxtable.addCell(pdfchargecell);
       chargetaxtable.addCell(pdfchargeamtcell);
   	   }
 	      
      }
   
}

//for(int i=0;i<this.billingTaxesLis.size();i++)
//{
//Phrase chunk = null;
//	   double taxAmt=0;
//	double taxAmt1=0;
//	double taxAmt2=0;
//	double taxAmt3=0;
//	   PdfPCell pdfservicecell = null;
//	   PdfPCell pdftaxamtcell = null;
//	 PdfPCell pdftaxamt1cell = null;
//	 PdfPCell pdftaxamt2cell = null;
//	 PdfPCell pdftaxamt3cell = null;
//
//if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
//  
//  
//  if((billingTaxesLis.get(i).getTaxChargeName().equals("VAT")))
//  	      	
//  {  
//	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//   pdfservicecell = new PdfPCell(chunk);
//  pdfservicecell.setBorder(0);
//  chargetaxtable.addCell(pdfservicecell);
//
//  taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
//  chunk=new Phrase(df.format(taxAmt),font1);
//  pdftaxamtcell = new PdfPCell(chunk);
//	pdftaxamtcell.setBorder(0);
//	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	chargetaxtable.addCell(pdftaxamtcell);
//  }
//  
//  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()!=12.36))
//  	      	
//  {  
//	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//   pdfservicecell = new PdfPCell(chunk);
//  pdfservicecell.setBorder(0);
//  chargetaxtable.addCell(pdfservicecell);
//
//  taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
//  chunk=new Phrase(df.format(taxAmt),font1);
//  pdftaxamtcell = new PdfPCell(chunk);
//	pdftaxamtcell.setBorder(0);
//	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	chargetaxtable.addCell(pdftaxamtcell);
//  }
//  
//  
//  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==12.36))
//	{
//		
//		Phrase chunk1 = new Phrase("Service Tax "+"@ 12.00",font1);
//		PdfPCell pdfservcell = new PdfPCell(chunk1);
//		pdfservcell.setBorder(0);
//	pdfservcell.setHorizontalAlignment(Element.ALIGN_LEFT);
// 	chargetaxtable.addCell(pdfservcell);
// 	
//		
//	taxAmt1=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
//  chunk=new Phrase(df.format(taxAmt1),font1);
//  pdftaxamt1cell = new PdfPCell(chunk);
//  pdftaxamt1cell.setBorder(0);
//  pdftaxamt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	chargetaxtable.addCell(pdftaxamt1cell);
//		
//		
//		Phrase chunk2 = new Phrase("Education Cess "+"@ 2.00",font1);
//		PdfPCell pdfEducesscell = new PdfPCell(chunk2);
//		pdfEducesscell.setBorder(0);
//	pdfEducesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
// 	chargetaxtable.addCell(pdfEducesscell);
//	
//	taxAmt2=(taxAmt1*2.00)/100;
//  chunk=new Phrase(df.format(taxAmt2),font1);
//  pdftaxamt2cell = new PdfPCell(chunk);
//  pdftaxamt2cell.setBorder(0);
//  pdftaxamt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	chargetaxtable.addCell(pdftaxamt2cell);	
//		
//		
//		Phrase chunk3 = new Phrase("High Education Cess "+"@ 1.00",font1);
//		PdfPCell pdfHighcesscell = new PdfPCell(chunk3);
//		pdfHighcesscell.setBorder(0);
//	pdfHighcesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
// 	chargetaxtable.addCell(pdfHighcesscell);
//		
//	taxAmt3=(taxAmt1*1.00)/100;
//  chunk=new Phrase(df.format(taxAmt3),font1);
//  pdftaxamt3cell = new PdfPCell(chunk);
//  pdftaxamt3cell.setBorder(0);
//  pdftaxamt3cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	chargetaxtable.addCell(pdftaxamt3cell);		
//		
//	}
//
//
//}
//}
		 }
		 }
		 
		 else{
			 
			 if(pay != 100){
				   
				    chunk=new Phrase("Refer Annexure for more details",font1);
			        PdfPCell annexure = new PdfPCell(chunk);
			        annexure.setBorder(0);
			        annexure.setHorizontalAlignment(Element.ALIGN_LEFT);
			      	chargetaxtable.addCell(annexure);
				 
				 
				 
				 remainigInfotable = new PdfPTable(1);
				 remainigInfotable.setWidthPercentage(100);
//***************************changes ends here ******************************************			
			
			//**********************rohan changes here on 26/5/15 for part payments********************************		
			System.out.println("billEntity.getArrPayTerms().size90==========="+invoiceentity.getArrayBillingDocument().size());
			
			System.out.println("contract count========="+invoiceentity.getContractCount());
			System.out.println("order type======"+invoiceentity.getTypeOfOrder().trim());
			
			
			
			if(invoiceentity.getArrayBillingDocument().size()>1)
			{
//				System.out.println("indise if condition");
//				
//				for(int i=0;i<invoiceentity.getArrayBillingDocument().size();i++){
//					System.out.println("inside for loop");
//					System.out.println("billing amt========="+invoiceentity.getArrayBillingDocument().get(i).getBillAmount());
//					System.out.println("aaaaaaaaa");
//					System.out.println("billing count========="+invoiceentity.getArrayBillingDocument().get(i).getBillId());
//					
//					System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
//					BillingDocument bill =new BillingDocument();
//					if(invoiceentity.getCompanyId()!=null){
//						bill=ofy().load().type(BillingDocument.class).filter("companyId",invoiceentity.getCompanyId()).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
//					}else{
//						bill=ofy().load().type(BillingDocument.class).filter("contractCount", invoiceentity.getContractCount()).filter("count", invoiceentity.getArrayBillingDocument().get(i).getBillId()).filter("typeOfOrder", invoiceentity.getTypeOfOrder().trim()).first().now();
//					}
////					int rooh=0;
////					 rooh=(int) (rooh+bill.getArrPayTerms().get(i).getPayTermPercent());
////					
////					 
////					 System.out.println("addition of payterm %====="+rooh);
//					payTerms.addAll(bill.getArrPayTerms());
//					
//					System.out.println("for loppp ends here");
//				}
			
				for(int i=0;i<payTerms.size();i++){
					System.out.println("pay term size ======"+payTerms.size());
				System.out.println("Pay Percent "+payTerms.get(i).getPayTermPercent());
				Phrase namePhase = new Phrase("Pay Terms "+payTerms.get(i).getPayTermPercent()+"%",font8);
				PdfPCell namephasecell = new PdfPCell(namePhase);
				namephasecell.addElement(namePhase);
				namephasecell.setBorder(0);
				remainigInfotable.addCell(namephasecell);
				total = ((con.getTotalAmount()*payTerms.get(i).getPayTermPercent())/100);
				String amt=total+"";
				System.out.println("total============"+amt);
				Phrase namePhasevalue = new Phrase(amt,font8);
				PdfPCell namephasevaluecell = new PdfPCell(namePhasevalue);
				namephasevaluecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//				namephasevaluecell.addElement(namePhasevalue);
				namephasevaluecell.setBorder(0);
				remainigInfotable.addCell(namephasevaluecell);
				
				}
			}
			else
			{
				System.out.println("indise else condition");
				
			System.out.println("Pay Percent "+invoiceentity.getArrPayTerms().get(0).getPayTermPercent());
			Phrase namePhase = new Phrase("Pay Terms "+invoiceentity.getArrPayTerms().get(0).getPayTermPercent()+"%",font8);
			PdfPCell namephasecell = new PdfPCell(namePhase);
			namephasecell.addElement(namePhase);
			namephasecell.setBorder(0);
			remainigInfotable.addCell(namephasecell);
			double total =0;
			total = ((con.getTotalAmount()*invoiceentity.getArrPayTerms().get(0).getPayTermPercent())/100);
			
			Phrase totalphase = new Phrase(total+"",font8);
			PdfPCell totalcell = new PdfPCell(totalphase);
			totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			totalcell.setBorder(0);
			remainigInfotable.addCell(totalcell);
			
			}			
//*******************************changes endes here******************************			
			
			
			
 if(cnt>breakpoint){
	   
	   for(int i=0;i<this.billingTaxesLis.size();i++)
	      {
		   	   double taxAmt=0;
		   	double taxAmt1=0;
		   	double taxAmt2=0;
		   	double taxAmt3=0;
			double taxAmt4=0;
	    	  
	    	  if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
	    		  
	    		  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("VAT")))
			  	      	
	    		  {  
	    			  
	    			  
	    			String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
	    			taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
	    			myList1.add(name);
	    			myList2.add(df.format(taxAmt));
	    			  
	    			  
	    		  }
	    		  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()!=12.36))
			  	      	
	    		  {  
	    		  
	    		  String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
	    		  taxAmt1=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
	    		  
	    		  myList3.add(name);
	    		  myList4.add(df.format(taxAmt1));
	    		  
	    		  
	    		  }
	    		  
	    		  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==12.36))
	  	      	{
	    		
	    		String name="Service Tax "+"@ 12.00";	  
	    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
	    		String name1="Education Cess "+"@ 2.00";
	    		taxAmt3=(taxAmt2*2.00)/100;
	    		String name2="High Education Cess "+"@ 1.00";
	    		taxAmt4=(taxAmt2*1.00)/100;
	    			 
	    		String str=" ";
	    		
	    		myList5.add(name);
	    		myList5.add(name1);
	    		myList5.add(name2);
	    		
	    		myList6.add(df.format(taxAmt2));
	    		myList6.add(df.format(taxAmt3));
	    		myList6.add(str);
	    		myList6.add(df.format(taxAmt4));
	    		
	  	      	}
	    		  
	    		  
	    		if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==14.5))
	  	      	{
	    		
	    		String name="Service Tax "+"@ 14.00";	  
	    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*14)/100;
	    		String name1="Education Cess "+"@ 0.50";
	    		taxAmt3=(taxAmt2*0.50)/100;
//	    		String name2="High Education Cess "+"@ 1.00";
//	    		taxAmt4=(taxAmt2*1.00)/100;
	    			 
	    		String str=" ";
	    		
	    		myList5.add(name);
	    		myList5.add(name1);
//	    		myList5.add(name2);
	    		
	    		myList6.add(df.format(taxAmt2));
	    		myList6.add(df.format(taxAmt3));
	    		myList6.add(str);
//	    		myList6.add(df.format(taxAmt4));
	    		
	  	      	}
	    	  }
	     	}
	   
	   
	   PdfPCell pdfservicecell=null;
		
		PdfPTable other1table=new PdfPTable(1);
		other1table.setWidthPercentage(100);
		for(int j=0;j<myList1.size();j++){
			chunk = new Phrase(myList1.get(j),font8);
			 pdfservicecell = new PdfPCell(chunk);
			pdfservicecell.setBorder(0);
			pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(pdfservicecell);
			
		}
		
		PdfPCell pdfservicecell1=null;
		
		PdfPTable other2table=new PdfPTable(1);
		other2table.setWidthPercentage(100);
		for(int j=0;j<myList2.size();j++){
			chunk = new Phrase(myList2.get(j),font8);
			 pdfservicecell1 = new PdfPCell(chunk);
			pdfservicecell1.setBorder(0);
			pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(pdfservicecell1);
			
		}
		
		 
		   PdfPCell pdfservice=null;
			
			other1table.setWidthPercentage(100);
			for(int j=0;j<myList3.size();j++){
				chunk = new Phrase(myList3.get(j),font8);
				pdfservice = new PdfPCell(chunk);
				pdfservice.setBorder(0);
				pdfservice.setHorizontalAlignment(Element.ALIGN_LEFT);
				other1table.addCell(pdfservice);
				
			}
			
			PdfPCell pdfservic1=null;
			
			other2table.setWidthPercentage(100);
			for(int j=0;j<myList4.size();j++){
				chunk = new Phrase(myList4.get(j),font8);
				pdfservic1 = new PdfPCell(chunk);
				pdfservic1.setBorder(0);
				pdfservic1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				other2table.addCell(pdfservic1);
				
			}
		
			 
			   PdfPCell pdfservice1=null;
				
				other1table.setWidthPercentage(100);
				for(int j=0;j<myList5.size();j++){
					chunk = new Phrase(myList5.get(j),font8);
					pdfservice1 = new PdfPCell(chunk);
					pdfservice1.setBorder(0);
					pdfservice1.setHorizontalAlignment(Element.ALIGN_LEFT);
					other1table.addCell(pdfservice1);
					
				}
				
				PdfPCell pdfservic2=null;
				
				other2table.setWidthPercentage(100);
				for(int j=0;j<myList6.size();j++){
					chunk = new Phrase(myList6.get(j),font8);
					pdfservic2 = new PdfPCell(chunk);
					pdfservic2.setBorder(0);
					pdfservic2.setHorizontalAlignment(Element.ALIGN_RIGHT);
					other2table.addCell(pdfservic2);
					
				}

				
				
		PdfPCell othercell = new PdfPCell();
		othercell.addElement(other1table);
		othercell.setBorder(0);
		remainigInfotable.addCell(othercell);
		
		PdfPCell othercell1 = new PdfPCell();
		othercell1.addElement(other2table);
		othercell1.setBorder(0);
		remainigInfotable.addCell(othercell1);
	   

	      PdfPCell chargecell = null;
	      PdfPCell chargeamtcell=null;
	      PdfPCell otherchargecell=null;
	   System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	   System.out.println("billing charges list size==="+this.billingChargesLis.size());
	    for(int i=0;i<this.billingChargesLis.size();i++)
	      {
	   	   Phrase chunk = null;
	   	Phrase chunk1 = new Phrase("",font8);
	   	   double chargeAmt=0;
	   	   PdfPCell pdfchargeamtcell = null;
	   	   PdfPCell pdfchargecell = null;
	   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
	   		   pdfchargecell=new PdfPCell(chunk);
	   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
	   		   chunk=new Phrase(df.format(chargeAmt),font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  	      	   pdfchargeamtcell.setBorder(0);
	   	   }
//	   	   else{
//	   		pdfchargecell=new PdfPCell(chunk1);
//	   		pdfchargeamtcell = new PdfPCell(chunk1);
//	   	   }
	   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
	   		   pdfchargecell=new PdfPCell(chunk);
	   		   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
	   		   chunk=new Phrase(chargeAmt+"",font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  	      	   pdfchargeamtcell.setBorder(0);
	   	   }
		     
		     chunk = new Phrase("Refer other charge details",font1);
		     otherchargecell=new PdfPCell(chunk);
		     otherchargecell.setBorder(0);
		     otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		     remainigInfotable.addCell(otherchargecell);
	      }
	    
	   
 }
 else if(cnt<= breakpoint)
 {
	   
	   for(int i=0;i<this.billingTaxesLis.size();i++)
	      {
		   	double taxAmt=0;
		   	double taxAmt1=0;
		   	double taxAmt2=0;
		   	double taxAmt3=0;
			double taxAmt4=0;
	    	  
	    	  if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
	    		  
	    		  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("VAT")))
			  	      	
	    		  {  
//		    	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//		         pdfservicecell = new PdfPCell(chunk);
//		        pdfservicecell.setBorder(0);
//		        chargetaxtable.addCell(pdfservicecell);
//	    	  
//		        
//		        chunk=new Phrase(df.format(taxAmt),font1);
//		        pdftaxamtcell = new PdfPCell(chunk);
//		      	pdftaxamtcell.setBorder(0);
//		      	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		      	chargetaxtable.addCell(pdftaxamtcell);
	    			  
	    			  
	    			String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
	    			taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
	    			myList1.add(name);
	    			myList2.add(df.format(taxAmt));
	    			  
	    			  
	    		  }
	    		  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()!=12.36))
			  	      	
	    		  {  
//		    	 chunk = new Phrase(billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent(),font1);
//		         pdfservicecell = new PdfPCell(chunk);
//		        pdfservicecell.setBorder(0);
//		        chargetaxtable.addCell(pdfservicecell);
//	    	  
//		        taxAmt=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
//		        chunk=new Phrase(df.format(taxAmt),font1);
//		        pdftaxamtcell = new PdfPCell(chunk);
//		      	pdftaxamtcell.setBorder(0);
//		      	pdftaxamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		      	chargetaxtable.addCell(pdftaxamtcell);
	    		  
	    		  String name=billingTaxesLis.get(i).getTaxChargeName()+" @ "+billingTaxesLis.get(i).getTaxChargePercent();
	    		  taxAmt1=billingTaxesLis.get(i).getTaxChargePercent()*billingTaxesLis.get(i).getTaxChargeAssesVal()/100;
	    		  
	    		  myList1.add(name);
	    		  myList2.add(df.format(taxAmt1));
	    		  
	    		  
	    		  }
	    		  
	    		  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==12.36))
	  	      	{
	  	      		
//	  	      		Phrase chunk1 = new Phrase("Service Tax "+"@ 12.00",font1);
//	  	      		PdfPCell pdfservcell = new PdfPCell(chunk1);
//	  	      		pdfservcell.setBorder(0);
//	  	      	pdfservcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	  	       	chargetaxtable.addCell(pdfservcell);
//	  	       	
//	  	      		
//	  	      	taxAmt1=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
//	  	        chunk=new Phrase(df.format(taxAmt1),font1);
//		        pdftaxamt1cell = new PdfPCell(chunk);
//		        pdftaxamt1cell.setBorder(0);
//		        pdftaxamt1cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		      	chargetaxtable.addCell(pdftaxamt1cell);
//	  	      		
//	  	      		
//	  	      		Phrase chunk2 = new Phrase("Education Cess "+"@ 2.00",font1);
//	  	      		PdfPCell pdfEducesscell = new PdfPCell(chunk2);
//	  	      		pdfEducesscell.setBorder(0);
//	  	      	pdfEducesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	  	       	chargetaxtable.addCell(pdfEducesscell);
//	  	      	
//	  	      	taxAmt2=(taxAmt1*2.00)/100;
//	  	        chunk=new Phrase(df.format(taxAmt2),font1);
//		        pdftaxamt2cell = new PdfPCell(chunk);
//		        pdftaxamt2cell.setBorder(0);
//		        pdftaxamt2cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		      	chargetaxtable.addCell(pdftaxamt2cell);	
//	  	      		
//	  	      		
//	  	      		Phrase chunk3 = new Phrase("High Education Cess "+"@ 1.00",font1);
//	  	      		PdfPCell pdfHighcesscell = new PdfPCell(chunk3);
//	  	      		pdfHighcesscell.setBorder(0);
//	  	      	pdfHighcesscell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	  	       	chargetaxtable.addCell(pdfHighcesscell);
//	  	      		
//	  	      	taxAmt3=(taxAmt1*1.00)/100;
//	  	        chunk=new Phrase(df.format(taxAmt3),font1);
//		        pdftaxamt3cell = new PdfPCell(chunk);
//		        pdftaxamt3cell.setBorder(0);
//		        pdftaxamt3cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		      	chargetaxtable.addCell(pdftaxamt3cell);		
	  	      		
	    			  String space=" ";
	    		String name="Service Tax "+"@ 12.00";	  
	    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*12)/100;
	    		String name1="Education Cess "+"@ 2.00";
	    		taxAmt3=(taxAmt2*2.00)/100;
	    		String name2="High Education Cess "+"@ 1.00";
	    		taxAmt4=(taxAmt2*1.00)/100;
	    			  
	    		myList1.add(name);
	    		myList1.add(name1);
	    		myList1.add(name2);
	    		
	    		myList2.add(df.format(taxAmt2));
	    		myList2.add(df.format(taxAmt3));
	    		myList2.add(space);
	    		myList2.add(df.format(taxAmt4));
	    		
	  	      	}
	    	  
	    		  if((billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax"))&&(billingTaxesLis.get(i).getTaxChargePercent()==14.5))
		  	      	{
		    		
		    		String name="Service Tax "+"@ 14.00";	  
		    		taxAmt2=(billingTaxesLis.get(i).getTaxChargeAssesVal()*14)/100;
		    		String name1="Education Cess "+"@ 0.50";
		    		taxAmt3=(taxAmt2*0.50)/100;
//		    		String name2="High Education Cess "+"@ 1.00";
//		    		taxAmt4=(taxAmt2*1.00)/100;
		    			 
		    		String str=" ";
		    		
		    		myList5.add(name);
		    		myList5.add(name1);
//		    		myList5.add(name2);
		    		
		    		myList6.add(df.format(taxAmt2));
		    		myList6.add(df.format(taxAmt3));
		    		myList6.add(str);
//		    		myList6.add(df.format(taxAmt4));
		    		
		  	      	}
	    		  
	    		  
	    		  
	    		  
	    	  }
	     	}
	   
	   PdfPCell pdfservicecell=null;
		
		PdfPTable other1table=new PdfPTable(1);
		other1table.setWidthPercentage(100);
		for(int j=0;j<myList1.size();j++){
			chunk = new Phrase(myList1.get(j),font8);
			 pdfservicecell = new PdfPCell(chunk);
			pdfservicecell.setBorder(0);
			pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(pdfservicecell);
			
		}
		
		PdfPCell pdfservicecell1=null;
		
		PdfPTable other2table=new PdfPTable(1);
		other2table.setWidthPercentage(100);
		for(int j=0;j<myList2.size();j++){
			chunk = new Phrase(myList2.get(j),font8);
			 pdfservicecell1 = new PdfPCell(chunk);
			pdfservicecell1.setBorder(0);
			pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(pdfservicecell1);
			
		}
		
		PdfPCell othercell = new PdfPCell();
		othercell.addElement(other1table);
		othercell.setBorder(0);
		remainigInfotable.addCell(othercell);
		
		PdfPCell othercell1 = new PdfPCell();
		othercell1.addElement(other2table);
		othercell1.setBorder(0);
		remainigInfotable.addCell(othercell1);
	   
	   
	    for(int i=0;i<this.billingChargesLis.size();i++)
	      {
	   	   Phrase chunk = null;
	   	Phrase chunk1 = new Phrase("",font8);
	   	   double chargeAmt=0;
	   	   PdfPCell pdfchargeamtcell = null;
	   	   PdfPCell pdfchargecell = null;
	   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
	   		   pdfchargecell=new PdfPCell(chunk);
//	   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
	   		 chargeAmt=billingChargesLis.get(i).getPayableAmt();
	   		   chunk=new Phrase(df.format(chargeAmt),font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  	      	   pdfchargeamtcell.setBorder(0);
	  	      	 pdfchargecell.setBorder(0);
		 	      pdfchargeamtcell.setBorder(0);
		 	     remainigInfotable.addCell(pdfchargecell);
		 	    remainigInfotable.addCell(pdfchargeamtcell);
	   	   }else{
	   		pdfchargecell=new PdfPCell(chunk1);
	   		pdfchargeamtcell = new PdfPCell(chunk1);
	   	 pdfchargecell.setBorder(0);
	      pdfchargeamtcell.setBorder(0);
	      remainigInfotable.addCell(pdfchargecell);
	      remainigInfotable.addCell(pdfchargeamtcell);
	   	   }
	   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
	   		   pdfchargecell=new PdfPCell(chunk);
//	   		   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
	   		chargeAmt=billingChargesLis.get(i).getPayableAmt();
	   		   chunk=new Phrase(chargeAmt+"",font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  	      	   pdfchargeamtcell.setBorder(0);
	  	      	 pdfchargecell.setBorder(0);
		 	      pdfchargeamtcell.setBorder(0);
		 	     remainigInfotable.addCell(pdfchargecell);
		 	    remainigInfotable.addCell(pdfchargeamtcell);
	   	   }else{
	   		pdfchargecell=new PdfPCell(chunk1);
	   		pdfchargeamtcell = new PdfPCell(chunk1);
	   	 pdfchargecell.setBorder(0);
	      pdfchargeamtcell.setBorder(0);
	      remainigInfotable.addCell(pdfchargecell);
	      remainigInfotable.addCell(pdfchargeamtcell);
	   	   }
	 	      
	      }
	    
	    
	    chunk = new Phrase("Net Payable",font1);
        PdfPCell netcell = new PdfPCell(chunk);
        netcell.setBorder(0);
        
      	Double invAmt=invoiceentity.getInvoiceAmount();
      	remainigInfotable.addCell(netcell);
      	int netpayble=(int) invAmt.doubleValue();
      	
      	chunk=new Phrase(netpayble+"",font8);
        PdfPCell amtcell = new PdfPCell(chunk);
        amtcell.setBorder(0);
        amtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        remainigInfotable.addCell(amtcell);
	    
	   
 }
 
			 }
			 
		 }		 
		 
/****************************************************************************************/

PdfPTable billamtable = new PdfPTable(2);
billamtable.setWidthPercentage(100);
billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
//billamtable.setSpacingBefore(10f);
//billamtable.setSpacingAfter(10f);


//for(int i=0;i<this.billingChargesLis.size();i++)
//{
//Phrase chunk = null;
//Phrase chunk1 = new Phrase("",font8);
//double chargeAmt=0;
//PdfPCell pdfchargeamtcell = null;
//PdfPCell pdfchargecell = null;
//if(billingChargesLis.get(i).getTaxChargePercent()!=0){
//   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
//   pdfchargecell=new PdfPCell(chunk);
//   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
//   chunk=new Phrase(df.format(chargeAmt),font8);
//	   pdfchargeamtcell = new PdfPCell(chunk);
//	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	   pdfchargeamtcell.setBorder(0);
//}else{
//pdfchargecell=new PdfPCell(chunk1);
//pdfchargeamtcell = new PdfPCell(chunk1);
//}
//if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
//   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
//   pdfchargecell=new PdfPCell(chunk);
//   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
//   chunk=new Phrase(chargeAmt+"",font8);
//	   pdfchargeamtcell = new PdfPCell(chunk);
//	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	   pdfchargeamtcell.setBorder(0);
//}else{
//pdfchargecell=new PdfPCell(chunk1);
//pdfchargeamtcell = new PdfPCell(chunk1);
//}
// pdfchargecell.setBorder(0);
//pdfchargeamtcell.setBorder(0);
// chargetaxtable.addCell(pdfchargecell);
// chargetaxtable.addCell(pdfchargeamtcell);
//}
for(int i=0;i<this.billingChargesLis.size();i++)
{
	if(i==billingChargesLis.size()-1){
		chunk = new Phrase("Net Payable",font1);
        PdfPCell pdfinvamtcell = new PdfPCell(chunk);
        pdfinvamtcell.setBorder(0);
      	Double invAmt=invoiceentity.getInvoiceAmount();
      	
      	int netpayble=(int) invAmt.doubleValue();
      	
      	chunk=new Phrase(netpayble+"",font8);
        PdfPCell pdfnetpayamt = new PdfPCell(chunk);
        pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
        pdfnetpayamt.setBorder(0);
        
//        chargetaxtable.addCell(pdfinvamtcell);
//        chargetaxtable.addCell(pdfnetpayamt);
        
        billamtable.addCell(pdfinvamtcell);
        billamtable.addCell(pdfnetpayamt);
	}
}

if(this.billingChargesLis.size()==0){

Phrase chunkinvAmttitle = new Phrase("Net Payable",font1);
PdfPCell invAmtTitleCell=new PdfPCell(chunkinvAmttitle);
invAmtTitleCell.setBorder(0);
Double invoiceAmount=invoiceentity.getInvoiceAmount();

int netpayble=(int) invoiceAmount.doubleValue();

Phrase chunkinvamt=new Phrase(netpayble+"",font8);
PdfPCell pdfinvamt = new PdfPCell(chunkinvamt);
pdfinvamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
pdfinvamt.setBorder(0);
  
//chargetaxtable.addCell(invAmtTitleCell);
//chargetaxtable.addCell(pdfinvamt);

billamtable.addCell(invAmtTitleCell);
  billamtable.addCell(pdfinvamt);
}

PdfPCell cagrecell=new PdfPCell();
cagrecell.addElement(chargetaxtable);
cagrecell.setBorder(0);

PdfPTable mainbilltable = new PdfPTable(1);
mainbilltable.setWidthPercentage(100);
mainbilltable.setHorizontalAlignment(Element.ALIGN_RIGHT);
mainbilltable.addCell(cagrecell);
//mainbilltable.addCell(billamtable);



PdfPTable taxinfotable=new PdfPTable(1);
//taxinfotable.setWidthPercentage(100);
//taxinfotable.addCell(mainbilltable);

PdfPTable parenttaxtable=new PdfPTable(2);
parenttaxtable.setWidthPercentage(100);
try {
parenttaxtable.setWidths(new float[]{65,35});
} catch (DocumentException e1) {
e1.printStackTrace();
}

PdfPCell amtWordsTblcell1 = new PdfPCell();
PdfPCell taxdatacell = new PdfPCell();

amtWordsTblcell1.addElement(termstable1);
taxdatacell.addElement(mainbilltable);

parenttaxtable.addCell(amtWordsTblcell1);
parenttaxtable.addCell(taxdatacell);

try {
	document.add(parenttaxtable);
} catch (DocumentException e) {
	e.printStackTrace();
}


  String amtInWords="Rupees:  "+ServiceInvoicePdf.convert(invoiceentity.getInvoiceAmount());
   System.out.println("amt"+amtInWords);
   Phrase amtwords=new Phrase(amtInWords+" Only.",font1);
   PdfPCell amtWordsCell=new PdfPCell();
   amtWordsCell.addElement(amtwords);
   amtWordsCell.setBorder(0);
   
   
   PdfPTable amountInWordsTable=new PdfPTable(1);
   amountInWordsTable.setWidthPercentage(100);
//   amountInWordsTable.setHorizontalAlignment(Element.ALIGN_TOP);
   amountInWordsTable.addCell(amtWordsCell);
	
  
   PdfPTable termpayTable=new PdfPTable(1);
   termpayTable.setWidthPercentage(100);
//   termpayTable.addCell(headingpayterms);
   termpayTable.addCell(termstable1);
//   termpayTable.addCell(amountInWordsTable);

   PdfPTable parenttaxtable1=new PdfPTable(2);
      parenttaxtable1.setWidthPercentage(100);
      try {
		parenttaxtable1.setWidths(new float[]{65,35});
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
      
	  PdfPCell amtWordsTblcell = new PdfPCell();
	  PdfPCell taxdatacell1 = new PdfPCell();
		
	  amtWordsTblcell.addElement(amountInWordsTable);
	  taxdatacell1.addElement(billamtable);
	  parenttaxtable1.addCell(amtWordsTblcell);
	  parenttaxtable1.addCell(taxdatacell1);
	
		try {
			document.add(parenttaxtable1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		logger.log(Level.SEVERE,"end of terms Conditions Info");
}


public void addFooter()
{
  logger.log(Level.SEVERE,"footer Info");
  
//  Phrase custsignphrase=new Phrase("Customer Signature",font10bold);
//  Phrase blanksign=new Phrase(" ",font10bold);
//  PdfPCell signblank=new PdfPCell();
//  signblank.addElement(blanksign);
//  signblank.setBorder(0);
//  
//  PdfPCell custsigncell=new PdfPCell();
//  custsigncell.addElement(custsignphrase);
//  custsigncell.setBorder(0);
//  
//  PdfPTable custsigntable=new PdfPTable(1);
//  custsigntable.setWidthPercentage(100);
//  custsigntable.addCell(signblank);
//  custsigntable.addCell(signblank);
//  custsigntable.addCell(signblank);
//  custsigntable.addCell(signblank);
//  custsigntable.addCell(signblank);
//  custsigntable.addCell(custsigncell);
  String terms="";
  Phrase desc1=null;
  
  Phrase dotline=null;
  
 if(!invComment.equals("")){
	 if(upcflag==false){
		 terms="Terms And Conditions";
	 }
	 if(upcflag==true){
		 terms="";
	 }
	 desc1=new Phrase(invComment,font8);
	 
 } 
 
  
  PdfPCell desc1cell=new PdfPCell();
  desc1cell.addElement(desc1);
  desc1cell.setBorder(0);
 
  
  
  
  Phrase term=new Phrase(terms,font10bold);
  
  PdfPCell termcell= new PdfPCell();
  termcell.addElement(term);
  termcell.setBorder(0);
  
  
  Phrase vatTaxNO=null;
  Phrase stTaxNO=null;
	  PdfPCell dotlinecell= new PdfPCell();
	  dotlinecell.addElement(dotline);
	  dotlinecell.setBorder(0);
	  
  
  PdfPTable desctable=new PdfPTable(1);
  desctable.setWidthPercentage(100);
  desctable.addCell(termcell);
  desctable.addCell(desc1cell);
  desctable.addCell(dotlinecell);
  
  PdfPCell blankkcell=new PdfPCell();
  blankkcell.addElement(Chunk.NEWLINE);
  blankkcell.setBorder(0);

  PdfPTable articletable=new PdfPTable(4);
  articletable.setWidthPercentage(100);
  articletable.setHorizontalAlignment(Element.ALIGN_LEFT);
  
  if(UniversalFlag){

	  if(con.getGroup().equalsIgnoreCase("Universal Pest Control Pvt. Ltd.")){
		  
	  int rrrr=0;
	  for(int i=0;i<this.articletype.size();i++){
		  
		  if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("ServiceInvoice")){  
		  rrrr=1;
	  }
	  
	  }
	  
	  
	  if(rrrr==1){
		  
		  dotline= new Phrase("------------------------------------------------------------------------------------"
			  		,font8);
		  PdfPCell dotlinecell1= new PdfPCell();
		  dotlinecell1.addElement(dotline);
		  dotlinecell1.setBorder(0);
		  
		  if(invComment.equals("")){
			  desctable.addCell(blankkcell);
			  desctable.addCell(blankkcell);
		  }
		 
		  desctable.addCell(dotlinecell1);
	  }

//	  PdfPTable articletable=new PdfPTable(4);
//	  articletable.setWidthPercentage(100);
//	  articletable.setHorizontalAlignment(Element.ALIGN_LEFT);
	  
	  //  rohan made this changes for universal pest control on 3-4-2017 
	  Phrase blanck=new Phrase("");
	 
	  for(int i=0;i<this.articletype.size();i++){
	  
		 if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("ServiceInvoice")){
		
			 
			 vatTaxNO=new Phrase(articletype.get(i).getArticleTypeName(),font8);
			 stTaxNO=new Phrase(articletype.get(i).getArticleTypeValue(),font8);
			 
			 PdfPCell vatcell = new PdfPCell();
			  vatcell.addElement(vatTaxNO);
			  vatcell.setBorder(0);
			  vatcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			  
				 PdfPCell vatcell1 = new PdfPCell();
				  vatcell1.addElement(stTaxNO);
				  vatcell1.setBorder(0);
				  vatcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				  
				  
				  PdfPCell vatcell2 = new PdfPCell();
				  vatcell2.addElement(blanck);
				  vatcell2.setBorder(0);
			  
			  
				  articletable.addCell(vatcell);
				  articletable.addCell(vatcell1);
				  

				  articletable.addCell(vatcell2);
				  articletable.addCell(vatcell2);

		 }
	  }
	}
}
  else{
	  
	  int rrrr=0;
	  for(int i=0;i<this.articletype.size();i++){
		  
		  if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equalsIgnoreCase("ServiceInvoice")){  
		  rrrr=1;
	  }
	  
	  }
	  
	  
	  if(rrrr==1){
		  
		  dotline= new Phrase("------------------------------------------------------------------------------------"
			  		,font8);
		  PdfPCell dotlinecell1= new PdfPCell();
		  dotlinecell1.addElement(dotline);
		  dotlinecell1.setBorder(0);
		  
		  if(invComment.equals("")){
			  desctable.addCell(blankkcell);
			  desctable.addCell(blankkcell);
		  }
		 
		  desctable.addCell(dotlinecell1);
	  }

//	  PdfPTable articletable=new PdfPTable(4);
//	  articletable.setWidthPercentage(100);
//	  articletable.setHorizontalAlignment(Element.ALIGN_LEFT);
	  
	  //  rohan made this changes for universal pest control on 3-4-2017 
	  Phrase blanck=new Phrase("");
	  System.out.println("flag="+UniversalFlag +"-"+"Contract Group"+con.getGroup());
	  
	  for(int i=0;i<this.articletype.size();i++){
			 
		  
			 if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("ServiceInvoice")){
			
				 
				 vatTaxNO=new Phrase(articletype.get(i).getArticleTypeName(),font8);
				 stTaxNO=new Phrase(articletype.get(i).getArticleTypeValue(),font8);
				 
				 PdfPCell vatcell = new PdfPCell();
				  vatcell.addElement(vatTaxNO);
				  vatcell.setBorder(0);
				  vatcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				  
					 PdfPCell vatcell1 = new PdfPCell();
					  vatcell1.addElement(stTaxNO);
					  vatcell1.setBorder(0);
					  vatcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					  
					  
					  PdfPCell vatcell2 = new PdfPCell();
					  vatcell2.addElement(blanck);
					  vatcell2.setBorder(0);
				  
				  
					  articletable.addCell(vatcell);
					  articletable.addCell(vatcell1);
					  

					  articletable.addCell(vatcell2);
					  articletable.addCell(vatcell2);

			 }
			 
	    }
  }
  
  PdfPCell erticlecell=new PdfPCell(articletable);
  erticlecell.setBorder(0);
  erticlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
  desctable.addCell(erticlecell);
  
  
//  if(comp.getVatTaxNo()!=null){
//   vatTaxNO=new Phrase(cust.getArticleTypeDetails().g+" "+comp.getVatTaxNo(),font8);
//  }else{
//	  
//	  vatTaxNO=new Phrase("",font8);
//  }
  
//  if(comp.getServiceTaxNo()!=null){
//   stTaxNO=new Phrase("Service tax no : "+comp.getServiceTaxNo(),font8);
//  }else{
//	  stTaxNO=new Phrase("",font8);
//  }
  
  
  
//  PdfPCell vatcell = new PdfPCell();
//  vatcell.addElement(vatTaxNO);
//  vatcell.setBorder(0);
  
  PdfPCell stcell = new PdfPCell();
  stcell.addElement(stTaxNO);
  stcell.setBorder(0);
  
  PdfPTable taxNOtable=new PdfPTable(1);
  taxNOtable.addCell(termcell);
  taxNOtable.addCell(desc1cell);
  taxNOtable.setSpacingAfter(60f);
  PdfPCell taxcell=new PdfPCell();
  taxcell.addElement(taxNOtable);
  taxcell.setBorder(0);
  
  
//  desctable.addCell(taxNOtable);

//  desctable.addCell(desc2cell);
//  desctable.addCell(desc3cell);
//  desctable.addCell(desc4cell);
//  desctable.addCell(desc5cell);
//  desctable.addCell(desc6cell);
  
  
  
  
  PdfPTable table = new PdfPTable(1);
  table.setWidthPercentage(100);
  
  Paragraph authophrase = null;
 
  Phrase blankphrase = new Phrase(" ");
  PdfPCell blankcell=new PdfPCell();
  blankcell.addElement(blankphrase);
  blankcell.setBorder(0);
  
  if(autorityFlag)
  {
	String auothority = "This is a computer generated invoice. No signature required";  
	authophrase = new Paragraph(auothority,font8bold);
	authophrase.setAlignment(Element.ALIGN_LEFT);
	
	table.addCell(blankcell);
	  table.addCell(blankcell);
	  table.addCell(blankcell);
  }
  else
  {
	  
	String auothority = " ";  
	authophrase = new Paragraph(auothority,font8bold);
		
		
	  String companyname ="";
	  if(multipleCompanyName)
		 {
		  if(con.getGroup()!=null && !con.getGroup().equals(""))
		  {
			  companyname =con.getGroup().trim().toUpperCase();
		  }
		  else
		  {
			  companyname = comp.getBusinessUnitName().trim().toUpperCase();
		  }
		 
		 }
		 else
		 {
			 companyname = comp.getBusinessUnitName().trim().toUpperCase(); 
		 }
  Paragraph companynamepara=new Paragraph();
  companynamepara.add("FOR "+companyname);
  companynamepara.setFont(font9bold);
  companynamepara.setAlignment(Element.ALIGN_CENTER);
  String authsign="AUTHORISED SIGNATORY";
  
  Paragraph authpara=new Paragraph();
  authpara.add(authsign); 
  authpara.setFont(font9bold);
  authpara.setAlignment(Element.ALIGN_CENTER);
  PdfPCell companynamecell=new PdfPCell();
  companynamecell.addElement(companynamepara);
  companynamecell.setBorder(0);
     
  PdfPCell authsigncell=new PdfPCell();
  authsigncell.addElement(authpara);
  authsigncell.setBorder(0);
 
  
  
  table.addCell(companynamecell);
  table.addCell(blankcell);
  table.addCell(blankcell);
  table.addCell(blankcell);
  table.addCell(authsigncell);
  }
 
  
  PdfPTable parentdesctable=new PdfPTable(2);
  parentdesctable.setWidthPercentage(100);
  PdfPCell signparent=new PdfPCell();
  PdfPCell descparent=new PdfPCell();
  PdfPCell authparent=new PdfPCell();
  
//  signparent.addElement(custsigntable);
  descparent.addElement(desctable);
  authparent.addElement(table);
//  parentdesctable.addCell(signparent);
  parentdesctable.addCell(descparent);
  parentdesctable.addCell(authparent);
  
  try {
	parentdesctable.setWidths(new float[]{65,35});
  } 
  catch (DocumentException e1) {
	e1.printStackTrace();
  }
  
  
  
  
  
  	 String endDucument = "*************************X--X--X*************************";
	 Paragraph endpara=new Paragraph();
	 endpara.add(endDucument);
	 endpara.setAlignment(Element.ALIGN_CENTER);
	 
	  PdfPTable parentbanktable=new PdfPTable(1);
	  parentbanktable.setWidthPercentage(100);
      
	  PdfPCell authorisedcell = new PdfPCell();
		
	  authorisedcell.addElement(parentdesctable);
	  parentbanktable.addCell(authorisedcell);
	  
	  Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
	  PdfPTable disclaimertable = new PdfPTable(1);
	  
	  	if(disclaimerflag==false){
		  
		  disclaimertable.setWidthPercentage(100);
		  Phrase disclaimerphrase = new Phrase(disclaimerText,font1);
				
		  PdfPCell disclaimercell = new PdfPCell(disclaimerphrase);
		  disclaimertable.addCell(disclaimercell);
	  }
	  
	  Phrase refphrase=new Phrase("Annexure 1 ",font10bold);
	  Paragraph repara=new Paragraph(refphrase);
	  
	  
	  try {
			document.add(parentdesctable);
			document.add(authophrase);
			 document.add(Chunk.NEWLINE);
			 if(disclaimerflag==true){
				  document.add(disclaimertable);
			  }
			 
		if((contractChargesLis.size()+contractTaxesLis.size())>4){
			

		    chunk=new Phrase("Annexure for more details",font1);
		    Paragraph para = new Paragraph(chunk);
		    para.setSpacingAfter(10f);
		    
			
			 PdfPTable parentcelltable = new PdfPTable(1);
			 parentcelltable.setWidthPercentage(100);
			 parentcelltable.addCell(remainigInfotable);

			document.add(Chunk.NEXTPAGE);
			document.add(para);
			document.add(parentcelltable);
		}	 
			 
		 Paragraph blank1 =new Paragraph();
		 blank1.add(Chunk.NEWLINE); 	 
		 
		if(this.salesProd.size() > 5){
//		if(eva>20){	
		document.newPage();
		
		if(upcflag==true){
			
			document.add(blank1);
			document.add(blank1);
			document.add(blank1);
			document.add(blank1);
			document.add(blank1);
				
		}
		
		document.add(repara);
		settingRemainingRowsToPDF(6);
		}if(this.salesProd.size() > 55){
//		}if(eva>80){
			document.newPage();
			if(upcflag==true){
				
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
					
			}
			document.add(repara);
		settingRemainingRowsToPDF(56);
		}if(this.salesProd.size() > 115){
//		}if(eva>140){	
			document.newPage();
			if(upcflag==true){
				
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
					
			}
			document.add(repara);
		settingRemainingRowsToPDF(116);
		}if(this.salesProd.size() > 175){
//		}if(eva>200){	
			document.newPage();
			if(upcflag==true){
				
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
				document.add(blank1);
					
			}
			document.add(repara);
		settingRemainingRowsToPDF(176);
		}
//		if(this.salesProd.size()>6){
//			document.add(endpara);
//			}
		
//			if(payTerms.get(0).getPayTermPercent()!=100){
//			
//			document.add(Chunk.NEXTPAGE);
//			
//			PdfPCell parentcell=new PdfPCell(chargetaxtableToNewPage);
//			
//			PdfPTable chargetaxtableparent=new PdfPTable(1);
//			chargetaxtableparent.addCell(parentcell);
//			chargetaxtableparent.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//			PdfPCell netPayableTablecell=new PdfPCell(netPayableTable);
//			
//			PdfPTable parentnetPayableTable=new PdfPTable(1);
//			parentnetPayableTable.addCell(netPayableTablecell);
//			parentnetPayableTable.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//			document.add(chargetaxtableparent);
//			document.add(parentnetPayableTable);
//			
//			}
		
			
			/*****************************************Start product Description************************************************************/		

			
		String prodetails="";
		int ctr=0;
		for(int i=0;i<this.salesProd.size();i++){
			for(int j=0;j<this.stringlis.size();j++){
			if(!stringlis.get(j).getComment().equals("")||custbranchlist.size()!=0){
				 ctr=ctr+1;
			}
			}
			if((i==this.salesProd.size()-1)&&(ctr>0)){
				document.add(Chunk.NEXTPAGE);
				if(upcflag==true)
				{
					Paragraph blank=new Paragraph();
					blank.add(Chunk.NEWLINE);
					
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
					document.add(blank);
				}
			 }
		}
		
if(invoiceentity.getCustomerBranch().equals("")||invoiceentity.getCustomerBranch().equals("--SELECT--")){
if(custbranchlist.size()!=0&&!cust.getSecondaryAdress().getAddrLine1().equals("")){
			
			System.out.println("in if ===============");
			
			Phrase title=new Phrase("Site Address");
			
//			PdfPCell titlecell=new PdfPCell();
//			titlecell.addElement(title);
//			titlecell.setBorder(0);
			
			Paragraph titlpara=new Paragraph();
			titlpara.add(title);
			titlpara.setAlignment(Element.ALIGN_LEFT);
			
			document.add(titlpara);
			document.add(Chunk.NEWLINE);
			for(int i = 0;i<custbranchlist.size();i++){
				
				System.out.println("in for ============================");
				
				
				
				
				Phrase clientname=null;
				
				clientname=new Phrase("Branch Name : "+custbranchlist.get(i).getBusinessUnitName().trim(),font10bold);
			
				
				PdfPCell custnamecell=new PdfPCell();
				custnamecell.addElement(clientname);
				custnamecell.setBorder(0);
			
				Phrase custaddress1=new Phrase("                                 "+custbranchlist.get(i).getAddress().getAddrLine1().trim(),font8);
				
				Phrase custaddress2=null;
				if(!custbranchlist.get(i).getAddress().getAddrLine2().equals("")){
					custaddress2=new Phrase("                                 "+custbranchlist.get(i).getAddress().getAddrLine2().trim(),font8);
				}else{
					custaddress2=new Phrase("",font10);
				}
				Phrase landmark=null;
				
				if(!custbranchlist.get(i).getAddress().getLandmark().equals("")){
					landmark=new Phrase("                                  "+custbranchlist.get(i).getAddress().getLandmark(),font8);
				}else{
					landmark=new Phrase("",font10);
				}
				Phrase locality=null;
				if(!custbranchlist.get(i).getAddress().getLocality().equals("")){
					locality=new Phrase("                                 "+custbranchlist.get(i).getAddress().getLocality(),font8);
				}else{
					locality=new Phrase("",font8);
				}
					Phrase city=new Phrase("                                 "+custbranchlist.get(i).getAddress().getCity().trim()+" - "+custbranchlist.get(i).getAddress().getPin(),font8);
				
					Phrase contactcust = null;
				if(custbranchlist.get(i).getLandline()!= null)
				{	
					 contactcust=new Phrase("MobilMobile: "+custbranchlist.get(i).getCellNumber1()+"    Phone: "+custbranchlist.get(i).getLandline()+"    Email:  "+custbranchlist.get(i).getEmail().trim()+" ",font8);
				}
				else
				{
					 contactcust=new Phrase("Mobile: "+custbranchlist.get(i).getCellNumber1()+"    Email:  "+custbranchlist.get(i).getEmail().trim()+" ",font8);
				}
				logger.log(Level.SEVERE,"222222222222222222222222222222222222222222222222222");
				PdfPCell custaddr1cell=new PdfPCell();
				custaddr1cell.addElement(custaddress1);
				custaddr1cell.setBorder(0);
				
				PdfPCell custaddr2cell=null;
				if(custaddress2!=null){
					custaddr2cell=new PdfPCell();
					custaddr2cell.addElement(custaddress2);
					custaddr2cell.setBorder(0);
				}
				
				PdfPCell custlocalitycell=new PdfPCell();
				custlocalitycell.addElement(locality);
				custlocalitycell.setBorder(0);
				
				PdfPCell lanmarkcell=new PdfPCell();
				lanmarkcell.addElement(landmark);
				lanmarkcell.setBorder(0);
				
				PdfPCell citycell=new PdfPCell();
				citycell.addElement(city);
				citycell.setBorder(0);
				
				PdfPCell custcontactcell=new PdfPCell();
				custcontactcell.addElement(contactcust);
				//custcontactcell.addElement(Chunk.NEWLINE);
				custcontactcell.setBorder(0);
				logger.log(Level.SEVERE,"3333333333333333333333333333333333333333333333");
				PdfPTable custtable=new PdfPTable(1);
				custtable.setWidthPercentage(100);
//				custtable.addCell(titlecell);
				custtable.addCell(custnamecell);
				custtable.addCell(custaddr1cell);
				if(!cust.getAdress().getAddrLine2().equals("")){
					custtable.addCell(custaddr2cell);
				}
				if(!cust.getAdress().getLandmark().equals("")){
					custtable.addCell(lanmarkcell);
				}
				if(!cust.getAdress().getLocality().equals("")){
					custtable.addCell(custlocalitycell);
				}
				custtable.addCell(citycell);
				custtable.addCell(custcontactcell);
				
				
				Paragraph blank=new Paragraph(" ");
				
				PdfPTable parenttable= new PdfPTable(1);
				parenttable.setWidthPercentage(100);
				
				PdfPCell custinfocell = new PdfPCell();
				
				custinfocell.addElement(custtable);
				parenttable.addCell(custinfocell);
				
				
				
				try {
					document.add(parenttable);
//					document.add(blank);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				
			}
			
		}
	}
		
				
		for(int i=0;i<this.salesProd.size();i++){
			for(int j=0;j<this.stringlis.size();j++){	
			if(!stringlis.get(j).getComment().equals("")){
				 prodetails="Products Details";
			}
		}
		}
		
		
			
			Phrase prophrase=new Phrase(prodetails,font12boldul);
			Paragraph propara=new Paragraph();
			propara.add(prophrase);
		
			if(upcflag==true)
			{
				logger.log(Level.SEVERE,"hi vijay "+invoiceentity.getCustomerBranch().equals("") );
				logger.log(Level.SEVERE,"hi vijay "+invoiceentity.getCustomerBranch().equals("--SELECT--"));
				
				if(custbranchlist.size()>=4){
					logger.log(Level.SEVERE,"if branch is >4");
					if(invoiceentity.getCustomerBranch().equals("") || invoiceentity.getCustomerBranch().equals("--SELECT--")){
						logger.log(Level.SEVERE," inside condtion");
					
					document.add(Chunk.NEXTPAGE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(propara);
					document.add(Chunk.NEWLINE);
					}else{
						System.out.println("inside new code else");
						document.add(Chunk.NEWLINE);
						document.add(propara);
						document.add(Chunk.NEWLINE);
					}
				}else{
					logger.log(Level.SEVERE,"else block of if branch is >4");
					document.add(Chunk.NEWLINE);
					document.add(propara);
					document.add(Chunk.NEWLINE);
				}
				
				
			}else{
				document.add(Chunk.NEWLINE);
				document.add(propara);
				document.add(Chunk.NEWLINE);
			}
			
			
			
			String descri="";
			Phrase descri1=new Phrase("",font10);
			Paragraph para1=new Paragraph("");
			System.out.println("pro size() ==== "+this.salesProd.size());
			
			int ctr1=0;
			
			//   *************rohan added code to print desc of multiple products
			
			PdfPTable table1 = new PdfPTable(1);
			table1.setWidthPercentage(100f);
			
			PdfPCell termCell = null;
			PdfPCell descri1Cell = null;
			
//			for(int i=0;i<this.salesProd.size();i++){
				for(int j=0;j<this.stringlis.size();j++){
				
					System.out.println("product name "+this.stringlis.get(j).getProductName());
					
				if(!stringlis.get(j).getComment().equals("")){
				term=new Phrase("Product Id : "+stringlis.get(j).getCount()+""+"        Product Name : "+stringlis.get(j).getProductName()+"\n",font10bold);
				termCell =  new PdfPCell(term);
				termcell.setBorder(0);
				
				descri1 = new Phrase(descri+stringlis.get(j).getComment(),font10);
				descri1Cell =  new PdfPCell(descri1);
				descri1Cell.setBorder(0);
				
				table1.addCell(termcell);
				table1.addCell(descri1Cell);
				
				ctr1=ctr1+1;
				}else{
					
					term = new Phrase("",font10);
					descri1 = new Phrase("",font10);
				}
				
				para1 = new Paragraph();
				para1.add(term);
				
				para1.add(descri1);
				para1.setAlignment(Element.ALIGN_LEFT);
				
				
				if(upcflag==true){
					
					if(ctr1>5){
						
						document.add(Chunk.NEXTPAGE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						
						document.add(para1);
//						document.add(para2);
					}else{
						
						document.add(para1);
//						document.add(para2);
					}
					
				}else{
					
					document.add(para1);
//					document.add(para2);
					
				}
				}
				
//		Paragraph para2 = new Paragraph();
//		para2.add(descri1);
			
//		}
			
	/*****************************************End product Description************************************************************/		

		
		} 
		catch (DocumentException e) {
			e.printStackTrace();
		}
	  
	  int firstBreakPoint=5;
		int cnt= this.billingChargesLis.size()+this.billingTaxesLis.size();
		System.out.println("cnt value==="+cnt);
		if(cnt > firstBreakPoint){
      
      otherchargestoAnnexur();
		}
		 logger.log(Level.SEVERE,"End of  footer Info");
}





private void otherchargestoAnnexur() {
	
	 logger.log(Level.SEVERE,"otherchargestoAnnexur Info");
	
	Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
	String charge ="Other charge details";
	Chunk prodchunk = new Chunk(charge,font10bold);
	Paragraph parag= new Paragraph();
	parag.add(prodchunk);
	
	
	PdfPTable chargetable = new PdfPTable(2);
	chargetable.setWidthPercentage(100);
	//********************
	double total=0;
	 
	 for(int i=0;i<this.billingChargesLis.size();i++)
	      {
	   	   Phrase chunk = null;
	   	Phrase chunk1 = new Phrase("",font8);
	   	   double chargeAmt=0;
	   	   PdfPCell pdfchargeamtcell = null;
	   	   PdfPCell pdfchargecell = null;
	   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
	   		   pdfchargecell=new PdfPCell(chunk);
	   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
	   		   chunk=new Phrase(df.format(chargeAmt),font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  	      	   pdfchargeamtcell.setBorder(0);
//	   	   }else{
//	   		pdfchargecell=new PdfPCell(chunk1);
//	   		pdfchargeamtcell = new PdfPCell(chunk1);
	   	   }
	   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
	   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
	   		   pdfchargecell=new PdfPCell(chunk);
	   		   chargeAmt=billingChargesLis.get(i).getTaxChargeAbsVal();
	   		   chunk=new Phrase(chargeAmt+"",font8);
	  	      	   pdfchargeamtcell = new PdfPCell(chunk);
	  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  	      	   pdfchargeamtcell.setBorder(0);
//	   	   }else{
//	   		pdfchargecell=new PdfPCell(chunk1);
//	   		pdfchargeamtcell = new PdfPCell(chunk1);
	  	      	   
	  	      	total=total+chargeAmt;   
	   	   }
	 	       pdfchargecell.setBorder(0);
	 	      pdfchargeamtcell.setBorder(0);
	 	     chargetable.addCell(pdfchargecell);
	 	      chargetable.addCell(pdfchargeamtcell);
	      }
	 
	 Phrase totalchunk = new Phrase("Total                                                                      "+
	 "                                       "+df.format(total),font10bold);
		PdfPCell totalcell= new PdfPCell();
		totalcell.addElement(totalchunk);
	 
	 PdfPCell chargecell= new PdfPCell();
	 chargecell.addElement(chargetable);
	 chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 
	 PdfPTable parent= new PdfPTable(1);
	 parent.setWidthPercentage(70);
	 parent.setSpacingBefore(10f);
	 parent.addCell(chargecell);
	 parent.addCell(totalcell);
	 parent.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 try {
		    document.add(Chunk.NEXTPAGE);
			document.add(parag);
			document.add(parent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
	
	 logger.log(Level.SEVERE," end of otherchargestoAnnexur Info");
}





private static final String[] tensNames = { "", " Ten", " Twenty", " Thirty", " Fourty", " Fifty", " Sixty", " Seventy", " Eighty", " Ninety" };

    private static final String[] numNames = { "", " One", " Two", " Three", " Four", " Five", " Six", " Seven", " Eight", " Nine", " Ten", " Eleven", " Twelve",
    	" Thirteen", " Fourteen", " Fifteen", " Sixteen", " Seventeen", " Eighteen", " Nineteen" };

//    public static String convert(int number){

    private static String convertLessThanOneThousand(int number) {
    	String soFar;
    	if (number % 100 < 20){ soFar = numNames[number % 100]; number /= 100; } else { soFar = numNames[number % 10]; number /= 10;
    	soFar = tensNames[number % 10] + soFar; number /= 10; } if (number == 0) return soFar; return numNames[number] + " Hundred" + soFar; 
    	}
    	public static  String convert(double number) {
    	// 0 to 999 999 999 999
    	if (number == 0) { return "Zero"; }
    	String snumber = Double.toString(number);
    	// pad with "0"
    	String mask = "000000000000"; DecimalFormat df = new DecimalFormat(mask); snumber = df.format(number);
    	int hyndredCrore = Integer.parseInt(snumber.substring(3,5));
    	int hundredLakh = Integer.parseInt(snumber.substring(5,7));
    	int hundredThousands = Integer.parseInt(snumber.substring(7,9));
    	int thousands = Integer.parseInt(snumber.substring(9,12));
    	String tradBillions;
    	switch (hyndredCrore) { case 0: tradBillions = ""; break; case 1 : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; break; default : tradBillions = convertLessThanOneThousand(hyndredCrore) + " Crore "; }

    	String result = tradBillions;
    	String tradMillions;
    	switch (hundredLakh) { case 0: tradMillions = ""; break; case 1 : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; break; default : tradMillions = convertLessThanOneThousand(hundredLakh) + " Lakh "; }
    	result = result + tradMillions;
    	String tradHundredThousands;

    	switch (hundredThousands) { case 0: tradHundredThousands = ""; break; case 1 : tradHundredThousands = "One Thousand "; break; default : tradHundredThousands = convertLessThanOneThousand(hundredThousands) + " Thousand "; }
    	result = result + tradHundredThousands;

    	String tradThousand;
    	tradThousand = convertLessThanOneThousand(thousands);
    	result = result + tradThousand;return result.replaceAll("^\\s+", "").replaceAll("file://b//s%7B2,%7D//b", " "); 
    	}
    	
    	
    	//
    	
    	List<ProductDetailsPO> productcount =new ArrayList<ProductDetailsPO>();
  	  //
  	  	    public List<ProductDetailsPO> getdata(){
  	  	    	
  	  	    	ProductDetailsPO po=new ProductDetailsPO();
  	  	    	po.setProductCategory("category");
//  	  	    	po.setPrduct("Product");
  	  	    	po.setProductQuantity(20);
  	  	    	po.setTax(65);
  	  	    	po.setVat(55);
  	  	    	po.setDiscount(22);
//  	  	    	po.setTotal(50);
  	  	    	productcount.add(po);
  	  	    	return productcount;
  	  	    }
    	
    	//
    	public void settingRemainingRowsToPDF(int flag){
    		
    		 logger.log(Level.SEVERE,"setting Remaining Rows To PDFInfo");
    		 logger.log(Level.SEVERE,"Flag Value in service invoice pdf 2"+flag);
    		PdfPTable table = new PdfPTable(8);
    		try {
    			table.setWidths(columnWidths8);
    		} catch (DocumentException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
    		table.setWidthPercentage(100);
    		
    		
    		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
			Phrase productdetails= new Phrase("Product Details",font1);
			Paragraph para = new Paragraph();
			para.add(Chunk.NEWLINE);
			para.add(Chunk.NEWLINE);
			para.add(productdetails);
			para.setAlignment(Element.ALIGN_CENTER);
			para.add(Chunk.NEWLINE);
				
//			PdfPTable table = new PdfPTable(8);
//			table.setWidthPercentage(100);
			
			Phrase category = new Phrase("SR. NO. ",font1);
//			Phrase productcode = new Phrase("ITEM DETAILS",font1);
	        Phrase productname = new Phrase("ITEM DETAILS",font1);
	        Phrase qty = new Phrase("NO OF BRANCHES",font1);
	        Phrase unit = new Phrase("UNIT",font1);
	        Phrase rate= new Phrase ("RATE",font1);
	        
	        Phrase percDisc = new Phrase("DISC %",font1);
	        Phrase adddisc = new Phrase("ADDL.DISC %",font1);
	        
	        Phrase servicetax= null;
	        Phrase servicetax1= null;
	        int flag12=0;
	        int vat=0;
	        int st=0;
	        for(int i=0; i<salesProd.size();i++){
	        	
	        if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()==0)){
	         servicetax = new Phrase("VAT %",font1);
	         vat=vat+1;
	         System.out.println("phrase value===="+servicetax.toString());
	        }
	        else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()==0))
	        {
	        	  servicetax = new Phrase("ST %",font1);
	        	  st=st+1;
	        }
	         else if((salesProd.get(i).getVatTax().getPercentage()>0)&&(salesProd.get(i).getServiceTax().getPercentage()>0)){
	        	  servicetax1 = new Phrase("VAT / ST %",font1);
	        	  flag12=flag12+1;
	        	  System.out.println("flag value;;;;;"+flag12);
	        }
	         else{
	        	 
	        	  servicetax = new Phrase("TAX %",font1);
	         }
	        }			        
	        Phrase total = new Phrase("AMOUNT",font1);
			
	        logger.log(Level.SEVERE,"Flag Value in service invoice pdf 1"+flag);
			 PdfPCell cellcategory = new PdfPCell(category);
			 cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 PdfPCell cellproductcode = new PdfPCell(productcode);
	         PdfPCell cellproductname = new PdfPCell(productname);
	         cellproductname.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellqty = new PdfPCell(qty);
	         cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellunit = new PdfPCell(unit);
	         cellunit.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell cellrate = new PdfPCell(rate);
	         cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
	         PdfPCell percDisccell = new PdfPCell(percDisc);
	         percDisccell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	         PdfPCell cellservicetax = new PdfPCell(tax);
//	         cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
//	         PdfPCell cellvat = new PdfPCell(vat);
	         
	         PdfPCell cellservicetax1= null;
	         PdfPCell cellservicetax= null;
	         PdfPCell cellservicetax2= null;
	         Phrase servicetax2=null;
	         
	         if(flag>0){
	          cellservicetax1 = new PdfPCell(servicetax1);
	         cellservicetax1.setHorizontalAlignment(Element.ALIGN_CENTER);
	         }
	         else if(vat>0 && st>0)
	         {
	        	 System.out.println("in side condition");
	        	 servicetax2 = new Phrase("VAT / ST %",font1);
	        	 cellservicetax2 = new PdfPCell(servicetax2);
	             cellservicetax2.setHorizontalAlignment(Element.ALIGN_CENTER);
	         }
	         else {
	        	  cellservicetax = new PdfPCell(servicetax);
	             cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
	         }
	         
	         
	         
	         PdfPCell celltotal= new PdfPCell(total);
	         celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
	         
	         table.addCell(cellcategory);
//	         table.addCell(cellproductcode);
	         table.addCell(cellproductname);
	         table.addCell(cellqty);
	         table.addCell(cellunit);
	         table.addCell(cellrate);
	         table.addCell(percDisccell);
	         
	         if(flag>0){
	             table.addCell(cellservicetax1);
	             }else if(vat>0 && st>0){
	            	  table.addCell(cellservicetax2);
	             }else
	             {
	            	 table.addCell(cellservicetax);
	             }
	         table.addCell(celltotal);
	         logger.log(Level.SEVERE,"Flag Value in service invoice pdf"+flag+"===="+this.salesProd.size());
    		for (int i = flag; i < this.salesProd.size(); i++) {
    			
    			Phrase chunk=new Phrase((i+1)+"",font8);
	        	 PdfPCell pdfcategcell = new PdfPCell(chunk);
	        	 pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
//	        	 chunk=new Phrase(salesProd.get(i).getProdCode(),font8);
//	        	 PdfPCell pdfcpcodecell = new PdfPCell(chunk);
	        	 logger.log(Level.SEVERE,"Flag Value in service invoice pdf"+flag+"===="+salesProd.get(i).getProdName());
	        	 chunk = new Phrase(salesProd.get(i).getProdName(),font8);
	        	 PdfPCell pdfnamecell = new PdfPCell(chunk);
	        	 
	        	 chunk = new Phrase(salesProd.get(i).getQuantity()+"",font8);
	        	 PdfPCell pdfqtycell = new PdfPCell(chunk);
	        	 pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
	        	 chunk = new Phrase(salesProd.get(i).getUnitOfMeasurement(),font8);
	        	 PdfPCell pdfunitcell = new PdfPCell(chunk);
	        	 pdfunitcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 
	        	 chunk = new Phrase(df.format(salesProd.get(i).getPrice()),font8);
	        	 PdfPCell pdfspricecell = new PdfPCell(chunk);
	        	 pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        	 
	         	 if(salesProd.get(i).getProdPercDiscount()!=0)
	          	    chunk = new Phrase(salesProd.get(i).getProdPercDiscount()+"",font8);
	          	 else
	          		 chunk = new Phrase("0"+"",font8);
	          	 PdfPCell pdfperdiscount = new PdfPCell(chunk);
	          	pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
	          	
	          	 PdfPCell pdfservice =null;
	        	 Phrase vatno=null;
	        	 Phrase stno=null;
	        	 if((salesProd.get(i).getVatTax().getPercentage()==0)&&(salesProd.get(i).getServiceTax().getPercentage()>0))
	        	 {
	        	 
	        		 if(salesProd.get(i).getServiceTax().getPercentage()!=0){
	        		 if(st==salesProd.size()){
	        			 chunk = new Phrase(salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	        		 }
	        		 else{
	        			 stno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage()+"",font8);
	        		 }
	        		 }
	        	    else
	        		 chunk = new Phrase("N.A"+"",font8);
	        		 if(st==salesProd.size()){
	        	  pdfservice = new PdfPCell(chunk);
	        		 }else{
	        			 pdfservice = new PdfPCell(stno);
	        		 }
	        	  
	        	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 }
	        	 else if((salesProd.get(i).getServiceTax().getPercentage()==0)&&(salesProd.get(i).getVatTax().getPercentage()>0))
	             {
	        		 
	        		 if(salesProd.get(i).getVatTax()!=null){
	        			 if(vat==salesProd.size()){
	        				 System.out.println("rohan=="+vat);
	        				 chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+"",font8);
	        			 }else{
	        				 System.out.println("mukesh");
	             	    vatno = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+salesProd.get(i).getServiceTax().getPercentage(),font8);
	        		 }
	        		 }
	             	    else{
	             		 chunk = new Phrase("N.A"+"",font8);
	             	    }
	        		 if(vat==salesProd.size()){
	             	  pdfservice = new PdfPCell(chunk);
	        		 }else{ pdfservice = new PdfPCell(vatno);}
	             	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	        	 }
	        	 else if((salesProd.get(i).getServiceTax().getPercentage()>0)&&(salesProd.get(i).getVatTax().getPercentage()>0)){
	        		 if((salesProd.get(i).getVatTax()!=null)&& (salesProd.get(i).getServiceTax()!=null))
	        			 
	              	    chunk = new Phrase(salesProd.get(i).getVatTax().getPercentage()+""+" / "+""
	              	    +salesProd.get(i).getServiceTax().getPercentage(),font8);
	              	 else
	              		 chunk = new Phrase("N.A"+"",font8);
	              	  pdfservice = new PdfPCell(chunk);
	              	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	        		 
	        	 }else {
	        		 
	        		 chunk = new Phrase("N.A"+"",font8);
	              	  pdfservice = new PdfPCell(chunk);
	              	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	        		 
	        	 }
	         	
	        	 chunk = new Phrase(df.format(salesProd.get(i).getTotalAmount()),font8);
	        	 PdfPCell pdftotalproduct = new PdfPCell(chunk);
	        	 pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
	        	 
	        	 
	        	 table.addCell(pdfcategcell);
//	        	 table.addCell(pdfcpcodecell);
	        	 table.addCell(pdfnamecell);
	        	 table.addCell(pdfqtycell);
	        	 table.addCell(pdfunitcell);
	        	 table.addCell(pdfspricecell);
	        	 table.addCell(pdfperdiscount);
	        	 table.addCell(pdfservice);
	        	 table.addCell(pdftotalproduct);
	        	 try {
					table.setWidths(columnWidths);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
	        	 
    			
	        		flag=flag+1;
	    			if(flag==56||flag==116||flag==176){
	    			break;
	    			}
    		}
    		
    		PdfPTable parentTableProd=new PdfPTable(1);
    	    parentTableProd.setWidthPercentage(100);
    	    parentTableProd.setSpacingBefore(10f);
    	    
    	    PdfPCell prodtablecell=new PdfPCell();
    	    prodtablecell.addElement(table);
    	    prodtablecell.setBorder(0);
    	    parentTableProd.addCell(prodtablecell);
    	   
    		
    		try {
    			document.add(parentTableProd);
    			
    		} catch (DocumentException e) {
    			e.printStackTrace();
    		}
    		
    		 logger.log(Level.SEVERE,"end of setting Remaining Rows To PDFInfo");
    	}
    	
    	public String getInvComment() throws Exception
    	{
    		logger.log(Level.SEVERE,"get invoice 1.....");
    		String commnt="";
    		if(invoiceentity!=null){
    			logger.log(Level.SEVERE,"get invoice 2.....");
    			commnt=invoiceentity.getComment();
    			logger.log(Level.SEVERE,"get invoice 3.....");
    		}
    		logger.log(Level.SEVERE,"get invoice 4.....");
    		return commnt;
    		
    	}
    	
    	public void productDetailsForEmail()
    	{
    		 for(int i=0;i<con.getItems().size();i++){
 				
 				if(invoiceentity.getCompanyId()!=null){
 					SuperProduct sup=ofy().load().type(SuperProduct.class).filter("companyId",invoiceentity.getCompanyId()).
 					filter("count", con.getItems().get(i).getPrduct().getCount()).first().now();
 				
 					
 					SuperProduct superprod= new SuperProduct();
 					
 					superprod.setCount(sup.getCount());
 					superprod.setProductName(sup.getProductName());
 					superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
 					
 					stringlis.add(superprod);
 				
 				}
 				}
 		 
    	}
    	
    	
    	public boolean findEqualPayTerm(int payDay)
    	{
    		int ctr=0;
    		for(int c=0;c<payTerms.size();c++)
    		{
    			if(payTerms.get(c).getPayTermDays()==payDay)
    			{
    				ctr=ctr+1;
    			}
    		}
    		
    		if(ctr>0)
    		{
    			return true;
    		}
    		else
    		{
    			return false;
    		}
    	}
    	
    //************************rohan method to return functional year ************	
    	
    	public String getFunnctionalYear(Date date)
    	{
    		
    		System.out.println("in side methos");
    		
    		
    		
    		 Calendar now = Calendar.getInstance();
    		 
    		 
    		 now.setTime(date);
//    		 now.add(Calendar.DATE, 360);
    		 
    	     int year=now.get(Calendar.YEAR);
    	     
    	     int month = now.get(Calendar.MONTH)+1;
    	     System.out.println("year "+year);
    	     System.out.println("Month "+month);
    	     
    	     
    	     String stmonthValue = comp.getStartMonth();
    	     int monthValue=0;
    	     
    	     if(stmonthValue.equals("JAN"))
    	     {
    	    	 monthValue=1;
    	     }
    	     if(stmonthValue.equals("FEB"))
    	     {
    	    	 monthValue=2;
    	     }
    	     if(stmonthValue.equals("MAR"))
    	     {
    	    	 monthValue=3;
    	     }
    	     if(stmonthValue.equals("APR"))
    	     {
    	    	 monthValue=4;
    	     }
    	     if(stmonthValue.equals("MAY"))
    	     {
    	    	 monthValue=5;
    	     }
    	     if(stmonthValue.equals("JUN"))
    	     {
    	    	 monthValue=6;
    	     }
    	     if(stmonthValue.equals("JUL"))
    	     {
    	    	 monthValue=7;
    	     }
    	     if(stmonthValue.equals("AUG"))
    	     {
    	    	 monthValue=8;
    	     }
    	     if(stmonthValue.equals("SEP"))
    	     {
    	    	 monthValue=9;
    	     }
    	     if(stmonthValue.equals("OCT"))
    	     {
    	    	 monthValue=10;
    	     }
    	     if(stmonthValue.equals("NOV"))
    	     {
    	    	 monthValue=11;
    	     }
    	     if(stmonthValue.equals("DEC"))
    	     {
    	    	 monthValue=12;
    	     }
    	     
    	     System.out.println("month value form company "+monthValue);
    	     
    	     if (month < monthValue) {
    	         System.out.println("Financial Year : " + (year - 1) + "-" + year);
    	         functionalYear=((year - 1) + "-" + year);
    	     } else {
    	         System.out.println("Financial Year : " + year + "-" + (year + 1));
    	         functionalYear=year + "-" + (year + 1);
    	     }
    	     
    	     return functionalYear;
    	} 
    	
    	public boolean isMultipleBillingOfSameContract(){
    		if(billingDoc.size()>1){
				List<BillingDocumentDetails> billtablelis = billingDoc;
				
				int counter=0;
				int contractId=billtablelis.get(0).getOrderId();
				for(int i=0;i<billtablelis.size();i++){
					if(billtablelis.get(i).getOrderId()==contractId){
						counter++;
					}
				}
				
				if(billtablelis.size()==counter)
				{
					return true;
				}
    		}
			return false;
		}
    	
    	
    	
  //*****************************changes ends here *****************************  	
    	

    	private void termsConditionsInfoForMultipleBilling() {
    		
    		System.out.println("RRRRRRRRRROOOOOHHHAAAANNNNNNN");
    		
  		  Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
  		  Phrase blankval=new Phrase(" ",font8);
  		  Phrase payphrase=new Phrase("Payment Terms :",font10bold);
  			payphrase.add(Chunk.NEWLINE);
  			payphrase.add(Chunk.NEWLINE);
  			PdfPTable table = new PdfPTable(3);
  			table.setWidthPercentage(100);
//  			table.setSpacingAfter(50f);
  			
  			
  			PdfPCell tablecell1 = new PdfPCell();
  			 if(this.payTermsLis.size()>5){
  				 tablecell1= new PdfPCell(table);
//  				 tablecell1.setBorder(0);
  			        }else{
  			        	tablecell1= new PdfPCell(table);
  			        	tablecell1.setBorder(0);
  			        }
  			
  			
  			
  			
  			Phrase paytermdays = new Phrase("Days",font1);
  			Phrase paytermpercent = new Phrase("Percent",font1);
  	      Phrase paytermcomment = new Phrase("Comment",font1);
  	    
  	      PdfPCell headingpayterms = new PdfPCell(payphrase);
  	      headingpayterms.setBorder(0);
  	      headingpayterms.setColspan(3);
  		    PdfPCell celldays = new PdfPCell(paytermdays);
  		    celldays.setBorder(0);
  		    celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
  			PdfPCell cellpercent = new PdfPCell(paytermpercent);
  			cellpercent.setBorder(0);
  			cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
  		    PdfPCell cellcomment = new PdfPCell(paytermcomment);
  		    cellcomment.setBorder(0);
  		    cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
//  			table.addCell(headingpayterms);
  	      table.addCell(celldays);
  	      table.addCell(cellpercent);
  	      table.addCell(cellcomment);
  		  
  	      
  	      if( this.payTermsLis.size()<= BreakPoint){
  				int size =  BreakPoint - this.payTermsLis.size();
  				      blankLines = size*(80/5);
  				  	System.out.println("blankLines size ="+blankLines);
  				System.out.println("blankLines size ="+blankLines);
  					}
  					else{
  						blankLines = 0;
  					}
  					table.setSpacingAfter(blankLines);	
  	      
  	      System.out.println("this.F.size()"+this.payTermsLis.size());
  	      
  	      PdfPCell table1cell = new PdfPCell();
  	      
  	      if(paytermsflag==true){
  	      
  	      for(int i=0;i<this.payTermsLis.size();i++)
  	      {
  	      	System.out.println(this.payTermsLis.size()+"  in for1");
//  	     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//  	     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//  	     	 pdfdayscell.setBorder(0);
  	      	
  	      	
  	      	
  	    	boolean validateEqualPayTerm=findEqualPayTerm(payTermsLis.get(i).getPayTermDays());
  			
  	    		
  	    		System.out.println(" pay percent in contract == "+payTermsLis.get(i).getPayTermPercent());
  	    		
  		    	
  	    		if(validateEqualPayTerm==true){
  		      		
  		      		Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8bold);
  			      	PdfPCell pdfdayscell = new PdfPCell(chunk);
  			      	pdfdayscell.setBorder(0);
  			     	  
  			     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
  			     	 	
  			     	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8bold);
  			     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
  			     	 pdfpercentcell.setBorder(0);
  			     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  			     	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8bold);
  			     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
  			     	 pdfcommentcell.setBorder(0);
  			     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  			     	
  			     	table.addCell(pdfdayscell);
  			     	table.addCell(pdfpercentcell);
  			     	table.addCell(pdfcommentcell);
  	    		
  	    	
  	      		
  	      	}else{
  	      		
  	      		System.out.println("ELSE-----");
  	      		
  	      		Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
  		      	PdfPCell pdfdayscell = new PdfPCell(chunk);
  		      	pdfdayscell.setBorder(0);
  		     	  
  		     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	 	
  		     	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8);
  		     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
  		     	 pdfpercentcell.setBorder(0);
  		     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
  		     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
  		     	 pdfcommentcell.setBorder(0);
  		     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	
  		     	table.addCell(pdfdayscell);
  		     	table.addCell(pdfpercentcell);
  		     	table.addCell(pdfcommentcell);
  	      		
  	      	}
  	      	
  	     	
  	     	count=i;
  	    	 ///
  	    	 if(count==4|| count==BreakPoint){
  	 				
  	 				flag1=BreakPoint;
  	 				break;
  	 	}
  	     	
  	     	}
  		  
  	    ///2nd table for pay terms start
  	      System.out.println(this.payTermsLis.size()+"  out");
  	      
//  	      if(this.payTermsLis.size()>4){
  	      	
  	     
  	      
  	      
  			PdfPTable table1 = new PdfPTable(3);
  			table1.setWidthPercentage(100);
  			
  			
  			
  			 if(this.payTermsLis.size()>5){
  				 table1cell= new PdfPCell(table1);
//  				 table1cell.setBorder(0);
  			        }else{
  			        	table1cell= new PdfPCell(blankval);
  			        	table1cell.setBorder(0);
  			        }
  			
  			Phrase paytermdays1 = new Phrase("Days",font1);
  			Phrase paytermpercent1 = new Phrase("Percent",font1);
  	      Phrase paytermcomment1 = new Phrase("Comment",font1);
  	    
  	      PdfPCell celldays1;
  	      PdfPCell cellpercent1;
  	      PdfPCell cellcomment1;
  	      
  	      System.out.println(this.payTermsLis.size()+" ...........b4 if");
  	      if(this.payTermsLis.size()>5){
  	      	System.out.println(this.payTermsLis.size()+" ...........af if");
  	       celldays1= new PdfPCell(paytermdays1);
  		    celldays1.setBorder(0);
  		    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
  	      }else{
  	      	celldays1= new PdfPCell(blankval);
  	  	    celldays1.setBorder(0);
  	      	
  	      }
  	      
  	      if(this.payTermsLis.size()>5){
  	      
  			 cellpercent1 = new PdfPCell(paytermpercent1);
  			cellpercent1.setBorder(0);
  			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
  	      }else{
  	      	cellpercent1 = new PdfPCell(blankval);
  	  		cellpercent1.setBorder(0);
  	      	
  	      }
  	      
  	      if(this.payTermsLis.size()>5){
  		    cellcomment1= new PdfPCell(paytermcomment1);
  		    cellcomment1.setBorder(0);
  		    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
  	      }else{
  	      	cellcomment1= new PdfPCell(blankval);
  	  	    cellcomment1.setBorder(0);
  	      }
  		
  		
  	      table1.addCell(celldays1);
  	      table1.addCell(cellpercent1);
  	      table1.addCell(cellcomment1);
  	     
  	   
  	      
  	      
  	      
  	      for(int i=5;i<this.payTermsLis.size();i++)
  	      {
  	      	System.out.println(this.payTermsLis.size()+"  in for");
  	      	
  	      	Phrase chunk = null;
//  	     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//  	     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//  	     	 pdfdayscell.setBorder(0);
  	    	if(payTermsLis.get(i).getPayTermDays()!=null){
  	     	  chunk = new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
  	    	}else{
  	    		chunk = new Phrase(" ",font8);
  	    	}
  	     	 	
  	     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
  	     	 	pdfdayscell.setBorder(0);
  	     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
  	     	 if(payTermsLis.get(i).getPayTermPercent()!=0){ 
  	     	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8);
  	     	 }else{
  	     		chunk = new Phrase(" ",font8);
  	     	 }
  	     	 
  	     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
  	     	 pdfpercentcell.setBorder(0);
  	     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  	     	if(payTermsLis.get(i).getPayTermComment()!=null){
  	     	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
  	     	}else{
  	     		chunk = new Phrase(" ",font8);
  	     	}
  	     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
  	     	 pdfcommentcell.setBorder(0);
  	     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  	     	
  	     	table1.addCell(pdfdayscell);
  	     	table1.addCell(pdfpercentcell);
  	     	table1.addCell(pdfcommentcell);
  	     	
  	    	 if(i==9|| count==BreakPoint){
  	 				
  	 				break;
  	    	 }
  	     	}
  	      
  	      ///2nd table for pay terms end
  	      
//  	      PdfPCell table1cell = new PdfPCell();
  	      table1cell.addElement(table1);
  	      
  	      
  	
  	      }else{
  		      
  		      for(int i=0;i<this.payTerms.size();i++)
  		      {
  		      	System.out.println(this.payTermsLis.size()+"  in for1");
//  		     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//  		     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//  		     	 pdfdayscell.setBorder(0);
  		    	
  		     	 	Phrase chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
  		     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
  		     	 	pdfdayscell.setBorder(0);
  		     	  
  		     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	 	
  		     	 chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
  		     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
  		     	 pdfpercentcell.setBorder(0);
  		     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	 chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
  		     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
  		     	 pdfcommentcell.setBorder(0);
  		     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     
  		     	table.addCell(pdfdayscell);
  		     	table.addCell(pdfpercentcell);
  		     	table.addCell(pdfcommentcell);
  		     	
  		     	
  		     	
  		     	count=i;
  		    	 ///
  		    	 if(count==4|| count==BreakPoint){
  		 				
  		 				flag1=BreakPoint;
  		 				break;
  		 	}
  		     	
  		     	
  		     	}
  			  
  		    ///2nd table for pay terms start
  		      System.out.println(this.payTerms.size()+"  out");
  		      
//  		      if(this.payTermsLis.size()>4){
  		      	
  		     
  		      
  		      
  				PdfPTable table1 = new PdfPTable(3);
  				table1.setWidthPercentage(100);
  				
  				
  				
  				 if(this.payTerms.size()>5){
  					 table1cell= new PdfPCell(table1);
//  					 table1cell.setBorder(0);
  				        }else{
  				        	table1cell= new PdfPCell(blankval);
  				        	table1cell.setBorder(0);
  				        }
  				
  				Phrase paytermdays1 = new Phrase("Days",font1);
  				Phrase paytermpercent1 = new Phrase("Percent",font1);
  		      Phrase paytermcomment1 = new Phrase("Comment",font1);
  		    
  		      PdfPCell celldays1;
  		      PdfPCell cellpercent1;
  		      PdfPCell cellcomment1;
  		      
  		      System.out.println(this.payTerms.size()+" ...........b4 if");
  		      if(this.payTermsLis.size()>5){
  		      	System.out.println(this.payTermsLis.size()+" ...........af if");
  		       celldays1= new PdfPCell(paytermdays1);
  			    celldays1.setBorder(0);
  			    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
  		      }else{
  		      	celldays1= new PdfPCell(blankval);
  		  	    celldays1.setBorder(0);
  		      	
  		      }
  		      
  		      if(this.payTerms.size()>5){
  		      
  				 cellpercent1 = new PdfPCell(paytermpercent1);
  				cellpercent1.setBorder(0);
  				cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
  		      }else{
  		      	cellpercent1 = new PdfPCell(blankval);
  		  		cellpercent1.setBorder(0);
  		      	
  		      }
  		      
  		      if(this.payTerms.size()>5){
  			    cellcomment1= new PdfPCell(paytermcomment1);
  			    cellcomment1.setBorder(0);
  			    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
  		      }else{
  		      	cellcomment1= new PdfPCell(blankval);
  		  	    cellcomment1.setBorder(0);
  		      }
  			
  			
  		      table1.addCell(celldays1);
  		      table1.addCell(cellpercent1);
  		      table1.addCell(cellcomment1);
  		     
  		   
  		      
  		      
  		      
  		      for(int i=5;i<this.payTerms.size();i++)
  		      {
  		      	System.out.println(this.payTerms.size()+"  in for");
  		      	
  		      	Phrase chunk = null;
//  		     	 Phrase chunk=new Phrase("Day: "+payTermsLis.get(i).getPayTermDays()+"                "+"Percent: "+payTermsLis.get(i).getPayTermPercent()+"               "+"Comment: "+payTermsLis.get(i).getPayTermComment().trim(),font8);
//  		     	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//  		     	 pdfdayscell.setBorder(0);
  		    	if(payTerms.get(i).getPayTermDays()!=null){
  		     	  chunk = new Phrase(payTerms.get(i).getPayTermDays()+"",font8);
  		    	}else{
  		    		chunk = new Phrase(" ",font8);
  		    	}
  		     	 	
  		     	 	PdfPCell pdfdayscell = new PdfPCell(chunk);
  		     	 	pdfdayscell.setBorder(0);
  		     	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	 if(payTerms.get(i).getPayTermPercent()!=0){ 
  		     	 chunk=new Phrase(payTerms.get(i).getPayTermPercent()+"",font8);
  		     	 }else{
  		     		chunk = new Phrase(" ",font8);
  		     	 }
  		     	 
  		     	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
  		     	 pdfpercentcell.setBorder(0);
  		     	pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	if(payTerms.get(i).getPayTermComment()!=null){
  		     	 chunk = new Phrase(payTerms.get(i).getPayTermComment().trim(),font8);
  		     	}else{
  		     		chunk = new Phrase(" ",font8);
  		     	}
  		     	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
  		     	 pdfcommentcell.setBorder(0);
  		     	pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
  		     	
  		     	table1.addCell(pdfdayscell);
  		     	table1.addCell(pdfpercentcell);
  		     	table1.addCell(pdfcommentcell);
  		     	
  		    	 if(i==9|| count==BreakPoint){
  		 				
  		 				break;
  		    	 }
  		     	}
  		      
  		      
  		      
  		      ///2nd table for pay terms end
  		      
  		      table1cell.addElement(table1);
  	      } 
  	      
  	      PdfPTable termstable1 = new PdfPTable(2);
  	      termstable1.setWidthPercentage(100);
  	      termstable1.addCell(headingpayterms);
  	      termstable1.addCell(tablecell1);
  	      termstable1.addCell(table1cell);
  	      
  	  
  	      
  	      
  		   PdfPTable chargetaxtable = new PdfPTable(2);
  		   chargetaxtable.setWidthPercentage(100);
  		   chargetaxtableToNewPage.setWidthPercentage(100);
  		   
  		   Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
  			PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
  			totalAmtCell.setBorder(0);
  			double totalAmt = 0;
  				totalAmt = con.getTotalAmount();

  			Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
  			PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
  			realtotalAmtCell.setBorder(0);
  			realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  			chargetaxtable.addCell(totalAmtCell);
  			chargetaxtable.addCell(realtotalAmtCell);
  		   
  			List<String> myList11 = new ArrayList<>();
  			List<String> myList22 = new ArrayList<>();
  			List<String> myList33 = new ArrayList<>();
  			List<String> myList44 = new ArrayList<>();
  			
  	//*****************rohan changes here on 5/6/15 for part payment *****************
  				 
  			 for(int i=0;i<this.contractTaxesLis.size();i++)
  		      {
  		    	  
  		   	  if(contractTaxesLis.get(i).getChargePercent()!=0){
  		   	  
  		   		  if(contractTaxesLis.get(i).getChargeName().equals("VAT")||contractTaxesLis.get(i).getChargeName().equals("CST")){
  					
  					System.out.println("1st loop"+contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent());
  					
  					String str = contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent();
  					double taxAmt1 = contractTaxesLis.get(i).getChargePercent()
  							* contractTaxesLis.get(i).getAssessableAmount() / 100;
  					
  					 myList11.add(str);
  					 myList22.add(df.format(taxAmt1));
  					 
  					 System.out.println("Size of mylist1 is () === "+myList11.size());
  				}
  				
  				if(contractTaxesLis.get(i).getChargeName().equals("Service Tax")){
  					
  					System.out.println("2nd loop == "+contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent());
  					
  					String str = contractTaxesLis.get(i).getChargeName().trim()+ " @ "+ contractTaxesLis.get(i).getChargePercent();
  					double taxAmt1 = contractTaxesLis.get(i).getChargePercent()
  							* contractTaxesLis.get(i).getAssessableAmount() / 100;
  					String ss=" ";
  					 myList33.add(str);
  					 myList44.add(df.format(taxAmt1));
  					 myList44.add(ss);
  					 System.out.println("Size of mylist2 is () === "+myList22.size());
  				}
  		   	  }
  		    }
  				 
  				 PdfPCell pdfservicecell=null;
  					
  					PdfPTable other1table=new PdfPTable(1);
  					other1table.setWidthPercentage(100);
  					for(int j=0;j<myList11.size();j++){
  						chunk = new Phrase(myList11.get(j),font8);
  						 pdfservicecell = new PdfPCell(chunk);
  						pdfservicecell.setBorder(0);
  						pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
  						other1table.addCell(pdfservicecell);
  						
  					}
  					
  					
  					
  					PdfPCell pdfservicecell1=null;
  					
  					PdfPTable other2table=new PdfPTable(1);
  					other2table.setWidthPercentage(100);
  					for(int j=0;j<myList22.size();j++){
  						chunk = new Phrase(myList22.get(j),font8);
  						 pdfservicecell1 = new PdfPCell(chunk);
  						pdfservicecell1.setBorder(0);
  						pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
  						other2table.addCell(pdfservicecell1);
  						
  					}
  					
  					PdfPCell chargescell=null;
  					for(int j=0;j<myList33.size();j++){
  						chunk = new Phrase(myList33.get(j),font8);
  						chargescell = new PdfPCell(chunk);
  						chargescell.setBorder(0);
  						chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
  						other1table.addCell(chargescell);
  						
  					}
  					
  					PdfPCell chargescell1=null;
  					for(int j=0;j<myList44.size();j++){
  						chunk = new Phrase(myList44.get(j),font8);
  						chargescell1 = new PdfPCell(chunk);
  						chargescell1.setBorder(0);
  						chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
  						other2table.addCell(chargescell1);
  						
  					}
  					
  					
  					
  					PdfPCell othercell = new PdfPCell();
  					othercell.addElement(other1table);
  					othercell.setBorder(0);
  					chargetaxtable.addCell(othercell);
  					
  					PdfPCell othercell1 = new PdfPCell();
  					othercell1.addElement(other2table);
  					othercell1.setBorder(0);
  					chargetaxtable.addCell(othercell1);
  				 
  					
  				 for(int i=0;i<this.invoiceentity.getBillingOtherCharges().size();i++)
  			      {
  			   	   Phrase chunk = null;
  			   	Phrase chunk1 = new Phrase("",font8);
  			   	   double chargeAmt=0;
  			   	   PdfPCell pdfchargeamtcell = null;
  			   	   PdfPCell pdfchargecell = null;
  			   	   if(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent()!=0){
  			   		   System.out.println(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+".......charge name"+"@"+contractChargesLis.get(i).getChargePercent());
  			   		   chunk = new Phrase(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+" @ "+contractChargesLis.get(i).getChargePercent(),font1);
  			   		   pdfchargecell=new PdfPCell(chunk);
  			   		pdfchargecell.setBorder(0);
  			   		pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
  			   		   chargeAmt=this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargePercent()*this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAssesVal()/100;
  			   		   chunk=new Phrase(df.format(chargeAmt),font8);
  			  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  			  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  			  	      	   pdfchargeamtcell.setBorder(0);
  			   	   }else{
  			   		pdfchargecell=new PdfPCell(chunk1);
  			   		pdfchargeamtcell = new PdfPCell(chunk1);
  			   	   }
  				      
  			   	PdfPCell pdfchargeamtcell1 = null;
  				   PdfPCell pdfchargecell1 = null;
  			   	   
  			   	   if(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAbsVal()!=0){
  			   		   chunk = new Phrase(this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeName()+"",font1);
  			   		pdfchargecell1=new PdfPCell(chunk);
  			   		chargeAmt=this.invoiceentity.getBillingOtherCharges().get(i).getTaxChargeAbsVal();
  			   		   chunk=new Phrase(chargeAmt+"",font8);
  			   		pdfchargeamtcell1 = new PdfPCell(chunk);
  			   		pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
  			   		pdfchargeamtcell1.setBorder(0);
  			   	    }else{
  			   		pdfchargecell1=new PdfPCell(chunk1);
  			   		pdfchargeamtcell1 = new PdfPCell(chunk1);
  			   	    }
  			 	      pdfchargecell.setBorder(0);
  			 	      pdfchargeamtcell.setBorder(0);
  				      pdfchargecell1.setBorder(0);
  				      pdfchargeamtcell1.setBorder(0);
  				      
  				       chargetaxtable.addCell(pdfchargecell1);
  				       chargetaxtable.addCell(pdfchargeamtcell1);
  			 	       chargetaxtable.addCell(pdfchargecell);
  			 	       chargetaxtable.addCell(pdfchargeamtcell);
  			 	       
  			      }  
  				 
  				
  				
  				//*******************************changes  here*********************
  				Phrase netname = new Phrase("Grand Total",font8);
  				PdfPCell netnamecell = new PdfPCell(netname);
  				netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
  				netnamecell.setBorder(0);
  				chargetaxtable.addCell(netnamecell);
  			 
  				
  				  			 	double netpayable=0;
  			 	netpayable = con.getNetpayable();
  				String netpayableamt=netpayable+"";
  				System.out.println("total============"+netpayableamt);
  				Phrase netpay = new Phrase(netpayableamt,font8);
  				PdfPCell netpaycell = new PdfPCell(netpay);
  				netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  				namephasevaluecell.addElement(namePhasevalue);
  				netpaycell.setBorder(0);
  				chargetaxtable.addCell(netpaycell);
  			 
  			 
  				Phrase blankSpace = new Phrase(" ",font8);
  				PdfPCell blankSpacecell = new PdfPCell(blankSpace);
  				blankSpacecell.setBorder(0);
  				chargetaxtable.addCell(blankSpacecell);
  				chargetaxtable.addCell(blankSpacecell);
  				
  				
  				
//  				double flatDics=0;
//  				
//  				 for(int i=0;i<billEntity.getSalesOrderProducts().size();i++){
//  						if(billEntity.getSalesOrderProducts().get(i).getFlatDiscount()!=0)
//  						{
//  							flatDics =flatDics+ billEntity.getSalesOrderProducts().get(i).getFlatDiscount();
//  						}
//  					
//  				 }
  				 
  				//******************8888888888888888888888888888888888888888888888888888
  				
//  				rohan
  				
  				ArrayList<BillingIdFlatDis> billFlatList=new ArrayList<BillingIdFlatDis>();
  				
   				double totalDisc=0;
  				 for(int z=0;z<salesProd.size();z++){	 
							 
						 //*****************rohan added this code for disc Amt ************8
  					 
//						 if(salesProd.get(z).getFlatDiscount()!=0)
//						 {
							 
							 if(billFlatList.size()==0){
								 BillingIdFlatDis billobj=new BillingIdFlatDis();
								 billobj.setBillingId(salesProd.get(z).getBiilingId());
								 billobj.setFlatAmt(salesProd.get(z).getFlatDiscount());
								 billFlatList.add(billobj);
								 
//								 totalDisc = totalDisc + salesProd.get(z).getFlatDiscount();
								 
								 
							 }else{
								 boolean flag=false;
								 
								 for(int i=0;i<billFlatList.size();i++){
									 if(billFlatList.get(i).getBillingId()==salesProd.get(z).getBiilingId()){
										 double disc=billFlatList.get(i).getFlatAmt();
										 disc=disc+salesProd.get(z).getFlatDiscount();
										 
										 billFlatList.get(i).setFlatAmt(disc);
										 flag=true;
									 }
								 }
								 
								 if(flag==false){
									 BillingIdFlatDis billobj=new BillingIdFlatDis();
									 billobj.setBillingId(salesProd.get(z).getBiilingId());
									 billobj.setFlatAmt(salesProd.get(z).getFlatDiscount());
									 billFlatList.add(billobj);
								 }
							 }
//						}
  				 }
  				 
  				 
  				 System.out.println("billFlatList "+billFlatList.size());
  				 
  				 double pay=0;
				 for(int i=0;i<payTerms.size();i++){
						
					 pay=payTerms.get(i).getPayTermPercent();
				 }
				 
				 ArrayList<Integer> billIDLIs = new ArrayList<Integer>();
				 billIDLIs.addAll(getBillingIds());
				 System.out.println("billIDLIs"+billIDLIs.size());
  				 System.out.println("Pay "+pay);
				 if(pay != 100){
  				//***************************changes ends here ******************************************			
  					
  					//**********************rohan changes here on 26/5/15 for part payments********************************		
  					System.out.println("billEntity.getArrPayTerms().size==========="+invoiceentity.getArrayBillingDocument().size());
  					
  					System.out.println("contract count========="+invoiceentity.getContractCount());
  					System.out.println("order type======"+invoiceentity.getTypeOfOrder().trim());
  					
  					
  					
//  					if(invoiceentity.getArrayBillingDocument().size()>1)
//  					{
  						 for(int k=0;k<this.payTerms.size();k++)
  					      {
  							 	List<String> myLis1 = new ArrayList<>();
  								List<String> myLis2 = new ArrayList<>();
  								List<String> myLis3 = new ArrayList<>();
  								List<String> myLis4 = new ArrayList<>();
  							 
  							 System.out.println("11111111111111111111111111"+payTerms.get(k).getPayTermPercent());
  							 System.out.println("22222222222222222222222222"+conbillingTaxesLis.get(k).getPayPercent());
  							 
//  							 if(payTerms.get(k).getPayTermPercent().equals(billingTaxesLis.get(k).getPaypercent())){
  							
  							 String payterm=this.conbillingTaxesLis.get(k).getPayPercent()+"";
  							 chunk= new Phrase("Pay Terms "+payterm+"%",font8bold);
  							 PdfPCell paytermCell = new PdfPCell(chunk);
  							 paytermCell.setBorder(0);
  							 chargetaxtable.addCell(paytermCell);
  							 
  							 double total =0;
  								
  							total = ((con.getTotalAmount()*payTerms.get(k).getPayTermPercent())/100);
  							
  							Phrase totalphase = new Phrase(df.format(total),font8bold);
  							PdfPCell totalcell = new PdfPCell(totalphase);
  							totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  							totalcell.setBorder(0);
  							chargetaxtable.addCell(totalcell);
  							 
  							System.out.println("salesProd.size()"+salesProd.size());
//  							for(int c=0;c<billIDLIs.size();c++){	
  							
  							
  							/*************************************************/
  							
  							for(int i=0;i< billFlatList.size();i++)
  							{
  								if(k==i){
  								 Phrase billId= new Phrase("Billing ("+billFlatList.get(i).getBillingId()+")",font8);
	  							 PdfPCell billIdCell = new PdfPCell(billId);
	  							 billIdCell.setBorder(0);
	  							 chargetaxtable.addCell(billIdCell);
	  							 
	  							 Phrase blk= new Phrase("",font8);
	  							 PdfPCell blkCell = new PdfPCell(blk);
	  							 blkCell.setBorder(0);
	  							 blkCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	  							 chargetaxtable.addCell(blkCell);
	  							 
	  							Phrase discAmt= new Phrase("Flat Disc("+billFlatList.get(i).getFlatAmt()+")",font8);
 							    PdfPCell discAmtCell = new PdfPCell(discAmt);
 							    discAmtCell.setBorder(0);
 							    chargetaxtable.addCell(discAmtCell);
 							 
 							    Phrase discAmtValue= new Phrase(total-billFlatList.get(i).getFlatAmt()+"",font8);
 						    	PdfPCell discAmtValueCell = new PdfPCell(discAmtValue);
 							    discAmtValueCell.setBorder(0);
 							    discAmtValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
 							    chargetaxtable.addCell(discAmtValueCell);
  								}
  							}
  							
  							
  							
//  							 for(int z=0;z<salesProd.size();z++){	 
//  								
//  								 
//  							 //*****************rohan added this code for disc Amt ************8
//  							 if(salesProd.get(z).getFlatDiscount()!=0)
//  							 {
//  								if(billIDLIs.get(k)==salesProd.get(z).getBiilingId()){
//  	  								 Phrase billId= new Phrase("Billig ("+salesProd.get(z).getBiilingId()+")",font8);
//  	  	  							 PdfPCell billIdCell = new PdfPCell(billId);
//  	  	  							 billIdCell.setBorder(0);
//  	  	  							 chargetaxtable.addCell(billIdCell);
//  	  	  							 
//  	  	  							 Phrase blk= new Phrase("",font8);
//  	  	  							 PdfPCell blkCell = new PdfPCell(blk);
//  	  	  							 blkCell.setBorder(0);
//  	  	  							 blkCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  	  	  							 chargetaxtable.addCell(blkCell);
//  	  								 
//  	  							} 
//  								 
//  							 Phrase discAmt= new Phrase("Flat Disc("+totalDisc+")",font8);
//  							 PdfPCell discAmtCell = new PdfPCell(discAmt);
//  							 discAmtCell.setBorder(0);
//  							 chargetaxtable.addCell(discAmtCell);
//  							 
//  							 Phrase discAmtValue= new Phrase(total-totalDisc+"",font8);
//  							 PdfPCell discAmtValueCell = new PdfPCell(discAmtValue);
//  							 discAmtValueCell.setBorder(0);
//  							 discAmtValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  							 chargetaxtable.addCell(discAmtValueCell);
//  							 
//  							 
////  							 Phrase AmtDisc= new Phrase("Disc Amt",font8);
////  							 PdfPCell AmtDiscCell = new PdfPCell(AmtDisc);
////  							 AmtDiscCell.setBorder(0);
////  							 chargetaxtable.addCell(AmtDiscCell);
////  							 
////  							 
////  							 Phrase amtDiscValue= new Phrase(total-salesProd.get(z).getFlatDiscount()+"",font8);
////  							 PdfPCell amtDiscValueCell = new PdfPCell(amtDiscValue);
////  							 amtDiscValueCell.setBorder(0);
////  							 amtDiscValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
////  							 chargetaxtable.addCell(amtDiscValueCell);
//  							 				 
//  							//*****************rohan ***********************************
//  							}
//  							 
//  							 
//  							}
  							 
  							 
  							 /********************************************/
  							 
  							//***************changes ends here *********************** 
  							
  							  System.out.println("billingTaxesLis size"+billingTaxesLis.size());
  							  System.out.println("iiiiiiii   value    "+k); 
  					    	  System.out.println("billingTaxesLis.get(k).getChargesList().get(i) list "+conbillingTaxesLis.get(k).getChargesList().size());
  					    	 for(int i=0;i<this.conbillingTaxesLis.get(k).getChargesList().size();i++){
  					    	  
  					    	  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0){
  					    	  
  					    		  System.out.println("billingTaxesLis.get(i).getTaxChargeName()"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
  					    		  
  					    		  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST")){
  										
  										System.out.println("1st loop"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
  										
  										String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
  										double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
  												* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
  										
  										 myLis1.add(str);
  										 myLis2.add(df.format(taxAmt1));
  										 
  									}
  									
  									if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")){
  										
  										 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
  										
  										System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
  										
  										String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
  										double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
  												* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
  										String blidnk=" ";
  										 myLis3.add(str);
  										 myLis4.add(df.format(taxAmt1));
  										 myLis4.add(blidnk);
  										 
  									}
  					    	  }
  					    	 }
  					    	 
  					    //*************rohan closes here ************	 
//  							 }
//  							 }
  						 
  						 PdfPCell pdfservicecell123=null;
  							
  							PdfPTable other1table123=new PdfPTable(1);
  							other1table123.setWidthPercentage(100);
  							for(int j=0;j<myLis1.size();j++){
  								chunk = new Phrase(myLis1.get(j),font8);
  								pdfservicecell123 = new PdfPCell(chunk);
  								pdfservicecell123.setBorder(0);
  								pdfservicecell123.setHorizontalAlignment(Element.ALIGN_LEFT);
  								other1table123.addCell(pdfservicecell123);
  								
  							}
  							
  							PdfPCell pdfservicecell1123=null;
  							
  							PdfPTable other2table123=new PdfPTable(1);
  							other2table123.setWidthPercentage(100);
  							for(int j=0;j<myLis2.size();j++){
  								chunk = new Phrase(myLis2.get(j),font8);
  								pdfservicecell1123 = new PdfPCell(chunk);
  								pdfservicecell1123.setBorder(0);
  								pdfservicecell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
  								other2table123.addCell(pdfservicecell1123);
  								
  							}
  							
  							PdfPCell chargescell123=null;
  							for(int j=0;j<myLis3.size();j++){
  								chunk = new Phrase(myLis3.get(j),font8);
  								chargescell123 = new PdfPCell(chunk);
  								chargescell123.setBorder(0);
  								chargescell123.setHorizontalAlignment(Element.ALIGN_LEFT);
  								other1table123.addCell(chargescell123);
  								
  							}
  							
  							PdfPCell chargescell1123=null;
  							for(int j=0;j<myLis4.size();j++){
  								chunk = new Phrase(myLis4.get(j),font8);
  								chargescell1123 = new PdfPCell(chunk);
  								chargescell1123.setBorder(0);
  								chargescell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
  								other2table123.addCell(chargescell1123);
  								
  							}
  							
  							
  							
  							PdfPCell othercell123 = new PdfPCell();
  							othercell123.addElement(other1table123);
  							othercell123.setBorder(0);
  							chargetaxtable.addCell(othercell123);
  							
  							PdfPCell othercell1123 = new PdfPCell();
  							othercell1123.addElement(other2table123);
  							othercell1123.setBorder(0);
  							chargetaxtable.addCell(othercell1123);
//  					    	 }
  						 //*******ROHAN CHANGES HERE FOR PART PAYMENT ON 5/6/15******************
//  					      }
  							
  						 //**********************CHANGES ENDS HERE********************************
  						 
  						 
//  						 PdfPCell chargecell22 = null;
//  					      PdfPCell chargeamtcell22=null;
//  					      PdfPCell otherchargecell22=null;
//  						 if(payTerms.get(k).getPayTermPercent().equals(billingChargesLis.get(0).getPayPercent())){
  						 for(int j=0;j<this.billingChargesLis.size();j++)
  					      {
  							 
  					   	   Phrase chunk = null;
  					   	   Phrase chunk1 = new Phrase("",font8);
  					   	   double chargeAmt=0;
  					   	   PdfPCell pdfchargeamtcell = null;
  					   	   PdfPCell pdfchargecell = null;
  					   	
  					   	 System.out.println("88888888888888888888888888"+payTerms.get(k).getPayTermPercent());
  						 System.out.println("99999999999999999999999999"+conbillingChargesLis.get(j).getPayPercent());
  						 
  						 if(payTerms.get(k).getPayTermPercent().equals(conbillingChargesLis.get(j).getPayPercent())){
  					   	   
  					   	   
  					   	 for(int i=0;i<this.conbillingChargesLis.get(j).getOtherChargesList().size();i++){
  					    	  
  					   	   if(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargePercent()!=0){
  					   		   chunk = new Phrase(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeName()+" @ "+conbillingChargesLis.get(i).getOtherChargesList().get(i).getTaxChargePercent(),font1);
  					   		   pdfchargecell=new PdfPCell(chunk);
  					   		pdfchargecell.setBorder(0);
  					   		chargetaxtable.addCell(pdfchargecell);
  					 	     
//  					   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
  					   		 chargeAmt=conbillingChargesLis.get(j).getOtherChargesList().get(i).getPayableAmt();
  					   		   chunk=new Phrase(df.format(chargeAmt),font8);
  					  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  					  	      	 pdfchargeamtcell.setBorder(0);
  					  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  					  	      chargetaxtable.addCell(pdfchargeamtcell);
  					  	      	
  					  	      	  
  					   	   }else{
  					   		pdfchargecell=new PdfPCell(chunk1);
  					   		pdfchargecell.setBorder(0);
  					   		pdfchargeamtcell = new PdfPCell(chunk1);
  					   		pdfchargeamtcell.setBorder(0);
  					   		chargetaxtable.addCell(pdfchargecell);
  					   		chargetaxtable.addCell(pdfchargeamtcell);
  					   	   }
  					   	   if(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
  					   		   chunk = new Phrase(conbillingChargesLis.get(j).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
  					   		   pdfchargecell=new PdfPCell(chunk);
  					   		pdfchargecell.setBorder(0);
  					   		chargetaxtable.addCell(pdfchargecell);
  					   		   chargeAmt=conbillingChargesLis.get(j).getOtherChargesList().get(i).getPayableAmt();
  					   		   chunk=new Phrase(chargeAmt+"",font8);
  					  	      	   pdfchargeamtcell = new PdfPCell(chunk);
  					  	      	  pdfchargeamtcell.setBorder(0);
  					  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
  					  	      	chargetaxtable.addCell(pdfchargeamtcell);
  					   	   }else{
  					   		pdfchargecell=new PdfPCell(chunk1);
  					   		pdfchargecell.setBorder(0);
  					   		pdfchargeamtcell = new PdfPCell(chunk1);
  					   		pdfchargeamtcell.setBorder(0);
  					   		chargetaxtable.addCell(pdfchargecell);
  					   		chargetaxtable.addCell(pdfchargeamtcell);
  					   	   }
  					   	   
  						      }
  						 }
  					      }
//  							 }
  							 
  							 Phrase chunk1 = new Phrase(" ",font8);
  							 PdfPCell pdfchargecell=new PdfPCell(chunk1);
  						   		pdfchargecell.setBorder(0);
  						   		PdfPCell pdfchargeamtcell = new PdfPCell(chunk1);
  						   		pdfchargeamtcell.setBorder(0);
  						   		chargetaxtable.addCell(pdfchargecell);
  						   		chargetaxtable.addCell(pdfchargeamtcell);
  					      }
  					      
//  					}
//  					else
//  					{
//  						System.out.println("indise else condition");
//  						
//  						double disccAmtValue=0;
//  						
//  						for(int x=0;x<billEntity.getSalesOrderProducts().size();x++){	 
//  							
//  							
//  							
//  							if(billEntity.getSalesOrderProducts().get(x).getPaymentPercent()==100){	
//  						
//  					System.out.println("Pay Percent "+billEntity.getArrPayTerms().get(0).getPayTermPercent());
//  					
//  					double payterms= 100*(billEntity.getArrPayTerms().get(0).getPayTermPercent()/100)*(billEntity.getSalesOrderProducts().get(x).getPaymentPercent()/100);
//  					Phrase namePhase = new Phrase("Pay Terms "+payterms+"%",font8);
//  					PdfPCell namephasecell = new PdfPCell(namePhase);
//  					namephasecell.addElement(namePhase);
//  					namephasecell.setBorder(0);
//  					chargetaxtable.addCell(namephasecell);
//  					double total =0;
//  					
//  					total = ((con.getTotalAmount()*billEntity.getArrPayTerms().get(0).getPayTermPercent())/100);
//  					
//  					Phrase totalphase = new Phrase(df.format(total),font8);
//  					PdfPCell totalcell = new PdfPCell(totalphase);
//  					totalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  					totalcell.setBorder(0);
//  					chargetaxtable.addCell(totalcell);
//  					
//  					//*********************rohan make changes here for payment %
//  						System.out.println("************************************************789789789******");
//  							}
//  						else
//  						{
//  						System.out.println("inside condition ***********************");
//  						double paymentAsperPercent= (billEntity.getSalesOrderProducts().get(x).getBaseBillingAmount()*billEntity.getSalesOrderProducts().get(x).getPaymentPercent())/100;
//  						double payment= 100*(billEntity.getArrPayTerms().get(0).getPayTermPercent()/100)*(billEntity.getSalesOrderProducts().get(x).getPaymentPercent()/100);
//  						 Phrase billPayPhrase= new Phrase("Pay Term "+payment+"%",font8);
//  						 PdfPCell BillPayCell = new PdfPCell(billPayPhrase);
//  						 BillPayCell.setBorder(0);
//  						 chargetaxtable.addCell(BillPayCell);
//  						
//  						Phrase paymentAsperPercentPhrase = new Phrase(df.format(paymentAsperPercent),font8);
//  						PdfPCell paymentAsperPercentCell = new PdfPCell(paymentAsperPercentPhrase);
//  						paymentAsperPercentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  						paymentAsperPercentCell.setBorder(0);
//  						chargetaxtable.addCell(paymentAsperPercentCell);
//  					}
//  							if(billEntity.getSalesOrderProducts().get(x).getFlatDiscount()!=0)
//  							{
//  								disccAmtValue = disccAmtValue + billEntity.getSalesOrderProducts().get(x).getFlatDiscount();	
//  							}
//  					}
//  					 
//  						 
//  						if(disccAmtValue!=0)
//  						{
//  						 Phrase discAmt= new Phrase("Flat Disc",font8);
//  						 PdfPCell discAmtCell = new PdfPCell(discAmt);
//  						 discAmtCell.setBorder(0);
//  						 chargetaxtable.addCell(discAmtCell);
//  						 
//  						 Phrase discAmtValue= new Phrase(disccAmtValue+"",font8);
//  						 PdfPCell discAmtValueCell = new PdfPCell(discAmtValue);
//  						 discAmtValueCell.setBorder(0);
//  						 discAmtValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  						 chargetaxtable.addCell(discAmtValueCell);
//  						} 				 
//  						//*****************rohan ***********************************
//  						
//  					
//  					//*********************rohan changes here ****************************
//  					List<String> myLis11 = new ArrayList<>();
//  					List<String> myLis22 = new ArrayList<>();
//  					List<String> myLis33 = new ArrayList<>();
//  					List<String> myLis44 = new ArrayList<>();
//  					//****************************rohan changes on 16/6/15*****************
//  					 for(int k=0;k<this.conbillingTaxesLis.size();k++)
//  				     {
//  				   	  
//  						 
//  						 System.out.println("billingTaxesLis size"+conbillingTaxesLis.size());
//  						  System.out.println("iiiiiiii   value    "+k); 
//  				   	  
//  				   	  for(int i=0;i<this.conbillingTaxesLis.get(k).getChargesList().size();i++){
//  				   	  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()!=0){
//  				   	  
//  				   		  System.out.println("billingTaxesLis.get(i).getChargesList() size"+conbillingTaxesLis.get(k).getChargesList().size());
//  				   		  
//  				   		  System.out.println("billingTaxesLis.get(i).getTaxChargeName()"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
//  				   		  
//  				   		  if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("VAT")||conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("CST")){
//  									
//  									System.out.println("1st loop"+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
//  									
//  									String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
//  									double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
//  											* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
//  									
//  									 myLis11.add(str);
//  									 myLis22.add(df.format(taxAmt1));
//  									 
////  									 System.out.println("Size of mylist1 is () === "+myList1.size());
//  								}
//  								
//  								if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent() != 14.5){
//  									
//  									 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
//  									
//  									System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
//  									
//  									String str = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent();
//  									double taxAmt1 = conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()
//  											* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
//  									String blidnk=" ";
//  									 myLis33.add(str);
//  									 myLis44.add(df.format(taxAmt1));
//  									 myLis44.add(blidnk);
//  									 
////  									 System.out.println("Size of mylist2 is () === "+myList2.size());
//  								}
//  								
//  								//*****************rohan added this for swachha bharata abhiyan tax details 
//  								
//  								
//  								if(conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().equals("Service Tax")&& conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent()==14.5){
//  									
//  									 System.out.println("billingTaxesLis.get(i).getTaxChargeName() service tax "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName());
//  									
//  									System.out.println("2nd loop == "+conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeName().trim()+ " @ "+ conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargePercent());
//  									
//  									String str = "Service Tax "+ " @ "+" 14%";
//  									double taxAmt1 = 14* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
//  									String blidnk=" ";
//  									 
//  									
//  									 myLis33.add(str);
//  									 myLis44.add(df.format(taxAmt1));
//  									 myLis44.add(blidnk);
//  									 
//  									 
//  									 String str123 = "Swachha Bharat Cess"+ " @ "+" 0.5%";
//  									 double taxAmt = 0.5* conbillingTaxesLis.get(k).getChargesList().get(i).getTaxChargeAssesVal() / 100;
//  									 
//  									 myLis33.add(str123);
//  									 myLis44.add(df.format(taxAmt));
//  									 myLis44.add(blidnk);
////  									 System.out.println("Size of mylist2 is () === "+myList2.size());
//  								}
//  								
//  								
//  				   	  }
//  				    	}
//  				     }
//  					 
//  					 PdfPCell pdfservicecell123=null;
//  						
//  						PdfPTable other1table123=new PdfPTable(1);
//  						other1table123.setWidthPercentage(100);
//  						for(int j=0;j<myLis11.size();j++){
//  							chunk = new Phrase(myLis11.get(j),font8);
//  							pdfservicecell123 = new PdfPCell(chunk);
//  							pdfservicecell123.setBorder(0);
//  							pdfservicecell123.setHorizontalAlignment(Element.ALIGN_LEFT);
//  							other1table123.addCell(pdfservicecell123);
//  							
//  						}
//  						
//  						PdfPCell pdfservicecell1123=null;
//  						
//  						PdfPTable other2table123=new PdfPTable(1);
//  						other2table123.setWidthPercentage(100);
//  						for(int j=0;j<myLis22.size();j++){
//  							chunk = new Phrase(myLis22.get(j),font8);
//  							pdfservicecell1123 = new PdfPCell(chunk);
//  							pdfservicecell1123.setBorder(0);
//  							pdfservicecell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  							other2table123.addCell(pdfservicecell1123);
//  							
//  						}
//  						
//  						PdfPCell chargescell123=null;
//  						for(int j=0;j<myLis33.size();j++){
//  							chunk = new Phrase(myLis33.get(j),font8);
//  							chargescell123 = new PdfPCell(chunk);
//  							chargescell123.setBorder(0);
//  							chargescell123.setHorizontalAlignment(Element.ALIGN_LEFT);
//  							other1table123.addCell(chargescell123);
//  							
//  						}
//  						
//  						PdfPCell chargescell1123=null;
//  						for(int j=0;j<myLis44.size();j++){
//  							chunk = new Phrase(myLis44.get(j),font8);
//  							chargescell1123 = new PdfPCell(chunk);
//  							chargescell1123.setBorder(0);
//  							chargescell1123.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  							other2table123.addCell(chargescell1123);
//  							
//  						}
//  						
//  						
//  						
//  						PdfPCell othercell123 = new PdfPCell();
//  						othercell123.addElement(other1table123);
//  						othercell123.setBorder(0);
//  						chargetaxtable.addCell(othercell123);
//  						
//  						PdfPCell othercell1123 = new PdfPCell();
//  						othercell1123.addElement(other2table123);
//  						othercell1123.setBorder(0);
//  						chargetaxtable.addCell(othercell1123);
//  					 
//  					 //*******ROHAN CHANGES HERE FOR PART PAYMENT ON 5/6/15******************
//  						
//  						
//  						
//  					 //**********************CHANGES ENDS HERE********************************
//  					 
//  					 
////  					 PdfPCell chargecell22 = null;
////  				     PdfPCell chargeamtcell22=null;
////  				     PdfPCell otherchargecell22=null;
//  						System.out.println(billingChargesLis.size());
//  				     System.out.println("rohan bhagde====================== "+billingChargesLis.size());
//  					 for(int k=0;k<this.conbillingChargesLis.size();k++)
//  				     {
//  				  	   Phrase chunk = null;
//  				  	   Phrase chunk1 = new Phrase("",font8);
//  				  	   double chargeAmt=0;
//  				  	   PdfPCell pdfchargeamtcell = null;
//  				  	   PdfPCell pdfchargecell = null;
//  				  	   
//  				  	   
//  				  	   for(int i=0;i<this.conbillingChargesLis.get(k).getOtherChargesList().size();i++){
//  				  	   
//  				  	   if(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargePercent()!=0){
//  				  		   chunk = new Phrase(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeName()+" @ "+conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargePercent(),font1);
//  				  		   pdfchargecell=new PdfPCell(chunk);
//  				  		pdfchargecell.setBorder(0);
//  				  		chargetaxtable.addCell(pdfchargecell);
//  					     
////  				  		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
//  				  		 chargeAmt=conbillingChargesLis.get(k).getOtherChargesList().get(i).getPayableAmt();
//  				  		   chunk=new Phrase(df.format(chargeAmt),font8);
//  				 	      	   pdfchargeamtcell = new PdfPCell(chunk);
//  				 	      	 pdfchargeamtcell.setBorder(0);
//  				 	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  				 	      	chargetaxtable.addCell(pdfchargeamtcell);
//  				 	      	
//  				 	      	  
//  				  	   }else{
//  				  		pdfchargecell=new PdfPCell(chunk1);
//  				  		pdfchargecell.setBorder(0);
//  				  		pdfchargeamtcell = new PdfPCell(chunk1);
//  				  		pdfchargeamtcell.setBorder(0);
//  				  		chargetaxtable.addCell(pdfchargecell);
//  				  		chargetaxtable.addCell(pdfchargeamtcell);
//  				  	   }
//  				  	   if(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeAbsVal()!=0){
//  				  		   chunk = new Phrase(conbillingChargesLis.get(k).getOtherChargesList().get(i).getTaxChargeName()+"",font1);
//  				  		   pdfchargecell=new PdfPCell(chunk);
//  				  		pdfchargecell.setBorder(0);
//  				  		chargetaxtable.addCell(pdfchargecell);
//  				  		   chargeAmt=conbillingChargesLis.get(k).getOtherChargesList().get(i).getPayableAmt();
//  				  		   chunk=new Phrase(chargeAmt+"",font8);
//  				 	      	   pdfchargeamtcell = new PdfPCell(chunk);
//  				 	      	  pdfchargeamtcell.setBorder(0);
//  				 	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  				 	      	chargetaxtable.addCell(pdfchargeamtcell);
//  				  	   }else{
//  				  		pdfchargecell=new PdfPCell(chunk1);
//  				  		pdfchargecell.setBorder(0);
//  				  		pdfchargeamtcell = new PdfPCell(chunk1);
//  				  		pdfchargeamtcell.setBorder(0);
//  				  		chargetaxtable.addCell(pdfchargecell);
//  				  		chargetaxtable.addCell(pdfchargeamtcell);
//  				  	   }
//  					      
//  					      }
//  				     }
//  					//************************changes ends here ***********************8
//  					
//  					}			
  					
  					
  					

//  				 	Phrase netname = new Phrase("Grand Total",font8);
//  					PdfPCell netnamecell = new PdfPCell(netname);
//  					netnamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
////  					namephasevaluecell.addElement(namePhasevalue);
//  					netnamecell.setBorder(0);
//  					chargetaxtable.addCell(netnamecell);
//  				 	
//  				 
//  				 	double netpayable=0;
//  				 	netpayable = con.getNetpayable();
//  					String netpayableamt=netpayable+"";
//  					System.out.println("total============"+netpayableamt);
//  					Phrase netpay = new Phrase(netpayableamt,font8);
//  					PdfPCell netpaycell = new PdfPCell(netpay);
//  					netpaycell.setHorizontalAlignment(Element.ALIGN_RIGHT);
////  					namephasevaluecell.addElement(namePhasevalue);
//  					netpaycell.setBorder(0);
//  					chargetaxtable.addCell(netpaycell);
//  				 
//  				 
//  					Phrase blankSpace = new Phrase(" ",font8);
//  					PdfPCell blankSpacecell = new PdfPCell(blankSpace);
//  					blankSpacecell.setBorder(0);
//  					chargetaxtable.addCell(blankSpacecell);
//  					chargetaxtable.addCell(blankSpacecell);
  					
  					
  					
  					
//  						  	      	 pdfchargeamtcell.setBorder(0);
//  						  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  						  	      	  chargetaxtable.addCell(pdfchargeamtcell);
//  						  	      	
//  						  	      	  
//  						   	   }else{
//  						   		pdfchargecell=new PdfPCell(chunk1);
//  						   		pdfchargecell.setBorder(0);
//  						   		pdfchargeamtcell = new PdfPCell(chunk1);
//  						   		pdfchargeamtcell.setBorder(0);
//  						   		chargetaxtable.addCell(pdfchargecell);
//  						   	  chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }
//  						   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
//  						   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
//  						   		   pdfchargecell=new PdfPCell(chunk);
//  						   		pdfchargecell.setBorder(0);
//  						   		chargetaxtable.addCell(pdfchargecell);
//  						   		   chargeAmt=billingChargesLis.get(i).getPayableAmt();
//  						   		   chunk=new Phrase(chargeAmt+"",font8);
//  						  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//  						  	      	  pdfchargeamtcell.setBorder(0);
//  						  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  						  	      	  chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }else{
//  						   		pdfchargecell=new PdfPCell(chunk1);
//  						   		pdfchargecell.setBorder(0);
//  						   		pdfchargeamtcell = new PdfPCell(chunk1);
//  						   		pdfchargeamtcell.setBorder(0);
//  						   		chargetaxtable.addCell(pdfchargecell);
//  							   	  chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }
//  						 	      
//  							      }
//  					}
  							
//  							 for(int i=0;i<this.billingTaxesLis.size();i++)
//  						      {
//  						    	  
//  						    	  if(billingTaxesLis.get(i).getTaxChargePercent()!=0){
//  						    	  
//  						    		  if(billingTaxesLis.get(i).getTaxChargeName().equals("VAT")||billingTaxesLis.get(i).getTaxChargeName().equals("CST")){
//  											
//  											System.out.println("1st loop"+billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent());
//  											
//  											String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
//  											double taxAmt1 = billingTaxesLis.get(i).getTaxChargePercent()
//  													* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
//  											
//  											 myList1.add(str);
//  											 myList2.add(df.format(taxAmt1));
//  											 
//  											 System.out.println("Size of mylist1 is () === "+myList1.size());
//  										}
//  										
//  										if(billingTaxesLis.get(i).getTaxChargeName().equals("Service Tax")){
//  											
//  											System.out.println("2nd loop == "+billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent());
//  											
//  											String str = billingTaxesLis.get(i).getTaxChargeName().trim()+ " @ "+ billingTaxesLis.get(i).getTaxChargePercent();
//  											double taxAmt1 = billingTaxesLis.get(i).getTaxChargePercent()
//  													* billingTaxesLis.get(i).getTaxChargeAssesVal() / 100;
//  											
//  											String blidnk=" ";
//  											myList3.add(str);
//  											 myList4.add(df.format(taxAmt1));
//  											 myList4.add(blidnk);
//  											 System.out.println("Size of mylist2 is () === "+myList2.size());
//  										}
//  						    	  }

//  							 PdfPCell pdfservicecell22=null;
//  								
//  								PdfPTable other1table22=new PdfPTable(1);
//  								other1table22.setWidthPercentage(100);
//  								for(int j=0;j<myList1.size();j++){
//  									chunk = new Phrase(myList1.get(j),font8);
//  									 pdfservicecell22 = new PdfPCell(chunk);
//  									pdfservicecell22.setBorder(0);
//  									pdfservicecell22.setHorizontalAlignment(Element.ALIGN_LEFT);
//  									other1table.addCell(pdfservicecell22);
//  									
//  								}
  								
//  								PdfPCell pdfservicecell122=null;
//  								
//  								PdfPTable other2table22=new PdfPTable(1);
//  								other2table22.setWidthPercentage(100);
//  								for(int j=0;j<myList2.size();j++){
//  									chunk = new Phrase(myList2.get(j),font8);
//  									 pdfservicecell122 = new PdfPCell(chunk);
//  									pdfservicecell122.setBorder(0);
//  									pdfservicecell122.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  									other2table.addCell(pdfservicecell122);
//  									
//  								}
  								
  								
//  								PdfPCell chargescell22=null;
//  								for(int j=0;j<myList3.size();j++){
//  									chunk = new Phrase(myList3.get(j),font8);
//  									chargescell22 = new PdfPCell(chunk);
//  									chargescell22.setBorder(0);
//  									chargescell22.setHorizontalAlignment(Element.ALIGN_LEFT);
//  									other1table22.addCell(chargescell22);
//  									
//  								}
  								
//  								PdfPCell chargescell122=null;
//  								for(int j=0;j<myList4.size();j++){
//  									chunk = new Phrase(myList4.get(j),font8);
//  									chargescell122 = new PdfPCell(chunk);
//  									chargescell122.setBorder(0);
//  									chargescell122.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  									other2table22.addCell(chargescell122);
//  									
//  								}
  								
  								
  								
//  								PdfPCell othercell22 = new PdfPCell();
//  								othercell22.addElement(other1table22);
//  								othercell22.setBorder(0);
//  								chargetaxtable.addCell(othercell22);
//  								
//  								PdfPCell othercell122 = new PdfPCell();
//  								othercell122.addElement(other2table22);
//  								othercell122.setBorder(0);
//  								chargetaxtable.addCell(othercell122);
  							 
  							 
  						      /****************************************************************************************/
  						      
//  						      for(int i=0;i<this.billingChargesLis.size();i++)
//  						      {
//  						   	   Phrase chunk = null;
//  						       Phrase chunk1 = new Phrase("",font8);
//  						   	   double chargeAmt=0;
//  						   	   PdfPCell pdfchargeamtcell = null;
//  						   	   PdfPCell pdfchargecell = null;
//  						   	   if(billingChargesLis.get(i).getTaxChargePercent()!=0){
//  						   		   
//  						   		   System.out.println("getTaxChargePercent");
//  						   		   
//  						   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+" @ "+billingChargesLis.get(i).getTaxChargePercent(),font1);
//  						   		   pdfchargecell=new PdfPCell(chunk);
////  						   		   chargeAmt=billingChargesLis.get(i).getTaxChargePercent()*billingChargesLis.get(i).getTaxChargeAssesVal()/100;
//  						   		chargeAmt=billingChargesLis.get(i).getPayableAmt();
//  						   		   chunk=new Phrase(df.format(chargeAmt),font8);
//  						  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//  						  	      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  						  	      	   pdfchargeamtcell.setBorder(0);
//  						  	      	   
//  						  	      	  pdfchargecell.setBorder(0);
//  							 	      pdfchargeamtcell.setBorder(0);
//  							 	       chargetaxtable.addCell(pdfchargecell);
//  							 	       chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }else{
//  						   		pdfchargecell=new PdfPCell(chunk1);
//  						   		pdfchargeamtcell = new PdfPCell(chunk1);
//  						   		
//  						   	  pdfchargecell.setBorder(0);
//  					 	      pdfchargeamtcell.setBorder(0);
//  					 	       chargetaxtable.addCell(pdfchargecell);
//  					 	       chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }
//  						   	   if(billingChargesLis.get(i).getTaxChargeAbsVal()!=0){
//  						   		   chunk = new Phrase(billingChargesLis.get(i).getTaxChargeName()+"",font1);
//  						   		   pdfchargecell=new PdfPCell(chunk);
//  						   		   chargeAmt=billingChargesLis.get(i).getPayableAmt();
//  						   		   
//  						   		 System.out.println("getTaxChargeAbsVal");
//  						   		   
//  						   		   chunk=new Phrase(chargeAmt+"",font8);
//  						  	      	   pdfchargeamtcell = new PdfPCell(chunk);
//  						  	      	   pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//  						  	      	   pdfchargeamtcell.setBorder(0);
//  						  	      	   
//  						  	      	  pdfchargecell.setBorder(0);
//  							 	      pdfchargeamtcell.setBorder(0);
//  							 	       chargetaxtable.addCell(pdfchargecell);
//  							 	       chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }else{
//  						   		pdfchargecell=new PdfPCell(chunk1);
//  						   		pdfchargeamtcell = new PdfPCell(chunk1);
//  						   		
//  						   	  pdfchargecell.setBorder(0);
//  					 	      pdfchargeamtcell.setBorder(0);
//  					 	       chargetaxtable.addCell(pdfchargecell);
//  					 	       chargetaxtable.addCell(pdfchargeamtcell);
//  						   	   }
//  						 	     
//  							      	
//  						      }
//  						}
  							 }
  				
  			 //******************************88888888888888888888888888888888888888888888

  				 
  	    
  	    PdfPTable billamtable = new PdfPTable(2);
  	    billamtable.setWidthPercentage(100);
  	    billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
  		       
  		      for(int i=0;i<this.billingChargesLis.size();i++){
  		       
  		      	if(i==billingChargesLis.size()-1){
  		      		chunk = new Phrase("Net Payable",font1);
  			        PdfPCell pdfinvamtcell = new PdfPCell(chunk);
  			        pdfinvamtcell.setBorder(0);
  			      	Double invAmt=invoiceentity.getInvoiceAmount();
  			      	
  			      	int netpayble=(int) invAmt.doubleValue();
  			      	
  			      	chunk=new Phrase(netpayble+"",font8);
  			        PdfPCell pdfnetpayamt = new PdfPCell(chunk);
  			        pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
  			        pdfnetpayamt.setBorder(0);
  			        
//  			        chargetaxtable.addCell(pdfinvamtcell);
//  			        chargetaxtable.addCell(pdfnetpayamt);
  			        
  			        billamtable.addCell(pdfinvamtcell);
  			        billamtable.addCell(pdfnetpayamt);
  			        
  			        netPayableTable.addCell(pdfinvamtcell);
  			        netPayableTable.addCell(pdfnetpayamt);
  		      	}
  	   	}
  	    
  	    if(this.billingChargesLis.size()==0){
  	  	  
  	  	  Phrase chunkinvAmttitle = new Phrase("Net Payable",font1);
  	  	  PdfPCell invAmtTitleCell=new PdfPCell(chunkinvAmttitle);
  	  	  invAmtTitleCell.setBorder(0);
  	  	  Double invoiceAmount=invoiceentity.getInvoiceAmount();
  	  	  
  	  	  int netpayble=(int) invoiceAmount.doubleValue();
  	  	  
  		      Phrase chunkinvamt=new Phrase(netpayble+"",font8);
  		      PdfPCell pdfinvamt = new PdfPCell(chunkinvamt);
  		      pdfinvamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
  		      pdfinvamt.setBorder(0);
  		        
//  		      chargetaxtable.addCell(invAmtTitleCell);
//  		      chargetaxtable.addCell(pdfinvamt);
  		      
  		      billamtable.addCell(invAmtTitleCell);
  		        billamtable.addCell(pdfinvamt);
  		        
  		        
  		        netPayableTable.addCell(invAmtTitleCell);
  		        netPayableTable.addCell(pdfinvamt);
  	    }
  	    
  	    PdfPCell cagrecell=new PdfPCell();
  	    cagrecell.addElement(chargetaxtable);
  	    cagrecell.setBorder(0);
  	    
  	    PdfPTable mainbilltable = new PdfPTable(1);
  	    mainbilltable.setWidthPercentage(100);
  	    mainbilltable.setHorizontalAlignment(Element.ALIGN_RIGHT);
  	    mainbilltable.addCell(cagrecell);
//  	    mainbilltable.addCell(billamtable);
  	    
  	    
  	    
  	    PdfPTable taxinfotable=new PdfPTable(1);
//  	    taxinfotable.setWidthPercentage(100);
//  	    taxinfotable.addCell(mainbilltable);
  	    
  	    PdfPTable parenttaxtable=new PdfPTable(2);
  	    parenttaxtable.setWidthPercentage(100);
  	    try {
  			parenttaxtable.setWidths(new float[]{65,35});
  		} catch (DocumentException e1) {
  			e1.printStackTrace();
  		}
  	    
  		  PdfPCell amtWordsTblcell1 = new PdfPCell();
  		  PdfPCell taxdatacell = new PdfPCell();
  			
  		  amtWordsTblcell1.addElement(termstable1);
  		  taxdatacell.addElement(mainbilltable);
  		  
  		  parenttaxtable.addCell(amtWordsTblcell1);
  		  parenttaxtable.addCell(taxdatacell);
  		
  			try {
  				document.add(parenttaxtable);
  			} catch (DocumentException e) {
  				e.printStackTrace();
  			}
  			
  			
  			  String amtInWords="Rupees:  "+SalesInvoicePdf.convert(invoiceentity.getInvoiceAmount());
  			   System.out.println("amt"+amtInWords);
  			   Phrase amtwords=new Phrase(amtInWords+" Only.",font1);
  			   PdfPCell amtWordsCell=new PdfPCell();
  			   amtWordsCell.addElement(amtwords);
  			   amtWordsCell.setBorder(0);
  			   
  			   
  			   PdfPTable amountInWordsTable=new PdfPTable(1);
  			   amountInWordsTable.setWidthPercentage(100);
//  			   amountInWordsTable.setHorizontalAlignment(Element.ALIGN_TOP);
  			   amountInWordsTable.addCell(amtWordsCell);
  				
  			  
  			   PdfPTable termpayTable=new PdfPTable(1);
  			   termpayTable.setWidthPercentage(100);
//  			   termpayTable.addCell(headingpayterms);
  			   termpayTable.addCell(termstable1);
//  			   termpayTable.addCell(amountInWordsTable);
  			
  			   PdfPTable parenttaxtable1=new PdfPTable(2);
  			      parenttaxtable1.setWidthPercentage(100);
  			      try {
  					parenttaxtable1.setWidths(new float[]{65,35});
  				} catch (DocumentException e1) {
  					e1.printStackTrace();
  				}
  			      
  				  PdfPCell amtWordsTblcell = new PdfPCell();
  				  PdfPCell taxdatacell1 = new PdfPCell();
  					
  				  amtWordsTblcell.addElement(amountInWordsTable);
  				  taxdatacell1.addElement(billamtable);
  				  parenttaxtable1.addCell(amtWordsTblcell);
  				  parenttaxtable1.addCell(taxdatacell1);
  				
  					try {
  						document.add(parenttaxtable1);
  						
  					} catch (DocumentException e) {
  						e.printStackTrace();
  					}
  					
  	
    	}

    	
		private ArrayList<Integer> getBillingIds() {
    		
    		ArrayList<Integer> billingIDlis = new ArrayList<Integer>();
    		HashSet<Integer> hset = new HashSet<Integer>();
    		
    		for(int i=0;i<salesProd.size();i++)
    		{
    			System.out.println("salesProd.get(i).getBiilingId()"+salesProd.get(i).getBiilingId());
    			hset.add(salesProd.get(i).getBiilingId());
    			System.out.println("hash set size "+hset.size());
    		}
    		
    		System.out.println("billingIDlis"+billingIDlis.size());
    		billingIDlis.addAll(hset);
    		
			return billingIDlis;
    	}
    	
    	
    	}
