package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
@SuppressWarnings("serial")
public class NBHCCommodityFumigationReportPdfServlet extends HttpServlet
{

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
		
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  

	  try {
		  String stringid=request.getParameter("Id");
		  stringid=stringid.trim();
		  Long count =Long.parseLong(stringid);
		  String preePrint = request.getParameter("preprint"); //Added by Ashwini
		  
		  NBHCCommodityFumigationReportPdf pdf=new NBHCCommodityFumigationReportPdf();
		  /** 30-10-2017 sagar sore [margins updated to show partitioned report]**/
		//  pdf.document=new Document(PageSize.A5);	
		  /**
		   * Date 27/11/2017
		   * By jayshree
		   * Des.To set the pagesetup in landscape mode changes are done
		   */
		  if(preePrint.equals("no")){
			  pdf.document=new Document(PageSize.A4.rotate(),20,20,0,0);
		  }else{
			  pdf.document=new Document(PageSize.A4.rotate(),20,20,30,30);
		  }
//		  pdf.document=new Document(PageSize.A4.rotate(),20,20,30,30);  //Commented by AShwini
		  //End By Jayshree
		  Document document = pdf.document;
		  PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
	  
	   document.open();
	   pdf.getfumigationReport(count);
	   pdf.createPdf(preePrint); //Added by Ashwini
//	   pdf.createPdf(); //commented by Ashwini
	   document.close();
		  
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	}


}
