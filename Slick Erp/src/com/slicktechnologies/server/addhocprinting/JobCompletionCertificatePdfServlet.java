package com.slicktechnologies.server.addhocprinting;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
public class JobCompletionCertificatePdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 421044671149521125L;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);

			JobCompletionCertificatePdf jobcerti = new JobCompletionCertificatePdf();
			
			jobcerti.document = new Document();
			Document document = jobcerti.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream());

			document.open();
			jobcerti.setpdfjobcerti(count);
			jobcerti.createPdf();
			document.close();

			

			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
