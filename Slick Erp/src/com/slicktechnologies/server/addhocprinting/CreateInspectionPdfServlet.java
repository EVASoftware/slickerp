package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.inventory.Inspection;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class CreateInspectionPdfServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4632861527912075645L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			stringid = stringid.trim();
			Long count = Long.parseLong(stringid);
			Inspection insp = ofy().load().type(Inspection.class).id(count).now();
			
			InspectionPdf insppdf = new InspectionPdf();
			
			insppdf.document = new Document();
			Document document = insppdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			if(insp.getStatus().equals(Inspection.CANCELLED)){
				writer.setPageEvent(new PdfCancelWatermark());
			}else															
			if(!insp.getStatus().equals(Inspection.APPROVED)){	
				 writer.setPageEvent(new PdfWatermark());
			} 
																		
			
			 
			 document.open();
			
			insppdf.setInspection(count);
			insppdf.createPdf();
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

}
