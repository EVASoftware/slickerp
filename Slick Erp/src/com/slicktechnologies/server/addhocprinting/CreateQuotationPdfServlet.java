package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class CreateQuotationPdfServlet extends HttpServlet{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4841549138121835585L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
	
		resp.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.

	try {
		String stringid = req.getParameter("Id");
		stringid=stringid.trim();
		Long count =Long.parseLong(stringid);
	    String preprintStatus=req.getParameter("preprint");
	    
		Quotation quotationEntity = ofy().load().type(Quotation.class).id(count).now();
		ServerAppUtility serverUtility = new ServerAppUtility();
		Company companyEntity = serverUtility.loadCompany(quotationEntity.getCompanyId());
				
	    QuotationPDFVersion1 quotationpdf = new QuotationPDFVersion1();
	    quotationpdf.document = new Document(); 
 	    Document document = quotationpdf.document;
 	    PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write the pdf in response
 	    
 	    
 	    document.setPageSize(PageSize.A4);
 	  	document.setMargins(30, 30, 120, 100);
 	  	document.setMarginMirroring(false);
		
// 	  	 if(preprintStatus.equals("no")){
 	  	   HeaderFooterPageEvent event = new HeaderFooterPageEvent(companyEntity,quotationEntity.getBranch(),preprintStatus);
 	  	   writer.setPageEvent(event);
// 	    }
 	  
 	 
 	    document.open();
 		   
    	quotationpdf.setservicequotation(count);
    	quotationpdf.createPdf(preprintStatus);
 	   
 	    
 	    document.close();
 	    

	}catch (Exception e) {
		e.printStackTrace();
	}
   }	

}
