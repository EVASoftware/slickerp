package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.PdfUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class NewContractRenewalVersionOnePdf {
	Logger logger = Logger.getLogger("NewContractRenewalVersionOnePdf.class");
//	ContractRenewal conRen;
	Company comp;
	Customer cust;
	Branch branchDt = null;
	Contract con;
	ContactPersonIdentification conDetails;
	public Document document;
//	int noOfLine=2;
	int noOfLine=12;  //10  //16
	int productcount=0;
	int vat;
	int st;
	boolean checkEmailId=false;
	boolean checkOldFormat=false;
	List<Branch> branchList=new ArrayList<Branch>();//By Jayshree
//	List<CompanyPayment>comppayment=new ArrayList<CompanyPayment>();//By Jayshree
	CompanyPayment comppayment;
	private Font font16boldul, font12bold, font8bold, font8,font7bold, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold;

	Phrase chunk;
	PdfPCell pdfcode,pdfname,pdfsacCode,pdfduration,pdfservices,pdfprice,pdftax,pdfnetPay, pdfQty,pdfPremises,pdfRemark;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt3 = new SimpleDateFormat("MMM-yyyy");
	DecimalFormat df = new DecimalFormat("#");

	boolean oldConDetFlag=false;
	/*
	 * Date:10/11/2018
	 * Developer:Ashwini
	 * 
	 */
	Contract oldContract;
	 boolean pecoppflag = false;
	 boolean pepcoppflag = false;


	 ContractRenewal conRenw;
	 
	 boolean contractQtyFlag = false;
	 double totaAmount = 0;
	 boolean discountFlag=false;//Ashwini Patil Date:27-09-2022 Need to print discount column if any product contains descount
	  boolean areaWiseCalRestricFlg;//3-11-2022
	 boolean PC_RemoveSalutationFromCustomerOnPdfFlag = false;
	 boolean upcflag = false;
	 boolean addcellNoFlag = false;
	 boolean pocNameFlag = false;
	 Font nameAddressBoldFont=new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);//Ashwini Patil
	 Font nameAddressFont=new Font(Font.FontFamily.HELVETICA, 7);//Ashwini Patil
	 Font nameAddressFont6 = new Font(Font.FontFamily.HELVETICA, 8);
	 CustomerBranchDetails customerBranch=null;
	 List<CustomerBranchDetails> customerBranchList;
	 List<CustomerBranchDetails> selectedBranches;
	 boolean multipleServiceBranchesFlag=false;	
	 boolean custbranchmailFlag = false;
	 HashMap<String,ArrayList<String>> branchWiseProductMap=new HashMap<String,ArrayList<String>>();//27-12-2022
	 PdfUtility pdfUtility=new PdfUtility();
		
	public NewContractRenewalVersionOnePdf() {
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
	  	font7bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//added by ashwini
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void setContractRewnewal(Contract contract, ContractRenewal conRenewal){
		logger.log(Level.SEVERE,"in setContractRewnewal");
		con=contract;
		conRenw = conRenewal;
//		if(conRenw!=null)
//			System.out.println("conRenw count"+conRenw.getContractId());
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();
		
		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", con.getCompanyId()).filter("count", con.getCustomerId()).first().now();
		
		if (con.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", con.getCount()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count",con.getCount()).first().now();
		
		oldContract=ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count", con.getRefContractCount()).first().now();
		
		if(oldContract!=null)
			logger.log(Level.SEVERE,"oldContract id="+oldContract.getCount());
		
		branchList=ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName",con.getBranch()).list();

		
		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", con.getCompanyId()).filter("paymentStatus", true).filter("paymentDefault", true).first().now();
		
		if(con.getPaymentMode()!=null && con.getPaymentMode().trim().length()>0){
			List<String> paymentDt = Arrays.asList(con.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", con.getCompanyId()).first()
						.now();
				
			}
		}	
		
		conDetails=ofy().load().type(ContactPersonIdentification.class)
				.filter("companyId", con.getCompanyId()).filter("name", con.getPocName()).first().now();
		/**
		 * Date : 05-12-2017 BY ANIL
		 * Loading process configuration,for checking whether to show old contract details or not
		 */
		if(con.getCompanyId()!=null){
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", con.getCompanyId()).filter("processName", "ContractRenewal").filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowOldContractDetails")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						oldConDetFlag=true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OldFormatRenewalLetter")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkOldFormat = true;
					}
					
					/*
					 * Date:10/11/2018
					 * Added by Ashwini
					 * Des:To made changes for pecopp only
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyforPecopp")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pecoppflag = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyforPepcopp")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pepcoppflag = true;
					}
					/*
					 * end by Ashwini
					 */
				}
			}
		}
		
		
if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(con !=null && con.getBranch() != null && con.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName", con.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", con.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		
       areaWiseCalRestricFlg=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("contract", "DisableAreaWiseBillingForAmc", comp.getCompanyId());

       if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot(AppConstants.COMPANY, AppConstants.PC_REMOVESALUTATIONFROMCUSTOMERONPDF, comp.getCompanyId())){
			PC_RemoveSalutationFromCustomerOnPdfFlag = true;
		}
       if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","CompanyAsLetterHead", comp.getCompanyId())){
    	   upcflag = true;
		}
       if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PrintAddressAndCellFromCustomerBranch", comp.getCompanyId())){
    	   addcellNoFlag = true;
		}
       if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PC_CustomerBranchEmail", comp.getCompanyId())){
    	   custbranchmailFlag = true;
		}
       
       if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Contract","PC_CustomerBranchPOC", comp.getCompanyId())){
    	   pocNameFlag = true;
		}
       
       ArrayList<String> custbranchlist=getCustomerBranchList(con.getItems());
       if(custbranchlist!=null){
   		if(custbranchlist.size()==1){
   			if(custbranchlist.contains("Service Address")==false){
   				logger.log(Level.SEVERE,"In Side AList1:");
   				customerBranch= ofy().load().type(CustomerBranchDetails.class)
   							.filter("cinfo.count",con.getCinfo().getCount())
   							.filter("companyId", con.getCompanyId())
   							.filter("buisnessUnitName", custbranchlist.get(0)).first().now();
   				
   				logger.log(Level.SEVERE,"AList1:" +customerBranch);
   				logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());				
   			}
   		}
   		else{
   			System.out.println("there are multiple service branches");
   			multipleServiceBranchesFlag=true;
   			selectedBranches = getSelectedBranches(con);
   			if(!custbranchlist.contains("Service Address")){
   				if(selectedBranches!=null){
   					customerBranch=selectedBranches.get(0);
   					System.out.println("customerBranch= "+customerBranch.getBusinessUnitName());
   				}
   			}
   			if(selectedBranches!=null){
   				List<SalesLineItem> itemList=con.getItems();
   				for(CustomerBranchDetails branch:selectedBranches){
   					System.out.println("ashwini selectedBranche="+branch.getBusinessUnitName());
   					HashSet<String> productSet=new HashSet<String>();
   					for(SalesLineItem itemObj:itemList){
   						System.out.println("ashwini item in for="+itemObj.getProductName());
   						if(itemObj.getCustomerBranchSchedulingInfo()!=null){
   							System.out.println("ashwini itemObj.getCustomerBranchSchedulingInfo()!=null");
   							ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
   							for(BranchWiseScheduling obj:branchSchedulingList){
   								System.out.println("obj.getBranchName()="+obj.getBranchName()+" ischeck"+obj.isCheck());
   								if(obj.isCheck()==true&&obj.getBranchName().equals(branch.getBusinessUnitName())){
   									productSet.add(itemObj.getProductName());
   								}
   							}
   						}
   					}
   					ArrayList<String> productList=new ArrayList<String>(productSet);
   					branchWiseProductMap.put(branch.getBusinessUnitName(), productList);
   				}
   				HashSet<String> productSet=new HashSet<String>();
   				for(SalesLineItem itemObj:itemList){
   					System.out.println("ashwini item in for="+itemObj.getProductName());
   					if(itemObj.getCustomerBranchSchedulingInfo()!=null){
   						System.out.println("ashwini itemObj.getCustomerBranchSchedulingInfo()!=null");
   						ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
   						for(BranchWiseScheduling obj:branchSchedulingList){
   							System.out.println("obj.getBranchName()="+obj.getBranchName()+" ischeck"+obj.isCheck());
   							if(obj.isCheck()==true&&obj.getBranchName().equals("Service Address")){
   								productSet.add(itemObj.getProductName());
   							}
   						}
   					}
   				}
   				ArrayList<String> productList=new ArrayList<String>(productSet);
   				branchWiseProductMap.put("Service Address", productList);
   				
   			}
   			
   		}
   	}
   			
	}


	
	public void createPdf(String preprintStatus,String callFrom) {
		

		if(pecoppflag){
			if(preprintStatus.contains("plane")){
//				Createblank();
				createLogo(document,comp);
				createCompanyAddress();
			}else if(preprintStatus.contains("yes")){
				createBlankforUPC();
			}else if(preprintStatus.contains("no")){
				createBlankforUPC();
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
				
			}
		}else{
		
		
		if(preprintStatus.contains("plane")){
//			Createblank();
			createLogo(document,comp);
			createCompanyAddress();
		}else if(preprintStatus.contains("yes")){
			createBlankforUPC();
		}else if(preprintStatus.contains("no")){
			createBlankforUPC();
			if(comp.getUploadHeader()!=null){
		    	createCompanyNameAsHeader(document,comp);
			}
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
			
		}
		}
		
	
		createHeading();
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "PrintServiceAddressOnRenewal", comp.getCompanyId())){
			createBillingAndServiceCustomerDetails();
		}else
			createCustomerDetails();
		createSubjectAndMsg(callFrom);
		if(checkOldFormat==true){
			if(oldConDetFlag){
				createOldContractProductDetails();
			}
	   createproductHeading();
	   createProductInfonew();
	   if(noOfLine!=0){
	   cretetotal();
	   footerInfo();
	  // Createblank();
	   footerDetails();
	   }
		if(noOfLine==0&&productcount==0){
			 cretetotal();
			 footerInfo();
			 Createblank();
			 footerDetails();
		}
			
			if(pepcoppflag==false){
				
				if(noOfLine==0&&productcount!=0){
					try {
						document.add(Chunk.NEXTPAGE);//------ remove this comment
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 if(preprintStatus.contains("yes")){
						createBlankforUPC();
						
					}else if(preprintStatus.contains("no")){
						createBlankforUPC();
						if(comp.getUploadHeader()!=null){
					    	createCompanyNameAsHeader(document,comp);
						}
						if(comp.getUploadFooter()!=null){
							createCompanyNameAsFooter(document,comp);
						}
						
					}
					Phrase ref = new Phrase(" ", font10bold);
					Paragraph pararef = new Paragraph();
					pararef.add(ref);
					pararef.setSpacingAfter(5f);
				   
				    try {
						document.add(pararef);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
									e.printStackTrace();
		}
				    
				    createRemainingProduct(productcount);
				    cretetotal();
				    footerInfo();
				//    Createblank();
				    footerDetails();
					}
				
			}
		}
		
		else{
			footerInfo();
			
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if(preprintStatus.contains("yes")){
				createBlankforUPC();
				
			}else if(preprintStatus.contains("no")){
				createBlankforUPC();
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
				
			}
			Phrase ref = new Phrase(" ", font10bold);
			Paragraph pararef = new Paragraph();
			pararef.add(ref);
			pararef.setSpacingAfter(5f);
		   
		    try {
				document.add(pararef);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    if(oldConDetFlag){
				createOldContractProductDetails();
			}
			createproductHeading();
			createProductInfonew();
			cretetotal();
			
			
		}
	    
		if(multipleServiceBranchesFlag&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal", "PrintServiceAddressOnRenewal", comp.getCompanyId())){
			PdfPTable annexTable=pdfUtility.print_customer_branches(cust, selectedBranches,branchWiseProductMap,0,0);
			try {
				document.newPage();//added on 27-08-2022
				document.add(annexTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
	
	
	
	
	
	
	

//	public void createPdf(String preprintStatus) {
//	
//		 if(preprintStatus.contains("plane"))
//		 {
//			createLogo(document,comp);
//			createCompanyAddress();
//		}else if(preprintStatus.contains("yes")){
//			createBlankforUPC();
//		}else if(preprintStatus.contains("no")){
//			createBlankforUPC();
//			if(comp.getUploadHeader()!=null){
//		    	createCompanyNameAsHeader(document,comp);
//			}
//			if(comp.getUploadFooter()!=null){
//				createCompanyNameAsFooter(document,comp);
//			}
//			
//		}
//		createHeading();
//		createCustomerDetails();
//		createSubjectAndMsg();
//		if(checkOldFormat==true){
//			if(oldConDetFlag){
//				createOldContractProductDetails();
//			}
//	   createproductHeading();
//	   createProductInfonew();
//	   if(noOfLine!=0){
//	   cretetotal();
//	   footerInfo();
//	  // Createblank();
//	   footerDetails();
//	   }
//		if(noOfLine==0&&productcount==0){
//			 cretetotal();
//			 footerInfo();
//			// Createblank();
//			 footerDetails();
//		}
//			
//			
//		}
//	}


	private void createRemainingProduct(int productcount2) {
//		
		
		
		if(pecoppflag){
			
			createproductHeading1();
			
		}else{
			createproductHeading();
		}
		
		
		PdfPTable table2 ;
		if(pecoppflag){
			 table2 = new PdfPTable(6); //Added by Ashwini
		}else{
			 table2 = new PdfPTable(8);
		}
		
		
		/**end**/
		table2.setWidthPercentage(100);
		try {
			if(pecoppflag){
				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f}); //Added by Ashwini
			}else{
				table2.setWidths(new float[]{3.3f,0.8f,1f,0.7f,0.7f,0.7f,1f,0.8f});
			}
			
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
           double consolidatedProdPrice = 0;
        
		for (int j = 0; j < con.getItems().size(); j++) {
			double calculatedPrice = con.getItems().get(j).getPrice();
			consolidatedProdPrice = consolidatedProdPrice + calculatedPrice;
		}
		double consolidatednetPayAmount=0;
		for (int k = 0; k < con.getItems().size(); k++) {
		double netPayAmount=0 ;
		
	     	if(con.getItems().get(k).getPrduct().getVatTax().isInclusive() && con.getItems().get(k).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(k).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(k).getPrice(),con.getItems().get(k).getServiceTax().getPercentage(),con.getItems().get(k).getVatTax().getPercentage(),
	        		 con.getItems().get(k).getVatTax().getTaxPrintName(),con.getItems().get(k).getServiceTax().getTaxPrintName());
	     	}
	     	consolidatednetPayAmount=consolidatednetPayAmount+netPayAmount;
		}
		for(int i=productcount2;i<con.getItems().size();i++){
			
			System.out.println("No of lines::"+noOfLine);
			System.out.println("product count::"+productcount);
			

			Phrase pdfoldprice;
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(con.getItems().get(i).getOldProductPrice())+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			
			/**End******/

			
			
			////
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_LEFT);
			 
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
	     				con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+con.getItems().get(i).getPrice());
	     	//End By Jayshreee
			
	     	

//	     	String prodPrice=;
	     	Phrase prodPrices=null;
	     	if(checkOldFormat){
	     		if(con.getItems().get(i).getPrice()!=0){
	     			prodPrices = new Phrase(df.format(con.getItems().get(i).getPrice()), font9);
				}else{
					prodPrices = new Phrase("0");
				}	
	     	}else{
	     		if(con.getItems().get(i).getPrice()!=0){
	     			prodPrices = new Phrase(df.format(newTaxAmount)+"", font9);
				}else{
					prodPrices = new Phrase("0");
				}	
	     	}
//	     	pdfnetPay = new PdfPCell(chunk);
			///// 
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
	     			chunktax = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(con.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==con.getItems().size()){
	     			 if(con.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==con.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
	        		 con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
//			pdfnetPay = new PdfPCell(netpayable);
//			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
			
			Calendar c = Calendar.getInstance();
			c.setTime(con.getItems().get(i).getStartDate());
			c.add(Calendar.DATE, con.getItems().get(i).getDuration() - 1);
			Date endDt = c.getTime();
			/**
			 * Date : 05/09/2017 BY ANIL
			 */
			String date = "";
			if (con.getItems().get(i).getEndDate() != null) {
				date = fmt.format(con.getItems().get(i).getStartDate()) + "\n"+ fmt.format(con.getItems().get(i).getEndDate());
			} else {
				date = fmt.format(con.getItems().get(i).getStartDate()) + "\n"+ fmt.format(endDt);
			}
			
			
			
			
			
//			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			Phrase startph=new Phrase(date, font9);
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/*** for HSN/SAC Code **/
			
		    if(con.getItems().get(i).getPrduct().getHsnNumber() != null){
		
				chunk =new Phrase(con.getItems().get(i).getPrduct().getHsnNumber(),font9);
				
			}else{
			chunk = new Phrase("");
			}
				
				pdfsacCode = new PdfPCell(chunk);
				pdfsacCode.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			
			table2.addCell(pdfname);
			table2.addCell(pdfsacCode);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			

			if(con.isConsolidatePrice()){

				if (i == 0) {
					chunk = new Phrase(df.format(consolidatedProdPrice), font9);
					pdfprice = new PdfPCell(chunk);
					if(con.getItems().size() > 1)
						pdfprice.setBorderWidthBottom(0);
					    pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdfprice);
				} else {
					chunk = new Phrase(" ", font9);
					pdfprice = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdfprice.setBorderWidthTop(0);
					}else{
						pdfprice.setBorderWidthBottom(0);
						pdfprice.setBorderWidthTop(0);
					}
					pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfprice);
				}
				
			}else{
				pdfprice = new PdfPCell(prodPrices);
				pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdfprice);
			}
//			table2.addCell(pdfprice);
			/***for taxes**/
			if(con.isConsolidatePrice()){

				if (i == 0) {
					
					pdftax = new PdfPCell(chunktax);
					if(con.getItems().size() > 1)
						pdftax.setBorderWidthBottom(0);
					    pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdftax);
				} else {
					chunk = new Phrase(" ", font9);
					pdftax = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdftax.setBorderWidthTop(0);
					}else{
						pdftax.setBorderWidthBottom(0);
						pdftax.setBorderWidthTop(0);
					}
					pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdftax);
				}
				
			}else{
				pdftax = new PdfPCell(chunktax);
		     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdftax);
			}
//			table2.addCell(pdftax);
			/***for net payable***/
			
			if(con.isConsolidatePrice()){

				if (i == 0) {
					chunk = new Phrase(df.format(consolidatednetPayAmount), font9);
					pdfnetPay = new PdfPCell(chunk);
					if(con.getItems().size() > 1)
						pdfnetPay.setBorderWidthBottom(0);
					    pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
					    table2.addCell(pdfnetPay);
				} else {
					chunk = new Phrase(" ", font9);
					pdfnetPay = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdfnetPay.setBorderWidthTop(0);
					}else{
						pdfnetPay.setBorderWidthBottom(0);
						pdfnetPay.setBorderWidthTop(0);
					}
					pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table2.addCell(pdfnetPay);
				}
				
			}else{
				pdfnetPay = new PdfPCell(netpayable);
				pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
				table2.addCell(pdfnetPay);
			}
//			table2.addCell(pdfnetPay);
			
			
			
		}
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	private double calculateNewTaxAmt(double newPrice , double serTax , double vatTax,String vatname,String sertaxname) {
		
		
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = newPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = newPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  newPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = newPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
		
		return totalAmt;
	}

	private double calculateOldTaxAmt(double oldPrice , double serTax , double vatTax,String vatname,String sertaxname) {
		
		
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = oldPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = oldPrice * (serTax/100);
				totalAmt = oldPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = oldPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = oldPrice * (serTax/100);
				totalAmt = oldPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  oldPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = oldPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
		
		return totalAmt;
	}

	private void cretetotal() {
		// TODO Auto-generated method stub
		/**   Date : 24-11-2017 BY MANISHA
		   * Description :To get the total amount on pdf..!!
		   * **/
		PdfPTable table3 ;
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		if(pecoppflag){
		 table3 = new PdfPTable(6);//Added by Ashwini
		}else{
		table3 = new PdfPTable(7);
		}
		/**end**/
		table3.setWidthPercentage(100);
		try {
			if(pecoppflag){
				table3.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
			}else{
			table3.setWidths(new float[]{3.7f,1.1f,1f,0.7f,0.7f,1f,0.8f});  //3.7f,1.1f,1f,0.7f,0.7f,1f,0.8f    2.5f,1.5f,1f,1f,1f,1f,1f
			}
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/**
		 * Date 9/1/2018
		 * By Jayshree
		 * Des.To add the amount in words 
		 */
		double totalamount = 0;
		System.out.println("totaAmount "+totaAmount);
		if(totaAmount>0){
			totalamount = totaAmount;
		}
		else{
			if(conRenw!=null) {
				totalamount = conRenw.getNetPayable();
			}
			else {
				totalamount = con.getNetpayable();
			}
		}
		
		totalamount = Math.round(totalamount);
		
//		String amount=SalesInvoicePdf.convert(con.getNetpayable());
		String amount=SalesInvoicePdf.convert(totalamount);
		String amtInWordsVal = "Amount in words : Rupees "+amount.toLowerCase()+ " only/-";
		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font1);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		if(pecoppflag){
			amtInWordsValCell.setColspan(4);	
		}else{
			amtInWordsValCell.setColspan(5);
		}
		
		table3.addCell(amtInWordsValCell);
		
		/*
		 * Added by Ashwini
		 */
		
	
			
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		table3.addCell(totcell);
		
		Phrase totalval = null;
		if(conRenw!=null){
//			totalval=new Phrase(Math.round(conRenw.getNetPayable())+"",font9);		// change by Viraj for decimal on Date 25-10-2018
			totalval=new Phrase(totalamount+"",font9);
		}
		else{
//			totalval=new Phrase(Math.round(con.getNetpayable())+"",font9);		// change by Viraj for decimal on Date 25-10-2018
			totalval=new Phrase(totalamount+"",font9);
		}
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table3.addCell(totalvalcell);
		
		
		
		
		
		try {
			document.add(table3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**End for Manisha**/
	}
	
	/*
	 * Date:10/11/2018
	 * Added by Ashwini
	 * Des:To add note coloumn
	 */
	
	private void createnote(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 10 ,Font.BOLD);
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		String  note = "Note : Price will be inclusive of applicable taxes" ;
		Phrase notephrase = new Phrase(note,font1);
		PdfPCell notecell = new PdfPCell(notephrase);
		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		notecell.setBorder(0);
		
		parent.addCell(notecell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createProductInfonew1(){
		

		PdfPTable table2 = new PdfPTable(6);
		/**end**/
		table2.setWidthPercentage(100);
		try {
			table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int i=0;i<con.getItems().size();i++){
			
			
			
			if(checkOldFormat==true){
			if(noOfLine==0){
				System.out.println("no of lins");
				productcount=i;
				break;
			}
			noOfLine=noOfLine-1;
			}

			Phrase pdfoldprice;
			/** Added by Viraj For Amt including Tax Calculation of oldPrice and newPrice **/
			
				double oldPrice = con.getItems().get(i).getOldProductPrice();
				double serOldPriceTax = oldPrice * (con.getItems().get(i).getServiceTax().getPercentage()/100);
				double vatOldPriceTax = oldPrice * (con.getItems().get(i).getVatTax().getPercentage()/100);
				System.out.println("ServiceTax: "+ serOldPriceTax);
				System.out.println("vatTax: "+ vatOldPriceTax);
				oldPrice = Math.round( con.getItems().get(i).getOldProductPrice() + serOldPriceTax + vatOldPriceTax );
			/** Ends **/
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(oldPrice)+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			System.out.println("oldPrice: "+pdfoldprice);
		
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_LEFT);
			 
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
		
				System.out.println("Product Type::"+con.getItems().get(i).getPrduct().getSpecification());
				
				if(con.getItems().get(i).getPrduct().getSpecification()!=null
						&& con.getItems().get(i).getPrduct().getSpecification().trim().length()>0){
				 chunk =new Phrase (con.getItems().get(i).getPrduct().getSpecification()+"",font9);
				 System.out.println("Product Type::"+con.getItems().get(i).getPrduct().getSpecification());
				}
			
			
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+con.getItems().get(i).getPrice());
	     	
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
	     	if(checkOldFormat){
	     		if(con.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}else{
	     		if(con.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}
			/**
			 * ends here
			 */
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
		
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
//	     		 if(st==con.getItems().size()){
	     			chunktax = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
//	     		 else{
//	     			 stno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     		 if(st==con.getItems().size()){
	     	    pdfservice = new PdfPCell(chunktax);
//	     		 }else{
//	     			 pdfservice = new PdfPCell(stno);
//	     		 }
	     	     /**
	     		  * ends here
	     		  */
	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(con.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==con.getItems().size()){
	     			 if(con.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 System.out.println("mukesh");
//	          	    vatno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage(),font9);
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==con.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	pdftax = new PdfPCell(chunktax);
	     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(df.format(netPayAmount)+"", font9);  // change by Viraj for decimal on Date 25-10-2018
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
			pdfnetPay = new PdfPCell(netpayable);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table2.addCell(pdfname);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			/**Manisha add column old price **/
		
			
			
			
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(oldpricevalcell);
			
			/**End**/
			table2.addCell(pdfprice);
//			table2.addCell(pdftax);
//			table2.addCell(pdfnetPay);
			
			
			
		}
		
		
		
		if(checkOldFormat==true){
			if(noOfLine==0&&productcount!=0){
				Phrase remainph=new Phrase (" ",font9bold);
				PdfPCell remaincell=new PdfPCell(remainph);
				remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
				remaincell.setColspan(8);
				table2.addCell(remaincell);
			}
		}
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
	}

	private void createProductInfonew() {
		
		
		
		PdfPTable table2 = null;
	
//		try {
//			table2.setWidths(new float[]{3.3f,0.8f,1f,0.7f,0.7f,0.7f,1f,0.8f});
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		/**
		 * @author Vijay Date : 09-12-2021
		 * Des :- if contract contains quantity then Qty will print on PDF 
		 * as per Nitin sir instructions
		 */
		if(contractQtyFlag){
			if(discountFlag) {
				table2 = new PdfPTable(10);
				try {
					table2.setWidths(new float[]{3.1f,0.7f,1.1f,0.7f,0.7f,0.7f,0.7f,0.7f,1.2f,0.8f});

				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}				
			}else {
				table2 = new PdfPTable(9);
				try {
					table2.setWidths(new float[]{3.1f,0.7f,1.1f,0.7f,0.7f,0.7f,0.7f,1.2f,0.8f});

				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		else{
			if(discountFlag) {
				table2 = new PdfPTable(9);
				try {
					table2.setWidths(new float[]{3.3f,0.8f,1f,0.7f,0.7f,0.7f,0.7f,1f,0.8f});
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}else {
				table2 = new PdfPTable(8);
				try {
					table2.setWidths(new float[]{3.3f,0.8f,1f,0.7f,0.7f,0.7f,1f,0.8f});
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		table2.setWidthPercentage(100);

		
		if(conRenw!=null){
			System.out.println("contract renewal entity");
			logger.log(Level.SEVERE,"conRenw!=null");
			/***for consolidated price**/
			double consolidatedProdPrice = 0;
			double consolidatedDiscount=0;
	        
			for (int j = 0; j < conRenw.getItems().size(); j++) {
				double calculatedPrice = conRenw.getItems().get(j).getPrice();
				consolidatedProdPrice = consolidatedProdPrice + calculatedPrice;
			
				//Ashwini Patil
				SalesLineItem  item=conRenw.getItems().get(j);
				double totalprice=item.getPrice();
				if(!item.getArea().equalsIgnoreCase("NA")  && !areaWiseCalRestricFlg){
					totalprice=item.getPrice()*Double.parseDouble(item.getArea());						
				}					
				double discAmt=0;
				double discPercentAmt=0;
				if(item.getDiscountAmt()>0)
					discAmt=item.getDiscountAmt();
				if(item.getPercentageDiscount()>0)
					discPercentAmt=totalprice*item.getPercentageDiscount()/100;				
				
				double totaldiscount = discAmt +discPercentAmt;
				consolidatedDiscount+=totaldiscount;
				//End
			}
			double consolidatednetPayAmount=0;
			for (int k = 0; k < conRenw.getItems().size(); k++) {
			double netPayAmount=0 ;
			
		     	if(conRenw.getItems().get(k).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(k).getPrduct().getServiceTax().isInclusive()){
		     		netPayAmount=conRenw.getItems().get(k).getPrice();
		     	}
		     	else{
		     	
		         netPayAmount = calculateTotalAmt(conRenw.getItems().get(k).getPrice(),conRenw.getItems().get(k).getServiceTax().getPercentage(),conRenw.getItems().get(k).getVatTax().getPercentage(),
		        		 conRenw.getItems().get(k).getVatTax().getTaxPrintName(),conRenw.getItems().get(k).getServiceTax().getTaxPrintName());
		     	}
		     	consolidatednetPayAmount=consolidatednetPayAmount+netPayAmount;
			}
			
			
			for(int i=0;i<conRenw.getItems().size();i++){
				
				System.out.println("No of lines::"+noOfLine);
				System.out.println("product count::"+productcount);
				

				if(pepcoppflag==false){
					
					if(checkOldFormat==true){
						if(noOfLine==0){
							System.out.println("no of lins");
							productcount=i;
							break;
							
						}
						noOfLine=noOfLine-1;
						}
				}
				
//				
				Phrase pdfoldprice;
				if(conRenw.getItems().get(i).getOldProductPrice()!=0){
					pdfoldprice = new Phrase(df.format(conRenw.getItems().get(i).getOldProductPrice())+"",font9);
				}else{
					pdfoldprice=new Phrase("0");
				}
				
			

				
				
				if(conRenw.getItems().get(i).getProductName()!=null){
					chunk = new Phrase(conRenw.getItems().get(i).getProductName() + "", font9);
				}else{
					chunk = new Phrase("");
				}
				
				pdfname = new PdfPCell(chunk);
				pdfname.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				/*** for HSN/SAC Code **/
				
				    if(conRenw.getItems().get(i).getPrduct().getHsnNumber() != null){
				
						chunk =new Phrase(conRenw.getItems().get(i).getPrduct().getHsnNumber(),font9);
						
					}else{
					chunk = new Phrase("");
					}
					
					
				pdfsacCode = new PdfPCell(chunk);
				pdfsacCode.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				 
//	            Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
				Calendar c = Calendar.getInstance();
				c.setTime(conRenw.getItems().get(i).getStartDate());
				c.add(Calendar.DATE, conRenw.getItems().get(i).getDuration() - 1);
				Date endDt = c.getTime();
	            String date = "";
				if (conRenw.getItems().get(i).getEndDate() != null) {
					date = fmt.format(conRenw.getItems().get(i).getStartDate()) +"\n"+ fmt.format(conRenw.getItems().get(i).getEndDate());
				} else {
					date = fmt.format(conRenw.getItems().get(i).getStartDate()) + "\n"+ fmt.format(endDt);
				}
	            
	            
				 Phrase startph=new Phrase (date,font9);
				PdfPCell startcell=new PdfPCell(startph);
				startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				
				
				
				
				
				
				 /////
				if(conRenw.getItems().get(i).getDuration()!=-1){
					 chunk =new Phrase (conRenw.getItems().get(i).getDuration()+"",font9);
				}
				else{
					 chunk= new Phrase("");
				}
				pdfduration = new PdfPCell(chunk);
				pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				////
				if(conRenw.getItems().get(i).getNumberOfServices()!=-1){
					 chunk =new Phrase (conRenw.getItems().get(i).getNumberOfServices()+"",font9);
				}
				else{
					 chunk= new Phrase("");
				}
				pdfservices = new PdfPCell(chunk);
				pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			
				double newTaxAmount=0 ;
		     	
		     	if(conRenw.getItems().get(i).getPrduct().getVatTax().isInclusive() && conRenw.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
		     		newTaxAmount=conRenw.getItems().get(i).getPrice();
		     	}
		     	else{
		     	
		     		newTaxAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
		     				conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());
		     	}
				
		     	System.out.println("new prise"+conRenw.getItems().get(i).getPrice());
		     	//End By Jayshreee
				
		     
		     	Phrase prodPrices=null;
		     	if(checkOldFormat){
		     		if(conRenw.getItems().get(i).getPrice()!=0){
		     			prodPrices = new Phrase(df.format(conRenw.getItems().get(i).getPrice()), font9);
					}else{
						prodPrices = new Phrase("0", font9);//Ashwini Patil 20-12-2023 font9 added as zero was getting displayed in larger font
					}	
		     	}else{
		     		if(conRenw.getItems().get(i).getPrice()!=0){
		     			prodPrices = new Phrase(df.format(newTaxAmount)+"", font9);
					}else{
						prodPrices = new Phrase("0", font9);//Ashwini Patil 20-12-2023 font9 added as zero was getting displayed in larger font
					}	
		     	}
	 
				
			   	 PdfPCell pdfservice =null;
		     	 
		     	 boolean taxNAFlag = false;
		     	Phrase chunktax = null;
		     	
		     	 if((conRenw.getItems().get(i).getVatTax().getPercentage()==0)&&(conRenw.getItems().get(i).getServiceTax().getPercentage()>0))
		     	 {
		     		
		     	 
		     		 if(conRenw.getItems().get(i).getServiceTax().getPercentage()!=0){
		     			chunktax = new Phrase(conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+" "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"%",font9);
		     		 }
		     	    else{
		     	    	chunktax = new Phrase("N.A"+"",font9);
		     	    	taxNAFlag = true;
		     	    }

		     	 }
		     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()==0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0))
		          {
		     		
		     		 
		     		 if(conRenw.getItems().get(i).getVatTax()!=null){

		     			 if(conRenw.getItems().get(i).getVatTax().getPercentage()>0){
		     				 System.out.println("rohan=="+vat);
		     				chunktax = new Phrase(conRenw.getItems().get(i).getVatTax().getTaxPrintName()+" "+conRenw.getItems().get(i).getVatTax().getPercentage()+"%",font9);
		     			 }else{
		     				 taxNAFlag = true;
		     				 chunktax = new Phrase("N.A"+"",font9);

		     			 }
		     		 }
		          	    else{
		          	    	chunktax = new Phrase("N.A"+"",font9);
		          	    }

		          	  pdfservice = new PdfPCell(chunktax);
		          	  pdfservice.setHorizontalAlignment(Element.ALIGN_LEFT);
		          	 
		      
		     	 }
		     	 else if((conRenw.getItems().get(i).getServiceTax().getPercentage()>0)&&(conRenw.getItems().get(i).getVatTax().getPercentage()>0)){
		     		 if((conRenw.getItems().get(i).getVatTax()!=null)&& (conRenw.getItems().get(i).getServiceTax()!=null))
		     			 
		     			chunktax = new Phrase("  "+conRenw.getItems().get(i).getVatTax().getTaxPrintName()+" "+conRenw.getItems().get(i).getVatTax().getPercentage()+"%"+"\n"
		     					+"  "+conRenw.getItems().get(i).getServiceTax().getTaxPrintName()+" "+conRenw.getItems().get(i).getServiceTax().getPercentage()+"%",font9);
		           	 else{
			           	 chunktax = new Phrase("N.A"+"",font9);
			           	 taxNAFlag = true;
		           	 }
		           	  pdfservice = new PdfPCell(chunktax);
		           	 pdfservice.setHorizontalAlignment(Element.ALIGN_LEFT);
		     		 
		     	 }else{
		     		taxNAFlag = true;
		     		chunktax = new Phrase("N.A"+"",font9);
		           	  pdfservice = new PdfPCell(chunktax);
		           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		     		 
		     	 }
				
		 
		     	double netPayAmount=0 ;
		     	
		     	System.out.println("conRenw.getItems().get(i).getPrduct().getVatTax().getPercentage() "+conRenw.getItems().get(i).getPrduct().getVatTax().getPercentage());
		     	System.out.println("conRenw.getItems().get(i).getPrduct().getServiceTax().getPercentage()!=0 "+conRenw.getItems().get(i).getPrduct().getServiceTax().getPercentage());
		     	if(conRenw.getItems().get(i).getPrduct().getVatTax().getPercentage()==0.0 && conRenw.getItems().get(i).getPrduct().getServiceTax().getPercentage()==0.0){
		     		System.out.println("here we are");
		     		netPayAmount=conRenw.getItems().get(i).getPrice();
		     		if(contractQtyFlag){
		     			Double qty = Double.parseDouble(conRenw.getItems().get(i).getArea());
		     			netPayAmount = conRenw.getItems().get(i).getPrice() * qty;
		     			
		     		}
		     	}
		     	else{
		     		if(contractQtyFlag){
		     			Double qty = 1.0;
		     			if(conRenw.getItems().get(i).getArea()!=null&&!conRenw.getItems().get(i).getArea().equals("")&&!conRenw.getItems().get(i).getArea().equals("NA"))//Ashwini Patil 20-12-2023 condition added on 20-12-2023
		     				qty=Double.parseDouble(conRenw.getItems().get(i).getArea());
		     			netPayAmount = conRenw.getItems().get(i).getPrice() * qty;
		     			 netPayAmount = calculateTotalAmt(netPayAmount,conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
				        		 conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());

		     		}
		     		else{
		     			 netPayAmount = calculateTotalAmt(conRenw.getItems().get(i).getPrice(),conRenw.getItems().get(i).getServiceTax().getPercentage(),conRenw.getItems().get(i).getVatTax().getPercentage(),
				        		 conRenw.getItems().get(i).getVatTax().getTaxPrintName(),conRenw.getItems().get(i).getServiceTax().getTaxPrintName());

		     		}
		        }
		     	totaAmount += netPayAmount;
		     	
				System.out.println("netPayAmount "+netPayAmount);
				
		     	Phrase netpayable = null;
				if(netPayAmount!=0){
					netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
				}else{
					
					netpayable = new Phrase("0",font9);
				}
			
				
				if(conRenw.getItems().get(i).getArea()!=null && !conRenw.getItems().get(i).getArea().equals("")){
						try {
							double area = Double.parseDouble(conRenw.getItems().get(i).getArea());
							 chunk =new Phrase (area+"",font9);

						} catch (Exception e) {
							e.printStackTrace();
							chunk= new Phrase("");
						}
				}
				else{
					 chunk= new Phrase("");
				}
				pdfQty = new PdfPCell(chunk);
				pdfQty.setHorizontalAlignment(Element.ALIGN_CENTER);
				
		
				table2.addCell(pdfname);
				table2.addCell(pdfsacCode);
				table2.addCell(startcell);
				table2.addCell(pdfduration);
				table2.addCell(pdfservices);
				
				if(contractQtyFlag){
					table2.addCell(pdfQty);
				}

//				if(conRenw.isConsolidatePrice()){
//
//					if (i == 0) {
//						chunk = new Phrase(df.format(consolidatedProdPrice), font9);
//						pdfprice = new PdfPCell(chunk);
//						if(con.getItems().size() > 1)
//							pdfprice.setBorderWidthBottom(0);
//						    pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//						    table2.addCell(pdfprice);
//					} else {
//						chunk = new Phrase(" ", font9);
//						pdfprice = new PdfPCell(chunk);
//						if(i == con.getItems().size()-1){							
//							pdfprice.setBorderWidthTop(0);
//						}else{
//							pdfprice.setBorderWidthBottom(0);
//							pdfprice.setBorderWidthTop(0);
//						}
//						pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table2.addCell(pdfprice);
//					}
//					
//				}else{
					pdfprice = new PdfPCell(prodPrices);
					pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfprice);
//				}
					
					
				if(discountFlag) {
						logger.log(Level.SEVERE,"in discountFlag");
//						if(con.isConsolidatePrice()){
//							PdfPCell discCell=null;
//							if (i == 0) {
//								chunk = new Phrase(df.format(consolidatedDiscount), font9);
//								discCell = new PdfPCell(chunk);
//								if(con.getItems().size() > 1)
//									discCell.setBorderWidthBottom(0);
//									discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//								    table2.addCell(discCell);
//							} else {
//								chunk = new Phrase(" ", font9);
//								discCell = new PdfPCell(chunk);
//								if(i == con.getItems().size()-1){							
//									discCell.setBorderWidthTop(0);
//								}else{
//									discCell.setBorderWidthBottom(0);
//									discCell.setBorderWidthTop(0);
//								}
//								discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//								table2.addCell(discCell);						
//							}
//						}						
//						else{
							SalesLineItem  item=conRenw.getItems().get(i);
							double totalprice=item.getPrice();
							if(!item.getArea().equalsIgnoreCase("NA")  && !areaWiseCalRestricFlg){
								totalprice=item.getPrice()*Double.parseDouble(item.getArea());						
							}					
							double discAmt=0;
							double discPercentAmt=0;
							if(item.getDiscountAmt()>0)
								discAmt=item.getDiscountAmt();
							if(item.getPercentageDiscount()>0)
								discPercentAmt=totalprice*item.getPercentageDiscount()/100;				
							
							double totaldiscount = discAmt +discPercentAmt;
							logger.log(Level.SEVERE,"totaldiscount="+totaldiscount);
							Phrase disc = new Phrase(df.format(totaldiscount)+"",font9);
							PdfPCell discCell = new PdfPCell(disc);
							discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							table2.addCell(discCell);	
//						}
										
					}

				/***for taxes**/
//				if(con.isConsolidatePrice()){
//
//					if (i == 0) {
//						
//						pdftax = new PdfPCell(chunktax);
//						if(con.getItems().size() > 1)
//							pdftax.setBorderWidthBottom(0);
//						    pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
//						    table2.addCell(pdftax);
//					} else {
//						chunk = new Phrase(" ", font9);
//						pdftax = new PdfPCell(chunk);
//						if(i == con.getItems().size()-1){							
//							pdftax.setBorderWidthTop(0);
//						}else{
//							pdftax.setBorderWidthBottom(0);
//							pdftax.setBorderWidthTop(0);
//						}
//						pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
//						table2.addCell(pdftax);
//					}
//					
//				}else{
					pdftax = new PdfPCell(chunktax);
					if(taxNAFlag){
				     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					}
					else{
				     	pdftax.setHorizontalAlignment(Element.ALIGN_LEFT);
					}
					table2.addCell(pdftax);
//				}

				/***for net payable***/
				
//				if(con.isConsolidatePrice()){
//
//					if (i == 0) {
//						chunk = new Phrase(df.format(consolidatednetPayAmount), font9);
//						pdfnetPay = new PdfPCell(chunk);
//						if(con.getItems().size() > 1)
//							pdfnetPay.setBorderWidthBottom(0);
//						    pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						    table2.addCell(pdfnetPay);
//					} else {
//						chunk = new Phrase(" ", font9);
//						pdfnetPay = new PdfPCell(chunk);
//						if(i == con.getItems().size()-1){							
//							pdfnetPay.setBorderWidthTop(0);
//						}else{
//							pdfnetPay.setBorderWidthBottom(0);
//							pdfnetPay.setBorderWidthTop(0);
//						}
//						pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
//						table2.addCell(pdfnetPay);
//					}
//					
//				}else{
					pdfnetPay = new PdfPCell(netpayable);
					pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table2.addCell(pdfnetPay);
//				}
//				table2.addCell(pdfnetPay);
				
					
					/**
					 * @author Vijay Date :- 15-07-2023
					 * Des :- to show print premises and remark
					 */
					int colsspan = 8;
				
					if(contractQtyFlag){
						if(discountFlag) {
							colsspan = 10;
						}
						else{
							colsspan =9;
						}
					}
					else{
						if(discountFlag) {
							colsspan =9;
						}
						else{
							colsspan =8;
						}
						
					}
					
					if(conRenw.getItems().get(i).getPremisesDetails()!=null && !conRenw.getItems().get(i).getPremisesDetails().equals("")){
						chunk = new Phrase(conRenw.getItems().get(i).getPremisesDetails(), font9);
						pdfPremises = new PdfPCell(chunk);
						pdfPremises.setHorizontalAlignment(Element.ALIGN_LEFT);
						pdfPremises.setColspan(colsspan);
						table2.addCell(pdfPremises);
						noOfLine = noOfLine-1;
					}
					
					if(conRenw.getItems().get(i).getRemark()!=null && !conRenw.getItems().get(i).getRemark().equals("")){
						chunk = new Phrase(conRenw.getItems().get(i).getRemark(), font9);
						pdfRemark = new PdfPCell(chunk);
						pdfRemark.setHorizontalAlignment(Element.ALIGN_LEFT);
						pdfRemark.setColspan(colsspan);
						table2.addCell(pdfRemark);
						noOfLine = noOfLine-1;
					}
					/**
					 * ends here
					 */
				
			}
			
		}
		else{
			logger.log(Level.SEVERE,"conRenw==null");
			/***for consolidated price**/
			double consolidatedProdPrice = 0;
			double consolidatedDiscount=0;
	        
			for (int j = 0; j < con.getItems().size(); j++) {
				double calculatedPrice = con.getItems().get(j).getPrice();
				consolidatedProdPrice = consolidatedProdPrice + calculatedPrice;
				
				//Ashwini Patil
				SalesLineItem  item=con.getItems().get(j);
				double totalprice=item.getPrice();
					
				if(!item.getArea().equalsIgnoreCase("NA")  && !areaWiseCalRestricFlg){
					totalprice=item.getPrice()*Double.parseDouble(item.getArea());						
				}					
				double discAmt=0;
				double discPercentAmt=0;
				if(item.getDiscountAmt()>0)
					discAmt=item.getDiscountAmt();
				if(item.getPercentageDiscount()>0)
					discPercentAmt=totalprice*item.getPercentageDiscount()/100;				
				
				double totaldiscount = discAmt +discPercentAmt;
				consolidatedDiscount+=totaldiscount;
				//End
			}
			double consolidatednetPayAmount=0;
			for (int k = 0; k < con.getItems().size(); k++) {
			double netPayAmount=0 ;
			
		     	if(con.getItems().get(k).getPrduct().getVatTax().isInclusive() && con.getItems().get(k).getPrduct().getServiceTax().isInclusive()){
		     		netPayAmount=con.getItems().get(k).getPrice();
		     	}
		     	else{
		     	
		         netPayAmount = calculateTotalAmt(con.getItems().get(k).getPrice(),con.getItems().get(k).getServiceTax().getPercentage(),con.getItems().get(k).getVatTax().getPercentage(),
		        		 con.getItems().get(k).getVatTax().getTaxPrintName(),con.getItems().get(k).getServiceTax().getTaxPrintName());
		     	}
		     	consolidatednetPayAmount=consolidatednetPayAmount+netPayAmount;
			}
			
			for(int i=0;i<con.getItems().size();i++){
				
				System.out.println("No of lines::"+noOfLine);
				System.out.println("product count::"+productcount);
				

				if(pepcoppflag==false){
					
					if(checkOldFormat==true){
						if(noOfLine==0){
							System.out.println("no of lins");
							productcount=i;
							break;
							
						}
						noOfLine=noOfLine-1;
						}
				}
				
//				
				Phrase pdfoldprice;
				if(con.getItems().get(i).getOldProductPrice()!=0){
					pdfoldprice = new Phrase(df.format(con.getItems().get(i).getOldProductPrice())+"",font9);
				}else{
					pdfoldprice=new Phrase("0");
				}
				
			

				
				
				if(con.getItems().get(i).getProductName()!=null){
					chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
				}else{
					chunk = new Phrase("");
				}
				
				pdfname = new PdfPCell(chunk);
				pdfname.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				
				/*** for HSN/SAC Code **/
				
				    if(con.getItems().get(i).getPrduct().getHsnNumber() != null){
				
						chunk =new Phrase(con.getItems().get(i).getPrduct().getHsnNumber(),font9);
						
					}else{
					chunk = new Phrase("");
					}
					
					
				pdfsacCode = new PdfPCell(chunk);
				pdfsacCode.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				 
//	            Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
				Calendar c = Calendar.getInstance();
				c.setTime(con.getItems().get(i).getStartDate());
				c.add(Calendar.DATE, con.getItems().get(i).getDuration() - 1);
				Date endDt = c.getTime();
	            String date = "";
				if (con.getItems().get(i).getEndDate() != null) {
					date = fmt.format(con.getItems().get(i).getStartDate()) +"\n"+ fmt.format(con.getItems().get(i).getEndDate());
				} else {
					date = fmt.format(con.getItems().get(i).getStartDate()) + "\n"+ fmt.format(endDt);
				}
	            
	            
				 Phrase startph=new Phrase (date,font9);
				PdfPCell startcell=new PdfPCell(startph);
				startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				
				
				
				
				
				
				 /////
				if(con.getItems().get(i).getDuration()!=-1){
					 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
				}
				else{
					 chunk= new Phrase("");
				}
				pdfduration = new PdfPCell(chunk);
				pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
				
				////
				if(con.getItems().get(i).getNumberOfServices()!=-1){
					 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
				}
				else{
					 chunk= new Phrase("");
				}
				pdfservices = new PdfPCell(chunk);
				pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
				
			
				double newTaxAmount=0 ;
		     	
		     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
		     		newTaxAmount=con.getItems().get(i).getPrice();
		     	}
		     	else{
		     	
		     		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
		     				con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
		     	}
				
		     	System.out.println("new prise"+con.getItems().get(i).getPrice());
		     	//End By Jayshreee
				
		     
		     	Phrase prodPrices=null;
		     	if(checkOldFormat){
		     		if(con.getItems().get(i).getPrice()!=0){
		     			prodPrices = new Phrase(df.format(con.getItems().get(i).getPrice()), font9);
					}else{
						prodPrices = new Phrase("0");
					}	
		     	}else{
		     		if(con.getItems().get(i).getPrice()!=0){
		     			prodPrices = new Phrase(df.format(newTaxAmount)+"", font9);
					}else{
						prodPrices = new Phrase("0");
					}	
		     	}
	 
				
			   	 PdfPCell pdfservice =null;
		     	 
		     	 
		     	Phrase chunktax = null;
		     	
		     	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
		     	 {
		     		
		     	 
		     		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
		     			chunktax = new Phrase(con.getItems().get(i).getServiceTax().getTaxPrintName()+" "+con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
		     		 }
		     	    else
		     	    	chunktax = new Phrase("N.A"+"",font9);

		     	 }
		     	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
		          {
		     		
		     		 
		     		 if(con.getItems().get(i).getVatTax()!=null){

		     			 if(con.getItems().get(i).getVatTax().getPercentage()>0){
		     				 System.out.println("rohan=="+vat);
		     				chunktax = new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+" "+con.getItems().get(i).getVatTax().getPercentage()+"",font9);
		     			 }else{
		     				 chunktax = new Phrase("N.A"+"",font9);

		     			 }
		     		 }
		          	    else{
		          	    	chunktax = new Phrase("N.A"+"",font9);
		          	    }

		          	  pdfservice = new PdfPCell(chunktax);
		          	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		          	 
		      
		     	 }
		     	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
		     		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
		     			 
		     			chunktax = new Phrase("  "+con.getItems().get(i).getVatTax().getTaxPrintName()+" "+con.getItems().get(i).getVatTax().getPercentage()+"%"+"\n"
		     					+"  "+con.getItems().get(i).getServiceTax().getTaxPrintName()+" "+con.getItems().get(i).getServiceTax().getPercentage()+"%",font9);
		           	 else
		           		chunktax = new Phrase("N.A"+"",font9);
		           	  pdfservice = new PdfPCell(chunktax);
		           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		     		 
		     	 }else{
		     		 
		     		chunktax = new Phrase("N.A"+"",font9);
		           	  pdfservice = new PdfPCell(chunktax);
		           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
		     		 
		     	 }
				
		 
		     	double netPayAmount=0 ;
		     	
		     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
		     		netPayAmount=con.getItems().get(i).getPrice();
		     		if(contractQtyFlag){
		     			Double qty = Double.parseDouble(con.getItems().get(i).getArea());
		     			netPayAmount = con.getItems().get(i).getPrice() * qty;
		     			
		     		}
		     	}
		     	else{
		     	
		     		if(contractQtyFlag){
		     			Double qty = Double.parseDouble(con.getItems().get(i).getArea());
		     			netPayAmount = con.getItems().get(i).getPrice() * qty;
		     			 netPayAmount = calculateTotalAmt(netPayAmount,con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
		     					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());

		     		}
		     		else{
		     			 netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
				        		 con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
		     		}
		        }
				
				
		     	Phrase netpayable = null;
				if(netPayAmount!=0){
					netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
				}else{
					
					netpayable = new Phrase("0",font9);
				}
			
				if(con.getItems().get(i).getArea()!=null && !con.getItems().get(i).getArea().equalsIgnoreCase("NA")){
					try {
						double area = Double.parseDouble(con.getItems().get(i).getArea());
						 chunk =new Phrase (area+"",font9);

					} catch (Exception e) {
						e.printStackTrace();
						chunk= new Phrase("");
					}
				}
				else{
					 chunk= new Phrase("");
				}
				pdfQty = new PdfPCell(chunk);
				pdfQty.setHorizontalAlignment(Element.ALIGN_CENTER);
			
		
				table2.addCell(pdfname);
				table2.addCell(pdfsacCode);
				table2.addCell(startcell);
				table2.addCell(pdfduration);
				table2.addCell(pdfservices);
				
				if(contractQtyFlag){
					table2.addCell(pdfQty);
				}
				
				if(con.isConsolidatePrice()){

					if (i == 0) {
						chunk = new Phrase(df.format(consolidatedProdPrice), font9);
						pdfprice = new PdfPCell(chunk);
						if(con.getItems().size() > 1)
							pdfprice.setBorderWidthBottom(0);
						    pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
						    table2.addCell(pdfprice);
					} else {
						chunk = new Phrase(" ", font9);
						pdfprice = new PdfPCell(chunk);
						if(i == con.getItems().size()-1){							
							pdfprice.setBorderWidthTop(0);
						}else{
							pdfprice.setBorderWidthBottom(0);
							pdfprice.setBorderWidthTop(0);
						}
						pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
						table2.addCell(pdfprice);
					}
					
				}else{
					pdfprice = new PdfPCell(prodPrices);
					pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfprice);
				}

				if(discountFlag) {
					logger.log(Level.SEVERE,"in discountFlag");
					if(con.isConsolidatePrice()){
						PdfPCell discCell=null;
						if (i == 0) {
							chunk = new Phrase(df.format(consolidatedDiscount), font9);
							discCell = new PdfPCell(chunk);
							if(con.getItems().size() > 1)
								discCell.setBorderWidthBottom(0);
								discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							    table2.addCell(discCell);
						} else {
							chunk = new Phrase(" ", font9);
							discCell = new PdfPCell(chunk);
							if(i == con.getItems().size()-1){							
								discCell.setBorderWidthTop(0);
							}else{
								discCell.setBorderWidthBottom(0);
								discCell.setBorderWidthTop(0);
							}
							discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
							table2.addCell(discCell);						
						}
					}						
					else{
						SalesLineItem  item=con.getItems().get(i);
						double totalprice=item.getPrice();
						if(!item.getArea().equalsIgnoreCase("NA")  && !areaWiseCalRestricFlg){
							totalprice=item.getPrice()*Double.parseDouble(item.getArea());						
						}					
						double discAmt=0;
						double discPercentAmt=0;
						if(item.getDiscountAmt()>0)
							discAmt=item.getDiscountAmt();
						if(item.getPercentageDiscount()>0)
							discPercentAmt=totalprice*item.getPercentageDiscount()/100;				
						
						double totaldiscount = discAmt +discPercentAmt;
						logger.log(Level.SEVERE,"totaldiscount="+totaldiscount);
						Phrase disc = new Phrase(df.format(totaldiscount)+"",font9);
						PdfPCell discCell = new PdfPCell(disc);
						discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
						table2.addCell(discCell);	
					}
									
				}
				
				/***for taxes**/
				if(con.isConsolidatePrice()){

					if (i == 0) {
						
						pdftax = new PdfPCell(chunktax);
						if(con.getItems().size() > 1)
							pdftax.setBorderWidthBottom(0);
						    pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
						    table2.addCell(pdftax);
					} else {
						chunk = new Phrase(" ", font9);
						pdftax = new PdfPCell(chunk);
						if(i == con.getItems().size()-1){							
							pdftax.setBorderWidthTop(0);
						}else{
							pdftax.setBorderWidthBottom(0);
							pdftax.setBorderWidthTop(0);
						}
						pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
						table2.addCell(pdftax);
					}
					
				}else{
					pdftax = new PdfPCell(chunktax);
			     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdftax);
				}

				/***for net payable***/
				
				if(con.isConsolidatePrice()){

					if (i == 0) {
						chunk = new Phrase(df.format(consolidatednetPayAmount), font9);
						pdfnetPay = new PdfPCell(chunk);
						if(con.getItems().size() > 1)
							pdfnetPay.setBorderWidthBottom(0);
						    pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
						    table2.addCell(pdfnetPay);
					} else {
						chunk = new Phrase(" ", font9);
						pdfnetPay = new PdfPCell(chunk);
						if(i == con.getItems().size()-1){							
							pdfnetPay.setBorderWidthTop(0);
						}else{
							pdfnetPay.setBorderWidthBottom(0);
							pdfnetPay.setBorderWidthTop(0);
						}
						pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
						table2.addCell(pdfnetPay);
					}
					
				}else{
					pdfnetPay = new PdfPCell(netpayable);
					pdfnetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
					table2.addCell(pdfnetPay);
				}
//				table2.addCell(pdfnetPay);
				
				
				
			}
			
			
		}
		

		if(pepcoppflag==false){
			
			if(checkOldFormat==true){
				if(noOfLine==0&&productcount!=0){
					Phrase remainph=new Phrase (" ",font9bold);
					PdfPCell remaincell=new PdfPCell(remainph);
					remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
					remaincell.setColspan(8);
					table2.addCell(remaincell);
				}
			}
			
		}
		
		/*
		 * end by Ashwini
		 */
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		  
		
		 
	}

	
	private void createproductHeading1(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f};
		
		PdfPTable table = new PdfPTable(6);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase name = new Phrase("NAME", font1);
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase startdate=new Phrase ("START/END DATE",font1);
		PdfPCell startdateph = new PdfPCell(startdate);
		startdateph.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase duration = new Phrase("DURATION",font1);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase services = new Phrase("SERVICES", font1);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase oldprice=new Phrase("PREVIOUS PRICE",font1);
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase price = new Phrase("NEW PRICE", font1);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		
		table.addCell(cellname);
		table.addCell(startdateph);
		table.addCell(cellduration);
		table.addCell(cellservices);
		table.addCell(celloldprice);
		table.addCell(cellprice);
		
		try{
			document.add(table);
			
		}catch(DocumentException e1){
			e1.printStackTrace();
			
		}
		
}

	/*
	 * end by Ashwini
	 */
	private void createproductHeading() {
			
		for(SalesLineItem lineitem : con.getItems()){
			if(lineitem.getArea()!=null && !lineitem.getArea().equals("")&& !lineitem.getArea().equals("NA")){ //Ashwini Patil 20-12-2023 NA condition added on 20-12-2023 as it was giving NumberFormatException 
				try {
					double qtyvalue = Double.parseDouble(lineitem.getArea());
					contractQtyFlag = true;
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
//		float[] relativeWidths = {3.3f,0.8f,1f,0.7f,0.7f,0.7f,1f,0.8f};  // 3.3f,0.8f,1f,0.7f,0.7f,0.7f,1f,0.8f  // 2f,1f,1f,1f,1f,1f,1f,1f
		
	
	//		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f,1f};
//		PdfPTable table = new PdfPTable(8);
//		/**end**/
//		table.setWidthPercentage(100);
//		
//		try {
//			table.setWidths(relativeWidths);
//		} catch (DocumentException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		System.out.println("contractQtyFlag == "+contractQtyFlag);
		
		Phrase sac=new Phrase("SAC",font1);
		Phrase netPay = new Phrase("Total", font1);
		Phrase name = new Phrase("Name", font1);
		Phrase duration = new Phrase("Duration (Day's)",font1);  // Duration (Day's)

		Phrase services = new Phrase("No.Of Services", font1); //Nr. Of Services
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		
		//Phrase oldprice=new Phrase("OLD PRICE",font1);  //commented by Ashwini
		/*
		 * Date:12/10/2018
		 * Developer:Ashwini
		 */
		Phrase oldprice = null;
		if(pepcoppflag){
			 oldprice=new Phrase("PREVIOUS PRICE",font1);
		}else{
			 oldprice=new Phrase("OLD PRICE",font1);
		}
		
		/*
		 * end by Ashwini
		 */
		Phrase price = new Phrase("Rate", font1);
		/**enD**/
		
		/** Date 4/1/2018
		 * By jayshree
		 * Des.add startdate and end date column
		 */
		Phrase startdate=new Phrase ("Start/End Date",font1);
		
		
		   Phrase servicetax= null;
		      Phrase servicetax1= null;
		      int flag=0;
		      int vat=0;
		      int st=0;
		      
		      
		      /*Date :22/11/2017  By:Manisha
		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
		       */
		   for(int i=0; i<con.getItems().size();i++){
		    	
			   //Ashwini Patil Date:27-09-2022 Added discount flag
			   if(con.getItems().get(i).getDiscountAmt()>0||con.getItems().get(i).getPercentageDiscount()>0) {
				   discountFlag=true;
			   }
			   
		    if (con.getItems().get(i).getVatTax().getTaxPrintName().trim()!= null
							&& !con.getItems().get(i).getServiceTax().getTaxPrintName().trim().equalsIgnoreCase(""))
		    {		    	
		    	
		    	       if(con.getItems().get(i).getVatTax().getTaxPrintName().trim()!=null
		    			&& !con.getItems().get(i).getServiceTax().getTaxPrintName().trim().equalsIgnoreCase(""))
		    			{
		    		
		    		       servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"/"
		    		                    +con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
		    		       flag=flag+1;
		    			} 
		    	       else
		    	       {
		    	    	   
		    	    	   servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
		    	    	   vat=vat+1;
		    	    	   
		    	       }
		    	
		    	
		    }
		    
		    else{
		    	   
		    	if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()==0)){
		  	       servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+"%",font1);
		  	       vat=vat+1;
		  	       System.out.println("phrase value===="+servicetax.toString());
		  	      }
		    	

			      else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()==0)
			    		  &&(con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
			      {
			      	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  st=st+1;
			      }
		    	
			      else if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0)
			    		  && (con.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (con.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
			    		 && (con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
			       {
			      	  servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  flag=flag+1;
			      	  System.out.println("flag value;;;;;"+flag);
			      }
		    	 
			    else if ((con.getItems().get(i).getServiceTax().getPercentage()>0) && (con.getItems().get(i).getVatTax().getPercentage()==0)
			    		&&(con.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!con.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
			    {
			    	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
			    	  st=st+1;
				}else{
			    	  servicetax = new Phrase(" ",font1);

				}
		    }
		     
		      
		      
//		       else{
//		      	 
//		      	  servicetax = new Phrase("TAX %",font1);
//		       }
		    }
		   
		   //Ashwini Patil Date:10-07-2024 For ultra
		   if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("ContractRenewal","DoNotPrintDiscountColumnOnRenewalPrint" , comp.getCompanyId())){
				discountFlag=false;			
			}
		      
//		  Phrase servicetaxes = new Phrase("SGST/CGST  % ",font1);
		      
		  Phrase servicetaxes = new Phrase("Tax ",font1);

		  	PdfPCell celltax = new PdfPCell(servicetaxes);
		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		PdfPCell sacCell=new PdfPCell(sac);
		sacCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell startdateph = new PdfPCell(startdate);
		startdateph.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellvat = new PdfPCell(vat);
//		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellstax = new PdfPCell(stax);
//		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		/**Manisha add column old price **/
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**End**/
		
		Phrase qty=new Phrase("Qty",font1);
		PdfPCell cellQty = new PdfPCell(qty);
		cellQty.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		PdfPTable table = null;
		if(contractQtyFlag){
			
			if(discountFlag) {
				table = new PdfPTable(10);
				float[] relativeWidths = {3.1f,0.7f,1.1f,0.7f,0.7f,0.7f,0.7f,0.7f,1.2f,0.8f};
				try {
					table.setWidths(relativeWidths);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}else {
				table = new PdfPTable(9);
				float[] relativeWidths = {3.1f,0.7f,1.1f,0.7f,0.7f,0.7f,0.7f,1.2f,0.8f};
				try {
					table.setWidths(relativeWidths);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			
		}
		else{
			if(discountFlag) {
				table = new PdfPTable(9);
				float[] relativeWidths = {3.3f,0.8f,1f,0.7f,0.7f,0.7f,0.7f,1f,0.8f};
				try {
					table.setWidths(relativeWidths);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}else {
				table = new PdfPTable(8);
				float[] relativeWidths = {3.3f,0.8f,1f,0.7f,0.7f,0.7f,1f,0.8f};
				try {
					table.setWidths(relativeWidths);
				} catch (DocumentException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
		table.setWidthPercentage(100);
		
		
		table.addCell(cellname);
		table.addCell(sacCell);
		table.addCell(startdateph);
		table.addCell(cellduration);
		table.addCell(cellservices);
		/**Manisha add column old price **/
//		table.addCell(celloldprice);
		/**end*/
		if(contractQtyFlag){
			table.addCell(cellQty);
		}
		table.addCell(cellprice);
		
		if(discountFlag) {
			Phrase discount = new Phrase("Discount",font1);
			PdfPCell discCell = new PdfPCell(discount);
			discCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(discCell);
		}
		
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createOldContractProductDetails() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		Phrase oldContDetPh = new Phrase("Old contract details : ", font9bold);
		PdfPCell oldContDetCell = new PdfPCell(oldContDetPh);
		oldContDetCell.setBorder(0);
		
		
		
		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f};
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		Phrase price = new Phrase("PRICE", font1);
		
		
		
		HashSet<String> taxName=new HashSet<String>();
		for(ProductOtherCharges tax:con.getProductTaxes()){
			taxName.add(tax.getChargeName().trim());
		}
		Iterator<String> it = taxName.iterator();
		String taxHeader="";
	    while(it.hasNext()){
	    	taxHeader=taxHeader+it.next()+"/";
	    }
	    if(!taxHeader.equals("")){
	    	taxHeader=taxHeader.substring(0,taxHeader.length()-1);
	    }
	    Phrase servicetax= new Phrase("SGST/CGST  % ",font1);	
	  	PdfPCell celltax = new PdfPCell(servicetax);
	  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellname);
		table.addCell(cellduration);
		table.addCell(cellservices);
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		for(int i=0;i<con.getItems().size();i++){
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(con.getItems().get(i).getPrice()+"", font9);
			}else{
				chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if ((con.getItems().get(i).getVatTax().getPercentage() == 0)
					&& (con.getItems().get(i).getServiceTax().getPercentage() > 0)) {

				if (con.getItems().get(i).getServiceTax().getPercentage() != 0) {
						chunk = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+ "", font9);
				}else{
					chunk = new Phrase("N.A" + "", font9);
				}
			} else if ((con.getItems().get(i).getServiceTax().getPercentage() == 0)
					&& (con.getItems().get(i).getVatTax().getPercentage() > 0)) {

				if (con.getItems().get(i).getVatTax() != null) {
					chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+ "", font9);
				} else {
					chunk = new Phrase("N.A" + "", font9);
				}
			} else if ((con.getItems().get(i).getServiceTax().getPercentage() > 0)
					&& (con.getItems().get(i).getVatTax().getPercentage() > 0)) {
				if ((con.getItems().get(i).getVatTax() != null)&& (con.getItems().get(i).getServiceTax() != null)){
					chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()
							+ ""+ " / "+ ""+ con.getItems().get(i).getServiceTax().getPercentage(), font9);
				}else{
					chunk = new Phrase("N.A" + "", font9);
				}
			} else {
				chunk = new Phrase("N.A" + "", font9);
			}
			pdftax = new PdfPCell(chunk);
			pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);

			double netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
			
			
			if(netPayAmount!=0){
				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				chunk = new Phrase(" ",font9);
			}
			pdfnetPay = new PdfPCell(chunk);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table.addCell(pdfname);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			
			table.addCell(pdfprice);
			table.addCell(pdftax);
			table.addCell(pdfnetPay);
		}
		

		Phrase blkph1=new Phrase("");
		PdfPCell blkcell=new PdfPCell(blkph1);
		blkcell.setBorderWidthRight(0);
		table.addCell(blkcell);
		
		Phrase blkph2=new Phrase("");
		PdfPCell blkcell2=new PdfPCell(blkph2);
		blkcell2.setBorderWidthLeft(0);
		blkcell2.setBorderWidthRight(0);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
//		table.addCell(blkcell2);
		
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(totcell);
		
//		Phrase totalval=new Phrase(" ",font9);
		Phrase totalval=new Phrase(Math.round(con.getNetpayable())+"",font9);
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(totalvalcell);
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.addCell(oldContDetCell);
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//		
//		try
//		{
//		Image image1=Image.getInstance("images/header.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,10f); //40f	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/pestomatic _letterhead-1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createBlankforUPC() {
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPdfAttachment(Contract con,Company company,Contract conRen,Customer cust,String preprintStatus){
		int noOfLine=12;          //10
		int productcount=0;
		this.con=con;
		comp=company;
		oldContract=conRen;
		this.cust=cust;
		if(this.con!=null)
			logger.log(Level.SEVERE,"this.con id="+this.con.getCount());
		if(oldContract!=null)
			logger.log(Level.SEVERE,"oldContract id="+oldContract.getCount());
		
		this.conRenw = ofy().load().type(ContractRenewal.class).filter("companyId", con.getCompanyId()).filter("contractId", con.getCount()).first().now(); //Ashwini Patil Date:19-10-2023
		
		
		branchList=ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName",con.getBranch()).list();

		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", con.getCompanyId()).filter(" paymentStatus", true).first().now();
		
		if(con.getPaymentMode()!=null && con.getPaymentMode().trim().length()>0){
			List<String> paymentDt = Arrays.asList(con.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", con.getCompanyId()).first()
						.now();
				
			}
		}
		
if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(con !=null && con.getBranch() != null && con.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName", con.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", con.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
//		Createblank();
//		createCompanyAddress();
		logger.log(Level.SEVERE, "preprintStatus value  "+preprintStatus);
		if(preprintStatus.contains("plane")){
			Createblank();
			createLogo(document,comp);
			createCompanyAddress();
		}else if(preprintStatus.contains("yes")){
			createBlankforUPC();
		}else if(preprintStatus.contains("no")){
			 createBlankforUPC();
			if(comp.getUploadHeader()!=null){
		    	createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
			
			

			
		}
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg("");
//		if(checkOldFormat==true){
//			if(oldConDetFlag){
//				createOldContractProductDetails();
//			}
	   createproductHeading();
	   createProductInfonew();
	   if(noOfLine!=0){
	   cretetotal();
	   footerInfo();
	   Createblank();
	   footerDetails();
	   }
		if(noOfLine==0&&productcount==0){
			 cretetotal();
			 footerInfo();
			 footerDetails();
		}
			/*
			 * commented by Ashwini
			 */
			
			//if(pepcoppflag==false){
				
				if(noOfLine==0&&productcount!=0){
					try {
						document.add(Chunk.NEXTPAGE);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					 if(preprintStatus.contains("yes")){
//						createBlankforUPC();
//					}else if(preprintStatus.contains("no")){
//						createBlankforUPC();
//						if(comp.getUploadHeader()!=null){
//					    	createCompanyNameAsHeader(document,comp);
//						}
//						if(comp.getUploadFooter()!=null){
//							createCompanyNameAsFooter(document,comp);
//						}
//					}
					Phrase ref = new Phrase("  ", font10bold);
					Paragraph pararef = new Paragraph();
					pararef.add(ref);
					pararef.setSpacingAfter(12);
				   
				    try {
						document.add(pararef);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
									e.printStackTrace();
		}
				    
				    createRemainingProduct(productcount);
				    cretetotal();
				    footerInfo();
				    Createblank();
				    footerDetails();
					}
				
		//	}
		//}


		
	}



	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	public  void createCompanyAddress()
	{
		
		
	
		DocumentUpload logodocument = comp.getLogo();
		
		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		 PdfPTable logotab=new PdfPTable(1);
		 logotab.setWidthPercentage(100);
		 
		 if (imageSignCell != null) {
			 logotab.addCell(imageSignCell);
			} 
		 else {
				Phrase blank = new Phrase(" ");
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setBorder(0);
				logotab.addCell(blankCell);
			}
		
		 //End by jayshree
		 
		 String companyName ="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyName=branchDt.getCorrespondenceName();
			}else{
				companyName = comp.getBusinessUnitName().trim().toUpperCase();
			}
		 
		 
		// Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
		 
//	     Paragraph p =new Paragraph();
//	     p.add(Chunk.NEWLINE);
//	     p.add(companyName);
//	     p.setAlignment(Element.ALIGN_CENTER);
		 Paragraph companynamepara = new Paragraph();
		 companynamepara.add(companyName);
		 companynamepara.setFont(font14bold);
		 
			
	     PdfPCell companyNameCell=new PdfPCell(companynamepara);
	     companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     companyNameCell.setFixedHeight(0);
	    
//	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
	     
	     String custAdd1="";
			String custFullAdd1="";
			
			if(comp.getAddress()!=null){
				
				if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
				
					if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
						custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
					}else{
						custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
					}
				}else{
					if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
						custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
					}else{
						custAdd1=comp.getAddress().getAddrLine1();
					}
				}
				
				if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
					if(comp.getAddress().getPin()!=0){
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
					}else{
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}
				}else{
					if(comp.getAddress().getPin()!=0){
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
					}else{
						custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
					}
				}
				
			}	
			
				Phrase addressline=new Phrase(custFullAdd1,font12);
				Paragraph addresspara=new Paragraph();
				addresspara.add(addressline);
				addresspara.setAlignment(Element.ALIGN_CENTER);
				
				PdfPCell addresscell=new PdfPCell();
				addresscell.addElement(addresspara);
				addresscell.setBorder(0);
			
	     
//		String addressline1="";
//		
//		if(comp.getAddress().getAddrLine2()!=null){
//			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
//		}
//		else{
//			addressline1=comp.getAddress().getAddrLine1();
//		}
//		Phrase addressline=new Phrase(addressline1,font12);
//		Paragraph addresspara=new Paragraph();
//		addresspara.add(addressline);
//		addresspara.setAlignment(Element.ALIGN_CENTER);
//		
//		
//		
//		String pinno;
//		if(comp.getAddress().getPin()!=0){
//			pinno=""+comp.getAddress().getPin();
//		}
//		else
//		{
//			pinno="  ";
//		}
//		Phrase locality=null;
//		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
//			System.out.println("inside both null condition1");
//			locality= new Phrase(comp.getAddress().getLandmark()+" ,"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
//			System.out.println("inside both null condition 2");
//			locality= new Phrase(comp.getAddress().getLandmark()+" ,"+comp.getAddress().getCity()+","+comp.getAddress().getState()+" , "+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		
//		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
//			System.out.println("inside both null condition 3");
//			locality= new Phrase(comp.getAddress().getLocality()+" ,"+comp.getAddress().getCity()+","+comp.getAddress().getState()+" , "+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
//			System.out.println("inside both null condition 4");
//			locality= new Phrase(comp.getAddress().getCity()+" ,"+comp.getAddress().getState()+" , "+comp.getAddress().getCountry()+","+pinno,font12);
//		}
//		
//		Paragraph localityPragraph=new Paragraph();
//		localityPragraph.add(locality);
//		localityPragraph.setAlignment(Element.ALIGN_CENTER);
//		
		
		String mobile="";
		String landline="";
		
		
		if(comp.getCellNumber1()!=0){
			mobile=comp.getCellNumber1()+"";
		}
		else{
			mobile="";
		}
		
		if(comp.getLandline()!=0){
			landline=comp.getLandline()+"";
		}
		else{
			landline="";
		}
		
		String email="";
		if(comp.getEmail()!=null&&comp.getEmail()!="")
		{
			email=comp.getEmail()+"";
		}else{
			email="";
		}
		
		
//		String email = "";
//		ServerAppUtility serverApp = new ServerAppUtility();
//
//		if (checkEmailId == true) {
//			branchmail = serverApp.getBranchEmail(comp, con.getBranch());
//			System.out.println("server method " + branchmail);
//
//		} else {
//			branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
//			System.out.println("server method 22" + branchmail);
//		}
//		
//		if(branchmail!=null){
//			email=branchmail;
//		}
//		else{
//			email="";
//		}
//		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		
//		String custCellno="";
//		String custContactDetails="";
//		
//		
//		if(comp.getCellNumber1()!=0){
//			custCellno=comp.getCellNumber1()+"";
//		}
//		else{
//			custCellno="";
//		}
//		
		
		Phrase contactinfo=new Phrase("Phone : "+mobile+"  "+"Email : "+email,font10);

		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
//		Phrase contactinfo1=new Phrase("Email : "+email,font10);
//		Paragraph realmobpara1=new Paragraph();
//		realmobpara1.add(contactinfo1);
//		realmobpara1.setAlignment(Element.ALIGN_CENTER);
		
//		PdfPCell addresscell=new PdfPCell();
//		addresscell.addElement(addresspara);
////		addresscell.setBorder(0);
//		PdfPCell localitycell=new PdfPCell();
//		localitycell.addElement(localityPragraph);
//		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
//		PdfPCell contactcell1=new PdfPCell();
//		contactcell1.addElement(realmobpara1);
//		contactcell1.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
	//	companyDetails.addCell(localitycell);
//		if(comp.getCellNumber1()!=0&&email!=null&&!email.equals(""))
//		{
		companyDetails.addCell(contactcell);   //contactcell1
//		}
//		if(email!=null&&!email.equals("")){
//		//companyDetails.addCell(contactcell1);
//		}
		companyDetails.setSpacingAfter(12f);
		
	
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		try {
			parent.setWidths(new float[]{100});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell logocell=new PdfPCell(logotab);
		logocell.setBorderWidthTop(0);
		logocell.setBorderWidthLeft(0);
		logocell.setBorderWidthRight(0);
//		parent.addCell(logocell);
		
		//End By Jayshree
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		comapnyCell.setBorderWidthTop(0);
		comapnyCell.setBorderWidthLeft(0);
		comapnyCell.setBorderWidthRight(0);
		comapnyCell.setFixedHeight(0);
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}




	private void createHeading() {
		String title1 = "";
		title1 = "Renewal Letter";

		Phrase titlephrase = new Phrase(title1, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorder(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		
		
		Phrase custInfo = new Phrase("Customer Id :", font10);
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		
		Phrase custInfovalue;
		custInfovalue = new Phrase(con.getCustomerId()+"", font10);
		
		PdfPCell custInfovalueCell = new PdfPCell(custInfovalue);
		custInfovalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfovalueCell.setBorder(0);
		
		
		
		Phrase date = new Phrase("Date :", font10);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setBorder(0);
		
		Phrase datevalue;
		if(conRenw!=null) {
			datevalue = new Phrase(fmt.format(conRenw.getDate())+"", font10);
		}
		else {
			datevalue = new Phrase(fmt.format(new Date())+"", font10);

		}
//		datevalue = new Phrase(fmt.format(con.getContractDate())+"", font10);
		
		
		PdfPCell datevalueCell = new PdfPCell(datevalue);
		datevalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datevalueCell.setBorder(0);
		
		
		
		
		
		
		float[] relativeWidths1 = {1.3f,5.4f,1f,2f};
		
		PdfPTable headTbl=new PdfPTable(4);
		headTbl.setWidthPercentage(100f);
		try {
			headTbl.setWidths(relativeWidths1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		headTbl.addCell(custInfoCell);
		headTbl.addCell(custInfovalueCell);
		headTbl.addCell(dateCell);
		headTbl.addCell(datevalueCell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell1=new PdfPCell(titlepdfcell);
		titlePdfCell1.setBorder(0);
		
		PdfPCell headIfo=new PdfPCell(headTbl);
		headIfo.setBorder(0);
		
		parent.addCell(titlePdfCell1);
		parent.addCell(headIfo);
		
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createCustomerDetails() {
		
		Phrase to = new Phrase("To,", font10);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		toCell.setBorder(0);
		
		String roleName="";
		 if(conDetails!=null&&conDetails.getRole()!=null){
			roleName=conDetails.getRole(); 
		 }else{
			 roleName="";
		 }
		
		Phrase role = new Phrase("The "+roleName, font10);
		PdfPCell roleCell = new PdfPCell(role);
		roleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		roleCell.setBorder(0);
		
		
		Phrase custInfo ;
		if(cust.isCompany()){
			custInfo = new Phrase(cust.getCompanyName(), font10);
		}else{
			custInfo = new Phrase(cust.getFullname(), font10);
		}
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			if(cust.getAdress().getAddrLine2()!=null){
				if(cust.getAdress().getLandmark()!=null){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
				}
			}else{
				if(cust.getAdress().getLandmark()!=null){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			/**
			 * Date 4/1/2018
			 * By jayshree
			 * Des.To change the sequence of country ,state ,city	And handle the null pointer for pin number	
			 */
			
			String pin="";
			if(cust.getAdress().getPin()!=0){
				pin=cust.getAdress().getPin()+"";
			}
			else{
				pin="";
			}
			if(cust.getAdress().getLocality()!=null){
//				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
				
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+cust.getAdress().getLocality()+"\n"+pin;

			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+pin;
			}
		}
		String custAddress="";
		if(cust.getAdress()!=null){
			custAddress=cust.getAdress().getCompleteAddress();
		}else{
			custAddress="";
		}
		Phrase custAddInfo = new Phrase(custAddress, font10);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		
		String cell="";
		if(cust.getCellNumber1()!=null){
			cell="Cell : "+cust.getCellNumber1()+"";
		}else{
			cell=" ";
		}
		
		Phrase cellNo = new Phrase(cell, font10);
		PdfPCell cellNoCell = new PdfPCell(cellNo);
		cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellNoCell.setBorder(0);
		
		String email="";
		if(cust.getEmail()!=null){
			email=cust.getEmail();
		}else{
			email="";
		}
		Phrase emailPr = new Phrase("E-mail : "+email, font10);
		PdfPCell emailCell = new PdfPCell(emailPr);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailCell.setBorder(0);
		
		PdfPTable customerTable=new PdfPTable(1);
		customerTable.setWidthPercentage(100);
		
		customerTable.addCell(toCell);
		if(conDetails!=null&&conDetails.getRole()!=null){
		customerTable.addCell(roleCell);
		}
		
		customerTable.addCell(custInfoCell);
		customerTable.addCell(custAddInfoCell);
		if(cust.getCellNumber1()!=0)
		{
		customerTable.addCell(cellNoCell);
		}
		if(email!=null&&!email.equals(""))
		{	
		customerTable.addCell(emailCell);
		}
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(customerTable);
		customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		customerCell.setBorder(0);
		
		
		
		PdfPTable headTbl=new PdfPTable(2);
		headTbl.setWidthPercentage(100f);
		
		headTbl.addCell(customerCell);
		headTbl.addCell(blankCell);
		
		PdfPCell parentCell = new PdfPCell();
		parentCell.addElement(headTbl);
		parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		parentCell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(parentCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
	}

	private void createSubjectAndMsg(String callFrom) {
//		/**
//		 * @author Anil @since 23-09-2021
//		 * Checking null condition here
//		 */
//		
//		int contractId=0;
//		if(oldContract!=null){
//			contractId=oldContract.getCount();
//		}else{
//			contractId=con.getCount();
//					
//		}
		
		Phrase sub=null;
//	    sub = new Phrase("Subject : Renewal of Contract Id "+oldContract.getCount(), font10);
//		logger.log(Level.SEVERE,"old contract Date"+oldContract.getEndDate());
		
		sub = new Phrase("Subject : Renewal of Contract Id "+con.getCount()+"", font10);
//		logger.log(Level.SEVERE,"old contract Date"+oldContract.getEndDate());
		
		PdfPCell subCell = new PdfPCell(sub);
		subCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		subCell.setBorder(0);
		subCell.setPaddingBottom(5);
		
		Phrase blank = new Phrase("", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell.setBorder(0);
		
		//Phrase dear = new Phrase("Dear Sir/Madam,", font10bold);
		String firstName=cust.getFullname();
		String[] nameArray = firstName.split("\\s+");
		if(nameArray.length>0){
			firstName=nameArray[0];
		}
		Phrase dear = new Phrase("Dear"+" "+firstName+",", font10);
		PdfPCell dearCell = new PdfPCell(dear);
		dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dearCell.setBorder(0);
		int maxServDuration=maxDuration();
		logger.log(Level.SEVERE,"no of max service days "+maxServDuration);
		
		String noOfServDays=numberOfDaysToMonthAndDay(maxServDuration);
		String title1 = "";
		/**title1="It is our Privilege to have been of service to you over the past year. "
				+ "we truly appreciate and value our association and trust you have found our services exemplary"
				+ " and to your complete satisfaction."+"\n"+"Your current contract for pest Management Services"
						+ " concludes on "+fmt3.format(oldContract.getEndDate())+" to order "
								+ "to enjoy uninterruplted service for a pest-free environment,"
								+ "we recommend you to renew the contract at the earliest."
								+ "Our renewal charges will be Rs "+df.format(con.getNetpayable())+""
										+ " in terms and conditions for a "+noOfServDays+" "
												+ "contract (for period "+(fmt.format(con.getStartDate()))+""
														+ " to "+fmt.format(con.getEndDate())+") would be mentioned below.";**/
		
		
		/**
		 *  Des : One day add extra in contract end date.
		 */
		Date renewDate=null;
		if(oldContract!=null&&callFrom!=null&&callFrom.equals("Contract")){ //Ashwini Patil Date:27-12-2023 if call from contract then it should pick old contract end date. Other call is from contract renewal so it should pick current contract end date.
			logger.log(Level.SEVERE,"taking renewal date from old contract "+oldContract.getCount()+" "+oldContract.getStartDate()+" - "+oldContract.getEndDate());
			renewDate=new Date(oldContract.getEndDate().getTime());
		}else{
			renewDate=new Date(con.getEndDate().getTime());
			logger.log(Level.SEVERE,"taking renewal date from new contract "+con.getCount()+con.getStartDate()+" - "+con.getEndDate());
		}
		logger.log(Level.SEVERE,"renewDate="+renewDate);
//		Date renewDate=new Date(oldContract.getEndDate().getTime());
		
		//Ashwini Patil Date:13-10-2023 commented following code as just pest, ultra and essevaerp reported that expiry date showing wrong. It is showing renewal start date
//		Calendar c = Calendar.getInstance();
//		c.setTime(renewDate);
//		c.add(Calendar.DATE,  1);
//		renewDate=c.getTime();
//		logger.log(Level.SEVERE,"renewDate after adding 1="+renewDate);
		
		title1="It is our privilege to provide service(s) to you over the past year.We trust that you have found our services exemplary and to your complete satisfaction."+"\n"+"\n"+"Your current contract is due for expiry on  "+fmt.format(renewDate)+". We request you to renew the contract with details mentioned below on or before  "+fmt.format(renewDate)+" to avoid inconvenience.";
		
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(subCell);
		parent.addCell(dearCell);
		
		parent.addCell(msgCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createProductInfo() {
		
		
		/**
		 * Date : 06-12-2017 BY ANIL
		 */
		Phrase newContDetPh = new Phrase("New contract details : ", font9bold);
		PdfPCell newContDetCell = new PdfPCell(newContDetPh);
		newContDetCell.setBorder(0);
		/**
		 * End
		 */
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f,1f};
		
		PdfPTable table = new PdfPTable(7);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		Phrase oldprice=new Phrase("OLD PRICE",font1);
		Phrase price = new Phrase("Rate", font1);
		/**enD**/
		
//		Phrase stax = new Phrase("S.TAX", font1);
//		Phrase vat = new Phrase("VAT", font1);
		
		   Phrase servicetax= null;
		      Phrase servicetax1= null;
		      int flag=0;
		      int vat=0;
		      int st=0;
		      
		      /*Date :22/11/2017  By:Manisha
		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
		       */
		   for(int i=0; i<con.getItems().size();i++){
		    	  
			   
		    if (con.getItems().get(i).getVatTax().getTaxPrintName()!= null
							&& !con.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
		    {
		    	
		    	       if(con.getItems().get(i).getServiceTax().getTaxPrintName()!=null
		    			&& !con.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase(""))
		    			{
		    		
		    		       servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"/"
		    		                    +con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
		    		       flag=flag+1;
		    			} 
		    	       else
		    	       {
		    	    	   
		    	    	   servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
		    	    	   vat=vat+1;
		    	    	   
		    	       }
		    	
		    	
		    }
		    
		    else{
		    	   
		    	if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()==0)){
		  	       servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+"%",font1);
		  	       vat=vat+1;
		  	       System.out.println("phrase value===="+servicetax.toString());
		  	      }
		    	

			      else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()==0)
			    		  &&(con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
			      {
			      	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  st=st+1;
			      }
		    	
			      else if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0)
			    		  && (con.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (con.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
			    		 && (con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
			       {
			      	  servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  flag=flag+1;
			      	  System.out.println("flag value;;;;;"+flag);
			      }
		    	 
			    else if ((con.getItems().get(i).getServiceTax().getPercentage()>0) && (con.getItems().get(i).getVatTax().getPercentage()==0)
			    		&&(con.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!con.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
			    {
			    	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
			    	  st=st+1;
				}
		    }
		     
		      
		      
//		       else{
//		      	 
//		      	  servicetax = new Phrase("TAX %",font1);
//		       }
		    }
		      
		      
		      /**End for Manisha**/
		      
		
		  	PdfPCell celltax = new PdfPCell(servicetax);
		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellvat = new PdfPCell(vat);
//		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellstax = new PdfPCell(stax);
//		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/**Manisha add column old price **/
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**End**/
		
		table.addCell(cellname);
		table.addCell(cellduration);
		table.addCell(cellservices);
		/**Manisha add column old price **/
//		table.addCell(celloldprice);
		/**end*/
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		
		for(int i=0;i<con.getItems().size();i++){
//			chunk = new Phrase((i+1)+"",font9);
//			pdfcode = new PdfPCell(chunk);
//			pdfcode.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			/**Manisha added old price cell in table**/
			Phrase pdfoldprice;
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(con.getItems().get(i).getOldProductPrice() +"",font9);
			}else{
				pdfoldprice=new Phrase("");
			}
			/**End******/
		
			
			
			
			////
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			if(con.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(con.getItems().get(i).getPrice()+"", font9);
			}else{
				 chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
//			if(con.getItems().get(i).getServiceTax()!=null){
//				chunk = new Phrase(con.getItems().get(i).getServiceTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfstax = new PdfPCell(chunk);
//			pdfstax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////
//			if(con.getItems().get(i).getVatTax()!=null){
//				chunk = new Phrase(con.getItems().get(i).getVatTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfvat = new PdfPCell(chunk);
//			pdfvat.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			///// 
			
		   	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==con.getItems().size()){
	      			 chunk = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	      		 }
	      		 else{
	      			 stno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font9);
	      		 if(st==con.getItems().size()){
	      	    pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(con.getItems().get(i).getVatTax()!=null){
	      			 if(vat==con.getItems().size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage(),font9);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font9);
	           	    }
	      		 if(vat==con.getItems().size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	      		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font9);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font9);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
			
	      	pdftax = new PdfPCell(chunk);
	      	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	      	/**   Date : 24-11-2017 BY MANISHA
	  	   * Description :To get the total amount on pdf..!!
	  	   * **/
	      	
			double netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
			
			/**Ends**/
			
			if(netPayAmount!=0){
				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				 chunk = new Phrase(" ",font9);
			}
			pdfnetPay = new PdfPCell(chunk);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			table.addCell(pdfname);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			/**Manisha add column old price **/
		
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table.addCell(oldpricevalcell);
			
			/**End**/
			table.addCell(pdfprice);
			table.addCell(pdftax);
			table.addCell(pdfnetPay);
		}
		
		  /**   Date : 24-11-2017 BY MANISHA
		   * Description :To get the total amount on pdf..!!
		   * **/

		Phrase blkph1=new Phrase("");
		PdfPCell blkcell=new PdfPCell(blkph1);
		blkcell.setBorderWidthRight(0);
		table.addCell(blkcell);
		
		Phrase blkph2=new Phrase("");
		PdfPCell blkcell2=new PdfPCell(blkph2);
		blkcell2.setBorderWidthLeft(0);
		blkcell2.setBorderWidthRight(0);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		table.addCell(totcell);
		
		Phrase totalval=new Phrase(con.getNetpayable()+"",font9);
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(totalvalcell);
		
		/**End for Manisha**/
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		/**
		 * Date:06-12-2017 BY ANIL
		 */
		if(oldConDetFlag){
			parent.addCell(newContDetCell);
		}
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		 
	}

	private void footerInfo() {
		
		
	//	
		
		
//		String title="";
//		title="We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
//				+ "Should you require further information on above Please do give us a call and we would gladly assist you.";
		
		
		
	
		String title="";
		title="We reiterate our commitment to ensure you complete satisfaction and look forward to continue our association."+"\n"+"\n"+"Please call us on "+comp.getCellNumber1()+". We would be glad to assist you for any further information.";

		
		
		Phrase msg = new Phrase(title, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		msgCell.setPaddingTop(8);
		
		/**
		 * @author Vijay Date 17-12-2020
		 * Des :- bug showing two times of description so below code commented
		 */
//		String contratDescription="";
//		if(con.getDescription()!=null){
//			contratDescription=con.getDescription();
//		}
//		
//		Phrase contractDesc = new Phrase(contratDescription, font10);
//		PdfPCell contractDescCell = new PdfPCell(contractDesc);
//		contractDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		contractDescCell.setBorder(0);
		
		String contratRenDescription=null;
		if(con.getDescription()!=null){
			contratRenDescription=con.getDescription();
		}
		
		/**
		 * Date 11/1/2018
		 * By jayshree
		 * Des.to set only 500 characters in description
		 */
		
		Phrase contractRenDesc =null;
		if(contratRenDescription.length()>500){
		contractRenDesc = new Phrase(contratRenDescription.substring(0,499), font10);
		}else{
			contractRenDesc = new Phrase(contratRenDescription, font10);
		}
		PdfPCell contractRenDescCell = new PdfPCell(contractRenDesc);
		contractRenDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractRenDescCell.setBorder(0);
		
		Phrase annexture = new Phrase("Please refere annexure for product details  ", font10bold);
		PdfPCell annextureCell = new PdfPCell(annexture);
		annextureCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		annextureCell.setBorder(0);
		annextureCell.setPaddingBottom(10);
		annextureCell.setPaddingTop(10);
		
		Phrase check1 = new Phrase("Cheque should be in favour of  "+"\""+comp.getBusinessUnitName()+"\"", font10bold);
		PdfPCell check1Cell = new PdfPCell(check1);
		check1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		check1Cell.setBorder(0);

		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(2);
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    PdfPCell blankCell = new PdfPCell(blank);
	    blankCell.setBorder(0);
	    
	    
	    Phrase b1=new Phrase();
		blank.add(" ");
		PdfPCell b1Cell1 = new PdfPCell();
		b1Cell1.addElement(b1);
		b1Cell1.setBorder(0);
		
		//parent.addCell(blankCell);
		parent.addCell(b1Cell1);
	    parent.addCell(b1Cell1);
		parent.addCell(msgCell);
//		if(con.getDescription()!=null){
//			parent.addCell(contractDescCell);
//		}
		if(con.getDescription()!=null){
			parent.addCell(contractRenDescCell);
		}
		
		
		if(checkOldFormat==false){
//		parent.addCell(annextureCell);
		}
//		parent.addCell(check1Cell);
		parent.addCell(blankCell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/****footer tab****/
		
		
		PdfPTable leftFooter=new PdfPTable(1);
		leftFooter.setWidthPercentage(100);
		
		
		String title2 = "";
		title2 = "Thank You";//By Jayshree remove the content
		
		Phrase thank = new Phrase(title2, font10);
		PdfPCell thankCell = new PdfPCell(thank);
		thankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		thankCell.setBorder(0);
		
		
		String companyName ="";
		if(branchDt!=null&&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyName=branchDt.getCorrespondenceName();
		}else{
			companyName = comp.getBusinessUnitName().trim().toUpperCase();
		}
		
		
		Phrase compn = new Phrase("For "+companyName, font10bold);
		PdfPCell compCell = new PdfPCell(compn);
        compCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		compCell.setBorder(0);
		
		
		
		Phrase sign = new Phrase("Authorised Signatory", font10); // Authorised Signatory   Sign. Authority
		PdfPCell signCell = new PdfPCell(sign);
		signCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		signCell.setBorder(0);
		
		
		
		/***
		 * Date 14-04-2018 By vijay for Digital Signator
		 */

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		} catch (Exception e) {
			e.printStackTrace();
		}

		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		signParaCell.setBorder(0);
			signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		/**
		 * ends here
		 */
		
		
			
		
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
		leftFooter.addCell(thankCell);
		leftFooter.addCell(compCell);
		
		/** Date 14-04-2018 By vijay for Digital Signatory ***/
		if (imageSignCell != null) {
			leftFooter.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			leftFooter.addCell(blank1Cell);
			leftFooter.addCell(blank1Cell);
			leftFooter.addCell(blank1Cell);
		}
		/**
		 * ends here
		 */
		leftFooter.addCell(signCell);
		leftFooter.setSpacingAfter(15f);
		
		
		
		PdfPTable rightTab=new PdfPTable(3);
		rightTab.setWidthPercentage(100);
		try {
			/*
			 *Date: 11/10/2018
			 * Developer:Ashwini
			 * Des:to shrink right table
			 */
			if(pecoppflag){
			 float[] columnWidths = { 1f, 1f, 2f };
			 rightTab.setWidths(columnWidths);
			}else{
				
				rightTab.setWidths(new float[]{20,30,50});
				
			}
			 
			/*
			 * end by Ashwini
			 */
//			rightTab.setWidths(new float[]{20,30,50}); //commented by Ashwini
		
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase modes=new Phrase("Modes",font9bold);
		PdfPCell modesCell=new PdfPCell(modes);
		modesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(modesCell);
		
		Phrase favering=new Phrase("Favouring",font9bold);
		PdfPCell faveringCell=new PdfPCell(favering);
		faveringCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(faveringCell);
		
		Phrase details=new Phrase("Details",font9bold);
		PdfPCell detailsCell=new PdfPCell(details);
		detailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(detailsCell);
		
		Phrase chequeph=new Phrase("Cheque/"+"\n"+"Demand Draft/"+"\n"+"Pay Order ",font9);
		PdfPCell chequeCell=new PdfPCell(chequeph);
		chequeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(chequeCell);
		if(pecoppflag || pepcoppflag){
			
			Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName(),font9);
			PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
			buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			rightTab.addCell(buesinessunitCell);
			
		}else{
		
		Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName()+"\n"+comp.getAddress().getCompleteAddress(),font9);
		PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
		buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(buesinessunitCell);
		}
		
//		Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName()+"\n"+comp.getAddress().getCompleteAddress(),font9);
//		PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
//		buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		rightTab.addCell(buesinessunitCell);
//	    
		/*
		 * Date:10/10/2018
		 * Added by Ashwini
		 * Developer:To add company branch Address
		 */
		if(pecoppflag){
		
		String branchaddress1 =" ";
		
		System.out.println("Branchlist::"+branchList);
		
		if(branchList!=null){
		for(Branch branch:branchList){
			System.out.println("I am inside for loop1");
			System.out.println("Branch from branchlist:"+branch.getBusinessUnitName());
			System.out.println("Branch from contractRenewal::"+con.getBranch());
			System.out.println("I am inside for loop2");
			if(con.getBranch().equalsIgnoreCase(branch.getBusinessUnitName())){
				branchaddress1 = branch.getAddress().getCompleteAddress();
				System.out.println("branch Address::"+branch.getAddress().getCompleteAddress());
			}
			
		}
		
		}else{
			
			branchaddress1 = comp.getAddress().getCompleteAddress();
			System.out.println("company Address::"+comp.getAddress().getCompleteAddress());
		}
		System.out.println("I am End of the for loop2");
		
		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+branchaddress1+"\n"+"2.Ensure your name and contract id is mentioned on the back of the cheque ",font9);
		PdfPCell detailsvalCell=new PdfPCell(detailsval);
		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(detailsvalCell);
		
	}else{
		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+comp.getAddress().getCompleteAddress()+"\n"+"2.Ensure your company name is mentioned on the back of the cheque ",font9);
		PdfPCell detailsvalCell=new PdfPCell(detailsval);
		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(detailsvalCell);
	}
		
		/*
		 * end by Ashwini
		 */
		
		
		/*
		 * commented by Ashwini
		 */
		
//		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+comp.getAddress().getCompleteAddress()+"\n"+"2.Ensure Your Company Name is mentioned on the back of the cheque ",font9);
//		PdfPCell detailsvalCell=new PdfPCell(detailsval);
//		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		rightTab.addCell(detailsvalCell);

		/*
		 * end by Ashwini
		 */
		Phrase rtgsph=new Phrase("RTGS/NEFT",font9);
		PdfPCell rtgsCell=new PdfPCell(rtgsph);
		rtgsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(rtgsCell);
		
		Phrase companyname=null;
		
		PdfPTable footerParent=new PdfPTable(2);
		footerParent.setWidthPercentage(100);
		
		try {
//			footerParent.setWidths(new float[]{70,30});
			/*
			 * Date:11/10/2018
			 * Developer:Ashwini
			 * 
			 */
			if(pecoppflag){
			footerParent.setWidths(new float[]{40,30});
			}else{
				footerParent.setWidths(new float[]{70,30});
			}
			
			/*
			 * end by Ashwini
			 */
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(comppayment!=null){
		System.out.println("companypayment111");
		PdfPCell leftCell=new PdfPCell(rightTab);
		leftCell.setBorder(0);
		footerParent.addCell(leftCell);
		
		PdfPCell rightCell=new PdfPCell(leftFooter);
		rightCell.setBorder(0);
		footerParent.addCell(rightCell);
		}
		else{
			System.out.println("company payment222");
			
			PdfPCell rightCell=new PdfPCell(leftFooter);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			footerParent.addCell(rightCell);
			
		}
//		try {
//			document.add(footerParent);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		
	}



	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=con.getItems();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
			if(entity.getPercentageDiscount()==null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal*entity.getQty();
				sum=sum+priceVal;
			}
			if(entity.getPercentageDiscount()!=null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
				priceVal=priceVal*entity.getQty();
				
				sum=sum+priceVal;
			}
			

		}
		return sum;
	}

	/**
	 * Developed by : Rohan Bhagde.
	 * Reason : This method is used for calculating Total amount including tax
	 * @param newPrice
	 * @param serTax
	 * @param vatTax
	 * @return
	 */
	   /**   Date : 24-11-2017 BY MANISHA
	   * Description :To get the total amount on pdf..!!
	   * **/
	public double calculateTotalAmt(double newPrice , double serTax , double vatTax,String vatname,String sertaxname)
	{
		double totalAmt= newPrice;
		double vatTaxAmt =0;
		double setTaxAmt =0;

		
//		System.out.println("new price "+newPrice );
//		System.out.println("service tax "+serTax );
//		System.out.println("vat tax"+vatTax );
		
//		System.out.println("service tax "+sertaxname );
//		System.out.println("vat tax "+vatname );
//		System.out.println("Price "+newPrice );
		
		
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = newPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = newPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  newPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = newPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
	//	
//	    if(vatname==null && vatTax==0 && sertaxname!=null && !sertaxname.equals("") && serTax!=0) 
//		{
//	    	setTaxAmt = newPrice * (serTax/100);
//			totalAmt =newPrice+setTaxAmt;
		return totalAmt;
	}

	       /****End******/

	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			// Here if both are inclusive then first remove service tax and then on that amount
			// calculate vat.
			double removeServiceTax=(entity.getPrice()/(1+service/100));
						
			//double taxPerc=service+vat;
			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		return tax;
	}
	private void footerDetails(){
		
		PdfPTable footerDetailTab=new PdfPTable(4);
		footerDetailTab.setWidthPercentage(100);
		
		PdfPTable gstTable=new PdfPTable(1);
		gstTable.setWidthPercentage(100);
		
		Phrase modes=new Phrase("GSTIN NO",font9bold);
		PdfPCell modesCell=new PdfPCell(modes);
		modesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		gstTable.addCell(modesCell);
		
		String gstvalue="";
		
		if(comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")){
			gstvalue=comp.getCompanyGSTTypeText();	
			logger.log(Level.SEVERE,"GSTIN 11 "+comp.getCompanyGSTTypeText());
		}else{
			gstvalue="";
		}
		logger.log(Level.SEVERE,"GSTIN VAlue--"+gstvalue);
		Phrase gstValue=new Phrase(gstvalue,font9bold);
		PdfPCell gstValueCell=new PdfPCell(gstValue);
		gstValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		gstValueCell.setPaddingTop(5f);
		gstTable.addCell(gstValueCell);
		
		PdfPCell gstCell=new PdfPCell(gstTable);
		footerDetailTab.addCell(gstCell);
		
		
//		PdfPTable sacTable=new PdfPTable(1);
//		sacTable.setWidthPercentage(100);
//
//		Phrase sac=new Phrase("SAC/HSN",font9bold);
//		PdfPCell sacCell=new PdfPCell(sac);
//		sacCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		sacTable.addCell(sacCell);
//		
//		String sacHsnValue="";
//		for(int i=0;i<comp.getArticleTypeDetails().size();i++){
//		if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("SAC Code")){
//			sacHsnValue=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
//			logger.log(Level.SEVERE,"SAC--"+sacHsnValue);
//		}else{
//			sacHsnValue="";
//		}
//		}
//		logger.log(Level.SEVERE,"SAC  No--"+sacHsnValue);
//		Phrase sacValue=new Phrase(sacHsnValue,font9bold);
//		PdfPCell sacValueCell=new PdfPCell(sacValue);
//		sacValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		sacTable.addCell(sacValueCell);
//		
//		PdfPCell sachsnCell=new PdfPCell(sacTable);
//		footerDetailTab.addCell(sachsnCell);
		
		PdfPTable paymentTable=new PdfPTable(1);
		paymentTable.setWidthPercentage(100);

		
		
		Phrase sac=new Phrase("Payment Terms",font9bold);
		PdfPCell paymentCell=new PdfPCell(sac);
		paymentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		paymentTable.addCell(paymentCell);
		
		String paymnetTerms="";
		if(con.getPayTerms()!=null&&!con.getPayTerms().equals("")){//Ashwini Patil Date:18-1-2024 Ultra pest want payment term selected in drop down to be printed
			paymnetTerms=con.getPayTerms(); 
		}
		else if(con.getPaymentTermsList()!=null&&con.getPaymentTermsList().size()!=0){
			paymnetTerms=con.getPaymentTermsList().get(0).getPayTermComment();
			
		}else{
			paymnetTerms="";
		}
		logger.log(Level.SEVERE,"Payment terms--"+paymnetTerms);
		Phrase sacValue=new Phrase(paymnetTerms,font9bold);
		PdfPCell sacValueCell=new PdfPCell(sacValue);
		sacValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sacValueCell.setPaddingTop(5f);
		paymentTable.addCell(sacValueCell);
		
		PdfPCell sachsnCell=new PdfPCell(paymentTable);
		footerDetailTab.addCell(sachsnCell);

		
		
		PdfPTable bankdetailsTab=new PdfPTable(3);
		bankdetailsTab.setWidthPercentage(100);
		bankdetailsTab.setKeepTogether(true);

		try {
			bankdetailsTab.setWidths(new float[]{40,2,58});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		Phrase bankDetails=new Phrase("Bank Details",font9bold);
		PdfPCell bankDetailsCell=new PdfPCell(bankDetails);
		bankDetailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		bankDetailsCell.setColspan(3);
		bankdetailsTab.addCell(bankDetailsCell);
		
		
		Phrase name=new Phrase("Name",font8bold);
		PdfPCell nameCell=new PdfPCell(name);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCell.setBorder(0);
		
//		String compName="";
//		if(comp.getBusinessUnitName()!=null){
//			compName=comp.getBusinessUnitName();
//		}else{
//			compName="";
//		}
		
		
		String compName="";
		if (comppayment!=null && comppayment.getPaymentFavouring() != null&& !comppayment.getPaymentFavouring().equals("")) {
				compName =comppayment.getPaymentFavouring();
		      }else{
					compName="";
	  		}

		
		Phrase compname=new Phrase(compName,font8);
		PdfPCell compnameCell=new PdfPCell(compname);
		compnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnameCell.setBorder(0);
		
		
		
		
		Phrase bankName=new Phrase("Bank ",font8bold);
		PdfPCell bankNameCell=new PdfPCell(bankName);
		bankNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankNameCell.setBorder(0);
		
		String bankNameValue="";
		if(comppayment!=null && comppayment.getPaymentBankName()!=null){
			bankNameValue=comppayment.getPaymentBankName();
		}else{
			bankNameValue="";
		}
		Phrase bank=new Phrase(bankNameValue,font8);
		PdfPCell bankCell=new PdfPCell(bank);
		bankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankCell.setBorder(0);
		
		
		Phrase branch=new Phrase("Branch",font8bold);
		PdfPCell branchCell=new PdfPCell(branch);
		branchCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchCell.setBorder(0);
		
		String branchValue="";
		if(comppayment!=null && comppayment.getPaymentBranch()!=null){
			branchValue=comppayment.getPaymentBranch();	
		}else{
			branchValue="";
		}
		Phrase branchName=new Phrase(branchValue,font8);
		PdfPCell branchNameCell=new PdfPCell(branchName);
		branchNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchNameCell.setBorder(0);
		
		
		Phrase curAcc=new Phrase("Account",font8bold);
		PdfPCell curAccCell=new PdfPCell(curAcc);
		curAccCell.setBorder(0);
		curAccCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String currentAccValue="";
		if(comppayment!=null && comppayment.getPaymentAccountNo()!=null){
			currentAccValue=comppayment.getPaymentAccountNo();	
		}else{
			currentAccValue="";
		}
		Phrase curAccno=new Phrase(currentAccValue,font8);
		PdfPCell curAccnoCell=new PdfPCell(curAccno);
		curAccnoCell.setBorder(0);
		curAccnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase ifsc=new Phrase("IFSC Code",font8bold);
		PdfPCell ifscCell=new PdfPCell(ifsc);
		ifscCell.setBorder(0);
		ifscCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String ifscValue="";
		if(comppayment!=null && comppayment.getPaymentIFSCcode()!=null){
			ifscValue=comppayment.getPaymentIFSCcode();
		}else{
			ifscValue="";
		}
		
		Phrase ifscCode=new Phrase(ifscValue,font8);
		PdfPCell ifscCodeCell=new PdfPCell(ifscCode);
		ifscCodeCell.setBorder(0);
		ifscCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String col=":";
		Phrase colon = new Phrase(col, font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		
		bankdetailsTab.addCell(nameCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(compnameCell);
		
		bankdetailsTab.addCell(bankNameCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(bankCell);
		
		bankdetailsTab.addCell(branchCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(branchNameCell);
		
		bankdetailsTab.addCell(curAccCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(curAccnoCell);
		
		bankdetailsTab.addCell(ifscCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(ifscCodeCell);
		
		PdfPCell detailsCell=new PdfPCell(bankdetailsTab);
		footerDetailTab.addCell(detailsCell);
		
		PdfPTable thnksTab=new PdfPTable(1);
		thnksTab.setWidthPercentage(100);
		thnksTab.setKeepTogether(true);
		
		Phrase thank=new Phrase("Thank You",font8bold);
		PdfPCell thankCell=new PdfPCell(thank);
		thankCell.setPaddingTop(5);
		thankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		thankCell.setBorder(0);
		thnksTab.addCell(thankCell);
		
		String compNameValue ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			compNameValue=branchDt.getCorrespondenceName();
		}else{
			compNameValue =comp.getBusinessUnitName().trim().toUpperCase();
		}
		
//		String compNameValue="";
//		if(comp.getBusinessUnitName()!=null){
//			compNameValue=comp.getBusinessUnitName();
//		}else{
//			compNameValue="";
//		}
		
		
		
		Phrase compNamedet=new Phrase("for "+compNameValue,font8bold);
		PdfPCell compNameValueCell=new PdfPCell(compNamedet);
		compNameValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		compNameValueCell.setBorder(0);
		thnksTab.addCell(compNameValueCell);
		
		DocumentUpload digitalDocument=null;
		if(comp.getUploadDigitalSign()!=null){
	    digitalDocument = comp.getUploadDigitalSign();	
		}else{
		digitalDocument = null;
		}
		
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setPaddingRight(2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		if(imageSignCell!=null){
		thnksTab.addCell(imageSignCell);	
		}else{
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			thnksTab.addCell(blank1Cell);
			thnksTab.addCell(blank1Cell);
			thnksTab.addCell(blank1Cell);
//			thnksTab.addCell(blank1Cell);

		}
		
		
		Phrase auth=new Phrase("Authorised Signatory",font8bold);
		PdfPCell authCell=new PdfPCell(auth);
		authCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		authCell.setBorder(0);
		authCell.setPaddingBottom(5);
		thnksTab.addCell(authCell);
		
		PdfPCell thankuCell=new PdfPCell(thnksTab);
		footerDetailTab.addCell(thankuCell);
		
		try {
			document.add(footerDetailTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

   public int maxDuration(){
	   ArrayList<Integer> servDurList= new ArrayList<Integer>();
	   
	   for(int i=0;i<con.getItems().size();i++){
		   servDurList.add(con.getItems().get(i).getDuration());
	   }
	   Collections.sort(servDurList); 
	   return servDurList.get(servDurList.size() - 1);
   }
    
   public String numberOfDaysToMonthAndDay(int noOfDays) {
	   String countabledays="";
	   
           if(noOfDays<=30){
        	   int days = noOfDays % 30;
        	   countabledays =days+" days";
           }else if(noOfDays==365){
        	   int year = noOfDays / 365;
        	   countabledays =year+" year";
           }else if(noOfDays==60){
        	   int month = noOfDays / 30;
        	   countabledays =month+" month";
           }else{
			int days = noOfDays % 30;
			int month = noOfDays / 30;
			if(days==0){
				countabledays= month + " Month";
			}else{
				countabledays= month + " Month and " + days + " days";	
			}
           }
           return countabledays;
	}
   
  
   private void createBillingAndServiceCustomerDetails() {
	   
	    ServerAppUtility serverAppUtility = new ServerAppUtility();
		
	    List<State> stateList = ofy().load().type(State.class)
				.filter("companyId", con.getCompanyId()).list();

		PdfPTable mainTable = new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		float[] columnHalfWidth = { 1f, 1f };
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/* Start Part 1 */
		PdfPTable part1Table = new PdfPTable(1);
		part1Table.setWidthPercentage(100);

		Phrase colon = new Phrase(":", font8bold);
		PdfPCell colonCell = new PdfPCell(colon);
		// colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon_bottonaligned = new Phrase(":", font8bold);
		PdfPCell colonCell_bottonaligned  = new PdfPCell(colon_bottonaligned);
		colonCell_bottonaligned.setBorder(0);
		colonCell_bottonaligned.setHorizontalAlignment(Element.ALIGN_LEFT);
		colonCell_bottonaligned.setPaddingTop(5);

		PdfPTable colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		float[] columnCollonWidth = { 1.8f, 0.2f, 7.5f };
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name = new Phrase("Name", font8bold); 
		PdfPCell nameCell = new PdfPCell(name);
		// nameCell.addElement(name);
		nameCell.setBorder(0);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT); 
		nameCell.setPaddingTop(5);//Ashwini Patil
		
		String tosir = null;
		String custName = "";
		
		String custNameForServiceAddress="";
		
		if(cust.getServiceAddressName()!=null&&!cust.getServiceAddressName().equals("")) {
			custNameForServiceAddress=cust.getServiceAddressName();
		}else if (cust.isCompany() == true && cust.getCompanyName() != null) {
			if(PC_RemoveSalutationFromCustomerOnPdfFlag){
				custNameForServiceAddress = cust.getCompanyName().trim();
			}
			else{
				custNameForServiceAddress = "M/S " + " " + cust.getCompanyName().trim();
			}
		} else if (cust.getSalutation() != null
				&& !cust.getSalutation().equals("")) {
			custNameForServiceAddress = cust.getSalutation() + " "
					+ cust.getFullname().trim();
		} else {
			custNameForServiceAddress = cust.getFullname().trim();
		}
		
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				System.out.println("PC_RemoveSalutationFromCustomerOnPdfFlag "+PC_RemoveSalutationFromCustomerOnPdfFlag);
				if(PC_RemoveSalutationFromCustomerOnPdfFlag){
					custName = cust.getCompanyName().trim();
				}
				else{
					custName = "M/S" + " " + cust.getCompanyName().trim();
				}
			} else if (cust.getSalutation() != null) {
				custName = cust.getSalutation() + " "
						+ cust.getFullname().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}

		String fullname = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			fullname = custName;
		} else {
			fullname = custName;
		}
		// ends here
		/**
		 * Date 28/12/2017 By Jayshree To check the null condition
		 */
		Phrase nameCellVal = null;
		
		if (cust.getCellNumber1() != null && cust.getCellNumber1() != 0) {
			nameCellVal = new Phrase(fullname, nameAddressBoldFont);			//Updated By: Viraj Date: 30-03-2019 Description:To create a new cell for mobile no.
		} else {
			nameCellVal = new Phrase(fullname, nameAddressBoldFont);			//Updated By: Viraj Date: 30-03-2019 Description:To create a new cell for mobile no.
		}
		
		PdfPCell nameCellValCell = new PdfPCell(nameCellVal);
		// nameCellValCell.addElement(nameCellVal);
		nameCellValCell.setBorder(0);
		nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCellValCell.setPaddingTop(5); //Ashwini Patil
		
		Phrase mob = new Phrase("Mobile", font8bold);
		PdfPCell mobCell = new PdfPCell(mob);
//		mobCell.addElement(mob);
		mobCell.setBorder(0);
//		mobCell.setPaddingTop(2);
		mobCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String cellNo1="";
		String pocName1="";
		if(cust.getCellNumber1()!=null){
			cellNo1=cust.getCellNumber1()+"";
		}else{
			cellNo1="";
		}
		

		Phrase address = new Phrase("Address", font8bold);
		PdfPCell addressCell = new PdfPCell(address);
		System.out.println("First Address Here "+address);
		// addressCell.addElement(address);
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String countryName = "";
		
	
		PdfPCell addressValCell=null;
//		Phrase addressVal;
		logger.log(Level.SEVERE,"UpscFlagBranch11111:" +upcflag);
		logger.log(Level.SEVERE,"billing address addcellNoFlag " +addcellNoFlag);
		if(upcflag||addcellNoFlag){

			
			String adrsValString1 = "";
			if(cust.getAdress()!=null) {
				logger.log(Level.SEVERE,"Billing Address from customer "+cust.getAdress().getCompleteAddress().trim());
				adrsValString1 = cust.getAdress().getCompleteAddress().trim();
				countryName = cust.getAdress().getCountry();
			}
			
			
					Contract conEntity = con;
					if(conEntity.getNewcustomerAddress()!=null && conEntity.getNewcustomerAddress().getAddrLine1()!=null && 
							!conEntity.getNewcustomerAddress().getAddrLine1().equals("")){
						adrsValString1 = conEntity.getNewcustomerAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Billing Address $="+adrsValString1);
						countryName = conEntity.getNewcustomerAddress().getCountry();
					}
					
				
					if(addcellNoFlag&&customerBranch!=null&&customerBranch.getBillingAddress()!=null){
						logger.log(Level.SEVERE,"Billing Address from customer branch "+customerBranch.getBillingAddress().getCompleteAddress().trim());
						adrsValString1 = customerBranch.getBillingAddress().getCompleteAddress().trim();
						countryName = customerBranch.getBillingAddress().getCountry();

					}
			

				adrsValString1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString1);
				
			
				Phrase addressVal = new Phrase(adrsValString1, nameAddressFont);
	    		 addressValCell = new PdfPCell(addressVal);//chng1
	    		System.out.println("Second Address Here222 "+addressVal+"amol");
	    		addressValCell.setBorder(0);
	    		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			}
		}
	else if(con!=null&&con.getNewcustomerAddress()!=null){
		Phrase addressVal=new Phrase(pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", con.getNewcustomerAddress().getCompleteAddress().trim()),font8);
  		 addressValCell = new PdfPCell(addressVal);//chng1
  		System.out.println("Second Address Here "+addressVal);

  		addressValCell.setBorder(0);
  		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
  		countryName = con.getNewcustomerAddress().getCountry();
       	
       }else{
			String adrsValString1 = "";
			if(cust.getAdress()!=null) {
				cust.getAdress().getCompleteAddress().trim();
				countryName = cust.getAdress().getCountry();
			}
			if(con.getNewcustomerAddress()!=null && con.getNewcustomerAddress().getAddrLine1()!=null && 
					!con.getNewcustomerAddress().getAddrLine1().equals("")){
				adrsValString1 = con.getNewcustomerAddress().getCompleteAddress();
				logger.log(Level.SEVERE,"Billing Address $="+adrsValString1);
				countryName = con.getNewcustomerAddress().getCountry();

			}

			adrsValString1=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString1);
			
			Phrase addressVal = new Phrase(adrsValString1, font8);
   		 addressValCell = new PdfPCell(addressVal);//chng1
   		System.out.println("Second Address Here "+addressVal);
//   		 addressValCell.addElement(addressVal);
   		addressValCell.setBorder(0);
   		addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);	
       	
       	
       }

		

		if(addcellNoFlag&&customerBranch!=null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0){
			cellNo1=customerBranch.getCellNumber1()+"";
		
			cellNo1 = serverAppUtility.getMobileNoWithCountryCode(cellNo1, countryName, comp.getCompanyId());
		
		}
		
		if(cust.getFullname()!=null){
			pocName1=toCamelCase(cust.getFullname());
		}else{
			pocName1="";
		}
	
		cellNo1 = serverAppUtility.getMobileNoWithCountryCode(cellNo1, countryName, comp.getCompanyId());
	
		Phrase mobVal=null;
		if(addcellNoFlag) {
			mobVal = new Phrase(cellNo1+" ( "+pocName1+" ) ", font8);
		}else {
			mobVal = new Phrase(cellNo1, font8);
		}
		
		PdfPCell mobValCell = new PdfPCell(mobVal);
	//	mobValCell.addElement(mobVal);
		mobValCell.setBorder(0);
//		mobValCell.setPaddingTop(2);
		mobValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/** Ends **/
      
		
	String gstTinStr = "";
	
	
		if (con.getCustomerGSTNumber() != null
				&& !con.getCustomerGSTNumber().equals("")) {
			gstTinStr = con.getCustomerGSTNumber().trim();
		} else {
			gstTinStr = serverAppUtility.getGSTINOfCustomer(cust,
					con.getNewCustomerBranch(),"Contract");
		}
	
	
	Phrase gstTin = new Phrase(" ", font8bold);
	if(!gstTinStr.equals("")){
		gstTin = new Phrase("GSTIN", font8bold);
	}
	PdfPCell gstTinCell = new PdfPCell(gstTin);
	// gstTinCell.addElement(gstTin);
	gstTinCell.setBorder(0);
	gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	/**
	 * Date 5/12/2017
	 * By Jayshree 
	 * To add the state code
	 */
	
	PdfPTable statetable = new PdfPTable(6);
	statetable.setWidthPercentage(100);
	
	try {
		statetable.setWidths(new float[]{50,10,25,5,15,10});
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	Phrase gstTinVal = new Phrase(gstTinStr, font8);
	PdfPCell gstTinValCell = new PdfPCell(gstTinVal);
	// gstTinValCell.addElement(gstTinVal);
	gstTinValCell.setBorder(0);
	gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	statetable.addCell(gstTinValCell);
	
	Phrase Blnk2=new Phrase(" ");
	PdfPCell blankcell=new PdfPCell(Blnk2);
	blankcell.setBorder(0);
	
	statetable.addCell(blankcell);
	
	
	Phrase stateCode=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
		stateCode = new Phrase("State Code", font8bold);
	}else{
		stateCode = new Phrase(" ", font8bold);
	}
	
	PdfPCell stateCodeCell = new PdfPCell(stateCode);
	// stateCodeCell.addElement(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	statetable.addCell(stateCodeCell);
	
	Phrase Blnk3=null;
	if(gstTinStr!=null && !gstTinStr.equals("")){
		Blnk3=new Phrase(":",font8);
	}else{
		Blnk3=new Phrase(" ",font8);
	}
	PdfPCell blankcell3=new PdfPCell(Blnk3);
	blankcell3.setBorder(0);
	statetable.addCell(blankcell3);
	
	String stCo = "";
	if (stateList != null) {
		for (int i = 0; i < stateList.size(); i++) {
			if (stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim())) {
				stCo = stateList.get(i).getStateCode().trim();
				break;
			}
		}
	}

		
		
		/**
		 * Date 27/2/2018 
		 * by Jayshree 
		 * Des.To add the customer Email id
		 */
		Phrase emailph=new Phrase("Email",font8bold);
		PdfPCell emailCell=new PdfPCell(emailph);
		emailCell.setBorder(0);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String custemailval = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			custemailval = customerBranch.getEmail();
		}

		}else{
			if (cust.getEmail() != null) {
				custemailval = cust.getEmail();
			} else {
				custemailval ="";
			}
		}
		
		
		
		Phrase emailphval=new Phrase(custemailval,font8);
		PdfPCell emailCellval=new PdfPCell(emailphval);
		emailCellval.setBorder(0);
		emailCellval.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell_bottonaligned);  //Ashwini Patil have changed coloncell
		colonTable.addCell(nameCellValCell);
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description:To create a new cell for mobile
		 */
		colonTable.addCell(mobCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(mobValCell);
		/** Ends **/
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(addressValCell); 
		/**
		 * Date 28/2/2018 By jayshree add cell
		 */
		if(cust.getEmail()!=null&&!cust.getEmail().equals("")){
			colonTable.addCell(emailCell);
			colonTable.addCell(colonCell);
			colonTable.addCell(emailCellval);
		}
		
		
		/**
		 * @author Priyanka @since 19-08-2021
		 */
		System.out.println("Priyanka Article type");
		String documentName="Contract";
		
		if(cust!=null&&cust.getArticleTypeDetails()!=null&&cust.getArticleTypeDetails().size()!=0){
			System.out.println("Article type size"+cust.getArticleTypeDetails().size());
			for(ArticleType type:cust.getArticleTypeDetails()){
				if(type.getDocumentName().equals(documentName)&&type.getArticlePrint().equalsIgnoreCase("Yes")){
//					if(type.getArticleTypeName().equalsIgnoreCase("GSTIN")){
//						continue;
//					}
					
//					custDetBillTbl.addCell(pdfUtility.getCell(type.getArticleTypeName(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
//					custDetBillTbl.addCell(colonCell);
//					custDetBillTbl.addCell(pdfUtility.getCell(type.getArticleTypeValue(), font8, Element.ALIGN_LEFT, 0, 0, 0)).setBorder(0);
				     
					Phrase article =null;
					article = new Phrase(type.getArticleTypeName(), font8bold);
					PdfPCell articleCell = new PdfPCell(article);
					articleCell.setBorder(0);
					articleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					Phrase articleVal = new Phrase(type.getArticleTypeValue(), font8);
					PdfPCell articleValCell = new PdfPCell(articleVal);
					articleValCell.setBorder(0);
					articleValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					colonTable.addCell(articleCell);
					colonTable.addCell(colonCell);
					colonTable.addCell(articleValCell);
					
				}
				
			}
		}
		
//		if(gstTinStr!=null&&!gstTinStr.equals("")){
//			colonTable.addCell(gstTinCell);
//			colonTable.addCell(colonCell);
//			colonTable.addCell(statecell);
//		}

		PdfPCell cell1 = new PdfPCell();
		cell1.setBorder(0);
		cell1.addElement(colonTable);

		float[] columnStateCodeCollonWidth = { 35, 5, 15, 45 };
		

		Phrase blak = new Phrase(" ", font8);
		PdfPCell blakCell = new PdfPCell(blak);
		blakCell.setBorder(0);

		
		part1Table.addCell(cell1);

	

		PdfPCell part1TableCell = new PdfPCell();
		part1TableCell.addElement(part1Table);

		
		PdfPTable part2Table = new PdfPTable(1);
		part2Table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

		Phrase name2 = new Phrase("Name", font8bold);
		PdfPCell name2Cell = new PdfPCell();
		name2Cell.addElement(name2);
		name2Cell.setBorder(0);
		name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil @since 28-10-2021
		 * If single customer branch is selected then print branch name in service address section instead if customer name
		 * raised by Nitin Sir and Pooname for IndoGulf 
		 */
		
//		String custBranchName=fullname; 
		String custBranchName=custNameForServiceAddress; //Ashwini Patil Date:28-04-2022 
		
		//Date:20-01-2022 By: Ashwini Patil Description: to print selected Branch name on pdf
//		if(customerBranch!=null){
////			custBranchName=custName+" - "+ customerBranch.getBusinessUnitName()+" branch";
//			custBranchName=customerBranch.getBusinessUnitName();//Ashwini Patil Date:28-04-2022
//		}
		if(customerBranch!=null){
			if(customerBranch.getServiceAddressName()!=null&&!customerBranch.getServiceAddressName().equals(""))
				custBranchName=customerBranch.getServiceAddressName();
			else
				custBranchName=customerBranch.getBusinessUnitName();
		}
		
//		Phrase name2CellVal = new Phrase(fullname, font8);			//Updated By: Viraj Date: 30-03-2019 Description:To create a new cell for mobile no.
		
		//Ashwini Patil changed font8 to nameAddressBoldFont
		Phrase name2CellVal = new Phrase(custBranchName, nameAddressBoldFont);
		PdfPCell name2CellValCell = new PdfPCell(name2CellVal);
		// name2CellValCell.addElement(name2CellVal);
		name2CellValCell.setBorder(0);
		name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		name2CellValCell.setPaddingTop(5); //Ashwini Patil
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description: To create a new cell for mobile
		 */
		Phrase mob1 = new Phrase("Mobile", font8bold);
		PdfPCell mob1Cell = new PdfPCell(mob1);
//		mob1Cell.addElement(mob1);
		mob1Cell.setBorder(0);
	//	mob1Cell.setPaddingTop(2);
		mob1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase address2 = new Phrase("Address", font8bold);
		PdfPCell address2Cell = new PdfPCell(address2);
		System.out.println("First Adddress 2 "+address2);
		// address2Cell.addElement(address2);
		address2Cell.setBorder(0);
		address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/*@author Abhinav Bihade
		 * @since 20/01/2020
		 *As per Rahul Tiwari's Requirement Bitco : Print customer branch address in contract Pdf Tested by Sonu Porel 
		*/
		PdfPCell address2ValCell = null;
		logger.log(Level.SEVERE,"Service address addcellNoFlag  " +addcellNoFlag);
		logger.log(Level.SEVERE,"Service address upcflag  " +upcflag);

		String countryName2 = "";
		if(upcflag||addcellNoFlag){
//			if(customerBranch!=null){
//				
//				Phrase address2Val=new Phrase(customerBranch.getAddress().getCompleteAddress().trim(),font8);
//				//serviceAddress1= customerBranch.getAddress();
//				//addressVal = customerBranch.getAddress().getCompleteAddress();
//				 //addressValCell = new PdfPCell(address2Val);//chng1
//				logger.log(Level.SEVERE,"customerBranch  Service address "+address2Val);
//			   		//address2ValCell.addElement(address2Val);
//			   		address2ValCell=new PdfPCell(address2Val);
//			   		address2ValCell.setBorder(0);
//			   		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				logger.log(Level.SEVERE,"UpscFlagBranch:" +upcflag);
//			
//		}
//			else{
			
			String adrsValString = "";
			
			if(cust.getSecondaryAdress()!=null) {
				adrsValString = cust.getSecondaryAdress().getCompleteAddress().trim();
			}
			
			
					if(con.getCustomerServiceAddress()!=null && con.getCustomerServiceAddress().getAddrLine1()!=null && 
							!con.getCustomerServiceAddress().getAddrLine1().equals("")){
						adrsValString = con.getCustomerServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address $="+adrsValString);
						countryName2 = con.getCustomerServiceAddress().getCountry();
					}
					
					//By Ashwini Patil to print branch address if branch is selected in contract
					if(customerBranch!=null)
					{
						adrsValString=customerBranch.getAddress().getCompleteAddress();
					}
					
					/**
					 * @author Anil @since 20-08-2021
					 * For creative pest if process PRINTADDRESSANDCELLFROMCUSTOMERBRANCH is active then after it was not printing
					 * branch address
					 */
					if(addcellNoFlag&&customerBranch!=null&&customerBranch.getAddress()!=null){
						logger.log(Level.SEVERE,"Billing Address from customer branch "+customerBranch.getAddress().getCompleteAddress().trim());
						adrsValString = customerBranch.getAddress().getCompleteAddress().trim();
						countryName2 = customerBranch.getAddress().getCountry();

					}
			
			/**
			 * @author Anil
			 * @since 18-01-2022
			 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
			 */
			adrsValString=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString);
			
			//Ashwini Patil changed font8 to nameAddressFont
			Phrase address2Val = new Phrase(adrsValString, nameAddressFont);
//				Phrase address2Val = new Phrase(cust.getSecondaryAdress()//Chng2
//						.getCompleteAddress().trim(), font8);
				 address2ValCell = new PdfPCell(address2Val);
				 logger.log(Level.SEVERE,"Second Address 456 "+address2Val);
				// address2ValCell.addElement(address2Val);
				address2ValCell.setBorder(0);
				address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			}
		}
		
		
		
		
		else if(con!=null&&con.getCustomerServiceAddress()!=null){
//		Phrase address2Val = new Phrase(cust.getSecondaryAdress()//Chng2
//				.getCompleteAddress().trim(), font8);
//		 address2ValCell = new PdfPCell(address2Val);
//		System.out.println("Second Address 2 "+address2Val);
//		// address2ValCell.addElement(address2Val);
//		address2ValCell.setBorder(0);
//		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * @author Anil
		 * @since 18-01-2022
		 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
		 */
//		Phrase address2Val=new Phrase(contEntity.getCustomerServiceAddress().getCompleteAddress().trim(),font8);
		Phrase address2Val=new Phrase(pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", con.getCustomerServiceAddress().getCompleteAddress().trim()),font8);
		 address2ValCell = new PdfPCell(address2Val);
		System.out.println("Second Address 2 "+address2Val);
		// address2ValCell.addElement(address2Val);
		address2ValCell.setBorder(0);
		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		countryName2 = con.getCustomerServiceAddress().getCountry();

		
       }else{
//		Phrase address2Val=new Phrase(contEntity.getCustomerServiceAddress().getCompleteAddress().trim(),font8);
//		 address2ValCell = new PdfPCell(address2Val);
//		System.out.println("Second Address 2 "+address2Val);
//		// address2ValCell.addElement(address2Val);
//		address2ValCell.setBorder(0);
//		address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
       	String adrsValString = "";
			
       	
			if(cust.getAdress()!=null) {
				adrsValString = cust.getAdress().getCompleteAddress().trim();
				countryName2 = cust.getAdress().getCountry();
			}
			
      
			
					if(con.getCustomerServiceAddress()!=null && con.getCustomerServiceAddress().getAddrLine1()!=null && 
							!con.getCustomerServiceAddress().getAddrLine1().equals("")){
						adrsValString = con.getCustomerServiceAddress().getCompleteAddress();
						logger.log(Level.SEVERE,"Service address $="+adrsValString);
						countryName2 =  con.getCustomerServiceAddress().getCountry();					
						}					
			
			/**
			 * @author Anil
			 * @since 18-01-2022
			 * Removing pin as heading from address section. raised by Nithila and Nitin sir for Innovative
			 */
			adrsValString=pdfUtility.removePinAsHeadingFromAddress(comp.getCompanyId(), "Contract", adrsValString);
			
				Phrase address2Val = new Phrase(adrsValString, font8);
				 address2ValCell = new PdfPCell(address2Val);
				 logger.log(Level.SEVERE,"Second Address 456 "+address2Val);
				// address2ValCell.addElement(address2Val);
				address2ValCell.setBorder(0);
				address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			}
				

       }
 
       /********* End by Deepak Salve *****/
       
		String cellNo="";

		String pocName="";
		if(addcellNoFlag&&customerBranch!=null&&customerBranch.getCellNumber1()!=null&&customerBranch.getCellNumber1()!=0){
			logger.log(Level.SEVERE, "custbranch name ");
		
			if(customerBranch.getCellNumber1()!=null){
				cellNo=customerBranch.getCellNumber1()+"";
			}
//			else{
//				cellNo=0l;
//			}
			
			if(customerBranch.getPocName()!=null){
				pocName=customerBranch.getPocName();
			}else{
				pocName="";
			}
			logger.log(Level.SEVERE, "cust cell no and poc name "+cellNo+pocName);
			
			cellNo = serverAppUtility.getMobileNoWithCountryCode(cellNo+"", countryName, comp.getCompanyId());
			
		}else{
			if(cust.getCellNumber1()!=null){
				cellNo=cust.getCellNumber1()+"";
			}
//			else{
//				cellNo=0l;
//			}
			
			if(cust.getFullname()!=null){
				pocName=cust.getFullname();
			}else{
				pocName="";
			}
			logger.log(Level.SEVERE, "cust cell no and poc name2222 "+cellNo+pocName);
		}
		
		/**
		 * @author Vijay Date :- 22-09-2021
		 * Des :- Adding country code(from country master) in cell number.
		 */
		cellNo = serverAppUtility.getMobileNoWithCountryCode(cellNo, countryName, comp.getCompanyId());
	
		Phrase mob1Val=null;
		if(addcellNoFlag) {
			if(pocName!=null&&!pocName.equals("")){
				mob1Val = new Phrase(cellNo+" ( "+pocName+" ) ", font8);
			}else{
				mob1Val = new Phrase(cellNo+"", font8);
			}
				
		}else {
			//By Ashwini Patil to print branch cell number on contract pdf
			if(customerBranch!=null)
				cellNo=customerBranch.getCellNumber1().toString();
			
			mob1Val = new Phrase(cellNo+"", font8);
		}
		
		
		
		logger.log(Level.SEVERE, "cust cell no and poc name 3333 "+cellNo+pocName);
		
		PdfPCell mob1ValCell = new PdfPCell(mob1Val);
//		mob1ValCell.addElement(mob1Val);
		mob1ValCell.setBorder(0);
	//	mob1ValCell.setPaddingTop(2);
		mob1ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		/** Ends **/
		
		String gstTin2Str = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("GSTIN")) {
				gstTin2Str = cust.getArticleTypeDetails().get(i)
						.getArticleTypeName().trim();
			}
		}

		Phrase gstTin2 = new Phrase("GSTIN", font8bold);
		PdfPCell gstTin2Cell = new PdfPCell(gstTin2);
		// gstTin2Cell.addElement(gstTin2);
		gstTin2Cell.setBorder(0);
		gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase gstTin2Val = new Phrase(gstTin2Str, font8);
		PdfPCell gstTin2ValCell = new PdfPCell(gstTin2Val);
		// gstTin2ValCell.addElement(gstTin2Val);
		gstTin2ValCell.setBorder(0);
		gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		/**
		 * Date 27/2/2018 
		 * by Jayshree 
		 * Des.To add the customer Email id
		 */
		Phrase emailph2=new Phrase("Email",font8bold);
		PdfPCell emailCell2=new PdfPCell(emailph2);
		emailCell2.setBorder(0);
		emailCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		String custemailval1 = null;
		if(custbranchmailFlag){
		if(customerBranch.getEmail()!=null&&!customerBranch.getEmail().equals("")){
			custemailval1 = customerBranch.getEmail();
		}

		}
		//by Ashwini Patil to print branch email on contract pdf if branch selected
		else{
			if(customerBranch!=null){	
				custemailval1 = customerBranch.getEmail();
			}
			else{
				if (cust.getEmail() != null) {
					custemailval1 = cust.getEmail();
				}else {
				custemailval1 ="";
				}
			}
		}
		
		Phrase emailphval2=new Phrase(custemailval1,font8);
		PdfPCell emailCellval2=new PdfPCell(emailphval2);
		emailCellval2.setBorder(0);
		emailCellval2.setHorizontalAlignment(Element.ALIGN_LEFT);
		//End By Jayshree
		
	/**Date 10-4-2019 by Amol 
		 * added a kind attn field**
		 */
		Phrase kindAttn=new Phrase("Kind Attn",font8bold);
		PdfPCell kindAttnCell2=new PdfPCell(kindAttn);
		kindAttnCell2.setBorder(0);
		kindAttnCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	/**
	 * @author Ashwini Patil
	 * @since 20-1-22
	 * annexure
	 */
		Phrase referBranchAnnexure=new Phrase("Check service loactions Annexure for more details",font8bold);
		PdfPCell referBranchAnnexureCell2=new PdfPCell(referBranchAnnexure);
		referBranchAnnexureCell2.setBorder(0);
		referBranchAnnexureCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		referBranchAnnexureCell2.setColspan(3);
	
		String pocname="";
		
		
		if(pocNameFlag){
		
			if(customerBranch!=null){
				logger.log(Level.SEVERE, "custbranch name ");
			    System.out.printf("customer branch in attn",customerBranch );
				
				
				if(customerBranch.getPocName()!=null){
					pocname=customerBranch.getPocName();
				}else{
					pocname="";
				}
				logger.log(Level.SEVERE, "cust  poc name "+pocname);
				System.out.println("cust  poc name"+pocname);
				
			}
		
		}
		else{
			//By Ashwini Patil to set poc name from branch on contract pdf
			if(customerBranch!=null){
				if(customerBranch.getPocName()!=null)
					pocname=customerBranch.getPocName();
			}				
			else{
					if(cust.getFullname()!=null)
						pocname=cust.getFullname();
					else
						pocname="";
				}
			}
			
			
			logger.log(Level.SEVERE, "poc name "+pocname);
		
		Phrase kindAttnvalue=new Phrase(pocname,font8);
		PdfPCell kindAttnvalueval2=new PdfPCell(kindAttnvalue);
		kindAttnvalueval2.setBorder(0);
		kindAttnvalueval2.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		colonTable.addCell(nameCell);
		colonTable.addCell(colonCell_bottonaligned); //Ashwini Patil has changed coloncell
		colonTable.addCell(name2CellValCell);
		/**@Sheetal : 10-02-2022
	    Des : Adding Kind Attn just after name cell requirement by pest-o-shield**/
		
		if(pocname!=null&&!pocname.equals("")){
			colonTable.addCell(kindAttnCell2);
			colonTable.addCell(colonCell);
			colonTable.addCell(kindAttnvalueval2);
		}
		/**
		 * Updated By: Viraj
		 * Date: 30-03-2019
		 * Description:To create a new cell for mobile
		 */
		colonTable.addCell(mob1Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(mob1ValCell);
		/** Ends **/
		colonTable.addCell(addressCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(address2ValCell);
		
		/**
		 * Date 27/2/2018 
		 * by Jayshree 
		 * Des.To add the customer Email id
		 */
		
		if(custemailval1!=null&&!custemailval1.equals("")){
			colonTable.addCell(emailCell2);
			colonTable.addCell(colonCell);
			colonTable.addCell(emailCellval2);
		}
		
//		if(pocname!=null&&!pocname.equals("")){
//			colonTable.addCell(kindAttnCell2);
//			colonTable.addCell(colonCell);
//			colonTable.addCell(kindAttnvalueval2);
//		}
		
		//By Ashwini Patil to add branch annexure table to contract pdf if multiple brnaches are selected
		if(multipleServiceBranchesFlag){
			colonTable.addCell(referBranchAnnexureCell2);
		}
		
		
		
		
		
		//End By Jayshree
		
		
		
		/**
		 * Date 5/12/2017 By Jayshree To Remove the Gstin number
		 */

		// colonTable.addCell(gstTinCell);
		// colonTable.addCell(colonCell);
		// colonTable.addCell(gstTinValCell);
		// End by jayshree
		PdfPCell cell2 = new PdfPCell();
		cell2.setBorder(0);
		cell2.addElement(colonTable);

		Phrase state2 = new Phrase("State", font8bold);
		PdfPCell state2Cell = new PdfPCell(state2);
		// state2Cell.addElement(state2);
		state2Cell.setBorder(0);
		state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2Val = new Phrase(cust.getSecondaryAdress().getState()
				.trim(), font8);
		PdfPCell state2ValCell = new PdfPCell(state2Val);
		// state2ValCell.addElement(state2Val);
		state2ValCell.setBorder(0);
		state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String stsecCo = "";
		if (stateList != null) {
			for (int i = 0; i < stateList.size(); i++) {
				if (stateList
						.get(i)
						.getStateName()
						.trim()
						.equalsIgnoreCase(
								cust.getSecondaryAdress().getState().trim())) {
					stsecCo = stateList.get(i).getStateCode().trim();
					break;
				}
			}
		}

		Phrase state2Code = new Phrase("State Code", font8bold);
		PdfPCell state2CodeCell = new PdfPCell(state2Code);
		// state2CodeCell.addElement(state2Code);
		state2CodeCell.setBorder(0);
		state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase state2CodeVal = new Phrase(stsecCo, font8);
		PdfPCell state2CodeValCell = new PdfPCell(state2CodeVal);
		// state2CodeValCell.addElement(state2CodeVal);
		// state2CodeValCell.setBorder(0);
		state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPTable state2table = new PdfPTable(2);
		state2table.setWidthPercentage(100);

		colonTable = new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2Cell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2ValCell);
		PdfPCell state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		colonTable = new PdfPTable(4);
		colonTable.setWidthPercentage(100);
		// float[] columnStateCodeCollonWidth = {35,5,15,45};
		try {
			colonTable.setWidths(columnStateCodeCollonWidth);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		colonTable.addCell(state2CodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(state2CodeValCell);
		colonTable.addCell(blakCell);

		state2ValcolonTableCell = new PdfPCell();
		state2ValcolonTableCell.setBorder(0);
		state2ValcolonTableCell.addElement(colonTable);
		state2table.addCell(state2ValcolonTableCell);

		PdfPCell state2TableCell = new PdfPCell(state2table);
		state2TableCell.setBorder(0);
		// state2TableCell.addElement(state2table);

		part2Table.addCell(cell2);

		/**
		 * Date 5/12/2017 By jayshree Des.To remove the state, state code,Gstin
		 * Value
		 */
		// part2Table.addCell(state2TableCell);

		PdfPCell part2TableCell = new PdfPCell(part2Table);
		// part2TableCell.addElement(part2Table);//Comment by jayshree
		/* Part 2 Ends */

		Phrase blankCell = new Phrase(" ", font8);

		/**
		 * Date 5/12/2017 Dev.By Jayshree Des.To add the Service address and
		 * billing address heading in table
		 */

		Phrase billingadd = new Phrase("Billing Address", nameAddressBoldFont);
		PdfPCell billingCell = new PdfPCell(billingadd);
		billingCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		Phrase serviceadd = new Phrase("Service Address", nameAddressBoldFont);
		PdfPCell serviceCell = new PdfPCell(serviceadd);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);

		mainTable.addCell(billingCell);
		mainTable.addCell(serviceCell);
		// End by jayshree
		mainTable.addCell(part1TableCell);
		mainTable.addCell(part2TableCell);
//		mainTable.addCell(blankCell);
//		mainTable.addCell(blankCell);

		
			try {
				document.add(mainTable);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		
			
	}
   private String toCamelCase(String s) {
       String[] parts = s.split(" ");
       String camelCaseString = "";
       for (String part : parts){
           if(part!=null && part.trim().length()>0)
          camelCaseString = camelCaseString + toProperCase(part);
           else
               camelCaseString=camelCaseString+part+" ";   
       }
       return camelCaseString;
    }
   
   private String toProperCase(String s) {
       String temp=s.trim();
       String spaces="";
       if(temp.length()!=s.length())
       {
       int startCharIndex=s.charAt(temp.indexOf(0));
       spaces=s.substring(0,startCharIndex);
       }
       temp=temp.substring(0, 1).toUpperCase() +
       spaces+temp.substring(1).toLowerCase()+" ";
       return temp;

   }
   
   private ArrayList<String> getCustomerBranchList(List<SalesLineItem> itemList) {
		HashSet<String> branchHs=new HashSet<String>();
		
		for(SalesLineItem itemObj:itemList){
			if(itemObj.getCustomerBranchSchedulingInfo()!=null){
				ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
				if(branchSchedulingList!=null) {
					for(BranchWiseScheduling obj:branchSchedulingList){
						if(obj.isCheck()==true){
							branchHs.add(obj.getBranchName());
						}
					}				
				}
			}
		}
		
		if(branchHs!=null&&branchHs.size()!=0){
			ArrayList<String> branchList=new ArrayList<String>(branchHs);
			logger.log(Level.SEVERE,"In Side AList3:"+branchList.size());
			return branchList;
		}
		
		return null;
	}
   public List<CustomerBranchDetails> getSelectedBranches(Contract contEntity){
		ArrayList<String> custbranchlist=getCustomerBranchList(contEntity.getItems());
		customerBranchList= ofy().load().type(CustomerBranchDetails.class)
				.filter("cinfo.count",contEntity.getCinfo().getCount())
				.filter("companyId", contEntity.getCompanyId()).list();
		
		List<CustomerBranchDetails> selectedBranches=new ArrayList<CustomerBranchDetails>();
		System.out.println("custbranchlist.size()="+custbranchlist.size());
		System.out.println("customerBranchList.size()="+customerBranchList.size());
		for(int i=0;i<custbranchlist.size();i++){
			System.out.println("in first for");
			for(CustomerBranchDetails c:customerBranchList){
				System.out.println("in second for c.getBusinessUnitName()="+c.getBusinessUnitName()+" and custbranchlist.get("+i+")="+custbranchlist.get(i));
				if(c.getBusinessUnitName().equals(custbranchlist.get(i))){
					System.out.println("in if of for loop");
					selectedBranches.add(c);
				}
			}			
		}
		return selectedBranches;
	}
	

}