package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class ServiceQuotationPdf {


	public Document document;
	
	Company comp;
	Customer cust;
	Contract con; //Added by Ashwini
	Quotation quot;
	Lead serviceLead;
	boolean upcflag=false;
	Phrase chunk;
	PdfPCell pdfpremise, pdfservices, pdfduration, pdffrequency, pdfprice,pdfblank,pdfttl,pdfttlval;
	PdfPCell pdfdays, pdfpercent, pdfcomment;
	PdfPCell  pdftaxname,pdftaxpercent, pdfassval, pdfamt;
	ProcessConfiguration processConfig;
	float[] colwidth = { 1.5f, 0.2f, 4.8f, 1.0f, 0.2f, 1.2f };
	float[] col1width = { 1.5f, 0.2f, 3.0f,3.0f, 1.0f, 0.2f, 3.0f};
	float[] tblcol1width = { 1.5f, 1.5f, 7.0f };
	float[] tblcolwidth = { 0.7f, 1.0f, 1.0f, 2.0f, 3.0f };
	
	Logger logger = Logger.getLogger("NameOfYourLogger");

	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12boldul, font12, font10bold, font10, font14bold, font9,font14boldul;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");

	DecimalFormat df = new DecimalFormat("0.00");
	
	private PdfPCell custlandcell;
	
	private String employeeDesig="Sales Executive";
	
	public ServiceQuotationPdf(){
		
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);

	}
	
	public void setservicequotation(Long count,String preprint) {
		
		quot = ofy().load().type(Quotation.class).id(count).now();
		
		 /*
		    * Date:14/09/2018
		    * Developer:Ashwini
		    * Des:to also add updated service Address
		    */
				
				
				if (quot.getCompanyId()!=null){
					con = ofy().load().type(Contract.class)
							.filter("count", quot.getCompanyId())
							.filter("companyId", quot.getCompanyId()).first().now();
				}
				/*
				 * End by Ashwini
				 */
				
		
		if (quot.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId())
					.filter("companyId", quot.getCompanyId()).first().now();
		
		if (quot.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", quot.getCustomerId()).first().now();

		if (quot.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", quot.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (quot.getCompanyId() != null)
			quot = ofy().load().type(Quotation.class)
					.filter("companyId", quot.getCompanyId())
					.filter("count", quot.getCount()).first().now();
		else
			quot = ofy().load().type(Quotation.class)
					.filter("count", quot.getCount()).first().now();
		
		 logger.log(Level.SEVERE,"before lead load comp id is"+quot.getCompanyId());
		if (quot.getCompanyId() != null){
			 logger.log(Level.SEVERE,"inside if lead load comp id is"+quot.getCompanyId());			
			serviceLead = ofy().load().type(Lead.class)
					.filter("companyId", quot.getCompanyId())
					.filter("count", quot.getLeadCount()).first().now();
		}
		else{
			 logger.log(Level.SEVERE,"inside else lead load comp id is"+quot.getCompanyId());
			serviceLead = ofy().load().type(Lead.class)
					.filter("count", quot.getLeadCount()).first().now();
		}
		
		
		if(quot.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", quot.getCompanyId()).filter("processName", "Contract").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag=true;
						
					}
				}
			}
		}
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(quot !=null && quot.getBranch() != null && quot.getBranch().trim().length() >0){
				
				Branch branchDt = ofy().load().type(Branch.class).filter("companyId",quot.getCompanyId()).filter("buisnessUnitName", quot.getBranch()).first().now();
							
				if(branchDt!=null){
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
		
			}
		}
	}
	
	
	public void createPdf(String preprint) {
		
		if(upcflag==false && preprint.equals("plane")){
		createBlankHeading();
		createCompanyHeadding();
		}else{
			
			if(preprint.equals("yes")){
				
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			
			if(preprint.equals("no")){
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}
			
		}
		createQuotationHeadding();
		createReferenceandDateHeadding();
		
		System.out.println("refrence no and date has been printed");
		
		createCustomerDetailsHeading();
		createSubjectandMessageHeadding();
//		createQuotationHeadding();
		createProductInfoHeadding();
		
//		createProductTaxHeadding();
//		System.out.println("Product info has been created");
		
		createTermsandPaymentHeadding();
		
//		craeteTandPHeadding();
		
		createValidityofquotationHeadding();
		
	}
	
	
	
	private void createBlankforUPC() {
		
		/*
		 * Commented by Ashwini
		 */
		
//			 Paragraph blank =new Paragraph();
//			    blank.add(Chunk.NEWLINE);
//			
//			
//			try {
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//				document.add(blank);
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
		
		/*
		 *Date:30/7/2018
		 * Developer:Ashwini
		 * Des:To increase header space
	  */
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" ,quot.getCompanyId())){
			 Paragraph blank =new Paragraph();
			 blank.add(Chunk.NEWLINE);
			
			
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}else{
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			
			try {
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				document.add(blank);
				
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * End by Ashwini
		 */
		}


	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	
	public  void createBlankHeading() {
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    blank.add(Chunk.NEWLINE);
	    
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void createLogo(Document doc, Company comp) {
		
		//********************logo for server ********************
		
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		
	public void createCompanyHeadding() {
		
		Phrase compname = new Phrase("" + comp.getBusinessUnitName(),font16boldul);
		Paragraph p = new Paragraph();
		p.add(compname);
		p.setAlignment(Element.ALIGN_CENTER);
		
//	System.out.println("company name has been created");

		PdfPCell compnamecell = new PdfPCell();
		compnamecell.addElement(p);
		compnamecell.setBorder(0);
		
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
			
			
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		
		
		PdfPTable table = new PdfPTable(1);
		table.addCell(compnamecell);
		table.addCell(addresscell);
		table.addCell(localitycell);
		table.addCell(contactcell);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(10f);
		
		PdfPCell cell = new PdfPCell();
		cell.addElement(table);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);
		
	
	
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}
		
		
//	*************************************************************************************************************	
		
	
	public void createReferenceandDateHeadding() {
		
		
//		For static dynamic entity: *************************************
	
		Phrase ref = new Phrase("Reference No.",font12bold);
		Paragraph refpara = new Paragraph();
		refpara.add(ref);
		refpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell refcell = new PdfPCell();
		refcell.addElement(refpara);
		refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase refno=null;
		if(quot.getReferenceNumber() !=null && !quot.getReferenceNumber().equals("")){
			refno = new Phrase(quot.getReferenceNumber());
		}
		else{
//			refno = new Phrase(" ");
			/** Date 10-01-2018 above one line old code commneted and below new code added by vijay 
			 * for reference no nothing then quotation id will print
			 **/
			refno = new Phrase(quot.getCount()+"",font10bold);
		}
		
		PdfPCell refnocell  = new PdfPCell();
		refnocell.addElement(refno);
		refnocell.setBorder(0);
		refnocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase date = new Phrase("Date",font12bold);
		Paragraph datepara = new Paragraph();
		datepara.add(date);
		datepara.setAlignment(Element.ALIGN_RIGHT);
		
		PdfPCell datecell = new PdfPCell();
		datecell.addElement(datepara);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		Phrase dt = null;
		
		if(quot.getReferenceDate() !=null){
			dt = new Phrase(fmt.format(quot.getReferenceDate()));
		}
		else{
//			dt = new Phrase(" ");
			/** Date 10-01-2018 above one line old code commneted and below new code added by vijay 
			 * if reference Date is nothing then quotation Date will print
			 */
			dt = new Phrase(fmt.format(quot.getQuotationDate()),font10bold);
		}
//		if(fmt.format(quot.getReferenceDate()) !=null){
//			dt = new Phrase("" + fmt.format(quot.getReferenceDate()));	
//			
//		}
		
		PdfPCell dtcell = new PdfPCell();
		dtcell.addElement(dt);
		dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
//		*****************************************************************************
		Phrase col = new Phrase(":",font12bold);
		Paragraph colpara = new Paragraph();
		colpara.add(col);
		colpara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPCell colcell = new PdfPCell();
		colcell.addElement(colpara);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	    ******************************************************************************	
		
		PdfPTable table1 = new PdfPTable(6);
		
		try {
			table1.setWidths(colwidth);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		table1.addCell(refcell);
		table1.addCell(colcell);
		table1.addCell(refnocell);
		
		table1.addCell(datecell);
		table1.addCell(colcell);
		table1.addCell(dtcell);
		
		
//		table1.addCell(refcell);
//		table1.addCell(colcell);
//		table1.addCell(refnocell);
		
		table1.setWidthPercentage(100f);
		
		PdfPCell cell1 = new PdfPCell();
		cell1.addElement(table1);
		cell1.setBorder(0);
	
		PdfPTable parent1 = new PdfPTable(1);
		
		parent1.addCell(cell1);
		parent1.setWidthPercentage(100);
		
		
		try {
			document.add(parent1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createCustomerDetailsHeading(){
		
//		For static and dynamic entity1 ***************************************************** 
		
		Phrase to = new Phrase("To,", font12);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setBorder(0);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		/**
		 * Date 10-01-2018 By Vijay
		 * Below old code commented and new code added below
		 */
		
//		Phrase company1 = null;
//		
//		if(cust.getCompanyName()!=null && cust.isCompany()==true){
//			
//			company1 = new Phrase("" + "The Manager," + "\n" + cust.getCompanyName()+ ",",font10);
//			System.out.println("the company name is: " + cust.getCompanyName());
//		}
//		
//		else{
//			
//			company1 = new Phrase(cust.getFullname(),font10);
//		}
//		
//		
////		Phrase manager = new Phrase("The Manager,",font10);
//		
//		PdfPCell company1cell = new PdfPCell(company1);
//		company1cell.setBorder(0);
//		company1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/**
		 * Date 10-01-2018 By Vijay
		 * customer name with address no space gap between Name and address
		 */
		
		/*
		 * commented by Ashwini
		 */
		
//		String customerInfo="";
//		if(cust.getCompanyName()!=null && cust.isCompany()==true){
//			customerInfo = "The Manager," + "\n" + cust.getCompanyName()+ ",";
//		}
//		else{
//			customerInfo = cust.getFullname();
//		}
//		/**
//		 * ends here
//		 */
//
//		Phrase custInfo = new Phrase(cust.getFullname(), font10);
//		PdfPCell custInfoCell = new PdfPCell(custInfo);
//		custInfoCell.setBorder(0); 
//		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		/*
		 * Date:20/10/2018
		 * Developer:Ashwini
		 * Des:To print Correspondence Name
		 */
		
		String customerInfo="";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			customerInfo = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				customerInfo = "M/S " + cust.getCompanyName().trim();
			} else {
				customerInfo = cust.getFullname().trim();
			}
		}
		
		/*
		 * end by Ashwini
		 */
		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			/**
			 * Date 10-01-2018 By vijay
			 * Below old code commented by Vijay
			 */
			
//			if(cust.getAdress().getAddrLine2()!=null){
//				if(cust.getAdress().getLandmark()!=null){
//					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
//				}else{
//					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
//				}
//			}else{
//				if(cust.getAdress().getLandmark()!=null){
//					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
//				}else{
//					custAdd1=cust.getAdress().getAddrLine1();
//				}
//			}
////			
//			if(cust.getAdress().getLocality()!=null){
//				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
//						
//			}else{
//				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+" "+cust.getAdress().getPin();
//			}
			
			/**
			 * Date 10-01-2018 By vijay
			 * As per new changes for address
			 */
			if(cust.getAdress().getAddrLine2()!=null && !cust.getAdress().getAddrLine2().equals("")){
				if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+",\n"+cust.getAdress().getAddrLine2()+",\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+",\n"+cust.getAdress().getAddrLine2();
				}
			}else{
				if(cust.getAdress().getLandmark()!=null && !cust.getAdress().getLandmark().equals("") ){
					custAdd1=cust.getAdress().getAddrLine1()+",\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(cust.getAdress().getLocality()!=null){
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin()+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry();

			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getPin()+","+cust.getAdress().getCity()+",\n"+cust.getAdress().getState()+" "+cust.getAdress().getCountry();
			}
			/**
			 * ends here
			 */
		}
		
//		Phrase custAddInfo = new Phrase(custFullAdd1, font10);
		/** above line old code
		 * Date 10-01-2018 below phrase added customer name and address without space gap
		 */
		
		
		Phrase custAddInfo = new Phrase(customerInfo+"\n"+custFullAdd1, font12);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		
		
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		PdfPCell pocCell=null;
		if(cust.getCompanyName()!=null && cust.isCompany()==true){
			
			 Phrase poc = new Phrase("POC : "+cust.getFullname(),font12);
			 pocCell = new PdfPCell(poc);
			 pocCell.setBorder(0);
		}
		
		PdfPTable table2 = new PdfPTable(1);
		table2.addCell(toCell);
		/** Date 10-01-2018 below one line old commented by vijay  **/
//		table2.addCell(company1cell);
		/** ends here **/
//		table2.addCell(custInfoCell);
		table2.addCell(custAddInfoCell);
		
		if(cust.getCompanyName()!=null && cust.isCompany()==true){
			table2.addCell(pocCell);
		}
		
		table2.setWidthPercentage(100f);		
		
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(table2);
		customerCell.setBorder(0);
		customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
		PdfPTable parent2 = new PdfPTable(1);
		parent2.addCell(customerCell);
		parent2.setWidthPercentage(100);
		
		try {
			document.add(parent2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void createSubjectandMessageHeadding(){
		
//		For static an dynamic entity2 ******************************************************************
		
//		Phrase sub = new Phrase("Subject  : Quotation for Pest Management Services",font12bold);
		
		Phrase para1 = new Phrase();
		Chunk chk1 = new Chunk("Subject : ", font12bold);
		Chunk chk2 = new Chunk("Quotation for Pest Management Service", font12);
		para1.add(chk1);
		para1.add(chk2);
		
		PdfPCell subcell = new PdfPCell();
		subcell.addElement(para1);
		subcell.setBorder(0);
		subcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase dear = new Phrase("Dear Sir/Madam,",font12);
		PdfPCell dearCell = new PdfPCell(dear);
		dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dearCell.setBorder(0);
		
		String title1 = "";
		 logger.log(Level.SEVERE,"service lead"+serviceLead);
		
		//Ashwini Patil Date:19-06-2022 Description: In quotation print Need actual designation of salesperson
		Employee emp= ofy().load().type(Employee.class)
			.filter("companyId", quot.getCompanyId())
			.filter("fullname", quot.getEmployee()).first().now();
		
		if(emp!=null&&!emp.getDesignation().equals("")) {
			employeeDesig=emp.getDesignation();
			logger.log(Level.SEVERE,"employeeDesig="+emp.getDesignation()+emp.getFullName());
		}
		if(serviceLead!=null){
			 logger.log(Level.SEVERE,"inside servicelead!=null");
			 System.out.println("Lead"+serviceLead);
			 
			if(serviceLead.getCreationDate()!=null){
				 logger.log(Level.SEVERE,"inside date is present");
			title1 = "Thank you for keen interest shown by you regarding pest management services for your "+"\n"+ "Premises. "+
					 "In response to your inquiry dated "+(fmt.format(serviceLead.getCreationDate())+" our "+employeeDesig+" "+ (quot.getEmployee())+" has carried out an inspection of your Premises, as per visit & inspection, we would like to make the following recommendations for services & reasonable rates for the same.");
			}
			else
			{
			title1 = "Thank you for keen interest shown by you regarding pest management services for your,"+"\n"+ "Premises. "+
					 "In response to your inquiry our "+employeeDesig+" "+ (quot.getEmployee())+" has carried out an inspection of your Premises, as per visit & inspection, we would like to make the following recommendations for services & reasonable rates for the same.";
			}
		}
		else
		{	
			 logger.log(Level.SEVERE,"inside service lead is null");
			title1 = "Thank you for keen interest shown by you regarding pest management services for your,"+"\n"+ "Premises. "+
					 "In response to your inquiry our "+employeeDesig+" "+ (quot.getEmployee())+" has carried out an inspection of your Premises, as per visit & inspection, we would like to make the following recommendations for services & reasonable rates for the same.";
		}
		
		Phrase msg = new Phrase(title1, font12);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setBorder(0);
		msgCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table3 = new PdfPTable(1);
		table3.addCell(subcell);
		table3.addCell(dearCell);
		table3.addCell(msgCell);
		
		table3.setWidthPercentage(100f);
		
		PdfPCell cell2 = new PdfPCell();
		cell2.addElement(table3);
		cell2.setBorder(0);
		
		PdfPTable parent3 = new PdfPTable(1);
		parent3.addCell(cell2);
		parent3.setWidthPercentage(100);
		
		
		try {
			document.add(parent3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void createQuotationHeadding() {
		
		Phrase quothead = new Phrase("QUOTATION",font12boldul);
		Paragraph quotheadpara = new Paragraph();
		quotheadpara.add(quothead);
		quotheadpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell quotheadcell = new PdfPCell(quotheadpara);
		quotheadcell.setBorder(0);
		quotheadcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		quotheadcell.setColspan(7);
		
//		Phrase blank = new Phrase("");
//		blank.add("");
//		PdfPCell blcell = new PdfPCell(blank);
//		blcell.setBorder(0);
//		blcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase quotdate = new Phrase("Date",font10bold);
		PdfPCell quotdatecell = new PdfPCell(quotdate);
		quotdatecell.setBorder(0);
		quotdatecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase quotdt = null;
		
	if(fmt.format(quot.getQuotationDate()) != null){
			
			quotdt = new Phrase("" + fmt.format(quot.getQuotationDate()),font10bold);
			
	}
		
		PdfPCell quotdtcell = new PdfPCell(quotdt);
		quotdtcell.setBorder(0);
		quotdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase quotid = new Phrase("Id",font10bold);
		PdfPCell quotidcell = new PdfPCell(quotid);
		quotidcell.setBorder(0);
		quotidcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase quotid1 = null;
		
		if(quot.getCount() !=0l){
			
			quotid1 = new Phrase("" + quot.getCount(),font10bold);
			
		}
		
		PdfPCell quotid1cell = new PdfPCell(quotid1);
		quotid1cell.setBorder(0);
		quotid1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase colon = new Phrase(":",font10bold);
		PdfPCell coloncell = new PdfPCell(colon);
		coloncell.setBorder(0);
		coloncell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable quottable = new PdfPTable(7);
		
		try {
			quottable.setWidths(col1width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
//		quottable.addCell(quotdatecell);
//		quottable.addCell(coloncell);
//		quottable.addCell(quotdtcell);
		quottable.addCell(quotheadcell);
//		quottable.addCell(blcell);
//		quottable.addCell(blcell);
//		quottable.addCell(quotidcell);
//		quottable.addCell(coloncell);
//		quottable.addCell(quotid1cell);
		quottable.setWidthPercentage(100f);
		
		PdfPCell cell3 = new PdfPCell(quottable);
		cell3.setBorder(0);
		
		PdfPTable parent4 = new PdfPTable(1);
		parent4.addCell(cell3);
		parent4.setWidthPercentage(100);
		parent4.setSpacingAfter(10f);
		
		
		try {
			document.add(parent4);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	public void createValidityofquotationHeadding(){
		
		Phrase validity = new Phrase("Validity of quotation: "+"" + fmt.format(quot.getValidUntill()),font12bold);
		PdfPCell validitycell = new PdfPCell(validity);
		validitycell.setBorder(0);
		validitycell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
//		Phrase validity1 = null;
//		if(quot.getValidUntill() !=null){
//			
//			validity1 = new Phrase("" + fmt.format(quot.getValidUntill()),font12);	
//			
//		}
		
//		PdfPCell validity1cell = new PdfPCell(validity1);
//		validity1cell.setBorder(0);
//		validity1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		/**
		 * Date 10-01-2018 By Vijay
		 * Below code added for Pepcopp changes replacing company cell number with Sales person cell number and
		 * Branch cell number
		 */
		Employee emp = ofy().load().type(Employee.class).filter("companyId", quot.getCompanyId()).filter("fullname", quot.getEmployee()).first().now();
		Branch branch = ofy().load().type(Branch.class).filter("companyId", quot.getCompanyId()).filter("buisnessUnitName", quot.getBranch()).first().now();
		
		
		String title3= "";
//		title3 = "We hope you will find our quotation reasonable and await your acceptance of this quotation; should you require further information on the above, please call us on "  + (comp.getCellNumber1() + "\n" + "Thanks & regards,");
		title3 = "We hope you will find our quotation reasonable and await your acceptance of this quotation; should you require further information on the above, please call us on Sales Person Contact No "  + (emp.getCellNumber1()+" Branch Contact No "+branch.getCellNumber1() + "\n\n" + "Thanks & regards,");
		/**
		 * ends here
		 */
		Phrase str2 = new Phrase(title3,font12);
		PdfPCell str2cell = new PdfPCell(str2);
		str2cell.setBorder(0);
		str2cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase blank1 = new Phrase();
	    blank1.add(Chunk.NEWLINE);
	    PdfPCell blank1cell = new PdfPCell(blank1);
	    blank1cell.setBorder(0);
		
		Phrase pest = null;
		if(comp.getBusinessUnitName() !=null){
			
			pest = new Phrase("" + comp.getBusinessUnitName(),font12bold);	
			
		}
		
		
		Phrase signatory =  new Phrase("AUTHORISED SIGNATORY");
		PdfPCell signatorycell= new PdfPCell(signatory);
		signatorycell.setBorder(0);
		
		Phrase newLine =  new Phrase(Chunk.NEWLINE);
		PdfPCell newLineCell= new PdfPCell(newLine);
		newLineCell.setBorder(0);
		
		
		PdfPCell pestcell = new PdfPCell(pest); 
		pestcell.setBorder(0);
		pestcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable validitytable = new PdfPTable(1);
		validitytable.addCell(validitycell);
		
//		validitytable.addCell(validity1cell);
		
		validitytable.addCell(str2cell);
		validitytable.addCell(blank1cell);
		validitytable.addCell(pestcell);
		validitytable.addCell(newLineCell);
		validitytable.addCell(newLineCell);
		validitytable.addCell(signatorycell);
		
		
		validitytable.setWidthPercentage(100f);
		
		PdfPCell cell6 = new PdfPCell(validitytable);
		cell6.setBorder(0);
		
		PdfPTable parent6 = new PdfPTable(1);
		parent6.addCell(cell6);
		parent6.setWidthPercentage(100);
		
		try {
			document.add(parent6);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
	}
	
	
	
	
	public void createProductInfoHeadding(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 10 ,Font.BOLD);
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(100);
		

		float[] tblcolwidth = { 2.6f, 2f, 2f, 1.6f, 1.8f };
		try {
			table.setWidths(tblcolwidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase premise = new Phrase("PREMISES UNDER CONTRACT", font1);
		Phrase services = new Phrase("SERVICES", font1);
		Phrase duration = new Phrase("PERIOD OF CONTRACT (DAYS)",font1);
		Phrase frequency = new Phrase("FREQUENCY OF SERVICES", font1);
		Phrase price = new Phrase("CONTRACT CHARGES IN Rs.", font1);
		
//		Phrase tax = new Phrase(quot.getProductTaxes().get(0).getChargeName() + "" + quot.getProductTaxes().get(0).getChargePercent() + "%",font8);
//		PdfPCell taxcell = new PdfPCell(tax);
//		taxcell.setBorder(0);
//
//		Phrase taxval = new Phrase(quot.getProductTaxes().get(0).getChargePayable() + "" ,font8);
//		PdfPCell taxvalcell = new PdfPCell(taxval);
//		taxvalcell.setBorder(0);
//		
		
		
		PdfPCell codecell = new PdfPCell(premise);
		codecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell servicescell = new PdfPCell(services);
		servicescell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell durationcell = new PdfPCell(duration);
		durationcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell frequencycell = new PdfPCell(frequency);
		frequencycell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell pricecell = new PdfPCell(price);
		pricecell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
				
		table.addCell(codecell);
		table.addCell(servicescell);
		table.addCell(durationcell);
		table.addCell(frequencycell);
		table.addCell(pricecell);
		

//		table.addCell(taxcell);
//		table.addCell(taxvalcell);
		
		
		
	for(int i=0; i<this.quot.getItems().size(); i++) {	

			chunk = new Phrase(quot.getItems().get(i).getPremisesDetails()+ "",font10);
			
			pdfpremise = new PdfPCell(chunk);
			pdfpremise.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(quot.getItems().get(i).getProductName() + "",font10);
			
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(quot.getItems().get(i).getDuration() + "",font10);
			
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(quot.getItems().get(i).getNumberOfServices() + "",font10);
			
			pdffrequency = new PdfPCell(chunk);
			pdffrequency.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			int pricechunk= (int) quot.getItems().get(i).getPrice();
			
			chunk = new Phrase(pricechunk+"",font10);
			
//			chunk = new Phrase(quot.getItems().get(i).getPrice() + "",font8);
			
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			table.addCell(pdfpremise);
			table.addCell(pdfservices);
			table.addCell(pdfduration);
			table.addCell(pdffrequency);
			table.addCell(pdfprice);
		
		}
	
	
	Phrase blank=new Phrase();
	blank.add(" ");
	
	PdfPCell blankCell = new PdfPCell();
	blankCell.addElement(blank);
	blankCell.setBorder(0);
	
	Phrase total = new Phrase("Total",font10);
	PdfPCell totalcell = new PdfPCell(total);
	
	Phrase totalvalue = new Phrase(df.format(quot.getTotalAmount())+"",font10);
	PdfPCell totalvalcell = new PdfPCell(totalvalue);
	totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);

	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(totalcell);
	table.addCell(totalvalcell);
	
	for(int i=0; i<this.quot.getProductTaxes().size(); i++){
		
		if(quot.getProductTaxes().get(i).getChargePercent() !=0){
			
			
		chunk = new Phrase("" + quot.getProductTaxes().get(i).getChargeName() + "" + quot.getProductTaxes().get(i).getChargePercent() + "%",font10);
		System.out.println("expected name: " + quot.getProductTaxes().get(i).getChargeName());
		pdftaxname = new PdfPCell(chunk);
		pdftaxname.setHorizontalAlignment(Element.ALIGN_LEFT);
		
	/*****************************************************************************************************/	
//		chunk = new Phrase(quot.getProductTaxes().get(i).getChargePercent()+"",font8);
//		System.out.println("expected ChargePercent:" + quot.getProductTaxes().get(i).getChargePercent());
//		pdftaxpercent = new PdfPCell(chunk);
//		pdftaxpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
		
//		chunk = new Phrase("" + quot.getProductTaxes().get(i).getAssessableAmount(),font8);
//		System.out.println("expected AssessableAmount: " + quot.getProductTaxes().get(i).getAssessableAmount());
//		pdfassval = new PdfPCell(chunk);
//		pdfassval.setHorizontalAlignment(Element.ALIGN_CENTER);
		
	/***********************************************************************************************/	
		
		chunk = new Phrase(df.format(quot.getProductTaxes().get(i).getChargePayable())+ "",font10);
		System.out.println("expected ChargeAbsValue: " + quot.getProductTaxes().get(i).getChargePayable());
		pdfamt = new PdfPCell(chunk);
		pdfamt.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		table.addCell(pdftaxname);
		table.addCell(pdfamt);
		
		}
		
	}
	
	Phrase subtotal = new Phrase("Sub total",font10);
	PdfPCell subtotalcell = new PdfPCell(subtotal);
	
	
	Phrase subtotalvalue = new Phrase(df.format(quot.getNetpayable())+"",font10);
	PdfPCell subtotalvalcell = new PdfPCell(subtotalvalue);
	subtotalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);

	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(blankCell);
	table.addCell(subtotalcell);
	table.addCell(subtotalvalcell);
	
	
	PdfPCell productcell = new PdfPCell();
	productcell.addElement(table);
	productcell.setBorder(0);
	
	PdfPTable parent7 = new PdfPTable(1);
	parent7.addCell(productcell);
	parent7.setWidthPercentage(100);
	
	
	try {
		document.add(parent7);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	}
	
	
	
	public void createTermsandPaymentHeadding(){
		
		Phrase terms = new Phrase("Terms of Payment: " ,font12bold);
		PdfPCell termscell = new PdfPCell(terms);
		termscell.setBorder(0);;
		termscell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		///////////////////////////////////////////////////////////////////////////////
		
		
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 10 ,Font.BOLD);
		PdfPTable table1 = new PdfPTable(3);
		table1.setWidthPercentage(100);
		
		
		try {
			table1.setWidths(tblcol1width);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		Phrase days = new Phrase("DAYS",font1);
		Phrase percent = new Phrase("PERCENT",font1);
		Phrase comment = new Phrase("COMMENTS",font1);
		
		
		PdfPCell dayscell = new PdfPCell(days);
		dayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell percentcell = new PdfPCell(percent);
		percentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell commentcell = new PdfPCell(comment);
		commentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table1.addCell(dayscell);
		table1.addCell(percentcell);
		table1.addCell(commentcell);
		
		for(int i=0; i<this.quot.getPaymentTermsList().size(); i++){
			
			chunk = new Phrase(quot.getPaymentTermsList().get(i).getPayTermDays() + "",font10);
			
			pdfdays = new PdfPCell(chunk);
			pdfdays.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(quot.getPaymentTermsList().get(i).getPayTermPercent() + "",font10);
			
			pdfpercent = new PdfPCell(chunk);
			pdfpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			chunk = new Phrase(quot.getPaymentTermsList().get(i).getPayTermComment() + "",font10);
			
			pdfcomment = new PdfPCell(chunk);

			
			table1.addCell(pdfdays);
			table1.addCell(pdfpercent);
			table1.addCell(pdfcomment);
			
		}
		
		
		
		PdfPCell tandpcell = new PdfPCell();
		tandpcell.addElement(table1);
		tandpcell.setBorder(0);
		
		
		///////////////////////////////////////////////////////////////
		
//		String title2 = "";
//		title2 = "Assurance: In case of pest resurgence during the contract period, interim calls, if any would be attended to without any extra cost.";
//		Phrase str1 = new Phrase(title2,font12);
		
		Phrase para1 = new Phrase();
		Chunk chk1 = new Chunk("Assurance: ", font12bold);
		Chunk chk2 = new Chunk("In case of pest resurgence during the contract period, interim calls, if any would be attended to without any extra cost.", font12);
		para1.add(chk1);
		para1.add(chk2);
		
		PdfPCell str1cell = new PdfPCell(para1);
		str1cell.setBorder(0);
		str1cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase blank=new Phrase();
		blank.add(" ");
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
		blankCell.setBorder(0);
		
		
		PdfPTable termstable = new PdfPTable(2);
		termstable.addCell(termscell);
		termstable.addCell(blankCell);
//		termstable.addCell(tandpcell);
//		termstable.addCell(blankCell);
	    termstable.setWidthPercentage(100f);
	    
	    PdfPCell cell4 = new PdfPCell();
	    cell4.addElement(termstable);
	    cell4.setBorder(0);
	    
	    PdfPTable parent5 = new PdfPTable(1);
	    parent5.addCell(cell4);
	    parent5.addCell(tandpcell);
	    parent5.addCell(str1cell);
	    parent5.setWidthPercentage(100);
	    
	    try {
			document.add(parent5);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		}
	
}
