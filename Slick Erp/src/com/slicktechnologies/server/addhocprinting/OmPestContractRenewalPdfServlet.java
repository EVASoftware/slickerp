package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

public class OmPestContractRenewalPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7082030249734295451L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid1 = req.getParameter("ContractId");
			stringid1 = stringid1.trim();
			int count = Integer.parseInt(stringid1);
			
			String stringid = req.getParameter("CompanyId");
			stringid = stringid.trim();
			Long companyId = Long.parseLong(stringid);
			
			
			String preprintStatus=req.getParameter("preprint");
			preprintStatus =preprintStatus.trim();
			String status = preprintStatus;
			
			ContractRenewal contract = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", count).first().now();
			
			OmPestContractRenewalPdf contractPdf = new OmPestContractRenewalPdf();
			
			contractPdf.document = new Document();
			Document document = contractPdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		
			 
			 document.open();
			
			 contractPdf.setContractRewnewal(contract);
			 contractPdf.createPdf(status);
			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
