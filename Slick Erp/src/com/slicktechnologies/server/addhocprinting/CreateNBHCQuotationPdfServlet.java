package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateNBHCQuotationPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5015552726516885595L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		
		resp.setContentType("application/pdf");
		
		  
		  try {
			  
			  String stringId = req.getParameter("Id");	
			  stringId=stringId.trim();
			  Long count =Long.parseLong(stringId);
			  
			  String preprintStatus = req.getParameter("preprint");	
			  preprintStatus=preprintStatus.trim();
			  
			  NBHCQuotationPdf nbhcPdf = new NBHCQuotationPdf();
			  nbhcPdf.document=new Document();
			  Document document = nbhcPdf.document;
			  
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream());// write the pdf in response
		
			
			  document.open();
			  nbhcPdf.setNBHCQuotation(count);
			  nbhcPdf.createPdf(preprintStatus);
			  document.close();
			  
		  } catch (DocumentException e) 
		  {
			e.printStackTrace();
		  } 
		  
		
		
		}
}
