package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.salesorder.SalesOrder;
import com.slicktechnologies.shared.common.workorder.WorkOrder;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class WorkOrderDeliveryNotePdf {

	Logger logger=Logger.getLogger("Work Order DeliveryNotePdf.class");

	WorkOrder workOrder;
	Customer cust;
	SalesOrder salesOrderEntity;
	Company comp;
	
	public Document document;
	
	boolean upcflag=false;
	ProcessConfiguration processConfig;
	List<State> stateList;
	List<ArticleType> articletype;

	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font6bold,font7,font7bold,font12,font16bold,font9bold,font10,font10bold,font14bold,font14,font9;

	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	Phrase ph=new Phrase(" ",font10);

	float[] columnHalfWidth = {1f,1f};
	float[] columnCollonWidth = {1.8f,0.2f,7.5f};
	float[] columnStateCodeCollonWidth = {35,5,15,45};
	float[] columnCollonGSTWidth = {0.8f,0.2f,1.3f};
	float[] columnMoreLeftWidths = {2f,1f};

	float[] column13CollonWidth = {0.1f,//Sr No
		       0.8f,//Services
		       0.2f,//HSN ACS
		       0.1f,//Qty
		       0.1f,//UOM 
		       0.1f,//Rate
		       0.1f,//Disc
		       0.1f,//Amount
//		       0.2f,//Taxable amt
//		       0.15f,//CGST
//		       0.15f,//SGST
//		       0.15f,//IGST
//		       0.15f,//Total
		       };


//	float[] column13CollonWidth = {0.1f,//Sr No
//		       0.4f,//Services
//		       0.4f,//HSN ACS
//		       0.15f,//UOM
//		       0.15f,//Qty
//		       0.2f,//Rate
//		       0.15f,//Disc
//		       0.2f,//Amount
////		       0.2f,//Taxable amt
////		       0.15f,//CGST
////		       0.15f,//SGST
////		       0.15f,//IGST
////		       0.15f,//Total
//		       };
	
	public WorkOrderDeliveryNotePdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
		font6bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
	}
	
	public void setWorkOrderDeliveryNote(long id){
		
		workOrder = ofy().load().type(WorkOrder.class).id(id).now();
		
		stateList=ofy().load().type(State.class).filter("companyId", workOrder.getCompanyId()).list();

		//Load Sales Order
		salesOrderEntity=ofy().load().type(SalesOrder.class).filter("count", workOrder.getOrderId()).filter("companyId",workOrder.getCompanyId()).first().now();
		
		//Load Customer
		cust=ofy().load().type(Customer.class).filter("count",salesOrderEntity.getCinfo().getCount()).filter("companyId", salesOrderEntity.getCompanyId()).first().now();
		
		if(salesOrderEntity.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", salesOrderEntity.getCompanyId()).filter("processName", "Quotation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag=true;
						
					}
					
				}
			}
		}
		
		//Load Company
		 comp=ofy().load().type(Company.class).filter("companyId",workOrder.getCompanyId()).first().now();
		
		
		articletype = new ArrayList<ArticleType>();
		if(cust.getArticleTypeDetails().size()!=0){
			articletype.addAll(cust.getArticleTypeDetails());
		}
		if(comp.getArticleTypeDetails().size()!=0){
			articletype.addAll(comp.getArticleTypeDetails());
		}
		
		
		
	}

	public void createPdf(String preprintStatus) {

		for(int i=0;i<3;i++)
		{
		if(upcflag==false && preprintStatus.equals("plane")){
			createHeader(i);
		}else{
			if(preprintStatus.equals("yes")){
				createBlankforUPC(i);
			}
			if(preprintStatus.equals("no")){
			    if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
				createBlankforUPC(i);
			}
		}
		
		createWorkOrderDeliveryNoteDetails();
		createCustomerDetails();
		createProductDetails();
		createProductDetailsVal();
		createFooterTaxPart();
		createFooterLastPart(preprintStatus);
		
		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	}
	
	

private void createFooterLastPart(String preprintStatus) {
	// TODO Auto-generated method stub
	PdfPTable bottomTable=new PdfPTable(2);
	bottomTable.setWidthPercentage(100);
	try {
		bottomTable.setWidths(columnMoreLeftWidths);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	PdfPTable leftTable=new PdfPTable(1);
	leftTable.setWidthPercentage(100);
	
	
	
//	String amtInWordsVal="Amount in Words : Rupees "+SalesInvoicePdf.convert(invoiceentity.getNetPayable());
	Phrase amtInWordsValphrase=new Phrase("",font10bold);
	
	PdfPCell amtInWordsValCell=new PdfPCell(amtInWordsValphrase);
	amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	amtInWordsValCell.setBorderWidthTop(0);
	amtInWordsValCell.setBorderWidthLeft(0);
	amtInWordsValCell.setBorderWidthRight(0);
	leftTable.addCell(amtInWordsValCell);
	
	
	Phrase termNcond=new Phrase("Terms and Conditions:",font7bold);
	PdfPCell termNcondCell=new PdfPCell();
	termNcondCell.setBorder(0);
	termNcondCell.addElement(termNcond);
	
	Phrase termNcondVal=new Phrase(workOrder.getWoDescription().trim(),font7bold);
	PdfPCell termNcondValCell=new PdfPCell();
	termNcondValCell.setBorder(0);
	termNcondValCell.addElement(termNcondVal);
	
	leftTable.addCell(termNcondCell);
	leftTable.addCell(termNcondValCell);
	
	//   rohan added this code for universal pest 
		if(!preprintStatus.equalsIgnoreCase("Plane")){
			for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
				
				if(comp.getArticleTypeDetails().get(i).getArticlePrint().equalsIgnoreCase("Yes")){
					
					Phrase articalType=new Phrase(comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue(),font10bold);
					PdfPCell articalTypeCell=new PdfPCell();
					articalTypeCell.setBorder(0);
					articalTypeCell.addElement(articalType);
					leftTable.addCell(articalTypeCell);
				}
			}
		}	
	PdfPCell leftCell=new PdfPCell();
	leftCell.addElement(leftTable);
	
	PdfPTable rightTable=new PdfPTable(1);
	rightTable.setWidthPercentage(100);
	
	PdfPTable innerRightTable=new PdfPTable(3);
	innerRightTable.setWidthPercentage(100);
	
	Phrase colon=new Phrase(" :",font7bold);
	PdfPCell colonCell=new PdfPCell();
	colonCell.setBorder(0);
	colonCell.addElement(colon);
	
	Phrase blank=new Phrase(" ",font7bold);
	PdfPCell blankCell=new PdfPCell();
	blankCell.setBorder(0);
	blankCell.addElement(blank);
	
	Phrase netPay=new Phrase("Net Payable",font7bold);
	
	PdfPCell netPayCell=new PdfPCell();
	netPayCell.setBorder(0);
	netPayCell.addElement(netPay);
	
	
	
	
	
	Phrase netPayVal=new Phrase("",font7bold);
//	Paragraph netPayPara=new Paragraph();
//	netPayPara.add(netPayVal);
//	netPayPara.setAlignment(Element.ALIGN_RIGHT);
	PdfPCell netPayValCell=new PdfPCell(netPayVal);
	netPayValCell.setBorder(0);
//	netPayValCell.addElement(netPayPara);
	netPayValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	
	Phrase gstReverseCharge=new Phrase("GST Payable on Reverse Charge",font6bold);
	PdfPCell gstReverseChargeCell=new PdfPCell();
	gstReverseChargeCell.setBorder(0);
	gstReverseChargeCell.addElement(gstReverseCharge);
	
	Phrase gstReverseChargeVal=new Phrase(" ",font7);
	PdfPCell gstReverseChargeValCell=new PdfPCell();
	gstReverseChargeValCell.setBorder(0);
	gstReverseChargeValCell.addElement(gstReverseChargeVal);
	
	innerRightTable.addCell(netPayCell);
	innerRightTable.addCell(colonCell);
	innerRightTable.addCell(netPayValCell);
	innerRightTable.addCell(blankCell);
	innerRightTable.addCell(blankCell);
	innerRightTable.addCell(blankCell);
	innerRightTable.addCell(gstReverseChargeCell);
	innerRightTable.addCell(colonCell);
	innerRightTable.addCell(gstReverseChargeValCell);
	
	PdfPCell rightUpperCell=new PdfPCell();
	rightUpperCell.addElement(innerRightTable);
	rightUpperCell.setBorderWidthLeft(0);
	rightUpperCell.setBorderWidthRight(0);
	rightUpperCell.setBorderWidthTop(0);
	
	String companyname = comp.getBusinessUnitName().trim().toUpperCase(); 
	
	
	rightTable.addCell(rightUpperCell);
	Phrase companyPhrase=new Phrase("For , "+companyname,font7bold);
	Paragraph companyPara=new Paragraph();
	companyPara.add(companyPhrase);
	companyPara.setAlignment(Element.ALIGN_CENTER);
	PdfPCell companyParaCell=new PdfPCell();
	companyParaCell.addElement(companyPara);
	companyParaCell.setBorder(0);
	
	rightTable.addCell(companyParaCell);
	rightTable.addCell(blankCell);
	rightTable.addCell(blankCell);
	rightTable.addCell(blankCell);
	Phrase signAuth=new Phrase("Authorised Signatory",font7bold);
	Paragraph signPara=new Paragraph();
	signPara.add(signAuth);
	signPara.setAlignment(Element.ALIGN_CENTER);
	PdfPCell signParaCell=new PdfPCell();
	signParaCell.addElement(signPara);
	signParaCell.setBorder(0);
	rightTable.addCell(signParaCell);
	
	PdfPCell lefttableCell=new PdfPCell();
	lefttableCell.addElement(leftTable);
	PdfPCell righttableCell=new PdfPCell();
	righttableCell.addElement(rightTable);
	
	bottomTable.addCell(lefttableCell);
	bottomTable.addCell(righttableCell);
	
	//
	
	Paragraph para = new Paragraph("Note : This is computer generated invoice no signature required.",font8);

	//
	try {
		document.add(bottomTable);
		document.add(para);
		if(workOrder.getWoTable().size() > 5 ){
			createAnnexureForRemainingProduct(5);
		}
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}



private void createAnnexureForRemainingProduct(int count) {

	Paragraph para = new Paragraph("Annexure 1 :", font10bold);
	para.setAlignment(Element.ALIGN_LEFT);

	try {
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEXTPAGE);
		document.add(para);
		document.add(Chunk.NEWLINE);

	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	createProductDetails();
	createProductDetailsMOreThanFive(count);
}

private void createProductDetails() {
	PdfPTable productTable=new PdfPTable(8);
	productTable.setWidthPercentage(100);
	try {
		productTable.setWidths(column13CollonWidth);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	Phrase srNophrase=new Phrase("Sr No",font7bold);
	PdfPCell srNoCell=new PdfPCell();
	srNoCell.addElement(srNophrase);
	srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	srNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	srNoCell.setRowspan(2); //1
	
	Phrase servicePhrase=new Phrase("Description of Goods",font7bold);
	PdfPCell servicePhraseCell=new PdfPCell(servicePhrase);
	 servicePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 servicePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	servicePhraseCell.setRowspan(2);//2
	
	Phrase hsnCode=new Phrase("HSN/SAC",font7bold);
	PdfPCell hsnCodeCell=new PdfPCell(hsnCode);
	hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	hsnCodeCell.setRowspan(2);//3
	
	Phrase UOMphrase=new Phrase("UOM",font7bold);
	PdfPCell UOMphraseCell=new PdfPCell(UOMphrase);
	UOMphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	UOMphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	UOMphraseCell.setRowspan(2);//
	
	Phrase qtyPhrase=new Phrase("Qty",font7bold);
	PdfPCell qtyPhraseCell=new PdfPCell(qtyPhrase);
	qtyPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	qtyPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	qtyPhraseCell.setRowspan(2);//5
	
	Phrase ratePhrase=new Phrase("Rate",font7bold);
	PdfPCell ratePhraseCell=new PdfPCell(ratePhrase);
	ratePhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	ratePhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	ratePhraseCell.setRowspan(2);//6
	
	Phrase amountPhrase=new Phrase("Amount",font7bold);
	PdfPCell amountPhraseCell=new PdfPCell(amountPhrase);
	amountPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	amountPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	amountPhraseCell.setRowspan(2);//7
	
	Phrase dicphrase=new Phrase("Disc %",font7bold);
	PdfPCell dicphraseCell=new PdfPCell(dicphrase);
	dicphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	dicphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	dicphraseCell.setRowspan(2);//8
	
//	Phrase taxValPhrase=new Phrase("Ass Val",font7bold);
//	PdfPCell taxValPhraseCell=new PdfPCell(taxValPhrase);
//	taxValPhraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	taxValPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	taxValPhraseCell.setRowspan(2);//9
//	
////	PdfPTable cgstcellTable=new PdfPTable(1);
////	cgstcellTable.setWidthPercentage(100);
//	
//	Phrase cgstphrase=new Phrase("CGST",font7bold);
//	PdfPCell cgstphraseCell=new PdfPCell(cgstphrase);
////	cgstphraseCell.addElement(cgstphrase);
////	cgstphraseCell.setBorder(0);
//	cgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	cgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
////	cgstcellTable.addCell(cgstphraseCell);
//	cgstphraseCell.setColspan(1);
////	cgstphraseCell.setRowspan(2);
//	
//	Phrase sgstphrase=new Phrase("SGST",font7bold);
//	PdfPCell sgstphraseCell=new PdfPCell(sgstphrase);
////	sgstphraseCell.setBorder(0);
//	sgstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	sgstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
////	sgstphraseCell.addElement(sgstphrase);
////	sgstcellTable.addCell(sgstphraseCell);
//	sgstphraseCell.setColspan(1);
////	sgstphraseCell.setRowspan(2);
//	
//	Phrase igstphrase=new Phrase("IGST",font7bold);
//	PdfPCell igstphraseCell=new PdfPCell(igstphrase);
////	igstphraseCell.setBorder(0);
//	igstphraseCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	igstphraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
////	igstphraseCell.addElement(igstphrase);
////	igstcellTable.addCell(igstphraseCell);
//	igstphraseCell.setColspan(1);
////	igstphraseCell.setRowspan(2);
//	
//	Phrase totalPhrase=new Phrase("Total",font7bold);
//	PdfPCell totalPhraseCell=new PdfPCell(totalPhrase);
//	totalPhraseCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	totalPhraseCell.setRowspan(2);//2
//	
//	
//	Phrase cgstpercentphrase=new Phrase("%",font6bold);
//	PdfPCell cgstpercentphraseCell=new PdfPCell(cgstpercentphrase);
////	cgstpercentphraseCell.setBorderWidthBottom(0);
////	cgstpercentphraseCell.setBorderWidthTop(0);
////	cgstpercentphraseCell.setBorderWidthLeft(0);
////	cgstpercentphraseCell.addElement();
////	innerCgstTable.addCell(cgstpercentphraseCell);
//	
////	Phrase cgstamtphrase=new Phrase("Amt",font6bold);
////	PdfPCell cgstamtphraseCell=new PdfPCell();
////	cgstamtphraseCell.addElement(cgstamtphrase);
////	
	
	
	productTable.addCell(srNoCell);
	productTable.addCell(servicePhraseCell);
	productTable.addCell(hsnCodeCell);
	productTable.addCell(qtyPhraseCell);
	productTable.addCell(UOMphraseCell);
	productTable.addCell(ratePhraseCell);
	productTable.addCell(dicphraseCell);
	productTable.addCell(amountPhraseCell);
	
//	productTable.addCell(taxValPhraseCell);
//	
//	productTable.addCell(cgstphraseCell);
//	productTable.addCell(sgstphraseCell);
//	productTable.addCell(igstphraseCell);
//	
//	productTable.addCell(totalPhraseCell);
//	
//	productTable.addCell(cgstpercentphraseCell);
////	productTable.addCell(cgstamtphraseCell);
//	productTable.addCell(cgstpercentphraseCell);
////	productTable.addCell(cgstamtphraseCell);
//	productTable.addCell(cgstpercentphraseCell);
////	productTable.addCell(cgstamtphraseCell);

	try {
		document.add(productTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		logger.log(Level.SEVERE,"Error::"+e);
	}
}


private void createProductDetailsVal(){

	int firstBreakPoint = 4;
	float blankLines = 0;
	double totalAmount=0;
	if (workOrder.getWoTable().size() <= firstBreakPoint) {
		int size = firstBreakPoint
				- workOrder.getWoTable().size();
		blankLines = size * (100 / 5);
		System.out.println("blankLines size =" + blankLines);
	} else {
		blankLines = 10f;
	}
	
	PdfPTable productTable = new PdfPTable(8);
	productTable.setWidthPercentage(100);
	try {
		productTable.setWidths(column13CollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	
	double totalQty = 0;
	
	for (int i = 0; i < workOrder.getWoTable().size(); i++) {

		if (i == 4) {
			break;
		}

//		totalQty = totalQty+workOrder.getBomTable()
		
		totalQty = totalQty + workOrder.getWoTable().get(i).getProductQty();
		
		int srNoVal = i + 1;
		Phrase srNo = new Phrase(srNoVal + "", font7);
		PdfPCell srNoCell = new PdfPCell(srNo);
//		srNoCell.addElement();
		productTable.addCell(srNoCell);

//		Phrase serviceName = new Phrase(workOrder.getWoTable().get(i).getProductName().trim(),
//				font7);
		System.out.println("PRod description length =="+workOrder.getWoTable().get(i).getProdDescription().trim().length());
		
		String productName= workOrder.getWoTable().get(i).getProductName().trim();
		if(workOrder.getWoTable().get(i).getProdDescription().trim()!=null){
			productName = productName+"\n"+workOrder.getWoTable().get(i).getProdDescription().trim();
		}
		
		Phrase serviceName = new Phrase(productName,
				font7);
		PdfPCell serviceNameCell = new PdfPCell();
		serviceNameCell.addElement(serviceName);
		productTable.addCell(serviceNameCell);

		ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId", workOrder.getCompanyId()).filter("productCode", workOrder.getWoTable().get(i).getProductCode()).first().now();
		Phrase hsnCode = null;
		if (itemproduct.getHsnNumber() != null) {
			hsnCode = new Phrase(itemproduct.getHsnNumber().trim(), font7);
		} else {
			hsnCode = new Phrase("", font7);
		}
		PdfPCell hsnCodeCell = new PdfPCell();
		hsnCodeCell.addElement(hsnCode);
		hsnCodeCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		hsnCodeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		productTable.addCell(hsnCodeCell);
		
		Phrase qty = new Phrase(workOrder.getWoTable()
				.get(i).getProductQty()
				+ "", font7);
		PdfPCell qtyCell = new PdfPCell();
		qtyCell.addElement(qty);
		qtyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
//		qtyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		productTable.addCell(qtyCell);
		
		Phrase uom ;
//		if(delnoteEntity.getDeliveryItems()
//				.get(i).getUnitOfMeasurement().trim()!=null){
//			uom = new Phrase(delnoteEntity.getDeliveryItems()
//					.get(i).getUnitOfMeasurement().trim(), font10);
//		}else{
			uom = new Phrase("", font7);
//		}
		
		PdfPCell uomCell = new PdfPCell();
		uomCell.addElement(uom);
		productTable.addCell(uomCell);

		

//		Phrase rate = new Phrase(df.format(delnoteEntity.getDeliveryItems().get(i).getPrice())
//				+ "", font10);
		Phrase rate = new Phrase("", font7);
		PdfPCell rateCell = new PdfPCell();
		rateCell.addElement(rate);
		productTable.addCell(rateCell);

		Phrase disc = new Phrase("", font7);
		PdfPCell discCell = new PdfPCell();
		// discCell.setBorder(0);
		// discCell.setBorderWidthBottom(0);
		// discCell.setBorderWidthTop(0);
		discCell.addElement(disc);
		productTable.addCell(discCell);
		
//		double amountValue = workOrder.getBomTable().get(i)
//				.getPrice()
//				* workOrder.getBomTable().get(i)
//						.getQty();
//		totalAmount = totalAmount + amountValue;
////		Phrase amount = new Phrase(df.format(amountValue) + "", font10);
		Phrase amount = new Phrase("", font7);
		PdfPCell amountCell = new PdfPCell();
		// amountCell.setBorder(0);
		// amountCell.setBorderWidthBottom(0);
		// amountCell.setBorderWidthTop(0);
		amountCell.addElement(amount);
		productTable.addCell(amountCell);

	}
	
	Phrase blankCell = new Phrase(" ", font7);
	PdfPCell productTableCell = null;
	if (workOrder.getWoTable().size() > firstBreakPoint) {
		Phrase my = new Phrase("Please Refer Annexure For More Details");
		productTableCell = new PdfPCell(my);

	} else {
		productTableCell = new PdfPCell(blankCell);
	}

	// PdfPCell productTableCell = new PdfPCell(blankCell);
	// productTableCell.setBorderWidthBottom(0);
	// productTableCell.setBorderWidthTop(0);
	productTableCell.setBorder(0);

	PdfPTable tab = new PdfPTable(1);
	tab.setWidthPercentage(100f);
	tab.addCell(productTableCell);
	// tab.addCell(premiseTblCell);
	tab.setSpacingAfter(blankLines);

	// last code for both table to be added in one table

	PdfPCell tab1;
	tab1 = new PdfPCell(productTable);
	tab1.setHorizontalAlignment(Element.ALIGN_CENTER);
	// tab1.setBorder(0);

	PdfPCell tab2 = new PdfPCell(tab);
	// tab2.setBorder(0);

	PdfPTable mainTable = new PdfPTable(1);
	mainTable.setWidthPercentage(100f);
	mainTable.addCell(tab1);
	mainTable.addCell(tab2);

	
	PdfPTable productTotalTable = new PdfPTable(8);
	productTotalTable.setWidthPercentage(100);
	
	Phrase blank = new Phrase(" ");
	PdfPCell blankcell = new PdfPCell(blank);
	blankcell.setBorderWidthRight(0);
	blankcell.setBorderWidthLeft(1);
	blankcell.setBorderWidthTop(0);
	blankcell.setBorderWidthBottom(0);
	
	Phrase blank2 = new Phrase(" ");
	PdfPCell blankcell2 = new PdfPCell(blank2);
	blankcell2.setBorder(0);
	
	Phrase blank3 = new Phrase(" ");
	PdfPCell blankcell3 = new PdfPCell(blank3);
	blankcell3.setBorderWidthRight(1);
	blankcell3.setBorderWidthLeft(0);
	blankcell3.setBorderWidthTop(0);
	blankcell3.setBorderWidthBottom(0);
	
	productTotalTable.addCell(blankcell);
	productTotalTable.addCell(blankcell2);

	Phrase total = new Phrase("TOTAL",font7);
	PdfPCell totalcell = new PdfPCell(total);
	totalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	totalcell.setBorder(0);
	
	productTotalTable.addCell(totalcell);
	
	System.out.println("Qty =="+totalQty);
	Phrase totalValue = new Phrase(totalQty+"",font7);
	PdfPCell totalValueCell = new PdfPCell(totalValue);
	totalValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	totalValueCell.setBorder(0);
	productTotalTable.addCell(totalValueCell);
	
	productTotalTable.addCell(blankcell2);
	productTotalTable.addCell(blankcell2);
	productTotalTable.addCell(blankcell2);
	productTotalTable.addCell(blankcell3);

	
	try {
		productTotalTable.setWidths(column13CollonWidth);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	try {
		document.add(mainTable);
		document.add(productTotalTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}

private void createProductDetailsMOreThanFive(int count) {
	
	PdfPTable productTable = new PdfPTable(8);
	productTable.setWidthPercentage(100);
	
	double totalQty =0;
		for (int i = count; i < workOrder.getWoTable().size(); i++) {

			try {
				productTable.setWidths(column13CollonWidth);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			totalQty += workOrder.getWoTable().get(i).getProductQty();
			
			int srNoVal = i + 1;
			Phrase srNo = new Phrase(srNoVal + "", font10);
			PdfPCell srNoCell = new PdfPCell();
			srNoCell.addElement(srNo);
			productTable.addCell(srNoCell);

			Phrase serviceName = new Phrase(workOrder.getWoTable()
					.get(i).getProductName().trim(), font10);
			PdfPCell serviceNameCell = new PdfPCell();
			serviceNameCell.addElement(serviceName);
			productTable.addCell(serviceNameCell);

			ItemProduct itemproduct = ofy().load().type(ItemProduct.class).filter("companyId", workOrder.getCompanyId()).filter("productCode", workOrder.getWoTable().get(i).getProductCode()).first().now();

			Phrase hsnCode = null;
			if (itemproduct.getHsnNumber() != null) {
				hsnCode = new Phrase(itemproduct.getHsnNumber().trim(), font10);
			} else {
				hsnCode = new Phrase("", font10);
			}
			PdfPCell hsnCodeCell = new PdfPCell();
			hsnCodeCell.addElement(hsnCode);
			productTable.addCell(hsnCodeCell);
			
			Phrase qty = new Phrase(workOrder.getWoTable().get(i)
					.getProductQty()
					+ "", font10);
			PdfPCell qtyCell = new PdfPCell();
			qtyCell.addElement(qty);
			productTable.addCell(qtyCell);
			
			Phrase uom;
			// if(delnoteEntity.getDeliveryItems()
			// .get(i).getUnitOfMeasurement().trim()!=null){
			// uom = new Phrase(delnoteEntity.getDeliveryItems()
			// .get(i).getUnitOfMeasurement().trim(), font10);
			// }else{
			uom = new Phrase("", font10);
			// }

			PdfPCell uomCell = new PdfPCell();
			uomCell.addElement(uom);
			productTable.addCell(uomCell);

			

			// Phrase rate = new
			// Phrase(df.format(delnoteEntity.getDeliveryItems().get(i).getPrice())
			// + "", font10);
			Phrase rate = new Phrase("", font10);
			PdfPCell rateCell = new PdfPCell();
			rateCell.addElement(rate);
			productTable.addCell(rateCell);

//			double amountValue = workOrder.getBomTable().get(i)
//					.getPrice()
//					* workOrder.getBomTable().get(i).getQty();
////			totalAmount = totalAmount + amountValue;
//			// Phrase amount = new Phrase(df.format(amountValue) + "", font10);
			

			// Phrase disc = new Phrase(df.format(invoiceentity
			// .getSalesOrderProducts().get(i).getFlatDiscount())
			// + "", font10);
			Phrase disc = new Phrase("", font10);
			PdfPCell discCell = new PdfPCell();
			// discCell.setBorder(0);
			// discCell.setBorderWidthBottom(0);
			// discCell.setBorderWidthTop(0);
			discCell.addElement(disc);
			productTable.addCell(discCell);

			Phrase amount = new Phrase("", font10);
			PdfPCell amountCell = new PdfPCell();
			// amountCell.setBorder(0);
			// amountCell.setBorderWidthBottom(0);
			// amountCell.setBorderWidthTop(0);
			amountCell.addElement(amount);
			productTable.addCell(amountCell);
			
				
			}
		
		
		PdfPTable productTotalTable = new PdfPTable(8);
		productTotalTable.setWidthPercentage(100);
		
		Phrase blank = new Phrase(" ");
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorderWidthRight(0);
		blankcell.setBorderWidthLeft(1);
		blankcell.setBorderWidthTop(0);
//		blankcell.setBorderWidthBottom(0);
		
		Phrase blank2 = new Phrase(" ");
		PdfPCell blankcell2 = new PdfPCell(blank2);
		blankcell2.setBorder(0);
		blankcell2.setBorderWidthBottom(1);
		
		Phrase blank3 = new Phrase(" ");
		PdfPCell blankcell3 = new PdfPCell(blank3);
		blankcell3.setBorderWidthRight(1);
		blankcell3.setBorderWidthLeft(0);
		blankcell3.setBorderWidthTop(0);
//		blankcell3.setBorderWidthBottom(0);
		
		productTotalTable.addCell(blankcell);
		productTotalTable.addCell(blankcell2);

		Phrase total = new Phrase("TOTAL",font7);
		PdfPCell totalcell = new PdfPCell(total);
		totalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalcell.setBorder(0);
		totalcell.setBorderWidthBottom(1);
		
		productTotalTable.addCell(totalcell);
		
		System.out.println("Qty =="+totalQty);
		Phrase totalValue = new Phrase(totalQty+"",font7);
		PdfPCell totalValueCell = new PdfPCell(totalValue);
		totalValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		totalValueCell.setBorder(0);
		totalValueCell.setBorderWidthBottom(1);
		
		productTotalTable.addCell(totalValueCell);
		
		productTotalTable.addCell(blankcell2);
		productTotalTable.addCell(blankcell2);
		productTotalTable.addCell(blankcell2);
		productTotalTable.addCell(blankcell3);

		
		try {
			productTotalTable.setWidths(column13CollonWidth);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			document.add(productTable);
			document.add(productTotalTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
}

	private void createFooterTaxPart(){

		// TODO Auto-generated method stub
		PdfPTable pdfPTaxTable=new PdfPTable(2);
		pdfPTaxTable.setWidthPercentage(100);
		try {
			pdfPTaxTable.setWidths(columnMoreLeftWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/**
		 * rohan added this code for payment terms for invoice details
		 */
		
		float[] column3widths = {2f,2f,6f};
		PdfPTable leftTable=new PdfPTable(3);
		leftTable.setWidthPercentage(100);
		try {
			leftTable.setWidths(column3widths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PdfPTable rightTable=new PdfPTable(1);
		rightTable.setWidthPercentage(100);
		
		PdfPTable rightInnerTable=new PdfPTable(1);
		rightInnerTable.setWidthPercentage(100);
//		try {
//			rightInnerTable.setWidths(columnCollonGSTWidth);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		Phrase colon=new Phrase(":",font7bold);
		PdfPCell colonCell=new PdfPCell();
		colonCell.addElement(colon);
		colonCell.setBorder(0);
		
		Phrase amtB4Taxphrase=new Phrase("Total Amount Before Tax",font7bold);
		PdfPCell amtB4TaxCell=new PdfPCell();
		amtB4TaxCell.setBorder(0);
		amtB4TaxCell.addElement(amtB4Taxphrase);
		
		Phrase amtB4TaxValphrase=new Phrase(" ",font7bold);
		PdfPCell amtB4ValTaxCell=new PdfPCell();
		amtB4ValTaxCell.setBorder(0);
		amtB4ValTaxCell.addElement(amtB4TaxValphrase);
		
		double cgstTotalVal=0,sgstTotalVal=0,igstTotalVal=0;
		
		String SGST="";
		String CGST ="";
		String IGST ="";
		for(int i=0;i<salesOrderEntity.getItems().size();i++){
			System.out.println("Hi vijay tax number ==");
			System.out.println(salesOrderEntity.getItems().get(i).getServiceTaxEdit());
			System.out.println(salesOrderEntity.getItems().get(i).getVatTaxEdit());
			if(salesOrderEntity.getItems().get(i).getVatTaxEdit()!=null && !salesOrderEntity.getItems().get(i).getVatTaxEdit().equalsIgnoreCase("NA") ||
					salesOrderEntity.getItems().get(i).getServiceTaxEdit()!=null && !salesOrderEntity.getItems().get(i).getServiceTaxEdit().equalsIgnoreCase("NA") ){
				
			
			if(salesOrderEntity.getItems().get(i).getVatTaxEdit().contains("SGST") || salesOrderEntity.getItems().get(i).getVatTaxEdit().contains("CGST")){
				SGST = "Sales "+ salesOrderEntity.getItems().get(i).getVatTaxEdit()+"%";
			}
			if(salesOrderEntity.getItems().get(i).getServiceTaxEdit().contains("CGST") || salesOrderEntity.getItems().get(i).getServiceTaxEdit().contains("SGST") ){
				CGST = "Sales "+salesOrderEntity.getItems().get(i).getServiceTaxEdit()+"%";
			}
			if(salesOrderEntity.getItems().get(i).getVatTaxEdit().contains("IGST") || salesOrderEntity.getItems().get(i).getServiceTaxEdit().contains("IGST")){
				if(salesOrderEntity.getItems().get(i).getVatTaxEdit().contains("IGST")){
					IGST = "Sales "+salesOrderEntity.getItems().get(i).getVatTaxEdit()+"%";
				}else{
					IGST = "Sales "+ salesOrderEntity.getItems().get(i).getServiceTaxEdit()+"%";
				}
			}
			
			}
		}
		
		System.out.println("Hi vijay GST Valuesss---"+SGST);
		System.out.println("Hi vijay GST Valuesss---"+CGST);
		System.out.println("Hi vijay GST Valuesss---"+IGST);

		
		Phrase CGSTphrase=new Phrase(SGST,font7bold);
		PdfPCell CGSTphraseCell=new PdfPCell(CGSTphrase);
		CGSTphraseCell.setBorder(0);
//		CGSTphraseCell.addElement(CGSTphrase);
		
		Phrase CGSTValphrase=new Phrase(df.format(cgstTotalVal)+"",font7);
//		Paragraph CGSTValphrasePara=new Paragraph();
//		CGSTValphrasePara.add(CGSTValphrase);
//		CGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell CGSTValphraseCell=new PdfPCell(CGSTValphrase);
		CGSTValphraseCell.setBorder(0);
		CGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		CGSTValphraseCell.addElement(CGSTValphrasePara);

		Phrase SGSTphrase=new Phrase(CGST,font7bold);
		PdfPCell SGSTphraseCell=new PdfPCell(SGSTphrase);
		SGSTphraseCell.setBorder(0);
//		SGSTphraseCell.addElement(SGSTphrase);
		
		Phrase SGSTValphrase=new Phrase(df.format(sgstTotalVal)+"",font7);
//		Paragraph SGSTValphrasePara=new Paragraph();
//		SGSTValphrasePara.add(SGSTValphrase);
//		SGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell SGSTValphraseCell=new PdfPCell(SGSTValphrase);
		SGSTValphraseCell.setBorder(0);
//		SGSTValphraseCell.addElement(SGSTValphrasePara);
		SGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Phrase IGSTphrase=new Phrase(IGST,font7bold);
		PdfPCell IGSTphraseCell=new PdfPCell(IGSTphrase);
		IGSTphraseCell.setBorder(0);
//		IGSTphraseCell.addElement(IGSTphrase);
		
		Phrase IGSTValphrase=new Phrase(df.format(igstTotalVal)+"",font7);
//		Paragraph IGSTValphrasePara=new Paragraph();
//		IGSTValphrasePara.add(IGSTValphrase);
//		IGSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell IGSTValphraseCell=new PdfPCell(IGSTValphrase);
		IGSTValphraseCell.setBorder(0);
		IGSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		IGSTValphraseCell.addElement(IGSTValphrasePara);

		Phrase GSTphrase=new Phrase("Total GST",font7bold);
		PdfPCell GSTphraseCell=new PdfPCell(GSTphrase);
		GSTphraseCell.setBorder(0);
//		GSTphraseCell.addElement(GSTphrase);
		
		double totalGSTValue=igstTotalVal+cgstTotalVal+cgstTotalVal;
		Phrase GSTValphrase=new Phrase(df.format(totalGSTValue)+"",font7bold);
//		Paragraph GSTValphrasePara=new Paragraph();
//		GSTValphrasePara.add(GSTValphrase);
//		GSTValphrasePara.setAlignment(Element.ALIGN_RIGHT);
		PdfPCell GSTValphraseCell=new PdfPCell(GSTValphrase);
		GSTValphraseCell.setBorder(0);
		GSTValphraseCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		GSTValphraseCell.addElement(GSTValphrasePara);
		
		rightInnerTable.addCell(CGSTphraseCell);
//		rightInnerTable.addCell(colonCell);
//		rightInnerTable.addCell(CGSTValphraseCell);
		rightInnerTable.addCell(SGSTphraseCell);
//		rightInnerTable.addCell(colonCell);
//		rightInnerTable.addCell(SGSTValphraseCell);
		rightInnerTable.addCell(IGSTphraseCell);
//		rightInnerTable.addCell(colonCell);
//		rightInnerTable.addCell(IGSTValphraseCell);
//		rightInnerTable.addCell(GSTphraseCell);
//		rightInnerTable.addCell(colonCell);
//		rightInnerTable.addCell(GSTValphraseCell);
		
		PdfPCell innerRightCell=new PdfPCell();
		innerRightCell.setBorder(0);
		innerRightCell.addElement(rightInnerTable);
		
		rightTable.addCell(innerRightCell);
		
		PdfPCell rightCell=new PdfPCell();
//		rightCell.setBorder(0);
		rightCell.addElement(rightTable);
		
		PdfPCell leftCell=new PdfPCell();
//		leftCell.setBorder(0);
		leftCell.addElement(leftTable);
		
		pdfPTaxTable.addCell(leftCell);
		pdfPTaxTable.addCell(rightCell);
		
		try {
			document.add(pdfPTaxTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

private void createCustomerDetails() {
	// TODO Auto-generated method stub
	PdfPTable mainTable=new PdfPTable(2);
	mainTable.setWidthPercentage(100);
	try {
		mainTable.setWidths(columnHalfWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	/*Start Part 1*/
	PdfPTable part1Table=new PdfPTable(1);
	part1Table.setWidthPercentage(100);
	
	Phrase colon=new Phrase(":",font7bold);
	PdfPCell colonCell=new PdfPCell(colon);
//	colonCell.addElement(colon);
	colonCell.setBorder(0);
	colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Phrase name=new Phrase("Name",font7bold);
	PdfPCell nameCell=new PdfPCell(name);
	nameCell.setBorder(0);
	nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	//  rohan added this code for considering customer printable name as well Date : 04-07-2017
	
	String tosir= null;
	String custName="";
	//   rohan modified this code for printing printable name 
	/**
	 * old code
	 */
//	if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
//	{
//		custName=cust.getCustPrintableName().trim();
//	}
//	else
//	{
//	
////		if(cust.isCompany()==true){
////			
////			
////		 tosir="M/S";
////		}
////		else{
////			tosir="";
////		}
//		
//		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//			custName="M/S"+cust.getCompanyName().trim();
//		}
//		else{
//			custName=cust.getFullname().trim();
//		}
//	}
	
	//Date 21/11/2017
		//Dev.By jayshree
		//Des.To add the salutation for customer name
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
//			if(cust.isCompany()==true){
//				
//				
//			 tosir="M/S";
//			}
//			else{
//				tosir="";
//			}
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName="M/S "+" "+cust.getCompanyName().trim();
			}
			else if(cust.getSalutation()!=null)
			{
				custName=cust.getSalutation()+" "+cust.getFullname().trim();
			}
			else
			{
				custName=cust.getFullname().trim();
			}
		}
		//End by jayshree
	
	String fullname= "";
	 if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
	   	{
	       	fullname = custName;
	   	}
	       else
	       {
	       	fullname =custName;
	       }
	//   ends here 
	
	Phrase nameCellVal=new Phrase(fullname +"       Mob : "+cust.getCellNumber1(),font7);
	PdfPCell nameCellValCell=new PdfPCell(nameCellVal);
//	nameCellValCell.addElement(nameCellVal);
	nameCellValCell.setBorder(0);
	nameCellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase address=new Phrase("Address",font7bold);
	PdfPCell addressCell=new PdfPCell(address);
	addressCell.setBorder(0);
	addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	System.out.println("Rohan cust "+cust.getName());

	String adrsValString = cust.getAdress().getCompleteAddress().trim();
	Phrase addressVal=new Phrase(adrsValString,font7);
	PdfPCell addressValCell=new PdfPCell(addressVal);
	addressValCell.setBorder(0);
	addressValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String gstTinStr="";
	for (int j = 0; j < cust.getArticleTypeDetails().size(); j++) {
		if(cust.getArticleTypeDetails().get(j).getArticleTypeName().equalsIgnoreCase("GSTIN")){
			gstTinStr=cust.getArticleTypeDetails().get(j).getArticleTypeValue().trim();
		}
	}
	
	
	Phrase gstTin=new Phrase("GSTIN",font7bold);
	PdfPCell gstTinCell=new PdfPCell(gstTin);
//	gstTinCell.addElement(gstTin);
	gstTinCell.setBorder(0);
	gstTinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase gstTinVal=new Phrase(gstTinStr,font7);
	PdfPCell gstTinValCell=new PdfPCell(gstTinVal);
//	gstTinValCell.addElement(gstTinVal);
	gstTinValCell.setBorder(0);
	gstTinValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	colonTable.addCell(nameCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(nameCellValCell);
	colonTable.addCell(addressCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(addressValCell);
	colonTable.addCell(gstTinCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(gstTinValCell);
	
	PdfPCell cell1=new PdfPCell();
	cell1.setBorder(0);
	cell1.addElement(colonTable);
	
	Phrase state=new Phrase("State",font7bold);
	PdfPCell stateCell=new PdfPCell(state);
//	stateCell.addElement(state);
	stateCell.setBorder(0);
	stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateVal=new Phrase(":   "+cust.getAdress().getState().trim(),font7);
	PdfPCell stateValCell=new PdfPCell(stateVal);
//	stateValCell.addElement(stateVal);
	stateValCell.setBorder(0);
	stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String stCo="";
	for(int i=0;i<stateList.size();i++){
		if(stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getAdress().getState().trim()))
		{
			stCo=stateList.get(i).getStateCode().trim();
			break;
		}
	}
	
	Phrase stateCode=new Phrase("State Code",font7bold);
	PdfPCell stateCodeCell=new PdfPCell(stateCode);
//	stateCodeCell.addElement(stateCode);
	stateCodeCell.setBorder(0);
	stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stateCodeVal=new Phrase(stCo,font7);
	PdfPCell stateCodeValCell=new PdfPCell(stateCodeVal);
//	stateCodeValCell.addElement(stateCodeVal);
//	stateCodeValCell.setBorder(0);
	stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable statetable=new PdfPTable(2);
	statetable.setWidthPercentage(100);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(stateCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(stateValCell);
	
	PdfPCell colonTableState=new PdfPCell();
	colonTableState.setBorder(0);
	colonTableState.addElement(colonTable);
	statetable.addCell(colonTableState);
	
	colonTable=new PdfPTable(4);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnStateCodeCollonWidth);
	} catch (DocumentException e)
	{
		e.printStackTrace();
	}
	Phrase blak= new Phrase(" ", font8);
	PdfPCell blakCell = new PdfPCell(blak);
	blakCell.setBorder(0);
	
	colonTable.addCell(stateCodeCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(stateCodeValCell);
	colonTable.addCell(blakCell);
	
	PdfPCell colonTableValState=new PdfPCell();
	colonTableValState.setBorder(0);
	colonTableValState.addElement(colonTable);
	statetable.addCell(colonTableValState);
	
	PdfPCell stateTableCell=new PdfPCell(statetable);
	stateTableCell.setBorder(0);
	stateTableCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	stateTableCell.addElement(statetable);
	
	part1Table.addCell(cell1);
	part1Table.addCell(stateTableCell);
	
	PdfPCell part1TableCell=new PdfPCell();
	part1TableCell.addElement(part1Table);
	
	/*Ends Part 1*/
	
	/*Part 2 Start*/
	PdfPTable part2Table=new PdfPTable(1);
	part2Table.setWidthPercentage(100);

	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	Phrase name2=new Phrase("Name",font7bold);
	PdfPCell name2Cell=new PdfPCell();
	name2Cell.addElement(name2);
	name2Cell.setBorder(0);
	name2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase name2CellVal=new Phrase(fullname +"       Mob : "+cust.getCellNumber1(),font7);
	PdfPCell name2CellValCell=new PdfPCell(name2CellVal);
//	name2CellValCell.addElement(name2CellVal);
	name2CellValCell.setBorder(0);
	name2CellValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase address2=new Phrase("Address",font7bold);
	PdfPCell address2Cell=new PdfPCell(address2);
//	address2Cell.addElement(address2);
	address2Cell.setBorder(0);
	address2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	///////////////////////////  Ajinkya added branch code for customer Branch On Date : 15/07/2017  //////////////////////////////
	String adrsValString1 = "";
	
	if(!salesOrderEntity.getShippingAddress().getAddrLine1().equals("")){
		adrsValString1 = salesOrderEntity.getShippingAddress().getCompleteAddress().trim();
	}
	
	Phrase address2Val=new Phrase(adrsValString1,font7);
	PdfPCell address2ValCell=new PdfPCell(address2Val);
	address2ValCell.setBorder(0);
	address2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	String gstTin2Str="";
	for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
		if(cust.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
			gstTin2Str=cust.getArticleTypeDetails().get(i).getArticleTypeName().trim();
		}
	}
	
	
	Phrase gstTin2=new Phrase("GSTIN",font7bold);
	PdfPCell gstTin2Cell=new PdfPCell(gstTin2);
	gstTin2Cell.setBorder(0);
	gstTin2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase gstTin2Val=new Phrase(gstTin2Str,font7);
	PdfPCell gstTin2ValCell=new PdfPCell(gstTin2Val);
	gstTin2ValCell.setBorder(0);
	gstTin2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	colonTable.addCell(nameCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(nameCellValCell);
	colonTable.addCell(addressCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(address2ValCell);
	colonTable.addCell(gstTinCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(gstTinValCell);
	
	PdfPCell cell2=new PdfPCell();
	cell2.setBorder(0);
	cell2.addElement(colonTable);
	
	Phrase state2=new Phrase("State",font7bold);
	PdfPCell state2Cell=new PdfPCell(state2);
//	state2Cell.addElement(state2);
	state2Cell.setBorder(0);
	state2Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String add="";
	if(!salesOrderEntity.getShippingAddress().getState().equals("")){
		add = salesOrderEntity.getShippingAddress().getState().trim();
	}
	else{
		add = cust.getSecondaryAdress().getState().trim();
	}
	
	Phrase state2Val=new Phrase(":   "+add,font7);
	PdfPCell state2ValCell=new PdfPCell(state2Val);
//	state2ValCell.addElement(state2Val);
	state2ValCell.setBorder(0);
	state2ValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	String stsecCo="";
	for(int i=0;i<stateList.size();i++){
		if(stateList.get(i).getStateName().trim().equalsIgnoreCase(cust.getSecondaryAdress().getState().trim()))
		{
			stsecCo=stateList.get(i).getStateCode().trim();
			break;
		}
	}
	
	Phrase state2Code=new Phrase("State Code",font7bold);
	PdfPCell state2CodeCell=new PdfPCell(state2Code);
//	state2CodeCell.addElement(state2Code);
	state2CodeCell.setBorder(0);
	state2CodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase state2CodeVal=new Phrase(stsecCo,font7);
	PdfPCell state2CodeValCell=new PdfPCell(state2CodeVal);
//	state2CodeValCell.addElement(state2CodeVal);
//	state2CodeValCell.setBorder(0);
	state2CodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	PdfPTable state2table=new PdfPTable(2);
	state2table.setWidthPercentage(100);
	
	colonTable=new PdfPTable(3);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(state2Cell);
	colonTable.addCell(colonCell);
	colonTable.addCell(state2ValCell);
	PdfPCell state2ValcolonTableCell =new PdfPCell();
	state2ValcolonTableCell.setBorder(0);
	state2ValcolonTableCell.addElement(colonTable);
	state2table.addCell(state2ValcolonTableCell);
	
	colonTable=new PdfPTable(4);
	colonTable.setWidthPercentage(100);
	try {
		colonTable.setWidths(columnStateCodeCollonWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	colonTable.addCell(state2CodeCell);
	colonTable.addCell(colonCell);
	colonTable.addCell(state2CodeValCell);
	colonTable.addCell(blakCell);
	
	state2ValcolonTableCell =new PdfPCell();
	state2ValcolonTableCell.setBorder(0);
	state2ValcolonTableCell.addElement(colonTable);
	state2table.addCell(state2ValcolonTableCell);
	
	PdfPCell state2TableCell=new PdfPCell(state2table);
	state2TableCell.setBorder(0);
//	state2TableCell.addElement(state2table);
	
	part2Table.addCell(cell2);
	part2Table.addCell(state2TableCell);
	
	PdfPCell part2TableCell=new PdfPCell(part2Table);
//	part2TableCell.addElement(part2Table);
	/*Part 2 Ends*/
	mainTable.addCell(part1TableCell);
	mainTable.addCell(part2TableCell);
	
	Phrase my = new Phrase("",font8);
	PdfPCell blankCell= new PdfPCell(my);
	mainTable.addCell(blankCell);
	mainTable.addCell(blankCell);
	
	try {
		document.add(mainTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}

	private void createWorkOrderDeliveryNoteDetails() {
		
		float[] columnHalfWidth = {1f,1f};
		// TODO Auto-generated method stub
		PdfPTable mainTable=new PdfPTable(2);
		mainTable.setWidthPercentage(100);
		try {
			mainTable.setWidths(columnHalfWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		PdfPTable part1Table=new PdfPTable(1);
		part1Table.setWidthPercentage(100);
		float[] columnrohanCollonWidth = {3.5f,0.2f,6.8f};
		PdfPTable colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnrohanCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Phrase colon=new Phrase(":",font7bold);
		PdfPCell colonCell=new PdfPCell(colon);
//		colonCell.addElement(colon);
		colonCell.setBorder(0);
		colonCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase reverseCharge=new Phrase("Reverse Charge(Y/N)",font7bold);
		PdfPCell reverseChargeCell=new PdfPCell(reverseCharge);
//		reverseChargeCell.addElement(reverseCharge);
		reverseChargeCell.setBorder(0);
		reverseChargeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase reverseChargeVal=new Phrase("No ",font7);
		PdfPCell reverseChargeValCell=new PdfPCell(reverseChargeVal);
//		reverseChargeValCell.addElement(reverseChargeVal);
		reverseChargeValCell.setBorder(0);
		reverseChargeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceNo=new Phrase("Delivery Note Id",font7bold);
		PdfPCell invoiceNoCell=new PdfPCell(invoiceNo);
//		invoiceNoCell.addElement(invoiceNo);
		invoiceNoCell.setBorder(0);
		invoiceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceNoVal=new Phrase(workOrder.getCount()+"",font7);
		PdfPCell invoiceNoValCell=new PdfPCell(invoiceNoVal);
//		invoiceNoValCell.addElement(invoiceNoVal);
		invoiceNoValCell.setBorder(0);
		invoiceNoValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//	
		Phrase invoiceDate=new Phrase("Delivery Date",font7bold);
		PdfPCell invoiceDateCell=new PdfPCell(invoiceDate);
//		invoiceDateCell.addElement(invoiceDate);
		invoiceDateCell.setBorder(0);
		invoiceDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase invoiceDateVal=new Phrase(fmt.format(workOrder.getCreationDate()),font7);
		PdfPCell invoiceDateValCell=new PdfPCell(invoiceDateVal);
//		invoiceDateValCell.addElement(invoiceDateVal);
		invoiceDateValCell.setBorder(0);
		invoiceDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase oderId=new Phrase("Sales Oder Id",font7bold);
		PdfPCell oderIdCell=new PdfPCell(oderId);
//		invoiceDateCell.addElement(invoiceDate);
		oderIdCell.setBorder(0);
		oderIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase oderIdValue=new Phrase(salesOrderEntity.getCount()+"",font7);
		PdfPCell oderIdValueValue=new PdfPCell(oderIdValue);
//		invoiceDateValCell.addElement(invoiceDateVal);
		oderIdValueValue.setBorder(0);
		oderIdValueValue.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase termsOfPayment = new Phrase("Mode/Terms of Payment",font7bold);
		PdfPCell termspaymentcell = new PdfPCell(termsOfPayment);
		termspaymentcell.setBorder(0);
		termspaymentcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String paymenttermsvalue = salesOrderEntity.getPaymentTermsList().get(0).getPayTermPercent()+"%  " +salesOrderEntity.getPaymentTermsList().get(0).getPayTermComment();
		Phrase paymentterms = new Phrase(paymenttermsvalue, font7);
		PdfPCell paymenttermscellValue = new PdfPCell(paymentterms);
		paymenttermscellValue.setBorder(0);
		paymenttermscellValue.setHorizontalAlignment(Element.ALIGN_LEFT);

		colonTable.addCell(reverseChargeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(reverseChargeValCell);
		colonTable.addCell(invoiceNoCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceNoValCell);
		colonTable.addCell(invoiceDateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(invoiceDateValCell);

		colonTable.addCell(oderIdCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(oderIdValueValue);
		
		colonTable.addCell(termspaymentcell);
		colonTable.addCell(colonCell);
		colonTable.addCell(paymenttermscellValue);

		PdfPCell pdfCell=new PdfPCell();
		pdfCell.setBorder(0);
		pdfCell.addElement(colonTable);
		
		part1Table.addCell(pdfCell);
		
		Phrase state=new Phrase("State",font7bold);
		PdfPCell stateCell=new PdfPCell(state);
//		stateCell.addElement(state);
		stateCell.setBorder(0);
		stateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stateVal=new Phrase(comp.getAddress().getState().trim(),font7);
		PdfPCell stateValCell=new PdfPCell(stateVal);
//		stateValCell.addElement(stateVal);
		stateValCell.setBorder(0);
		stateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stateCode=new Phrase("State Code",font7bold);
		PdfPCell stateCodeCell=new PdfPCell(stateCode);
//		stateCodeCell.addElement(stateCode);
		stateCodeCell.setBorder(0);
		stateCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase stateCodeVal=new Phrase(" ",font7);
		PdfPCell stateCodeValCell=new PdfPCell();
		stateCodeValCell.addElement(stateCodeVal);
		stateCodeValCell.setBorder(0);
		stateCodeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable statetable=new PdfPTable(2);
		statetable.setWidthPercentage(100);
		
		colonTable=new PdfPTable(3);
		float[] columnCollonWidth = {1.8f,0.2f,7.5f};
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateValCell);
		statetable.addCell(colonTable);
		
//		float[] columnrohanrrCollonWidth = {2.5f,0.2f,6.8f};
		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		colonTable.addCell(stateCodeCell);
		colonTable.addCell(colonCell);
		colonTable.addCell(stateCodeValCell);
		statetable=new PdfPTable(2);
		statetable.setWidthPercentage(100);
		statetable.addCell(colonTable);
		
		PdfPCell stateTableCell=new PdfPCell();
		stateTableCell.setBorder(0);
		stateTableCell.addElement(statetable);
		part1Table.addCell(stateTableCell);
		
		PdfPTable part2Table=new PdfPTable(1);
		part2Table.setWidthPercentage(100);
		
		Phrase transportmode=new Phrase("Transport Mode",font7bold);
		PdfPCell transportmodeCell=new PdfPCell(transportmode);
		transportmodeCell.setBorder(0);
		transportmodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String transMode="";
		if(workOrder.getGroup()!=null){
			transMode=workOrder.getGroup();
		}
		Phrase transModeVal=new Phrase(transMode,font7);
		PdfPCell transModeValCell=new PdfPCell(transModeVal);
		transModeValCell.setBorder(0);
		transModeValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		Phrase vechicleNo=new Phrase("Vehicle Number",font7bold);
		PdfPCell vechicleNoCell=new PdfPCell(vechicleNo);
		vechicleNoCell.setBorder(0);
		vechicleNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		String vechicalnumber="";
		if(workOrder.getUserTrainingOne()!=null){
			vechicalnumber=workOrder.getUserTrainingOne();
		}
		
		Phrase vechicleNoValue=new Phrase(vechicalnumber,font7);
		PdfPCell vechicleNoValueCell=new PdfPCell(vechicleNoValue);
		vechicleNoValueCell.setBorder(0);
		vechicleNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		//  delivery date
		
		Phrase deliveryDate=new Phrase("Date of Supply",font7bold);
		PdfPCell deliveryDateCell=new PdfPCell(deliveryDate);
		deliveryDateCell.setBorder(0);
		deliveryDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		Phrase deliveryDateValue;
		if(workOrder.getRefDate()!=null){
			deliveryDateValue=new Phrase(fmt.format(workOrder.getRefDate()),font7);
		}else{
			deliveryDateValue=new Phrase(" ",font7);
		}
		
		PdfPCell deliveryDateValueCell=new PdfPCell(deliveryDateValue);
		deliveryDateValueCell.setBorder(0);
		deliveryDateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		//   place of supply 
		
		Phrase placeOfSupply=new Phrase("Place Of Supply",font7bold);
		PdfPCell placeOfSupplyCell=new PdfPCell(placeOfSupply);
		placeOfSupplyCell.setBorder(0);
		placeOfSupplyCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		Phrase placeOfSupplyValue=new Phrase(salesOrderEntity.getShippingAddress().getCity(),font7);
		PdfPCell placeOfSupplyValueCell=new PdfPCell(placeOfSupplyValue);
		placeOfSupplyValueCell.setBorder(0);
		placeOfSupplyValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);


		Phrase refenrecNo=new Phrase("Other Reference(s)",font7bold);
		PdfPCell refenrecNoCell=new PdfPCell(refenrecNo);
		refenrecNoCell.setBorder(0);
		refenrecNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		Phrase refenrecNoValue=new Phrase(workOrder.getRefNum(),font7);
		PdfPCell refenrecNoValueCell=new PdfPCell(refenrecNoValue);
		refenrecNoValueCell.setBorder(0);
		refenrecNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		
		PdfPTable periodtable=new PdfPTable(2);
		periodtable.setWidthPercentage(100);
		float[] columnrohanrrCollonWidth = {3f,0.2f,7.7f}; //2.8
		PdfPTable concolonTable=new PdfPTable(3);
		concolonTable.setWidthPercentage(100);
		try {
			concolonTable.setWidths(columnrohanrrCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		concolonTable.addCell(transportmodeCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(transModeValCell);
		
		concolonTable.addCell(vechicleNoCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(vechicleNoValueCell);
		
		concolonTable.addCell(deliveryDateCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(deliveryDateValueCell);
		
		concolonTable.addCell(placeOfSupplyCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(placeOfSupplyValueCell);
		
		
		concolonTable.addCell(refenrecNoCell);
		concolonTable.addCell(colonCell);
		concolonTable.addCell(refenrecNoValueCell);
		
		float[] columnDateCollonWidth = {1.5f,0.2f,1.2f};
		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		colonTable.addCell(startDateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(startDateValCell);
		
		PdfPCell startcolonTableCell=new PdfPCell(colonTable);
		startcolonTableCell.setBorder(0);
//		startcolonTableCell.addElement(colonTable);
		periodtable.addCell(startcolonTableCell);
		
		colonTable=new PdfPTable(3);
		colonTable.setWidthPercentage(100);
		try {
			colonTable.setWidths(columnDateCollonWidth);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		colonTable.addCell(endDateCell);
//		colonTable.addCell(colonCell);
//		colonTable.addCell(endDateValCell);
		
		PdfPCell endcolonTableCell=new PdfPCell();
		endcolonTableCell.setBorder(0);
		endcolonTableCell.addElement(colonTable);
		periodtable.addCell(endcolonTableCell);
		
		PdfPCell periodTableCell=new PdfPCell();
		periodTableCell.setBorder(0);
		periodTableCell.addElement(periodtable);
		
		PdfPCell concolonTableCell=new PdfPCell();
		concolonTableCell.setBorder(0);
		concolonTableCell.addElement(concolonTable);
		
		
		
		part2Table.addCell(concolonTableCell);
		part2Table.addCell(periodTableCell);
		
		PdfPCell part1Cell=new PdfPCell();
		part1Cell.setBorderWidthRight(0);
		part1Cell.addElement(part1Table);
		
		mainTable.addCell(part1Cell);
		
		part1Cell=new PdfPCell();
		part1Cell.addElement(part2Table);
		mainTable.addCell(part1Cell);
		
		Phrase billingAddress=new Phrase("Bill to Party",font8bold);
		PdfPCell billAdressCell=new PdfPCell(billingAddress);
		billAdressCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		mainTable.addCell(billAdressCell);
		Phrase serviceaddress=new Phrase("Ship to Party",font8bold);
		PdfPCell serviceCell=new PdfPCell(serviceaddress);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		mainTable.addCell(serviceCell);
		try {
			document.add(mainTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
//		try
//		{
//		Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}
	

//  latest delivery note pdf code by rohan Date : 31-07-2017

private void createHeader(int value) 
{
	//Date 21/11/2017 
	//Dev.By Jayshree
	//Des.to add the logo in Table
	DocumentUpload logodocument =comp.getLogo();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	PdfPCell imageSignCell = null;
	Image image2=null;
	try {
		image2 = Image.getInstance(new URL(hostUrl+logodocument.getUrl()));
		image2.scalePercent(20f);
//		image2.setAbsolutePosition(40f,765f);	
//		doc.add(image2);
		
		imageSignCell = new PdfPCell(image2);
		imageSignCell.setBorder(0);
		imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		imageSignCell.setFixedHeight(20);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	Image image1=null;
//	try
//	{
//	 image1=Image.getInstance("images/ipclogo4.jpg ");//images/ipclogo4.jpg
//	image1.scalePercent(20f);
////	image1.setAbsolutePosition(40f,765f);	
////	doc.add(image1);
//	
//	
//	
//	
//	
//	imageSignCell=new PdfPCell();
//	imageSignCell.addElement(image1);
//	imageSignCell.setFixedHeight(20);
//	imageSignCell.setBorder(0);
//	imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}


PdfPTable logotab =new PdfPTable(1);
logotab.setWidthPercentage(100);



	if(imageSignCell!=null)
	{
	logotab.addCell(imageSignCell);
	}
	else
	{
	Phrase logoph=new Phrase(" ");
	PdfPCell logoBlank=new PdfPCell(logoph);
	logoBlank.setBorder(0);
	logotab.addCell(logoBlank);
	}
//complete logotab

	Phrase companyName = null;
	if(comp!=null){
		companyName=new Phrase(comp.getBusinessUnitName().trim(),font16bold);
	}
	
	Paragraph companyNamepara=new Paragraph();
	companyNamepara.add(companyName);
	companyNamepara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell companyNameCell=new PdfPCell();
	companyNameCell.addElement(companyNamepara);
	companyNameCell.setBorder(0);
	companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase companyAddr = null;
	if(comp!=null){
		companyAddr=new Phrase(comp.getAddress().getCompleteAddress().trim(),font12);
	}
	Paragraph companyAddrpara=new Paragraph();
	companyAddrpara.add(companyAddr);
	companyAddrpara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell companyAddrCell=new PdfPCell();
	companyAddrCell.addElement(companyAddrpara);
	companyAddrCell.setBorder(0);
	companyAddrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	Phrase companyGSTTIN = null;
	String gstinValue="";
		
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
				gstinValue=comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
				break;
			}
		}
		
		
		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			if(!comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
				gstinValue=gstinValue+","+comp.getArticleTypeDetails().get(i).getArticleTypeName()+" : "+comp.getArticleTypeDetails().get(i).getArticleTypeValue().trim();
			}
		}
	
	
	if(!gstinValue.equals("")){
		companyGSTTIN=new Phrase(gstinValue,font12bold);
	}

	Paragraph companyGSTTINpara=new Paragraph();
	companyGSTTINpara.add(companyGSTTIN);
	companyGSTTINpara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell companyGSTTINCell=new PdfPCell();
	companyGSTTINCell.addElement(companyGSTTINpara);
	companyGSTTINCell.setBorder(0);
	companyGSTTINCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	PdfPTable pdfPTable=new PdfPTable(1);
	pdfPTable.setWidthPercentage(100);
	pdfPTable.addCell(companyNameCell);
	pdfPTable.addCell(companyAddrCell);
	pdfPTable.addCell(companyGSTTINCell);
	
	//Date 21/11/2017
		//Dev.By Jayshree 
		//Des.To add logo tab in table
		PdfPTable header =new PdfPTable(2);
		header.setWidthPercentage(100);
		
		
		try {
			header.setWidths(new float[]{20,80});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		if(imageSignCell!=null){
		PdfPCell left=new PdfPCell(logotab);
		left.setBorder(0);
		header.addCell(left);
		
		PdfPCell right=new PdfPCell(pdfPTable);
		right.setBorder(0);
		header.addCell(right);
		}
		else
		{
			PdfPCell right=new PdfPCell(pdfPTable);
			right.setBorder(0);
			right.setColspan(2);
			header.addCell(right);
		}
		try {
			document.add(header);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		try {
//		document.add(pdfPTable);
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
		
		//End by jayshree
	

	
	//  rohan added this code 
	PdfPTable mytbale = new PdfPTable(3);
	mytbale.setWidthPercentage(100f);
	mytbale.setSpacingAfter(5f);
	mytbale.setSpacingBefore(5f);
	

//	try {
//		mytbale.setWidths(myWidth);
//	} catch (DocumentException e1) {
//		// TODO Auto-generated catch block
//		e1.printStackTrace();
//	}
	
	Image uncheckedImg=null;
	try {
		uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	uncheckedImg.scalePercent(9);

	
	Image checkedImg=null;
	try {
		checkedImg = Image.getInstance("images/checked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	checkedImg.scalePercent(9);

	
	Phrase myblank=new Phrase("   ",font10);
	PdfPCell myblankCell=new PdfPCell(myblank);
	myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase myblankborderZero=new Phrase(" ",font10);
	PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
	myblankborderZeroCell.setBorder(0);
	myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
	Paragraph para1=new Paragraph();
	para1.setIndentationLeft(10f);
	para1.add(myblank);
	if(value==0){
		para1.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para1.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para1.add(stat1Phrase);
	para1.setAlignment(Element.ALIGN_MIDDLE);
	
	PdfPCell stat1PhraseCell=new PdfPCell(para1);
	stat1PhraseCell.setBorder(0);
	stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
	Paragraph para2=new Paragraph();
	para2.setIndentationLeft(10f);
	
	if(value==1){
		para2.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para2.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para2.add(stat2Phrase);
	para2.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell stat2PhraseCell=new PdfPCell(para2);
	stat2PhraseCell.setBorder(0);
	stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
	Paragraph para3=new Paragraph();
	para3.setIndentationLeft(10f);
	
	if(value==2){
		para3.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para3.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	para3.add(stat3Phrase);
	para3.setAlignment(Element.ALIGN_JUSTIFIED);
	
	PdfPCell stat3PhraseCell=new PdfPCell(para3);
	stat3PhraseCell.setBorder(0);
	stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	mytbale.addCell(stat1PhraseCell);
	mytbale.addCell(stat2PhraseCell);
	mytbale.addCell(stat3PhraseCell);

	PdfPTable tab = new PdfPTable(1);
	tab.setWidthPercentage(100f);
	
	PdfPCell cell = new PdfPCell(mytbale);
	tab.addCell(cell);
	try {
		document.add(tab);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
// ends here 
		String titlepdf="Delivery Note";
			
			Phrase titlephrase=new Phrase(titlepdf,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell titlecell=new PdfPCell();
			titlecell.addElement(titlepdfpara);
			titlecell.setBorder(0);
			
			Phrase blankphrase=new Phrase("",font8);
			PdfPCell blankCell=new PdfPCell();
			blankCell.addElement(blankphrase);
			blankCell.setBorder(0);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
			titlepdftable.addCell(blankCell);
			titlepdftable.addCell(titlecell);
			titlepdftable.addCell(blankCell);
			
			 Paragraph blank =new Paragraph();
			    blank.add(Chunk.NEWLINE);
			
			PdfPTable parent=new PdfPTable(1);
			parent.setWidthPercentage(100);
			
			PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
			parent.addCell(titlePdfCell);
			
			try {
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
	}	

private void createBlankforUPC(int i) 
{
	Image uncheckedImg=null;
	try {
		uncheckedImg = Image.getInstance("images/unchecked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	uncheckedImg.scalePercent(9);
	
	Image checkedImg=null;
	try {
		checkedImg = Image.getInstance("images/checked_checkbox.png");
	} catch (BadElementException | IOException e3) {
		e3.printStackTrace();
	}
	checkedImg.scalePercent(9);

	PdfPTable mytbale = new PdfPTable(3);
	mytbale.setSpacingAfter(5f);
	mytbale.setWidthPercentage(100f);
	
	Phrase myblank=new Phrase("   ",font10);
	PdfPCell myblankCell=new PdfPCell(myblank);
	myblankCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase myblankborderZero=new Phrase(" ",font10);
	PdfPCell myblankborderZeroCell=new PdfPCell(myblankborderZero);
	myblankborderZeroCell.setBorder(0);
	myblankborderZeroCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat1Phrase=new Phrase("  Original for Receipient",font10);
	Paragraph para1=new Paragraph();
	para1.setIndentationLeft(10f);
	para1.add(myblank);
	if(i==0){
		para1.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para1.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para1.add(stat1Phrase);
	para1.setAlignment(Element.ALIGN_MIDDLE);
	
	PdfPCell stat1PhraseCell=new PdfPCell(para1);
	stat1PhraseCell.setBorder(0);
	stat1PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	Phrase stat2Phrase=new Phrase("  Duplicate for Supplier/Transporter",font10);
	Paragraph para2=new Paragraph();
	para2.setIndentationLeft(10f);
	
	if(i==1){
		para2.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para2.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	
	para2.add(stat2Phrase);
	para2.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell stat2PhraseCell=new PdfPCell(para2);
	stat2PhraseCell.setBorder(0);
	stat2PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase stat3Phrase=new Phrase("  Triplicate for Supplier",font10);
	Paragraph para3=new Paragraph();
	para3.setIndentationLeft(10f);
	
	if(i==2){
		para3.add(new Chunk(checkedImg, 0, 0, true));	
	}
	else{
		para3.add(new Chunk(uncheckedImg, 0, 0, true));
	}
	para3.add(stat3Phrase);
	para3.setAlignment(Element.ALIGN_JUSTIFIED);
	
	PdfPCell stat3PhraseCell=new PdfPCell(para3);
	stat3PhraseCell.setBorder(0);
	stat3PhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);

	mytbale.addCell(stat1PhraseCell);
	mytbale.addCell(stat2PhraseCell);
	mytbale.addCell(stat3PhraseCell);
	
	//  ends here 
	String titlepdf="Delivery Note ";
		
		Phrase titlephrase=new Phrase(titlepdf,font14bold);
		Paragraph titlepdfpara=new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell titlecell=new PdfPCell();
		titlecell.addElement(titlepdfpara);
		titlecell.setBorder(0);
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(titlecell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(10f);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(mytbale);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
}

	
}
