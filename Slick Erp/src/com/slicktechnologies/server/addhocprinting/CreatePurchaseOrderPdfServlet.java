package com.slicktechnologies.server.addhocprinting;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.purchaseOrderPdf.PurchseOrderUpdatedPdf;
import com.slicktechnologies.server.addhocprinting.sasha.PurchaseOrderPdfForSasha;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;

import static com.googlecode.objectify.ObjectifyService.ofy;


public class CreatePurchaseOrderPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4192616077384586232L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			
			String stringid = req.getParameter("Id");
			
			stringid = stringid.trim();
			String preprintStatus = req.getParameter("preprint");
			Long count = Long.parseLong(stringid);
			String pdftype = req.getParameter("type");//Date 13-7-2018 add by jayshree
			PurchaseOrder po = ofy().load().type(PurchaseOrder.class).id(count).now();
			
			/**
			 * date 13-7-2018
			 * by jayshree
			 * des.to call the sasha po pdf
			 */
			/** date 25.8.2018 added by komal**/
			if(pdftype == null){
				pdftype = "";
			}/** end komal**/
			if(pdftype.equalsIgnoreCase("POPDFV1")){

				PurchaseOrderPdfForSasha popdfsasha = new PurchaseOrderPdfForSasha();
				popdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				Document document = popdfsasha.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				document.open();
				popdfsasha.setPurchaseOrder(count);
				popdfsasha.createPdf();
				document.close();
				
				
			}else if(pdftype.equalsIgnoreCase("PurchaseOrderUpdatedPdf")){
				PurchseOrderUpdatedPdf popdfsasha = new PurchseOrderUpdatedPdf();
				popdfsasha.document = new Document(PageSize.A4,20,20,10,10);
				Document document = popdfsasha.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				document.open();
				popdfsasha.setPurchaseOrder(count);
				popdfsasha.createPdf();
				document.close();
			
				/**
				 *     Added By : Priyanka Bhagwat
				 *     Date : 1/06/2021
				 *     Des : When Submit PO then SMS and EMAIL will be send Req by Pecopp raised By Ashwini.	
				 */
			}else if(po!=null&&ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("PurchaseOrder","OldVersionPurchaseOrderPdf", po.getCompanyId())){
			//}else if(pdftype.equalsIgnoreCase("OldVersionPurchaseOrderPdf")){//OldVersionPurchaseOrderPdf
				PurchaseOrderPdf popdf = new PurchaseOrderPdf();
				popdf.document = new Document();
				Document document = popdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																			// the
				if(po.getStatus().equals("Cancelled")){
					writer.setPageEvent(new PdfCancelWatermark());
				}else															// pdf
				if(!po.getStatus().equals("Approved")){	
					 writer.setPageEvent(new PdfWatermark());
				} 
				document.open();
				popdf.setPurchaseOrder(count);
				popdf.createPdf(preprintStatus);
				document.close();
				
			}
			else
			{
			PurchaseOrderVersionOnePdf popdf = new PurchaseOrderVersionOnePdf();
			popdf.document = new Document();
			Document document = popdf.document;
			PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
																		// the
			if(po.getStatus().equals("Cancelled")){
				writer.setPageEvent(new PdfCancelWatermark());
			}else															// pdf
			if(!po.getStatus().equals("Approved")){	
				 writer.setPageEvent(new PdfWatermark());
			} 
			document.open();
			popdf.setPurchaseOrder(count);
			popdf.createPdf(preprintStatus);
			document.close();
			}	
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
	
	
	
	

}
