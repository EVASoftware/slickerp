package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;

public class UniversalPestQuotationPdf {

	public Document document;
	Quotation quotation;
	Company comp;
	Customer cust;
	float blankLines=0;
	List<PaymentTerms> payTermsLis;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	
	BaseColor mycolor = WebColors.getRGBColor("#C0C0C0");
	
	private  Font font16boldul,font12bold,font8bold,font8,font9bold,font12boldul,font10boldul,font12,bold,
	font16bold,font10,font10bold,font14bold;
	
	public 	UniversalPestQuotationPdf()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,9);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10boldul= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD|Font.UNDERLINE);
		font9bold= new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD |Font.UNDERLINE);
	}
	
	public void setservicequotation(Long count) {
		
		quotation=ofy().load().type(Quotation.class).id(count).now();
		payTermsLis=quotation.getPaymentTermsList();
		
		if (quotation.getCompanyId() != null)
		{
			comp = ofy().load().type(Company.class).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else
		{
			comp = ofy().load().type(Company.class).first().now();
		}
		
		
		if (quotation.getCompanyId() != null)
		{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).filter("companyId", quotation.getCompanyId()).first().now();
		}
		else{
			cust = ofy().load().type(Customer.class).filter("count", quotation.getCustomerId()).first().now();
		}
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	public void createPdf(String preprintStatus) {
	System.out.println("in side create pdf method");
	
	//   1st page  
		if(preprintStatus.equals("yes")){
			
			System.out.println("inside prit yes");
			createBlankforUPC();
		}
		
		if(preprintStatus.equals("no")){
		System.out.println("inside prit no");
		
		if(comp.getUploadHeader()!=null){
		createCompanyNameAsHeader(document,comp);
		}
		
		if(comp.getUploadFooter()!=null){
		createCompanyNameAsFooter(document,comp);
		}
		createBlankforUPC();
		}
	
		createCustomerDetails();
		createSubjectAndOtehrDetails();
		createProductDetails();
		
		
		
		//   2 nd page 
		
		frequencyDeatails();
		/**
		 * below method contains code for
		 * 1. notes
		 * 2. payment terms 
		 * 3. quotation validity
		 * 4. other contact details 
		 *  
		 */
		otherremainingDetails();
		
		//   3rd Page 
		createClientsPage();
	}

	private void createBlankforUPC() {
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}


private void createCompanyNameAsHeader(Document doc, Company comp) {
	
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}


private void createCustomerDetails() {
	
	float[] relativeWidths2Column = { 0.7f, 9.3f };	
	//    date heading 
	PdfPTable table = new PdfPTable(2);
	table.setWidthPercentage(100f);
	table.setSpacingBefore(15f);
	table.setSpacingAfter(15f);
	
	try {
		table.setWidths(relativeWidths2Column);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	Phrase date= new Phrase("Date :", font8bold);
	PdfPCell dateCell = new PdfPCell(date);
	dateCell.setBorder(0);
	dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	table.addCell(dateCell);
	
	Phrase dateValue = new Phrase(fmt.format(quotation.getQuotationDate()),font8);
	PdfPCell dateValueCell = new PdfPCell(dateValue);
	dateValueCell.setBorder(0);
	dateValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	table.addCell(dateValueCell);
	
	try {
		document.add(table);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	
	
	//    customer details 
	
	PdfPTable customerDetailsTable = new PdfPTable(1);
	customerDetailsTable.setWidthPercentage(100f);
	
	
	Phrase customerFullName= new Phrase("To,"+"\n"+cust.getFullname(),font8);
	PdfPCell customerFullNameCell = new PdfPCell(customerFullName);
	customerFullNameCell.setBorder(0);
	customerFullNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	customerDetailsTable.addCell(customerFullNameCell);
	
//		if(cust.getRefrNumber1()==null)
//		{
//			Phrase customerId = new Phrase("Customer Id :" +cust.getRefrNumber1(),font8);
//			PdfPCell customerIdCell = new PdfPCell(customerId);
//			customerIdCell.setBorder(0);
//			customerIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			customerDetailsTable.addCell(customerIdCell);
//		}
		
		
		// customer Address details 
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			if(cust.getAdress().getAddrLine2()!= null && !cust.getAdress().getAddrLine2().equals("")){
				if(cust.getAdress().getLandmark()!= null && !cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getAddrLine2();
				}
			}else{
				if(cust.getAdress().getLandmark()!= null && !cust.getAdress().getLandmark().equals("")){
					custAdd1=cust.getAdress().getAddrLine1()+"\n"+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			if(cust.getAdress().getLocality()!=null && !cust.getAdress().getLocality().equals("")){
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+" - "+cust.getAdress().getPin()+"\n"+cust.getAdress().getLocality()+"\n"+cust.getAdress().getState()+"\n"+cust.getAdress().getCountry();
						
			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+" - "+cust.getAdress().getPin()+"\n"+cust.getAdress().getState()+"\n"+cust.getAdress().getCountry();
			}
		}
		
		
		Phrase custAddInfo = new Phrase(custFullAdd1, font8);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		customerDetailsTable.addCell(custAddInfoCell);
		
		try {
			document.add(customerDetailsTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}


	
		private void createProductDetails() {
		
			PdfPTable productTable = new PdfPTable(1);
			productTable.setWidthPercentage(100f);
			productTable.setSpacingBefore(10f);
			
			Phrase ourService = new Phrase("Our Services",bold);
			PdfPCell ourServiceCell = new PdfPCell(ourService);
			ourServiceCell.setBorder(0);
			productTable.addCell(ourServiceCell);
			
			Phrase blank = new Phrase(" ",font8);
			PdfPCell blankCell = new PdfPCell(blank);
			blankCell.setBorder(0);
			productTable.addCell(blankCell);
			
			for (int i = 0; i < quotation.getItems().size(); i++) 
			{
				ServiceProduct serProd = ofy().load().type(ServiceProduct.class).filter("productCode", quotation.getItems().get(i).getProductCode()).first().now();
			
				
				Phrase prodNamePhrase = new Phrase(quotation.getItems().get(i).getProductName(),bold); 
				PdfPCell prodNameCell = new PdfPCell(prodNamePhrase);
				prodNameCell.setBorder(0);
				productTable.addCell(prodNameCell);
				
				String prodDisc ="";
				if(serProd!=null)
				{
					String str1=null,str2 = null,str3=null,str4 = null;
					if(serProd.getComment()!=null )
					{
						str1 = serProd.getComment();
					}
					else
					{
						str1 = "";
					}
				
					if(serProd.getCommentdesc()!=null )
					{
						str2 = serProd.getCommentdesc();
					}
					else
					{
						str2 = "";
					}
				
					if(serProd.getCommentdesc1()!=null )
					{
						str3 = serProd.getCommentdesc1();
					}
					else
					{
						str3 = "";
					}
					
					if(serProd.getCommentdesc2()!=null )
					{
						str4 = serProd.getCommentdesc2();
					}
					else
					{
						str4 = "";
					}
					prodDisc =str1+str2+str3+str4;
					System.out.println("proddis="+prodDisc);
					Phrase proddisc = new Phrase(prodDisc,font8); 
					PdfPCell proddiscCell = new PdfPCell(proddisc);
					proddiscCell.setBorder(0);
					productTable.addCell(blankCell);
					productTable.addCell(proddiscCell);
					
				}
			}
			
			
			try {
				document.add(productTable);
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

		
	private void createSubjectAndOtehrDetails() {
	
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(20f);
		Phrase sub = new Phrase("Sub : Quotation for Pest Control treatment at your premises.",font8);
		PdfPCell subCell = new PdfPCell(sub);
		subCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subCell.setBorder(0);
		table.addCell(subCell);
		
		Phrase blank = new Phrase(" ",font8);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell.setBorder(0);
		table.addCell(blankCell);
		
		
		Phrase  resp = new Phrase("Respected Sir,",font8);
		PdfPCell respCell = new PdfPCell(resp);
		respCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		respCell.setBorder(0);
		table.addCell(respCell);
		
		table.addCell(blankCell);
		
		
		
		Phrase note = new Phrase("In reference to our discussion, we are very glad to give you the quotation of Pest Control Service at your premises. ",font8);
		PdfPCell noteCell = new PdfPCell(note);
		noteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		noteCell.setBorder(0);
		table.addCell(noteCell);
		table.addCell(blankCell);
		table.addCell(blankCell);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}	
	
		
		
	private void frequencyDeatails(){
		float[] columnWidths ={0.8f, 5.8f, 3.5f};
		
		Paragraph note = new Paragraph("Frequency",font8bold);
		note.setAlignment(Element.ALIGN_LEFT);
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100f);
		table.setSpacingBefore(20f);
		table.setSpacingAfter(30f);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase srNo = new Phrase("Sr No",font8bold);
		PdfPCell srNoCell = new PdfPCell(srNo);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(srNoCell);
		
		Phrase treatment = new Phrase("Treatment",font8bold);
		PdfPCell treatmentCell = new PdfPCell(treatment);
		treatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(treatmentCell);
		
		Phrase frequency = new Phrase("Frequency",font8bold);
		PdfPCell frequencyCell = new PdfPCell(frequency);
		frequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(frequencyCell);
		
		for (int i = 0; i < quotation.getItems().size(); i++)
		{
			Phrase srNoValue = new Phrase(i+1+"",font8);
			PdfPCell srNoValueCell = new PdfPCell(srNoValue);
			srNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			srNoValueCell.setBackgroundColor(mycolor);
			table.addCell(srNoValueCell);
			
			Phrase treatmentValue = new Phrase(quotation.getItems().get(i).getProductName(),font8);
			PdfPCell treatmentValueCell = new PdfPCell(treatmentValue);
			treatmentValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treatmentValueCell.setBackgroundColor(mycolor);
			table.addCell(treatmentValueCell);
			
			Phrase frequencyValue = new Phrase(quotation.getItems().get(i).getNumberOfServices()+" Service",font8);
			PdfPCell frequencyValueCell = new PdfPCell(frequencyValue);
			frequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(frequencyValueCell);
		}
		
		
		Paragraph charges = new Paragraph("Charges",font8bold);
		charges.setAlignment(Element.ALIGN_LEFT);
		
		PdfPTable valuetable = new PdfPTable(3);
		valuetable.setWidthPercentage(100f);
		valuetable.setSpacingBefore(20f);
		valuetable.setSpacingAfter(30f);
		try {
			valuetable.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase valuesrNo = new Phrase("Sr No",font8bold);
		PdfPCell valuesrNoCell = new PdfPCell(valuesrNo);
		srNoCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		valuetable.addCell(valuesrNoCell);
		
		Phrase valuetreatment = new Phrase("Area",font8bold);
		PdfPCell valuetreatmentCell = new PdfPCell(valuetreatment);
		valuetreatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		valuetable.addCell(valuetreatmentCell);
		
		Phrase valuefrequency = new Phrase("Charges For AMC",font8bold);
		PdfPCell valuefrequencyCell = new PdfPCell(valuefrequency);
		valuefrequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		valuetable.addCell(valuefrequencyCell);
		
		for (int i = 0; i < quotation.getItems().size(); i++)
		{
			Phrase srNoValue = new Phrase(i+1+"",font8);
			PdfPCell srNoValueCell = new PdfPCell(srNoValue);
			srNoValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			srNoValueCell.setBackgroundColor(mycolor);
			valuetable.addCell(srNoValueCell);
			
			Phrase treatmentValue = null;
			if(quotation.getItems().get(i).getPremisesDetails()!= null && !quotation.getItems().get(i).getPremisesDetails().equals(""))
			{
				treatmentValue = new Phrase(quotation.getItems().get(i).getPremisesDetails(),font8);
			}
			else
			{
				treatmentValue = new Phrase("NA",font8);
			}
			PdfPCell treatmentValueCell = new PdfPCell(treatmentValue);
			treatmentValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treatmentValueCell.setBackgroundColor(mycolor);
			valuetable.addCell(treatmentValueCell);
			
			Phrase frequencyValue = null; 
			if(quotation.getItems().get(i).getServiceTax().getPercentage()!=0)
			{
				frequencyValue = new Phrase(quotation.getItems().get(i).getPrice()+"/-" +"for "+quotation.getItems().get(i).getProductName()+" *  GST Extra",font8);
			}
			else
			{
				frequencyValue = new Phrase(quotation.getItems().get(i).getPrice()+"/-" +"for "+quotation.getItems().get(i).getProductName(),font8);
			}
			PdfPCell frequencyValueCell = new PdfPCell(frequencyValue);
			frequencyCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			valuetable.addCell(frequencyValueCell);
		}
		
		
		
		try {
			document.add(note);
			document.add(table);
			document.add(charges);
			document.add(valuetable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void otherremainingDetails() {
		
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		Phrase note = new Phrase("Note",bold);
		PdfPCell noteCell = new PdfPCell(note);
		noteCell.setBorder(0);
		table.addCell(noteCell);
		
		Phrase note1 = new Phrase("1) Your repesentative should be present during the spraying operation to remove or shift articals.",font8);
		PdfPCell note1Cell = new PdfPCell(note1);
		note1Cell.setBorder(0);
		table.addCell(note1Cell);
		
		Phrase note2 = new Phrase("2) Computer,electronic items should be properly covered.",font8);
		PdfPCell note2Cell = new PdfPCell(note2);
		note2Cell.setBorder(0);
		table.addCell(note2Cell);
		
		Phrase note3 = new Phrase("3) Closets,Bathrooms and Drainage inspection pit should be kept open.",font8);
		PdfPCell note3Cell = new PdfPCell(note3);
		note3Cell.setBorder(0);
		table.addCell(note3Cell);
		
		Phrase note4 = new Phrase("4) All the eatable itmes should be kept away from the treated area.",font8);
		PdfPCell note4Cell = new PdfPCell(note4);
		note4Cell.setBorder(0);
		table.addCell(note4Cell);
		
		Phrase note5 = new Phrase("5) We are not responsible for any kind of loss and damage of article in your premises",font8);
		PdfPCell note5Cell = new PdfPCell(note5);
		note5Cell.setBorder(0);
		table.addCell(note5Cell);
		
		
		
		try {
			document.add(table);
			calculatePaymentTerms();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

	private void calculatePaymentTerms() {
		float[] columnWidths={2f,8f};
		
		
		PdfPTable paytermTable = new PdfPTable(2);
		paytermTable.setWidthPercentage(100f);
		try {
			paytermTable.setWidths(columnWidths);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase payphrase=new Phrase("Payment Terms     :",font8bold);
		PdfPCell paytitle=new PdfPCell(payphrase);
		paytitle.setBorder(0);
		paytermTable.addCell(paytitle);
		
		String payterm="";
		 for(int i=0;i<this.payTermsLis.size();i++)
		 {
			 if(payTermsLis.get(i).getPayTermPercent()==100)
			 {
				 payterm = payTermsLis.get(i).getPayTermPercent()+"% "+payTermsLis.get(i).getPayTermComment();
			
				 	Phrase value=new Phrase(payterm,font8);
					PdfPCell valueCell=new PdfPCell(value);
					valueCell.setBorder(0);
					paytermTable.addCell(valueCell);
			 
			 }
			 else
			 {
				 
				
				 
				 payterm = payTermsLis.get(i).getPayTermPercent()+"% "+payTermsLis.get(i).getPayTermComment();
					
				 	Phrase value=new Phrase(payterm,font8);
					PdfPCell valueCell=new PdfPCell(value);
					valueCell.setBorder(0);
					paytermTable.addCell(valueCell);
					
					 if(i < this.payTermsLis.size()-1){
							Phrase blank=new Phrase(" ",font8);
							PdfPCell blankCell=new PdfPCell(blank);
							blankCell.setBorder(0);
							paytermTable.addCell(blankCell);
						} 
			 }
		 }
		
		
		
//		PdfPTable table = new PdfPTable(3);
//		table.setHorizontalAlignment(Element.ALIGN_LEFT);
//		
//		PdfPCell tablecell = new PdfPCell();
//		if(this.payTermsLis.size()>5){
//			tablecell.addElement(table);
//		}
//		else{
//			tablecell.addElement(table);
//			tablecell.setBorder(0);
//		}
//		
//		
//		Phrase paytermdays = new Phrase("Days",font8);
//		Phrase paytermpercent = new Phrase("Percent",font8);
//	    Phrase paytermcomment = new Phrase("Comment",font8);
//	  
//       PdfPCell celldays = new PdfPCell(paytermdays);
//       celldays.setBorder(0);
//       celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
//	   PdfPCell cellpercent = new PdfPCell(paytermpercent);
//	   cellpercent.setBorder(0);
//	   cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
//       PdfPCell cellcomment = new PdfPCell(paytermcomment);
//       cellcomment.setBorder(0);
//       cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
//	   
//       table.addCell(celldays);
//	   table.addCell(cellpercent);
//	   table.addCell(cellcomment);
//	   
//	   
////	   if(this.payTermsLis.size()<=5){
////		   int size = 5 - this.payTermsLis.size();
////		   blankLines=size*(80/5);
////	   }
////	   else{
////		   blankLines=0;
////	   }
////	   table.setSpacingAfter(blankLines);
//	   
//	   
//	 
//	   for(int i=0;i<this.payTermsLis.size();i++)
//	   {
//	  	 Phrase chunk=new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
//	  	 PdfPCell pdfdayscell = new PdfPCell(chunk);
//	  	 pdfdayscell.setBorder(0);
//   	   	 pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	  	 
//	  	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8);
//	  	 PdfPCell pdfpercentcell = new PdfPCell(chunk);
//	  	 pdfpercentcell.setBorder(0);
//	  	 pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//	  	 
//	  	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
//	  	 PdfPCell pdfcommentcell = new PdfPCell(chunk);
//	  	 pdfcommentcell.setBorder(0);
//	  	 pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//	  	 if(i==5){
//	  		 break;
//	  	 }
//	  	 
//	  	 table.addCell(pdfdayscell);
//	  	 table.addCell(pdfpercentcell);
//	  	 table.addCell(pdfcommentcell);
//	  	 }
//		
//	   
//		
//	   ///
//	   PdfPTable table1= new PdfPTable(3);
//	   
//	   PdfPCell table1cell = new PdfPCell();
//		Phrase blank = new Phrase();
//		 if(this.payTermsLis.size()>5){
//			 table1cell= new PdfPCell(table1);
//		        }else{
//		        	table1cell= new PdfPCell(blank);
//		        	table1cell.setBorder(0);
//		        }
//		 
//			Phrase paytermdays1 = new Phrase("Days",font8);
//			Phrase paytermpercent1 = new Phrase("Percent",font8);
//	        Phrase paytermcomment1 = new Phrase("Comment",font8);
//	      
//	       
//	        PdfPCell celldays1;
//	        PdfPCell cellpercent1;
//	        PdfPCell cellcomment1;
//	        
//	        System.out.println(this.payTermsLis.size()+" ...........b4 if");
//	        if(this.payTermsLis.size()>5){
//	        	System.out.println(this.payTermsLis.size()+" ...........af if");
//	         celldays1= new PdfPCell(paytermdays1);
//		    celldays1.setBorder(0);
//		    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        }else{
//	        	celldays1= new PdfPCell(blank);
//	    	    celldays1.setBorder(0);
//	    	    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        }
//	        
//	        if(this.payTermsLis.size()>5){
//	        
//			cellpercent1 = new PdfPCell(paytermpercent1);
//			cellpercent1.setBorder(0);
//			cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        }else{
//	        	cellpercent1 = new PdfPCell(blank);
//	    		cellpercent1.setBorder(0);
//	    		cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        	
//	        }
//	        
//	        if(this.payTermsLis.size()>5){
//		    cellcomment1= new PdfPCell(paytermcomment1);
//		    cellcomment1.setBorder(0);
//		    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        }else{
//	        	cellcomment1= new PdfPCell(blank);
//	    	    cellcomment1.setBorder(0);
//	    	    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	        }
//	        table1.addCell(celldays1);
//	        table1.addCell(cellpercent1);
//	        table1.addCell(cellcomment1);
//	       
//		 
//	   
//	  for(int i=5;i<this.payTermsLis.size();i++)
//	  {
//	 	 Phrase chunk=new Phrase(payTermsLis.get(i).getPayTermDays()+"",font8);
//	 	 PdfPCell pdfdayscell1 = new PdfPCell(chunk);
//	 	 pdfdayscell1.setBorder(0);
//	 	 pdfdayscell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	 	 
//	 	 chunk=new Phrase(payTermsLis.get(i).getPayTermPercent()+"",font8);
//	 	 PdfPCell pdfpercentcell1 = new PdfPCell(chunk);
//	 	 pdfpercentcell1.setBorder(0);
//	 	 pdfpercentcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//	 	 
//	 	 chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),font8);
//	 	 PdfPCell pdfcommentcell1 = new PdfPCell(chunk);
//	 	 pdfcommentcell1.setBorder(0);
//	 	 pdfcommentcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//
//	 	 table1.addCell(pdfdayscell1);
//	 	 table1.addCell(pdfpercentcell1);
//	 	 table1.addCell(pdfcommentcell1);
//	 	 
//	 	 if(i==9){
//	 		 break;
//	 	 }
//	 	}
//	   
//	  
//	  PdfPTable maitable=new PdfPTable(2);
//	  maitable.setWidthPercentage(100);
//	  maitable.addCell(tablecell);
//	  maitable.addCell(table1cell);
//	  maitable.setSpacingAfter(2f);
//		
//	  PdfPCell mainTable = new PdfPCell();
//	  mainTable.addElement(maitable);
//	  mainTable.setBorder(0);
//	  
//	  
//	Phrase descPhrase=null;
//			
//	String titleterms="Payment Terms";
//	Phrase termsphrase=new Phrase(titleterms,font8bold);
//	termsphrase.add(Chunk.NEWLINE);
//	PdfPCell termstitlecell=new PdfPCell();
//	termstitlecell.addElement(termsphrase);
//	termstitlecell.setBorder(0);
//	PdfPTable termsTable=new PdfPTable(1);
//	termsTable.addCell(termstitlecell);
//	
//	descPhrase=new Phrase();
//	PdfPCell desccell=new PdfPCell();
//	desccell.addElement(descPhrase);
//	desccell.setBorder(0);
//	   
//	PdfPTable termsTable1=new PdfPTable(1);
//	termsTable1.setWidthPercentage(100);
//	termsTable1.setSpacingAfter(2f);
//	   
//	termsTable1.addCell(paytitle);
//	termsTable1.addCell(mainTable);
//	termsTable1.addCell(desccell);
	  
	
	
	
	
	
	
	PdfPTable valitUntiTable = new PdfPTable(2);
	valitUntiTable.setWidthPercentage(100f);
	try {
		valitUntiTable.setWidths(columnWidths);
	} catch (DocumentException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	Phrase validityPhrase = new Phrase("Quotation Valid Until : ",font8bold);
	PdfPCell validiyCell = new PdfPCell(validityPhrase);
	validiyCell.setBorder(0);
	validiyCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	valitUntiTable.addCell(validiyCell);
	
	Phrase validityValue  = new Phrase(fmt.format(quotation.getValidUntill()),font8);
	PdfPCell validityValueCell = new PdfPCell(validityValue);
	validityValueCell.setBorder(0);
	validityValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	valitUntiTable.addCell(validityValueCell);
	
	valitUntiTable.setSpacingAfter(10f);
	
	
	//   remainig part 
	
	PdfPTable parentTable  = new PdfPTable(1);
	parentTable.setWidthPercentage(100f);
	
	
	Phrase note = new Phrase("If a chance given to us, we are bound to keep your premises pest free.We await "
			+ "your acceptance of this quotation. In case of any query please feel free to call "
			+ comp.getPocName()+" (Director) on +91"+comp.getPocCell(),font8);
	PdfPCell noteCell = new PdfPCell(note);
	noteCell.setBorder(0);
	noteCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	parentTable.addCell(noteCell);

	
	Phrase thanking = new Phrase("\n"+"\n"+"Thanking you,",font8);
	PdfPCell thankingCell = new PdfPCell(thanking);
	thankingCell.setBorder(0);
	thankingCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	parentTable.addCell(thankingCell);
	
	
	Phrase yourSincerely = new Phrase("\n"+"Sincerely Yours",font8);
	PdfPCell yourSincerelyCell = new PdfPCell(yourSincerely);
	yourSincerelyCell.setBorder(0);
	yourSincerelyCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	parentTable.addCell(yourSincerelyCell);
	
	Phrase compName = new Phrase("\n"+"For,"+quotation.getGroup()+"\n"+"\n"+"\n",font8);
	PdfPCell compNameCell = new PdfPCell(compName);
	compNameCell.setBorder(0);
	compNameCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	parentTable.addCell(compNameCell);
	
	Phrase pocName = new Phrase(comp.getPocName(),font8);
	PdfPCell pocNameCell = new PdfPCell(pocName);
	pocNameCell.setBorder(0);
	pocNameCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	parentTable.addCell(pocNameCell);
	
	Phrase director = new Phrase("(Director)",font8);
	PdfPCell directorCell = new PdfPCell(director);
	directorCell.setBorder(0);
	directorCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
	parentTable.addCell(directorCell);
	
	
	 try 
	 {
		 
	   document.add(paytermTable);
		   
	   document.add(valitUntiTable);
	   document.add(parentTable);
	 } catch (DocumentException e) {
		e.printStackTrace();
	}
	}

	private void createClientsPage() {
	
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		
		Phrase dottedPh = new Phrase("-------------------------------------------------------------------------------"
				+ "--------------------------------------------------------------------------------------------"
				,font8);
		PdfPCell dottedCell = new PdfPCell(dottedPh);
		dottedCell.setBorder(0);
		table.addCell(dottedCell);
		
		Phrase someClients = new Phrase("Some of Our Clients :",font10bold);
		PdfPCell someClientsCell = new PdfPCell(someClients);
		someClientsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		someClientsCell.setBorder(0);
		table.addCell(someClientsCell);
		
		table.addCell(dottedCell);
		
		Phrase niko = new Phrase("Cafe Coffee Day � Whole Gujarat",font10bold);
		PdfPCell nikoCell = new PdfPCell(niko);
		nikoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nikoCell.setBorder(0);
		table.addCell(nikoCell);
		
		table.addCell(dottedCell);
		
		Phrase pizza = new Phrase("Pizza Hut � Whole Gujarat",font10bold);
		PdfPCell pizzaCell = new PdfPCell(pizza);
		pizzaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pizzaCell.setBorder(0);
		table.addCell(pizzaCell);
		
		table.addCell(dottedCell);
		
		Phrase idbi = new Phrase("Niko Resources Ltd.",font10bold);
		PdfPCell idbiCell = new PdfPCell(idbi);
		idbiCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		idbiCell.setBorder(0);
		table.addCell(idbiCell);
		
		table.addCell(dottedCell);
		
		Phrase stinless = new Phrase("Welspun India Ltd.",font10bold);
		PdfPCell stinlessCell = new PdfPCell(stinless);
		stinlessCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		stinlessCell.setBorder(0);
		table.addCell(stinlessCell);
		
		table.addCell(dottedCell);
		
		Phrase yesBank = new Phrase("IDBI Bank",font10bold);
		PdfPCell yesBankCell = new PdfPCell(yesBank);
		yesBankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		yesBankCell.setBorder(0);
		table.addCell(yesBankCell);
		
		table.addCell(dottedCell);
		
		Phrase tata = new Phrase("Oriental Bank of Commence",font10bold);
		PdfPCell tataCell = new PdfPCell(tata);
		tataCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		tataCell.setBorder(0);
		table.addCell(tataCell);
		
		table.addCell(dottedCell);
		
		Phrase pidilite = new Phrase("Fortune Resorts",font10bold);
		PdfPCell pidiliteCell = new PdfPCell(pidilite);
		pidiliteCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pidiliteCell.setBorder(0);
		table.addCell(pidiliteCell);
		
		table.addCell(dottedCell);
		
		Phrase cafe = new Phrase("Sterling Resorts",font10bold);
		PdfPCell cafeCell = new PdfPCell(cafe);
		cafeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cafeCell.setBorder(0);
		table.addCell(cafeCell);
		
		table.addCell(dottedCell);
		
		Phrase hospital = new Phrase("Bajaj Allianz General Insurance Company",font10bold);
		PdfPCell hospitalCell = new PdfPCell(hospital);
		hospitalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		hospitalCell.setBorder(0);
		table.addCell(hospitalCell);
		
		table.addCell(dottedCell);
		
		Phrase ford = new Phrase("IRB Infrastructure Developers Ltd.",font10bold);
		PdfPCell fordCell = new PdfPCell(ford);
		fordCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		fordCell.setBorder(0);
		table.addCell(fordCell);
		
		table.addCell(dottedCell);
		
		Phrase national = new Phrase("Subway",font10bold);
		PdfPCell nationalCell = new PdfPCell(national);
		nationalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nationalCell.setBorder(0);
		table.addCell(nationalCell);
		
		table.addCell(dottedCell);
		
		Phrase samsung = new Phrase("SNS Group ( Builders)",font10bold);
		PdfPCell samsungCell = new PdfPCell(samsung);
		samsungCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		samsungCell.setBorder(0);
		table.addCell(samsungCell);
		
		table.addCell(dottedCell);
		
		Phrase adani = new Phrase("Sangini Group (Builders)",font10bold);
		PdfPCell adaniCell = new PdfPCell(adani);
		adaniCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		adaniCell.setBorder(0);
		table.addCell(adaniCell);
		
		table.addCell(dottedCell);
		
		Phrase logistics = new Phrase("Milestone Group (Builders)",font10bold);
		PdfPCell logisticsCell = new PdfPCell(logistics);
		logisticsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		logisticsCell.setBorder(0);
		table.addCell(logisticsCell);
		
		table.addCell(dottedCell);
		
		Phrase infrastcure = new Phrase("Asutosh Hospital",font10bold);
		PdfPCell infrastcureCell = new PdfPCell(infrastcure);
		infrastcureCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		infrastcureCell.setBorder(0);
		table.addCell(infrastcureCell);
		
		table.addCell(dottedCell);
		
		Phrase welspun = new Phrase("Rainbow Hospital",font10bold);
		PdfPCell welspunCell = new PdfPCell(welspun);
		welspunCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		welspunCell.setBorder(0);
		table.addCell(welspunCell);
		table.addCell(dottedCell);
		
		
		Phrase iconHosp = new Phrase("Icon Hospital",font10bold);
		PdfPCell iconHospCell = new PdfPCell(iconHosp);
		iconHospCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		iconHospCell.setBorder(0);
		table.addCell(iconHospCell);
		table.addCell(dottedCell);
		
		Phrase childHospital = new Phrase("Param Children Hospital",font10bold);
		PdfPCell childHospitalCell = new PdfPCell(childHospital);
		childHospitalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		childHospitalCell.setBorder(0);
		table.addCell(childHospitalCell);
		table.addCell(dottedCell);
		
		Phrase welcareHospital = new Phrase("Welcare Hospital",font10bold);
		PdfPCell welcareHospitalCell = new PdfPCell(welcareHospital);
		welcareHospitalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		welcareHospitalCell.setBorder(0);
		table.addCell(welcareHospitalCell);
		table.addCell(dottedCell);
		
		
		Phrase tanishq = new Phrase("Tanishq  (Jewellers)",font10bold);
		PdfPCell tanishqCell = new PdfPCell(tanishq);
		tanishqCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		tanishqCell.setBorder(0);
		table.addCell(tanishqCell);
		table.addCell(dottedCell);
		
		
		Phrase kalyan = new Phrase("Kalyan (Jewellers)",font10bold);
		PdfPCell kalyanCell = new PdfPCell(kalyan);
		kalyanCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		kalyanCell.setBorder(0);
		table.addCell(kalyanCell);
		table.addCell(dottedCell);
		
		/** Date 23-03-2018 By vijay **/ 
		Phrase idpEducation = new Phrase("IDP Education India Pvt Ltd",font10bold);
		PdfPCell idpEducationCell = new PdfPCell(idpEducation);
		idpEducationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		idpEducationCell.setBorder(0);
		table.addCell(idpEducationCell);
		table.addCell(dottedCell);
		
		Phrase mercureDwarka = new Phrase("Mercure Dwarka Hotel",font10bold);
		PdfPCell mercureDwarkaCell = new PdfPCell(mercureDwarka);
		mercureDwarkaCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		mercureDwarkaCell.setBorder(0);
		table.addCell(mercureDwarkaCell);
		table.addCell(dottedCell);
		
		Phrase merrionHotel = new Phrase("Merrion Hotel",font10bold);
		PdfPCell merrionHotelCell = new PdfPCell(merrionHotel);
		merrionHotelCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		merrionHotelCell.setBorder(0);
		table.addCell(merrionHotelCell);
		table.addCell(dottedCell);
		
		/**
		 * ends here
		 */
		
		try {
			document.add(com.itextpdf.text.Chunk.NEXTPAGE);
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
