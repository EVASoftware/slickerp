package com.slicktechnologies.server.addhocprinting;


import static com.googlecode.objectify.ObjectifyService.ofy;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;

public class RenewalLetterPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2391882660747743855L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {

			String stringid1 = req.getParameter("ContractId");
			stringid1 = stringid1.trim();
			System.out.println("CompanyId::::::::::::::::::::"+stringid1.trim());
			int count = Integer.parseInt(stringid1);
			
			String stringid = req.getParameter("CompanyId");
			stringid = stringid.trim();
			System.out.println("CompanyId::::::::::::::::::::"+stringid.trim());
			Long companyId = Long.parseLong(stringid);

//ContractRenewal renew = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", count).first().now();
ContractRenewal wo = ofy().load().type(ContractRenewal.class).filter("companyId", companyId).filter("contractId", count).first().now();

			
			System.out.println("INSIDE renew:::::::::::::::::::::::::::::"
					+ wo);

			RenewalLetterPdf renewpdf = new RenewalLetterPdf();

			renewpdf.document = new Document();
			Document document = renewpdf.document;
			PdfWriter writer = PdfWriter.getInstance(document,
					resp.getOutputStream()); // write the pdf in response
			// if(!wo.getStatus().equals("Renewed")){
			// writer.setPageEvent(new PdfWatermark());
			//
			// }
			// else {
			// // if(!wo.getStatus().equals("Inprocessed")){
			// writer.setPageEvent(new PdfCancelWatermark());
			// }

			document.open();

			renewpdf.setpdfrenewal(wo);
			renewpdf.createPdf();

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}
}
