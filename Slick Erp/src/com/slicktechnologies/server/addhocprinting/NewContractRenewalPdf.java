package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contactidentification.ContactPersonIdentification;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class NewContractRenewalPdf {
	Logger logger = Logger.getLogger("NewContractRenewalPdf.class");
//	ContractRenewal con;
	Company comp;
	Customer cust;
	
	Contract con;
	ContactPersonIdentification conDetails;
	public Document document;
//	int noOfLine=2;
	int noOfLine=10;
	int productcount=0;
	int vat;
	int st;
	boolean checkEmailId=false;
	boolean checkOldFormat=false;
	List<Branch> branchList=new ArrayList<Branch>();//By Jayshree
//	List<CompanyPayment>comppayment=new ArrayList<CompanyPayment>();//By Jayshree
	CompanyPayment comppayment;
	private Font font16boldul, font12bold, font8bold, font8,font7bold, font16bold,font12boldul,font12,font14bold,font10,font10bold,font10boldul,font9,font9bold;

	Phrase chunk;
	PdfPCell pdfcode,pdfname,pdfduration,pdfservices,pdfprice,pdftax,pdfnetPay;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt3 = new SimpleDateFormat("MMM-yyyy");
	DecimalFormat df = new DecimalFormat("#.00");

	boolean oldConDetFlag=false;
	/*
	 * Date:10/11/2018
	 * Developer:Ashwini
	 * 
	 */
	Contract oldContract;
	 boolean pecoppflag = false;
	 boolean pepcoppflag = false;


	public NewContractRenewalPdf() {
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
	   font7bold = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);//added by ashwini
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD| Font.UNDERLINE);
		
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	}

	public void setContractRewnewal(Contract conRenewal){
		con=conRenewal;
		
		if (con.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class).filter("companyId", con.getCompanyId()).first().now();
		
		if (con.getCompanyId() == null)
			cust = ofy().load().type(Customer.class).filter("count", con.getCustomerId()).first().now();
		else
			cust = ofy().load().type(Customer.class).filter("companyId", con.getCompanyId()).filter("count", con.getCustomerId()).first().now();
		
		if (con.getCompanyId() == null)
			con = ofy().load().type(Contract.class).filter("count", con.getCount()).first().now();
		else
			con = ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count",con.getCount()).first().now();
		
		oldContract=ofy().load().type(Contract.class).filter("companyId", con.getCompanyId()).filter("count", con.getRefContractCount()).first().now();
		
		branchList=ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName",con.getBranch()).list();

		
		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", con.getCompanyId()).filter(" paymentStatus", true).first().now();
		
		if(con.getPaymentMode()!=null && con.getPaymentMode().trim().length()>0){
			List<String> paymentDt = Arrays.asList(con.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", con.getCompanyId()).first()
						.now();
				
			}
		}	
		
		conDetails=ofy().load().type(ContactPersonIdentification.class)
				.filter("companyId", con.getCompanyId()).filter("name", con.getPocName()).first().now();
		/**
		 * Date : 05-12-2017 BY ANIL
		 * Loading process configuration,for checking whether to show old contract details or not
		 */
		if(con.getCompanyId()!=null){
			ProcessConfiguration processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", con.getCompanyId()).filter("processName", "ContractRenewal").filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ShowOldContractDetails")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						oldConDetFlag=true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("ActiveBranchEmailId")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkEmailId = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OldFormatRenewalLetter")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						checkOldFormat = true;
					}
					
					/*
					 * Date:10/11/2018
					 * Added by Ashwini
					 * Des:To made changes for pecopp only
					 */
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyforPecopp")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pecoppflag = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyforPepcopp")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						pepcoppflag = true;
					}
					/*
					 * end by Ashwini
					 */
				}
			}
		}
		
if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){

			Branch branchDt = null;
			logger.log(Level.SEVERE,"Process active --");
			if(con !=null && con.getBranch() != null && con.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName", con.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", con.getCompanyId()).first()
								.now();
						
						
						if(comppayment != null){
							
						}
						
					}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}
		}
		
		
		
	}



	public void createPdf(String preprintStatus) {
		
		
		/*
		 * Date:10/11/2018
		 * Added by Ashwini
		 * Des:To add preprintstatus for pepcopp
		 */
		if(pecoppflag){
			if(preprintStatus.contains("plane")){
				Createblank();
//				createLogo(document,comp);
				createCompanyAddress();
			}else if(preprintStatus.contains("yes")){
				createBlankforUPC();
			}else if(preprintStatus.contains("no")){
				createBlankforUPC();
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
			}
		}else{
		
		
		if(preprintStatus.contains("plane")){
			Createblank();
//			createLogo(document,comp);
			createCompanyAddress();
		}else if(preprintStatus.contains("yes")){
			createBlankforUPC();
		}else if(preprintStatus.contains("no")){
			createBlankforUPC();
			if(comp.getUploadHeader()!=null){
		    	createCompanyNameAsHeader(document,comp);
			}
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
		}
		}
		
	
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg();
		if(checkOldFormat==true){
			if(oldConDetFlag){
				createOldContractProductDetails();
			}
	   createproductHeading();
	   createProductInfonew();
	   if(noOfLine!=0){
	   cretetotal();
	   footerInfo();
	   footerDetails();
	   }
		if(noOfLine==0&&productcount==0){
			 cretetotal();
			 footerInfo();
			 footerDetails();
		}
			/*
			 * commented by Ashwini
			 */
			
			if(pepcoppflag==false){
				
				if(noOfLine==0&&productcount!=0){
					try {
						document.add(Chunk.NEXTPAGE);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 if(preprintStatus.contains("yes")){
						createBlankforUPC();
					}else if(preprintStatus.contains("no")){
						createBlankforUPC();
						if(comp.getUploadHeader()!=null){
					    	createCompanyNameAsHeader(document,comp);
						}
						if(comp.getUploadFooter()!=null){
							createCompanyNameAsFooter(document,comp);
						}
					}
					Phrase ref = new Phrase("Annexure  ", font10bold);
					Paragraph pararef = new Paragraph();
					pararef.add(ref);
					pararef.setSpacingAfter(15);
				   
				    try {
						document.add(pararef);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
									e.printStackTrace();
		}
				    
				    createRemainingProduct(productcount);
				    cretetotal();
				    footerInfo();
				    footerDetails();
					}
				
			}
		}
		
		else{
			footerInfo();
			
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 if(preprintStatus.contains("yes")){
				createBlankforUPC();
			}else if(preprintStatus.contains("no")){
				createBlankforUPC();
				if(comp.getUploadHeader()!=null){
			    	createCompanyNameAsHeader(document,comp);
				}
				if(comp.getUploadFooter()!=null){
					createCompanyNameAsFooter(document,comp);
				}
			}
			Phrase ref = new Phrase("Annexure  ", font10bold);
			Paragraph pararef = new Paragraph();
			pararef.add(ref);
			pararef.setSpacingAfter(15);
		   
		    try {
				document.add(pararef);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    if(oldConDetFlag){
				createOldContractProductDetails();
			}
			createproductHeading();
			createProductInfonew();
			cretetotal();
			
			
		}
	    
	    
		
		
	}


	private void createRemainingProduct(int productcount2) {
//		
		
		
		if(pecoppflag){
			
			createproductHeading1();
			
		}else{
			createproductHeading();
		}
		
		
		PdfPTable table2 ;
		if(pecoppflag){
			 table2 = new PdfPTable(6); //Added by Ashwini
		}else{
			 table2 = new PdfPTable(7);
		}
		
		
		/**end**/
		table2.setWidthPercentage(100);
		try {
			if(pecoppflag){
				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f}); //Added by Ashwini
			}else{
				table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f});
			}
			
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		for(int i=productcount2;i<con.getItems().size();i++){
//			
//			
//			
//			/**
//			 * Date 5-6-2018 by jayshree
//			 * des.to print the old taxincluding amt
//			 */
//			double oldTaxinclAmt;
//			if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
//				oldTaxinclAmt=con.getItems().get(i).getOldProductPrice();
//	     	}
//			else{
//			
//			 oldTaxinclAmt = calculateOldTaxAmt(con.getItems().get(i).getOldProductPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
//					 con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
//			}
//			
//			
//			Phrase pdfoldprice;
//			if(con.getItems().get(i).getOldProductPrice()!=0){
//				pdfoldprice = new Phrase(oldTaxinclAmt+"",font9);
//			}else{
//				pdfoldprice=new Phrase("");
//			}
//			
//			
//			/**End******/
//			
//			////
//			if(con.getItems().get(i).getProductName()!=null){
//				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
//			}else{
//				chunk = new Phrase("");
//			}
//			
//			pdfname = new PdfPCell(chunk);
//			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
//			 
//			 /////
//			if(con.getItems().get(i).getDuration()!=-1){
//				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
//			}
//			else{
//				 chunk= new Phrase("");
//			}
//			pdfduration = new PdfPCell(chunk);
//			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			if(pecoppflag){
//			if(con.getItems().get(i).getPrduct().getSpecification()!=null
//					&& con.getItems().get(i).getPrduct().getSpecification().trim().length()>0){
//			 chunk =new Phrase (con.getItems().get(i).getPrduct().getSpecification()+"",font9);
//			 System.out.println("Product Type::"+con.getItems().get(i).getPrduct().getSpecification());
//			}else{
//				chunk= new Phrase("");
//
//			}
//			}else{
//				if(con.getItems().get(i).getNumberOfServices()!=-1){
//					 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
//				}
//				else{
//					 chunk= new Phrase("");
//				}
//			}
//			pdfservices = new PdfPCell(chunk);
//			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////	
//			
//			/**
//			 * Date 5-6-2018 by jayshree
//			 * des.add new price tax including amt
//			 */
//			
//		double newTaxAmount=0 ;
//	 	
//	 	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
//	 		newTaxAmount=con.getItems().get(i).getPrice();
//	 	}
//	 	else{
//	 	
//	 		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
//				con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
//	 	}
//			
//			if(con.getItems().get(i).getPrice()!=0){
//				chunk = new Phrase(newTaxAmount+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfprice = new PdfPCell(chunk);
//			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//		   	 PdfPCell pdfservice =null;
//	      	 Phrase vatno=null;
//	      	 Phrase stno=null;
//	      	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
//	      	 {
//	      	 
//	      		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
//	      		 if(st==con.getItems().size()){
//	      			 chunk = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	      		 }
//	      		 else{
//	      			 stno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	      		 }
//	      		 }
//	      	    else
//	      		 chunk = new Phrase("N.A"+"",font9);
//	      		 if(st==con.getItems().size()){
//	      	    pdfservice = new PdfPCell(chunk);
//	      		 }else{
//	      			 pdfservice = new PdfPCell(stno);
//	      		 }
//	      	  
//	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      	 }
//	      	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
//	           {
//	      		 
//	      		 if(con.getItems().get(i).getVatTax()!=null){
//	      			 if(vat==con.getItems().size()){
//	      				 System.out.println("rohan=="+vat);
//	      				 chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
//	      			 }else{
//	      				 System.out.println("mukesh");
//	           	    vatno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage(),font9);
//	      		 }
//	      		 }
//	           	    else{
//	           		 chunk = new Phrase("N.A"+"",font9);
//	           	    }
//	      		 if(vat==con.getItems().size()){
//	           	  pdfservice = new PdfPCell(chunk);
//	      		 }else{ pdfservice = new PdfPCell(vatno);}
//	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      	 }
//	      	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
//	      		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
//	      			 
//	            	    chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
//	            	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
//	            	 else
//	            		 chunk = new Phrase("N.A"+"",font9);
//	            	  pdfservice = new PdfPCell(chunk);
//	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      		 
//	      	 }else{
//	      		 
//	      		 chunk = new Phrase("N.A"+"",font9);
//	            	  pdfservice = new PdfPCell(chunk);
//	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
//	      		 
//	      	 }
//			
//	      	pdftax = new PdfPCell(chunk);
//	      	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//			
//	      	/**   Date : 24-11-2017 BY MANISHA
//	  	   * Description :To get the total amount on pdf..!!
//	  	   * **/
//	      	
//			double netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
//					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
//			
//			/**Ends**/
//			
//			if(netPayAmount!=0){
//				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
//			}else{
//				 chunk = new Phrase(" ",font9);
//			}
//			pdfnetPay = new PdfPCell(chunk);
//			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//			/**
//			 * Date 4/1/2018
//			 * By Jayshree
//			 * Des.To add the start date and end date
//			 */
//			
//			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
//			PdfPCell startcell=new PdfPCell(startph);
//			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			table2.addCell(pdfname);
//			table2.addCell(startph);
//			table2.addCell(pdfduration);
//			table2.addCell(pdfservices);
//			/**Manisha add column old price **/
//		
//			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
//			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
////			table2.addCell(oldpricevalcell);
//			
//			/**End**/
//			table2.addCell(pdfprice);
//			/*
//			 * Date:16-10-2018
//			 * Developer:Ashwini
//			 */
////			if(pecoppflag==false){
//				table2.addCell(pdftax);
//				table2.addCell(pdfnetPay);
//				
////			}
//		}
           double consolidatedProdPrice = 0;
        
		for (int j = 0; j < con.getItems().size(); j++) {
			double calculatedPrice = con.getItems().get(j).getPrice();
			consolidatedProdPrice = consolidatedProdPrice + calculatedPrice;
		}
		double consolidatednetPayAmount=0;
		for (int k = 0; k < con.getItems().size(); k++) {
		double netPayAmount=0 ;
		
	     	if(con.getItems().get(k).getPrduct().getVatTax().isInclusive() && con.getItems().get(k).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(k).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(k).getPrice(),con.getItems().get(k).getServiceTax().getPercentage(),con.getItems().get(k).getVatTax().getPercentage(),
	        		 con.getItems().get(k).getVatTax().getTaxPrintName(),con.getItems().get(k).getServiceTax().getTaxPrintName());
	     	}
	     	consolidatednetPayAmount=consolidatednetPayAmount+netPayAmount;
		}
		for(int i=productcount2;i<con.getItems().size();i++){
			
			System.out.println("No of lines::"+noOfLine);
			System.out.println("product count::"+productcount);
			
			/*
			 *Date:12/10/2018
			 * Added by Ashwini
			 */
//			if(pepcoppflag==false){
//				
//				if(checkOldFormat==true){
//					if(noOfLine==0){
//						System.out.println("no of lins");
//						productcount=i;
//						break;
//						
//					}
//					noOfLine=noOfLine-1;
//					}
//			}
			
//			
			Phrase pdfoldprice;
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(con.getItems().get(i).getOldProductPrice())+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			
			/**End******/

			
			
			////
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
	     				con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+con.getItems().get(i).getPrice());
	     	//End By Jayshreee
			
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
//	     	String prodPrice=;
	     	Phrase prodPrices=null;
	     	if(checkOldFormat){
	     		if(con.getItems().get(i).getPrice()!=0){
	     			prodPrices = new Phrase(df.format(con.getItems().get(i).getPrice()), font9);
				}else{
					prodPrices = new Phrase("0");
				}	
	     	}else{
	     		if(con.getItems().get(i).getPrice()!=0){
	     			prodPrices = new Phrase(df.format(newTaxAmount)+"", font9);
				}else{
					prodPrices = new Phrase("0");
				}	
	     	}
//	     	pdfnetPay = new PdfPCell(chunk);
			///// 
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
	     			chunktax = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(con.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==con.getItems().size()){
	     			 if(con.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==con.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
	        		 con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
//			pdfnetPay = new PdfPCell(netpayable);
//			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
			
			Calendar c = Calendar.getInstance();
			c.setTime(con.getItems().get(i).getStartDate());
			c.add(Calendar.DATE, con.getItems().get(i).getDuration() - 1);
			Date endDt = c.getTime();
			/**
			 * Date : 05/09/2017 BY ANIL
			 */
			String date = "";
			if (con.getItems().get(i).getEndDate() != null) {
				date = fmt.format(con.getItems().get(i).getStartDate()) + " To "+ fmt.format(con.getItems().get(i).getEndDate());
			} else {
				date = fmt.format(con.getItems().get(i).getStartDate()) + " To "+ fmt.format(endDt);
			}
			
			
			
			
			
//			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			Phrase startph=new Phrase(date, font9);
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table2.addCell(pdfname);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			

			if(con.isConsolidatePrice()){

				if (i == 0) {
					chunk = new Phrase(df.format(consolidatedProdPrice), font9);
					pdfprice = new PdfPCell(chunk);
					if(con.getItems().size() > 1)
						pdfprice.setBorderWidthBottom(0);
					    pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdfprice);
				} else {
					chunk = new Phrase(" ", font9);
					pdfprice = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdfprice.setBorderWidthTop(0);
					}else{
						pdfprice.setBorderWidthBottom(0);
						pdfprice.setBorderWidthTop(0);
					}
					pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfprice);
				}
				
			}else{
				pdfprice = new PdfPCell(prodPrices);
				pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdfprice);
			}
//			table2.addCell(pdfprice);
			/***for taxes**/
			if(con.isConsolidatePrice()){

				if (i == 0) {
					
					pdftax = new PdfPCell(chunktax);
					if(con.getItems().size() > 1)
						pdftax.setBorderWidthBottom(0);
					    pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdftax);
				} else {
					chunk = new Phrase(" ", font9);
					pdftax = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdftax.setBorderWidthTop(0);
					}else{
						pdftax.setBorderWidthBottom(0);
						pdftax.setBorderWidthTop(0);
					}
					pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdftax);
				}
				
			}else{
				pdftax = new PdfPCell(chunktax);
		     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdftax);
			}
//			table2.addCell(pdftax);
			/***for net payable***/
			
			if(con.isConsolidatePrice()){

				if (i == 0) {
					chunk = new Phrase(df.format(consolidatednetPayAmount), font9);
					pdfnetPay = new PdfPCell(chunk);
					if(con.getItems().size() > 1)
						pdfnetPay.setBorderWidthBottom(0);
					    pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdfnetPay);
				} else {
					chunk = new Phrase(" ", font9);
					pdfnetPay = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdfnetPay.setBorderWidthTop(0);
					}else{
						pdfnetPay.setBorderWidthBottom(0);
						pdfnetPay.setBorderWidthTop(0);
					}
					pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfnetPay);
				}
				
			}else{
				pdfnetPay = new PdfPCell(netpayable);
				pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdfnetPay);
			}
//			table2.addCell(pdfnetPay);
			
			
			
		}
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	private double calculateNewTaxAmt(double newPrice , double serTax , double vatTax,String vatname,String sertaxname) {
		
		
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = newPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = newPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  newPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = newPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
		
		return totalAmt;
	}

	private double calculateOldTaxAmt(double oldPrice , double serTax , double vatTax,String vatname,String sertaxname) {
		
		
		double totalAmt= 0;
		double vatTaxAmt =0;
		double setTaxAmt =0;
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = oldPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = oldPrice * (serTax/100);
				totalAmt = oldPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = oldPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = oldPrice * (serTax/100);
				totalAmt = oldPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  oldPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = oldPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
		
		return totalAmt;
	}

	private void cretetotal() {
		// TODO Auto-generated method stub
		/**   Date : 24-11-2017 BY MANISHA
		   * Description :To get the total amount on pdf..!!
		   * **/
		PdfPTable table3 ;
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		if(pecoppflag){
		 table3 = new PdfPTable(6);//Added by Ashwini
		}else{
		table3 = new PdfPTable(7);
		}
		/**end**/
		table3.setWidthPercentage(100);
		try {
			if(pecoppflag){
				table3.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
			}else{
			table3.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f});
			}
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/**
		 * Date 9/1/2018
		 * By Jayshree
		 * Des.To add the amount in words 
		 */
		String amount=SalesInvoicePdf.convert(con.getNetpayable());
		String amtInWordsVal = "Amount in words : Rupees "+amount.toLowerCase()+ " only/-";
		Phrase amtInWordsValphrase = new Phrase(amtInWordsVal, font1);

		PdfPCell amtInWordsValCell = new PdfPCell(amtInWordsValphrase);
		amtInWordsValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		if(pecoppflag){
			amtInWordsValCell.setColspan(4);	
		}else{
			amtInWordsValCell.setColspan(5);
		}
		
		table3.addCell(amtInWordsValCell);
		
		/*
		 * Added by Ashwini
		 */
		
	
			
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table3.addCell(totcell);
		
		Phrase totalval=new Phrase(df.format(con.getNetpayable())+"",font9);		// change by Viraj for decimal on Date 25-10-2018
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table3.addCell(totalvalcell);
		
		
		
		
		
		try {
			document.add(table3);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**End for Manisha**/
	}
	
	/*
	 * Date:10/11/2018
	 * Added by Ashwini
	 * Des:To add note coloumn
	 */
	
	private void createnote(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 10 ,Font.BOLD);
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		String  note = "Note : Price will be inclusive of applicable taxes" ;
		Phrase notephrase = new Phrase(note,font1);
		PdfPCell notecell = new PdfPCell(notephrase);
		notecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		notecell.setBorder(0);
		
		parent.addCell(notecell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
	
	private void createProductInfonew1(){
		

		PdfPTable table2 = new PdfPTable(6);
		/**end**/
		table2.setWidthPercentage(100);
		try {
			table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(int i=0;i<con.getItems().size();i++){
			
			
			
			if(checkOldFormat==true){
			if(noOfLine==0){
				System.out.println("no of lins");
				productcount=i;
				break;
			}
			noOfLine=noOfLine-1;
			}

			Phrase pdfoldprice;
			/** Added by Viraj For Amt including Tax Calculation of oldPrice and newPrice **/
			
				double oldPrice = con.getItems().get(i).getOldProductPrice();
				double serOldPriceTax = oldPrice * (con.getItems().get(i).getServiceTax().getPercentage()/100);
				double vatOldPriceTax = oldPrice * (con.getItems().get(i).getVatTax().getPercentage()/100);
				System.out.println("ServiceTax: "+ serOldPriceTax);
				System.out.println("vatTax: "+ vatOldPriceTax);
				oldPrice = Math.round( con.getItems().get(i).getOldProductPrice() + serOldPriceTax + vatOldPriceTax );
			/** Ends **/
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(oldPrice)+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			System.out.println("oldPrice: "+pdfoldprice);
		
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
		
				System.out.println("Product Type::"+con.getItems().get(i).getPrduct().getSpecification());
				
				if(con.getItems().get(i).getPrduct().getSpecification()!=null
						&& con.getItems().get(i).getPrduct().getSpecification().trim().length()>0){
				 chunk =new Phrase (con.getItems().get(i).getPrduct().getSpecification()+"",font9);
				 System.out.println("Product Type::"+con.getItems().get(i).getPrduct().getSpecification());
				}
			
			
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+con.getItems().get(i).getPrice());
	     	
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
	     	if(checkOldFormat){
	     		if(con.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}else{
	     		if(con.getItems().get(i).getPrice()!=0){
					chunk = new Phrase(df.format(Math.round(newTaxAmount))+"", font9);
				}else{
					 chunk = new Phrase("0");
				}	
	     	}
			/**
			 * ends here
			 */
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
		
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
//	     		 if(st==con.getItems().size()){
	     			chunktax = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
//	     		 else{
//	     			 stno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
//	     		 }
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     		 if(st==con.getItems().size()){
	     	    pdfservice = new PdfPCell(chunktax);
//	     		 }else{
//	     			 pdfservice = new PdfPCell(stno);
//	     		 }
	     	     /**
	     		  * ends here
	     		  */
	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(con.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==con.getItems().size()){
	     			 if(con.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 System.out.println("mukesh");
//	          	    vatno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage(),font9);
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==con.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	pdftax = new PdfPCell(chunktax);
	     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(df.format(netPayAmount)+"", font9);  // change by Viraj for decimal on Date 25-10-2018
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
			pdfnetPay = new PdfPCell(netpayable);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
			Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table2.addCell(pdfname);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			/**Manisha add column old price **/
		
			
			
			
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			table2.addCell(oldpricevalcell);
			
			/**End**/
			table2.addCell(pdfprice);
//			table2.addCell(pdftax);
//			table2.addCell(pdfnetPay);
			
			
			
		}
		
		
		
		if(checkOldFormat==true){
			if(noOfLine==0&&productcount!=0){
				Phrase remainph=new Phrase ("Please Refer annexure For Remaining Product",font9bold);
				PdfPCell remaincell=new PdfPCell(remainph);
				remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
				remaincell.setColspan(8);
				table2.addCell(remaincell);
			}
		}
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		
	}

	private void createProductInfonew() {
		
		
		
		/**
		 * Date 4/1/2018 
		 * By Jayshree
		 * Des.to add the product details in new tables
		 */
		
		
		
		PdfPTable table2 = new PdfPTable(7);
		/**end**/
		table2.setWidthPercentage(100);
		try {
			table2.setWidths(new float[]{2.5f,1.5f,1f,1f,1f,1f,1f});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		/***for consolidated price**/
		double consolidatedProdPrice = 0;
        
		for (int j = 0; j < con.getItems().size(); j++) {
			double calculatedPrice = con.getItems().get(j).getPrice();
			consolidatedProdPrice = consolidatedProdPrice + calculatedPrice;
		}
		double consolidatednetPayAmount=0;
		for (int k = 0; k < con.getItems().size(); k++) {
		double netPayAmount=0 ;
		
	     	if(con.getItems().get(k).getPrduct().getVatTax().isInclusive() && con.getItems().get(k).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(k).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(k).getPrice(),con.getItems().get(k).getServiceTax().getPercentage(),con.getItems().get(k).getVatTax().getPercentage(),
	        		 con.getItems().get(k).getVatTax().getTaxPrintName(),con.getItems().get(k).getServiceTax().getTaxPrintName());
	     	}
	     	consolidatednetPayAmount=consolidatednetPayAmount+netPayAmount;
		}
		
		for(int i=0;i<con.getItems().size();i++){
			
			System.out.println("No of lines::"+noOfLine);
			System.out.println("product count::"+productcount);
			
			/*
			 *Date:12/10/2018
			 * Added by Ashwini
			 */
			if(pepcoppflag==false){
				
				if(checkOldFormat==true){
					if(noOfLine==0){
						System.out.println("no of lins");
						productcount=i;
						break;
						
					}
					noOfLine=noOfLine-1;
					}
			}
			
//			
			Phrase pdfoldprice;
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(df.format(con.getItems().get(i).getOldProductPrice())+"",font9);
			}else{
				pdfoldprice=new Phrase("0");
			}
			
			/**End******/

			
			
			////
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
//            Phrase startph=new Phrase (fmt.format(con.getStartDate())+"\n"+fmt.format(con.getEndDate()),font9);
			Calendar c = Calendar.getInstance();
			c.setTime(con.getItems().get(i).getStartDate());
			c.add(Calendar.DATE, con.getItems().get(i).getDuration() - 1);
			Date endDt = c.getTime();
            String date = "";
			if (con.getItems().get(i).getEndDate() != null) {
				date = fmt.format(con.getItems().get(i).getStartDate()) +"\n"+ fmt.format(con.getItems().get(i).getEndDate());
			} else {
				date = fmt.format(con.getItems().get(i).getStartDate()) + " To "+ fmt.format(endDt);
			}
            
            
			 Phrase startph=new Phrase (date,font9);
			PdfPCell startcell=new PdfPCell(startph);
			startcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
			
			
			
			
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			
			/**
			 * Date 
			 */
			double newTaxAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		newTaxAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	     		newTaxAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
	     				con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			
	     	System.out.println("new prise"+con.getItems().get(i).getPrice());
	     	//End By Jayshreee
			
	     	
	     	/**
	     	 * Date 02-08-2018 By Vijay
	     	 * Des :- old contract pdf new amount only dislay on new price column and old code should show including taxes so old code added in else block
	     	 * as per sonu. 
	     	 */
//	     	String prodPrice=;
	     	Phrase prodPrices=null;
	     	if(checkOldFormat){
	     		if(con.getItems().get(i).getPrice()!=0){
	     			prodPrices = new Phrase(df.format(con.getItems().get(i).getPrice()), font9);
				}else{
					prodPrices = new Phrase("0");
				}	
	     	}else{
	     		if(con.getItems().get(i).getPrice()!=0){
	     			prodPrices = new Phrase(df.format(newTaxAmount)+"", font9);
				}else{
					prodPrices = new Phrase("0");
				}	
	     	}
//	     	pdfnetPay = new PdfPCell(chunk);
			///// 
			
		   	 PdfPCell pdfservice =null;
	     	 Phrase vatno=null;
	     	 Phrase stno=null;
	     	 
	     	Phrase chunktax = null;
	     	
	     	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	     	 {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     	 
	     		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
	     			chunktax = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	     		 }
	     	    else
	     	    	chunktax = new Phrase("N.A"+"",font9);
//	     	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	          {
	     		/** Date :- 03-08-2018 By Vijay
	     		  *  Des :- below old commented for proper tax caculation print on pdf
	     		  */
	     		 
	     		 if(con.getItems().get(i).getVatTax()!=null){
//	     			 if(vat==con.getItems().size()){
	     			 if(con.getItems().get(i).getVatTax().getPercentage()>0){
	     				 System.out.println("rohan=="+vat);
	     				chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	     			 }else{
	     				 chunktax = new Phrase("N.A"+"",font9);

	     			 }
	     		 }
	          	    else{
	          	    	chunktax = new Phrase("N.A"+"",font9);
	          	    }
//	     		 if(vat==con.getItems().size()){
	          	  pdfservice = new PdfPCell(chunktax);
//	     		 }else{ pdfservice = new PdfPCell(vatno);}
	          	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	          	 
	          	 /**
	     		  * ends here
	     		  */
	     	 }
	     	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	     		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	     			 
	     			chunktax = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	           	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	           	 else
	           		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }else{
	     		 
	     		chunktax = new Phrase("N.A"+"",font9);
	           	  pdfservice = new PdfPCell(chunktax);
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	     		 
	     	 }
			
	     	
			
			
			
	     	/**   Date : 24-11-2017 BY MANISHA
	 	   * Description :To get the total amount on pdf..!!
	 	   * **/
	     	/**
	     	 * date 6-2-2018 by jayshree
	     	 */
	     	double netPayAmount=0 ;
	     	
	     	if(con.getItems().get(i).getPrduct().getVatTax().isInclusive() && con.getItems().get(i).getPrduct().getServiceTax().isInclusive()){
	     		netPayAmount=con.getItems().get(i).getPrice();
	     	}
	     	else{
	     	
	         netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
	        		 con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
	     	}
			/**Ends**/
			
	     	Phrase netpayable = null;
			if(netPayAmount!=0){
				netpayable = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				/** Date 03-08-2018 By Vijay if price is 0 then will print in pdf 0 ***/
				netpayable = new Phrase("0",font9);
			}
//			pdfnetPay = new PdfPCell(netpayable);
//			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			/**
			 * Date 4/1/2018
			 * By Jayshree
			 * Des.To add the start date and end date
			 */
			
			
			
			table2.addCell(pdfname);
			table2.addCell(startcell);
			table2.addCell(pdfduration);
			table2.addCell(pdfservices);
			

			if(con.isConsolidatePrice()){

				if (i == 0) {
					chunk = new Phrase(df.format(consolidatedProdPrice), font9);
					pdfprice = new PdfPCell(chunk);
					if(con.getItems().size() > 1)
						pdfprice.setBorderWidthBottom(0);
					    pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdfprice);
				} else {
					chunk = new Phrase(" ", font9);
					pdfprice = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdfprice.setBorderWidthTop(0);
					}else{
						pdfprice.setBorderWidthBottom(0);
						pdfprice.setBorderWidthTop(0);
					}
					pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfprice);
				}
				
			}else{
				pdfprice = new PdfPCell(prodPrices);
				pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdfprice);
			}
//			table2.addCell(pdfprice);
			/***for taxes**/
			if(con.isConsolidatePrice()){

				if (i == 0) {
					
					pdftax = new PdfPCell(chunktax);
					if(con.getItems().size() > 1)
						pdftax.setBorderWidthBottom(0);
					    pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdftax);
				} else {
					chunk = new Phrase(" ", font9);
					pdftax = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdftax.setBorderWidthTop(0);
					}else{
						pdftax.setBorderWidthBottom(0);
						pdftax.setBorderWidthTop(0);
					}
					pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdftax);
				}
				
			}else{
				pdftax = new PdfPCell(chunktax);
		     	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdftax);
			}
//			table2.addCell(pdftax);
			/***for net payable***/
			
			if(con.isConsolidatePrice()){

				if (i == 0) {
					chunk = new Phrase(df.format(consolidatednetPayAmount), font9);
					pdfnetPay = new PdfPCell(chunk);
					if(con.getItems().size() > 1)
						pdfnetPay.setBorderWidthBottom(0);
					    pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
					    table2.addCell(pdfnetPay);
				} else {
					chunk = new Phrase(" ", font9);
					pdfnetPay = new PdfPCell(chunk);
					if(i == con.getItems().size()-1){							
						pdfnetPay.setBorderWidthTop(0);
					}else{
						pdfnetPay.setBorderWidthBottom(0);
						pdfnetPay.setBorderWidthTop(0);
					}
					pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
					table2.addCell(pdfnetPay);
				}
				
			}else{
				pdfnetPay = new PdfPCell(netpayable);
				pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
				table2.addCell(pdfnetPay);
			}
//			table2.addCell(pdfnetPay);
			
			
			
		}
		
		/*
		 * commented by Ashwini
		 */
//		if(checkOldFormat==true){
//			if(noOfLine==0&&productcount!=0){
//				Phrase remainph=new Phrase ("Please Refer annexure For Remaining Product",font9bold);
//				PdfPCell remaincell=new PdfPCell(remainph);
//				remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				remaincell.setColspan(8);
//				table2.addCell(remaincell);
//			}
//		}
		
		/*
		 * Date:12/10/2018
		 * Developer:Ashwini
		 * Des:To add products in same page(only for pepcop)
		 */
		if(pepcoppflag==false){
			
			if(checkOldFormat==true){
				if(noOfLine==0&&productcount!=0){
					Phrase remainph=new Phrase ("Please Refer annexure For Remaining Product",font9bold);
					PdfPCell remaincell=new PdfPCell(remainph);
					remaincell.setHorizontalAlignment(Element.ALIGN_LEFT);
					remaincell.setColspan(8);
					table2.addCell(remaincell);
				}
			}
			
		}
		
		/*
		 * end by Ashwini
		 */
		try {
			document.add(table2);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		  
		
		 
	}
	
	
	/*
	 *Date:12/10/2018
	 * Added by Ashwini
	 * Des:To change the heading for pecopp
	 */
	
	private void createproductHeading1(){
		
Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f};
		
		PdfPTable table = new PdfPTable(6);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Phrase name = new Phrase("NAME", font1);
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase startdate=new Phrase ("START/END DATE",font1);
		PdfPCell startdateph = new PdfPCell(startdate);
		startdateph.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase duration = new Phrase("DURATION",font1);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase services = new Phrase("SERVICES", font1);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase oldprice=new Phrase("PREVIOUS PRICE",font1);
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase price = new Phrase("NEW PRICE", font1);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		
		
		table.addCell(cellname);
		table.addCell(startdateph);
		table.addCell(cellduration);
		table.addCell(cellservices);
		table.addCell(celloldprice);
		table.addCell(cellprice);
		
		try{
			document.add(table);
			
		}catch(DocumentException e1){
			e1.printStackTrace();
			
		}
		
}

	/*
	 * end by Ashwini
	 */
	private void createproductHeading() {
		
	Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
//		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f,1f,1f};
		float[] relativeWidths = {2.5f,1.5f,1f,1f,1f,1f,1f};
		PdfPTable table = new PdfPTable(7);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION (IN DAYS)",font1);
		Phrase services = new Phrase("SERVICES", font1);
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		
		//Phrase oldprice=new Phrase("OLD PRICE",font1);  //commented by Ashwini
		/*
		 * Date:12/10/2018
		 * Developer:Ashwini
		 */
		Phrase oldprice = null;
		if(pepcoppflag){
			 oldprice=new Phrase("PREVIOUS PRICE",font1);
		}else{
			 oldprice=new Phrase("OLD PRICE",font1);
		}
		
		/*
		 * end by Ashwini
		 */
		Phrase price = new Phrase("RATE", font1);
		/**enD**/
		
		/** Date 4/1/2018
		 * By jayshree
		 * Des.add startdate and end date column
		 */
		Phrase startdate=new Phrase ("START/END DATE",font1);
		
		
		   Phrase servicetax= null;
		      Phrase servicetax1= null;
		      int flag=0;
		      int vat=0;
		      int st=0;
		      
		      /*Date :22/11/2017  By:Manisha
		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
		       */
		   for(int i=0; i<con.getItems().size();i++){
		    	  
		    if (con.getItems().get(i).getVatTax().getTaxPrintName().trim()!= null
							&& !con.getItems().get(i).getServiceTax().getTaxPrintName().trim().equalsIgnoreCase(""))
		    {
		    	
		    	       if(con.getItems().get(i).getVatTax().getTaxPrintName().trim()!=null
		    			&& !con.getItems().get(i).getServiceTax().getTaxPrintName().trim().equalsIgnoreCase(""))
		    			{
		    		
		    		       servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"/"
		    		                    +con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
		    		       flag=flag+1;
		    			} 
		    	       else
		    	       {
		    	    	   
		    	    	   servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
		    	    	   vat=vat+1;
		    	    	   
		    	       }
		    	
		    	
		    }
		    
		    else{
		    	   
		    	if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()==0)){
		  	       servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+"%",font1);
		  	       vat=vat+1;
		  	       System.out.println("phrase value===="+servicetax.toString());
		  	      }
		    	

			      else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()==0)
			    		  &&(con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
			      {
			      	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  st=st+1;
			      }
		    	
			      else if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0)
			    		  && (con.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (con.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
			    		 && (con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
			       {
			      	  servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  flag=flag+1;
			      	  System.out.println("flag value;;;;;"+flag);
			      }
		    	 
			    else if ((con.getItems().get(i).getServiceTax().getPercentage()>0) && (con.getItems().get(i).getVatTax().getPercentage()==0)
			    		&&(con.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!con.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
			    {
			    	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
			    	  st=st+1;
				}else{
			    	  servicetax = new Phrase(" ",font1);

				}
		    }
		     
		      
		      
//		       else{
//		      	 
//		      	  servicetax = new Phrase("TAX %",font1);
//		       }
		    }
		      
		  Phrase servicetaxes = new Phrase("SGST/CGST%",font1);
		      
		
		  	PdfPCell celltax = new PdfPCell(servicetaxes);
		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/** Date 4/1/2018
		 * By jayshree
		 * Des.add startdate and end date column
		 */
		PdfPCell startdateph = new PdfPCell(startdate);
		startdateph.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellvat = new PdfPCell(vat);
//		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellstax = new PdfPCell(stax);
//		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/**Manisha add column old price **/
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**End**/
		
		table.addCell(cellname);
		table.addCell(startdateph);
		table.addCell(cellduration);
		table.addCell(cellservices);
		/**Manisha add column old price **/
//		table.addCell(celloldprice);
		/**end*/
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private void createOldContractProductDetails() {

		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		Phrase oldContDetPh = new Phrase("Old contract details : ", font9bold);
		PdfPCell oldContDetCell = new PdfPCell(oldContDetPh);
		oldContDetCell.setBorder(0);
		
		
		
		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f};
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		Phrase price = new Phrase("PRICE", font1);
		
		
		
		HashSet<String> taxName=new HashSet<String>();
		for(ProductOtherCharges tax:con.getProductTaxes()){
			taxName.add(tax.getChargeName().trim());
		}
		Iterator<String> it = taxName.iterator();
		String taxHeader="";
	    while(it.hasNext()){
	    	taxHeader=taxHeader+it.next()+"/";
	    }
	    if(!taxHeader.equals("")){
	    	taxHeader=taxHeader.substring(0,taxHeader.length()-1);
	    }
	    Phrase servicetax= new Phrase(taxHeader,font1);	
	  	PdfPCell celltax = new PdfPCell(servicetax);
	  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(cellname);
		table.addCell(cellduration);
		table.addCell(cellservices);
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		for(int i=0;i<con.getItems().size();i++){
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if(con.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(con.getItems().get(i).getPrice()+"", font9);
			}else{
				chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			if ((con.getItems().get(i).getVatTax().getPercentage() == 0)
					&& (con.getItems().get(i).getServiceTax().getPercentage() > 0)) {

				if (con.getItems().get(i).getServiceTax().getPercentage() != 0) {
						chunk = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+ "", font9);
				}else{
					chunk = new Phrase("N.A" + "", font9);
				}
			} else if ((con.getItems().get(i).getServiceTax().getPercentage() == 0)
					&& (con.getItems().get(i).getVatTax().getPercentage() > 0)) {

				if (con.getItems().get(i).getVatTax() != null) {
					chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+ "", font9);
				} else {
					chunk = new Phrase("N.A" + "", font9);
				}
			} else if ((con.getItems().get(i).getServiceTax().getPercentage() > 0)
					&& (con.getItems().get(i).getVatTax().getPercentage() > 0)) {
				if ((con.getItems().get(i).getVatTax() != null)&& (con.getItems().get(i).getServiceTax() != null)){
					chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()
							+ ""+ " / "+ ""+ con.getItems().get(i).getServiceTax().getPercentage(), font9);
				}else{
					chunk = new Phrase("N.A" + "", font9);
				}
			} else {
				chunk = new Phrase("N.A" + "", font9);
			}
			pdftax = new PdfPCell(chunk);
			pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);

			double netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
			
			
			if(netPayAmount!=0){
				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				chunk = new Phrase(" ",font9);
			}
			pdfnetPay = new PdfPCell(chunk);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			table.addCell(pdfname);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			
			table.addCell(pdfprice);
			table.addCell(pdftax);
			table.addCell(pdfnetPay);
		}
		

		Phrase blkph1=new Phrase("");
		PdfPCell blkcell=new PdfPCell(blkph1);
		blkcell.setBorderWidthRight(0);
		table.addCell(blkcell);
		
		Phrase blkph2=new Phrase("");
		PdfPCell blkcell2=new PdfPCell(blkph2);
		blkcell2.setBorderWidthLeft(0);
		blkcell2.setBorderWidthRight(0);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
//		table.addCell(blkcell2);
		
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(totcell);
		
//		Phrase totalval=new Phrase(" ",font9);
		Phrase totalval=new Phrase(Math.round(con.getNetpayable())+"",font9);
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(totalvalcell);
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.addCell(oldContDetCell);
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsHeader(Document doc, Company comp) {
		
		DocumentUpload document =comp.getUploadHeader();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,725f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
//		
//		try
//		{
//		Image image1=Image.getInstance("images/header.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,725f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createCompanyNameAsFooter(Document doc, Company comp) {
		
		
		DocumentUpload document =comp.getUploadFooter();

		//patch
		String hostUrl;
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f,40f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		try
//		{
//		Image image1=Image.getInstance("images/pestomatic _letterhead-1.jpg");
//		image1.scalePercent(15f);
//		image1.scaleAbsoluteWidth(520f);
//		image1.setAbsolutePosition(40f,40f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		}

	private void createBlankforUPC() {
		
		Phrase blankphrase=new Phrase("",font8);
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);
		
		PdfPTable titlepdftable=new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		parent.addCell(titlePdfCell);
		
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPdfAttachment(Contract con,Company company,Contract conRen,Customer cust,String preprintStatus){
		int noOfLine=10;
		int productcount=0;
		this.con=con;
		comp=company;
		oldContract=conRen;
		this.cust=cust;
		branchList=ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName",con.getBranch()).list();

		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", con.getCompanyId()).filter(" paymentStatus", true).first().now();
		
		if(con.getPaymentMode()!=null && con.getPaymentMode().trim().length()>0){
			List<String> paymentDt = Arrays.asList(con.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				comppayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", con.getCompanyId()).first()
						.now();
				
			}
		}	
		
//		Createblank();
//		createCompanyAddress();
		logger.log(Level.SEVERE, "preprintStatus value  "+preprintStatus);
		if(preprintStatus.contains("plane")){
			Createblank();
//			createLogo(document,comp);
			createCompanyAddress();
		}else if(preprintStatus.contains("yes")){
			createBlankforUPC();
		}else if(preprintStatus.contains("no")){
			createBlankforUPC();
			if(comp.getUploadHeader()!=null){
		    	createCompanyNameAsHeader(document,comp);
			}
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
			}
		}
		createHeading();
		createCustomerDetails();
		createSubjectAndMsg();
//		if(checkOldFormat==true){
//			if(oldConDetFlag){
//				createOldContractProductDetails();
//			}
	   createproductHeading();
	   createProductInfonew();
	   if(noOfLine!=0){
	   cretetotal();
	   footerInfo();
	   footerDetails();
	   }
		if(noOfLine==0&&productcount==0){
			 cretetotal();
			 footerInfo();
			 footerDetails();
		}
			/*
			 * commented by Ashwini
			 */
			
//			if(pepcoppflag==false){
				
				if(noOfLine==0&&productcount!=0){
					try {
						document.add(Chunk.NEXTPAGE);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					 if(preprintStatus.contains("yes")){
//						createBlankforUPC();
//					}else if(preprintStatus.contains("no")){
//						createBlankforUPC();
//						if(comp.getUploadHeader()!=null){
//					    	createCompanyNameAsHeader(document,comp);
//						}
//						if(comp.getUploadFooter()!=null){
//							createCompanyNameAsFooter(document,comp);
//						}
//					}
					Phrase ref = new Phrase("Annexure  ", font10bold);
					Paragraph pararef = new Paragraph();
					pararef.add(ref);
					pararef.setSpacingAfter(15);
				   
				    try {
						document.add(pararef);
					} catch (DocumentException e) {
						// TODO Auto-generated catch block
									e.printStackTrace();
		}
				    
				    createRemainingProduct(productcount);
				    cretetotal();
				    footerInfo();
				    footerDetails();
					}
				
//			}
//		}
		
	}



	private void Createblank() {
		
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createLogo(Document doc, Company comp) {
		//********************logo for server ********************
			DocumentUpload document =comp.getLogo();
			//patch
			String hostUrl; 
			String environment = System.getProperty("com.google.appengine.runtime.environment");
			if (environment.equals("Production")) {
			    String applicationId = System.getProperty("com.google.appengine.application.id");
			    String version = System.getProperty("com.google.appengine.application.version");
			    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
			} else {
			    hostUrl = "http://localhost:8888";
			}
			try {
				Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
				image2.scalePercent(20f);
				image2.setAbsolutePosition(40f,765f);	
				doc.add(image2);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}


	public  void createCompanyAddress()
	{
		
		/**
		 * Date 3/1/2018
		 * By jayshree
		 * Des.To add the logo at proper position
		 */
		
		DocumentUpload logodocument = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + logodocument.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(20);
		} catch (Exception e) {
			e.printStackTrace();
		}

		
		 PdfPTable logotab=new PdfPTable(1);
		 logotab.setWidthPercentage(100);
		 
		 if (imageSignCell != null) {
			 logotab.addCell(imageSignCell);
			} 
		 else {
				Phrase blank = new Phrase(" ");
				PdfPCell blankCell = new PdfPCell(blank);
				blankCell.setBorder(0);
				logotab.addCell(blankCell);
			}
		
		 //End by jayshree
		 
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
		 
//	     Paragraph p =new Paragraph();
//	     p.add(Chunk.NEWLINE);
//	     p.add(companyName);
//	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell(companyName);
	     companyNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	     companyNameCell.setFixedHeight(0);
	    
//	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		
		/**
		 * Date 10/1/2018 
		 * By jayshree
		 * Des.to check the null condition for pin number remove the comma from add
		 */
		String pinno;
		if(comp.getAddress().getPin()!=0){
			pinno=","+comp.getAddress().getPin()+",";
		}
		else
		{
			pinno="  ";
		}
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+
				      pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()
				      +pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()
				      +pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()
				      +pinno+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		
		/**
		 * Date 4/1/2018
		 * By jayshree
		 * To handle the null pointer condition
		 */
		
		String mobile="";
		String landline="";
		String email="";
		
		if(comp.getCellNumber1()!=0){
			mobile=comp.getCellNumber1()+"";
		}
		else{
			mobile="";
		}
		
		if(comp.getLandline()!=0){
			landline=comp.getLandline()+"";
		}
		else{
			landline="";
		}
		
		
		String branchmail = "";
		ServerAppUtility serverApp = new ServerAppUtility();

		if (checkEmailId == true) {
			branchmail = serverApp.getBranchEmail(comp,
					con.getBranch());
			System.out.println("server method " + branchmail);

		} else {
			branchmail = serverApp.getCompanyEmail(comp.getCompanyId());
			System.out.println("server method 22" + branchmail);
		}
		
		if(branchmail!=null){
			email=branchmail;
		}
		else{
			email="";
		}
//		Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Tel: "+comp.getLandline()+"    Email: "+comp.getEmail().trim(),font10);
		Phrase contactinfo=new Phrase("Phone: "+mobile+"  Email: "+email,font10);

		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
		
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.setSpacingAfter(10f);
		
	//	
		/**
		 * Date 4/1/2018
		 * By jayshree
		 * Des.To add the logo at proper position
		 */
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		try {
			parent.setWidths(new float[]{100});
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		PdfPCell logocell=new PdfPCell(logotab);
		logocell.setBorderWidthTop(0);
		logocell.setBorderWidthLeft(0);
		logocell.setBorderWidthRight(0);
//		parent.addCell(logocell);
		
		//End By Jayshree
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		comapnyCell.setBorderWidthTop(0);
		comapnyCell.setBorderWidthLeft(0);
		comapnyCell.setBorderWidthRight(0);
		comapnyCell.setFixedHeight(0);
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
//			document.add(Chunk.NEWLINE);
//			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}




	private void createHeading() {
		String title1 = "";
		title1 = "Renewal Letter";

		Phrase titlephrase = new Phrase(title1, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorder(0);
		titlepdfcell.addElement(Chunk.NEWLINE);

		
		
		Phrase custInfo = new Phrase("Customer Id :", font10);
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		
		Phrase custInfovalue = new Phrase(con.getCustomerId()+"", font10);
		PdfPCell custInfovalueCell = new PdfPCell(custInfovalue);
		custInfovalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfovalueCell.setBorder(0);
		
		
		
		Phrase date = new Phrase("Date :", font10);
		PdfPCell dateCell = new PdfPCell(date);
		dateCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		dateCell.setBorder(0);
		
		Phrase datevalue;
//		datevalue = new Phrase(fmt.format(con.getStartDate())+"", font10);
		datevalue = new Phrase(fmt.format(con.getContractDate())+"", font10);
		
		PdfPCell datevalueCell = new PdfPCell(datevalue);
		datevalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datevalueCell.setBorder(0);
		
		
		
		
		
		
		float[] relativeWidths1 = {1.3f,5.5f,1f,2f};
		
		PdfPTable headTbl=new PdfPTable(4);
		headTbl.setWidthPercentage(100f);
		try {
			headTbl.setWidths(relativeWidths1);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		headTbl.addCell(custInfoCell);
		headTbl.addCell(custInfovalueCell);
		headTbl.addCell(dateCell);
		headTbl.addCell(datevalueCell);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell titlePdfCell1=new PdfPCell(titlepdfcell);
		titlePdfCell1.setBorder(0);
		
		PdfPCell headIfo=new PdfPCell(headTbl);
		headIfo.setBorder(0);
		
		parent.addCell(titlePdfCell1);
		parent.addCell(headIfo);
		
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
//			document.add(Chunk.NEWLINE);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createCustomerDetails() {
		
		Phrase to = new Phrase("To,", font10);
		PdfPCell toCell = new PdfPCell(to);
		toCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		toCell.setBorder(0);
		
		String roleName="";
		 if(conDetails!=null&&conDetails.getRole()!=null){
			roleName=conDetails.getRole(); 
		 }else{
			 roleName="";
		 }
		
		Phrase role = new Phrase("The "+roleName, font10);
		PdfPCell roleCell = new PdfPCell(role);
		roleCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		roleCell.setBorder(0);
		
		
		Phrase custInfo ;
		if(cust.isCompany()){
			custInfo = new Phrase(cust.getCompanyName(), font10);
		}else{
			custInfo = new Phrase(cust.getFullname(), font10);
		}
		PdfPCell custInfoCell = new PdfPCell(custInfo);
		custInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custInfoCell.setBorder(0);
		
		
		
		
		String custAdd1="";
		String custFullAdd1="";
		
		if(cust.getAdress()!=null){
			
			if(cust.getAdress().getAddrLine2()!=null){
				if(cust.getAdress().getLandmark()!=null){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getAddrLine2();
				}
			}else{
				if(cust.getAdress().getLandmark()!=null){
					custAdd1=cust.getAdress().getAddrLine1()+","+cust.getAdress().getLandmark();
				}else{
					custAdd1=cust.getAdress().getAddrLine1();
				}
			}
			
			/**
			 * Date 4/1/2018
			 * By jayshree
			 * Des.To change the sequence of country ,state ,city	And handle the null pointer for pin number	
			 */
			
			String pin="";
			if(cust.getAdress().getPin()!=0){
				pin=cust.getAdress().getPin()+"";
			}
			else{
				pin="";
			}
			if(cust.getAdress().getLocality()!=null){
//				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCountry()+","+cust.getAdress().getState()+","+cust.getAdress().getCity()+"\n"+cust.getAdress().getLocality()+" "+cust.getAdress().getPin();
				
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+"\n"+cust.getAdress().getLocality()+" "+pin;

			}else{
				custFullAdd1=custAdd1+"\n"+cust.getAdress().getCity()+","+cust.getAdress().getState()+","+cust.getAdress().getCountry()+" "+pin;
			}
		}
		String custAddress="";
		if(cust.getAdress()!=null){
			custAddress=cust.getAdress().getCompleteAddress();
		}else{
			custAddress="";
		}
		Phrase custAddInfo = new Phrase(custAddress, font10);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		custAddInfoCell.setBorder(0);
		
		
		Phrase blank = new Phrase(" ", font10);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		blankCell.setBorder(0);
		
		
		String cell="";
		if(cust.getCellNumber1()!=null){
			cell="Cell :"+cust.getCellNumber1()+"";
		}else{
			cell="Cell :";
		}
		
		Phrase cellNo = new Phrase(cell, font10);
		PdfPCell cellNoCell = new PdfPCell(cellNo);
		cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellNoCell.setBorder(0);
		
		String email="";
		if(cust.getEmail()!=null){
			email=cust.getEmail();
		}else{
			email="";
		}
		Phrase emailPr = new Phrase("E-mail :"+email, font10);
		PdfPCell emailCell = new PdfPCell(emailPr);
		emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailCell.setBorder(0);
		
		PdfPTable customerTable=new PdfPTable(1);
		customerTable.setWidthPercentage(100);
		
		customerTable.addCell(toCell);
		if(conDetails!=null&&conDetails.getRole()!=null){
		customerTable.addCell(roleCell);
		}
		customerTable.addCell(custInfoCell);
		customerTable.addCell(custAddInfoCell);
		customerTable.addCell(cellNoCell);
		customerTable.addCell(emailCell);
		
		PdfPCell customerCell = new PdfPCell();
		customerCell.addElement(customerTable);
		customerCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		customerCell.setBorder(0);
		
		
		
		PdfPTable headTbl=new PdfPTable(2);
		headTbl.setWidthPercentage(100f);
		
		headTbl.addCell(customerCell);
		headTbl.addCell(blankCell);
		
		PdfPCell parentCell = new PdfPCell();
		parentCell.addElement(headTbl);
		parentCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		parentCell.setBorder(0);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(parentCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
			
	}

	private void createSubjectAndMsg() {
		int contractId=0;
		if(oldContract!=null){
			contractId=oldContract.getCount();
		}else{
			contractId=con.getCount();
		}
		
		Phrase sub=null;
	    sub = new Phrase("Subject : Renewal of Contract Id "+contractId, font10);
		logger.log(Level.SEVERE,"old contract Date"+contractId);
		
		PdfPCell subCell = new PdfPCell(sub);
		subCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		subCell.setBorder(0);
		subCell.setPaddingBottom(5);
		
		Phrase blank = new Phrase("", font10bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		blankCell.setBorder(0);
		
		Phrase dear = new Phrase("Dear Sir/Madam,", font10bold);
		PdfPCell dearCell = new PdfPCell(dear);
		dearCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		dearCell.setBorder(0);
		int maxServDuration=maxDuration();
		logger.log(Level.SEVERE,"no of max service days "+maxServDuration);
		
		String noOfServDays=numberOfDaysToMonthAndDay(maxServDuration);
		String title1 = "";
		title1="It is our Privilege to have been of service to you over the past year. we truly appreciate and value our association and trust you have found our services exemplary and to your complete satisfaction."+"\n"+"Your current contract for pest Management Services concludes on "+fmt3.format(oldContract.getEndDate())+" to order to enjoy uninterruplted service for a pest-free environment,we recommend you to renew the contract at the earliest.Our renewal charges will be Rs "+df.format(con.getNetpayable())+" in terms and conditions for a "+noOfServDays+" contract (for period "+(fmt.format(con.getStartDate()))+" to "+fmt.format(con.getEndDate())+") would be mentioned below.";
		Phrase msg = new Phrase(title1, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		parent.addCell(subCell);
		parent.addCell(dearCell);
		
		parent.addCell(msgCell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void createProductInfo() {
		
		
		/**
		 * Date : 06-12-2017 BY ANIL
		 */
		Phrase newContDetPh = new Phrase("New contract details : ", font9bold);
		PdfPCell newContDetCell = new PdfPCell(newContDetPh);
		newContDetCell.setBorder(0);
		/**
		 * End
		 */
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		float[] relativeWidths = {2.5f,1f,1f,1f,1f,1f,1f};
		
		PdfPTable table = new PdfPTable(7);
		/**end**/
		table.setWidthPercentage(100);
		try {
			table.setWidths(relativeWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		Phrase netPay = new Phrase("NET PAYABLE", font1);
		Phrase name = new Phrase("NAME", font1);
		Phrase duration = new Phrase("DURATION",font1);
		Phrase services = new Phrase("SERVICES", font1);
		/**Date :25/11/2017 BY: Manisha
		 * Description:TO add old price column in pdf..!!!
		 */
		Phrase oldprice=new Phrase("OLD PRICE",font1);
		Phrase price = new Phrase("Rate", font1);
		/**enD**/
		
//		Phrase stax = new Phrase("S.TAX", font1);
//		Phrase vat = new Phrase("VAT", font1);
		
		   Phrase servicetax= null;
		      Phrase servicetax1= null;
		      int flag=0;
		      int vat=0;
		      int st=0;
		      
		      /*Date :22/11/2017  By:Manisha
		       * Description :To update the tax name in pdf, as taxes are selected from drop down..!!
		       */
		   for(int i=0; i<con.getItems().size();i++){
		    	  
		    if (con.getItems().get(i).getVatTax().getTaxPrintName()!= null
							&& !con.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
		    {
		    	
		    	       if(con.getItems().get(i).getServiceTax().getTaxPrintName()!=null
		    			&& !con.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase(""))
		    			{
		    		
		    		       servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"/"
		    		                    +con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
		    		       flag=flag+1;
		    			} 
		    	       else
		    	       {
		    	    	   
		    	    	   servicetax=new Phrase(con.getItems().get(i).getVatTax().getTaxPrintName()+"%",font1);
		    	    	   vat=vat+1;
		    	    	   
		    	       }
		    	
		    	
		    }
		    
		    else{
		    	   
		    	if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()==0)){
		  	       servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+"%",font1);
		  	       vat=vat+1;
		  	       System.out.println("phrase value===="+servicetax.toString());
		  	      }
		    	

			      else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()==0)
			    		  &&(con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equals("")))
			      {
			      	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  st=st+1;
			      }
		    	
			      else if((con.getItems().get(i).getVatTax().getPercentage()>0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0)
			    		  && (con.getItems().get(i).getVatTax().getTaxPrintName()==null) &&  (con.getItems().get(i).getVatTax().getTaxPrintName().equalsIgnoreCase(""))
			    		 && (con.getItems().get(i).getServiceTax().getTaxPrintName()==null) && (con.getItems().get(i).getServiceTax().getTaxPrintName().equalsIgnoreCase("")))
			       {
			      	  servicetax = new Phrase(con.getItems().get(i).getVatTax().getTaxName()+con.getItems().get(i).getServiceTax().getTaxName()+"%",font1);
			      	  flag=flag+1;
			      	  System.out.println("flag value;;;;;"+flag);
			      }
		    	 
			    else if ((con.getItems().get(i).getServiceTax().getPercentage()>0) && (con.getItems().get(i).getVatTax().getPercentage()==0)
			    		&&(con.getItems().get(i).getServiceTax().getTaxPrintName()!=null) && (!con.getItems().get(i).getServiceTax().getTaxPrintName().equals(""))) 
			    {
			    	  servicetax = new Phrase(con.getItems().get(i).getServiceTax().getTaxPrintName()+"%",font1);
			    	  st=st+1;
				}
		    }
		     
		      
		      
//		       else{
//		      	 
//		      	  servicetax = new Phrase("TAX %",font1);
//		       }
		    }
		      
		      
		      /**End for Manisha**/
		      
		
		  	PdfPCell celltax = new PdfPCell(servicetax);
		  	celltax.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		PdfPCell cellname = new PdfPCell(name);
		cellname.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellduration = new PdfPCell(duration);
		cellduration.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellservices = new PdfPCell(services);
		cellservices.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellprice = new PdfPCell(price);
		cellprice.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellvat = new PdfPCell(vat);
//		cellvat.setHorizontalAlignment(Element.ALIGN_CENTER);
//		PdfPCell cellstax = new PdfPCell(stax);
//		cellstax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellNetPay = new PdfPCell(netPay);
		cellNetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		/**Manisha add column old price **/
		PdfPCell celloldprice=new PdfPCell(oldprice);
		celloldprice.setHorizontalAlignment(Element.ALIGN_CENTER);
		/**End**/
		
		table.addCell(cellname);
		table.addCell(cellduration);
		table.addCell(cellservices);
		/**Manisha add column old price **/
//		table.addCell(celloldprice);
		/**end*/
		table.addCell(cellprice);
		table.addCell(celltax);
		table.addCell(cellNetPay);
		
		
		for(int i=0;i<con.getItems().size();i++){
//			chunk = new Phrase((i+1)+"",font9);
//			pdfcode = new PdfPCell(chunk);
//			pdfcode.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			/**Manisha added old price cell in table**/
			Phrase pdfoldprice;
			if(con.getItems().get(i).getOldProductPrice()!=0){
				pdfoldprice = new Phrase(con.getItems().get(i).getOldProductPrice() +"",font9);
			}else{
				pdfoldprice=new Phrase("");
			}
			/**End******/
		
			
			
			
			////
			if(con.getItems().get(i).getProductName()!=null){
				chunk = new Phrase(con.getItems().get(i).getProductName() + "", font9);
			}else{
				chunk = new Phrase("");
			}
			
			pdfname = new PdfPCell(chunk);
			pdfname.setHorizontalAlignment(Element.ALIGN_CENTER);
			 
			 /////
			if(con.getItems().get(i).getDuration()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getDuration()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfduration = new PdfPCell(chunk);
			pdfduration.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
			if(con.getItems().get(i).getNumberOfServices()!=-1){
				 chunk =new Phrase (con.getItems().get(i).getNumberOfServices()+"",font9);
			}
			else{
				 chunk= new Phrase("");
			}
			pdfservices = new PdfPCell(chunk);
			pdfservices.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////	
			if(con.getItems().get(i).getPrice()!=0){
				chunk = new Phrase(con.getItems().get(i).getPrice()+"", font9);
			}else{
				 chunk = new Phrase("");
			}
			pdfprice = new PdfPCell(chunk);
			pdfprice.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			////
//			if(con.getItems().get(i).getServiceTax()!=null){
//				chunk = new Phrase(con.getItems().get(i).getServiceTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfstax = new PdfPCell(chunk);
//			pdfstax.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			////
//			if(con.getItems().get(i).getVatTax()!=null){
//				chunk = new Phrase(con.getItems().get(i).getVatTax()+"", font9);
//			}else{
//				 chunk = new Phrase("");
//			}
//			pdfvat = new PdfPCell(chunk);
//			pdfvat.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			///// 
			
		   	 PdfPCell pdfservice =null;
	      	 Phrase vatno=null;
	      	 Phrase stno=null;
	      	 if((con.getItems().get(i).getVatTax().getPercentage()==0)&&(con.getItems().get(i).getServiceTax().getPercentage()>0))
	      	 {
	      	 
	      		 if(con.getItems().get(i).getServiceTax().getPercentage()!=0){
	      		 if(st==con.getItems().size()){
	      			 chunk = new Phrase(con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	      		 }
	      		 else{
	      			 stno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage()+"",font9);
	      		 }
	      		 }
	      	    else
	      		 chunk = new Phrase("N.A"+"",font9);
	      		 if(st==con.getItems().size()){
	      	    pdfservice = new PdfPCell(chunk);
	      		 }else{
	      			 pdfservice = new PdfPCell(stno);
	      		 }
	      	  
	      	  pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((con.getItems().get(i).getServiceTax().getPercentage()==0)&&(con.getItems().get(i).getVatTax().getPercentage()>0))
	           {
	      		 
	      		 if(con.getItems().get(i).getVatTax()!=null){
	      			 if(vat==con.getItems().size()){
	      				 System.out.println("rohan=="+vat);
	      				 chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+"",font9);
	      			 }else{
	      				 System.out.println("mukesh");
	           	    vatno = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+con.getItems().get(i).getServiceTax().getPercentage(),font9);
	      		 }
	      		 }
	           	    else{
	           		 chunk = new Phrase("N.A"+"",font9);
	           	    }
	      		 if(vat==con.getItems().size()){
	           	  pdfservice = new PdfPCell(chunk);
	      		 }else{ pdfservice = new PdfPCell(vatno);}
	           	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      	 }
	      	 else if((con.getItems().get(i).getServiceTax().getPercentage()>0)&&(con.getItems().get(i).getVatTax().getPercentage()>0)){
	      		 if((con.getItems().get(i).getVatTax()!=null)&& (con.getItems().get(i).getServiceTax()!=null))
	      			 
	            	    chunk = new Phrase(con.getItems().get(i).getVatTax().getPercentage()+""+" / "+""
	            	    +con.getItems().get(i).getServiceTax().getPercentage(),font9);
	            	 else
	            		 chunk = new Phrase("N.A"+"",font9);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }else{
	      		 
	      		 chunk = new Phrase("N.A"+"",font9);
	            	  pdfservice = new PdfPCell(chunk);
	            	 pdfservice.setHorizontalAlignment(Element.ALIGN_CENTER);
	      		 
	      	 }
			
	      	pdftax = new PdfPCell(chunk);
	      	pdftax.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			
	      	/**   Date : 24-11-2017 BY MANISHA
	  	   * Description :To get the total amount on pdf..!!
	  	   * **/
	      	
			double netPayAmount = calculateTotalAmt(con.getItems().get(i).getPrice(),con.getItems().get(i).getServiceTax().getPercentage(),con.getItems().get(i).getVatTax().getPercentage(),
					con.getItems().get(i).getVatTax().getTaxPrintName(),con.getItems().get(i).getServiceTax().getTaxPrintName());
			
			/**Ends**/
			
			if(netPayAmount!=0){
				chunk = new Phrase(Math.round(netPayAmount)+"", font9);
			}else{
				 chunk = new Phrase(" ",font9);
			}
			pdfnetPay = new PdfPCell(chunk);
			pdfnetPay.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			table.addCell(pdfname);
			table.addCell(pdfduration);
			table.addCell(pdfservices);
			/**Manisha add column old price **/
		
			PdfPCell oldpricevalcell=new PdfPCell(pdfoldprice);
			oldpricevalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			table.addCell(oldpricevalcell);
			
			/**End**/
			table.addCell(pdfprice);
			table.addCell(pdftax);
			table.addCell(pdfnetPay);
		}
		
		  /**   Date : 24-11-2017 BY MANISHA
		   * Description :To get the total amount on pdf..!!
		   * **/

		Phrase blkph1=new Phrase("");
		PdfPCell blkcell=new PdfPCell(blkph1);
		blkcell.setBorderWidthRight(0);
		table.addCell(blkcell);
		
		Phrase blkph2=new Phrase("");
		PdfPCell blkcell2=new PdfPCell(blkph2);
		blkcell2.setBorderWidthLeft(0);
		blkcell2.setBorderWidthRight(0);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		table.addCell(blkcell2);
		
		Phrase totalph=new Phrase("TOTAL",font1);
		PdfPCell totcell=new PdfPCell(totalph);
		totcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(totcell);
		
		Phrase totalval=new Phrase(Math.round(con.getNetpayable())+"",font9);
		PdfPCell totalvalcell=new PdfPCell(totalval);
		totalvalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(totalvalcell);
		
		/**End for Manisha**/
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		prodtablecell.setBorder(0);
		
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		/**
		 * Date:06-12-2017 BY ANIL
		 */
		if(oldConDetFlag){
			parent.addCell(newContDetCell);
		}
		parent.addCell(prodtablecell);
		parent.setSpacingAfter(15f);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		 
	}

	private void footerInfo() {
		
		
		
		String title="";
		title="We reiterate our commitment to ensure you complete satisfaction and look forward to hearing from you soon."
				+ "Should you require further information on above Please do give us a call and we would gladly assist you.";
		
		Phrase msg = new Phrase(title, font10);
		PdfPCell msgCell = new PdfPCell(msg);
		msgCell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED);
		msgCell.setBorder(0);
		
		/**
		 * @author Vijay Date 17-12-2020
		 * Des :- bug showing two times of description so below code commented
		 */
//		String contratDescription="";
//		if(con.getDescription()!=null){
//			contratDescription=con.getDescription();
//		}
//		
//		Phrase contractDesc = new Phrase(contratDescription, font10);
//		PdfPCell contractDescCell = new PdfPCell(contractDesc);
//		contractDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		contractDescCell.setBorder(0);
		
		String contratRenDescription=null;
		if(con.getDescription()!=null){
			contratRenDescription=con.getDescription();
		}
		
		/**
		 * Date 11/1/2018
		 * By jayshree
		 * Des.to set only 500 characters in description
		 */
		
		Phrase contractRenDesc =null;
		if(contratRenDescription.length()>500){
		contractRenDesc = new Phrase(contratRenDescription.substring(0,499), font10);
		}else{
			contractRenDesc = new Phrase(contratRenDescription, font10);
		}
		PdfPCell contractRenDescCell = new PdfPCell(contractRenDesc);
		contractRenDescCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		contractRenDescCell.setBorder(0);
		
		Phrase annexture = new Phrase("Please refere annexure for product details  ", font10bold);
		PdfPCell annextureCell = new PdfPCell(annexture);
		annextureCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		annextureCell.setBorder(0);
		annextureCell.setPaddingBottom(10);
		annextureCell.setPaddingTop(10);
		
		Phrase check1 = new Phrase("Cheque should be in favour of  "+"\""+comp.getBusinessUnitName()+"\"", font10bold);
		PdfPCell check1Cell = new PdfPCell(check1);
		check1Cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		check1Cell.setBorder(0);

		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.setSpacingBefore(2);
		
		Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    PdfPCell blankCell = new PdfPCell(blank);
	    blankCell.setBorder(0);
	    
		parent.addCell(msgCell);
//		if(con.getDescription()!=null){
//			parent.addCell(contractDescCell);
//		}
		if(con.getDescription()!=null){
			parent.addCell(contractRenDescCell);
		}
		
		
		if(checkOldFormat==false){
//		parent.addCell(annextureCell);
		}
//		parent.addCell(check1Cell);
		parent.addCell(blankCell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		/****footer tab****/
		
		
		PdfPTable leftFooter=new PdfPTable(1);
		leftFooter.setWidthPercentage(100);
		
		
		String title2 = "";
		title2 = "Thank You";//By Jayshree remove the content
		
		Phrase thank = new Phrase(title2, font10);
		PdfPCell thankCell = new PdfPCell(thank);
		thankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		thankCell.setBorder(0);
		
		
		
		
		Phrase compn = new Phrase("For "+comp.getBusinessUnitName(), font10bold);
		PdfPCell compCell = new PdfPCell(compn);
        compCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		compCell.setBorder(0);
		
		
		
		Phrase sign = new Phrase("Authorised Signatory", font10); // Authorised Signatory   Sign. Authority
		PdfPCell signCell = new PdfPCell(sign);
		signCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		signCell.setBorder(0);
		
		
		
		/***
		 * Date 14-04-2018 By vijay for Digital Signator
		 */

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		} catch (Exception e) {
			e.printStackTrace();
		}

		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font10bold);
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		signParaCell.setBorder(0);
			signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		/**
		 * ends here
		 */
		
		
			
		
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
//		leftFooter.addCell(blankCell);
		leftFooter.addCell(thankCell);
		leftFooter.addCell(compCell);
		
		/** Date 14-04-2018 By vijay for Digital Signatory ***/
		if (imageSignCell != null) {
			leftFooter.addCell(imageSignCell);
		} else {
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			leftFooter.addCell(blank1Cell);
			leftFooter.addCell(blank1Cell);
			leftFooter.addCell(blank1Cell);
		}
		/**
		 * ends here
		 */
		leftFooter.addCell(signCell);
		leftFooter.setSpacingAfter(15f);
		
		
		
		PdfPTable rightTab=new PdfPTable(3);
		rightTab.setWidthPercentage(100);
		try {
			/*
			 *Date: 11/10/2018
			 * Developer:Ashwini
			 * Des:to shrink right table
			 */
			if(pecoppflag){
			 float[] columnWidths = { 1f, 1f, 2f };
			 rightTab.setWidths(columnWidths);
			}else{
				
				rightTab.setWidths(new float[]{20,30,50});
				
			}
			 
			/*
			 * end by Ashwini
			 */
//			rightTab.setWidths(new float[]{20,30,50}); //commented by Ashwini
		
			
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Phrase modes=new Phrase("Modes",font9bold);
		PdfPCell modesCell=new PdfPCell(modes);
		modesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(modesCell);
		
		Phrase favering=new Phrase("Favouring",font9bold);
		PdfPCell faveringCell=new PdfPCell(favering);
		faveringCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(faveringCell);
		
		Phrase details=new Phrase("Details",font9bold);
		PdfPCell detailsCell=new PdfPCell(details);
		detailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		rightTab.addCell(detailsCell);
		
		Phrase chequeph=new Phrase("Cheque/"+"\n"+"Demand Draft/"+"\n"+"Pay Order ",font9);
		PdfPCell chequeCell=new PdfPCell(chequeph);
		chequeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(chequeCell);
		if(pecoppflag || pepcoppflag){
			
			Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName(),font9);
			PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
			buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			rightTab.addCell(buesinessunitCell);
			
		}else{
		
		Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName()+"\n"+comp.getAddress().getCompleteAddress(),font9);
		PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
		buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(buesinessunitCell);
		}
		
//		Phrase buesinessunitph=new Phrase(comp.getBusinessUnitName()+"\n"+comp.getAddress().getCompleteAddress(),font9);
//		PdfPCell buesinessunitCell=new PdfPCell(buesinessunitph);
//		buesinessunitCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		rightTab.addCell(buesinessunitCell);
//	    
		/*
		 * Date:10/10/2018
		 * Added by Ashwini
		 * Developer:To add company branch Address
		 */
		if(pecoppflag){
		
		String branchaddress1 =" ";
		
		System.out.println("Branchlist::"+branchList);
		
		if(branchList!=null){
		for(Branch branch:branchList){
			System.out.println("I am inside for loop1");
			System.out.println("Branch from branchlist:"+branch.getBusinessUnitName());
			System.out.println("Branch from contractRenewal::"+con.getBranch());
			System.out.println("I am inside for loop2");
			if(con.getBranch().equalsIgnoreCase(branch.getBusinessUnitName())){
				branchaddress1 = branch.getAddress().getCompleteAddress();
				System.out.println("branch Address::"+branch.getAddress().getCompleteAddress());
			}
			
		}
		
		}else{
			
			branchaddress1 = comp.getAddress().getCompleteAddress();
			System.out.println("company Address::"+comp.getAddress().getCompleteAddress());
		}
		System.out.println("I am End of the for loop2");
		
		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+branchaddress1+"\n"+"2.Ensure your name and contract id is mentioned on the back of the cheque ",font9);
		PdfPCell detailsvalCell=new PdfPCell(detailsval);
		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(detailsvalCell);
		
	}else{
		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+comp.getAddress().getCompleteAddress()+"\n"+"2.Ensure your company name is mentioned on the back of the cheque ",font9);
		PdfPCell detailsvalCell=new PdfPCell(detailsval);
		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(detailsvalCell);
	}
		
		/*
		 * end by Ashwini
		 */
		
		
		/*
		 * commented by Ashwini
		 */
		
//		Phrase detailsval=new Phrase("1. Send via Courier to our office address"+"\n"+comp.getAddress().getCompleteAddress()+"\n"+"2.Ensure Your Company Name is mentioned on the back of the cheque ",font9);
//		PdfPCell detailsvalCell=new PdfPCell(detailsval);
//		detailsvalCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		rightTab.addCell(detailsvalCell);

		/*
		 * end by Ashwini
		 */
		Phrase rtgsph=new Phrase("RTGS/NEFT",font9);
		PdfPCell rtgsCell=new PdfPCell(rtgsph);
		rtgsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		rightTab.addCell(rtgsCell);
		
		Phrase companyname=null;
		
		PdfPTable footerParent=new PdfPTable(2);
		footerParent.setWidthPercentage(100);
		
		try {
//			footerParent.setWidths(new float[]{70,30});
			/*
			 * Date:11/10/2018
			 * Developer:Ashwini
			 * 
			 */
			if(pecoppflag){
			footerParent.setWidths(new float[]{40,30});
			}else{
				footerParent.setWidths(new float[]{70,30});
			}
			
			/*
			 * end by Ashwini
			 */
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(comppayment!=null){
		System.out.println("companypayment111");
		PdfPCell leftCell=new PdfPCell(rightTab);
		leftCell.setBorder(0);
		footerParent.addCell(leftCell);
		
		PdfPCell rightCell=new PdfPCell(leftFooter);
		rightCell.setBorder(0);
		footerParent.addCell(rightCell);
		}
		else{
			System.out.println("company payment222");
			
			PdfPCell rightCell=new PdfPCell(leftFooter);
			rightCell.setBorder(0);
			rightCell.setColspan(2);
			footerParent.addCell(rightCell);
			
		}
//		try {
//			document.add(footerParent);
//		} catch (DocumentException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		
	}



	public double calculateTotalExcludingTax()
	{
		List<SalesLineItem>list=con.getItems();
		double sum=0,priceVal=0;
		for(int i=0;i<list.size();i++)
		{
			SalesLineItem entity=list.get(i);
			SuperProduct prod=entity.getPrduct();
			double taxAmt=removeAllTaxes(prod);
			if(entity.getPercentageDiscount()==null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal*entity.getQty();
				sum=sum+priceVal;
			}
			if(entity.getPercentageDiscount()!=null){
				priceVal=entity.getPrice()-taxAmt;
				priceVal=priceVal-(priceVal*entity.getPercentageDiscount()/100);
				priceVal=priceVal*entity.getQty();
				
				sum=sum+priceVal;
			}
			

		}
		return sum;
	}

	/**
	 * Developed by : Rohan Bhagde.
	 * Reason : This method is used for calculating Total amount including tax
	 * @param newPrice
	 * @param serTax
	 * @param vatTax
	 * @return
	 */
	   /**   Date : 24-11-2017 BY MANISHA
	   * Description :To get the total amount on pdf..!!
	   * **/
	public double calculateTotalAmt(double newPrice , double serTax , double vatTax,String vatname,String sertaxname)
	{
		double totalAmt= newPrice;
		double vatTaxAmt =0;
		double setTaxAmt =0;

		
//		System.out.println("new price "+newPrice );
//		System.out.println("service tax "+serTax );
//		System.out.println("vat tax"+vatTax );
		
//		System.out.println("service tax "+sertaxname );
//		System.out.println("vat tax "+vatname );
//		System.out.println("Price "+newPrice );
		
		
		
		if(vatTax!=0 && vatname!=null && !vatname.equals("")){
			vatTaxAmt = newPrice * (vatTax / 100);
			if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + vatTaxAmt + setTaxAmt;
		    }else{
				totalAmt = newPrice + vatTaxAmt;
			}
		}else{
		    if(serTax!=0 && sertaxname!=null && !sertaxname.equals("")){   
				setTaxAmt = newPrice * (serTax/100);
				totalAmt = newPrice + setTaxAmt;
			}
			if(vatTax!=0 && (vatname==null||vatname.equals("")) ){
				vatTaxAmt =  newPrice * (vatTax / 100);
				totalAmt=vatTaxAmt;
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = totalAmt * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}else{
				if(serTax !=0 && (sertaxname==null||sertaxname.equals(""))){
					setTaxAmt = newPrice * (serTax / 100);
					totalAmt = totalAmt +setTaxAmt;
//					System.out.println("new price "+totalAmt );
				}
			}
			
		}
		
		
	//	
//	    if(vatname==null && vatTax==0 && sertaxname!=null && !sertaxname.equals("") && serTax!=0) 
//		{
//	    	setTaxAmt = newPrice * (serTax/100);
//			totalAmt =newPrice+setTaxAmt;
		return totalAmt;
	}

	       /****End******/

	public double removeAllTaxes(SuperProduct entity)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
			// Here if both are inclusive then first remove service tax and then on that amount
			// calculate vat.
			double removeServiceTax=(entity.getPrice()/(1+service/100));
						
			//double taxPerc=service+vat;
			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
			retrServ=(removeServiceTax/(1+vat/100));
			retrServ=entity.getPrice()-retrServ;
		}
		tax=retrVat+retrServ;
		return tax;
	}
	private void footerDetails(){
		
		PdfPTable footerDetailTab=new PdfPTable(4);
		footerDetailTab.setWidthPercentage(100);
		
		PdfPTable gstTable=new PdfPTable(1);
		gstTable.setWidthPercentage(100);
		
		Phrase modes=new Phrase("GSTIN NO",font9bold);
		PdfPCell modesCell=new PdfPCell(modes);
		modesCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		gstTable.addCell(modesCell);
		
		String gstvalue="";
		
		if(comp.getCompanyGSTType().trim().equalsIgnoreCase("GST Applicable")){
			gstvalue=comp.getCompanyGSTTypeText();	
			logger.log(Level.SEVERE,"GSTIN 11 "+comp.getCompanyGSTTypeText());
		}else{
			gstvalue="";
		}
		logger.log(Level.SEVERE,"GSTIN VAlue--"+gstvalue);
		Phrase gstValue=new Phrase(gstvalue,font9bold);
		PdfPCell gstValueCell=new PdfPCell(gstValue);
		gstValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		gstTable.addCell(gstValueCell);
		
		PdfPCell gstCell=new PdfPCell(gstTable);
		footerDetailTab.addCell(gstCell);
		
		
		PdfPTable sacTable=new PdfPTable(1);
		sacTable.setWidthPercentage(100);

		Phrase sac=new Phrase("SAC/HSN",font9bold);
		PdfPCell sacCell=new PdfPCell(sac);
		sacCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sacTable.addCell(sacCell);
		
		String sacHsnValue="";
		for(int i=0;i<comp.getArticleTypeDetails().size();i++){
		if(comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("SAC Code")){
			sacHsnValue=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
			logger.log(Level.SEVERE,"SAC--"+sacHsnValue);
		}else{
			sacHsnValue="";
		}
		}
		logger.log(Level.SEVERE,"SAC  No--"+sacHsnValue);
		Phrase sacValue=new Phrase(sacHsnValue,font9bold);
		PdfPCell sacValueCell=new PdfPCell(sacValue);
		sacValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		sacTable.addCell(sacValueCell);
		
		PdfPCell sachsnCell=new PdfPCell(sacTable);
		footerDetailTab.addCell(sachsnCell);
		
		
		PdfPTable bankdetailsTab=new PdfPTable(3);
		bankdetailsTab.setWidthPercentage(100);

		try {
			bankdetailsTab.setWidths(new float[]{40,2,58});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		Phrase bankDetails=new Phrase("Bank Details",font9bold);
		PdfPCell bankDetailsCell=new PdfPCell(bankDetails);
		bankDetailsCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		bankDetailsCell.setColspan(3);
		bankdetailsTab.addCell(bankDetailsCell);
		
		
		Phrase name=new Phrase("Name",font8bold);
		PdfPCell nameCell=new PdfPCell(name);
		nameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		nameCell.setBorder(0);
		
		String compName="";
		if(comp.getBusinessUnitName()!=null){
			compName=comp.getBusinessUnitName();
		}else{
			compName="";
		}
		
		Phrase compname=new Phrase(compName,font8);
		PdfPCell compnameCell=new PdfPCell(compname);
		compnameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		compnameCell.setBorder(0);
		
		
		
		
		Phrase bankName=new Phrase("Bank Name",font8bold);
		PdfPCell bankNameCell=new PdfPCell(bankName);
		bankNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankNameCell.setBorder(0);
		
		String bankNameValue="";
		if(comppayment!=null){
		if(comppayment.getPaymentBankName()!=null){
			bankNameValue=comppayment.getPaymentBankName();
		}else{
			bankNameValue="";
		}
		}
		Phrase bank=new Phrase(bankNameValue,font8);
		PdfPCell bankCell=new PdfPCell(bank);
		bankCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankCell.setBorder(0);
		
		
		Phrase branch=new Phrase("Branch",font8bold);
		PdfPCell branchCell=new PdfPCell(branch);
		branchCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchCell.setBorder(0);
		
		String branchValue="";
		if(comppayment!=null){
			if(comppayment.getPaymentBranch()!=null){
				branchValue=comppayment.getPaymentBranch();	
			}else{
				branchValue="";
			}	
		}
		Phrase branchName=new Phrase(branchValue,font8);
		PdfPCell branchNameCell=new PdfPCell(branchName);
		branchNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		branchNameCell.setBorder(0);
		
		
		Phrase curAcc=new Phrase("Current A/c",font8bold);
		PdfPCell curAccCell=new PdfPCell(curAcc);
		curAccCell.setBorder(0);
		curAccCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String currentAccValue="";
		if(comppayment!=null){
		if(comppayment.getPaymentAccountNo()!=null){
			currentAccValue=comppayment.getPaymentAccountNo();	
		}else{
			currentAccValue="";
		}
		}
		Phrase curAccno=new Phrase(currentAccValue,font8);
		PdfPCell curAccnoCell=new PdfPCell(curAccno);
		curAccnoCell.setBorder(0);
		curAccnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase ifsc=new Phrase("IFSC Code",font8bold);
		PdfPCell ifscCell=new PdfPCell(ifsc);
		ifscCell.setBorder(0);
		ifscCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String ifscValue="";
		if(comppayment!=null){
		if(comppayment.getPaymentIFSCcode()!=null){
			ifscValue=comppayment.getPaymentIFSCcode();
		}else{
			ifscValue="";
		}
		}
		
		Phrase ifscCode=new Phrase(ifscValue,font8);
		PdfPCell ifscCodeCell=new PdfPCell(ifscCode);
		ifscCodeCell.setBorder(0);
		ifscCodeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		String col=":";
		Phrase colon = new Phrase(col, font10bold);
		PdfPCell colonCell = new PdfPCell(colon);
		colonCell.setBorder(0);
		
		bankdetailsTab.addCell(nameCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(compnameCell);
		
		bankdetailsTab.addCell(bankNameCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(bankCell);
		
		bankdetailsTab.addCell(branchCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(branchNameCell);
		
		bankdetailsTab.addCell(curAccCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(curAccnoCell);
		
		bankdetailsTab.addCell(ifscCell);
		bankdetailsTab.addCell(colonCell);
		bankdetailsTab.addCell(ifscCodeCell);
		
		PdfPCell detailsCell=new PdfPCell(bankdetailsTab);
		footerDetailTab.addCell(detailsCell);
		
		PdfPTable thnksTab=new PdfPTable(1);
		thnksTab.setWidthPercentage(100);
		
		Phrase thank=new Phrase("Thank You",font8bold);
		PdfPCell thankCell=new PdfPCell(thank);
		thankCell.setPaddingTop(5);
		thankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		thankCell.setBorder(0);
		thnksTab.addCell(thankCell);
		
		String compNameValue="";
		if(comp.getBusinessUnitName()!=null){
			compNameValue=comp.getBusinessUnitName();
		}else{
			compNameValue="";
		}
		
		
		
		Phrase compNamedet=new Phrase("for "+compNameValue,font8bold);
		PdfPCell compNameValueCell=new PdfPCell(compNamedet);
		compNameValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		compNameValueCell.setBorder(0);
		thnksTab.addCell(compNameValueCell);
		
		DocumentUpload digitalDocument=null;
		if(comp.getUploadDigitalSign()!=null){
	    digitalDocument = comp.getUploadDigitalSign();	
		}else{
		digitalDocument = null;
		}
		
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			imageSignCell.setPaddingRight(2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		
		if(imageSignCell!=null){
		thnksTab.addCell(imageSignCell);	
		}else{
			Phrase blank1 = new Phrase(" ", font10);
			PdfPCell blank1Cell = new PdfPCell(blank1);
			blank1Cell.setBorder(0);
			thnksTab.addCell(blank1Cell);
			thnksTab.addCell(blank1Cell);
			thnksTab.addCell(blank1Cell);
//			thnksTab.addCell(blank1Cell);

		}
		
		
		Phrase auth=new Phrase("Authorised Signatory",font8bold);
		PdfPCell authCell=new PdfPCell(auth);
		authCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		authCell.setBorder(0);
		authCell.setPaddingBottom(5);
		thnksTab.addCell(authCell);
		
		PdfPCell thankuCell=new PdfPCell(thnksTab);
		footerDetailTab.addCell(thankuCell);
		
		try {
			document.add(footerDetailTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

   public int maxDuration(){
	   ArrayList<Integer> servDurList= new ArrayList<Integer>();
	   
	   for(int i=0;i<con.getItems().size();i++){
		   servDurList.add(con.getItems().get(i).getDuration());
	   }
	   Collections.sort(servDurList); 
	   return servDurList.get(servDurList.size() - 1);
   }
    
   public String numberOfDaysToMonthAndDay(int noOfDays) {
	   String countabledays="";
	   
           if(noOfDays<=30){
        	   int days = noOfDays % 30;
        	   countabledays =days+" days";
           }else if(noOfDays==365){
        	   int year = noOfDays / 365;
        	   countabledays =year+" year";
           }else if(noOfDays==60){
        	   int month = noOfDays / 30;
        	   countabledays =month+" month";
           }else{
			int days = noOfDays % 30;
			int month = noOfDays / 30;
			if(days==0){
				countabledays= month + " Month";
			}else{
				countabledays= month + " Month and " + days + " days";	
			}
           }
           return countabledays;
	}
   
   
}