package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.paymentinfo.invoicedetails.InvoiceCharges;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

public class HygeiaInvoicePdf {


	Logger logger = Logger.getLogger("Name of logger");

	ProcessConfiguration processConfig;
	boolean hygeiaPest = false;

	public Document document;

	Company comp;
	Customer cust;
	Invoice invoice;
	Phrase chunk;
	Contract con;
	Phrase col;
	ServiceProduct serprod;

	List<PaymentTerms> payTerms = new ArrayList<PaymentTerms>();
	List<ContractCharges> billingTaxesLis;
	List<InvoiceCharges> conbillingTaxesLis = new ArrayList<InvoiceCharges>();
	List<InvoiceCharges> conbillingChargesLis = new ArrayList<InvoiceCharges>();

	PdfPCell pdfserv, pdffreq, pdfperiod, pdfamt, pdfttl, pdftaxAmt,
			pdftaxAmt1, pdftaxAmt2;

	float[] colWidth = { 0.5f, 0.4f };
	// float[] colWidth1 = { 0.3f, 0.1f, 0.3f };
	float[] colWidth1 = { 30f, 70f };
	// float[] colWidth2 = { 0.3f, 1.1f };
	float[] colWidth2 = { 15f, 85f };

	DecimalFormat df = new DecimalFormat("0.00");

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1 = new SimpleDateFormat("MMM-yyyy");

	private Font font16boldul, font12bold, font8bold, font8, font16bold,
			font12boldul, font12, font14bold, font10, font10bold, font10boldul,
			font9, font9bold, font14boldul, font9boldul;

	public HygeiaInvoicePdf() {

		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font14boldul = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD
				| Font.UNDERLINE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
				| Font.UNDERLINE);

	}

	public void setpdfinvoice(Long count) {

		invoice = ofy().load().type(Invoice.class).id(count).now();

		if (invoice.getCompanyId() != null)
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoice.getCompanyId()).first().now();
		else
			comp = ofy().load().type(Company.class).first().now();

		if (invoice.getCompanyId() != null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoice.getCustomerId())
					.filter("companyId", invoice.getCompanyId()).first().now();

		if (invoice.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoice.getCustomerId()).first().now();

		if (invoice.getCompanyId() == null)
			con = ofy().load().type(Contract.class)
					.filter("count", invoice.getContractCount()).first().now();
		else
			con = ofy().load().type(Contract.class)
					.filter("companyId", invoice.getCompanyId())
					.filter("count", invoice.getContractCount()).first().now();

		// *******loading billing**********
		// if (invoice.getArrayBillingDocument().size() != 0) {
		//
		// for (int i = 0; i < invoice.getArrayBillingDocument().size(); i++) {
		// InvoiceCharges taxes = new InvoiceCharges();
		// taxes.setChargesList(invoice.getBillingTaxes());
		// taxes.setPayPercent(invoice.getArrPayTerms().get(0)
		// .getPayTermPercent());
		// conbillingTaxesLis.add(taxes);
		//
		// InvoiceCharges charges = new InvoiceCharges();
		// charges.setPayPercent(invoice.getArrPayTerms().get(0)
		// .getPayTermPercent());
		// charges.setOtherChargesList(invoice.getBillingOtherCharges());
		// conbillingChargesLis.add(charges);
		// }
		//
		// }

		if (invoice.getArrayBillingDocument().size() > 1) {
			for (int i = 0; i < invoice.getArrayBillingDocument().size(); i++) {
				BillingDocument bill = new BillingDocument();
				if (invoice.getCompanyId() != null) {
					bill = ofy()
							.load()
							.type(BillingDocument.class)
							.filter("companyId", invoice.getCompanyId())
							.filter("contractCount", invoice.getContractCount())
							.filter("count",
									invoice.getArrayBillingDocument().get(i)
											.getBillId())
							.filter("typeOfOrder",
									invoice.getTypeOfOrder().trim()).first()
							.now();
				} else {
					bill = ofy()
							.load()
							.type(BillingDocument.class)
							.filter("contractCount", invoice.getContractCount())
							.filter("count",
									invoice.getArrayBillingDocument().get(i)
											.getBillId())
							.filter("typeOfOrder",
									invoice.getTypeOfOrder().trim()).first()
							.now();
				}
				payTerms.addAll(bill.getArrPayTerms());

			}

		} else {
			payTerms.addAll(invoice.getArrPayTerms());
		}

		// *********process config code********************
		if (invoice.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoice.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						hygeiaPest = true;
					}
				}
			}
		}

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		String amountInWord = ServiceInvoicePdf.convert(Math.round(invoice
				.getInvoiceAmount()));

	}

	public void createPdf() {

		createLogo(document, comp);
		createBlankHeading();
		createTitle();
		createInvoiceInfo();
		createCustomerInfo();
		createBlankHeading1();
		createAddressAndProdTableInfo();
		createProductTaxInfo();
		createInWordsDetail();
		createBottomInfo();

	}

	private void createLogo(Document doc, Company comp) {

		DocumentUpload document = comp.getLogo();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f, 765f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createBlankHeading() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);
		blank.add(Chunk.NEWLINE);

		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createTitle() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);
		PdfPCell blcell = new PdfPCell(blank);
		blcell.setBorder(0);

		Phrase purchase = new Phrase("INVOICE", font14bold);
		PdfPCell percell = new PdfPCell(purchase);
		percell.setBorder(0);
		percell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPTable pertable = new PdfPTable(1);
		pertable.setWidthPercentage(100);
		pertable.addCell(percell);
		pertable.addCell(blcell);

		PdfPCell cell = new PdfPCell(pertable);
		// cell.setBorder(0);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);

		PdfPTable table = new PdfPTable(1);
		table.addCell(cell);
		table.addCell(blcell);
		table.setWidthPercentage(100);

		try {
			document.add(table);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createInvoiceInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		Phrase no = null;
		if (invoice.getCount() != 0) {
			no = new Phrase("Invoice No. : " + invoice.getCount(), font12bold);
		} else {
			no = new Phrase(" ", font10bold);
		}
		PdfPCell nocell = new PdfPCell(no);
		nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(nocell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// *************righ table************
		PdfPTable righTable = new PdfPTable(1);
		righTable.setWidthPercentage(100f);

		Phrase date = null;
		if (invoice.getInvoiceDate() != null) {
			date = new Phrase("Invoice Date :  "
					+ fmt.format(invoice.getInvoiceDate()), font12bold);
		} else {
			date = new Phrase(" ", font10bold);
		}
		PdfPCell datecell = new PdfPCell(date);
		datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		righTable.addCell(datecell);

		PdfPCell rightCell = new PdfPCell(righTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createCustomerInfo() {

		PdfPTable table = new PdfPTable(3);

		try {
			table.setWidths(new float[] { 45, 30, 20 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		Phrase name = null;
		if (cust.isCompany() == true) {
			name = new Phrase(" " + cust.getCompanyName(), font10bold);
		} else {
			name = new Phrase("To " + "\n" + "\n" + cust.getFullname(),
					font10bold);
		}
		PdfPCell namecell = new PdfPCell(name);
		namecell.setBorder(0);
		namecell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getAdress() != null || !cust.getAdress().equals("")) {
			System.out.println("INSIDE CUSTOMER ADDRESS..............");

			if (cust.getAdress().getAddrLine2() != null
					|| !cust.getAdress().getAddrLine2().equals("")) {
				if (cust.getAdress().getLandmark() != null
						|| !cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2() + ","
							+ cust.getAdress().getLandmark();
				} else {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getAddrLine2();
				}
			} else {
				if (cust.getAdress().getLandmark() != null
						|| !cust.getAdress().getLandmark().equals("")) {
					custAdd1 = cust.getAdress().getAddrLine1() + ","
							+ cust.getAdress().getLandmark() + ",";
				} else {
					custAdd1 = cust.getAdress().getAddrLine1();
				}
			}

			if (cust.getAdress().getLocality() != null
					|| !cust.getAdress().getLocality().equals("")) {

				custFullAdd1 = custAdd1 + cust.getAdress().getCountry() + ","
						+ cust.getAdress().getState() + ","
						+ cust.getAdress().getCity() + " "
						+ cust.getAdress().getPin();
			}

		}
		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setBorder(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		leftTable.addCell(namecell);
		leftTable.addCell(blcell);
		leftTable.addCell(custAddInfoCell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		// leftCell.setBorder(0);

		// ************center table**************
		PdfPTable centerTable = new PdfPTable(1);
		centerTable.setWidthPercentage(100f);

		Phrase no = new Phrase("Order Cum- Contract No. ", font10bold);
		PdfPCell nocell = new PdfPCell(no);
		// nocell.setBorder(0);
		nocell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase dt = new Phrase("Contract Date ", font10bold);
		PdfPCell dtcell = new PdfPCell(dt);
		// dtcell.setBorder(0);
		dtcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase ref = new Phrase("P.O. Ref ", font10bold);
		PdfPCell refcell = new PdfPCell(ref);
		// refcell.setBorder(0);
		refcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase bill = new Phrase("Bill For the Month ", font10bold);
		PdfPCell billcell = new PdfPCell(bill);
		// billcell.setBorder(0);
		billcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		Phrase pay = new Phrase("Payment Terms ", font10bold);
		PdfPCell paycell = new PdfPCell(pay);
		// paycell.setBorder(0);
		paycell.setHorizontalAlignment(Element.ALIGN_LEFT);

		centerTable.addCell(nocell);
		centerTable.addCell(dtcell);
		centerTable.addCell(refcell);
		centerTable.addCell(billcell);
		centerTable.addCell(paycell);

		PdfPCell centerCell = new PdfPCell(centerTable);
		centerCell.setBorder(0);

		// *************righ table************
		PdfPTable rightTable = new PdfPTable(1);

		// try {
		// table.setWidths(colWidth);
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
		rightTable.setWidthPercentage(100f);

		// *******con no info***********
		Phrase no1 = null;
		if (con.getCount() != 0) {
			no1 = new Phrase(" " + con.getCount(), font10bold);
		} else {
			no1 = new Phrase(" ", font10bold);
		}
		PdfPCell no1cell = new PdfPCell(no1);
		// no1cell.setBorder(0);
		no1cell.setHorizontalAlignment(Element.ALIGN_CENTER);

		// ********con date info***********
		Phrase date = null;
		if (con.getContractDate() != null) {
			date = new Phrase(" " + fmt.format(con.getContractDate()),
					font10bold);
		} else {
			date = new Phrase(" ", font10bold);
		}
		PdfPCell datecell = new PdfPCell(date);
		// datecell.setBorder(0);
		datecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		// ********invoice ref info***********
		Phrase refno = null;
		if (invoice.getRefNumber() != null) {
			refno = new Phrase(" " + invoice.getRefNumber(), font10bold);
		} else {
			refno = new Phrase(" ", font10bold);
		}
		PdfPCell refnocell = new PdfPCell(refno);
		// refnocell.setBorder(0);
		refnocell.setHorizontalAlignment(Element.ALIGN_CENTER);

		// ********billing info***********
		Phrase billing = null;
		PdfPCell billingCell = null;
		for (int i = 0; i < invoice.getArrayBillingDocument().size(); i++) {
			if (invoice.getArrayBillingDocument().get(i).getBillingDate() != null) {
				billing = new Phrase(""
						+ (fmt1.format(invoice.getArrayBillingDocument().get(i)
								.getBillingDate())) + ",", font10bold);
			}
			billingCell = new PdfPCell(billing);
			billingCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}

		// ********pay terms info***********
		Phrase terms = null;
		if (invoice.getInvoiceGroup() != null) {
			terms = new Phrase(" " + invoice.getInvoiceGroup(), font10bold);
		} else {
			terms = new Phrase(" ", font10bold);
		}
		PdfPCell termscell = new PdfPCell(terms);
		// termscell.setBorder(0);
		termscell.setHorizontalAlignment(Element.ALIGN_CENTER);

		rightTable.addCell(no1cell);
		rightTable.addCell(datecell);
		rightTable.addCell(refnocell);
		rightTable.addCell(billingCell);
		rightTable.addCell(termscell);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(centerCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		// parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createBlankHeading1() {

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPCell blcell = new PdfPCell(blank);
		// blcell.setBorder(0);

		PdfPTable bltable = new PdfPTable(1);
		bltable.addCell(blcell);
		bltable.setWidthPercentage(100);

		try {
			document.add(bltable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createAddressAndProdTableInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 35, 65 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// ***********left table**************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		// customer service address
		Phrase address = new Phrase("Site Address ", font9bold);
		PdfPCell addresscell = new PdfPCell(address);
		// addresscell.setBorder(0);
		addresscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		leftTable.addCell(addresscell);

		String custAdd1 = "";
		String custFullAdd1 = "";

		if (cust.getSecondaryAdress() != null
				|| !cust.getSecondaryAdress().equals("")) {
			System.out.println("INSIDE CUSTOMER ADDRESS..............");

			if (cust.getSecondaryAdress().getAddrLine2() != null
					|| !cust.getSecondaryAdress().getAddrLine2().equals("")) {
				if (cust.getSecondaryAdress().getLandmark() != null
						|| !cust.getSecondaryAdress().getLandmark().equals("")) {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getAddrLine2() + ","
							+ cust.getSecondaryAdress().getLandmark();
				} else {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getAddrLine2();
				}
			} else {
				if (cust.getSecondaryAdress().getLandmark() != null
						|| !cust.getSecondaryAdress().getLandmark().equals("")) {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1() + ","
							+ cust.getSecondaryAdress().getLandmark() + ",";
				} else {
					custAdd1 = cust.getSecondaryAdress().getAddrLine1();
				}
			}

			if (cust.getSecondaryAdress().getLocality() != null
					|| !cust.getSecondaryAdress().getLocality().equals("")) {

				custFullAdd1 = custAdd1
						+ cust.getSecondaryAdress().getCountry() + ","
						+ cust.getSecondaryAdress().getState() + ","
						+ cust.getSecondaryAdress().getCity() + " "
						+ cust.getSecondaryAdress().getPin();
			}

		}

		Phrase custAddInfo = new Phrase(custFullAdd1, font9);
		PdfPCell custAddInfoCell = new PdfPCell(custAddInfo);
		custAddInfoCell.setBorder(0);
		custAddInfoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(custAddInfoCell);

		PdfPCell leftCell = new PdfPCell(leftTable);

		// **********right table*************
		PdfPTable rightTable = new PdfPTable(4);

		try {
			rightTable.setWidths(new float[] { 25, 20, 50, 20 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		rightTable.setWidthPercentage(100f);

		Phrase service = new Phrase("Services ", font9bold);
		Phrase frequency = new Phrase("Freq ", font9bold);
		Phrase period = new Phrase("Contract Period ", font9bold);
		Phrase amount = new Phrase("Amount ", font9bold);

		PdfPCell servcell = new PdfPCell(service);
		servcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell freqcell = new PdfPCell(frequency);
		freqcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell timecell = new PdfPCell(period);
		timecell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell amtcell = new PdfPCell(amount);
		amtcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		rightTable.addCell(servcell);
		rightTable.addCell(freqcell);
		rightTable.addCell(timecell);
		rightTable.addCell(amtcell);

		for (int i = 0; i < invoice.getSalesOrderProducts().size(); i++) {

			if (invoice.getSalesOrderProducts().get(i).getProdName() != null) {
				chunk = new Phrase(invoice.getSalesOrderProducts().get(i)
						.getProdName()
						+ "", font9);
			} else {
				chunk = new Phrase("", font9);
			}
			pdfserv = new PdfPCell(chunk);
			pdfserv.setHorizontalAlignment(Element.ALIGN_CENTER);
			rightTable.addCell(pdfserv);

			/******************************** Patch for terms for treatment *****************************/
			if (invoice.getSalesOrderProducts().get(i).getPrduct().getCount() != 0) {
				serprod = ofy()
						.load()
						.type(ServiceProduct.class)
						.filter("count",
								invoice.getSalesOrderProducts().get(i)
										.getPrduct().getCount()).first().now();
			}

			if (serprod != null) {
				logger.log(Level.SEVERE, serprod.getTermsoftreatment());
				if ((serprod.getTermsoftreatment() != null)
						&& (!serprod.getTermsoftreatment().equals(""))) {
					System.out.println("Inside If");
					logger.log(Level.SEVERE, "Inside If");

					chunk = new Phrase(serprod.getTermsoftreatment() + "",
							font8);
				} else {
					System.out.println("Inside else");
					logger.log(Level.SEVERE, "Inside else");
					chunk = new Phrase(invoice.getSalesOrderProducts().get(i)
							.getOrderServices()
							+ "", font8);
				}
			} else {
				chunk = new Phrase("", font8);
			}

			pdffreq = new PdfPCell(chunk);
			pdffreq.setHorizontalAlignment(Element.ALIGN_CENTER);
			rightTable.addCell(pdffreq);
			/***********************************************************************************/

			if (invoice.getContractStartDate() != null
					&& invoice.getContractEndDate() != null) {
				chunk = new Phrase(fmt.format(invoice.getContractStartDate())
						+ " To " + fmt.format(invoice.getContractEndDate()),
						font9);
			} else {
				chunk = new Phrase("", font9);
			}
			pdfperiod = new PdfPCell(chunk);
			pdfperiod.setHorizontalAlignment(Element.ALIGN_CENTER);
			rightTable.addCell(pdfperiod);

			if (invoice.getSalesOrderProducts().get(i).getPrice() != 0) {
				chunk = new Phrase(Math.round(invoice.getSalesOrderProducts()
						.get(i).getPrice())
						+ "", font9);
			} else {
				chunk = new Phrase("", font9);
			}
			pdfamt = new PdfPCell(chunk);
			pdfamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			rightTable.addCell(pdfamt);

		}

		PdfPCell rightCell = new PdfPCell(rightTable);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);
		// parentTable.addCell(blcell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/****************************************************************************************************/
	private void createProductTaxInfo() {

		PdfPTable table = new PdfPTable(3);

		try {
			table.setWidths(new float[] { 28.9f, 44.2f, 9.4f });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// ***********left table**************
		PdfPTable leftTable = new PdfPTable(1);
		leftTable.setWidthPercentage(100f);

		// **********payment terms info**************
		Phrase pay = new Phrase("Payment Terms : ", font9bold);
		PdfPCell payCell = new PdfPCell(pay);
		payCell.setBorder(0);
		payCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		leftTable.addCell(payCell);
		leftTable.addCell(blcell);

		// *****pay table*******
		PdfPTable payTable = new PdfPTable(2);

		try {
			payTable.setWidths(new float[] { 25f, 75f });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		payTable.setWidthPercentage(100f);

		Phrase percent = new Phrase("Percent", font9bold);
		PdfPCell percentCell = new PdfPCell(percent);
		percentCell.setBorder(0);
		percentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		payTable.addCell(percentCell);

		Phrase comment = new Phrase("Comment", font9bold);
		PdfPCell commentCell = new PdfPCell(comment);
		commentCell.setBorder(0);
		commentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		payTable.addCell(commentCell);

		Phrase payPercent = null;
		PdfPCell payPercentCell = null;

		Phrase payComment = null;
		PdfPCell payCommentCell = null;

		for (int i = 0; i < invoice.getArrPayTerms().size(); i++) {

			if (invoice.getArrPayTerms().get(i).getPayTermPercent() != null) {
				payPercent = new Phrase(Math.round(invoice.getArrPayTerms()
						.get(i).getPayTermPercent())
						+ "%", font9);
			} else {
				payPercent = new Phrase("", font9);
			}
			payPercentCell = new PdfPCell(payPercent);
			payPercentCell.setBorder(0);
			payPercentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			payTable.addCell(payPercentCell);

			if (invoice.getArrPayTerms().get(i).getPayTermComment() != null) {
				payComment = new Phrase(invoice.getArrPayTerms().get(i)
						.getPayTermComment()
						+ "", font9);
			} else {
				payComment = new Phrase("", font9);
			}
			payCommentCell = new PdfPCell(payComment);
			payCommentCell.setBorder(0);
			payCommentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			payTable.addCell(payCommentCell);

		}

		PdfPCell pay1Cell = new PdfPCell(payTable);
		pay1Cell.setBorder(0);
		leftTable.addCell(pay1Cell);

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// ***********center table**************
		PdfPTable centerTable = new PdfPTable(1);
		centerTable.setWidthPercentage(100f);

		Phrase subTotal = new Phrase("Sub Total ", font9bold);
		PdfPCell subTotalcell = new PdfPCell(subTotal);
		subTotalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centerTable.addCell(subTotalcell);

		Phrase conTotal = new Phrase("Contract Value ", font9);
		PdfPCell conTotalcell = new PdfPCell(conTotal);
		conTotalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centerTable.addCell(conTotalcell);

		/************************************************************************************/
		Phrase payTotal = null;
		PdfPCell payTotalcell = null;

		for (int i = 0; i < invoice.getArrPayTerms().size(); i++) {
			if (invoice.getArrPayTerms().get(i).getPayTermPercent() != null) {
				payTotal = new Phrase("Payment Term "
						+ Math.round(invoice.getArrPayTerms().get(i)
								.getPayTermPercent()) + "%", font9);
			}
		}
		payTotalcell = new PdfPCell(payTotal);
		payTotalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centerTable.addCell(payTotalcell);

		// if (invoice.getArrayBillingDocument().size() > 1) {
		// for (int i = 0; i < this.payTerms.size(); i++) {
		// if (payTerms.get(i).getPayTermPercent()
		// .equals(billingTaxesLis.get(i).getPaypercent())) {
		// for (int j = 0; j < invoice.getSalesOrderProducts().size(); j++) {
		// if (payTerms
		// .get(i)
		// .getPayTermPercent()
		// .equals(conbillingTaxesLis.get(i)
		// .getPayPercent())) {
		//
		// String payterm = this.conbillingTaxesLis.get(i)
		// .getPayPercent() + "";
		// chunk = new Phrase(
		// "Payment Terms " + payterm + "%", font8bold);
		// PdfPCell paytermCell = new PdfPCell(chunk);
		// paytermCell.setBorder(0);
		// centerTable.addCell(paytermCell);
		// }
		// }
		// }
		// }
		// }

		/*****************************************************************************/

		for (int i = 0; i < invoice.getBillingTaxes().size(); i++) {
			if (invoice.getBillingTaxes().get(i).getTaxChargeName()
					.equals("Service Tax")
					&& invoice.getBillingTaxes().get(i).getTaxChargePercent() == 14.5) {
				System.out.println("inside condition ::::::::");

				String str = "Service Tax " + " @ " + " 14%";

				Phrase strp = new Phrase(str, font9);
				PdfPCell strcell = new PdfPCell(strp);
				strcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				centerTable.addCell(strcell);

				String str2 = "Swatcha Bharat Cess" + " @ " + " 0.5%";

				Phrase str2p = new Phrase(str2, font9);
				PdfPCell str2cell = new PdfPCell(str2p);
				str2cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				centerTable.addCell(str2cell);

			} else if (invoice.getBillingTaxes().get(i).getTaxChargeName()
					.equals("Service Tax")
					&& invoice.getBillingTaxes().get(i).getTaxChargePercent() == 15) {

				String str = "Service Tax " + " @ " + " 14%";

				Phrase strp = new Phrase(str, font9);
				PdfPCell strcell = new PdfPCell(strp);
				strcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				centerTable.addCell(strcell);

				String str1 = "Swachha Bharat Cess" + " @ " + " 0.5%";

				Phrase str1p = new Phrase(str1, font9);
				PdfPCell str1cell = new PdfPCell(str1p);
				str1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				centerTable.addCell(str1cell);

				String str2 = "Krishi Kalyan Cess" + " @ " + " 0.5%";

				Phrase str2p = new Phrase(str2, font9);
				PdfPCell str2cell = new PdfPCell(str2p);
				str2cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				centerTable.addCell(str2cell);

			}
		}

		Phrase round = new Phrase("Round Up ", font9);
		PdfPCell roundcell = new PdfPCell(round);
		roundcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centerTable.addCell(roundcell);

		Phrase grand = new Phrase("Net Payable ", font9bold);
		PdfPCell grandcell = new PdfPCell(grand);
		grandcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		centerTable.addCell(grandcell);

		PdfPCell centerCell = new PdfPCell(centerTable);

		// ***********right table**************
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		double amount = 0;
		for (int i = 0; i < invoice.getSalesOrderProducts().size(); i++) {

			amount = amount
					+ Math.round(invoice.getSalesOrderProducts().get(i)
							.getTotalAmount());
		}
		Phrase ttl = new Phrase("" + amount, font9);
		pdfttl = new PdfPCell(ttl);
		pdfttl.setHorizontalAlignment(Element.ALIGN_RIGHT);
		rightTable.addCell(pdfttl);

		Phrase conttl = null;
		PdfPCell conttlCell = null;
		for (int i = 0; i < invoice.getSalesOrderProducts().size(); i++) {
			if (invoice.getTotalSalesAmount() != 0) {
				conttl = new Phrase("" + invoice.getTotalSalesAmount(), font9);
			}
		}
		conttlCell = new PdfPCell(conttl);
		conttlCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		rightTable.addCell(conttlCell);

		/**********************************************************************/
		// for (int i = 0; i < invoice.getBillingOtherCharges().size(); i++) {
		//
		// if (invoice.getBillingOtherCharges().get(i).getTaxChargeAssesVal() !=
		// 0) {
		// assesAmt = new Phrase(""
		// + invoice.getBillingOtherCharges().get(i)
		// .getTaxChargeAssesVal(), font9);
		// } else {
		// assesAmt = new Phrase("", font9);
		// }
		// assesCell = new PdfPCell(assesAmt);
		// assesCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// rightTable.addCell(assesCell);
		// }

		// /////////////////////////////////////////////////////

		double assesAmt = 0;
		PdfPCell assesCell = null;
		for (int i = 0; i < invoice.getSalesOrderProducts().size(); i++) {
			for (int j = 0; j < invoice.getArrPayTerms().size(); j++) {
				assesAmt = (invoice.getArrPayTerms().get(j).getPayTermPercent())
						* (amount) / 100;
			}

		}
		Phrase assesAmtp = new Phrase(assesAmt + "", font9);
		assesCell = new PdfPCell(assesAmtp);
		assesCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		rightTable.addCell(assesCell);

		/***********************************************************************/
		// if (invoice.getArrayBillingDocument().size() > 1) {
		// for (int i = 0; i < this.payTerms.size(); i++) {
		// if (payTerms.get(i).getPayTermPercent()
		// .equals(billingTaxesLis.get(i).getPaypercent())) {
		// for (int j = 0; j < invoice.getSalesOrderProducts().size(); j++) {
		// if (payTerms
		// .get(i)
		// .getPayTermPercent()
		// .equals(conbillingTaxesLis.get(i)
		// .getPayPercent())) {
		//
		// double total = 0;
		// total = ((con.getTotalAmount() * payTerms.get(i)
		// .getPayTermPercent()) / 100);
		//
		// Phrase totalphase = new Phrase(df.format(total),
		// font8bold);
		// PdfPCell totalcell = new PdfPCell(totalphase);
		// totalcell
		// .setHorizontalAlignment(Element.ALIGN_RIGHT);
		// totalcell.setBorder(0);
		// rightTable.addCell(totalcell);
		// }
		// }
		// }
		// }
		// }

		/*******************************************************************/

		for (int i = 0; i < invoice.getBillingTaxes().size(); i++) {

			if (invoice.getBillingTaxes().get(i).getTaxChargeName()
					.equals("Service Tax")
					&& invoice.getBillingTaxes().get(i).getTaxChargePercent() == 14.5) {

				// double taxAmt = 14 * invoice.getTotalBillingAmount() / 100;
				double taxAmt = 14 * (assesAmt) / 100;
				Phrase taxAmtp = new Phrase(Math.round(taxAmt) + "", font9);

				pdftaxAmt = new PdfPCell(taxAmtp);
				pdftaxAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rightTable.addCell(pdftaxAmt);

				// double taxAmt1 = 0.5 * invoice.getTotalBillingAmount() / 100;
				double taxAmt1 = 0.5 * (assesAmt) / 100;
				Phrase taxAmt1p = new Phrase(Math.round(taxAmt1) + "", font9);

				pdftaxAmt1 = new PdfPCell(taxAmt1p);
				pdftaxAmt1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rightTable.addCell(pdftaxAmt1);

			} else if (invoice.getBillingTaxes().get(i).getTaxChargeName()
					.equals("Service Tax")
					&& invoice.getBillingTaxes().get(i).getTaxChargePercent() == 15) {

				// double taxAmt = 14 * invoice.getTotalBillingAmount() / 100;
				double taxAmt = 14 * (assesAmt) / 100;
				Phrase taxAmtp = new Phrase(Math.round(taxAmt) + "", font9);

				pdftaxAmt = new PdfPCell(taxAmtp);
				pdftaxAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rightTable.addCell(pdftaxAmt);

				// double taxAmt1 = 0.5 * invoice.getTotalBillingAmount() / 100;
				double taxAmt1 = 0.5 * (assesAmt) / 100;
				Phrase taxAmt1p = new Phrase(Math.round(taxAmt1) + "", font9);

				pdftaxAmt1 = new PdfPCell(taxAmt1p);
				pdftaxAmt1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rightTable.addCell(pdftaxAmt1);

				// double taxAmt2 = 0.5 * invoice.getTotalBillingAmount() / 100;
				double taxAmt2 = 0.5 * (assesAmt) / 100;
				Phrase taxAmt2p = new Phrase(Math.round(taxAmt2) + "", font9);

				pdftaxAmt2 = new PdfPCell(taxAmt2p);
				pdftaxAmt2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				rightTable.addCell(pdftaxAmt2);

			}

		}

		double roundval = Math.round(invoice.getInvoiceAmount());
		// double roundval = Math.round(con.getNetpayable());
		System.out.println("Round up value:::::::::::"
				+ Math.round(invoice.getInvoiceAmount()));

		Phrase roundp = new Phrase(roundval + "", font9);
		PdfPCell roundpcell = new PdfPCell(roundp);
		roundpcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		rightTable.addCell(roundpcell);

		Phrase grandp = new Phrase(roundval + "", font9bold);
		PdfPCell grandpcell = new PdfPCell(grandp);
		grandpcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		rightTable.addCell(grandpcell);

		PdfPCell rightCell = new PdfPCell(rightTable);

		table.addCell(leftCell);
		table.addCell(centerCell);
		table.addCell(rightCell);

		PdfPCell parentCell = new PdfPCell(table);
		// cell.setBorder(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(parentCell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/*******************************************************************************/

	private void createInWordsDetail() {

		PdfPTable rstable = new PdfPTable(2);

		try {
			rstable.setWidths(colWidth2);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		rstable.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorderWidthTop(0);
		blcell.setBorderWidthBottom(0);

		Phrase rs = new Phrase("In Words   :", font10bold);
		PdfPCell rscell = new PdfPCell(rs);
		rscell.setBorder(0);
		rscell.setHorizontalAlignment(Element.ALIGN_LEFT);

		String amountInWord = ServiceInvoicePdf.convert(Math.round(invoice
				.getInvoiceAmount()));
		Phrase bl1 = new Phrase("" + amountInWord + " Only ", font10bold);
		PdfPCell bl1cell = new PdfPCell(bl1);
		bl1cell.setBorder(0);
		bl1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

		rstable.addCell(rscell);
		rstable.addCell(bl1cell);

		PdfPCell cell = new PdfPCell(rstable);
		cell.setBorderWidthBottom(0);

		PdfPTable amttable = new PdfPTable(1);
		amttable.addCell(cell);
		amttable.addCell(blcell);
		amttable.setWidthPercentage(100);

		try {
			document.add(amttable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	private void createBottomInfo() {

		PdfPTable table = new PdfPTable(2);

		try {
			table.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);

		Phrase bl = new Phrase(" ");
		PdfPCell blcell = new PdfPCell(bl);
		blcell.setBorder(0);

		// *************left table************
		PdfPTable leftTable = new PdfPTable(2);

		try {
			table.setWidths(colWidth1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		leftTable.setWidthPercentage(100f);

		Phrase col = new Phrase(":");
		PdfPCell colcell = new PdfPCell(col);
		colcell.setBorder(0);
		colcell.setHorizontalAlignment(Element.ALIGN_LEFT);

		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {

			PdfPCell articleInfocell = null;
			PdfPCell articleInfo1cell = null;

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
				Phrase abc = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName() != null) {
					abc = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeName()
							+ " : ", font9);
				}
				articleInfocell = new PdfPCell(abc);
				articleInfocell.setBorder(0);
				articleInfocell.setHorizontalAlignment(Element.ALIGN_LEFT);

				Phrase abc1 = null;
				if (comp.getArticleTypeDetails().get(i).getArticleTypeValue() != null) {
					abc1 = new Phrase(comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue(), font9);
				}
				articleInfo1cell = new PdfPCell(abc1);
				articleInfo1cell.setBorder(0);
				articleInfo1cell.setHorizontalAlignment(Element.ALIGN_LEFT);

				leftTable.addCell(articleInfocell);
				// leftTable.addCell(colcell);
				leftTable.addCell(articleInfo1cell);
			}
		}

		PdfPCell leftCell = new PdfPCell(leftTable);
		leftCell.setBorder(0);

		// *************righ table************
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setWidthPercentage(100f);

		Phrase company = null;
		if (comp.getBusinessUnitName() != null) {
			company = new Phrase("For "
					+ comp.getBusinessUnitName().toUpperCase(), font10bold);
		} else {
			company = new Phrase(" ", font10bold);
		}

		Phrase sign = new Phrase("( Authorized Signatory ) ", font10bold);

		PdfPCell compcell = new PdfPCell(company);
		compcell.setBorder(0);
		compcell.setHorizontalAlignment(Element.ALIGN_CENTER);

		PdfPCell signcell = new PdfPCell(sign);
		signcell.setBorder(0);
		signcell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		rightTable.addCell(compcell);
		rightTable.addCell(blcell);
		rightTable.addCell(blcell);
		rightTable.addCell(blcell);
		rightTable.addCell(signcell);
		rightTable.setSpacingAfter(4f);

		PdfPCell rightCell = new PdfPCell(rightTable);
		rightCell.setBorder(0);

		table.addCell(leftCell);
		table.addCell(rightCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBorderWidthTop(0);

		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100);
		parentTable.addCell(cell);

		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	public void createPdfForEmail(Invoice invDetails, Company comp2,Customer c, Contract servcont) {
	
		invoice = invDetails;
		comp = comp2;
		cust = c;
		con =servcont;
		
		
		if (invoice.getArrayBillingDocument().size() > 1) {
			for (int i = 0; i < invoice.getArrayBillingDocument().size(); i++) {
				BillingDocument bill = new BillingDocument();
				if (invoice.getCompanyId() != null) {
					bill = ofy()
							.load()
							.type(BillingDocument.class)
							.filter("companyId", invoice.getCompanyId())
							.filter("contractCount", invoice.getContractCount())
							.filter("count",
									invoice.getArrayBillingDocument().get(i)
											.getBillId())
							.filter("typeOfOrder",
									invoice.getTypeOfOrder().trim()).first()
							.now();
				} else {
					bill = ofy()
							.load()
							.type(BillingDocument.class)
							.filter("contractCount", invoice.getContractCount())
							.filter("count",
									invoice.getArrayBillingDocument().get(i)
											.getBillId())
							.filter("typeOfOrder",
									invoice.getTypeOfOrder().trim()).first()
							.now();
				}
				payTerms.addAll(bill.getArrPayTerms());

			}

		} else {
			payTerms.addAll(invoice.getArrPayTerms());
		}

		// *********process config code********************
		if (invoice.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoice.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			System.out.println("PROCESS CONFIG");
			if (processConfig != null) {
				System.out.println("PROCESS CONFIG NOT NULL");
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("HygeiaPestManagement")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						System.out.println("PROCESS CONFIG FLAG FALSE");
						hygeiaPest = true;
					}
				}
			}
		}

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));

		String amountInWord = ServiceInvoicePdf.convert(Math.round(invoice
				.getInvoiceAmount()));
		
		createLogo(document, comp);
		createBlankHeading();
		createTitle();
		createInvoiceInfo();
		createCustomerInfo();
		createBlankHeading1();
		createAddressAndProdTableInfo();
		createProductTaxInfo();
		createInWordsDetail();
		createBottomInfo();
	}
}
