package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.ServicePo;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;

public class ServicePoPdf {	
	
	ServicePo po;
	List<SalesLineItem> products;
	List<PaymentTerms> payTermsLis;
	List<ProductOtherCharges> prodCharges;
	List<ProductOtherCharges> prodTaxes;
	ArrayList<SuperProduct>stringlis= new ArrayList<SuperProduct>();
	List<ArticleType> articletype;
	Vendor vendor;
	ProcessConfiguration processConfig;
	boolean CompanyNameLogoflag=false;
	Company comp;
	Employee emp;
	public Document document;
	int vendorID;
	SuperProduct sup;
	double total=0;
	
	/****************************************************************************************/
	
	int flag=0;
	Phrase chunk;
	int firstBreakPoint = 15;
	int BreakPoint=5;
	float size;
	int count=0;
	int rohan=123;
	float blankLines;
	PdfPCell pdfcategcell,pdfnamecell,pdfuomcell,pdfqtycell,pdfspricecell,pdfperdiscount,pdfservice,pdfvattax,pdftotalproduct;
	Logger logger=Logger.getLogger("Purchaseorder");
	float[] columnWidths = {0.5f, 3.5f, 0.5f, 0.5f,0.7f,0.5f,0.9f,1.2f};
	float[] columnWidths2={1.5f,0.2f,1.2f,1.6f,0.2f,1.4f};
	private Font font16boldul, font12bold, font8bold,font9bold, font8, font12boldul,font12,font14bold,font10,font10bold,font9;
	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df = new DecimalFormat("0.00");
	
	final static String disclaimerText = "I/We hereby certify that my/our registration certificate"
			+ " Under the Maharashtra Value Added Tax Act 2002, is in force on the date on which the sale "
			+ "of the goods specified in this Invoice is made by me/us and that the transaction of sales "
			+ "covered by this Invoice has been effected by me/us and it shall be accounted for in the turnover "
			+ "of sales while filling of return and the due tax, if any, payable on the sale has been paid or shall be paid.";
		
		
		
	/**
	 * This map stores warehouse name as key and its address as value.
	 * Date:04-10-2016 By Anil
	 * Release :30 Sept 2016 
	 * Project: PURCHASE MODIFICATION(NBHC)
	 */
	HashMap<String,String> warehouseAddressMap=new HashMap<String,String>();
	
	
	/**
	 * This is a configuration flag for vendor instruction to be printed.
	 * Date : 15-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project : PURCHASE MODIFICATION(NBHC)
	 */
	boolean vendInsFlag=false;	
		
	public ServicePoPdf() {
		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD| Font.UNDERLINE);
		new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD | Font.UNDERLINE);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 8);
		font9 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font10 = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
	}
		
	public void setPurchaseOrder(long count) {
		// Load PurchaseOrder
		po = ofy().load().type(ServicePo.class).id(count).now();
	
		// Load Company
		if (po.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", po.getCompanyId()).first().now();
		
//******************rohan chnages here for purchase engg details **************
		
		// Load Company
		if (po.getCompanyId() == null)
			emp = ofy().load().type(Employee.class).filter("fullname", po.getEmployee()).first().now();
		else
			emp = ofy().load().type(Employee.class)
					.filter("companyId", po.getCompanyId()).filter("fullname", po.getEmployee()).first().now();
		
		
		
		
		//********************changes ends here *************************
		
		
	
		// Load vendor
		if (po.getVinfo() != null) {
			vendorID = po.getVinfo().getCount();
		}
		vendor = (Vendor) ofy().load().type(Vendor.class).filter("companyId", po.getCompanyId()).filter("count", vendorID).first().now();

		
		
		/**
		 * This code is added ,so that for same product multiple description should not get printed.
		 * Date : 04-06-2016 By Anil
		 * Release : 30 Sep 2016
		 * Project : PURCHASE MODIFICATION(NBHC)
		 */
		HashSet<Integer> uniqueProdLis=new HashSet<Integer>();
		for(SalesLineItem obj:po.getItems()){
			uniqueProdLis.add(obj.getPrduct().getCount());
		}
		ArrayList<Integer> prodLis=new ArrayList<Integer>();
		prodLis.addAll(uniqueProdLis);
		
		
		for(Integer obj:prodLis){
			
			sup = ofy().load().type(SuperProduct.class).filter("companyId", po.getCompanyId())
					.filter("count",obj).first().now();
			
			SuperProduct superprod = new SuperProduct();
			superprod.setCount(sup.getCount());
			superprod.setProductName(sup.getProductName());
			superprod.setComment(sup.getComment() + " "+ sup.getCommentdesc());
			superprod.setProductImage(sup.getProductImage());
			stringlis.add(superprod);
		}
		
		/**
		 * END
		 */
		
		/***
		 * Storing warehouse name and its address in map.
		 */
		
		for(SalesLineItem obj:po.getItems()){
			if(obj.getWarehouseName().equals("")){
				break;
			}
			
			WareHouse warehouse=ofy().load().type(WareHouse.class).filter("companyId", po.getCompanyId())
					.filter("buisnessUnitName",obj.getWarehouseName().trim()).first().now();
			
			if(warehouse!=null){
				System.out.println("WH ___________-");
				if(warehouseAddressMap!=null&&warehouseAddressMap.size()!=0){
					if(!warehouseAddressMap.containsKey(obj.getWarehouseName().trim())){
						warehouseAddressMap.put(obj.getWarehouseName().trim(), warehouse.getAddress().getCompleteAddress());
					}
				}else{
					warehouseAddressMap.put(obj.getWarehouseName().trim(), warehouse.getAddress().getCompleteAddress());
				}
			}
		}
		
		/**
		 * END
		 */
			
		if(po.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", po.getCompanyId()).filter("processName", "PurchaseOrder").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						CompanyNameLogoflag=true;
					}
				}
			}
		}
		
		products = po.getItems();
		prodCharges = po.getProductCharges();
		prodTaxes = po.getProductTaxes();
		payTermsLis = po.getPaymentTermsList();
		
		articletype = new ArrayList<ArticleType>();
		if(vendor.getArticleTypeDetails().size()!=0){
			articletype.addAll(vendor.getArticleTypeDetails());
		}
		if(comp.getArticleTypeDetails().size()!=0){
			articletype.addAll(comp.getArticleTypeDetails());
		}
		
		logger.log(Level.SEVERE,"Size of Article Typefor Vendor"+articletype.size());
		
		/**
		 * Checking configuration for vendor instruction
		 * Date : 15 -10 -016 By ANil
		 * Release : 30-Sept-2016
		 * Project:PURCHASE MODIFICATION(NBHC)
		 */
		if(po.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", po.getCompanyId()).filter("processName", "PurchaseOrder").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("VENDORINSTRUCTIONONPOPDF")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						vendInsFlag=true;
					}
				}
			}
		}
		/**
		 * End
		 */
	}
		
	/**
	 * This method returns the address of warehouse passed as parameter.
	 * Date : 04-10-2016 By Anil
	 * Release : 30 Sept 2016
	 * Project: PURCHASE MODIFICATION (NBHC)
	 * I/P: Warehouse name 
	 * O/P: Warehouse Complete address
	 */
	public String getWareHouseAddress(String warehouseName){
		if(warehouseAddressMap!=null&&warehouseAddressMap.size()!=0){
			if(warehouseAddressMap.containsKey(warehouseName.trim())){
				return warehouseAddressMap.get(warehouseName.trim());
			}
		}
		return "";
	}
	
	/**
	 * END
	 */
		
	public void createPdf() {
		Createblank();
		if(CompanyNameLogoflag==true){
			createLogo(document,comp);
		}
		else{
			createLogo123(document,comp);
		}
		createHeadingInfo();
		createProductInfo();
		termsConditionsInfo();
		footerInfo();
	
	}
		
		
	public  void createPdfForEmail(Company comp,Vendor cust ,ServicePo po,Employee employeeEnt) {
		 
	      this.comp=comp;
	      this.po=po;
	      this.vendor=cust;
	      this.emp=employeeEnt;
	      this.products = po.getItems();
	      this.payTermsLis = po.getPaymentTermsList();
	      this.prodCharges = po.getProductCharges();
	      this.prodTaxes = po.getProductTaxes();
	      articletype = new ArrayList<ArticleType>();
	  	if(cust.getArticleTypeDetails().size()!=0){
	  		articletype.addAll(cust.getArticleTypeDetails());
	  	}
	  	if(comp.getArticleTypeDetails().size()!=0){
	  		articletype.addAll(comp.getArticleTypeDetails());
	  	}
	  	

		if(po.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", po.getCompanyId()).filter("processName", "PurchaseOrder").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintCompanyNameAsLogo")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.CompanyNameLogoflag=true;
					}
				}
			}
		}
	  	
	  	commentDetailsForEmail();
	      Createblank();
	      if(CompanyNameLogoflag==true){
				createLogo(document,comp);
			}
			else{
				createLogo123(document,comp);
			}
	      createHeadingInfo();
			createProductInfo();
			termsConditionsInfo();
			footerInfo();
	      
	    }


	private void Createblank() {
	    Paragraph blank =new Paragraph();
	    blank.add(Chunk.NEWLINE);
	    try {
			document.add(blank);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void createLogo(Document doc, Company comp) {
		
		//********************logo for server ********************
		DocumentUpload document =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(250f);
			image2.setAbsolutePosition(40f,745f);
			doc.add(image2);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
	//***********************for local **************************
	
	//try
	//{
	//Image image1=Image.getInstance("images/logo1.png");
	//
	//image1.scalePercent(15f);
	//image1.scaleAbsoluteWidth(250f);
	//image1.setAbsolutePosition(35f,725f);	
	//doc.add(image1);
	//}
	//catch(Exception e)
	//{
	//	e.printStackTrace();
	//}
	//
}

		

private void createLogo123(Document doc, Company comp) {
		

//		********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,745f);	
		doc.add(image2);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,765f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/ultrapest.jpg");
//	image1.scalePercent(20f);
//	image1.setAbsolutePosition(40f,745f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
//	
	}


		
		
	public void createHeadingInfo() {
		
		Paragraph p =new Paragraph();
		Phrase blankspace=null;
		Phrase companyName=null;
		
		if(CompanyNameLogoflag==true){
			blankspace = new Phrase(" ",font12);
			
			p.add(blankspace);
			}
		else {
			
			companyName = new Phrase("                  "+comp.getBusinessUnitName().toUpperCase(),font12bold);
		}

	p.add(Chunk.NEWLINE);
//		p.add(companyName);
	
		PdfPCell companyHeadingCell = new PdfPCell();
		companyHeadingCell.setBorder(0);
		companyHeadingCell.addElement(p);
		 if(CompanyNameLogoflag==true){
		    	companyHeadingCell.addElement(blankspace);
		    	companyHeadingCell.addElement(blankspace);
		    	}else {
		    		companyHeadingCell.addElement(companyName);
		    	}
		Phrase adressline1 = new Phrase(comp.getAddress().getAddrLine1(),font10);
		Phrase adressline2 = null;
		if (comp.getAddress().getAddrLine2() != null) {
			adressline2 = new Phrase(comp.getAddress().getAddrLine2(), font10);
		}else{
			
			adressline2 = new Phrase("",font10);
		}
		Phrase landmark = null;
		Phrase locality = null;
		Phrase city = null;
		if (comp.getAddress().getLandmark() != null) {
			 landmark =new Phrase(comp.getAddress().getLandmark(),font10);
			 
		}else{
			landmark =new Phrase("",font10);
		}
		if(comp.getAddress().getLocality()!=null){
			locality = new Phrase(comp.getAddress().getLocality(),font10);
		}else{
			locality = new Phrase("",font10);
		}
			
			city = new Phrase( comp.getAddress().getCity() + " - "+ comp.getAddress().getPin(), font10);
	
		PdfPCell addressline1cell = new PdfPCell();
		PdfPCell addressline2cell = new PdfPCell();
		addressline1cell.addElement(adressline1);
		addressline1cell.setBorder(0);
			
		addressline2cell.addElement(adressline2);
		addressline2cell.setBorder(0);
	
		PdfPCell landmarkcell = new PdfPCell();
		landmarkcell.addElement(landmark);
		landmarkcell.setBorder(0);
		
		PdfPCell localitycell = new PdfPCell();
		localitycell.addElement(locality);
		localitycell.setBorder(0);
		
		PdfPCell citycell = new PdfPCell();
		citycell.addElement(city);
		citycell.setBorder(0);
	
		String contactinfo = "Mobile: " + comp.getContact().get(0).getCellNo1();
		if (comp.getContact().get(0).getCellNo2() != 0)
			contactinfo = contactinfo + ","
					+ comp.getContact().get(0).getCellNo2();
		if (comp.getContact().get(0).getLandline() !=0) {
			contactinfo = contactinfo + "     " + " Phone: "
					+ comp.getContact().get(0).getLandline();
		}
	
		Phrase contactnos = new Phrase(contactinfo, font9);
		Phrase email = new Phrase("Email 1: "+ comp.getContact().get(0).getEmail(),font9);
		Phrase email2 = new Phrase("Email 2: "+ comp.getPocEmail(),font9);
		
		//********************rohan changes here for purchase engg details ******************
		Phrase PurchaseEnggName = new Phrase("Purchase Engg. :"+emp.getFullname()+" / "+emp.getCellNumber1(),font9); 
		Phrase PurchaseEnggEmail = new Phrase("Email :"+emp.getEmail(),font9);
		
		PdfPCell PurchaseEnggNamecell = new PdfPCell();
		PurchaseEnggNamecell.addElement(PurchaseEnggName);
		PurchaseEnggNamecell.setBorder(0);
		PurchaseEnggNamecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPCell PurchaseEnggEmailcell = new PdfPCell();
		PurchaseEnggEmailcell.addElement(PurchaseEnggEmail);
		PurchaseEnggEmailcell.setBorder(0);
		PurchaseEnggEmailcell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
		
		
		//***************************changes here *******************************************
		
		
		
		
	
		PdfPCell contactcell = new PdfPCell();
		contactcell.addElement(contactnos);
		contactcell.setBorder(0);
		PdfPCell emailcell = new PdfPCell();
		emailcell.setBorder(0);
		emailcell.addElement(email);
		
		PdfPCell email2cell = new PdfPCell();
		email2cell.setBorder(0);
		email2cell.addElement(email2);
	
		PdfPTable companytable = new PdfPTable(1);
		companytable.addCell(companyHeadingCell);
		companytable.addCell(addressline1cell);
		if (!comp.getAddress().getAddrLine2().equals("")) {
			companytable.addCell(addressline2cell);
		}
		if(!comp.getAddress().getLandmark().equals("")){
			companytable.addCell(landmarkcell);
		}
		if(!comp.getAddress().getLocality().equals("")){
			companytable.addCell(localitycell);
		}
	
		companytable.addCell(citycell);
		companytable.addCell(contactcell);
		companytable.addCell(emailcell);
		companytable.addCell(email2cell);
		
		companytable.addCell(PurchaseEnggNamecell);
		companytable.addCell(PurchaseEnggEmailcell);
		
		
		companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
		companytable.setWidthPercentage(100);
	
		
		
		/**
		 * Vendor Info
		 */
	
		
		String tosir="To, M/S";
		
		String vendorN = vendor.getVendorName();
	
		Phrase vendorNa = new Phrase(vendorN, font14bold);
		Paragraph fulln = new Paragraph();
		fulln.add(Chunk.NEWLINE);
		fulln.setFont(font12bold);
		fulln.add(tosir+"  "+vendorN);
	
		
	
		
		
		
		Phrase vendoradress = new Phrase("                  "+vendor.getPrimaryAddress().getAddrLine1(), font10);
		Phrase vendoradress2 = null;
		if (vendor.getPrimaryAddress().getAddrLine2() != null) {
			vendoradress2 = new Phrase("                  "+vendor.getPrimaryAddress().getAddrLine2(), font10);
		} 
		
		
		Phrase vendorlocality = null;
		Phrase venderlandmek=null;
		if (vendor.getPrimaryAddress().getLandmark() != null) {
			String landmarks = vendor.getPrimaryAddress().getLandmark();
			venderlandmek = new Phrase("                  "+landmarks , font10);
		} else {
			venderlandmek = new Phrase("", font10);
		}
		
		if(vendor.getPrimaryAddress().getLocality()!=null){
			String locality1= vendor.getPrimaryAddress().getLocality();
			vendorlocality=new Phrase("                  "+locality1,font10);
		}else{
			vendorlocality=new Phrase("",font10);
		}
	
		Phrase cityphrase=new Phrase("                  "+vendor.getPrimaryAddress().getCity() + " - "
				+ vendor.getPrimaryAddress().getPin(), font10);
	
		PdfPCell locacell = new PdfPCell();
		locacell.addElement(vendorlocality);
		locacell.setBorder(0);
		
		PdfPCell city1cell = new PdfPCell();
		city1cell.addElement(cityphrase);
		city1cell.setBorder(0);
	//	citycell.addElement(Chunk.NEWLINE);
		
		PdfPCell venaddressline1cell = new PdfPCell();
		PdfPCell venaddressline2cell = new PdfPCell();
		
		venaddressline1cell.addElement(vendoradress);
		venaddressline1cell.setBorder(0);
	//	venaddressline1cell.addElement(Chunk.NEWLINE);
		
		if (vendoradress2 != null) {
			venaddressline2cell.addElement(vendoradress2);
			venaddressline2cell.setBorder(0);
	//		venaddressline2cell.addElement(Chunk.NEWLINE);
		}
		
		PdfPCell venlocalitycell = new PdfPCell();
		venlocalitycell.addElement(venderlandmek);
		venlocalitycell.setBorder(0);
	//	venlocalitycell.addElement(Chunk.NEWLINE);
		
		
		String contactinfon = "Cell: " + vendor.getCellNumber1();
		if (vendor.getCellNumber2() != -1)
			contactinfon = contactinfon + "," + vendor.getCellNumber2();
		if (vendor.getLandline() != -1) {
			contactinfon = contactinfon + "     " + " Tel: "
					+ vendor.getLandline();
		}
	
		Phrase vendorcontact = new Phrase(contactinfon, font9);
		Phrase vendoremail = new Phrase("Email: " + vendor.getEmail(), font9);
	
		PdfPCell vendorcontactcell = new PdfPCell();
		vendorcontactcell.addElement(vendorcontact);
		vendorcontactcell.setBorder(0);
	
		PdfPCell vendoremailcell = new PdfPCell();
		vendoremailcell.addElement(vendoremail);
		vendoremailcell.setBorder(0);
		
		
		PdfPCell vendorNcell = new PdfPCell();
		vendorNcell.addElement(fulln);
		vendorNcell.setBorder(0);
	
		String vendorFullName = "";
//		if (vendor.getMiddleName() != null) {
//			vendorFullName ="Attn: "+vendor.getFirstName().trim() + " "+ vendor.getMiddleName().trim() + " "
//					+ vendor.getLastName().trim();
//		} else {
//			vendorFullName ="Attn: "+ vendor.getFirstName().trim() + " "+ vendor.getLastName().trim();
//		}
	
		// new code added by vijay for vendor poc full name
		
		if(vendor.getfullName()!=null){
			vendorFullName ="Attn: "+ vendor.getfullName().trim();
		}
		
		Phrase vendorName = new Phrase(vendorFullName, font9);
		Paragraph fullname = new Paragraph();
		fullname.add(vendorName);
	
		PdfPCell vendornamecell = new PdfPCell();
		vendornamecell.addElement(fullname);
		vendornamecell.setBorder(0);
		
		PdfPCell refcell=null;
		PdfPCell refcell1=null;
		PdfPCell refdatecell=null;
		PdfPCell refdatecell1=null;
		Phrase refphrase;
		Phrase refphrase1;
		if(!po.getRefNum().equals("")){
			
			 refphrase=new Phrase("Reference No.",font9);
			 refphrase1=new Phrase(po.getRefNum(),font9);
			
			
			refcell = new PdfPCell();
			refcell.addElement(refphrase);
			refcell.setBorder(0);
			refcell1 = new PdfPCell();
			refcell1.addElement(refphrase1);
			refcell1.setBorder(0);
			
			
		}else{
			
			refphrase=new Phrase("",font9);
			 refphrase1=new Phrase("",font9);
			
			
			refcell = new PdfPCell();
			refcell.addElement(refphrase);
			refcell.setBorder(0);
			refcell1 = new PdfPCell();
			refcell1.addElement(refphrase1);
			refcell1.setBorder(0);
		}
		
		Phrase refdatephrase;
		Phrase refdatephrase1;
		 if(po.getRefDate()!=null){
			 
			 refdatephrase=new Phrase("Reference Date",font9);
			refdatephrase1=new Phrase(fmt.format(po.getRefDate()),font9);
			 
				refdatecell = new PdfPCell();
				refdatecell.addElement(refdatephrase);
				refdatecell.setBorder(0);
				refdatecell1 = new PdfPCell();
				refdatecell1.addElement(refdatephrase1);
				refdatecell1.setBorder(0);
				refdatecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				
				
		 }else{
			 	refdatephrase=new Phrase("",font9);
				refdatephrase1=new Phrase("",font9);
				 
					refdatecell = new PdfPCell();
					refdatecell.addElement(refdatephrase);
					refdatecell.setBorder(0);
					refdatecell1 = new PdfPCell();
					refdatecell1.addElement(refdatephrase1);
					refdatecell1.setBorder(0);
					refdatecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			 
		 }
		
		
//		PdfPCell quocell = null;
//		PdfPCell quocell1 = null;
//		PdfPCell quodateCell = null;
//		PdfPCell quodateCell1 = null;
//
//		Phrase quophrase;
//		Phrase quophrase1;
//		if (po.getRFQID() != 0) {
//			quophrase = new Phrase("Quotation No.", font9);
//			quophrase1 = new Phrase(po.getRFQID() + "", font9);
//			quocell = new PdfPCell();
//			quocell.addElement(quophrase);
//			quocell.setBorder(0);
//			quocell1 = new PdfPCell();
//			quocell1.addElement(quophrase1);
//			quocell1.setBorder(0);
//		} else {
//			quophrase = new Phrase("", font9);
//			quophrase1 = new Phrase("", font9);
//			quocell = new PdfPCell();
//			quocell.addElement(quophrase);
//			quocell.setBorder(0);
//			quocell1 = new PdfPCell();
//			quocell1.addElement(quophrase1);
//			quocell1.setBorder(0);
//		}
//		
//		Phrase quodatephrase;
//		Phrase quodatephrase1;
//		if (po.getRFQDate() != null) {
//			quodatephrase = new Phrase("Quotation Date", font9);
//			quodatephrase1 = new Phrase(fmt.format(po.getRFQDate()), font9);
//			quodateCell = new PdfPCell();
//			quodateCell.addElement(quodatephrase);
//			quodateCell.setBorder(0);
//			quodateCell1 = new PdfPCell();
//			quodateCell1.addElement(quodatephrase1);
//			quodateCell1.setBorder(0);
//			quodateCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		} else {
//			quodatephrase = new Phrase("", font9);
//			quodatephrase1 = new Phrase("", font9);
//			quodateCell = new PdfPCell();
//			quodateCell.addElement(quodatephrase);
//			quodateCell.setBorder(0);
//			quodateCell1 = new PdfPCell();
//			quodateCell1.addElement(quodatephrase1);
//			quodateCell1.setBorder(0);
//			quodateCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		}
		 
		 Phrase seperator=new Phrase(":",font9);
		 PdfPCell seperatorcell=new PdfPCell();
		 seperatorcell.addElement(seperator);
		 seperatorcell.setBorder(0);
		 seperatorcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPTable infotable=new PdfPTable(6);
		 infotable.setHorizontalAlignment(100);
		 try {
			infotable.setWidths(columnWidths2);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	
		if (!po.getRefNum().equals("")) {
			infotable.addCell(refcell);
			infotable.addCell(seperatorcell);
			infotable.addCell(refcell1);
		}
		if (po.getRefDate() != null) {
			infotable.addCell(refdatecell);
			infotable.addCell(seperatorcell);
			infotable.addCell(refdatecell1);
		}
		
//		if(po.getRFQID()!=0){
//			 infotable.addCell(quocell);
//			 infotable.addCell(seperatorcell);
//			 infotable.addCell(quocell1);
//			 
//		 }
//		 if(po.getRFQDate()!=null){
//			 infotable.addCell(quodateCell);
//			 infotable.addCell(seperatorcell);
//			 infotable.addCell(quodateCell1);
//		 }
		
		PdfPCell  infotablecell1=new PdfPCell(infotable);
		infotablecell1.setBorder(0);
		
		
		
	
		/*
		 * Vendor Table
		 */
	
		PdfPTable vendortable = new PdfPTable(1);
		vendortable.setWidthPercentage(100);
		vendortable.setHorizontalAlignment(Element.ALIGN_LEFT);
		vendortable.addCell(vendorNcell);
		vendortable.addCell(venaddressline1cell);
		if (!vendor.getPrimaryAddress().getAddrLine2().equals("")) {
			vendortable.addCell(venaddressline2cell);
		}
		if(!vendor.getPrimaryAddress().getLandmark().equals("")){
			vendortable.addCell(venlocalitycell);
		}
		if(!vendor.getPrimaryAddress().getLocality().equals("")){
			vendortable.addCell(locacell);
		}
		
		vendortable.addCell(city1cell);
		
		vendortable.addCell(vendorcontactcell);
		vendortable.addCell(vendoremailcell);
		vendortable.addCell(vendornamecell);
		
		if(po.getRefNum()!=null){
			
			vendortable.addCell(infotablecell1);
		}
		
		
		
		PdfPTable headparenttable = new PdfPTable(2);
		headparenttable.setWidthPercentage(100);
		try {
			headparenttable.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	
		PdfPCell companyinfocell = new PdfPCell();
		PdfPCell vendorinfocell = new PdfPCell();
	
		companyinfocell.addElement(companytable);
		vendorinfocell.addElement(vendortable);
		headparenttable.addCell(companyinfocell);
		headparenttable.addCell(vendorinfocell);
	
		String title = "";
		title = "Service PO";
	
		String countinfo = "";
		ServicePo purentity = (ServicePo) po;
		countinfo = "ID : "+purentity.getCount() + "";
	
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		String creationdateinfo = "";
		ServicePo purchentity = (ServicePo) po;
		creationdateinfo = "Date: " + fmt.format(purchentity.getServicePoDate());
	
		Phrase titlephrase = new Phrase(title, font12bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
		titlepdfpara.add(Chunk.NEWLINE);
		PdfPCell titlepdfcell = new PdfPCell();
		titlepdfcell.addElement(titlepdfpara);
		titlepdfcell.setBorderWidthRight(0);
		titlepdfcell.setBorderWidthLeft(0);
		titlepdfcell.addElement(Chunk.NEWLINE);
	
		Phrase idphrase = new Phrase(countinfo, font12);
		Paragraph idofpdfpara = new Paragraph();
		idofpdfpara.add(idphrase);
		idofpdfpara.setAlignment(Element.ALIGN_LEFT);
		idofpdfpara.add(Chunk.NEWLINE);
		PdfPCell countcell = new PdfPCell();
		countcell.addElement(idofpdfpara);
		countcell.setBorderWidthRight(0);
		countcell.addElement(Chunk.NEWLINE);
	
		Phrase dateofpdf = new Phrase(creationdateinfo, font12);
		Paragraph creatndatepara = new Paragraph();
		creatndatepara.add(dateofpdf);
		creatndatepara.setAlignment(Element.ALIGN_RIGHT);
		creatndatepara.add(Chunk.NEWLINE);
		PdfPCell creationcell = new PdfPCell();
		creationcell.addElement(creatndatepara);
		creationcell.setBorderWidthLeft(0);
		creationcell.addElement(Chunk.NEWLINE);
	
		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.addCell(countcell);
		titlepdftable.addCell(titlepdfcell);
		titlepdftable.addCell(creationcell);
	
		try {
			document.add(headparenttable);
			document.add(titlepdftable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}}
	
	
	public void createProductInfo() {
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		Phrase productdetails = new Phrase("Product Details", font1);
		Paragraph para = new Paragraph();
		para.add(productdetails);
		para.setAlignment(Element.ALIGN_CENTER);
	
		PdfPTable table = new PdfPTable(8);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100);
		
		
		Phrase category = new Phrase("SR NO", font1);
		Phrase product = new Phrase("ITEM DETAILS ", font1);
		Phrase qty = new Phrase("QTY", font1);
		Phrase uom = new Phrase("UNIT",font1);
		Phrase rate = new Phrase("RATE", font1);
		Phrase percdisc = new Phrase("% Disc", font1);
		Phrase servicetax = new Phrase("TAX", font1);
	//	Phrase vat = new Phrase("VAT", font1);
	
	
	 Paragraph tax= new Paragraph();
       //*************chnges mukesh on 22/4/2015******************** 
//        SalesQuotation salesQu=null;
//        if(qp instanceof SalesQuotation){
//        	salesQu=(SalesQuotation)qp;
//        }
        Phrase svat=null;
        Phrase vvat=null;
        Phrase vat=null;
        int cout=0;
        int flag21=0;
        int st=0;
        int st1=0;
        
      
        for (int i = 0; i < this.products.size(); i++) {
        	 
			if (products.get(i).getServiceTax().getPercentage() > 0 && products.get(i).getVatTax().getPercentage() > 0) {
				vvat = new Phrase("VAT / ST %", font1);
				cout = 1;
			} else if (products.get(i).getServiceTax().getPercentage() > 0&& products.get(i).getVatTax().getPercentage() == 0) {
				vat = new Phrase("ST %", font1);
				st = 1;
			} else if (products.get(i).getServiceTax().getPercentage() == 0&& products.get(i).getVatTax().getPercentage() > 0) {
				vat = new Phrase("VAT %", font1);
				st1 = 1;
			} else {
				vat = new Phrase("TAX %", font1);
			}
        }
        
        
        Phrase stvat=null;
        if((cout>0)){
        	tax.add(vvat);
        	tax.setAlignment(Element.ALIGN_CENTER);
        }else if(st==1&&st1==1){
         	
         	stvat=new Phrase("VAT / ST %",font1);
         	tax.add(stvat);
         	tax.setAlignment(Element.ALIGN_CENTER);
         }else if(flag21>0){
        	tax.add(svat);
        	tax.setAlignment(Element.ALIGN_CENTER);
        }else{
        tax.add(vat);
        tax.setAlignment(Element.ALIGN_CENTER);
        }
	
	
	
	
	
	
	
	Phrase total = new Phrase("AMOUNT", font1);

	PdfPCell cellcategory = new PdfPCell(category);
	cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell cellproduct = new PdfPCell(product);
	cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell cellqty = new PdfPCell(qty);
	cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell celluom = new PdfPCell(uom);
	celluom.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell cellrate = new PdfPCell(rate);
	cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell cellperdisc = new PdfPCell(percdisc);
	cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
//	PdfPCell cellvat = new PdfPCell(vat);
	PdfPCell cellservicetax = new PdfPCell(tax);
	cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell celltotal = new PdfPCell(total);
	celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);

	table.addCell(cellcategory);
	table.addCell(cellproduct);
	table.addCell(cellqty);
	table.addCell(celluom);
	table.addCell(cellrate);
	table.addCell(cellperdisc);
//	table.addCell(cellvat);
	table.addCell(cellservicetax);
	table.addCell(celltotal);
	
	
	//************************here i have made changes
	//*************calculating spaces here*************
	
		if (this.products.size()*3 <= firstBreakPoint) {
			int size = firstBreakPoint - this.products.size()*3;
			blankLines = size * (160 / 15);
			System.out.println("blankLines size =" + blankLines);
			System.out.println("blankLines size =" + blankLines);
		} else {
			blankLines = 10f;
		}
		table.setSpacingAfter(blankLines);

		Phrase rephrse = null;

		int	flagr=0;
	
		for (int i = 0; i < this.products.size(); i++) {

			chunk = new Phrase((i + 1) + "", font8);
			pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (products.get(i).getProductName() != null) {

				/////////
				
				if (!products.get(i).getWarehouseName().equals("")) {
					chunk = new Phrase(products.get(i).getProductName()
							+ "\n"+ "Delivery Address :"
							+ "\n"+ "Warehouse Name : "+ products.get(i).getWarehouseName()
							+ "\n"+ getWareHouseAddress(products.get(i).getWarehouseName()), font8);
				} 
				else {
					chunk = new Phrase(products.get(i).getProductName(), font8);
				}
				
				////////

			} else {
				chunk = new Phrase("");
			}
			pdfnamecell = new PdfPCell(chunk);

			if (products.get(i).getQty() != 0) {
				chunk = new Phrase(products.get(i).getQty() + "",
						font8);
			} else {
				chunk = new Phrase("");
			}
			pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if (products.get(i).getUnitOfMeasurement() != null) {

				chunk = new Phrase(products.get(i).getUnitOfMeasurement(),
						font8);
			} else {
				chunk = new Phrase("");
			}
			pdfuomcell = new PdfPCell(chunk);
			pdfuomcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedRate = products.get(i).getPrice() - taxVal;
			chunk = new Phrase(df.format(calculatedRate), font8);
			pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);

			if (products.get(i).getServiceTax().getPercentage() != 0)
				chunk = new Phrase(products.get(i).getServiceTax().getPercentage() + "", font8);
			else
				chunk = new Phrase("");

			pdfservice = new PdfPCell(chunk);
			pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
			// if (products.get(i).getVat() != null)
			// chunk = new Phrase(products.get(i).getVat() + "", font8);
			// else
			// chunk = new Phrase("");
			// pdfvattax = new PdfPCell(chunk);

			// ///

			PdfPCell pdfservice1 = null;
			// ******************************
//			double cstval = 0;
//			int cstFlag = 0;
//			if (po instanceof ServicePo) {
//				ServicePo salesq = (ServicePo) po;
//
//				if (salesq.getcForm() != null) {
//					System.out.println(salesq.getcForm().trim() + "...print..");
//
//					if (salesq.getcForm().trim().equals(AppConstants.YES)
//							|| salesq.getcForm().trim().equals(AppConstants.NO)) {
//						cstFlag = 1;
//						cstval = salesq.getCstpercent();
//					}
//				}
//			}

			System.out.println("one");

			if ((products.get(i).getVatTax().getPercentage() > 0)&& (products.get(i).getServiceTax().getPercentage() > 0)) {

				System.out.println("two");
				flagr = 1;
				if ((products.get(i).getVatTax().getPercentage() != 0)&& (products.get(i).getServiceTax().getPercentage() != 0)) {

					if (cout > 0) {
						chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage())+ " / " + df.format(products.get(i).getServiceTax().getPercentage()),font8);
					}
				} else {

					chunk = new Phrase("0" + "", font8);
				}
				pdfservice1 = new PdfPCell(chunk);

				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() > 0)&& (products.get(i).getVatTax().getPercentage() == 0)) {

				System.out.println("three");
				if (products.get(i).getServiceTax().getPercentage() != 0) {
					if (flag21 == 1 || cout == 1) {
						chunk = new Phrase("0.00" + " / "+ df.format(products.get(i).getServiceTax().getPercentage()), font8);
					} else if (st == 1 && st1 == 1) {
						chunk = new Phrase("0.00" + " / "+ df.format(products.get(i).getServiceTax().getPercentage()), font8);
					} else {
						chunk = new Phrase(df.format(products.get(i).getServiceTax().getPercentage()),font8);
					}

				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

			} else if ((products.get(i).getServiceTax().getPercentage() == 0)&& (products.get(i).getVatTax().getPercentage() > 0)) {
				System.out.println("eight");
				if (flag21 == 1 || cout == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage())+ " / " + "0.00", font8);
				} else if (st == 1 && st1 == 1) {
					chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage())+ " / " + "0.00", font8);
				} else {
					chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage()),font8);
				}
				pdfservice1 = new PdfPCell(chunk);
				pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);

				System.out.println("service tax flagr:::::::::::::   " + flagr);

			} else {
				if (flagr == 1) {
					chunk = new Phrase("0.00 / 0.00", font8);
				} else {
					chunk = new Phrase("0.00", font8);
				}
				pdfservice1 = new PdfPCell(chunk);
			}

			// //

			if (products.get(i).getPercentageDiscount() != 0)
				chunk = new Phrase(products.get(i).getPercentageDiscount() + "", font8);
			else
				chunk = new Phrase("0" + "", font8);
			pdfperdiscount = new PdfPCell(chunk);
			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);

			double totalVal = (products.get(i).getPrice() - taxVal)* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal- (totalVal * products.get(i).getPercentageDiscount() / 100);
			} else {
				chunk = new Phrase("");
			}
			chunk = new Phrase(df.format(totalVal), font8);
			pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);

			count = i;

			table.addCell(pdfcategcell);
			table.addCell(pdfnamecell);
			table.addCell(pdfqtycell);
			table.addCell(pdfuomcell);
			table.addCell(pdfspricecell);
			table.addCell(pdfperdiscount);
			// table.addCell(pdfvattax);
			pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(pdfservice1);
			table.addCell(pdftotalproduct);

			if (count == this.products.size()*4 || count == firstBreakPoint) {
				rephrse = new Phrase("Refer Annexure 1 for additional products ", font8);
				flag = firstBreakPoint;
				break;
			}

			System.out.println("flag value" + flag);
			if (firstBreakPoint == flag) {

				termsConditionsInfo();
				footerInfo();
			}

		}
	
			PdfPCell prodtablecell=new PdfPCell();
			prodtablecell.addElement(table);
			prodtablecell.addElement(rephrse);	
			PdfPTable parentTableProd=new PdfPTable(1);
	        parentTableProd.setWidthPercentage(100);
	        parentTableProd.addCell(prodtablecell);
	       
			
				try {
					document.add(parentTableProd);
				} catch (DocumentException e) {
					e.printStackTrace();
				}
}
public void termsConditionsInfo() {
	
	
	Phrase blankval=new Phrase(" ",font8);
	String titleterms = "Payment Terms";
	Phrase termsphrase = new Phrase(titleterms, font10bold);
	termsphrase.add(Chunk.NEWLINE);
	PdfPCell termstitlecell = new PdfPCell();
	termstitlecell.addElement(termsphrase);
	termstitlecell.setBorder(0);

	Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);

	PdfPTable table = new PdfPTable(3);
	table.setWidthPercentage(100);
	
	
	PdfPCell tablecell1 = new PdfPCell();
	 if(this.payTermsLis.size()>5){
		 tablecell1= new PdfPCell(table);
//		 tablecell1.setBorder(0);
	        }else{
	        	tablecell1= new PdfPCell(table);
	        	tablecell1.setBorder(0);
	        }
	
	
	
	Phrase paytermdays = new Phrase("Days", font1);
	Phrase paytermpercent = new Phrase("Percent", font1);
	Phrase paytermcomment = new Phrase("Comment", font1);

	PdfPCell celldays = new PdfPCell(paytermdays);
	celldays.setBorder(0);
	celldays.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell cellpercent = new PdfPCell(paytermpercent);
	cellpercent.setBorder(0);
	cellpercent.setHorizontalAlignment(Element.ALIGN_CENTER);
	PdfPCell cellcomment = new PdfPCell(paytermcomment);
	cellcomment.setBorder(0);
	cellcomment.setHorizontalAlignment(Element.ALIGN_CENTER);
	
	table.addCell(celldays);
	table.addCell(cellpercent);
	table.addCell(cellcomment);

	
	 if( this.payTermsLis.size()<= BreakPoint){
 			int size =  BreakPoint - this.payTermsLis.size();
 			      blankLines = size*(60/5);
 			  	System.out.println("blankLines size ="+blankLines);
 			System.out.println("blankLines size ="+blankLines);
 				}
 				else{
 					blankLines = 0;
 				}
 				table.setSpacingAfter(blankLines);	
	
	
	
	
	
	for (int i = 0; i < this.payTermsLis.size(); i++) {
		Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
				font8);
		PdfPCell pdfdayscell = new PdfPCell(chunk);
		pdfdayscell.setBorder(0);
		pdfdayscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
				font8);
		PdfPCell pdfpercentcell = new PdfPCell(chunk);
		pdfpercentcell.setBorder(0);
		pdfpercentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),
				font8);
		PdfPCell pdfcommentcell = new PdfPCell(chunk);
		pdfcommentcell.setBorder(0);
		pdfcommentcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table.addCell(pdfdayscell);
		table.addCell(pdfpercentcell);
		table.addCell(pdfcommentcell);
		
		
		
		 count=i;
	   	 ///
	   	 if(count==4){
					
					break;
		}
	}
	
	///
	PdfPTable table1 = new PdfPTable(3);
	table1.setWidthPercentage(100);
	
	PdfPCell table1cell = new PdfPCell();
	
	 if(this.payTermsLis.size()>5){
		 table1cell= new PdfPCell(table1);
//		 table1cell.setBorder(0);
	        }else{
	        	table1cell= new PdfPCell(blankval);
	        	table1cell.setBorder(0);
	        }
	
	
	Phrase paytermdays1 = new Phrase("Days", font1);
	Phrase paytermpercent1 = new Phrase("Percent", font1);
	Phrase paytermcomment1 = new Phrase("Comment", font1);

//	PdfPCell celldays1 = new PdfPCell(paytermdays1);
//	celldays1.setBorder(0);
//	PdfPCell cellpercent1 = new PdfPCell(paytermpercent1);
//	cellpercent1.setBorder(0);
//	PdfPCell cellcomment1 = new PdfPCell(paytermcomment1);
//	cellcomment1.setBorder(0);

	 PdfPCell celldays1;
        PdfPCell cellpercent1;
        PdfPCell cellcomment1;
        
        System.out.println(this.payTermsLis.size()+" ...........b4 if");
        if(this.payTermsLis.size()>4){
        	System.out.println(this.payTermsLis.size()+" ...........af if");
         celldays1= new PdfPCell(paytermdays1);
	    celldays1.setBorder(0);
	    celldays1.setHorizontalAlignment(Element.ALIGN_CENTER);
        }else{
        	celldays1= new PdfPCell(blankval);
    	    celldays1.setBorder(0);
        	
        }
        
        if(this.payTermsLis.size()>4){
        
		 cellpercent1 = new PdfPCell(paytermpercent1);
		cellpercent1.setBorder(0);
		cellpercent1.setHorizontalAlignment(Element.ALIGN_CENTER);
        }else{
        	cellpercent1 = new PdfPCell(blankval);
    		cellpercent1.setBorder(0);
        	
        }
        
        if(this.payTermsLis.size()>4){
	    cellcomment1= new PdfPCell(paytermcomment1);
	    cellcomment1.setBorder(0);
	    cellcomment1.setHorizontalAlignment(Element.ALIGN_CENTER);
        }else{
        	cellcomment1= new PdfPCell(blankval);
    	    cellcomment1.setBorder(0);
        }
	
	
	
	
	
	
	table1.addCell(celldays1);
	table1.addCell(cellpercent1);
	table1.addCell(cellcomment1);

	for (int i = 5; i < this.payTermsLis.size(); i++) {
		Phrase chunk = new Phrase(payTermsLis.get(i).getPayTermDays() + "",
				font8);
		PdfPCell pdfdayscell1 = new PdfPCell(chunk);
		pdfdayscell1.setBorder(0);
		pdfdayscell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		chunk = new Phrase(payTermsLis.get(i).getPayTermPercent() + "",
				font8);
		PdfPCell pdfpercentcell1 = new PdfPCell(chunk);
		pdfpercentcell1.setBorder(0);
		pdfpercentcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		chunk = new Phrase(payTermsLis.get(i).getPayTermComment().trim(),
				font8);
		PdfPCell pdfcommentcell1 = new PdfPCell(chunk);
		pdfcommentcell1.setBorder(0);
		pdfcommentcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		table1.addCell(pdfdayscell1);
		table1.addCell(pdfpercentcell1);
		table1.addCell(pdfcommentcell1);
		
		
		 if(i==9){
   				
//			flag1=BreakPoint;
			break;
}
		
	}
	
	///
	
	PdfPTable temspaytable=new PdfPTable(2);
	temspaytable.setWidthPercentage(100);
	temspaytable.addCell(tablecell1);
	temspaytable.addCell(table1cell);
	
	
	PdfPCell paycell = new PdfPCell();
	paycell.setBorder(0);
	paycell.addElement(temspaytable);
	
	PdfPTable termsTable = new PdfPTable(1);
	termsTable.setWidthPercentage(100);
//	termsTable.setSpacingAfter(120f);
	termsTable.addCell(termstitlecell);
	// if(qp instanceof SalesQuotation){
	// }

	// termsTable.addCell(desccell);
	termsTable.addCell(paycell);
	
	//amt in words
//	termsTable.addCell(amtWordsCell);

	PdfPTable chargetaxtable = new PdfPTable(2);
	chargetaxtable.setWidthPercentage(100);
	chargetaxtable.setHorizontalAlignment(Element.ALIGN_RIGHT);

	Phrase totalAmtPhrase = new Phrase("Total Amount   " + "", font1);
	PdfPCell totalAmtCell = new PdfPCell(totalAmtPhrase);
	totalAmtCell.setBorder(0);
	double totalAmt = 0;
	if (po instanceof ServicePo) {
		totalAmt = ((ServicePo) po).getTotalAmount1();
	}

	Phrase realtotalAmt = new Phrase(df.format(totalAmt), font1);
	PdfPCell realtotalAmtCell = new PdfPCell(realtotalAmt);
	realtotalAmtCell.setBorder(0);
	realtotalAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	chargetaxtable.addCell(totalAmtCell);
	chargetaxtable.addCell(realtotalAmtCell);

	
	int first=7;
	int cnt= this.prodTaxes.size()+this.prodCharges.size();
	
	List<String> myList1 = new ArrayList<>();
	List<String> myList2 = new ArrayList<>();
	List<String> myList3 = new ArrayList<>();
	List<String> myList4 = new ArrayList<>();
	
	if(cnt>first){
		
		for (int i = 0; i < this.prodTaxes.size(); i++) {
			
			if(prodTaxes.get(i).getChargePercent()!=0){
				/////
				if(prodTaxes.get(i).getChargeName().equals("VAT")||prodTaxes.get(i).getChargeName().equals("CST")){
					
					System.out.println("1st loop"+prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent());
					
					String str = prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent();
					double taxAmt1 = prodTaxes.get(i).getChargePercent()
							* prodTaxes.get(i).getAssessableAmount() / 100;
					
					 myList1.add(str);
					 myList2.add(df.format(taxAmt1));
					 
					 System.out.println("Size of mylist1 is () === "+myList1.size());
				}
				
				if(prodTaxes.get(i).getChargeName().equals("Service Tax")){
					
					System.out.println("2nd loop == "+prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent());
					
					String str = prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent();
					double taxAmt1 = prodTaxes.get(i).getChargePercent()
							* prodTaxes.get(i).getAssessableAmount() / 100;
					 myList3.add(str);
					 myList4.add(df.format(taxAmt1));
					 
					 System.out.println("Size of mylist2 is () === "+myList2.size());
				}
			
				////
			}
		}
		
		PdfPCell pdfservicecell=null;
		
		PdfPTable other1table=new PdfPTable(1);
		other1table.setWidthPercentage(100);
		for(int j=0;j<myList1.size();j++){
			chunk = new Phrase(myList1.get(j),font8);
			 pdfservicecell = new PdfPCell(chunk);
			pdfservicecell.setBorder(0);
			pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(pdfservicecell);
			
		}
		
		PdfPCell pdfservicecell1=null;
		
		PdfPTable other2table=new PdfPTable(1);
		other2table.setWidthPercentage(100);
		for(int j=0;j<myList2.size();j++){
			chunk = new Phrase(myList2.get(j),font8);
			 pdfservicecell1 = new PdfPCell(chunk);
			pdfservicecell1.setBorder(0);
			pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(pdfservicecell1);
			
		}
		
		PdfPCell chargescell=null;
		for(int j=0;j<myList3.size();j++){
			chunk = new Phrase(myList3.get(j),font8);
			chargescell = new PdfPCell(chunk);
			chargescell.setBorder(0);
			chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(chargescell);
			
		}
		
		PdfPCell chargescell1=null;
		for(int j=0;j<myList4.size();j++){
			chunk = new Phrase(myList4.get(j),font8);
			chargescell1 = new PdfPCell(chunk);
			chargescell1.setBorder(0);
			chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(chargescell1);
			
		}
		
		
		
		
		PdfPCell othercell = new PdfPCell();
		othercell.addElement(other1table);
		othercell.setBorder(0);
		chargetaxtable.addCell(othercell);
		
		PdfPCell othercell1 = new PdfPCell();
		othercell1.addElement(other2table);
		othercell1.setBorder(0);
		chargetaxtable.addCell(othercell1);
		
		
		
		
		PdfPCell chargecell = null;
	      PdfPCell chargeamtcell=null;
	      PdfPCell otherchargecell=null;
		
		for (int i = 0; i <this.prodCharges.size(); i++) {			
			Phrase chunk = null;
			Phrase chunk1 = new Phrase("",font8);
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			
			System.out.println(prodCharges.get(i).getChargeName()+" out if1st");
			
			if (prodCharges.get(i).getChargePercent() != 0) {
				System.out.println(prodCharges.get(i).getChargeName()+" in if1st");
				chunk = new Phrase(prodCharges.get(i).getChargeName() + " @ "+ prodCharges.get(i).getChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				chargeAmt = prodCharges.get(i).getChargePercent()* prodCharges.get(i).getAssessableAmount() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
			}else{
				pdfchargecell = new PdfPCell(chunk1);
				pdfchargeamtcell = new PdfPCell(chunk1);
			}
			
			
//			double chargeAmt1 = 0;
			PdfPCell pdfchargeamtcell1 = null;
			PdfPCell pdfchargecell1 = null;
//			
//			System.out.println(prodCharges.get(i).getChargeAbsValue() +" out if2st");
			if (prodCharges.get(i).getChargeAbsValue() != 0) {
				
				System.out.println(prodCharges.get(i).getChargeAbsValue() +" in if2st");
				chunk = new Phrase(prodCharges.get(i).getChargeName() + "",font1);
				pdfchargecell1 = new PdfPCell(chunk);
				chargeAmt = prodCharges.get(i).getChargeAbsValue();
				chunk = new Phrase(chargeAmt + "", font8);
				pdfchargeamtcell1 = new PdfPCell(chunk);
				pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}else{
				pdfchargecell1 = new PdfPCell(chunk1);
				pdfchargeamtcell1 = new PdfPCell(chunk1);
			}
			pdfchargecell.setBorder(0);
			pdfchargeamtcell.setBorder(0);
//			chargetaxtable.addCell(pdfchargecell);
//			chargetaxtable.addCell(pdfchargeamtcell);
//			
//			if (prodCharges.get(i).getChargeAbsValue() != 0) {
//			pdfchargecell1.setBorder(0);
//			pdfchargeamtcell1.setBorder(0);
//			chargetaxtable.addCell(pdfchargecell1);
//			chargetaxtable.addCell(pdfchargeamtcell1);
//			}
			
			 total = total+chargeAmt; 	
		     
		     if(total!=0){
		   		   chunk = new Phrase("Other charges total",font1);
		   		   chargecell=new PdfPCell(chunk);
		   		   chargecell.setBorder(0);
		   		chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		   		  
		   		chunk=new Phrase(df.format(total),font8);
		  	      	   chargeamtcell = new PdfPCell(chunk);
		  	      	chargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		  	      	   chargeamtcell.setBorder(0);
		   	  
		     }
			
		}
		
		Phrase other = new Phrase("Refer other charge details",font1);
	     otherchargecell=new PdfPCell(other);
	     otherchargecell.setBorder(0);
	     otherchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
	     Phrase other1 = new Phrase(" ",font1);
	    PdfPCell otherchargecell1=new PdfPCell(other1);
	     otherchargecell1.setBorder(0);
	     otherchargecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	     
		
	     chargetaxtable.addCell(chargecell);
	     chargetaxtable.addCell(chargeamtcell);
	     chargetaxtable.addCell(otherchargecell);
	     chargetaxtable.addCell(otherchargecell1);
		
		
	}else {
		for (int i = 0; i < this.prodTaxes.size(); i++) {
			
			if(prodTaxes.get(i).getChargePercent()!=0){
		
			if(prodTaxes.get(i).getChargeName().equals("VAT")||prodTaxes.get(i).getChargeName().equals("CST")){
				
				System.out.println("1st loop"+prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent());
				
				String str = prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent();
				double taxAmt1 = prodTaxes.get(i).getChargePercent()
						* prodTaxes.get(i).getAssessableAmount() / 100;
				
				 myList1.add(str);
				 myList2.add(df.format(taxAmt1));
				 
				 System.out.println("Size of mylist1 is () === "+myList1.size());
			}
			
			if(prodTaxes.get(i).getChargeName().equals("Service Tax")){
				
				System.out.println("2nd loop == "+prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent());
				
				String str = prodTaxes.get(i).getChargeName().trim()+ " @ "+ prodTaxes.get(i).getChargePercent();
				double taxAmt1 = prodTaxes.get(i).getChargePercent()
						* prodTaxes.get(i).getAssessableAmount() / 100;
				 myList3.add(str);
				 myList4.add(df.format(taxAmt1));
				 
				 System.out.println("Size of mylist2 is () === "+myList2.size());
			}
			
			}
			
			
		}
		
		
		
		PdfPCell pdfservicecell=null;
		
		PdfPTable other1table=new PdfPTable(1);
		other1table.setWidthPercentage(100);
		for(int j=0;j<myList1.size();j++){
			chunk = new Phrase(myList1.get(j),font8);
			 pdfservicecell = new PdfPCell(chunk);
			pdfservicecell.setBorder(0);
			pdfservicecell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(pdfservicecell);
			
		}
		
		PdfPCell pdfservicecell1=null;
		
		PdfPTable other2table=new PdfPTable(1);
		other2table.setWidthPercentage(100);
		for(int j=0;j<myList2.size();j++){
			chunk = new Phrase(myList2.get(j),font8);
			 pdfservicecell1 = new PdfPCell(chunk);
			pdfservicecell1.setBorder(0);
			pdfservicecell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(pdfservicecell1);
			
		}
		
		PdfPCell chargescell=null;
		for(int j=0;j<myList3.size();j++){
			chunk = new Phrase(myList3.get(j),font8);
			chargescell = new PdfPCell(chunk);
			chargescell.setBorder(0);
			chargescell.setHorizontalAlignment(Element.ALIGN_LEFT);
			other1table.addCell(chargescell);
			
		}
		
		PdfPCell chargescell1=null;
		for(int j=0;j<myList4.size();j++){
			chunk = new Phrase(myList4.get(j),font8);
			chargescell1 = new PdfPCell(chunk);
			chargescell1.setBorder(0);
			chargescell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			other2table.addCell(chargescell1);
		}
		
		PdfPCell othercell = new PdfPCell();
		othercell.addElement(other1table);
		othercell.setBorder(0);
		chargetaxtable.addCell(othercell);
		
		PdfPCell othercell1 = new PdfPCell();
		othercell1.addElement(other2table);
		othercell1.setBorder(0);
		chargetaxtable.addCell(othercell1);
		
		for (int i = 0; i <this.prodCharges.size(); i++) {			
			Phrase chunk = null;
			Phrase chunk1 = new Phrase("",font8);
			double chargeAmt = 0;
			PdfPCell pdfchargeamtcell = null;
			PdfPCell pdfchargecell = null;
			
			System.out.println(prodCharges.get(i).getChargeName()+" out if1st");
			
			if (prodCharges.get(i).getChargePercent() != 0) {
				System.out.println(prodCharges.get(i).getChargeName()+" in if1st");
				chunk = new Phrase(prodCharges.get(i).getChargeName() + " @ "+ prodCharges.get(i).getChargePercent(), font1);
				pdfchargecell = new PdfPCell(chunk);
				chargeAmt = prodCharges.get(i).getChargePercent()* prodCharges.get(i).getAssessableAmount() / 100;
				chunk = new Phrase(df.format(chargeAmt), font8);
				pdfchargeamtcell = new PdfPCell(chunk);
				pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				pdfchargeamtcell.setBorder(0);
			}else{
				pdfchargecell = new PdfPCell(chunk1);
				pdfchargeamtcell = new PdfPCell(chunk1);
			}
			
			
			double chargeAmt1 = 0;
			PdfPCell pdfchargeamtcell1 = null;
			PdfPCell pdfchargecell1 = null;
			
			System.out.println(prodCharges.get(i).getChargeAbsValue() +" out if2st");
			if (prodCharges.get(i).getChargeAbsValue() != 0) {
				
				System.out.println(prodCharges.get(i).getChargeAbsValue() +" in if2st");
				chunk = new Phrase(prodCharges.get(i).getChargeName() + "",font1);
				pdfchargecell1 = new PdfPCell(chunk);
				chargeAmt1 = prodCharges.get(i).getChargeAbsValue();
				chunk = new Phrase(chargeAmt1 + "", font8);
				pdfchargeamtcell1 = new PdfPCell(chunk);
				pdfchargeamtcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			}else{
				pdfchargecell1 = new PdfPCell(chunk1);
				pdfchargeamtcell1 = new PdfPCell(chunk1);
			}
			pdfchargecell.setBorder(0);
			pdfchargeamtcell.setBorder(0);
			chargetaxtable.addCell(pdfchargecell);
			chargetaxtable.addCell(pdfchargeamtcell);
			
			if (prodCharges.get(i).getChargeAbsValue() != 0) {
			pdfchargecell1.setBorder(0);
			pdfchargeamtcell1.setBorder(0);
			chargetaxtable.addCell(pdfchargecell1);
			chargetaxtable.addCell(pdfchargeamtcell1);
			}
		}
	}
	

	/****************************************************************************************/


	PdfPTable billamtable = new PdfPTable(2);
      billamtable.setWidthPercentage(100);
//      billamtable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
	
	
		
	/************************************************************************************************/
	for (int i = 0; i <this.prodCharges.size(); i++){
		
		System.out.println("======I  " + i + "   size" + prodCharges.size());
		if (i == prodCharges.size() - 1) {
			chunk = new Phrase("Net Payable", font1);
			PdfPCell pdfnetpaycell = new PdfPCell(chunk);
			pdfnetpaycell.setBorder(0);
//			pdfnetpaycell.setBorderWidthLeft(0);
			Double netPayAmt = (double) 0;
			int netpayble = 0;
			if (po instanceof ServicePo) {
				netPayAmt = ((ServicePo) po).getNetPayable();
				 netpayble=(int) netPayAmt.doubleValue();
			}

			chunk = new Phrase(netpayble + "", font8);
			PdfPCell pdfnetpayamt = new PdfPCell(chunk);
			pdfnetpayamt.setBorder(0);
			pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			pdfnetpayamt.setBorderWidthRight(0);

			billamtable.addCell(pdfnetpaycell);
			billamtable.addCell(pdfnetpayamt);
		}
		
	}
	
	
	
	if (this.prodCharges.size() == 0) {
		Double netPayAmt = (double) 0;
		int netpayble = 0;
		Phrase chunknetpaytitle = new Phrase("Net Payable", font1);
		PdfPCell netpaytitlecell = new PdfPCell(chunknetpaytitle);
		netpaytitlecell.setBorder(0);
//		netpaytitlecell.setBorderWidthLeft(0);
		// if(po instanceof PurchaseOrder){
		netPayAmt = ((ServicePo) po).getNetPayable();
		 netpayble=(int) netPayAmt.doubleValue();
		// }

		Phrase chunknetpay = new Phrase(netpayble + "", font8);
		PdfPCell pdfnetpayamt = new PdfPCell(chunknetpay);
		pdfnetpayamt.setBorder(0);
		pdfnetpayamt.setHorizontalAlignment(Element.ALIGN_RIGHT);
//		pdfnetpayamt.setBorderWidthRight(0);

		billamtable.addCell(netpaytitlecell);
		billamtable.addCell(pdfnetpayamt);
	}
	
	/*********************************************************************************************/

	PdfPTable taxinfotable = new PdfPTable(1);
	taxinfotable.setWidthPercentage(100);
	taxinfotable.addCell(chargetaxtable);

	PdfPTable parenttaxtable = new PdfPTable(2);
	parenttaxtable.setWidthPercentage(100);
	try {
		parenttaxtable.setWidths(new float[] { 70, 30 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	PdfPCell termsdatacell = new PdfPCell();
	PdfPCell taxdatacell = new PdfPCell();

	termsdatacell.addElement(termsTable);
	taxdatacell.addElement(chargetaxtable);
	parenttaxtable.addCell(termsdatacell);
	parenttaxtable.addCell(taxdatacell);

	try {
		document.add(parenttaxtable);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	

	 //*****************for amt in words****************************************
	   
      String amtInWords="Amount In Words : ";
//      String  amtInFigure= InvoicePdf.convert(((PurchaseOrder) po).getNetpayble());
      String  amtInFigure= "Rupees: "+SalesInvoicePdf.convert(((ServicePo) po).getNetPayable())+" Only";
	   System.out.println("amt"+amtInFigure);
	   Phrase amtwords=new Phrase(amtInWords,font8bold);
	   Phrase amtFigure=new Phrase(amtInFigure,font1);
	   PdfPCell amtWordsCell=new PdfPCell();
//	   amtWordsCell.addElement(amtwords);
	   amtWordsCell.addElement(amtFigure);
	   amtWordsCell.setBorder(0);
      
      //*********************************************************

	   PdfPTable termsTable1=new PdfPTable(1);
	   termsTable1.setWidthPercentage(100);
	 //  termsTable.setSpacingAfter(120f);
	   termsTable1.addCell(amtWordsCell);
	   

	   PdfPTable parenttaxtable1=new PdfPTable(2);
	     parenttaxtable1.setWidthPercentage(100);
	     try {
			parenttaxtable1.setWidths(new float[]{70,30});
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
	     
		  PdfPCell amtWordsTblcell = new PdfPCell();
		  PdfPCell taxdatacell1 = new PdfPCell();
			
		  amtWordsTblcell.addElement(termsTable1);
		  taxdatacell1.addElement(billamtable);
		  parenttaxtable1.addCell(amtWordsTblcell);
		  parenttaxtable1.addCell(taxdatacell1);
		
			try {
				document.add(parenttaxtable1);
			} catch (DocumentException e) {
				e.printStackTrace();
			}




}

	public void footerInfo()
	{
		
	//	Phrase banktitle=new Phrase("Disclaimer",font12boldul);
	//	Paragraph banktitlepara=new Paragraph();
	//	banktitlepara.add(banktitle);
	////	banktitlepara.setAlignment(Element.ALIGN_LEFT);
	//	
	//	PdfPCell banktitlecell=new PdfPCell();
	//	banktitlecell.addElement(banktitlepara);
	//	banktitlecell.setBorder(0);
	//	banktitlecell.setColspan(6);
	////	banktitlecell.setBorderWidthRight(0);
	//	
	//	
	//	
	//	Phrase disclaimer=new Phrase(disclaimerText,font8);
	//	Paragraph banktitlepara1=new Paragraph();
	//	banktitlepara1.add(disclaimer);
	//	
	//	PdfPCell banktitlecell1=new PdfPCell();
	//	banktitlecell1.addElement(banktitlepara1);
	//	banktitlecell1.setBorder(0);
	//	banktitlecell1.setColspan(6);
		
	//	PdfPTable bankinfotable=new PdfPTable(1);
	//	bankinfotable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase typename=null;
		Phrase typevalue=null;
		
		PdfPTable artictable=new PdfPTable(2);
		artictable.setWidthPercentage(100);
		logger.log(Level.SEVERE,"article for  out =========== ");
		for(int i=0;i<this.articletype.size();i++){
			if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("Purchase Order")){
				typename = new Phrase(articletype.get(i).getArticleTypeName(),font8);
				typevalue = new Phrase(articletype.get(i).getArticleTypeValue(),font8);
				 
			 	PdfPCell tymanecell=new PdfPCell();
				tymanecell.addElement(typename);
				tymanecell.setBorder(0);
				
				PdfPCell typevalcell=new PdfPCell();
				typevalcell.addElement(typevalue);
				typevalcell.setBorder(0);
				
				artictable.addCell(tymanecell);
				artictable.addCell(typevalcell);
			 }
		}
		
			
			
		PdfPTable headertable=new PdfPTable(1);
		headertable.setWidthPercentage(100);
		
		if(vendInsFlag){
			String vendorInstruction="This Purchase Order is subject to the Terms and Conditions incorporated herein by this reference."
					+ " For a copy of the Terms and Conditions, please refer to the attached Term & Conditions. "
					+ "It is a System generated Purchase Order hence signature is not required.";
			
			Phrase header1=null;
			header1 = new Phrase(vendorInstruction,font8);
			PdfPCell headercell1 = new PdfPCell();
			headercell1.addElement(header1);
			headercell1.setBorder(0);
			
			headertable.addCell(headercell1);
		}
		
		
		
		
		////
		
		
		
		////
		Phrase blankphrase1 = new Phrase(" ");
		Phrase blankphrase2 = new Phrase(" ");
		 
		 String companyname=comp.getBusinessUnitName().trim().toUpperCase();
		 Paragraph companynamepara=new Paragraph();
		 companynamepara.add("FOR "+companyname);
		 companynamepara.setFont(font9bold);
		 companynamepara.setAlignment(Element.ALIGN_CENTER);
		 
	
		 //******************end of document***********************
		 
		   String endDucument = "*************************X--X--X*************************";
		   Paragraph endpara=new Paragraph();
		   endpara.add(endDucument);
		   endpara.setAlignment(Element.ALIGN_CENTER);
		 
		 //***************************************************
		 
		 
		 String authsign="AUTHORISED SIGNATORY";
	     Paragraph authpara=new Paragraph();
	     authpara.add(authsign);
	     authpara.setFont(font9bold);
	     authpara.setAlignment(Element.ALIGN_CENTER);
	     
	     PdfPCell companynamecell=new PdfPCell();
	     companynamecell.addElement(companynamepara);
	     companynamecell.setBorder(0);
	     
	     PdfPCell authsigncell=new PdfPCell();
	     authsigncell.addElement(authpara);
	     authsigncell.setBorder(0);
	
	     Phrase ppp=new Phrase(" ");
	     PdfPCell blacell=new PdfPCell();
	     blacell.addElement(ppp);
	     blacell.setBorder(0);
	     PdfPTable table = new PdfPTable(1);
		 table.addCell(companynamecell);
		 table.addCell(blacell);
		 table.addCell(blacell);
		 table.addCell(blacell);
		 table.addCell(authsigncell);
		 
		 table.setWidthPercentage(100);
		 
		 
		  PdfPTable parentbanktable=new PdfPTable(3);
		  parentbanktable.setSpacingAfter(40f);
		  parentbanktable.setWidthPercentage(100);
	      try {
	    	  parentbanktable.setWidths(new float[]{25,45,30});
		  } 
	      catch (DocumentException e1) {
			e1.printStackTrace();
		}
	      
	      
	      PdfPCell bankdatacell1 = new PdfPCell();
		  PdfPCell bankdatacell = new PdfPCell();
		  PdfPCell authorisedcell = new PdfPCell();
		  
		  bankdatacell.addElement(artictable);
		  bankdatacell1.addElement(headertable);
		  authorisedcell.addElement(table);
		  
		  parentbanktable.addCell(bankdatacell);
		  parentbanktable.addCell(bankdatacell1);
		  parentbanktable.addCell(authorisedcell);
		  
		  
		  
		  Phrase refphrase=new Phrase("Annexure 1 ",font10bold);
		  Paragraph repara=new Paragraph(refphrase);
		  
		  
		  try {
				document.add(parentbanktable);
				
				if(this.products.size() > 15){
	//			if(rohan>20){	
				document.newPage();
				document.add(repara);
				settingRemainingRowsToPDF(16);
				}if(this.products.size() > 75){
	//			}if(rohan>80){
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(76);
				}if(this.products.size() > 135){
	//			}if(rohan>140){	
					document.newPage();
					document.add(repara);
				settingRemainingRowsToPDF(136);
				}if(this.products.size() > 195){
	//			}if(rohan>200){	
					document.newPage();
					document.add(repara);
					
				settingRemainingRowsToPDF(196);
				}
	//			if(this.products.size()>20){
	////				document.add(endpara);
	//				}
			} 
			catch (DocumentException e) {
				e.printStackTrace();
			}
		  
		  
		  int proddes=0;
			 for(int i=0;i<this.stringlis.size();i++){
				 
				 if(!stringlis.get(i).getComment().equals("")){
					 proddes=1;
					System.out.println("pro coment+++   "+stringlis.get(i).getComment());
				 }
				 
			 }
			 
			 int first=7;
				int cnt= this.prodTaxes.size()+this.prodCharges.size();
		  
		  if(proddes==1||cnt>first){
			  try {
					productdetails();
				} catch (DocumentException e) {
					e.printStackTrace();
				}
		  }
		  
		  
		
	}
	
	private void productdetails() throws DocumentException {
	
		 Phrase nextpage=new Phrase(Chunk.NEXTPAGE);
			document.add(nextpage);
		
			String title = "";
			title = "Purchase Order";
	
			String countinfo = "";
			ServicePo purentity = (ServicePo) po;
			countinfo = "ID : "+purentity.getCount() + "";
	
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
			String creationdateinfo = "";
			ServicePo purchentity = (ServicePo) po;
			creationdateinfo = "Date: " + fmt.format(purchentity.getServicePoDate());
	
			Phrase titlephrase = new Phrase(title, font12bold);
			Paragraph titlepdfpara = new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			titlepdfpara.add(Chunk.NEWLINE);
			PdfPCell titlepdfcell = new PdfPCell();
			titlepdfcell.addElement(titlepdfpara);
			titlepdfcell.setBorderWidthRight(0);
			titlepdfcell.setBorderWidthLeft(0);
			titlepdfcell.addElement(Chunk.NEWLINE);
	
			Phrase idphrase = new Phrase(countinfo, font12);
			Paragraph idofpdfpara = new Paragraph();
			idofpdfpara.add(idphrase);
			idofpdfpara.setAlignment(Element.ALIGN_LEFT);
			idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell countcell = new PdfPCell();
			countcell.addElement(idofpdfpara);
			countcell.setBorderWidthRight(0);
			countcell.addElement(Chunk.NEWLINE);
	
			Phrase dateofpdf = new Phrase(creationdateinfo, font12);
			Paragraph creatndatepara = new Paragraph();
			creatndatepara.add(dateofpdf);
			creatndatepara.setAlignment(Element.ALIGN_RIGHT);
			creatndatepara.add(Chunk.NEWLINE);
			PdfPCell creationcell = new PdfPCell();
			creationcell.addElement(creatndatepara);
			creationcell.setBorderWidthLeft(0);
			creationcell.addElement(Chunk.NEWLINE);
	
			PdfPTable titlepdftable = new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.addCell(countcell);
			titlepdftable.addCell(titlepdfcell);
			titlepdftable.addCell(creationcell);
		
			document.add(titlepdftable);
			document.add(Chunk.NEWLINE);
			
			String prodetails="";
			
			for(int i=0;i<this.stringlis.size();i++){
				if(!stringlis.get(i).getComment().equals("")){
					prodetails="Products Details ";
				}
			}
			
			
			
			Phrase prophrase=new Phrase(prodetails,font12bold);
			Paragraph propara=new Paragraph();
			propara.add(prophrase);
			document.add(propara);
			document.add(Chunk.NEWLINE);
			
			Phrase term=new Phrase("",font10);
			
			String descri="";
			Phrase descri1=new Phrase("",font10);
			Paragraph para1=new Paragraph("");
			System.out.println("pro size() ==== "+this.products.size());
			
			for(int i=0;i<this.stringlis.size();i++){
				
				System.out.println("commentm === "+stringlis.get(i).getComment());
				System.out.println("pro name ==== "+stringlis.get(i).getProductName());
				
				
				if(!stringlis.get(i).getComment().equals("")){
					
					
	//				document.add(Chunk.NEWLINE);
					
					
					term=new Phrase("Product Id : "+stringlis.get(i).getCount()+""+"        Product Name : "+stringlis.get(i).getProductName(),font10);
					
				
				descri1 = new Phrase(descri+stringlis.get(i).getComment(),font10);
				}else{
					
					term = new Phrase("",font10);
					descri1 = new Phrase("",font10);
				}
				
				
			/*************************************************************************************************/
				
				para1 = new Paragraph();
				para1.add(term);
				para1.setAlignment(Element.ALIGN_LEFT);
				Paragraph para2 = new Paragraph();
				para2.add(descri1);
			
				document.add(para1);
				
				if(!stringlis.get(i).getComment().equals("")){
					
					
					DocumentUpload document12 =stringlis.get(i).getProductImage();
					if(document12!=null&&!document12.getUrl().equals("")){
					//patch
					String hostUrl; 
					String environment = System.getProperty("com.google.appengine.runtime.environment");
					if (environment.equals("Production")) {
					    String applicationId = System.getProperty("com.google.appengine.application.id");
					    String version = System.getProperty("com.google.appengine.application.version");
					    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
					} else {
					    hostUrl = "http://localhost:8888";
					}
					
					try {
						Image image2 = Image.getInstance(new URL(hostUrl+document12.getUrl()));
						image2.scalePercent(20f);
	//					image2.setAbsolutePosition(40f,745f);
						
						document.add(image2);
					} catch (Exception e) {
						e.printStackTrace();
						}
					}
					}
			
		/****************************************************************************************************/
				document.add(para2);
		
		}
		
		int first=7;
		int cnt= this.prodTaxes.size()+this.prodCharges.size();
		System.out.println("cnt value==="+cnt);
		if(cnt > first){
      
      otherchargestoAnnexur();
		}
		
		
}

////
//
// List<ProductDetailsPO> productcount =new ArrayList<ProductDetailsPO>();
//  
//  	    public List<ProductDetailsPO> getdata(){
//  	    	
//  	    	ProductDetailsPO po=new ProductDetailsPO();
//  	    	po.setProductCategory("category");
////  	    	po.setPrduct("Product");
//  	    	po.setProductQuantity(20);
//  	    	po.setTax(65);
//  	    	po.setVat(55);
//  	    	po.setDiscount(22);
////  	    	po.setTotal(50);
//  	    	productcount.add(po);
//  	    	return productcount;
//  	    }

////

public double removeAllTaxes(SuperProduct entity) {
	double vat = 0, service = 0;
	double tax = 0, retrVat = 0, retrServ = 0;

	if (entity instanceof ItemProduct) {
		ItemProduct prod = (ItemProduct) entity;
		if (prod.getVatTax() != null
				&& prod.getVatTax().isInclusive() == true) {
			vat = prod.getVatTax().getPercentage();
		}
		if (prod.getServiceTax() != null
				&& prod.getServiceTax().isInclusive() == true) {
			service = prod.getServiceTax().getPercentage();
		}
	}

	if (vat != 0 && service == 0) {
		retrVat = entity.getPrice() / (1 + (vat / 100));
		retrVat = entity.getPrice() - retrVat;
	}
	if (service != 0 && vat == 0) {
		retrServ = entity.getPrice() / (1 + service / 100);
		retrServ = entity.getPrice() - retrServ;
	}
	if (service != 0 && vat != 0) {
		// Here if both are inclusive then first remove service tax and then
		// on that amount
		// calculate vat.
		double removeServiceTax = (entity.getPrice() / (1 + service / 100));

		// double taxPerc=service+vat;
		// retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed
		// below
		retrServ = (removeServiceTax / (1 + vat / 100));
		retrServ = entity.getPrice() - retrServ;
	}
	tax = retrVat + retrServ;
	return tax;
}


		
		public void settingRemainingRowsToPDF(int flag){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA, 8 ,Font.BOLD);
		PdfPTable table = new PdfPTable(8);
		table.setWidthPercentage(100);
		try {
			table.setWidths(columnWidths);
		} catch (DocumentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Phrase category = new Phrase("SR.NO.", font1);
		Phrase product = new Phrase("ITEM DETAILS ", font1);
		Phrase qty = new Phrase("QTY", font1);
		Phrase uom = new Phrase("UNIT",font1);
		Phrase rate = new Phrase("RATE", font1);
		Phrase percdisc = new Phrase("% Disc", font1);
		Phrase servicetax = new Phrase("TAX", font1);
		//Phrase vat = new Phrase("VAT", font1);
		
		/////
		
		Paragraph tax= new Paragraph();
		//*************chnges mukesh on 22/4/2015******************** 
		// SalesQuotation salesQu=null;
		// if(qp instanceof SalesQuotation){
		// 	salesQu=(SalesQuotation)qp;
		// }
		 Phrase svat=null;
		 Phrase vvat=null;
		 Phrase vat=null;
		 int cout=0;
		 int flag1=0;
		 int st=0;
		 int st1=0;
		 
		
		 for (int i = 0; i < this.products.size(); i++) {
		      
		     if(products.get(i).getServiceTax().getPercentage()>0&&products.get(i).getVatTax().getPercentage()>0){
		     	 vvat = new Phrase("VAT / ST %",font1);
		     	 cout=1;
		     }
		     else if(products.get(i).getServiceTax().getPercentage()>0&&products.get(i).getVatTax().getPercentage()==0){
		  	    vat = new Phrase("ST %",font1);
		  	   st=1;
		     }
		     else if(products.get(i).getServiceTax().getPercentage()==0&&products.get(i).getVatTax().getPercentage()>0){
		     	 vat = new Phrase("VAT %",font1);
		     	 st1=1;
		     }
		     else{
		         	 vat = new Phrase("TAX %",font1);
		      }
		      
		      
		 }
		 Phrase stvat=null;
		 if((cout>0)){
		 	tax.add(vvat);
		 	tax.setAlignment(Element.ALIGN_CENTER);
		 }else if(st==1&&st1==1){
		  	
		  	stvat=new Phrase("VAT / ST %",font1);
		  	tax.add(stvat);
		  	tax.setAlignment(Element.ALIGN_CENTER);
		  }else if(flag1>0){
		 	tax.add(svat);
		 	tax.setAlignment(Element.ALIGN_CENTER);
		 }else{
		 tax.add(vat);
		 tax.setAlignment(Element.ALIGN_CENTER);
		 }
		
		
		
		
		
		
		/////
		Phrase total = new Phrase("AMOUNT", font1);
		
		PdfPCell cellcategory = new PdfPCell(category);
		cellcategory.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellproduct = new PdfPCell(product);
		cellproduct.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellqty = new PdfPCell(qty);
		cellqty.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celluom = new PdfPCell(uom);
		celluom.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellrate = new PdfPCell(rate);
		cellrate.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell cellperdisc = new PdfPCell(percdisc);
		cellperdisc.setHorizontalAlignment(Element.ALIGN_CENTER);
		//PdfPCell cellvat = new PdfPCell(vat);
		PdfPCell cellservicetax = new PdfPCell(tax);
		cellservicetax.setHorizontalAlignment(Element.ALIGN_CENTER);
		PdfPCell celltotal = new PdfPCell(total);
		celltotal.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		////
		
		//Font font1 = new Font(Font.FontFamily.HELVETICA, 8 | Font.BOLD);
		//	Phrase category = new Phrase("Category ", font1);
		////	Phrase product = new Phrase("Product ", font1);
		//	Phrase qty = new Phrase("Quantity", font1);
		////	Phrase rate = new Phrase("Rate", font1);
		//	Phrase servicetax = new Phrase("Service Tax", font1);
		//	Phrase vat = new Phrase("VAT", font1);
		//	Phrase percdisc = new Phrase("% Discount", font1);
		////	Phrase total = new Phrase("Total", font1);
		//
		//	PdfPCell cellcategory = new PdfPCell(category);
		////	PdfPCell cellproduct = new PdfPCell(product);
		//	PdfPCell cellqty = new PdfPCell(qty);
		////	PdfPCell cellrate = new PdfPCell(rate);
		//	PdfPCell cellperdisc = new PdfPCell(percdisc);
		//	PdfPCell cellvat = new PdfPCell(vat);
		//	PdfPCell cellservicetax = new PdfPCell(servicetax);
		////	PdfPCell celltotal = new PdfPCell(total);
		//
		//	table.addCell(cellcategory);
		////	table.addCell(cellproduct);
		//	table.addCell(cellqty);
		////	table.addCell(cellrate);
		//	table.addCell(cellperdisc);
		//	table.addCell(cellvat);
		//	table.addCell(cellservicetax);
		
		
		
		////
		
		table.addCell(cellcategory);
		table.addCell(cellproduct);
		table.addCell(cellqty);
		table.addCell(celluom);
		table.addCell(cellrate);
		table.addCell(cellperdisc);
		//table.addCell(cellvat);
		table.addCell(cellservicetax);
		table.addCell(celltotal);
		
		for (int i = flag; i < this.products.size(); i++) {
			
			chunk = new Phrase((i + 1) + "", font8);
			pdfcategcell = new PdfPCell(chunk);
			pdfcategcell.setHorizontalAlignment(Element.ALIGN_CENTER);

			if(products.get(i).getProductName()!=null){
			chunk = new Phrase(products.get(i).getProductName(), font8);
			}else{
				 chunk = new Phrase("");
			}
			pdfnamecell = new PdfPCell(chunk);
			
			
			if(products.get(i).getQty()!=0){
			chunk = new Phrase(products.get(i).getQty() + "", font8);
			}else{
			chunk = new Phrase("");
			}
			pdfqtycell = new PdfPCell(chunk);
			pdfqtycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			if(products.get(i).getUnitOfMeasurement()!=null){
				 
				 chunk =new Phrase (products.get(i).getUnitOfMeasurement(),font8);
				 }
			else{
				 chunk= new Phrase("");
			}
			pdfuomcell = new PdfPCell(chunk);
			pdfuomcell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			double taxVal = removeAllTaxes(products.get(i).getPrduct());
			double calculatedRate = products.get(i).getPrice() - taxVal;
			chunk = new Phrase(df.format(calculatedRate), font8);
			pdfspricecell = new PdfPCell(chunk);
			pdfspricecell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			
			///
			
			PdfPCell pdfservice1=null;
			 //******************************
//			 double cstval=0;
//			 int cstFlag=0;
//			 if(po instanceof ServicePo){
//				 ServicePo salesq=(ServicePo)po;
//		   		
//		   		if(salesq.getcForm()!=null){
//		   		System.out.println(salesq.getcForm().trim()+"...print..");
//		   		
//		   		if(salesq.getcForm().trim().equals(AppConstants.YES)||salesq.getcForm().trim().equals(AppConstants.NO))
//		   		{
//		   			cstFlag=1;
//		   			cstval=salesq.getCstpercent();
//		   		}
//		   		}
//		   	}
			 
			 
			 System.out.println("one");
			 
			  if((products.get(i).getVatTax().getPercentage()>0)&& (products.get(i).getServiceTax().getPercentage()>0)){
		    	   
				  System.out.println("two");
				  if((products.get(i).getVatTax().getPercentage()!=0)&&(products.get(i).getServiceTax().getPercentage()!=0)){
					  
					if(cout>0){
		      	    chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage())+" / "+df.format(products.get(i).getServiceTax().getPercentage()),font8);
					}
					}else{
		      		
					  chunk = new Phrase("0"+"",font8);
				  }
					  pdfservice1 = new PdfPCell(chunk);
		 		 
		 		  
		 		  pdfservice1 = new PdfPCell(chunk);
		      	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				  
				  
				  
			  }
		   else if((products.get(i).getServiceTax().getPercentage()>0) && (products.get(i).getVatTax().getPercentage()==0)){
		  	
		  	 System.out.println("three");
		  	 if(products.get(i).getServiceTax().getPercentage()!=0){
		  		if(flag1==1||cout==1){
		  				chunk = new Phrase("0.00"+" / "+df.format(products.get(i).getServiceTax().getPercentage()),font8);
		  		}else if(st==1&&st1==1){
		  			chunk = new Phrase("0.00"+" / "+df.format(products.get(i).getServiceTax().getPercentage()),font8);
				}else{
		  				chunk = new Phrase(df.format(products.get(i).getServiceTax().getPercentage()),font8);
		  		}
		  		
		  	 }else{
		   		 chunk = new Phrase("0.00",font8);
		   	 }
		   	  pdfservice1 = new PdfPCell(chunk);
		   	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		   	 
		   }
		   else if((products.get(i).getServiceTax().getPercentage()==0)&&(products.get(i).getVatTax().getPercentage()>0)){
		  	 System.out.println("eight");
		  	 if(flag1==1||cout==1){
		  		 chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
		  	 }else if(st==1&&st1==1){
		  		chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage())+" / "+"0.00",font8);
			 }else{
		  		 chunk = new Phrase(df.format(products.get(i).getVatTax().getPercentage()),font8);
		  	 }
		  		 pdfservice1 = new PdfPCell(chunk);
		       	 pdfservice1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		       	 
		
		   }else{
		  		 chunk= new Phrase("0.00 / 0.00",font8);
		  	 pdfservice1 = new PdfPCell(chunk);
		   }
			
			
			
			
			///
			
			
		//	if (products.get(i).getTax() != null)
		//		chunk = new Phrase(products.get(i).getTax() + "", font8);
		//	else
		//		chunk = new Phrase("");
		//	
		//	pdfservice = new PdfPCell(chunk);	
		//	pdfservice.setHorizontalAlignment(Element.ALIGN_RIGHT);
			//if (products.get(i).getVat() != null)
			//	chunk = new Phrase(products.get(i).getVat() + "", font8);
			//else
			//	chunk = new Phrase("");
			//pdfvattax = new PdfPCell(chunk);
			
			if (products.get(i).getPercentageDiscount() != 0)
				chunk = new Phrase(products.get(i).getPercentageDiscount() + "", font8);
			else
				chunk = new Phrase("0"+"",font8);
			pdfperdiscount = new PdfPCell(chunk);
			pdfperdiscount.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			double totalVal = (products.get(i).getPrice() - taxVal)* products.get(i).getQty();
			if (products.get(i).getPercentageDiscount() != 0) {
				totalVal = totalVal- (totalVal * products.get(i).getPercentageDiscount() / 100);
			}else{
				chunk = new Phrase("");
			}	
			chunk = new Phrase(df.format(totalVal), font8);
			pdftotalproduct = new PdfPCell(chunk);
			pdftotalproduct.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			count=i;
			
			
						table.addCell(pdfcategcell);
						table.addCell(pdfnamecell);
						table.addCell(pdfqtycell);
						table.addCell(pdfuomcell);
						table.addCell(pdfspricecell);
						table.addCell(pdfperdiscount);
			//			table.addCell(pdfvattax);
						pdfservice1.setHorizontalAlignment(Element.ALIGN_CENTER);
						table.addCell(pdfservice1);
						table.addCell(pdftotalproduct);	
			
			////
			
			
		//for (int i=flag; i<rohan;i++) {
		//	
		//	if(getdata().get(i).getProductCategory()!=null){
		//	 chunk = new Phrase(getdata().get(i).getProductCategory(),font8);
		//	
		//	}else{
		//		 chunk = new Phrase("" ,font8);
		//	}
		//	PdfPCell pdfcategcell = new PdfPCell(chunk);
		//
		//	
		//	
		//	
		//	if(getdata().get(i).getProductQuantity()!=0){
		//	chunk = new Phrase(getdata().get(i).getProductQuantity() + "", font8);
		//	}
		//	else{
		//		chunk = new Phrase("", font8);
		//		PdfPCell pdfqtycell = new PdfPCell(chunk);
		//	}
		//	PdfPCell pdfqtycell = new PdfPCell(chunk);
		//	
		//	
		//	
		//	if(getdata().get(i).getTax()!=0){
		//		chunk = new Phrase(getdata().get(i).getTax() + "", font8);
		//	}else{
		//		chunk = new Phrase("", font8);
		//	}
		//	PdfPCell pdfservice = new PdfPCell(chunk);
		//
		//	
		//	
		//	
		//	if(getdata().get(i).getVat()!=0){
		//	
		//		chunk = new Phrase(getdata().get(i).getVat() + "", font8);
		//	}else{
		//		chunk = new Phrase( "", font8);
		//	}
		//	PdfPCell pdfvattax = new PdfPCell(chunk);
		//	
		//	
		//	
		//	
		//	if(getdata().get(i).getDiscount()!=0)
		//	{
		//		chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
		//
		//		}else{
		//			chunk = new Phrase(getdata().get(i).getDiscount() + "", font8);
		//		}	
		//	
		//	PdfPCell pdfperdiscount = new PdfPCell(chunk);
		//
		//		table.addCell(pdfcategcell);
		//		table.addCell(pdfqtycell);
		//		table.addCell(pdfperdiscount);
		//		table.addCell(pdfvattax);
		//		table.addCell(pdfservice);
		
			flag=flag+1;
			if(flag==80||flag==140||flag==200){
			break;
			}
		}
		
		PdfPCell prodtablecell=new PdfPCell();
		prodtablecell.addElement(table);
		
		PdfPTable parentTableProd=new PdfPTable(1);
		parentTableProd.setWidthPercentage(100);
		parentTableProd.setSpacingBefore(10f);
		parentTableProd.addCell(prodtablecell);
		
		
		try {
			document.add(parentTableProd);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		}


		private void otherchargestoAnnexur(){
		
		Font font1 = new Font(Font.FontFamily.HELVETICA  , 8|Font.BOLD);
		String charge ="Other charge details";
		Chunk prodchunk = new Chunk(charge,font10bold);
		Paragraph parag= new Paragraph();
		parag.add(prodchunk);
		
		
		PdfPTable chargetable = new PdfPTable(2);
		chargetable.setWidthPercentage(100);
		//********************
		double total=0;
		 for(int i=0;i<this.prodCharges.size();i++)
		 {
			   Phrase chunk = null;
			   double chargeAmt=0;
			   Phrase blank11=new Phrase("",font1);
			   PdfPCell blankcell=new PdfPCell(blank11);
			   PdfPCell pdfchargeamtcell = null;
			   PdfPCell pdfchargecell = null;
			   if(prodCharges.get(i).getChargePercent()!=0){
				   chunk = new Phrase(prodCharges.get(i).getChargeName()+" @ "+prodCharges.get(i).getChargePercent(),font1);
				   pdfchargecell=new PdfPCell(chunk);
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				   chargeAmt=prodCharges.get(i).getChargePercent()*prodCharges.get(i).getAssessableAmount()/100;
				   chunk=new Phrase(df.format(chargeAmt),font8);
			      	   pdfchargeamtcell = new PdfPCell(chunk);
			      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	   pdfchargeamtcell.setBorder(0);
			      	   
			   }
			   if(prodCharges.get(i).getChargeAbsValue()!=0){
				   chunk = new Phrase(prodCharges.get(i).getChargeName(),font1);
				   pdfchargecell=new PdfPCell(chunk);
				
				pdfchargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				   chargeAmt=prodCharges.get(i).getChargeAbsValue();
				   chunk=new Phrase(chargeAmt+"",font8);
			      	   pdfchargeamtcell = new PdfPCell(chunk);
			      	pdfchargeamtcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			      	   pdfchargeamtcell.setBorder(0);
			   }
			 pdfchargecell.setBorder(0);
			 
			total=total+chargeAmt;
			if(chargeAmt!=0){
			chargetable.addCell(pdfchargecell);
			chargetable.addCell(pdfchargeamtcell);
			}
		      	
		 }
		 
		 
		 
		 PdfPTable taotatable= new PdfPTable(2);
		 taotatable.setWidthPercentage(100);
		 
		 Phrase totalchunk = new Phrase("Total",font10bold);
			PdfPCell totalcell= new PdfPCell(totalchunk);
		//	totalcell.addElement(totalchunk);
			totalcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			totalcell.setBorder(0);
			
		 Phrase toatlamt = new Phrase(df.format(total),font10bold);
		 PdfPCell totalcell1= new PdfPCell(toatlamt);
		//	totalcell1.addElement(toatlamt);
			totalcell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			totalcell1.setBorder(0);
			
			taotatable.addCell(totalcell); 
			taotatable.addCell(totalcell1); 
		 
			 PdfPCell chargecell1= new PdfPCell();
			 chargecell1.addElement(taotatable);
			
			
			
			
			
			
		 PdfPCell chargecell= new PdfPCell();
		 chargecell.addElement(chargetable);
		 chargecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 PdfPTable parent= new PdfPTable(1);
		 parent.setWidthPercentage(70);
		 parent.setSpacingBefore(10f);
		 parent.addCell(chargecell);
		 parent.addCell(chargecell1);
		 parent.setHorizontalAlignment(Element.ALIGN_LEFT);
		 
		 
		 
		 try {
			    document.add(Chunk.NEWLINE);
			 	document.add(Chunk.NEWLINE);
				document.add(parag);
				document.add(parent);
			} catch (DocumentException e) {
				e.printStackTrace();
			}	 
		
		}

	public void commentDetailsForEmail()
	{
		for(int i=0;i<po.getItems().size();i++){
			
			if(po.getCompanyId()!=null){
				sup=ofy().load().type(SuperProduct.class).filter("companyId",po.getCompanyId()).
				filter("count", po.getItems().get(i).getPrduct().getCount()).first().now();
			
				
				SuperProduct superprod= new SuperProduct();
				
				superprod.setCount(sup.getCount());
				superprod.setProductName(sup.getProductName());
				superprod.setComment(sup.getComment()+" "+sup.getCommentdesc());
				
				stringlis.add(superprod);
			
			}
			
			else{
				sup=ofy().load().type(SuperProduct.class).filter("count", po.getItems().get(i).getPrduct().getCount()).first().now();
			}
			}
	}






}
