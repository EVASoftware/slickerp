package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.AssesmentReport;

public class CreateNBHCAssessmentReportServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4871595321346471448L;

	AssesmentReport assessmentRepObj;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		Logger logger = Logger.getLogger("CreateNBHCAssessmentReportServlet.class");
				
		try {
			String stringId = req.getParameter("Id");
			stringId = stringId.trim();
			Long count = Long.parseLong(stringId);

			String preprintStatus = req.getParameter("preprint");
			preprintStatus = preprintStatus.trim();
			
			
			assessmentRepObj=ofy().load().type(AssesmentReport.class).id(count).now();
			
			if(assessmentRepObj!=null){
				if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("AssesmentReport","onlyForNBHC",assessmentRepObj.getCompanyId())){
					NBHCAssessmentReportPdf assessmentReportPdf = new NBHCAssessmentReportPdf();
					assessmentReportPdf.document = new Document();
					assessmentReportPdf.document = new Document(PageSize.A4.rotate());
					Document document = assessmentReportPdf.document;
					PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());// write the pdf in response
					document.open();
					assessmentReportPdf.setAssessmentDetails(count);
					assessmentReportPdf.createPdf(preprintStatus);
					document.close();
				}else{
					SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
					fmt.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					String filename=assessmentRepObj.getCinfo().getFullName()+" - "+assessmentRepObj.getCount()+" - "+fmt.format(assessmentRepObj.getAssessmentDate())+".pdf";
					resp.setHeader("Content-Disposition", "attachment; filename=" + filename);//Ashwini Patil Date:22-09-2022
					logger.log(Level.SEVERE,"filename="+filename);
					AssessmentReportPdf assessmentReportPdf = new AssessmentReportPdf();
					assessmentReportPdf.document = new Document();
//					assessmentReportPdf.document = new Document(PageSize.A4.rotate());
					Document document = assessmentReportPdf.document;
					PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());// write the pdf in response
					document.open();
					assessmentReportPdf.setAssessmentDetails(count,assessmentRepObj);
					assessmentReportPdf.createPdf(preprintStatus);
					document.close();
				}
			}

			

		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
