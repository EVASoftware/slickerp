package com.slicktechnologies.server.addhocprinting;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.fumigation.Fumigation;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;

public class FumigationAusPdf{

	public Document document;

	Fumigation fumigation;
	ProcessConfiguration processConfig;
	boolean upcflag=false;
	boolean fumigationTitleFlag=false;
	Company comp;
	List<ArticleType> articletype;
	 Logger logger = Logger.getLogger("NameOfYourLogger");
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font16bold,font9bold,font10,font10bold,font14bold,font14,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	DecimalFormat df=new DecimalFormat("0.00");
	
	
	private boolean paytermsflag=false;
	private boolean disclaimerflag=false;
	
	float[] columnWidths={2.0f,3.0f,2.0f};
	float[] columnWidths1={2.0f,0.5f,2.5f,3.5f,0.5f,2.5f};
	float[] columnWidths3={3.0f,0.5f,2.5f,2.5f,0.5f,2.5f};
	float[] columnWidths2={0.5f,1.5f};
	 
	 
	 float[] columnWidths41={5.5f,1.0f,0.5f,1.5f};
	 float[] columnWidths4={1.5f,7.0f};
	 float[] columnWidths15={2.0f,0.5f,1.5f,0.5f,1.5f};
	 float[] columnWidths5={2.0f,1.5f,2.0f,1.5f,2.5f,2.5f};
	 float[] columnWidths6={3.0f,0.8f,0.2f,3.0f};
	 
	 float[] columnWidths7={1.7f,0.7f,1.5f,0.7f,1.5f,0.7f,2.5f};
	 float[] columnWidths8={4.5f,5.5f};
	 float[] columnWidths16={1.0f,0.5f,1.0f,0.5f,1.0f};
	 
	public FumigationAusPdf() {
		
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font9bold = new Font(Font.FontFamily.HELVETICA,9,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD|Font.UNDERLINE);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10=new Font(Font.FontFamily.HELVETICA,10);
		font10bold = new Font(Font.FontFamily.HELVETICA  , 10, Font.BOLD);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		font14 = new Font(Font.FontFamily.HELVETICA  , 14, Font.NORMAL); 
		font9 = new Font(Font.FontFamily.HELVETICA  , 9, Font.NORMAL); 
	}
	
	public void setFumigation(Long count,String preprint) {
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD| Font.UNDERLINE);
		
		
		fumigation=ofy().load().type(Fumigation.class).id(count).now();
	
		if(fumigation.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",fumigation.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		
/************************************Letter Head Flag*******************************/
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Fumigation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						upcflag=true;
					}
				}
			}
		}
		
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Fumigation").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("FumigationTitlePrint")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						fumigationTitleFlag=true;
					}
				}
			}
		}
		
		
//*********************article type information	**************************	
		
		 articletype = new ArrayList<ArticleType>();
			if(comp.getArticleTypeDetails().size()!=0){
				articletype.addAll(comp.getArticleTypeDetails());
			}
			fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	}

	
	//*******************************PAYMENT EMIAL LOGIC (ROHAN)********************************** 
	

	public void createPdfForEmail(Fumigation fumigation, Company comp2,Customer c) {
		
		this.fumigation=fumigation;
		this.comp=comp2;
//		this.cust=custEntity;
//		this.con=contractEntity;
//		this.billEntity=billingEntity;
//		this.salesProd=invoiceentity.getSalesOrderProducts();
//		this.contractTaxesLis=con.getProductTaxes();
//		this.contractChargesLis=con.getProductCharges();
//		this.billingTaxesLis=invoiceentity.getBillingTaxes();
//		this.billingChargesLis=invoiceentity.getBillingOtherCharges();
//		this.payTermsLis=contractEntity.getPaymentTermsList();
		
		if(fumigation.getCompanyId()!=null){
			this.processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceInvoicePayTerm")&&processConfig.getProcessList().get(k).isStatus()==true){
						this.paytermsflag=true;
					}
					else{
						this.paytermsflag=false;
					}
				}
			}
		}
		
/************************************Letter Head Flag*******************************/
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.upcflag=true;
					}
				}
			}
		}
		
		
		 articletype = new ArrayList<ArticleType>();
			if(comp.getArticleTypeDetails().size()!=0){
				articletype.addAll(comp.getArticleTypeDetails());
			}
		
		
		/**********************************Disclaimer Flag******************************************/
		
		if(fumigation.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", fumigation.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.disclaimerflag=true;
					}
				}
			}
		}
		if(upcflag==false){
			
			System.out.println("2222222222222222222222222222222");	
			System.out.println("contract inside plane");
			
			if(comp.getLogo()!=null){
			createLogo(document,comp);
			}
			createCompanyAddress();
			}else{
			
//			if(preprint.equals("yes")){
//				
//				System.out.println("333333333333333333333333333333333");	
//				System.out.println("inside prit yes");
//				createBlankforUPC();
//			}
			
//			if(preprint.equals("no")){
				System.out.println("4444444444444444444444444444444444444444444");	
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}
			
//		}
		
		
		if(fumigationTitleFlag==false){
			CreateHeading();
		}
		fumigationDetails();
		createGoodsDetails();
		createTreatmentDetails();
		remainingAllDetails();
		latsDetails();
		
	}

	
	//*********************************CHANGES ENDS HERE *********************************
	
	
	public void createPdf(String preprint) {
		
		System.out.println("..................................."+preprint);	
		if(upcflag==false && preprint.equals("plane")){
			
			System.out.println("2222222222222222222222222222222");	
			System.out.println("contract inside plane");
			
			if(comp.getLogo()!=null){
			createLogo(document,comp);
			}
			createCompanyAddress();
			}else{
			
			if(preprint.equals("yes")){
				
				System.out.println("333333333333333333333333333333333");	
				System.out.println("inside prit yes");
				createBlankforUPC();
			}
			
			if(preprint.equals("no")){
				System.out.println("4444444444444444444444444444444444444444444");	
			System.out.println("inside prit no");
			
			if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
			if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
			createBlankforUPC();
			}
			
		}
		
		
		if(fumigationTitleFlag==false){
			CreateHeading();
		}
		fumigationDetails();
		createGoodsDetails();
		createTreatmentDetails();
		remainingAllDetails();
		latsDetails();
	}

	
	
private void createCompanyNameAsHeader(Document doc, Company comp) {
		
	DocumentUpload document =comp.getUploadHeader();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
	
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE .jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,725f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createCompanyNameAsFooter(Document doc, Company comp) {
	
	
	DocumentUpload document =comp.getUploadFooter();

	//patch
	String hostUrl;
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f,40f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
	
//	try
//	{
//	Image image1=Image.getInstance("images/SHRADDHA PEST CONTROL SERVICES LATER PADE 1.jpg");
//	image1.scalePercent(15f);
//	image1.scaleAbsoluteWidth(520f);
//	image1.setAbsolutePosition(40f,40f);	
//	doc.add(image1);
//	}
//	catch(Exception e)
//	{
//		e.printStackTrace();
//	}
	}

private void createBlankforUPC() {
		
		
		 Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		
		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	
	
	
	

	private void latsDetails() {
		
		PdfPTable lastTable = new PdfPTable(4);
		lastTable.setWidthPercentage(100f);
		lastTable.setSpacingBefore(10f);
		 try {
			 lastTable.setWidths(columnWidths6);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		 
		
		 
		 
		Phrase blank = new Phrase(" ", font9);
		PdfPCell blankcell = new PdfPCell(blank);
		blankcell.setBorder(0);
		Phrase message = new Phrase(fumigation.getMoredecl(), font9bold);
		PdfPCell messageCell = new PdfPCell(message);
		messageCell.setBorder(0);
		lastTable.addCell(messageCell);
		
		Phrase hologram = new Phrase("                        Holograme", font9);
		PdfPCell hologramCell = new PdfPCell(hologram);
		hologramCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		hologramCell.setBorder(0);
		
		PdfPTable table =new PdfPTable(1); 
		table.addCell(hologramCell);
		
		
		PdfPCell tablCell = new PdfPCell(table);
		lastTable.addCell(tablCell);
		
		lastTable.addCell(blankcell);
		
		
		lastTable.addCell(blankcell);
		
		
		 
		 Phrase place = new Phrase("Place",font9);
		 PdfPCell placeCell = new PdfPCell(place);
		 placeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 placeCell.setBorder(0);
		 
		 Phrase placeValue = new Phrase(fumigation.getPlace(),font9);
		 PdfPCell placeValueCell = new PdfPCell(placeValue);
		 placeValueCell.setBorder(0);
		 
		
		 Phrase date = new Phrase("Issued Date",font9);
		 PdfPCell dateCell = new PdfPCell(date);
		 dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 dateCell.setBorder(0);
		
		 logger.log(Level.SEVERE,"Hiiiiiiiiiiiiiiiiiiiiiiii date of issued "+fmt.format(fumigation.getDateofissue()));
		 Phrase dateValue = new Phrase(fmt.format(fumigation.getDateofissue()),font9);
		 PdfPCell dateValueCell = new PdfPCell(dateValue);
		 dateValueCell.setBorder(0);
		 
		 Phrase doted = new Phrase(":",font9);
		 PdfPCell dotedCell = new PdfPCell(doted);
		 dotedCell.setBorder(0);
		 
		 Phrase space = new Phrase("",font9);
		 PdfPCell spaceCell = new PdfPCell(space);
		 spaceCell.setBorder(0);
		 
		 PdfPTable placetable = new PdfPTable(4);
		 placetable.setWidthPercentage(100f);
		 placetable.setSpacingBefore(5f);
		 placetable.addCell(spaceCell);
		 placetable.addCell(placeCell);
		 placetable.addCell(dotedCell);
		 placetable.addCell(placeValueCell);
		 placetable.addCell(spaceCell);
		 placetable.addCell(dateCell);
		 placetable.addCell(dotedCell);
		 placetable.addCell(dateValueCell);
		
		 try {
			 placetable.setWidths(columnWidths41);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		 
		 
		 
		try {
			document.add(placetable);
			document.add(lastTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	private void fumigationDetails() {
		
		String name = fumigation.getNameoffumigation().toUpperCase();
		
		Phrase namePhrase = new Phrase("AFAS - "+name+" FUMIGATION CERTIFICATE",font10bold);
		Paragraph namepara = new Paragraph(namePhrase);
		namepara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPTable table123= new PdfPTable(6);
		table123.setWidthPercentage(100f);
		
		Phrase certificateNo = new Phrase("Certificate Number",font9bold);
		PdfPCell certificateNoCell = new PdfPCell(certificateNo);
		certificateNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificateNoCell.setBorder(0);
		

		
		Phrase certificateNoValue = new Phrase(fumigation.getCertificateNo(),font9bold);
		PdfPCell certificateNoValueCell = new PdfPCell(certificateNoValue);
		certificateNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		certificateNoValueCell.setBorderWidthBottom(0);
		
		
		Phrase newPhars = new Phrase(" ",font9bold);
		PdfPCell newphcell = new PdfPCell(newPhars);
		newphcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		newphcell.setBorderWidthTop(0);
		
		
		String afas="AFAS Registration Number";
		
		Phrase afasRegNo = new Phrase(afas,font9bold);
		PdfPCell afasRegNoCell = new PdfPCell(afasRegNo);
		afasRegNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		afasRegNoCell.setBorder(0);
		
		
		
		String aei="AEI Registration Number";
		Phrase aeiRegNo = new Phrase(aei,font9bold);
		PdfPCell aeiRegNoCell = new PdfPCell(aeiRegNo);
		aeiRegNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		aeiRegNoCell.setBorder(0);

		
		
//**************article type information **********************
		
		
		System.out.println("article type list size"+articletype.size());
		
		String artclName=null;
		String artclValue=null;
		
		  PdfPTable articletable=new PdfPTable(4);
		  articletable.setWidthPercentage(100);
		  articletable.setHorizontalAlignment(Element.ALIGN_LEFT);
		  
		  
		  
		  for(int i=0;i<this.articletype.size();i++){
				 
			  
				 if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().equals("Fumigation")){
				
					 
					 if(afas.equals(articletype.get(i).getArticleTypeName()))
					 {
						 artclValue=articletype.get(i).getArticleTypeValue();
					 }
					 
					 if(aei.equals(articletype.get(i).getArticleTypeName()))
					 {
						 artclName=articletype.get(i).getArticleTypeValue();
					 }
					 
				 }
				 
				 
				 
		  }
		
		
//************************changes ends here *********************
		
		  Phrase afasRegNoValue = new Phrase(artclValue,font9bold);
			PdfPCell afasRegNoValueCell = new PdfPCell(afasRegNoValue);
			afasRegNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			afasRegNoValueCell.setBorder(0);
			afasRegNoValueCell.setBorderWidthBottom(0);
			
			Phrase aeiRegNoValue = new Phrase(artclName,font9bold);
			PdfPCell aeiRegNoValueCell = new PdfPCell(aeiRegNoValue);
			aeiRegNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			aeiRegNoValueCell.setBorder(0);
			aeiRegNoValueCell.setBorderWidthTop(0);
		  
		
		Phrase blank = new Phrase(" ",font9bold);
		PdfPCell blankCell = new PdfPCell(blank);
		blankCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		blankCell.setBorder(0);
		
		Phrase dots = new Phrase(" : ",font9bold);
		PdfPCell dotscell = new PdfPCell(dots);
		dotscell.setHorizontalAlignment(Element.ALIGN_CENTER);
		dotscell.setBorder(0);
		
		table123.addCell(certificateNoCell);
		table123.addCell(dotscell);
		table123.addCell(certificateNoValueCell);
		table123.addCell(afasRegNoCell);
		table123.addCell(dotscell);
		table123.addCell(afasRegNoValueCell);
		table123.addCell(blankCell);
		table123.addCell(blankCell);
		table123.addCell(newphcell);
		table123.addCell(aeiRegNoCell);
		table123.addCell(dotscell);
		table123.addCell(aeiRegNoValueCell);
//		table123.setSpacingAfter(10f);
		
		try {
			table123.setWidths(columnWidths1);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase targetFumigation = new Phrase("TARGET OF FUMIGATION DETAILS",font9bold);
		Paragraph targepara = new Paragraph(targetFumigation);
		targepara.setAlignment(Element.ALIGN_LEFT);
		
		PdfPTable newTable = new PdfPTable(1);
		newTable.setWidthPercentage(100f);
		
		PdfPCell newCell= new PdfPCell();
		newCell.addElement(namepara);
		newCell.setBorderWidthBottom(0);
		
		newCell.setBorderWidthLeft(0);
		newCell.setBorderWidthRight(0);
		
		PdfPCell newCell1= new PdfPCell();
		newCell1.addElement(table123);
		newCell1.setBorderWidthTop(0);
		newCell1.setBorderWidthBottom(0);
		
		newCell1.setBorderWidthLeft(0);
		newCell1.setBorderWidthRight(0);
		
		PdfPCell newCell2= new PdfPCell();
		newCell2.addElement(targepara);
		newCell2.setBorderWidthTop(0);
		
		newCell2.setBorderWidthLeft(0);
		newCell2.setBorderWidthRight(0);
		
		
		newTable.addCell(newCell);
		newTable.addCell(newCell1);
		newTable.addCell(newCell2);
		
		try {
			document.add(newTable);
			
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase tarFum = new Phrase("Target of fumigation :",font9);
		PdfPCell tarFumCell = new PdfPCell(tarFum);
		tarFumCell.setBorder(0);
		
		//****************
		
		PdfPTable demoCrosstable = new PdfPTable(1);
		demoCrosstable.setWidthPercentage(25f);
		PdfPCell demoCrosstableCell = new PdfPCell();
		Phrase cross = new Phrase("X",font9);
		PdfPCell crossCell = new PdfPCell(cross);
		crossCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoCrosstable.addCell(crossCell);
		
		demoCrosstableCell.addElement(demoCrosstable);
		demoCrosstableCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoCrosstableCell.setBorder(0);
		
		
		
		
		PdfPTable demoPlanetable = new PdfPTable(1);
		demoPlanetable.setWidthPercentage(25f);
		Phrase plane = new Phrase(" ",font9);
		PdfPCell planeCell = new PdfPCell(plane);
		planeCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoPlanetable.addCell(planeCell);
		PdfPCell demoplanetableCell = new PdfPCell();
		
		demoplanetableCell.addElement(demoPlanetable);
		demoplanetableCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoplanetableCell.setBorder(0);
		
		//*******************
		
		
		Phrase com=null;
		PdfPCell comCell=null;
//		if(fumigation.getCommoditybool()==true){
			com= new Phrase("Commodity",font9);
			comCell = new PdfPCell(com);
			comCell.setBorder(0);
//		}
//		else{
//			com = new Phrase("Commodity",font9);
//			comCell = new PdfPCell(com);
//			comCell.setBorder(0);
//		}
		
		
		Phrase packing=null;
		PdfPCell packingCell=null;
//		if(fumigation.getPacking()==true){
			packing = new Phrase("Packing",font9);
			packingCell = new PdfPCell(packing);
			packingCell.setBorder(0);
//		}
//		else{
//			packing	 = new Phrase("Packing",font9);
//			packingCell = new PdfPCell(packing);
//			packingCell.setBorder(0);
//		}
		
		
		Phrase both=null;
		PdfPCell bothcell=null;
//		if(fumigation.getBothCommodityandPacking()==true){
			both= new Phrase("Both Commodiy and Packing",font9);
			bothcell= new PdfPCell(both);
			bothcell.setBorder(0);
//		}
//		else{
//			both= new Phrase("Both Commodiy and Packing",font9);
//			bothcell = new PdfPCell(both);
//			bothcell.setBorder(0);
//		}
		
		
		PdfPTable fumigationDetailsTable = new PdfPTable(7);
		
		try {
			fumigationDetailsTable.setWidths(columnWidths7);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

		
		fumigationDetailsTable.setWidthPercentage(100f);
//		fumigationDetailsTable.setSpacingBefore(20f);
//		fumigationDetailsTable.setSpacingAfter(10f);
		fumigationDetailsTable.addCell(tarFumCell);
		if(fumigation.getCommoditybool()==true){
			fumigationDetailsTable.addCell(demoCrosstableCell);
		}
		else
		{
			fumigationDetailsTable.addCell(demoplanetableCell);
		}
		fumigationDetailsTable.addCell(comCell);
		
		if(fumigation.getPacking()==true){
			fumigationDetailsTable.addCell(demoCrosstableCell);
		}
		else
		{
			fumigationDetailsTable.addCell(demoplanetableCell);
		}
		fumigationDetailsTable.addCell(packingCell);
		
		if(fumigation.getBothCommodityandPacking()==true){
			fumigationDetailsTable.addCell(demoCrosstableCell);
		}
		else
		{
			fumigationDetailsTable.addCell(demoplanetableCell);
		}
		fumigationDetailsTable.addCell(bothcell);
		
		
		
		
		
//		Phrase dotsphrase = new Phrase(":",font9);
//		PdfPCell dotcell = new PdfPCell(dotsphrase);
//		dotcell.addElement(dotsphrase);
//		dotcell.setBorder(0);
//		dotcell.setHorizontalAlignment(Element.ALIGN_CENTER);
		
		Phrase dotsphrase = new Phrase(" ",font9);
		PdfPCell dotcell = new PdfPCell(dotsphrase);
		dotcell.addElement(dotsphrase);
		dotcell.setBorder(0);
		
		
		Phrase commodidy = new Phrase("Commodity         :",font9);
		PdfPCell commodityCell = new PdfPCell(commodidy);
		commodityCell.setBorder(0);

		Phrase commodidyValue = new Phrase(fumigation.getCommodity(),font9);
		PdfPCell commodidyValueCell = new PdfPCell(commodidyValue);
		commodidyValueCell.setBorder(0);
		
		
		PdfPTable table = new PdfPTable(2);
		try {
			table.setWidths(columnWidths4);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		table.setWidthPercentage(100f);
		table.addCell(commodityCell);
//		table.addCell(dotcell);
		table.addCell(commodidyValueCell);
		
		
		
		
		Phrase Packing = new Phrase("Consignment link :",font9);
		PdfPCell PackingCell = new PdfPCell(Packing);
		PackingCell.setBorder(0);
		
		Phrase PackingValue = new Phrase(fumigation.getConsignmentlink(),font9);
		PdfPCell PackingValueCell = new PdfPCell(PackingValue);
		PackingValueCell.setBorder(0);
		
		PdfPTable table1 = new PdfPTable(2);
//		table1.setSpacingAfter(10f);
		 try {
			 table1.setWidths(columnWidths4);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		table1.setWidthPercentage(100f);
		table1.addCell(PackingCell);
//		table1.addCell(dotcell);
		table1.addCell(PackingValueCell);
		
		
		
		
		
		Phrase conOrigin = new Phrase("Country of origin :",font9);
		PdfPCell conOriginCell = new PdfPCell(conOrigin); 
		conOriginCell.setBorder(0);
		
		Phrase conOriginValue =new Phrase(fumigation.getImportcountry(),font9);
		PdfPCell conOriginValueCell = new PdfPCell(conOriginValue); 
		conOriginValueCell.setBorder(0);
		
		Phrase pol = new Phrase("Port of loading :",font9);
		PdfPCell polCell = new PdfPCell(pol); 
		polCell.setBorder(0);
		
		Phrase polValue =new Phrase(fumigation.getPortncountryloading(),font9);
		PdfPCell polValueCell = new PdfPCell(polValue); 
		polValueCell.setBorder(0);
		
		Phrase codest = new Phrase("Country of destination :",font9);
		PdfPCell codestCell = new PdfPCell(codest); 
		codestCell.setBorder(0);
		
		Phrase codestValue =new Phrase(fumigation.getCountryofdestination(),font9);
		PdfPCell codestValueCell = new PdfPCell(codestValue); 
		codestValueCell.setBorder(0);
		
		
		PdfPTable table2 = new PdfPTable(6);
		table2.setWidthPercentage(100f);
//		table2.setSpacingAfter(10f);
		
		table2.addCell(conOriginCell);
//		table2.addCell(dotcell);
		table2.addCell(conOriginValueCell);
		
		table2.addCell(polCell);
//		table2.addCell(dotcell);
		table2.addCell(polValueCell);
		
		table2.addCell(codestCell);
//		table2.addCell(dotcell);
		table2.addCell(codestValueCell);
		
		 try {
			 table2.setWidths(columnWidths5);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		
		 
		 
		 PdfPTable newTable1 = new PdfPTable(1);
		 newTable1.setWidthPercentage(100f);
			
			PdfPCell newCell11= new PdfPCell();
			newCell11.addElement(fumigationDetailsTable);
			newCell11.setBorderWidthBottom(0);
			
			newCell11.setBorderWidthLeft(0);
			newCell11.setBorderWidthRight(0);
			
			PdfPCell newCell12= new PdfPCell();
			newCell12.addElement(table);
			newCell12.setBorderWidthTop(0);
			newCell12.setBorderWidthBottom(0);
			
			newCell12.setBorderWidthLeft(0);
			newCell12.setBorderWidthRight(0);
			
			PdfPCell newCell13= new PdfPCell();
			newCell13.addElement(table1);
			newCell13.setBorderWidthTop(0);
			newCell13.setBorderWidthBottom(0);
			
			newCell13.setBorderWidthLeft(0);
			newCell13.setBorderWidthRight(0);
			
			PdfPCell newCell14= new PdfPCell();
			newCell14.addElement(table2);
			newCell14.setBorderWidthTop(0);
			
			newCell14.setBorderWidthLeft(0);
			newCell14.setBorderWidthRight(0);
			
			newTable1.addCell(newCell11);
			newTable1.addCell(newCell12);
			newTable1.addCell(newCell13);
			newTable1.addCell(newCell14);
			
			try {
				document.add(newTable1);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		 
	}

private void createGoodsDetails() {
		  
		Phrase fromaddress=new Phrase("Name and address Of exporter",font9bold);
		PdfPCell fromaddcell=new PdfPCell();
		fromaddcell.addElement(fromaddress);
		fromaddcell.setBorderWidthBottom(0);
		fromaddcell.setBorderWidthLeft(0);
		fromaddcell.setBorderWidthTop(0);
		  
		  
		String fromcompany;
		  
		 if(fumigation.getFromCompanyname()!=null){
			 
			 fromcompany=fumigation.getFromCompanyname();
		 }else{
			 fromcompany=" ";
		 }
		 
		  Phrase fromcompanypharse=new Phrase(fromcompany,font9bold);
		  PdfPCell fromcompcell=new PdfPCell();
		  fromcompcell.addElement(fromcompanypharse);
		  fromcompcell.setBorder(0);
		  
		  String addresslin1=" ";
		  
		  if(!fumigation.getFromaddress().getAddrLine1().equals("")){
			  addresslin1 =fumigation.getFromaddress().getAddrLine1().trim()+" , ";
		  }
		  String addressline2=" ";
		  
		  if(!fumigation.getFromaddress().getAddrLine2().equals("")){
			  
			  addressline2 =fumigation.getFromaddress().getAddrLine2().trim()+" , ";
			  
		  }
		 
		  String landmark=" ";
		  if(!fumigation.getFromaddress().getLandmark().equals("")){
			  
			  landmark=fumigation.getFromaddress().getLandmark()+" , ";
			  
		  }
		  String locality=" ";
		  
		  if(!fumigation.getFromaddress().getLocality().equals("")){
			  
			  locality=fumigation.getFromaddress().getLocality()+" , ";
		  }
		  
		  
		  
		  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality,font9);
		  PdfPCell fromaddrescell1=new PdfPCell();
		  fromaddrescell1.addElement(addressline1);
		  fromaddrescell1.setBorder(0);
		  
		  
		  String city=" ";
		  if(!fumigation.getFromaddress().getCity().equals("")){
			  city=fumigation.getFromaddress().getCity()+",";
		  }
		  String state=" ";
		  if(!fumigation.getFromaddress().getState().equals("")){
			  state=fumigation.getFromaddress().getState()+"-";
		  }
		  String pin=" ";
		  if(fumigation.getFromaddress().getPin()!=0){
			  pin=fumigation.getFromaddress().getPin()+",";
		  }
		  String country=" ";
		  if(!fumigation.getFromaddress().getCountry().equals("")){
			  country=fumigation.getFromaddress().getCountry();
		  }
		  
		  
		  
		  Phrase addressline3=new Phrase(city+state+pin+country,font9);
		  PdfPCell fromaddrescell2=new PdfPCell();
		  fromaddrescell2.addElement(addressline3);
		  fromaddrescell2.setBorder(0);
		  
		  
		  Phrase toaddress=new Phrase("Name and address Of importer",font9bold);
			PdfPCell toaddcell=new PdfPCell();
			toaddcell.addElement(toaddress);
			toaddcell.setBorderWidthBottom(0);
			toaddcell.setBorderWidthRight(0);
			toaddcell.setBorderWidthTop(0);
		  
		  
			
			 String tocompany;
			  
			 if(fumigation.getFromCompanyname()!=null){
				 
				 tocompany=fumigation.getToCompanyname();
			 }else{
				 tocompany=" ";
			 }
			 
			  Phrase tocompanypharse=new Phrase(tocompany,font9bold);
			  PdfPCell tocompcell=new PdfPCell();
			  tocompcell.addElement(tocompanypharse);
			  tocompcell.setBorder(0);
			  
			  String toaddresslin1=" ";
			  if(!fumigation.getTomaddress().getAddrLine1().equals("")){
			  toaddresslin1=fumigation.getTomaddress().getAddrLine1().trim()+" , ";
			  }
			  
			  String toaddressline2=" ";
			  
			  if(!fumigation.getTomaddress().getAddrLine2().equals("")){
				  
				  toaddressline2 =fumigation.getTomaddress().getAddrLine2().trim()+" , ";
				  
			  }
			 
			  String tolandmark=" ";
			  if(!fumigation.getTomaddress().getLandmark().equals("")){
				  
				  tolandmark=fumigation.getTomaddress().getLandmark()+" , ";
				  
			  }
			  String tolocality=" ";
			  
			  if(!fumigation.getTomaddress().getLocality().equals("")){
				  
				  tolocality=fumigation.getTomaddress().getLocality()+" , ";
			  }
			  
			  
			  
			  Phrase toaddressline1=new Phrase(toaddresslin1+toaddressline2+tolandmark+tolocality,font10);
			  PdfPCell toaddrescell1=new PdfPCell();
			  toaddrescell1.addElement(toaddressline1);
			  toaddrescell1.setBorder(0);
			  
			  
			  String tocity=" ";
			  if(!fumigation.getTomaddress().getCity().equals("")){
				  tocity=fumigation.getTomaddress().getCity()+",";
			  }
			  String tostate=" ";
			  if(!fumigation.getTomaddress().getState().equals("")){
				  tostate=fumigation.getTomaddress().getState()+"-";
			  }
			  String topin=" ";
			  if(fumigation.getTomaddress().getPin()!=0){
				  topin=fumigation.getTomaddress().getPin()+"";
			  }
			  String tocountry=" ";
			  if(!fumigation.getTomaddress().getCountry().equals("")){
				  tocountry=fumigation.getTomaddress().getCountry();
			  }
			  
			  
			  
			  Phrase toaddressline3=new Phrase(tocity+tostate+topin+tocountry,font10);
			  PdfPCell toaddrescell2=new PdfPCell();
			  toaddrescell2.addElement(toaddressline3);
			  toaddrescell2.setBorder(0);
			
			
			
			
			
			
			PdfPTable fromaddresstable=new PdfPTable(1);
			fromaddresstable.setWidthPercentage(100f);
			fromaddresstable.addCell(fromcompcell);
			fromaddresstable.addCell(fromaddrescell1);
			fromaddresstable.addCell(fromaddrescell2);
			
			
			PdfPTable toaddresstable=new PdfPTable(1);
			toaddresstable.setWidthPercentage(100f);
			toaddresstable.addCell(tocompcell);
			toaddresstable.addCell(toaddrescell1);
			toaddresstable.addCell(toaddrescell2);
			
			
			PdfPCell fromaddresscell=new PdfPCell(fromaddresstable);
			fromaddresscell.setBorderWidthTop(0);
			fromaddresscell.setBorderWidthBottom(0);
			fromaddresscell.setBorderWidthLeft(0);
			
			PdfPCell toaddresscell=new PdfPCell(toaddresstable);
			toaddresscell.setBorderWidthTop(0);
			toaddresscell.setBorderWidthBottom(0);
			toaddresscell.setBorderWidthRight(0);
			
			PdfPTable addresstable=new PdfPTable(2);
			addresstable.setWidthPercentage(100f);
			
			addresstable.addCell(fromaddcell);
			addresstable.addCell(toaddcell);
			addresstable.addCell(fromaddresscell);
			addresstable.addCell(toaddresscell);
			
			
		PdfPCell addresstableCell = new PdfPCell();
		addresstableCell.addElement(addresstable);
		addresstableCell.setBorderWidthRight(0);
		addresstableCell.setBorderWidthLeft(0);
			
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		parent.addCell(addresstableCell);
		
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
	

private void createTreatmentDetails() {

	
	String	titlepdf="TREATEMENT DETAILS";
	
	Phrase titlephrase=new Phrase(titlepdf,font10bold);
	Paragraph titlepdfpara=new Paragraph();
	titlepdfpara.add(titlephrase);
	
	PdfPCell titlecell=new PdfPCell(titlepdfpara);
	titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	titlecell.setBorderWidthLeft(0);
	titlecell.setBorderWidthRight(0);
	
	
	PdfPTable titlepdftable1=new PdfPTable(1);
	titlepdftable1.setWidthPercentage(100);
	titlepdftable1.addCell(titlecell);
	
	
	try {
		document.add(titlepdftable1);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
	Phrase blankphrase=new Phrase("",font8);
	PdfPCell blankCell=new PdfPCell();
	blankCell.addElement(blankphrase);
	blankCell.setBorder(0);

	
	 Phrase dateoffumi=new Phrase("Date Fumigation Completed",font9);
	 PdfPCell datefumicell=new PdfPCell();
	 datefumicell.addElement(dateoffumi);
	 datefumicell.setBorder(0);
	 datefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 Phrase dateoffumiValue=null;
		 if(fumigation.getDateoffumigation()!=null){
			 dateoffumiValue=new Phrase(fmt.format(fumigation.getDateoffumigation()),font9);
		 }
	 else
	 {
		 dateoffumiValue=new Phrase("",font9);
	 }
	 
	 PdfPCell datefumiValuecell=new PdfPCell();
	 datefumiValuecell.addElement(dateoffumiValue);
	 datefumiValuecell.setBorder(0);
	 datefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	
	 Phrase placeoffumi=new Phrase("Place of Fumigation",font9);
	 PdfPCell placefumicell=new PdfPCell();
	 placefumicell.addElement(placeoffumi);
	 placefumicell.setBorder(0);
	 placefumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 Phrase placefumiValue=null;
		 if(fumigation.getPlaceoffumigation()!=null){
			 placefumiValue=new Phrase(fumigation.getPlaceoffumigation(),font9);
		 }
	 else
	 {
		 placefumiValue=new Phrase("",font9);
	 }
	 
	 PdfPCell placefumiValuecell=new PdfPCell();
	 placefumiValuecell.addElement(placefumiValue);
	 placefumiValuecell.setBorder(0);
	 placefumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	
	 Phrase dosefumigation=new Phrase("AQIS Prescribed Dose Rate",font9);
	 PdfPCell dosafumicell=new PdfPCell();
	 dosafumicell.addElement(dosefumigation);
	 dosafumicell.setBorder(0);
	 dosafumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 Phrase dosefumiValue=null;
		 if(fumigation.getDoseratefumigation()!=null){
			 dosefumiValue=new Phrase(fumigation.getDoseratefumigation()+" gms/m3",font9);
		 }
	 else
	 {
		 dosefumiValue=new Phrase("",font9);
	 }
	 
	 PdfPCell dosafumiValuecell=new PdfPCell();
	 dosafumiValuecell.addElement(dosefumiValue);
	 dosafumiValuecell.setBorder(0);
	 dosafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	 Phrase durationfumi=new Phrase("Exposure period(hrs)",font9);
	 PdfPCell durarationfumicell=new PdfPCell();
	 durarationfumicell.addElement(durationfumi);
	 durarationfumicell.setBorder(0);
	 durarationfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 Phrase durafumiValue=null;
		 if(fumigation.getDurartionfumigation()!=null){
			 durafumiValue=new Phrase(fumigation.getDurartionfumigation()+" hrs",font9);
		 }
	 else
	 {
		 durafumiValue=new Phrase("",font9);
	 }
	 
	 PdfPCell durafumiValuecell=new PdfPCell();
	 durafumiValuecell.addElement(durafumiValue);
	 durafumiValuecell.setBorder(0);
	 durafumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	 
	 
	 
	 
	 Phrase minaietemp=new Phrase("Forecast minimum temp(in "+"\u00b0"+"C)",font9);
	 PdfPCell tempfumicell=new PdfPCell();
	 tempfumicell.addElement(minaietemp);
	 tempfumicell.setBorder(0);
	 tempfumicell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 Phrase tempfumiValue=null;
		 if(fumigation.getMinairtemp()!=null){
			 tempfumiValue=new Phrase(fumigation.getMinairtemp()+"\u00b0"+"C",font9);
		 }
	 else
	 {
		 tempfumiValue=new Phrase("",font9);
	 }
	 
	 PdfPCell tempfumiValuecell=new PdfPCell();
	 tempfumiValuecell.addElement(tempfumiValue);
	 tempfumiValuecell.setBorder(0);
	 tempfumiValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 

	 Phrase appliedDoseRate=new Phrase("Applied Dose Rate",font9);
	 PdfPCell appliedDoseRateCell=new PdfPCell();
	 appliedDoseRateCell.addElement(appliedDoseRate);
	 appliedDoseRateCell.setBorder(0);
	 appliedDoseRateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 Phrase appliedDoseRateValue=null;
		 if(fumigation.getMinairtemp()!=null){
			 String a=3+"";
			 appliedDoseRateValue=new Phrase(fumigation.getAppliedDoseRate()+" gms/m3",font9);
		 }
	 else
	 {
		 appliedDoseRateValue=new Phrase("",font9);
	 }
	 
	 PdfPCell appliedDoseRateValuecell=new PdfPCell();
	 appliedDoseRateValuecell.addElement(appliedDoseRateValue);
	 appliedDoseRateValuecell.setBorder(0);
	 appliedDoseRateValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
	 
	 
	 
	 Phrase seperator=new Phrase(":",font9);
	 PdfPCell seperatorcell=new PdfPCell();
	 seperatorcell.addElement(seperator);
	 seperatorcell.setBorder(0);
	 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
	 
	 PdfPTable infotable=new PdfPTable(6);
//	 infotable.setSpacingAfter(5f);
	 infotable.setWidthPercentage(100f);
	 
	 try {
		infotable.setWidths(columnWidths3);
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
		 
		 if(fumigation.getDateoffumigation()!=null){
		 infotable.addCell(datefumicell);
		 infotable.addCell(seperatorcell);
		 infotable.addCell(datefumiValuecell);
		 }
		 
		 if(fumigation.getPlaceoffumigation()!=null){
		 infotable.addCell(placefumicell);
		 infotable.addCell(seperatorcell);
		 infotable.addCell(placefumiValuecell);
		 }
		 
		 if(fumigation.getDoseratefumigation()!=null){
		 infotable.addCell(dosafumicell);
		 infotable.addCell(seperatorcell);
		 infotable.addCell(dosafumiValuecell);
		 }
		
		 
		 
		 if(fumigation.getDurartionfumigation()!=null){
		 infotable.addCell(durarationfumicell);
		 infotable.addCell(seperatorcell);
		 infotable.addCell(durafumiValuecell);
		 }	
		 
		 System.out.println("*****************************************"+fumigation.getMinairtemp());
		 if(fumigation.getMinairtemp()!=null){
			 infotable.addCell(tempfumicell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(tempfumiValuecell);
		}
	 
		 System.out.println("****************************************"+fumigation.getAppliedDoseRate());
		 if(fumigation.getAppliedDoseRate()!=null){
			 infotable.addCell(appliedDoseRateCell);
			 infotable.addCell(seperatorcell);
			 infotable.addCell(appliedDoseRateValuecell);
		}
	 
		 PdfPCell infocell=new PdfPCell(infotable);
		 infocell.setBorder(0);
		 
		 
//	 PdfPTable maintable=new PdfPTable(1);
//	 maintable.setWidthPercentage(100f);
//	 maintable.addCell(infocell);
		 
//			PdfPCell maincell=new PdfPCell();
//			maincell.addElement(maintable);
	 
	PdfPTable titlepdftable=new PdfPTable(1);
	titlepdftable.setWidthPercentage(100);
//	titlepdftable.addCell(titlecell);
	
	PdfPCell titlePdfCell=new PdfPCell();
	titlePdfCell.addElement(titlepdftable);
	titlePdfCell.setBorder(0);
	
	
	PdfPTable parent=new PdfPTable(1);
	parent.setWidthPercentage(100);
	
	parent.addCell(titlePdfCell);
	parent.addCell(infocell);
	
	
	PdfPTable newparent=new PdfPTable(1);
	newparent.setWidthPercentage(100);
	
	PdfPCell parentCell=new PdfPCell();
	parentCell.addElement(parent);
	parentCell.setBorderWidthRight(0);
	parentCell.setBorderWidthLeft(0);
	parentCell.setBorderWidthTop(0);
	
	newparent.addCell(parentCell);
	
	try {
		document.add(newparent);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
	
}


private void CreateHeading() {

		
		
		
//		String	titlepdf="                      FUMIGATION CERTIFICATE";
		String	titlepdf="FUMIGATION CERTIFICATE";
		
//		Phrase titlephrase=new Phrase(titlepdf,font10bold);
//		Paragraph titlepdfpara=new Paragraph();
//		titlepdfpara.add(titlephrase);
//		titlepdfpara.setAlignment(Element.ALIGN_LEFT);
//		
//		PdfPCell titlecell=new PdfPCell(titlepdfpara);
//		titlecell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		titlecell.setBorder(0);
		
		
		Phrase titlephrase = new Phrase(titlepdf, font12boldul);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);
//		titlepdfpara.add(Chunk.NEWLINE);
		
		PdfPCell titlecell = new PdfPCell();
		titlecell.addElement(titlepdfpara);
//		titlecell.setBorderWidthRight(0);
//		titlecell.setBorderWidthLeft(0);
		titlecell.setBorder(0);
//		titlecell.addElement(Chunk.NEWLINE);
		
		
//		PdfPTable titlepdftable=new PdfPTable(1);
//		titlepdftable.setWidthPercentage(100);
//		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
//		titlepdftable.addCell(titlecell);
		
	
		
		//****************rohan remove this from following code as per new changes 
		
		Phrase treatnophrase=new Phrase(" ",font8);
		PdfPCell treatcell=new PdfPCell(treatnophrase);
//		treatcell.setBorderWidthRight(0);
//		treatcell.addElement(Chunk.NEWLINE);
		treatcell.setBorderWidthLeft(0);
		treatcell.setBorder(0);
		
//		Phrase dateissuephrase=new Phrase("dated : "+fmt.format(fumigation.getDateofissue()),font8);
//		PdfPCell dateissuecell=new PdfPCell(dateissuephrase);
//		dateissuecell.setBorder(0);
//		dateissuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase dateissuephrase=new Phrase(" ");
		PdfPCell dateissuecell=new PdfPCell(dateissuephrase);
//		dateissuecell.setBorderWidthLeft(0);
		dateissuecell.setBorder(0);
//		dateissuecell.addElement(Chunk.NEWLINE);
		dateissuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		
		/*********************code for article type 
		
		Phrase typename=null;
 		Phrase typevalue=null;
 		
//			e2.printStackTrace();
//		}
 		PdfPCell tymanecell=new PdfPCell();
 		tymanecell.setBorder(0);
 		Phrase nnn=new Phrase("");
 		PdfPCell blankcekk=new PdfPCell(nnn);
 		blankcekk.setBorder(0);
 		System.out.println("b4   for   ===== articletype=="+this.articletype.size());
		for(int i=0;i<this.articletype.size();i++){
				 
			System.out.println("in    for   ===== articletype=="+this.articletype.size());
					
					
					if(articletype.get(i).getArticlePrint().equalsIgnoreCase("YES") && articletype.get(i).getDocumentName().trim().equalsIgnoreCase("Fumigation")){
						
						
						System.out.println("cust docu name if in == "+articletype.get(i).getDocumentName());
						
						 typename = new Phrase(articletype.get(i).getArticleTypeName()+"                :  "+articletype.get(i).getArticleTypeValue(),font9);
//						 typevalue = new Phrase(articletype.get(i).getArticleTypeValue(),font8);
						 
						 
							tymanecell.addElement(typename);
							tymanecell.setBorder(0);
							
//							PdfPCell typevalcell=new PdfPCell();
//							typevalcell.addElement(typevalue);
//							typevalcell.setBorder(0);
							
//							artictable.addCell(tymanecell);
//							artictable.addCell(typevalcell);
							
//							artictable.addCell(blankcekk);
//							artictable.addCell(blankcekk);
					 }else{
						 tymanecell.addElement(nnn);
							tymanecell.setBorder(0);
					 }
					
		}
		
//		PdfPCell articcell=new PdfPCell(artictable);
// 		articcell.setBorder(0);
// 		
// 		custtable.addCell(articcell);
		
		
		
		
		***************************************/
		
		Paragraph blank =new Paragraph();
		blank.add(Chunk.NEWLINE);
		
		PdfPTable parent=new PdfPTable(3);
		parent.setWidthPercentage(100);
		
//		PdfPCell titlePdfCell=new PdfPCell();
//		titlePdfCell.addElement(titlepdftable);
//		titlePdfCell.setBorder(0);
		
		PdfPCell blankCell=new PdfPCell();
		blankCell.addElement(Chunk.NEWLINE);
		blankCell.setBorder(0);
		
		parent.addCell(treatcell);
		parent.addCell(titlecell);
		parent.addCell(dateissuecell);
		
		parent.addCell(blankCell);
		parent.addCell(blankCell);
		parent.addCell(blankCell);
		
		try {
			parent.setWidths(columnWidths);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		
		PdfPTable parent1=new PdfPTable(1);
		parent1.setWidthPercentage(100);
		
		PdfPCell parent1cell=new PdfPCell();
		parent1cell.addElement(parent);
		parent1cell.setBorderWidthLeft(0);
		parent1cell.setBorderWidthRight(0);
		
		parent1.addCell(parent1cell);
		
		
		try {
//			if(upcflag==true){
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			document.add(blank);
//			}
			document.add(parent1);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}
	
private void createLogo(Document doc, Company comp) {
		
		
		//********************logo for server ********************
	DocumentUpload document =comp.getLogo();

	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,725f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
		}
	
	//***********************for local **************************
	
//			try
//			{
//			Image image1=Image.getInstance("images/suma.jpg");
//			image1.scalePercent(20f);
//			image1.setAbsolutePosition(40f,725f);	
//			doc.add(image1);
//		 	}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//			}
			
	
	}
	
	public  void createCompanyAddress()
	{
		 Phrase companyName= new Phrase(comp.getBusinessUnitName(),font16bold);
	     Paragraph p =new Paragraph();
	     p.add(Chunk.NEWLINE);
	     p.add(companyName);
	     p.setAlignment(Element.ALIGN_CENTER);
	     PdfPCell companyNameCell=new PdfPCell();
	     companyNameCell.addElement(p);
	     companyNameCell.setBorder(0);
	     
		String addressline1="";
		
		if(comp.getAddress().getAddrLine2()!=null){
			addressline1=comp.getAddress().getAddrLine1()+" , "+comp.getAddress().getAddrLine2();
		}
		else{
			addressline1=comp.getAddress().getAddrLine1();
		}
		Phrase addressline=new Phrase(addressline1,font12);
		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		Phrase locality=null;
		if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition1");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((!comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 2");
			locality= new Phrase(comp.getAddress().getLandmark()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==false)){
			System.out.println("inside both null condition 3");
			locality= new Phrase(comp.getAddress().getLocality()+" , "+comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		else if((comp.getAddress().getLandmark().equals(""))&&(comp.getAddress().getLocality().equals("")==true)){
			System.out.println("inside both null condition 4");
			locality= new Phrase(comp.getAddress().getCity()+" , "
				      +comp.getAddress().getPin()+" , "+comp.getAddress().getState()+" , "+comp.getAddress().getCountry(),font12);
		}
		
		Paragraph localityPragraph=new Paragraph();
		localityPragraph.add(locality);
		localityPragraph.setAlignment(Element.ALIGN_CENTER);
		Phrase contactinfo=null;
		if(comp.getLandline()!=0){
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Phone: "+comp.getLandline()+"    Email1: "+comp.getEmail().trim(),font10);
		}
		else{
			contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+"    Email1: "+comp.getEmail().trim(),font10);
		}
		
		
		Phrase contactemail=new Phrase("Email2: "+comp.getPocEmail(),font10);
		Paragraph realmobpara1=new Paragraph();
		realmobpara1.add(contactemail);
		realmobpara1.setAlignment(Element.ALIGN_CENTER);
		
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
		PdfPCell localitycell=new PdfPCell();
		localitycell.addElement(localityPragraph);
		localitycell.setBorder(0);
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPCell contactcell1=new PdfPCell();
		contactcell1.addElement(realmobpara1);
		contactcell1.setBorder(0);
		
		PdfPTable companyDetails=new PdfPTable(1);
		companyDetails.setWidthPercentage(100);
//		companyDetails.setSpacingAfter(5f);
		companyDetails.addCell(companyNameCell);
		companyDetails.addCell(addresscell);
		companyDetails.addCell(localitycell);
		companyDetails.addCell(contactcell);
		companyDetails.addCell(contactcell1);
		
		PdfPTable parent=new PdfPTable(1);
		parent.setWidthPercentage(100);
		
		PdfPCell comapnyCell=new PdfPCell(companyDetails);
//		PdfPCell titlePdfCell=new PdfPCell(titlepdftable);
		comapnyCell.setBorderWidthTop(0);
		comapnyCell.setBorderWidthLeft(0);
		comapnyCell.setBorderWidthRight(0);
		parent.addCell(comapnyCell);
//		parent.addCell(titlePdfCell);
		
		try {
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	public void remainingAllDetails(){
		
		
		Phrase note1 = new Phrase("How was ths fumigation conducted?", font9);
		PdfPCell note1Cell = new PdfPCell();
		note1Cell.addElement(note1);
		note1Cell.setBorder(0);
		
		PdfPTable demoCrosstable = new PdfPTable(1);
		demoCrosstable.setWidthPercentage(25f);
		demoCrosstable.setSpacingBefore(2f);
		PdfPCell demoCrosstableCell = new PdfPCell();
		Phrase cross = new Phrase("X",font9);
		PdfPCell crossCell = new PdfPCell(cross);
		crossCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoCrosstable.addCell(crossCell);
		
		demoCrosstableCell.addElement(demoCrosstable);
		demoCrosstableCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoCrosstableCell.setBorder(0);
		
		
		
		
		PdfPTable demoPlanetable = new PdfPTable(1);
		demoPlanetable.setWidthPercentage(25f);
		demoPlanetable.setSpacingBefore(5f);
		Phrase plane = new Phrase(" ",font9);
		PdfPCell planeCell = new PdfPCell(plane);
		planeCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoPlanetable.addCell(planeCell);
		PdfPCell demoplanetableCell = new PdfPCell();
		
		demoplanetableCell.addElement(demoPlanetable);
		demoplanetableCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		demoplanetableCell.setBorder(0);
		
		
		
		
		
		
		
		Phrase stackUnderSheet=null;
		PdfPCell stackUnderSheetCell=null;
//		if(fumigation.getStackUnderSheet()==true){
			
		 stackUnderSheet = new Phrase("Stack Under Sheet", font9);
		 stackUnderSheetCell = new PdfPCell();
		stackUnderSheetCell.addElement(stackUnderSheet);
		stackUnderSheetCell.setBorder(0);
//		}
//		else{
//			 stackUnderSheet = new Phrase("Stack Under Sheet", font9);
//			 stackUnderSheetCell = new PdfPCell();
//			 stackUnderSheetCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			 stackUnderSheetCell.addElement(stackUnderSheet);
//			stackUnderSheetCell.setBorder(0);
//		}
		
		
		Phrase containerUnderSheet=null;
		PdfPCell containerUnderSheetCell=null;
//		if(fumigation.getContainerUnderSheet()==true){
		 containerUnderSheet = new Phrase("Container/s Under Sheet", font9);
		 containerUnderSheetCell = new PdfPCell();
		containerUnderSheetCell.addElement(containerUnderSheet);
		containerUnderSheetCell.setBorder(0);
//		}
//		else
//		{
//			 containerUnderSheet = new Phrase("[ ] Container/s Under Sheet", font9);
//			 containerUnderSheetCell = new PdfPCell();
//			containerUnderSheetCell.addElement(containerUnderSheet);
//			containerUnderSheetCell.setBorder(0);
//		}
//		
		
		Phrase permentChamber=null;
		PdfPCell permentChamberCell=null;
//		if(fumigation.getPermanentChamber()==true){
		 permentChamber = new Phrase("Permanent Chamber", font9);
		 permentChamberCell = new PdfPCell();
		permentChamberCell.addElement(permentChamber);
		permentChamberCell.setBorder(0);
//		}
//		else{
//			 permentChamber = new Phrase("[ ] Permanent Chamber", font9);
//			 permentChamberCell = new PdfPCell();
//			permentChamberCell.addElement(permentChamber);
//			permentChamberCell.setBorder(0);
//		}
		
		
		Phrase pressureTest = null;
		PdfPCell pressureTestCell= null;
//		if(fumigation.getPressureTestedContainer()==true){
		 pressureTest = new Phrase("Pressure tested container/s", font9);
		 pressureTestCell = new PdfPCell();
		pressureTestCell.addElement(pressureTest);
		pressureTestCell.setBorder(0);
//		}
//		else{
//			 pressureTest = new Phrase("[ ] Pressure tested container/s", font9);
//			 pressureTestCell = new PdfPCell();
//			pressureTestCell.addElement(pressureTest);
//			pressureTestCell.setBorder(0);
//		}
		
		
		Phrase blank = new Phrase(" ", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blank);
		blankCell.setBorder(0);
		
		PdfPTable fumigationTable = new PdfPTable(5);
		fumigationTable.setWidthPercentage(100f);
		 try {
			 fumigationTable.setWidths(columnWidths15);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		fumigationTable.addCell(note1Cell);
		if(fumigation.getStackUnderSheet()==true){
		fumigationTable.addCell(demoCrosstableCell);
		}
		else
		{
		fumigationTable.addCell(demoplanetableCell);
		}
		fumigationTable.addCell(stackUnderSheetCell);
		
		if(fumigation.getContainerUnderSheet()==true){
			fumigationTable.addCell(demoCrosstableCell);
			}
			else
			{
			fumigationTable.addCell(demoplanetableCell);
		}
		
		
		fumigationTable.addCell(containerUnderSheetCell);
		fumigationTable.addCell(blankCell);
		
		if(fumigation.getPermanentChamber()==true){
			fumigationTable.addCell(demoCrosstableCell);
			}
			else
			{
			fumigationTable.addCell(demoplanetableCell);
		}
		fumigationTable.addCell(permentChamberCell);
		
		
		if(fumigation.getPressureTestedContainer()==true){
			fumigationTable.addCell(demoCrosstableCell);
			}
			else
			{
			fumigationTable.addCell(demoplanetableCell);
		}
		
		fumigationTable.addCell(pressureTestCell);
		
		
		
		Phrase containerNo= new Phrase("Container number/s (where applicable) : ", font9);
		PdfPCell containerNoCell = new PdfPCell();
		containerNoCell.addElement(containerNo);
		containerNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		containerNoCell.setBorder(0);
		
		Phrase containerNOValue = new Phrase(fumigation.getContainerNo(),font9);
		PdfPCell containerNOValueCell = new PdfPCell();
		containerNOValueCell.addElement(containerNOValue);
		containerNOValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		containerNOValueCell.setBorder(0);
		
		PdfPTable containerNOtable = new PdfPTable(2);
		containerNOtable.setWidthPercentage(100f);
		try {
			containerNOtable.setWidths(new float[] { 25,50 });
		} catch (DocumentException e2) {
			e2.printStackTrace();
		}
//		try {
//			containerNOtable.setWidths(columnWidths8);
//			} catch (DocumentException e1) {
//				e1.printStackTrace();
//			}
//		containerNOtable.setSpacingAfter(10f);
		containerNOtable.addCell(containerNoCell);
		containerNOtable.addCell(containerNOValueCell);
		
		
		PdfPTable parentTable = new PdfPTable(1);
		parentTable.setWidthPercentage(100f);
		
		PdfPCell parentTableCell = new PdfPCell();
		parentTableCell.addElement(fumigationTable);
		parentTableCell.setBorderWidthBottom(0);
		
		parentTableCell.setBorderWidthRight(0);
		parentTableCell.setBorderWidthLeft(0);
		
		
		PdfPCell parentTableCell1 = new PdfPCell();
		parentTableCell1.addElement(containerNOtable);
		parentTableCell1.setBorderWidthTop(0);
		
		parentTableCell1.setBorderWidthRight(0);
		parentTableCell1.setBorderWidthLeft(0);
		
		parentTable.addCell(parentTableCell);
		parentTable.addCell(parentTableCell1);
		
		
		
		try {
			document.add(parentTable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		
		
		
		String note12= "Does the target of fumigation conform to the AQIS plastic wrapping, impervious surface ";
		String note123="and timber thickness requirements at the time of fumigation?";
		Phrase note12Phrase =  new Phrase(note12,font9);
		PdfPCell note12Cell =  new PdfPCell();
		note12Cell.addElement(note12Phrase);
		note12Cell.setBorder(0);
		
		
		Phrase note123Phrase =  new Phrase(note123,font9);
		PdfPCell note123Cell =  new PdfPCell();
		note123Cell.addElement(note123Phrase);
		note123Cell.setBorder(0);
		
		
		Phrase blank123 =  new Phrase(" ",font9);
		PdfPCell blank123Cell =  new PdfPCell();
		blank123Cell.addElement(blank123);
		blank123Cell.setBorder(0);
		
		note12Cell.setBorderWidthLeft(0);
		String answer=fumigation.getNote1().trim();
		Phrase noteValue = new Phrase(fumigation.getNote1(),font9);
		PdfPCell noteValueCell =  new PdfPCell();
		noteValueCell.addElement(noteValue);
		noteValueCell.setBorder(0);
		
		noteValueCell.setBorderWidthRight(0);
		
		
		//**********rohan changes here*************
		PdfPTable yesTable = new PdfPTable(5);
		yesTable.setWidthPercentage(100f);
//		yesTable.setSpacingBefore(10f);
//		yesTable.setSpacingAfter(10f);
		
		 try {
			 yesTable.setWidths(columnWidths16);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		
		Phrase yesPhrse =  new Phrase("YES",font9bold);
		PdfPCell yesCell = new PdfPCell(yesPhrse);
		yesCell.setBorder(0);
		PdfPTable smallyes = new PdfPTable(1);
		smallyes.addCell(yesCell);
		PdfPCell smalltableCell =new PdfPCell(smallyes);
		yesTable.addCell(smalltableCell);
		
		Phrase slashPhrse =  new Phrase("/",font9);
		PdfPCell slashCell = new PdfPCell(slashPhrse);
		slashCell.setBorder(0);
		yesTable.addCell(slashCell);
		
		Phrase naPhrse =  new Phrase("NA",font9);
		PdfPCell naCell = new PdfPCell(naPhrse);
		naCell.setBorder(0);
		yesTable.addCell(naCell);
		
		yesTable.addCell(slashCell);
		
		Phrase noPhrse =  new Phrase("NO",font9);
		PdfPCell noCell = new PdfPCell(noPhrse);
		noCell.setBorder(0);
		yesTable.addCell(noCell);
		
		
		PdfPCell yesTableCell = new PdfPCell(yesTable);
		yesTableCell.setBorder(0);
		
		
		
		
		PdfPTable naTable = new PdfPTable(5);
		naTable.setWidthPercentage(100f);
		
//		naTable.setSpacingBefore(10f);
//		naTable.setSpacingAfter(10f);
		 try {
			 naTable.setWidths(columnWidths16);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		
		
		Phrase yesPhrse1 =  new Phrase("YES",font9);
		PdfPCell yesCell1 = new PdfPCell(yesPhrse1);
		yesCell1.setBorder(0);
		naTable.addCell(yesCell1);
		
		Phrase slashPhrse1 =  new Phrase("/",font9);
		PdfPCell slashCell1 = new PdfPCell(slashPhrse1);
		slashCell1.setBorder(0);
		naTable.addCell(slashCell1);
		
		Phrase naPhrse1 =  new Phrase("NA",font9bold);
		PdfPCell naCell1 = new PdfPCell(naPhrse1);
		naTable.addCell(naCell1);
		
		naTable.addCell(slashCell);
		
		Phrase noPhrse1 =  new Phrase("NO",font9);
		PdfPCell noCell1 = new PdfPCell(noPhrse1);
		noCell1.setBorder(0);
		naTable.addCell(noCell1);
		
		
		PdfPCell naTableCell = new PdfPCell(naTable);
		naTableCell.setBorder(0);
		
		
		PdfPTable noTable = new PdfPTable(5);
		noTable.setWidthPercentage(100f);
		
//		noTable.setSpacingBefore(10f);
//		noTable.setSpacingAfter(10f);
		 try {
			 noTable.setWidths(columnWidths16);
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
		
		
		Phrase yesPhrse2 =  new Phrase("YES",font9);
		PdfPCell yesCell2 = new PdfPCell(yesPhrse2);
		yesCell2.setBorder(0);
		noTable.addCell(yesCell2);
		
		Phrase slashPhrse2 =  new Phrase("/",font9);
		PdfPCell slashCell2 = new PdfPCell(slashPhrse2);
		slashCell2.setBorder(0);
		noTable.addCell(slashCell2);
		
		Phrase naPhrse2 =  new Phrase("NA",font9);
		PdfPCell naCell2 = new PdfPCell(naPhrse2);
		naCell2.setBorder(0);
		noTable.addCell(naCell2);
		
		noTable.addCell(slashCell);
		
		Phrase noPhrse2 =  new Phrase("NO",font9bold);
		PdfPCell noCell2 = new PdfPCell(noPhrse2);
		noTable.addCell(noCell2);
		
		
		PdfPCell noTableCell = new PdfPCell(noTable);
		noTableCell.setBorder(0);
		
		
		
		
		
		//*****************************************
		
		PdfPTable notetable   = new PdfPTable(2);
		 try {
			 notetable.setWidths(new float[]{80,20});
			  } 
			  catch (DocumentException e1) {
				e1.printStackTrace();
			  }
		notetable.setWidthPercentage(100f);
		notetable.addCell(note12Cell); 
		if(answer.endsWith("YES")){
		notetable.addCell(yesTableCell);
		}
		else if(answer.endsWith("NO"))
		{
		notetable.addCell(noTableCell);	
		}
		else
		{
			notetable.addCell(naTableCell);
		}
		notetable.addCell(note123Cell); 
		notetable.addCell(blank123Cell);
		
		
		PdfPTable parentTable1 = new PdfPTable(1);
		parentTable1.setWidthPercentage(100f);
		
		PdfPCell parentTableCell11 = new PdfPCell();
		parentTableCell11.addElement(notetable);
//		parentTableCell11.setBorderWidthBottom(0);
		
		parentTableCell11.setBorderWidthLeft(0);
		parentTableCell11.setBorderWidthRight(0);
		
		parentTable1.addCell(parentTableCell11);
		
		
		try {
			document.add(parentTable1);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		Phrase string12=new Phrase("Ventilation  ",font9bold);
		
		String string1 = "Final TLV reading (ppm) : ";
		String string2="   (not required for Stack or Permanent Chamber fumigations)";

		Phrase string123= null;
		System.out.println("11111111111111111111111111111111111111111111111111111111111111111"+fumigation.getVentilation());
			if(fumigation.getVentilation()!=null){
			string123= new Phrase(string1+" "+fumigation.getVentilation()+" "+string2,font9);	
			}
			else{
				string123= new Phrase(" ",font9);	
			}
			
			
			PdfPCell stringCell0 = new PdfPCell();
			stringCell0.addElement(string12);
			stringCell0.setHorizontalAlignment(Element.ALIGN_LEFT);
			stringCell0.setBorder(0);
			
		PdfPCell stringCell = new PdfPCell();
		stringCell.addElement(string123);
		stringCell.setBorder(0);
		
		PdfPTable ventilationtable=new PdfPTable(2);
		ventilationtable.setWidthPercentage(100f);
		try {
			ventilationtable.setWidths(new float[]{12,88});
			  } 
			  catch (DocumentException e1) {
				e1.printStackTrace();
			  }
//		
			
		ventilationtable.addCell(stringCell0);
		ventilationtable.addCell(stringCell);
			
			
			PdfPTable parentTable11 = new PdfPTable(1);
			parentTable11.setWidthPercentage(100f);
			
			PdfPCell parentTableCell111 = new PdfPCell();
			parentTableCell111.addElement(ventilationtable);
			
			parentTableCell111.setBorderWidthLeft(0);
			parentTableCell111.setBorderWidthRight(0);
			
			parentTable11.addCell(parentTableCell111);
			
			try {
				document.add(parentTable11);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
		
			
			Phrase declaration = new  Phrase("DECLARATION",font10bold);
			PdfPCell declarationcell = new PdfPCell(declaration);
			declarationcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			declarationcell.setBorderWidthLeft(0);
			declarationcell.setBorderWidthRight(0);
			
			Phrase declarationValue = new  Phrase(fumigation.getDeclaration(),font9);
			PdfPCell declarationValueCell = new PdfPCell(declarationValue);
			declarationValueCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			declarationValueCell.setBorderWidthTop(0);
			
			declarationValueCell.setBorderWidthLeft(0);
			declarationValueCell.setBorderWidthRight(0);
			
			PdfPTable declarationtable = new PdfPTable(1);
			declarationtable.setWidthPercentage(100f);
			declarationtable.addCell(declarationcell);
			declarationtable.addCell(declarationValueCell);
//			declarationtable.setSpacingAfter(10f);
			
			
			
			
			
			
			
			
			Phrase additionaldeclaration = new  Phrase("ADDITIONAL DECLARATION",font10bold);
			PdfPCell additionaldeclarationCell = new PdfPCell(additionaldeclaration);
			additionaldeclarationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			additionaldeclarationCell.setBorderWidthLeft(0);
			additionaldeclarationCell.setBorderWidthRight(0);

			Phrase additionaldeclarationValue = new  Phrase(fumigation.getAdditionaldeclaration(),font9bold);
			PdfPCell additionaldeclarationValueCell = new PdfPCell(additionaldeclarationValue);
			additionaldeclarationValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			additionaldeclarationValueCell.setBorderWidthLeft(0);
			additionaldeclarationValueCell.setBorderWidthRight(0);
			additionaldeclarationValueCell.setBorderWidthBottom(0);
			
			PdfPTable aaditionaldeclarationtable = new PdfPTable(1);
			aaditionaldeclarationtable.setWidthPercentage(100f);
			aaditionaldeclarationtable.addCell(additionaldeclarationCell);
			aaditionaldeclarationtable.addCell(additionaldeclarationValueCell);
			
			
			
			
			
		try {
			document.add(declarationtable);
			document.add(aaditionaldeclarationtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}
	
}
