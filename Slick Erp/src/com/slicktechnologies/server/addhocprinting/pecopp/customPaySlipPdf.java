package com.slicktechnologies.server.addhocprinting.pecopp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.addhocprinting.ServiceInvoicePdf;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Vendor;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class customPaySlipPdf {
	

	
	CustomerPayment custpay;
	Customer cust;
	Company comp;
	Branch branchDt = null;
	Vendor vendor;
	Branch branch;
	public  Document document;
	public GenricServiceImpl impl;
	private  Font font16boldul,font12bold,font8bold,font8,font12boldul,font12,font10bold,font10,font14bold,font9;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt1= new SimpleDateFormat("MMM-yyyy");
	  Logger logger = Logger.getLogger("NameOfYourLogger");

	  float[] columnWidths2={1.2f,0.2f,1.5f,1.4f,0.2f,1.5f};
	  float[] columnWidths={1.2f,0.4f,1.5f,1.2f,0.4f,1.5f};
	  
	  float[] columnWidths3 = {3f, 0.5f, 2f, 2.5f,0.5f,2f,2.5f,0.5f,3.5f};
	  
	  float[] relativeWidths ={0.8f,9.2f};
	  
	DecimalFormat df=new DecimalFormat("0.00");
	ProcessConfiguration processConfig;
	boolean PrintPaySlipOnSamePage=false;
	 boolean paytermsflag=false;
	 boolean upcflag;
	 boolean disclaimerflag=false;
	 /**
	  *  nidhi
	  *  
	  */
	 CompanyPayment comppayment;
	 private PdfPCell imageSignCell;
	 /**
	  * end
	  */
	 
	 
	 public customPaySlipPdf()
	{
		font16boldul = new Font(Font.FontFamily.HELVETICA  , 16, Font.BOLD);
		new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12bold= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA,8,Font.BOLD);
		font8= new Font(Font.FontFamily.HELVETICA,8);
		font9= new Font(Font.FontFamily.HELVETICA,9);
		font12boldul= new Font(Font.FontFamily.HELVETICA,12,Font.BOLD);
		font12=new Font(Font.FontFamily.HELVETICA,12);
		font10bold= new Font(Font.FontFamily.HELVETICA,10,Font.BOLD);
		font10=new Font(Font.FontFamily.HELVETICA,10,Font.NORMAL);
		font14bold = new Font(Font.FontFamily.HELVETICA  , 14, Font.BOLD);
		impl= new GenricServiceImpl();
		/**
		 * Date 09/06/2017 
		 *    ajinkya added this as suggested by Anil sir for Payment Date issue
		 */
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		fmt1.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		/**
		 * End Here 
		 */
		
		
	}

	public void setCustomerPaySlip(long count)
	{
		custpay=ofy().load().type(CustomerPayment.class).id(count).now();
		
		//Load Customer
		if(custpay.getCompanyId()!=null)
			 cust=ofy().load().type(Customer.class).filter("companyId", custpay.getCompanyId()).filter("count",custpay.getCustomerId()).first().now();
		
		//Load Vendor
		if(custpay.getCompanyId()!=null)
			 vendor=ofy().load().type(Vendor.class).filter("companyId", custpay.getCompanyId()).filter("count",custpay.getCustomerId()).first().now();
		
		//Load Company
		if(custpay.getCompanyId()!=null)
			 comp=ofy().load().type(Company.class).filter("companyId",custpay.getCompanyId()).first().now();
		else
			comp=ofy().load().type(Company.class).first().now();
		
		//Load Branch
		if(custpay.getCompanyId()==null)
			branch=ofy().load().type(Branch.class).filter("companyId",custpay.getCompanyId()).first().now();
		else
			branch=ofy().load().type(Branch.class).filter("companyId",custpay.getCompanyId()).filter("buisnessUnitName",custpay.getBranch().trim()).first().now();

		
		if(custpay.getCompanyId()!=null){
			
			System.out.println("inside condition");
			
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "CustomerPayment").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					System.out.println("xxxxxxxxxxxxx"+processConfig.getProcessList().get(k).getProcessType());
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("PrintPaySlipOnSamePage")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						System.out.println("value true condition");
						
						PrintPaySlipOnSamePage=true;
					}
				}
			}
		}
		/**
		 * nidhi
		 * 06-04-2018
		 * for branch as a company process configration
		 * 
		 */
		logger.log(Level.SEVERE,"get for check configration --");
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(custpay !=null && custpay.getBranch() != null && custpay.getBranch().trim().length()>0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",custpay.getCompanyId()).filter("buisnessUnitName", custpay.getBranch()).first().now();
				
				
				if(branchDt != null && branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
				
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						 comppayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", custpay.getCompanyId()).first()
								.now();
						
						if(comppayment != null){
							comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
						}
						
					}
					
					
				}
			}
		}
		
		/**
		 * end
		 */
		
		
	}
	
	
	//*******************************PAYMENT EMIAL LOGIC (ROHAN)********************************** 
	
	public  void createPdfForEmail(CustomerPayment custpay,Company companyEntity,Customer custEntity ) 
	{
		this.custpay=custpay;
		this.comp=companyEntity;
		this.cust=custEntity;
//		this.con=contractEntity;
//		this.billEntity=billingEntity;
//		this.salesProd=invoiceentity.getSalesOrderProducts();
//		this.contractTaxesLis=con.getProductTaxes();
//		this.contractChargesLis=con.getProductCharges();
//		this.billingTaxesLis=invoiceentity.getBillingTaxes();
//		this.billingChargesLis=invoiceentity.getBillingOtherCharges();
//		this.payTermsLis=contractEntity.getPaymentTermsList();
		
		if(custpay.getCompanyId()!=null){
			this.processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceInvoicePayTerm")&&processConfig.getProcessList().get(k).isStatus()==true){
						this.paytermsflag=true;
					}
					else{
						this.paytermsflag=false;
					}
				}
			}
		}
		
/************************************Letter Head Flag*******************************/
		
		if(custpay.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.upcflag=true;
					}
				}
			}
		}
		
		
		/**********************************Disclaimer Flag******************************************/
		
		if(custpay.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.disclaimerflag=true;
					}
				}
			}
		}
		
//		productDetailsForEmail();
//		 articletype = new ArrayList<ArticleType>();
//		 if(cust.getArticleTypeDetails().size()!=0){
//				articletype.addAll(cust.getArticleTypeDetails());
//			}
//			if(comp.getArticleTypeDetails().size()!=0){
//				articletype.addAll(comp.getArticleTypeDetails());
//			}
//		 arrPayTerms=invoiceentity.getArrPayTerms();
//		 
//		
//		try {
//			createLogo(document,comp);
//			createCompanyAddress();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		
//	  try {
//			createCustInfo1();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		createProductTable();
//		termsConditionsInfo();
//		addFooter();
	
		
		
		System.out.println("flag value ===="+PrintPaySlipOnSamePage);
		if(PrintPaySlipOnSamePage==true){
//		createLogo1(document,comp);
		Createblank();
		if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
			createCompanyHedding2();
		}else{
			createCompanyHedding1();
		}
		
		createPaymentDetails1();
//		createRemarkTable();
		footerInfo1();
		
		createPaySlipDetails1();
		}
		else{
					
				if(comp.getUploadHeader()!=null){
					createSpcingForHeading();
					createCompanyAddressDetails();
				createCompanyNameAsHeader(document,comp);
				}
				else
				{
					createLogo(document,comp);
					Createblank();
					if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
						createCompanyHedding4();
					}else{
						createCompanyHedding3();
					}
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
				
			
			createPaymentDetails();
			createRemarkTable();
			footerInfo();
		}
	
	}

	/** 1-11-2017 sagar sore **/
	public  void createPdfForEmailGST(CustomerPayment custpay,Company companyEntity,Customer custEntity, Document document) 
	{
		this.custpay=custpay;
		this.comp=companyEntity;
		this.cust=custEntity;
		if(custpay.getCompanyId()!=null){
			this.processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("ServiceInvoicePayTerm")&&processConfig.getProcessList().get(k).isStatus()==true){
						this.paytermsflag=true;
					}
					else{
						this.paytermsflag=false;
					}
				}
			}
		}
		
/************************************Letter Head Flag*******************************/
		
		if(custpay.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("CompanyAsLetterHead")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.upcflag=true;
					}
				}
			}
		}
		
		
		/**********************************Disclaimer Flag******************************************/
		
		if(custpay.getCompanyId()!=null){
			processConfig=ofy().load().type(ProcessConfiguration.class).filter("companyId", custpay.getCompanyId()).filter("processName", "Invoice").filter("configStatus", true).first().now();
			if(processConfig!=null){
				for(int k=0;k<processConfig.getProcessList().size();k++)
				{
					if(processConfig.getProcessList().get(k).getProcessType().trim().equalsIgnoreCase("DisclaimerText")&&processConfig.getProcessList().get(k).isStatus()==true)
					{
						this.disclaimerflag=true;
					}
				}
			}
		}
			
		System.out.println("flag value ===="+PrintPaySlipOnSamePage);
	    this.createPdf("plane");	
	}
	
	
	//*********************************CHANGES ENDS HERE *********************************
	
	private void createRemarkTable() {
		
		if(custpay.getComment()!=null)
		{
			PdfPTable table = new PdfPTable(2);
			table.setWidthPercentage(100f);
			try {
				table.setWidths(relativeWidths);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			Phrase comment = new Phrase("Remark :",font8bold);
			PdfPCell commentCell = new PdfPCell(comment);
			commentCell.setBorder(0);

			Phrase commentValue = new Phrase(custpay.getComment(),font8);
			PdfPCell commentValueCell = new PdfPCell(commentValue);
			commentValueCell.setBorder(0);
			
			table.addCell(commentCell);
			table.addCell(commentValueCell);
			
			PdfPCell tableCell = new PdfPCell(table);
			
			PdfPTable parentTable = new PdfPTable(1);
			parentTable.setWidthPercentage(100f);
			parentTable.addCell(tableCell);
			
			
			try {
				document.add(parentTable);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public  void createPdf(String preprintStatus) 
	{
		
		
		
		System.out.println("flag value ===="+PrintPaySlipOnSamePage);
		if(PrintPaySlipOnSamePage==true){
		createTitle();
		createLogo1(document,comp);
		Createblank();
		if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
			createCompanyHedding2();
		}else{
			createCompanyHedding1();
		}
		
		createPaymentDetails1();
		footerInfo1();
		
		createPaySlipDetails1();
		}
		else{
			
			createTitle();
			if(preprintStatus.equals("plane")){
				
				System.out.println("contract inside plane");
				
				createLogo(document,comp);
				Createblank();
				if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
					createCompanyHedding4();
				}else{
					createCompanyHedding3();
				}
				
				
			}else{
				
				if(preprintStatus.equals("yes")){
					
					System.out.println("inside prit yes");
					createSpcingForHeading();
					createCompanyAddressDetails();
				}
				
				if(preprintStatus.equals("no")){
				
				System.out.println("inside prit no");
				
				if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
				if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
				createSpcingForHeading();
				createCompanyAddressDetails();
				}
			}
			
			createPaymentDetails();
//			createRemarkTable();
//			footerInfo();
			footerInfo1();
		}
    }
	
	private void createTitle() {
		System.out.println("inside title");
		Phrase titlephrase = new Phrase("Payment Receipt", font14bold);
		Paragraph titlepdfpara = new Paragraph();
		titlepdfpara.add(titlephrase);
		titlepdfpara.setAlignment(Element.ALIGN_CENTER);

//		PdfPCell titlecell = new PdfPCell();
//		titlecell.addElement(titlepdfpara);
//		titlecell.setBorder(0);
		
		try {
			document.add(titlepdfpara);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

	private void createCompanyAddressDetails() {
		
		//************************changes made by rohan	for M/s **************** 
//		String tosir= null;
//		if(cust.isCompany()==true){
//			
//			
//		 tosir="To, M/S";
//		}
//		else{
//			tosir="To,";
//		}
//		//*******************changes ends here ********************sss*************
//		
//		
//		String custName="";
//		if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//			custName=cust.getCompanyName().trim();
//		}
//		else{
//			custName=cust.getFullname().trim();
//		}
		
		String tosir= null;
		String custName="";
		//   rohan modified this code for printing printable name 
		
		if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
			custName=cust.getCustPrintableName().trim();
		}
		else
		{
		
			if(cust.isCompany()==true){
				
				
			 tosir="To, M/S";
			}
			else{
				tosir="To,";
			}
			
			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
				custName=cust.getCompanyName().trim();
			}
			else{
				custName=cust.getFullname().trim();
			}
		}
		
	    Paragraph fullname =new Paragraph();
	    fullname.add(Chunk.NEWLINE);
	    if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
		{
	    	fullname.add(custName);
		}
	    else
	    {
	    	fullname.add(tosir+"   "+custName);
	    }
	    fullname.setFont(font10bold);
	    
			PdfPCell custnamecell=new PdfPCell();
			custnamecell.addElement(fullname);
			custnamecell.setBorder(0);
		
		
		 String addresslin1=" ";
		  if(!cust.getAdress().getAddrLine1().equals("")){
			  addresslin1=cust.getAdress().getAddrLine1().trim()+" , ";
		  }
		  
		  String addressline2=" ";
		  
		  if(!cust.getAdress().getAddrLine2().equals("")){
			  addressline2 =cust.getAdress().getAddrLine2().trim()+" , ";
		  }
		 
		  String landmark=" ";
		  if(!cust.getAdress().getLandmark().equals("")){
			  landmark=cust.getAdress().getLandmark()+" , ";
		  }
		  
		  String locality=" ";
		  if(!cust.getAdress().getLocality().equals("")){
			  locality=cust.getAdress().getLocality()+" , ";
		  }
		  
//		  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality,font9);
//		  PdfPCell fromaddrescell1=new PdfPCell();
//		  fromaddrescell1.addElement(addressline1);
//		  fromaddrescell1.setBorder(0);
		  
		  
		  String city=" ";
		  if(!cust.getAdress().getCity().equals("")){
			  city=cust.getAdress().getCity()+",";
		  }
		  String state=" ";
		  if(!cust.getAdress().getState().equals("")){
			  state=cust.getAdress().getState()+"-";
		  }
		  String pin=" ";
		  if(cust.getAdress().getPin()!=0){
			  pin=cust.getAdress().getPin()+",";
		  }
		  String country=" ";
		  if(cust.getAdress().getCountry()!=null){
			  country=cust.getAdress().getCountry();
		  }
		  
		  
		  
		  Phrase addressline1=new Phrase(addresslin1+addressline2+landmark+locality+city+state+pin+country,font9);
		  PdfPCell fromaddrescell1=new PdfPCell();
		  fromaddrescell1.addElement(addressline1);
		  fromaddrescell1.setBorder(0);
		  
//		  Phrase blank=new Phrase(" ",font9);
//		  PdfPCell blankCell=new PdfPCell();
//		  fromaddrescell1.addElement(addressline1);
//		  fromaddrescell1.setBorder(0);
		  
		  Phrase namePhrase= new Phrase("Customer Details",font10bold); 
		  PdfPCell headcell = new PdfPCell(namePhrase);
		  headcell.setBorder(0);
		  headcell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  
		  //*******************rohan added cust details *****************
		  Phrase email= new Phrase("Email :"+cust.getEmail(),font9); 
		  PdfPCell emailCell = new PdfPCell(email);
		  emailCell.setBorder(0);
		  emailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  
		  Phrase cellNo= new Phrase("Cell : "+cust.getCellNumber1(),font9); 
		  PdfPCell cellNoCell = new PdfPCell(cellNo);
		  cellNoCell.setBorder(0);
		  cellNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		  /**
		   * Updated By: Viraj
		   * Date: 15-03-2019
		   * Description: To add orderID, orderDate, InvoiceID, Invoice Date
		   */
		  String startdtValue=null;
		  if(custpay.getContractCount()!=null) {
			 startdtValue=custpay.getContractCount()+"";
		  }
		  else {
			  startdtValue="";
		  }
		  Phrase orderId = new Phrase("Order ID: "+ startdtValue,font10);
		  PdfPCell orderIdCell = new PdfPCell(orderId);
		  orderIdCell.setBorder(0);
		  
		  String enddtValue=null;
		  if(custpay.getOrderCreationDate()!=null) {
			  enddtValue=fmt.format(custpay.getOrderCreationDate());
		  }
		  else {
			  enddtValue="";
		  }
		  Phrase orderDate = new Phrase("Order Date: "+ enddtValue,font10);
		  PdfPCell orderDateCell = new PdfPCell(orderDate);
		  orderDateCell.setBorder(0);
		  
		  int refNovalueValue=0;
		  if(custpay.getInvoiceCount()!=0) {
			  refNovalueValue=custpay.getInvoiceCount();
		  }
		  Phrase invoiceId = new Phrase("Invoice ID: "+ refNovalueValue,font10);
		  PdfPCell invoiceIdCell = new PdfPCell(invoiceId);
		  invoiceIdCell.setBorder(0);
		  
		  String refDtValue=null;
		  if(custpay.getInvoiceDate()!=null) {
			  refDtValue=fmt.format(custpay.getInvoiceDate());
		  }
		  else {
			  refDtValue="";
		  }
		  Phrase invoiceDate = new Phrase("Invoice Date: "+ refDtValue,font10);
		  PdfPCell invoiceDateCell = new PdfPCell(invoiceDate);
		  invoiceDateCell.setBorder(0);
		  
		  PdfPTable addtable1 = new PdfPTable(2);
		  addtable1.setWidthPercentage(100f);
		  addtable1.addCell(orderIdCell);
		  addtable1.addCell(orderDateCell);
		  addtable1.addCell(invoiceIdCell);
		  addtable1.addCell(invoiceDateCell);
		  /** Ends **/
		  //***********************changes ends here *********************
		  
		  PdfPTable addtable = new PdfPTable(1);
		  addtable.setWidthPercentage(100f);
		  addtable.addCell(headcell);
		  addtable.addCell(custnamecell);
		  addtable.addCell(fromaddrescell1);
		  addtable.addCell(emailCell);
		  addtable.addCell(cellNoCell);
		  
		  PdfPCell tablecell = new PdfPCell();
		  tablecell.addElement(addtable);
		  
		  /**
		   * Updated by: Viraj
		   * Date: 15-03-2019
		   * Description: To add orderID, orderDate, InvoiceID, Invoice Date
		   */
		  PdfPCell tablecell1 = new PdfPCell();
		  tablecell1.addElement(addtable1);
		  /** Ends **/
		  
		  /**Date 4-10-2020 by Amol added a payment ACKNOWLEDGMENT .payment count and payment date
		   * raised by Ashwini Bhagwat.
		   */
		  
		    String title="";
			title="Details of your payment";
			
			Phrase titlephrase =new Phrase(title,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			titlepdfpara.add(Chunk.NEWLINE);
			PdfPCell titlepdfcell=new PdfPCell();
			titlepdfcell.addElement(titlepdfpara);
//			titlepdfcell.setBorderWidthRight(0);
//			titlepdfcell.setBorderWidthLeft(0);
			titlepdfcell.setBorder(0);
			
			String paymentDate=null;
			
			if(custpay.getPaymentDate()!=null){
				paymentDate=fmt.format(custpay.getPaymentDate())+"";
			}else{
				paymentDate="";
			}
			Phrase dateofpdf=new Phrase(paymentDate,font14bold);
			Paragraph creatndatepara=new Paragraph();
			creatndatepara.add(dateofpdf);
			creatndatepara.setAlignment(Element.ALIGN_RIGHT);
			creatndatepara.add(Chunk.NEWLINE);
			PdfPCell creationcell=new PdfPCell();
			creationcell.addElement(creatndatepara);
			creationcell.setBorderWidthLeft(0);
			creationcell.setBorder(0);
			
			String customerpaymentID="";
			if(custpay.getCount()!=0){
				customerpaymentID=custpay.getCount()+"";
			}else{
				customerpaymentID="";
			}
			Phrase idphrase=new Phrase(customerpaymentID,font14bold);
			Paragraph idofpdfpara=new Paragraph();
			idofpdfpara.add(idphrase);
			idofpdfpara.setAlignment(Element.ALIGN_LEFT);
			idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell countcell=new PdfPCell();
			countcell.addElement(idofpdfpara);
			countcell.setBorderWidthRight(0);
			countcell.setBorder(0);
		
		PdfPTable TitleTable=new PdfPTable(3);
		TitleTable.setWidthPercentage(100);
		TitleTable.addCell(countcell);
		TitleTable.addCell(titlepdfcell);
		TitleTable.addCell(creationcell);
		  
		 PdfPCell titleCell = new PdfPCell();
		 titleCell.addElement(TitleTable);
		 titleCell.setColspan(2);
		  
		  /**
		   * end by Amol
		   */
		  
		  
		  
		  PdfPTable parentaddtable = new PdfPTable(2);
		  parentaddtable.setWidthPercentage(100f);
		  parentaddtable.addCell(tablecell);
		  parentaddtable.addCell(tablecell1);
		  parentaddtable.addCell(titleCell);
		  try {
			document.add(parentaddtable);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		}
	
	private void createCompanyNameAsHeader(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}		

private void createSpcingForHeading() {
	
	/*
	 * Commented by Ashwini
	 */

//Phrase spacing = new Phrase(Chunk.NEWLINE);
//Paragraph spacingPara = new Paragraph(spacing);
//try {
//	document.add(spacingPara);
//	document.add(spacingPara);
//	document.add(spacingPara);
//	document.add(spacingPara);
//} catch (DocumentException e) {
//	e.printStackTrace();
//}
	
	/*
	 * Date:30/07/2018
	 * Developer:Ashwini
	 * Des:To increase heading space while printing payment details.
	 */
	
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Header","INCREASEUPPERHEADER" , cust.getCompanyId())){
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
			
	}
	else{
		Phrase spacing = new Phrase(Chunk.NEWLINE);
		Paragraph spacingPara = new Paragraph(spacing);
		
		try {
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		document.add(spacingPara);
		} catch (DocumentException e) {
		e.printStackTrace();
		}
}

	/*
	 * End by Ashwini
	 */
}
	
	
		
		private void createPaySlipDetails1(){
			
			createLogo1(document,comp);
//			Createblank();
			if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				createCompanyHedding2();
			}else{
				createCompanyHedding1();
			}
			
			createPaymentDetails1();
			footerInfo1();
		
		}

		private void createCompanyHedding2() {


			Phrase companyName= new Phrase("                  "+comp.getBusinessUnitName(),font12boldul);
		    Paragraph p =new Paragraph();
		    p.add(Chunk.NEWLINE);
		    p.add(companyName);
		    
		    PdfPCell companyHeadingCell=new PdfPCell();
		    companyHeadingCell.setBorder(0);
		    companyHeadingCell.addElement(p);
		    
		    Phrase branchphrase=null;
		    PdfPCell companybranchCell=null;
		    if(!custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
		    {
			    String branch1="Branch Name";
			    branchphrase=new Phrase(branch1+" - "+branch.getBusinessUnitName(),font10);
			    companybranchCell=new PdfPCell();
			    companybranchCell.setBorder(0);
			    companybranchCell.addElement(branchphrase);
		    }
		    
		    
		    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
			Phrase adressline2=null;
			if(comp.getAddress().getAddrLine2()!=null)
			{
				adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
			}else{
				adressline2= new Phrase("",font10);
			}
			
			Phrase landmark=null;
			if(comp.getAddress().getLandmark()!=null)
			{
				landmark= new Phrase(comp.getAddress().getLandmark(),font10);
			}
			else
			{
				landmark= new Phrase("",font12);
			}
			
			
			Phrase locality=null;
			if(comp.getAddress().getLocality()!=null)
			{
				locality= new Phrase(comp.getAddress().getLocality(),font10);
			}
			else
			{
				locality= new Phrase("",font12);
			}
			Phrase	city=new Phrase(comp.getAddress().getCity()+" - "+comp.getAddress().getPin(),font10);
			
			
			PdfPCell citycell= new PdfPCell();
			citycell.addElement(city);
			citycell.setBorder(0);
			PdfPCell addressline1cell=new PdfPCell();
			PdfPCell addressline2cell=new PdfPCell();
			addressline1cell.addElement(adressline1);
			addressline1cell.setBorder(0);
				
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
			
			PdfPCell landmarkcell=new PdfPCell();
			landmarkcell.addElement(landmark);
			landmarkcell.setBorder(0);
			
			PdfPCell localitycell=new PdfPCell();
			localitycell.addElement(locality);
			localitycell.setBorder(0);
			
			String contactinfo="Cell: "+comp.getContact().get(0).getCellNo1();
			if(comp.getContact().get(0).getCellNo2()!=0)
				contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
			if(comp.getContact().get(0).getLandline()!=0){
				contactinfo=contactinfo+"     "+" Tel: "+comp.getContact().get(0).getLandline();
			}
			
			Phrase contactnos=new Phrase(contactinfo,font9);
			Phrase email= new Phrase("Email: "+comp.getContact().get(0).getEmail(),font9);
			
			PdfPCell contactcell=new PdfPCell();
			contactcell.addElement(contactnos);
			contactcell.setBorder(0);
			PdfPCell emailcell=new PdfPCell();
			emailcell.setBorder(0);
			emailcell.addElement(email);
			
			
			PdfPTable companytable=new PdfPTable(1);
			companytable.addCell(companyHeadingCell);
			if(!custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				companytable.addCell(companybranchCell);
			}
			companytable.addCell(addressline1cell);
			if(!comp.getAddress().getAddrLine2().equals(""))
			{
				companytable.addCell(addressline2cell);
			}
			if(!comp.getAddress().getLandmark().equals("")){
				companytable.addCell(landmarkcell);
			}
			if(!comp.getAddress().getLocality().equals("")){
				companytable.addCell(localitycell);
			}
			companytable.addCell(citycell);
			companytable.addCell(contactcell);
			companytable.addCell(emailcell);
			companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
			companytable.setWidthPercentage(100);
			
			
			/**
			 * Customer Info
			 */
//			String tosir="To, M/S";
			//************************changes made by rohan	for M/s **************** 
			String tosir= null;
			if(cust.isCompany()==true){
				
				
			 tosir="To, M/S";
			}
			else{
				tosir="To,";
			}
			//*******************changes ends here ********************sss*************
			
			
			
			String custName="";
//			if(vendor.isCompany()==true&&cust.getCompanyName()!=null){
//				custName=cust.getCompanyName().trim();
//			}
//			else{
				custName=vendor.getVendorName().trim();
//			}
			
			Phrase tosirphrase= new Phrase(tosir,font12bold);
			
			Phrase customername= new Phrase(custName,font12boldul);
		    Paragraph fullname =new Paragraph();
		    fullname.add(Chunk.NEWLINE);
		    fullname.add(tosir+"   "+custName);
		    fullname.setFont(font12bold);
		    
				PdfPCell custnamecell=new PdfPCell();
				custnamecell.addElement(fullname);
				custnamecell.setBorder(0);
				 
				Phrase customeradress= new Phrase("                   "+vendor.getPrimaryAddress().getAddrLine1(),font10);
			Phrase customeradress2=null;
			if(vendor.getPrimaryAddress().getAddrLine2()!=null){
			   customeradress2= new Phrase("                   "+vendor.getPrimaryAddress().getAddrLine2(),font10);
			}
			
			PdfPCell custaddress1=new PdfPCell();
			custaddress1.addElement(customeradress);
			custaddress1.setBorder(0);
			
			PdfPCell custaddress2=new PdfPCell();
			if(vendor.getPrimaryAddress().getAddrLine2()!=null){
				custaddress2.addElement(customeradress2);
				custaddress2.setBorder(0);
				}
			
			
			String custlandmark2="";
			Phrase landmar2=null;
			
			String  localit2="";
			Phrase custlocality2= null;
			
			if(vendor.getPrimaryAddress().getLandmark()!=null)
			{
				custlandmark2 = vendor.getPrimaryAddress().getLandmark();
				landmar2=new Phrase("                   "+custlandmark2,font10);
			}
			
			
			if(vendor.getPrimaryAddress().getLocality()!=null){
				localit2=vendor.getPrimaryAddress().getLocality();
				custlocality2=new Phrase("                   "+localit2,font10);
				}
			
			Phrase cityState2=new Phrase("                   "+vendor.getPrimaryAddress().getCity()
					+" - "+vendor.getPrimaryAddress().getPin()
//					+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
					,font10);
			
			PdfPCell custlandcell2=new PdfPCell();
			custlandcell2.addElement(landmar2);
			custlandcell2.setBorder(0);
			
			PdfPCell custlocalitycell2=new PdfPCell();
			custlocalitycell2.addElement(custlocality2);
			custlocalitycell2.setBorder(0);
			
			PdfPCell custcitycell2=new PdfPCell();
			custcitycell2.addElement(cityState2);
			custcitycell2.setBorder(0); 
//			custcitycell2.addElement(Chunk.NEWLINE);
				 
			Phrase custcontact=new Phrase("Mobile: "+vendor.getCellNumber1()+"",font9);
				PdfPCell custcontactcell=new PdfPCell();
				custcontactcell.addElement(custcontact);
				custcontactcell.setBorder(0);
				
				Phrase custemail=new Phrase("email: "+vendor.getEmail(),font9);
				PdfPCell custemailcell=new PdfPCell();
				custemailcell.addElement(custemail);
				custemailcell.setBorder(0);
				
					
				
				 PdfPCell infotablecell=null;
				 PdfPCell infotablecell1=null;
					 
				 Phrase startdt=new Phrase("Order ID",font9);
				 PdfPCell startdtcell=new PdfPCell();
				 startdtcell.addElement(startdt);
				 startdtcell.setBorder(0);
				 startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase startdtValue=null;
					 if(custpay.getContractCount()!=null){
						 startdtValue=new Phrase(custpay.getContractCount()+"",font9);
					 }
				 else
				 {
					  startdtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell startdtValuecell=new PdfPCell();
				 startdtValuecell.addElement(startdtValue);
				 startdtValuecell.setBorder(0);
				 startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase enddt=new Phrase("Order Date",font9);
				 PdfPCell enddtcell=new PdfPCell();
				 enddtcell.addElement(enddt);
				 enddtcell.setBorder(0);
				 enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase enddtValue=null;
					 if(custpay.getOrderCreationDate()!=null){
						
						  enddtValue=new Phrase(fmt.format(custpay.getOrderCreationDate()),font9);
				 }
				 else
				 {
					  enddtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell enddtValuecell=new PdfPCell();
				 enddtValuecell.addElement(enddtValue);
				 enddtValuecell.setBorder(0);
				 enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 
				 
				 Phrase refNo=new Phrase("Invoice ID",font9);
				 PdfPCell refNocell=new PdfPCell();
				 refNocell.addElement(refNo);
				 refNocell.setBorder(0);
				 refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase refNovalueValue=null;
					 if(custpay.getInvoiceCount()!=0){
						 refNovalueValue=new Phrase(custpay.getInvoiceCount()+"",font9);
				 }
				 else
				 {
					  refNovalueValue=new Phrase("",font9);
				 }
				
				 PdfPCell refNovalueValuecell=new PdfPCell();
				 refNovalueValuecell.addElement(refNovalueValue);
				 refNovalueValuecell.setBorder(0);
				 refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDt=new Phrase("Invoice Date",font9);
				 PdfPCell refDtcell=new PdfPCell();
				 refDtcell.addElement(refDt);
				 refDtcell.setBorder(0);
				 refDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDtValue=null;
				 
					 if(custpay.getInvoiceDate()!=null){
						  refDtValue=new Phrase(fmt.format(custpay.getInvoiceDate()),font9);
					 }
				 else
				 {
					  refDtValue=new Phrase("",font9);
				 }
				
				 PdfPCell refDtValuecell=new PdfPCell();
				 refDtValuecell.addElement(refDtValue);
				 refDtValuecell.setBorder(0);
				 refDtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase seperator=new Phrase(":",font9);
				 PdfPCell seperatorcell=new PdfPCell();
				 seperatorcell.addElement(seperator);
				 seperatorcell.setBorder(0);
				 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				 
				 PdfPTable infotable=new PdfPTable(6);
				 infotable.setHorizontalAlignment(100);
				
				 if(custpay.getContractCount()!=0){
				 infotable.addCell(startdtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(startdtValuecell);
				 }
				 
				 if(custpay.getOrderCreationDate()!=null){
				 infotable.addCell(enddtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(enddtValuecell);
				 }
				 
				 if(custpay.getInvoiceCount()!=0){
				 infotable.addCell(refNocell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refNovalueValuecell);
				 }
				 
				 if(custpay.getInvoiceDate()!=null){
				 infotable.addCell(refDtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refDtValuecell);
				 }
				
				 
				 
				 try {
					infotable.setWidths(columnWidths2);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} 
				 
				  infotablecell=new PdfPCell(infotable);
				  infotablecell.setBorder(0);
				 
					
				
				PdfPTable custtable=new PdfPTable(1);
				custtable.setWidthPercentage(100);
				custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
				custtable.addCell(custnamecell);
				custtable.addCell(custaddress1);
				if(!vendor.getPrimaryAddress().getAddrLine2().equals("")){
					custtable.addCell(custaddress2);
				}
				if(!vendor.getPrimaryAddress().getLandmark().equals("")){
					custtable.addCell(custlandcell2);
				}
				if(!vendor.getPrimaryAddress().getLocality().equals("")){
					
				custtable.addCell(custlocalitycell2);
				}
				custtable.addCell(custcitycell2);
				custtable.addCell(custcontactcell);
				custtable.addCell(custemailcell);
				
				custtable.addCell(infotablecell);
				
				// if company is true then the name of contact person
				
//				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//					Phrase realcontact=new Phrase("Attn: "+cust.getFullname(),font10);
//					realcontact.add(Chunk.NEWLINE);
//					PdfPCell realcontactcell=new PdfPCell();
//					realcontactcell.addElement(realcontact);
//					realcontactcell.setBorder(0);
//					custtable.addCell(realcontactcell);
//				}
				 
				
				PdfPTable headparenttable= new PdfPTable(2);
			headparenttable.setWidthPercentage(100);
			try {
				headparenttable.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			PdfPCell  companyinfocell = new PdfPCell();
			PdfPCell custinfocell = new PdfPCell();
			
			
			companyinfocell.addElement(companytable);
			custinfocell.addElement(custtable);
			headparenttable.addCell(companyinfocell);
			headparenttable.addCell(custinfocell);

			
			String title="";
				title="Details of your payment";
				
			
			Date conEnt=null;
			
				conEnt=custpay.getPaymentDate();
			
			
			
			String countinfo="ID : "+custpay.getCount();
			
			String creationdateinfo="Date: "+fmt.format(conEnt);
			
			Phrase titlephrase =new Phrase(title,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			titlepdfpara.add(Chunk.NEWLINE);
			PdfPCell titlepdfcell=new PdfPCell();
			titlepdfcell.addElement(titlepdfpara);
			titlepdfcell.setBorderWidthRight(0);
			titlepdfcell.setBorderWidthLeft(0);
//			titlepdfcell.addElement(Chunk.NEWLINE);
			
			Phrase idphrase=new Phrase(countinfo,font14bold);
			Paragraph idofpdfpara=new Paragraph();
			idofpdfpara.add(idphrase);
			idofpdfpara.setAlignment(Element.ALIGN_LEFT);
			idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell countcell=new PdfPCell();
			countcell.addElement(idofpdfpara);
			countcell.setBorderWidthRight(0);
//			countcell.addElement(Chunk.NEWLINE);
			
			Phrase dateofpdf=new Phrase(creationdateinfo,font14bold);
			Paragraph creatndatepara=new Paragraph();
			creatndatepara.add(dateofpdf);
			creatndatepara.setAlignment(Element.ALIGN_RIGHT);
			creatndatepara.add(Chunk.NEWLINE);
			PdfPCell creationcell=new PdfPCell();
			creationcell.addElement(creatndatepara);
			creationcell.setBorderWidthLeft(0);
//			creationcell.addElement(Chunk.NEWLINE);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.addCell(countcell);
			titlepdftable.addCell(titlepdfcell);
			titlepdftable.addCell(creationcell);
			
			
			
			
			try {
				document.add(headparenttable);
				document.add(titlepdftable);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		
			
			
		}

		private void footerInfo1() {
				
			
			
			 String thankPart1="We thank you very for the payment.";
		     Paragraph thankPart1para=new Paragraph();
		     thankPart1para.add(thankPart1);
		     thankPart1para.setAlignment(Element.ALIGN_CENTER);
		     thankPart1para.setFont(font12boldul);

		     PdfPCell thankucell=new PdfPCell();
		     thankucell.addElement(thankPart1para);
		     thankucell.setBorder(0);
		     
		     String thankyoupart2="We look forward to a long & mutually beneficial association.";
		     Paragraph thankyoupart2Para=new Paragraph();
		     thankyoupart2Para.add(thankyoupart2);
		     thankyoupart2Para.setAlignment(Element.ALIGN_CENTER);
		     thankyoupart2Para.setFont(font12boldul);

		     PdfPCell thankyouPart2cell=new PdfPCell();
		     thankyouPart2cell.addElement(thankyoupart2Para);
		     thankyouPart2cell.setBorder(0);
		     
		     String companyName= comp.getBusinessUnitName().trim();
		     Paragraph companyNamepara=new Paragraph();
		     companyNamepara.add(companyName);
		     companyNamepara.setAlignment(Element.ALIGN_CENTER);
		     companyNamepara.setFont(font12boldul);

		     
		     PdfPCell companyNamepCell=new PdfPCell();
		     companyNamepCell.addElement(companyNamepara);
		     companyNamepCell.setBorder(0);
		     
		     String note= "This document is auto generated and does not carry a signature";
		     Paragraph notepara=new Paragraph();
		     notepara.add(note);
		     notepara.setAlignment(Element.ALIGN_CENTER);
		     notepara.setFont(font12boldul);
		     
		     PdfPCell noteCell=new PdfPCell();
		     noteCell.addElement(notepara);
		     noteCell.setBorder(0);

		     Phrase ppp=new Phrase(" ");
		     PdfPCell blacell=new PdfPCell();
		     blacell.addElement(ppp);
		     blacell.setBorder(0);
		     
		     Paragraph blankpara =new Paragraph();
		     blankpara.add(Chunk.NEWLINE);
		     PdfPCell parablacell=new PdfPCell();
		     parablacell.addElement(blankpara);
		     parablacell.setBorder(0);
		     
		     PdfPTable table = new PdfPTable(1);
		     table.setWidthPercentage(100);
			 table.addCell(parablacell);
			 table.addCell(thankucell);
			 table.addCell(thankyouPart2cell);

			 table.addCell(parablacell);
			 table.addCell(companyNamepCell);
			 table.addCell(noteCell);
			 table.addCell(parablacell);
			 
		     
		     PdfPCell parenttable=new PdfPCell(table);
		     
		     PdfPTable table2 = new PdfPTable(1);
		     table2.setWidthPercentage(100);
		     table2.addCell(parenttable);
		     

			  try {
					document.add(table2);
					
				} 
				catch (DocumentException e) {
					e.printStackTrace();
				}
			
			
		}
			


		private void createPaymentDetails1() {

//			String paydetails="Payment Details";
//			
//			Paragraph pay=new Paragraph();
//			pay.add(paydetails);
//			pay.setFont(font12);
//			pay.setAlignment(Element.ALIGN_CENTER);
			
//			PdfPCell paycell=new PdfPCell(pay);
//			paycell.setBorder(0);
//			paycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase payMethodPhrasetitle = new Phrase("Payment Method ",font10);
			
			Phrase paymethod=null;
			
			if(custpay.getPaymentMethod()!=null){
				paymethod=new Phrase(custpay.getPaymentMethod(),font10);
			}else{
				paymethod=new Phrase("",font10);
			}
			 
			Phrase amountphrase=new Phrase("Payable Amount",font10);
			
			Phrase amount=null;
			if(custpay.getPaymentAmt()!=0){
				amount=new Phrase("INR   "+custpay.getPaymentAmt()+"",font10);
			}else{
				amount=new Phrase("",font10);
			}
			
			//******************for Amt Receive ***************** 
			System.out.println("RRRR Amt receive"+custpay.getPaymentReceived()+"===pay payable "+custpay.getPaymentAmt());
			Phrase amtReceive=new Phrase("Amount Received",font10);
			
			Phrase amtReceivePhrase=null;
			if(custpay.getPaymentReceived()!=0){
				amtReceivePhrase=new Phrase("INR   "+custpay.getPaymentReceived()+"",font10);
			}else{
				amtReceivePhrase=new Phrase("",font10);
			}
			
			PdfPCell amtReceiveCell=new PdfPCell();
			amtReceiveCell.addElement(amtReceive);
			amtReceiveCell.setBorder(0);
			
			PdfPCell amtReceivePhraseCell=new PdfPCell();
			amtReceivePhraseCell.addElement(amtReceivePhrase);
			amtReceivePhraseCell.setBorder(0);
		
			//*********************ends here ********************
			
			PdfPCell paymethodcell=new PdfPCell();
			paymethodcell.addElement(payMethodPhrasetitle);
			paymethodcell.setBorder(0);
			
			PdfPCell paymethodcell1=new PdfPCell();
			paymethodcell1.addElement(paymethod);
			paymethodcell1.setBorder(0);
			paymethodcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			PdfPCell amountcell=new PdfPCell();
			amountcell.addElement(amountphrase);
			amountcell.setBorder(0);
			
			PdfPCell amountcell1=new PdfPCell();
			amountcell1.addElement(amount);
			amountcell1.setBorder(0);
			amountcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			
			Phrase bankphrase=new Phrase("Bank Name",font10);
			
			Phrase bankname=null;
			
			if(custpay.getBankName()!=null){
				
				bankname=new Phrase(custpay.getBankName(),font10);
			}else{
				bankname=new Phrase("",font10);
			}
			
			
			Phrase branchphrase=new Phrase("Branch Name",font10);
			
			Phrase branchvalue=null;
			if(custpay.getBankBranch()!=null){
				branchvalue=new Phrase(custpay.getBankBranch(),font10);
			}
			
			
			PdfPCell banknamecell=new PdfPCell();
			banknamecell.addElement(bankphrase);
			banknamecell.setBorder(0);
			
			PdfPCell banknamecell1=new PdfPCell();
			banknamecell1.addElement(bankname);
			banknamecell1.setBorder(0);
			banknamecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			PdfPCell branchname=new PdfPCell();
			branchname.addElement(branchphrase);
			branchname.setBorder(0);
			
			PdfPCell branchname1=new PdfPCell();
			branchname1.addElement(branchvalue);
			branchname1.setBorder(0);
			branchname1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			Phrase chequephrase=new Phrase("Cheque No.",font10);
			Phrase chequename=null;

			if(custpay.getChequeNo()!=null && !custpay.getChequeNo().equals("") ){
				
				chequename=new Phrase(custpay.getChequeNo()+"",font10);
			}else{
				chequename=new Phrase(" ",font10);
			}
			
			Phrase chequedate=new Phrase("Cheque Date",font10);
			Phrase chquedatevalue=null;
			if(custpay.getChequeDate()!=null){
				chquedatevalue=new Phrase(fmt.format(custpay.getChequeDate()),font10);
			}else{
				chquedatevalue=new Phrase("",font10);
			}
			
			PdfPCell chequenocell=new PdfPCell();
			chequenocell.addElement(chequephrase);
			chequenocell.setBorder(0);
			
			PdfPCell chequenocell1=new PdfPCell();
			chequenocell1.addElement(chequename);
			chequenocell1.setBorder(0);
			chequenocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			PdfPCell chequedatecell=new PdfPCell();
			chequedatecell.addElement(chequedate);
			chequedatecell.setBorder(0);
			
			PdfPCell chequedatecell1=new PdfPCell();
			chequedatecell1.addElement(chquedatevalue);
			chequedatecell1.setBorder(0);
			chequedatecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			Phrase escidphrase=new Phrase("Reference No.",font10);
			
			Phrase ecsid=null;
			
			if(custpay.getReferenceNo()!=null){
				
				ecsid=new Phrase(custpay.getReferenceNo(),font10);
			}else{
				ecsid=new Phrase("",font10);
			}
			
			
			Phrase tansferphrase=new Phrase("Transfer Date",font10);
			
			Phrase tadtevalue=null;
			if(custpay.getAmountTransferDate()!=null){
				tadtevalue=new Phrase(fmt.format(custpay.getAmountTransferDate()),font10);
			}
			
			
			
			PdfPCell ecscell=new PdfPCell();
			ecscell.addElement(escidphrase);
			ecscell.setBorder(0);
			
			PdfPCell ecscell1=new PdfPCell();
			ecscell1.addElement(ecsid);
			ecscell1.setBorder(0);
			ecscell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			PdfPCell tansdatecell=new PdfPCell();
			tansdatecell.addElement(tansferphrase);
			tansdatecell.setBorder(0);
			
			PdfPCell tansdatecell1=new PdfPCell();
			tansdatecell1.addElement(tadtevalue);
			tansdatecell1.setBorder(0);
			tansdatecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			
			
			Phrase blank=new Phrase(" ",font10);
			PdfPCell blankcell=new PdfPCell();
			blankcell.addElement(blank);
			blankcell.setBorder(0);
			
			Phrase colon=new Phrase(":",font10);
			PdfPCell coloncell=new PdfPCell();
			coloncell.addElement(colon);
			coloncell.setBorder(0);
			coloncell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		
			// ********  rohan added this tds value in pdf ***************
			
			Phrase tdfPercentage = new Phrase("TDS %",font10);
			PdfPCell tdfPercentageCell = new PdfPCell(tdfPercentage);
			tdfPercentageCell.setBorder(0);
			
			String tdsPercent ="";
			if(custpay.getTdsPercentage()!= null && !custpay.getTdsPercentage().equals("")){
				 tdsPercent = custpay.getTdsPercentage();
			}
			Phrase tdfPercentageValue = new Phrase(tdsPercent,font10);
			PdfPCell tdfPercentageValueCell = new PdfPCell(tdfPercentageValue);
			tdfPercentageValueCell.setBorder(0);
		
			Phrase tdsAmt = new Phrase("TDS Amount",font10);
			PdfPCell tdsAmtCell = new PdfPCell(tdsAmt);
			tdsAmtCell.setBorder(0);
			
			Phrase tdsAmtValue = new Phrase(custpay.getTdsTaxValue()+"",font10);
			PdfPCell tdsAmtValueCell = new PdfPCell(tdsAmtValue);
			tdsAmtValueCell.setBorder(0);
			
			Phrase netpay = new Phrase("Net Amount",font10);
			PdfPCell netpayCell = new PdfPCell(netpay);
			netpayCell.setBorder(0);
			
			Phrase netpayValue = new Phrase(custpay.getNetPay()+"",font10);
			PdfPCell netpayValueCell = new PdfPCell(netpayValue);
			netpayValueCell.setBorder(0);
			
			//********************changes emds here **********************
			
			PdfPTable paymenttable=new PdfPTable(9);
			try {
				paymenttable.setWidths(columnWidths3);
			} catch (DocumentException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			paymenttable.setWidthPercentage(100);
//			paymenttable.setSpacingAfter(20f);
			
//			try {
//				paymenttable.setWidths(columnWidths);55
//			} catch (DocumentException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			
			paymenttable.addCell(amountcell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(amountcell1);
			
			paymenttable.addCell(amtReceiveCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(amtReceivePhraseCell);
			
			
			
			paymenttable.addCell(paymethodcell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(paymethodcell1);
			
			
			
			
			
			paymenttable.addCell(banknamecell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(banknamecell1);

			
			
			paymenttable.addCell(branchname);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(branchname1);
			paymenttable.addCell(chequenocell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(chequenocell1);
			
			
			
			paymenttable.addCell(chequedatecell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(chequedatecell1);
			
			
			paymenttable.addCell(ecscell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(ecscell1);
			paymenttable.addCell(tansdatecell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(tansdatecell1);
			
			
			//  rohan 
			paymenttable.addCell(tdfPercentageCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(tdfPercentageValueCell);
			paymenttable.addCell(tdsAmtCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(tdsAmtValueCell);
			
			
			paymenttable.addCell(netpayCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(netpayValueCell);
			
			
			
//			PdfPTable paytable=new PdfPTable(1);
//			paytable.setWidthPercentage(100);
//			paytable.addCell(paycell);
//			paytable.setSpacingBefore(15f);
//			paytable.setSpacingAfter(20f);
			
//			PdfPCell paytcell=new PdfPCell(paytable);
//			paytcell.setBorder(0);
			
			PdfPCell paytcell1=new PdfPCell(paymenttable);
			paytcell1.setBorder(0);
			
			PdfPTable suparatable=new PdfPTable(1);
			suparatable.setWidthPercentage(100);
//			suparatable.addCell(paytcell);
			suparatable.addCell(paytcell1);
			
			PdfPTable parentatble=new PdfPTable(1); 
			parentatble.setWidthPercentage(100);
			parentatble.addCell(suparatable);
//			parentatble.addCell(paymenttable);
			try {
//				document.add(pay);
				document.add(parentatble);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
	}

		private void createCompanyHedding1() {


			
			Phrase companyName= new Phrase(comp.getBusinessUnitName(),font12boldul);
		    Paragraph p =new Paragraph();
		    p.add(Chunk.NEWLINE);
		    p.add(companyName);
		    
		    PdfPCell companyHeadingCell=new PdfPCell();
		    companyHeadingCell.setBorder(0);
		    companyHeadingCell.addElement(p);
		    
		    Phrase branchphrase=null;
		    PdfPCell companybranchCell=null;
		    if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
		    {
			    String branch1="Branch Name";
			    branchphrase=new Phrase(branch1+" - "+branch.getBusinessUnitName(),font10);
			    companybranchCell=new PdfPCell();
			    companybranchCell.setBorder(0);
			    companybranchCell.addElement(branchphrase);
		    }
		    
		    
		    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
			Phrase adressline2=null;
			if(comp.getAddress().getAddrLine2()!=null)
			{
				adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
			}else{
				adressline2= new Phrase("",font10);
			}
			
			Phrase landmark=null;
			if(comp.getAddress().getLandmark()!=null)
			{
				landmark= new Phrase(comp.getAddress().getLandmark(),font10);
			}
			else
			{
				landmark= new Phrase("",font12);
			}
			
			
			Phrase locality=null;
			if(comp.getAddress().getLocality()!=null)
			{
				locality= new Phrase(comp.getAddress().getLocality(),font10);
			}
			else
			{
				locality= new Phrase("",font12);
			}
			Phrase	city=new Phrase(comp.getAddress().getCity()+" - "+comp.getAddress().getPin(),font10);
			
			
			PdfPCell citycell= new PdfPCell();
			citycell.addElement(city);
			citycell.setBorder(0);
			PdfPCell addressline1cell=new PdfPCell();
			PdfPCell addressline2cell=new PdfPCell();
			addressline1cell.addElement(adressline1);
			addressline1cell.setBorder(0);
				
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
			
			PdfPCell landmarkcell=new PdfPCell();
			landmarkcell.addElement(landmark);
			landmarkcell.setBorder(0);
			
			PdfPCell localitycell=new PdfPCell();
			localitycell.addElement(locality);
			localitycell.setBorder(0);
			
			String contactinfo="Cell: "+comp.getContact().get(0).getCellNo1();
			if(comp.getContact().get(0).getCellNo2()!=0)
				contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
			if(comp.getContact().get(0).getLandline()!=0){
				contactinfo=contactinfo+"     "+" Tel: "+comp.getContact().get(0).getLandline();
			}
			
			Phrase contactnos=new Phrase(contactinfo,font9);
			Phrase email= new Phrase("Email: "+comp.getContact().get(0).getEmail(),font9);
			
			PdfPCell contactcell=new PdfPCell();
			contactcell.addElement(contactnos);
			contactcell.setBorder(0);
			PdfPCell emailcell=new PdfPCell();
			emailcell.setBorder(0);
			emailcell.addElement(email);
			
			
			PdfPTable companytable=new PdfPTable(1);
			companytable.addCell(companyHeadingCell);
			if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				companytable.addCell(companybranchCell);
			}
			companytable.addCell(addressline1cell);
			if(!comp.getAddress().getAddrLine2().equals(""))
			{
				companytable.addCell(addressline2cell);
			}
			if(!comp.getAddress().getLandmark().equals("")){
				companytable.addCell(landmarkcell);
			}
			if(!comp.getAddress().getLocality().equals("")){
				companytable.addCell(localitycell);
			}
			companytable.addCell(citycell);
			companytable.addCell(contactcell);
			companytable.addCell(emailcell);
			companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
			companytable.setWidthPercentage(100);
			
			
			/**
			 * Customer Info
			 */
//			String tosir="To, M/S";
			
			//************************changes made by rohan	for M/s **************** 
//			String tosir= null;
//			if(cust.isCompany()==false){
//				
//				
//			 tosir="To, M/S";
//			}
//			else{
//				tosir=" ";
//			}
//			//*******************changes ends here ********************************
//			
//			String custName="";
//			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//				custName=cust.getCompanyName().trim();
//			}
//			else{
//				custName=cust.getFullname().trim();
//			}
			
			
			String tosir= null;
			String custName="";
			//   rohan modified this code for printing printable name 
			
			if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
				custName=cust.getCustPrintableName().trim();
			}
			else
			{
			
				if(cust.isCompany()==true){
					
					
				 tosir="To, M/S";
				}
				else{
					tosir="To,";
				}
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					custName=cust.getCompanyName().trim();
				}
				else{
					custName=cust.getFullname().trim();
				}
			}
			
//			Phrase tosirphrase= new Phrase(tosir,font12bold);
//			
//			Phrase customername= new Phrase(custName,font12boldul);
		    Paragraph fullname =new Paragraph();
		    fullname.add(Chunk.NEWLINE);
		    if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
		    	fullname.add(custName);
			}
		    else
		    {
		    	fullname.add(tosir+"   "+custName);
		    }
		    fullname.setFont(font12bold);
		    
				PdfPCell custnamecell=new PdfPCell();
				custnamecell.addElement(fullname);
				custnamecell.setBorder(0);
				 
				Phrase customeradress= new Phrase("                   "+cust.getAdress().getAddrLine1(),font10);
			Phrase customeradress2=null;
			if(cust.getAdress().getAddrLine2()!=null){
			   customeradress2= new Phrase("                   "+cust.getAdress().getAddrLine2(),font10);
			}
			
			PdfPCell custaddress1=new PdfPCell();
			custaddress1.addElement(customeradress);
			custaddress1.setBorder(0);
			
			PdfPCell custaddress2=new PdfPCell();
			if(cust.getAdress().getAddrLine2()!=null){
				custaddress2.addElement(customeradress2);
				custaddress2.setBorder(0);
				}
			
			
			String custlandmark2="";
			Phrase landmar2=null;
			
			String  localit2="";
			Phrase custlocality2= null;
			
			if(cust.getAdress().getLandmark()!=null)
			{
				custlandmark2 = cust.getAdress().getLandmark();
				landmar2=new Phrase("                   "+custlandmark2,font10);
			}
			
			
			if(cust.getAdress().getLocality()!=null){
				localit2=cust.getAdress().getLocality();
				custlocality2=new Phrase("                   "+localit2,font10);
				}
			
			Phrase cityState2=new Phrase("                   "+cust.getAdress().getCity()
					+" - "+cust.getAdress().getPin()
//					+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
					,font10);
			
			PdfPCell custlandcell2=new PdfPCell();
			custlandcell2.addElement(landmar2);
			custlandcell2.setBorder(0);
			
			PdfPCell custlocalitycell2=new PdfPCell();
			custlocalitycell2.addElement(custlocality2);
			custlocalitycell2.setBorder(0);
			
			PdfPCell custcitycell2=new PdfPCell();
			custcitycell2.addElement(cityState2);
			custcitycell2.setBorder(0); 
//			custcitycell2.addElement(Chunk.NEWLINE);
				 
			Phrase custcontact=new Phrase("Mobile: "+cust.getCellNumber1()+"",font9);
				PdfPCell custcontactcell=new PdfPCell();
				custcontactcell.addElement(custcontact);
				custcontactcell.setBorder(0);
				
				Phrase custemail=new Phrase("email: "+cust.getEmail(),font9);
				PdfPCell custemailcell=new PdfPCell();
				custemailcell.addElement(custemail);
				custemailcell.setBorder(0);
				
					
				
				 PdfPCell infotablecell=null;
				 PdfPCell infotablecell1=null;
					 
				 Phrase startdt=new Phrase("Order ID",font9);
				 PdfPCell startdtcell=new PdfPCell();
				 startdtcell.addElement(startdt);
				 startdtcell.setBorder(0);
				 startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase startdtValue=null;
					 if(custpay.getContractCount()!=null){
						 startdtValue=new Phrase(custpay.getContractCount()+"",font9);
					 }
				 else
				 {
					  startdtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell startdtValuecell=new PdfPCell();
				 startdtValuecell.addElement(startdtValue);
				 startdtValuecell.setBorder(0);
				 startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase enddt=new Phrase("Order Date",font9);
				 PdfPCell enddtcell=new PdfPCell();
				 enddtcell.addElement(enddt);
				 enddtcell.setBorder(0);
				 enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase enddtValue=null;
					 if(custpay.getOrderCreationDate()!=null){
						
						  enddtValue=new Phrase(fmt.format(custpay.getOrderCreationDate()),font9);
				 }
				 else
				 {
					  enddtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell enddtValuecell=new PdfPCell();
				 enddtValuecell.addElement(enddtValue);
				 enddtValuecell.setBorder(0);
				 enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 
				 
				 Phrase refNo=new Phrase("Invoice ID",font9);
				 PdfPCell refNocell=new PdfPCell();
				 refNocell.addElement(refNo);
				 refNocell.setBorder(0);
				 refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase refNovalueValue=null;
					 if(custpay.getInvoiceCount()!=0){
						 refNovalueValue=new Phrase(custpay.getInvoiceCount()+"",font9);
				 }
				 else
				 {
					  refNovalueValue=new Phrase("",font9);
				 }
				
				 PdfPCell refNovalueValuecell=new PdfPCell();
				 refNovalueValuecell.addElement(refNovalueValue);
				 refNovalueValuecell.setBorder(0);
				 refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDt=new Phrase("Invoice Date",font9);
				 PdfPCell refDtcell=new PdfPCell();
				 refDtcell.addElement(refDt);
				 refDtcell.setBorder(0);
				 refDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDtValue=null;
				 
					 if(custpay.getInvoiceDate()!=null){
						  refDtValue=new Phrase(fmt.format(custpay.getInvoiceDate()),font9);
					 }
				 else
				 {
					  refDtValue=new Phrase("",font9);
				 }
				
				 PdfPCell refDtValuecell=new PdfPCell();
				 refDtValuecell.addElement(refDtValue);
				 refDtValuecell.setBorder(0);
				 refDtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase seperator=new Phrase(":",font9);
				 PdfPCell seperatorcell=new PdfPCell();
				 seperatorcell.addElement(seperator);
				 seperatorcell.setBorder(0);
				 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				 
				 PdfPTable infotable=new PdfPTable(6);
				 infotable.setHorizontalAlignment(100);
				
				 if(custpay.getContractCount()!=0){
				 infotable.addCell(startdtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(startdtValuecell);
				 }
				 
				 if(custpay.getOrderCreationDate()!=null){
				 infotable.addCell(enddtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(enddtValuecell);
				 }
				 
				 if(custpay.getInvoiceCount()!=0){
				 infotable.addCell(refNocell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refNovalueValuecell);
				 }
				 
				 if(custpay.getInvoiceDate()!=null){
				 infotable.addCell(refDtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refDtValuecell);
				 }
				
				 
				 
				 try {
					infotable.setWidths(columnWidths2);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} 
				 
				  infotablecell=new PdfPCell(infotable);
				  infotablecell.setBorder(0);
				 
					
				
				PdfPTable custtable=new PdfPTable(1);
				custtable.setWidthPercentage(100);
				custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
				custtable.addCell(custnamecell);
				custtable.addCell(custaddress1);
				if(!cust.getAdress().getAddrLine2().equals("")){
					custtable.addCell(custaddress2);
				}
				if(!cust.getAdress().getLandmark().equals("")){
					custtable.addCell(custlandcell2);
				}
				if(!cust.getAdress().getLocality().equals("")){
					
				custtable.addCell(custlocalitycell2);
				}
				custtable.addCell(custcitycell2);
				custtable.addCell(custcontactcell);
				custtable.addCell(custemailcell);
				
				custtable.addCell(infotablecell);
				
				// if company is true then the name of contact person
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					Phrase realcontact=new Phrase("Attn: "+cust.getFullname(),font10);
					realcontact.add(Chunk.NEWLINE);
					PdfPCell realcontactcell=new PdfPCell();
					realcontactcell.addElement(realcontact);
					realcontactcell.setBorder(0);
					custtable.addCell(realcontactcell);
				}
				 
				
				PdfPTable headparenttable= new PdfPTable(2);
			headparenttable.setWidthPercentage(100);
			try {
				headparenttable.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			PdfPCell  companyinfocell = new PdfPCell();
			PdfPCell custinfocell = new PdfPCell();
			
			
			companyinfocell.addElement(companytable);
			custinfocell.addElement(custtable);
			headparenttable.addCell(companyinfocell);
			headparenttable.addCell(custinfocell);

			
			String title="";
				title="Details of your payment";
				
			
			Date conEnt=null;
			
				conEnt=custpay.getPaymentDate();
			
			
			
			String countinfo="ID : "+custpay.getCount();
			
			String creationdateinfo="Date: "+fmt.format(conEnt);
			
			Phrase titlephrase =new Phrase(title,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			titlepdfpara.add(Chunk.NEWLINE);
			PdfPCell titlepdfcell=new PdfPCell();
			titlepdfcell.addElement(titlepdfpara);
			titlepdfcell.setBorderWidthRight(0);
			titlepdfcell.setBorderWidthLeft(0);
//			titlepdfcell.addElement(Chunk.NEWLINE);
			
			Phrase idphrase=new Phrase(countinfo,font14bold);
			Paragraph idofpdfpara=new Paragraph();
			idofpdfpara.add(idphrase);
			idofpdfpara.setAlignment(Element.ALIGN_LEFT);
			idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell countcell=new PdfPCell();
			countcell.addElement(idofpdfpara);
			countcell.setBorderWidthRight(0);
//			countcell.addElement(Chunk.NEWLINE);
			
			Phrase dateofpdf=new Phrase(creationdateinfo,font14bold);
			Paragraph creatndatepara=new Paragraph();
			creatndatepara.add(dateofpdf);
			creatndatepara.setAlignment(Element.ALIGN_RIGHT);
			creatndatepara.add(Chunk.NEWLINE);
			PdfPCell creationcell=new PdfPCell();
			creationcell.addElement(creatndatepara);
			creationcell.setBorderWidthLeft(0);
//			creationcell.addElement(Chunk.NEWLINE);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.addCell(countcell);
			titlepdftable.addCell(titlepdfcell);
			titlepdftable.addCell(creationcell);
			
			
			
			
			try {
				document.add(headparenttable);
				document.add(titlepdftable);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
	
			
			
	

		private void createLogo1(Document doc, Company comp) {
			
		
			//********************logo for server ********************
		DocumentUpload document =comp.getLogo();
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,745f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		
		/**
		 * Date 14-02-2018 By Vijay for Logo not appearing proper issue
		 * above old code commented and below new code added 
		 */
		
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + document.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		
		try {
			doc.add(logoTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/**
		 * ends here
		 */

		

		//***********************for local **************************
		
//		try
//		{
//		Image image1=Image.getInstance("images/logo15.jpg");
//		image1.scalePercent(10f);
//		image1.setAbsolutePosition(40f,745f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		
		
		}
		
		
//		private void Createblank() {
//			
//		    Paragraph blank =new Paragraph();
//		    blank.add(Chunk.NEWLINE);
//		    try {
//				document.add(blank);
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
//			
//			
//		}
//		
//		private void createCompanyHedding() {
//			
//			
//			String No="No.    : ";
//			String no1="UTH / 130";
//			
//			
//			
//			Phrase nophrase=new Phrase(No+" "+no1,font10);
////			Phrase nophrase1=new Phrase(no1,font10);
//			
//			
//			PdfPCell nocell=new PdfPCell(nophrase);
//			nocell.setBorder(0);
//			nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
////			PdfPCell nocell1=new PdfPCell(nophrase1);
////			nocell1.setBorder(0);
////			nocell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			String date="DATE   : ";
//			
//			String date1=null;
//			
//			if(custpay.getPaymentDate()!=null){
//				date1=fmt.format(custpay.getPaymentDate());
//			}
//			
//			Phrase datephrase=new Phrase(date+" "+date1,font10);
//			
////			Phrase datephrase1=new Phrase(date1,font10);
//			
//			PdfPCell datecell=new PdfPCell(datephrase);
//			datecell.setBorder(0);
//			datecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
////			PdfPCell datecell1=new PdfPCell(datephrase1);
////			datecell1.setBorder(0);
////			datecell1.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			Phrase blankphrase=new Phrase("  ",font8);
//			PdfPCell blancell=new PdfPCell(blankphrase);
//			blancell.setBorder(0);
//			
//			PdfPTable Notable=new PdfPTable(2);
//			Notable.setWidthPercentage(100);
//			Notable.setHorizontalAlignment(Element.ALIGN_CENTER);
//			Notable.addCell(blancell);
//			Notable.addCell(nocell);
////			Notable.addCell(nocell1);
//			Notable.addCell(blancell);
//			Notable.addCell(datecell);
////			Notable.addCell(datecell1);
//			
////			PdfPTable blanktable=new PdfPTable(2);
////			PdfPCell blaCell=new PdfPCell();
////			blaCell.setBorder(0);
////			blaCell.addElement(blanktable);
////			
////			
////			
////			PdfPTable parenttable=new PdfPTable(3);
////			parenttable.addCell(blanktable);
////			parenttable.addCell(blanktable);
////			parenttable.addCell(Notable);
//			
//			
//			
//			try {
//				document.add(Notable);
//				document.add(Chunk.NEWLINE);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			
//			
//			
//			String title="RECEIPT";
//			
//			Phrase titlephrase=new Phrase(title,font12bold);
//
//			PdfPCell tiltecell=new PdfPCell(titlephrase);
//			tiltecell.setBorder(0);
//			tiltecell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			PdfPTable titletable=new PdfPTable(1);
//			titletable.addCell(tiltecell);
//			titletable.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//			int np=(int) custpay.getPaymentReceived();
//			String  amtInFigure= ServiceInvoicePdf.convert(np);
//			
//			//******************rohan changes 
//			String chno="";
////			int chno=0;
//			if(custpay.getChequeNo()!=""){
//				chno=custpay.getChequeNo();
//			}
//			
//			String bankname="";
//			
//			bankname=custpay.getBankName().toUpperCase();
//			
//			
//			int billno=0;
//			if(custpay.getInvoiceCount()!=0){
//				billno=custpay.getInvoiceCount();
//			}
//			
//			
//			Phrase dess=new Phrase("RECEIVED FROM M/S."+" "+custpay.getPersonInfo().getFullName().toUpperCase()+" "+"A SUM OF RS."+" "+amtInFigure.toUpperCase()+" ONLY"+" "+"BY CHEQUE NO. "+chno+" "+"DRAWN ON THE"+" "+bankname+" "+"AS FULL PAYMENT OF BILL NO."+" "+billno,font10);
//			
//			Phrase para=new Phrase("FOR"+"  "+comp.getBusinessUnitName().toUpperCase(),font10);
//			PdfPCell paracell=new PdfPCell(para);
//			paracell.setBorder(0);
//			paracell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			Phrase para1=new Phrase("(AUTHORISED SIGNATORY)",font10bold);
//			PdfPCell para1cell=new PdfPCell(para1);
//			para1cell.setBorder(0);
//			para1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//			
//			
//			Integer amount=0;
//			
//			amount=custpay.getPaymentReceived();
//			
//			Phrase amountpara=new Phrase("Rs. "+" "+amount+"",font10);
//			PdfPCell amountcell=new PdfPCell(amountpara);
//			amountcell.setBorder(0);
//			amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//			String parastr="SUBJECT TO REALISATION OF CHEQUE";
//			Phrase amountpara1=new Phrase(parastr,font10);
//			PdfPCell amountcell1=new PdfPCell(amountpara1);
//			amountcell1.setBorder(0);
//			amountcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//			
//			
////			Phrase blank=new Phrase("   ",font10);
////			PdfPCell blankcell=new PdfPCell(blank);
////			blankcell.setBorder(0);
//			
//			
//			PdfPTable paratable=new PdfPTable(2);
//			paratable.setWidthPercentage(100);
////			paratable.setHorizontalAlignment(Element.ALIGN_RIGHT);
////			paratable.addCell(blankcell);
//			paratable.addCell(paracell);
////			paratable.addCell(blankcell);
////			paratable.addCell(blankcell);
////			paratable.addCell(blankcell);
////			paratable.addCell(blankcell);
//			paratable.addCell(amountcell);
////			paratable.addCell(blankcell);
//			paratable.addCell(amountcell1);
//			paratable.addCell(para1cell);
//			
//			
//			
//			try {
//				document.add(titletable);
////				document.add(Chunk.NEWLINE);
//				document.add(dess);
////				document.add(Chunk.NEWLINE);
////				document.add(Chunk.NEWLINE);
//				document.add(Chunk.NEWLINE);
//				document.add(paratable);
//				
//			} catch (DocumentException e) {
//				e.printStackTrace();
//			}
//			
//			
//			
////			Integer amount=0;
////			
////			amount=custpay.getPaymentReceived();
////			
////			Phrase amountpara=new Phrase("Rs. "+" "+amount+"",font10);
////			PdfPCell amountcell=new PdfPCell(amountpara);
////			amountcell.setBorder(0);
////			amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
////			
////			String parastr="SUBJECT TO REALISATION OF CHEQUE";
////			Phrase amountpara1=new Phrase(parastr,font10);
////			PdfPCell amountcell1=new PdfPCell(amountpara1);
////			amountcell1.setBorder(0);
////			amountcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
////			
////			
////			PdfPTable amtable=new PdfPTable(1);
////			amtable.addCell(amountcell);
////			amtable.addCell(amountcell1);
////			amtable.setHorizontalAlignment(Element.ALIGN_LEFT);
////			
////			try {
////				
////				document.add(amtable);
////			} catch (DocumentException e) {
////				e.printStackTrace();
////			}
////			
//		}
		
	//*************************xxxxx*******************	
		

		private void createCompanyHedding4() {


			Phrase companyName= new Phrase("                  "+comp.getBusinessUnitName(),font12boldul);
		    Paragraph p =new Paragraph();
		    p.add(Chunk.NEWLINE);
		    p.add(companyName);
		    
		    PdfPCell companyHeadingCell=new PdfPCell();
		    companyHeadingCell.setBorder(0);
		    companyHeadingCell.addElement(p);
		    
		    Phrase branchphrase=null;
		    PdfPCell companybranchCell=null;
		    if(!custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
		    {
			    String branch1="Branch Name";
			    branchphrase=new Phrase(branch1+" - "+branch.getBusinessUnitName(),font10);
			    companybranchCell=new PdfPCell();
			    companybranchCell.setBorder(0);
			    companybranchCell.addElement(branchphrase);
		    }
		    
		    
		    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
			Phrase adressline2=null;
			if(comp.getAddress().getAddrLine2()!=null)
			{
				adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
			}else{
				adressline2= new Phrase("",font10);
			}
			
			Phrase landmark=null;
			if(comp.getAddress().getLandmark()!=null)
			{
				landmark= new Phrase(comp.getAddress().getLandmark(),font10);
			}
			else
			{
				landmark= new Phrase("",font12);
			}
			
			
			Phrase locality=null;
			if(comp.getAddress().getLocality()!=null)
			{
				locality= new Phrase(comp.getAddress().getLocality(),font10);
			}
			else
			{
				locality= new Phrase("",font12);
			}
			Phrase	city=new Phrase(comp.getAddress().getCity()+" - "+comp.getAddress().getPin(),font10);
			
			
			PdfPCell citycell= new PdfPCell();
			citycell.addElement(city);
			citycell.setBorder(0);
			PdfPCell addressline1cell=new PdfPCell();
			PdfPCell addressline2cell=new PdfPCell();
			addressline1cell.addElement(adressline1);
			addressline1cell.setBorder(0);
				
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
			
			PdfPCell landmarkcell=new PdfPCell();
			landmarkcell.addElement(landmark);
			landmarkcell.setBorder(0);
			
			PdfPCell localitycell=new PdfPCell();
			localitycell.addElement(locality);
			localitycell.setBorder(0);
			
			String contactinfo="Cell: "+comp.getContact().get(0).getCellNo1();
			if(comp.getContact().get(0).getCellNo2()!=0)
				contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
			if(comp.getContact().get(0).getLandline()!=0){
				contactinfo=contactinfo+"     "+" Tel: "+comp.getContact().get(0).getLandline();
			}
			
			Phrase contactnos=new Phrase(contactinfo,font9);
			Phrase email= new Phrase("Email: "+comp.getContact().get(0).getEmail(),font9);
			
			PdfPCell contactcell=new PdfPCell();
			contactcell.addElement(contactnos);
			contactcell.setBorder(0);
			PdfPCell emailcell=new PdfPCell();
			emailcell.setBorder(0);
			emailcell.addElement(email);
			
			
			PdfPTable companytable=new PdfPTable(1);
			companytable.addCell(companyHeadingCell);
			if(!custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				companytable.addCell(companybranchCell);
			}
			companytable.addCell(addressline1cell);
			if(!comp.getAddress().getAddrLine2().equals(""))
			{
				companytable.addCell(addressline2cell);
			}
			if(!comp.getAddress().getLandmark().equals("")){
				companytable.addCell(landmarkcell);
			}
			if(!comp.getAddress().getLocality().equals("")){
				companytable.addCell(localitycell);
			}
			companytable.addCell(citycell);
			companytable.addCell(contactcell);
			companytable.addCell(emailcell);
			companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
			companytable.setWidthPercentage(100);
			
			
			/**
			 * Customer Info
			 */
//			String tosir="To, M/S";
			//************************changes made by rohan	for M/s **************** 
			String tosir= null;
//			if(cust.isCompany()==true){
			 tosir="To, M/S";
//			}
//			else{
//				tosir="To,";
//			}
			//*******************changes ends here ********************sss*************
			
			
			
			String custName="";
//			if(vendor.isCompany()==true&&cust.getCompanyName()!=null){
//				custName=cust.getCompanyName().trim();
//			}
//			else{
				custName=vendor.getVendorName().trim();
//			}
			
			Phrase tosirphrase= new Phrase(tosir,font12bold);
			
			Phrase customername= new Phrase(custName,font12boldul);
		    Paragraph fullname =new Paragraph();
		    fullname.add(Chunk.NEWLINE);
		    fullname.add(tosir+"   "+custName);
		    fullname.setFont(font12bold);
		    
				PdfPCell custnamecell=new PdfPCell();
				custnamecell.addElement(fullname);
				custnamecell.setBorder(0);
				 
				Phrase customeradress= new Phrase("                   "+vendor.getPrimaryAddress().getAddrLine1(),font10);
			Phrase customeradress2=null;
			if(vendor.getPrimaryAddress().getAddrLine2()!=null){
			   customeradress2= new Phrase("                   "+vendor.getPrimaryAddress().getAddrLine2(),font10);
			}
			
			PdfPCell custaddress1=new PdfPCell();
			custaddress1.addElement(customeradress);
			custaddress1.setBorder(0);
			
			PdfPCell custaddress2=new PdfPCell();
			if(vendor.getPrimaryAddress().getAddrLine2()!=null){
				custaddress2.addElement(customeradress2);
				custaddress2.setBorder(0);
				}
			
			
			String custlandmark2="";
			Phrase landmar2=null;
			
			String  localit2="";
			Phrase custlocality2= null;
			
			if(vendor.getPrimaryAddress().getLandmark()!=null)
			{
				custlandmark2 = vendor.getPrimaryAddress().getLandmark();
				landmar2=new Phrase("                   "+custlandmark2,font10);
			}
			
			
			if(vendor.getPrimaryAddress().getLocality()!=null){
				localit2=vendor.getPrimaryAddress().getLocality();
				custlocality2=new Phrase("                   "+localit2,font10);
				}
			
			Phrase cityState2=new Phrase("                   "+vendor.getPrimaryAddress().getCity()
					+" - "+vendor.getPrimaryAddress().getPin()
//					+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
					,font10);
			
			PdfPCell custlandcell2=new PdfPCell();
			custlandcell2.addElement(landmar2);
			custlandcell2.setBorder(0);
			
			PdfPCell custlocalitycell2=new PdfPCell();
			custlocalitycell2.addElement(custlocality2);
			custlocalitycell2.setBorder(0);
			
			PdfPCell custcitycell2=new PdfPCell();
			custcitycell2.addElement(cityState2);
			custcitycell2.setBorder(0); 
//			custcitycell2.addElement(Chunk.NEWLINE);
				 
			Phrase custcontact=new Phrase("Mobile: "+vendor.getCellNumber1()+"",font9);
				PdfPCell custcontactcell=new PdfPCell();
				custcontactcell.addElement(custcontact);
				custcontactcell.setBorder(0);
				
				Phrase custemail=new Phrase("email: "+vendor.getEmail(),font9);
				PdfPCell custemailcell=new PdfPCell();
				custemailcell.addElement(custemail);
				custemailcell.setBorder(0);
				
					
				
				 PdfPCell infotablecell=null;
				 PdfPCell infotablecell1=null;
					 
				 Phrase startdt=new Phrase("Order ID",font9);
				 PdfPCell startdtcell=new PdfPCell();
				 startdtcell.addElement(startdt);
				 startdtcell.setBorder(0);
				 startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase startdtValue=null;
					 if(custpay.getContractCount()!=null){
						 startdtValue=new Phrase(custpay.getContractCount()+"",font9);
					 }
				 else
				 {
					  startdtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell startdtValuecell=new PdfPCell();
				 startdtValuecell.addElement(startdtValue);
				 startdtValuecell.setBorder(0);
				 startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase enddt=new Phrase("Order Date",font9);
				 PdfPCell enddtcell=new PdfPCell();
				 enddtcell.addElement(enddt);
				 enddtcell.setBorder(0);
				 enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase enddtValue=null;
					 if(custpay.getOrderCreationDate()!=null){
						
						  enddtValue=new Phrase(fmt.format(custpay.getOrderCreationDate()),font9);
				 }
				 else
				 {
					  enddtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell enddtValuecell=new PdfPCell();
				 enddtValuecell.addElement(enddtValue);
				 enddtValuecell.setBorder(0);
				 enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 
				 
				 Phrase refNo=new Phrase("Invoice ID",font9);
				 PdfPCell refNocell=new PdfPCell();
				 refNocell.addElement(refNo);
				 refNocell.setBorder(0);
				 refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase refNovalueValue=null;
					 if(custpay.getInvoiceCount()!=0){
						 refNovalueValue=new Phrase(custpay.getInvoiceCount()+"",font9);
				 }
				 else
				 {
					  refNovalueValue=new Phrase("",font9);
				 }
				
				 PdfPCell refNovalueValuecell=new PdfPCell();
				 refNovalueValuecell.addElement(refNovalueValue);
				 refNovalueValuecell.setBorder(0);
				 refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDt=new Phrase("Invoice Date",font9);
				 PdfPCell refDtcell=new PdfPCell();
				 refDtcell.addElement(refDt);
				 refDtcell.setBorder(0);
				 refDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDtValue=null;
				 
					 if(custpay.getInvoiceDate()!=null){
						  refDtValue=new Phrase(fmt.format(custpay.getInvoiceDate()),font9);
					 }
				 else
				 {
					  refDtValue=new Phrase("",font9);
				 }
				
				 PdfPCell refDtValuecell=new PdfPCell();
				 refDtValuecell.addElement(refDtValue);
				 refDtValuecell.setBorder(0);
				 refDtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase seperator=new Phrase(":",font9);
				 PdfPCell seperatorcell=new PdfPCell();
				 seperatorcell.addElement(seperator);
				 seperatorcell.setBorder(0);
				 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				 
				 PdfPTable infotable=new PdfPTable(6);
				 infotable.setHorizontalAlignment(100);
				
				 if(custpay.getContractCount()!=0){
				 infotable.addCell(startdtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(startdtValuecell);
				 }
				 
				 if(custpay.getOrderCreationDate()!=null){
				 infotable.addCell(enddtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(enddtValuecell);
				 }
				 
				 if(custpay.getInvoiceCount()!=0){
				 infotable.addCell(refNocell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refNovalueValuecell);
				 }
				 
				 if(custpay.getInvoiceDate()!=null){
				 infotable.addCell(refDtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refDtValuecell);
				 }
				
				 
				 
				 try {
					infotable.setWidths(columnWidths2);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} 
				 
				  infotablecell=new PdfPCell(infotable);
				  infotablecell.setBorder(0);
				 
					
				
				PdfPTable custtable=new PdfPTable(1);
				custtable.setWidthPercentage(100);
				custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
				custtable.addCell(custnamecell);
				custtable.addCell(custaddress1);
				if(!vendor.getPrimaryAddress().getAddrLine2().equals("")){
					custtable.addCell(custaddress2);
				}
				if(!vendor.getPrimaryAddress().getLandmark().equals("")){
					custtable.addCell(custlandcell2);
				}
				if(!vendor.getPrimaryAddress().getLocality().equals("")){
					
				custtable.addCell(custlocalitycell2);
				}
				custtable.addCell(custcitycell2);
				custtable.addCell(custcontactcell);
				custtable.addCell(custemailcell);
				
				custtable.addCell(infotablecell);
				
				// if company is true then the name of contact person
				
//				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//					Phrase realcontact=new Phrase("Attn: "+cust.getFullname(),font10);
//					realcontact.add(Chunk.NEWLINE);
//					PdfPCell realcontactcell=new PdfPCell();
//					realcontactcell.addElement(realcontact);
//					realcontactcell.setBorder(0);
//					custtable.addCell(realcontactcell);
//				}
				 
				
				PdfPTable headparenttable= new PdfPTable(2);
			headparenttable.setWidthPercentage(100);
			try {
				headparenttable.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			PdfPCell  companyinfocell = new PdfPCell();
			PdfPCell custinfocell = new PdfPCell();
			
			
			companyinfocell.addElement(companytable);
			custinfocell.addElement(custtable);
			headparenttable.addCell(companyinfocell);
			headparenttable.addCell(custinfocell);

			
			String title="";
				title="Details of your payment";
				
			
			Date conEnt=null;
			
				conEnt=custpay.getPaymentDate();
			
			
			
			String countinfo="ID : "+custpay.getCount();
			
			String creationdateinfo="Date: "+fmt.format(conEnt);
			
			Phrase titlephrase =new Phrase(title,font14bold);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			titlepdfpara.add(Chunk.NEWLINE);
			PdfPCell titlepdfcell=new PdfPCell();
			titlepdfcell.addElement(titlepdfpara);
			titlepdfcell.setBorderWidthRight(0);
			titlepdfcell.setBorderWidthLeft(0);
//			titlepdfcell.addElement(Chunk.NEWLINE);
			
			Phrase idphrase=new Phrase(countinfo,font14bold);
			Paragraph idofpdfpara=new Paragraph();
			idofpdfpara.add(idphrase);
			idofpdfpara.setAlignment(Element.ALIGN_LEFT);
			idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell countcell=new PdfPCell();
			countcell.addElement(idofpdfpara);
			countcell.setBorderWidthRight(0);
//			countcell.addElement(Chunk.NEWLINE);
			
			Phrase dateofpdf=new Phrase(creationdateinfo,font14bold);
			Paragraph creatndatepara=new Paragraph();
			creatndatepara.add(dateofpdf);
			creatndatepara.setAlignment(Element.ALIGN_RIGHT);
			creatndatepara.add(Chunk.NEWLINE);
			PdfPCell creationcell=new PdfPCell();
			creationcell.addElement(creatndatepara);
			creationcell.setBorderWidthLeft(0);
//			creationcell.addElement(Chunk.NEWLINE);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.addCell(countcell);
			titlepdftable.addCell(titlepdfcell);
			titlepdftable.addCell(creationcell);
			
			
			
			
			try {
				document.add(headparenttable);
				document.add(titlepdftable);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		
			
			
		}

		private void footerInfo() {
				
			double amount=0;
			
			if(custpay.getNetPay()!=0){
				amount=custpay.getNetPay();
			}else{
				amount=custpay.getPaymentReceived();
			}
			
			String  amtInFigure= ServiceInvoicePdf.convert(amount).toUpperCase();
			
			Phrase amountpara=null;
			if(!amtInFigure.equalsIgnoreCase("ZERO"))
			{
				 amountpara=new Phrase("Rs. "+" "+amtInFigure+" "+"ONLY",font10);
			}
			else
			{
				 amountpara=new Phrase(" ",font10);
			}
			
			PdfPCell amountcell=new PdfPCell(amountpara);
			amountcell.setBorder(0);
			amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			amountcell.setHorizontalAlignment(Element.ALIGN_BOTTOM);
			
			String parastr=" ";
			
			if(custpay.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
				parastr="SUBJECT TO REALISATION OF CHEQUE";
			}
			
			Phrase amountpara1=new Phrase(parastr,font10);
			PdfPCell amountcell1=new PdfPCell(amountpara1);
			amountcell1.setBorder(0);
			amountcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			amountcell1.setHorizontalAlignment(Element.ALIGN_BOTTOM);
			
			 
			
			/***Date 18-9-2020 by Amol print company name instead of branch name **/
			String companyname="";
			if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
				companyname= branchDt.getCorrespondenceName();
				logger.log(Level.SEVERE,"3rd:");
			}
			else {
				companyname = comp.getBusinessUnitName().trim().toUpperCase();
			}
			
			 logger.log(Level.SEVERE, "company Name "+companyname);
//			 String companyname=comp.getBusinessUnitName().trim().toUpperCase();
			 Paragraph companynamepara=new Paragraph();
			 companynamepara.add("FOR "+companyname);
			 companynamepara.setAlignment(Element.ALIGN_CENTER);
			 companynamepara.setFont(font10bold);


				DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		
				String environment = System.getProperty("com.google.appengine.runtime.environment");
				String hostUrl;
				if (environment.equals("Production")) {
					String applicationId = System.getProperty("com.google.appengine.application.id");
					String version = System.getProperty("com.google.appengine.application.version");
					hostUrl = "http://" + version + "." + applicationId+ ".appspot.com/";
				} else {
					hostUrl = "http://localhost:8888";
				}
			    imageSignCell = null;
			    Image image2 = null;
			    logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
			    try {
			      image2 = Image.getInstance(new URL(hostUrl + digitalDocument.getUrl()));
			      image2.scalePercent(15.0F);
			      image2.scaleAbsoluteWidth(100.0F);
			      
			      imageSignCell = new PdfPCell(image2);
			      imageSignCell.setBorder(0);
			      imageSignCell.setHorizontalAlignment(1);
			     
			    }
			    catch (Exception e) {
			      e.printStackTrace();
			    }
			 
			 String authsign="AUTHORISED SIGNATORY";
		     Paragraph authpara=new Paragraph();
		     authpara.add(authsign);
		     authpara.setAlignment(Element.ALIGN_CENTER);
		     authpara.setFont(font10bold);
		     
		     PdfPCell companynamecell=new PdfPCell();
		     companynamecell.addElement(companynamepara);
		     companynamecell.setBorder(0);
		     
		     PdfPCell authsigncell=new PdfPCell();
		     authsigncell.addElement(authpara);
		     authsigncell.setBorder(0);

		     Phrase ppp=new Phrase(" ");
		     PdfPCell blacell=new PdfPCell();
		     blacell.addElement(ppp);
		     blacell.setBorder(0);
		     PdfPTable table = new PdfPTable(1);
		     table.setWidthPercentage(100);
			 table.addCell(companynamecell);
			 
			 if (imageSignCell != null) {
				 table.addCell(imageSignCell);
			    } else {
			      Phrase blank1 = new Phrase(" ", font10);
			      PdfPCell blank1Cell = new PdfPCell(blank1);
			      blank1Cell.setBorder(0);
			      table.addCell(blank1Cell);
			      table.addCell(blank1Cell);
			      table.addCell(blank1Cell);
			      table.addCell(blank1Cell);
			    }
//			 table.addCell(blacell);
//			 table.addCell(blacell);
//			 table.addCell(blacell);
//			 table.addCell(blacell);
			 table.addCell(authsigncell);
			 
			PdfPTable blancktable=new PdfPTable(1);
			blancktable.setWidthPercentage(100);
			blancktable.addCell(blacell);
			blancktable.addCell(amountcell); 
			blancktable.addCell(blacell);
			blancktable.addCell(blacell);
			blancktable.addCell(blacell);
			blancktable.addCell(blacell);
			blancktable.addCell(amountcell1);
			blancktable.setHorizontalAlignment(Element.ALIGN_BOTTOM);
			
			  PdfPTable parentbanktable=new PdfPTable(2);
			  parentbanktable.setSpacingAfter(40f);
			  parentbanktable.setWidthPercentage(100);
		      try {
		    	  parentbanktable.setWidths(new float[]{60,40});
			  } 
		      catch (DocumentException e1) {
				e1.printStackTrace();
			}
		      
		      
		      PdfPCell bankdatacell1 = new PdfPCell();
			  PdfPCell authorisedcell = new PdfPCell();
			  
			  bankdatacell1.addElement(blancktable);
			  authorisedcell.addElement(table);
			  
			  parentbanktable.addCell(bankdatacell1);
			  parentbanktable.addCell(authorisedcell);
			  
			  
			  
			  
			  
			  try {
					document.add(parentbanktable);
					
				} 
				catch (DocumentException e) {
					e.printStackTrace();
				}
			  
			  
		}
			


		private void createPaymentDetails() {

//			String paydetails="Payment Details";
//			
//			Paragraph pay=new Paragraph();
//			pay.add(paydetails);
//			pay.setFont(font12);
//			pay.setAlignment(Element.ALIGN_CENTER);
			
//			PdfPCell paycell=new PdfPCell(pay);
//			paycell.setBorder(0);
//			paycell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase payMethodPhrasetitle = new Phrase("Payment Method ",font10);
			
			Phrase paymethod=null;
			
			if(custpay.getPaymentMethod()!=null){
				paymethod=new Phrase(custpay.getPaymentMethod(),font10);
			}else{
				paymethod=new Phrase("",font10);
			}
			 
			Phrase amountphrase=new Phrase("Payable Amount",font10);
			
			Phrase amount=null;
			if(custpay.getPaymentAmt()!=0){
				amount=new Phrase("INR   "+custpay.getPaymentAmt()+"",font10);
			}else{
				amount=new Phrase("",font10);
			}
			
			//******************for Amt Receive ***************** 
			System.out.println("RRRR Amt receive"+custpay.getPaymentReceived()+"===pay payable "+custpay.getPaymentAmt());
			Phrase amtReceive=new Phrase("Amount Received",font10);
			
			Phrase amtReceivePhrase=null;
			if(custpay.getPaymentReceived()!=0){
				amtReceivePhrase=new Phrase("INR   "+custpay.getPaymentReceived()+"",font10);
			}else{
				amtReceivePhrase=new Phrase("",font10);
			}
			
			PdfPCell amtReceiveCell=new PdfPCell();
			amtReceiveCell.addElement(amtReceive);
			amtReceiveCell.setBorder(0);
			
			PdfPCell amtReceivePhraseCell=new PdfPCell();
			amtReceivePhraseCell.addElement(amtReceivePhrase);
			amtReceivePhraseCell.setBorder(0);
		
			//*********************ends here ********************
			
			PdfPCell paymethodcell=new PdfPCell();
			paymethodcell.addElement(payMethodPhrasetitle);
			paymethodcell.setBorder(0);
			
			PdfPCell paymethodcell1=new PdfPCell();
			paymethodcell1.addElement(paymethod);
			paymethodcell1.setBorder(0);
			paymethodcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			PdfPCell amountcell=new PdfPCell();
			amountcell.addElement(amountphrase);
			amountcell.setBorder(0);
			
			PdfPCell amountcell1=new PdfPCell();
			amountcell1.addElement(amount);
			amountcell1.setBorder(0);
			amountcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			/**
			 * Rohan modified this code  
			 */
			//     branch detals 
			
			PdfPCell banknamecell = null;
			PdfPCell banknamecell1 = null;
			PdfPCell branchname = null;
			PdfPCell branchname1 = null;
			PdfPCell chequenocell = null;
			PdfPCell chequenocell1 =null;
			PdfPCell ecscell = null;
			PdfPCell chequedatecell1 =null;
			PdfPCell chequedatecell = null;
			PdfPCell chequeIssuedBycell= null;
			PdfPCell chequeIssuedByValuecell1 = null;
			PdfPCell ecscell1 = null;
			PdfPCell tansdatecell = null;
			PdfPCell tansdatecell1 = null;
			if(custpay.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
					Phrase bankphrase=new Phrase("Bank Name",font10);
					
					Phrase bankname=null;
					
					if(custpay.getBankName()!=null){
						
						bankname=new Phrase(custpay.getBankName(),font10);
					}else{
						bankname=new Phrase("",font10);
					}
					
					
					Phrase branchphrase=new Phrase("Branch Name",font10);
					
					Phrase branchvalue=null;
					if(custpay.getBankBranch()!=null){
						branchvalue=new Phrase(custpay.getBankBranch(),font10);
					}
					
					
					banknamecell=new PdfPCell();
					banknamecell.addElement(bankphrase);
					banknamecell.setBorder(0);
					
					banknamecell1=new PdfPCell();
					banknamecell1.addElement(bankname);
					banknamecell1.setBorder(0);
					banknamecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					branchname=new PdfPCell();
					branchname.addElement(branchphrase);
					branchname.setBorder(0);
					
					branchname1=new PdfPCell();
					branchname1.addElement(branchvalue);
					branchname1.setBorder(0);
					branchname1.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					Phrase chequephrase=new Phrase("Cheque No.",font10);
					Phrase chequename=null;
					if(custpay.getChequeNo()!=null && !custpay.getChequeNo().equals("")){
						
						chequename=new Phrase(custpay.getChequeNo()+"",font10);
					}else{
						chequename=new Phrase(" ",font10);
					}
					
					Phrase chequedate=new Phrase("Cheque Date",font10);
					Phrase chquedatevalue=null;
					if(custpay.getChequeDate()!=null){
						chquedatevalue=new Phrase(fmt.format(custpay.getChequeDate()),font10);
					}else{
						chquedatevalue=new Phrase("",font10);
					}
					
					chequenocell=new PdfPCell();
					chequenocell.addElement(chequephrase);
					chequenocell.setBorder(0);
					
					chequenocell1=new PdfPCell();
					chequenocell1.addElement(chequename);
					chequenocell1.setBorder(0);
					chequenocell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					chequedatecell=new PdfPCell();
					chequedatecell.addElement(chequedate);
					chequedatecell.setBorder(0);
					
					chequedatecell1=new PdfPCell();
					chequedatecell1.addElement(chquedatevalue);
					chequedatecell1.setBorder(0);
					chequedatecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
					
					Phrase chequeIssuedBy=new Phrase("Cheque Issued By",font10);
					Phrase chequeIssuedByValue=null;
					
					if(custpay.getChequeIssuedBy()!=null){
						
						chequeIssuedByValue=new Phrase(custpay.getChequeIssuedBy(),font10);
					}else{
						chequeIssuedByValue=new Phrase("",font10);
					}
					
					
					chequeIssuedBycell=new PdfPCell();
					chequeIssuedBycell.addElement(chequeIssuedBy);
					chequeIssuedBycell.setBorder(0);
					
					chequeIssuedByValuecell1=new PdfPCell();
					chequeIssuedByValuecell1.addElement(chequeIssuedByValue);
					chequeIssuedByValuecell1.setBorder(0);
					chequeIssuedByValuecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
		}
			// ********  rohan added this tds value in pdf ***************
			
			PdfPCell tdfPercentageCell = null;
			PdfPCell tdfPercentageValueCell = null;
			PdfPCell tdsAmtCell = null;
			PdfPCell tdsAmtValueCell = null;
			PdfPCell netpayCell = null;
			PdfPCell netpayValueCell = null;
			if(custpay.getTdsTaxValue()!=0)
			{
						Phrase tdfPercentage = new Phrase("TDS %",font10);
						tdfPercentageCell = new PdfPCell(tdfPercentage);
						tdfPercentageCell.setBorder(0);
						
						Phrase tdfPercentageValue = new Phrase(custpay.getTdsPercentage()+"",font10);
						tdfPercentageValueCell = new PdfPCell(tdfPercentageValue);
						tdfPercentageValueCell.setBorder(0);
					
						Phrase tdsAmt = new Phrase("TDS Amount",font10);
						tdsAmtCell = new PdfPCell(tdsAmt);
						tdsAmtCell.setBorder(0);
						
						Phrase tdsAmtValue = new Phrase(custpay.getTdsTaxValue()+"",font10);
						tdsAmtValueCell = new PdfPCell(tdsAmtValue);
						tdsAmtValueCell.setBorder(0);
						
						Phrase netpay = new Phrase("Net Amount",font10);
						netpayCell = new PdfPCell(netpay);
						netpayCell.setBorder(0);
						
						Phrase netpayValue = new Phrase(custpay.getNetPay()+"",font10);
						netpayValueCell = new PdfPCell(netpayValue);
						netpayValueCell.setBorder(0);
			}		
			/**
			 * Updated By: Viraj 
			 * Date: 15-03-2019
			 * Description: To add balance amount
			 */
			if(custpay.getPaymentMethod().equalsIgnoreCase("NEFT")) {
				Phrase tansferphrase=new Phrase("NEFT Transfer Date",font10);
				
				Phrase tadtevalue=null;
				if(custpay.getAmountTransferDate()!=null){
					tadtevalue=new Phrase(fmt.format(custpay.getAmountTransferDate()),font10);
				}
				tansdatecell=new PdfPCell();
				tansdatecell.addElement(tansferphrase);
				tansdatecell.setBorder(0);
				
				tansdatecell1=new PdfPCell();
				tansdatecell1.addElement(tadtevalue);
				tansdatecell1.setBorder(0);
				tansdatecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
				
				Phrase escidphrase=new Phrase("NEFT Reference No.",font10);
				Phrase ecsid=null;
				if(custpay.getReferenceNo()!=null){
					ecsid=new Phrase(custpay.getReferenceNo(),font10);
				}else{
					ecsid=new Phrase("",font10);
				}			
				
				ecscell=new PdfPCell();
				ecscell.addElement(escidphrase);
				ecscell.setBorder(0);
				
				ecscell1=new PdfPCell();
				ecscell1.addElement(ecsid);
				ecscell1.setBorder(0);
				ecscell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			}
			
			Phrase balAmt = new Phrase("INR   "+"Balance Amount",font10);
			PdfPCell balAmtCell = new PdfPCell(balAmt);
			balAmtCell.setBorder(0);
			balAmtCell.setPaddingTop(6);
			
			
			double balVal = 0.0;
			balVal = custpay.getPaymentAmt() - custpay.getPaymentReceived();
			
			Phrase balAmtVal = new Phrase("INR   "+balVal+"",font10);
			PdfPCell balAmtValCell = new PdfPCell(balAmtVal);
			balAmtValCell.setBorder(0);
			balAmtValCell.setPaddingTop(6);
			balAmtValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			/** Ends **/
			
						//********************changes emds here **********************
			
			Phrase blank=new Phrase(" ",font10);
			PdfPCell blankcell=new PdfPCell();
			blankcell.addElement(blank);
			blankcell.setBorder(0);
			
			Phrase colon=new Phrase(":",font10);
			PdfPCell coloncell=new PdfPCell();
			coloncell.addElement(colon);
			coloncell.setBorder(0);
			coloncell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			
			PdfPTable paymenttable=new PdfPTable(6);
			paymenttable.setWidthPercentage(100);
			paymenttable.setSpacingAfter(20f);
			
			try {
				paymenttable.setWidths(columnWidths);
			} catch (DocumentException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			
			
			paymenttable.addCell(amountcell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(amountcell1);
			
			paymenttable.addCell(amtReceiveCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(amtReceivePhraseCell);
			
			if(custpay.getTdsTaxValue()!=0)
			{
		//  rohan 
			paymenttable.addCell(tdfPercentageCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(tdfPercentageValueCell);
			
			paymenttable.addCell(tdsAmtCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(tdsAmtValueCell);
			
			paymenttable.addCell(netpayCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(netpayValueCell);
			}
			
			paymenttable.addCell(paymethodcell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(paymethodcell1);
			
			if(custpay.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
				paymenttable.addCell(banknamecell);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(banknamecell1);
				
				paymenttable.addCell(branchname);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(branchname1);
				
				paymenttable.addCell(chequenocell);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(chequenocell1);
				
				
				paymenttable.addCell(chequedatecell);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(chequedatecell1);
				
				
				paymenttable.addCell(chequeIssuedBycell);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(chequeIssuedByValuecell1);
			}
			/**
			 * Updated By: Viraj
			 * Date: 15-03-2019
			 * Description: To show balance amount
			 */
			
			if(custpay.getPaymentMethod().equalsIgnoreCase("NEFT")) {
				paymenttable.addCell(ecscell);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(ecscell1);
				
				paymenttable.addCell(tansdatecell);
				paymenttable.addCell(coloncell);
				paymenttable.addCell(tansdatecell1);
			}
			paymenttable.addCell(balAmtCell);
			paymenttable.addCell(coloncell);
			paymenttable.addCell(balAmtValCell);
			/** Ends **/
			
					paymenttable.addCell(blankcell);
					paymenttable.addCell(blankcell);
					paymenttable.addCell(blankcell);
					
			
//			PdfPTable paytable=new PdfPTable(1);
//			paytable.setWidthPercentage(100);
//			paytable.addCell(paycell);
//			paytable.setSpacingBefore(15f);
//			paytable.setSpacingAfter(20f);
			
//			PdfPCell paytcell=new PdfPCell(paytable);
//			paytcell.setBorder(0);
			
			PdfPCell paytcell1=new PdfPCell(paymenttable);
			paytcell1.setBorder(0);
			
			PdfPTable suparatable=new PdfPTable(1);
			suparatable.setWidthPercentage(100);
//			suparatable.addCell(paytcell);
			suparatable.addCell(paytcell1);
			
			PdfPTable parentatble=new PdfPTable(1); 
			parentatble.setWidthPercentage(100);
			parentatble.addCell(suparatable);
//			parentatble.addCell(paymenttable);
			try {
//				document.add(pay);
				document.add(parentatble);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
	}

		private void createCompanyHedding3() {


			
			Phrase companyName= new Phrase(comp.getBusinessUnitName(),font12boldul);
		    Paragraph p =new Paragraph();
		    p.add(Chunk.NEWLINE);
		    p.add(companyName);
		    
		    PdfPCell companyHeadingCell=new PdfPCell();
		    companyHeadingCell.setBorder(0);
		    companyHeadingCell.addElement(p);
		    companyHeadingCell.setHorizontalAlignment(Element.ALIGN_LEFT);

		    Phrase branchphrase=null;
		    PdfPCell companybranchCell=null;
		    if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE))
		    {
			    String branch1="Branch Name";
			    branchphrase=new Phrase(branch1+" - "+branch.getBusinessUnitName(),font10);
			    companybranchCell=new PdfPCell();
			    companybranchCell.setBorder(0);
			    companybranchCell.addElement(branchphrase);
		    }
		    
		    
		    Phrase adressline1= new Phrase(comp.getAddress().getAddrLine1(),font10);
			Phrase adressline2=null;
			if(comp.getAddress().getAddrLine2()!=null)
			{
				adressline2= new Phrase(comp.getAddress().getAddrLine2(),font10);
			}else{
				adressline2= new Phrase("",font10);
			}
			
			Phrase landmark=null;
			if(comp.getAddress().getLandmark()!=null)
			{
				landmark= new Phrase(comp.getAddress().getLandmark(),font10);
			}
			else
			{
				landmark= new Phrase("",font12);
			}
			
			
			Phrase locality=null;
			if(comp.getAddress().getLocality()!=null)
			{
				locality= new Phrase(comp.getAddress().getLocality(),font10);
			}
			else
			{
				locality= new Phrase("",font12);
			}
			String cityName = comp.getAddress().getCity();
			if(comp.getAddress().getLocality()!=null && !comp.getAddress().getLocality().equals("") && comp.getAddress().getPin()!=0){
				cityName +=" - "+comp.getAddress().getPin();
			}
			Phrase	city=new Phrase(cityName,font10);
//			Phrase	city=new Phrase(comp.getAddress().getCity()+" - "+comp.getAddress().getPin(),font10);
			
			
			PdfPCell citycell= new PdfPCell();
			citycell.addElement(city);
			citycell.setBorder(0);
			PdfPCell addressline1cell=new PdfPCell();
			PdfPCell addressline2cell=new PdfPCell();
			addressline1cell.addElement(adressline1);
			addressline1cell.setBorder(0);
				
			addressline2cell.addElement(adressline2);
			addressline2cell.setBorder(0);
			
			PdfPCell landmarkcell=new PdfPCell();
			landmarkcell.addElement(landmark);
			landmarkcell.setBorder(0);
			
			PdfPCell localitycell=new PdfPCell();
			localitycell.addElement(locality);
			localitycell.setBorder(0);
			
			String contactinfo="Mobile: "+comp.getContact().get(0).getCellNo1();
			if(comp.getContact().get(0).getCellNo2()!=0)
				contactinfo=contactinfo+","+comp.getContact().get(0).getCellNo2();
			if(comp.getContact().get(0).getLandline()!=0){
				contactinfo=contactinfo+"     "+" Tel: "+comp.getContact().get(0).getLandline();
			}
			
			Phrase contactnos=new Phrase(contactinfo,font9);
			Phrase email= new Phrase("Email: "+comp.getContact().get(0).getEmail(),font9);
			
			PdfPCell contactcell=new PdfPCell();
			contactcell.addElement(contactnos);
			contactcell.setBorder(0);
			PdfPCell emailcell=new PdfPCell();
			emailcell.setBorder(0);
			emailcell.addElement(email);
			
			
			PdfPTable companytable=new PdfPTable(1);
			companytable.addCell(companyHeadingCell);
			if(custpay.getTypeOfOrder().trim().equals(AppConstants.ORDERTYPEPURCHASE)){
				companytable.addCell(companybranchCell);
			}
			companytable.addCell(addressline1cell);
			if(!comp.getAddress().getAddrLine2().equals(""))
			{
				companytable.addCell(addressline2cell);
			}
			if(!comp.getAddress().getLandmark().equals("")){
				companytable.addCell(landmarkcell);
			}
			if(!comp.getAddress().getLocality().equals("")){
				companytable.addCell(localitycell);
			}
			companytable.addCell(citycell);
			companytable.addCell(contactcell);
			companytable.addCell(emailcell);
			companytable.setHorizontalAlignment(Element.ALIGN_LEFT);
			companytable.setWidthPercentage(100);
			
			
			/**
			 * Customer Info
			 */
//			String tosir="To, M/S";
			
			//************************changes made by rohan	for M/s **************** 
//			String tosir= null;
//			if(cust.isCompany()==false){
//				
//				
//			 tosir="To, M/S";
//			}
//			else{
//				tosir=" ";
//			}
//			//*******************changes ends here ********************************
//			
//			String custName="";
//			if(cust.isCompany()==true&&cust.getCompanyName()!=null){
//				custName=cust.getCompanyName().trim();
//			}
//			else{
//				custName=cust.getFullname().trim();
//			}
			
			
			String tosir= null;
			String custName="";
			//   rohan modified this code for printing printable name 
			
			if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
				custName=cust.getCustPrintableName().trim();
			}
			else
			{
			
				if(cust.isCompany()==true){
					
					
				 tosir="To, M/S";
				}
				else{
					tosir="To,";
				}
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					custName=cust.getCompanyName().trim();
				}
				else{
					custName=cust.getFullname().trim();
				}
			}
			
			
//			Phrase tosirphrase= new Phrase(tosir,font12bold);
//			
//			Phrase customername= new Phrase(custName,font12boldul);
		    Paragraph fullname =new Paragraph();
		    fullname.add(Chunk.NEWLINE);
		    if(cust.getCustPrintableName()!=null && !cust.getCustPrintableName().equals(""))
			{
		    	fullname.add(custName);
			}
		    else
		    {
		    	fullname.add(tosir+"   "+custName);
		    }
		    fullname.setFont(font12bold);
		    
				PdfPCell custnamecell=new PdfPCell();
				custnamecell.addElement(fullname);
				custnamecell.setBorder(0);
				 
				Phrase customeradress= new Phrase(cust.getAdress().getAddrLine1(),font10);
			Phrase customeradress2=null;
			if(cust.getAdress().getAddrLine2()!=null){
			   customeradress2= new Phrase(cust.getAdress().getAddrLine2(),font10);
			}
			
			PdfPCell custaddress1=new PdfPCell();
			custaddress1.addElement(customeradress);
			custaddress1.setBorder(0);
			
			PdfPCell custaddress2=new PdfPCell();
			if(cust.getAdress().getAddrLine2()!=null){
				custaddress2.addElement(customeradress2);
				custaddress2.setBorder(0);
				}
			
			
			String custlandmark2="";
			Phrase landmar2=null;
			
			String  localit2="";
			Phrase custlocality2= null;
			
			if(cust.getAdress().getLandmark()!=null)
			{
				custlandmark2 = cust.getAdress().getLandmark();
				landmar2=new Phrase(custlandmark2,font10);
			}
			
			
			if(cust.getAdress().getLocality()!=null){
				localit2=cust.getAdress().getLocality();
				custlocality2=new Phrase(localit2,font10);
				}
			
			String citypin = cust.getAdress().getCity();
			if(cust.getAdress().getLocality()!=null && !cust.getAdress().getLocality().equals("")&&cust.getAdress().getPin()!=0){
				citypin += cust.getAdress().getPin();
			}
			Phrase cityState2=new Phrase(citypin,font10);
			
//			Phrase cityState2=new Phrase(cust.getAdress().getCity()
//					+" - "+cust.getAdress().getPin()
////					+" "+cust.getAdress().getState()+" "+cust.getAdress().getCountry()
//					,font10);
			
			PdfPCell custlandcell2=new PdfPCell();
			custlandcell2.addElement(landmar2);
			custlandcell2.setBorder(0);
			
			PdfPCell custlocalitycell2=new PdfPCell();
			custlocalitycell2.addElement(custlocality2);
			custlocalitycell2.setBorder(0);
			
			PdfPCell custcitycell2=new PdfPCell();
			custcitycell2.addElement(cityState2);
			custcitycell2.setBorder(0); 
//			custcitycell2.addElement(Chunk.NEWLINE);
				 
			Phrase custcontact=new Phrase("Mobile: "+cust.getCellNumber1()+"",font9);
				PdfPCell custcontactcell=new PdfPCell();
				custcontactcell.addElement(custcontact);
				custcontactcell.setBorder(0);
				
				Phrase custemail=new Phrase("email: "+cust.getEmail(),font9);
				PdfPCell custemailcell=new PdfPCell();
				custemailcell.addElement(custemail);
				custemailcell.setBorder(0);
				
					
				
				 PdfPCell infotablecell=null;
				 PdfPCell infotablecell1=null;
					 
				 Phrase startdt=new Phrase("Order ID",font9);
				 PdfPCell startdtcell=new PdfPCell();
				 startdtcell.addElement(startdt);
				 startdtcell.setBorder(0);
				 startdtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase startdtValue=null;
					 if(custpay.getContractCount()!=null){
						 startdtValue=new Phrase(custpay.getContractCount()+"",font9);
					 }
				 else
				 {
					  startdtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell startdtValuecell=new PdfPCell();
				 startdtValuecell.addElement(startdtValue);
				 startdtValuecell.setBorder(0);
				 startdtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase enddt=new Phrase("Order Date",font9);
				 PdfPCell enddtcell=new PdfPCell();
				 enddtcell.addElement(enddt);
				 enddtcell.setBorder(0);
				 enddtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase enddtValue=null;
					 if(custpay.getOrderCreationDate()!=null){
						
						  enddtValue=new Phrase(fmt.format(custpay.getOrderCreationDate()),font9);
				 }
				 else
				 {
					  enddtValue=new Phrase("",font9);
				 }
				 
				 PdfPCell enddtValuecell=new PdfPCell();
				 enddtValuecell.addElement(enddtValue);
				 enddtValuecell.setBorder(0);
				 enddtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 
				 
				 Phrase refNo=new Phrase("Invoice ID",font9);
				 PdfPCell refNocell=new PdfPCell();
				 refNocell.addElement(refNo);
				 refNocell.setBorder(0);
				 refNocell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 Phrase refNovalueValue=null;
					 if(custpay.getInvoiceCount()!=0){
						 refNovalueValue=new Phrase(custpay.getInvoiceCount()+"",font9);
				 }
				 else
				 {
					  refNovalueValue=new Phrase("",font9);
				 }
				
				 PdfPCell refNovalueValuecell=new PdfPCell();
				 refNovalueValuecell.addElement(refNovalueValue);
				 refNovalueValuecell.setBorder(0);
				 refNovalueValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDt=new Phrase("Invoice Date",font9);
				 PdfPCell refDtcell=new PdfPCell();
				 refDtcell.addElement(refDt);
				 refDtcell.setBorder(0);
				 refDtcell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase refDtValue=null;
				 
					 if(custpay.getInvoiceDate()!=null){
						  refDtValue=new Phrase(fmt.format(custpay.getInvoiceDate()),font9);
					 }
				 else
				 {
					  refDtValue=new Phrase("",font9);
				 }
				
				 PdfPCell refDtValuecell=new PdfPCell();
				 refDtValuecell.addElement(refDtValue);
				 refDtValuecell.setBorder(0);
				 refDtValuecell.setHorizontalAlignment(Element.ALIGN_LEFT);
				 
				 
				 Phrase seperator=new Phrase(":",font9);
				 PdfPCell seperatorcell=new PdfPCell();
				 seperatorcell.addElement(seperator);
				 seperatorcell.setBorder(0);
				 seperatorcell.setHorizontalAlignment(Element.ALIGN_CENTER);
				 
				 PdfPTable infotable=new PdfPTable(6);
				 infotable.setHorizontalAlignment(100);
				
				 if(custpay.getContractCount()!=0){
				 infotable.addCell(startdtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(startdtValuecell);
				 }
				 
				 if(custpay.getOrderCreationDate()!=null){
				 infotable.addCell(enddtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(enddtValuecell);
				 }
				 
				 if(custpay.getInvoiceCount()!=0){
				 infotable.addCell(refNocell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refNovalueValuecell);
				 }
				 
				 if(custpay.getInvoiceDate()!=null){
				 infotable.addCell(refDtcell);
				 infotable.addCell(seperatorcell);
				 infotable.addCell(refDtValuecell);
				 }
				
				 
				 
				 try {
					infotable.setWidths(columnWidths2);
				} catch (DocumentException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				} 
				 
				  infotablecell=new PdfPCell(infotable);
				  infotablecell.setBorder(0);
				 
					
				
				PdfPTable custtable=new PdfPTable(1);
				custtable.setWidthPercentage(100);
				custtable.setHorizontalAlignment(Element.ALIGN_LEFT);
				custtable.addCell(custnamecell);
				custtable.addCell(custaddress1);
				if(!cust.getAdress().getAddrLine2().equals("")){
					custtable.addCell(custaddress2);
				}
				if(!cust.getAdress().getLandmark().equals("")){
					custtable.addCell(custlandcell2);
				}
				if(!cust.getAdress().getLocality().equals("")){
					
				custtable.addCell(custlocalitycell2);
				}
				custtable.addCell(custcitycell2);
				custtable.addCell(custcontactcell);
				custtable.addCell(custemailcell);
				
				custtable.addCell(infotablecell);
				
				// if company is true then the name of contact person
				
				if(cust.isCompany()==true&&cust.getCompanyName()!=null){
					Phrase realcontact=new Phrase("Attn: "+cust.getFullname(),font10);
					realcontact.add(Chunk.NEWLINE);
					PdfPCell realcontactcell=new PdfPCell();
					realcontactcell.addElement(realcontact);
					realcontactcell.setBorder(0);
					custtable.addCell(realcontactcell);
				}
				 
				
				PdfPTable headparenttable= new PdfPTable(2);
			headparenttable.setWidthPercentage(100);
			try {
				headparenttable.setWidths(new float[]{50,50});
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}
			
			PdfPCell  companyinfocell = new PdfPCell();
			PdfPCell custinfocell = new PdfPCell();
			
			
			companyinfocell.addElement(companytable);
			custinfocell.addElement(custtable);
			headparenttable.addCell(companyinfocell);
			headparenttable.addCell(custinfocell);

			
			String title="";
				title="Details of your payment";
				
			
			Date conEnt=null;
			
				conEnt=custpay.getPaymentDate();
			
			
			
			String countinfo="ID : "+custpay.getCount();
			
			String creationdateinfo="Date: "+fmt.format(conEnt);
			
			Phrase titlephrase =new Phrase(title,font12boldul);
			Paragraph titlepdfpara=new Paragraph();
			titlepdfpara.add(titlephrase);
			titlepdfpara.setAlignment(Element.ALIGN_CENTER);
			titlepdfpara.add(Chunk.NEWLINE);
			PdfPCell titlepdfcell=new PdfPCell();
			titlepdfcell.addElement(titlepdfpara);
			titlepdfcell.setBorderWidthRight(0);
			titlepdfcell.setBorderWidthLeft(0);
//			titlepdfcell.addElement(Chunk.NEWLINE);
			
			Phrase idphrase=new Phrase(countinfo,font12boldul);
			Paragraph idofpdfpara=new Paragraph();
			idofpdfpara.add(idphrase);
			idofpdfpara.setAlignment(Element.ALIGN_LEFT);
			idofpdfpara.add(Chunk.NEWLINE);
			PdfPCell countcell=new PdfPCell();
			countcell.addElement(idofpdfpara);
			countcell.setBorderWidthRight(0);
//			countcell.addElement(Chunk.NEWLINE);
			
			Phrase dateofpdf=new Phrase(creationdateinfo,font12boldul);
			Paragraph creatndatepara=new Paragraph();
			creatndatepara.add(dateofpdf);
			creatndatepara.setAlignment(Element.ALIGN_RIGHT);
			creatndatepara.add(Chunk.NEWLINE);
			PdfPCell creationcell=new PdfPCell();
			creationcell.addElement(creatndatepara);
			creationcell.setBorderWidthLeft(0);
//			creationcell.addElement(Chunk.NEWLINE);
			
			PdfPTable titlepdftable=new PdfPTable(3);
			titlepdftable.setWidthPercentage(100);
			titlepdftable.addCell(countcell);
			titlepdftable.addCell(titlepdfcell);
			titlepdftable.addCell(creationcell);
			
			
			
			
			try {
				document.add(headparenttable);
				document.add(titlepdftable);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
		}
	
			
			
	

		private void createLogo(Document doc, Company comp) {
			
		
			//********************logo for server ********************
		DocumentUpload document =comp.getLogo();
		
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
//		try {
//			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
//			image2.scalePercent(20f);
//			image2.setAbsolutePosition(40f,745f);	
//			doc.add(image2);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		
		/**
		 * Date 14-02-2018 By Vijay for Logo not appearing proper issue
		 * above old code commented and below new code added 
		 */
		
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image
					.getInstance(new URL(hostUrl + document.getUrl()));
			image2.scalePercent(20f);
			// image2.setAbsolutePosition(40f,765f);
			// doc.add(image2);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			imageSignCell.setFixedHeight(40);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		PdfPTable logoTab = new PdfPTable(1);
		logoTab.setWidthPercentage(100);

		if (imageSignCell != null) {
			logoTab.addCell(imageSignCell);
		} else {
			Phrase logoblank = new Phrase(" ");
			PdfPCell logoblankcell = new PdfPCell(logoblank);
			logoblankcell.setBorder(0);
			logoTab.addCell(logoblankcell);
		}
		
		try {
			doc.add(logoTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/**
		 * ends here
		 */


		//***********************for local **************************
		
//		try
//		{
//		Image image1=Image.getInstance("images/logo15.jpg");
//		image1.scalePercent(10f);
//		image1.setAbsolutePosition(40f,745f);	
//		doc.add(image1);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
		
		
		}
		
		
		private void Createblank() {
			
		    Paragraph blank =new Paragraph();
		    blank.add(Chunk.NEWLINE);
		    try {
				document.add(blank);
			} catch (DocumentException e) {
				e.printStackTrace();
			}
			
			
		}
		
		private void createCompanyHedding() {
			
			
			String No="No.    : ";
			String no1="UTH / 130";
			
			
			
			Phrase nophrase=new Phrase(No+" "+no1,font10);
//			Phrase nophrase1=new Phrase(no1,font10);
			
			
			PdfPCell nocell=new PdfPCell(nophrase);
			nocell.setBorder(0);
			nocell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
//			PdfPCell nocell1=new PdfPCell(nophrase1);
//			nocell1.setBorder(0);
//			nocell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			String date="DATE   : ";
			
			String date1=null;
			
			if(custpay.getPaymentDate()!=null){
				date1=fmt.format(custpay.getPaymentDate());
			}
			
			Phrase datephrase=new Phrase(date+" "+date1,font10);
			
//			Phrase datephrase1=new Phrase(date1,font10);
			
			PdfPCell datecell=new PdfPCell(datephrase);
			datecell.setBorder(0);
			datecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
//			PdfPCell datecell1=new PdfPCell(datephrase1);
//			datecell1.setBorder(0);
//			datecell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase blankphrase=new Phrase("  ",font8);
			PdfPCell blancell=new PdfPCell(blankphrase);
			blancell.setBorder(0);
			
			PdfPTable Notable=new PdfPTable(2);
			Notable.setWidthPercentage(100);
			Notable.setHorizontalAlignment(Element.ALIGN_CENTER);
			Notable.addCell(blancell);
			Notable.addCell(nocell);
//			Notable.addCell(nocell1);
			Notable.addCell(blancell);
			Notable.addCell(datecell);
//			Notable.addCell(datecell1);
			
//			PdfPTable blanktable=new PdfPTable(2);
//			PdfPCell blaCell=new PdfPCell();
//			blaCell.setBorder(0);
//			blaCell.addElement(blanktable);
//			
//			
//			
//			PdfPTable parenttable=new PdfPTable(3);
//			parenttable.addCell(blanktable);
//			parenttable.addCell(blanktable);
//			parenttable.addCell(Notable);
			
			
			
			try {
				document.add(Notable);
				document.add(Chunk.NEWLINE);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
			String title="RECEIPT";
			
			Phrase titlephrase=new Phrase(title,font12bold);

			PdfPCell tiltecell=new PdfPCell(titlephrase);
			tiltecell.setBorder(0);
			tiltecell.setHorizontalAlignment(Element.ALIGN_CENTER);
			PdfPTable titletable=new PdfPTable(1);
			titletable.addCell(tiltecell);
			titletable.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			int np=(int) custpay.getPaymentReceived();
			String  amtInFigure= ServiceInvoicePdf.convert(np);
			
			//******************rohan changes 
			String chno="";
//			int chno=0;
			if(custpay.getChequeNo()!=""){
				chno=custpay.getChequeNo();
			}
			
			String bankname="";
			
			bankname=custpay.getBankName().toUpperCase();
			
			
			int billno=0;
			if(custpay.getInvoiceCount()!=0){
				billno=custpay.getInvoiceCount();
			}
			
			
			Phrase dess=new Phrase("RECEIVED FROM M/S."+" "+custpay.getPersonInfo().getFullName().toUpperCase()+" "+"A SUM OF RS."+" "+amtInFigure.toUpperCase()+" ONLY"+" "+"BY CHEQUE NO. "+chno+" "+"DRAWN ON THE"+" "+bankname+" "+"AS FULL PAYMENT OF BILL NO."+" "+billno,font10);
			
			Phrase para=new Phrase("FOR"+"  "+comp.getBusinessUnitName().toUpperCase(),font10);
			PdfPCell paracell=new PdfPCell(para);
			paracell.setBorder(0);
			paracell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			Phrase para1=new Phrase("(AUTHORISED SIGNATORY)",font10bold);
			PdfPCell para1cell=new PdfPCell(para1);
			para1cell.setBorder(0);
			para1cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			
			
			double amount=0;
			
			amount=custpay.getPaymentReceived();
			
			Phrase amountpara=new Phrase("Rs. "+" "+amount+"",font10);
			PdfPCell amountcell=new PdfPCell(amountpara);
			amountcell.setBorder(0);
			amountcell.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			String parastr="SUBJECT TO REALISATION OF CHEQUE";
			Phrase amountpara1=new Phrase(parastr,font10);
			PdfPCell amountcell1=new PdfPCell(amountpara1);
			amountcell1.setBorder(0);
			amountcell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			
			
			Phrase blank=new Phrase("   ",font10);
			PdfPCell blankcell=new PdfPCell(blank);
			blankcell.setBorder(0);
			
			
			PdfPTable paratable=new PdfPTable(2);
			paratable.setWidthPercentage(100);
//			paratable.setHorizontalAlignment(Element.ALIGN_RIGHT);
			paratable.addCell(blankcell);
			paratable.addCell(paracell);
			paratable.addCell(blankcell);
			paratable.addCell(blankcell);
			paratable.addCell(blankcell);
			paratable.addCell(blankcell);
			paratable.addCell(amountcell);
			paratable.addCell(blankcell);
			paratable.addCell(amountcell1);
			paratable.addCell(para1cell);
			
			
			
			try {
				document.add(titletable);
				document.add(Chunk.NEWLINE);
				document.add(dess);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				document.add(Chunk.NEWLINE);
				document.add(paratable);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			}
		
		}
	//**************************xxxxxx*****************	
		
		



}
