package com.slicktechnologies.server.addhocprinting.pecopp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;
import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;

public class createCustomPaySlipPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8099169443742956580L;

	@Override
	 protected void doGet(HttpServletRequest request,
	   HttpServletResponse response) throws ServletException, IOException {
	  
		 response.setContentType("application/pdf");  // Type of response , helps browser to identify the response type.
	  
		 System.out.println("inside new ");
		 
	  try {
		  String stringid=request.getParameter("Id");
		   stringid=stringid.trim();
		   
		   String preprintStatus=request.getParameter("preprint");
		   
		   Long count =Long.parseLong(stringid);
		   CustomerPayment custPayEntity=ofy().load().type(CustomerPayment.class).id(count).now();
		   
			   
		   	   customPaySlipPdf custpaypdf=new customPaySlipPdf();
			   custpaypdf.document=new Document();
			   Document document = custpaypdf.document;
			   PdfWriter writer=PdfWriter.getInstance(document, response.getOutputStream()); // write the pdf in response
			   
			   /**
				 * Added by Rahul on 16 June 2017
				 * this will add waterMark in Pdf if pdf is in created state
				 */
			   if(custPayEntity.getStatus().equals(CustomerPayment.CANCELLED)){//By jayshree Date 16/11/2017 Changes are made to add the cancelled watermark
				   
				   writer.setPageEvent(new PdfCancelWatermark());	
			   }
			   
			   if(!custPayEntity.getStatus().equals("Closed")){
				   writer.setPageEvent(new PdfWatermark());	
			   }
			   document.open();
			   
			   custpaypdf.setCustomerPaySlip(count);
			   custpaypdf.createPdf(preprintStatus);
			   document.close();
			   
		   
	  
	  } 
	  catch (DocumentException e) {
		  e.printStackTrace();
	  }
	 
	   }

	
}
