package com.slicktechnologies.server.addhocprinting.pecopp;




import static com.googlecode.objectify.ObjectifyService.ofy;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.slicktechnologies.server.addhocprinting.PdfCancelWatermark;

import com.slicktechnologies.server.addhocprinting.PdfWatermark;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.salesprocess.Invoice;


/**
 * 
 * @author Jayshree
 * Description :For Pecopp Pdf : It will print Invoice and Contract Pdf of Pecopp
 * Called From : ContractForm and InvoiceForm
 * Date: 10 Spet 2017
 * Created By : Jayshree Chavhan
 *
 */
public class PecoppPdfServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3159490300847278065L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("application/pdf");
		try {
			String pdfType = req.getParameter("type");
			String stringId = req.getParameter("Id");
			stringId = stringId.trim();
			Long count = Long.parseLong(stringId);
			String preprintStatus=req.getParameter("preprint");
			
			if(pdfType.equals("Contract"))
			{
				Contract con = ofy().load().type(Contract.class).id(count).now();
				
				PecoppContractPdfPrint printpdf = new PecoppContractPdfPrint();
				printpdf.document = new Document();
				Document document = printpdf.document;
				PdfWriter writer = PdfWriter.getInstance(document,resp.getOutputStream());
				/**
				 * Date 11/10/2017
				 * By. jayshree 
				 * changes are made to add the wotermark in contrcat pdf
				 */
				if(con.getStatus().equals("Created"))
				{
					writer.setPageEvent(new PdfWatermark());
				}
				else if(con.getStatus().equals("Cancelled")){
						writer.setPageEvent(new PdfCancelWatermark());
					}
				
				document.open();
				printpdf.loadpecopp(count);
				printpdf.createPrintPdf(preprintStatus);
				document.close();
				
				
				
		
			}
			Invoice invoiceentity=ofy().load().type(Invoice.class).id(count).now();
			
			 if(pdfType.equals("Invoice")){
				PecoppInvoicePrintPdf invoicePdf = new PecoppInvoicePrintPdf();
				invoicePdf.document = new Document();
				Document document = invoicePdf.document;
				PdfWriter writer=PdfWriter.getInstance(document, resp.getOutputStream()); // write
				/**
				 * Date 11/10/2017
				 * By. jayshree 
				 * changes are made to add the watermark in invoice pdf
				 */
				if(invoiceentity.getStatus().equals("Created"))
					writer.setPageEvent(new PdfWatermark());
				
				else if(invoiceentity.getStatus().equals("Cancelled")){
					writer.setPageEvent(new PdfCancelWatermark());
				}
				document.open();
				invoicePdf.loadPecoppInvoice(count);
				//invoicePdf.createInvoicePdf();
				invoicePdf.createInvoicePdf(preprintStatus);
				document.close();
				
				
//				else
//			   if(!invoiceentity.getStatus().equals("Approved")){	
//					 writer.setPageEvent(new PdfWatermark());
//				} 
				
			
		}
			
		}
		
			catch (DocumentException e) {
				e.printStackTrace();
			}
		
		}
	
	
}
	
	





