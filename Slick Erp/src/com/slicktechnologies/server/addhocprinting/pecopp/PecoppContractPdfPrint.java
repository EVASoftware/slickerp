package com.slicktechnologies.server.addhocprinting.pecopp;


import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.client.views.contract.SalesLineItemTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;


/**
 * 
 * @author Jayshree 
 * Description :For Pecopp Pdf : It will print Contract Pecopp
 * Called From : PecoppPdfServlet.java on print of Contract 
 * Date: 10 Sept 2017 
 * Created By : Jayshree Chavhan
 */
public class PecoppContractPdfPrint {


Logger logger = Logger.getLogger("ServiceGSTInvoice.class");
public Document document;
Contract con;
Customer cust;
Company comp;
PurchaseOrder po;
double totalamt;
String product1="";
String product2="";
Invoice invoice;
boolean consolidatePrice = false;//Added by Ashwini
/**
 * Date 27/11/207
 * dev.By jayshree
 * Des.increse the no of line to adjust the seven product in first page
 */
int noOfLine=7,productCount=0;
//End by jayshree
private Font font16boldul, font12bold, font8bold, font8, font9bold,
		font12Wbold, font12boldul, font10boldul, font12, font16bold,
		font10, font10bold, font10ul, font9boldul, font14bold, font9,
		font7, font7bold, font9red, font9boldred, font12boldred,font11, font23;

private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
private SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
DecimalFormat df = new DecimalFormat("0.00");

List<ContractCharges> billingTaxesLis; // ajinkya added this 03/07/2017
List<CustomerBranchDetails> custbranchlist;
List<CustomerBranchDetails> customerbranchlist;
List<PaymentTerms> payTermsLis;

ProcessConfiguration processConfig;
List<State> stateList;
boolean productDescFlag = false, upcflag = false, UniversalFlag = false,
		multipleCompanyName = false, printPremiseDetails = false;

List<BillingDocument> billList;
SimpleDateFormat sdf;
/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
//Branch companyBranch = null;

/**
 *  Date : 11-03-2021 by Priyanka
 *  Des : - BranchAsCompany Process configuration activation.
 */
Branch branchDt = null;
CompanyPayment comppayment, comppayment1;
CustomerBranchDetails customerBranch=null;

private SimpleDateFormat datetimeformat = new SimpleDateFormat("dd/MM/yyyy HH:MM:SS");

float[] columnHalfWidth = { 1f, 1f };


public PecoppContractPdfPrint() {
	super();

	font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
			| Font.UNDERLINE);
	font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
	font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
	font8bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	font8 = new Font(Font.FontFamily.HELVETICA, 7);
	font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
			| Font.UNDERLINE);
	font12 = new Font(Font.FontFamily.HELVETICA, 12);
	
	font11 = new Font(Font.FontFamily.HELVETICA, 11);
	font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
			BaseColor.WHITE);
	font10 = new Font(Font.FontFamily.HELVETICA, 9);
	font10bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
	font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL
			| Font.UNDERLINE);
	font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
	font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
			| Font.UNDERLINE);
	font9bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
	font9boldul = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD
			| Font.UNDERLINE);
	font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,
			BaseColor.RED);
	font9 = new Font(Font.FontFamily.HELVETICA, 8);
	font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,
			BaseColor.RED);
	font7 = new Font(Font.FontFamily.HELVETICA, 7);
	font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
	font23 = new Font(Font.FontFamily.HELVETICA, 23);
	font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
			BaseColor.WHITE);

	fmt.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	sdf=new SimpleDateFormat("dd/MM/yyyy");
	sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	
}

public void loadpecopp(long count) {
	// TODO Auto-generated method stub
	con = ofy().load().type(Contract.class).id(count).now();
	//
	// comp =ofy().load().type(Company.class).filter("companyId",
	// con.getCompanyId()).first().now();
	//
	//
	cust = ofy().load().type(Customer.class)
			.filter("companyId", con.getCompanyId())
			.filter("count", con.getCustomerId()).first().now();
	// Load Company
	if (con.getCompanyId() == null)
		comp = ofy().load().type(Company.class).first().now();
	else
		comp = ofy().load().type(Company.class)
				.filter("companyId", con.getCompanyId()).first().now();

	
	
	/************************************ Letter Head Flag *******************************/

	if (con.getCompanyId() != null) {
		processConfig = ofy().load().type(ProcessConfiguration.class)
				.filter("companyId", con.getCompanyId())
				.filter("processName", "Contract")
				.filter("configStatus", true).first().now();
		if (processConfig != null) {
			for (int k = 0; k < processConfig.getProcessList().size(); k++) {
				if (processConfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("CompanyAsLetterHead")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					upcflag = true;
				}

				if (processConfig.getProcessList().get(k).getProcessType()
						.trim().equalsIgnoreCase("OnlyForUniversal")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					UniversalFlag = true;
				}

				if (processConfig
						.getProcessList()
						.get(k)
						.getProcessType()
						.trim()
						.equalsIgnoreCase(
								"PrintMultipleCompanyNamesFromInvoiceGroup")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					multipleCompanyName = true;
				}

				if (processConfig.getProcessList().get(k).getProcessType()
						.trim()
						.equalsIgnoreCase("printProductPremisesInPdf")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					printPremiseDetails = true;
				}

				if (processConfig.getProcessList().get(k).getProcessType()
						.trim()
						.equalsIgnoreCase("PrintProductDescriptionOnPdf")
						&& processConfig.getProcessList().get(k).isStatus() == true) {
					productDescFlag = true;
				}
			}
		}
	}
	/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
//	if(con.getBranch() != null){
//		if(con.getCompanyId() != null){
//			branchDt = ofy().load().type(Branch.class).filter("companyId", con.getCompanyId())
//					 		.filter("buisnessUnitName", con.getBranch()).first().now();
//		}else{
//			branchDt = ofy().load().type(Branch.class).filter("buisnessUnitName", con.getBranch()).first().now();
//		}
//	}
	
	
	ArrayList<String> custbranchlist=getCustomerBranchList(con.getItems());
	
	if(custbranchlist!=null&&custbranchlist.size()==1&& custbranchlist.contains("Service Address")==false){
			logger.log(Level.SEVERE,"In Side AList1:");
			customerBranch= ofy().load().type(CustomerBranchDetails.class)
						.filter("cinfo.count",con.getCinfo().getCount())
						.filter("companyId", con.getCompanyId())
						.filter("buisnessUnitName", custbranchlist.get(0)).first().now();
			
			logger.log(Level.SEVERE,"AList1:" +customerBranch);
			logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());
		}
	
	
	/**
	 *  Date : 22-03-2021 Added by Priyanka.
	 *  Des : BranchAsProcessConfiguration active.
	 */
	
	if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
		
		logger.log(Level.SEVERE,"Process active --");
		if(con !=null && con.getBranch() != null && con.getBranch().trim().length() >0){
			
			branchDt = ofy().load().type(Branch.class).filter("companyId",con.getCompanyId()).filter("buisnessUnitName", con.getBranch()).first().now();
			
		
			if(branchDt!=null){
				logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
				
				if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
					
				
				List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
				
				if(paymentDt.get(0).trim().matches("[0-9]+")){
					
					
					
					int payId = Integer.parseInt(paymentDt.get(0).trim());
					
					comppayment = ofy().load().type(CompanyPayment.class)
							.filter("count", payId)
							.filter("companyId", con.getCompanyId()).first()
							.now();
					
					
					if(comppayment != null){
						
						}
					
					}
				}
				
				comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
			}
		}
	}

	
	
	
}

private ArrayList<String> getCustomerBranchList(List<SalesLineItem> itemList) {
	HashSet<String> branchHs=new HashSet<String>();
	for(SalesLineItem itemObj:itemList){
		if(itemObj.getCustomerBranchSchedulingInfo()!=null){
			ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
			for(BranchWiseScheduling obj:branchSchedulingList){
				if(obj.isCheck()==true){
					branchHs.add(obj.getBranchName());
				}
			}
		}
	}
	
	if(branchHs!=null&&branchHs.size()!=0){
		ArrayList<String> branchList=new ArrayList<String>(branchHs);
		logger.log(Level.SEVERE,"In Side AList3:"+branchList.size());
		return branchList;
	}
	
	return null;
}
// Added By Priyanka
public void createPrintPdf(String preprintStatus) {
	
	if(upcflag==false && preprintStatus.equals("plane")){
		//createBlankforUPC();
		createLogo(document, comp);
		createCompanyHeadding();
		}
	else{
		if(preprintStatus.equals("yes")){
				createBlankforUPC();
			}
		
		if(preprintStatus.equals("no")){
		System.out.println("inside prit no");
		if(comp.getUploadHeader()!=null){
			createCompanyNameAsHeader(document,comp);
			}
			
		if(comp.getUploadFooter()!=null){
			createCompanyNameAsFooter(document,comp);
			}
		  }
	   }
	
	createPrintSample();
}

public void createPrintSample() {
	 
//	 if (comp.getUploadHeader() != null) {
//	 createCompanyNameAsHeader(document, comp);
//	 }
//	 if (comp.getUploadFooter() != null) {
//	 createCompanyNameAsFooter(document, comp);
//	 }
	//Added By Priyanka
	if(upcflag) {
	createBlankforUPC();
	}
	createHeaderTable();
	createContractHeaderTable();
	createcustomerDetailTable();
	createPerioddetailtab();
	createProductHeadertab();
	createConProductDetail();
	createconTaxDetailTable();
	createComTaxTable();
	createConPaymentTable();
	
	createFooterTable();
	//createRestProductAnnexure(9);
	createRestProductDetail();
}

/**
 *   Added By Priyanka 
 *   When Branch As process configuration Active then company heading and logo should be print
 */

public void createCompanyHeadding() {
	
	String companyname ="";
	if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
		companyname=" "+branchDt.getCorrespondenceName();
	}else{
	 companyname = " "+comp.getBusinessUnitName().trim().toUpperCase();
	}

	Paragraph companynamepara = new Paragraph();
	companynamepara.add(companyname);
	companynamepara.setFont(font14bold);
	companynamepara.setAlignment(Element.ALIGN_CENTER);

	PdfPCell companynamecell = new PdfPCell();
	companynamecell.addElement(companynamepara);
	companynamecell.setBorder(0);
	

	
	String custAdd1="";
	String custFullAdd1="";
	
	if(comp.getAddress()!=null){
		
		if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
		
			if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
				custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
			}else{
				custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
			}
		}else{
			if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
				custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
			}else{
				custAdd1=comp.getAddress().getAddrLine1();
			}
		}
		
		if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
			if(comp.getAddress().getPin()!=0){
				custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
			}else{
				custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
			}
		}else{
			if(comp.getAddress().getPin()!=0){
				custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry()+","+comp.getAddress().getPin();
			}else{
				custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
			}
		}
		
	}	
	
		Phrase addressline=new Phrase(custFullAdd1,font11);

		Paragraph addresspara=new Paragraph();
		addresspara.add(addressline);
		addresspara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell addresscell=new PdfPCell();
		addresscell.addElement(addresspara);
		addresscell.setBorder(0);
	   
	
	
	String contactinfo="";
	if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
		contactinfo =  "Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim();
	}
	//Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim(),font10);
	
	Paragraph realmobpara=new Paragraph();
	realmobpara.add(contactinfo);
	realmobpara.setFont(font11);
	realmobpara.setAlignment(Element.ALIGN_CENTER);
	
	PdfPCell contactcell=new PdfPCell();
	contactcell.addElement(realmobpara);
	contactcell.setBorder(0);
	
	PdfPTable table = new PdfPTable(1);
	table.addCell(companynamecell);
	table.addCell(addresscell);
//	table.addCell(localitycell);
	table.addCell(contactcell);
	table.setWidthPercentage(100f);
	table.setSpacingAfter(10f);
	
	PdfPCell cell = new PdfPCell();
	cell.addElement(table);
	cell.setBorderWidthLeft(0);
	cell.setBorderWidthRight(0);
	cell.setBorderWidthTop(0);
	
	PdfPTable parenttable = new PdfPTable(1);
	parenttable.addCell(cell);
	parenttable.setWidthPercentage(100);
	


try {
	document.add(parenttable);
} catch (DocumentException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}

}


private void createLogo(Document doc, Company comp) {

//********************logo for server ********************

	DocumentUpload document =comp.getLogo();
	//patch
	String hostUrl; 
	String environment = System.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
	    String applicationId = System.getProperty("com.google.appengine.application.id");
	    String version = System.getProperty("com.google.appengine.application.version");
	    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
	} else {
	    hostUrl = "http://localhost:8888";
	}
	
	try {
		Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
		image2.scalePercent(20f);
		image2.setAbsolutePosition(40f,765f);	
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}
}


/**
 *  End
 */







private void createHeaderTable() {
PdfPTable table = new PdfPTable(1);
table.setWidthPercentage(100f);
table.setSpacingAfter(20);

String title;
if (con.getCategory().equalsIgnoreCase("residential")) {
	title = "INVOICE CUM SERVICE CONTRACT";
} else {
	title = "SERVICE CONTRACT"; // added by Priyanka.
}

Phrase headPh = new Phrase(title, font10bold);
PdfPCell headCell = new PdfPCell(headPh);
// headCell.addElement(headPh);//Comment by jayshree Date 25/11/2017
headCell.setBorder(0);
headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
headCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
table.addCell(headCell);

	try {
		document.add(table);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

private void createContractHeaderTable() {
	// TODO Auto-generated method stub

	PdfPTable contab = new PdfPTable(3);
	contab.setWidthPercentage(100f);

	try {
		contab.setWidths(new float[] { 25, 5, 70 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	Phrase conphrase = new Phrase("Contract No", font9bold);
	PdfPCell conCell = new PdfPCell(conphrase);
//	conCell.addElement(conphrase);//Comment by jayshree Date 25/11/2017
	conCell.setBorder(0);
	conCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	//conCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	conCell.setPaddingTop(3f); // added by Priyanka
	conCell.setPaddingBottom(3f);
	contab.addCell(conCell);

	Phrase colPhrase1 = new Phrase(":", font9bold);
	PdfPCell colCell1 = new PdfPCell(colPhrase1);
//	colCell1.addElement(colPhrase1);
	colCell1.setBorder(0);
	colCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	colCell1.setPaddingTop(3f);
	colCell1.setPaddingBottom(3f);
	contab.addCell(colCell1);

	// Phrase conValPhrase =new Phrase("",font8bold);
	Phrase conValPhrase = null;
	if (con.getCount() != 0) {
		conValPhrase = new Phrase(con.getCount() + "", font9);
	} else {
		conValPhrase = new Phrase(" ", font10);
	}
	PdfPCell conValCell = new PdfPCell(conValPhrase);
//	conValCell.addElement(conValPhrase);//Comment by jayshree Date 25/11/2017
	conValCell.setBorder(0);
	conValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	conValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	conValCell.setPaddingTop(3f);
	conValCell.setPaddingBottom(3f);
	contab.addCell(conValCell);

	//
/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/
//	Phrase billPhrase = new Phrase("Bill No", font9bold);
//	PdfPCell billCell = new PdfPCell(billPhrase);
////	billCell.addElement(billPhrase);//Comment by jayshree Date 25/11/2017
//	billCell.setBorder(0);
//	billCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	billCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	contab.addCell(billCell);

	Phrase colPhrase3 = new Phrase(":", font9bold);
	PdfPCell colCell3 = new PdfPCell(colPhrase3);
//	colCell3.addElement(colPhrase3);//Comment by jayshree Date 25/11/2017
	colCell3.setBorder(0);
	colCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	contab.addCell(colCell3);

	/**
	 ******* Date 05/09/2017 By jayshree add the billing no in contract pdf
	 */
	billList = ofy().load().type(BillingDocument.class)
			.filter("companyId", comp.getCompanyId())
			.filter("contractCount", con.getCount()).list();
	
	invoice = ofy().load().type(Invoice.class)
			.filter("companyId", comp.getCompanyId())
			.filter("contractCount", con.getCount()).first().now();

	String billId;
//	if (billList.size() != 0) {
//		billId = billList.get(0).getCount() + "";
//	} else {
//		billId = "";
//
//	}
	if(invoice!=null){
		billId = invoice.getCount() + "";
	}else{
		billId="";
	}
	Phrase billvaluePhrase2 = new Phrase(billId, font9);
	PdfPCell billValueCell2 = new PdfPCell(billvaluePhrase2);
//	billValueCell2.addElement(billvaluePhrase2);//Comment by jayshree Date 25/11/2017
	billValueCell2.setBorder(0);
	billValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
	billValueCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	contab.addCell(billValueCell2);

	PdfPTable datetab = new PdfPTable(3);
	datetab.setWidthPercentage(100f);

	try {
		datetab.setWidths(new float[] { 15, 5, 80 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	Phrase datePhrase = new Phrase("Date", font9bold);
	PdfPCell dateCell = new PdfPCell(datePhrase);
//	dateCell.addElement(datePhrase);//Comment by jayshree Date 25/11/2017
	dateCell.setBorder(0);
	dateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	dateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	dateCell.setPaddingTop(3f);
	dateCell.setPaddingBottom(3f);
	datetab.addCell(dateCell);

	Phrase colPhrase2 = new Phrase(":", font9);
	PdfPCell colCell2 = new PdfPCell(colPhrase2);
//	colCell2.addElement(colPhrase2);//Comment by jayshree Date 25/11/2017
	colCell2.setBorder(0);
	colCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
	colCell2.setPaddingTop(3f);
	colCell2.setPaddingBottom(3f);
	datetab.addCell(colCell2);

	// Phrase datPhrase =new Phrase("",font8bold);
	Phrase datPhrase = null;
	if (con.getContractDate() != null) {
		datPhrase = new Phrase(fmt2.format(con.getContractDate()), font9);
	} else {
		datPhrase = new Phrase(" ", font10);
	}
	PdfPCell datCell = new PdfPCell(datPhrase);
//	datCell.addElement(datPhrase);//Comment by jayshree Date 25/11/2017
	datCell.setBorder(0);
	datCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	datCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	datCell.setPaddingTop(3f);
	datCell.setPaddingBottom(3f);
	datetab.addCell(datCell);

	Phrase contractTypePhrase = new Phrase("", font9);
	PdfPCell contractTypeCell = new PdfPCell(contractTypePhrase);
//	contractTypeCell.addElement(contractTypePhrase);//Comment by jayshree Date 25/11/2017
	contractTypeCell.setBorder(0);
	contractTypeCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	contractTypeCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	datetab.addCell(contractTypeCell);

	Phrase colPhrase4 = new Phrase("", font9bold);
	PdfPCell colCell4 = new PdfPCell(colPhrase4);
//	colCell4.addElement(colPhrase4);//Comment by jayshree Date 25/11/2017
	colCell4.setBorder(0);
	colCell4.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
	datetab.addCell(colCell4);

	Phrase newPhrase = new Phrase("", font9bold);
	PdfPCell newCell = new PdfPCell(newPhrase);
//	newCell.addElement(newPhrase);//Comment by jayshree Date 25/11/2017
	newCell.setBorder(0);

	newCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	newCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	datetab.addCell(newCell);

	PdfPTable bordertab = new PdfPTable(2);
	bordertab.setWidthPercentage(100f);

	try {
		bordertab.setWidths(new float[] { 50, 50 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	PdfPCell outcell = new PdfPCell(contab);
//	outcell.addElement(contab);//Comment by jayshree Date 25/11/2017
	bordertab.addCell(outcell);
	;

	PdfPCell outcell2 = new PdfPCell(datetab);
//	outcell2.addElement(datetab);//Comment by jayshree Date 25/11/2017
	bordertab.addCell(outcell2);

	try {
		document.add(bordertab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	PdfPTable headAdd = new PdfPTable(2);
	headAdd.setWidthPercentage(100f);

	try {
		headAdd.setWidths(new float[] { 50, 50 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	Phrase billadd = new Phrase("Billing Address", font9bold);
	PdfPCell billaddCell = new PdfPCell(billadd);
	billaddCell.setVerticalAlignment(Element.ALIGN_CENTER);
	billaddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	headAdd.addCell(billaddCell);

	Phrase serviceadd = new Phrase("Service Address", font9bold);
	PdfPCell serviceCell = new PdfPCell(serviceadd);
	
	serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	serviceCell.setVerticalAlignment(Element.ALIGN_CENTER);
	headAdd.addCell(serviceCell);

	try {
		document.add(headAdd);
	} catch (DocumentException e1) {
		e1.printStackTrace();

	}

}

private void createcustomerDetailTable() {
	// TODO Auto-generated method stub
	
	// //

	PdfPTable customerDetailTable2 = new PdfPTable(3);
	customerDetailTable2.setWidthPercentage(100f);
	// customerDetailTab1.setSpacingBefore(2f);

	try {
		customerDetailTable2.setWidths(new float[] {25, 3, 75 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	//Date 16/11/2017 By jayshree
	// add the reference no and Value
/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/
//	Phrase refno=new Phrase ("Ref No",font9);
//	PdfPCell refnoCell=new PdfPCell(refno);
//	refnoCell.setBorder(0);
//	refnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	refnoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	customerDetailTable2.addCell(refnoCell);
	
	Phrase colPhrase6 = new Phrase(":", font9);
	PdfPCell colCell6 = new PdfPCell(colPhrase6);
//	colCell6.addElement(colPhrase6);
	colCell6.setBorder(0);
	colCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
//	colCell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	customerDetailTable2.addCell(colCell6);
	
	/**
	 * Date 25/11/2017
	 * Dev. Jayshree
	 * Des:to set the ref no and ref date
	 * 
	 */
/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/	
//	PdfPTable refcomtab=new PdfPTable(4);
//	refcomtab.setWidthPercentage(100);
//	
//	try {
//		refcomtab.setWidths(new float[]{40,25,3,32});
//	} catch (DocumentException e2) {
//		// TODO Auto-generated catch block
//		e2.printStackTrace();
//	}
//	
//	Phrase refnoValue=null;
//	if(con.getReferenceNo()!=null){
//	refnoValue=new Phrase (con.getRefNo(),font9);
//	}
//	else{
//		refnoValue=new Phrase (" ",font9);
//	}
	
	
//	PdfPCell refnoCellValue=new PdfPCell(refnoValue);
//	refnoCellValue.setBorder(0);
//	refnoCellValue.setHorizontalAlignment(Element.ALIGN_LEFT);
//	refnoCellValue.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	refcomtab.addCell(refnoCellValue);
/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/	
//	Phrase refDate=new Phrase ("Ref Date",font9);
//	PdfPCell refDateCell=new PdfPCell(refDate);
//	refDateCell.setBorder(0);
//	refDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	refDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	refcomtab.addCell(refDateCell);
//	
//	refcomtab.addCell(colCell6);
//	
//	Phrase refDateVal=null;
//	if(con.getRefDate()!=null){
//	 refDateVal=new Phrase (fmt.format(con.getRefDate()),font9);
//	}
//	else{
//		
//		refDateVal=new Phrase (" ");
//	}
//	PdfPCell refDateValCell=new PdfPCell(refDateVal);
//	refDateValCell.setBorder(0);
//	refDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	refDateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	refcomtab.addCell(refDateValCell);
//	
//	PdfPCell refComCell=new PdfPCell(refcomtab);
//	refComCell.setBorder(0);
//	customerDetailTable2.addCell(refComCell);
	//End By Jayshree
	String gstin = "", panno = "";
	for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {

		if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
				.equalsIgnoreCase("GSTIN")) {
			gstin = cust.getArticleTypeDetails().get(i)
					.getArticleTypeValue();
		}

		if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
				.equalsIgnoreCase("PAN NO")) {
			panno = cust.getArticleTypeDetails().get(i)
					.getArticleTypeValue();
		}
	}
	Phrase gstinPhrase = null;
	gstinPhrase = new Phrase("GSTIN", font9);
	PdfPCell gstinCell = new PdfPCell(gstinPhrase);
//	gstinCell.addElement(gstinPhrase);//Comment by jayshree Date 25/11/2017
	gstinCell.setBorder(0);
	gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	gstinCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable2.addCell(gstinCell);
//
	
	customerDetailTable2.addCell(colCell6);
////
	Phrase gstinValuePhrase = null;
	gstinValuePhrase = new Phrase(gstin, font9);
	PdfPCell gstinValueCell = new PdfPCell(gstinValuePhrase);
//	gstinValueCell.addElement(gstinValuePhrase);//Comment by jayshree Date 25/11/2017
	gstinValueCell.setBorder(0);
	gstinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	gstinValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable2.addCell(gstinValueCell);

	Phrase panNoPhrase = null;
	panNoPhrase = new Phrase("PAN NO", font9);
	PdfPCell panNoCell = new PdfPCell(panNoPhrase);
//	panNoCell.addElement(panNoPhrase);//Comment by jayshree Date 25/11/2017
	panNoCell.setBorder(0);
	panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	panNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable2.addCell(panNoCell);
//
	Phrase colPhrase101 = new Phrase(":", font9);
	PdfPCell colCell101 = new PdfPCell(colPhrase101);
//	colCell101.addElement(colPhrase101);//Comment by jayshree Date 25/11/2017
	colCell101.setBorder(0);
	colCell101.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell101.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable2.addCell(colCell101);
////
	Phrase panNoValuePhrase = null;
	panNoValuePhrase = new Phrase(panno, font9);
	PdfPCell panNoValueCell = new PdfPCell(panNoValuePhrase);
//	panNoValueCell.addElement(panNoValuePhrase);//Comment by jayshree Date 25/11/2017
	panNoValueCell.setBorder(0);
	panNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	panNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable2.addCell(panNoValueCell);
      
	
	
	
	
	
	// //

	Phrase mobileNoPhrase = new Phrase("Mobile", font9);
	PdfPCell mobileNoCell = new PdfPCell();
	mobileNoCell.addElement(mobileNoPhrase);
	mobileNoCell.setBorder(0);
	mobileNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	mobileNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	//customerDetailTable2.addCell(mobileNoCell);

	Phrase colPhrase15 = new Phrase(":", font9);
	PdfPCell colCell15 = new PdfPCell();
	colCell15.addElement(colPhrase15);
	colCell15.setBorder(0);
	colCell15.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell15.setVerticalAlignment(Element.ALIGN_MIDDLE);
	//customerDetailTable2.addCell(colCell15);

	// Phrase mobileNoValuePhrase=new Phrase("9820835447",font7);
	Phrase mobileNoValuePhrase = null;
	if (con.getCustomerCellNumber() != 0) {
		mobileNoValuePhrase = new Phrase(con.getCustomerCellNumber() + "",
				font8);
	} else {
		mobileNoValuePhrase = new Phrase(" ", font9);
	}
	PdfPCell mobileNoValueCell = new PdfPCell();
	mobileNoValueCell.addElement(mobileNoValuePhrase);
	mobileNoValueCell.setBorder(0);
	mobileNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	mobileNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	//customerDetailTable2.addCell(mobileNoValueCell);
//	// //
	
//	PdfPTable refcomtab1=new PdfPTable(1);
//	refcomtab1.setWidthPercentage(100);
//	
//	try {
//		refcomtab1.setWidths(new float[]{100});
//	} catch (DocumentException e2) {
//		// TODO Auto-generated catch block
//		e2.printStackTrace();
//	}
	
	
    String telNovalue =" ";
	if (cust.getLandline() !=null && cust.getLandline() != 0) {
		telNovalue = cust.getLandline() + "";
	} 
	
	Phrase telNoPhrase1 = new Phrase("Tel. No  : "+telNovalue, font9);

	PdfPCell telNoCell1 = new PdfPCell();
	telNoCell1.addElement(telNoPhrase1);
	telNoCell1.setBorder(0);
	telNoCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	telNoCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	if(telNovalue!=null&&!telNovalue.equals("")) {
		customerDetailTable2.addCell(telNoCell1);
	}
	
	
//	PdfPCell refComCell=new PdfPCell(refcomtab1);
//	refComCell.setBorder(0);
//	customerDetailTable2.addCell(refComCell);
//	
//	Phrase colPhrase12 = new Phrase(":", font9);
//	PdfPCell colCell12 = new PdfPCell();
//	colCell12.addElement(colPhrase12);
//	colCell12.setBorder(0);
//	colCell12.setHorizontalAlignment(Element.ALIGN_LEFT);
//	colCell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	customerDetailTable2.addCell(colCell12);

	// Phrase telNovaluePhrase1=new Phrase("Tel.No.");
//	Phrase telNovaluePhrase1 = null;
//	if (cust.getLandline() != 0) {
//		telNovaluePhrase1 = new Phrase(cust.getLandline() + "", font9);
//	} else {
//		telNovaluePhrase1 = new Phrase(" ", font9);
//	}
//	PdfPCell telNovalueCell1 = new PdfPCell();
//	telNovalueCell1.addElement(telNovaluePhrase1);
//	telNovalueCell1.setBorder(0);
//	telNovalueCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//	telNovalueCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	if (cust.getLandline() != 0&&cust.getLandline()!=null) {
//	customerDetailTable2.addCell(telNovalueCell1);
//	}
	// /

	PdfPTable customerDetailTable3 = new PdfPTable(3);//6
	customerDetailTable3.setWidthPercentage(100f);
	// customerDetailTab1.setSpacingBefore(2f);
/**
 * Date 27/11/2017
 * Dev.Jayshree
 * To set the column width changes are made
 */
	try {
		customerDetailTable3.setWidths(new float[] {25, 3, 75 });//25, 3, 75//24, 3, 28, 20, 2, 23
		//end By jayshree
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	Phrase mobileNoPh = new Phrase("Mobile", font9);//25/11/2017
	PdfPCell mobileNoC = new PdfPCell(mobileNoPh);
//	mobileNoC.addElement(mobileNoPh);//Comment by jayshree Date 25/11/2017
	mobileNoC.setBorder(0);
	mobileNoC.setHorizontalAlignment(Element.ALIGN_LEFT);
	mobileNoC.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable3.addCell(mobileNoC);

	Phrase colPhrase16 = new Phrase(":", font9);
	PdfPCell colCell16 = new PdfPCell(colPhrase16);
//	colCell16.addElement(colPhrase16);//Comment by jayshree Date 25/11/2017
	colCell16.setBorder(0);
	colCell16.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable3.addCell(colCell16);

	// Phrase mobileNoValuePhrase=new Phrase("9820835447",font7);
	
	
	Phrase mobileNoValuePh = null;
	if (con.getCustomerCellNumber() != 0) {
		mobileNoValuePh = new Phrase(con.getCustomerCellNumber() + "",
				font9);
	} else {
		mobileNoValuePh = new Phrase(" ", font9);
	}
	
//	Phrase mobileNoValuePh = null;
//	if(customerBranch!=null){
//		if (customerBranch.getCellNumber1() != 0) {
//			mobileNoValuePh = new Phrase(customerBranch.getCellNumber1() + "",
//					font9);
//		} else {
//			mobileNoValuePh = new Phrase(" ", font9);
//		}	
//	}else{
//		if (con.getCustomerCellNumber() != 0) {
//			mobileNoValuePh = new Phrase(con.getCustomerCellNumber() + "",
//					font9);
//		} else {
//			mobileNoValuePh = new Phrase(" ", font9);
//		}	
//	}
	
	PdfPCell mobileNoValueC = new PdfPCell(mobileNoValuePh);
//	mobileNoValueC.addElement(mobileNoValuePh);//Comment by jayshree Date 25/11/2017
	mobileNoValueC.setBorder(0);
	mobileNoValueC.setHorizontalAlignment(Element.ALIGN_LEFT);
//	mobileNoValueC.setVerticalAlignment(Element.ALIGN_MIDDLE);
	customerDetailTable3.addCell(mobileNoValueC);
	// //
/** Commented By Priyanka - Pecopp dont need this on pdf as per new format**/
	
//	PdfPTable refcomtab11=new PdfPTable(1);
//	refcomtab1.setWidthPercentage(100);
//	
//	try {
//		refcomtab11.setWidths(new float[]{100});
//	} catch (DocumentException e2) {
//		// TODO Auto-generated catch block
//		e2.printStackTrace();
//	}
//	
	
//    String telNovalue1 =" ";
//	if (cust.getLandline() !=null && cust.getLandline() != 0) {
//		telNovalue1 = cust.getLandline() + "";
//	} 
//	
//	Phrase telNoPhrase11 = new Phrase("Tel. No  : "+telNovalue1, font9);
//
//	PdfPCell telNoCell11 = new PdfPCell();
//	telNoCell11.addElement(telNoPhrase11);
//	telNoCell11.setBorder(0);
//	telNoCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
//	telNoCell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	if(telNovalue!=null&&!telNovalue.equals("")) {
//		customerDetailTable3.addCell(telNoCell11);
//	}
	
	
//	PdfPCell refComCell1=new PdfPCell(refcomtab11);
//	refComCell.setBorder(0);
//	customerDetailTable3.addCell(refComCell1);
	
	
	
	
	Phrase telNoPh = new Phrase("Tel. No", font9);

	PdfPCell telNoC = new PdfPCell(telNoPh);
//	telNoC.addElement(telNoPh);//Comment by jayshree Date 25/11/2017
	telNoC.setBorder(0);
	telNoC.setHorizontalAlignment(Element.ALIGN_LEFT);
	telNoC.setVerticalAlignment(Element.ALIGN_MIDDLE);
	

	Phrase colPhrase13 = new Phrase(":", font9);
	PdfPCell colCell13 = new PdfPCell(colPhrase13);
//	colCell13.addElement(colPhrase13);//Comment by jayshree Date 25/11/2017
	colCell13.setBorder(0);
	colCell13.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
	

	// Phrase telNovaluePhrase1=new Phrase("Tel.No.");
	Phrase telNovaluePh = null;
	if (cust.getLandline() != 0) {
		telNovaluePh = new Phrase(cust.getLandline() + "", font9);
	} else {
		telNovaluePh = new Phrase(" ", font9);
	}
	PdfPCell telNovalueC = new PdfPCell(telNovaluePh);
//	telNovalueC.addElement(telNovaluePh);//Comment by jayshree Date 25/11/2017
	telNovalueC.setBorder(0);
	telNovalueC.setHorizontalAlignment(Element.ALIGN_LEFT);
	telNovalueC.setVerticalAlignment(Element.ALIGN_MIDDLE);
	if(cust.getLandline() != 0&&cust.getLandline()!=null){
	customerDetailTable3.addCell(telNoC);
	customerDetailTable3.addCell(colCell13);
	customerDetailTable3.addCell(telNovalueC);
	}
	
	
	PdfPTable borderTable = new PdfPTable(2);
	borderTable.setWidthPercentage(100f);
	// borderTable.setSpacingBefore(5f);
	/**
	 * Date 25/09/2017 
	 * By- Jayshree chavan
	 * Description-changes are made to add the service address and and 
	 * 				billing address in different cell
	 * @param addressToTake
	 * @return
	 */
	PdfPCell borderCell2 = new PdfPCell(getCustomerDetails("BillingAddress"));
//	borderCell2.addElement(getCustomerDetails("BillingAddress"));//Comment by jayshree Date 25/11/2017
	borderCell2.setBorderWidthBottom(0);
	borderTable.addCell(borderCell2);

	PdfPCell borderCell3 = new PdfPCell(getCustomerDetails("ServiceAddress"));
//	borderCell3.addElement(getCustomerDetails("ServiceAddress"));//Comment by jayshree Date 25/11/2017
	borderCell3.setBorderWidthBottom(0);
	borderTable.addCell(borderCell3);

	PdfPCell borderCell4 = new PdfPCell(customerDetailTable2);
//	borderCell4.addElement(customerDetailTable2);//Comment by jayshree Date 25/11/2017
	borderCell4.setBorderWidthTop(0);
	borderCell4.setBorderWidthBottom(0);
	borderTable.addCell(borderCell4);

	
	
	PdfPCell borderCell6 = new PdfPCell(customerDetailTable3);
//	borderCell6.addElement(customerDetailTable3);//Comment by jayshree Date 25/11/2017
	borderCell6.setBorderWidthTop(0);
	borderCell6.setBorderWidthBottom(0);
	borderTable.addCell(borderCell6);
	
	PdfPCell borderCell7 = new PdfPCell(customerDetailTable3);
//	borderCell7.addElement(customerDetailTable3);//Comment by jayshree Date 25/11/2017
	borderCell7.setBorderWidthTop(0);
	borderTable.addCell(borderCell7);
	
	Phrase border5=new Phrase(" ");
	PdfPCell borderCell5 = new PdfPCell(border5);
	borderCell5.setBorderWidthTop(0);
	borderTable.addCell(borderCell5);

	try {
		document.add(borderTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

}
/**
 * Date 25/09/2017 
 * By- Jayshree chavan
 * Description-changes are made to add the service address and and 
 * 				billing address in different cell
 * @param addressToTake
 * @return
 */
	private PdfPTable getCustomerDetails(String addressToTake) {
		PdfPTable customerDetailTab1 = new PdfPTable(3);
		customerDetailTab1.setWidthPercentage(100f);
//		 customerDetailTab1.setSpacingBefore(2f);//comment By jayshree Date25/11/2017
		
		try {
			customerDetailTab1.setWidths(new float[] { 25, 3, 75 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase customerNamePhrase = new Phrase("Customer ", font9);
		PdfPCell customerNameCell = new PdfPCell(customerNamePhrase);
//		customerNameCell.addElement(customerNamePhrase);//Comment by jayshree Date 25/11/2017
		customerNameCell.setBorder(0);
		customerNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(customerNameCell);

		Phrase colPhrase5 = new Phrase(":", font8bold);
		PdfPCell colCell5 = new PdfPCell(colPhrase5);
//		colCell5.addElement(colPhrase5);//Comment by jayshree Date 25/11/2017
		colCell5.setBorder(0);
		colCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell5);

		// Phrase customerNameValuePhrase=new
		// Phrase("Dyaneshwar Surve",font8bold);
		Phrase customerNameValuePhrase = null;
		String custName = "";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S " + cust.getCompanyName().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}
		customerNameValuePhrase = new Phrase(custName, font9);
		PdfPCell customerNameValueCell = new PdfPCell(customerNameValuePhrase);
//		customerNameValueCell.addElement(customerNameValuePhrase);//Comment by jayshree Date 25/11/2017
		customerNameValueCell.setBorder(0);
		customerNameValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNameValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(customerNameValueCell);
		// /

		Phrase addressPhrase = new Phrase("Address", font9);
		PdfPCell addressCell = new PdfPCell(addressPhrase);
//		addressCell.addElement(addressPhrase);//Comment by jayshree Date 25/11/2017
		addressCell.setBorder(0);

		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addressCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(addressCell);

		Phrase colPhrase7 = new Phrase(":", font9bold);
		PdfPCell colCell7 = new PdfPCell(colPhrase7);
//		colCell7.addElement(colPhrase7);//Comment by jayshree Date 25/11/2017
		colCell7.setBorder(0);

		colCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
//		colCell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell7);
		/**
		 * Date 25/09/2017 
		 * By- Jayshree chavan
		 * Description-changes are made to add the service address and and 
		 * 				billing address in different cell
		 * @param addressToTake
		 * @return
		 */
		// Phrase addressValuePhrase=new Phrase("1201,12th",font8);
		/**
		 * Date : 04-11-2017 BY ANIL
		 * Pin code issue ,earlier jayshree was fetching pin from customer's billing address
		 * updated for city locality and pin
		 */
		Address serviceAddress=new Address();
		Phrase addressValuePhrase = null;
		if(addressToTake.trim().equalsIgnoreCase("BillingAddress")){
			logger.log(Level.SEVERE, "inside BillingAddress "+addressToTake);
			if(customerBranch!=null){
				if (customerBranch.getBillingAddress().getCompleteAddress() != null) {
					addressValuePhrase = new Phrase(customerBranch.getBillingAddress().getCompleteAddress() , font9);
					serviceAddress=customerBranch.getBillingAddress();
				} else {
					addressValuePhrase = new Phrase(" ", font9);
				}	
				logger.log(Level.SEVERE, "customer branch BillingAddress "+customerBranch.getBillingAddress().getCompleteAddress());
			}else{
				logger.log(Level.SEVERE, "inside else BillingAddress "+addressToTake);
				
				
//				if (cust.getAdress().getCompleteAddress() != null) {
//					addressValuePhrase = new Phrase(cust.getAdress()
//							.getCompleteAddress(), font9);
//					serviceAddress=cust.getAdress();
//				} else {
//					addressValuePhrase = new Phrase(" ", font9);
//				}	
				
				/**
				 * @author Vijay Date :- 21-04-2022
				 * Des :- address first print from contract then if it does not exist then it will print from customer
				 */
				String address = "";
				if (cust.getAdress().getCompleteAddress() != null) {
					address = cust.getAdress().getCompleteAddress();
				}
				try {
					if(con.getNewcustomerAddress()!=null) {
						serviceAddress = con.getNewcustomerAddress();	
						address = con.getNewcustomerAddress().getCompleteAddress();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				addressValuePhrase = new Phrase(address,font9);
			}
			
			
			
			
		}else if(addressToTake.trim().equalsIgnoreCase("ServiceAddress")){
			logger.log(Level.SEVERE, "inside ServiceAddress "+addressToTake);
			if(customerBranch!=null){
				if (customerBranch.getAddress().getCompleteAddress() != null) {
					addressValuePhrase = new Phrase(customerBranch.getAddress().getCompleteAddress() , font9);
					serviceAddress=customerBranch.getAddress();
				} else {
					addressValuePhrase = new Phrase(" ", font9);
				}
				logger.log(Level.SEVERE, "inside customer branch ServiceAddress "+customerBranch.getAddress().getCompleteAddress());
			}else{
				
//				if (cust.getSecondaryAdress().getCompleteAddress() != null) {
//					addressValuePhrase = new Phrase(cust.getSecondaryAdress().getCompleteAddress() , font9);
//					serviceAddress=cust.getSecondaryAdress();
//				} else {
//					addressValuePhrase = new Phrase(" ", font9);
//				}
//				logger.log(Level.SEVERE, "inside customer  ServiceAddress "+cust.getSecondaryAdress().getCompleteAddress());
				
				/**
				 * @author Vijay Date :- 21-04-2022
				 * Des :- address first print from contract then if it does not exist then it will print from customer
				 */
				
				String address = "";
				
				if (cust.getAdress().getCompleteAddress() != null) {
					address = cust.getSecondaryAdress().getCompleteAddress();
				}
				try {
					if(con.getNewcustomerAddress()!=null) {
						serviceAddress = con.getCustomerServiceAddress();	
						address = con.getCustomerServiceAddress().getCompleteAddress();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				addressValuePhrase = new Phrase(address,font9);
				
			}
			
		}
			
		
		PdfPCell addressValueCell = new PdfPCell(addressValuePhrase);
//		addressValueCell.addElement(addressValuePhrase);//Comment by jayshree Date 25/11/2017
		addressValueCell.setBorder(0);

		addressValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		addressValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(addressValueCell);
		// *******end***

		Phrase localityPhrase = new Phrase("Locality", font9);
		PdfPCell localityCell = new PdfPCell(localityPhrase);
//		localityCell.addElement(localityPhrase);//Comment by jayshree Date 25/11/2017
		localityCell.setBorder(0);
		localityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		localityCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(localityCell);

		Phrase colPhrase9 = new Phrase(":", font9);
		PdfPCell colCell9 = new PdfPCell(colPhrase9);
//		colCell9.addElement(colPhrase9);//Comment by jayshree Date 25/11/2017
		colCell9.setBorder(0);
		colCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell9.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell9);

		// Phrase localityValuePhrase=new Phrase("Thane(West)",font8);
		Phrase localityValuePhrase = null;
		if (serviceAddress.getLocality() != null) {
			localityValuePhrase = new Phrase(serviceAddress.getLocality(),
					font9);
		} else {
			localityValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell localityValueCell = new PdfPCell(localityValuePhrase);
//		localityValueCell.addElement(localityValuePhrase);
		localityValueCell.setBorder(0);
		localityValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		localityValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(localityValueCell);

		// /

		Phrase cityPhrase = new Phrase("City", font9);
		PdfPCell cityCell = new PdfPCell(cityPhrase);
//		cityCell.addElement(cityPhrase);//Comment by jayshree Date 25/11/2017
		cityCell.setBorder(0);
		cityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cityCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(cityCell);

		Phrase colPhrase10 = new Phrase(":", font9);
		PdfPCell colCell10 = new PdfPCell(colPhrase10);
//		colCell10.addElement(colPhrase10);//Comment by jayshree Date 25/11/2017
		colCell10.setBorder(0);
		colCell10.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell10.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell10);

		// Phrase cityValuePhrase=new Phrase("Mumbai",font8);
		Phrase cityValuePhrase = null;
		if (serviceAddress.getCity() != null) {
			cityValuePhrase = new Phrase(serviceAddress.getCity(), font9);
		} else {
			cityValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell cityValueCell = new PdfPCell(cityValuePhrase);
//		cityValueCell.addElement(cityValuePhrase);//Comment by jayshree Date 25/11/2017
		cityValueCell.setBorder(0);
		cityValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cityValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(cityValueCell);

		// //

		Phrase emailIdPhrase = new Phrase("Email ID", font9);

		PdfPCell emailIdCell = new PdfPCell(emailIdPhrase);
//		emailIdCell.addElement(emailIdPhrase);//Comment by jayshree Date 25/11/2017
		emailIdCell.setBorder(0);
		emailIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailIdCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(emailIdCell);

		Phrase colPhrase14 = new Phrase(":", font9);
		PdfPCell colCell14 = new PdfPCell(colPhrase14);
//		colCell14.addElement(colPhrase14);//Comment by jayshree Date 25/11/2017
		colCell14.setBorder(0);
		colCell14.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell14);

		// Phrase emailIdValuePhrase=new Phrase("",font8);
		Phrase emailIdValuePhrase = null;
		
		if(customerBranch!=null){
			if (customerBranch.getEmail() != null) {
				emailIdValuePhrase = new Phrase(customerBranch.getEmail(), font9);
			} else {
				emailIdValuePhrase = new Phrase(" ", font9);
			}	
		}else{
			if (cust.getEmail() != null) {
				emailIdValuePhrase = new Phrase(cust.getEmail(), font9);
			} else {
				emailIdValuePhrase = new Phrase(" ", font9);
			}	
		}
		
		PdfPCell emailIdValueCell = new PdfPCell(emailIdValuePhrase);
//		emailIdValueCell.addElement(emailIdValuePhrase);//Comment by jayshree Date 25/11/2017
		emailIdValueCell.setBorder(0);
		emailIdValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailIdValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(emailIdValueCell);
		// /

		Phrase pinNoPhrase = new Phrase("PIN Code", font9);
		PdfPCell pinNoCell = new PdfPCell(pinNoPhrase);
//		pinNoCell.addElement(pinNoPhrase);//Comment by jayshree Date 25/11/2017
		pinNoCell.setBorder(0);
		pinNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pinNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(pinNoCell);

		Phrase colPhrase11 = new Phrase(":", font9);
		PdfPCell colCell11 = new PdfPCell(colPhrase11);
//		colCell11.addElement(colPhrase11);//Comment by jayshree Date 25/11/2017
		colCell11.setBorder(0);
		colCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell11);

		// Phrase pinNoValuePhrase=new Phrase("400604",font8);
		Phrase pinNoValuePhrase = null;
		if (serviceAddress.getPin() != 0) {
			pinNoValuePhrase = new Phrase(serviceAddress.getPin() + "", font9);
		} else {
			pinNoValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell pinNoValueCell = new PdfPCell(pinNoValuePhrase);
//		pinNoValueCell.addElement(pinNoValuePhrase);//Comment by jayshree Date 25/11/2017
		pinNoValueCell.setBorder(0);
		pinNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pinNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(pinNoValueCell);
		
		
		
		
		
		return customerDetailTab1;
	}
	

private void createPerioddetailtab() {

	PdfPTable periodDetailTab = new PdfPTable(6);
	periodDetailTab.setWidthPercentage(100f);
//	 customerDetailTab1.setSpacingBefore(2f);

	try {
		periodDetailTab.setWidths(new float[] { 20, 2, 28, 20, 2, 28 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	PdfPCell periodblank = new PdfPCell();
	periodblank.setBorder(0);
	periodblank.setColspan(6);
	periodDetailTab.addCell(periodblank);

	Phrase particularsPremisesPhrase = new Phrase(
			"Premises Details", font9);
	PdfPCell particularPremisesCell = new PdfPCell(particularsPremisesPhrase);
	particularPremisesCell.setBorder(0);
	particularPremisesCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	particularPremisesCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(particularPremisesCell);

	Phrase colPhrase16 = new Phrase(":", font9);
	PdfPCell colCell16 = new PdfPCell(colPhrase16);
	colCell16.setBorder(0);
	colCell16.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(colCell16);

	// Phrase particularsPremisesValuePhrase=new Phrase("",font8);
	Phrase particularsPremisesValuePhrase = null;
	if (con.getType() != null) {
		particularsPremisesValuePhrase = new Phrase(con.getType(), font9);
	} else {
		particularsPremisesValuePhrase = new Phrase(" ", font9);
	}
	PdfPCell particularPremisesValueCell = new PdfPCell(particularsPremisesValuePhrase);
	particularPremisesValueCell.setBorder(0);
	particularPremisesValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	particularPremisesValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(particularPremisesValueCell);

	// //
	Phrase areaPhrase = new Phrase("", font9);
	PdfPCell areaCell = new PdfPCell(areaPhrase);
	areaCell.setBorder(0);
	periodDetailTab.addCell(areaCell);

	Phrase colPhrase18 = new Phrase("", font9);
	PdfPCell colCell18 = new PdfPCell(colPhrase18);
//	colCell18.addElement(colPhrase18);//Comment by jayshree Date 25/11/2017
	colCell18.setBorder(0);
	periodDetailTab.addCell(colCell18);

	
	Phrase areaValuePhrase=new Phrase("");
	PdfPCell areaValueCell = new PdfPCell(areaValuePhrase);
//	areaValueCell.addElement(areaValuePhrase);//Comment by jayshree Date 25/11/2017
	areaValueCell.setBorder(0);
	periodDetailTab.addCell(areaValueCell);

	Phrase orderRefPhrase = new Phrase("Order Ref", font9);
	PdfPCell orderRefCell = new PdfPCell(orderRefPhrase);
//	orderRefCell.addElement(orderRefPhrase);//Comment by jayshree Date 25/11/2017
	orderRefCell.setBorder(0);
	orderRefCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	orderRefCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(orderRefCell);

	Phrase colPhrase17 = new Phrase(":", font9);
	PdfPCell colCell17 = new PdfPCell(colPhrase17);
//	colCell17.addElement(colPhrase17);//Comment by jayshree Date 25/11/2017
	colCell17.setBorder(0);
	colCell17.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell17.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(colCell17);

	// Phrase orderRefValuePhrase=new Phrase("",font8);
	Phrase orderRefValuePhrase = null;
	
	/**
	 * Date 27/11/2017
	 * Dev.By jayshree
	 * des.To add the referenceNum 2 insted of reference num 1
	 */
	if (con.getRefNo2() != null) {
		orderRefValuePhrase = new Phrase(con.getRefNo2(), font9);
	} else {
		orderRefValuePhrase = new Phrase(" ", font9);
	}
	PdfPCell orderRefvalueCell = new PdfPCell(orderRefValuePhrase);
//	orderRefvalueCell.addElement(orderRefValuePhrase);//Comment by jayshree Date 25/11/2017
	orderRefvalueCell.setBorder(0);
	orderRefvalueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	orderRefvalueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(orderRefvalueCell);
//End By Jayshree
	


	Phrase datedPhrase = new Phrase("Dated", font9);
	PdfPCell datedCell = new PdfPCell(datedPhrase);
//	datedCell.addElement(datedPhrase);//Comment by jayshree Date 25/11/2017
	datedCell.setBorder(0);
	datedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	datedCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(datedCell);

	Phrase colPhrase19 = new Phrase(":", font9);
	PdfPCell colCell19 = new PdfPCell(colPhrase19);
//	colCell19.addElement(colPhrase19);//Comment by jayshree Date 25/11/2017
	colCell19.setBorder(0);
	colCell19.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell19.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(colCell19);

	// Phrase datedValuePhrase=new Phrase("",font8);
	Phrase datedValuePhrase = null;
	System.out.println("rohan con ref Date ====" + con.getRefDate());
	if (con.getRefDate() == null) {
		System.out.println("iin side if");
		datedValuePhrase = new Phrase(" ", font9);
	} else {
		System.out.println("iin side else");
		datedValuePhrase = new Phrase(fmt2.format(con.getRefDate()), font9);

	}
	PdfPCell datedValueCell = new PdfPCell(datedValuePhrase);
//	datedValueCell.addElement(datedValuePhrase);//Comment by jayshree Date 25/11/2017
	datedValueCell.setBorder(0);
	datedValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	datedValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(datedValueCell);

	// /
	Phrase periodOfContractFromPhrase = new Phrase(
			"Contract starts on", font9bold);
	PdfPCell periodOfContractFromCell = new PdfPCell(periodOfContractFromPhrase);
//	periodOfContractFromCell.addElement(periodOfContractFromPhrase);//Comment by jayshree Date 25/11/2017
	periodOfContractFromCell.setBorder(0);
	periodOfContractFromCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	periodOfContractFromCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(periodOfContractFromCell);

	Phrase colPhrase20 = new Phrase(":", font9);
	PdfPCell colCell20 = new PdfPCell(colPhrase20);
//	colCell20.addElement(colPhrase20);//Comment by jayshree Date 25/11/2017
	colCell20.setBorder(0);
	colCell20.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell20.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(colCell20);

	// Phrase periodOfContractFromvaluePhrase=new
	// Phrase("27/07/2017",font8bold);
	Phrase periodOfContractFromvaluePhrase = null;
	if (con.getStartDate() != null) {
		periodOfContractFromvaluePhrase = new Phrase(sdf.format(con
				.getStartDate()), font9);
	} else {
		periodOfContractFromvaluePhrase = new Phrase(" ", font9);
	}
	PdfPCell periodOfContractFromValueCell = new PdfPCell(periodOfContractFromvaluePhrase);
//	periodOfContractFromValueCell
//			.addElement(periodOfContractFromvaluePhrase);//Comment by jayshree Date 25/11/2017
	periodOfContractFromValueCell.setBorder(0);
	periodOfContractFromValueCell
			.setHorizontalAlignment(Element.ALIGN_LEFT);
	periodOfContractFromValueCell
			.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(periodOfContractFromValueCell);

	Phrase periodOfContractToPhrase = new Phrase("Contract ends on",
			font9bold);
	PdfPCell periodOfContractToCell = new PdfPCell(periodOfContractToPhrase);
//	periodOfContractToCell.addElement(periodOfContractToPhrase);//Comment by jayshree Date 25/11/2017
	periodOfContractToCell.setBorder(0);
	periodOfContractToCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	periodOfContractToCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(periodOfContractToCell);

	Phrase colPhrase21 = new Phrase(":", font9bold);
	PdfPCell colCell21 = new PdfPCell(colPhrase21);
//	colCell21.addElement(colPhrase21);//Comment by jayshree Date 25/11/2017
	colCell21.setBorder(0);
	colCell21.setHorizontalAlignment(Element.ALIGN_LEFT);
	colCell21.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(colCell21);

	// Phrase periodOfContractTovaluePhrase=new
	// Phrase("26/07/2018",font8bold);
	Phrase periodOfContractTovaluePhrase = null;
	if (con.getEndDate() != null) {
		periodOfContractTovaluePhrase = new Phrase(sdf.format(con
				.getEndDate()), font9);
	} else {
		periodOfContractTovaluePhrase = new Phrase(" ", font9);
	}
	PdfPCell periodOfContractToValueCell = new PdfPCell(periodOfContractTovaluePhrase);
//	periodOfContractToValueCell.addElement(periodOfContractTovaluePhrase);//Comment by jayshree Date 25/11/2017
	periodOfContractToValueCell.setBorder(0);
	periodOfContractToValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	periodOfContractToValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	periodDetailTab.addCell(periodOfContractToValueCell);

	PdfPTable borderTab = new PdfPTable(1);
	borderTab.setWidthPercentage(100f);
//	 borderTable.setSpacingBefore(5f);

	PdfPCell border = new PdfPCell(periodDetailTab);
//	border.addElement(periodDetailTab);
	borderTab.addCell(border);

	try {
		document.add(borderTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

private void createProductHeadertab() {
	// TODO Auto-generated method stub

	PdfPTable prodHedTab = new PdfPTable(4);
	prodHedTab.setWidthPercentage(100f);
	prodHedTab.setSpacingBefore(10f);//Comment by jayshree

	try {
		prodHedTab.setWidths(new float[] { 30, 30,25, 15 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	Phrase TypeOfTreatmentPhrase = new Phrase("Type of Treatment",
			font9bold);
	PdfPCell TypeOfTreatmentCell = new PdfPCell(TypeOfTreatmentPhrase);
	// TypeOfTreatmentCell.addElement(TypeOfTreatmentPhrase);//Comment by jayshree Date 25/11/2017
	TypeOfTreatmentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// TypeOfTreatmentCell.setVerticalAlignment(Element.ALIGN_TOP);
	TypeOfTreatmentCell.setVerticalAlignment(Element.ALIGN_CENTER);
	prodHedTab.addCell(TypeOfTreatmentCell);

	Phrase frequencyPhrase = new Phrase("Frequency/Number of Services ", font9bold);
	PdfPCell frequencyCell = new PdfPCell(frequencyPhrase);
	// frequencyCell.addElement(frequencyPhrase);//Comment by jayshree Date 25/11/2017
	frequencyCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
	frequencyCell.setVerticalAlignment(Element.ALIGN_CENTER);
	prodHedTab.addCell(frequencyCell);

	/**
	 * Date 25/09/2017 
	 * By- Jayshree chavan
	 * Description-changes are made to add the specification column
	 
	 
	 */
	Phrase specificationPhrase = new Phrase("Specification", font9bold);
	PdfPCell specificationCell = new PdfPCell(specificationPhrase);
	// frequencyCell.addElement(frequencyPhrase);//Comment by jayshree Date 25/11/2017
	specificationCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
	specificationCell.setVerticalAlignment(Element.ALIGN_CENTER);
	prodHedTab.addCell(specificationCell);
	
	Phrase amountPhrase = new Phrase("Amount", font9bold);
	PdfPCell amountCell = new PdfPCell(amountPhrase);
	// amountCell.addElement(amountPhrase);//Comment by jayshree Date 25/11/2017
	amountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
	amountCell.setVerticalAlignment(Element.ALIGN_CENTER);
	// amountCell.setVerticalAlignment(Element.ALIGN_TOP);
	prodHedTab.addCell(amountCell);

	try {
		document.add(prodHedTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

private void createConProductDetail() {
	/**
	 * Date 25/09/2017 
	 * By- Jayshree chavan
	 * Description-changes are made for print 
	 * the remaining product in the next page i.e more than ten product are in
	 * next page
	 
	 
	 */
	
	if(con.getDiscountAmt()!=0)
	{
		noOfLine=noOfLine-2;
		System.out.println("number line"+noOfLine);
		
	}
	
	int consolitaedCounter=0;//Added by Ashwini
		for (int i = 0; i < con.getItems().size(); i++) {
			
			
			if (noOfLine == 0) {
				productCount = i;
				
				PdfPTable conProductDetail1 = new PdfPTable(1);
				conProductDetail1.setWidthPercentage(100f);
				Phrase indicate = new Phrase(
						"Please Refere Annexure For More Details ", font9);
				PdfPCell indicateCell = new PdfPCell(indicate);
				indicateCell.setVerticalAlignment(Element.ALIGN_LEFT);
				conProductDetail1.addCell(indicateCell);
				
				try {
					document.add(conProductDetail1);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			}
			PdfPTable conProductDetail1 = new PdfPTable(1);
			conProductDetail1.setWidthPercentage(100f);
			noOfLine = noOfLine - 1;
			System.out.println("con.getItems().get(i).getProductName().trim().length()"+con.getItems().get(i).getProductName()
					.trim().length());
			
			if (con.getItems().get(i).getProductName()
					.trim().length() > 50) {
				noOfLine = noOfLine - 1;
			}
//			System.out.println("number line 2" + noOfLine);

			

			
			// ****************end*************************
			/**
			 * Date 25/09/2017 By- Jayshree chavan Description-changes are made
			 * to split the product name if(-)present in product name
			 */
			String productName = con.getItems().get(i).getProductName();
			if (productName.contains("-")) {
				String[] product = productName.split(("-"));
				product1 = product[0];
				product2 = product[1];
			} else {
				product1 = con.getItems().get(i).getProductName();
				product2 = "";
			}

			PdfPTable conProductDetail = new PdfPTable(4);
			conProductDetail.setWidthPercentage(100f);

			try {
				conProductDetail.setWidths(new float[] { 30, 30, 25, 15 });
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

			Phrase generalDisinfestationPhrase = new Phrase(product1, font9bold);
			PdfPCell generalDisinfestationCell = new PdfPCell(
					generalDisinfestationPhrase);
			// generalDisinfestationCell.addElement(generalDisinfestationPhrase);//Comment by jayshree Date 25/11/2017
			generalDisinfestationCell
					.setHorizontalAlignment(Element.ALIGN_LEFT);
			generalDisinfestationCell
					.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationCell.setVerticalAlignment(Element.ALIGN_TOP);
			conProductDetail.addCell(generalDisinfestationCell);

			Phrase generalDisinfestationFrequencyPhrase;
			if (!product2.equals("")) {
				generalDisinfestationFrequencyPhrase = new Phrase(product2
						+ " " + "("
						+ con.getItems().get(i).getNumberOfServices()
						+ "  Services)", font9);
			} else {
				generalDisinfestationFrequencyPhrase = new Phrase("("
						+ con.getItems().get(i).getNumberOfServices()
						+ "  Services)", font9);
			}

			PdfPCell generalDisinfestationFrequencyCell = new PdfPCell(
					generalDisinfestationFrequencyPhrase);
			// generalDisinfestationFrequencyCell.addElement(generalDisinfestationFrequencyPhrase);//Comment by jayshree Date 25/11/2017
			generalDisinfestationFrequencyCell
					.setHorizontalAlignment(Element.ALIGN_LEFT);
			generalDisinfestationFrequencyCell
					.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationFrequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
			conProductDetail.addCell(generalDisinfestationFrequencyCell);
			// ********************end************

			Phrase specificationPhrase = new Phrase(con.getItems().get(i)
					.getSpecification(), font9);
			PdfPCell specificationCell = new PdfPCell(specificationPhrase);
			// frequencyCell.addElement(frequencyPhrase);//Comment by jayshree Date 25/11/2017
			specificationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
			specificationCell.setVerticalAlignment(Element.ALIGN_CENTER);
			conProductDetail.addCell(specificationCell);
			
			/*
			 * Commented by Ashwini
			 */

//		double assessTotal = getAssessTotalAmount(con.getItems().get(i));
//			Phrase generalDisinfestationAmountPhrase = new Phrase(
//					df.format(assessTotal) + "", font9bold);
//			PdfPCell generalDisinfestationAmountCell = new PdfPCell(
//					generalDisinfestationAmountPhrase);
//			// generalDisinfestationAmountCell.addElement(generalDisinfestationAmountPhrase);//Comment by jayshree Date 25/11/2017
//			generalDisinfestationAmountCell
//					.setHorizontalAlignment(Element.ALIGN_RIGHT);
//			generalDisinfestationAmountCell
//					.setVerticalAlignment(Element.ALIGN_CENTER);
//			// generalDisinfestationAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
//			conProductDetail.addCell(generalDisinfestationAmountCell);
//
//			totalamt = totalamt + assessTotal;
//
//			try {
//				document.add(conProductDetail);
//			} catch (DocumentException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			/*
			 * Date:01/08/2018
			 * Developer:Ashwini
			 * Des:To obtain customize amount in amount coloum.
//			 */
			Phrase chunk = null;
		
			if(consolidatePrice || con.isConsolidatePrice()){
				Phrase Amountphrase = null;
				if(consolitaedCounter == 0){
					Amountphrase=new Phrase(df.format(con.getFinalTotalAmt())+"", font9bold);
				}
				consolitaedCounter++;
				System.out.println("Consulated:"+consolitaedCounter);
			PdfPCell Amountcell=new PdfPCell(Amountphrase);
			Amountcell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			Amountcell.setVerticalAlignment(Element.ALIGN_CENTER);
			Amountcell.setBorder(0);
			Amountcell.setBorderWidthRight(1);
			Amountcell.setBorderWidthLeft(1);
			conProductDetail.addCell(Amountcell);
			
			try {
				document.add(conProductDetail);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	else{
			double assessTotal = getAssessTotalAmount(con.getItems().get(i));
			
			System.out.println("assessTotal "+assessTotal);
			Phrase generalDisinfestationAmountPhrase = new Phrase(
					df.format(assessTotal) + "", font9bold);
			PdfPCell generalDisinfestationAmountCell = new PdfPCell(
					generalDisinfestationAmountPhrase);
			// generalDisinfestationAmountCell.addElement(generalDisinfestationAmountPhrase);//Comment by jayshree Date 25/11/2017
			generalDisinfestationAmountCell
					.setHorizontalAlignment(Element.ALIGN_RIGHT);
			generalDisinfestationAmountCell
					.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
			conProductDetail.addCell(generalDisinfestationAmountCell);

			totalamt = totalamt + assessTotal;

			try {
				document.add(conProductDetail);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
			/*
			 * End by Ashwini
			 */
			
		}
}


	
/**
 * Date 25/09/2017 
 * By- Jayshree chavan
 * Description-changes are made for to print the total amount 
 * with checking the discount and area is includes
 
 
 */
private double getAssessTotalAmount(SalesLineItem salesLineItem) {
	// TODO Auto-generated method stub
	double totalAmount = 0;
	
	double producprice = salesLineItem.getPrice();
	
	if(salesLineItem.isInclusive()){
		SuperProduct product = salesLineItem.getPrduct();
		double tax = removeAllTaxes(product);
		producprice = producprice-tax;
		logger.log(Level.SEVERE, "is inclusive"+producprice);
	}
	logger.log(Level.SEVERE, "producprice"+producprice);
	
	if (salesLineItem.getArea().trim().equalsIgnoreCase("NA")
			|| salesLineItem.getArea().equalsIgnoreCase("")) {
		System.out.println("salesLineItem.getPrice() "+salesLineItem.getPrice());
		if (salesLineItem.getPercentageDiscount() == null
				|| salesLineItem.getPercentageDiscount() == 0) {
			if (salesLineItem.getDiscountAmt() == 0) {
				
				totalAmount = producprice;
			} else {
				totalAmount = producprice
						- salesLineItem.getDiscountAmt();
			}
		} else {
			double disPercentAmount = getPercentAmount(salesLineItem, false);
			if (salesLineItem.getDiscountAmt() == 0) {
				totalAmount = producprice - disPercentAmount;
			} else {
				totalAmount = producprice - disPercentAmount
						- salesLineItem.getDiscountAmt();
			}
		}
	} else {

		logger.log(Level.SEVERE, "Area Present");
		if (salesLineItem.getPercentageDiscount() == null
				&& salesLineItem.getPercentageDiscount() == 0) {
			logger.log(Level.SEVERE,
					"ZERO Area Present --PercentageDiscount");
			if (salesLineItem.getDiscountAmt() == 0) {
				logger.log(Level.SEVERE,
						"ZERO Area Present --PercentageDiscount--Discount Amt");
				totalAmount = producprice
						* Double.parseDouble(salesLineItem.getArea().trim());
			} else {
				logger.log(Level.SEVERE,
						"NON ZERO Area Present --PercentageDiscount--Discount Amt");
				totalAmount = (producprice * Double
						.parseDouble(salesLineItem.getArea().trim()))
						- salesLineItem.getDiscountAmt();
			}
		} else {
			logger.log(Level.SEVERE,
					"NON ZERO Area Present --PercentageDiscount");
			double disPercentAmount = getPercentAmount(salesLineItem, true);
			if (salesLineItem.getDiscountAmt() == 0) {
				logger.log(Level.SEVERE,
						"ZERO Area Present --PercentageDiscount--Disc Amt");
				totalAmount = (producprice * Double
						.parseDouble(salesLineItem.getArea().trim()))
						- disPercentAmount;
			} else {
				logger.log(Level.SEVERE,
						"NON ZERO Area Present --PercentageDiscount--Disc Amt");
				totalAmount = (producprice * Double
						.parseDouble(salesLineItem.getArea().trim()))
						- disPercentAmount - salesLineItem.getDiscountAmt();
			}
		}
		
	
//	if (salesLineItem.getArea().trim().equalsIgnoreCase("NA")
//			|| salesLineItem.getArea().equalsIgnoreCase("")) {
//		System.out.println("salesLineItem.getPrice() "+salesLineItem.getPrice());
//		if (salesLineItem.getPercentageDiscount() == null
//				|| salesLineItem.getPercentageDiscount() == 0) {
//			if (salesLineItem.getDiscountAmt() == 0) {
//				
//				totalAmount = salesLineItem.getPrice();
//			} else {
//				totalAmount = salesLineItem.getPrice()
//						- salesLineItem.getDiscountAmt();
//			}
//		} else {
//			double disPercentAmount = getPercentAmount(salesLineItem, false);
//			if (salesLineItem.getDiscountAmt() == 0) {
//				totalAmount = salesLineItem.getPrice() - disPercentAmount;
//			} else {
//				totalAmount = salesLineItem.getPrice() - disPercentAmount
//						- salesLineItem.getDiscountAmt();
//			}
//		}
//	} else {
//
//		logger.log(Level.SEVERE, "Area Present");
//		if (salesLineItem.getPercentageDiscount() == null
//				&& salesLineItem.getPercentageDiscount() == 0) {
//			logger.log(Level.SEVERE,
//					"ZERO Area Present --PercentageDiscount");
//			if (salesLineItem.getDiscountAmt() == 0) {
//				logger.log(Level.SEVERE,
//						"ZERO Area Present --PercentageDiscount--Discount Amt");
//				totalAmount = salesLineItem.getPrice()
//						* Double.parseDouble(salesLineItem.getArea().trim());
//			} else {
//				logger.log(Level.SEVERE,
//						"NON ZERO Area Present --PercentageDiscount--Discount Amt");
//				totalAmount = (salesLineItem.getPrice() * Double
//						.parseDouble(salesLineItem.getArea().trim()))
//						- salesLineItem.getDiscountAmt();
//			}
//		} else {
//			logger.log(Level.SEVERE,
//					"NON ZERO Area Present --PercentageDiscount");
//			double disPercentAmount = getPercentAmount(salesLineItem, true);
//			if (salesLineItem.getDiscountAmt() == 0) {
//				logger.log(Level.SEVERE,
//						"ZERO Area Present --PercentageDiscount--Disc Amt");
//				totalAmount = (salesLineItem.getPrice() * Double
//						.parseDouble(salesLineItem.getArea().trim()))
//						- disPercentAmount;
//			} else {
//				logger.log(Level.SEVERE,
//						"NON ZERO Area Present --PercentageDiscount--Disc Amt");
//				totalAmount = (salesLineItem.getPrice() * Double
//						.parseDouble(salesLineItem.getArea().trim()))
//						- disPercentAmount - salesLineItem.getDiscountAmt();
//			}
//		}

	}
	logger.log(Level.SEVERE, "Assesable Value::::" + totalAmount);
	return totalAmount;
}


private double getPercentAmount(SalesLineItem salesLineItem,
		boolean isAreaPresent) {
	// TODO Auto-generated method stub
	double percentAmount = 0;
	if (isAreaPresent) {
		percentAmount = ((salesLineItem.getPrice()
				* Double.parseDouble(salesLineItem.getArea().trim()) * salesLineItem
				.getPercentageDiscount()) / 100);
	} else {
		percentAmount = ((salesLineItem.getPrice() * salesLineItem
				.getPercentageDiscount()) / 100);
	}
	return percentAmount;
}
//*******************End*******************
private void createconTaxDetailTable() {

	PdfPTable conTaxDetailTab = new PdfPTable(2);
	conTaxDetailTab.setWidthPercentage(100f);

	try {
		conTaxDetailTab.setWidths(new float[] { 85, 15 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	Phrase totalPhrase = new Phrase("Total", font9bold);
	PdfPCell totalCell = new PdfPCell(totalPhrase);
	// totalCell.addElement(totalPhrase);//Comment by jayshree Date 25/11/2017
	// totalCell.setColspan(2);
	totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	totalCell.setVerticalAlignment(Element.ALIGN_CENTER);
	// totalCell.setVerticalAlignment(Element.ALIGN_TOP);
	conTaxDetailTab.addCell(totalCell);
	/**
	 * Date 25/09/2017 
	 * By- Jayshree chavan
	 * Description-changes are made for to print the total amount 
	 * with checking the discount and area is includes
	 
	 
	 */
	
	double assessTotal=0;
	for (int i = 0; i < con.getItems().size(); i++) {
		assessTotal=assessTotal+getAssessTotalAmount(con.getItems().get(i));
	}
		
		
	
	Phrase totalAmountPhrase = new Phrase(df.format(assessTotal) + "", font9bold);
	PdfPCell totalAmountCell = new PdfPCell(totalAmountPhrase);
	// totalAmountCell.addElement(totalAmountPhrase);//Comment by jayshree Date 25/11/2017
	// totalCell.setColspan(2);
	totalAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	totalAmountCell.setVerticalAlignment(Element.ALIGN_CENTER);
	// totalAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
	conTaxDetailTab.addCell(totalAmountCell);
	boolean disflag=false;
//	boolean totflag=false;
	if(con.getDiscountAmt()!=0)
	{
		disflag=true;
	}
	else
	{
		disflag=false;
	}
	
	if(disflag==true)
	{
	Phrase discountPhrase = new Phrase("Discount", font9bold);
	PdfPCell discountCell = new PdfPCell(discountPhrase);
	discountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	discountCell.setVerticalAlignment(Element.ALIGN_CENTER);
	conTaxDetailTab.addCell(discountCell);
	
	
	Phrase discountAmount = new Phrase(df.format(con.getDiscountAmt())+"", font9bold);
	PdfPCell discountAmountCell = new PdfPCell(discountAmount);
	discountAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	discountAmountCell.setVerticalAlignment(Element.ALIGN_CENTER);
	conTaxDetailTab.addCell(discountAmountCell);
	
	Phrase taxebleamtPhrase = new Phrase("Total", font9bold);
	PdfPCell taxebleamtCell = new PdfPCell(taxebleamtPhrase);
	taxebleamtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	taxebleamtCell.setVerticalAlignment(Element.ALIGN_CENTER);
	conTaxDetailTab.addCell(taxebleamtCell);
	
	
	Phrase taxebleamtValPh = new Phrase(df.format(con.getFinalTotalAmt())+"", font9bold);
	PdfPCell taxebleamtValCell = new PdfPCell(taxebleamtValPh);
	taxebleamtValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	taxebleamtValCell.setVerticalAlignment(Element.ALIGN_CENTER);
	conTaxDetailTab.addCell(taxebleamtValCell);
	
	}
		
//**************End**************
	/**
	 * Date 25/09/2017 
	 * By- Jayshree chavan
	 * Description-changes are made to add the product taxes in the 
	 * table
	 
	 
	 */
	for (int i = 0; i < con.getProductTaxes().size(); i++) {

		Phrase cgstPhrase = new Phrase(con.getProductTaxes().get(i)
				.getChargeName()
				+ "@" + con.getProductTaxes().get(i).getChargePercent(),
				font9bold);
		PdfPCell cgstCell = new PdfPCell(cgstPhrase);
		// cgstCell.addElement(cgstPhrase);//Comment by jayshree Date 25/11/2017
		// cgstCell.setColspan(2);
		cgstCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cgstCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// cgstCell.setVerticalAlignment(Element.ALIGN_TOP);
		conTaxDetailTab.addCell(cgstCell);

		Phrase cgstValuePhrase = new Phrase(df.format(con.getProductTaxes().get(i)
				.getChargePayable())
				+ "", font9bold);
		PdfPCell cgstValueCell = new PdfPCell(cgstValuePhrase);
		// cgstValueCell.addElement(cgstValuePhrase);//Comment by jayshree Date 25/11/2017
		// cgstValueCell.setColspan(2);
		cgstValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cgstValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// cgstValueCell.setVerticalAlignment(Element.ALIGN_TOP);
		conTaxDetailTab.addCell(cgstValueCell);

	}
	//******************End*********************************

	Phrase grandtotalPhrase = new Phrase("Grand Total", font9bold);
	PdfPCell grandtotalCell = new PdfPCell(grandtotalPhrase);
	// grandtotalCell.addElement(grandtotalPhrase);//Comment by jayshree Date 25/11/2017
	// grandtotalCell.setColspan(2);
	grandtotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	grandtotalCell.setVerticalAlignment(Element.ALIGN_CENTER);
	// grandtotalCell.setVerticalAlignment(Element.ALIGN_TOP);
	conTaxDetailTab.addCell(grandtotalCell);

	Phrase grandtotalAmountPhrase = new Phrase(df.format(con.getNetpayable()) + "",
			font9bold);
	PdfPCell grandtotalAmountCell = new PdfPCell(grandtotalAmountPhrase);
	// grandtotalAmountCell.addElement(grandtotalAmountPhrase);//Comment by jayshree Date 25/11/2017
	// grandtotalAmountCell.setColspan(2);
	grandtotalAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	grandtotalAmountCell.setVerticalAlignment(Element.ALIGN_CENTER);
	// grandtotalAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
	conTaxDetailTab.addCell(grandtotalAmountCell);

	try {
		document.add(conTaxDetailTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
/**
 * Date 25/09/2017 
 * By- Jayshree chavan
 * Description-changes are made for to 
 * add the company  GSTIN,PAN,SAC Value  
 
 
 */
private void createComTaxTable() {

	PdfPTable comTaxTable = new PdfPTable(1);
	comTaxTable.setWidthPercentage(100f);

//	try {
//		comTaxTable.setWidths(new float[] { 30, 70 });
//	} catch (DocumentException e1) {
//		e1.printStackTrace();
//	}

	String gstinValue = "", panNovalue = "", sacvalue = "", esicvalue ="";;
	/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
	if(branchDt != null){
		if(branchDt.getGSTINNumber() != null && !branchDt.getGSTINNumber().equals("")){
			gstinValue = branchDt.getGSTINNumber();
		}
	}
	
	/**
	 *   Date : 12-04-2021 Added By Priyanka 
	 *   print GSTN, PAN , ESIC , SAC number from master as well as Branch. 
	 */
	if(branchDt != null){
		if(branchDt.getPANNumber() != null && !branchDt.getPANNumber().equals("")){
			panNovalue = branchDt.getPANNumber();
		}
	}
	
	if(branchDt != null){
		if(branchDt.getEsicCode()!= null && !branchDt.getEsicCode().equals("")){
			esicvalue = branchDt.getEsicCode();
		}
	}
	

	for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
		/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
			if (gstinValue.equals("")) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue();
				}
			}

		if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
				.equalsIgnoreCase("PAN NO")) {
			panNovalue = comp.getArticleTypeDetails().get(i)
					.getArticleTypeValue();
		}

		if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
				.equalsIgnoreCase("SAC No")) {
			sacvalue = comp.getArticleTypeDetails().get(i)
					.getArticleTypeValue();
		}
		
		if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
				.equalsIgnoreCase("ESIC NO")) {
			esicvalue = comp.getArticleTypeDetails().get(i)
					.getArticleTypeValue();
		}
	}
	
	
	/** By Priyanka - Pecopp dont need this on pdf as per new format**/
	Phrase gstinPhraseone = new Phrase("PECOPP  Details  : "  ,font9);
	PdfPCell gstinCellone = new PdfPCell(gstinPhraseone);
	gstinCellone.setHorizontalAlignment(Element.ALIGN_LEFT);
	//gstinCellone.setBorder(0);
	
    
	Phrase gstinPhrase = new Phrase("GSTIN  : " +gstinValue  ,font9);
	PdfPCell gstinCell = new PdfPCell(gstinPhrase);
	gstinCell.setBorderWidthBottom(0);
	gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	

	Phrase panNoPhrase = new Phrase("PAN NO  : " +panNovalue  ,font9);
	PdfPCell panNoCell = new PdfPCell(panNoPhrase);
	panNoCell.setBorderWidthTop(0);
	panNoCell.setBorderWidthBottom(0);
	panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	Phrase sacNoPhrase = new Phrase("SAC NO  : " +sacvalue  ,font9);
	PdfPCell sacNoCell = new PdfPCell(sacNoPhrase);
	sacNoCell.setBorderWidthTop(0);
	sacNoCell.setBorderWidthBottom(0);
	sacNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	
	Phrase esicNoPhrase = new Phrase("ESIC Regn. No  : " +esicvalue  ,font9);
	PdfPCell esicNoCell = new PdfPCell(esicNoPhrase);
	esicNoCell.setBorderWidthTop(0);
	esicNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	
	//gstinCellone
	comTaxTable.addCell(gstinCellone);
	if(gstinValue!=null&&!gstinValue.equals("")) {
		comTaxTable.addCell(gstinCell);
		}
	if(panNovalue!=null&&!panNovalue.equals("")) {
		comTaxTable.addCell(panNoCell);
		}
	if(sacvalue!=null&&!sacvalue.equals("")) {
		comTaxTable.addCell(sacNoCell);
		}
	if(esicvalue!=null&&!esicvalue.equals("")) {
		comTaxTable.addCell(esicNoCell);
		}
	
	/**
	 *  End
	 */
	
	
	
	
//	Phrase gstinPhrase = null;
//	gstinPhrase = new Phrase("GSTIN", font9);
//
//	PdfPCell gstinCell = new PdfPCell(gstinPhrase);
////	gstinCell.addElement(gstinPhrase);//Comment by jayshree Date 25/11/2017
////	gstinCell.setBorder(0);
//	gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	gstinCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(gstinCell!=null) {
//	comTaxTable.addCell(gstinCell);
//	}
//	
//	// Phrase gstinValuePhrase=new Phrase( gstinNo,font8);
//	Phrase gstinValuePhrase = null;
//	gstinValuePhrase = new Phrase(gstinValue, font9);
//	PdfPCell gstinValueCell = new PdfPCell(gstinValuePhrase);
////	gstinValueCell.addElement(gstinValuePhrase);//Comment by jayshree Date 25/11/2017
////	gstinValueCell.setBorder(0);
//	gstinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	gstinValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(gstinValueCell!=null) {
//	comTaxTable.addCell(gstinValueCell);
//	}
//	// /
//
//	// Phrase panNoPhrase=new Phrase("PAN NO",font8);
//	Phrase panNoPhrase = null;
//	panNoPhrase = new Phrase("PAN NO", font9);
//	PdfPCell panNoCell = new PdfPCell(panNoPhrase);
////	panNoCell.addElement(panNoPhrase);//Comment by jayshree Date 25/11/2017
////	panNoCell.setBorder(0);
//	panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	panNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(panNoCell!=null) {
//	comTaxTable.addCell(panNoCell);
//	}
//
//	Phrase panNoValuePhrase = null;
//	panNoValuePhrase = new Phrase(panNovalue, font9);
//	PdfPCell panNoValueCell = new PdfPCell(panNoValuePhrase);
////	panNoValueCell.addElement(panNoValuePhrase);//Comment by jayshree Date 25/11/2017
////	panNoValueCell.setBorder(0);
//	panNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	panNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(panNoValueCell!=null){	
//    comTaxTable.addCell(panNoValueCell);
//	}
//	//
//	// Phrase panNoPhrase=new Phrase("PAN NO",font8);
//	Phrase sacNoPhrase = null;
//	sacNoPhrase = new Phrase("SAC NO", font9);
//	PdfPCell sacNoCell = new PdfPCell(sacNoPhrase);
////	sacNoCell.addElement(sacNoPhrase);//Comment by jayshree Date 25/11/2017
////	sacNoCell.setBorder(0);
//	sacNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	sacNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(sacNoCell!=null) {
//	comTaxTable.addCell(sacNoCell);
//	}
//	
//	Phrase sacNoValuePhrase = null;
//	sacNoValuePhrase = new Phrase(sacvalue, font9);
//	PdfPCell sacNoValueCell = new PdfPCell(sacNoValuePhrase);
////	sacNoValueCell.addElement(sacNoValuePhrase);//Comment by jayshree Date 25/11/2017
////	sacNoValueCell.setBorder(0);
//	sacNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	sacNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(sacNoValueCell!=null) {
//    comTaxTable.addCell(sacNoValueCell);
//	}
//	
//	Phrase esicNoPhrase = null;
//	esicNoPhrase = new Phrase("ESIC NO", font9);
//	PdfPCell esicNoCell = new PdfPCell(esicNoPhrase);
////	esicNoCell.addElement(sacNoPhrase);//By Jayshree comment this line Date25/11/2017
////	esicNoCell.setBorder(0);
//	esicNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	esicNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(esicNoCell!=null) {
//	comTaxTable.addCell(esicNoCell);
//	}
//	
//	Phrase esicNoValuePhrase = null;
//	esicNoValuePhrase = new Phrase(esicvalue, font9);
//	PdfPCell esicNoValueCell = new PdfPCell(esicNoValuePhrase);
//	//esicNoValueCell.addElement(esicNoValuePhrase);//By Jayshree comment this line Date25/11/2017
////	esicNoValueCell.setBorder(0);
//	esicNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	esicNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//	if(esicNoValueCell!=null){
//	comTaxTable.addCell(esicNoValueCell);
//	}
	
//	ArticleType articleType=null;
//	for(int i=0;i<comp.getArticleTypeDetails().size();i++){
//		if(comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("Contract")
//				&&comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
//		    articleType=comp.getArticleTypeDetails().get(i);	
//			
//		}
//	}
	    ServerAppUtility serverApp = new ServerAppUtility();
    
	    String gstin = "";
	
		logger.log(Level.SEVERE, "GST Applicable");
		gstin = serverApp.getGSTINOfCompany(comp, con
				.getBranch().trim());
		System.out.println("gstin" + gstin);

	
	
	
//	if(articleType!=null){
//     logger.log(Level.SEVERE, "Inside articleType contract");
//		Phrase articleNamePhra = new Phrase(articleType.getArticleTypeName(), font9);
//		PdfPCell articleName = new PdfPCell(articleNamePhra);
//		articleName.setHorizontalAlignment(Element.ALIGN_LEFT);
//		articleName.setVerticalAlignment(Element.ALIGN_CENTER);
//		comTaxTable.addCell(articleName);
//		
//		logger.log(Level.SEVERE, "Inside articleType value contract "+gstin);
//		Phrase articleValuePh = new Phrase(gstin, font9);
//		PdfPCell articleValuePhrase = new PdfPCell(articleValuePh);
//		articleValuePhrase.setHorizontalAlignment(Element.ALIGN_LEFT);
//		articleValuePhrase.setVerticalAlignment(Element.ALIGN_CENTER);
//		comTaxTable.addCell(articleValuePhrase);
//	}
//	
//	comp.getArticleTypeDetails().remove(articleType);
//	String articleTypeName="";
//    String articleTypeValue="";
//	for(int i=0;i<comp.getArticleTypeDetails().size();i++)
//	{
//		if(comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("Contract")){
//		if(comp.getArticleTypeDetails().get(i).getArticleTypeName() !=null && comp.getArticleTypeDetails().get(i).getArticleTypeValue() !=null)
//		{
//			articleTypeName = comp.getArticleTypeDetails().get(i).getArticleTypeName();	
//			articleTypeValue=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
//			
//			Phrase articleNamePhrase = new Phrase(articleTypeName, font9);
//			PdfPCell articleNameCell = new PdfPCell(articleNamePhrase);
//			articleNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			articleNameCell.setVerticalAlignment(Element.ALIGN_CENTER);
//			comTaxTable.addCell(articleNameCell);
//
//			
//			Phrase articleValuePhrase = new Phrase(articleTypeValue, font9);
//			PdfPCell articleValuePhraseCell = new PdfPCell(articleValuePhrase);
//			articleValuePhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//			articleValuePhraseCell.setVerticalAlignment(Element.ALIGN_CENTER);
//			comTaxTable.addCell(articleValuePhraseCell);
//			
//		}
//		}
//	}
//	
//	
	
	
	
	
	
	
	
	try {
		document.add(comTaxTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
}
//*****************end********************


/**
 * Date 25/09/2017 
 * By- Jayshree chavan
 * Description-changes are made to add the payment term
 *  ,percentage & comment the table according to the product detail
 
 
 */
private void createConPaymentTable() {
	// TODO Auto-generated method stub

	PdfPTable conPaymentTab = new PdfPTable(1);
	conPaymentTab.setWidthPercentage(100f);

	try {
		conPaymentTab.setWidths(new float[] { 100 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}

	
	
	
	
	
String payment = "";
	
	
	if (con.getPayTerms() != null && !con.getPayTerms().equals(""))
	{
		payment = con.getPayTerms();
	} else 
	{
	
		for (int j = 0; j < con.getPaymentTermsList().size(); j++) 
		{
			if (j != 0)
			{
				if ((j + 1) != con.getPaymentTermsList().size())
				{
					payment = payment
							+ ", "
							+ (con.getPaymentTermsList().get(j)
									.getPayTermPercent()
									+ "% " + con.getPaymentTermsList()
									.get(j).getPayTermComment());
				} else
				{
					payment = payment
							+ " & "
							+ (con.getPaymentTermsList().get(j)
									.getPayTermPercent()
									+ "% " + con.getPaymentTermsList()
									.get(j).getPayTermComment());
				}
			}
			else 
			{
				payment = con.getPaymentTermsList().get(j)
						.getPayTermPercent()
						+ "% "
						+ con.getPaymentTermsList().get(j)
								.getPayTermComment();
			}

		}

	}
	
	
	
	
	Phrase paymentTermsPhrase = new Phrase("Payment Terms : "+payment, font9bold);
	PdfPCell paymentTermCell = new PdfPCell(paymentTermsPhrase);
	// paymentTermCell.addElement(paymentTermsPhrase);//Comment by jayshree Date 25/11/2017
	paymentTermCell.setBorder(0);
	//paymentTermCell.setColspan(2);
	paymentTermCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	paymentTermCell.setVerticalAlignment(Element.ALIGN_CENTER);
	// paymentTermCell.setVerticalAlignment(Element.ALIGN_TOP);
	conPaymentTab.addCell(paymentTermCell);
	
	Phrase blank = new Phrase(" ", font10);
	PdfPCell blankCell = new PdfPCell(blank);
	blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	blankCell.setBorder(0);
	conPaymentTab.addCell(blankCell);
	

	try {
		document.add(conPaymentTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
//	PdfPTable prodPaymentTab = new PdfPTable(1);
//	prodPaymentTab.setWidthPercentage(100f);
//	prodPaymentTab.setSpacingBefore(2);
//
//	try {
//		prodPaymentTab.setWidths(new float[] { 100 });
//	} catch (DocumentException e1) {
//		e1.printStackTrace();
//	}
	
	
	//String payment = "";
	
	
//	if (con.getPayTerms() != null && !con.getPayTerms().equals(""))
//	{
//		payment = con.getPayTerms();
//	} else 
//	{
//	
//		for (int j = 0; j < con.getPaymentTermsList().size(); j++) 
//		{
//			if (j != 0)
//			{
//				if ((j + 1) != con.getPaymentTermsList().size())
//				{
//					payment = payment
//							+ ", "
//							+ (con.getPaymentTermsList().get(j)
//									.getPayTermPercent()
//									+ "% " + con.getPaymentTermsList()
//									.get(j).getPayTermComment());
//				} else
//				{
//					payment = payment
//							+ " & "
//							+ (con.getPaymentTermsList().get(j)
//									.getPayTermPercent()
//									+ "% " + con.getPaymentTermsList()
//									.get(j).getPayTermComment());
//				}
//			}
//			else 
//			{
//				payment = con.getPaymentTermsList().get(j)
//						.getPayTermPercent()
//						+ "% "
//						+ con.getPaymentTermsList().get(j)
//								.getPayTermComment();
//			}
//
//		}
//
//	}
	
	

//	for (int i = 0; i <con.getItems().size() ; i++)
//	{
//	String productName=con.getItems().get(i).getProductName();
//		if(productName.contains("-"))
//		{
//		String[] product=productName.split(("-"));
//		 product1=product[0];
//		 product2=product[1];
//		}
//		else
//		{
//		product1=con.getItems().get(i).getProductName();
//		product2="";
//		}
//	}
//			if(con.getItems().size()==1)
//			{
////				Phrase prodNamePhrase2 = new Phrase(product1, font9bold);
////				PdfPCell prodNameCell2 = new PdfPCell(
////						prodNamePhrase2);
////				// generalDisinfestationCell2.addElement(generalDisinfestationPhrase2);//Comment by jayshree Date 25/11/2017
////				prodNameCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
////				prodNameCell2.setVerticalAlignment(Element.ALIGN_CENTER);
////				// generalDisinfestationCell2.setVerticalAlignment(Element.ALIGN_TOP);
////				prodPaymentTab.addCell(prodNameCell2);
//	
//	
//				Phrase paymentPerPhrase2 ;
//				paymentPerPhrase2= new Phrase(payment, font9);
//				PdfPCell paymentPerCell2 = new PdfPCell(paymentPerPhrase2);
//				// generalDisinfestationCell2.addElement(generalDisinfestationPhrase2);//Comment by jayshree Date 25/11/2017
//				paymentPerCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
//				paymentPerCell2.setVerticalAlignment(Element.ALIGN_CENTER);
//				// generalDisinfestationCell2.setVerticalAlignment(Element.ALIGN_TOP);
//				prodPaymentTab.addCell(paymentPerCell2);
//			}
		

//	else
//	{
//		Phrase paymentValPhrase2 = new Phrase(payment, font9);
//		PdfPCell paymentValCell2 = new PdfPCell(
//				paymentValPhrase2);
//		// generalDisinfestationCell2.addElement(generalDisinfestationPhrase2);//Comment by jayshree Date 25/11/2017
//		paymentValCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
//		paymentValCell2.setVerticalAlignment(Element.ALIGN_CENTER);
//		paymentValCell2.setColspan(2);
//		// generalDisinfestationCell2.setVerticalAlignment(Element.ALIGN_TOP);
//		prodPaymentTab.addCell(paymentValCell2);
//	}
	
//	try {
//		document.add(prodPaymentTab);
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	
	
	PdfPTable commentTab = new PdfPTable(2);
	commentTab.setWidthPercentage(100f);
	/**
	 * Date 25/11/2017
	 * comment by Jayshree
	 */
//	commentTab.setSpacingBefore(7f);
//End
	try {
		commentTab.setWidths(new float[] { 30,70 });
	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	
	System.out.println("character"+con.getDescription().length());
	
		
	Phrase commentph =new Phrase("Special Instructions",font9bold);
	PdfPCell commentCell=new PdfPCell(commentph);
	commentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	commentTab.addCell(commentCell);
	
	
	Phrase commentValph =new Phrase(con.getDescription(),font9);
	PdfPCell commentValCell=new PdfPCell(commentValph);
	commentValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	commentValCell.setFixedHeight(40);
	commentTab.addCell(commentValCell);
	
	try {
		document.add(commentTab);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}


// **************************end*****************************

private void createFooterTable() {

//	PdfPTable footerTab = new PdfPTable(1);
//	footerTab.setWidthPercentage(100f);
	
	
//	footerTab.setSpacingBefore(2f);//By jayshree Date 25/11/2017

//	Phrase footerTabPhrase = new Phrase(
//			"I have read and accepted the terms and conditions printed overleaf.",
//			font9);
//	PdfPCell footerTabCell = new PdfPCell(footerTabPhrase);
////	footerTabCell.addElement(footerTabPhrase);
//	footerTabCell.setBorder(0);
//	footerTabCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	footerTab.addCell(footerTabCell);
	
	
	
		
	PdfPTable footerTab = new PdfPTable(4);
	footerTab.setWidthPercentage(100f);
	try {
		footerTab.setWidths(new float[] {19, 3, 6,72});
//		footerTab.setWidths(new float[] {19, 11, 70});

	} catch (DocumentException e1) {
		e1.printStackTrace();
	}
	
	Phrase colPhrase6 = new Phrase(":", font9);
	PdfPCell colCell6 = new PdfPCell(colPhrase6);
	colCell6.setBorder(0);
	colCell6.setHorizontalAlignment(Element.ALIGN_LEFT);

	Phrase blank = new Phrase(" ", font10);
	PdfPCell blankCell = new PdfPCell(blank);
	blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	blankCell.setBorder(0);
//	footerTab.addCell(blankCell);
	
	Phrase blank1 = new Phrase(" ", font10);
	PdfPCell blankCell1 = new PdfPCell(blank1);
	blankCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	blankCell1.setBorder(0);
//	footerTab.addCell(blankCell1);
	
	footerTab.addCell(blankCell1);
	footerTab.addCell(blankCell1);
	footerTab.addCell(blankCell1);
	footerTab.addCell(blankCell1);
//	footerTab.addCell(blankCell1);


	footerTab.addCell(blankCell1);
	footerTab.addCell(blankCell1);
	footerTab.addCell(blankCell1);
	footerTab.addCell(blankCell1);
//	footerTab.addCell(blankCell1);


	
	Phrase footerTabPhrase = new Phrase("Accepted by",font9);
	PdfPCell footerTabCell = new PdfPCell(footerTabPhrase);
	footerTabCell.setBorder(0);
	footerTabCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell.setVerticalAlignment(Element.ALIGN_CENTER);
	footerTab.addCell(footerTabCell);
	
	footerTab.addCell(colCell6);
	
	
	PdfPCell imageCell = new PdfPCell();
	Image image1 = null;
	try {
		image1 = Image.getInstance("images/tickmarkimage.png");
		image1.scalePercent(7f);
		image1.setAlignment(Element.ALIGN_CENTER);
		// image1.setAbsolutePosition(40f,765f);
		// doc.add(image1);
		
		String pocName = "";
		if(con.getCinfo().getPocName()!=null){
			if(cust.getSalutation()!=null && !cust.getSalutation().equals("")){
				pocName = "  "+cust.getSalutation() +" "+con.getCinfo().getPocName();
			}
			else{
				pocName = "  "+ con.getCinfo().getPocName();
			}
		}
		
//		imageCell = new PdfPCell(image1);
////		imageCell.setImage(image1);
//		imageCell.setFixedHeight(15);
//		imageCell.setBorder(0);
//		imageCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase pocNamepharse = new Phrase(pocName,font9);

		Chunk chunk = new Chunk(image1,0,0,true);
		
		Phrase phchunk = new Phrase(chunk);

		Paragraph phimagewithtext = new Paragraph();
		phimagewithtext.add(phchunk);
		phimagewithtext.add(pocNamepharse);

		
		
		PdfPCell cell = new PdfPCell();
		cell.addElement(phimagewithtext);
		cell.setBorder(0);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_CENTER);

		
		PdfPCell cellwithtext = new PdfPCell();
		cellwithtext.addElement(pocNamepharse);
		cellwithtext.setBorder(0);
		cellwithtext.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellwithtext.setVerticalAlignment(Element.ALIGN_CENTER);

		
		PdfPCell cellimage = new PdfPCell();
		cellimage.addElement(image1);
		cellimage.setBorder(0);
		cellimage.setHorizontalAlignment(Element.ALIGN_CENTER);
		cellimage.setVerticalAlignment(Element.ALIGN_CENTER);
//		cellimage.setColspan(2);

		if(con.getAcceptedDateTime()!=null && con.getIpAddress()!=null && !con.getIpAddress().equals("")) {

		
//			footerTab.addCell(cell);
			footerTab.addCell(cellimage);
			footerTab.addCell(cellwithtext);

		}
		else {
			footerTab.addCell(blankCell1);
			footerTab.addCell(blankCell1);

		}


		
		
		
	} catch (Exception e) {
		e.printStackTrace();
	}

	
	
	Phrase footerTabPhrase1 = new Phrase("Acceptance Date & Time",font9);
	PdfPCell footerTabCell1 = new PdfPCell(footerTabPhrase1);
	footerTabCell1.setBorder(0);
	footerTabCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
	footerTab.addCell(footerTabCell1);
	
	footerTab.addCell(colCell6);

	
	String stracceptanceDateTime = "";
	if(con.getAcceptedDateTime()!=null) {
		stracceptanceDateTime = datetimeformat.format(con.getAcceptedDateTime());
	}
	Phrase acceptanceValuePhrase = new Phrase(stracceptanceDateTime, font9);
	PdfPCell acceptanceValueCell = new PdfPCell(acceptanceValuePhrase);
	acceptanceValueCell.setBorder(0);
	acceptanceValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	acceptanceValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	acceptanceValueCell.setColspan(2);
	footerTab.addCell(acceptanceValueCell);
	
//	footerTab.addCell(blankCell1);
//	footerTab.addCell(blankCell1);

	Phrase footerTabPhrase11 = new Phrase("IP Address",font9);
	PdfPCell footerTabCell11 = new PdfPCell(footerTabPhrase11);
	footerTabCell11.setBorder(0);
	footerTabCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
	footerTab.addCell(footerTabCell11);
	
	
	footerTab.addCell(colCell6);

	String ipAddress = "";
	if(con.getIpAddress()!=null) {
		ipAddress = con.getIpAddress();
	}
	Phrase phipAddress = new Phrase(ipAddress, font9);
	PdfPCell ipAddressValueCell = new PdfPCell(phipAddress);
	ipAddressValueCell.setBorder(0);
	ipAddressValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
	ipAddressValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	ipAddressValueCell.setColspan(2);

	footerTab.addCell(ipAddressValueCell);
	
//	footerTab.addCell(blankCell1);

	
//	Phrase blank = new Phrase(" ", font10);
//	PdfPCell blankCell = new PdfPCell(blank);
//	blankCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	blankCell.setBorder(0);
//	footerTab.addCell(blankCell);
//	
//	Phrase blank1 = new Phrase(" ", font10);
//	PdfPCell blankCell1 = new PdfPCell(blank1);
//	blankCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	blankCell1.setBorder(0);
//	footerTab.addCell(blankCell1);
//	
//	Phrase footerTabPhrase = new Phrase("Accepted by : ",font9);
//	PdfPCell footerTabCell = new PdfPCell(footerTabPhrase);
//	footerTabCell.setBorder(0);
//	footerTabCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	footerTab.addCell(footerTabCell);
//	
//	Phrase footerTabPhrase1 = new Phrase("Acceptance Date & Time : ",font9);
//	PdfPCell footerTabCell1 = new PdfPCell(footerTabPhrase1);
//	footerTabCell1.setBorder(0);
//	footerTabCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	footerTab.addCell(footerTabCell1);
//	
//	Phrase footerTabPhrase11 = new Phrase("IP Address : ",font9);
//	PdfPCell footerTabCell11 = new PdfPCell(footerTabPhrase11);
//	footerTabCell11.setBorder(0);
//	footerTabCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
//	footerTabCell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	footerTab.addCell(footerTabCell11);
	
	

//	try {
//		document.add(footerTab);
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	
	

	
//*****
	PdfPTable footerTab1 = new PdfPTable(1);
	footerTab1.setWidthPercentage(100f);
//	footerTab1.setSpacingBefore(75);

	
	Phrase footerBlankPhrase2 = new Phrase(" ");
	PdfPCell footerBlankCell2 = new PdfPCell(footerBlankPhrase2);
	footerBlankCell2.setBorder(0);
//	footerBlankCell2.addElement(footerBlankPhrase2);//Comment by jayshree Date 25/11/2017
//	footerTab1.addCell(footerBlankCell2);
//	footerTab1.addCell(footerBlankCell2);
//	footerTab1.addCell(footerBlankCell2);
//	footerTab1.addCell(footerBlankCell2);
//	footerTab1.addCell(footerBlankCell2);
//	footerTab1.addCell(footerBlankCell2);
	
	
	

//	Phrase onBehalfPhrase = new Phrase("On Behalf of Client", font9bold);
//	PdfPCell onBehalfCell = new PdfPCell(onBehalfPhrase);
//	// onBehalfCell.addElement(onBehalfPhrase);//Comment by jayshree Date 25/11/2017
//	onBehalfCell.setBorder(0);
//	// authorisedCell.setColspan(2);
//	onBehalfCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//	onBehalfCell.setVerticalAlignment(Element.ALIGN_TOP);
//	onBehalfCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//	footerTab1.addCell(onBehalfCell);
	
	//******
	PdfPTable footerTab2 = new PdfPTable(1);
	footerTab2.setWidthPercentage(100f);
	//footerTab1.setSpacingBefore();

	
	
	DocumentUpload digitalDocument = comp.getUploadDigitalSign();
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}
	PdfPCell imageSignCell = null;
	Image image2 = null;
	try {
		image2 = Image.getInstance(new URL(hostUrl
				+ digitalDocument.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(100f);

		imageSignCell = new PdfPCell(image2);
		imageSignCell.setBorder(0);
		imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

	} catch (Exception e) {
		e.printStackTrace();
	}

	Phrase signAuth;
	
		if (comp.getSignatoryText() != null
				&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
			signAuth = new Phrase(" ", font9);  //Phrase authorisedPhrase2 = new Phrase();
		} else {
			signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
		}
	
	
	PdfPCell signParaCell = new PdfPCell(signAuth);
	signParaCell.setBorder(0);
	//	signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	/**
	 * ends here
	 */
	

	
	
	
//	DocumentUpload digitalDocument = comp.getUploadDigitalSign();
//	String hostUrl;
//	String environment = System
//			.getProperty("com.google.appengine.runtime.environment");
//	if (environment.equals("Production")) {
//		String applicationId = System
//				.getProperty("com.google.appengine.application.id");
//		String version = System
//				.getProperty("com.google.appengine.application.version");
//		hostUrl = "http://" + version + "." + applicationId
//				+ ".appspot.com/";
//	} else {
//		hostUrl = "http://localhost:8888";
//	}
//	PdfPCell imageSignCell = null;
////	Image image2 = null;
////	logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
////	try {
////		image2 = Image.getInstance(new URL(hostUrl
////				+ digitalDocument.getUrl()));
////		image2.scalePercent(15f);
////		image2.scaleAbsoluteWidth(100f);
////
////		 
////	imageSignCell = new PdfPCell(image2);
////	imageSignCell.setBorder(0);
////	imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
////
////	}
////	catch (Exception e) {
////		e.printStackTrace();
////	}
//	
//	
////	Image image1 = null;
////	try {
////		image1 = Image.getInstance("images/digisign2copy.png");
////	} catch (BadElementException e2) {
////		// TODO Auto-generated catch block
////		e2.printStackTrace();
////	} catch (MalformedURLException e2) {
////		// TODO Auto-generated catch block
////		e2.printStackTrace();
////	} catch (IOException e2) {
////		// TODO Auto-generated catch block
////		e2.printStackTrace();
////	}
//////	String image1="";
////	image1.scalePercent(15f);
////	image1.scaleAbsoluteWidth(100f);
////	imageSignCell=new PdfPCell(image1);
////	imageSignCell.setBorder(0);
//////		 if (authOnLeft) {
//////			 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//////		 } else {
////			 imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//////		 }
//////		 }
//////	 	catch(Exception e)
//////	 	{
//////	 	e.printStackTrDayace();
//////		 }
//////	 	
//	
	if(imageSignCell!=null)
	{
		footerTab2.addCell(blankCell1);
		footerTab2.addCell(blankCell1);
		footerTab2.addCell(imageSignCell);
	}
	else
	{
	Phrase blank3=new Phrase(" ",font9);
	PdfPCell blankCell3=new PdfPCell(blank3);
	blankCell3.setBorder(0);	
	footerTab2.addCell(blankCell3);
	footerTab2.addCell(blankCell3);
	footerTab2.addCell(blankCell3);
	footerTab2.addCell(blankCell3);
	footerTab2.addCell(blankCell3);//By jayshree
//	footerTab2.addCell(blankCell);
//	footerTab2.addCell(blankCell);
//	footerTab2.addCell(blankCell);
//	footerTab2.addCell(blankCell);
	
	}
		

	
	
	signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	signParaCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	footerTab2.addCell(signParaCell);
	/**
	 * Date : 22-03-2021 Added by : Priyanka
	 * Des : Consider branch as company flow for Pecopp.
	 */
	
	String companyname ="";
	if(branchDt!=null&&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
		companyname=branchDt.getCorrespondenceName();
	}else{
	 companyname = comp.getBusinessUnitName().trim().toUpperCase();
	}
	
	Phrase pecoppFooterPhrase = new Phrase("For "+companyname, font9bold);
	PdfPCell pecoppFooterCell = new PdfPCell(pecoppFooterPhrase);
	// pecoppFooterCell.addElement(pecoppFooterPhrase);//Comment by jayshree Date 25/11/2017
	pecoppFooterCell.setBorder(0);
	// authorisedCell.setColspan(2);
	pecoppFooterCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
	pecoppFooterCell.setVerticalAlignment(Element.ALIGN_TOP);
	footerTab2.addCell(pecoppFooterCell);

	PdfPTable footerTable = new PdfPTable(1);
	footerTable.setWidthPercentage(100f);
	footerTable.setSpacingBefore(7f);//By Jayshree Date25/11/2017

//	try {
//		footerTable.setWidths(new float[] { 30, 40, 35 });
////		footerTable.setWidths(new float[] { 100 });
//
//	} catch (DocumentException e1) {
//		e1.printStackTrace();
//	}
	
	
//	
//	PdfPCell footerBlankCel = new PdfPCell(footerTab1);
//	footerBlankCel.setBorder(0);
////	footerBlankCel.addElement(footerTab1);
//	footerTable.addCell(footerBlankCel);
//	
//	Phrase footerBlankph = new Phrase(" ");
//	PdfPCell footerBlank = new PdfPCell(footerBlankph);
//	footerBlank.setBorder(0);
////	footerBlank.addElement(footerBlankph);
//	footerTable.addCell(footerBlank);
	
	PdfPCell footercell2 = new PdfPCell(footerTab2);
	footercell2.setBorder(0);
//	footercell.addElement(footerTab2);
	footerTable.addCell(footercell2);
	
	

//	try {
//		document.add(footerTable);
//	} catch (DocumentException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
	
	PdfPTable mainTable = new PdfPTable(2);
	mainTable.setWidthPercentage(100);
	try {
		mainTable.setWidths(columnHalfWidth);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	
	PdfPCell footercell = new PdfPCell(footerTab);
	footercell.setBorder(0);
	
	
	mainTable.addCell(footercell);
	mainTable.addCell(footercell2);
	mainTable.addCell(blankCell);
	mainTable.addCell(blankCell);

	try {
		document.add(mainTable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}


	
}

private void createRestProductAnnexure(int prodSize) {

	try {
		document.add(Chunk.NEXTPAGE);
	} catch (DocumentException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
	try {
		document.add(new Phrase("Annexure",font9bold));
	} catch (DocumentException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
	
		createProductHeadertab();
//		double disc=con.getDiscountAmt();
	
		
	for (int i = productCount; i < con.getItems().size(); i++) {
		
		System.out.println(" remaining product"+productCount);
		PdfPTable conProductDetail = new PdfPTable(4);
		conProductDetail.setWidthPercentage(100f);

		try {
			conProductDetail.setWidths(new float[] { 30, 30,25, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		String productName=con.getItems().get(i).getProductName();
		if(productName.contains("-"))
		{
			String[] product=productName.split(("-"));
			 product1=product[0];
			 product2=product[1];
		}
		else
		{
			product1=con.getItems().get(i).getProductName();
			product2="";
		}
			
		

		
		Phrase generalDisinfestationPhrase = new Phrase(product1, font9bold);
		PdfPCell generalDisinfestationCell = new PdfPCell(
				generalDisinfestationPhrase);
		// generalDisinfestationCell.addElement(generalDisinfestationPhrase);
		generalDisinfestationCell
				.setHorizontalAlignment(Element.ALIGN_LEFT);
		generalDisinfestationCell
				.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// generalDisinfestationCell.setVerticalAlignment(Element.ALIGN_TOP);
		conProductDetail.addCell(generalDisinfestationCell);

		Phrase generalDisinfestationFrequencyPhrase = new Phrase(product2+"("+con
				.getItems().get(i).getNumberOfServices()
				+ "  Services)", font8);
		PdfPCell generalDisinfestationFrequencyCell = new PdfPCell(
				generalDisinfestationFrequencyPhrase);
		// generalDisinfestationFrequencyCell.addElement(generalDisinfestationFrequencyPhrase);
		generalDisinfestationFrequencyCell
				.setHorizontalAlignment(Element.ALIGN_LEFT);
		generalDisinfestationFrequencyCell
				.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// generalDisinfestationFrequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
		conProductDetail.addCell(generalDisinfestationFrequencyCell);
		
		Phrase specificationPhrase = new Phrase(con.getItems().get(i).getSpecification(), font9bold);
		PdfPCell specificationCell = new PdfPCell(specificationPhrase);
		// frequencyCell.addElement(frequencyPhrase);
		specificationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
		specificationCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		conProductDetail.addCell(specificationCell);
		
		double assessTotal=getAssessTotalAmount(con.getItems().get(i));
		Phrase generalDisinfestationAmountPhrase = new Phrase(df.format(assessTotal)
				+ "", font9bold);
		PdfPCell generalDisinfestationAmountCell = new PdfPCell(
				generalDisinfestationAmountPhrase);
		// generalDisinfestationAmountCell.addElement(generalDisinfestationAmountPhrase);
		generalDisinfestationAmountCell
				.setHorizontalAlignment(Element.ALIGN_CENTER);
		generalDisinfestationAmountCell
				.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// generalDisinfestationAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
		conProductDetail.addCell(generalDisinfestationAmountCell);

		totalamt = totalamt + assessTotal;
		
		try {
			document.add(conProductDetail);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

		
	}
}



private void createCompanyNameAsHeader(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadHeader();

	// patch
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl
				+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 725f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

private void createCompanyNameAsFooter(Document doc, Company comp) {

	DocumentUpload document = comp.getUploadFooter();

	// patch
	String hostUrl;
	String environment = System
			.getProperty("com.google.appengine.runtime.environment");
	if (environment.equals("Production")) {
		String applicationId = System
				.getProperty("com.google.appengine.application.id");
		String version = System
				.getProperty("com.google.appengine.application.version");
		hostUrl = "http://" + version + "." + applicationId
				+ ".appspot.com/";
	} else {
		hostUrl = "http://localhost:8888";
	}

	try {
		Image image2 = Image.getInstance(new URL(hostUrl
				+ document.getUrl()));
		image2.scalePercent(15f);
		image2.scaleAbsoluteWidth(520f);
		image2.setAbsolutePosition(40f, 40f);
		doc.add(image2);
	} catch (Exception e) {
		e.printStackTrace();
	}

}

	private void createRestProductDetail() {
		if (noOfLine == 0 && productCount != 0) {
			createRestProductAnnexure(productCount);
		}
	}

private void createBlankforUPC() {
	Phrase blankphrase = new Phrase("", font9);
	PdfPCell blankCell = new PdfPCell(blankphrase);
//	blankCell.addElement(blankphrase);
	blankCell.setBorder(0);

	PdfPTable titlepdftable = new PdfPTable(3);
	titlepdftable.setWidthPercentage(100);
	titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
	titlepdftable.addCell(blankCell);
	titlepdftable.addCell(blankCell);

	Paragraph blank = new Paragraph();
	blank.add(Chunk.NEWLINE);

	PdfPTable parent = new PdfPTable(1);
	parent.setWidthPercentage(100);

	PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
	titlePdfCell.setBorder(0);
	parent.addCell(titlePdfCell);

	try {
		document.add(blank);
		document.add(blank);
		document.add(blank);
		document.add(blank);
		//document.add(blank);
		document.add(parent);
	} catch (DocumentException e) {
		e.printStackTrace();
	}
}


private double removeAllTaxes(SuperProduct entity) {
	
	
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
		if(entity instanceof ServiceProduct)
		{
			ServiceProduct prod=(ServiceProduct) entity;
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
		}

		if(entity instanceof ItemProduct)
		{
			ItemProduct prod=(ItemProduct) entity;
			if(prod.getVatTax()!=null&&prod.getVatTax().isInclusive()==true){
				vat=prod.getVatTax().getPercentage();
			}
			if(prod.getServiceTax()!=null&&prod.getServiceTax().isInclusive()==true){
				service=prod.getServiceTax().getPercentage();
			}
		}
		
		if(vat!=0&&service==0){
			retrVat=entity.getPrice()/(1+(vat/100));
			retrVat=entity.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=entity.getPrice()/(1+service/100);
			retrServ=entity.getPrice()-retrServ;
		}
		if(service!=0&&vat!=0){
//			// Here if both are inclusive then first remove service tax and then on that amount
//			// calculate vat.
//			double removeServiceTax=(entity.getPrice()/(1+service/100));
//						
//			//double taxPerc=service+vat;
//			//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
//			retrServ=(removeServiceTax/(1+vat/100));
//			retrServ=entity.getPrice()-retrServ;
			
			
			if(entity instanceof ItemProduct)
			{
				ItemProduct prod=(ItemProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{

					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
//					retrVat=0;

				}
				else{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
			
			if(entity instanceof ServiceProduct)
			{
				ServiceProduct prod=(ServiceProduct) entity;
				if(prod.getServiceTax().getTaxPrintName()!=null && ! prod.getServiceTax().getTaxPrintName().equals("")
				   && prod.getVatTax().getTaxPrintName()!=null && ! prod.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(entity.getPrice()/(1+dot/100));
					retrServ=entity.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(entity.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=entity.getPrice()-retrServ;
				}
			}
		}
		tax=retrVat+retrServ;
		return tax;
	
}


}
