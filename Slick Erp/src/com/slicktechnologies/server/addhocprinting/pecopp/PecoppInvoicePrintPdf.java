package com.slicktechnologies.server.addhocprinting.pecopp;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.paymentmodelayer.companypayment.CompanyPayment;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

/**
 * 
 * @author Jayshree Description :For Pecopp Pdf : It will print Invoice Pecopp
 *         Called From : PecoppPdfServlet.java on print of Invoice Date: 10 Spet
 *         2017 Created By : Jayshree Chavan
 */
public class PecoppInvoicePrintPdf {

	public Document document;

	Customer cust;
	Company comp;
	Invoice invoiceentity;
	Contract con;
	double totalamt = 0;
	String product1 = "";
	String product2 = "";
	/**
	 * Date 27/11/2017
	 * By jayshree
	 * Des.increse the noofline to add the seven product in first page
	 */
	Logger logger = Logger.getLogger("NameOfYourLogger");
	int noOfLine = 7, productCount = 0;
	//End by jayshree
	private Font font16boldul, font12bold, font8bold, font8, font9bold,
			font12Wbold, font12boldul, font10boldul, font12, font16bold,
			font10, font10bold, font10ul, font9boldul, font14bold, font9,
			font7, font7bold, font9red, font9boldred, font12boldred,font11,font23;

	private SimpleDateFormat fmt = new SimpleDateFormat("dd-MMM-yyyy");
	private SimpleDateFormat fmt2 = new SimpleDateFormat("dd/MM/yyyy");
	DecimalFormat df = new DecimalFormat("0.00");

	List<ContractCharges> billingTaxesLis; 
	List<CustomerBranchDetails> custbranchlist;
	List<CustomerBranchDetails> customerbranchlist;
	List<PaymentTerms> payTermsLis;
	List<CompanyPayment>comppayment = new ArrayList<CompanyPayment>(); //Added by Ashwini
	
	CompanyPayment compPayment;
	ProcessConfiguration processConfig;
	List<State> stateList;
	boolean productDescFlag = false, upcflag = false, UniversalFlag = false,
			multipleCompanyName = false, printPremiseDetails = false, isPlaneFormat = false;

	SimpleDateFormat sdf;
	/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
	//Branch companyBranch = null;
	/**
	 *  Date : 11-03-2021 by Priyanka
	 *  Des : - BranchAsCompany Process configuration activation.
	 */
	Branch branchDt = null;
	
	
	CustomerBranchDetails customerBranch=null;
	/**
	 * @author Vijay Date 18-10-2021
	 * Des :- added for UPCL invoice print with process configuration
	 */
	boolean printCustomCopy;
	
	public PecoppInvoicePrintPdf() {
		super();

		font16boldul = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD
				| Font.UNDERLINE);
		font16bold = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
		font12bold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
		font8bold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
		font8 = new Font(Font.FontFamily.HELVETICA, 9);
		font12boldul = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD
				| Font.UNDERLINE);
		font12 = new Font(Font.FontFamily.HELVETICA, 12);   //
		font11 = new Font(Font.FontFamily.HELVETICA, 11);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);
		font10 = new Font(Font.FontFamily.HELVETICA, 10);
		font10bold = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);
		font10ul = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL
				| Font.UNDERLINE);
		font14bold = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		font10boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9bold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD);
		font9boldul = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD
				| Font.UNDERLINE);
		font9boldred = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD,
				BaseColor.RED);
		font9 = new Font(Font.FontFamily.HELVETICA, 8);
		font9red = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL,
				BaseColor.RED);
		font7 = new Font(Font.FontFamily.HELVETICA, 7);
		font7bold = new Font(Font.FontFamily.HELVETICA, 7, Font.BOLD);
		font23 = new Font(Font.FontFamily.HELVETICA, 23);
		font12Wbold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD,
				BaseColor.WHITE);

		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

	}

	public void loadPecoppInvoice(long count) {
		// Load Invoice
		invoiceentity = ofy().load().type(Invoice.class).id(count).now();
		//
		// billingDoc=invoiceentity.getArrayBillingDocument();
		// invoiceOrderType=invoiceentity.getTypeOfOrder().trim();
		// arrPayTerms=invoiceentity.getArrPayTerms();
		// Load Customer

		/*
		 * Date:19/10/2018
		 * Developer:Ashwini
		 */
		
		comppayment = ofy().load().type(CompanyPayment.class)
				.filter("companyId", invoiceentity.getCompanyId()).filter(" paymentStatus", true).list();
		
		/*
		 * end by Ashwini
		 */
		
		if (invoiceentity.getCompanyId() == null)
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.first().now();
		else
			cust = ofy().load().type(Customer.class)
					.filter("count", invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		// Load Company
		if (invoiceentity.getCompanyId() == null)
			comp = ofy().load().type(Company.class).first().now();
		else
			comp = ofy().load().type(Company.class)
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();

		if (invoiceentity.getCompanyId() != null)
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount())
					.filter("companyId", invoiceentity.getCompanyId()).first()
					.now();
		else
			con = ofy().load().type(Contract.class)
					.filter("count", invoiceentity.getContractCount()).first()
					.now();

		// payTermsLis = con.getPaymentTermsList();

		if (invoiceentity.getCompanyId() == null)
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount()).list();
		else
			custbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		/****************************** vijay ************************/

		System.out.println("Branch name======"
				+ invoiceentity.getCustomerBranch());
		if (invoiceentity.getCompanyId() == null)
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch()).list();
		else
			customerbranchlist = ofy()
					.load()
					.type(CustomerBranchDetails.class)
					.filter("cinfo.count",
							invoiceentity.getPersonInfo().getCount())
					.filter("buisnessUnitName",
							invoiceentity.getCustomerBranch())
					.filter("companyId", invoiceentity.getCompanyId()).list();

		System.out.println("Banch updated====="
				+ invoiceentity.getCustomerBranch());
		/****************************** vijay ************************/

		stateList = ofy().load().type(State.class)
				.filter("companyId", invoiceentity.getCompanyId()).list();

		/************************************ Letter Head Flag *******************************/

		if (invoiceentity.getCompanyId() != null) {
			processConfig = ofy().load().type(ProcessConfiguration.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("processName", "Invoice")
					.filter("configStatus", true).first().now();
			if (processConfig != null) {
				for (int k = 0; k < processConfig.getProcessList().size(); k++) {
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("CompanyAsLetterHead")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						upcflag = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim().equalsIgnoreCase("OnlyForUniversal")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						UniversalFlag = true;
					}

					if (processConfig
							.getProcessList()
							.get(k)
							.getProcessType()
							.trim()
							.equalsIgnoreCase(
									"PrintMultipleCompanyNamesFromInvoiceGroup")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						multipleCompanyName = true;
					}

					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("printProductPremisesInPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printPremiseDetails = true;
					}
					
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintProductDescriptionOnPdf")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						productDescFlag = true;
					}
					if (processConfig.getProcessList().get(k).getProcessType()
							.trim()
							.equalsIgnoreCase("PrintCustomInvoiceCopy")
							&& processConfig.getProcessList().get(k).isStatus() == true) {
						printCustomCopy = true;
					}
				}
			}		
		}
		/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
		if(invoiceentity.getBranch() != null){
			if(invoiceentity.getCompanyId() != null){
				branchDt = ofy().load().type(Branch.class).filter("companyId", invoiceentity.getCompanyId())
						 		.filter("buisnessUnitName", invoiceentity.getBranch()).first().now();
			}else{
				branchDt = ofy().load().type(Branch.class).filter("buisnessUnitName", invoiceentity.getBranch()).first().now();
			}
		}
		
		ArrayList<String> custbranchlist=getCustomerBranchList(con.getItems());
		
		if(custbranchlist!=null&&custbranchlist.size()==1&& custbranchlist.contains("Service Address")==false){
				logger.log(Level.SEVERE,"In Side AList1:");
				customerBranch= ofy().load().type(CustomerBranchDetails.class)
							.filter("cinfo.count",con.getCinfo().getCount())
							.filter("companyId", con.getCompanyId())
							.filter("buisnessUnitName", custbranchlist.get(0)).first().now();
				
				logger.log(Level.SEVERE,"AList1:" +customerBranch);
				logger.log(Level.SEVERE,"AList2:" +custbranchlist.size());
			}
		
		
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", comp.getCompanyId())){
			
			logger.log(Level.SEVERE,"Process active --");
			if(invoiceentity !=null && invoiceentity.getBranch() != null && invoiceentity.getBranch().trim().length() >0){
				
				branchDt = ofy().load().type(Branch.class).filter("companyId",invoiceentity.getCompanyId()).filter("buisnessUnitName", con.getBranch()).first().now();
				
			
				if(branchDt!=null){
					logger.log(Level.SEVERE,"Process active --"+branchDt.getPaymentMode());		
					
					if(branchDt.getPaymentMode()!=null && !branchDt.getPaymentMode().trim().equals("")){
						
					
					List<String> paymentDt = Arrays.asList(branchDt.getPaymentMode().trim().split("/"));
					
					if(paymentDt.get(0).trim().matches("[0-9]+")){
						
						
						
						int payId = Integer.parseInt(paymentDt.get(0).trim());
						
						compPayment = ofy().load().type(CompanyPayment.class)
								.filter("count", payId)
								.filter("companyId", con.getCompanyId()).first()
								.now();
						
						
						if(compPayment != null){
							
							}
						
						}
					}
					
					comp = ServerAppUtility.changeBranchASCompany(branchDt, comp);
				}
			}			
		}
		//By Ashwini Patil to Retrieve CompanyPayment info from InvoiceEntity
		if(invoiceentity.getPaymentMode()!=null && invoiceentity.getPaymentMode().trim().length()>0){

			List<String> paymentDt = Arrays.asList(invoiceentity.getPaymentMode().trim().split("/"));
			
			if(paymentDt.get(0).trim().matches("[0-9]+")){
				
				int payId = Integer.parseInt(paymentDt.get(0).trim());
				
				compPayment = ofy().load().type(CompanyPayment.class)
						.filter("count", payId)
						.filter("companyId", invoiceentity.getCompanyId()).first()
						.now();
				
			}
		
		}
		
		
		
	}

	private ArrayList<String> getCustomerBranchList(List<SalesLineItem> itemList) {
		HashSet<String> branchHs=new HashSet<String>();
		for(SalesLineItem itemObj:itemList){
			if(itemObj.getCustomerBranchSchedulingInfo()!=null){
				ArrayList<BranchWiseScheduling> branchSchedulingList = itemObj.getCustomerBranchSchedulingInfo().get(itemObj.getProductSrNo());
				for(BranchWiseScheduling obj:branchSchedulingList){
					if(obj.isCheck()==true){
						branchHs.add(obj.getBranchName());
					}
				}
			}
		}
		
		if(branchHs!=null&&branchHs.size()!=0){
			ArrayList<String> branchList=new ArrayList<String>(branchHs);
			logger.log(Level.SEVERE,"In Side AList3:"+branchList.size());
			return branchList;
		}
		
		return null;
	}
	/**
	 * @author Priyanka Bhagwat - 27-01-2021
	 * Des :- Old format print of PDF and header and footer not print on pdf issue raised by Ashwini.
	 */
	public void createInvoicePdf(String preprintStatus) {
		if(upcflag==false && preprintStatus.equals("plane")){
			//createBlankforUPC();
			createLogo(document, comp);
			createCompanyHeadding();
			}
		else{
			if(preprintStatus.equals("yes")){
					createBlankforUPC();
				}
			
			if(preprintStatus.equals("no")){
			System.out.println("inside prit no");
			//createBlankforUPC();
			if(comp.getUploadHeader()!=null){
				createCompanyNameAsHeader(document,comp);
				}
				
			if(comp.getUploadFooter()!=null){
				createCompanyNameAsFooter(document,comp);
				}
			  }
		   }
		
		
		createInvoiceSample();
//		createInvoiceSample();

		
	}

	private void createInvoiceSample() {
		if(upcflag) {
		createBlankforUPC();
		  }
		//createLogo(document, comp);
		//createCompanyHeadding();
		createHeaderTable();
		createInvoiceHeaderTable();
		createCustomerTable();
		createPeriodTable();
		createProductHeaderTable();
		createProductDetailTable();
		createTaxDetailTable();
		createComTaxTable();
		createPaymentTable();
//		createInvoiceFootertable();
		createFooterTable();
		// createRestProductAnnexure(9);
		createRestProductDetail();
		createBranchInfoTab();
	   
		
	}

	/**
	 *   Added By Priyanka 
	 *   When Branch As process configuration Active then company heading and logo should be print
	 */
	
public void createCompanyHeadding() {
		
	    String companyname ="";
		if(branchDt != null &&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyname=" "
					+  branchDt.getCorrespondenceName();
		}else{
		 companyname = " "
				+ comp.getBusinessUnitName().trim().toUpperCase();
		}

		Paragraph companynamepara = new Paragraph();
		companynamepara.add(companyname);
		companynamepara.setFont(font14bold);
		companynamepara.setAlignment(Element.ALIGN_CENTER);

		PdfPCell companynamecell = new PdfPCell();
		companynamecell.addElement(companynamepara);
		companynamecell.setBorder(0);
		

		
		String custAdd1="";
		String custFullAdd1="";
		
		if(comp.getAddress()!=null){
			
			if(comp.getAddress().getAddrLine2()!=null && !comp.getAddress().getAddrLine2().equals("")){
			
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("")){
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2()+","+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1()+","+comp.getAddress().getAddrLine2();
				}
			}else{
				if(comp.getAddress().getLandmark()!=null && !comp.getAddress().getLandmark().equals("") ){
					custAdd1=comp.getAddress().getAddrLine1()+"\n"+comp.getAddress().getLandmark();
				}else{
					custAdd1=comp.getAddress().getAddrLine1();
				}
			}
			
			if(comp.getAddress().getLocality()!=null&& !comp.getAddress().getLocality().equals("")){
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getPin()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getLocality()+","+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}else{
				if(comp.getAddress().getPin()!=0){
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getPin()+","+comp.getAddress().getCity()+", "+comp.getAddress().getState()+", "+comp.getAddress().getCountry();
				}else{
					custFullAdd1=custAdd1+"\n"+comp.getAddress().getCity()+","+comp.getAddress().getState()+","+comp.getAddress().getCountry();
				}
			}
			
		}	
		
			Phrase addressline=new Phrase(custFullAdd1,font11);
	
			Paragraph addresspara=new Paragraph();
			addresspara.add(addressline);
			addresspara.setAlignment(Element.ALIGN_CENTER);
			
			PdfPCell addresscell=new PdfPCell();
			addresscell.addElement(addresspara);
			addresscell.setBorder(0);
		   
		
		
		String contactinfo="";
		if (comp.getCellNumber1() != null && comp.getCellNumber1() != 0) {
			contactinfo =  "Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim();
		}
		//Phrase contactinfo=new Phrase("Mobile: "+comp.getCellNumber1()+" Email: "+comp.getEmail().trim(),font10);
		
		Paragraph realmobpara=new Paragraph();
		realmobpara.add(contactinfo);
		realmobpara.setFont(font11);
		realmobpara.setAlignment(Element.ALIGN_CENTER);
		
		PdfPCell contactcell=new PdfPCell();
		contactcell.addElement(realmobpara);
		contactcell.setBorder(0);
		
		PdfPTable table = new PdfPTable(1);
		table.addCell(companynamecell);
		table.addCell(addresscell);
//		table.addCell(localitycell);
		table.addCell(contactcell);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(10f);
		
		PdfPCell cell = new PdfPCell();
		cell.addElement(table);
		cell.setBorderWidthLeft(0);
		cell.setBorderWidthRight(0);
		cell.setBorderWidthTop(0);
		
		PdfPTable parenttable = new PdfPTable(1);
		parenttable.addCell(cell);
		parenttable.setWidthPercentage(100);
		
	
	
	try {
		document.add(parenttable);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
}


private void createLogo(Document doc, Company comp) {
	
	//********************logo for server ********************
	
		DocumentUpload document =comp.getLogo();
		//patch
		String hostUrl; 
		String environment = System.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
		    String applicationId = System.getProperty("com.google.appengine.application.id");
		    String version = System.getProperty("com.google.appengine.application.version");
		    hostUrl = "http://"+version+"."+applicationId+".appspot.com/";
		} else {
		    hostUrl = "http://localhost:8888";
		}
		
		try {
			Image image2 = Image.getInstance(new URL(hostUrl+document.getUrl()));
			image2.scalePercent(20f);
			image2.setAbsolutePosition(40f,765f);	
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void createFooterTable() {

		PdfPTable footerTab = new PdfPTable(1);
		footerTab.setWidthPercentage(100f);
//		footerTab.setSpacingBefore(2f);//By jayshree Date 25/11/2017

		if(printCustomCopy){
			Phrase blank=new Phrase(" ",font9);
			PdfPCell blankCell=new PdfPCell(blank);
			blankCell.setBorder(0);	
			footerTab.addCell(blankCell);	
		}
		
		Phrase footerTabPhrase = new Phrase(
				"I have read and accepted the terms and conditions printed overleaf.",
				font9);
		PdfPCell footerTabCell = new PdfPCell(footerTabPhrase);
//		footerTabCell.addElement(footerTabPhrase);
		footerTabCell.setBorder(0);
		footerTabCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		footerTabCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		footerTab.addCell(footerTabCell);

		

		/*
		 * Date:20-10-2018
		 * Added by Ashwini
		 * Des:To add customer payment details 
		 */
		PdfPTable bankDetailTab = new PdfPTable(3);
		bankDetailTab.setWidthPercentage(100f);
		
		try{
			bankDetailTab.setWidths(new float[]{10,40,50});
		}catch(DocumentException e){
			e.printStackTrace();
		}
		
		Phrase rtgsph=new Phrase("RTGS/NEFT",font9);
		PdfPCell rtgsCell=new PdfPCell(rtgsph);
//		rtgsCell.setBorder(1);
		rtgsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankDetailTab.addCell(rtgsCell);
		
		System.out.println("comppayment size::"+comppayment.size());
		Phrase companyname = null;
		if(comppayment.size()==1){
			System.out.println("first condition"+comppayment.size());
		if(comppayment.get(0).getPaymentComName()!=null){
		 companyname=new Phrase(comppayment.get(0).getPaymentComName(),font9);
		}else{
			companyname=new Phrase("",font9);
		}
		
		PdfPCell companynameCell=new PdfPCell(companyname);
		companynameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankDetailTab.addCell(companynameCell);
		
		Phrase bankdetail=null;
		
		if(comppayment.get(0).getPaymentAccountNo()!=null){
		 bankdetail=new Phrase("Account Number : "+comppayment.get(0).getPaymentAccountNo()+"\n"+"Bank Name : "+comppayment.get(0).getPaymentBankName()+"\n"+comppayment.get(0).getAddressInfo().getCompleteAddress()+"\n"+"IFSC/RTGS/NEFT Code : "+comppayment.get(0).getPaymentIFSCcode(),font9);
		}
		else{
			 bankdetail=new Phrase("",font9);

		}
		PdfPCell bankdetailCell=new PdfPCell(bankdetail);
		bankdetailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		bankDetailTab.addCell(bankdetailCell);
		
		}
		else if(comppayment.size()!=0){ 
			System.out.println("second condition"+comppayment.size());
			
			/**
			 * @author Ashwini Patil
			 * @since 27-01-2022
			 * For Entomist client -to retrieve bank name from selected payment mode and print it on invoice
			 */			
			
			String pmode="";
			String selectedPaymentModeId ="";
			if(invoiceentity.getPaymentMode()!=null && !invoiceentity.getPaymentMode().equals("")) {
				pmode=invoiceentity.getPaymentMode();
				String[] pmodearr=pmode.split("/");
				selectedPaymentModeId=pmodearr[0].trim();
			}
			
			logger.log(Level.SEVERE, "selectedPaymentModeId="+selectedPaymentModeId+"end");
			//if payment mode is selected before printing invoice then code below will get executed. Otherwise payment info will be retrieved either from Branch or InvoiceEntity
			if(!selectedPaymentModeId.equals("")){
				
				for(int p=0;p<comppayment.size();p++){
					System.out.println("second condition 222"+comppayment.size()+"bank="+comppayment.get(p).getPaymentBankName());
					
					int paymentId = Integer.parseInt(selectedPaymentModeId);
					if(comppayment.get(p).getCount() == paymentId)					
					{
						System.out.println("second condition 333"+comppayment.size());
						if(comppayment.get(p).getPaymentComName()!=null){
						 companyname=new Phrase(comppayment.get(p).getPaymentComName(),font9);
						}else{
							companyname=new Phrase("",font9);
						}
						
						PdfPCell companynameCell=new PdfPCell(companyname);
						companynameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						bankDetailTab.addCell(companynameCell);
						
						Phrase bankdetail=null;				
						//from here actual bank details gets printed
						if(comppayment.get(p).getPaymentAccountNo()!=null){	
								bankdetail=new Phrase("Account Number : "+comppayment.get(p).getPaymentAccountNo()+"\n"+"Bank Name : "+comppayment.get(p).getPaymentBankName()+"\n"+comppayment.get(p).getAddressInfo().getCompleteAddress()+"\n"+"IFSC/RTGS/NEFT Code : "+comppayment.get(p).getPaymentIFSCcode(),font9);			
						}
						else{
							 bankdetail=new Phrase("",font9);
						}
						PdfPCell bankdetailCell=new PdfPCell(bankdetail);
						bankdetailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
						bankDetailTab.addCell(bankdetailCell);
						}
					
				}
				
			}
			else if(compPayment!=null){
				
					if(compPayment.getPaymentComName()!=null){
						companyname=new Phrase(compPayment.getPaymentComName(),font9);
					}else{
						companyname=new Phrase("",font9);
					}
					
					PdfPCell companynameCell=new PdfPCell(companyname);
					companynameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					bankDetailTab.addCell(companynameCell);
					
					Phrase bankdetail=null;				
					
					if(compPayment.getPaymentAccountNo()!=null){	
							bankdetail=new Phrase("Account Number : "+compPayment.getPaymentAccountNo()+"\n"+"Bank Name : "+compPayment.getPaymentBankName()+"\n"+compPayment.getAddressInfo().getCompleteAddress()+"\n"+"IFSC/RTGS/NEFT Code : "+compPayment.getPaymentIFSCcode(),font9);			
					}
					else{
						 bankdetail=new Phrase("",font9);
					}
					PdfPCell bankdetailCell=new PdfPCell(bankdetail);
					bankdetailCell.setHorizontalAlignment(Element.ALIGN_LEFT);
					bankDetailTab.addCell(bankdetailCell);
			}		
			
		}
		try{
			if(printCustomCopy){
				document.add(bankDetailTab);
			}
			else{
				document.add(Chunk.NEWLINE);
				document.add(bankDetailTab);
			}
			
			
		}catch(DocumentException e){
			e.printStackTrace();
		}
		
		try {
			document.add(footerTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * end by Ashwini
		 */
	
		PdfPTable footerTab1 = new PdfPTable(1);
		footerTab1.setWidthPercentage(100f);
//		footerTab1.setSpacingBefore(75);

		
		Phrase footerBlankPhrase2 = new Phrase(" ");
		PdfPCell footerBlankCell2 = new PdfPCell(footerBlankPhrase2);
		footerBlankCell2.setBorder(0);
//		footerBlankCell2.addElement(footerBlankPhrase2);//Comment by jayshree Date 25/11/2017
		footerTab1.addCell(footerBlankCell2);
//		footerTab1.addCell(footerBlankCell2);
//		footerTab1.addCell(footerBlankCell2);
//		footerTab1.addCell(footerBlankCell2);
//		footerTab1.addCell(footerBlankCell2);
//		footerTab1.addCell(footerBlankCell2);
		
		Phrase authorisedPhrase = new Phrase("Authorised Signatory", font9);
		PdfPCell authorisedCell = new PdfPCell(authorisedPhrase);
		// authorisedCell.addElement(authorisedPhrase);//Comment by jayshree Date 25/11/2017
		// authorisedCell.setColspan(2);
		authorisedCell.setBorder(0);
		authorisedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		authorisedCell.setVerticalAlignment(Element.ALIGN_BOTTOM);
		footerTab1.addCell(authorisedCell);

		Phrase onBehalfPhrase = new Phrase("On Behalf of Client", font9bold);
		PdfPCell onBehalfCell = new PdfPCell(onBehalfPhrase);
		// onBehalfCell.addElement(onBehalfPhrase);//Comment by jayshree Date 25/11/2017
		onBehalfCell.setBorder(0);
		// authorisedCell.setColspan(2);
		onBehalfCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		onBehalfCell.setVerticalAlignment(Element.ALIGN_TOP);
		onBehalfCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		footerTab1.addCell(onBehalfCell);
		
		//******
		PdfPTable footerTab2 = new PdfPTable(1);
		footerTab2.setWidthPercentage(100f);
		//footerTab1.setSpacingBefore();

		
		
		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		Image image2 = null;
		try {
			image2 = Image.getInstance(new URL(hostUrl
					+ digitalDocument.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(100f);

			imageSignCell = new PdfPCell(image2);
			imageSignCell.setBorder(0);
			imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);

		} catch (Exception e) {
			e.printStackTrace();
		}

		Phrase signAuth;
		
			if (comp.getSignatoryText() != null
					&& comp.getSignatoryText().trim().equalsIgnoreCase("")) {
				signAuth = new Phrase("Authorised Signatory", font9);  //Phrase authorisedPhrase2 = new Phrase();
			} else {
				signAuth = new Phrase(comp.getSignatoryText().trim(), font10bold);
			}
		
		
		PdfPCell signParaCell = new PdfPCell(signAuth);
		signParaCell.setBorder(0);
		//	signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		/**
		 * ends here
		 */
		

		
		
		
//		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
//		String hostUrl;
//		String environment = System
//				.getProperty("com.google.appengine.runtime.environment");
//		if (environment.equals("Production")) {
//			String applicationId = System
//					.getProperty("com.google.appengine.application.id");
//			String version = System
//					.getProperty("com.google.appengine.application.version");
//			hostUrl = "http://" + version + "." + applicationId
//					+ ".appspot.com/";
//		} else {
//			hostUrl = "http://localhost:8888";
//		}
//		PdfPCell imageSignCell = null;
////		Image image2 = null;
////		logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
////		try {
////			image2 = Image.getInstance(new URL(hostUrl
////					+ digitalDocument.getUrl()));
////			image2.scalePercent(15f);
////			image2.scaleAbsoluteWidth(100f);
//	//
////			 
////		imageSignCell = new PdfPCell(image2);
////		imageSignCell.setBorder(0);
////		imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//	//
////		}
////		catch (Exception e) {
////			e.printStackTrace();
////		}
//		
//		
////		Image image1 = null;
////		try {
////			image1 = Image.getInstance("images/digisign2copy.png");
////		} catch (BadElementException e2) {
////			// TODO Auto-generated catch block
////			e2.printStackTrace();
////		} catch (MalformedURLException e2) {
////			// TODO Auto-generated catch block
////			e2.printStackTrace();
////		} catch (IOException e2) {
////			// TODO Auto-generated catch block
////			e2.printStackTrace();
////		}
//////		String image1="";
////		image1.scalePercent(15f);
////		image1.scaleAbsoluteWidth(100f);
////		imageSignCell=new PdfPCell(image1);
////		imageSignCell.setBorder(0);
//////			 if (authOnLeft) {
//////				 imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//////			 } else {
////				 imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//////			 }
//////			 }
//////		 	catch(Exception e)
//////		 	{
//////		 	e.printStackTrDayace();
//////			 }
//////		 	
//		
		if(imageSignCell!=null)
		{
			if(!printCustomCopy){
				footerTab2.addCell(imageSignCell);
			}
		}
		else
		{
		Phrase blank=new Phrase(" ",font9);
		PdfPCell blankCell=new PdfPCell(blank);
		blankCell.setBorder(0);	
		footerTab2.addCell(blankCell);
		footerTab2.addCell(blankCell);//By jayshree
//		footerTab2.addCell(blankCell);
//		footerTab2.addCell(blankCell);
//		footerTab2.addCell(blankCell);
//		footerTab2.addCell(blankCell);
		
		}
			

		
		
		signParaCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		signParaCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		footerTab2.addCell(signParaCell);

		
		if(printCustomCopy){
			Phrase blank=new Phrase(" ",font9);
			PdfPCell blankCell=new PdfPCell(blank);
			blankCell.setBorder(0);	
			footerTab2.addCell(blankCell);
			footerTab2.addCell(blankCell);
			footerTab2.addCell(blankCell);
		}
		
	//

		/**
		 * Date : 22-03-2021 Added by : Priyanka
		 * Des : Consider branch as company flow for Pecopp.
		 */
		
		String companyName ="";
		if(branchDt!=null&&branchDt.getCorrespondenceName()!=null&&!branchDt.getCorrespondenceName().equals("")){
			companyName=branchDt.getCorrespondenceName();
		}else{
			companyName = comp.getBusinessUnitName().trim().toUpperCase();
		}
		Phrase pecoppFooterPhrase = new Phrase("For "
				+companyName, font9bold);
		PdfPCell pecoppFooterCell = new PdfPCell(pecoppFooterPhrase);
		// pecoppFooterCell.addElement(pecoppFooterPhrase);//Comment by jayshree Date 25/11/2017
		pecoppFooterCell.setBorder(0);
		// authorisedCell.setColspan(2);
		pecoppFooterCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		pecoppFooterCell.setVerticalAlignment(Element.ALIGN_TOP);
		footerTab2.addCell(pecoppFooterCell);

		PdfPTable footerTable = new PdfPTable(3);
		footerTable.setWidthPercentage(100f);
		footerTable.setSpacingBefore(10f);//By Jayshree Date25/11/2017

		try {
			footerTable.setWidths(new float[] { 30, 40, 35 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		if(printCustomCopy){
			
			Phrase footerBlankph = new Phrase(" ");
			PdfPCell footerBlank = new PdfPCell(footerBlankph);
			footerBlank.setBorder(0);
//			footerBlank.addElement(footerBlankph);
			footerTable.addCell(footerBlank);
			
		}
		else{
			
			PdfPCell footerBlankCel = new PdfPCell(footerTab1);
			footerBlankCel.setBorder(0);
//			footerBlankCel.addElement(footerTab1);
			footerTable.addCell(footerBlankCel);
			
		}
		
		
		
		Phrase footerBlankph = new Phrase(" ");
		PdfPCell footerBlank = new PdfPCell(footerBlankph);
		footerBlank.setBorder(0);
//		footerBlank.addElement(footerBlankph);
		footerTable.addCell(footerBlank);
		
		PdfPCell footercell = new PdfPCell(footerTab2);
		footercell.setBorder(0);
//		footercell.addElement(footerTab2);
		footerTable.addCell(footercell);
		
		try {
			document.add(footerTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void createHeaderTable() {
		System.out.print("helllo");

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.setSpacingAfter(10f);

		String invoiceTitle = "TAX INVOICE ";
		if(printCustomCopy){
			invoiceTitle = "INVOICE ";
		}
		Phrase headPh = new Phrase(invoiceTitle, font10bold);
		PdfPCell headCell = new PdfPCell(headPh);
		// headCell.addElement(headPh);
		headCell.setBorder(0);
		headCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		table.addCell(headCell);

		try {
			document.add(table);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createInvoiceHeaderTable() {

		PdfPTable billTab = new PdfPTable(3);
		billTab.setWidthPercentage(100f);
		billTab.setSpacingBefore(1f);

		try {
			billTab.setWidths(new float[] { 25, 5, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase billPhrase = new Phrase("Bill Number", font9bold);
		PdfPCell billCell = new PdfPCell(billPhrase);
//		billCell.addElement(billPhrase);//By Jayshree comment this line Date25/11/2017
		billCell.setBorder(0);
		billCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		billTab.addCell(billCell);

		Phrase colPhrase3 = new Phrase(":", font9bold);
		PdfPCell colCell3 = new PdfPCell(colPhrase3);
//		colCell3.addElement(colPhrase3);//By Jayshree comment this line Date25/11/2017
		colCell3.setBorder(0);
		colCell3.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		billTab.addCell(colCell3);

		// Phrase billvaluePhrase2 =new Phrase("",font8bold);
		Phrase billvaluePhrase2 = null;
		if (invoiceentity.getCount() != 0) {
			billvaluePhrase2 = new Phrase(invoiceentity.getCount() + "", font9);
		} else {
			billvaluePhrase2 = new Phrase(" ", font9);
		}
		PdfPCell billValueCell2 = new PdfPCell(billvaluePhrase2);
//		billValueCell2.addElement(billvaluePhrase2);//By Jayshree comment this line Date25/11/2017
		billValueCell2.setBorder(0);
		billValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		billValueCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		billTab.addCell(billValueCell2);

		Phrase datedPhrase = new Phrase("Dated", font9bold);
		PdfPCell datedCell = new PdfPCell(datedPhrase);
		datedCell.setBorder(0);
		datedCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		billTab.addCell(datedCell);

		Phrase colPhrase = new Phrase(":", font9bold);
		PdfPCell colCell = new PdfPCell(colPhrase);
//		colCell.addElement(colPhrase);//By Jayshree comment this line Date25/11/2017
		colCell.setBorder(0);
		colCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		billTab.addCell(colCell);

		// Phrase datedValuePhrase=new Phrase("27/07/2017",font8);
		Phrase datedValuePhrase = null;
		if (invoiceentity.getInvoiceDate() != null) {
			datedValuePhrase = new Phrase(fmt2.format(invoiceentity
					.getInvoiceDate()), font9);
		} else {
			datedValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell datedValueCell = new PdfPCell(datedValuePhrase);
		datedValueCell.setBorder(0);
		datedValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		billTab.addCell(datedValueCell);

		PdfPTable contractTab = new PdfPTable(3);
		contractTab.setWidthPercentage(100f);
		contractTab.setSpacingBefore(1f);

		try {
			contractTab.setWidths(new float[] { 25, 5, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase conphrase = new Phrase("Contract No", font9bold);
		PdfPCell conCell = new PdfPCell(conphrase);
//		conCell.addElement(conphrase);//By Jayshree comment this line Date25/11/2017
		conCell.setBorder(0);
		conCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		conCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		contractTab.addCell(conCell);

		Phrase colPhrase1 = new Phrase(":", font9bold);
		PdfPCell colCell1 = new PdfPCell(colPhrase1);
//		colCell1.addElement(colPhrase1);//By Jayshree comment this line Date25/11/2017
		colCell1.setBorder(0);
		colCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		contractTab.addCell(colCell1);

		// Phrase thPhrase =new Phrase("",font8bold);
		Phrase thPhrase = null;
		if (invoiceentity.getContractCount() != 0) {
			thPhrase = new Phrase(invoiceentity.getContractCount() + "", font9);
		} else {
			thPhrase = new Phrase(" ", font9);
		}
		PdfPCell thCell = new PdfPCell(thPhrase);
//		thCell.addElement(thPhrase);//By Jayshree comment this line Date25/11/2017
		thCell.setBorder(0);
		thCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		thCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		contractTab.addCell(thCell);

		Phrase datedPhrase2 = new Phrase("Dated", font9bold);
		PdfPCell datedCell2 = new PdfPCell(datedPhrase2);
		datedCell2.setBorder(0);
		datedCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		contractTab.addCell(datedCell2);

		Phrase colPhrase2 = new Phrase(":", font9bold);
		PdfPCell colCell2 = new PdfPCell(colPhrase2);
//		colCell2.addElement(colPhrase2);//By Jayshree comment this line Date25/11/2017
		colCell2.setBorder(0);
		colCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		contractTab.addCell(colCell2);

		// Phrase datedValuePhrase2=new Phrase("27/07/2017",font8);
		Phrase datedValuePhrase2 = null;
		if (con.getContractDate() != null) {
			datedValuePhrase2 = new Phrase(sdf.format(con.getContractDate()),
					font9);
		} else {
			datedValuePhrase2 = new Phrase(" ", font9);
		}
		PdfPCell datedValueCell2 = new PdfPCell(datedValuePhrase2);
		datedValueCell2.setBorder(0);
		datedValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
		datedValueCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		contractTab.addCell(datedValueCell2);

		PdfPTable outertab = new PdfPTable(2);
		outertab.setWidthPercentage(100f);
		outertab.setSpacingBefore(1f);

		try {
			outertab.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell outcell = new PdfPCell(billTab);
//		outcell.addElement(billTab);//By Jayshree comment this line Date25/11/2017
		outertab.addCell(outcell);
		;

		PdfPCell outcell2 = new PdfPCell(contractTab);
//		outcell2.addElement(contractTab);//By Jayshree comment this line Date25/11/2017
		outertab.addCell(outcell2);

		try {
			document.add(outertab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable headAdd = new PdfPTable(2);
		headAdd.setWidthPercentage(100f);

		try {
			headAdd.setWidths(new float[] { 50, 50 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase billadd = new Phrase("Billing Address", font9bold);
		PdfPCell billaddCell = new PdfPCell(billadd);
		billaddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		billaddCell.setVerticalAlignment(Element.ALIGN_CENTER);
		headAdd.addCell(billaddCell);

		Phrase serviceadd = new Phrase("Service Address", font9bold);
		PdfPCell serviceCell = new PdfPCell(serviceadd);
		serviceCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		serviceCell.setVerticalAlignment(Element.ALIGN_JUSTIFIED);
		headAdd.addCell(serviceCell);

		try {
			document.add(headAdd);
		} catch (DocumentException e1) {
			e1.printStackTrace();

		}

	}

	private void createCustomerTable() {
		/**
		 * Date 25/09/2017 By- Jayshree chavan Description-changes are made to
		 * add the customer GSTIN ,PAN,SACNO
		 */

		// /

		PdfPTable customerDetailTable2 = new PdfPTable(3);
		customerDetailTable2.setWidthPercentage(100f);
		// customerDetailTab1.setSpacingBefore(2f);//By Jayshree comment this

		try {
			customerDetailTable2.setWidths(new float[] { 25, 3, 75 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		//Date 16/11/2017
		// By Jayshree to add the ref no and ref value
		Phrase refno=new Phrase ("Ref No",font9);
		PdfPCell refnoCell=new PdfPCell(refno);
		refnoCell.setBorder(0);
		refnoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		refnoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable2.addCell(refnoCell);
		
		Phrase colPhrase6 = new Phrase(":", font9);
		PdfPCell colCell6 = new PdfPCell(colPhrase6);
//		colCell6.addElement(colPhrase6);//By Jayshree comment this line Date25/11/2017
		colCell6.setBorder(0);
		colCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable2.addCell(colCell6);
		
		/**
		 * Date 25/11/2017
		 * Dev. Jayshree
		 * Des:to set the ref no and ref date
		 * 
		 */
		
		PdfPTable refcomtab=new PdfPTable(4);
		refcomtab.setWidthPercentage(100);
		
		try {
			refcomtab.setWidths(new float[]{40,25,3,32});
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		Phrase refnoValue=null;
		if(con.getReferenceNo()!=null){
			refnoValue=new Phrase (con.getRefNo(),font9);
		}else{
			refnoValue=new Phrase (" ",font9);
		}
		
		
		PdfPCell refnoCellValue=new PdfPCell(refnoValue);
		refnoCellValue.setBorder(0);
		refnoCellValue.setHorizontalAlignment(Element.ALIGN_LEFT);
		refnoCellValue.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		refcomtab.addCell(refnoCellValue);
		
		Phrase refDate=new Phrase ("Ref Date",font9);
		PdfPCell refDateCell=new PdfPCell(refDate);
		refDateCell.setBorder(0);
		refDateCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		refDateCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		refcomtab.addCell(refDateCell);
//		refcomtab.addCell(colCell6);
		
		Phrase refDateVal=null;
		if(con.getRefDate()!=null){
		 refDateVal=new Phrase (fmt.format(con.getRefDate()),font9);
		}else{
			refDateVal=new Phrase (" ");
		}
		PdfPCell refDateValCell=new PdfPCell(refDateVal);
		refDateValCell.setBorder(0);
		refDateValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		refDateValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		refcomtab.addCell(refDateValCell);
		
		/**
		 * @author Anil @since 28-09-2021
		 * If ref num and ref date is not present then no need to print label as well
		 * Raised by swati for UPCL
		 */
		
		if(con!=null&&con.getReferenceNo()!=null&&!con.getReferenceNo().equals("")
				&&con.getRefDate()!=null){
			refcomtab.addCell(refnoCellValue);
			refcomtab.addCell(refDateCell);
			refcomtab.addCell(colCell6);
			refcomtab.addCell(refDateValCell);
			
			PdfPCell refComCell=new PdfPCell(refcomtab);
			refComCell.setBorder(0);
			
			customerDetailTable2.addCell(refnoCell);
			customerDetailTable2.addCell(colCell6);
			customerDetailTable2.addCell(refComCell);
		}else if(con!=null&&con.getReferenceNo()!=null&&!con.getReferenceNo().equals("")){
			refnoCellValue.setColspan(4);
			refcomtab.addCell(refnoCellValue);
			PdfPCell refComCell=new PdfPCell(refcomtab);
			refComCell.setBorder(0);
			customerDetailTable2.addCell(refnoCell);
			customerDetailTable2.addCell(colCell6);
			customerDetailTable2.addCell(refComCell);
			
		}else if(con!=null&&con.getRefDate()!=null){
			refcomtab.addCell(refDateCell);
			refcomtab.addCell(colCell6);
			refDateValCell.setColspan(2);
			refcomtab.addCell(refDateValCell);
			PdfPCell refComCell=new PdfPCell(refcomtab);
			refComCell.setBorder(0);
			refComCell.setColspan(3);
			customerDetailTable2.addCell(refComCell);
		}
		
		//End By Jayshree
		
		//End
		String gstin = "", panno = "";
		for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {

//			if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
//					.equalsIgnoreCase("GSTIN")) {
//				gstin = cust.getArticleTypeDetails().get(i)
//						.getArticleTypeValue();
//			}
			
			/**
			 * Date 25-06-2018 By Vijay
			 * below if condition added for GST number from Invoice, in invoice does not exist then else block for gst number print from customer
			 */
			
			if(invoiceentity.getGstinNumber()!=null && !invoiceentity.getGstinNumber().equals("")){
				gstin = invoiceentity.getGstinNumber();
			}else{
				if (cust.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")) {
					gstin = cust.getArticleTypeDetails().get(i).getArticleTypeValue();
				}
			}
			/**
			 * ends here
			 */

			if (cust.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("PAN NO")) {
				panno = cust.getArticleTypeDetails().get(i).getArticleTypeValue();
			}
		}
		Phrase gstinPhrase = null;
		gstinPhrase = new Phrase("GSTIN", font9);
		PdfPCell gstinCell = new PdfPCell(gstinPhrase);
//		gstinCell.addElement(gstinPhrase);//By Jayshree comment this line Date25/11/2017
		gstinCell.setBorder(0);
		gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable2.addCell(gstinCell);
		
//		customerDetailTable2.addCell(colCell6);
		Phrase gstinValuePhrase = null;
		gstinValuePhrase = new Phrase(gstin, font9);
		PdfPCell gstinValueCell = new PdfPCell(gstinValuePhrase);
//		gstinValueCell.addElement(gstinValuePhrase);//By Jayshree comment this line Date25/11/2017
		gstinValueCell.setBorder(0);
		gstinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		gstinValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable2.addCell(gstinValueCell);
		
		/**
		 * @author Anil @since 28-09-2021
		 * If gstin number not present then no need to print label as well
		 * Raised by swati for UPCL
		 */
		if(gstin!=null&&!gstin.equals("")){
			customerDetailTable2.addCell(gstinCell);
			customerDetailTable2.addCell(colCell6);
			customerDetailTable2.addCell(gstinValueCell);
		}

		Phrase panNoPhrase = null;
		panNoPhrase = new Phrase("PAN NO", font9);
		PdfPCell panNoCell = new PdfPCell(panNoPhrase);
//		panNoCell.addElement(panNoPhrase);//By Jayshree comment this line Date25/11/2017
		panNoCell.setBorder(0);
		panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		panNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTable2.addCell(panNoCell);
		//
		Phrase colPhrase101 = new Phrase(":", font9);
		PdfPCell colCell101 = new PdfPCell(colPhrase101);
//		colCell101.addElement(colPhrase101);//By Jayshree comment this line Date25/11/2017
		colCell101.setBorder(0);
		colCell101.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell101.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable2.addCell(colCell101);
		// //
		Phrase panNoValuePhrase = null;
		panNoValuePhrase = new Phrase(panno, font9);
		PdfPCell panNoValueCell = new PdfPCell(panNoValuePhrase);
//		panNoValueCell.addElement(panNoValuePhrase);//By Jayshree comment this line Date25/11/2017
		panNoValueCell.setBorder(0);
		panNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		panNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable2.addCell(panNoValueCell);
		
		
		/**
		 * @author Anil @since 28-09-2021
		 * If gstin number not present then no need to print label as well
		 * Raised by swati for UPCL
		 */
		if(panno!=null&&!panno.equals("")){
			customerDetailTable2.addCell(panNoCell);
			customerDetailTable2.addCell(colCell101);
			customerDetailTable2.addCell(panNoValueCell);
		}

		PdfPTable customerDetailTable3 = new PdfPTable(6);
		customerDetailTable3.setWidthPercentage(100f);
		// customerDetailTab1.setSpacingBefore(2f);

		try {
			/**
			 * Date 27/11/2017
			 * dev.jayshree
			 * Des.To set the column width changes are done 
			 */
			customerDetailTable3.setWidths(new float[] { 24, 3, 28, 20, 2, 23 });
			//end by jayshree
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		// *************************END************

		Phrase mobileNoPhrase = new Phrase("Mobile", font9);
		PdfPCell mobileNoCell = new PdfPCell(mobileNoPhrase);
//		mobileNoCell.addElement(mobileNoPhrase);//By Jayshree comment this line Date25/11/2017
		mobileNoCell.setBorder(0);
		mobileNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		mobileNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTable3.addCell(mobileNoCell);

		Phrase colPhrase15 = new Phrase(":", font9);
		PdfPCell colCell15 = new PdfPCell(colPhrase15);
//		colCell15.addElement(colPhrase15);//By Jayshree comment this line Date25/11/2017
		colCell15.setBorder(0);
		colCell15.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell15.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTable3.addCell(colCell15);

		// Phrase mobileNoValuePhrase=new Phrase("9820835447",font7);
		Phrase mobileNoValuePhrase = null;
		
		if(customerBranch!=null){
			if (customerBranch.getCellNumber1() != 0) {
				mobileNoValuePhrase = new Phrase(customerBranch.getCellNumber1() + "",
						font9);
			} else {
				mobileNoValuePhrase = new Phrase(" ", font9);
			}
		}else{
			if (invoiceentity.getCellNumber() != 0) {
				mobileNoValuePhrase = new Phrase(
						invoiceentity.getCellNumber() + "", font9);
			} else {
				mobileNoValuePhrase = new Phrase(" ", font9);
			}
		}
		
		
		
		
		PdfPCell mobileNoValueCell = new PdfPCell(mobileNoValuePhrase);
//		mobileNoValueCell.addElement(mobileNoValuePhrase);//By Jayshree comment this line Date25/11/2017
		mobileNoValueCell.setBorder(0);
		mobileNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		mobileNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTable3.addCell(mobileNoValueCell);
		// //

		Phrase telNoPhrase1 = new Phrase("Tel. No", font9);

		PdfPCell telNoCell1 = new PdfPCell(telNoPhrase1);
//		telNoCell1.addElement(telNoPhrase1);//By Jayshree comment this line Date25/11/2017
		telNoCell1.setBorder(0);
		telNoCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		telNoCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable3.addCell(telNoCell1);

		Phrase colPhrase12 = new Phrase(":", font9);
		PdfPCell colCell12 = new PdfPCell(colPhrase12);
//		colCell12.addElement(colPhrase12);//By Jayshree comment this line Date25/11/2017
		colCell12.setBorder(0);
		colCell12.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable3.addCell(colCell12);

		// Phrase telNovaluePhrase1=new Phrase("Tel.No.");
		Phrase telNovaluePhrase1 = null;
		if (cust.getLandline() != 0) {
			telNovaluePhrase1 = new Phrase(cust.getLandline() + "", font9);
		} else {
			telNovaluePhrase1 = new Phrase(" ", font9);
		}
		PdfPCell telNovalueCell1 = new PdfPCell(telNovaluePhrase1);
//		telNovalueCell1.addElement(telNovaluePhrase1);//By Jayshree comment this line Date25/11/2017
		telNovalueCell1.setBorder(0);
		telNovalueCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		telNovalueCell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//		customerDetailTable3.addCell(telNovalueCell1);
		
		
		/**
		 * @author Anil @since 28-09-2021
		 * If Tel no. not present then no need to print label as well
		 * Raised by swati for UPCL
		 */
		if(cust!=null&&cust.getLandline()!=null&&cust.getLandline()!=0){
			customerDetailTable3.addCell(telNoCell1);
			customerDetailTable3.addCell(colCell12);
			customerDetailTable3.addCell(telNovalueCell1);
		}else{
			mobileNoValueCell.setColspan(4);
		}

		// PdfPTable customerDetailTable3 = new PdfPTable(6);
		// customerDetailTable3.setWidthPercentage(100f);
		// // customerDetailTab1.setSpacingBefore(2f);
		//
		// try {
		// customerDetailTable3.setWidths(new float[] { 8, 2, 15, 8, 2, 15 });
		// } catch (DocumentException e1) {
		// e1.printStackTrace();
		// }
		// Phrase mobileNoPh = new Phrase("Mobile", font9);
		// PdfPCell mobileNoC = new PdfPCell();
		// mobileNoC.addElement(mobileNoPh);
		// mobileNoC.setBorder(0);
		// mobileNoC.setHorizontalAlignment(Element.ALIGN_LEFT);
		// mobileNoC.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// customerDetailTable3.addCell(mobileNoC);
		//
		// Phrase colPhrase16 = new Phrase(":", font9);
		// PdfPCell colCell16 = new PdfPCell();
		// colCell16.addElement(colPhrase16);
		// colCell16.setBorder(0);
		// colCell16.setHorizontalAlignment(Element.ALIGN_LEFT);
		// colCell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// customerDetailTable3.addCell(colCell16);
		//
		// // Phrase mobileNoValuePhrase=new Phrase("9820835447",font7);
		// Phrase mobileNoValuePh = null;
		// if (con.getCustomerCellNumber() != 0) {
		// mobileNoValuePh = new Phrase(con.getCustomerCellNumber() + "",
		// font9);
		// } else {
		// mobileNoValuePh = new Phrase(" ", font9);
		// }
		// PdfPCell mobileNoValueC = new PdfPCell();
		// mobileNoValueC.addElement(mobileNoValuePh);
		// mobileNoValueC.setBorder(0);
		// mobileNoValueC.setHorizontalAlignment(Element.ALIGN_LEFT);
		// mobileNoValueC.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// customerDetailTable3.addCell(mobileNoValueC);
		// // //
		//
		// Phrase telNoPh = new Phrase("Tel. No", font9);
		//
		// PdfPCell telNoC = new PdfPCell();
		// telNoC.addElement(telNoPh);
		// telNoC.setBorder(0);
		// telNoC.setHorizontalAlignment(Element.ALIGN_LEFT);
		// telNoC.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// customerDetailTable3.addCell(telNoC);
		//
		// Phrase colPhrase13 = new Phrase(":", font9);
		// PdfPCell colCell13 = new PdfPCell();
		// colCell13.addElement(colPhrase13);
		// colCell13.setBorder(0);
		// colCell13.setHorizontalAlignment(Element.ALIGN_LEFT);
		// colCell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// customerDetailTable3.addCell(colCell13);
		//
		// // Phrase telNovaluePhrase1=new Phrase("Tel.No.");
		// Phrase telNovaluePh = null;
		// if (cust.getLandline() != 0) {
		// telNovaluePh = new Phrase(cust.getLandline() + "", font9);
		// } else {
		// telNovaluePh = new Phrase(" ", font9);
		// }
		// PdfPCell telNovalueC = new PdfPCell();
		// telNovalueC.addElement(telNovaluePh);
		// telNovalueC.setBorder(0);
		// telNovalueC.setHorizontalAlignment(Element.ALIGN_LEFT);
		// telNovalueC.setVerticalAlignment(Element.ALIGN_MIDDLE);
		// customerDetailTable3.addCell(telNovalueC);

		PdfPTable borderTable = new PdfPTable(2);
		borderTable.setWidthPercentage(100f);
		// borderTable.setSpacingBefore(5f);

		/**
		 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for
		 * to add the service address and billing address
		 */
		PdfPCell borderCell2 = new PdfPCell();
		borderCell2.addElement(getCustomerDetails("BillingAddress"));
		borderCell2.setBorderWidthBottom(0);
		borderTable.addCell(borderCell2);

		PdfPCell borderCell3 = new PdfPCell();
		borderCell3.addElement(getCustomerDetails("ServiceAddress"));
		borderCell3.setBorderWidthBottom(0);
		borderTable.addCell(borderCell3);
		// ****************end**************
		PdfPCell borderCell4 = new PdfPCell(customerDetailTable2);
//		borderCell4.addElement(customerDetailTable2);//By Jayshree comment this line Date25/11/2017
		borderCell4.setBorderWidthBottom(0);
		borderCell4.setBorderWidthTop(0);
		borderTable.addCell(borderCell4);

		PdfPCell borderCell5 = new PdfPCell(customerDetailTable3);
//		borderCell5.addElement(customerDetailTable3);//By Jayshree comment this line Date25/11/2017
		borderCell5.setBorderWidthBottom(0);
		borderCell5.setBorderWidthTop(0);
		borderTable.addCell(borderCell5);

		PdfPCell borderCell7 = new PdfPCell(customerDetailTable3);
//		borderCell7.addElement(customerDetailTable3);//By Jayshree comment this line Date25/11/2017
		borderCell7.setBorderWidthTop(0);
		borderTable.addCell(borderCell7);

		Phrase border = new Phrase(" ");
		PdfPCell borderCell6 = new PdfPCell(border);
		borderCell6.setBorderWidthTop(0);
		borderTable.addCell(borderCell6);

		try {
			document.add(borderTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for to
	 * add the service address and billing address
	 */
	private Element getCustomerDetails(String addressToTake) {
		PdfPTable customerDetailTab1 = new PdfPTable(3);
		customerDetailTab1.setWidthPercentage(100f);
		// customerDetailTab1.setSpacingBefore(2f);

		try {
			customerDetailTab1.setWidths(new float[] { 25, 3, 75 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		// *****************
		Phrase customerNamePhrase = new Phrase("Customer ", font9);
		PdfPCell customerNameCell = new PdfPCell(customerNamePhrase);
//		customerNameCell.addElement(customerNamePhrase);//By Jayshree comment this line Date25/11/2017
		customerNameCell.setBorder(0);
		customerNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNameCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(customerNameCell);

		Phrase colPhrase5 = new Phrase(":", font9bold);
		PdfPCell colCell5 = new PdfPCell(colPhrase5);
//		colCell5.addElement(colPhrase5);//By Jayshree comment this line Date25/11/2017
		colCell5.setBorder(0);
		colCell5.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell5.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(colCell5);

		Phrase customerNameValuePhrase = null;
		String custName="";
		if (cust.getCustPrintableName() != null
				&& !cust.getCustPrintableName().equals("")) {
			custName = cust.getCustPrintableName().trim();
		} else {

			if (cust.isCompany() == true && cust.getCompanyName() != null) {
				custName = "M/S " + cust.getCompanyName().trim();
			} else {
				custName = cust.getFullname().trim();
			}
		}
		
		customerNameValuePhrase = new Phrase(custName, font9);
		PdfPCell customerNameValueCell = new PdfPCell(customerNameValuePhrase);
//		customerNameValueCell.addElement(customerNameValuePhrase);//By Jayshree comment this line Date25/11/2017
		customerNameValueCell.setBorder(0);
		customerNameValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		customerNameValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(customerNameValueCell);
		// /

		Phrase addressPhrase = new Phrase("Address", font9);
		PdfPCell addressCell = new PdfPCell(addressPhrase);
//		addressCell.addElement(addressPhrase);//By Jayshree comment this line Date25/11/2017
		addressCell.setBorder(0);
		addressCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// addressCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(addressCell);

		Phrase colPhrase7 = new Phrase(":", font9bold);
		PdfPCell colCell7 = new PdfPCell(colPhrase7);
//		colCell7.addElement(colPhrase7);//By Jayshree comment this line Date25/11/2017
		colCell7.setBorder(0);
		colCell7.setHorizontalAlignment(Element.ALIGN_LEFT);
		// colCell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(colCell7);

		/**
		 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for
		 * to add the service address and billing address
		 */
		// Phrase addressValuePhrase=new Phrase("1201,12th",font8);
		Phrase addressValuePhrase = null;
		Address serviceAddress = null;
		String billingAddress="";
		if (addressToTake.trim().equals("BillingAddress")) {
			logger.log(Level.SEVERE, "inside billing address iff");
		/**Date 19-5-2020 by Amol 
		 *   print billing address from customer branch raised by vaishnavi Mam
		 */
			if(invoiceentity.getCustomerBranch() != null
					&& !invoiceentity.getCustomerBranch().equals("")){
				logger.log(Level.SEVERE, "inside billing address customer branch condition");
				CustomerBranchDetails branch = ofy()
						.load()
						.type(CustomerBranchDetails.class)
						.filter("companyId", comp.getCompanyId())
						.filter("buisnessUnitName",
								invoiceentity.getCustomerBranch())
						.filter("cinfo.count",
								invoiceentity.getPersonInfo().getCount()).first()
						.now();
				
				if (branch != null && branch.getBillingAddress()!=null) {
					serviceAddress = branch.getBillingAddress();
					logger.log(Level.SEVERE, "customer branch billing address value "+serviceAddress);
					addressValuePhrase =  new Phrase(branch.getBillingAddress().getCompleteAddress(), font9);
					
				}
				
			}else{
				/**
				 * @author Vijay Chougule Date 05-09-2020
				 * Des :- if customer billing address exist in contract then it will print from contract
				 * or else it will print from Customer billing address old code added in else part
				 */
				if(con.getNewcustomerAddress()!=null){
					serviceAddress = con.getNewcustomerAddress();
					logger.log(Level.SEVERE, "inside else billing address"+serviceAddress);
					if (con.getNewcustomerAddress().getCompleteAddress() != null) {
						addressValuePhrase = new Phrase(con.getNewcustomerAddress()
								.getCompleteAddress(), font9);
					} else {
						addressValuePhrase = new Phrase(" ", font9);
					}
				}
				else{
					serviceAddress = cust.getAdress();
					logger.log(Level.SEVERE, "inside else billing address"+serviceAddress);
					if (cust.getAdress().getCompleteAddress() != null) {
						addressValuePhrase = new Phrase(cust.getAdress()
								.getCompleteAddress(), font9);
					} else {
						addressValuePhrase = new Phrase(" ", font9);
					}
				}
				
			}
			
		} else if (addressToTake.trim().equals("ServiceAddress")) {

			// if (cust.getSecondaryAdress().getCompleteAddress() != null) {
			String customerServiceAddress = " ";

			if (invoiceentity.getCustomerBranch() != null
					&& !invoiceentity.getCustomerBranch().equals("")) {
				/**
				 * @author Anil @since 18-08-2021
				 * Added customer id filer for loading customer branch
				 * issue raised by Ashwini for Pecopp
				 */
				CustomerBranchDetails branch = ofy().load()
						.type(CustomerBranchDetails.class)
						.filter("companyId", comp.getCompanyId())
						.filter("buisnessUnitName",invoiceentity.getCustomerBranch())
						.filter("cinfo.count",invoiceentity.getPersonInfo().getCount())
						.first().now();
				if (branch != null) {
					serviceAddress = branch.getAddress();

					customerServiceAddress = branch.getAddress()
							.getCompleteAddress();
				}
				addressValuePhrase = new Phrase(customerServiceAddress, font9);
			} else {

				if (con.getCustomerServiceAddress() != null) {
					serviceAddress = con.getCustomerServiceAddress();
					customerServiceAddress = con.getCustomerServiceAddress()
							.getCompleteAddress();
					addressValuePhrase = new Phrase(customerServiceAddress,
							font9);
				} else {
					serviceAddress = cust.getSecondaryAdress();
					customerServiceAddress = cust.getSecondaryAdress()
							.getCompleteAddress();
					addressValuePhrase = new Phrase(customerServiceAddress,
							font9);
				}

				// } else {
				// addressValuePhrase = new Phrase(" ", font9);
			}

		} else {
			addressValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell addressValueCell = new PdfPCell(addressValuePhrase);
//		addressValueCell.addElement(addressValuePhrase);//By Jayshree comment this line Date25/11/2017
		addressValueCell.setBorder(0);
		addressValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// addressValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(addressValueCell);
		// //
		// *****************************end*************
		Phrase localityPhrase = new Phrase("Locality", font9);
		PdfPCell localityCell = new PdfPCell(localityPhrase);
//		localityCell.addElement(localityPhrase);//By Jayshree comment this line Date25/11/2017
		localityCell.setBorder(0);
		localityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		localityCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(localityCell);

		Phrase colPhrase9 = new Phrase(":", font9);
		PdfPCell colCell9 = new PdfPCell(colPhrase9);
//		colCell9.addElement(colPhrase9);//By Jayshree comment this line Date25/11/2017
		colCell9.setBorder(0);
		colCell9.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell9.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(colCell9);

		// Phrase localityValuePhrase=new Phrase("Thane(West)",font8);
		Phrase localityValuePhrase = null;
		if (serviceAddress!=null && serviceAddress.getLocality() != null) {
			localityValuePhrase = new Phrase(serviceAddress.getLocality(),
					font9);
		} else {
			localityValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell localityValueCell = new PdfPCell(localityValuePhrase);
//		localityValueCell.addElement(localityValuePhrase);//By Jayshree comment this line Date25/11/2017
		localityValueCell.setBorder(0);
		localityValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		localityValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(localityValueCell);

		// //

		Phrase cityPhrase = new Phrase("City", font9);
		PdfPCell cityCell = new PdfPCell(cityPhrase);
//		cityCell.addElement(cityPhrase);//By Jayshree comment this line Date25/11/2017
		cityCell.setBorder(0);
		cityCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cityCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(cityCell);

		Phrase colPhrase10 = new Phrase(":", font9);
		PdfPCell colCell10 = new PdfPCell(colPhrase10);
//		colCell10.addElement(colPhrase10);//By Jayshree comment this line Date25/11/2017
		colCell10.setBorder(0);
		colCell10.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell10.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(colCell10);

		// Phrase cityValuePhrase=new Phrase("Mumbai",font8);
		Phrase cityValuePhrase = null;
		if (serviceAddress!=null && serviceAddress.getCity() != null) {
			cityValuePhrase = new Phrase(serviceAddress.getCity(), font9);
		} else {
			cityValuePhrase = new Phrase(" ", font9);
		}
		PdfPCell cityValueCell = new PdfPCell(cityValuePhrase);
//		cityValueCell.addElement(cityValuePhrase);//By Jayshree comment this line Date25/11/2017
		cityValueCell.setBorder(0);
		cityValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cityValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(cityValueCell);

		// //

		Phrase emailIdPhrase = new Phrase("Email ID", font9);

		PdfPCell emailIdCell = new PdfPCell(emailIdPhrase);
//		emailIdCell.addElement(emailIdPhrase);//By Jayshree comment this line Date25/11/2017
		emailIdCell.setBorder(0);
		emailIdCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailIdCell.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(emailIdCell);

		Phrase colPhrase14 = new Phrase(":", font9);
		PdfPCell colCell14 = new PdfPCell(colPhrase14);
//		colCell14.addElement(colPhrase14);//By Jayshree comment this line Date25/11/2017
		colCell14.setBorder(0);
		colCell14.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell14.setVerticalAlignment(Element.ALIGN_CENTER);
		customerDetailTab1.addCell(colCell14);

		// Phrase emailIdValuePhrase=new Phrase("",font8);
		Phrase emailIdValuePhrase = null;
		if(customerBranch!=null){
			if (customerBranch.getEmail() != null) {
				emailIdValuePhrase = new Phrase(customerBranch.getEmail(), font9);
			} else {
				emailIdValuePhrase = new Phrase(" ", font9);
			}	
		}else{
			if (cust.getEmail() != null) {
				emailIdValuePhrase = new Phrase(cust.getEmail(), font9);
			} else {
				emailIdValuePhrase = new Phrase(" ", font9);
			}	
		}
		PdfPCell emailIdValueCell = new PdfPCell(emailIdValuePhrase);
//		emailIdValueCell.addElement(emailIdValuePhrase);//By Jayshree comment this line Date25/11/2017
		emailIdValueCell.setBorder(0);
		emailIdValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		emailIdValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		customerDetailTab1.addCell(emailIdValueCell);

		// //

		Phrase pinNoPhrase = new Phrase("Pin No", font9);
		PdfPCell pinNoCell = new PdfPCell(pinNoPhrase);
//		pinNoCell.addElement(pinNoPhrase);//By Jayshree comment this line Date25/11/2017
		pinNoCell.setBorder(0);
		pinNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pinNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		customerDetailTab1.addCell(pinNoCell);

		Phrase colPhrase11 = new Phrase(":", font9);
		PdfPCell colCell11 = new PdfPCell(colPhrase11);
//		colCell11.addElement(colPhrase11);//By Jayshree comment this line Date25/11/2017
		colCell11.setBorder(0);
		colCell11.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell11.setVerticalAlignment(Element.ALIGN_CENTER);
//		customerDetailTab1.addCell(colCell11);

		// Phrase pinNoValuePhrase=new Phrase("400604",font8);
		Phrase pinNoValuePhrase = null;
		/**
		 * Date : 04-11-2017 BY ANIL
		 * Pin code issue ,earlier jayshree was fetching pin from customer's billing address
		 */
		if (serviceAddress!=null && serviceAddress.getPin()!= 0) {
			pinNoValuePhrase = new Phrase(serviceAddress.getPin() + "", font9);
		} else {
			pinNoValuePhrase = new Phrase(" ", font9);
		}
		/**
		 * End
		 */
		
		PdfPCell pinNoValueCell = new PdfPCell(pinNoValuePhrase);
//		pinNoValueCell.addElement(pinNoValuePhrase);//By Jayshree comment this line Date25/11/2017
		pinNoValueCell.setBorder(0);
		pinNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		pinNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		customerDetailTab1.addCell(pinNoValueCell);
		
		if (serviceAddress!=null && serviceAddress.getPin()!= 0) {
			customerDetailTab1.addCell(pinNoCell);
			customerDetailTab1.addCell(colCell11);
			customerDetailTab1.addCell(pinNoValueCell);
		}

		// String gstinValueStr = "", panNoStr = "";
		// ServerAppUtility server=new ServerAppUtility();
		// gstinValueStr=server.getGSTINOfCustomer(cust,
		// invoiceentity.getCustomerBranch());
		//
		// for (int i = 0; i < cust.getArticleTypeDetails().size(); i++) {
		// // if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
		// // .equalsIgnoreCase("GSTIN")) {
		// // gstinValueStr = cust.getArticleTypeDetails().get(i)
		// // .getArticleTypeValue();
		// // }
		//
		// if (cust.getArticleTypeDetails().get(i).getArticleTypeName()
		// .equalsIgnoreCase("PANNO")) {
		// panNoStr = cust.getArticleTypeDetails().get(i)
		// .getArticleTypeValue();
		// }
		// }
		// Phrase gstinPhrase=new Phrase("GSTIN",font8);
		/*
		 * Phrase gstinPhrase = null; gstinPhrase = new Phrase("GSTIN", font9);
		 * 
		 * PdfPCell gstinCell = new PdfPCell();
		 * gstinCell.addElement(gstinPhrase); gstinCell.setBorder(0);
		 * gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * gstinCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * if(addressToTake.trim().equals("BillingAddress")) //
		 * customerDetailTab1.addCell(gstinCell);
		 * 
		 * Phrase colPhrase6 = new Phrase(":", font9); PdfPCell colCell6 = new
		 * PdfPCell(); colCell6.addElement(colPhrase6); colCell6.setBorder(0);
		 * colCell6.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * colCell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * if(addressToTake.trim().equals("BillingAddress")) //
		 * customerDetailTab1.addCell(colCell6);
		 * 
		 * // Phrase gstinValuePhrase=new Phrase( gstinNo,font8); Phrase
		 * gstinValuePhrase = null; gstinValuePhrase = new Phrase(gstinValueStr,
		 * font9); PdfPCell gstinValueCell = new PdfPCell();
		 * gstinValueCell.addElement(gstinValuePhrase);
		 * gstinValueCell.setBorder(0);
		 * gstinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * gstinValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * if(addressToTake.trim().equals("BillingAddress")) //
		 * customerDetailTab1.addCell(gstinValueCell);
		 * 
		 * // /
		 * 
		 * // Phrase panNoPhrase=new Phrase("PAN NO",font8); Phrase panNoPhrase
		 * = null; panNoPhrase = new Phrase("PAN NO", font9); PdfPCell panNoCell
		 * = new PdfPCell(); panNoCell.addElement(panNoPhrase);
		 * panNoCell.setBorder(0);
		 * panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * panNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * if(addressToTake.trim().equals("BillingAddress")) //
		 * customerDetailTab1.addCell(panNoCell);
		 * 
		 * Phrase colPhrase101 = new Phrase(":", font9); PdfPCell colCell101 =
		 * new PdfPCell(); colCell101.addElement(colPhrase101);
		 * colCell101.setBorder(0);
		 * colCell101.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * colCell101.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * if(addressToTake.trim().equals("BillingAddress")) //
		 * customerDetailTab1.addCell(colCell101);
		 * 
		 * Phrase panNoValuePhrase = null; panNoValuePhrase = new
		 * Phrase(panNoStr, font9); PdfPCell panNoValueCell = new PdfPCell();
		 * panNoValueCell.addElement(panNoValuePhrase);
		 * panNoValueCell.setBorder(0);
		 * panNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * panNoValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * if(addressToTake.trim().equals("BillingAddress")) //
		 * customerDetailTab1.addCell(panNoValueCell);
		 */
		return customerDetailTab1;
	}

	private void createPeriodTable() {
		// TODO Auto-generated method stub

		PdfPTable periodTab = new PdfPTable(6);
		periodTab.setWidthPercentage(100f);
		// customerDetailTab1.setSpacingBefore(2f);

		try {
			periodTab.setWidths(new float[] { 25, 5, 20, 25, 5, 20 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		PdfPCell periodblank = new PdfPCell();
		periodblank.setBorder(0);
		periodblank.setColspan(6);
		periodTab.addCell(periodblank);

		Phrase periodOfContractFromPhrase = new Phrase(
				"Period of Contract (From)", font9);
		PdfPCell periodOfContractFromCell = new PdfPCell(periodOfContractFromPhrase);
//		periodOfContractFromCell.addElement(periodOfContractFromPhrase);//By Jayshree comment this line Date25/11/2017
		periodOfContractFromCell.setBorder(0);
		periodOfContractFromCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		periodOfContractFromCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(periodOfContractFromCell);

		Phrase colPhrase13 = new Phrase(":", font9);
		PdfPCell colCell13 = new PdfPCell(colPhrase13);
//		colCell13.addElement(colPhrase13);//By Jayshree comment this line Date25/11/2017
		colCell13.setBorder(0);
		colCell13.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(colCell13);

		// Phrase periodOfContractFromvaluePhrase=new
		// Phrase("27/07/2017",font8);
		Phrase periodOfContractFromvaluePhrase = null;
		if (invoiceentity.getContractStartDate() != null) {
			periodOfContractFromvaluePhrase = new Phrase(
					sdf.format(invoiceentity.getContractStartDate()), font9);
		} else {
			periodOfContractFromvaluePhrase = new Phrase(" ", font9);
		}
		PdfPCell periodOfContractFromValueCell = new PdfPCell(periodOfContractFromvaluePhrase);
//		periodOfContractFromValueCell
//				.addElement(periodOfContractFromvaluePhrase);//By Jayshree comment this line Date25/11/2017
		periodOfContractFromValueCell.setBorder(0);
		periodOfContractFromValueCell
				.setHorizontalAlignment(Element.ALIGN_LEFT);
		periodOfContractFromValueCell
				.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(periodOfContractFromValueCell);

		Phrase periodOfContractToPhrase = new Phrase("Period of Contract (To)",
				font9);
		PdfPCell periodOfContractToCell = new PdfPCell(periodOfContractToPhrase);
//		periodOfContractToCell.addElement(periodOfContractToPhrase);//By Jayshree comment this line Date25/11/2017
		periodOfContractToCell.setBorder(0);
		periodOfContractToCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		periodOfContractToCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(periodOfContractToCell);

		Phrase colPhrase14 = new Phrase(":", font9);
		PdfPCell colCell14 = new PdfPCell(colPhrase14);
//		colCell14.addElement(colPhrase14);
		colCell14.setBorder(0);
		colCell14.setHorizontalAlignment(Element.ALIGN_LEFT);
		colCell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(colCell14);

		// Phrase periodOfContractTovaluePhrase=new Phrase("26/07/2018",font8);
		Phrase periodOfContractTovaluePhrase = null;
		if (invoiceentity.getContractEndDate() != null) {
			periodOfContractTovaluePhrase = new Phrase(sdf.format(invoiceentity
					.getContractEndDate()), font9);
		} else {
			periodOfContractTovaluePhrase = new Phrase(" ", font9);
		}
		PdfPCell periodOfContractToValueCell = new PdfPCell(periodOfContractTovaluePhrase);
//		periodOfContractToValueCell.addElement(periodOfContractTovaluePhrase);//By Jayshree comment this line Date25/11/2017
		periodOfContractToValueCell.setBorder(0);
		periodOfContractToValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		periodOfContractToValueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(periodOfContractToValueCell);

		sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		Phrase billForThePeriodPhrase = null;
		if (invoiceentity.getBillingPeroidFromDate() != null
				&& invoiceentity.getBillingPeroidToDate() != null) {
			billForThePeriodPhrase = new Phrase("Bill For the period  "
					+ " (Start "
					+ sdf.format(invoiceentity.getBillingPeroidFromDate())
					+ ") to " + "( End "
					+ sdf.format(invoiceentity.getBillingPeroidToDate()) + ")",
					font9);
		} else {
			billForThePeriodPhrase = new Phrase(" ", font9);
		}
		// Phrase billForThePeriodPhrase=new Phrase("Bill For the period  "+
		// " (Start" +sdf.format(
		// invoiceentity.getBillingPeroidFromDate())+") to "+"( End"
		// +sdf.format(invoiceentity.getBillingPeroidToDate())+ ")",font8);
		PdfPCell billForThePeriodCell = new PdfPCell(billForThePeriodPhrase);
		// billForThePeriodCell.addElement(billForThePeriodPhrase);//By Jayshree comment this line Date25/11/2017
		billForThePeriodCell.setBorder(0);
		billForThePeriodCell.setColspan(6);
		billForThePeriodCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		billForThePeriodCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		periodTab.addCell(billForThePeriodCell);

		PdfPTable borderTab = new PdfPTable(1);
		borderTab.setWidthPercentage(100f);
		// borderTable.setSpacingBefore(5f);

		PdfPCell border = new PdfPCell();
		border.addElement(periodTab);
		borderTab.addCell(border);

		try {
			document.add(borderTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void createProductHeaderTable() {

		PdfPTable productHeadTab = new PdfPTable(4);
		productHeadTab.setWidthPercentage(100f);
		productHeadTab.setSpacingBefore(10f);//By Jayshree Date 25/11/2017

		try {
			productHeadTab.setWidths(new float[] { 30, 30, 25, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase TypeOfTreatmentPhrase = new Phrase(" Type of Treatment",
				font9bold);
		PdfPCell TypeOfTreatmentCell = new PdfPCell(TypeOfTreatmentPhrase);
		// TypeOfTreatmentCell.addElement(TypeOfTreatmentPhrase);//By Jayshree comment this line Date25/11/2017
		TypeOfTreatmentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// TypeOfTreatmentCell.setVerticalAlignment(Element.ALIGN_TOP);
		TypeOfTreatmentCell.setVerticalAlignment(Element.ALIGN_CENTER);
		productHeadTab.addCell(TypeOfTreatmentCell);

		Phrase frequencyPhrase = new Phrase("  Frequency", font9bold);
		PdfPCell frequencyCell = new PdfPCell(frequencyPhrase);
		// frequencyCell.addElement(frequencyPhrase);//By Jayshree comment this line Date25/11/2017
		frequencyCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
		frequencyCell.setVerticalAlignment(Element.ALIGN_CENTER);
		productHeadTab.addCell(frequencyCell);

		/**
		 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for
		 * to add the specification in the table
		 */

		Phrase specificationPhrase = new Phrase("  Specification", font9bold);
		PdfPCell specificationCell = new PdfPCell(specificationPhrase);
		// frequencyCell.addElement(frequencyPhrase);//By Jayshree comment this line Date25/11/2017
		specificationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
		specificationCell.setVerticalAlignment(Element.ALIGN_CENTER);
		productHeadTab.addCell(specificationCell);
		// *********************End********************
		Phrase amountPhrase = new Phrase(" Amount ", font9bold); 
		PdfPCell amountCell = new PdfPCell(amountPhrase);
		// amountCell.addElement(amountPhrase);//By Jayshree comment this line Date25/11/2017
		amountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		amountCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// amountCell.setVerticalAlignment(Element.ALIGN_TOP);
		productHeadTab.addCell(amountCell);

		try {
			document.add(productHeadTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for to
	 * print the remaining product in the next page ie more than 10 product in
	 * the next page
	 */
	private void createProductDetailTable() {
		// int noOfLine=10;

		if (invoiceentity.getDiscountAmt() != 0) {
			noOfLine = noOfLine - 2;
			System.out.println("number line" + noOfLine);

		}
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			if (noOfLine == 0) {
				productCount = i;

				PdfPTable conProductDetail1 = new PdfPTable(1);
				conProductDetail1.setWidthPercentage(100f);
				Phrase indicate = new Phrase(
						"Please Refere Annexure For more Detail ", font9);
				PdfPCell indicateCell = new PdfPCell(indicate);
				indicateCell.setVerticalAlignment(Element.ALIGN_LEFT);
				conProductDetail1.addCell(indicateCell);

				try {
					document.add(conProductDetail1);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				break;
			}
			PdfPTable conProductDetail1 = new PdfPTable(1);
			conProductDetail1.setWidthPercentage(100f);
			noOfLine = noOfLine - 1;
//			System.out.println("con.getItems().get(i).getProductName().trim().length()"+ con.getItems().get(i).getProductName().trim().length());

			if (invoiceentity.getSalesOrderProducts().get(i).getProdName()
					.trim().length() > 50) {
				noOfLine = noOfLine - 1;
			}

			// ******************end***********************
			/**
			 * Date 25/09/2017 By- Jayshree chavan Description-changes are made
			 * for to split the productname if the (-)is present in the product
			 * name
			 */
			String productName = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();
			if (productName.contains("-")) {
				String[] product = productName.split(("-"));
				product1 = product[0];
				product2 = product[1];
			} else {
				product1 = invoiceentity.getSalesOrderProducts().get(i)
						.getProdName();
				product2 = "";
			}
			PdfPTable productDetailTab = new PdfPTable(4);
			productDetailTab.setWidthPercentage(100f);

			try {
				productDetailTab.setWidths(new float[] { 30, 30, 25, 15 });
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

			Phrase treatmentValPhrase = new Phrase(product1, font9bold);
			PdfPCell treatmentValCell = new PdfPCell(treatmentValPhrase);
			// generalDisinfestationCell.addElement(generalDisinfestationPhrase);//By Jayshree comment this line Date25/11/2017
			treatmentValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treatmentValCell.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationCell.setVerticalAlignment(Element.ALIGN_TOP);
			productDetailTab.addCell(treatmentValCell);

			Phrase serviceNoPhrase;
			if (!product2.equals("")) {
				serviceNoPhrase = new Phrase(product2
						+ " "
						+ "("
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getOrderServices() + "  Services )", font9);
			} else {
				serviceNoPhrase = new Phrase("("
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getOrderServices() + "  Services )", font9);
			}
			PdfPCell serviceNoCell = new PdfPCell(serviceNoPhrase);
			// generalDisinfestationFrequencyCell.addElement(generalDisinfestationFrequencyPhrase);//By Jayshree comment this line Date25/11/2017
			serviceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			serviceNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationFrequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
			productDetailTab.addCell(serviceNoCell);

			// *************************End******************

			// ServiceProduct
			// serviceProduct=ofy().load().type(ServiceProduct.class).filter("companyId",
			// invoiceentity.getCompanyId()).filter("count",invoiceentity.getSalesOrderProducts().get(i).getProdId()).first().now();
			Phrase specificationPhrase = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getPrduct()
					.getSpecification(), font9);
			PdfPCell specificationCell = new PdfPCell(specificationPhrase);
			// frequencyCell.addElement(frequencyPhrase);//By Jayshree comment this line Date25/11/2017
			specificationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
			specificationCell.setVerticalAlignment(Element.ALIGN_CENTER);
			productDetailTab.addCell(specificationCell);

			Phrase serviceAmountPhrase = new Phrase(df.format(invoiceentity
					.getSalesOrderProducts().get(i).getBasePaymentAmount())
					+ "", font9bold);
			PdfPCell serviceAmountCell = new PdfPCell(serviceAmountPhrase);
			// generalDisinfestationAmountCell.addElement(generalDisinfestationAmountPhrase);//By Jayshree comment this line Date25/11/2017
			serviceAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			serviceAmountCell.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
			productDetailTab.addCell(serviceAmountCell);

			try {
				document.add(productDetailTab);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
	
	private void createTaxDetailTable() {

		PdfPTable taxDetailTab = new PdfPTable(2);
		taxDetailTab.setWidthPercentage(100);

		try {
			taxDetailTab.setWidths(new float[] { 85, 15 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase totalPhrase = new Phrase(" Total ", font9bold);
		PdfPCell totalCell = new PdfPCell(totalPhrase);
		// totalCell.addElement(totalPhrase);//By Jayshree comment this line Date25/11/2017
		// totalCell.setColspan(2);
		totalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// totalCell.setVerticalAlignment(Element.ALIGN_TOP);
		taxDetailTab.addCell(totalCell);

		double totalamt = 0;
		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			if (invoiceentity.getSalesOrderProducts().get(i)
					.getBasePaymentAmount() != 0
					&& invoiceentity.getSalesOrderProducts().get(i)
							.getPaymentPercent() != 0) {
				totalamt = totalamt
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getBasePaymentAmount();
			} else {
				totalamt = totalamt
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getBaseBillingAmount();
			}
		}

		Phrase totalAmountPhrase = new Phrase(df.format(totalamt) + "", font9bold);
		PdfPCell totalAmountCell = new PdfPCell(totalAmountPhrase);
		totalAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		totalAmountCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// totalAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
		taxDetailTab.addCell(totalAmountCell);
		boolean disflag = false;
		// boolean totflag=false;
		if (invoiceentity.getDiscountAmt() != 0) {
			disflag = true;
		} else {
			disflag = false;
		}

		if (disflag == true) {
			Phrase discountPhrase = new Phrase(" Discount ", font9bold);
			PdfPCell discountCell = new PdfPCell(discountPhrase);
			discountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountCell.setVerticalAlignment(Element.ALIGN_CENTER);
			taxDetailTab.addCell(discountCell);

			Phrase dicountAmtPhrase = new Phrase(df.format(invoiceentity.getDiscountAmt())
					+ "", font9bold);
			PdfPCell dicountAmtCell = new PdfPCell(dicountAmtPhrase);
			dicountAmtCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			dicountAmtCell.setVerticalAlignment(Element.ALIGN_CENTER);
			taxDetailTab.addCell(dicountAmtCell);

			Phrase taxeblePhrase = new Phrase(" Total ", font9bold);
			PdfPCell taxebleCell = new PdfPCell(taxeblePhrase);
			taxebleCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			taxebleCell.setVerticalAlignment(Element.ALIGN_CENTER);
			taxDetailTab.addCell(taxebleCell);

			Phrase taxebleValPhrase = new Phrase(df.format(
					invoiceentity.getFinalTotalAmt()) + "", font9bold);
			PdfPCell taxebleValCell = new PdfPCell(taxebleValPhrase);
			taxebleValCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			taxebleValCell.setVerticalAlignment(Element.ALIGN_CENTER);
			taxDetailTab.addCell(taxebleValCell);

		}

		for (int i = 0; i < invoiceentity.getBillingTaxes().size(); i++) {

			Phrase cgstPhrase = new Phrase(invoiceentity.getBillingTaxes()
					.get(i).getTaxChargeName()
					+ "@"
					+ invoiceentity.getBillingTaxes().get(i)
							.getTaxChargePercent() +"%", font9bold); //% added by Ashwini Patil
			PdfPCell cgstCell = new PdfPCell(cgstPhrase);
			// cgstCell.addElement(cgstPhrase);
			cgstCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cgstCell.setVerticalAlignment(Element.ALIGN_CENTER);
			// cgstCell.setVerticalAlignment(Element.ALIGN_TOP);
			taxDetailTab.addCell(cgstCell);

			Phrase cgstValuePhrase = new Phrase(df.format(invoiceentity.getBillingTaxes()
					.get(i).getPayableAmt())
					+ "", font9bold);
			PdfPCell cgstValueCell = new PdfPCell(cgstValuePhrase);
			// cgstValueCell.addElement(cgstValuePhrase);//By Jayshree comment this line Date25/11/2017
			// cgstValueCell.setColspan(2);
			cgstValueCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cgstValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
			// cgstValueCell.setVerticalAlignment(Element.ALIGN_TOP);
			taxDetailTab.addCell(cgstValueCell);

		}

		Phrase grandtotalPhrase = new Phrase(" Grand Total ", font9bold);
		PdfPCell grandtotalCell = new PdfPCell(grandtotalPhrase);
		// grandtotalCell.addElement(grandtotalPhrase);//By Jayshree comment this line Date25/11/2017
		// grandtotalCell.setColspan(2);
		grandtotalCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		grandtotalCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// grandtotalCell.setVerticalAlignment(Element.ALIGN_TOP);
		taxDetailTab.addCell(grandtotalCell);

		Phrase grandtotalAmountPhrase = new Phrase(df.format(
				invoiceentity.getNetPayable())+ "", font9bold);
		PdfPCell grandtotalAmountCell = new PdfPCell(grandtotalAmountPhrase);
		// grandtotalAmountCell.addElement(grandtotalAmountPhrase);//By Jayshree comment this line Date25/11/2017
		// grandtotalAmountCell.setColspan(2);
		grandtotalAmountCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		grandtotalAmountCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// grandtotalAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
		taxDetailTab.addCell(grandtotalAmountCell);

		try {
			document.add(taxDetailTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void createComTaxTable() {

		PdfPTable compTaxTab = new PdfPTable(1);
		compTaxTab.setWidthPercentage(100f);

//		try {
//			compTaxTab.setWidths(new float[] { 30, 70 });
//		} catch (DocumentException e1) {
//			e1.printStackTrace();
//		}
	
		/**
		 *   Added By Priyanka Date : 10-04-2021
		 *   When Branch As process configuration Active then all details  should be print
		 */
		String gstinValue = "", panNovalue = "", sacvalue = "", esicvalue ="";
		/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
		if(branchDt != null){
			if(branchDt.getGSTINNumber() != null && !branchDt.getGSTINNumber().equals("")){
				gstinValue = branchDt.getGSTINNumber();
			}
		}
		
		if(branchDt != null){
			if(branchDt.getPANNumber() != null && !branchDt.getPANNumber().equals("")){
				panNovalue = branchDt.getPANNumber();
			}
		}
		
		
		if(branchDt != null){
			if(branchDt.getEsicCode()!= null && !branchDt.getEsicCode().equals("")){
				esicvalue = branchDt.getEsicCode();
			}
		}
		
		

		for (int i = 0; i < comp.getArticleTypeDetails().size(); i++) {
			/** date 20.12.2018 added by komal to load branch for branch gstin on print **/
			if (gstinValue.equals("")) {
				if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
						.equalsIgnoreCase("GSTIN")) {
					gstinValue = comp.getArticleTypeDetails().get(i)
							.getArticleTypeValue();
				}
			}

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("PAN NO")) {
				panNovalue = comp.getArticleTypeDetails().get(i)
						.getArticleTypeValue();
			}

			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("SAC")) {
				sacvalue = comp.getArticleTypeDetails().get(i)
						.getArticleTypeValue();
			}
			
			if (comp.getArticleTypeDetails().get(i).getArticleTypeName()
					.equalsIgnoreCase("ESIC NO")) {
				esicvalue = comp.getArticleTypeDetails().get(i)
						.getArticleTypeValue();
			}
		}
		
		
		
		
		Phrase gstinPhrase = new Phrase("GSTIN  : " +gstinValue  ,font9);
		PdfPCell gstinCell = new PdfPCell(gstinPhrase);
		//gstinCell.setBorder(0);
		gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		

		Phrase panNoPhrase = new Phrase("PAN NO  : " +panNovalue  ,font9);
		PdfPCell panNoCell = new PdfPCell(panNoPhrase);
		//gstinCell.setBorder(0);
		panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase sacNoPhrase = new Phrase("SAC NO  : " +sacvalue  ,font9);
		PdfPCell sacNoCell = new PdfPCell(sacNoPhrase);
		//gstinCell.setBorder(0);
		sacNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		Phrase esicNoPhrase = new Phrase("ESIC NO  : " +esicvalue  ,font9);
		PdfPCell esicNoCell = new PdfPCell(esicNoPhrase);
		//gstinCell.setBorder(0);
		esicNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		if(gstinValue!=null&&!gstinValue.equals("")) {
			compTaxTab.addCell(gstinCell);
			}
		if(panNovalue!=null&&!panNovalue.equals("")) {
			compTaxTab.addCell(panNoCell);
			}
		if(sacvalue!=null&&!sacvalue.equals("")) {
			compTaxTab.addCell(sacNoCell);
			}
		if(esicvalue!=null&&!esicvalue.equals("")) {
			compTaxTab.addCell(esicNoCell);
			}
			
		
//		
//		// Phrase gstinPhrase=new Phrase("GSTIN",font8);
//		Phrase gstinPhrase = null;
//		gstinPhrase = new Phrase("GSTIN", font9);
//
//		PdfPCell gstinCell = new PdfPCell(gstinPhrase);
////		gstinCell.addElement(gstinPhrase);//By Jayshree comment this line Date25/11/2017
////		gstinCell.setBorder(0);
//		gstinCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		gstinCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(gstinPhrase!=null)
//		{
//		compTaxTab.addCell(gstinCell);
//		}
//		// Phrase gstinValuePhrase=new Phrase( gstinNo,font8);
//		Phrase gstinValuePhrase = null;
//		gstinValuePhrase = new Phrase(gstinValue, font9);
//		PdfPCell gstinValueCell = new PdfPCell(gstinValuePhrase);
////		gstinValueCell.addElement(gstinValuePhrase);//By Jayshree comment this line Date25/11/2017
////		gstinValueCell.setBorder(0);
//		gstinValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		gstinValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(gstinValueCell!=null)
//		{
//		compTaxTab.addCell(gstinValueCell);
//		}
//		// /
//
//		// Phrase panNoPhrase=new Phrase("PAN NO",font8);
//		Phrase panNoPhrase = null;
//		panNoPhrase = new Phrase("PAN NO", font9);
//		PdfPCell panNoCell = new PdfPCell(panNoPhrase);
//		// panNoCell.addElement(panNoPhrase);//By Jayshree comment this line Date25/11/2017
////		panNoCell.setBorder(0);
//		 panNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		panNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(panNoCell!=null) {
//		compTaxTab.addCell(panNoCell);
//		}
//		
//		Phrase panNoValuePhrase = null;
//		panNoValuePhrase = new Phrase(panNovalue, font9);
//		PdfPCell panNoValueCell = new PdfPCell(panNoValuePhrase);
//		// panNoValueCell.addElement(panNoValuePhrase);//By Jayshree comment this line Date25/11/2017
////		panNoValueCell.setBorder(0);
//		 panNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		panNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(panNoValueCell!=null) {
//     	compTaxTab.addCell(panNoValueCell);
//		}
//		//
//		// Phrase panNoPhrase=new Phrase("PAN NO",font8);
//		Phrase sacNoPhrase = null;
//		sacNoPhrase = new Phrase("SAC NO", font9);
//		PdfPCell sacNoCell = new PdfPCell(sacNoPhrase);
////		sacNoCell.addElement(sacNoPhrase);//By Jayshree comment this line Date25/11/2017
////		sacNoCell.setBorder(0);
//		sacNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		sacNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(sacNoCell!=null) {
//		compTaxTab.addCell(sacNoCell);
//		}
//		
//		Phrase sacNoValuePhrase = null;
//		sacNoValuePhrase = new Phrase(sacvalue, font9);
//		PdfPCell sacNoValueCell = new PdfPCell(sacNoValuePhrase);
////		sacNoValueCell.addElement(sacNoValuePhrase);//By Jayshree comment this line Date25/11/2017
////		sacNoValueCell.setBorder(0);
//		sacNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		sacNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(sacNoValueCell!=null) {
//		compTaxTab.addCell(sacNoValueCell);
//		}
//		
//		Phrase esicNoPhrase = null;
//		esicNoPhrase = new Phrase("ESIC NO", font9);
//		PdfPCell esicNoCell = new PdfPCell(esicNoPhrase);
////		sacNoCell.addElement(sacNoPhrase);//By Jayshree comment this line Date25/11/2017
////		sacNoCell.setBorder(0);
//		esicNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		esicNoCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(esicNoCell!=null) {
//		compTaxTab.addCell(esicNoCell);
//		}
//		
//		Phrase esicNoValuePhrase = null;
//		esicNoValuePhrase = new Phrase(esicvalue, font9);
//		PdfPCell esicNoValueCell = new PdfPCell(esicNoValuePhrase);
////		sacNoValueCell.addElement(sacNoValuePhrase);//By Jayshree comment this line Date25/11/2017
////		esicNoValueCell.setBorder(0);
//		esicNoValueCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//		esicNoValueCell.setVerticalAlignment(Element.ALIGN_CENTER);
//		if(esicNoValueCell!=null) {
//		compTaxTab.addCell(esicNoValueCell);
//		}
//		ArticleType articleType=null;
//		for(int i=0;i<comp.getArticleTypeDetails().size();i++){
//			if(comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("ServiceInvoice")
//					&&comp.getArticleTypeDetails().get(i).getArticleTypeName().equalsIgnoreCase("GSTIN")){
//			    articleType=comp.getArticleTypeDetails().get(i);	
//				
//			}
//		}
		ServerAppUtility serverApp = new ServerAppUtility();
        
		    String gstin = "";
		
			logger.log(Level.SEVERE, "GST Applicable");
			gstin = serverApp.getGSTINOfCompany(comp, invoiceentity
					.getBranch().trim());
			System.out.println("gstin" + gstin);

		
		
		
//		if(articleType!=null){
//         logger.log(Level.SEVERE, "Inside articleType");
//			Phrase articleNamePhra = new Phrase(articleType.getArticleTypeName(), font9);
//			PdfPCell articleName = new PdfPCell(articleNamePhra);
//			articleName.setHorizontalAlignment(Element.ALIGN_LEFT);
//			articleName.setVerticalAlignment(Element.ALIGN_CENTER);
//			compTaxTab.addCell(articleName);
//			
//			logger.log(Level.SEVERE, "Inside articleType value "+gstin);
//			Phrase articleValuePh = new Phrase(gstin, font9);
//			PdfPCell articleValuePhrase = new PdfPCell(articleValuePh);
//			articleValuePhrase.setHorizontalAlignment(Element.ALIGN_LEFT);
//			articleValuePhrase.setVerticalAlignment(Element.ALIGN_CENTER);
//			compTaxTab.addCell(articleValuePhrase);
//		}
//		
//		comp.getArticleTypeDetails().remove(articleType);
//		String articleTypeName="";
//	    String articleTypeValue="";
//		for(int i=0;i<comp.getArticleTypeDetails().size();i++)
//		{
//			if(comp.getArticleTypeDetails().get(i).getDocumentName().equalsIgnoreCase("ServiceInvoice")){
//			if(comp.getArticleTypeDetails().get(i).getArticleTypeName() !=null && comp.getArticleTypeDetails().get(i).getArticleTypeValue() !=null)
//			{
//				articleTypeName = comp.getArticleTypeDetails().get(i).getArticleTypeName();	
//				articleTypeValue=comp.getArticleTypeDetails().get(i).getArticleTypeValue();
//				
//				Phrase articleNamePhrase = new Phrase(articleTypeName, font9);
//				PdfPCell articleNameCell = new PdfPCell(articleNamePhrase);
//				articleNameCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				articleNameCell.setVerticalAlignment(Element.ALIGN_CENTER);
//				compTaxTab.addCell(articleNameCell);
//
//				
//				Phrase articleValuePhrase = new Phrase(articleTypeValue, font9);
//				PdfPCell articleValuePhraseCell = new PdfPCell(articleValuePhrase);
//				articleValuePhraseCell.setHorizontalAlignment(Element.ALIGN_LEFT);
//				articleValuePhraseCell.setVerticalAlignment(Element.ALIGN_CENTER);
//				compTaxTab.addCell(articleValuePhraseCell);
//				
//			}
//			}
//		}

		try {
			document.add(compTaxTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for to
	 * add the payment term percentage and comment according to the product and
	 * check the condition ie if more than one product is present
	 */
	private void createPaymentTable() {
		// TODO Auto-generated method stub

		PdfPTable paymentTab = new PdfPTable(1);
		paymentTab.setWidthPercentage(100f);

		try {
			paymentTab.setWidths(new float[] { 100 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

		Phrase paymentTermsPhrase = new Phrase("Payment Terms ", font9bold);
		PdfPCell paymentTermCell = new PdfPCell(paymentTermsPhrase);
		// paymentTermCell.addElement(paymentTermsPhrase);//By Jayshree comment this line Date25/11/2017
		paymentTermCell.setBorder(0);
		// paymentTermCell.setColspan(3);
		paymentTermCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		paymentTermCell.setVerticalAlignment(Element.ALIGN_CENTER);
		// paymentTermCell.setVerticalAlignment(Element.ALIGN_TOP);
		paymentTab.addCell(paymentTermCell);

		try {
			document.add(paymentTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPTable prodPaymentTab = new PdfPTable(2);
		prodPaymentTab.setWidthPercentage(100f);
		prodPaymentTab.setSpacingBefore(3);

		try {
			prodPaymentTab.setWidths(new float[] { 30, 70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		String paymenterm = "";
		for (int i = 0; i < invoiceentity.getArrPayTerms().size(); i++) {

			paymenterm = invoiceentity.getArrPayTerms().get(i)
					.getPayTermPercent()
					+ "%"
					+ invoiceentity.getArrPayTerms().get(i).getPayTermComment();
		}

		for (int i = 0; i < invoiceentity.getSalesOrderProducts().size(); i++) {

			String productName = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();
			if (productName.contains("-")) {
				String[] product = productName.split(("-"));
				product1 = product[0];
				product2 = product[1];
			} else {
				product1 = invoiceentity.getSalesOrderProducts().get(i)
						.getProdName();
				product2 = "";
			}
		}
		if (invoiceentity.getSalesOrderProducts().size() == 1) {
			Phrase prodNamePhrase2 = new Phrase(product1, font9bold);
			PdfPCell prodNameCell2 = new PdfPCell(prodNamePhrase2);
			// generalDisinfestationCell2.addElement(generalDisinfestationPhrase2);//By Jayshree comment this line Date25/11/2017
			prodNameCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			prodNameCell2.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationCell2.setVerticalAlignment(Element.ALIGN_TOP);
			prodPaymentTab.addCell(prodNameCell2);

			Phrase paytermValuePhrase2;
			paytermValuePhrase2 = new Phrase(paymenterm, font9);
			PdfPCell paytermValueCell2 = new PdfPCell(paytermValuePhrase2);
			// generalDisinfestationCell2.addElement(generalDisinfestationPhrase2);//By Jayshree comment this line Date25/11/2017
			paytermValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			paytermValueCell2.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationCell2.setVerticalAlignment(Element.ALIGN_TOP);
			prodPaymentTab.addCell(paytermValueCell2);
		}

		else {
			Phrase paymentValuePhrase2 = new Phrase(paymenterm, font9);
			PdfPCell paymentValueCell2 = new PdfPCell(paymentValuePhrase2);
			// generalDisinfestationCell2.addElement(generalDisinfestationPhrase2);//By Jayshree comment this line Date25/11/2017
			paymentValueCell2.setHorizontalAlignment(Element.ALIGN_LEFT);
			paymentValueCell2.setColspan(2);

			paymentValueCell2.setVerticalAlignment(Element.ALIGN_CENTER);
			// generalDisinfestationCell2.setVerticalAlignment(Element.ALIGN_TOP);
			prodPaymentTab.addCell(paymentValueCell2);
		}

		try {
			document.add(prodPaymentTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		PdfPTable commentTab = new PdfPTable(2);
		commentTab.setWidthPercentage(100f);
		/**
		 * date 25/11/2017
		 * By jayshree
		 * Des.to remove the space comment this code
		 */
//		commentTab.setSpacingBefore(10f);
//End for jayshree
		try {
			commentTab.setWidths(new float[] { 30,70 });
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
		Phrase commentph =new Phrase("Special Instruction",font9bold);
		PdfPCell commentCell=new PdfPCell(commentph);
		commentCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		commentTab.addCell(commentCell);
		
		Phrase commentValph =new Phrase(invoiceentity.getComment(),font9);
		PdfPCell commentValCell=new PdfPCell(commentValph);
		commentValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		commentValCell.setFixedHeight(40);
		commentTab.addCell(commentValCell);
		
		try {
			document.add(commentTab);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	// *******************************End********************************
	private void createInvoiceFootertable() {

		// TODO Auto-generated method stub
		// ***************
		PdfPTable footerTab1 = new PdfPTable(1);
		footerTab1.setWidthPercentage(100f);
		 footerTab1.setSpacingBefore(15f);

		Phrase footerTablePhrase = new Phrase("For "
				+ comp.getBusinessUnitName(), font9bold);
		PdfPCell footerTableCell = new PdfPCell(footerTablePhrase);
		// footerTableCell.addElement(footerTablePhrase);//By Jayshree comment this line Date25/11/2017
		footerTableCell.setBorder(0);
		footerTableCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		footerTableCell.setVerticalAlignment(Element.ALIGN_CENTER);
		footerTab1.addCell(footerTableCell);

		DocumentUpload digitalDocument = comp.getUploadDigitalSign();
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}
		PdfPCell imageSignCell = null;
		// Image image2 = null;
		// // logger.log(Level.SEVERE, "hostUrl::" + hostUrl);
		// try {
		// image2 = Image.getInstance(new URL(hostUrl
		// + digitalDocument.getUrl()));
		// image2.scalePercent(15f);
		// image2.scaleAbsoluteWidth(100f);
		//
		//
		// imageSignCell = new PdfPCell(image2);
		// imageSignCell.setBorder(0);
		// // if (authOnLeft) {
		// // imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// // } else {
		// imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// // }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		// Image image1 = null;
		// try {
		// image1 = Image.getInstance("images/digisign2copy.png");
		// } catch (BadElementException e2) {
		// // TODO Auto-generated catch block
		// e2.printStackTrace();
		// } catch (MalformedURLException e2) {
		// // TODO Auto-generated catch block
		// e2.printStackTrace();
		// } catch (IOException e2) {
		// // TODO Auto-generated catch block
		// e2.printStackTrace();
		// }
		// // String image1="";
		// image1.scalePercent(15f);
		// image1.scaleAbsoluteWidth(100f);
		// imageSignCell=new PdfPCell(image1);
		// imageSignCell.setBorder(0);
		// // if (authOnLeft) {
		// // imageSignCell.setHorizontalAlignment(Element.ALIGN_LEFT);
		// // } else {
		// imageSignCell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		// // }
		// // }
		// // catch(Exception e)
		// // {
		// // e.printStackTrDayace();
		// // }
		//

		if (imageSignCell != null) {
			footerTab1.addCell(imageSignCell);
		} else {
			Phrase blank = new Phrase(" ", font9);
			PdfPCell blankCell = new PdfPCell(blank);
			// blankCell.addElement(blank);
			blankCell.setBorder(0);
			footerTab1.addCell(blankCell);
			footerTab1.addCell(blankCell);
			footerTab1.addCell(blankCell);
			// footerTab1.addCell(blankCell);
			// footerTab1.addCell(blankCell);
			// footerTab1.addCell(blankCell);
		}
		Phrase footerTablePhrase2 = new Phrase("Authorised Signatory", font9);
		PdfPCell footerTableCell2 = new PdfPCell(footerTablePhrase2);
		// footerTableCell.addElement(footerTablePhrase2);//By Jayshree comment this line Date25/11/2017
		footerTableCell2.setBorder(0);
		footerTableCell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		footerTableCell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		footerTab1.addCell(footerTableCell2);

		// ***********

		PdfPTable footerTable2 = new PdfPTable(1);
		footerTable2.setWidthPercentage(100f);
		footerTable2.setSpacingBefore(10f);

		// Phrase footer = new Phrase("");
		PdfPCell footercell2 = new PdfPCell();
		// footerTableCell.addElement(footerTablePhrase);//By Jayshree comment this line Date25/11/2017
		footercell2.setBorder(0);
		footercell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		footercell2.setVerticalAlignment(Element.ALIGN_CENTER);
		footercell2.addElement(footerTab1);
		footerTable2.addCell(footercell2);

		try {
			document.add(footerTable2);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Date 25/09/2017 By- Jayshree chavan Description-changes are made for to
	 * print the remaining product in the next page
	 */
	private void createRestProductAnnexure(int prodSize) {

		try {
			document.add(Chunk.NEXTPAGE);
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		try {
			document.add(new Phrase("Annexure", font9bold));
		} catch (DocumentException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		createProductHeaderTable();

		for (int i = productCount; i < invoiceentity.getSalesOrderProducts()
				.size(); i++) {
			PdfPTable productDetailTab = new PdfPTable(4);
			productDetailTab.setWidthPercentage(100f);

			try {
				productDetailTab.setWidths(new float[] { 30, 30, 25, 15 });
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

			String productName = invoiceentity.getSalesOrderProducts().get(i)
					.getProdName();
			if (productName.contains("-")) {
				String[] product = productName.split(("-"));
				product1 = product[0];
				product2 = product[1];
			} else {
				product1 = invoiceentity.getSalesOrderProducts().get(i)
						.getProdName();
				product2 = "";
			}

			Phrase treatmentValPhrase = new Phrase(product1, font9);
			PdfPCell treatmentValCell = new PdfPCell(treatmentValPhrase);
			// generalDisinfestationCell.addElement(generalDisinfestationPhrase);//By Jayshree comment this line Date25/11/2017
			treatmentValCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			treatmentValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// generalDisinfestationCell.setVerticalAlignment(Element.ALIGN_TOP);
			productDetailTab.addCell(treatmentValCell);

			Phrase serviceNoPhrase;
			if (!product2.equals("")) {
				serviceNoPhrase = new Phrase(product2
						+ "("
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getOrderServices() + "  Services)", font9);
			} else {
				serviceNoPhrase = new Phrase("("
						+ invoiceentity.getSalesOrderProducts().get(i)
								.getOrderServices() + "  Services)", font9);
			}
			PdfPCell serviceNoCell = new PdfPCell(serviceNoPhrase);
			// generalDisinfestationFrequencyCell.addElement(generalDisinfestationFrequencyPhrase);//By Jayshree comment this line Date25/11/2017
			serviceNoCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			serviceNoCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// generalDisinfestationFrequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
			productDetailTab.addCell(serviceNoCell);

			ServiceProduct serviceProduct = ofy()
					.load()
					.type(ServiceProduct.class)
					.filter("companyId", invoiceentity.getCompanyId())
					.filter("count",
							invoiceentity.getSalesOrderProducts().get(i)
									.getProdId()).first().now();
			Phrase specificationPhrase = new Phrase(
					serviceProduct.getSpecification(), font9);
			PdfPCell specificationCell = new PdfPCell(specificationPhrase);
			// frequencyCell.addElement(frequencyPhrase);//By Jayshree comment this line Date25/11/2017
			specificationCell.setHorizontalAlignment(Element.ALIGN_LEFT);
			// frequencyCell.setVerticalAlignment(Element.ALIGN_TOP);
			specificationCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			productDetailTab.addCell(specificationCell);

			Phrase serviceAmountPhrase = new Phrase(invoiceentity
					.getSalesOrderProducts().get(i).getBasePaymentAmount()
					+ "", font9);
			PdfPCell serviceAmountCell = new PdfPCell(serviceAmountPhrase);
			// generalDisinfestationAmountCell.addElement(generalDisinfestationAmountPhrase);//By Jayshree comment this line Date25/11/2017
			serviceAmountCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			serviceAmountCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			// generalDisinfestationAmountCell.setVerticalAlignment(Element.ALIGN_TOP);
			productDetailTab.addCell(serviceAmountCell);

			totalamt = totalamt
					+ invoiceentity.getSalesOrderProducts().get(i)
							.getBasePaymentAmount();

			try {
				document.add(productDetailTab);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	/***
	 * date 12/10/2017 by jayshree changes are made to add the customer branch
	 * detail in table if customer branch is selected.
	 */
	private void createBranchInfoTab() {

		if (invoiceentity.getCustomerBranch() != null
				&& !invoiceentity.getCustomerBranch().equals(" "))

		{
			if(invoiceentity.getCustomerBranch().equals("--SELECT--")){
			try {
				document.add(Chunk.NEXTPAGE);
			} catch (DocumentException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			try {
				document.add(new Phrase("Branch Details", font9bold));
			} catch (DocumentException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			PdfPTable custBranchHeadTab = new PdfPTable(5);
			custBranchHeadTab.setWidthPercentage(100f);

			try {
				custBranchHeadTab.setWidths(new float[] { 5, 20, 30, 15, 20 });
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

			Phrase branchSrPh = new Phrase("Sr.No", font9);
			PdfPCell branchSrCell = new PdfPCell(branchSrPh);
			branchSrCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			branchSrCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			custBranchHeadTab.addCell(branchSrCell);

			Phrase branchNamePh = new Phrase("Branch Name", font9);
			PdfPCell branchNameCell = new PdfPCell(branchNamePh);
			branchNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			branchNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			custBranchHeadTab.addCell(branchNameCell);

			Phrase branchAddPh = new Phrase("Address", font9);
			PdfPCell branchAddCell = new PdfPCell(branchAddPh);
			branchAddCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			branchAddCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			custBranchHeadTab.addCell(branchAddCell);

			Phrase mobilePh = new Phrase("Mobile", font9);
			PdfPCell mobileCell = new PdfPCell(mobilePh);
			mobileCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			mobileCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			custBranchHeadTab.addCell(mobileCell);

			Phrase contactPersonPh = new Phrase("Contact Person", font9);
			PdfPCell contactPersonCell = new PdfPCell(contactPersonPh);
			contactPersonCell.setHorizontalAlignment(Element.ALIGN_CENTER);
			contactPersonCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			custBranchHeadTab.addCell(contactPersonCell);

			if (customerbranchlist.size() > 0) {
				try {
					document.add(custBranchHeadTab);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			PdfPTable custBranchDetailTab = new PdfPTable(5);
			custBranchDetailTab.setWidthPercentage(100f);

			try {
				custBranchDetailTab
						.setWidths(new float[] { 5, 20, 30, 15, 20 });
			} catch (DocumentException e1) {
				e1.printStackTrace();
			}

			// CustomerBranchDetails
			// branch=ofy().load().type(CustomerBranchDetails.class)
			// .filter("companyId",
			// comp.getCompanyId()).filter("buisnessUnitName",invoiceentity.
			// getCustomerBranch()).first().now();
			// // for (int i = 0; i <customerbranchlist.size() ; i++){
			// branchName=branch.getBranch();
			// branchAdd=branch.getAddress().getCompleteAddress();
			// mobileNo=branch.getCellNumber1();
			// contactPerson=branch.getPointOfContact().getFullname();
			// srno=i+1;}
			// }
			for (int i = 0; i < customerbranchlist.size(); i++) {
				System.out.println(" customer branch"
						+ customerbranchlist.get(i).getBusinessUnitName());

				Phrase branchSrValPh = new Phrase(i + 1 + "", font9);
				PdfPCell branchSrValCell = new PdfPCell(branchSrValPh);
				branchSrValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				branchSrValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				custBranchDetailTab.addCell(branchSrValCell);

				Phrase branchNameValPh = new Phrase(customerbranchlist.get(i)
						.getBusinessUnitName(), font9);
				PdfPCell branchNameValCell = new PdfPCell(branchNameValPh);
				branchNameValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				branchNameValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				custBranchDetailTab.addCell(branchNameValCell);

				Phrase branchAddValPh = new Phrase(customerbranchlist.get(i)
						.getAddress().getCompleteAddress(), font9);
				PdfPCell branchAddValCell = new PdfPCell(branchAddValPh);
				branchAddValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				branchAddValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				custBranchDetailTab.addCell(branchAddValCell);

				Phrase mobileValPh = new Phrase(customerbranchlist.get(i)
						.getCellNumber1() + "", font9);
				PdfPCell mobileValCell = new PdfPCell(mobileValPh);
				mobileValCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				mobileValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				custBranchDetailTab.addCell(mobileValCell);

				Phrase contactPersonValPh = new Phrase(customerbranchlist
						.get(i).getPocName(), font9);
				PdfPCell contactPersonValCell = new PdfPCell(contactPersonValPh);
				contactPersonValCell
						.setHorizontalAlignment(Element.ALIGN_CENTER);
				contactPersonValCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
				custBranchDetailTab.addCell(contactPersonValCell);

			}

			try {
				document.add(custBranchDetailTab);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		}

	}

	// ****************End**********************
	private void createCompanyNameAsHeader(Document doc, Company comp) {
		System.out.print("hiiiiiii");

		DocumentUpload document = comp.getUploadHeader();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 725f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createCompanyNameAsFooter(Document doc, Company comp) {

		DocumentUpload document = comp.getUploadFooter();

		// patch
		String hostUrl;
		String environment = System
				.getProperty("com.google.appengine.runtime.environment");
		if (environment.equals("Production")) {
			String applicationId = System
					.getProperty("com.google.appengine.application.id");
			String version = System
					.getProperty("com.google.appengine.application.version");
			hostUrl = "http://" + version + "." + applicationId
					+ ".appspot.com/";
		} else {
			hostUrl = "http://localhost:8888";
		}

		try {
			Image image2 = Image.getInstance(new URL(hostUrl
					+ document.getUrl()));
			image2.scalePercent(15f);
			image2.scaleAbsoluteWidth(520f);
			image2.setAbsolutePosition(40f, 40f);
			doc.add(image2);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createRestProductDetail() {

		if (noOfLine == 0 && productCount != 0) {
			createRestProductAnnexure(productCount);
		}
	}

	private void createBlankforUPC() {
		System.out.print("jayshree");
		Phrase blankphrase = new Phrase("", font9);
		PdfPCell blankCell = new PdfPCell();
		blankCell.addElement(blankphrase);
		blankCell.setBorder(0);

		PdfPTable titlepdftable = new PdfPTable(3);
		titlepdftable.setWidthPercentage(100);
		titlepdftable.setHorizontalAlignment(Element.ALIGN_CENTER);
		titlepdftable.addCell(blankCell);
		titlepdftable.addCell(blankCell);

		Paragraph blank = new Paragraph();
		blank.add(Chunk.NEWLINE);

		PdfPTable parent = new PdfPTable(1);
		parent.setWidthPercentage(100);

		PdfPCell titlePdfCell = new PdfPCell(titlepdftable);
		titlePdfCell.setBorder(0);
		parent.addCell(titlePdfCell);

		try {
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(blank);
			document.add(parent);
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}

}
