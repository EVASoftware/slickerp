package com.slicktechnologies.server.clientsms;
import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.http.impl.BHttpConnectionBase;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;

import com.google.gson.JsonElement;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.Slick_Erp;
import com.slicktechnologies.client.services.SmsService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.AppUtility;
import com.slicktechnologies.client.views.contractrenewal.RenewalResult;
import com.slicktechnologies.client.views.customertraining.CustomerTrainingAttendies;
import com.slicktechnologies.server.CommonServiceImpl;
import com.slicktechnologies.server.Email;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessprocesslayer.PurchaseOrder;
import com.slicktechnologies.shared.common.businessprocesslayer.VendorDetails;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.FeedbackUrlAsPerCustomerCategory;
import com.slicktechnologies.shared.common.complain.Complain;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.deliverynote.DeliveryNote;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.inventory.GRN;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.sms.SMSDetails;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SmsServiceImpl extends RemoteServiceServlet implements SmsService {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5581356515574227371L;
	private SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
	Logger logger=Logger.getLogger("Sms Logger");
    boolean  EnableSMSAPIWEBTACTIC=false;
    boolean EnableSMSAPIBHASH=false;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private DecimalFormat decimalformat = new DecimalFormat("0.00"); //Ashwini Patil
	//by Ashwini Patil
	boolean EnableSMSUjumbe=false;
	
	public static List<EmailTemplateConfiguration> communicationChannelConfigurationlist;
	
	@Override
//	public Integer sendSmsToClient(String msgTemplate, long clientCell,String accSid, String userName, String password, long companyId) {
	public Integer sendSmsToClient(String msgTemplate, long clientCell, long companyId) {

		logger.log(Level.SEVERE,"Hiiiiiiiiiiiiiiiiiiiiiiii Inside Method 1");
		/***Date 13-4-2019 added by Amol
		 * Added a API  for BitcoPestControl raised by Sonu
		 *name=EnableSMSAPIWEBTACTIC
		 */
		
		EnableSMSAPIWEBTACTIC = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "EnableSMSAPIWEBTACTIC",companyId);
	
		EnableSMSAPIBHASH=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "EnableSMSAPIBHASH", companyId);
		
		EnableSMSUjumbe=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", AppConstants.PC_ENABLEUJUMBESMSAPI, companyId);
		
		
//      String mobiles ="91"+clientCell+"";
		String mobiles =clientCell+"";

		 SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId)
					.filter("status", true).first().now();
			logger.log(Level.SEVERE,"smsConfig"+ smsConfig);
		if(smsConfig!=null){
			if(smsConfig.getMobileNoPrefix()!=null && !smsConfig.getMobileNoPrefix().equals("")){
				mobiles = smsConfig.getMobileNoPrefix()+clientCell;
			}
		}
		else{
			logger.log(Level.SEVERE,"sms configuration is not active ");
		}
		
		String userName = smsConfig.getAuthToken();
		String password = smsConfig.getPassword();
		String accSid = smsConfig.getAccountSID();
		
		logger.log(Level.SEVERE,"User Id"+userName);
		logger.log(Level.SEVERE,"Password"+password);
		logger.log(Level.SEVERE,"MSG"+msgTemplate);
		logger.log(Level.SEVERE,"Sender ID"+accSid);
		

       
        String message = msgTemplate;

        logger.log(Level.SEVERE,"Hi vijay SMS Mobile No ===="+mobiles);
        logger.log(Level.SEVERE,"SMS ==== "+msgTemplate);
        
		if(EnableSMSAPIBHASH){
			
			/**
			 * Date 15 jun 2017 added by vijay for BHASH SMS API
			 * here i am calling BHASH sms api 
			 */
			
	        //Prepare Url
	        URLConnection myURLConnection=null;
	        URL myURL=null;
	        BufferedReader reader=null;

	        //encoding message
	        String encoded_message=URLEncoder.encode(message);
//	        http://bhashsms.com/api/sendmsg.php?user=success&pass=654321&sender=SUCCES&phone=7338881299&text=Test%20SMS&priority=ndnd&stype=normal
	        //Send SMS API
	        String mainUrl="http://bhashsms.com/api/sendmsg.php?";
	        //Prepare parameter string
	        StringBuilder sbPostData= new StringBuilder(mainUrl);
	        sbPostData.append("user="+userName);
	        sbPostData.append("&pass="+password);
	        sbPostData.append("&sender="+accSid);
	        sbPostData.append("&phone="+clientCell);
	        sbPostData.append("&text="+encoded_message);
	        sbPostData.append("&priority="+"ndnd");
	        sbPostData.append("&stype="+"normal");
	        //final string
	        mainUrl = sbPostData.toString();
	        
	        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
	        
	        try
	        {
	            //prepare connection
	            myURL = new URL(mainUrl);
	            myURLConnection = myURL.openConnection();
	            myURLConnection.connect();
	            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
	            //reading response
	            String response;
	            while ((response = reader.readLine()) != null)
	            //print response
	            System.out.println(response);

	            //finally close connection
	            reader.close();
	        }
	        catch (IOException e)
	        {
	    		logger.log(Level.SEVERE,"Hi Inside Catch Exception"+e);

	                e.printStackTrace();
	        }
			System.out.println(" sms sent successfully");
			logger.log(Level.SEVERE," sms sent successfully");
			
			
			
			/**
			 * end here
			 */
			
		}else if(EnableSMSAPIWEBTACTIC){
			
			logger.log(Level.SEVERE,"EnableSMSAPIWEBTACTIC sms sent successfully");
	        //Prepare Url
	        URLConnection myURLConnection=null;
	        URL myURL=null;
	        BufferedReader reader=null;

	        //encoding message
	        String encoded_message=URLEncoder.encode(message);
//	        http://bhashsms.com/api/sendmsg.php?user=success&pass=654321&sender=SUCCES&phone=7338881299&text=Test%20SMS&priority=ndnd&stype=normal
	        //Send SMS API
	        String mainUrl="http://websms.esandesh.in/websms/sendsms.aspx?";
	        //Prepare parameter string
	        StringBuilder sbPostData= new StringBuilder(mainUrl);
	        sbPostData.append("userid="+userName);
	        sbPostData.append("&password="+password);
	        sbPostData.append("&sender="+accSid);
	        sbPostData.append("&mobileno="+clientCell);
	        sbPostData.append("&msg="+encoded_message);
//	        sbPostData.append("&priority="+"ndnd");
//	        sbPostData.append("&stype="+"normal");
	        //final string
	        mainUrl = sbPostData.toString();
	        
	        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
	        
	        try
	        {
	            //prepare connection
	            myURL = new URL(mainUrl);
	            myURLConnection = myURL.openConnection();
	            myURLConnection.connect();
	            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
	            //reading response
	            String response;
	            while ((response = reader.readLine()) != null)
	            //print response
	            System.out.println(response);

	            //finally close connection
	            reader.close();
	        }
	        catch (IOException e)
	        {
	    		logger.log(Level.SEVERE,"Hi Inside Catch Exception"+e);

	                e.printStackTrace();
	        }
			System.out.println(" sms sent successfully");
			logger.log(Level.SEVERE," sms sent successfully");
			
			/**
			 * end here
			 */
		}
		
		//By Ashwini Patil
		else if(EnableSMSUjumbe){
			
			logger.log(Level.SEVERE,"EnableSMSUjumbe sms in process");
			logger.log(Level.SEVERE,"mobiles "+mobiles);
			logger.log(Level.SEVERE,"message "+message);
			logger.log(Level.SEVERE,"accSid "+accSid);
			String strrespomse = SendSMSUjumbe(message,mobiles,accSid,smsConfig.getAuthToken(),smsConfig.getPassword(),smsConfig.getSmsAPIUrl());
			
			logger.log(Level.SEVERE,"strrespomse "+strrespomse);

		}
		else{
	        //Prepare Url
	        URLConnection myURLConnection=null;
	        URL myURL=null;
	        BufferedReader reader=null;

	        //encoding message
	        String encoded_message=URLEncoder.encode(message);

//	        /** Date 18-03-2019 by Vijay updated with new API link for bhavana telecoms
//	         */
//	        String mainUrl="http://smpp.keepintouch.co.in/vendorsms/pushsms.aspx?";

//	        SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId)
//	        							.filter("status", true).first().now();
//			logger.log(Level.SEVERE,"smsConfig"+ smsConfig);
			if(smsConfig!=null){
			
			 	String mainUrl=smsConfig.getSmsAPIUrl();

		        String userNameMsg = mainUrl.replace("{UserName}".trim(), userName+"");
				String passwordMsg = userNameMsg.replace("{Password}".trim(),password);
				String PhoneNoMsg = passwordMsg.replace("{MobileNo}".trim(),mobiles);
				String SenderIdMsg = PhoneNoMsg.replace("{SenderId}".trim(),accSid);
				String finalurlMsg = SenderIdMsg.replace("{Message}".trim(),encoded_message);

		        logger.log(Level.SEVERE," finalurlMsg URL called==== "+finalurlMsg);
		        
		        try
		        {
		            //prepare connection
		            myURL = new URL(finalurlMsg);
		            myURLConnection = myURL.openConnection();
		            myURLConnection.connect();
		            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		            //reading response
		            String response;
		            while ((response = reader.readLine()) != null)
		            //print response
		            System.out.println(response);
		    		logger.log(Level.SEVERE,"response "+response);

		            //finally close connection
		            reader.close();
		        }
		        catch (IOException e)
		        {
		    		logger.log(Level.SEVERE,"Hi Inside Catch Exception");

		                e.printStackTrace();
		        }
				System.out.println(" sms sent successfully");
				logger.log(Level.SEVERE," sms sent successfully");
				
			}
	       
			
			/**
			 * ends here
			 */
			
			
		}
		
		
		return 1;
		
		
		
	}
	
	
	
	






	//************************************for sending sms to customer list for training  **********************	
	
		@Override
		public Integer sendSmsToCustomers(String msgTemplate,
				ArrayList<CustomerTrainingAttendies> custlist, String accSid,
				String userName, String password,  boolean bhashSMSAPIFlag) {
			Logger logger=Logger.getLogger("Sms Logger");
			logger.log(Level.SEVERE,"inside customer Hiiiiiiiiiiiiiiiiiiiiiiii Inside Method 1");
			
			System.out.println("in side sms service  =======");
			
			
			logger.log(Level.SEVERE,"User Id"+userName);
			logger.log(Level.SEVERE,"Password"+password);
			logger.log(Level.SEVERE,"MSG"+msgTemplate);
			logger.log(Level.SEVERE,"Sender ID"+accSid);
			
//	        //Your message to send, Add URL encoding here.
	        String message = msgTemplate;

	        logger.log(Level.SEVERE,"SMS ==== "+msgTemplate);
	        
			if(bhashSMSAPIFlag){
				
				/**
				 * Date 15 jun 2017 added by vijay for BHASH SMS API
				 */
			       //Multiple mobiles numbers separated by comma
				String mobiles ="";
				
				for(int i=0;i<custlist.size();i++){
					if(i==custlist.size()-1){
						mobiles += custlist.get(i).getCustCell();

					}else{
						mobiles += custlist.get(i).getCustCell()+",";

					}
				}
				
		        logger.log(Level.SEVERE,"Hi vijay SMS Mobile No ===="+mobiles);

		        //Prepare Url
		        URLConnection myURLConnection=null;
		        URL myURL=null;
		        BufferedReader reader=null;

		        //encoding message
		        String encoded_message=URLEncoder.encode(message);
//		        http://bhashsms.com/api/sendmsg.php?user=success&pass=654321&sender=SUCCES&phone=7338881299&text=Test%20SMS&priority=ndnd&stype=normal
		        //Send SMS API
		        String mainUrl="http://bhashsms.com/api/sendmsg.php?";
		        //Prepare parameter string
		        StringBuilder sbPostData= new StringBuilder(mainUrl);
		        sbPostData.append("user="+userName);
		        sbPostData.append("&pass="+password);
		        sbPostData.append("&sender="+accSid);
		        sbPostData.append("&phone="+mobiles);
		        sbPostData.append("&text="+encoded_message);
		        sbPostData.append("&priority="+"ndnd");
		        sbPostData.append("&stype="+"normal");
		        //final string
		        mainUrl = sbPostData.toString();
		        
		        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
		        
		        try
		        {
		            //prepare connection
		            myURL = new URL(mainUrl);
		            myURLConnection = myURL.openConnection();
		            myURLConnection.connect();
		            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		            //reading response
		            String response;
		            while ((response = reader.readLine()) != null)
		            //print response
		            System.out.println(response);

		            //finally close connection
		            reader.close();
		        }
		        catch (IOException e)
		        {
		    		logger.log(Level.SEVERE,"Hi Inside Catch Exception"+e);

		                e.printStackTrace();
		        }
				System.out.println(" sms sent successfully");
				logger.log(Level.SEVERE," sms sent successfully");
				
				
				
				/**
				 * end here
				 */
			}else{

				/**
				 *  New sms API code from bhavana change on 10 feb 2017
				 */
				

				String mobiles ="";
				
				for(int i=0;i<custlist.size();i++){
					mobiles += "91"+custlist.get(i).getCustCell()+",";
				}
		        logger.log(Level.SEVERE,"Hi vijay SMS Mobile No ===="+mobiles);
		        
		        
		        //Prepare Url
		        URLConnection myURLConnection=null;
		        URL myURL=null;
		        BufferedReader reader=null;

		        //encoding message
		        String encoded_message=URLEncoder.encode(message);

		        //Send SMS API
//		        String mainUrl="http://103.242.119.152/vendorsms/pushsms.aspx?";
		        /**
		         * Date 18-03-2019 by Vijay updated with new API link for bhavana telecoms
		         */
		        String mainUrl="http://smpp.keepintouch.co.in/vendorsms/pushsms.aspx?";
		        
		        //Prepare parameter string
		        StringBuilder sbPostData= new StringBuilder(mainUrl);
		        sbPostData.append("user="+userName);
		        sbPostData.append("&password="+password);
		        sbPostData.append("&msisdn="+mobiles);
		        sbPostData.append("&sid="+accSid);
		        sbPostData.append("&msg="+encoded_message);
		        sbPostData.append("&fl="+"0");
		        sbPostData.append("&gwid="+"2");
		        //final string
		        mainUrl = sbPostData.toString();
		        
		        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
		        
		        try
		        {
		            //prepare connection
		            myURL = new URL(mainUrl);
		            myURLConnection = myURL.openConnection();
		            myURLConnection.connect();
		            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		            //reading response
		            String response;
		            while ((response = reader.readLine()) != null)
		            //print response
		            System.out.println(response);

		            //finally close connection
		            reader.close();
		        }
		        catch (IOException e)
		        {
		    		logger.log(Level.SEVERE,"Hi Inside Catch Exception");

		                e.printStackTrace();
		        }
				System.out.println(" sms sent successfully");
				logger.log(Level.SEVERE," sms sent successfully");
				
				/**
				 * ends here
				 */
			}
			
			return 1;
		}
		
		

		@Override
		public Integer sendContractRenewalSMS(String msgTemplate,
				ArrayList<RenewalResult> custlist, String accSid,
				String userName, String password,long companyId,  boolean bhashSMSAPIFlag) {
			
			Logger logger=Logger.getLogger("Sms Logger");
			logger.log(Level.SEVERE,"inside customer Hiiiiiiiiiiiiiiiiiiiiiiii Inside Method 1");
			
			/**
			 *  New sms API code from bhavana change on 10 feb 2017
			 */
			logger.log(Level.SEVERE,"User Id"+userName);
			logger.log(Level.SEVERE,"Password"+password);


			logger.log(Level.SEVERE,"Sender ID"+accSid);
			
			 for(int  i=0;i<custlist.size();i++){
				
				Contract contract=ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", custlist.get(i).getContractId()).first().now();
				
				Customer cust=ofy().load().type(Customer.class).filter("companyId", companyId).filter("count",custlist.get(i).getCustomerId()).first().now();
				
				Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				String cutomerName = msgTemplate.replace("{CustomerName}", custlist.get(i).getCustomerName());
//				String productName = cutomerName.replace("{ProductName}", custlist.get(i).getProductName());
				String serviceNo = cutomerName.replace("{ContractId}", custlist.get(i).getContractId()+"");
				/** date 09.1.2019 added by komal to save date and company name **/
				String endDate = serviceNo.replace("{endDate}", fmt.format(contract.getEndDate()));
				endDate = endDate.replace("{EndDate}", fmt.format(contract.getEndDate()));
				
				String companyName  = "";
				if(company != null){
					companyName = company.getBusinessUnitName();
				}
				String actualMsg = endDate.replace("{companyName}", companyName);
				
				System.out.println("ACTUAL MSG ====="+actualMsg);
				System.out.println("contact no ====="+cust.getCellNumber1());
				
				String mobiles ="91"+cust.getCellNumber1()+"";
				 
				String cellNumber = cust.getCellNumber1()+"";
				 
	        String message = actualMsg;

	        logger.log(Level.SEVERE,"Hi vijay SMS Mobile No ===="+mobiles);
	        logger.log(Level.SEVERE,"SMS ==== "+actualMsg);
			
			
			if(bhashSMSAPIFlag){
				
				/**
				 * Date 15 jun 2017 added by vijay for BHASH SMS API
				 */
				
		        //Prepare Url
		        URLConnection myURLConnection=null;
		        URL myURL=null;
		        BufferedReader reader=null;

		        //encoding message
		        String encoded_message=URLEncoder.encode(message);
//		        http://bhashsms.com/api/sendmsg.php?user=success&pass=654321&sender=SUCCES&phone=7338881299&text=Test%20SMS&priority=ndnd&stype=normal
		        //Send SMS API
		        String mainUrl="http://bhashsms.com/api/sendmsg.php?";
		        //Prepare parameter string
		        StringBuilder sbPostData= new StringBuilder(mainUrl);
		        sbPostData.append("user="+userName);
		        sbPostData.append("&pass="+password);
		        sbPostData.append("&sender="+accSid);
		        sbPostData.append("&phone="+cellNumber);
		        sbPostData.append("&text="+encoded_message);
		        sbPostData.append("&priority="+"ndnd");
		        sbPostData.append("&stype="+"normal");
		        //final string
		        mainUrl = sbPostData.toString();
		        
		        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
		        
		        try
		        {
		            //prepare connection
		            myURL = new URL(mainUrl);
		            myURLConnection = myURL.openConnection();
		            myURLConnection.connect();
		            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		            //reading response
		            String response;
		            while ((response = reader.readLine()) != null)
		            //print response
		            System.out.println(response);

		            //finally close connection
		            reader.close();
		        }
		        catch (IOException e)
		        {
		    		logger.log(Level.SEVERE,"Hi Inside Catch Exception"+e);

		                e.printStackTrace();
		        }
				System.out.println(" sms sent successfully");
				logger.log(Level.SEVERE," sms sent successfully");
				
				/**
				 * end here
				 */
				
			}else{
				
		        
		        //Prepare Url
		        URLConnection myURLConnection=null;
		        URL myURL=null;
		        BufferedReader reader=null;

		        //encoding message
		        String encoded_message=URLEncoder.encode(message);

		        //Send SMS API
//		        String mainUrl="http://103.242.119.152/vendorsms/pushsms.aspx?";
		        /**
		         * Date 18-03-2019 by Vijay updated with new API link for bhavana telecoms
		         */
		        String mainUrl="http://smpp.keepintouch.co.in/vendorsms/pushsms.aspx?";
		        
		        //Prepare parameter string
		        StringBuilder sbPostData= new StringBuilder(mainUrl);
		        sbPostData.append("user="+userName);
		        sbPostData.append("&password="+password);
		        sbPostData.append("&msisdn="+mobiles);
		        sbPostData.append("&sid="+accSid);
		        sbPostData.append("&msg="+encoded_message);
		        sbPostData.append("&fl="+"0");
		        sbPostData.append("&gwid="+"2");
		        //final string
		        mainUrl = sbPostData.toString();
		        
		        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
		        
		        try
		        {
		            //prepare connection
		            myURL = new URL(mainUrl);
		            myURLConnection = myURL.openConnection();
		            myURLConnection.connect();
		            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
		            //reading response
		            String response;
		            while ((response = reader.readLine()) != null)
		            //print response
		            System.out.println(response);

		            //finally close connection
		            reader.close();
		        }
		        catch (IOException e)
		        {
		    		logger.log(Level.SEVERE,"Hi Inside Catch Exception");

		                e.printStackTrace();
		        }
				System.out.println(" sms sent successfully");
				logger.log(Level.SEVERE," sms sent successfully");
				
				 }
				
				/**
				 * end here
				 */
			}
			
			
			
			
			return 1;
		}



		
		
		/**
		 * Updated By: Viraj
		 * Date: 12-04-2019
		 * Description: changed lead sms send method 
		 */
		@Override
		public ArrayList<Integer> checkSMSConfigAndSendSMS(long companyId,long cellNumber,long leadId,
				String companyName, int customerId) {
			logger.log(Level.SEVERE,"Inside Lead SMS send");
			ArrayList<Integer> intgerlist = new ArrayList<Integer>();

//				SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status",true).filter("companyId", companyId).first().now();
//				if(smsConfig==null){
//					intgerlist.add(-1);
//					return intgerlist;
//				}
//				if(smsConfig!=null){
//					if(smsConfig.getStatus()==false){
//						intgerlist.add(2);
//						return intgerlist;
//					}
//					String senderId = smsConfig.getAccountSID();
//					String userName = smsConfig.getAuthToken();
//					String password = smsConfig.getPassword();
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- added below method to check customer DND status if customer DND status is Active 
					 * then SMS will not send to customer
					 */
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerId, companyId);
					if(!dndstatusFlag){
						
					SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", "Lead Enquiry").first().now();
					if(smsTemplate==null){
						intgerlist.add(3);
						return intgerlist;
					}else{
						if(smsTemplate.getStatus()==false){
							System.out.println("888");
							intgerlist.add(4);
							return intgerlist;
						}else{
//							String prodName ="";
//							for(int i=0;i<productslist.size();i++){
//								if(productslist.size()==1){
//									prodName+=productslist.get(i).getProductName();
//								}else{
//									prodName+=productslist.get(i).getProductName()+",";
//								}
//								
//							}
							String templateMsgwithbraces = smsTemplate.getMessage();
//							String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
//							String productName = cutomerName.replace("{ProductName}", prodName);
//							String netpayable = productName.replace("{NetPayable}", netPayable+"");
//							String signgleServiceAmt = netpayable.replace("{SingleSerAmt}", singleSerAmt);
							String compName = templateMsgwithbraces.replace("{companyName}", companyName);
							String actaulMsg = compName.replace("{leadId}", leadId+"");
							System.out.println("value");
							logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
//							int value =  sendSmsToClient(actaulMsg, cellNumber, senderId,  userName,  password, companyId);
//							System.out.println(" Valuee==="+value);
//							intgerlist.add(value);
							
							/**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							sendMessage(AppConstants.SERVICEMODULE,AppConstants.LEAD,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actaulMsg,cellNumber,false);
							logger.log(Level.SEVERE,"after sendMessage method");
							intgerlist.add(1);
							
						}
					}
					}
					else{
						intgerlist.add(5);
						return intgerlist;
					}
//				}
			
			
			return intgerlist;
		}
		/** Ends **/
		

		/**
		 * Date 16-03-2017
		 * added by vijay
		 * checking SMS configuration and SMS template is Active then
		 *  Sending SMS for Contract Cancellation 
		 * requirement from Petra pest
		 */
		@Override
		public ArrayList<Integer> checkSMSConfigAndSendContractCancellationSMS(long companyId, String customerName, long custCellNo,
				int contractId, String companyName, boolean bhashSMSAPIFlag,int customerId) {

			
			logger.log(Level.SEVERE,"Inside contract cancellation SMS send");
			ArrayList<Integer> intgerlist = new ArrayList<Integer>();

			try {
				
//				SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status", true).filter("companyId", companyId).first().now();
//				
//				if(smsConfig==null){
//					intgerlist.add(-1);
//					return intgerlist;
//				}
//				
//				if(smsConfig!=null){
//					if(smsConfig.getStatus()==false){
//						intgerlist.add(-2);
//						return intgerlist;
//					}
//					String senderId = smsConfig.getAccountSID();
//					String userName = smsConfig.getAuthToken();
//					String password = smsConfig.getPassword();
					
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- added below method to check customer DND status if customer DND status is Active 
					 * then SMS will not send to customer
					 */
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerId, companyId);
					if(!dndstatusFlag){
						
					SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", "Contract Cancellation").first().now();
					if(smsTemplate==null){
						intgerlist.add(-3);
						return intgerlist;
					}
					if (smsTemplate != null) {
						if (smsTemplate.getStatus() == false) {
							intgerlist.add(-4);
							return intgerlist;
						} else {
							
							String templateMsgwithbraces = smsTemplate.getMessage();
							String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
							String contractid = cutomerName.replace("{ContractId}", contractId+"");
							String actaulMsg = contractid.replace("{CompanyName}", companyName);
							System.out.println("actaulMsg"+actaulMsg);
							logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
//							int value =  sendSmsToClient(actaulMsg, custCellNo, senderId,  userName,  password, companyId);
//							System.out.println(" Valuee==="+value);
//							intgerlist.add(value);
		
							/**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							sendMessage(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actaulMsg,custCellNo,false);
							logger.log(Level.SEVERE,"after sendMessage method");
							intgerlist.add(1);
							
						}
		
					}
				  }
//				}
					
			} catch (Exception e) {
				// TODO: handle exception
			}
			
				
			
			
			return intgerlist;
		}
		/**
		 * ends here
		 */

		/**
		 * Date 17-03-2017
		 * added by vijay
		 * for sending sms to customer for complaint raised requirement from Petra pest
		 */

		@Override
		public ArrayList<Integer> checkSMSConfigAndSendComplaintRegisterSMS(long companyId, String serviceid, String customerName,
				int complaintId , String companyName,long custCellNo,int customerId) {
			/**
			 * @author Anil @since 02-09-2021
			 * If customer branch is selected in complaint then sms should be sent to customer branch poc
			 * Raised by Ashwini Bhagwat for Pecopp
			 */
			logger.log(Level.SEVERE,"Customer Cell No. "+custCellNo);
			Complain complaintEntity = ofy().load().type(Complain.class).filter("companyId", companyId).filter("count", complaintId).first().now();
			
			if(complaintEntity!=null&&complaintEntity.getCustomerBranch()!=null&&!complaintEntity.getCustomerBranch().equals("")){
				CustomerBranchDetails custBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId",companyId).filter("buisnessUnitName",complaintEntity.getCustomerBranch()).first().now();
				if(custBranch!=null){
					if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
						custCellNo=custBranch.getCellNumber1();
						logger.log(Level.SEVERE,"Customer Branch Cell No. "+custCellNo);
					}
				}
			}
			
			if(complaintEntity!=null){
				if(complaintEntity.getServiceId()!=null&&complaintEntity.getServiceId()>0){
					if(serviceid==null||serviceid.equals("")){
						serviceid=complaintEntity.getServiceId()+"";
					}
				}
				
				if(serviceid==null||serviceid.equals("")){
					serviceid=complaintEntity.getProductName();
				}
			}
			
			logger.log(Level.SEVERE,"Inside Lead SMS send");
			ArrayList<Integer> intgerlist = new ArrayList<Integer>();
				
//				SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status", true).filter("companyId", companyId).first().now();
//				
//				if(smsConfig==null){
//					intgerlist.add(-1);
//					return intgerlist;
//				}
//				
//				if(smsConfig!=null){
//					if(smsConfig.getStatus()==false){
//						intgerlist.add(-2);
//						return intgerlist;
//					}
					
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- added below method to check customer DND status if customer DND status is Active 
					 * then SMS will not send to customer
					 */
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerId, companyId);
					if(!dndstatusFlag){
						
//					String senderId = smsConfig.getAccountSID();
//					String userName = smsConfig.getAuthToken();
//					String password = smsConfig.getPassword();
					
					SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", "Complaint Created").first().now();
					if(smsTemplate==null){
						intgerlist.add(-3);
						return intgerlist;
					}
					if (smsTemplate != null) {
						if (smsTemplate.getStatus() == false) {
							intgerlist.add(-4);
							return intgerlist;
						} else {
							
							String templateMsgwithbraces = smsTemplate.getMessage();
							
							/**
							 * Date 29 jun 2017 added by vijay this is for Eco friendly	
							 */
							if(EnableSMSAPIBHASH){
								customerName = getCustomerName(customerName,companyId);

							}
							/**
							 * ends here
							 */
							
							String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
							String serviceId = cutomerName.replace("{ServiceID}", serviceid+"");
							String complaintid = serviceId.replace("{ComplaintID}", complaintId+"");
							String actaulMsg = complaintid.replace("{CompanyName}", companyName);
							System.out.println("actaulMsg"+actaulMsg);
							logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
//							int value =  sendSmsToClient(actaulMsg, custCellNo, senderId,  userName,  password,companyId);
//							System.out.println(" Valuee==="+value);
//							intgerlist.add(value);
		
							/**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							sendMessage(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSUPPORT,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actaulMsg,custCellNo,false);
							logger.log(Level.SEVERE,"after sendMessage method");
							intgerlist.add(1);
							
						}
		
					}
					
					}
					else{
						intgerlist.add(-5);
						return intgerlist;
					}
//				}
			
			return intgerlist;
		}

		/**
		 * ends here
		 */


		
		/**
		 * Date 17-03-2017
		 * added by vijay
		 * for sending sms to customer for complaint Resolved (complated) requirement from Petra pest
		 */
		
		@Override
		public ArrayList<Integer> checkSMSConfigAndSendComplaintResolvedSMS(long companyId, Date serviceDate, String customerName,
				int complaintId, String companyName, long custCellNo, int customerId) {
			

			/**
			 * @author Anil @since 02-09-2021
			 * If service is of type complaint then we will not send service completion sms 
			 * will send sms on complaint completion
			 * Requirement raise by Rutuza, Vaishnavi and Nitin sir for Universal Pest Control
			 */
			logger.log(Level.SEVERE,"Customer Cell No. "+custCellNo);
			Complain complaintEntity = ofy().load().type(Complain.class).filter("companyId", companyId).filter("count", complaintId).first().now();
			String smsEventName="Complaint Completed";
			String complaintClosingDate="";
			Service service = ofy().load().type(Service.class).filter("companyId",companyId).filter("ticketNumber",complaintId).first().now();
			if(service!=null&&!service.getServiceType().equals("")&&service.getServiceType().equals("Complaint Service")){
				smsEventName="Complaint Service Completed";
			}
			if(complaintEntity!=null&&complaintEntity.getCompletionDate()!=null){
				complaintClosingDate=fmt.format(complaintEntity.getCompletionDate());
			}
			if(complaintEntity!=null&&complaintEntity.getCustomerBranch()!=null&&!complaintEntity.getCustomerBranch().equals("")){
				CustomerBranchDetails custBranch=ofy().load().type(CustomerBranchDetails.class).filter("companyId",companyId).filter("buisnessUnitName",complaintEntity.getCustomerBranch()).first().now();
				if(custBranch!=null){
					if(custBranch.getCellNumber1()!=null&&custBranch.getCellNumber1()!=0){
						custCellNo=custBranch.getCellNumber1();
						logger.log(Level.SEVERE,"Customer Branch Cell No. "+custCellNo);
					}
				}
			}
			
			
			logger.log(Level.SEVERE,"Inside Complaint SMS evnt - "+smsEventName+" / "+complaintClosingDate);
			ArrayList<Integer> intgerlist = new ArrayList<Integer>();
			
			
//				SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status", true).filter("companyId", companyId).first().now();
//				
//				if(smsConfig!=null){
//					if(smsConfig.getStatus()==false){
//						intgerlist.add(-1);
//						return intgerlist;
//					}
//					String senderId = smsConfig.getAccountSID();
//					String userName = smsConfig.getAuthToken();
//					String password = smsConfig.getPassword();
					
					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- added below method to check customer DND status if customer DND status is Active 
					 * then SMS will not send to customer
					 */
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerId, companyId);
					if(!dndstatusFlag){
					
					SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", smsEventName).filter("status", true).first().now();
					if(smsTemplate==null){
						intgerlist.add(-2);
						return intgerlist;
					}
					if (smsTemplate != null) {
						if (smsTemplate.getStatus() == false) {
							intgerlist.add(-3);
							return intgerlist;
						} else {
							
							
							String templateMsgwithbraces = smsTemplate.getMessage();
							System.out.println("SMS==="+templateMsgwithbraces);
							
							/**
							 * Date 29 jun 2017 added by vijay this is for Eco friendly	
							 */
							if(EnableSMSAPIBHASH){
								customerName = getCustomerName(customerName,companyId);
							}
							/**
							 * ends here
							 */
							
							String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
							String complaintid = cutomerName.replace("{ComplaintID}", complaintId+"");
							String servicedate = complaintid.replace("{ServiceDate}", fmt.format(serviceDate));
							String actualMsg = servicedate.replace("{CompanyName}", companyName);
							
							actualMsg = actualMsg.replace("{ComplaintClosureDate}", complaintClosingDate);
							System.out.println("actaulMsg"+actualMsg);
							
							if(smsEventName.equals("Complaint Service Completed")){
								Company company = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
								
								if(company!=null&&company.getFeedbackUrlList()!=null&&company.getFeedbackUrlList().size()!=0){
									String feedbackFormUrl="";
									Customer customer=ofy().load().type(Customer.class).filter("companyId",service.getCompanyId()).filter("count",service.getPersonInfo().getCount()).first().now();
									if(customer!=null){
										if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
											for(FeedbackUrlAsPerCustomerCategory feedback:company.getFeedbackUrlList()){
												if(feedback.getCustomerCategory().equals(customer.getCategory())){
													feedbackFormUrl=feedback.getFeedbackUrl();
													break;
												}
											}
										}
									}
									if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
										logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
										StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
								        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
								        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
								        sbPostData.append("&serviceId="+service.getCount());
								        sbPostData.append("&serviceDate="+fmt.format(serviceDate));
								        sbPostData.append("&serviceName="+service.getProductName());
								        sbPostData.append("&technicianName="+service.getEmployee());
								        sbPostData.append("&branch="+service.getBranch());
								        feedbackFormUrl = sbPostData.toString();
								        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
								        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
								        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,company.getCompanyId());
								        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
										actualMsg=actualMsg.replace("{Link}", feedbackFormUrl);
									}else{
										actualMsg=actualMsg.replace(" Please share your feedback by clicking on this link {Link} -", "-");
										actualMsg=actualMsg.trim();
									}
								}
							}
							
							logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
//							int value =  sendSmsToClient(actualMsg, custCellNo, senderId,  userName,  password, companyId);
//							System.out.println(" Valuee==="+value);
//							intgerlist.add(value);
							
							
							/**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							sendMessage(AppConstants.SERVICEMODULE,AppConstants.CUSTOMERSUPPORT,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actualMsg,custCellNo,false);
							logger.log(Level.SEVERE,"after sendMessage method");
							intgerlist.add(1);
						}
		
					}
					
					}
					else{
							intgerlist.add(-5);
							return intgerlist;
					}
//				}
			
			return intgerlist;
		}
		
		/**
		 * ends here
		 */

		/**
		 * Date 29 jun 2017 added by vijay for gtetting customer correspondence Name or full name
		 * @param customerName
		 * @param companyId
		 * @return
		 */
		
		private String getCustomerName(String customerName, long companyId) {
			
			String custName;
			
			Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("fullname", customerName).first().now();
			
			if(customer!=null){
				
				if(!customer.getCustPrintableName().equals("")){
					custName = customer.getCustPrintableName();
				}else{
					custName = customer.getFullname();
				}
				
			}else{
				custName = customerName;
			}
			
			
			return custName;
		}
		
		/**
		 * ends here
		 */
		
		/**
		 * Rahul Verma added this for technician app
		 * @param technicianCellNo 
		 * @param customerId 
		 */
		public String sendSMSFromTechnician(Company comp,long companyId,String event,String customerName,String technicianName,String serviceDate,long customerCell,String productNam, long technicianCellNo, int customerId){
			
			logger.log(Level.SEVERE,"Inside sendSMSFromTechnicianOnStarted SMS send");
			
				SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status", true).filter("companyId", companyId).first().now();
				
//				if(smsConfig!=null){
//					if(smsConfig.getStatus()==false){
//						return "SMS Config Not Active";
//					}
					

					/**
					 * @author Vijay Date :- 09-08-2021
					 * Des :- added below method to check customer DND status if customer DND status is Active 
					 * then SMS will not send to customer
					 */
					ServerAppUtility serverapputility = new ServerAppUtility();
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerId, companyId);
					if(!dndstatusFlag){
						
//					String senderId = smsConfig.getAccountSID();
//					String userName = smsConfig.getAuthToken();
//					String password = smsConfig.getPassword();
					
					SmsTemplate smsTemplate = ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("event", event).first().now();
					if(smsTemplate==null){
						return "SMS Template not found!";
					}
					if (smsTemplate != null) {
						if (smsTemplate.getStatus() == false) {
							return "SMS Template not Active!!";
						} else {
							
							String templateMsgwithbraces = smsTemplate.getMessage();
							System.out.println("SMS==="+templateMsgwithbraces);
							
							/**
							 * Date 29 jun 2017 added by vijay this is for Eco friendly	
							 */
//							if(bhashSMSAPIFlag){
								customerName = getCustomerName(customerName,companyId);
//							}
							/**
							 * ends here
							 */
							if(event.equalsIgnoreCase("Technician Started")){
								String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
								String technician = cutomerName.replace("{TechnicianName}", technicianName);
								String servicedate = technician.replace("{ServiceDate}", serviceDate);
								String productName = servicedate.replace("{ProductName}", productNam);
								String actaulMsg = productName.replace("{CompanyName}", comp.getBusinessUnitName());
								System.out.println("actaulMsg"+actaulMsg);
								
								/**
								 * @author Anil @since 10-03-2021
								 * adding key field technician mobile number
								 */
								if(technicianCellNo!=0){
									actaulMsg = actaulMsg.replace("{MobileNumber}", technicianCellNo+"");
								}
								
								logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
//								int value =  sendSmsToClient(actaulMsg, customerCell, senderId,  userName,  password, companyId);
//								System.out.println(" Valuee==="+value);
//								if(value==1){
//									return "SMS sent successfully!!";
//								}
								
								/**
								 * @author Vijay Date :- 29-04-2022
								 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
								 * whats and sms based on communication channel configuration.
								 */
								sendMessage(AppConstants.PEDIO,AppConstants.PEDIO,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actaulMsg,customerCell,false);
								logger.log(Level.SEVERE,"after sendMessage method");
								return "Message Sent Successfully";
								
							}else if(event.equalsIgnoreCase("Technician Reported")){

								String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
								String technician = cutomerName.replace("{TechnicianName}", technicianName);
								String productName = technician.replace("{ProductName}", productNam);
								String actaulMsg = productName.replace("{CompanyName}", comp.getBusinessUnitName());
								System.out.println("actaulMsg"+actaulMsg);
								
								/**
								 * @author Anil @since 10-03-2021
								 * adding key field technician mobile number
								 */
								if(technicianCellNo!=0){
									actaulMsg = actaulMsg.replace("{MobileNumber}", technicianCellNo+"");
								}
								
								logger.log(Level.SEVERE,"Calling SMS API method to send SMS");
//								int value =  sendSmsToClient(actaulMsg, customerCell, senderId,  userName,  password, companyId);
//								System.out.println(" Valuee==="+value);
//								if(value==1){
//									return "SMS sent successfully!!";
//								}

								/**
								 * @author Vijay Date :- 29-04-2022
								 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
								 * whats and sms based on communication channel configuration.
								 */
								sendMessage(AppConstants.PEDIO,AppConstants.PEDIO,smsTemplate.getEvent(),smsTemplate.getCompanyId(),actaulMsg,customerCell,false);
								logger.log(Level.SEVERE,"after sendMessage method");
								return "Message Sent Successfully";
								
							}
		
						}
		
					}
				  }
				else{
					return "Can not send SMS customer DND status is active";
	
				}
					
//				}
			
			return "Failed to send sms!!";
		}



			@Override
			public String validateAndSendSMS(SMSDetails smsDetails) {
			logger.log(Level.SEVERE,"inside validateAndSendSMS method ");
			SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
//			SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("status", true).filter("companyId",  smsDetails.getModel().getCompanyId()).first().now();
			
			Company companyEntity = ofy().load().type(Company.class).filter("companyId", smsDetails.getModel().getCompanyId()).first().now();
			
			/**
			 * @author Vijay Date :- 09-08-2021
			 * Des :- added below method to check customer DND status if customer DND status is Active 
			 * then SMS will not send to customer
			 */
			if(smsDetails.getCommunicationChannel().equals(AppConstants.SMS)){
				try {
					ServerAppUtility serverapputility = new ServerAppUtility();
					long customerCellNumber = Long.parseLong(smsDetails.getMobileNumber().trim());
					boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(customerCellNumber,smsDetails.getModel().getCompanyId(),true);
					
					if(!dndstatusFlag){
						return "Can not send SMS customer DND status is active";
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			
			
			boolean paymentGatewayRequestLinkFlag = false;
			
			SmsServiceImpl smsimpl = new SmsServiceImpl();
			String companyName = companyEntity.getBusinessUnitName();
			String actualMessage = "";
			CommonServiceImpl commonserviceimpl = new CommonServiceImpl();
			String jsonData ="";

			try {
					if(smsDetails.getSmsTemplate()!=null ) {
						if( smsDetails.getSmsTemplate().getEvent()!=null && smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.LEADPAYMENTGATEWAY) || 
								smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.QUOTATIONPAYMENTGATEWAY) || 
								smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTPAYMENTGATEWAY) ||
								smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTRENEWALPAYMENTGATEWAY) ||
								smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.INVOICEPAYMENTGATEWAY)
								){
							paymentGatewayRequestLinkFlag = true;
						}
					}
					
					if(paymentGatewayRequestLinkFlag){
						String str = commonserviceimpl.validatePaymentDocument(smsDetails.getEntityName(),smsDetails.getModel().getCount(),smsDetails.getModel().getCompanyId());
						if(str!=null && !str.equals("")){
							return str;
						}
					}
					
					EnableSMSAPIBHASH=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "EnableSMSAPIBHASH", smsDetails.getModel().getCompanyId());

//					if(smsDetails.getSmsTemplate().getEvent().trim().equals("PDF Link")){
//						String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
//						String companyCellNumber = "";
//						if(companyEntity.getCellNumber1()!=0){
//							companyCellNumber = companyEntity.getCellNumber1()+"";
//						}
//					
//						String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL());
//						String documentName = getDocumentName(smsDetails.getEntityName());
//						
//						String custName = templateMsgWithBraces.replace("{Document Type}", documentName);
//						String tinyurl = ServerAppUtility.getTinyUrl(pdflink);
//						String strtinyurl = custName.replace("{PdfLink}", tinyurl);
//						String companyCellno = strtinyurl.replace("{CompanyMobile}", companyCellNumber);
//						actualMessage = companyCellno.replace("{CompanyName}", companyName);
//						logger.log(Level.SEVERE, "actualMessage" + actualMessage);
//
//						logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//						String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), smsConfig.getAccountSID(), smsConfig.getAuthToken(), smsConfig.getPassword(), smsConfig.getCompanyId(), smsConfig.getSmsAPIUrl());
//						logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//						return "true";
//					}
					
					} catch (Exception e) {
						e.printStackTrace();
					}
				
//						if(smsDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP) && smsDetails.getSmsTemplate()==null) {
//								actualMessage = smsDetails.getMessage();
//								logger.log(Level.SEVERE,"send message on whats app from popup"+smsDetails.getMobileNumber());
//								String response = sendMessageOnWhatsApp(companyEntity.getCompanyId(),smsDetails.getMobileNumber(),actualMessage);
//								logger.log(Level.SEVERE,"response "+response);
//								return response;
//						}	
							
					
						if(smsDetails.getSmsTemplate()==null ) {
							if(smsDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)) {
								actualMessage = smsDetails.getMessage();
								return sendMessageOnWhatsApp(companyEntity.getCompanyId(), smsDetails.getMobileNumber(), actualMessage);
							}
						}
						
			
						if(smsDetails.getEntityName().trim().equals("Lead")){
							Lead lead = (Lead) smsDetails.getModel();
							
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.LEADPAYMENTGATEWAY)){
								String paymentGatewayUniqueId ="";

								Lead leadEntity = ofy().load().type(Lead.class).filter("companyId",smsDetails.getModel().getCompanyId()).filter("count", lead.getCount())
										.first().now();
							
								if(leadEntity!=null && (leadEntity.getNumberRange()==null || leadEntity.getNumberRange().equals(""))){
//									return commonserviceimpl.validateNumberRange(leadEntity.getCompanyId(),AppConstants.LEAD);
									String numberRangeValidation = commonserviceimpl.validateNumberRange(smsDetails.getModel().getCompanyId(),AppConstants.LEAD);
									if(!numberRangeValidation.equals("")){
										return numberRangeValidation;
									}
								}
								
								if(leadEntity!=null && leadEntity.getPaymentGatewayUniqueId()!=null && !leadEntity.getPaymentGatewayUniqueId().equals("")){
									paymentGatewayUniqueId = leadEntity.getPaymentGatewayUniqueId();
								}
								else{
									Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
														.filter("count", lead.getPersonInfo().getCount()).first().now();
									jsonData = commonserviceimpl.createJsonData(leadEntity,AppConstants.LEAD, smsDetails.getAppURL(), smsDetails.getAppId(), companyEntity, customer,"",smsDetails.getLandingPageText());
									
									
									paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, smsDetails.getModel(), smsDetails.getEntityName());

								}
								
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String paymentGatewaylink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
								logger.log(Level.SEVERE, "paymentGatewaylink" + paymentGatewaylink);

								String custName = templateMsgWithBraces.replace("{CustomerName}", leadEntity.getPersonInfo().getFullName());
								String tinyurl = ServerAppUtility.getTinyUrl(paymentGatewaylink,lead.getCompanyId());
								String compName=custName.replace("{ShortLink}", tinyurl);
								actualMessage = compName.replace("{CompanyName}", companyName); //Ashwini Patil
								
								
								logger.log(Level.SEVERE, "tinyurl" + tinyurl);
								logger.log(Level.SEVERE, AppConstants.LEADPAYMENTGATEWAY+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel() );
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.LEADENQUIRY)){
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String compName = templateMsgwithbraces.replace("{CompanyName}", companyName); //Ashwini Patil made C capital
								actualMessage = compName.replace("{leadId}", smsDetails.getModel().getCount()+"");
								logger.log(Level.SEVERE, AppConstants.LEADENQUIRY+" actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
							}else if(smsDetails.getSmsTemplate().getEvent().trim().equals("Old Salesperson")){
								
								Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
										   .filter("count", lead.getPersonInfo().getCount()).first().now();
									
									String companyCellNumber = "";
									if(customer.getCellNumber1()!=0){
										companyCellNumber = customer.getCellNumber1()+"";
									}
									
									String custName = "";
									if(customer.getCompanyName()!=null){
										custName = customer.getCompanyName()+"";
									}
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String Leadid =templateMsgwithbraces.replace("{leadId}", smsDetails.getModel().getCount()+"");
								String compName  = Leadid.replace("{CustomerName}", custName);
								String cellNo =compName.replace("{CellNo}", companyCellNumber);
								actualMessage=cellNo.replace("{CompanyName}", companyName); //Ashwini Patil
								logger.log(Level.SEVERE, "Old Salesperson actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
							}else if(smsDetails.getSmsTemplate().getEvent().trim().equals("New Lead")){
								Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
										   .filter("count", lead.getPersonInfo().getCount()).first().now();
									
									String companyCellNumber = "";
									if(customer.getCellNumber1()!=0){
										companyCellNumber = customer.getCellNumber1()+"";
									}
									
									String custName = "";
									if(customer.getCompanyName()!=null){
										custName = customer.getCompanyName()+"";
									}
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String Leadid =templateMsgwithbraces.replace("{leadId}", smsDetails.getModel().getCount()+"");
								String compName  = Leadid.replace("{CustomerName}", custName);
								String cellNo =compName.replace("{CellNo}", companyCellNumber);
								actualMessage =cellNo.replace("{CompanyName}", companyName); //Ashwini Patil
								logger.log(Level.SEVERE, "New Lead actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actaulMsg, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
							}else if(smsDetails.getSmsTemplate().getEvent().trim().equals("Salesperson Changed")){
								Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
										   .filter("count", lead.getPersonInfo().getCount()).first().now();
									
									String companyCellNumber = "";
									if(customer.getCellNumber1()!=0){
										companyCellNumber = customer.getCellNumber1()+"";
									}
									
									String custName = "";
									if(customer.getCompanyName()!=null){
										custName = customer.getCompanyName()+"";
									}
								
								
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String Leadid =templateMsgwithbraces.replace("{leadId}", smsDetails.getModel().getCount()+"");
								String compName  = Leadid.replace("{CustomerName}", custName);
								String cellNo =compName.replace("{CellNo}", companyCellNumber);
								actualMessage =cellNo.replace("{CompanyName}", companyName); //Ashwini Patil
								logger.log(Level.SEVERE, "Salesperson Changed actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actaulMsg, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
								//Lead Unsuccessful
							}else if(smsDetails.getSmsTemplate().getEvent().trim().equals("Lead Unsuccessful")){
								Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
										   .filter("count", lead.getPersonInfo().getCount()).first().now();
									
									String companyCellNumber = "";
									if(customer.getCellNumber1()!=0){
										companyCellNumber = customer.getCellNumber1()+"";
									}
									
									String custName = "";
									if(customer.getCompanyName()!=null){
										custName = customer.getCompanyName()+"";
									}
								
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String Leadid =templateMsgwithbraces.replace("{leadId}", smsDetails.getModel().getCount()+"");
								String compName  = Leadid.replace("{CustomerName}", custName);
								String cellNo =compName.replace("{CellNo}", companyCellNumber);
								actualMessage=cellNo.replace("{CompanyName}", companyName); //Ashwini Patil
								logger.log(Level.SEVERE, "Lead Unsuccessful actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actaulMsg, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
								//Lead Unsuccessful
							}else if(smsDetails.getSmsTemplate().getEvent().trim().equals("Lead Successful")){
								Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
										   .filter("count", lead.getPersonInfo().getCount()).first().now();
									
									String companyCellNumber = "";
									if(customer.getCellNumber1()!=0){
										companyCellNumber = customer.getCellNumber1()+"";
									}
									
									String custName = "";
									if(customer.getCompanyName()!=null){
										custName = customer.getCompanyName()+"";
									}
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String Leadid =templateMsgwithbraces.replace("{leadId}", smsDetails.getModel().getCount()+"");
								String compName  = Leadid.replace("{CustomerName}", custName);
								String cellNo =compName.replace("{CellNo}", companyCellNumber);
								actualMessage=cellNo.replace("{CompanyName}", companyName); //Ashwini Patil
								logger.log(Level.SEVERE, "Lead Successful actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
								//Lead Unsuccessful
							}else if(smsDetails.getSmsTemplate().getEvent().trim().equals("Lead Cancelled")){
								Customer customer = ofy().load().type(Customer.class).filter("companyId", lead.getCompanyId())
										   .filter("count", lead.getPersonInfo().getCount()).first().now();
									
									String companyCellNumber = "";
									if(customer.getCellNumber1()!=0){
										companyCellNumber = customer.getCellNumber1()+"";
									}
									
									String custName = "";
									if(customer.getCompanyName()!=null){
										custName = customer.getCompanyName()+"";
									}
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String Leadid =templateMsgwithbraces.replace("{leadId}", smsDetails.getModel().getCount()+"");
								String compName  = Leadid.replace("{CustomerName}", custName);
								String cellNo =compName.replace("{CellNo}", companyCellNumber);
								actualMessage=cellNo.replace("{CompanyName}", companyName); //Ashwini Patil
								logger.log(Level.SEVERE, "Lead Cancelled actualMessage" + actualMessage);

//								String smsresponseMessage = sendSmsToClient(actaulMsg, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage"+smsresponseMessage);
//								return smsresponseMessage;
								//Lead Unsuccessful
							}//Lead Cancelled
							
//							logger.log(Level.SEVERE, "Lead actualMessage" + actualMessage);
//							if(actualMessage!=null && !actualMessage.equals("")) {
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
//							}

							
						}
						else if(smsDetails.getEntityName().trim().equals("Quotation")){
							String paymentGatewayUniqueId ="";

							Quotation quotation = (Quotation) smsDetails.getModel();
							
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(quotation.getCinfo().getCount(), quotation.getCompanyId());
//							if(dndstatusFlag){
//								return "Can not send SMS customer DND status is active";
//							}
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.QUOTATIONPAYMENTGATEWAY)){
								logger.log(Level.SEVERE, "Quoation Payment Gateway SMS");

								Quotation quotationEntity = ofy().load().type(Quotation.class).filter("companyId",quotation.getCompanyId()).filter("count", quotation.getCount())
										.first().now();
								if(quotationEntity!=null && (quotationEntity.getQuotationNumberRange()==null || quotationEntity.getQuotationNumberRange().equals(""))){
//									return commonserviceimpl.validateNumberRange(quotationEntity.getCompanyId(),AppConstants.QUOTATION);
									String numberRangeValidation = commonserviceimpl.validateNumberRange(smsDetails.getModel().getCompanyId(),AppConstants.QUOTATION);
									if(!numberRangeValidation.equals("")){
										return numberRangeValidation;
									}
								}
								if(quotationEntity!=null && quotationEntity.getPaymentGatewayUniqueId()!=null && !quotationEntity.getPaymentGatewayUniqueId().equals("")){
									paymentGatewayUniqueId = quotationEntity.getPaymentGatewayUniqueId();
								}
								else{
									String pdflink = commonserviceimpl.getPDFURL(AppConstants.QUOTATION, quotationEntity, quotationEntity.getCount(), quotationEntity.getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									System.out.println("PDF Link "+pdflink);
									Customer customer = ofy().load().type(Customer.class).filter("companyId", quotationEntity.getCompanyId())
														.filter("count", quotationEntity.getCinfo().getCount()).first().now();
									jsonData = commonserviceimpl.createJsonData(smsDetails.getModel(),AppConstants.QUOTATION, smsDetails.getAppURL(), smsDetails.getAppId(), companyEntity, customer,pdflink,smsDetails.getLandingPageText());
								
//									String error ="";
//									error = commonserviceimpl.validateInternetNumberRange(smsDetails.getModel().getCompanyId());
//									if(!error.trim().equals("") && error.trim().length()>0){
//										return error;
//									}
									
									paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, smsDetails.getModel(), smsDetails.getEntityName());

								}
								
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String paymentGatewaylink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
								logger.log(Level.SEVERE, "paymentGatewaylink" + paymentGatewaylink);

								String custName = templateMsgWithBraces.replace("{CompanyName}", companyName);
								String tinyurl = ServerAppUtility.getTinyUrl(paymentGatewaylink,quotationEntity.getCompanyId());
								actualMessage = custName.replace("{LandingPageLink}", tinyurl);
								
								logger.log(Level.SEVERE, "tinyurl" + tinyurl);
								logger.log(Level.SEVERE, AppConstants.QUOTATIONPAYMENTGATEWAY+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;

							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.QUOTATIONPDFLINK)){
								
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String companyCellNumber = "";
								if(companyEntity.getCellNumber1()!=0){
									companyCellNumber = companyEntity.getCellNumber1()+"";
								}
							
								String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
//								String documentName = getDocumentName(smsDetails.getEntityName());
								
								logger.log(Level.SEVERE, "	Quotation PDF link  == " + pdflink);
								

//								String[] pdflinks = pdflink.split("&");
//
								String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
//								String tinyurl = ServerAppUtility.getTinyUrl(pdflinks[0]);

//
//								StringBuilder strbuilder = new StringBuilder();
//								try {
//									strbuilder.append(pdflinks[1]);
//									strbuilder.append(pdflinks[2]);
//									strbuilder.append(pdflinks[3]);
//								} catch (Exception e) {
//								}
//
//								logger.log(Level.SEVERE, "strbuilder" + strbuilder.toString());
//								tinyurl = tinyurl+strbuilder.toString();
								
//								tinyurl = strbuilder.toString();
//								logger.log(Level.SEVERE, "tinyurl ==" + strbuilder.toString());

								String strtinyurl = templateMsgWithBraces.replace("{QuotationPdfLink}", tinyurl);
								String companyCellno = strtinyurl.replace("{CompanyMobile}", companyCellNumber);
								actualMessage = companyCellno.replace("{CompanyName}", companyName);
								logger.log(Level.SEVERE, AppConstants.QUOTATIONPDFLINK+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.QUOTATIONAPPROVAL)){
								actualMessage = smsDetails.getSmsTemplate().getMessage();
							
								if(actualMessage.contains("{DocumentId}")){
									actualMessage = actualMessage.replace("{DocumentId}", quotation.getCount()+"");
								}
								if(actualMessage.contains("{DocumentDate}")){
									actualMessage = actualMessage.replace("{DocumentDate}", dateFormat.format(quotation.getQuotationDate()));
								}
								if(actualMessage.contains("{NetPayable}")){
									actualMessage = actualMessage.replace("{NetPayable}", quotation.getNetpayable()+"");
								}
								if(actualMessage.contains("{CustomerName}")){
									actualMessage = actualMessage.replace("{CustomerName}", quotation.getCinfo().getFullName()+"");
								}
								if(actualMessage.contains("{CompanyName}")){
									actualMessage = actualMessage.replace("{CompanyName}", companyName);
								}
								if(actualMessage.contains("{PDFLink}")){
									smsDetails.setPdfURL(commonserviceimpl.getCompleteURL(companyEntity));
									String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage = actualMessage.replace("{PDFLink}", tinyurl);
								}

							}

							
//							logger.log(Level.SEVERE, "Quotation actualMessage" + actualMessage);
//							if(actualMessage!=null && !actualMessage.equals("")) {
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
//							}
							
						}
						else if(smsDetails.getEntityName().trim().equals("Contract")){
							Contract contract = (Contract) smsDetails.getModel();
							String paymentGatewayUniqueId ="";

//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(contract.getCinfo().getCount(), contract.getCompanyId());
//							if(dndstatusFlag){
//								return "Can not send SMS customer DND status is active";
//							}
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTPAYMENTGATEWAY)){
								logger.log(Level.SEVERE, "Contract Payment Gateway SMS");

								Contract contractEntity = ofy().load().type(Contract.class).filter("companyId",contract.getCompanyId()).filter("count", contract.getCount())
										.first().now();
								if(contractEntity!=null && (contractEntity.getNumberRange()==null || contractEntity.getNumberRange().equals(""))){
//									return commonserviceimpl.validateNumberRange(contractEntity.getCompanyId(),null);
									String numberRangeValidation = commonserviceimpl.validateNumberRange(smsDetails.getModel().getCompanyId(),null);
									if(!numberRangeValidation.equals("")){
										return numberRangeValidation;
									}
								}
								if(contractEntity!=null && contractEntity.getPaymentGatewayUniqueId()!=null && !contractEntity.getPaymentGatewayUniqueId().equals("")){
									paymentGatewayUniqueId = contractEntity.getPaymentGatewayUniqueId();
								}
								else{
									String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT, contractEntity, contractEntity.getCount(), contractEntity.getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									System.out.println("PDF Link "+pdflink);
									Customer customer = ofy().load().type(Customer.class).filter("companyId", contractEntity.getCompanyId())
														.filter("count", contractEntity.getCinfo().getCount()).first().now();
									jsonData = commonserviceimpl.createJsonData(smsDetails.getModel(),AppConstants.CONTRACT, smsDetails.getAppURL(), smsDetails.getAppId(), companyEntity, customer,pdflink,smsDetails.getLandingPageText());
								
//									String error ="";
//									error = commonserviceimpl.validateInternetNumberRange(smsDetails.getModel().getCompanyId());
//									if(!error.trim().equals("") && error.trim().length()>0){
//										return error;
//									}
									
									paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, smsDetails.getModel(), smsDetails.getEntityName());

								}
								
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String paymentGatewaylink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
								logger.log(Level.SEVERE, "paymentGatewaylink" + paymentGatewaylink);

								String netpayable = templateMsgWithBraces.replace("{NetPayable}", contractEntity.getNetpayable()+"");
								String contractId = netpayable.replace("{Contract ID}", contractEntity.getCount()+"");
								String strcompanyName = contractId.replace("{CompanyName}", companyName);
								String tinyurl = ServerAppUtility.getTinyUrl(paymentGatewaylink,companyEntity.getCompanyId());
								actualMessage = strcompanyName.replace("{LandingPageLink}", tinyurl);
								
								logger.log(Level.SEVERE, "tinyurl" + tinyurl);
								logger.log(Level.SEVERE, AppConstants.CONTRACTPAYMENTGATEWAY+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTPDFLINK)){
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String companyCellNumber = "";
								if(companyEntity.getCellNumber1()!=0){
									companyCellNumber = companyEntity.getCellNumber1()+"";
								}
							
								String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
//								String documentName = getDocumentName(smsDetails.getEntityName());
								logger.log(Level.SEVERE, "Quotation pdflink " + pdflink);

								String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());

								String strtinyurl = templateMsgWithBraces.replace("{ContractPdfLink}", tinyurl);
								String companyCellno = strtinyurl.replace("{CompanyMobile}", companyCellNumber);
								actualMessage = companyCellno.replace("{CompanyName}", companyName);
								logger.log(Level.SEVERE, AppConstants.CONTRACTPDFLINK+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTAPPROVED)){
								String templatemsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								ProcessConfiguration processconfiguration = ofy().load().type(ProcessConfiguration.class).filter("processList.processName", "ContractApprovedSMS").filter("processList.processType", "SMSWithReferenceNumber").filter("processList.status", true).filter("companyId", contract.getCompanyId()).first().now();
								String fullname = contract.getCinfo().getFullName();
								String constartDate = dateFormat.format(contract.getStartDate());
								String conendDate = dateFormat.format(contract.getEndDate());
								if(EnableSMSAPIBHASH){
									fullname = getCustomerName(fullname,contract.getCompanyId());
									
									String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
									String companyname = customerName.replace("{companyName}",companyName);
									String contractStartDate = companyname.replace("{contractstartDate}", constartDate);
									actualMessage = contractStartDate.replace("{contractendDate}", conendDate);
								}
								/**
								 * ends here
								 * else for normal sms template
								 */
								else{
									
									String customerName = templatemsgwithbraces.replace("{customerName}",fullname);
									String companyname = customerName.replace("{companyName}",companyName);
									String contractId;
									if(processconfiguration!=null){
										contractId= companyname.replace("{referenceNum}", contract.getRefNo()+"");
									}else{
										contractId = companyname.replace("{contractId}", contract.getCount()+"");
									}
									String contractStartDate = contractId.replace("{contractstartDate}", constartDate);
									actualMessage = contractStartDate.replace("{contractendDate}", conendDate);
								}
								
								/**
								 * @author Vijay Date :- 29-12-2022
								 * Des :- updated code to send PDF link and other variables
								 */
								if(actualMessage.contains("{DocumentDate}")){
									actualMessage = actualMessage.replace("{DocumentDate}", dateFormat.format(contract.getContractDate()));
								}
								if(actualMessage.contains("{PDFLink}")){
									logger.log(Level.SEVERE, "commonserviceimpl.getCompleteURL(comp)" + commonserviceimpl.getCompleteURL(companyEntity));
									
									String pdflink = commonserviceimpl.getPDFURL("Contract", contract, contract.getCount(), contract.getCompanyId(), companyEntity, commonserviceimpl.getCompleteURL(companyEntity),null);
									logger.log(Level.SEVERE, "Contract pdflink " + pdflink);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage = actualMessage.replace("{PDFLink}", tinyurl);
								}
								if(actualMessage.contains("{NetPayable}")){
									actualMessage = actualMessage.replace("{NetPayable}", contract.getNetpayable()+"");
								}

								/**
								 * ends here
								 */
								
								logger.log(Level.SEVERE, AppConstants.CONTRACTAPPROVED+" actualMessage" + actualMessage);
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTCANCELLATION)) {
								
								String templatemsgwithbraces = smsDetails.getSmsTemplate().getMessage();

								String cutomerName = templatemsgwithbraces.replace("{CustomerName}", contract.getCinfo().getFullName());
								String contractid = cutomerName.replace("{ContractId}", contract.getCount()+"");
								actualMessage = contractid.replace("{CompanyName}", companyName);
								logger.log(Level.SEVERE, AppConstants.CONTRACTCANCELLATION+" actaulMsg" + actualMessage);
								
							}
							//Ashwini Patil Date:24-01-2023
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE)) {
								String custportalLink=ServerAppUtility.getCustomerPortalLink(companyEntity);
								logger.log(Level.SEVERE,"custportalLink="+custportalLink);
								if(!custportalLink.equalsIgnoreCase("failed")) {
									
								
								String acManagerName="";
								if(contract.getAccountManager()!=null)
									acManagerName=contract.getAccountManager();
								String templatemsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String customerName = templatemsgwithbraces.replace("{CustomerName}", contract.getCinfo().getFullName());
								String customerCell = customerName.replace("{CustomerMobile}", contract.getCinfo().getCellNumber()+"");
								String conId=customerCell.replace("{ContractId}", contract.getCount()+"");
								String conDate=conId.replace("{ContractDate}",dateFormat.format( contract.getContractDate()));
								String conStartdate=conDate.replace("{ContractStartDate}", dateFormat.format(contract.getStartDate()));
								String conEnddate=conStartdate.replace("{ContractEndDate}",dateFormat.format(contract.getEndDate()));
								String acManager=conEnddate.replace("{AccountManager}",acManagerName);
								String portalLink=acManager.replace("{PortalLink}",custportalLink);
								
								actualMessage = portalLink.replace("{CompanyName}", ServerAppUtility.getCompanyName(contract.getBranch(), contract.getCompanyId()));
								logger.log(Level.SEVERE, AppConstants.SHARECUSTOMERPORTALLINKTEMPLATE+" actaulMsg" + actualMessage);
								}else
								{
									logger.log(Level.SEVERE, "Failed to create customer portal link");
								}
							}
							
//							logger.log(Level.SEVERE, "Contract actualMessage" + actualMessage);
//							if(actualMessage!=null && !actualMessage.equals("")) {
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
//							}
							
						}
						else if(smsDetails.getEntityName().trim().equals(AppConstants.CONTRACT_RENEWAL)){
							ContractRenewal contractRenewal = (ContractRenewal) smsDetails.getModel();
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(contractRenewal.getCustomerId(), contractRenewal.getCompanyId());
//							if(dndstatusFlag){
//								return "Can not send SMS customer DND status is active";
//							}
							
							String paymentGatewayUniqueId ="";
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTRENEWALPAYMENTGATEWAY)){
								logger.log(Level.SEVERE, "Contract Renewal Payment Gateway SMS");

								ContractRenewal contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId",contractRenewal.getCompanyId()).filter("contractId", contractRenewal.getContractId())
										.first().now();
								if(contractRenewalEntity!=null && contractRenewalEntity.getPaymentGatewayUniqueId()!=null && !contractRenewalEntity.getPaymentGatewayUniqueId().equals("")){
									paymentGatewayUniqueId = contractRenewalEntity.getPaymentGatewayUniqueId();
								}
								else{
									String pdflink = commonserviceimpl.getPDFURL(AppConstants.CONTRACT_RENEWAL, contractRenewalEntity, contractRenewalEntity.getContractId(), contractRenewalEntity.getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									System.out.println("PDF Link "+pdflink);
									Customer customer = ofy().load().type(Customer.class).filter("companyId", contractRenewalEntity.getCompanyId())
														.filter("count", contractRenewalEntity.getCustomerId()).first().now();
									jsonData = commonserviceimpl.createJsonData(smsDetails.getModel(),AppConstants.CONTRACT_RENEWAL, smsDetails.getAppURL(), smsDetails.getAppId(), companyEntity, customer,pdflink,smsDetails.getLandingPageText());
								
//									String error ="";
//									error = commonserviceimpl.validateInternetNumberRange(smsDetails.getModel().getCompanyId(),null);
//									if(!error.trim().equals("") && error.trim().length()>0){
//										return error;
//									}
									
									paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, smsDetails.getModel(), smsDetails.getEntityName());

								}
								
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String paymentGatewaylink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
								logger.log(Level.SEVERE, "paymentGatewaylink" + paymentGatewaylink);
								String tinyurl = ServerAppUtility.getTinyUrl(paymentGatewaylink,companyEntity.getCompanyId());
								String customerName = templateMsgWithBraces.replace("{CustomerName}",contractRenewalEntity.getCustomerName());
								String landingPagelink = customerName.replace("{LandingPageLink}",tinyurl);
								actualMessage = landingPagelink.replace("{CompanyName}", companyName);
								
								logger.log(Level.SEVERE, "tinyurl" + tinyurl);
								logger.log(Level.SEVERE, AppConstants.CONTRACTRENEWALPAYMENTGATEWAY+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;

							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTRENEWALPDFLINK)){
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String companyCellNumber = "";
								if(companyEntity.getCellNumber1()!=0){
									companyCellNumber = companyEntity.getCellNumber1()+"";
								}
							
								String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
//								String documentName = getDocumentName(smsDetails.getEntityName());
								logger.log(Level.SEVERE, "ContractRenewal pdflink " + pdflink);

								String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());

								String strtinyurl = templateMsgWithBraces.replace("{ContractRenewalPdfLink}", tinyurl);
								String companyCellno = strtinyurl.replace("{CompanyMobile}", companyCellNumber);
								actualMessage = companyCellno.replace("{CompanyName}", companyName);
								logger.log(Level.SEVERE, AppConstants.CONTRACTRENEWALPDFLINK+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.CONTRACTRENEWALDUEREMINDER)){
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								System.out.println("contractRenewal.getContractId() "+contractRenewal.getContractId());
								
								String enddate = dateFormat.format(contractRenewal.getContractEndDate());

								String customerName = templateMsgWithBraces.replace("{CustomerName}",contractRenewal.getCustomerName()+"");
								String contractId = customerName.replace("{ContractId}", contractRenewal.getContractId()+"");
								String conendDate = contractId.replace("{EndDate}", enddate);
								conendDate = conendDate.replace("{endDate}", enddate);
								actualMessage = conendDate.replace("{CompanyName}", companyName);
								

								logger.log(Level.SEVERE, AppConstants.CONTRACTRENEWALDUEREMINDER+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							//Ashwini Patil Date:2-08-2024 independant of template name, tags should get replaced
							else {
							
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								System.out.println("contractRenewal.getContractId() "+contractRenewal.getContractId());
								
								String enddate = dateFormat.format(contractRenewal.getContractEndDate());

								String customerName = templateMsgWithBraces.replace("{CustomerName}",contractRenewal.getCustomerName()+"");
								String contractId = customerName.replace("{ContractId}", contractRenewal.getContractId()+"");
								String conendDate = contractId.replace("{EndDate}", enddate);
								conendDate = conendDate.replace("{endDate}", enddate);
								actualMessage = conendDate.replace("{CompanyName}", companyName);
								actualMessage = actualMessage.replace("{companyName}", companyName);
								String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
								logger.log(Level.SEVERE, "ContractRenewal pdflink " + pdflink);

								String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());

								actualMessage = actualMessage.replace("{ContractRenewalPdfLink}", tinyurl);
			
								logger.log(Level.SEVERE, AppConstants.CONTRACTRENEWALDUEREMINDER+" actualMessage" + actualMessage);

							}
							
//							logger.log(Level.SEVERE, "Contract Renewal actualMessage" + actualMessage);
//							if(actualMessage!=null && !actualMessage.equals("")) {
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
//							}
							
						}
						else if(smsDetails.getEntityName().trim().equals("Invoice")){
							Invoice invoice = (Invoice) smsDetails.getModel();
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(invoice.getPersonInfo().getCount(), invoice.getCompanyId());
//							if(dndstatusFlag){
//								return "Can not send SMS customer DND status is active";
//							}
							
							String paymentGatewayUniqueId = "";
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.INVOICEPAYMENTGATEWAY)){
								logger.log(Level.SEVERE, "Invoice Payment Gateway SMS");
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();

								Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("companyId",invoice.getCompanyId()).filter("count", invoice.getCount())
										.first().now();
								if(invoiceEntity!=null && invoiceEntity.getPaymentGatewayUniqueId()!=null && !invoiceEntity.getPaymentGatewayUniqueId().equals("")){
									paymentGatewayUniqueId = invoiceEntity.getPaymentGatewayUniqueId();
								}
								else{
									String pdflink = commonserviceimpl.getPDFURL("Invoice", invoiceEntity, invoiceEntity.getCount(), invoiceEntity.getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									System.out.println("PDF Link "+pdflink);
									Customer customer = ofy().load().type(Customer.class).filter("companyId", invoiceEntity.getCompanyId())
														.filter("count", invoiceEntity.getPersonInfo().getCount()).first().now();
									jsonData = commonserviceimpl.createJsonData(smsDetails.getModel(),"Invoice", smsDetails.getAppURL(), smsDetails.getAppId(), companyEntity, customer,pdflink,smsDetails.getLandingPageText());
								
//									String error ="";
//									error = commonserviceimpl.validateInternetNumberRange(smsDetails.getModel().getCompanyId());
//									if(!error.trim().equals("") && error.trim().length()>0){
//										return error;
//									}
									
									paymentGatewayUniqueId = commonserviceimpl.callPaymnetGatewayUniqueIdAPI(jsonData, smsDetails.getModel(), smsDetails.getEntityName());

								}
								
								String paymentGatewaylink = AppConstants.PAYMENTGATEWAYSMSLINK+paymentGatewayUniqueId;
								logger.log(Level.SEVERE, "paymentGatewaylink" + paymentGatewaylink);

								String netpayable = templateMsgWithBraces.replace("{NetPayable}", invoiceEntity.getNetPayable()+"");
								
								
								String contractId ="";
								
								//Ashwini Patil Date:5-10-2023 for orion
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "GenerateEinvoiceFromReferenceNumber", invoiceEntity.getCompanyId())){
									if(invoice.getRefNumber()!=null&&!invoice.getRefNumber().equals(""))
										contractId= netpayable.replace("{Invoice ID}", invoiceEntity.getRefNumber()+"");
									else
										contractId= netpayable.replace("{Invoice ID}", invoiceEntity.getCount()+"");
								}else{
									contractId= netpayable.replace("{Invoice ID}", invoiceEntity.getCount()+"");
								}
								
								
								
								String strcompanyName = contractId.replace("{CompanyName}", companyName);
								String tinyurl = ServerAppUtility.getTinyUrl(paymentGatewaylink,companyEntity.getCompanyId());
								actualMessage = strcompanyName.replace("{LandingPageLink}", tinyurl);
								
								logger.log(Level.SEVERE, "tinyurl" + tinyurl);
								logger.log(Level.SEVERE, AppConstants.INVOICEPAYMENTGATEWAY+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.INVOICEPDFLINK)){
								
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String companyCellNumber = "";
								if(companyEntity.getCellNumber1()!=0){
									companyCellNumber = companyEntity.getCellNumber1()+"";
								}
							
								String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
//								String documentName = getDocumentName(smsDetails.getEntityName());
								logger.log(Level.SEVERE, "Invoice pdflink " + pdflink);
								String[] pdflinks = pdflink.split("&");
								

								String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());

								StringBuilder strbuilder = new StringBuilder(tinyurl);
								strbuilder.append(tinyurl);
								strbuilder.append(pdflinks[0]);
								
								try {
									strbuilder.append(pdflinks[1]);
									strbuilder.append(pdflinks[2]);
									strbuilder.append(pdflinks[3]);
								} catch (Exception e) {
								}
								
								String strtinyurl = templateMsgWithBraces.replace("{InvoicePdfLink}", tinyurl);
								String companyCellno = strtinyurl.replace("{CompanyMobile}", companyCellNumber);
								String invoiceDate=companyCellno.replace("{InvoiceDate}",  dateFormat.format(invoice.getInvoiceDate()));
								String invoiceNo ="";
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "GenerateEinvoiceFromReferenceNumber", invoice.getCompanyId())){
									
									if(invoice !=null&&invoice.getRefNumber()!=null&&!invoice.getRefNumber().equals(""))
										invoiceNo= invoiceDate.replace("{InvoiceNumber}", invoice.getRefNumber()+"");
									else
										invoiceNo=invoiceDate.replace("{InvoiceNumber}", invoice.getCount()+"");
								}else{
									invoiceNo = invoiceDate.replace("{InvoiceNumber}", invoice.getCount()+"");									
								}
								
								String invoiceAmount=invoiceNo.replace("{InvoiceAmount}", decimalformat.format(invoice.getNetPayable()));
								actualMessage = invoiceAmount.replace("{CompanyName}", companyName);
								logger.log(Level.SEVERE, AppConstants.INVOICEPDFLINK+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.INVOICEAPPROVAL)){
								
								actualMessage = smsDetails.getSmsTemplate().getMessage();
							
								if(actualMessage.contains("{DocumentId}")){
									actualMessage = actualMessage.replace("{DocumentId}", invoice.getCount()+"");
								}
								if(actualMessage.contains("{DocumentDate}")){
									actualMessage = actualMessage.replace("{DocumentDate}", dateFormat.format(invoice.getInvoiceDate()));
								}
								if(actualMessage.contains("{NetPayable}")){
									actualMessage = actualMessage.replace("{NetPayable}", invoice.getNetPayable()+"");
								}
								if(actualMessage.contains("{CustomerName}")){
									actualMessage = actualMessage.replace("{CustomerName}", invoice.getPersonInfo().getFullName()+"");
								}
								if(actualMessage.contains("{CompanyName}")){
									actualMessage = actualMessage.replace("{CompanyName}", companyName);
								}
								if(actualMessage.contains("{FromDate}") && invoice.getBillingPeroidFromDate()!=null){
									actualMessage = actualMessage.replace("{FromDate}", dateFormat.format(invoice.getBillingPeroidFromDate()));
								}
								if(actualMessage.contains("{ToDate}") && invoice.getBillingPeroidToDate()!=null){
									actualMessage = actualMessage.replace("{ToDate}", dateFormat.format(invoice.getBillingPeroidToDate()));
								}
								if(actualMessage.contains("{PDFLink}")){
									smsDetails.setPdfURL(commonserviceimpl.getCompleteURL(companyEntity));
									String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									logger.log(Level.SEVERE, "Invoice pdflink " + pdflink);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage = actualMessage.replace("{PDFLink}", tinyurl);
								}
								if(actualMessage.contains("{FromDate}") && invoice.getBillingPeroidFromDate()!=null){
									actualMessage = actualMessage.replace("{FromDate}", dateFormat.format(invoice.getBillingPeroidFromDate()));
								}
								if(actualMessage.contains("{ToDate}") && invoice.getBillingPeroidToDate()!=null){
									actualMessage = actualMessage.replace("{ToDate}", dateFormat.format(invoice.getBillingPeroidToDate()));
								}
							
							}

							
//							logger.log(Level.SEVERE, "Invoice actualMessage" + actualMessage);
//							if(actualMessage!=null && !actualMessage.equals("")) {
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
//							}
							
						}
						else if(smsDetails.getEntityName().trim().equals("Complain")){
							Complain complain = (Complain) smsDetails.getModel();
							
//							/**
//							 * @author Vijay Date :- 09-08-2021
//							 * Des :- added below method to check customer DND status if customer DND status is Active 
//							 * then SMS will not send to customer
//							 */
//							ServerAppUtility serverapputility = new ServerAppUtility();
//							boolean dndstatusFlag = serverapputility.validateCustomerDNDStatus(complain.getPersoninfo().getCount(), complain.getCompanyId());
//							if(dndstatusFlag){
//								return "Can not send SMS customer DND status is active";
//							}
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.COMPLAINCREATED)){
								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String customerName = complain.getPersoninfo().getFullName();
								/**
								 * Date 29 jun 2017 added by vijay this is for Eco friendly	
								 */
								if(EnableSMSAPIBHASH){
									customerName = getCustomerName(customerName,smsDetails.getModel().getCompanyId());

								}
								/**
								 * ends here
								 */
								
								String serviceid = "";
								if(complain.getServiceList()!=null && complain.getServiceList().size()>0) {
									serviceid = complain.getServiceList().get(0).getCount()+"";
								}
								
								
								String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
								String serviceId = cutomerName.replace("{ServiceID}", serviceid+"");
								String complaintid = serviceId.replace("{ComplaintID}", complain.getCount()+"");
								actualMessage = complaintid.replace("{CompanyName}", companyName);
						        logger.log(Level.SEVERE, AppConstants.COMPLAINCREATED+" actualMessage" + actualMessage);

//								String smsresponseMessage = smsimpl.sendSmsToClient(actaulMsg, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS smsresponseMessage "+smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.COMPLAINCOMPLETED) ||
									smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.COMPLAINSERVICECOMPLETED)){

								String templateMsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								String customerName = complain.getPersoninfo().getFullName();
								
								/**
								 * @author Anil @since 02-09-2021
								 * If service is of type complaint then we will not send service completion sms 
								 * will send sms on complaint completion
								 * Requirement raise by Rutuza, Vaishnavi and Nitin sir for Universal Pest Control
								 */
								Complain complaintEntity = (Complain) smsDetails.getModel();
								String smsEventName=AppConstants.COMPLAINCOMPLETED;
								String complaintClosingDate="";
								Service service = ofy().load().type(Service.class).filter("companyId",companyEntity.getCompanyId()).filter("ticketNumber",complaintEntity.getCount()).first().now();
								if(service!=null&&!service.getServiceType().equals("")&&service.getServiceType().equals("Complaint Service")){
									smsEventName="Complaint Service Completed";
								}
								if(complaintEntity!=null&&complaintEntity.getCompletionDate()!=null){
									complaintClosingDate=fmt.format(complaintEntity.getCompletionDate());
								}
								
								logger.log(Level.SEVERE,"Inside Complaint SMS evnt - "+smsEventName+" / "+complaintClosingDate);

								/**
								 * Date 29 jun 2017 added by vijay this is for Eco friendly	
								 */
								if(EnableSMSAPIBHASH){
									customerName = getCustomerName(customerName,smsDetails.getModel().getCompanyId());
								}
								/**
								 * ends here
								 */
								
								String cutomerName = templateMsgwithbraces.replace("{CustomerName}", customerName);
								String complaintid = cutomerName.replace("{ComplaintID}", complain.getCount()+"");
								String servicedate = complaintid.replace("{ServiceDate}", fmt.format(complain.getServiceDate()));
								actualMessage = servicedate.replace("{CompanyName}", companyName);
								
								actualMessage = actualMessage.replace("{ComplaintClosureDate}", complaintClosingDate);
								System.out.println("actaulMsg"+actualMessage);
								
								if(smsEventName.equals("Complaint Service Completed")){
									Company company = ofy().load().type(Company.class).filter("companyId", service.getCompanyId()).first().now();
									
									if(company!=null&&company.getFeedbackUrlList()!=null&&company.getFeedbackUrlList().size()!=0){
										String feedbackFormUrl="";
										Customer customer=ofy().load().type(Customer.class).filter("companyId",service.getCompanyId()).filter("count",service.getPersonInfo().getCount()).first().now();
										if(customer!=null){
											if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
												for(FeedbackUrlAsPerCustomerCategory feedback:company.getFeedbackUrlList()){
													if(feedback.getCustomerCategory().equals(customer.getCategory())){
														feedbackFormUrl=feedback.getFeedbackUrl();
														break;
													}
												}
											}
										}
										if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("")){
											logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
											StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
									        sbPostData.append("?customerId="+service.getPersonInfo().getCount());
									        sbPostData.append("&customerName="+service.getPersonInfo().getFullName());
									        sbPostData.append("&serviceId="+service.getCount());
									        sbPostData.append("&serviceDate="+fmt.format(service.getServiceDate()));
									        sbPostData.append("&serviceName="+service.getProductName());
									        sbPostData.append("&technicianName="+service.getEmployee());
									        sbPostData.append("&branch="+service.getBranch());
									        feedbackFormUrl = sbPostData.toString();
									        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
									        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
									        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
									        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
									        actualMessage=actualMessage.replace("{Link}", feedbackFormUrl);
										}else{
											actualMessage=actualMessage.replace(" Please share your feedback by clicking on this link {Link} -", "-");
											actualMessage=actualMessage.trim();
										}
									}
								}
						        logger.log(Level.SEVERE, AppConstants.COMPLAINCOMPLETED+" actualMessage" + actualMessage);

								
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(),companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE,"Calling SMS API method to send SMS"+smsresponseMessage);
//								return smsresponseMessage;

							}
							
							
						}
						else if(smsDetails.getEntityName().trim().equals("Service")){
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.SERVICERECORDPDFLINK)){
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String companyCellNumber = "";
								if(companyEntity.getCellNumber1()!=0){
									companyCellNumber = companyEntity.getCellNumber1()+"";
								}
							
								String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
								logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
								String[] pdflinks = pdflink.split("&");
								

								String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());

								StringBuilder strbuilder = new StringBuilder(tinyurl);
								strbuilder.append(tinyurl);
								strbuilder.append(pdflinks[0]);
								
								try {
									strbuilder.append(pdflinks[1]);
									strbuilder.append(pdflinks[2]);
									strbuilder.append(pdflinks[3]);
								} catch (Exception e) {
								}
								Service serviceentity = ofy().load().type(Service.class).filter("companyId", smsDetails.getModel().getCompanyId()).filter("count", smsDetails.getModel().getCount()).first().now();
								
								String strnextServiceDate = "";
								String completedserviceDate = "";
								int nextserviceId = smsDetails.getModel().getCount();
								logger.log(Level.SEVERE, "before increment nextserviceId" + nextserviceId);
								if(nextserviceId!=0) {
									nextserviceId++;
								}
								logger.log(Level.SEVERE, "nextserviceId" + nextserviceId);

								Service nextserviceentity = ofy().load().type(Service.class).filter("companyId", smsDetails.getModel().getCompanyId()).filter("count", nextserviceId).first().now();

								if(serviceentity!=null){
//									strnextServiceDate = dateFormat.format(serviceentity.getServiceDate());
									try {
										completedserviceDate = dateFormat.format(serviceentity.getServiceCompletionDate());
									} catch (Exception e) {
									}

								}
								
								if(nextserviceentity!=null){
									try {
										strnextServiceDate = dateFormat.format(nextserviceentity.getServiceDate());
									} catch (Exception e) {
									}

								}
										
								String strtinyurl = templateMsgWithBraces.replace("{ServiceCardPdfLink}", tinyurl);
								String companyCellno = strtinyurl.replace("{ServiceCompletionDate}", completedserviceDate);
								String nextServiceDate = companyCellno.replace("{NextScheduleDate}", strnextServiceDate);
								actualMessage = nextServiceDate.replace("{CompanyName}", companyName);
								logger.log(Level.SEVERE, AppConstants.SERVICERECORDPDFLINK+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.SERVICECOMPLETION)) {
								
								Service serviceentity = null;
								
								try {
									serviceentity = (Service) smsDetails.getModel();
								} catch (Exception e) {
									// TODO: handle exception
								}
										
								if(serviceentity==null) {
									serviceentity = ofy().load().type(Service.class).filter("companyId", smsDetails.getModel().getCompanyId()).filter("count", smsDetails.getModel().getCount()).first().now();
								}
								
								final String templateMsgwithbraces = smsDetails.getMessage(); 
								final String prodName = serviceentity.getProductName();
								final String serNo = serviceentity.getServiceSerialNo()+"";
								final String serDate = dateFormat.format(serviceentity.getServiceDate());
								
								String custName = serviceentity.getPersonInfo().getFullName();
								String cutomerName = templateMsgwithbraces.replace("{CustomerName}", custName);
								String productName = cutomerName.replace("{ProductName}", prodName);
								String serviceNo = productName.replace("{ServiceNo}", serNo);
								String serviceDate =  serviceNo.replace("{ServiceDate}", serDate);
								actualMessage = serviceDate.replace("{companyName}", companyName);
								
								if(actualMessage.contains("{DocumentId}")){
									actualMessage =  actualMessage.replace("{DocumentId}", serviceentity.getCount()+"");
								}
								if(actualMessage.contains("{PDFLink}")){
									String pdflink = commonserviceimpl.getPDFURL("Service", serviceentity, serviceentity.getCount(), serviceentity.getCompanyId(), companyEntity, commonserviceimpl.getCompleteURL(companyEntity),null);
									logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage =  actualMessage.replace("{PDFLink}", tinyurl);
								}
								
								
								/**
								 * @author Anil @since 09-03-2021
								 * loading feedback form as per the customer category
								 */
								if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
									/**
									 * @author Anil @since 03-03-2021
									 * Adding feedback form url on service completion sms
									 * Raised by Nitin Sir for Pecopp
									 */
									String feedbackFormUrl="";
									Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceentity.getCompanyId()).filter("count",serviceentity.getPersonInfo().getCount()).first().now();
									if(customer!=null){
										if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
											for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
												if(feedback.getCustomerCategory().equals(customer.getCategory())){
													feedbackFormUrl=feedback.getFeedbackUrl();
													break;
												}
											}
										}
									}
									
									
									boolean customergroupHighFrequency = true;
									if(customer!=null){
											
										String customerGroup = customer.getGroup();
										if(customerGroup.trim().equals(AppConstants.HIGHFREQUENCYSERVICES)){
											Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", serviceentity.getCompanyId())
													.filter("count", serviceentity.getContractCount()).first().now();
												double frequency =0;
												for(SalesLineItem saleslineitem : contractEntity.getItems()){
													if(saleslineitem.getProductCode().trim().equals(serviceentity.getProduct().getProductCode().trim())){
														frequency = saleslineitem.getDuration()/saleslineitem.getNumberOfServices();
														break;
													}
												}
												logger.log(Level.SEVERE, "frequency" + frequency);
												logger.log(Level.SEVERE, "company.getMinimumDuration()" + companyEntity.getMinimumDuration());
									
												if(frequency<companyEntity.getMinimumDuration()){
													customergroupHighFrequency = false;
												}
										}
										
									}
										
									if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("") && customergroupHighFrequency){
										logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
										StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
								        sbPostData.append("?customerId="+serviceentity.getPersonInfo().getCount());
								        sbPostData.append("&customerName="+serviceentity.getPersonInfo().getFullName());
								        sbPostData.append("&serviceId="+serviceentity.getCount());
								        sbPostData.append("&serviceDate="+serDate);
								        sbPostData.append("&serviceName="+serviceentity.getProductName());
								        sbPostData.append("&technicianName="+serviceentity.getEmployee());
								        sbPostData.append("&branch="+serviceentity.getBranch());
								        feedbackFormUrl = sbPostData.toString();
								        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
								        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
								        /**
								         * @author Anil @since 10-03-2021
								         * Modified feedback Url i.e. tiny utl
								         */
								        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
								        
								        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
								        actualMessage=actualMessage.replace("{Link}", feedbackFormUrl);
									}
									/**
									 * @author Anil @since 19-05-2021
									 * As per Nitin sir's instruction if no schduling URL found then 
									 * remove {Link} keyword from SMS template as well
									 */
									else{
//										actualMsg=actualMsg.replace("{Link}", "");
										actualMessage=actualMessage.replace(" Please share your feedback by clicking on this link {Link} -", "-");
										actualMessage=actualMessage.trim();
									}
								}
								
								if(actualMessage.contains("{DocumentId}")){
									actualMessage =  actualMessage.replace("{DocumentId}", serviceentity.getCount()+"");
								}
								if(actualMessage.contains("{PDFLink}")){
									smsDetails.setPdfURL(commonserviceimpl.getCompleteURL(companyEntity));
									String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage =  actualMessage.replace("{PDFLink}", tinyurl);
								}
								
								
								

								
								logger.log(Level.SEVERE, AppConstants.SERVICECOMPLETION+" actualMessage" + actualMessage);
//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
								
							}
							
							/*
							 * Ashwini Patil 
							 * Date:5-02-2024
							 * Problem: When we configure automatic message templates for manual message popup, placeholders are not getting replaced as here code is checking template name.
							 * Solution: we are writting one else block which will get executed when not matching template found
							 * here we are trying to replace all the placeholders related to service
							 */
							else {
								Service serviceentity = null;
								
								try {
									serviceentity = (Service) smsDetails.getModel();
								} catch (Exception e) {
									// TODO: handle exception
								}
										
								if(serviceentity==null) {
									serviceentity = ofy().load().type(Service.class).filter("companyId", smsDetails.getModel().getCompanyId()).filter("count", smsDetails.getModel().getCount()).first().now();
								}
								Company comp = ofy().load().type(Company.class).filter("companyId", serviceentity.getCompanyId()).first().now();
								
								final String templateMsgwithbraces = smsDetails.getMessage(); 
								final String prodName = serviceentity.getProductName();
								final String serNo = serviceentity.getServiceSerialNo()+"";
								final String serDate = dateFormat.format(serviceentity.getServiceDate());
								
								String custName = serviceentity.getPersonInfo().getFullName();
								String replacedMsg = templateMsgwithbraces.replace("{CustomerName}", custName);
								replacedMsg = replacedMsg.replace("{ProductName}", prodName);
								replacedMsg = replacedMsg.replace("{ServiceNo}", serNo);
								replacedMsg =  replacedMsg.replace("{ServiceDate}", serDate);
								replacedMsg = replacedMsg.replace("{companyName}", companyName);
								replacedMsg = replacedMsg.replace("{CompanyName}", companyName);
																
								
								replacedMsg=replacedMsg.replace("{ServiceNumber}", serviceentity.getCount()+"");
								replacedMsg=replacedMsg.replace("{DD-MM-YY}", sdf.format(serviceentity.getServiceDate())+"");
								replacedMsg=replacedMsg.replace("{TechnicianName}", serviceentity.getEmployee());
								replacedMsg=replacedMsg.replace("{Date}", sdf.format(serviceentity.getServiceDate())+"");
								replacedMsg=replacedMsg.replace("{Time}", serviceentity.getServiceTime()+"");
								replacedMsg=replacedMsg.replace("{companynumber}",comp.getCellNumber1()+"");
								
								if(replacedMsg.contains("{ServiceCompletionDate}")) {
									if(serviceentity!=null&&serviceentity.getCompletedDate()!=null)
										replacedMsg=replacedMsg.replace("{ServiceCompletionDate}",sdf.format(serviceentity.getCompletedDate()));	
									else
										replacedMsg=replacedMsg.replace("{ServiceCompletionDate}","");	
								}
						
								if(serviceentity.getEmployee()!=null&&!serviceentity.getEmployee().equals("")) {
								Employee emp = ofy().load().type(Employee.class).filter("companyId", comp.getCompanyId()).filter("fullname", serviceentity.getEmployee()).first().now();				
								replacedMsg=replacedMsg.replace("{Number}", emp.getContacts().get(0).getCellNo1()+"");
//								
								}
								
								if(replacedMsg.contains("{DocumentId}")){
									replacedMsg =  replacedMsg.replace("{DocumentId}", serviceentity.getCount()+"");
								}
								if(replacedMsg.contains("{PDFLink}")){
									String pdflink = commonserviceimpl.getPDFURL("Service", serviceentity, serviceentity.getCount(), serviceentity.getCompanyId(), companyEntity, commonserviceimpl.getCompleteURL(companyEntity),null);
									logger.log(Level.SEVERE, "Service record pdflink " + pdflink);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									replacedMsg =  replacedMsg.replace("{PDFLink}", tinyurl);
								}
								
								actualMessage=replacedMsg;
								if(companyEntity!=null&&companyEntity.getFeedbackUrlList()!=null&&companyEntity.getFeedbackUrlList().size()!=0){
									
									String feedbackFormUrl="";
									Customer customer=ofy().load().type(Customer.class).filter("companyId",serviceentity.getCompanyId()).filter("count",serviceentity.getPersonInfo().getCount()).first().now();
									if(customer!=null){
										if(customer.getCategory()!=null&&!customer.getCategory().equals("")){
											for(FeedbackUrlAsPerCustomerCategory feedback:companyEntity.getFeedbackUrlList()){
												if(feedback.getCustomerCategory().equals(customer.getCategory())){
													feedbackFormUrl=feedback.getFeedbackUrl();
													break;
												}
											}
										}
									}
									
									
									boolean customergroupHighFrequency = true;
									if(customer!=null){
											
										String customerGroup = customer.getGroup();
										if(customerGroup.trim().equals(AppConstants.HIGHFREQUENCYSERVICES)){
											Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", serviceentity.getCompanyId())
													.filter("count", serviceentity.getContractCount()).first().now();
												double frequency =0;
												for(SalesLineItem saleslineitem : contractEntity.getItems()){
													if(saleslineitem.getProductCode().trim().equals(serviceentity.getProduct().getProductCode().trim())){
														frequency = saleslineitem.getDuration()/saleslineitem.getNumberOfServices();
														break;
													}
												}
												logger.log(Level.SEVERE, "frequency" + frequency);
												logger.log(Level.SEVERE, "company.getMinimumDuration()" + companyEntity.getMinimumDuration());
									
												if(frequency<companyEntity.getMinimumDuration()){
													customergroupHighFrequency = false;
												}
										}
										
									}
										
									if(feedbackFormUrl!=null&&!feedbackFormUrl.equals("") && customergroupHighFrequency){
										logger.log(Level.SEVERE, "ZOHO FEEDBACK URL " + feedbackFormUrl);
										StringBuilder sbPostData= new StringBuilder(feedbackFormUrl);
								        sbPostData.append("?customerId="+serviceentity.getPersonInfo().getCount());
								        sbPostData.append("&customerName="+serviceentity.getPersonInfo().getFullName());
								        sbPostData.append("&serviceId="+serviceentity.getCount());
								        sbPostData.append("&serviceDate="+serDate);
								        sbPostData.append("&serviceName="+serviceentity.getProductName());
								        sbPostData.append("&technicianName="+serviceentity.getEmployee());
								        sbPostData.append("&branch="+serviceentity.getBranch());
								        feedbackFormUrl = sbPostData.toString();
								        feedbackFormUrl=feedbackFormUrl.replaceAll("\\s", "%20");
								        feedbackFormUrl=feedbackFormUrl.replaceAll("&", "%26");
								       
								        feedbackFormUrl=ServerAppUtility.getTinyUrl(feedbackFormUrl,companyEntity.getCompanyId());
								        
								        logger.log(Level.SEVERE, "FINAL ZOHO FEEDBACK URL " + feedbackFormUrl);
								        actualMessage=actualMessage.replace("{Link}", feedbackFormUrl);
									}
									
									else{
//										actualMsg=actualMsg.replace("{Link}", "");
										actualMessage=actualMessage.replace(" Please share your feedback by clicking on this link {Link} -", "-");
										actualMessage=actualMessage.trim();
									}
								}
						
								logger.log(Level.SEVERE, smsDetails.getSmsTemplate().getEvent().trim()+" actualMessage" + actualMessage);

							}
							
						}
						else if(smsDetails.getEntityName().trim().equals("CustomerPayment")){
							CustomerPayment customerPayment = (CustomerPayment) smsDetails.getModel();
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.AMOUNTRECEIVED)){
								String templatemsgwithbraces = smsDetails.getSmsTemplate().getMessage();
								
								String amountRecieved = templatemsgwithbraces.replace("{AmountRecieved}", customerPayment.getPaymentReceived()+"");
								String paymentDate= amountRecieved.replace("{Date}",dateFormat.format(customerPayment.getPaymentDate()));
								if(customerPayment.getAmountTransferDate()!=null) //18-10-2023
									paymentDate= paymentDate.replace("{ReceivedDate}",dateFormat.format(customerPayment.getAmountTransferDate()));
								else
									paymentDate= paymentDate.replace("{ReceivedDate}","");
								
								String companyname =paymentDate.replace("{CompanyName}", companyName);
								
								
								//Ashwini Patil Date:5-10-2023 for orion
								String invoiceNo ="";
								if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "GenerateEinvoiceFromReferenceNumber", customerPayment.getCompanyId())){
									Invoice invoice = ofy().load().type(Invoice.class).filter("companyId",customerPayment.getCompanyId()).filter("count", customerPayment.getInvoiceCount())
											.first().now();
									if(invoice !=null&&invoice.getRefNumber()!=null&&!invoice.getRefNumber().equals(""))
										invoiceNo= companyname.replace("{InvoiceNumber}", invoice.getRefNumber()+"");
									else
										invoiceNo=companyname.replace("{InvoiceNumber}", customerPayment.getInvoiceCount()+"");
								}else{
									invoiceNo = companyname.replace("{InvoiceNumber}", customerPayment.getInvoiceCount()+"");									
								}
								actualMessage = invoiceNo.replace("{InvoiceDate}", dateFormat.format(customerPayment.getInvoiceDate()));
								
								actualMessage=actualMessage.replace("{OrderId}", customerPayment.getContractCount()+"");
								
								logger.log(Level.SEVERE, AppConstants.AMOUNTRECEIVED+" actualMessage" + actualMessage);

//								logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//								String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//								logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//								return smsresponseMessage;
							}
							else if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.AMOUNTPAYABLE)) {
									String templatemsgwithbraces = smsDetails.getSmsTemplate().getMessage();
									
									String amountPayable = templatemsgwithbraces.replace("{PayableAmt}", customerPayment.getPaymentReceived()+"");
									String paymentDate= amountPayable.replace("{InvoiceNo}",customerPayment.getInvoiceCount()+"");
									String companyname =paymentDate.replace("{InvoiceDate}", dateFormat.format(customerPayment.getInvoiceDate()));
									actualMessage = companyname.replace("{PaymentMethod}", customerPayment.getPaymentMethod()+"");
									
									logger.log(Level.SEVERE, AppConstants.AMOUNTPAYABLE+" actualMessage" + actualMessage);

//									logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
//									String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
//									logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
//									return smsresponseMessage;
							}
						}
						else if(smsDetails.getEntityName().trim().equals("DeliveryNote")){
							DeliveryNote deliveryNoteEntity = (DeliveryNote) smsDetails.getModel();
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.DELIVERYNOTEAPPROVED)) {
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String deliveryDate = dateFormat.format(deliveryNoteEntity.getDeliveryDate());
								String customerName = templateMsgWithBraces.replace("{CustomerName}", deliveryNoteEntity.getCinfo().getFullName()+"");
								String orderId = customerName.replace("{OrderID}",deliveryNoteEntity.getSalesOrderCount()+"");
								String deliveryDatemsg = orderId.replace("{DeliveryDate}",deliveryDate);
								String lrnNumber = deliveryDatemsg.replace("{LRNo}", deliveryNoteEntity.getLrNumber());
								String deliveryPersonName = lrnNumber.replace("{DeliveryPerson}", deliveryNoteEntity.getDriverName());
								actualMessage = deliveryPersonName.replace("{CellNo}", deliveryNoteEntity.getDriverPhone()+"");
								
								logger.log(Level.SEVERE,AppConstants.DELIVERYNOTEAPPROVED+" actualMsg "+actualMessage);
							}
							
						}
						else if(smsDetails.getEntityName().trim().equals("PurchaseOrder")){
							PurchaseOrder purchaseorder = (PurchaseOrder) smsDetails.getModel();
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.PURCHASEORDER)) {
								String vendorName = "";
								String cellNumber = "";
								if(purchaseorder.getVendorDetails()!=null&&purchaseorder.getVendorDetails().size()!=0){
									for(VendorDetails vendor:purchaseorder.getVendorDetails()){
										if(vendor.getStatus()){
											vendorName = vendor.getVendorName();
											if(vendor.getVendorPhone()!=0){
												logger.log(Level.SEVERE, "vendor.getVendorPhone() "+vendor.getVendorPhone());
												cellNumber = vendor.getVendorPhone()+"";
											}
											else{
												ServerAppUtility apputility = new ServerAppUtility();
												cellNumber = apputility.getvendorCellNumber(purchaseorder.getCompanyId(),vendor.getVendorId());
											}
										}
									}
								}
								smsDetails.setMobileNumber(cellNumber);	
								String templateMsgWithBraces = smsDetails.getSmsTemplate().getMessage();
								String purchaseorderno = templateMsgWithBraces.replace("{PurchaseOrderNo}",purchaseorder.getCount()+"");
								String purDate = purchaseorderno.replace("{PODate}",  dateFormat.format(purchaseorder.getPODate()));
								actualMessage = purDate.replace("{CompanyName}", companyName);
								if(actualMessage.contains("{VendorName}")){
									actualMessage = actualMessage.replace("{VendorName}", vendorName);
								}
								if(actualMessage.contains("{NetPayable}")){
									actualMessage = actualMessage.replace("{NetPayable}", purchaseorder.getNetpayble()+"");
								}

								if(actualMessage.contains("{PDFLink}")){
									smsDetails.setPdfURL(commonserviceimpl.getCompleteURL(companyEntity));
									String pdflink = commonserviceimpl.getPDFURL(smsDetails.getDocumentName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									logger.log(Level.SEVERE, "PurchaseOrder pdflink " + pdflink);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage = actualMessage.replace("{PDFLink}", tinyurl);
								}
								
								logger.log(Level.SEVERE,AppConstants.PURCHASEORDER+" actualMsg "+actualMessage);
							}
							
						}
						else if(smsDetails.getEntityName().trim().equals("GRN")){
							GRN grnEntity = (GRN) smsDetails.getModel();
							
							if(smsDetails.getSmsTemplate().getEvent().trim().equals(AppConstants.GRNAPPROVAL)) {
									
								actualMessage = smsDetails.getSmsTemplate().getMessage();
								
								if(actualMessage.contains("{DocumentId}")){
									actualMessage = actualMessage.replace("{DocumentId}", grnEntity.getCount()+"");
								}
								if(actualMessage.contains("{DocumentDate}")){
									actualMessage = actualMessage.replace("{DocumentDate}", dateFormat.format(grnEntity.getCreationDate()));
								}
								if(actualMessage.contains("{VendorName}")){
									actualMessage = actualMessage.replace("{VendorName}", grnEntity.getVendorInfo().getFullName());
								}
								if(actualMessage.contains("{CompanyName}")){
									actualMessage = actualMessage.replace("{CompanyName}", companyName);
								}
								if(actualMessage.contains("{PDFLink}")){
									smsDetails.setPdfURL(commonserviceimpl.getCompleteURL(companyEntity));
									String pdflink = commonserviceimpl.getPDFURL(smsDetails.getEntityName(), smsDetails.getModel(), smsDetails.getModel().getCount(), smsDetails.getModel().getCompanyId(), companyEntity, smsDetails.getPdfURL(),null);
									String tinyurl = ServerAppUtility.getTinyUrl(pdflink,companyEntity.getCompanyId());
									actualMessage = actualMessage.replace("{PDFLink}", tinyurl);
								}

								logger.log(Level.SEVERE,AppConstants.PURCHASEORDER+" actualMsg "+actualMessage);
							}
							
						}
						else {
							logger.log(Level.SEVERE," no matching entity "+smsDetails.getEntityName());

						}

						logger.log(Level.SEVERE, "Final actualMessage" + actualMessage);
						logger.log(Level.SEVERE, "mobileNo" + smsDetails.getMobileNumber());
						logger.log(Level.SEVERE, "smsDetails.getCommunicationChannel()" + smsDetails.getCommunicationChannel());

						logger.log(Level.SEVERE, "smsDetails.getModelName()" + smsDetails.getModuleName());
						logger.log(Level.SEVERE, "smsDetails.getDocumentName()" + smsDetails.getDocumentName());

						if(actualMessage!=null && !actualMessage.equals("")) {
							logger.log(Level.SEVERE, "inside actual message condition");

						if(smsDetails.getModuleName()!=null && !smsDetails.getModuleName().equals("")
								&& smsDetails.getDocumentName()!=null && !smsDetails.getDocumentName().equals("")) {
							
							logger.log(Level.SEVERE, "inside automatic message" );

							/**
							 * @author Vijay Date :- 03-05-2022
							 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
							 * whats and sms based on communication channel configuration.
							 */
							long mobilenumber = Long.parseLong(smsDetails.getMobileNumber());
							sendMessage(smsDetails.getModuleName(),smsDetails.getDocumentName(),smsDetails.getSmsTemplate().getEvent(),companyEntity.getCompanyId(),actualMessage,mobilenumber,true);
							logger.log(Level.SEVERE,"after sendMessage method");
							
						}
						else {
							
							String smsresponseMessage = smsimpl.sendSmsToClient(actualMessage, smsDetails.getMobileNumber(), companyEntity.getCompanyId(), smsDetails.getCommunicationChannel());
							logger.log(Level.SEVERE, "smsresponseMessage" + smsresponseMessage);
							return smsresponseMessage;
						}
							
							
							
						}
						else {
							if(smsDetails.getCommunicationChannel().equals(AppConstants.WHATSAPP)) {
								actualMessage = smsDetails.getMessage();
								return sendMessageOnWhatsApp(companyEntity.getCompanyId(), smsDetails.getMobileNumber(), actualMessage);
							}
						}
						
					
//				}
//				else{
//					return "SMS configuration status is inactive!";
//				}
//				
//			}
//			else{
//				return "SMS configuration is not defined!";
//			}
			return "";
		}
		
		
		
		



//		public String sendSmsToClient(String actualMessage, String clientCell,String accSid, String userName, String password, long companyId, String SMSVendorAPI) {
		public String sendSmsToClient(String actualMessage, String clientCell,long companyId,String communicationChannel) {

			String responsemsg ="";
			logger.log(Level.SEVERE,"inside sendSMSToClient Method "+actualMessage);
			logger.log(Level.SEVERE,"clientCell"+clientCell);
			logger.log(Level.SEVERE,"communicationChannel"+communicationChannel);
			
			
			
			
			/**
			 * @author Vijay Date :- 28-04-2022
			 * Des :- when user will send message on whats app  message from popup on every screen then to send message on whatsapp
			 * below method will call.
			 */
			if(communicationChannel.equals(AppConstants.WHATSAPP)){
				logger.log(Level.SEVERE,"send message on whats app from popup");
				responsemsg = sendMessageOnWhatsApp(companyId,clientCell,actualMessage);
				logger.log(Level.SEVERE,"response "+responsemsg);
			}
			
			EnableSMSAPIWEBTACTIC = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "EnableSMSAPIWEBTACTIC",companyId);
			EnableSMSAPIBHASH=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", "EnableSMSAPIBHASH", companyId);
			EnableSMSUjumbe=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("SMS", AppConstants.PC_ENABLEUJUMBESMSAPI, companyId);

			if(communicationChannel.equals(AppConstants.SMS)){
				SmsConfiguration smsConfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId)
						.filter("status", true).first().now();
				logger.log(Level.SEVERE,"smsConfig"+ smsConfig);
				if(smsConfig!=null){
					if(smsConfig.getMobileNoPrefix()!=null && !smsConfig.getMobileNoPrefix().equals("")){
						clientCell = smsConfig.getMobileNoPrefix()+clientCell;
					}
					if(smsConfig.getStatus()){
						
						if(smsConfig.getAccountSID().equals("") ){
							return "Sender id does not exist in sms configuration!";

						}
						if(smsConfig.getAuthToken().equals("") ){
							return "User name does not exist in sms configuration!";

						}
						if(smsConfig.getPassword().equals("") ){
							return "Password does not exist in sms configuration!";

						}
						
						
						
						logger.log(Level.SEVERE,"User Id"+smsConfig.getAuthToken());
						logger.log(Level.SEVERE,"Password"+smsConfig.getPassword());
						logger.log(Level.SEVERE,"MSG"+actualMessage);
						logger.log(Level.SEVERE,"Sender ID"+smsConfig.getAccountSID());


//				        String mobiles ="91"+clientCell+"";
						
				        String message = actualMessage;

				        logger.log(Level.SEVERE,"Hi SMS Mobile No ===="+clientCell);
				        logger.log(Level.SEVERE,"SMS ==== "+actualMessage);
				        
						if(EnableSMSAPIBHASH){
							
							/**
							 * Date 15 jun 2017 added by vijay for BHASH SMS API
							 * here i am calling BHASH sms api 
							 */
							
					        //Prepare Url
					        URLConnection myURLConnection=null;
					        URL myURL=null;
					        BufferedReader reader=null;

					        //encoding message
					        String encoded_message=URLEncoder.encode(message);
					        //Send SMS API
					        String mainUrl="http://bhashsms.com/api/sendmsg.php?";
					        //Prepare parameter string
					        StringBuilder sbPostData= new StringBuilder(mainUrl);
					        sbPostData.append("user="+smsConfig.getAuthToken());
					        sbPostData.append("&pass="+smsConfig.getPassword());
					        sbPostData.append("&sender="+smsConfig.getAccountSID());
					        sbPostData.append("&phone="+clientCell);
					        sbPostData.append("&text="+encoded_message);
					        sbPostData.append("&priority="+"ndnd");
					        sbPostData.append("&stype="+"normal");
					        //final string
					        mainUrl = sbPostData.toString();
					        
					        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
					        
					        try
					        {
					            //prepare connection
					            myURL = new URL(mainUrl);
					            myURLConnection = myURL.openConnection();
					            myURLConnection.connect();
					            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					            //reading response
					            String response;
					            while ((response = reader.readLine()) != null)
					            //print response
					            System.out.println(response);

					            //finally close connection
					            reader.close();
					        }
					        catch (IOException e)
					        {
					    		logger.log(Level.SEVERE,"Hi Inside Catch Exception"+e);

					                e.printStackTrace();
					        }
							System.out.println(" sms sent successfully");
							logger.log(Level.SEVERE," sms sent successfully");
							return "SMS sent successfully";

							
							
							/**
							 * end here
							 */
							
						}else if(EnableSMSAPIWEBTACTIC){
							
							logger.log(Level.SEVERE,"EnableSMSAPIWEBTACTIC sms sent successfully");
					        //Prepare Url
					        URLConnection myURLConnection=null;
					        URL myURL=null;
					        BufferedReader reader=null;

					        //encoding message
					        String encoded_message=URLEncoder.encode(message);
					        //Send SMS API
					        String mainUrl="http://websms.esandesh.in/websms/sendsms.aspx?";
					        //Prepare parameter string
					        StringBuilder sbPostData= new StringBuilder(mainUrl);
					        sbPostData.append("userid="+smsConfig.getAuthToken());
					        sbPostData.append("&password="+smsConfig.getPassword());
					        sbPostData.append("&sender="+smsConfig.getAccountSID());
					        sbPostData.append("&mobileno="+clientCell);
					        sbPostData.append("&msg="+encoded_message);
					        //final string
					        mainUrl = sbPostData.toString();
					        
					        logger.log(Level.SEVERE,"URL called==== "+mainUrl);
					        
					        try
					        {
					            //prepare connection
					            myURL = new URL(mainUrl);
					            myURLConnection = myURL.openConnection();
					            myURLConnection.connect();
					            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					            //reading response
					            String response;
					            while ((response = reader.readLine()) != null)
					            //print response
					            System.out.println(response);

					            //finally close connection
					            reader.close();
					        }
					        catch (IOException e)
					        {
					    		logger.log(Level.SEVERE,"Hi Inside Catch Exception"+e);

					                e.printStackTrace();
					        }
							System.out.println(" sms sent successfully");
							logger.log(Level.SEVERE," sms sent successfully");
							return "SMS sent successfully";

							/**
							 * end here
							 */
						}
						else if(EnableSMSUjumbe) {
							
							logger.log(Level.SEVERE,"EnableSMSUjumbe sms in process");
							String strrespomse = SendSMSUjumbe(message,clientCell,smsConfig.getAccountSID(),smsConfig.getAuthToken(),smsConfig.getPassword(),smsConfig.getSmsAPIUrl());
							logger.log(Level.SEVERE,"strrespomse "+strrespomse);
							return "SMS sent successfully";

						}
						else{
							
					        //Prepare Url
					        URLConnection myURLConnection=null;
					        URL myURL=null;
					        BufferedReader reader=null;

					        //encoding message
					        String encoded_message=URLEncoder.encode(message);

							
						 	String mainUrl = smsConfig.getSmsAPIUrl();

					        String userNameMsg = mainUrl.replace("{UserName}".trim(), smsConfig.getAuthToken()+"");
							String passwordMsg = userNameMsg.replace("{Password}".trim(),smsConfig.getPassword());
							String PhoneNoMsg = passwordMsg.replace("{MobileNo}".trim(),clientCell);
							String SenderIdMsg = PhoneNoMsg.replace("{SenderId}".trim(),smsConfig.getAccountSID());
							String finalurlMsg = SenderIdMsg.replace("{Message}".trim(),encoded_message);

					        logger.log(Level.SEVERE," finalurlMsg URL called==== "+finalurlMsg);
					        String data="";
					        try
					        {
					            //prepare connection
					            myURL = new URL(finalurlMsg);
					            myURLConnection = myURL.openConnection();
					            myURLConnection.connect();
					            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
					            //reading response
					            String response;
					            while ((response = reader.readLine()) != null)
					    		logger.log(Level.SEVERE,"response "+response);
					    		
					    		String temp;

					    		try {
					    			while ((temp = reader.readLine()) != null) {
					    				data = data + temp;
					    				
					    			}
					    		} catch (IOException e) {
					    			// TODO Auto-generated catch block
					    			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
					    			e.printStackTrace();
					    		}

					    		logger.log(Level.SEVERE, "Data::::::::::" + data);
					    		
					            //finally close connection
					            reader.close();
					    		
					    		
					        }
					        catch (IOException e)
					        {
					    		logger.log(Level.SEVERE,"Hi Inside Catch Exception");
					            e.printStackTrace();
					            return "SMS server down please try after some time!";
					        }
							System.out.println(" sms sent successfully");
							logger.log(Level.SEVERE," sms sent successfully");
							
							return "SMS sent successfully";
							/**
							 * ends here
							 */
							
							
						}
						
						
					}
					else{
							return "SMS configuration status is inactive!";
					}
				}
				else{
					return "SMS configuration is not defined!";
				}
			
			
			}	
			return responsemsg;
		}
		
		










		private String getDocumentName(String entityName) {
			
			if(entityName.trim().equals(AppConstants.CONTRACT_RENEWAL)){
				return "Contract Renewal";
			}
			return entityName;
		}


		private String SendSMSUjumbe(String message, String mobiles, String accSid, String authToken, String password,
				String smsAPIUrl) {
			
//			    String finalMessage = "\"" + message + "\"";
//		        String finalmobiles = "\"" + mobiles + "\"";
//		        String finalaccSid = "\"" + accSid + "\"";

		        JSONObject jMsgObj = new JSONObject();	        
		        JSONObject jMsgBodyObj = new JSONObject();
		        jMsgBodyObj.put("numbers", mobiles);
		        jMsgBodyObj.put("message",message);
		        jMsgBodyObj.put("sender", accSid);
		        jMsgBodyObj.put("source_id", "12345_a unique_identifier_for_each_message");
		        jMsgBodyObj.put("delivery_report_endpoint", "https://you_link_to_post_the_delivery_report");
		        
		       
		        jMsgObj.put("message_bag", jMsgBodyObj);
		        
		        JSONArray jArray=new JSONArray();	
		        jArray.put(jMsgObj);
		        
		        
				JSONObject JFinalObj =new JSONObject();
				JFinalObj.put("data", jArray);
		        
				String jsonString = JFinalObj.toJSONString().replaceAll("\\\\", "");
				logger.log(Level.SEVERE, "jsonString == " + jsonString);
				
				OkHttpClient client = new OkHttpClient().newBuilder()
						  .build();
						MediaType mediaType = MediaType.parse("application/json");
//						String strnewString = "{\n    \"data\": [\n        {\n            \"message_bag\": {\n    "
//								+ "            \"numbers\": "+finalmobiles+",\n                "
//								+ "\"message\": "+finalMessage+",\n          "
//								+ "      \"sender\": "+finalaccSid+",\n               "
//								+ " \"source_id\":\"12345_a unique_identifier_for_each_message\",\n               "
//								+ " \"delivery_report_endpoint\":\"https://you_link_to_post_the_delivery_report\" \n            }\n        }\n    ]\n}";
						
//						RequestBody body = RequestBody.create(mediaType, "{\n    \"data\": [\n        {\n            \"message_bag\": {\n    "
//								+ "            \"numbers\": \"254723660400\",\n                "
//								+ "\"message\": \"New Message test chnages by Vijay \",\n          "
//								+ "      \"sender\": \"RAFIKI_PEST\",\n               "
//								+ " \"source_id\":\"123456_a unique_identifier_for_each_message\",\n               "
//								+ " \"delivery_report_endpoint\":\"https://you_link_to_post_the_delivery_report\" \n            }\n        }\n    ]\n}");

						RequestBody body = RequestBody.create(mediaType,jsonString);
						String emailAPI = smsAPIUrl+authToken; // auth token will be Email id here
						logger.log(Level.SEVERE,"emailAPI"+emailAPI);
						Request request = new Request.Builder()
						  .url(emailAPI)
						  .method("POST", body)
						  .addHeader("x-authorization", password)
						  .addHeader("email", authToken)
						  .addHeader("Content-Type", "application/json")
						  .build();
						try {
							Response response = client.newCall(request).execute();
							logger.log(Level.SEVERE,"EnableSMSUjumbe response "+response);

						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
//						Request request = new Request.Builder()
////							  .url("https://ujumbesms.co.ke/api/messaging?email=service@rafikipestcontrol.com")
//							  .method("POST", body)
//							  .addHeader("x-authorization", "ZjM0NDYzNTA3YmE4MDg3YzVkYzBlZmQzOGM1Y2Uy")
//							  .addHeader("email", "service@rafikipestcontrol.com")
//							  .addHeader("Content-Type", "application/json")
//							  .build();
						
			return null;
		}

       /**Added by sheetal : 15-03-2022
        * Des : For Sending SMS automatically when clicked on lead cancelled or lead unsuccessful**/
		@Override
		public String sendSMSToCustomer(Lead lead) {
			 SMSDetails smsdetails = new SMSDetails();
			 smsdetails.setModel(lead);
			 SmsTemplate smsEntity = null;
			 if(lead.getStatus().equals(AppConstants.CANCELLED)){
				 smsEntity=ofy().load().type(SmsTemplate.class).filter("companyId", lead.getCompanyId()).filter("event", "Lead Cancelled").filter("status",true).first().now();
			 }
			 if(lead.getStatus().equals(AppConstants.Unsucessful)){
				 smsEntity=ofy().load().type(SmsTemplate.class).filter("companyId", lead.getCompanyId()).filter("event", "Lead Unsuccessful").filter("status",true).first().now();
			 }
			 if(smsEntity!=null) {
			 smsdetails.setSmsTemplate(smsEntity);
			 smsdetails.setEntityName("Lead");
			 smsdetails.setModuleName(AppConstants.SERVICEMODULE);
			 smsdetails.setDocumentName(AppConstants.LEAD);
			 smsdetails.setCommunicationChannel(AppConstants.SMS);
			 String mobileNo=Long.toString(lead.getPersonInfo().getCellNumber());
			 smsdetails.setMobileNumber(mobileNo);
			 logger.log(Level.SEVERE,"Inside sendSMSToCustomer");
			return validateAndSendSMS(smsdetails);
			 }
			return null;
		}
		
		@Override
		public String sendMessageOnWhatsApp(long companyId, String mobileNo,	String actualMessage) {
			/**
			 * @author Ashwini Patil
			 * @since 1-03-2023
			 * if message contains % character then blank msg gets sent. Reported by surat pest control.
			 * As per whatsapp portal guidelines % need to be replaced with %25
			 */
			if(actualMessage!=null&&actualMessage.contains("%")) { 
				actualMessage=actualMessage.replace("%", "%25");
			}
			
			/**
			 * @author Ashwini Patil
			 * @since 26-07-2023
			 * if message contains & then message is not getting delivered
			 * need to replace & with %26
			 */
			if(actualMessage!=null&&actualMessage.contains("&")) { 
				actualMessage=actualMessage.replace("&", "%26");
			}
			
			/**
			 * @author Ashwini Patil
			 * @since 24-07-2023
			 * if message contains enter ie newline then that is going as normal space 
			 * so client is not getting expected format in whatsapp message
			 * need to replace newline with %0A
			 */
			if(actualMessage!=null&&actualMessage.contains("\n")) { 
				actualMessage=actualMessage.replace("\n", "%0A");
			}
			
			Company companyEntity = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			if(companyEntity!=null){
				if(companyEntity.getWhatsAppApiUrl()!=null && !companyEntity.getWhatsAppApiUrl().equals("") &&
						companyEntity.getWhatsAppInstaceId()!=null && !companyEntity.getWhatsAppInstaceId().equals("")){
					if(companyEntity.isWhatsAppApiStatus()){
						
						 //Prepare Url
				        URLConnection myURLConnection=null;
				        URL myURL=null;
				        BufferedReader reader=null;

				        //encoding message
//				        String encoded_message=URLEncoder.encode(message);

//				        http://wasmsapi.com/api/sendText?token=<instanceid>&phone=91xxxxxxxxxx&message=hello
//				        http://wasmsapi.com/api/sendText?token=<instanceid>&phone=<MobileNo>&message=<Message>

					 	String mainUrl = companyEntity.getWhatsAppApiUrl();
					 	
					 	String clientCell = mobileNo;
//					 	if(clientCell.length()==10){
					 		if(companyEntity.getMobileNoPrefix()!=0){
					 			clientCell = companyEntity.getMobileNoPrefix()+mobileNo+"";
					 		}
					 		
//					 	}
				        logger.log(Level.SEVERE,"clientCell"+clientCell);
				        logger.log(Level.SEVERE,"actualMessage"+actualMessage);
				        actualMessage = actualMessage.replace(" ", "%20");
				        actualMessage = actualMessage.replace("&", "and");
				        logger.log(Level.SEVERE,"actualMessage after replace "+actualMessage);

				        String mobileNoMsg = mainUrl.replace("<instanceid>", companyEntity.getWhatsAppInstaceId());
						String message = mobileNoMsg.replace("<MobileNo>",clientCell);
						String finalurlMsg = message.replace("<Message>",actualMessage);

						if(finalurlMsg.contains("<AccessToken>") && companyEntity.getAccessToken()!=null && !companyEntity.getAccessToken().equals("")) {
							finalurlMsg = finalurlMsg.replace("<AccessToken>",companyEntity.getAccessToken());
					        logger.log(Level.SEVERE,"companyEntity.getAccessToken() "+companyEntity.getAccessToken());

						}
				        logger.log(Level.SEVERE," finalurlMsg URL called==== "+finalurlMsg);
				        String data="";
				        try
				        {
				            //prepare connection
				            myURL = new URL(finalurlMsg);
				            myURLConnection = myURL.openConnection();
				            myURLConnection.setConnectTimeout(6000);
				            myURLConnection.connect();
				            reader= new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
				            //reading response
				            String response;
				            while ((response = reader.readLine()) != null)
				    		logger.log(Level.SEVERE,"response "+response);
				    		
				    		String temp;

				    		try {
				    			while ((temp = reader.readLine()) != null) {
				    				data = data + temp;
				    			}
				    		} catch (IOException e) {
				    			// TODO Auto-generated catch block
				    			logger.log(Level.SEVERE, "IOException5 ERROR::::::::::" + e);
				    			e.printStackTrace();
				    		}

				    		logger.log(Level.SEVERE, "Data::::::::::" + data);
				    		
				            //finally close connection
				            reader.close();
				    		
				    		
				        }
				        catch (IOException e)
				        {
				    		logger.log(Level.SEVERE,"Hi Inside Catch Exception");
				            e.printStackTrace();
				            return "Failed";
				        }
						System.out.println(" WhatsApp Message sent successfully");
						logger.log(Level.SEVERE," WhatsApp message sent successfully");
						
						return "WhatsApp Message sent successfully";
						
					}
					else{
						return "WhatsApp integration status is inactive";
					}
				}
				else{
					return "WhatsApp integration is not defined";
				}
			}
			return "Failed";
		}



		


		public boolean checkCommunicationChannelConfiguration(String moduleName, String documentName, String communicationChannel,String templateName, long companyId, String actualMsg, String mobileNo, boolean messagefromPopupFlag) {
			
			EmailTemplateConfiguration emailTemplateconfigurationEntity = ofy().load().type(EmailTemplateConfiguration.class)
					.filter("moduleName", moduleName).filter("documentName", documentName)
					.filter("communicationChannel", communicationChannel).filter("templateName", templateName).first().now();
			if(emailTemplateconfigurationEntity!=null){
				if(emailTemplateconfigurationEntity.isStatus()){
					logger.log(Level.SEVERE,"communicationChannel "+communicationChannel);
					logger.log(Level.SEVERE,"(emailTemplateconfigurationEntity.isAutomicMsg() "+emailTemplateconfigurationEntity.isAutomaticMsg());
					logger.log(Level.SEVERE,"emailTemplateconfigurationEntity.isSendMsgAuto() "+emailTemplateconfigurationEntity.isSendMsgAuto());
					if(messagefromPopupFlag) {
						return true;
					}
					if(emailTemplateconfigurationEntity.isAutomaticMsg() && emailTemplateconfigurationEntity.isSendMsgAuto()) {
						return true;
					}
					else {
						return false;
					}
					
//					return true;
				}
				else{
					return false;
				}
			
			}
			else{
				logger.log(Level.SEVERE,"if communication configuration not defined then sms will sent as per old logic "
						+ "to manage existing flow of SMS");
				logger.log(Level.SEVERE,"communicationChannel "+communicationChannel);
				if(actualMsg!=null && mobileNo!=null && communicationChannel.equals(AppConstants.SMS)){
					String smsResponse = sendSmsToClient(actualMsg, mobileNo, companyId, AppConstants.SMS);
					logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
				}
			}
			return false;
		}



		@Override
		public String sendMessage(String moduleName, String documentName, String templateName, Long companyId, String actualMsg, long mobileNo, boolean messagefromPopupFlag) {

			boolean smsCommunicationChannelConfigurationFlag = checkCommunicationChannelConfiguration(moduleName,documentName,AppConstants.SMS,templateName,companyId,actualMsg,mobileNo+"",messagefromPopupFlag);
			logger.log(Level.SEVERE,"smsCommunicationChannelConfigurationFlag "+smsCommunicationChannelConfigurationFlag);
			
			if(smsCommunicationChannelConfigurationFlag){
				String smsResponse = sendSmsToClient(actualMsg, mobileNo+"", companyId, AppConstants.SMS);
				logger.log(Level.SEVERE,"for sending sms smsResponse"+smsResponse);
			}
			boolean whatsAppCommunicationChannelConfigurationFlag = checkCommunicationChannelConfiguration(moduleName,documentName,AppConstants.WHATSAPP,templateName,companyId,null,null,messagefromPopupFlag);
			logger.log(Level.SEVERE,"whats app CommunicationChannelConfigurationFlag "+whatsAppCommunicationChannelConfigurationFlag);
			if(whatsAppCommunicationChannelConfigurationFlag){
				String response = sendMessageOnWhatsApp(companyId, mobileNo+"", actualMsg);
				logger.log(Level.SEVERE,"whats app response"+response);

			}
			return "Message sent successfully";
			
		}


		@Override
		public String checkConfigAndSendContractDownloadOTPMessage(long companyId, String loggedInUser, int otp) {
			// TODO Auto-generated method stub
			logger.log(Level.SEVERE,"companyId="+companyId+" loggedInUser="+loggedInUser+" otp="+otp); 
			Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			String smsResponse="",waResponse="",emailResponse="";
			if(comp!=null) {
				SmsTemplate smstemplate = ofy().load().type(SmsTemplate.class).filter("event", AppConstants.RestrictContractsDownloadWithOTP).filter("companyId", companyId).filter("status", true).first().now();
				EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
						.filter("templateName", AppConstants.RestrictContractsDownloadWithOTP)	
						.filter("templateStatus", true).first().now();
				Employee employee = ofy().load().type(Employee.class).filter("companyId",companyId).filter("fullname",loggedInUser).first().now();
				
				SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
				DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");				 
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				if(smstemplate==null&&emailTemplate==null) {
					return AppConstants.RestrictContractsDownloadWithOTP+" template does not exist in Communication Template as well as in Email Templates!";
				}
				long cellNo;
				if(employee!=null)
					cellNo=ServerAppUtility.getCompanyCellNo(employee.getBranchName(), comp);
				else
					cellNo=comp.getCellNumber1();
				
				if(smstemplate!=null) {
					logger.log(Level.SEVERE,"smstemplateEntity not null"); 					
					EmailTemplateConfiguration smstemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", "Contract")
							.filter("communicationChannel", AppConstants.SMS).filter("templateName", AppConstants.RestrictContractsDownloadWithOTP)
							.filter("status", true).first().now();
					EmailTemplateConfiguration whatsapptemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", "Contract")
							.filter("communicationChannel",  AppConstants.WHATSAPP).filter("templateName", AppConstants.RestrictContractsDownloadWithOTP)
							.filter("status", true).first().now();
					if(smstemplateconfig==null&&whatsapptemplateconfig==null)
						return AppConstants.RestrictContractsDownloadWithOTP+" template communication configuration missing! Please defile Module name:Service, Document Name: Contract and Communication channel: SMS / WhatsApp";
					else {						
						if(cellNo==0) 
							return "Could not send OTP as Cell number is missing in branch/company!";
						
						String msg=smstemplate.getMessage();
						msg=msg.replace("{OTP}", otp+"");
						msg=msg.replace("{UserName}", loggedInUser);
						if(employee!=null&&employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
							msg=msg.replace("{MobileNumber}", employee.getCellNumber1()+"");	
						else
							msg=msg.replace("{MobileNumber}", "");	
						msg=msg.replace("{DateTime}", dateFormat.format(new Date()));
						
						if(smstemplateconfig!=null) {
							logger.log(Level.SEVERE,"smstemplateconfig not null");
							String res=smsserviceimpl.sendSmsToClient(msg, cellNo+"", companyId, AppConstants.SMS);						
							logger.log(Level.SEVERE,"sms response="+res);
							if(res.equalsIgnoreCase("SMS sent successfully"))
								smsResponse="success";
							else
								smsResponse=res;
							//SMS sent successfully
						}
						if(whatsapptemplateconfig!=null) {
							logger.log(Level.SEVERE,"whatsapptemplateconfig not null");
							String res=smsserviceimpl.sendMessageOnWhatsApp(companyId, cellNo+"", msg);
							logger.log(Level.SEVERE,"wa response="+res);
							if(res.equalsIgnoreCase("WhatsApp Message sent successfully"))
								waResponse="success";
							else
								waResponse=res;
							//WhatsApp Message sent successfully
						}
					}
				}
				if(emailTemplate!=null) {
					logger.log(Level.SEVERE,"emailTemplate not null");
					EmailTemplateConfiguration emailtemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", "Contract")
							.filter("communicationChannel", AppConstants.EMAIL).filter("templateName", AppConstants.RestrictContractsDownloadWithOTP)
							.filter("status", true).first().now();
					
					if(emailtemplateconfig==null)
						return AppConstants.RestrictContractsDownloadWithOTP+" template communication configuration missing! Please defile Module name:Service, Document Name: Contract and Communication channel: Email";
					else {
						logger.log(Level.SEVERE,"emailtemplateconfig not null");
						
						String compemail="";
						if(employee!=null)
							compemail=ServerAppUtility.getCompanyEmail(employee.getBranchName(), comp);
						else
							compemail=comp.getEmail();
						
						if(compemail!=null&&!compemail.equals("")) {
							Email email = new Email();
							
							try {
								String message=emailTemplate.getEmailBody();
								String resultString = message.replaceAll("[\n]", "<br>");
								String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
								logger.log(Level.SEVERE, "message resultString1 "+resultString1);
								resultString1=resultString1.replace("{OTP}", otp+"");
								resultString1=resultString1.replace("{UserName}", loggedInUser);
								if(employee!=null&&employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
									resultString1=resultString1.replace("{MobileNumber}", employee.getCellNumber1()+"");
								resultString1=resultString1.replace("{DateTime}", dateFormat.format(new Date()));
								
								
								EmailDetails emaildetails = new EmailDetails();
								
								ArrayList<String> toemailidlist = new ArrayList<String>();
								toemailidlist.add(compemail);
								
								emaildetails.setSubject(emailTemplate.getSubject());
								emaildetails.setToEmailId(toemailidlist);
								emaildetails.setFromEmailid(compemail);
								emaildetails.setEmailBody(resultString1);
								
								String res=email.sendEmailToReceipient(emaildetails, companyId);
								logger.log(Level.SEVERE, "email response="+res);
								emailResponse="success";
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}else {
							return "Could not sent OTP as email id is missing in branch/company";
						}						
					}
					
				}
				
				if(smsResponse.equalsIgnoreCase("success")||waResponse.equalsIgnoreCase("success")||emailResponse.equalsIgnoreCase("success"))
					return "success";
				else
					return "Failed to send OTP";
			}else
				return "Unable to load company info";
			
		}



		@Override
		public String checkConfigAndSendServiceDownloadOTPMessage(long companyId, String loggedInUser, int otp) {

			// TODO Auto-generated method stub
			logger.log(Level.SEVERE,"companyId="+companyId+" loggedInUser="+loggedInUser+" otp="+otp); 
			Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			String smsResponse="",waResponse="",emailResponse="";
			if(comp!=null) {
				SmsTemplate smstemplate = ofy().load().type(SmsTemplate.class).filter("event", AppConstants.RestrictServiceDownloadWithOTP).filter("companyId", companyId).filter("status", true).first().now();
				EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
						.filter("templateName", AppConstants.RestrictServiceDownloadWithOTP)	
						.filter("templateStatus", true).first().now();
				Employee employee = ofy().load().type(Employee.class).filter("companyId",companyId).filter("fullname",loggedInUser).first().now();
				
				SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
				DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");				 
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				if(smstemplate==null&&emailTemplate==null) {
					return AppConstants.RestrictServiceDownloadWithOTP+" template does not exist in Communication Template as well as in Email Templates!";
				}
				long cellNo;
				if(employee!=null)
					cellNo=ServerAppUtility.getCompanyCellNo(employee.getBranchName(), comp);
				else
					cellNo=comp.getCellNumber1();
				
				if(smstemplate!=null) {
					logger.log(Level.SEVERE,"smstemplateEntity not null"); 					
					EmailTemplateConfiguration smstemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", "Customer Service")
							.filter("communicationChannel", AppConstants.SMS).filter("templateName", AppConstants.RestrictServiceDownloadWithOTP)
							.filter("status", true).first().now();
					EmailTemplateConfiguration whatsapptemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", "Customer Service")
							.filter("communicationChannel",  AppConstants.WHATSAPP).filter("templateName", AppConstants.RestrictServiceDownloadWithOTP)
							.filter("status", true).first().now();
					if(smstemplateconfig==null&&whatsapptemplateconfig==null)
						return AppConstants.RestrictServiceDownloadWithOTP+" template communication configuration missing! Please defile Module name:Service, Document Name: Customer Service and Communication channel: SMS / WhatsApp";
					else {						
						if(cellNo==0) 
							return "Could not send OTP as Cell number is missing in branch/company!";
						
						String msg=smstemplate.getMessage();
						msg=msg.replace("{OTP}", otp+"");
						msg=msg.replace("{UserName}", loggedInUser);
						if(employee!=null&&employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
							msg=msg.replace("{MobileNumber}", employee.getCellNumber1()+"");	
						else
							msg=msg.replace("{MobileNumber}", "");	
						msg=msg.replace("{DateTime}", dateFormat.format(new Date()));
						
						if(smstemplateconfig!=null) {
							logger.log(Level.SEVERE,"smstemplateconfig not null");
							String res=smsserviceimpl.sendSmsToClient(msg, cellNo+"", companyId, AppConstants.SMS);						
							logger.log(Level.SEVERE,"sms response="+res);
							if(res.equalsIgnoreCase("SMS sent successfully"))
								smsResponse="success";
							else
								smsResponse=res;
							//SMS sent successfully
						}
						if(whatsapptemplateconfig!=null) {
							logger.log(Level.SEVERE,"whatsapptemplateconfig not null");
							String res=smsserviceimpl.sendMessageOnWhatsApp(companyId, cellNo+"", msg);
							logger.log(Level.SEVERE,"wa response="+res);
							if(res.equalsIgnoreCase("WhatsApp Message sent successfully"))
								waResponse="success";
							else
								waResponse=res;
							//WhatsApp Message sent successfully
						}
					}
				}
				if(emailTemplate!=null) {
					logger.log(Level.SEVERE,"emailTemplate not null");
					EmailTemplateConfiguration emailtemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", "Customer Service")
							.filter("communicationChannel", AppConstants.EMAIL).filter("templateName", AppConstants.RestrictServiceDownloadWithOTP)
							.filter("status", true).first().now();
					
					if(emailtemplateconfig==null)
						return AppConstants.RestrictServiceDownloadWithOTP+" template communication configuration missing! Please defile Module name:Service, Document Name: Customer Service and Communication channel: Email";
					else {
						logger.log(Level.SEVERE,"emailtemplateconfig not null");
						
						String compemail="";
						if(employee!=null)
							compemail=ServerAppUtility.getCompanyEmail(employee.getBranchName(), comp);
						else
							compemail=comp.getEmail();
						
						if(compemail!=null&&!compemail.equals("")) {
							Email email = new Email();
							
							try {
								String message=emailTemplate.getEmailBody();
								String resultString = message.replaceAll("[\n]", "<br>");
								String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
								logger.log(Level.SEVERE, "message resultString1 "+resultString1);
								resultString1=resultString1.replace("{OTP}", otp+"");
								resultString1=resultString1.replace("{UserName}", loggedInUser);
								if(employee!=null&&employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
									resultString1=resultString1.replace("{MobileNumber}", employee.getCellNumber1()+"");
								resultString1=resultString1.replace("{DateTime}", dateFormat.format(new Date()));
								
								
								EmailDetails emaildetails = new EmailDetails();
								
								ArrayList<String> toemailidlist = new ArrayList<String>();
								toemailidlist.add(compemail);
								
								emaildetails.setSubject(emailTemplate.getSubject());
								emaildetails.setToEmailId(toemailidlist);
								emaildetails.setFromEmailid(compemail);
								emaildetails.setEmailBody(resultString1);
								
								String res=email.sendEmailToReceipient(emaildetails, companyId);
								logger.log(Level.SEVERE, "email response="+res);
								emailResponse="success";
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}else {
							return "Could not sent OTP as email id is missing in branch/company";
						}						
					}
					
				}
				
				if(smsResponse.equalsIgnoreCase("success")||waResponse.equalsIgnoreCase("success")||emailResponse.equalsIgnoreCase("success"))
					return "success";
				else
					return "Failed to send OTP";
			}else
				return "Unable to load company info";
			
		}


		@Override
		public String checkConfigAndSendWrongOTPAttemptMessage(long companyId, String loggedInUser, int otp,
				String documentname) {

			// TODO Auto-generated method stub
			logger.log(Level.SEVERE,"companyId="+companyId+" loggedInUser="+loggedInUser+" otp="+otp); 
			Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
			String smsResponse="",waResponse="",emailResponse="",error="";
			if(comp!=null) {
				SmsTemplate smstemplate = ofy().load().type(SmsTemplate.class).filter("event", AppConstants.InvalidOTPDownloadNotification).filter("companyId", companyId).filter("status", true).first().now();
				EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
						.filter("templateName", AppConstants.InvalidOTPDownloadNotification)	
						.filter("templateStatus", true).first().now();
				Employee employee = ofy().load().type(Employee.class).filter("companyId",companyId).filter("fullname",loggedInUser).first().now();
				
				SmsServiceImpl smsserviceimpl = new SmsServiceImpl();
				DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");				 
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
				
				if(smstemplate==null&&emailTemplate==null) {
					error+= AppConstants.InvalidOTPDownloadNotification+" template does not exist in Communication Template as well as in Email Templates!";
				}
				long cellNo;
				if(employee!=null)
					cellNo=ServerAppUtility.getCompanyCellNo(employee.getBranchName(), comp);
				else
					cellNo=comp.getCellNumber1();
				
				if(smstemplate!=null) {
					logger.log(Level.SEVERE,"smstemplateEntity not null"); 					
					EmailTemplateConfiguration smstemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Account Configuration").filter("documentName", "Company")
							.filter("communicationChannel", AppConstants.SMS).filter("templateName", AppConstants.InvalidOTPDownloadNotification)
							.filter("status", true).first().now();
					EmailTemplateConfiguration whatsapptemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Account Configuration").filter("documentName", "Company")
							.filter("communicationChannel",  AppConstants.WHATSAPP).filter("templateName", AppConstants.InvalidOTPDownloadNotification)
							.filter("status", true).first().now();
					if(smstemplateconfig==null&&whatsapptemplateconfig==null)
						error+= AppConstants.InvalidOTPDownloadNotification+" template communication configuration missing! Please defile Module name:Account Configuration, Document Name: Company and Communication channel: SMS / WhatsApp";
					else {	
						if(cellNo==0)
							error+= "Could not send OTP as Cell number is missing in branch/company!";
						else {
						String msg=smstemplate.getMessage();
						msg=msg.replace("{OTP}", otp+"");
						msg=msg.replace("{UserName}", loggedInUser);
						if(employee!=null&&employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
							msg=msg.replace("{MobileNumber}", employee.getCellNumber1()+"");	
						else
							msg=msg.replace("{MobileNumber}", "");	
						msg=msg.replace("{DateTime}", dateFormat.format(new Date()));
						msg=msg.replace("{DocumentName}",documentname);
						
						if(smstemplateconfig!=null) {
							logger.log(Level.SEVERE,"smstemplateconfig not null");
							String res=smsserviceimpl.sendSmsToClient(msg, cellNo+"", companyId, AppConstants.SMS);						
							logger.log(Level.SEVERE,"sms response="+res);
							if(res.equalsIgnoreCase("SMS sent successfully"))
								smsResponse="success";
							else
								smsResponse=res;
							//SMS sent successfully
						}
						if(whatsapptemplateconfig!=null) {
							logger.log(Level.SEVERE,"whatsapptemplateconfig not null");
							String res=smsserviceimpl.sendMessageOnWhatsApp(companyId, cellNo+"", msg);
							logger.log(Level.SEVERE,"wa response="+res);
							if(res.equalsIgnoreCase("WhatsApp Message sent successfully"))
								waResponse="success";
							else
								waResponse=res;
							//WhatsApp Message sent successfully
						}
						
						}
					}
				}
				if(emailTemplate!=null) {
					logger.log(Level.SEVERE,"emailTemplate not null");
					EmailTemplateConfiguration emailtemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
							.filter("companyId", companyId).filter("moduleName", "Account Configuration").filter("documentName", "Company")
							.filter("communicationChannel", AppConstants.EMAIL).filter("templateName", AppConstants.InvalidOTPDownloadNotification)
							.filter("status", true).first().now();
					
					if(emailtemplateconfig==null)
						error+= AppConstants.InvalidOTPDownloadNotification+" template communication configuration missing! Please defile Module name: Account Configuration, Document Name: Company and Communication channel: Email";
					else {
						logger.log(Level.SEVERE,"emailtemplateconfig not null");
						
						String compemail="";
						if(employee!=null)
							compemail=ServerAppUtility.getCompanyEmail(employee.getBranchName(), comp);
						else
							compemail=comp.getEmail();
						
						if(compemail!=null&&!compemail.equals("")) {
							Email email = new Email();
							
							try {
								String message=emailTemplate.getEmailBody();
								String resultString = message.replaceAll("[\n]", "<br>");
								String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
								logger.log(Level.SEVERE, "message resultString1 "+resultString1);
								resultString1=resultString1.replace("{OTP}", otp+"");
								resultString1=resultString1.replace("{UserName}", loggedInUser);
								if(employee!=null&&employee.getCellNumber1()!=null&&employee.getCellNumber1()>0)
									resultString1=resultString1.replace("{MobileNumber}", employee.getCellNumber1()+"");
								resultString1=resultString1.replace("{DateTime}", dateFormat.format(new Date()));
								resultString1=resultString1.replace("{DocumentName}",documentname);
								
								
								EmailDetails emaildetails = new EmailDetails();
								
								ArrayList<String> toemailidlist = new ArrayList<String>();
								toemailidlist.add(compemail);
								
								emaildetails.setSubject(emailTemplate.getSubject());
								emaildetails.setToEmailId(toemailidlist);
								emaildetails.setFromEmailid(compemail);
								emaildetails.setEmailBody(resultString1);
								
								String res=email.sendEmailToReceipient(emaildetails, companyId);
								logger.log(Level.SEVERE, "email response="+res);
								emailResponse="success";
								
							} catch (Exception e) {
								e.printStackTrace();
							}
							
						}else {
							return "Could not sent OTP as email id is missing in branch/company";
						}						
					}
					
				}
				
				if(smsResponse.equalsIgnoreCase("success")||waResponse.equalsIgnoreCase("success")||emailResponse.equalsIgnoreCase("success"))
					return "success";
				else
					return error;
			}else
				return "Unable to load company info";
			
		
		}
		
	
				@Override
				public java.lang.String checkEmailConfigAndSendComplaintResolvedEmail(
						long companyId, Date serviceDate,
						java.lang.String customerName, int complaintId,
						java.lang.String companyName, long custCellNo,
						int customerId) {

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					sdf.setTimeZone(TimeZone.getTimeZone("IST"));
					Complain complain = ofy().load().type(Complain.class).filter("companyId", companyId).filter("count", complaintId).first().now();
					Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
					Customer cust=ofy().load().type(Customer.class).filter("companyId", companyId).filter("count",customerId).first().now();
					
					String eventName="Complaint Completed";
					String complaintClosingDate="";
					Service service = ofy().load().type(Service.class).filter("companyId",companyId).filter("ticketNumber",complaintId).first().now();
					if(service!=null&&!service.getServiceType().equals("")&&service.getServiceType().equals("Complaint Service")){
						eventName="Complaint Service Completed";
					}
					
					EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
							.filter("templateName",eventName)	
							.filter("templateStatus", true).first().now();
							
					if(emailTemplate!=null) {
						logger.log(Level.SEVERE,"emailTemplate not null");
						EmailTemplateConfiguration emailtemplateconfig = ofy().load().type(EmailTemplateConfiguration.class)
								.filter("companyId", companyId).filter("moduleName", "Service").filter("documentName", AppConstants.CUSTOMERSUPPORT)
								.filter("communicationChannel", AppConstants.EMAIL).filter("templateName", eventName)
								.filter("status", true).first().now();
						
						if(emailtemplateconfig==null)
							logger.log(Level.SEVERE,eventName+" template communication configuration missing! Please defile Module name:Service, Document Name: Customer Support and Communication channel: Email");
						else {
							logger.log(Level.SEVERE,"emailtemplateconfig not null");
							
							String compemail="";
							if(complain!=null)
								compemail=ServerAppUtility.getCompanyEmail(complain.getBranch(), comp);
							else
								compemail=comp.getEmail();
							
							if(compemail!=null&&!compemail.equals("")) {
								Email email = new Email();
								
								try {
									String message=emailTemplate.getEmailBody();
									String resultString = message.replaceAll("[\n]", "<br>");
									String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
									logger.log(Level.SEVERE, "message resultString1 "+resultString1);
									if(resultString1.contains("{CustomerName}")) {
										resultString1=resultString1.replace("{CustomerName}",complain.getPersoninfo().getFullName());	
									}
									if(resultString1.contains("{ComplainID}")) {
										resultString1=resultString1.replace("{ComplainID}","Complain ID: "+complain.getCount()+"");	
									}
									if(resultString1.contains("{DateTime}")) {
										resultString1=resultString1.replace("{DateTime}","Date and Time: "+sdf.format(complain.getDate())+"");	
									}
									if(resultString1.contains("{Description}")) {
										if(complain.getDestription()!=null&&!complain.getDestription().equals(""))
											resultString1=resultString1.replace("{Description}","Description: "+complain.getDestription());	
										else
											resultString1=resultString1.replace("{Description}","");	
									}
									if(resultString1.contains("{Status}")) {
										resultString1=resultString1.replace("{Status}","Status: "+complain.getCompStatus());	
									}
									if(resultString1.contains("{CustomerId}")) {
										resultString1=resultString1.replace("{CustomerId}","Customer ID: "+complain.getPersoninfo().getCount()+"");	
									}
									if(resultString1.contains("{ServiceId}")) {
										if(service!=null)
											resultString1=resultString1.replace("{ServiceId}","Service ID: "+service.getCount());	
										else
											resultString1=resultString1.replace("{ServiceId}","");
									}
									if(resultString1.contains("{ProductName}")) {
										if(complain.getPic()!=null)
											resultString1=resultString1.replace("{ProductName}","Product Name: "+complain.getPic().getProductName());	
										else
											resultString1=resultString1.replace("{ProductName}","");	
									}
									if(resultString1.contains("{ServiceCompletionDate}")) {
										if(service!=null&&service.getCompletedDate()!=null)
											resultString1=resultString1.replace("{ServiceCompletionDate}","Service Completion Date: "+sdf.format(service.getCompletedDate()));	
										else
											resultString1=resultString1.replace("{ServiceCompletionDate}","");	
									}
									if(resultString1.contains("{CompanySignature}")) {
										resultString1=resultString1.replace("{CompanySignature}",ServerAppUtility.getCompanySignature(complain.getBranch(),complain.getCompanyId()));	
									}
									
									EmailDetails emaildetails = new EmailDetails();
									
									ArrayList<String> toemailidlist = new ArrayList<String>();
									if(cust!=null&&cust.getEmail()!=null&&!cust.getEmail().equals(""))
										toemailidlist.add(cust.getEmail());
									
									emaildetails.setSubject(emailTemplate.getSubject());
									emaildetails.setToEmailId(toemailidlist);
									emaildetails.setFromEmailid(compemail);
									emaildetails.setEmailBody(resultString1);
									if(toemailidlist.size()>0) {
										String res=email.sendEmailToReceipient(emaildetails, companyId);
										logger.log(Level.SEVERE, "email response="+res);
										return res;
									}
									else{
										logger.log(Level.SEVERE, "could not send email as email id is missing in customer "+customerName+"/"+customerId);
										return "Error: could not send email as email id is missing in customer "+customerName+"/"+customerId;
									}
									} catch (Exception e) {
									e.printStackTrace();
								}
								
							}else {
								logger.log(Level.SEVERE, "Could not sent OTP as email id is missing in branch/company");
								return "Error: Could not sent OTP as email id is missing in branch/company";
							}						
						}
						
					}else{
						logger.log(Level.SEVERE,eventName+"email  template not defined");	
						return "Error: "+eventName+"email  template not defined";
					}
					
				
					return "Error: Failed";
				}
		
		


}
