package com.slicktechnologies.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gwt.i18n.client.NumberFormat;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.client.views.contract.ContractForm;
import com.slicktechnologies.server.clientsms.SmsServiceImpl;
import com.slicktechnologies.server.email.SendGridEmailServlet;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.Quotation;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessprocesslayer.Lead;
import com.slicktechnologies.shared.common.businessunitlayer.Branch;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.contractrenewal.ContractRenewal;
import com.slicktechnologies.shared.common.email.EmailDetails;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.DocumentUpload;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.SmsConfiguration;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.personlayer.Employee;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ContractInvoicePayment extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3233792410883343541L;
	
	
	Logger logger = Logger.getLogger("Logger");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Company companyEntity = null;
		String branchName = "";
		String salesPerson = "";
		String	customerName="";
		String customerCellNo = "";
		String callingSystem="Other";
		double netPayble = 0;
		String productList="";
		try {
		
//		resp.setHeader("Access-Control-Allow-Origin", AppConstants.PAYMNETGATEWAY);
		resp.setHeader("Access-Control-Allow-Origin", "*");


		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		dateTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		String jsonString = "";
		try {
			jsonString =req.getParameter("contractDetails");
			logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);
		}catch(Exception e) {
			
		}
		
		//Ashwini Patil Date:18-05-2023 For Zoho integration data required to be passed in body in json form
		String data="";
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(req.getInputStream())); 
			String temp;
			
			while ((temp = br.readLine()) != null) {
				data = data + temp+"\n"+"\n";
			}
			logger.log(Level.SEVERE,"data data::::::::::"+data);		
		}catch(Exception e) {
			logger.log(Level.SEVERE,"no data found");
		}
			
		JSONObject jsonobj = null;
		try {
			if(data!=null&!data.equals(""))
				jsonobj = new JSONObject(data);
			else
				jsonobj = new JSONObject(jsonString);
		} catch (JSONException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception =="+ e.getLocalizedMessage());
			resp.getWriter().println("Json data is not valid data. May contains special character");
			return;
		}
		
		if(jsonobj!=null){
//			long authcode = 5348024557502464l; //local
			long companyId = 0;
			
			
			List<Company> companylist = ofy().load().type(Company.class).list();
			label : for(Company comp : companylist){
				if(comp.getAccessUrl().equalsIgnoreCase("my")){
					companyId = comp.getCompanyId();
					companyEntity = comp;
					break;
				}
			}
			logger.log(Level.SEVERE, "authcode companyId" + companyId);

				String apiCallFromPaymentGateway =  jsonobj.optString("callfrompaymentGateway");
				String ipAddress = "";
				if(jsonobj.optString("IPAddress")!=null){
					ipAddress = jsonobj.optString("IPAddress");
				}
				
				String status =  jsonobj.optString("status");
				logger.log(Level.SEVERE, "status " + status);
				
				String referenceNumber = "";
				if(jsonobj.optString("evaReference")!=null){
					referenceNumber = jsonobj.optString("evaReference");
				}
				logger.log(Level.SEVERE, "evaReference"+referenceNumber);
				ServerAppUtility apputility = new ServerAppUtility();
				
				String	transactionType = "";
				if(jsonobj.optString("transactionType")!=null){
					transactionType = apputility.decodeText(jsonobj.optString("transactionType"));
				}
				logger.log(Level.SEVERE, "transactionType " + transactionType);
				
				
				boolean createOnlyContractAndServicesFlag=false;
				try {
					if(jsonobj.optString("CreateOnlyContractAndServices")!=null){
						String flag=jsonobj.optString("CreateOnlyContractAndServices");
						if(flag.equalsIgnoreCase("yes")||flag.equalsIgnoreCase("true"))
							createOnlyContractAndServicesFlag=true;
						
					}
				}catch(Exception e) {
					logger.log(Level.SEVERE, "createOnlyServices key not found");
				}
				logger.log(Level.SEVERE, "createOnlyContractAndServicesFlag="+createOnlyContractAndServicesFlag);
				
				
					/*
					Ashwini Patil 
					Date:18-10-2023 
					Rex is using zoho for creating quotation and they are creating contract in eva using contractinvoicepaymnet api.
					When they multiple times change quotation status to won, multiple contracts getting created for same quotation.
					Requirement is if contract exist for zoho quotation id then do not create new contract. Update the existing contract.
					*/
				String zohoQuotID = "";
				try {
				if(jsonobj.optString("zohoQuotID")!=null){
					zohoQuotID = jsonobj.optString("zohoQuotID");	
					callingSystem="ZOHO";
				}
				}catch(Exception e) {
				}
				logger.log(Level.SEVERE, "zohoQuotID"+zohoQuotID);
				

				
				/**
				 * @author Vijay  below if condition will execute when this API will call from Payment Gateway
				 */
				if(apiCallFromPaymentGateway!=null && !apiCallFromPaymentGateway.equals("") && apiCallFromPaymentGateway.equalsIgnoreCase("Yes")){
					logger.log(Level.SEVERE, "apiCallFromPaymentGateway  for payment gateway " + apiCallFromPaymentGateway);

					try {
						if(apputility.decodeText(transactionType)==null || apputility.decodeText(transactionType).equals("")){
							logger.log(Level.SEVERE, "transactionType cant be null or blank" );
							resp.getWriter().println("Failed - transactionType cant be null or blank");
						}
					} catch (Exception e) {
						resp.getWriter().println("Failed - transactionType cant be null or blank");
					}
					
					try {
						
					String razorpayReference = "";
					if(jsonobj.optString("razorpayReference")!=null){
						razorpayReference = jsonobj.optString("razorpayReference");
					}
					
					try {
						if(jsonobj.optString("documentType")==null || jsonobj.optString("documentType").equals("")){
							logger.log(Level.SEVERE, "documentType cant be null or blank" );
							resp.getWriter().println("Failed - documentType cant be null or blank");
						}
					} catch (Exception e) {
						resp.getWriter().println("Failed - documentType cant be null or blank");
					}
					
					Date acceptanceDateTime = null;
					
					try {
						if(jsonobj.optString("Date and Time")==null || jsonobj.optString("Date and Time").equals("")){
							logger.log(Level.SEVERE, "Date and Time cant be null or blank" );
							resp.getWriter().println("Failed - Date and Time cant be null or blank");
						}
						else {
							String strAcceptanceDateTime = jsonobj.optString("Date and Time");
							acceptanceDateTime = dateTimeFormat.parse(strAcceptanceDateTime);
							logger.log(Level.SEVERE, "acceptanceDateTime"+acceptanceDateTime );

						}
					} catch (Exception e) {
						resp.getWriter().println("Failed - Date and Time cant be null or blank");
						e.printStackTrace();
					}
					
					logger.log(Level.SEVERE, "after some mandatory fields" ); 
//					Pay & Confirm Now 
//					Confirm Now & Pay Later
					
//					String transactionType = apputility.decodeText(jsonobj.optString("transactionType"));
					
//					String	transactionType = apputility.decodeText(jsonobj.optString("transactionType"));
//
//					logger.log(Level.SEVERE, "transactionType " + transactionType);
//					
//					try {
//						if(apputility.decodeText(transactionType)==null || apputility.decodeText(transactionType).equals("")){
//							logger.log(Level.SEVERE, "transactionType cant be null or blank" );
//							resp.getWriter().println("Failed - transactionType cant be null or blank");
//						}
//					} catch (Exception e) {
//						resp.getWriter().println("Failed - transactionType cant be null or blank");
//					}
					
					

					String documentType = jsonobj.optString("documentType");
					logger.log(Level.SEVERE, "documentType " + documentType);
					
					
					if(documentType.trim().equals(AppConstants.LEAD)){
						logger.log(Level.SEVERE, "inside lead referenceNumber " + referenceNumber);
						Lead leadEntity = ofy().load().type(Lead.class).filter("companyId", companyId)
								.filter("paymentGatewayUniqueId", referenceNumber).first().now();
							logger.log(Level.SEVERE, "leadEntity " + leadEntity);
						
						if(status.equalsIgnoreCase("Successful")){
							try {
								
							if(leadEntity!=null){
								String msg = "";
								ArrayList<SalesLineItem> productlist = new ArrayList<SalesLineItem>();
								/**
								 * @author Vijay Date 11-02-2022
								 * Des :- if duplicate Sr no found in product list then here not processing payment document
								 * issue pecopp having some lead duplicate SR no so creating double services
								 */
								boolean duplicateSrNoflag = validateDuplicateProductSrNo(leadEntity.getLeadProducts());
								logger.log(Level.SEVERE, "duplicateSrNoflag " + duplicateSrNoflag);
								if(duplicateSrNoflag==false) {
									return;
								}
								logger.log(Level.SEVERE, "duplicateSrNoflag ==" + duplicateSrNoflag);

								for(SalesLineItem items : leadEntity.getLeadProducts()){
									ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
									scheduleBranchlist = getshceduleBranchServiceAddress(leadEntity.getBranch(), items.getUnitOfMeasurement());
									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
									customerBranchlist.put(items.getProductSrNo(), scheduleBranchlist);
									items.setCustomerBranchSchedulingInfo(customerBranchlist);
									productlist.add(items);
								}
								leadEntity.setLeadProducts(productlist);
								
								String paymentMethod = "Online";
								validatepaymentMethod(paymentMethod,companyId);
								String numberRange = "";
								if(leadEntity.getNumberRange()!=null && !leadEntity.getNumberRange().equals("")){
									numberRange = leadEntity.getNumberRange();
								}
								else{
									numberRange = "Internet";
								}
								
								//By Ashwini Patil Date: 4-03-2022 to avoid duplicate contract creation one more condition is added to if
								Contract conEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
										.filter("leadCount", leadEntity.getCount()).first().now();
								logger.log(Level.SEVERE, "conEntity against lead= " + conEntity);
								
								if(!leadEntity.getStatus().equals(Quotation.QUOTATIONSUCESSFUL)&&conEntity==null){
									msg = reactOnCreateBillInvoicePayment(leadEntity.getPersonInfo(),leadEntity.getBranch(),leadEntity.getEmployee(),
											paymentMethod,leadEntity.getEmployee(),leadEntity.getGroup(),leadEntity.getCategory(),
											leadEntity.getType(),leadEntity.getLeadProducts(),referenceNumber,companyId,
											leadEntity.getTotalAmount(),leadEntity.getDiscountAmt(),leadEntity.getFinalTotalAmt(),
											leadEntity.getRoundOffAmt(),leadEntity.getNetpayable(),leadEntity.getProductTaxes(),leadEntity.getCount(),
											razorpayReference,leadEntity.getDescription(),0,numberRange,transactionType,ipAddress,documentType,leadEntity.getCount(),null,acceptanceDateTime,null,createOnlyContractAndServicesFlag,zohoQuotID);
						
									logger.log(Level.SEVERE, "msg" + msg);
									if(msg!=null && !msg.equals("")){
										resp.getWriter().println(msg);
									}
									else{
//										resp.getWriter().println("Failed - Contract Invoice Payment Closed Successfully");
										
//										sendTransactionEmail(leadEntity.getCount(),documentType,leadEntity.getPersonInfo().getFullName(),
//												leadEntity.getPersonInfo().getCellNumber(), leadEntity.getPersonInfo().getCount(),
//												0,acceptanceDateTime,razorpayReference,leadEntity.getNetpayable(),transactionType,status,"PG_PaymentSuccessful",leadEntity.getCompanyId(),
//												leadEntity.getBranch());
									}
								}
								else{
									logger.log(Level.SEVERE, "Failed - Lead is already Successful Please check lead status");
									resp.getWriter().println("Failed - Lead is already Successful Please check lead status");
								}
								 
							}
							else {
								logger.log(Level.SEVERE, "Failed - evaReference not found in ERP");
								resp.getWriter().println("Failed - evaReference not found in ERP");
							}
							
							} catch (Exception e) {
								e.printStackTrace();
								logger.log(Level.SEVERE, "Failed - "+e.getMessage());
								resp.getWriter().println("Failed - "+e.getMessage());
								sendemail(companyEntity,"Lead Payment Gateway","Lead",leadEntity.getCount());
							}
						}
						else{
							if(status.equalsIgnoreCase("Unsuccessful")){
								sendUnsucessfulSMSToCustomer(leadEntity.getCompanyId(),leadEntity.getPersonInfo().getFullName(),
										leadEntity.getPersonInfo().getCellNumber(), leadEntity.getNetpayable(),AppConstants.SERVICEMODULE,AppConstants.LEAD);
								
								sendTransactionEmail(leadEntity.getCount(),documentType,leadEntity.getPersonInfo().getFullName(),
										leadEntity.getPersonInfo().getCellNumber(), leadEntity.getPersonInfo().getCount(),
										0,acceptanceDateTime,razorpayReference,leadEntity.getNetpayable(),transactionType,status,"PG_PaymentUnsucessful",leadEntity.getCompanyId(),
										leadEntity.getBranch());
							}
							else{
								resp.getWriter().println("Failed - status can not be blank or null");
							}
						}
					}
					else if(documentType.trim().equals(AppConstants.QUOTATION) ){

						logger.log(Level.SEVERE, "Quotation referenceNumber " + referenceNumber);
						
						try{
							
						Quotation quotationEntity = ofy().load().type(Quotation.class).filter("companyId", companyId)
								.filter("paymentGatewayUniqueId", referenceNumber).first().now();
							logger.log(Level.SEVERE, "quotationEntity " + quotationEntity);
							
						if(status.equalsIgnoreCase("Successful")){
							
							if(quotationEntity!=null){
								String msg = "";
								ArrayList<SalesLineItem> productlist = new ArrayList<SalesLineItem>();
								
								/**
								 * @author Vijay Date 11-02-2022
								 * Des :- if duplicate Sr no found in product list then here not processing payment document
								 * issue pecopp having some lead duplicate SR no so creating double services
								 */
								boolean duplicateSrNoflag = validateDuplicateProductSrNo(quotationEntity.getItems());
								logger.log(Level.SEVERE, "duplicateSrNoflag " + duplicateSrNoflag);
								if(duplicateSrNoflag==false) {
									return;
								}
								logger.log(Level.SEVERE, "duplicateSrNoflag ==" + duplicateSrNoflag);
								
//								for(SalesLineItem items : quotationEntity.getItems()){
//									ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
//									scheduleBranchlist = getshceduleBranchServiceAddress(quotationEntity.getBranch(), items.getUnitOfMeasurement());
//									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
//									customerBranchlist.put(items.getProductSrNo(), scheduleBranchlist);
//									items.setCustomerBranchSchedulingInfo(customerBranchlist);
//									productlist.add(items);
//								}
//								quotationEntity.setItems(productlist);
								
								String paymentMethod = "Online";
								validatepaymentMethod(paymentMethod,companyId);
								String numberRange = "";
								if(quotationEntity.getQuotationNumberRange()!=null && !quotationEntity.getQuotationNumberRange().equals("")){
									numberRange = quotationEntity.getQuotationNumberRange();
								}
								else{
									numberRange = "Internet";
								}
								
								//By Ashwini Patil Date: 4-03-2022 to avoid duplicate contract creation one more condition is added to if
								Contract conEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
										.filter("quotationCount", quotationEntity.getCount()).first().now();
								logger.log(Level.SEVERE, "conEntity against quotaion=" + conEntity);
								
								if(!quotationEntity.getStatus().equals(Quotation.QUOTATIONSUCESSFUL)&& conEntity==null){
									msg = reactOnCreateBillInvoicePayment(quotationEntity.getCinfo(),quotationEntity.getBranch(),quotationEntity.getEmployee(),
											paymentMethod,quotationEntity.getApproverName(),quotationEntity.getGroup(),quotationEntity.getCategory(),
											quotationEntity.getType(),quotationEntity.getItems(),referenceNumber,companyId,
											quotationEntity.getTotalAmount(),quotationEntity.getDiscountAmt(),quotationEntity.getFinalTotalAmt(),
											quotationEntity.getRoundOffAmt(),quotationEntity.getNetpayable(),quotationEntity.getProductTaxes(),quotationEntity.getCount(),
											razorpayReference,quotationEntity.getDescription(),0,numberRange,transactionType,ipAddress,documentType,quotationEntity.getLeadCount(),quotationEntity.getServiceScheduleList(),acceptanceDateTime,null,createOnlyContractAndServicesFlag,zohoQuotID);
						
									logger.log(Level.SEVERE, "msg" + msg);
									if(msg!=null && !msg.equals("")){
//										if(msg.equals("Success")) {
//											sendTransactionEmail(quotationEntity.getCount(),documentType,quotationEntity.getCinfo().getFullName(),
//													quotationEntity.getCinfo().getCellNumber(), quotationEntity.getCinfo().getCount(),
//													0,acceptanceDateTime,razorpayReference,quotationEntity.getNetpayable(),transactionType,status,"PG_PaymentSuccessful",quotationEntity.getCompanyId(),
//													quotationEntity.getBranch());
//										}
										resp.getWriter().println(msg);
									}
									else{
//										resp.getWriter().println("Contract Invoice Payment Closed Successfully");
										
										
									}
								}
								else{
									logger.log(Level.SEVERE, "Quotation is already Successful Please check quotation status");
									resp.getWriter().println("Failed - Quotation is already Successful Please check lead status");
								}
								
								 
							}
							else {
								logger.log(Level.SEVERE, "evaReference not found in ERP");
								resp.getWriter().println("Failed - evaReference not found in ERP");
							}
							
						}
						else{

							if(status.equalsIgnoreCase("Unsuccessful")){
								sendUnsucessfulSMSToCustomer(quotationEntity.getCompanyId(),quotationEntity.getCinfo().getFullName(),
										quotationEntity.getCinfo().getCellNumber(), quotationEntity.getNetpayable(),AppConstants.SERVICEMODULE,AppConstants.QUOTATION);
								
								sendTransactionEmail(quotationEntity.getCount(),documentType,quotationEntity.getCinfo().getFullName(),
										quotationEntity.getCinfo().getCellNumber(), quotationEntity.getCinfo().getCount(),
										0,acceptanceDateTime,razorpayReference,quotationEntity.getNetpayable(),transactionType,status,"PG_PaymentUnsucessful",quotationEntity.getCompanyId(),
										quotationEntity.getBranch());
							}
							else{
								logger.log(Level.SEVERE, "Failed - status can not be blank or null");
								resp.getWriter().println("Failed - status can not be blank or null");
							}
						}
						
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Quotation payment gateway Exception" + e.getMessage());
						resp.getWriter().println("failed - "+e.getMessage());
					}
					
					}
					else if(documentType.trim().equals(AppConstants.CONTRACT) ){
						
						logger.log(Level.SEVERE, "Contract referenceNumber " + referenceNumber);
						try{
							
						Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
								.filter("paymentGatewayUniqueId", referenceNumber).first().now();
							logger.log(Level.SEVERE, "contractEntity " + contractEntity);
						String msg = "";
						String paymentMethod = "Online";

						logger.log(Level.SEVERE, "inside contract transactionType" + transactionType);


						/**
						 * @author Vijay Date 11-02-2022
						 * Des :- if duplicate Sr no found in product list then here not processing payment document
						 * issue pecopp having some lead duplicate SR no so creating double services
						 */
						boolean duplicateSrNoflag = validateDuplicateProductSrNo(contractEntity.getItems());
						logger.log(Level.SEVERE, "duplicateSrNoflag " + duplicateSrNoflag);
						if(duplicateSrNoflag==false) {
							return;
						}
						logger.log(Level.SEVERE, "duplicateSrNoflag ==" + duplicateSrNoflag);
						
						if(status.equalsIgnoreCase("Successful") && transactionType.equalsIgnoreCase("Pay & Confirm Now")){
							if(contractEntity!=null){
							
								List<BillingDocument> billingList = ofy().load().type(BillingDocument.class).filter("contractCount", contractEntity.getCount())
														.filter("companyId", companyId)	.list();
								if(billingList.size()==1){
									if(billingList.get(0).getStatus().equals(BillingDocument.CREATED)){
										double totalbillingAmt = billingList.get(0).getTotalBillingAmount();
										logger.log(Level.SEVERE, "totalbillingAmt" + totalbillingAmt);
										billingList.get(0).setTotalBillingAmount(Math.round(totalbillingAmt));
										logger.log(Level.SEVERE, "Math.round(totalbillingAmt)" + Math.round(totalbillingAmt));

										msg = createInvoice(billingList.get(0),paymentMethod,0,contractEntity, false);
										logger.log(Level.SEVERE, "msg == " + msg);
										
										if(acceptanceDateTime!=null) {
											contractEntity.setAcceptedDateTime(acceptanceDateTime);
											ofy().save().entity(contractEntity);
										}

										sendTransactionEmail(contractEntity.getCount(),documentType,contractEntity.getCinfo().getFullName(),
												contractEntity.getCinfo().getCellNumber(), contractEntity.getCinfo().getCount(),
												contractEntity.getCount(),acceptanceDateTime,razorpayReference,contractEntity.getNetpayable(),transactionType,"Successful","PG_PaymentSuccessful",contractEntity.getCompanyId(),
												contractEntity.getBranch());
									}
									else{
										logger.log(Level.SEVERE, "billingList size" + billingList.size());
										resp.getWriter().println("Failed - Billing document should be in created status");
									}
								}
								else{
									logger.log(Level.SEVERE, "Failed - Multiple billing document found ");
									resp.getWriter().println("Failed - Multiple billing document found ");
								}


							}
							else {
								logger.log(Level.SEVERE, "Failed - evaReference not found in ERP");
								resp.getWriter().println("Failed - evaReference not found in ERP");
							}
							
						}
						else{
							if(status.equalsIgnoreCase("Unsuccessful")){
								sendUnsucessfulSMSToCustomer(contractEntity.getCompanyId(),contractEntity.getCinfo().getFullName(),
										contractEntity.getCinfo().getCellNumber(), contractEntity.getNetpayable(),AppConstants.SERVICEMODULE,AppConstants.CONTRACT);
								
								sendTransactionEmail(contractEntity.getCount(),documentType,contractEntity.getCinfo().getFullName(),
										contractEntity.getCinfo().getCellNumber(), contractEntity.getCinfo().getCount(),
										contractEntity.getCount(),acceptanceDateTime,razorpayReference,contractEntity.getNetpayable(),transactionType,status,"PG_PaymentUnsucessful",contractEntity.getCompanyId(),
										contractEntity.getBranch());
							}
							else{
								logger.log(Level.SEVERE, "Failed - status can not be blank or null");
								resp.getWriter().println("Failed - status can not be blank or null");
							}
						}
						
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Contract payment gateway Exception" + e.getMessage());
						resp.getWriter().println("Failed - "+e.getMessage());
					}
						
					}
					else if(documentType.trim().equals(AppConstants.CONTRACT_RENEWAL)){

						logger.log(Level.SEVERE, "Contract Renewal referenceNumber " + referenceNumber);
						try{
							
						ContractRenewal contractRenewalEntity = ofy().load().type(ContractRenewal.class).filter("companyId", companyId)
								.filter("paymentGatewayUniqueId", referenceNumber).filter("status", AppConstants.PROCESSED).first().now();
							logger.log(Level.SEVERE, "contractRenewalEntity " + contractRenewalEntity);
							
						if(status.equalsIgnoreCase("Successful")){
							
							if(contractRenewalEntity!=null){
								String msg = "";
								ArrayList<SalesLineItem> productlist = new ArrayList<SalesLineItem>();
								
//								for(SalesLineItem items : contractRenewalEntity.getItems()){
//									ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
//									scheduleBranchlist = getshceduleBranchServiceAddress(contractRenewalEntity.getBranch(), items.getUnitOfMeasurement());
//									HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
//									customerBranchlist.put(items.getProductSrNo(), scheduleBranchlist);
//									items.setCustomerBranchSchedulingInfo(customerBranchlist);
//									productlist.add(items);
//								}
//								contractRenewalEntity.setItems(productlist);
								
								Contract contractEntity = ofy().load().type(Contract.class).filter("companyId", companyId)
															.filter("count", contractRenewalEntity.getContractId()).first().now();
								
								String paymentMethod = "Online";
								validatepaymentMethod(paymentMethod,companyId);
//								String numberRange = "Internet";
								String numberRange = "";
								if(contractEntity.getNumberRange()!=null && !contractEntity.getNumberRange().equals("")){
									numberRange = contractEntity.getNumberRange();
								}
								else{
									numberRange = "Internet";
								}
								
//								double totalAmount =0;
//								for(SalesLineItem item : contractRenewalEntity.getItems()){
//									totalAmount += item.getTotalAmount();
//									
//									item.setStartDate(DateUtility.getDateWithTimeZone("IST", new Date()));
//									item.setEndDate(getProductEndDate(item.getStartDate(),item.getDuration()));
//								}
//								ContractInvoicePayment contractPayment = new ContractInvoicePayment();
//								
//								ArrayList<ProductOtherCharges> prodTaxeslist = new ArrayList<ProductOtherCharges>();
//								prodTaxeslist = contractPayment.getproductTaxeslist(contractRenewalEntity.getItems());
//								DecimalFormat decimalformat = new DecimalFormat();
//								for(ProductOtherCharges othercharges : prodTaxeslist){
//									double calcAmt=othercharges.getAssessableAmount()*othercharges.getChargePercent()/100;
//									decimalformat.format(calcAmt);
//									othercharges.setChargePayable(calcAmt);
//								}
//								logger.log(Level.SEVERE, "prodTaxeslist size " + prodTaxeslist.size());
//								logger.log(Level.SEVERE, "contractRenewalEntity.getContractId() " +contractRenewalEntity.getContractId());

								
								double totalAmount =0;
								for(SalesLineItem item : contractRenewalEntity.getItems()){
									totalAmount += item.getPrice();
									logger.log(Level.SEVERE, "item.getTotalAmount" + totalAmount);
									item.setTotalAmount(item.getPrice());
									item.setStartDate(DateUtility.getDateWithTimeZone("IST", new Date()));
									item.setEndDate(getProductEndDate(item.getStartDate(),item.getDuration()));
									if(item.getServiceTax()!=null)
									item.setServiceTaxEdit(item.getServiceTax().getTaxConfigName());
									if(item.getVatTax()!=null)
									item.setVatTaxEdit(item.getVatTax().getTaxConfigName());
								}
								ContractInvoicePayment contractPayment = new ContractInvoicePayment();
								
								ArrayList<ProductOtherCharges> prodTaxeslist = new ArrayList<ProductOtherCharges>();
								prodTaxeslist = contractPayment.getproductTaxeslist(contractRenewalEntity.getItems());
								DecimalFormat decimalformat = new DecimalFormat();
								for(ProductOtherCharges othercharges : prodTaxeslist){
									double calcAmt=othercharges.getAssessableAmount()*othercharges.getChargePercent()/100;
									decimalformat.format(calcAmt);
									othercharges.setChargePayable(calcAmt);
									logger.log(Level.SEVERE, "othercharges.getAssessableAmount()" + othercharges.getAssessableAmount());
									logger.log(Level.SEVERE, "othercharges.getChargePercent()" + othercharges.getChargePercent());

									logger.log(Level.SEVERE, "calcAmt" + calcAmt);
								}
								
								logger.log(Level.SEVERE, "prodTaxeslist size " + prodTaxeslist.size());
							
								
								if(!contractEntity.isRenewFlag()){
									msg = reactOnCreateBillInvoicePayment(contractEntity.getCinfo(),contractRenewalEntity.getBranch(),contractRenewalEntity.getSalesPerson(),
											paymentMethod,contractRenewalEntity.getSalesPerson(),contractEntity.getGroup(),contractEntity.getCategory(),
											contractEntity.getType(),contractRenewalEntity.getItems(),referenceNumber,companyId,
											totalAmount,0,totalAmount,
											0,contractRenewalEntity.getNetPayable(),prodTaxeslist,contractRenewalEntity.getCount(),
											razorpayReference,contractRenewalEntity.getDescription(),0,numberRange,transactionType,ipAddress,documentType,contractRenewalEntity.getContractId(),null,acceptanceDateTime,null,createOnlyContractAndServicesFlag,zohoQuotID);
						
									logger.log(Level.SEVERE, "msg" + msg);
									if(msg!=null && !msg.equals("")){
										resp.getWriter().println(msg);
									}
									
								}
								else{
									logger.log(Level.SEVERE, "Failed - This contract is already renewed can not proceed further");
									resp.getWriter().println("Failed - This contract is already renewed can not proceed further");
								}
							}
							else {
								logger.log(Level.SEVERE, "Failed - evaReference not found in ERP");
								resp.getWriter().println("Failed - evaReference not found in ERP");
							}
							
						}
						else{
							if(status.equalsIgnoreCase("Unsuccessful")){
								sendUnsucessfulSMSToCustomer(contractRenewalEntity.getCompanyId(),contractRenewalEntity.getCustomerName(),
										contractRenewalEntity.getCustomerCell(), contractRenewalEntity.getNetPayable(),AppConstants.SERVICEMODULE,AppConstants.CONTRACT_RENEWAL);
								
								sendTransactionEmail(contractRenewalEntity.getContractId(),documentType,contractRenewalEntity.getCustomerName(),
										contractRenewalEntity.getCustomerCell(), contractRenewalEntity.getCustomerId(),
										0,acceptanceDateTime,razorpayReference,contractRenewalEntity.getNetPayable(),transactionType,status,"PG_PaymentUnsucessful",contractRenewalEntity.getCompanyId(),
										contractRenewalEntity.getBranch());
							}
							else{
								logger.log(Level.SEVERE, "Failed - status can not be blank or null");
								resp.getWriter().println("Failed - status can not be blank or null");
							}
						}
					
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Contract payment gateway Exception" + e.getMessage());
						resp.getWriter().println("Failed - "+e.getMessage());
					}
					
					}
					else if(documentType.trim().equals("Invoice")){
						
						try{
							
						logger.log(Level.SEVERE, "Invoice Renewal referenceNumber " + referenceNumber);
						
						Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", companyId)
								.filter("paymentGatewayUniqueId", referenceNumber).first().now();
							logger.log(Level.SEVERE, "invoiceEntity " + invoiceEntity);
							String paymentMethod = "Online";
							
							logger.log(Level.SEVERE, "transactionType" +transactionType);

						if(status.equalsIgnoreCase("Successful") && transactionType.equals(AppConstants.PAYANDCONFIRMNOW)){
							
							if(invoiceEntity!=null){
								String msg = reactonUpdatePaymentDocument(invoiceEntity,paymentMethod,razorpayReference);
								resp.getWriter().println(msg);

								sendTransactionEmail(invoiceEntity.getCount(),documentType,invoiceEntity.getPersonInfo().getFullName(),
										invoiceEntity.getPersonInfo().getCellNumber(), invoiceEntity.getPersonInfo().getCount(),
										invoiceEntity.getContractCount(),acceptanceDateTime,razorpayReference,invoiceEntity.getNetPayable(),transactionType,"Successful","PG_PaymentSuccessful",invoiceEntity.getCompanyId(),
										invoiceEntity.getBranch());
							}
							else {
								logger.log(Level.SEVERE, "Failed - evaReference not found in ERP");
								resp.getWriter().println("Failed - evaReference not found in ERP");
							}
							
						}
						else{
							if(status.equalsIgnoreCase("Unsuccessful")){
								sendUnsucessfulSMSToCustomer(invoiceEntity.getCompanyId(),invoiceEntity.getPersonInfo().getFullName(),
										invoiceEntity.getPersonInfo().getCellNumber(), invoiceEntity.getNetPayable(),AppConstants.ACCOUNTMODULE,AppConstants.Invoice);
								
								sendTransactionEmail(invoiceEntity.getCount(),documentType,invoiceEntity.getPersonInfo().getFullName(),
										invoiceEntity.getPersonInfo().getCellNumber(), invoiceEntity.getPersonInfo().getCount(),
										0,acceptanceDateTime,razorpayReference,invoiceEntity.getNetPayable(),transactionType,status,"PG_PaymentUnsucessful",invoiceEntity.getCompanyId(),
										invoiceEntity.getBranch());
							}
							else{
								logger.log(Level.SEVERE, "Failed - status can not be blank or null");
								resp.getWriter().println("Failed - status can not be blank or null");
							}
						}
					
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Failed - Contract payment gateway Exception" + e.getMessage());
						resp.getWriter().println("Failed - "+e.getMessage());
					}
					
					}
					else{
						resp.getWriter().println("Failed - Document type does not match");

					}
					
					} catch (Exception e) {
						e.printStackTrace();
						logger.log(Level.SEVERE, "Exception "+e.getMessage());
						resp.getWriter().println("Failed - "+e.getMessage());
					}
				}
				else{
					
					long authcode = jsonobj.optLong("companyId");
					logger.log(Level.SEVERE, "companyId " + companyId);

					if(authcode!=companyId){
						resp.getWriter().println("Authentication failed");
					}
					else{
						String msg ="";
						
						
						if(jsonobj.optString("Branch")!=null){
							branchName = jsonobj.optString("Branch");
						}
						
						if(jsonobj.optString("Sales Person")!=null){
							salesPerson = jsonobj.optString("Sales Person");
						}
						String paymentMethod = "";
						if(jsonobj.optString("Payment Method")!=null){
							paymentMethod = jsonobj.optString("Payment Method");
						}
						String approverName = "";
						if(jsonobj.optString("Approver Name")!=null){
							approverName = jsonobj.optString("Approver Name");
						}
						
						String contractGroup = "";
						if(jsonobj.optString("Contract Group")!=null){
							contractGroup = jsonobj.optString("Contract Group");
						}
						String contractCategory = "";
						if(jsonobj.optString("Contract Category")!=null){
							contractCategory = jsonobj.optString("Contract Category");
						}
						String contractType = "";
						if(jsonobj.optString("Contract Type")!=null){
							contractType = jsonobj.optString("Contract Type");
						}
						
						if(jsonobj.optString("Customer Name")!=null){
							customerName = jsonobj.optString("Customer Name");
						}
						
						if(jsonobj.optString("Customer Cell")!=null){
							customerCellNo = jsonobj.optString("Customer Cell");
						}
						if(approverName==null || approverName.equals("")) {
							if(salesPerson!=null) {
								approverName = salesPerson;
							}
						}
						
						String referenceNumber1 = "";
						if(jsonobj.optString("referenceNumber1")!=null){
							referenceNumber1 = jsonobj.optString("referenceNumber1");
						}
						
						String strContractDate = jsonobj.optString("Contract Date");
						Date contractDate = null;
						try {
							contractDate = dateFormat.parse(strContractDate);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						String referenceNumber2 = "";
						if(jsonobj.optString("referenceNumber2")!=null){
							referenceNumber2 = jsonobj.optString("referenceNumber2");
						}
						String numberRange = "";
						if(jsonobj.optString("Number Range")!=null){
							numberRange = jsonobj.optString("Number Range");
						}
						String description = jsonobj.optString("Remark");
						double advancePayment = jsonobj.optDouble("Advance Payment");
						
						msg += validateBranch(branchName,companyId); 
						msg += validatesalesPerson(salesPerson,companyId,branchName); 
						msg += validatepaymentMethod(paymentMethod,companyId); 
						msg += validateapproverName(approverName,companyId,branchName); 
						msg += validatecontractGroup(contractGroup,companyId); 

						
//						double contractAmt = jsonobj.optDouble("contractAmount");
						
						JSONArray warehouseCodesArray = new JSONArray();
						try {
							warehouseCodesArray = jsonobj.getJSONArray("productDetails");
						} catch (JSONException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						List<SalesLineItem> productlist = new ArrayList<SalesLineItem>();
						HashSet<String> hsproudctCode = new HashSet<String>();
//						List<TaxDetails> taxDetailslist = ofy().load().type(TaxDetails.class).filter("companyId", companyId).list();

						for(int j=0; j<warehouseCodesArray.length(); j++){
							try {
								JSONObject prodJsobObj = warehouseCodesArray.getJSONObject(j);
								
								SalesLineItem saleslineitem = new SalesLineItem();

								ServiceProduct serviceprod = new ServiceProduct();
								saleslineitem.setPrduct(serviceprod);
								
//								String productName = prodJsobObj.optString("Service Name");
								String productCode = prodJsobObj.optString("Service Code");
								if(productList==null||productList.equals(""))
									productList+=productCode;
								else
									productList+=", "+productCode;
								int numberOfServices = prodJsobObj.optInt("Number Of Services");
								int duration = prodJsobObj.optInt("Duration");
								double price = prodJsobObj.optDouble("Price");
								
								double tax1Amount = prodJsobObj.optDouble("Tax 1 Amount");
								double tax2Amount = prodJsobObj.optDouble("Tax 2 Amount");
								logger.log(Level.SEVERE, "tax1Amount " + tax1Amount);
								logger.log(Level.SEVERE, "tax2Amount " + tax2Amount);

								
								String strContractStartDate = prodJsobObj.optString("startDate");
								Date contractStartDate = null;
								try {
									contractStartDate = dateFormat.parse(strContractStartDate);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								String strContractEndDate = prodJsobObj.optString("endDate");
								Date contractEndDate = null;
								try {
									contractEndDate = dateFormat.parse(strContractEndDate);
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								saleslineitem.setPrice(price);
//								saleslineitem.setProductName(productName);
								saleslineitem.setProductCode(productCode);
								saleslineitem.setNumberOfService(numberOfServices);
								saleslineitem.setDuration(duration);
								saleslineitem.setStartDate(contractStartDate);
								saleslineitem.setEndDate(contractEndDate);

								hsproudctCode.add(productCode);
								productlist.add(saleslineitem);
								
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						
						List<ServiceProduct> serviceproductlist = new ArrayList<ServiceProduct>();
						if(hsproudctCode.size()!=0){
							ArrayList<String> arrayproductlist = new ArrayList<String>(hsproudctCode);
							
							serviceproductlist = ofy().load().type(ServiceProduct.class)
															.filter("companyId", companyId).filter("productCode IN", arrayproductlist).list();
							
							logger.log(Level.SEVERE, "serviceproductlist " + serviceproductlist.size());
							
							for(String productCode : arrayproductlist){
								boolean flag = false;
								for(ServiceProduct serviceproduct : serviceproductlist){
									if(productCode.trim().equals(serviceproduct.getProductCode().trim())){
										flag =  true;
									}
								}
								if(flag==false){
									msg += " Service Code - "+ productCode +" does not exist in the ERP System. Please create service product in ERP System. ";
								}
							}
							if(serviceproductlist.size()==0){
								msg += " Service Code - "+ arrayproductlist.toString() +" does not exist in the ERP System. Please create service product in ERP System. ";
							}
						}
						
						Customer customer = getCustomerDetails(customerCellNo,companyId,jsonobj);
						PersonInfo personinfo = new PersonInfo();

						if(customer==null){
							customer = CreateCustomerAndGetDetails(jsonobj,customerName,customerCellNo,companyId);
						}else {
							//Ashwini Patil Date:18-09-2023 Rex wants to add GST Number in customer through zoho api call
							String gstNo="";
							try {
								if(jsonobj.optString("customerGSTNO")!=null){
									gstNo = jsonobj.optString("customerGSTNO");
									logger.log(Level.SEVERE, "gstNo "+gstNo);
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if(gstNo!=null&&!gstNo.equals("")) {
								logger.log(Level.SEVERE, "gstNo not null");
								ArrayList <ArticleType> articleList=null;
								if(customer.getArticleTypeDetails()!=null) 
									articleList=customer.getArticleTypeDetails();					
								else 
									articleList=new ArrayList<ArticleType>();
								
								String[] docNames=new String[] {"Invoice Details","Contract","Quotation","Sales Order","ServiceInvoice","SalesInvoice","SalesQuotation"};
								
								for(int i=0;i<docNames.length;i++) {
									boolean presentFlag=false;
								
									for(ArticleType a:articleList) {
										if(a.getDocumentName().equals(docNames[i])&&a.getArticleTypeName().equals("GSTIN")) {
											presentFlag=true;
											break;
										}
									}
									logger.log(Level.SEVERE, "presentFlag for "+docNames[i]+" = "+presentFlag);
									if(!presentFlag) {
										ArticleType a=new ArticleType();
										a.setArticleTypeName("GSTIN");
										a.setArticleTypeValue(gstNo);
										a.setArticlePrint("YES");
										a.setDocumentName(docNames[i]);
										articleList.add(a);		
									}
								}
								customer.setArticleTypeDetails(articleList);
								logger.log(Level.SEVERE, "gstNo set in article list of customer");
								GenricServiceImpl genimpl = new GenricServiceImpl();
								genimpl.save(customer);
							}
							
						}
						if(customer.isCompany()){
							personinfo.setFullName(customer.getCompanyName());
							personinfo.setPocName(customer.getFullname());
						}
						else{
							personinfo.setFullName(customer.getFullname());
							personinfo.setPocName(customer.getFullname());
						}
						
						personinfo.setCount(customer.getCount());
						personinfo.setCellNumber(customer.getCellNumber1());
						if(customer.getEmail()!=null)
						personinfo.setEmail(customer.getEmail());
						
						
						ArrayList<SalesLineItem> updatedproductlist = new ArrayList<SalesLineItem>();
						double totalAmount = 0;
						double taxAmount = 0;
						
						ArrayList<ProductOtherCharges> prodTaxeslist = new ArrayList<ProductOtherCharges>();

						if(serviceproductlist.size()!=0){
							updatedproductlist = getUpdatedProdutList(productlist,companyId,branchName,serviceproductlist,customer,companyEntity);
							
							for(SalesLineItem lineItem : updatedproductlist){
								totalAmount += lineItem.getPrice();
							}
							logger.log(Level.SEVERE, "updatedproductlist size " + updatedproductlist.size());

							prodTaxeslist = getproductTaxeslist(updatedproductlist);
							logger.log(Level.SEVERE, "prodTaxeslist size " + prodTaxeslist.size());

							DecimalFormat decimalformat = new DecimalFormat();
							for(ProductOtherCharges othercharges : prodTaxeslist){
								double calcAmt=othercharges.getAssessableAmount()*othercharges.getChargePercent()/100;
								decimalformat.format(calcAmt);
								othercharges.setChargePayable(calcAmt);
							}
							
							
							for(ProductOtherCharges taxlinetitem : prodTaxeslist){
								taxAmount += taxlinetitem.getChargePayable();
							}
							netPayble  = totalAmount + taxAmount;
							
							logger.log(Level.SEVERE, "totalAmount " + totalAmount);
							logger.log(Level.SEVERE, "taxAmount " + taxAmount);
							logger.log(Level.SEVERE, "netPayble " + netPayble);
						}
						if(customerCellNo.equals("")) {
							msg += " Customer Cell number can not be blank!";
						}
						
						
						
						logger.log(Level.SEVERE,"Error MSG "+msg);
						if(!msg.equals("")){
							resp.getWriter().println(msg);

						}
						else{
							
//								
//							msg = reactOnCreateBillInvoicePayment(personinfo, branchName,salesPerson,paymentMethod,approverName,contractGroup
//									,contractCategory,contractType,updatedproductlist,referenceNumber2,companyId,totalamountwithoutTax,
//									0,totalamountwithoutTax,0,netPayble,prodTaxeslist,-1, referenceNumber1,description,advancePayment, numberRange,transactionType,ipAddress,"",-1,null,null);
					
							
							msg = reactOnCreateBillInvoicePayment(personinfo, branchName,salesPerson,paymentMethod,approverName,contractGroup
									,contractCategory,contractType,updatedproductlist,referenceNumber2,companyId,totalAmount,
									0,totalAmount,0,netPayble,prodTaxeslist,-1, referenceNumber1,description,advancePayment, numberRange,transactionType,ipAddress,"",-1,null,null,customer,createOnlyContractAndServicesFlag,zohoQuotID);
							if(!msg.equals("")){
								resp.getWriter().println(msg);
							}
							else{
								resp.getWriter().println(" Contract Invoice Payment Closed Successfully.");
							}
						}
						
						
					}
					
				
					
				}
			
			
				
//			}
			

		}
		else{
			resp.getWriter().println("Json Data not found");
		}
		
		}
		catch(Exception e) {
			e.printStackTrace();

			if(companyEntity!=null) {
				SendGridEmailServlet sdEmail=new SendGridEmailServlet();
				ArrayList<String> toEmailList=new ArrayList<String>();
				
				String branchEmail=ServerAppUtility.getCompanyEmail(branchName, companyEntity);
				if(branchEmail!=null) {
					toEmailList.add(branchEmail);
				}
				
				toEmailList.add("support@evasoftwaresolutions.com ");
				logger.log(Level.SEVERE,"sending error report to "+companyEntity.getEmail()); 
				String finalReport="Dear Sir/Madam,<br> The system was unable to create the contract against following details: <br> DateTime: "+new Date()+"<br>";
				String appid=ServerAppUtility.getAppid(companyEntity);
				if(appid!=null)
					finalReport+=" Appid: "+appid+"<br>";
				if(branchName!=null)
					finalReport+=" Branch: "+branchName+"<br>";
				if(salesPerson!=null)
					finalReport+=" Sales Person: "+salesPerson+"<br>";
				if(customerName!=null)
					finalReport+=" Customer Name: "+customerName+"<br>";
				if(customerCellNo!=null)
					finalReport+=" Customer Cell: "+customerCellNo+"<br>";
				if(callingSystem!=null)
					finalReport+=" Calling System: "+callingSystem+"<br>";
				if(netPayble>0)
					finalReport+=" Net Payable: "+netPayble+"<br>";
				if(productList!=null)
					finalReport+=" Product List: "+productList+"<br>";
				String error=e.getMessage();
				if(error!=null)
					finalReport+= " Error: "+e.getMessage();
				finalReport+= "<br><br><br> Kindly contact support@evasoftwaresolutions.com";
				sdEmail.sendMailWithSendGrid(companyEntity.getEmail(), toEmailList, null, null, "Error occured while creating contract through API", finalReport, "text/html",null,null,"application/pdf",null);													
				resp.getWriter().println("Failed");
			}
			
		}
		
		
	}

	

	

	

	

	

	
	public boolean validateDuplicateProductSrNo(List<SalesLineItem> productlist) {
			HashSet<Integer> hsproductSrNo = new HashSet<Integer>();
			for(SalesLineItem lineItem : productlist) {
				hsproductSrNo.add(lineItem.getProductSrNo());
			}
			logger.log(Level.SEVERE, "hsproductSrNo size "+hsproductSrNo.size());
			logger.log(Level.SEVERE, "productlist.size()"+productlist.size());
			if(productlist.size()==hsproductSrNo.size()) {
				return true;
			}
		return false;
	}

	public Date getProductEndDate(Date startDate, int duration) {
		
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
	
		Calendar cal=Calendar.getInstance();
		cal.setTime(startDate);
		cal.add(Calendar.DATE, +duration);
		
		Date endDate=null;
		
		try {
			endDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			cal.set(Calendar.HOUR_OF_DAY,23);
			cal.set(Calendar.MINUTE,59);
			cal.set(Calendar.SECOND,59);
			cal.set(Calendar.MILLISECOND,999);
			endDate=cal.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return endDate;
	}

	private String reactonUpdatePaymentDocument(Invoice invoiceEntity, String paymentMethod, String razorpayReference) {

		CustomerPayment customerPayment = ofy().load().type(CustomerPayment.class).filter("companyId", invoiceEntity.getCompanyId())
							.filter("invoiceCount", invoiceEntity.getCount()).filter("status", Invoice.CREATED).first().now();
		if(customerPayment!=null){
			
			double invoiceamt=invoiceEntity.getInvoiceAmount();
			int payAmt=(int)(invoiceamt);
			customerPayment.setPaymentAmt(payAmt);
			
			customerPayment.setPaymentReceived(payAmt);
			logger.log(Level.SEVERE,"payAmt "+payAmt);
			customerPayment.setNetPay(payAmt);
			
			double taxAmount = 0;
			taxAmount = Math.round(invoiceEntity.getTotalAmtIncludingTax() - (invoiceEntity.getFinalTotalAmt() + invoiceEntity.getTotalOtherCharges()));
			logger.log(Level.SEVERE,"taxAmount "+taxAmount);
			logger.log(Level.SEVERE,"invoice.getNetPayable() - taxAmount "+(invoiceEntity.getNetPayable() - taxAmount));
//			if(advancePayment>0) {
//				if(advancePayment>taxAmount) {
//					customerPayment.setReceivedTax(taxAmount);
//					customerPayment.setReceivedRevenue(advancePayment-taxAmount);
//				}
//				else {
//					customerPayment.setReceivedTax(taxAmount);
//					customerPayment.setReceivedRevenue(0);
//				}
//			}
//			else {
				customerPayment.setReceivedRevenue(invoiceEntity.getNetPayable() - taxAmount);
				customerPayment.setReceivedTax(taxAmount);
//			}
			customerPayment.setStatus(CustomerPayment.CREATED);
			if(paymentMethod!=null){
				customerPayment.setPaymentMethod(paymentMethod);
			}
			if(invoiceEntity.getRefNumber()!=null) {
				customerPayment.setRefNumber(invoiceEntity.getRefNumber());
			}
			if(razorpayReference!=null)
			customerPayment.setReferenceNo(razorpayReference);
			
			customerPayment.setAmountTransferDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			
			ofy().save().entity(customerPayment);
			
			
			
			
			return "Payment document updated successfully";

		}
		else{
			return "No open payment document found";
		}
		
	}

	private void sendSucessfullSMSToCustomer(Contract contractEntity, long contractId) {

		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", contractEntity.getCompanyId()).filter("status", true).first().now();
		logger.log(Level.SEVERE, "smsconfig" + smsconfig);

//		if(smsconfig!=null){
//			
//			String accountSid = smsconfig.getAccountSID();
//			String authoToken = smsconfig.getAuthToken();
//			String password = smsconfig.getPassword();
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",contractEntity.getCompanyId()).filter("event","Payment Successful").filter("status",true).first().now();
			logger.log(Level.SEVERE, "smsEntity" + smsEntity);

			if(smsEntity!=null){
				
				String templateMsgWithBraces = smsEntity.getMessage();
				
				String custName = templateMsgWithBraces.replace("{CustomerName}", contractEntity.getCinfo().getFullName());
				String actualmsg = custName.replace("{ContractId}", contractId+"");
				SmsServiceImpl smsimpl = new SmsServiceImpl();
				long customercell = contractEntity.getCinfo().getCellNumber();
				logger.log(Level.SEVERE, "customercell" + customercell);
				if(customercell!=0){
//					int value =  smsimpl.sendSmsToClient(actualmsg, customercell, accountSid,  authoToken,  password, contractEntity.getCompanyId());
					
					/** @author Vijay Date :- 29-04-2022
					 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
					 * whats and sms based on communication channel configuration.
					 */
					smsimpl.sendMessage(AppConstants.SERVICEMODULE,AppConstants.CONTRACT,smsEntity.getEvent(),smsEntity.getCompanyId(),actualmsg,customercell,false);
					logger.log(Level.SEVERE,"after sendMessage method");
				}
			}
		
//		}
	
	
	}

	private void sendUnsucessfulSMSToCustomer(Long companyId, String fullName,
			Long cellNumber, double netpayable,String moduleName,String documentName) {


		SmsConfiguration smsconfig = ofy().load().type(SmsConfiguration.class).filter("companyId", companyId).filter("status", true).first().now();
		logger.log(Level.SEVERE, "smsconfig" + smsconfig);

//		if(smsconfig!=null){
//			
//			String accountSid = smsconfig.getAccountSID();
//			String authoToken = smsconfig.getAuthToken();
//			String password = smsconfig.getPassword();
			
			SmsTemplate smsEntity = ofy().load().type(SmsTemplate.class).filter("companyId",companyId).filter("event","Payment Unsuccessful").filter("status",true).first().now();
			logger.log(Level.SEVERE, "smsEntity" + smsEntity);

			if(smsEntity!=null){
				Company comp = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
				
				String templateMsgWithBraces = smsEntity.getMessage();
				
				String custName = templateMsgWithBraces.replace("{CustomerName}", fullName);
				String netAmount = custName.replace("{Net Amount}", netpayable+"");
				String actualmsg = netAmount.replace("{company phone number}", comp.getCellNumber1()+"");
				SmsServiceImpl smsimpl = new SmsServiceImpl();
				long customercell = cellNumber;
				logger.log(Level.SEVERE, "customercell" + customercell);
				if(customercell!=0){
//					int value =  smsimpl.sendSmsToClient(actualmsg, customercell, accountSid,  authoToken,  password, companyId);

					/** @author Vijay Date :- 29-04-2022
					 * Des :- Below method will check communication channel configuration for whatsApp and SMS and send message on
					 * whats and sms based on communication channel configuration.
					 */
					smsimpl.sendMessage(moduleName,documentName,smsEntity.getEvent(),smsEntity.getCompanyId(),actualmsg,customercell,false);
					logger.log(Level.SEVERE,"after sendMessage method");
				}
			}
		
//		}
	
	}

	private Customer getCustomerDetails(String customerCellNo, long companyId, JSONObject jsonobj) {
		
		try {
			
			long custmercellno = Long.parseLong(customerCellNo);
			Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", companyId)
									.filter("contacts.cellNo1", custmercellno).first().now();
			logger.log(Level.SEVERE, "customerEntity " + customerEntity);
			
			boolean isserviceAddressFlag = false;
			try {
				
			JSONArray jsonarray = new JSONArray();
			jsonarray = jsonobj.getJSONArray("serviceAddress");
			for(int i=0;i<jsonarray.length();i++){
					JSONObject jobj = jsonarray.getJSONObject(i);
					
					String addressLine1 = "";
					if(jobj.optString("Address Line 1")!=null){
						addressLine1 = jobj.optString("Address Line 1");
					}
					String addressLine2 = "";
					if(jobj.optString("Address Line 2")!=null){
						addressLine2 = jobj.optString("Address Line 2");
					}
					String landmark = "";
					if(jobj.optString("Landmark")!=null){
						landmark = jobj.optString("Landmark");
					}
					String Locality = "";
					if(jobj.optString("Locality")!=null){
						Locality = jobj.optString("Locality");
					}
					String city = "";
					if(jobj.optString("City")!=null){
						city = jobj.optString("City");
					}
					String State = "";
					if(jobj.optString("State")!=null){
						State = jobj.optString("State");
					}
					String country = "";
					if(jobj.optString("Country")!=null){
						country = jobj.optString("Country");
					}
					
					int pinCode = jobj.optInt("Pin");
					
					Address serviceAddress = new Address();
					serviceAddress.setAddrLine1(addressLine1);
					serviceAddress.setAddrLine2(addressLine2);
					serviceAddress.setLandmark(landmark);
					serviceAddress.setLocality(Locality);
					serviceAddress.setCity(city);
					serviceAddress.setState(State);
					serviceAddress.setCountry(country);
					serviceAddress.setPin(pinCode);
					
					customerEntity.setSecondaryAdress(serviceAddress);
					
					isserviceAddressFlag = true;
				
			}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			try {
				JSONArray billingAddressjsonarray = new JSONArray();
				
					billingAddressjsonarray = jsonobj.getJSONArray("billingAddress");
				
				for(int j=0;j<billingAddressjsonarray.length();j++){
						JSONObject jobj = billingAddressjsonarray.getJSONObject(j);
						
						String addressLine1 = "";
						if(jobj.optString("Address Line 1")!=null){
							addressLine1 = jobj.optString("Address Line 1");
						}
						String addressLine2 = "";
						if(jobj.optString("Address Line 2")!=null){
							addressLine2 = jobj.optString("Address Line 2");
						}
						String landmark = "";
						if(jobj.optString("Landmark")!=null){
							landmark = jobj.optString("Landmark");
						}
						String Locality = "";
						if(jobj.optString("Locality")!=null){
							Locality = jobj.optString("Locality");
						}
						String city = "";
						if(jobj.optString("City")!=null){
							city = jobj.optString("City");
						}
						String State = "";
						if(jobj.optString("State")!=null){
							State = jobj.optString("State");
						}
						String country = "";
						if(jobj.optString("Country")!=null){
							country = jobj.optString("Country");
						}
						
						int pinCode = jobj.optInt("Pin");
						
						Address billingAddress = new Address();
						billingAddress.setAddrLine1(addressLine1);
						billingAddress.setAddrLine2(addressLine2);
						billingAddress.setLandmark(landmark);
						billingAddress.setLocality(Locality);
						billingAddress.setCity(city);
						billingAddress.setState(State);
						billingAddress.setCountry(country);
						billingAddress.setPin(pinCode);
						customerEntity.setAdress(billingAddress);
						
						if(!isserviceAddressFlag){
							customerEntity.setSecondaryAdress(billingAddress);
						}
					
				}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return customerEntity;

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}

	private String validatecontractGroup(String contractGroup, long companyId) {

		String msg = "";
		int configtype = 31;

		if(!contractGroup.equals("")){
			Config config = ofy().load().type(Config.class).filter("companyId", companyId)
									.filter("name", contractGroup)
									.filter("type", configtype).first().now();
			logger.log(Level.SEVERE, "employee " + config);
			if(config==null){
				if(contractGroup.equals("Online")){
					createContractGroup(contractGroup,configtype,companyId);
				}
				else{
					msg += " Contract Group "+contractGroup +" does not exist in the ERP System. Please define first."; 
				}
			}
		}
		else{
			msg +=" Contract Group should not be blank.";
		}
		
		return msg;
	
		
	}

	private void createContractGroup(String contractGroup, int configtype, long companyId) {

		Config configEntity = new Config();
		configEntity.setCompanyId(companyId);
		configEntity.setName(contractGroup);
		configEntity.setType(configtype);
		configEntity.setStatus(true);
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(configEntity);
	
	}

	private String validateapproverName(String approverName, long companyId, String branchName) {
		String msg = "";
		if(!approverName.equals("")){
			Employee employee = ofy().load().type(Employee.class).filter("companyId", companyId)
									.filter("fullname", approverName).first().now();
			logger.log(Level.SEVERE, "employee " + employee);
			if(employee==null){
				if(approverName.equals("Online")){
					createEmployee(approverName,branchName,companyId);
				}
				else{
					msg += " Approver Name "+approverName +" does not exist in the ERP System. Please define first."; 
				}
			}
		}
		else{
			msg +=" Approver Name should not be blank.";
		}
		
		return msg;
		
	
	}

	private String validatepaymentMethod(String paymentMethod, long companyId) {
		String msg = "";
		int configtype = 16;
		if(!paymentMethod.equals("")){
			Config employee = ofy().load().type(Config.class).filter("companyId", companyId)
									.filter("name", paymentMethod)
									.filter("type", configtype).first().now();
			logger.log(Level.SEVERE, "employee " + employee);
			if(employee==null){
				if(paymentMethod.equals("Online")){
					createPaymentMethod(paymentMethod,configtype,companyId);
				}
				else{
					msg += " Payment Method "+paymentMethod +" does not exist in the ERP System. Please define first."; 
				}
			}
		}
		else{
			msg +=" Payment Method should not be blank.";
		}
		
		return msg;
	}

	private void createPaymentMethod(String paymentMethod, int configtype, long companyId) {
		Config configEntity = new Config();
		configEntity.setCompanyId(companyId);
		configEntity.setName(paymentMethod);
		configEntity.setType(configtype);
		configEntity.setStatus(true);
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(configEntity);
	}

	private String validatesalesPerson(String salesPerson, long companyId, String branchName) {
		String msg = "";
		if(!salesPerson.equals("")){
			Employee employee = ofy().load().type(Employee.class).filter("companyId", companyId)
									.filter("fullname", salesPerson).first().now();
			logger.log(Level.SEVERE, "employee " + employee);
			if(employee==null){
				if(salesPerson.equals("Online")){
					createEmployee(salesPerson,branchName,companyId);
				}
				else{
					msg += " Sales Person "+salesPerson +" does not exist in the ERP System. Please define first."; 
				}
			}
		}
		else{
			msg +=" Sales Person Name should not be blank";
		}
		
		return msg;
		
	
	}

	private void createEmployee(String salesPerson, String branchName, long companyId) {
		Employee employee = new Employee();
		employee.setCompanyId(companyId);
		employee.setFullname(salesPerson);
		employee.setBranchName(branchName);
		employee.setStatus(true);

		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(employee);
		
	}

	private String validateBranch(String branchName, long companyId) {
		String msg = "";
		if(!branchName.equals("")){
			Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", companyId)
									.filter("buisnessUnitName", branchName).first().now();
			logger.log(Level.SEVERE, "branchEntity " + branchEntity);
			if(branchEntity==null){
				if(branchName.equals("Online")){
					createbranch(branchName,companyId);
				}
				else{
					msg += "Branch Name "+branchName +" does not exist in the ERP System, Please define first."; 
				}
			}
		}
		else{
			msg +="Branch Name should not be blank";
		}
		
		return msg;
		
	}

	private void createbranch(String branchName, long companyId) {

		Branch branchEntity = new Branch();
		branchEntity.setCompanyId(companyId);
		branchEntity.setBusinessUnitName(branchName);
		branchEntity.setBranchCode(branchName);
		branchEntity.setstatus(true);
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		genimpl.save(branchEntity);
	}

	public ArrayList<ProductOtherCharges> getproductTaxeslist(List<SalesLineItem> salesLineItemLis) {
		
		ArrayList<ProductOtherCharges> taxList = new ArrayList<ProductOtherCharges>();
//		NumberFormat nf=NumberFormat.getFormat("#.00");

		DecimalFormat nf = new DecimalFormat("#.00");
		
		for(int i=0;i<salesLineItemLis.size();i++)
		{
			double priceqty=Double.parseDouble(nf.format(salesLineItemLis.get(i).getPrice()));

			logger.log(Level.SEVERE, "salesLineItemLis.get(i).getVatTax().getPercentage() " + salesLineItemLis.get(i).getVatTax().getPercentage());

			if(salesLineItemLis.get(i).getVatTax().getPercentage()!=0){
				System.out.println("hi vijay vat=="+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getVatTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getVatTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside vat GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getVatTax().getTaxName()"+salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getVatTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getVatTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getVatTax().getPercentage(),
							salesLineItemLis.get(i).getVatTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}

					taxList.add(pocentity);
				}
			}
			
			logger.log(Level.SEVERE, "salesLineItemLis.get(i).getServiceTax().getPercentage() " + salesLineItemLis.get(i).getServiceTax().getPercentage());

			if(salesLineItemLis.get(i).getServiceTax().getPercentage()!=0){
				
				System.out.println(" HI Vijay Service =="+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
				if(salesLineItemLis.get(i).getServiceTax().getTaxPrintName()!=null && !salesLineItemLis.get(i).getServiceTax().getTaxPrintName().equals(""))
				{
					System.out.println("Inside service GST");
					ProductOtherCharges pocentity=new ProductOtherCharges();
					System.out.println("salesLineItemLis.get(i).getServiceTax().getTaxName()"+salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargeName(salesLineItemLis.get(i).getServiceTax().getTaxPrintName());
					pocentity.setChargePercent(salesLineItemLis.get(i).getServiceTax().getPercentage());
					pocentity.setIndexCheck(i+1);
					int indexValue=this.checkTaxPercent(salesLineItemLis.get(i).getServiceTax().getPercentage(),
							 		salesLineItemLis.get(i).getServiceTax().getTaxPrintName(),taxList);
					
					if(indexValue!=-1){
						System.out.println("Index As Not -1");
						pocentity.setAssessableAmount(priceqty+taxList.get(indexValue).getAssessableAmount());
//						this.getProdTaxTable().getDataprovider().getList().remove(indexValue);
						taxList.remove(indexValue);
					}
					if(indexValue==-1){
						pocentity.setAssessableAmount(priceqty);
					}
//					this.prodTaxTable.getDataprovider().getList().add(pocentity);
					
					taxList.add(pocentity);
				}
			}
			
		}
		
		return taxList;
	}

	
	public int checkTaxPercent(double taxValue,String taxName, ArrayList<ProductOtherCharges> taxesList)
	{
		System.out.println("rohan taxesList size"+taxesList.size());
		for(int i=0;i<taxesList.size();i++)
		{
			double listval=taxesList.get(i).getChargePercent();
			int taxval=(int)listval;
			
			if(taxName.equals("Service")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("Service Tax")){
					return i;
				}
			}
			if(taxName.equals("VAT")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("VAT")){
					return i;
				}
			}
			
			//  rohan added this for GST
			if(taxName.equals("CGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("CGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("SGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("SGST")){
					return i;
				}
			}
			
		//  rohan added this for GST
			if(taxName.equals("IGST")){
				if(taxValue==taxesList.get(i).getChargePercent()&&taxval!=0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
				if(taxval==0&&taxValue==0&&taxesList.get(i).getChargeName().trim().equals("IGST")){
					return i;
				}
			}
			
		}
		return -1;
	}
	
	
	private Tax getTaxDetails(String taxName, List<TaxDetails> taxDetailslist,double taxpecentage) {
		
		for(TaxDetails taxDetails : taxDetailslist){
			if(taxDetails.getTaxPrintName().trim().equalsIgnoreCase(taxName.trim()) && taxDetails.getTaxChargePercent()==taxpecentage){
				
				Tax tax = new Tax();
				tax.setPercentage(taxDetails.getTaxChargePercent());
				tax.setTaxConfigName(taxDetails.getTaxChargeName());
				tax.setTaxName(taxDetails.getTaxChargeName());
				tax.setTaxPrintName(taxDetails.getTaxPrintName());
				
				return tax;
			}
		}
		
		return null;
	}

	private ArrayList<SalesLineItem> getUpdatedProdutList(List<SalesLineItem> productlist, long companyId, String branchName, List<ServiceProduct> serviceproductlist, Customer customer, Company companyEntity) {
		
		ArrayList<SalesLineItem> newupdatedproductlist = new ArrayList<SalesLineItem>();
		List<TaxDetails> taxDetailslist = ofy().load().type(TaxDetails.class).filter("companyId", companyId).list();

		
		
		int productSrNo = 0;
		for(SalesLineItem salesitem : productlist){
			SalesLineItem saleslineItem = new SalesLineItem();
			ServiceProduct serviceprod = getserviceProduct(salesitem.getProductCode(), serviceproductlist);
			ServiceProduct serviceproduct = new ServiceProduct();
			serviceproduct = mycloneServiceProduct(serviceprod,companyId);
			saleslineItem.setPrduct(serviceproduct);
			saleslineItem.setProductSrNo(++productSrNo);
			saleslineItem.setDuration(salesitem.getDuration());
			saleslineItem.setNumberOfService(salesitem.getNumberOfServices());
			saleslineItem.setStartDate(salesitem.getStartDate());
			
			Date prosuctStartdDate = saleslineItem.getStartDate();
			int prodDuration = saleslineItem.getDuration()-1;
			Calendar cal = Calendar.getInstance();
			cal.setTime(prosuctStartdDate);
			cal.add(Calendar.DATE, prodDuration);
			Date productEndDate = cal.getTime();
			saleslineItem.setEndDate(DateUtility.getDateWithTimeZone("IST", productEndDate));

			saleslineItem.setPrice(salesitem.getPrice());
			saleslineItem.setTotalAmount(serviceproduct.getPrice());
			
				if(serviceproduct.getServiceTax()!=null) {
					saleslineItem.setServiceTax(serviceproduct.getServiceTax());
					saleslineItem.setServiceTaxEdit(serviceproduct.getServiceTax().getTaxConfigName());
				}
				else{
				  	  Tax tax1 = new Tax();
				  	  tax1 = getTaxDetails("NA", taxDetailslist,0);
					  saleslineItem.setServiceTax(tax1);
					  saleslineItem.setServiceTaxEdit(tax1.getTaxConfigName());

				}
				
				if(serviceproduct.getVatTax()!=null) {
					saleslineItem.setVatTax(serviceproduct.getVatTax());
					saleslineItem.setVatTaxEdit(serviceproduct.getVatTax().getTaxConfigName());

				}
				else{
					 Tax tax2 = new Tax();
					 tax2 = getTaxDetails("NA", taxDetailslist,0);
					 saleslineItem.setVatTax(tax2);
					 saleslineItem.setVatTaxEdit(tax2.getTaxConfigName());
				}
			
				
			ServerAppUtility serverapputility = new ServerAppUtility();
			saleslineItem = serverapputility.updateTaxesDetails(saleslineItem, companyEntity, taxDetailslist, customer.getAdress().getState());	
			logger.log(Level.SEVERE, "service tax -- "+saleslineItem.getServiceTax().isInclusive());
			logger.log(Level.SEVERE, "service tax -- "+saleslineItem.getVatTax().isInclusive());
			
//			if(saleslineItem.getServiceTax().isInclusive() || saleslineItem.getVatTax().isInclusive() ) {
//				saleslineItem.setInclusive(true);
//			}
			
			double taxAmt = removeAllTaxes(saleslineItem);
			logger.log(Level.SEVERE, "taxAmt "+taxAmt);
			double amt = saleslineItem.getPrice() - taxAmt;
			logger.log(Level.SEVERE, "amt "+amt);
			saleslineItem.setPrice(amt);
			
			saleslineItem.setQuantity(1d);

			ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
			scheduleBranchlist = getshceduleBranchServiceAddress(branchName, saleslineItem.getUnitOfMeasurement());
			HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
			customerBranchlist.put(saleslineItem.getProductSrNo(), scheduleBranchlist);
			saleslineItem.setCustomerBranchSchedulingInfo(customerBranchlist);
			
			saleslineItem.getServiceTax().setInclusive(false);
			saleslineItem.getVatTax().setInclusive(false);

			logger.log(Level.SEVERE, "service tax =="+saleslineItem.getServiceTax().isInclusive());
			logger.log(Level.SEVERE, "service tax -== "+saleslineItem.getVatTax().isInclusive());
			
			
			newupdatedproductlist.add(saleslineItem);
		}
		
		return newupdatedproductlist;
	}

	private ServiceProduct mycloneServiceProduct(ServiceProduct serviceprod,long companyId) {
		ServiceProduct serProduct = new ServiceProduct();
		serProduct.setDuration(serviceprod.getDuration());
		serProduct.setNumberOfServices(serviceprod.getNumberOfServices());
		serProduct.setPrice(serviceprod.getPrice());
		serProduct.setServiceTime(serviceprod.getServiceTime());
		serProduct.setTermsoftreatment(serviceprod.getTermsoftreatment());
		serProduct.setWarrantyPeriod(serviceprod.getWarrantyPeriod());
		serProduct.setProductCode(serviceprod.getProductCode());
		serProduct.setProductName(serviceprod.getProductName());
		serProduct.setSpecifications(serviceprod.getSpecifications());
		serProduct.setComment(serviceprod.getComment());
		serProduct.setCommentdesc(serviceprod.getCommentdesc());
		serProduct.setCommentdesc1(serviceprod.getCommentdesc1());
		serProduct.setCommentdesc2(serviceprod.getCommentdesc2());
		serProduct.setUnitOfMeasurement(serviceprod.getUnitOfMeasurement());
		serProduct.setPrice(serviceprod.getPrice());
		if(serviceprod.getTermsAndConditions()!=null)
			serProduct.setTermsAndConditions(serviceprod.getTermsAndConditions());
		else
			serProduct.setTermsAndConditions(new DocumentUpload());
		serProduct.setStatus(serviceprod.isStatus());
		serProduct.setProductCategory(serviceprod.getProductCategory());
		serProduct.setProductType(serviceprod.getProductType());
		serProduct.setProductGroup(serviceprod.getProductGroup());
		serProduct.setProductClassification(serviceprod.getProductClassification());
		if(serviceprod.getProductImage()!=null)
			serProduct.setProductImage(serviceprod.getProductImage());
		else
			serviceprod.setProductImage(new DocumentUpload());
			
		/**
		 * @author Vijay Date :- 29-03-2022
		 * Des :- below image field used in item product only duw to this service contract data get created with corrupt data
		 * TO avoid this i have added with new documentUpload object so corrupt data will not create 
		 */
		serProduct.setProductImage1(new DocumentUpload()); 
		serProduct.setProductImage2(new DocumentUpload());
		serProduct.setProductImage3(new DocumentUpload());
		/**
		 * ends here
		 */
		serProduct.setServiceTax(serviceprod.getServiceTax());
		serProduct.setVatTax(serviceprod.getVatTax());
		serProduct.setRefNumber1(serviceprod.getRefNumber1());
		serProduct.setRefNumber2(serviceprod.getRefNumber2());
		serProduct.setHsnNumber(serviceprod.getHsnNumber());
		serProduct.setPurchasePrice(serviceprod.getPrice());
		serProduct.setPurchaseTax1(serviceprod.getPurchaseTax1());
		serProduct.setPurchaseTax2(serviceprod.getPurchaseTax2());
		serProduct.setCompanyId(serviceprod.getCompanyId());
		serProduct.setUserId(serviceprod.getUserId());
		serProduct.setId(serviceprod.getId());
		serProduct.setCreatedBy(serviceprod.getCreatedBy());
		serProduct.setCount(serviceprod.getCount());
		
		return serProduct;
	}

	private ServiceProduct getserviceProduct(String productCode,List<ServiceProduct> serviceproductlist) {
		for(ServiceProduct product : serviceproductlist){
			if(productCode.trim().equals(product.getProductCode().trim())){
				return product;
			}
		}
		return null;
	}


	private String reactOnCreateBillInvoicePayment(PersonInfo personInfo, String branchName, String salesPerson, String paymentMethod, String approverName, 
					String contractGroup, String contractCategory, String contractType, List<SalesLineItem> list, 
					String referenceNumber, long companyId, double totalAmt, double discountAmt, double finalTotalAmt, 
					double roundoff, double netPayble, List<ProductOtherCharges> producttaxlist, int documentId, String razorpayReferenceNo1, String description, double advancePayment, String numberRange, String transactionType, String ipAddress, String documentType, int referenceDocumentId, List<ServiceSchedule> serviceschedulelist, Date acceptanceDateTime, Customer customer, Boolean createOnlyContractAndServicesFlag,String zohoQuotID) {
		
		String msg = "Success";
		
//		try {
			logger.log(Level.SEVERE, "in try");
			Contract contractEntity=null;
			if(zohoQuotID!=null&&!zohoQuotID.equals(""))
			{
				logger.log(Level.SEVERE, "zohoQuotID not null");
				contractEntity=ofy().load().type(Contract.class).filter("companyId",companyId).filter("zohoQuotID", zohoQuotID).first().now();
				if(contractEntity!=null) {	
					logger.log(Level.SEVERE, "contract for zohoQuotationID: "+zohoQuotID +" already exist."+" existing contract id: "+contractEntity.getCount()+" contract status:"+contractEntity.getStatus());
					if(contractEntity.getStatus().equals(Contract.APPROVED))
						return "Contract for this quotation has been already approved in EVA. You cannot make changes in quotation.";
						
				}else {			
					logger.log(Level.SEVERE, "new contract");
					contractEntity= new Contract();
				}
				contractEntity.setZohoQuotID(zohoQuotID);
			}else
				contractEntity= new Contract();
			
		
//		contractEntity.setItems(list);
		if(documentType.equals(AppConstants.QUOTATION)) {
			List<SalesLineItem> itemlist = getupdatedTotalAmount(list);
			contractEntity.setItems(itemlist);
		}
		else {
			contractEntity.setItems(list);
		}
		contractEntity.setCinfo(personInfo);
		contractEntity.setBranch(branchName);
		contractEntity.setEmployee(salesPerson);
		contractEntity.setPaymentMethod(paymentMethod);
		contractEntity.setApproverName(approverName);
		contractEntity.setGroup(contractGroup);
		contractEntity.setCategory(contractCategory);
		contractEntity.setType(contractType);
		if(numberRange!=null && !numberRange.equals("")) {
			contractEntity.setNumberRange(numberRange);
		}
		contractEntity.setCompanyId(companyId);	
		
		Date contractStartDate = getcontractStartDate(contractEntity.getItems());
		Date contractEndDate = getContractEndDate(contractEntity.getItems());
		logger.log(Level.SEVERE, "contractStartDate"+contractStartDate);
		logger.log(Level.SEVERE, "contractEndDate"+contractEndDate);
		contractEntity.setEndDate(contractEndDate);
		contractEntity.setStartDate(contractStartDate);
		contractEntity.setContractDate(contractStartDate);
	
		contractEntity.setTotalAmount(totalAmt);
		contractEntity.setDiscountAmt(discountAmt);
		contractEntity.setFinalTotalAmt(finalTotalAmt);
		contractEntity.setInclutaxtotalAmount(netPayble);
		contractEntity.setRoundOffAmt(roundoff);
		contractEntity.setNetpayable(netPayble);
		contractEntity.setGrandTotal(netPayble);
		contractEntity.setGrandTotalAmount(netPayble);
		
		List<PaymentTerms> paymentTermsList = new ArrayList<PaymentTerms>();
		PaymentTerms paymentterm = new PaymentTerms();
		paymentterm.setPayTermDays(0);
		paymentterm.setPayTermPercent(100d);
		paymentterm.setPayTermComment("Full Payment");
		paymentTermsList.add(paymentterm);
		contractEntity.setPaymentTermsList(paymentTermsList);
		
		if(transactionType.trim().equals(AppConstants.PAYANDCONFIRMNOW)){
			contractEntity.setStatus(Contract.APPROVED);
		}
		else{
			contractEntity.setStatus(Contract.CREATED);
		}
		
		int numberOfServiceLimit = 700;
		ArrayList<ServiceSchedule> serviceList = new ArrayList<ServiceSchedule> ();

		if(serviceschedulelist!=null && serviceschedulelist.size()>0){
			contractEntity.setServiceScheduleList(serviceschedulelist);
			serviceList.addAll(serviceschedulelist);
		}
		else{
			serviceList = createServiceList(contractEntity.getItems(),companyId,branchName);

			if(serviceList.size()>numberOfServiceLimit){
				
			}
			else{
				contractEntity.setServiceScheduleList(serviceList);
			}
		}
		logger.log(Level.SEVERE, "serviceList"+serviceList.size());

		contractEntity.setCompanyId(companyId);	
		
		contractEntity.setProductTaxes(producttaxlist);
		contractEntity.setRefNo2(referenceNumber);
		if(razorpayReferenceNo1!=null && !razorpayReferenceNo1.equalsIgnoreCase("null")){ // rezor pay reference number or for website its reference No 1
			contractEntity.setRefNo(razorpayReferenceNo1);
		}
		if(documentType.trim().equalsIgnoreCase("Lead")) {
			if(referenceDocumentId!=-1){
				contractEntity.setLeadCount(referenceDocumentId);
			}
		}
		else if(documentType.trim().equalsIgnoreCase("Quotation")) {
			if(documentId!=-1) {
				contractEntity.setQuotationCount(documentId);
			}
		}
		else if(documentType.trim().equalsIgnoreCase(AppConstants.CONTRACT_RENEWAL)) {
			if(referenceDocumentId!=-1)
			contractEntity.setRefContractCount(referenceDocumentId); // this is old contract id sending from method
		}
		
		if(description!=null) {
			contractEntity.setDescription(description);
		}
		
		if(ipAddress!=null && !ipAddress.equals("")){
			contractEntity.setIpAddress(ipAddress);
		}
		if(acceptanceDateTime!=null) {
			contractEntity.setAcceptedDateTime(acceptanceDateTime);
		}
		
		
		try {
			if(customer!=null) {
				if(customer.getAdress()!=null)
				contractEntity.setNewcustomerAddress(customer.getAdress());
				if(customer.getSecondaryAdress()!=null)
				contractEntity.setCustomerServiceAddress(customer.getSecondaryAdress());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		GenricServiceImpl genimpl = new GenricServiceImpl();
		ReturnFromServer server = new ReturnFromServer();
		server = genimpl.save(contractEntity);
		logger.log(Level.SEVERE,"Contract saved successfull");
		long contractId = server.count;
		
		sendSucessfullSMSToCustomer(contractEntity,contractId);
		
		if(serviceList.size()>numberOfServiceLimit){
			for(ServiceSchedule sch:serviceList){
				sch.setDocumentType("Contract");
				sch.setDocumentId(server.count);
				sch.setCompanyId(companyId);
			}
			ofy().save().entities(serviceList).now();
			logger.log(Level.SEVERE,"Service list saved successfully"+serviceList.size());
		}
		
		
		
		int documentid = 0;
		if(documentType.trim().equalsIgnoreCase("Lead")) {
			if(referenceDocumentId!=-1){
				documentid = referenceDocumentId;
			}
		}
		else if(documentType.trim().equalsIgnoreCase("Quotation")) {
			if(documentId!=-1) {
				documentid = documentId;
			}
		}
		else if(documentType.trim().equalsIgnoreCase(AppConstants.CONTRACT_RENEWAL)) {
			if(referenceDocumentId!=-1)
			documentid = referenceDocumentId;
		}
		else if(documentType.trim().equalsIgnoreCase("Contract")) {
				documentid = contractEntity.getCount();
		}
		
		if(transactionType.trim().equals(AppConstants.PAYANDCONFIRMNOW)){
			logger.log(Level.SEVERE,"inside  payandconfirmnow "+transactionType+" createOnlyContractAndServices flag="+createOnlyContractAndServicesFlag);

			try {
				
				/*
				 * Ashwini Patil
				 * Date:29-09-2023
				 * For some clients we are going to manage contract and services through eva and lead, quotation, bill, invoice, payment from zoho
				 * In that case we dont want to create bills, invoices and payments in eva.
				 * so if CreateOnlyContractAndServices flag is false then we will create bills and invoices
				 * if CreateOnlyContractAndServices flag is true then we will not create bills and invoices. We will create only contract and services.
				 */
				if(!createOnlyContractAndServicesFlag)
					msg = createBillInvoiceAmountDocument(contractEntity,advancePayment);
				
				ContractServiceImplementor contractserviceimpl = new ContractServiceImplementor();
				contractserviceimpl.makeServices(contractEntity);
				contractserviceimpl.ChangeLeadStatus(contractEntity);
				contractserviceimpl.changeQuotationStatus(contractEntity);
				

				sendTransactionEmail(documentid,documentType,contractEntity.getCinfo().getFullName(),
						contractEntity.getCinfo().getCellNumber(), contractEntity.getCinfo().getCount(),
						contractEntity.getCount(),acceptanceDateTime,razorpayReferenceNo1,contractEntity.getNetpayable(),transactionType,"Successful","PG_PaymentSuccessful",contractEntity.getCompanyId(),
						contractEntity.getBranch());
				
			} catch (Exception e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "create bill exception "+e.getMessage());
			}

			
		}
		else{
			logger.log(Level.SEVERE,"transaction type is not payandconfirmnow"+transactionType);
			sendTransactionEmail(documentid,documentType,contractEntity.getCinfo().getFullName(),
					contractEntity.getCinfo().getCellNumber(), contractEntity.getCinfo().getCount(),
					contractEntity.getCount(),acceptanceDateTime,razorpayReferenceNo1,contractEntity.getNetpayable(),transactionType,"Successful","PG_BookingConfirmed",contractEntity.getCompanyId(),
					contractEntity.getBranch());
		}

		
		
		
		
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.log(Level.SEVERE,"Contract creation failed - "+e.getMessage());
//			return "Failed - "+e.getMessage();
//		}
		
		return msg;
	}
	
	
	private List<SalesLineItem> getupdatedTotalAmount(List<SalesLineItem> list) {
		CommonServiceImpl commonservice = new CommonServiceImpl();
		
		for(SalesLineItem lineItem : list) {
			lineItem.setTotalAmount(Double.parseDouble(commonservice.getTotalAmount(lineItem)));
		}
		return list;
	}

	private String createBillInvoiceAmountDocument(Contract contractEntity, double advancePayment) {
		
		String msg = createBillingDocument(contractEntity,advancePayment);
		
		return msg;
	}

	private String createBillingDocument(Contract contractEntity, double advancePayment) {
		
		String msg ="Success";
		
		try {
			
		
		 /**
		  *  nidhi
		  *  Date : 4-12-2017
		  *  Check process configration for 
		  */
		 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", contractEntity.getCompanyId());
		 
		 /**
		  * end
		  */
		
		Date currentDate=DateUtility.getDateWithTimeZone("IST", new Date());

		BillingDocument billingDocEntity=new BillingDocument();
		List<SalesOrderProductLineItem> salesProdLis=null;
		List<ContractCharges> billTaxLis=null;
		ArrayList<PaymentTerms> billingPayTerms=new ArrayList<PaymentTerms>();
		PaymentTerms paymentTerms=new PaymentTerms();
		
		Date conStartDate=null;
		DateFormat dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		System.out.println("Contract Start Date"+conStartDate);
	
		if(contractEntity.getCinfo()!=null)
			billingDocEntity.setPersonInfo(contractEntity.getCinfo());
		if(contractEntity.getCount()!=0)
			billingDocEntity.setContractCount(contractEntity.getCount());
		if(contractEntity.getStartDate()!=null)
			billingDocEntity.setContractStartDate(contractEntity.getStartDate());
		if(contractEntity.getEndDate()!=null)
			billingDocEntity.setContractEndDate(contractEntity.getEndDate());
		if(contractEntity.getNetpayable()!=0)
			billingDocEntity.setTotalSalesAmount(contractEntity.getNetpayable());
		salesProdLis = retrieveSalesproduct(contractEntity,100);
		
		ArrayList<SalesOrderProductLineItem> salesOrdArr=new ArrayList<SalesOrderProductLineItem>();
		salesOrdArr.addAll(salesProdLis);
		System.out.println("Size here here"+salesOrdArr.size());
		if(salesOrdArr.size()!=0)
			billingDocEntity.setSalesOrderProducts(salesOrdArr);
		if(contractEntity.getCompanyId()!=null)
		billingDocEntity.setCompanyId(contractEntity.getCompanyId());

		
		Date invoiceDate= contractEntity.getContractDate();;
		
		logger.log(Level.SEVERE," Billing Invoice paymnent Date"+invoiceDate);
		
		if(invoiceDate!=null){
			billingDocEntity.setBillingDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
			logger.log(Level.SEVERE," Billing Date While SAving"+billingDocEntity.getBillingDate());
			billingDocEntity.setInvoiceDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
			logger.log(Level.SEVERE," Invoice Date While SAving"+billingDocEntity.getInvoiceDate());
			billingDocEntity.setPaymentDate(DateUtility.getDateWithTimeZone("IST", invoiceDate));
			logger.log(Level.SEVERE," Payment Date While SAving"+billingDocEntity.getPaymentDate());
		}
		
		/**
		 * ends here
		 */
		if(contractEntity.getPaymentMethod()!=null)
			billingDocEntity.setPaymentMethod(contractEntity.getPaymentMethod());
		if(contractEntity.getApproverName()!=null)
			billingDocEntity.setApproverName(contractEntity.getApproverName());
		if(contractEntity.getEmployee()!=null)
			billingDocEntity.setEmployee(contractEntity.getEmployee());
		if(contractEntity.getBranch()!=null)
			billingDocEntity.setBranch(contractEntity.getBranch());

		ArrayList<ContractCharges>billTaxArrROHAN=new ArrayList<ContractCharges>();
		if(contractEntity.getProductTaxes().size()!=0){
			ArrayList<ContractCharges>billTaxArr=new ArrayList<ContractCharges>();
			billTaxLis=this.listForBillingTaxes(contractEntity, 100);
			billTaxArr.addAll(billTaxLis);
			billingDocEntity.setBillingTaxes(billTaxArr);
			billTaxArrROHAN.addAll(billTaxLis);
		}
		
		if(contractEntity.getStartDate()!=null){
			billingDocEntity.setContractStartDate(DateUtility.getDateWithTimeZone("IST", contractEntity.getStartDate()));
		}
		if(contractEntity.getEndDate()!=null){
			billingDocEntity.setContractEndDate(DateUtility.getDateWithTimeZone("IST", contractEntity.getEndDate()));
		}
		
		double grossValue=contractEntity.getTotalAmount();
		billingDocEntity.setGrossValue(grossValue);
		
		double totBillAmt=contractEntity.getTotalBillAmt(salesProdLis);
		double taxAmt= contractEntity.getTotalFromTaxTable(billTaxArrROHAN);

		System.out.println("totBillAmt" +totBillAmt);
		System.out.println(" totBillAmt with round == "+Math.round(totBillAmt));
		
		System.out.println("taxAmt "+taxAmt);
		System.out.println("taxAmt with round"+Math.round(taxAmt));
		
		double totalBillingAmount = totBillAmt+taxAmt;
		System.out.println("Toatl billing amount without round ===  "+totalBillingAmount);
		System.out.println("Toatl billing amount with round ===  "+Math.round(totalBillingAmount));
		
		billingDocEntity.setTotalBillingAmount(totalBillingAmount);
		
		billingDocEntity.setOrderCreationDate(contractEntity.getCreationDate());
		
		DecimalFormat df = new DecimalFormat("0.00");
		
		paymentTerms.setPayTermDays(0);
		paymentTerms.setPayTermPercent(100d);
		paymentTerms.setPayTermComment("Payment Recieved");
		billingPayTerms.add(paymentTerms);
		billingDocEntity.setArrPayTerms(billingPayTerms);
		billingDocEntity.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
		billingDocEntity.setStatus(BillingDocument.APPROVED);
		billingDocEntity.setAccountType(AppConstants.BILLINGACCOUNTTYPEAR);
		billingDocEntity.setOrderCformStatus("");
		billingDocEntity.setOrderCformPercent(-1);

		if(contractEntity.getNumberRange()!=null)
			billingDocEntity.setNumberRange(contractEntity.getNumberRange());
		if(contractEntity.getCustomerGSTNumber()!=null && !contractEntity.getCustomerGSTNumber().equals(""))
		billingDocEntity.setGstinNumber(contractEntity.getCustomerGSTNumber());
		
		billingDocEntity.setBillingPeroidFromDate(contractEntity.getStartDate());
		billingDocEntity.setBillingPeroidToDate(contractEntity.getEndDate());
		
		
		/** Date 03-09-2017 added by vijay for total amount before taxes**/
		billingDocEntity.setTotalAmount(totBillAmt);
		/** Date 05-09-2017 added by vijay for Discount amount**/
		double discountamt = 0;
		if(contractEntity.getDiscountAmt()!=0){
			discountamt = contractEntity.getDiscountAmt();
			billingDocEntity.setDiscountAmt(discountamt);
		}
		/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
		double totalfinalAmt=totBillAmt-discountamt;
		billingDocEntity.setFinalTotalAmt(totalfinalAmt);
		 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
		double totalamtincludingtax=totalfinalAmt+taxAmt;
		billingDocEntity.setTotalAmtIncludingTax(totalamtincludingtax);
		 /** Date 05-09-2017 added by vijay for roundoff amount **/
		double roundoff=0;
		if(contractEntity.getRoundOffAmt()!=0){
			roundoff=contractEntity.getRoundOffAmt();
			billingDocEntity.setRoundOffAmt(roundoff);
		}
		
		/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
		billingDocEntity.setGrandTotalAmount(totalamtincludingtax);
		
		/*** Date 10-10-2017 added by vijay for total billing amount *********/
		billingDocEntity.setTotalBillingAmount(Math.round(totalamtincludingtax+roundoff));
		
		/**
		 * Date :24-10-2017 BY ANIL
		 * Copying contract reference no to billing reference 
		 * as required by NBHC
		 */
		if(contractEntity.getRefNo()!=null){
			billingDocEntity.setRefNumber(contractEntity.getRefNo());
		}
		/**
		 * End
		 */
		/**
		 *  nidhi
		 *  Date : 4-12-2017
		 *  For copy configration details to bill
		 */
		if(confiFlag){
			billingDocEntity.setBillingCategory(contractEntity.getCategory());
			billingDocEntity.setBillingType(contractEntity.getType());
			billingDocEntity.setBillingGroup(contractEntity.getGroup());
		}
		/**
		 *  end
		 */
		/** Date 06/2/2018 added by komal for consolidate price checkbox **/
		billingDocEntity.setConsolidatePrice(contractEntity.isConsolidatePrice());
		/** 
		 * end komal
		 */
		/**
		 * nidhi
		 * Date : 29-01-2018
		 * save poc name from multiple contact list
		 */
		if(contractEntity.getPocName()!=null && !contractEntity.getPocName().trim().equals("")){
			billingDocEntity.getPersonInfo().setPocName(contractEntity.getPocName());
		}
		/** 
		 * end
		 */
		/**
		 *  nidhi
		 *  27-04-2018
		 *  for contract renewal flag
		 */
		billingDocEntity.setRenewContractFlag(contractEntity.isRenewContractFlag());
		
		
		List<ContractCharges> billingtaxlist = billingDocEntity.getBillingTaxes();
		List<ContractCharges> taxBillingArr = new ArrayList<ContractCharges>();
		if(billingtaxlist.size()!=0){
			System.out.println("SIZE====="+billingtaxlist.size());
			for(int k=0;k<billingtaxlist.size();k++){
				double totalCalcAmt=0;
				totalCalcAmt = billingtaxlist.get(k).getTaxChargeAssesVal()*billingtaxlist.get(k).getTaxChargePercent()/100;
				billingtaxlist.get(k).setPayableAmt(totalCalcAmt);
				System.out.println("AMONT+++"+totalCalcAmt);
				taxBillingArr.add(billingtaxlist.get(k));
			}
			billingDocEntity.setBillingTaxes(taxBillingArr);
		
		}
		
		double payableamt=0;
		payableamt = calculateTotalBillAmt(billingDocEntity.getSalesOrderProducts()) + calculateTotalTaxes(billingDocEntity.getBillingTaxes()) + calculateTotalBillingCharges(billingDocEntity.getBillingOtherCharges());
		payableamt=Math.round(payableamt);
		System.out.println("Payable amt =="+payableamt);
		
		
//		logger.log(Level.SEVERE, "billing payableamt "+payableamt);
//		billingDocEntity.setTotalBillingAmount(payableamt);
//		
//		logger.log(Level.SEVERE, "billing totalfinalAmt "+totalfinalAmt);
//
//		/** Date 03-09-2017 added by vijay for total amount before taxes**/
//		billingDocEntity.setTotalAmount(totalfinalAmt);
//		/** Date 05-09-2017 added by vijay for total amount after discount amount before taxes**/
//		billingDocEntity.setFinalTotalAmt(totalfinalAmt);
//		 /** Date 05-09-2017 added by vijay for total amount after taxes and before other charges **/
//		billingDocEntity.setTotalAmtIncludingTax(payableamt);
//		/*** Date 06-09-2017 added by vijay for grand total before roundoff *********/
//		billingDocEntity.setGrandTotalAmount(payableamt);
		
		/**
		 *  nidhi
		 *  Date : 4-12-2017
		 *  For copy configration details to bill
		 */
		if(confiFlag){
			billingDocEntity.setBillingCategory(contractEntity.getCategory());
			billingDocEntity.setBillingType(contractEntity.getType());
			billingDocEntity.setBillingGroup(contractEntity.getGroup());
		}
		/**
		 *  end
		 */
		/** date 06-02-2018 added by komal for consolidate price**/
		billingDocEntity.setConsolidatePrice(contractEntity.isConsolidatePrice());
		/**
		 * end komal
		 */
		/**
		 * nidhi
		 * Date : 29-01-2018
		 * save poc name from multiple contact list
		 */
		if(contractEntity.getPocName()!=null && !contractEntity.getPocName().trim().equals("")){
			billingDocEntity.getPersonInfo().setPocName(contractEntity.getPocName());
		}
		/** 
		 * end
		 */
		
		GenricServiceImpl impl=new GenricServiceImpl();
		ReturnFromServer server = new ReturnFromServer();
		server = impl.save(billingDocEntity);
		int billingId = server.count;
		logger.log(Level.SEVERE,"Here Billing created successfully"+billingId);
		
		List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
		
		BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
		
		if(billingDocEntity.getContractCount()!=null){
			billingDocDetails.setOrderId(billingDocEntity.getContractCount());
		}
		
		billingDocDetails.setBillId(billingId);
		
		if(billingDocEntity.getBillingDate()!=null)
			billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
		if(billingDocEntity.getBillAmount()!=null){
//			billingDocDetails.setBillAmount(payableamt);
			billingDocDetails.setBillAmount(billingDocEntity.getTotalBillingAmount());
		}
		if(billingDocEntity.getStatus()!=null)
			billingDocDetails.setBillstatus(billingDocEntity.getStatus());
		billtablelis.add(billingDocDetails);
		
		billingDocEntity.setArrayBillingDocument(billtablelis);
		ofy().save().entities(billingDocEntity);
		
		msg = createInvoice(billingDocEntity,contractEntity.getPaymentMethod(),advancePayment,contractEntity,true);
		
		} catch (Exception e) {
			e.printStackTrace();
			return "Failed - "+e.getMessage();
		}
		
		return msg;
	}

	private String createInvoice(BillingDocument billingDocEntity, String paymentMethod, double advancePayment, Contract contractEntity, boolean contractPDFEmailFlag) {
		
		try {
		
			List<BillingDocumentDetails> billtablelis = new ArrayList<BillingDocumentDetails>();
			BillingDocumentDetails billingDocDetails=new BillingDocumentDetails();
			
			if(billingDocEntity.getContractCount()!=null){
				billingDocDetails.setOrderId(billingDocEntity.getContractCount());
			}
			if(billingDocEntity.getCount()!=0)
				billingDocDetails.setBillId(billingDocEntity.getCount());
			if(billingDocEntity.getBillingDate()!=null)
				billingDocDetails.setBillingDate(billingDocEntity.getBillingDate());
			if(billingDocEntity.getTotalBillingAmount()!=null)
				billingDocDetails.setBillAmount(billingDocEntity.getTotalBillingAmount());
			if(billingDocEntity.getStatus()!=null)
				billingDocDetails.setBillstatus(billingDocEntity.getStatus());
			billtablelis.add(billingDocDetails);
			
			ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
			billtablearr.addAll(billtablelis);
			billingDocEntity.setArrayBillingDocument(billtablelis);
			
			
		boolean approveInvoiceCreatePaymentDocFlag = true;
		GeneralServiceImpl genimpl = new GeneralServiceImpl();
		logger.log(Level.SEVERE,"calling create invoice method");
		String strmsg = genimpl.createTaxInvoice(billingDocEntity, AppConstants.CREATETAXINVOICE, null, null, null, null, null,approveInvoiceCreatePaymentDocFlag);
		logger.log(Level.SEVERE,"After create invoice method");

		Invoice invoiceEntity = ofy().load().type(Invoice.class).filter("companyId", billingDocEntity.getCompanyId())
								.filter("contractCount", billingDocEntity.getContractCount()).first().now();
		logger.log(Level.SEVERE,"invoiceEntity "+invoiceEntity);
		String msg = "";
		if(invoiceEntity!=null){
			 
			 msg = createPaymentDetails(invoiceEntity,paymentMethod,advancePayment);
//			 Email sendEmail = new Email();
//			 sendEmail.initiateInvoiceEmail(invoiceEntity);
			 
			 Email sendEmail = new Email();
			 
			 Company companyEntity = ofy().load().type(Company.class).filter("companyId", contractEntity.getCompanyId()).first().now();
			 Customer customerEntity = ofy().load().type(Customer.class).filter("companyId", contractEntity.getCompanyId())
	 					.filter("count", contractEntity.getCinfo().getCount()).first().now();
			 ArrayList<String> toemailId = new ArrayList<String>();
			 toemailId.add(customerEntity.getEmail());
			 
			 if(contractPDFEmailFlag){
				 EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("templateName", AppConstants.DIGITALCONTRACT)
					 		.filter("templateStatus", true).first().now();
			 
			 
			 if(emailTemplate!=null){
				
				 
				 EmailDetails emaildetails = new EmailDetails();
				 emaildetails.setEmailBody(emailTemplate.getEmailBody());
				 emaildetails.setSubject(emailTemplate.getSubject());
				 emaildetails.setPdfAttachment(true);
				 emaildetails.setEntityName(AppConstants.CONTRACT);
				 emaildetails.setModel(contractEntity);
				 emaildetails.setFromEmailid(companyEntity.getEmail());
				 emaildetails.setToEmailId(toemailId);
				 
				 sendEmail.sendEmail(emaildetails, contractEntity.getCompanyId());
				 
			 }
			 else{
				 sendEmail.setServiceContract(contractEntity);
				 sendEmail.initializeServiceContract();
				 sendEmail.initializeServCont();

			 }
			 }

			 EmailTemplate invoiceEmailTemplate = ofy().load().type(EmailTemplate.class).filter("templateName", AppConstants.DIGITALINVOICE)
					 		.filter("templateStatus", true).first().now();
			 if(invoiceEmailTemplate!=null){
				 
				 EmailDetails email = new EmailDetails();
				 email.setEmailBody(invoiceEmailTemplate.getEmailBody());
				 email.setSubject(invoiceEmailTemplate.getSubject());
				 email.setPdfAttachment(true);
				 email.setEntityName("Invoice");
				 email.setModel(invoiceEntity);
				 email.setFromEmailid(companyEntity.getEmail());
				 
				 email.setToEmailId(toemailId);
				 sendEmail.sendEmail(email, invoiceEntity.getCompanyId());
			 }
			 else{
				 sendEmail.initiateInvoiceEmail(invoiceEntity);

			 }
			 
		}
		else{
			return "Failed - invoice not found";
		}
			return msg; 
				
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
			
		}
		
		
	}

	private String createPaymentDetails(Invoice invoice, String paymentMethod, double advancePayment) {
		
		try {
			
		
		Approvals model = sendApprovalRequest(invoice.getCount(),invoice.getApproverName(),invoice.getCompanyId(),invoice.getBranch(),invoice.getEmployee(),
							invoice.getCreatedBy(),ApproverFactory.INVOICEDETAILS);
		
		if(model!=null){
			String taskName="ApprovalTaskQueueProcess"+"$"+model.getCompanyId()+"$"+model.getId()+"$"+model.getStatus();
			Queue queue = QueueFactory.getQueue("documentCancellation-queue");
			queue.add(TaskOptions.Builder.withUrl("/slick_erp/documentCancellationQueue").param("taskKeyAndValue", taskName));
		}
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Failed - "+e.getMessage();
		}
		
		CustomerPayment customerPayment = ofy().load().type(CustomerPayment.class).filter("companyId", invoice.getCompanyId())
				.filter("contractCount", invoice.getContractCount()).first().now();
		logger.log(Level.SEVERE,"customerPayment "+customerPayment);
		if(customerPayment!=null){
			double invoiceamt=invoice.getInvoiceAmount();
			int payAmt=(int)(invoiceamt);
			customerPayment.setPaymentAmt(payAmt);
			
			if(advancePayment>0) {
				customerPayment.setPaymentReceived(advancePayment);
				logger.log(Level.SEVERE,"payAmt advancePayment "+advancePayment);
				customerPayment.setNetPay(advancePayment);
			}
			else {
				customerPayment.setPaymentReceived(payAmt);
				logger.log(Level.SEVERE,"payAmt "+payAmt);
				customerPayment.setNetPay(payAmt);
			}
			
			

			double taxAmount = 0;
			taxAmount = Math.round(invoice.getTotalAmtIncludingTax() - (invoice.getFinalTotalAmt() + invoice.getTotalOtherCharges()));
			logger.log(Level.SEVERE,"taxAmount "+taxAmount);
			logger.log(Level.SEVERE,"invoice.getNetPayable() - taxAmount "+(invoice.getNetPayable() - taxAmount));
			if(advancePayment>0) {
				if(advancePayment>taxAmount) {
					customerPayment.setReceivedTax(taxAmount);
					customerPayment.setReceivedRevenue(advancePayment-taxAmount);
				}
				else {
					customerPayment.setReceivedTax(taxAmount);
					customerPayment.setReceivedRevenue(0);
				}
				customerPayment.setStatus(CustomerPayment.CLOSED);

			}
			else {
				customerPayment.setReceivedRevenue(invoice.getNetPayable() - taxAmount);
				customerPayment.setReceivedTax(taxAmount);
				customerPayment.setStatus(CustomerPayment.CREATED);
			}
			if(paymentMethod!=null){
				customerPayment.setPaymentMethod(paymentMethod);
			}
			if(invoice.getRefNumber()!=null && !invoice.getRefNumber().equals("0")) {
				customerPayment.setReferenceNo(invoice.getRefNumber());
			}
			customerPayment.setRefNumber("");
			
			customerPayment.setAmountTransferDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			
			customerPayment.setPaymentDate(DateUtility.getDateWithTimeZone("IST", new Date()));
			
			ofy().save().entity(customerPayment);
			
			if(advancePayment>0) {
				createBalancePaymentDocument(customerPayment);
			}
		}
		else{
			return "Failed - payment Document not found";
		}

		} catch (Exception e) {
			e.printStackTrace();
			return "Failed - "+ e.getMessage();
		}
		
		return "Success";
	}

	

	private Approvals sendApprovalRequest(int count, String username, long companyId, String branch, String employee,
						String createdby, String documentName) {
			
			logger.log(Level.SEVERE, "Approval request Sending =="+count);
			Approvals approval = new Approvals();
			try {
			approval.setApproverName(username);
			approval.setBranchname(branch);
			approval.setBusinessprocessId(count);
			if(username!=null){
			approval.setRequestedBy(username);
			}
			if(employee!=null)
			approval.setPersonResponsible(employee);
			if(createdby!=null){
			approval.setDocumentCreatedBy(createdby);
			}
			approval.setApprovalLevel(1);
			approval.setStatus(Approvals.APPROVED);
			approval.setBusinessprocesstype(documentName);
			approval.setDocumentValidation(false);
			approval.setCompanyId(companyId);
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(approval);
			logger.log(Level.SEVERE, "Approval Sent successfully"+documentName);
			
			} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception"+e.getMessage());
			}
			return approval;
}

	private double calculateTotalBillingCharges(List<ContractCharges> billingOtherCharges) {

		List<ContractCharges> list = billingOtherCharges;
		double sum = 0;
		for (int i = 0; i < list.size(); i++) {
			ContractCharges entity = list.get(i);
			sum = sum+ (entity.getChargesBalanceAmt() * entity.getPaypercent() / 100);
		}
		return sum;

	}


	private double calculateTotalTaxes(List<ContractCharges> billingTaxes) {

		List<ContractCharges>list=billingTaxes;
		double sum=0;
		for(int i=0;i<list.size();i++)
		{
			ContractCharges entity=list.get(i);
			sum=sum+(entity.getTaxChargeAssesVal()*entity.getTaxChargePercent()/100);
		}
		System.out.println("Sum Taxes"+sum);
		return sum;

	}

	private double calculateTotalBillAmt(ArrayList<SalesOrderProductLineItem> salesOrderProducts) {

		System.out.println("Entered in table calculate");
		List<SalesOrderProductLineItem>list=salesOrderProducts;

		double caltotalamt=0;
		for(int i=0;i<list.size();i++)
		{
			SalesOrderProductLineItem entity=list.get(i);
			
			if(entity.getFlatDiscount()==0)
			{
				caltotalamt=caltotalamt+(entity.getBaseBillingAmount()*entity.getPaymentPercent())/100;
			}
			else
			{
				caltotalamt=caltotalamt+((entity.getBaseBillingAmount()*entity.getPaymentPercent())/100)-entity.getFlatDiscount();
			}
		}
		return caltotalamt;

	}
	
	public List<ContractCharges> listForBillingTaxes(Contract contractEntity, double paymentPercent) {
		ArrayList<ContractCharges> arrBillTax=new ArrayList<ContractCharges>();
		double assessValue=0;
		for(int i=0;i<contractEntity.getProductTaxes().size();i++){
			ContractCharges taxDetails=new ContractCharges();
			taxDetails.setTaxChargeName(contractEntity.getProductTaxes().get(i).getChargeName());
			taxDetails.setTaxChargePercent(contractEntity.getProductTaxes().get(i).getChargePercent());
			assessValue=contractEntity.getProductTaxes().get(i).getAssessableAmount()*paymentPercent/100;
			logger.log(Level.SEVERE,"get assess value" + contractEntity.getProductTaxes().get(i).getAssessableAmount()*paymentPercent/100);
			taxDetails.setTaxChargeAssesVal(assessValue);
			logger.log(Level.SEVERE,"get assess value"+contractEntity.getProductTaxes().get(i).getAssessableAmount());
			taxDetails.setIdentifyTaxCharge(contractEntity.getProductTaxes().get(i).getIndexCheck());
			arrBillTax.add(taxDetails);
		}
		return arrBillTax;
	}

	private List<SalesOrderProductLineItem> retrieveSalesproduct(Contract model, int paymentermPercentage) {
		

		ArrayList<SalesOrderProductLineItem> salesProdArr=new ArrayList<SalesOrderProductLineItem>();
		double totalTax=0,prodPrice=0,baseBillingAmount=0,percAmt=0,discAmt=0;
		String prodDesc1="",prodDesc2="";
		for(int i=0;i<model.getItems().size();i++)
		{
			SuperProduct superProdEntity=ofy().load().type(SuperProduct.class).filter("count", model.getItems().get(i).getPrduct().getCount()).first().now();
			if(superProdEntity!=null){
				System.out.println("Superprod Not Null");
				if(superProdEntity.getComment()!=null){
					System.out.println("Desc 1 Not Null");
					prodDesc1=superProdEntity.getComment();
				}
				if(superProdEntity.getCommentdesc()!=null){
					System.out.println("Desc 2 Not Null");
					prodDesc2=superProdEntity.getCommentdesc();
				}
			}
			
			SalesOrderProductLineItem salesOrder=new SalesOrderProductLineItem();
			//SuperProduct productEnt=this.getItems().get(i).getPrduct();
			salesOrder.setProdId(model.getItems().get(i).getPrduct().getCount());
			salesOrder.setProdCategory(model.getItems().get(i).getProductCategory());
			salesOrder.setProdCode(model.getItems().get(i).getProductCode());
			salesOrder.setProdName(model.getItems().get(i).getProductName());
			salesOrder.setQuantity(model.getItems().get(i).getQty());
			salesOrder.setOrderDuration(model.getItems().get(i).getDuration());
			salesOrder.setOrderServices(model.getItems().get(i).getNumberOfServices());
			
			totalTax=model.removeTaxAmt(model.getItems().get(i).getPrduct());
			System.out.println("totalTax =="+totalTax);
			prodPrice=model.getItems().get(i).getPrice()-totalTax;
			System.out.println("Prod Price"+prodPrice);
			salesOrder.setPrice(prodPrice);
			
			salesOrder.setVatTaxEdit(model.getItems().get(i).getVatTaxEdit());
			salesOrder.setServiceTaxEdit(model.getItems().get(i).getServiceTaxEdit());
			salesOrder.setVatTax(model.getItems().get(i).getVatTax());
			salesOrder.setServiceTax(model.getItems().get(i).getServiceTax());
			
			salesOrder.setProdPercDiscount(model.getItems().get(i).getPercentageDiscount());
			salesOrder.setProdDesc1(prodDesc1);
			salesOrder.setProdDesc2(prodDesc2);
			if(model.getItems().get(i).getUnitOfMeasurement()!=null){
				salesOrder.setUnitOfMeasurement(model.getItems().get(i).getUnitOfMeasurement());
			}
			salesOrder.setArea(model.getItems().get(i).getArea());
			salesOrder.setDiscountAmt(model.getItems().get(i).getDiscountAmt());
			
			double area =0;
			if(!model.getItems().get(i).getArea().equalsIgnoreCase("NA"))
			{
				area = Double.parseDouble(model.getItems().get(i).getArea());
			}
			else
			{
				area = 1.0;
			}
			
			
			if((model.getItems().get(i).getPercentageDiscount()==null && model.getItems().get(i).getPercentageDiscount()==0) && (model.getItems().get(i).getDiscountAmt()==0) ){
				
				System.out.println("inside both 0 condition");
				percAmt=prodPrice * area;
			}
			
			else if((model.getItems().get(i).getPercentageDiscount()!=null)&& (model.getItems().get(i).getDiscountAmt()!=0)){
				
				System.out.println("inside both not null condition");
				percAmt=prodPrice * area;
				percAmt=percAmt-(percAmt*model.getItems().get(i).getPercentageDiscount()/100);
				percAmt=percAmt-(model.getItems().get(i).getDiscountAmt());
			}
			else 
			{
				System.out.println("inside oneof the null condition");
				
					if(model.getItems().get(i).getPercentageDiscount()!=null && model.getItems().get(i).getPercentageDiscount()!=0){
						System.out.println("inside getPercentageDiscount oneof the null condition");
						percAmt=prodPrice * area;
						percAmt=percAmt-(percAmt*model.getItems().get(i).getPercentageDiscount()/100);
					}
					else 
					{
						System.out.println("inside getDiscountAmt oneof the null condition");
						percAmt=prodPrice * area;
						percAmt=percAmt-(model.getItems().get(i).getDiscountAmt());
					}
			}
			
			salesOrder.setTotalAmount(percAmt);
			
			if(paymentermPercentage!=0){
				baseBillingAmount=(percAmt*paymentermPercentage)/100;
			}
		
			salesOrder.setBaseBillingAmount(baseBillingAmount);
			salesOrder.setPaymentPercent(100.0);
			
			/**
			 * Date : 04-08-2017 By Anil
			 * Setting base bill amount to base payment amount
			 */
			salesOrder.setBasePaymentAmount(Math.round(baseBillingAmount));
			/**
			 * End
			 */
			
			salesOrder.setIndexVal(i+1);
			/**
			 * nidhi
			 * 20-06-2018
			 * for product sr no
			 */
			salesOrder.setProductSrNumber(model.getItems().get(i).getProductSrNo());
			salesOrder.setPrduct(superProdEntity);
			/**
			 * nidhi
			 * 10-08-2018
			 * 
			 */
			salesOrder.setProModelNo(model.getItems().get(i).getProModelNo());
			salesOrder.setProSerialNo(model.getItems().get(i).getProSerialNo());
			/**
			 * end
			 */
			
			salesOrder.setBillingDocBaseAmount(salesOrder.getBaseBillingAmount());

			salesProdArr.add(salesOrder);
		}
		return salesProdArr;
		
	   
	}

	private ArrayList<BranchWiseScheduling> getshceduleBranchServiceAddress(String branchName, String unitOfMeasurement) {
		ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
		BranchWiseScheduling custBranchSchedule = new BranchWiseScheduling();
		custBranchSchedule.setBranchName("Service Address");
		custBranchSchedule.setCheck(true);
		custBranchSchedule.setServicingBranch(branchName);
		custBranchSchedule.setArea(0);
		custBranchSchedule.setUnitOfMeasurement(unitOfMeasurement);
		custBranchSchedule.setServiceDuration(0);
		scheduleBranchlist.add(custBranchSchedule);
		return scheduleBranchlist;
	}
	
	private Date getContractEndDate(List<SalesLineItem> items) {
		Date endDate = items.get(0).getEndDate();
		Date Date = items.get(0).getEndDate();
		for(SalesLineItem salesline : items){
			if(salesline.getEndDate().after(Date)){
				endDate = salesline.getEndDate();
			}
		}
		return endDate;
	}

	private Date getcontractStartDate(List<SalesLineItem> items) {
		Date startDate = items.get(0).getStartDate();
		Date Date = items.get(0).getStartDate();
		for(SalesLineItem salesline : items){
			if(salesline.getStartDate().before(Date)){
				startDate = salesline.getStartDate();
			}
		}
		return startDate;
	}
		
	
	private ArrayList<ServiceSchedule> createServiceList(List<SalesLineItem> salesitemlis, long companyId, String branchName) {

		ArrayList<ServiceSchedule> serschelist=new ArrayList<ServiceSchedule>();
		serschelist.clear();
		
		for(int i=0;i<salesitemlis.size();i++){	
			int qty=(int) salesitemlis.get(i).getQty();
			for(int k=0;k < qty;k++){
				System.out.println("Inside branch ===");
				long noServices=(long) (salesitemlis.get(i).getNumberOfServices());
				
				if(noServices>0) {
					
					int noOfdays=salesitemlis.get(i).getDuration();
					int interval= (int) (noOfdays/salesitemlis.get(i).getNumberOfServices());
					
					Date servicedate=(salesitemlis.get(i).getStartDate());
					Date d=new Date(servicedate.getTime());
					
					Date productEndDate= new Date(servicedate.getTime());
					Calendar cal1 = Calendar.getInstance();
					cal1.setTime(DateUtility.getDateWithTimeZone("IST", productEndDate));
					cal1.add(Calendar.DATE, noOfdays); 
					
					Date prodenddate=cal1.getTime();
					productEndDate=prodenddate;
					System.out.println("productEndDate =="+productEndDate);
					Date tempdate=new Date();
					for(int j=0;j<noServices;j++){
						System.out.println("No of services");
						if(j>0)
						{
							Calendar cal2 = Calendar.getInstance();
							cal2.setTime(d);
							cal2.add(Calendar.DATE, interval); 
							System.out.println("interval =="+interval);
							d = cal2.getTime();
							System.out.println("D =="+d);
							tempdate=d;
							System.out.println("tempdate"+tempdate);
		
						}
						
						ServiceSchedule ssch=new ServiceSchedule();
						Date Stardaate=servicedate;
						ssch.setSerSrNo(salesitemlis.get(i).getProductSrNo());
						ssch.setScheduleStartDate(DateUtility.getDateWithTimeZone("IST", Stardaate));
						ssch.setScheduleProdId(salesitemlis.get(i).getPrduct().getCount());
						System.out.println("Product Id"+salesitemlis.get(i).getPrduct().getCount());
						ssch.setScheduleProdName(salesitemlis.get(i).getProductName());
						ssch.setScheduleDuration(salesitemlis.get(i).getDuration());
						ssch.setScheduleNoOfServices(salesitemlis.get(i).getNumberOfServices());
						ssch.setScheduleServiceNo(j+1);
						ssch.setScheduleProdStartDate(DateUtility.getDateWithTimeZone("IST", Stardaate));
						ssch.setScheduleProdEndDate(DateUtility.getDateWithTimeZone("IST", productEndDate));
						ssch.setTotalServicingTime(salesitemlis.get(i).getServicingTIme());
						ssch.setScheduleServiceTime("Flexible");
						if(j==0){
							cal1.setTime(DateUtility.getDateWithTimeZone("IST", servicedate));
							int week = cal1.get(Calendar.WEEK_OF_MONTH);
							ssch.setWeekNo(week);
							ssch.setScheduleServiceDate(servicedate);
						}
						if(j!=0){
							if(productEndDate.before(d)==false){
								cal1.setTime(DateUtility.getDateWithTimeZone("IST", tempdate));
								int week = cal1.get(Calendar.WEEK_OF_MONTH);
								ssch.setWeekNo(week);
								ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST", tempdate));
							}else{
								cal1.setTime(DateUtility.getDateWithTimeZone("IST", productEndDate));
								int week = cal1.get(Calendar.WEEK_OF_MONTH);
								ssch.setWeekNo(week);
								ssch.setScheduleServiceDate(DateUtility.getDateWithTimeZone("IST", productEndDate));
							}
						}
						ssch.setScheduleProBranch("Service Address");

						ssch.setServicingBranch(branchName);
						SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
						String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (ssch.getScheduleServiceDate())));
						logger.log(Level.SEVERE,"get service Day --" +serviceDay);
						ssch.setScheduleServiceDay(serviceDay);
						ssch.setCompanyId(companyId);
						
					serschelist.add(ssch);
					
				}
			
				}
				
			}
	  }
		
		return serschelist;
	}
	
	private void createBalancePaymentDocument(CustomerPayment customerPayment) {
		
			CustomerPayment paydtls=new CustomerPayment();
			paydtls.setPersonInfo(customerPayment.getPersonInfo());
			paydtls.setContractCount(customerPayment.getContractCount());
			paydtls.setTotalSalesAmount(customerPayment.getTotalSalesAmount());
			paydtls.setArrayBillingDocument(customerPayment.getArrayBillingDocument());
			paydtls.setTotalBillingAmount(customerPayment.getTotalBillingAmount());
			paydtls.setInvoiceCount(customerPayment.getInvoiceCount());
			paydtls.setInvoiceDate(customerPayment.getInvoiceDate());
			if(customerPayment.getInvoiceType()!=null)
				paydtls.setInvoiceType(customerPayment.getInvoiceType());
			if(customerPayment.getPaymentMethod()!=null)
				paydtls.setPaymentMethod(customerPayment.getPaymentMethod());
			if(customerPayment.getContractStartDate()!=null){
				paydtls.setContractStartDate(customerPayment.getContractStartDate());
			}
			if(customerPayment.getContractEndDate()!=null){
				paydtls.setContractEndDate(customerPayment.getContractEndDate());
			}
			paydtls.setInvoiceAmount(customerPayment.getInvoiceAmount());
				
			double payableAmt=customerPayment.getPaymentAmt();
			double payreceived=customerPayment.getPaymentReceived();
			int payAmt=(int) (payableAmt-payreceived);
			paydtls.setPaymentAmt(payAmt);
			
			if(customerPayment.getPaymentDate()!=null)
				paydtls.setPaymentDate(customerPayment.getPaymentDate());
			
			paydtls.setTypeOfOrder(customerPayment.getTypeOfOrder());
			
			paydtls.setOrderCreationDate(customerPayment.getOrderCreationDate());
			if(customerPayment.getOrderCformStatus()!=null){
				paydtls.setOrderCformStatus(customerPayment.getOrderCformStatus());
			}
			if(customerPayment.getOrderCformPercent()!=-1){
				paydtls.setOrderCformPercent(customerPayment.getOrderCformPercent());
			}
			paydtls.setOrderCreationDate(customerPayment.getOrderCreationDate());
			
			paydtls.setAccountType(customerPayment.getAccountType());
			
			paydtls.setStatus(CustomerPayment.CREATED);
			
			paydtls.setBranch(customerPayment.getBranch());
			
			/**
			 * added by vijay on Date 3 march 2017
			 */
			paydtls.setAddintoPettyCash(customerPayment.isAddintoPettyCash());
			if(customerPayment.getPettyCashName()!=null)
			paydtls.setPettyCashName(customerPayment.getPettyCashName());
			
			/**
			 * ends here
			 */
			
			/**
			 * Date:25-11-2017 BY ANIL
			 * setting number range to newly generated payment document
			 */
			if(customerPayment.getNumberRange()!=null){
				paydtls.setNumberRange(customerPayment.getNumberRange());
			}
			/**
			 * End
			 */
			
			
			/** date 21/02/2018 added by komal for actual revenue and tax **/
			if(payreceived < customerPayment.getPaymentReceived()){
				double tax = customerPayment.getBalanceTax();
				
				if(payreceived < tax){
					paydtls.setBalanceRevenue(Math.round(customerPayment.getBalanceRevenue() - customerPayment.getReceivedRevenue()));
					paydtls.setBalanceTax(Math.round(tax - customerPayment.getReceivedTax()));
				}
				if(payreceived == tax){
					paydtls.setBalanceRevenue(Math.round(customerPayment.getBalanceRevenue()));
					paydtls.setBalanceTax(0);
				}
				if(payreceived > tax){
					paydtls.setBalanceRevenue(Math.round(customerPayment.getBalanceRevenue()-customerPayment.getReceivedRevenue()));
					paydtls.setBalanceTax(0);
				}
			}
			
			/*** Date 07-08-2020 Bug :- partial payment tax amount not showing and TDS set default as 0 **/
			paydtls.setTdsPercentage(0+"");
			double balanceTax = (paydtls.getPaymentAmt() - paydtls.getBalanceRevenue());
			paydtls.setBalanceTax(balanceTax);
			paydtls.setCompanyId(customerPayment.getCompanyId());
			
			GenricServiceImpl impl = new GenricServiceImpl();
			impl.save(paydtls);
			
			logger.log(Level.SEVERE,"Balance payment document created");

	}
	
	private void sendemail(Company companyEntity, String subject, String documentType, int count) {
		ArrayList<String> toEmailList = new ArrayList<String>();
		toEmailList.add("support@evasoftwaresolutions.com");
		if(companyEntity.getEmail()!=null && !companyEntity.getEmail().equals("")){
			toEmailList.add(companyEntity.getEmail());
		}
		try {
			logger.log(Level.SEVERE,"Inside Email Method");
			String mailBody = "Dear Sir, <br> "+ subject +" failed. "+documentType +" id - "+ count +". Please check log";
			Email email = new Email();
			
			email.sendEmail(subject+" failed ", mailBody, toEmailList, null, companyEntity.getEmail());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private void sendTransactionEmail(int documentId, String documentType, String fullName, Long cellNumber, int customerId,
			int orderId, Date acceptanceDateTime, String razorpayReference, double netpayable, String transactionType,
			String status, String emailTemplateName, Long companyId, String branchName) {
		
		try {
			logger.log(Level.SEVERE, "Inside sendTransactionEmail method for "+status);

		logger.log(Level.SEVERE, "emailTemplateName "+emailTemplateName);

		EmailTemplate emailTemplate = ofy().load().type(EmailTemplate.class).filter("companyId", companyId)
						.filter("templateName", emailTemplateName).first().now();
		logger.log(Level.SEVERE, "emailTemplate "+emailTemplate);

		Company company = ofy().load().type(Company.class).filter("companyId", companyId).first().now();
		
		
		String emailMessage =  getDefaultEmailTemplate();
		String emailSubject = "Digital Payment Transaction  <Successful/Unsuccessful>";
				
		if(emailTemplate!=null) {
			emailMessage = emailTemplate.getEmailBody();
			emailSubject = emailTemplate.getSubject();
		}
		
		logger.log(Level.SEVERE, "emailSubject"+emailSubject);
		logger.log(Level.SEVERE, "emailMessage"+emailMessage);

		if(emailSubject.contains("<Successful/Unsuccessful>")) {
			String strdocument = emailSubject.replace("<Successful/Unsuccessful>", status);
			emailSubject = strdocument;
		}
			
			
			if(emailMessage.contains("<Customer>")) {
				String str = fullName;
				String strdocument = emailMessage.replace("<Customer>", str);
				emailMessage = strdocument;
			}

			if(emailMessage.contains("<DocumentName - Document ID >")) {
				String str = documentType+" - "+documentId;
				String strdocument = emailMessage.replace("<DocumentName - Document ID >", str);
				emailMessage = strdocument;

			}
			if(emailMessage.contains("<Customer Name>  \\  <Mobile Number> \\ <Cusotmer ID>")) {
				String str = fullName+" \\ "+cellNumber +" \\ "+customerId;
				String strdocument = emailMessage.replace("<Customer Name>  \\  <Mobile Number> \\ <Cusotmer ID>", str);
				emailMessage = strdocument;

			}
			if(status.trim().equals(Quotation.QUOTATIONSUCESSFUL)) {
				if(emailMessage.contains("Contract -  <contract id>")) {
					String str = orderId+"";
					String strdocument = emailMessage.replace("<contract id>", str);
					emailMessage = strdocument;
				}
			}
			else {
				if(emailMessage.contains("Contract -  <contract id>")) {
					String strdocument = emailMessage.replace("Contract -  <contract id>", "");
					emailMessage = strdocument;
				}
			}
			
			if(emailMessage.contains("<Transaction Date>  \\ <Time>")) {
				String date = dateTimeFormat.format(acceptanceDateTime);
				String strdocument = emailMessage.replace("<Transaction Date>  \\ <Time>", date);
				emailMessage = strdocument;
			}
			
			logger.log(Level.SEVERE, "condition for digital ref "+(status.trim().equals(Quotation.QUOTATIONSUCESSFUL) && !transactionType.equalsIgnoreCase("Confirm Now & Pay Later")));
			logger.log(Level.SEVERE, "transactionType" +transactionType);
			logger.log(Level.SEVERE, "razorpayReference" +razorpayReference);

			if(status.trim().equals(Quotation.QUOTATIONSUCESSFUL) && !transactionType.equalsIgnoreCase("Confirm Now & Pay Later")) {
				if(emailMessage.contains("<reference id>")) {
					String reference = razorpayReference;
					if(reference!=null && !reference.equals("0")) {
						String strdocument = emailMessage.replace("<reference id>", reference);
						emailMessage = strdocument;
					}
					else {
						if(emailMessage.contains("Digital Payment Reference ID - <reference id>")) {
							String strdocument = emailMessage.replace("Digital Payment Reference ID - <reference id>", "");
							emailMessage = strdocument;
						}
					}
					
				}
				
			}
			else {
				if(emailMessage.contains("Digital Payment Reference ID - <reference id>")) {
					String strdocument = emailMessage.replace("Digital Payment Reference ID - <reference id>", "");
					emailMessage = strdocument;
				}
				
				logger.log(Level.SEVERE, "emailMessage == " +emailMessage);

			}
			if(emailMessage.contains("<Due/paid amount>")) {
				String strnetpayable = netpayable+"";
				String strdocument = emailMessage.replace("<Due/paid amount>", strnetpayable);
				emailMessage = strdocument;
			}
			
			if(emailMessage.contains("Confirm & Pay Now \\ Confirm Now & Pay Later")) {
				String strdocument = emailMessage.replace("Confirm & Pay Now \\ Confirm Now & Pay Later", transactionType);
				emailMessage = strdocument;
			}
			
			if(emailMessage.contains("<Successful/Unsuccessful/Payment Failed>")) {
				String strdocument = emailMessage.replace("<Successful/Unsuccessful/Payment Failed>", status);
				emailMessage = strdocument;
			}
		
			
			boolean branchAsCompanyFlag = false;
			
			 Branch branchEntity = ofy().load().type(Branch.class).filter("companyId", companyId).filter("buisnessUnitName", branchName)
		   				.first().now();
			 
		   if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Branch", "BranchAsCompany", companyId)) {
			   branchAsCompanyFlag = true;
			   
			   	if(branchEntity!=null) {
			   		
			   		if(emailMessage.contains("<branch/company mobile number>")) {
						String strdocument = emailMessage.replace("<branch/company mobile number>", branchEntity.getCellNumber1()+"");
						emailMessage = strdocument;
					}
			   		
			   		if(emailMessage.contains("<branch/company email id>")) {
						String strdocument = emailMessage.replace("<branch/company email id>", branchEntity.getEmail());
						emailMessage = strdocument;
					}
			   		
					if(emailMessage.contains("<Company Name>")) {
						String str = company.getBusinessUnitName();
						if(branchEntity.getCorrespondenceName()!=null && !branchEntity.getCorrespondenceName().equals("")) {
							str = branchEntity.getCorrespondenceName();
						}
						String strdocument = emailMessage.replace("<Company Name>", str);
						emailMessage = strdocument;
					}

					if(emailMessage.contains("<company / branch email>")) {
						if(branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")){
							String strdocument = emailMessage.replace("<company / branch email>", branchEntity.getEmail());
							emailMessage = strdocument;
						}
					}

					if(emailMessage.contains("<company / branch mobile>")) {
						if(branchEntity.getCellNumber1()!=null && !branchEntity.getCellNumber1().equals("0")){
							String strdocument = emailMessage.replace("<company / branch mobile>", branchEntity.getCellNumber1()+"");
							emailMessage = strdocument;
						}
					}
					
					
			   	}
			   	else {
			   		if(emailMessage.contains("<branch/company mobile number>")) {
						String strdocument = emailMessage.replace("<branch/company mobile number>", company.getCellNumber1()+"");
						emailMessage = strdocument;
					}
			   		
			   		if(emailMessage.contains("<branch/company email id>")) {
						String strdocument = emailMessage.replace("<branch/company email id>", company.getEmail());
						emailMessage = strdocument;
					}
			   		
					if(emailMessage.contains("<Company Name>")) {
						String strdocument = emailMessage.replace("<Company Name>", company.getBusinessUnitName());
						emailMessage = strdocument;
					}

					if(emailMessage.contains("<company / branch email>")) {
						if(company.getEmail()!=null && !company.getEmail().equals("")){
							String strdocument = emailMessage.replace("<company / branch email>", company.getEmail());
							emailMessage = strdocument;
						}
					}

					if(emailMessage.contains("<company / branch mobile>")) {
						if(company.getCellNumber1()!=null && !company.getCellNumber1().equals("0")){
							String strdocument = emailMessage.replace("<company / branch mobile>", company.getCellNumber1()+"");
							emailMessage = strdocument;
						}
					}
			   	}
				
		   }
		   else {
		   		
		   		if(emailMessage.contains("<branch/company mobile number>")) {
					String strdocument = emailMessage.replace("<branch/company mobile number>", company.getCellNumber1()+"");
					emailMessage = strdocument;
				}
		   		
		   		if(emailMessage.contains("<branch/company email id>")) {
					String strdocument = emailMessage.replace("<branch/company email id>", company.getEmail());
					emailMessage = strdocument;
				}
		   		
				if(emailMessage.contains("<Company Name>")) {
					String strdocument = emailMessage.replace("<Company Name>", company.getBusinessUnitName());
					emailMessage = strdocument;
				}

				if(emailMessage.contains("<company / branch email>")) {
					if(company.getEmail()!=null && !company.getEmail().equals("")){
						String strdocument = emailMessage.replace("<company / branch email>", company.getEmail());
						emailMessage = strdocument;
					}
				}

				if(emailMessage.contains("<company / branch mobile>")) {
					if(company.getCellNumber1()!=null && !company.getCellNumber1().equals("0")){
						String strdocument = emailMessage.replace("<company / branch mobile>", company.getCellNumber1()+"");
						emailMessage = strdocument;
					}
				}
				
		   }
		   
		   
		   if(emailMessage.contains("\n")) {
				String resultString = emailMessage.replaceAll("[\n]", "<br>");
				String resultString1 = resultString.replaceAll("[\\s]", "&nbsp;");
				emailMessage = resultString1;

			}
		   
		   if(!emailMessage.contains("Digital Payment Reference ID")){
			   if(emailMessage.contains("<br><br>Amount")) {
				   emailMessage = emailMessage.replace("<br><br>Amount", "<br>Amount");
			   }
			   
		   }
		   
		   EmailDetails emaildetails = new EmailDetails();
		   
		   emaildetails.setSubject(emailSubject);
		   emaildetails.setEmailBody(emailMessage);
		   
		   
		   Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count", customerId)
				   				.first().now();
		   
		   String customerEmail = "";
		   if(customer!=null && customer.getEmail()!=null && !customer.getEmail().equals("")) {
			   customerEmail = customer.getEmail();
		   }
		   String branchEmail = "";
		   logger.log(Level.SEVERE, "branchAsCompanyFlag "+branchAsCompanyFlag);

		   if(branchEntity!=null && branchEntity.getEmail()!=null && !branchEntity.getEmail().equals("")) {
			   logger.log(Level.SEVERE, "branchEntity.getEmail()"+branchEntity.getEmail());

			   branchEmail = branchEntity.getEmail();
		   }
		   
		   String companyEmail = "";

		   if(company!=null && company.getEmail()!=null && !company.getEmail().equals("")) {
			   companyEmail = company.getEmail();
		   }
		   
		   ArrayList<String> toEmailId = new ArrayList<String>();
		   toEmailId.add(customerEmail);
		   
		   
		   ArrayList<String> ccEmailId = new ArrayList<String>();
		   if(branchAsCompanyFlag) {
			   ccEmailId.add(branchEmail);
		   }
		   else{
			   ccEmailId.add(companyEmail);
		   }
		   
		   if(branchAsCompanyFlag) {
			   logger.log(Level.SEVERE, "branchEmail "+branchEmail);
			   emaildetails.setFromEmailid(branchEmail); 
		   }
		   else {
			   logger.log(Level.SEVERE, "companyEmail "+companyEmail);
			   emaildetails.setFromEmailid(companyEmail); 
		   }
		   
		   emaildetails.setToEmailId(toEmailId);
		   emaildetails.setCcEmailId(ccEmailId);
		   
		   Email email = new Email();
		   email.sendEmailToReceipient(emaildetails, companyId);
		   
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getDefaultEmailTemplate() {
		
		String emailBody = "Hello, <Customer> \n" + 
				"\n" + 
				"Thank you. Kindly note the details of digital payment transaction carried out by you.\n" + 
				"\n" + 
				"Reference  - <DocumentName - Document ID > \n" + 
				"Customer  - <Customer Name>  \\  <Mobile Number> \\ <Cusotmer ID> \n" + 
				"Contract -  <contract id> \n" + 
				"Date - <Transaction Date>  \\ <Time>\n" + 
				"Digital Payment Reference ID - <reference id> \n" + 
				"Amount  - <Due/paid amount>  \n" + 
				"Option Opted - Confirm & Pay Now \\ Confirm Now & Pay Later\n" + 
				"Status - <Successful/Unsuccessful/Payment Failed>\n" + 
				"\n" + 
				"In case you have any questions,  pls. get in touch with us on <branch/company mobile number> or email us at <branch/company email id>.\n" + 
				"\n" + 
				"\n" + 
				"Thanks & regards,\n" + 
				"<Company Name>\n" + 
				"<company / branch email>\n" + 
				"<company / branch mobile>";
		
		return emailBody;
	}

	
	private Customer CreateCustomerAndGetDetails(JSONObject jsonobj, String customerName, String customerCellNo, long companyId) {
		
		logger.log(Level.SEVERE, "inside new customer creation method");
		Customer customer = new Customer();
		boolean isCompany = false;
		if(jsonobj.optString("isCommercial")!=null){
			isCompany = true;
		}
		if(isCompany){
			String companyName = customerName;
			customer.setCompany(isCompany);
			customer.setCompanyName(companyName);
			
			String pocName = "";
			if(jsonobj.optString("customerPOC")!=null){
				pocName = jsonobj.optString("customerPOC");
			}
			if(!pocName.equals("")) {
				customer.setFullname(pocName);
			}
			else {
				customer.setFullname(companyName);
			}
		}
		else{
			customer.setFullname(customerName);
		}
		if(customerCellNo!=null && !customerCellNo.equals("")){
			long cellNo = Long.parseLong(customerCellNo);
			customer.setCellNumber1(cellNo);
		}
		
		customer.setCompanyId(companyId);
		
		String customerEmail = "";
		if(jsonobj.optString("customerEmail")!=null){
			customerEmail = jsonobj.optString("customerEmail");
		}
		customer.setEmail(customerEmail);
		
		String customerGroup = "";
		if(jsonobj.optString("customerGroup")!=null){
			customerGroup = jsonobj.optString("customerGroup");
		}
		customer.setGroup(customerGroup);
		
		String customerCategory = "";
		if(jsonobj.optString("customerCategory")!=null){
			customerCategory = jsonobj.optString("customerCategory");
		}
		customer.setCategory(customerCategory);
		
		String customerType = "";
		if(jsonobj.optString("customerType")!=null){
			customerType = jsonobj.optString("customerType");
		}
		customer.setType(customerType);
		
		
		String branch = "";
		if(jsonobj.optString("customerBranch")!=null){
			branch = jsonobj.optString("customerBranch");
		}
		customer.setBranch(branch);
		
		String salesPerson = "";
		if(jsonobj.optString("salesPerson")!=null){
			salesPerson = jsonobj.optString("salesPerson");
		}
		customer.setEmployee(salesPerson);
		
		
		String strdateOfBirth = jsonobj.optString("DOB");
		Date dateOfBirth = null;
		try {
			dateOfBirth = dateFormat.parse(strdateOfBirth);
			customer.setDob(dateOfBirth);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		boolean isserviceAddressFlag = false;
		JSONArray jsonarray = new JSONArray();
		try {
			jsonarray = jsonobj.getJSONArray("serviceAddress");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(int i=0;i<jsonarray.length();i++){
			try {
				JSONObject jobj = jsonarray.getJSONObject(i);
				
				String addressLine1 = "";
				if(jobj.optString("Address Line 1")!=null){
					addressLine1 = jobj.optString("Address Line 1");
				}
				String addressLine2 = "";
				if(jobj.optString("Address Line 2")!=null){
					addressLine2 = jobj.optString("Address Line 2");
				}
				String landmark = "";
				if(jobj.optString("Landmark")!=null){
					landmark = jobj.optString("Landmark");
				}
				String Locality = "";
				if(jobj.optString("Locality")!=null){
					Locality = jobj.optString("Locality");
				}
				String city = "";
				if(jobj.optString("City")!=null){
					city = jobj.optString("City");
				}
				String State = "";
				if(jobj.optString("State")!=null){
					State = jobj.optString("State");
				}
				String country = "";
				if(jobj.optString("Country")!=null){
					country = jobj.optString("Country");
				}
				
				int pinCode = jobj.optInt("Pin");
				
				Address serviceAddress = new Address();
				serviceAddress.setAddrLine1(addressLine1);
				serviceAddress.setAddrLine2(addressLine2);
				serviceAddress.setLandmark(landmark);
				serviceAddress.setLocality(Locality);
				serviceAddress.setCity(city);
				serviceAddress.setState(State);
				serviceAddress.setCountry(country);
				serviceAddress.setPin(pinCode);
				
				customer.setSecondaryAdress(serviceAddress);
				
				isserviceAddressFlag = true;
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
			JSONArray billingAddressjsonarray = new JSONArray();
			try {
				billingAddressjsonarray = jsonobj.getJSONArray("billingAddress");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int j=0;j<billingAddressjsonarray.length();j++){
				try {
					JSONObject jobj = jsonarray.getJSONObject(j);
					
					String addressLine1 = "";
					if(jobj.optString("Address Line 1")!=null){
						addressLine1 = jobj.optString("Address Line 1");
					}
					String addressLine2 = "";
					if(jobj.optString("Address Line 2")!=null){
						addressLine2 = jobj.optString("Address Line 2");
					}
					String landmark = "";
					if(jobj.optString("Landmark")!=null){
						landmark = jobj.optString("Landmark");
					}
					String Locality = "";
					if(jobj.optString("Locality")!=null){
						Locality = jobj.optString("Locality");
					}
					String city = "";
					if(jobj.optString("City")!=null){
						city = jobj.optString("City");
					}
					String State = "";
					if(jobj.optString("State")!=null){
						State = jobj.optString("State");
					}
					String country = "";
					if(jobj.optString("Country")!=null){
						country = jobj.optString("Country");
					}
					
					int pinCode = jobj.optInt("Pin");
					
					Address billingAddress = new Address();
					billingAddress.setAddrLine1(addressLine1);
					billingAddress.setAddrLine2(addressLine2);
					billingAddress.setLandmark(landmark);
					billingAddress.setLocality(Locality);
					billingAddress.setCity(city);
					billingAddress.setState(State);
					billingAddress.setCountry(country);
					billingAddress.setPin(pinCode);
					customer.setAdress(billingAddress);
					
					if(!isserviceAddressFlag){
						customer.setSecondaryAdress(billingAddress);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		
			//Ashwini Patil Date:18-09-2023 Rex wants to add GST Number in customer through zoho api call
			String gstNo="";
			try {
				if(jsonobj.optString("customerGSTNO")!=null){
					gstNo = jsonobj.optString("customerGSTNO");
					logger.log(Level.SEVERE, "gstNo "+gstNo);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(gstNo!=null&&!gstNo.equals("")) {
				logger.log(Level.SEVERE, "gstNo not null");
				ArrayList <ArticleType> articleList=null;
				if(customer.getArticleTypeDetails()!=null) 
					articleList=customer.getArticleTypeDetails();					
				else 
					articleList=new ArrayList<ArticleType>();
				
				String[] docNames=new String[] {"Invoice Details","Contract","Quotation","Sales Order","ServiceInvoice","SalesInvoice","SalesQuotation"};
				
				for(int i=0;i<docNames.length;i++) {
					ArticleType a=new ArticleType();
					a.setArticleTypeName("GSTIN");
					a.setArticleTypeValue(gstNo);
					a.setArticlePrint("YES");
					a.setDocumentName(docNames[i]);
					articleList.add(a);				
				}
				customer.setArticleTypeDetails(articleList);
				logger.log(Level.SEVERE, "gstNo set in article list of customer");
			}
			
			
			SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
			int customerId=(int) numGen.getLastUpdatedNumber("Customer", companyId, (long) 1);
			customer.setCount(customerId);
			
			GenricServiceImpl genimpl = new GenricServiceImpl();
			genimpl.save(customer);
			
			
		// Article Type should change into array
		return customer;
	}
	
	public double removeAllTaxes(SalesLineItem saleslineItem)
	{
		double vat = 0,service = 0;
		double tax=0,retrVat=0,retrServ=0;
			
		if(saleslineItem.getServiceTax()!=null&&saleslineItem.getServiceTax().isInclusive()==true){
				service=saleslineItem.getServiceTax().getPercentage();
		}
		if(saleslineItem.getVatTax()!=null&&saleslineItem.getVatTax().isInclusive()==true){
				vat=saleslineItem.getVatTax().getPercentage();
		}

		
		if(vat!=0&&service==0){
			retrVat=saleslineItem.getPrice()/(1+(vat/100));
			retrVat=saleslineItem.getPrice()-retrVat;
		}
		if(service!=0&&vat==0){
			retrServ=saleslineItem.getPrice()/(1+service/100);
			retrServ=saleslineItem.getPrice()-retrServ;
		}

		logger.log(Level.SEVERE, "retrServ "+retrServ);
		logger.log(Level.SEVERE, "retrVat "+retrVat);
		logger.log(Level.SEVERE, "service "+service);
		logger.log(Level.SEVERE, "vat "+vat);

		if(service!=0&&vat!=0){
			
			
				if(saleslineItem.getServiceTax().getTaxPrintName()!=null && ! saleslineItem.getServiceTax().getTaxPrintName().equals("")
				   && saleslineItem.getVatTax().getTaxPrintName()!=null && ! saleslineItem.getVatTax().getTaxPrintName().equals(""))
				{
					double dot = service + vat;
					retrServ=(saleslineItem.getPrice()/(1+dot/100));
					retrServ=saleslineItem.getPrice()-retrServ;
				}
				else
				{
					// Here if both are inclusive then first remove service tax and then on that amount
					// calculate vat.
					double removeServiceTax=(saleslineItem.getPrice()/(1+service/100));
					//double taxPerc=service+vat;
					//retrServ=(entity.getPrice()/(1+taxPerc/100)); //line changed below
					retrServ=(removeServiceTax/(1+vat/100));
					retrServ=saleslineItem.getPrice()-retrServ;
				}
		}
		logger.log(Level.SEVERE, "retrVat == "+retrVat);
		logger.log(Level.SEVERE, "retrServ == "+retrServ);
		tax=retrVat+retrServ;
		
		return tax;
	}

}
