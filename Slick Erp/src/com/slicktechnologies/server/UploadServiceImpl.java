package com.slicktechnologies.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.blobstore.BlobInfo;
import com.google.appengine.api.blobstore.BlobInfoFactory;
import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.googlecode.objectify.Objectify;






import com.slicktechnologies.server.taskqueue.DocumentUploadTaskQueue;

import static com.googlecode.objectify.ObjectifyService.ofy;;

@SuppressWarnings("serial")
public class UploadServiceImpl extends HttpServlet{
	
	BlobstoreService blobstoreservice=BlobstoreServiceFactory.getBlobstoreService();
	
	Logger logger = Logger.getLogger("Name of logger");
	 public void doPost(HttpServletRequest req, HttpServletResponse res)
		      throws ServletException, IOException {

		 ServletInputStream str= req.getInputStream();
		 //create a bufferd reader object
	 
		 DataMigrationImpl.blobkey = null;
		  Map<String, List<BlobKey>> blobs = blobstoreservice.getUploads(req);
		  System.out.println("Size of blob "+blobs.size());
		   logger.log(Level.SEVERE,"Block size -- " + blobs.size());
		    List<BlobKey> blobKey = blobs.get("upload");
		    System.out.println("Blobs "+blobKey.get(0).getKeyString());
		    
		    //********vijay **************
		    String onlyblobkey = blobKey.get(0).getKeyString();
		    
		    
		    
            String x="/blobservice?blob-key=" + blobKey.get(0).getKeyString();
            //res.sendRedirect("/testingevafinal/uploadservice?blob-key=" +x);
            res.setHeader("Content-Type", "text/html");
            PrintWriter writer = res.getWriter();
            writer.write(x);
            
            
            ExcelReadingImpl.blobkey=new BlobKey(onlyblobkey);
            DataMigrationImpl.blobkey=new BlobKey(onlyblobkey);
            DataMigrationTaskQueueImpl.blobkey=new BlobKey(onlyblobkey);
            DocumentUploadTaskQueue.blobkey = new BlobKey(onlyblobkey);
            /**
             * @author Ashwini Patil 
             * @since 23-03-2023
             * multisite contract upload was working from Service-> transactions upload -> contract upload
             * but it was not working from contract -> upload -> Multisite contract upload
             * Problem was blob key not getting set
             * so now it has been set
             */
            DocumentUploadTaskQueue.contractUploadBlobKey = new BlobKey(onlyblobkey);
		  }

		  
		  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
		      throws ServletException, IOException {
               System.out.println("ajay");
		    //Send the meta-data id back to the client in the HttpServletResponse response
		    String id = req.getParameter("blob-key");
		    resp.setHeader("Content-Type", "text/html");
		    resp.getWriter().println(id);
		   
		   

		  }


}
