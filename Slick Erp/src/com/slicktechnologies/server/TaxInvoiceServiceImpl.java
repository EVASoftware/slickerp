package com.slicktechnologies.server;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashMap;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.ObjectifyService;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.slicktechnologies.client.services.TaxInvoiceService;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.utility.ApproverFactory;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.Approvals;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.articletype.ArticleType;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.productlayer.SuperProduct;
import com.slicktechnologies.shared.common.salesorder.OtherCharges;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.BillingDocumentDetails;
import com.slicktechnologies.shared.common.salesprocess.CustomerPayment;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.VendorInvoice;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class TaxInvoiceServiceImpl extends RemoteServiceServlet implements TaxInvoiceService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5439306074849528361L;

	Logger logger = Logger.getLogger("Name of logger");

	
	@Override
	public String createTaxInvoice(BillingDocument model, String invoiceType, String invoiceCategory,
			String invoiceType2, String invoiceGruop, String approverName, String userName,
			boolean approveInvoiceCreatePaymentDocFlag) {
		
		
		logger.log(Level.SEVERE,"Inside Invoice creation method "+DateUtility.getDateWithTimeZone("IST", new Date()));
		String response="";
		if(model.getInvoiceCount()!=0&&model.getStatus().equals("Invoiced")){
			response="Invoice already created.";
			return response;
		}
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForEva",model.getCompanyId())){
			model.setRateContractServiceId(0);
		}
		if (model.getRateContractServiceId()!= 0){
			List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", model.getCompanyId()).filter("count", model.getRateContractServiceId()).list();
			if(serviceList!=null&&serviceList.size()!=0){
				for(Service obj:serviceList){
					if(!obj.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
						response="Service for this bill is not completed. Please complete it and try again!!";
						return response;
					}
				}
			}
		}
		response=createNewInvoice(model, invoiceType, invoiceCategory, invoiceType2, invoiceGruop, approverName, userName);
		logger.log(Level.SEVERE,"RESPONSE : "+response);
		return response;
	}

	public String createNewInvoice(BillingDocument model,String invoiceType, String invoiceCategory, String invoiceType2, String invoiceGruop, String approverName, String userName){

		boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType",model.getCompanyId());
		/*** Date 26-09-2019 by Vijay for NBHC CCPM Auto invoice process added here ***/
		boolean autoinvoiceflag = ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument","EnableAutoInvoice", model.getCompanyId());
		logger.log(Level.SEVERE, "autoinvoiceflag "+autoinvoiceflag);
		
		boolean uploadConsumableFlag=ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("CNC", "PC_UploadLocationWiseBudget", model.getCompanyId());
		
		Invoice inventity;
		if(model.getAccountType().equalsIgnoreCase(AppConstants.ACCOUNTTYPEAP)){
			inventity = new VendorInvoice();
		}else{
			inventity = new Invoice();
		}

		PersonInfo pinfo = new PersonInfo();
//		if (form.getTbpersonCount().getValue() != null) {
			pinfo.setCount(model.getPersonInfo().getCount());
//		}
//		if (form.getTbpersonName().getValue() != null) {
			pinfo.setFullName(model.getPersonInfo().getFullName());
//		}
//		if (form.getTbpersonCell().getValue() != null) {
			pinfo.setCellNumber(model.getPersonInfo().getCellNumber());
//		}
//		if (form.getTbpocName().getValue() != null) {
			pinfo.setPocName(model.getPersonInfo().getPocName());
//		}
			pinfo.setEmail(model.getPersonInfo().getEmail());
		if (model.getGrossValue() != 0) {
			inventity.setGrossValue(model.getGrossValue());
		}

//		if (form.getDopaytermspercent().getValue() != 0) {
			inventity.setTaxPercent(model.getArrPayTerms().get(0).getPayTermPercent());
//		}

		if (pinfo != null){
			inventity.setPersonInfo(pinfo);
		}
		
//		if(inventity.getContractCount()==0){
			inventity.setContractCount(model.getContractCount());
//		}
		
		if (model.getContractStartDate() != null) {
			inventity.setContractStartDate(model.getContractStartDate());
		}
		if (model.getContractEndDate() != null) {
			inventity.setContractEndDate(model.getContractEndDate());
		}
//		if (form.getDosalesamount().getValue()!=null)
			inventity.setTotalSalesAmount(model.getTotalSalesAmount());
		
//		List<BillingDocumentDetails> billtablelis = form.billingDocumentTable.getDataprovider().getList();
//		ArrayList<BillingDocumentDetails> billtablearr = new ArrayList<BillingDocumentDetails>();
//		billtablearr.addAll(billtablelis);
		inventity.setArrayBillingDocument(model.getArrayBillingDocument());
		
//		System.out.println("MULTIPLE CONTRAT STATUS ::: "+isMultipleContractBilling());
		if(isMultipleContractBilling(model.getArrayBillingDocument())){
			inventity.setMultipleOrderBilling(true);
			/**
			 * @author Anil
			 * @since 13-08-2020
			 * Earlier for multicontract bill we were setting contract count as zero which causes error at the 
			 * time of invoice printing
			 * So On Approval of Nitin sir and vaishali commenting this code
			 */
//			if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "OnlyForOrionPestSolutions",model.getCompanyId())==false){
//				inventity.setContractCount(0);
//			}
			inventity.setRemark(model.getRemark());
		}
		
//		if (form.getDototalbillamt().getValue() != null)
			inventity.setTotalBillingAmount(model.getTotalBillingAmount());
		
		
//		if (form.getDototalbillamt().getValue() != null)
			inventity.setNetPayable(model.getTotalBillingAmount());
		
			inventity.setDiscount(0);
		//   ends here 
		
			
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "OnlyForNBHC",model.getCompanyId())){
			inventity.setInvoiceDate(new Date());
		}else{
//			if (form.getDbinvoicedate().getValue() != null)
				inventity.setInvoiceDate(model.getInvoiceDate());
		}
		
			
//		if (form.getDbinvoicedate().getValue() != null)
//			inventity.setInvoiceDate(form.getDbinvoicedate().getValue());
//		if (form.getDbpaymentdate().getValue() != null)
			inventity.setPaymentDate(model.getPaymentDate());
//		if (form.getOlbApproverName().getValue() != null)
			inventity.setApproverName(model.getApproverName());
//		if (form.getOlbEmployee().getValue() != null)
			inventity.setEmployee(model.getEmployee());
//		if (form.getOlbbranch().getValue() != null)
			inventity.setBranch(model.getBranch());
		
		inventity.setOrderCreationDate(model.getOrderCreationDate());
		inventity.setCompanyId(model.getCompanyId());
		inventity.setInvoiceAmount(model.getTotalBillingAmount());
		inventity.setInvoiceType(invoiceType);
		inventity.setPaymentMethod(model.getPaymentMethod());
		inventity.setAccountType(model.getAccountType());
		inventity.setTypeOfOrder(model.getTypeOfOrder());
		
		if(model.getPaymentMode()!=null)
			inventity.setPaymentMode(model.getPaymentMode());
		
		if(inventity.getTypeOfOrder()==null || inventity.getTypeOfOrder().equals("")){
			logger.log(Level.SEVERE, "Type of order value"+inventity.getTypeOfOrder());
			return "Type of order is blank please contact EVA support !";
		}
		if(invoiceType.trim().equalsIgnoreCase(AppConstants.CREATETAXINVOICE.trim())){
			if(autoinvoiceflag){
				inventity.setStatus(Invoice.REQUESTED);
			}else{
				inventity.setStatus(Invoice.CREATED);
			}
		}else{
			inventity.setStatus(BillingDocument.PROFORMAINVOICE);
		}
		
//		List<SalesOrderProductLineItem>productlist=form.getSalesProductTable().getDataprovider().getList();
		List<SalesOrderProductLineItem>productlist=model.getSalesOrderProducts();
		ArrayList<SalesOrderProductLineItem>prodList=new ArrayList<SalesOrderProductLineItem>();

		/**
		 * @author Anil @since 12-11-2021
		 * Sunrise SCM
		 */
//		prodList.addAll(productlist);
		if(uploadConsumableFlag){
			try{
				logger.log(Level.SEVERE, "uploadConsumableFlag "+uploadConsumableFlag+" bill sub type "+model.getSubBillType()+" item size "+model.getSalesOrderProducts().size());
				if(model.getSubBillType()!= null && !model.getSubBillType().equals("")&&(model.getSubBillType().equals(AppConstants.FIXEDCONSUMABLES)||model.getSubBillType().equals(AppConstants.CONSUMABLES)||model.getSubBillType().equals(AppConstants.CONSOLIDATEBILL))){
					ArrayList<SalesOrderProductLineItem>mergedProdList=new ArrayList<SalesOrderProductLineItem>();
					for(SalesOrderProductLineItem item1:model.getSalesOrderProducts()){
						boolean updateFlag=false;
						logger.log(Level.SEVERE, "prod 1 "+item1.getProdName()+" bill type 1 "+item1.getBillType());
						if(item1.getBillType()!=null&&!item1.getBillType().equals("")&&item1.getBillType().equals("Consumable")){
							if(mergedProdList.size()!=0){
								for(SalesOrderProductLineItem item2:mergedProdList){
									logger.log(Level.SEVERE, "prod 1 "+item1.getProdName()+" prod 2 "+item2.getProdName()+" bill type 2 "+item2.getBillType());
									if(item1.getProdName().equals(item2.getProdName())&&item2.getBillType().equals("Consumable")){
										item2.setPrice(item2.getPrice()+item1.getPrice());
										item2.setTotalAmount(item2.getTotalAmount()+item1.getTotalAmount());
										item2.setBaseBillingAmount(item2.getBaseBillingAmount()+item1.getBaseBillingAmount());
										item2.setBasePaymentAmount(item2.getBasePaymentAmount()+item1.getBaseBillingAmount());
										
										if(item2.getSalesAnnexureMap()!=null&&item2.getSalesAnnexureMap().size()!=0&&item1.getSalesAnnexureMap()!=null&&item1.getSalesAnnexureMap().size()!=0){
											item2.getSalesAnnexureMap().get(item2.getProdName()).addAll(item1.getSalesAnnexureMap().get(item1.getProdName()));
										}
										updateFlag=true;
									}
								}
								
								if(!updateFlag){
									item1.setBillProductName(item1.getProdName());
									mergedProdList.add(item1);
									logger.log(Level.SEVERE, "Adding new  !!! ");
								}
								
							}else{
								item1.setBillProductName(item1.getProdName());
								mergedProdList.add(item1);
								
								logger.log(Level.SEVERE, "Adding for first time !!! ");
							}
						}else{
							item1.setBillProductName(item1.getProdName());
							mergedProdList.add(item1);
							
							logger.log(Level.SEVERE, "Different billing type !!! ");
						}
					}
					logger.log(Level.SEVERE, "mergedProdList "+mergedProdList.size());
					prodList.addAll(mergedProdList);
				}else{
					prodList.addAll(productlist);
				}
			}catch(Exception e){
				prodList.addAll(productlist);
			}
		}else{
			prodList.addAll(productlist);
		}
		System.out.println("SALES PRODUCT TABLE SIZE ::: "+prodList.size());
				
//		List<OtherCharges>OthChargeTbl=form.tblOtherCharges.getDataprovider().getList();
		List<OtherCharges>OthChargeTbl=model.getOtherCharges();
		ArrayList<OtherCharges>ocList=new ArrayList<OtherCharges>();
		ocList.addAll(OthChargeTbl);
		System.out.println("OTHER TAXES TABLE SIZE ::: "+ocList.size());
		
//		List<ContractCharges>taxestable=form.getBillingTaxTable().getDataprovider().getList();
		List<ContractCharges>taxestable=model.getBillingTaxes();
		ArrayList<ContractCharges>taxesList=new ArrayList<ContractCharges>();
		taxesList.addAll(taxestable);
		System.out.println("TAXES TABLE SIZE ::: "+taxesList.size());
		
//		List<ContractCharges>otherChargesable=form.getBillingChargesTable().getDataprovider().getList();
		List<ContractCharges>otherChargesable=model.getBillingOtherCharges();
		ArrayList<ContractCharges>otherchargesList=new ArrayList<ContractCharges>();
		otherchargesList.addAll(otherChargesable);
		System.out.println("OTHER CHARGES TABLE SIZE ::: "+otherchargesList.size());
		
		ArrayList<SalesOrderProductLineItem> updatedProdList=new ArrayList<SalesOrderProductLineItem>();
		for (SalesOrderProductLineItem prod : prodList) {
			
			if(prod.getBillingDocBaseAmount()==0||prod.getBillingDocBaseAmount()==0.0){
				prod.setBillingDocBaseAmount(prod.getBaseBillingAmount());
			}
			prod.setBaseBillingAmount(prod.getBasePaymentAmount());
			prod.setPaymentPercent(100);
			prod.setBasePaymentAmount(prod.getBasePaymentAmount());
			
			updatedProdList.add(prod);
			
		}
		inventity.setSalesOrderProductFromBilling(updatedProdList);
		
		
		inventity.setOtherCharges(ocList);
//		if(form.dbOtherChargesTotal.getValue()!=null){
			inventity.setTotalOtherCharges(model.getTotalOtherCharges());
//		}
		
		inventity.setBillingTaxes(taxesList);
		inventity.setBillingOtherCharges(otherchargesList);
		
		inventity.setCustomerBranch(model.getCustomerBranch());

		inventity.setAccountType(model.getAccountType());
		inventity.setArrPayTerms(model.getArrPayTerms());
		
//		if (form.getOlbbillinggroup().getValue() != null) {
			inventity.setInvoiceGroup(model.getGroup());
//		}
		if (model.getOrderCformStatus() != null) {
			inventity.setOrderCformStatus(model.getOrderCformStatus());
		}
		if (model.getOrderCformPercent() != 0&& model.getOrderCformPercent() != -1) {
			inventity.setOrderCformPercent(model.getOrderCformPercent());
		} else {
			inventity.setOrderCformPercent(-1);
		}
		
		if(model.getNumberRange()!=null)
			inventity.setNumberRange(model.getNumberRange());
		
//		if(form.getDbbillingperiodFromDate().getValue()!=null)
			inventity.setBillingPeroidFromDate(model.getBillingPeroidFromDate());
//		if(form.getDbbillingperiodToDate().getValue()!=null)
			inventity.setBillingPeroidToDate(model.getBillingPeroidToDate());
		
//		if (form.getTbSegment().getValue() != null)
			inventity.setSegment(model.getSegment());
		
//		if (!form.getTbrateContractServiceid().getValue().equals(""))
			inventity.setRateContractServiceId(model.getRateContractServiceId());
		
//		if(form.getQuantity().getValue() !=null && form.getQuantity().getValue() > 0){
			inventity.setQuantity(model.getQuantity());
//		}
		
//		if(form.getOlbUOM().getValue()!=null){
			inventity.setUom(model.getUom());
//		}
		
//		if(form.getTbReferenceNumber().getValue()!=null){
			inventity.setRefNumber(model.getRefNumber());
//		}
	
			if(model.getGstinNumber()!=null)
				inventity.setGstinNumber(model.getGstinNumber());//Ashwini Patil Date:21-02-2024 Ultra pest reported this bug
		inventity.setTotalAmtExcludingTax(model.getTotalAmount());
		inventity.setDiscountAmt(model.getDiscountAmt());
		inventity.setFinalTotalAmt(model.getFinalTotalAmt());
		inventity.setTotalAmtIncludingTax(model.getTotalAmtIncludingTax());
		inventity.setTotalBillingAmount(model.getTotalBillingAmount());
//		if(form.getTbroundOffAmt().getValue()!=null&&!form.getTbroundOffAmt().getValue().equals("")){
			inventity.setDiscount(model.getRoundOffAmt());
//		}
		
		/**
		 * Date : 06-11-2017 BY ANIL
		 * setting billing comment to invoice
		 * for NBHC
		 */
//		if(form.getTacomment().getValue()!=null){
			inventity.setComment(model.getComment());
//		}
		
		if(confiFlag){
			
			/**
			* @author Vijay Date :- 19-11-2020
			* Des :- bug - category type and group not mapped in invoice so updated the code
			*/
			inventity.setInvoiceCategory(model.getBillingCategory());
			inventity.setInvoiceConfigType(model.getBillingType());
			inventity.setInvoiceGroup(model.getBillingGroup());
		}
		
		inventity.setConsolidatePrice(model.isConsolidatePrice());
		
		inventity.setRenewContractFlag(model.isRenewContractFlag());
		
		if(model.getSubBillType()!= null && !model.getSubBillType().equals("")){
			inventity.setSubBillType(model.getSubBillType());
		}
		
		/** date 15.10.2018 added by komal for subbilltye **/
		if(model.getSubBillType()!= null && !model.getSubBillType().equals("")){
			inventity.setSubBillType(model.getSubBillType());
			if(model.getSubBillType().equalsIgnoreCase(AppConstants.ARREARSBILL)){
				inventity.setCncBillAnnexureId(model.getCncBillAnnexureId());
			}
		}
		
		if(model.getServiceId()!=null){
			inventity.setServiceId(model.getServiceId());
		}
		
		if(model.getCancleAmtRemark()!=null && !model.getCancleAmtRemark().equals("")){
			inventity.setCancleAmtRemark(model.getCancleAmtRemark());
		}
		
		/** Date 11-08-2020 by Vijay storing CNC Project Name and Contract Number in invoice ***/
		if(model.getProjectName()!=null){
			inventity.setProjectName(model.getProjectName());
		}
		/** Date 14-08-2020 by vijay mapping CNC contract number value in invoice details ***/
		if(model.getCncContractNumber()!=null){
			inventity.setContractNumber(model.getCncContractNumber());
		}
		/**
		 * @author Vijay Chougule
		 * @Since Date 18-08-2020 
		 * Des :- when below process config is Active then it will calculate the Total Man Power and Total Man Days
		 */
		if(ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("BillingDocument", "AddManpowercolumn", inventity.getCompanyId())){
			double totalManPower = 0;
			double totalManDays=0;
			double labourManPower = 0;
			double overtimeManPower=0;
			double holidayManPower = 0;
			
			for(SalesOrderProductLineItem product : inventity.getSalesOrderProducts()){
				System.out.println("product.getBillType()"+product.getBillType());
				logger.log(Level.SEVERE, "product.getBillType() "+product.getBillType());
				if(product.getBillType()!=null && !product.getBillType().equals("") && ( product.getBillType().equals("Labour") || product.getBillType().equals("Overtime") || product.getBillType().equals("Holiday")) ){
					if(product.getManpower()!=null && !product.getManpower().equals("")){
						if(product.getBillType().equals("Labour")){
							labourManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Overtime")){
							overtimeManPower += Double.parseDouble(product.getManpower());
						}
						else if(product.getBillType().equals("Holiday")){
							holidayManPower += Double.parseDouble(product.getManpower());
						}
					}
					if(product.getArea()!=null && !product.getArea().equals("")&& !product.getArea().equalsIgnoreCase("NA")){
						totalManDays +=Double.parseDouble(product.getArea());
						System.out.println("totalManDays "+totalManDays);
					}
				}
				
			}
			totalManPower = getmaxManPower(labourManPower,overtimeManPower,holidayManPower);
			inventity.setTotalManPower(totalManPower);
			inventity.setTotalManDays(totalManDays);
		}
		/**
		 * ends here
		 */
		
		/*** Date 12-08-2020 by Vijay for Sasha Credit Period storing in billing and invoice to calculate payment Date ***/
		inventity.setCreditPeriod(model.getCreditPeriod());
		if(model.getCreditPeriod()!=0 && ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Invoice", "AddCreditPeriodAndDispatchDate", model.getCompanyId())){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			TimeZone.setDefault(TimeZone.getTimeZone("IST"));
			dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
			
			Calendar cal=Calendar.getInstance();
			cal.setTime(inventity.getInvoiceDate());
			cal.add(Calendar.DATE, inventity.getCreditPeriod());
			
			Date paymentDate=null;
			
			try {
				paymentDate=dateFormat.parse(dateFormat.format(cal.getTime()));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			logger.log(Level.SEVERE,"paymentDate "+paymentDate);
			inventity.setPaymentDate(paymentDate);
		
		}
		/**
		 * ends here
		 */
		
		/*** Date 26-09-2019 by Vijay for NBHC CCPM AutoInvoice invoice details from popup ****/
		if(autoinvoiceflag){	
			inventity.setInvoiceCategory(invoiceCategory);
			inventity.setInvoiceConfigType(invoiceType2);
			inventity.setInvoiceGroup(invoiceGruop);
			inventity.setApproverName(approverName);
			logger.log(Level.SEVERE, "Auto Invoice for NBHC CCPM");
			
		}
		/** Date 09-10-2020 by Vijay set value of created by requirement raised by Rahul Tiwari ***/
		if(userName!=null){
			inventity.setCreatedBy(userName);
		}
		
		/**
		 * @author Anil @since 05-04-2021
		 * For Alkosh raised by Rahul Tiwari
		 */
		inventity.setManagementFeesPercent(model.getManagementFeesPercent());
		
		
		
		/**
		 * @author Anil
		 * @since 02-02-2022
		 * for service wise billing calculating billing period depending on the service date
		 * earlier we are modifying the billing duration on invoice pdf only but it required to store in
		 * billing period of invoice so control goes to user 
		 * Raised by Nitin Sir and Jayesh 
		 */
		try{
			if(inventity!=null&&inventity.getRateContractServiceId()!=0){
				List<Integer> billingId=new ArrayList<Integer>();
				for(BillingDocumentDetails obj:inventity.getArrayBillingDocument()){
					billingId.add(obj.getBillId());
				}
				
				if(billingId.size()!=0){
					List<BillingDocument> billingList=ofy().load().type(BillingDocument.class).filter("companyId", inventity.getCompanyId()).filter("count IN",billingId).list();
					if(billingList!=null&&billingList.size()!=0){
						List<Integer> serviceId=new ArrayList<Integer>();
						for(BillingDocument obj:billingList){
							serviceId.add(obj.getRateContractServiceId());
						}
						if(serviceId.size()!=0){
							List<Service> serviceList=ofy().load().type(Service.class).filter("companyId", inventity.getCompanyId()).filter("count IN",serviceId).list();
					
							if(serviceList!=null&&serviceList.size()!=0){
								
								Comparator<Service> comp=new Comparator<Service>() {
									@Override
									public int compare(Service o1, Service o2) {
										return o1.getServiceDate().compareTo(o2.getServiceDate());
									}
								};
								Collections.sort(serviceList, comp);
								Date fromDt=null;
								Date toDt=null;
								if(serviceList.size()==1){
									fromDt=serviceList.get(0).getServiceDate();
									toDt=serviceList.get(0).getServiceDate();
								}else{
									fromDt=serviceList.get(0).getServiceDate();
									toDt=serviceList.get(serviceList.size()-1).getServiceDate();
									
									fromDt=DateUtility.getStartDateofMonth(fromDt);
									toDt=DateUtility.getEndDateofMonth(toDt);
								}
								
								inventity.setBillingPeroidFromDate(fromDt);
								inventity.setBillingPeroidToDate(toDt);
								
								logger.log(Level.SEVERE,"Billing Duration "+fromDt+"    -    "+toDt);
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.log(Level.SEVERE,"Exception in calculating billing duration from services for service wise bill!!");
		}
		
		/**
		 * @author Vijay 28-03-2022 for do not print service address flag updating to invoice
		 */
		inventity.setDonotprintServiceAddress(model.isDonotprintServiceAddress());
		
		/**Sheetal:30-03-2022 for payment mode to updating to invoice**/
		if(model.getPaymentMode()!=null && !model.getPaymentMode().equals("")) {
		  inventity.setPaymentMode(model.getPaymentMode());
		}
		
		/**
		 * @author Vijay Date :- 25-01-2022
		 * Des :- to manage invoice pdf print on invoice
		 */
		if(model.getInvoicePdfNameToPrint()!=null){
			inventity.setInvoicePdfNameToPrint(model.getInvoicePdfNameToPrint());
		}
		/**
		 * ends here
		 */
		
		GenricServiceImpl impl=new GenricServiceImpl();
		ReturnFromServer object=impl.save(inventity);
		logger.log(Level.SEVERE," Invoice Id : "+object.count);
		
		/*** Date 26-09-2019 by Vijay for NBHC CCPM AutoInvoice invoice sending Approval request  ****/
		if(autoinvoiceflag){
			logger.log(Level.SEVERE, "Auto Invoice sending approval request to first level");
			Approvals approval = new Approvals();
			approval.setApproverName(inventity.getApproverName());
			approval.setBranchname(inventity.getBranch());
			approval.setBusinessprocessId(object.count);
			if(userName!=null){
			approval.setRequestedBy(userName);
			}
			approval.setPersonResponsible(inventity.getEmployee());
			if(inventity.getCreatedBy()!=null){
				approval.setDocumentCreatedBy(inventity.getCreatedBy());
			}
			approval.setApprovalLevel(1);
			approval.setStatus(Approvals.PENDING);
			approval.setBusinessprocesstype(ApproverFactory.INVOICEDETAILS);
			approval.setBpId(inventity.getPersonInfo().getCount()+"");
			approval.setBpName(inventity.getPersonInfo().getFullName());
			approval.setDocumentValidation(false);
			approval.setCompanyId(model.getCompanyId());
			GenricServiceImpl implnew=new GenricServiceImpl();
			implnew.save(approval);
			
			logger.log(Level.SEVERE, "Auto Invoice Approval Sent successfully");

		}

		ArrayList<Integer> billIdList=new ArrayList<Integer>();
		ArrayList<Integer> billOrderIdList=new ArrayList<Integer>();
		for(int i=0;i<model.getArrayBillingDocument().size();i++){
			billIdList.add(model.getArrayBillingDocument().get(i).getBillId());
			billOrderIdList.add(model.getArrayBillingDocument().get(i).getOrderId());
		}
		
		List<BillingDocument>billingList=ofy().load().type(BillingDocument.class).filter("companyId", model.getCompanyId())
				.filter("contractCount IN", billOrderIdList).filter("count IN", billIdList).filter("typeOfOrder", model.getTypeOfOrder()).list();
		if(billingList!=null&&billIdList.size()!=0){
			logger.log(Level.SEVERE,"Billing List Size : "+billingList.size());
			for(BillingDocument bill:billingList){
//				for(Integer orderId:billOrderIdList){
//					if(bill.getContractCount()==orderId){
						bill.setStatus(BillingDocument.BILLINGINVOICED);
						bill.setInvoiceCount(object.count);
//					}
//				}
			}
			ofy().save().entities(billingList).now();
		}
		
		return object.count+"-";
			
	}
	
	private double getmaxManPower(double labourManPower,double overtimeManPower, double holidayManPower) {
		logger.log(Level.SEVERE, "labourManPower"+labourManPower);
		logger.log(Level.SEVERE, "overtimeManPower"+overtimeManPower);
		logger.log(Level.SEVERE, "holidayManPower"+holidayManPower);

		double maxManPower = 0;
		if(labourManPower>=overtimeManPower && labourManPower>=holidayManPower){
				maxManPower = labourManPower;
		}
		else if(overtimeManPower>=holidayManPower && overtimeManPower>=labourManPower){
				maxManPower = overtimeManPower;
		}
		else{
			maxManPower = holidayManPower;
		}
		
		return maxManPower;
	}

	public boolean isMultipleContractBilling(List<BillingDocumentDetails> billtablelis){
		int contractId=billtablelis.get(0).getOrderId();
		for(int i=0;i<billtablelis.size();i++){
			if(billtablelis.get(i).getOrderId()!=contractId){
				return true;
			}
		}
		return false;
	}

	@Override
	public String createPayment(Invoice model) {

		try {
		GenricServiceImpl impl=new GenricServiceImpl();
		CustomerPayment paydetails=new CustomerPayment();
		paydetails.setPersonInfo(model.getPersonInfo());
		paydetails.setContractCount(model.getContractCount());
		if(model.getContractStartDate()!=null){
			paydetails.setContractStartDate(model.getContractStartDate());
		}
		if(model.getContractEndDate()!=null){
			paydetails.setContractEndDate(model.getContractEndDate());
		}
		paydetails.setTotalSalesAmount(model.getTotalSalesAmount());
		paydetails.setArrayBillingDocument(model.getArrayBillingDocument());
		
		
		paydetails.setTotalBillingAmount(model.getInvoiceAmount());
	
		paydetails.setInvoiceAmount(model.getInvoiceAmount());
		
		
		paydetails.setInvoiceCount(model.getCount());
		paydetails.setInvoiceDate(model.getInvoiceDate());
		paydetails.setInvoiceType(model.getInvoiceType());
		paydetails.setPaymentDate(model.getPaymentDate());
		double invoiceamt=model.getInvoiceAmount();
		int payAmt=(int)(invoiceamt);
		paydetails.setPaymentAmt(payAmt);
		paydetails.setPaymentAmtInDecimal(invoiceamt);//Ashwini Patil
		paydetails.setPaymentMethod(model.getPaymentMethod());
		paydetails.setTypeOfOrder(model.getTypeOfOrder());
		paydetails.setStatus(CustomerPayment.CREATED);
		paydetails.setOrderCreationDate(model.getOrderCreationDate());
		paydetails.setBranch(model.getBranch());
		paydetails.setAccountType(model.getAccountType());
		paydetails.setEmployee(model.getEmployee());
		paydetails.setCompanyId(model.getCompanyId());
		paydetails.setOrderCformStatus(model.getOrderCformStatus());
		paydetails.setOrderCformPercent(model.getOrderCformPercent());
		
		
		if(model.getPaymentMethod()!= null && !model.getPaymentMethod().equals(""))
		{
			if(model.getPaymentMethod().equalsIgnoreCase("Cheque"))
			{
				paydetails.setChequeIssuedBy(model.getPersonInfo().getFullName());
			}
			else
			{
				paydetails.setChequeIssuedBy("");
			}
		}
		else
		{
			paydetails.setChequeIssuedBy("");
		}
			if(model.getNumberRange()!=null)
		paydetails.setNumberRange(model.getNumberRange());
		
		if(model.getBillingPeroidFromDate()!=null)
			paydetails.setBillingPeroidFromDate(model.getBillingPeroidFromDate());
		if(model.getBillingPeroidToDate()!=null)
			paydetails.setBillingPeroidToDate(model.getBillingPeroidToDate());
		
		if(model.getSegment()!=null){
			paydetails.setSegment(model.getSegment());
		}
		
		if(model.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(model.getRateContractServiceId());
		}
		
		if(model.getRateContractServiceId()!=0){
			paydetails.setRateContractServiceId(model.getRateContractServiceId());
		}
	
		if( model.getQuantity() > 0){
			paydetails.setQuantity(model.getQuantity());
		}
		
		if( model.getUom() !=null){
			paydetails.setUom(model.getUom());
		}
	
		if( model.getRefNumber() !=null){
			paydetails.setRefNumber(model.getRefNumber());
		}
		
		if( model.getInvRefNumber() !=null){
			paydetails.setInvRefNumber(model.getInvRefNumber());
		}
		
		double taxAmount = 0;
		taxAmount = Math.round(model.getTotalAmtIncludingTax() - (model.getFinalTotalAmt() + model.getTotalOtherCharges()));
		paydetails.setBalanceTax(taxAmount);
		paydetails.setBalanceRevenue(model.getNetPayable() - taxAmount);
	
		paydetails.setRenewContractFlag(model.isRenewContractFlag());
		
		if(model.getSubBillType() != null && !model.getSubBillType().equals("")){
			paydetails.setSubBillType(model.getSubBillType());
		}
	
		paydetails.setTdsTaxValue(model.getTdsTaxValue());
		
		if(model.getTdsPercentage()!=null){
			paydetails.setTdsPercentage(model.getTdsPercentage()+"");
		}
		paydetails.setTdsApplicable(model.isTdsApplicable());
		
		
		if(model.getPaymentMode()!=null && !model.getPaymentMode().equals("")) {
		   paydetails.setPaymentMode(model.getPaymentMode());
		}
		
		if(model.getInvoiceConfigType()!=null){
			paydetails.setInvoiceConfigType(model.getInvoiceConfigType());
		}
		
		impl.save(paydetails);
		}catch(Exception e) {
			e.printStackTrace();
			return "Failed";
		}
    
		return "Success";
	}

	@Override
	public String validateInvoiceForZohoIntegration(Invoice invoice) {
		
		String error="";
		if(invoice!=null) {
			
			Company comp = ofy().load().type(Company.class).filter("companyId", invoice.getCompanyId()).first().now();
			Customer cust = ofy().load().type(Customer.class).filter("companyId", invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
			HashSet<Integer> prodIdSet=new HashSet<Integer>();
			if(invoice.getSalesOrderProducts()!=null) {
				for(SalesOrderProductLineItem item:invoice.getSalesOrderProducts()) {
					prodIdSet.add(item.getProdId());
				}
			}
			List<SuperProduct> prodlist=ofy().load().type(SuperProduct.class).filter("companyId", invoice.getCompanyId()).filter("count IN", prodIdSet).list();
			
			if(comp!=null) {
				if(comp.getZohoOrganisationID()==null||comp.getZohoOrganisationID().equals(""))
					error+="Zoho organisation id is missing in Company information. Go to Settings -> Company -> Zoho Integration tab -> enter Organization id.\n";
				if(comp.getZohoPermissionsCode()==null||comp.getZohoPermissionsCode().equals(""))
					error+="Zoho refresh token is missing in Company information.  Go to Settings -> Company -> Zoho Integration tab -> enter refresh token.\n";
				if(comp.getZohoClientID()==null||comp.getZohoClientID().equals(""))
					error+="Zoho Client ID is missing in Company information.  Go to Settings -> Company -> Zoho Integration tab -> enter Client ID.\n";
				if(comp.getZohoClientSecret()==null||comp.getZohoClientSecret().equals(""))
					error+="Zoho Client Secret is missing in Company information.  Go to Settings -> Company -> Zoho Integration tab -> enter Client Secret.\n";
			
			}else
				error+="Unable to load company.";
			
			if(cust !=null) {
				if(cust.getZohoCustomerId()==0)
					error+="Zoho customer id is missing in customer.  Go to Service -> Customer -> search and open customer details -> Reference/General tab ->add zoho customer id.\n";
			}else
				error+="Unable to load customer.";
			
			if(prodlist!=null) {
				for(SuperProduct prod:prodlist) {
					if(prod.getZohoItemID()==0)
						error+="Zoho item id is missing in Service Product "+prod.getProductName()+ ". Go to Product -> Service Product -> search and open product details -> Zoho Item ID.\n";
				}
			}else
				error+="Unable to load products.";
			
		}else
			return "Failed";
		
		if(error.equals(""))
			return "Success";
		else
			return error;
	}

	@Override
	public String createInvoiceInZohoBooks(int invoiceId,long companyId,String loggedInUser) {
		logger.log(Level.SEVERE,"in createInvoiceInZohoBooks request for invoiceId="+invoiceId);
		String result="success";
		Invoice invoice = ofy().load().type(Invoice.class).filter("companyId",companyId).filter("count", invoiceId).first().now();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		DecimalFormat df = new DecimalFormat("0.00");
		if(invoice!=null) {
			
			Contract con=ofy().load().type(Contract.class).filter("companyId",companyId).filter("count", invoice.getContractCount()).first().now();
			
			
			if(invoice.getZohoInvoiceUniqueID()!=0&&invoice.getZohoInvoiceID()!=null&&!invoice.getZohoInvoiceID().equals("")) {
				String updationresult=updateInvoiceInZohoBooks(invoice.getCount(), invoice.getCompanyId(), loggedInUser);
				return updationresult;
			}
			Company comp = ofy().load().type(Company.class).filter("companyId", invoice.getCompanyId()).first().now();
			Customer cust = ofy().load().type(Customer.class).filter("companyId", invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
			HashSet<Integer> prodIdSet=new HashSet<Integer>();
			HashMap<Integer, Long> prodZohoIdMap=new HashMap<Integer, Long>();
			if(invoice.getSalesOrderProducts()!=null) {
				for(SalesOrderProductLineItem item:invoice.getSalesOrderProducts()) {
					prodIdSet.add(item.getProdId());
				}
			}
			List<SuperProduct> prodlist=ofy().load().type(SuperProduct.class).filter("companyId", invoice.getCompanyId()).filter("count IN", prodIdSet).list();
			for(SuperProduct prod:prodlist) {
				prodZohoIdMap.put(prod.getCount(), prod.getZohoItemID());
			}
			
			
			String orgId=comp.getZohoOrganisationID();
			String refreshToken=comp.getZohoPermissionsCode();
			String accessToken="";
			String clientid=comp.getZohoClientID();
			String clientSecret=comp.getZohoClientSecret();
			
			JSONObject mainObj=new JSONObject();
			mainObj.put("customer_id", cust.getZohoCustomerId());
			mainObj.put("date", sdf.format(invoice.getInvoiceDate()));
			mainObj.put("due_date", sdf.format(invoice.getPaymentDate()));
			mainObj.put("discount", invoice.getDiscountAmt());
			mainObj.put("is_discount_before_tax", true);
			mainObj.put("discount_type", "item_level");
			mainObj.put("is_inclusive_tax", "false");
			mainObj.put("exchange_rate", "1");
			mainObj.put("payment_terms_label", "Due on Receipt");
			if(invoice.getEmployee()!=null&&!invoice.getEmployee().equals(""))
				mainObj.put("salesperson_name", invoice.getEmployee());//Ashwini Patil Date:12-08-2024
			//"payment_terms_label": "Due on Receipt",
			
			int productCount=invoice.getSalesOrderProducts().size();
			JSONArray itemListArray=new JSONArray();
			
			for(int i=0;i<productCount;i++) {
				SalesOrderProductLineItem item= invoice.getSalesOrderProducts().get(i);
				JSONObject itemObj=new JSONObject();
				itemObj.put("item_id", prodZohoIdMap.get(item.getProdId()));
				if(item.getHsnCode()!=null)
					itemObj.put("hsn_or_sac", item.getHsnCode());
				else
					itemObj.put("hsn_or_sac", "");
				itemObj.put("name", item.getProdName());
				if(invoice.getComment()!=null)
					itemObj.put("description",invoice.getComment());
				else
					itemObj.put("description","");
				itemObj.put("item_order", i);
				itemObj.put("bcy_rate", df.format(item.getPrice()));
				itemObj.put("rate", df.format(item.getPrice()));
				if(!item.getArea().equals("NA") )
					itemObj.put("quantity",Integer.parseInt(item.getArea()));
				else
					itemObj.put("quantity",1);	
				if(item.getUnitOfMeasurement()!=null)
					itemObj.put("unit",item.getUnitOfMeasurement());
				else
					itemObj.put("unit","");
				if(item.getFlatDiscount()!=0)
					itemObj.put("discount",item.getFlatDiscount());
				else
					itemObj.put("discount",0);
				itemObj.put("discount_amount",0);
				itemListArray.put(itemObj);
			}
			mainObj.put("line_items", itemListArray);
			
			JSONArray custom_fields=new JSONArray();
			JSONObject customObj=new JSONObject();
			customObj.put("api_name", "cf_contract_date");
			if(con!=null)
				customObj.put("value",sdf.format(con.getContractDate()));
			else
				customObj.put("value","");
			custom_fields.put(customObj);
			
			customObj=new JSONObject();
			customObj.put("api_name", "cf_po_number");
			if(con!=null&&con.getPoNumber()!=null)
				customObj.put("value",con.getPoNumber());
			else
				customObj.put("value","");
			custom_fields.put(customObj);
			
			customObj=new JSONObject();
			customObj.put("api_name", "cf_po_date");
			if(con!=null&&con.getPoDate()!=null)
				customObj.put("value",sdf.format(con.getPoDate()));
			else
				customObj.put("value","");
			custom_fields.put(customObj);
			
			customObj=new JSONObject();
			customObj.put("api_name", "cf_billing_from");
			if(invoice.getBillingPeroidFromDate()!=null)
				customObj.put("value",sdf.format(invoice.getBillingPeroidFromDate()));
			else
				customObj.put("value","");
			custom_fields.put(customObj);
			
			customObj=new JSONObject();
			customObj.put("api_name", "cf_billing_to");
			if(invoice.getBillingPeroidToDate()!=null)
				customObj.put("value",sdf.format(invoice.getBillingPeroidToDate()));
			else
				customObj.put("value","");
			custom_fields.put(customObj);
			mainObj.put("custom_fields", custom_fields);
			
			String urlForAccessToken="https://accounts.zoho.com/oauth/v2/token?"
					+ "refresh_token="+ refreshToken
					+ "&client_id="+clientid//1000.X4IH6P16MDWMLVTBXU25VOOB4G1OXT
					+ "&client_secret="+clientSecret//5a515dbc7d037d572bb8ef7503355580ade75ece65
					+ "&grant_type=refresh_token";
			
			OkHttpClient client = new OkHttpClient().newBuilder()
				      .build();
				    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
				  
				    RequestBody body = RequestBody.create(mediaType, "");
				    Request request = new Request.Builder()
				      .url(urlForAccessToken)
				      .method("POST", body)
//				      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
				      .build();
				  Response response;
					
					try {
						response = client.newCall(request).execute();
						String res=response.body().string();
						logger.log(Level.SEVERE,"1st Response: "+res);
						org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
						if(res.contains("access_token")) {
							accessToken=resObj.getString("access_token");
							String createInvoiceUrl="https://www.zohoapis.com/books/v3/invoices?organization_id="+orgId;
							logger.log(Level.SEVERE,"createInvoiceUrl= "+createInvoiceUrl);
							String jsonString=mainObj.toJSONString();
						    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
						   
							RequestBody body2 = RequestBody.create(mediaType, jsonString);
							Request request2 = new Request.Builder()
								      .url(createInvoiceUrl)
								      .method("POST", body2)
								      .addHeader("Authorization", "Zoho-oauthtoken "+accessToken)
								      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
								      .build();
								  Response response2;
									
									try {
										response2 = client.newCall(request2).execute();
										String res2=response2.body().string();
										logger.log(Level.SEVERE,"2nd Response: "+res2);
										if(res2.contains("Error"))
											return res2;
										else if(res2.contains("invoice_id")) {
											org.json.JSONObject res2Obj=new org.json.JSONObject(res2.trim());											
											org.json.JSONObject resData=res2Obj.getJSONObject("invoice");
											String zohoInvoiceUniqueId=resData.getString("invoice_id");
											String zohoInvoiceId=resData.getString("invoice_number");
											invoice.setZohoInvoiceID(zohoInvoiceId);
											invoice.setZohoInvoiceUniqueID(Long.parseLong(zohoInvoiceUniqueId));
											invoice.setZohoSyncDate(new Date());
											invoice.setZohoSyncDetails("Synced by "+loggedInUser);
											ofy().save().entity(invoice);
											logger.log(Level.SEVERE,"zoho invoice id "+zohoInvoiceUniqueId+" saved in Eva invoice. with value "+Long.parseLong(zohoInvoiceUniqueId) );
										}else
											return res2;
									}catch(Exception ex) {
										ex.printStackTrace();
										result="Failed";
									}
						
						}
						
					}catch(Exception e) {
						e.printStackTrace();
						result="Failed";
					}
			
			
			
		}
		return result;
	}

	@Override
	public String updateInvoiceInZohoBooks(int invoiceId, long companyId, String loggedInUser) {

		logger.log(Level.SEVERE,"in updateInvoiceInZohoBooks request for invoiceId="+invoiceId);
		
		String result="success";
		Invoice invoice = ofy().load().type(Invoice.class).filter("companyId",companyId).filter("count", invoiceId).first().now();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		DecimalFormat df = new DecimalFormat("0.00");
		if(invoice!=null) {
			
			Company comp = ofy().load().type(Company.class).filter("companyId", invoice.getCompanyId()).first().now();
			Customer cust = ofy().load().type(Customer.class).filter("companyId", invoice.getCompanyId()).filter("count", invoice.getPersonInfo().getCount()).first().now();
			HashSet<Integer> prodIdSet=new HashSet<Integer>();
			HashMap<Integer, Long> prodZohoIdMap=new HashMap<Integer, Long>();
			if(invoice.getSalesOrderProducts()!=null) {
				for(SalesOrderProductLineItem item:invoice.getSalesOrderProducts()) {
					prodIdSet.add(item.getProdId());
				}
			}
			List<SuperProduct> prodlist=ofy().load().type(SuperProduct.class).filter("companyId", invoice.getCompanyId()).filter("count IN", prodIdSet).list();
			for(SuperProduct prod:prodlist) {
				prodZohoIdMap.put(prod.getCount(), prod.getZohoItemID());
			}
			
			
			String orgId=comp.getZohoOrganisationID();
			String refreshToken=comp.getZohoPermissionsCode();
			String accessToken="";
			String clientid=comp.getZohoClientID();
			String clientSecret=comp.getZohoClientSecret();
			
			JSONObject mainObj=new JSONObject();
			mainObj.put("customer_id", cust.getZohoCustomerId());
			mainObj.put("date", sdf.format(invoice.getInvoiceDate()));
			mainObj.put("due_date", sdf.format(invoice.getPaymentDate()));
			mainObj.put("discount", invoice.getDiscountAmt());
			mainObj.put("is_discount_before_tax", true);
			mainObj.put("discount_type", "item_level");
			mainObj.put("is_inclusive_tax", "false");
			mainObj.put("exchange_rate", "1");
			mainObj.put("payment_terms_label", "Due on Receipt");
			mainObj.put("invoice_number",invoice.getZohoInvoiceID());
			if(invoice.getEmployee()!=null&&!invoice.getEmployee().equals(""))
				mainObj.put("salesperson_name", invoice.getEmployee()); //Ashwini Patil Date:12-08-2024
			//"payment_terms_label": "Due on Receipt",
			
			int productCount=invoice.getSalesOrderProducts().size();
			JSONArray itemListArray=new JSONArray();
			
			for(int i=0;i<productCount;i++) {
				SalesOrderProductLineItem item= invoice.getSalesOrderProducts().get(i);
				JSONObject itemObj=new JSONObject();
				itemObj.put("item_id", prodZohoIdMap.get(item.getProdId()));
				if(item.getHsnCode()!=null)
					itemObj.put("hsn_or_sac", item.getHsnCode());
				else
					itemObj.put("hsn_or_sac", "");
				itemObj.put("name", item.getProdName());
				if(invoice.getComment()!=null)
					itemObj.put("description",invoice.getComment());
				else
					itemObj.put("description","");
				itemObj.put("item_order", i);
				itemObj.put("bcy_rate", df.format(item.getPrice()));
				itemObj.put("rate", df.format(item.getPrice()));
				if(!item.getArea().equals("NA") )
					itemObj.put("quantity",Integer.parseInt(item.getArea()));
				else
					itemObj.put("quantity",1);	
				if(item.getUnitOfMeasurement()!=null)
					itemObj.put("unit",item.getUnitOfMeasurement());
				else
					itemObj.put("unit","");
				if(item.getFlatDiscount()!=0)
					itemObj.put("discount",item.getFlatDiscount());
				else
					itemObj.put("discount",0);
				itemObj.put("discount_amount",0);
				itemListArray.put(itemObj);
			}
			mainObj.put("line_items", itemListArray);
			
			
			String urlForAccessToken="https://accounts.zoho.in/oauth/v2/token?"
					+ "refresh_token="+ refreshToken
					+ "&client_id="+clientid//1000.X4IH6P16MDWMLVTBXU25VOOB4G1OXT
					+ "&client_secret="+clientSecret//5a515dbc7d037d572bb8ef7503355580ade75ece65
					+ "&grant_type=refresh_token";
			
			OkHttpClient client = new OkHttpClient().newBuilder()
				      .build();
				    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
				  
				    RequestBody body = RequestBody.create(mediaType, "");
				    Request request = new Request.Builder()
				      .url(urlForAccessToken)
				      .method("POST", body)
//				      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
				      .build();
				  Response response;
					
					try {
						response = client.newCall(request).execute();
						String res=response.body().string();
						logger.log(Level.SEVERE,"1st Response: "+res);
						org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
						if(res.contains("access_token")) {
							accessToken=resObj.getString("access_token");
							String updateInvoiceUrl="https://www.zohoapis.in/books/v3/invoices/"+invoice.getZohoInvoiceUniqueID()+"?organization_id="+orgId;
							logger.log(Level.SEVERE,"updateInvoiceUrl= "+updateInvoiceUrl);
							String jsonString=mainObj.toJSONString();
						    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
						   
							RequestBody body2 = RequestBody.create(mediaType, jsonString);
							Request request2 = new Request.Builder()
								      .url(updateInvoiceUrl)
								      .method("PUT", body2)
								      .addHeader("Authorization", "Zoho-oauthtoken "+accessToken)
								      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
								      .build();
								  Response response2;
									
									try {
										response2 = client.newCall(request2).execute();
										String res2=response2.body().string();
										logger.log(Level.SEVERE,"2nd Response: "+res2);
										if(res2.contains("Error"))
											return res2;
										else if(res2.contains("invoice_id")) {
											invoice.setZohoSyncDate(new Date());
											invoice.setZohoSyncDetails(invoice.getZohoSyncDetails()+"\n Again Synced by "+loggedInUser);
											
											ofy().save().entity(invoice);
											logger.log(Level.SEVERE,"zoho invoice updated" );
										}else
											return res2;
									}catch(Exception ex) {
										ex.printStackTrace();
										result="Failed";
									}
							
						}
						
					}catch(Exception e) {
						e.printStackTrace();
						result="Failed";
					}
			
			
			
		}
		return result;
	
	}

	@Override
	public String validateCustomerForZohoIntegration(Customer cust) {

		
		String error="";
		if(cust!=null) {
			
			Company comp = ofy().load().type(Company.class).filter("companyId", cust.getCompanyId()).first().now();
			
			if(comp!=null) {
				if(comp.getZohoOrganisationID()==null||comp.getZohoOrganisationID().equals(""))
					error+="Zoho organisation id is missing in Company information. Go to Settings -> Company -> Zoho Integration tab -> enter Organization id.\n";
				if(comp.getZohoPermissionsCode()==null||comp.getZohoPermissionsCode().equals(""))
					error+="Zoho refresh token is missing in Company information.  Go to Settings -> Company -> Zoho Integration tab -> enter refresh token.\n";
				if(comp.getZohoClientID()==null||comp.getZohoClientID().equals(""))
					error+="Zoho Client ID is missing in Company information.  Go to Settings -> Company -> Zoho Integration tab -> enter Client ID.\n";
				if(comp.getZohoClientSecret()==null||comp.getZohoClientSecret().equals(""))
					error+="Zoho Client Secret is missing in Company information.  Go to Settings -> Company -> Zoho Integration tab -> enter Client Secret.\n";
			
			}else
				error+="Unable to load company.";
			
		}else
			return "Failed";
		
		if(error.equals(""))
			return "Success";
		else
			return error;
	
	}

	@Override
	public String createCustomerInZohoBooks(int custId, long companyId) {

		
		String result="zohosuccess";
		Customer cust = ofy().load().type(Customer.class).filter("companyId",companyId).filter("count", custId).first().now();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		DecimalFormat df = new DecimalFormat("0.00");
		if(cust!=null) {			
			
			Company comp = ofy().load().type(Company.class).filter("companyId", cust.getCompanyId()).first().now();
			HashSet<Integer> prodIdSet=new HashSet<Integer>();
			HashMap<Integer, Long> prodZohoIdMap=new HashMap<Integer, Long>();
			
			String orgId=comp.getZohoOrganisationID();
			String refreshToken=comp.getZohoPermissionsCode();
			String accessToken="";
			String clientid=comp.getZohoClientID();
			String clientSecret=comp.getZohoClientSecret();
			String compName="";
			String fullname="";
			String custCell="";
			String gstIn="";
			String custType="";
			String billStateCode="";
			String shipStateCode="";
			
			if(cust.isCompany()){
				compName=cust.getCompanyName();
				custType="business";
			}else {
				compName=cust.getFullname();
				custType="individual";
			}
			fullname=	cust.getFullname();
			if(cust.getCellNumber1()!=null)
				custCell=cust.getCellNumber1()+"";
			if(cust.getArticleTypeDetails()!=null) {
				for(ArticleType at:cust.getArticleTypeDetails()) {
					if(at.getArticleTypeName().equalsIgnoreCase("GSTIN")) {
						gstIn=at.getArticleTypeValue();
						break;
					}
				}
			}
			List<State>  listOfSates = ObjectifyService.ofy().load().type(State.class)
				      .filter("companyId", cust.getCompanyId()).list();
				    
			for (int i = 0; i < listOfSates.size(); i++) {
				if (listOfSates.get(i).getStateName().trim().equalsIgnoreCase(
								cust.getAdress().getState().trim())) {
					if(listOfSates.get(i).getStateCode()!=null)
						billStateCode = listOfSates.get(i).getStateCode().trim();
					break;
				}
			}
			for (int i = 0; i < listOfSates.size(); i++) {
				if (listOfSates.get(i).getStateName().trim().equalsIgnoreCase(
								cust.getSecondaryAdress().getState().trim())) {
					if(listOfSates.get(i).getStateCode()!=null)
						shipStateCode = listOfSates.get(i).getStateCode().trim();
					break;
				}
			}
			
			if(billStateCode==null||billStateCode.equals(""))
				return "State code not found for state "+cust.getAdress().getState()+". Please go to settings -> state - edit and add state code";
			
			if(shipStateCode==null||shipStateCode.equals(""))
				return "State code not found for state "+cust.getSecondaryAdress().getState()+". Please go to settings -> state - edit and add state code";

			
			JSONObject mainObj=new JSONObject();
			mainObj.put("contact_name", compName);
			mainObj.put("company_name", compName);
			mainObj.put("language_code", "en");
			mainObj.put("contact_type", "customer");
			mainObj.put("customer_sub_type", custType);
			mainObj.put("gst_no", gstIn);
			
			JSONObject billingAddress=new JSONObject();
			billingAddress.put("attention", fullname);
			if(cust.getAdress().getAddrLine1()!=null)
				billingAddress.put("address", cust.getAdress().getAddrLine1());
			if(cust.getAdress().getAddrLine2()!=null)
				billingAddress.put("street2", cust.getAdress().getAddrLine2());
			billingAddress.put("state_code", billStateCode);
			if(cust.getAdress().getCity()!=null)
				billingAddress.put("city", cust.getAdress().getCity());
			if(cust.getAdress().getState()!=null)
				billingAddress.put("state", cust.getAdress().getState());
			if(cust.getAdress().getCountry()!=null)
				billingAddress.put("country", cust.getAdress().getCountry());
			billingAddress.put("phone", custCell);
			
			mainObj.put("billing_address", billingAddress);
			
			JSONObject serviceAddress=new JSONObject();
			serviceAddress.put("attention", fullname);
			if(cust.getSecondaryAdress().getAddrLine1()!=null)
				serviceAddress.put("address", cust.getSecondaryAdress().getAddrLine1());
			if(cust.getSecondaryAdress().getAddrLine2()!=null)
				serviceAddress.put("street2", cust.getSecondaryAdress().getAddrLine2());
			serviceAddress.put("state_code", shipStateCode);
			if(cust.getSecondaryAdress().getCity()!=null)
				serviceAddress.put("city", cust.getSecondaryAdress().getCity());
			if(cust.getSecondaryAdress().getState()!=null)
				serviceAddress.put("state", cust.getSecondaryAdress().getState());
			if(cust.getSecondaryAdress().getCountry()!=null)
				serviceAddress.put("country", cust.getSecondaryAdress().getCountry());
			serviceAddress.put("phone", custCell);
			mainObj.put("shipping_address", serviceAddress);
			
			JSONArray contactArray=new JSONArray();
			JSONObject contactObj=new JSONObject();
			if(cust.getEmail()!=null&&!cust.getEmail().equals(""))
				contactObj.put("email", cust.getEmail());
			contactObj.put("mobile", custCell);
			contactObj.put("last_name", fullname);
			contactArray.put(contactObj);
			mainObj.put("contact_persons", contactArray);
			
			
					
			
			
			String urlForAccessToken="https://accounts.zoho.in/oauth/v2/token?"
					+ "refresh_token="+ refreshToken
					+ "&client_id="+clientid//1000.X4IH6P16MDWMLVTBXU25VOOB4G1OXT
					+ "&client_secret="+clientSecret//5a515dbc7d037d572bb8ef7503355580ade75ece65
					+ "&grant_type=refresh_token";
			
			OkHttpClient client = new OkHttpClient().newBuilder()
				      .build();
				    MediaType mediaType = MediaType.parse("application/json;charset=utf-8");
				  
				    RequestBody body = RequestBody.create(mediaType, "");
				    Request request = new Request.Builder()
				      .url(urlForAccessToken)
				      .method("POST", body)
//				      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
				      .build();
				  Response response;
					
					try {
						response = client.newCall(request).execute();
						String res=response.body().string();
						logger.log(Level.SEVERE,"1st Response: "+res);
						org.json.JSONObject resObj=new org.json.JSONObject(res.trim());
						if(res.contains("access_token")) {
							accessToken=resObj.getString("access_token");
							String createCustomerUrl="https://www.zohoapis.in/books/v3/contacts?organization_id="+orgId;
							logger.log(Level.SEVERE,"createInvoiceUrl= "+createCustomerUrl);
							String jsonString=mainObj.toJSONString();
						    logger.log(Level.SEVERE,"jsonString ::"+jsonString);
						   
							RequestBody body2 = RequestBody.create(mediaType, jsonString);
							Request request2 = new Request.Builder()
								      .url(createCustomerUrl)
								      .method("POST", body2)
								      .addHeader("Authorization", "Zoho-oauthtoken "+accessToken)
								      .addHeader("CONTENT-TYPE", "application/json;charset=utf-8")
								      .build();
								  Response response2;
									
									try {
										response2 = client.newCall(request2).execute();
										String res2=response2.body().string();
										logger.log(Level.SEVERE,"2nd Response: "+res2);
										if(res2.contains("Error"))
											return res2;
										else if(res2.contains("contact")) {
											org.json.JSONObject res2Obj=new org.json.JSONObject(res2.trim());											
											org.json.JSONObject resData=res2Obj.getJSONObject("contact");
											String zohoCustUniqueId=resData.getString("contact_id");
											cust.setZohoCustomerId(Long.parseLong(zohoCustUniqueId));
											ofy().save().entity(cust);
											logger.log(Level.SEVERE,"zoho cust id "+zohoCustUniqueId+" saved in Eva invoice. with value "+Long.parseLong(zohoCustUniqueId) );
										}else if(res2.contains("message")) {
											org.json.JSONObject res2Obj=new org.json.JSONObject(res2.trim());											
											String msg=res2Obj.getString("message");
											return msg;
										}else
											return res2;
									}catch(Exception ex) {
										ex.printStackTrace();
										result="Failed";
									}
							
						}
						
					}catch(Exception e) {
						e.printStackTrace();
						result="Failed";
					}
			
			
			
		}
		return result;
	
	}

	
	/*
	 * Ashwini Patil 
	 * Date:06-06-2024
	 * Ultima search has uploaded contracts using customer with service details 
	 * but for same company name they put different poc names and due to which, system created multipla customers
	 * now they want to keep one customer and discard duplicate customer
	 * This program will change customer id in all documents such as contract, bill, invoice,payment and service	 
	 */	
	@Override
	public String UpdateCustomerIdInAllDocs(Customer model, String custIdToBeSet, String loggedInUser) {
		logger.log(Level.SEVERE,loggedInUser+"user requested to update documents of customer id "+model.getCount()+" to new customer id "+custIdToBeSet);
		int newCustId=0;
		try {
			newCustId=Integer.parseInt(custIdToBeSet);
			Customer newcust = ofy().load().type(Customer.class).filter("companyId", model.getCompanyId()).filter("count", newCustId).first().now();
			if(newcust!=null) {
//				if(model.isCompany()&&newcust.isCompany()) {
//					if(!model.getCompanyName().equals(newcust.getCompanyName()))
//						return "Found different Company name in customer id "+ custIdToBeSet+". Cannot update customer id in all documents.";
//				}else if(!model.isCompany()&&!newcust.isCompany()) {
//					if(!model.getFullname().equals(newcust.getFullname()))
//						return "Found different customer name in customer id "+ custIdToBeSet+". Cannot update customer id in all documents.";
//			
//				}else 
//					return "You can update customer id only if iscompany status is same in both customers and company name/full name is same in both the customers";
				
				Queue queue = QueueFactory.getQueue("DocumentQueue-queue");
				queue.add(TaskOptions.Builder.withUrl("/slick_erp/DocumentTaskQueue").param("OperationName", "UpdateCustomerIdInAllDocs")
						.param("companyId", model.getCompanyId()+"").param("oldCustId", model.getCount()+"").param("newCustId",custIdToBeSet).param("loggedInUser",loggedInUser));
				return "Updation Process has started. Check after some time.";
			}else
				return "Enter valid customer id!";
		}catch(Exception e) {
			return "Enter correct customer Id";
		}
	}

}
