package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.role.UserRole;

public class CopyPedioConfiguration  extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6267962309845600359L;

	
	Logger logger=Logger.getLogger("CopyPedioConfiguration.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST:== ");
		
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		
		String data = req.getParameter("jsonString");
		logger.log(Level.SEVERE,"DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlCalled : "+urlCalled);
		String url = urlCalled.replace("http://", "");
		url = url.replace("https://", "");
		String[] splitUrl = url.split("\\.");
		logger.log(Level.SEVERE,"SplitURL : "+splitUrl[0]);
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One"+screenData);
		long companyId=0l;
		if(comp!=null){
			companyId= comp.getCompanyId();
			Gson gson = new Gson();
			org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();

			// Pedio Number Geeration
			try {
				ArrayList<String> processNamelist = new ArrayList<String>();
				processNamelist.add("RegisterDevice");
				processNamelist.add("CustomerBranchServiceLocation");
				processNamelist.add("AssesmentReport");
				processNamelist.add("EmployeeTrackingDetails");
				processNamelist.add("CustomerBranchDetails");
				processNamelist.add("AppRegistrationHistory");


				List<NumberGeneration> numberGenerationList=ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
								.filter("processName IN", processNamelist).filter("status", true).list();
				if(numberGenerationList!=null&&numberGenerationList.size()!=0) {
					logger.log(Level.SEVERE, "Number Generation SIZE : "+numberGenerationList.size());
					String numberGen = gson.toJson(numberGenerationList);
					
					JSONArray jarray = null;
					try {
						jarray = new JSONArray(numberGen);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					jObj.put("numberGeneration", jarray);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// Pedio User Role
			try {
				
				ArrayList<String> processNamelist = new ArrayList<String>();
				processNamelist.add("Operator");
				processNamelist.add("Auditor");
				processNamelist.add("App Register");
				List<UserRole> userrolelist = ofy().load().type(UserRole.class).filter("companyId", companyId)
								.filter("roleName IN", processNamelist).filter("roleStatus", true).list();
				if(userrolelist!=null&&userrolelist.size()!=0) {
					logger.log(Level.SEVERE, "User Role SIZE : "+userrolelist.size());
					
					String userrole = gson.toJson(userrolelist);
					JSONArray jarray = null;
					try {
						jarray = new JSONArray(userrole);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					jObj.put("userRole", jarray);

				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				ArrayList<String> processNamelist = new ArrayList<String>();
				processNamelist.add("EVA Pedio");
				processNamelist.add("Service");

//				List<ProcessConfiguration> processconfiglist = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId)
//								.filter("processName", "EVA Pedio").filter("configStatus", true).list();
				List<ProcessConfiguration> processconfiglist = ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId)
						.filter("processName IN",processNamelist).filter("configStatus", true).list();
	
				if(processconfiglist!=null&&processconfiglist.size()!=0) {
					logger.log(Level.SEVERE, "EVA Pedio Process Config list SIZE : "+processconfiglist.size());
					
					String processconfig = gson.toJson(processconfiglist);
					JSONArray jarray = null;
					try {
						jarray = new JSONArray(processconfig);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					jObj.put("processConfiguration", jarray);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try { 
				List<ConfigCategory> moduleNamelist = ofy().load().type(ConfigCategory.class).filter("companyId", companyId)
									.filter("internalType", 13).filter("status", true).list();
				if(moduleNamelist!=null&&moduleNamelist.size()!=0) {
					logger.log(Level.SEVERE, "moduleNamelist list SIZE : "+moduleNamelist.size());
					
					String modulename = gson.toJson(moduleNamelist);
					JSONArray jarray = null;
					try {
						jarray = new JSONArray(modulename);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					jObj.put("moduleName", jarray);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try { 
				List<Type> documentnamelist = ofy().load().type(Type.class).filter("companyId", companyId)
									.filter("internalType", 13).filter("status", true).list();
				if(documentnamelist!=null&&documentnamelist.size()!=0) {
					logger.log(Level.SEVERE, "documentnamelist list SIZE : "+documentnamelist.size());
					
					String documentname = gson.toJson(documentnamelist);
					JSONArray jarray = null;
					try {
						jarray = new JSONArray(documentname);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					jObj.put("documentName", jarray);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			
			

			String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE,"jsonString:: "+jsonString);
		
			resp.getWriter().print(jsonString);
			
		}
		else{
			resp.getWriter().print("Company not found.");
			return;
		}
	}	
}
