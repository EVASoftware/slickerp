package com.slicktechnologies.server.webservices;

public class CreateServiceServletObjectClass {

	String productCode;
	int numberOfService;
	int durationInDays;
	
	
	
	
	public CreateServiceServletObjectClass(String productCode,
			int numberOfService, int durationInDays) {
		super();
		this.productCode = productCode;
		this.numberOfService = numberOfService;
		this.durationInDays = durationInDays;
	}
	
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getNumberOfService() {
		return numberOfService;
	}
	public void setNumberOfService(int numberOfService) {
		this.numberOfService = numberOfService;
	}
	public int getDurationInDays() {
		return durationInDays;
	}
	public void setDurationInDays(int durationInDays) {
		this.durationInDays = durationInDays;
	}
}
