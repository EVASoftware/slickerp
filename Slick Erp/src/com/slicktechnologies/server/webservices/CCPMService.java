package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;
import com.slicktechnologies.shared.common.tallyaccounts.AccountingInterface;

public class CCPMService extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6076588443191885057L;
	/**
	 * 
	 */
	 public static final String APPID = "";
	 public static final long AUTHCODE = 0l;//eva
	 
	Logger logger=Logger.getLogger("CCPMService.class");
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		RegisterServiceImpl registerImpl=new RegisterServiceImpl();
		String urlCalled=req.getRequestURL().toString().trim();
		String url=urlCalled.replace("http://", "");
		String[] splitUrl=url.split("\\.");
		String accessUrl = splitUrl[0];
		String appID=splitUrl[1];
		logger.log(Level.SEVERE, "accessUrl" + accessUrl+"appID "+appID);
		String jsonString = req.getParameter("data").trim();
		logger.log(Level.SEVERE, "jsonString:::" + jsonString);
		JSONObject mainObject = null;
		String response="";
		try {
			mainObject = new JSONObject(jsonString);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response="ERROR parsing Data (Invalid JSON Format)";
		}
		if(response.trim().length()==0){
		if(mainObject.optString("actionTask").trim().equalsIgnoreCase("invoiceupdation")){
			response=updateInterfaceandDocument(mainObject,accessUrl,appID);
		}else{
			response="No such actionTask available!!";
		}
		
		}
		resp.getWriter().println(response);
				
	}

	private String updateInterfaceandDocument(JSONObject mainObject, String accessUrl, String appID) {
		// TODO Auto-generated method stub
		String response="successfull";
		
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl.trim()).first().now();
		long authCode = 0l;
		if (mainObject.optString("authCode").trim().length() > 0) {
			try {
				authCode = Long.parseLong(mainObject.optString("authCode")
						.trim());
			} catch (Exception e) {
				e.printStackTrace();
				return "ERROR in AUTHCODE";
			}
		} else {
			return "AuthCode is mandatory!!";
		}
		String url="";
		String reqAPPID="";
		if (mainObject.optString("url").trim().length() > 0) {
			url=mainObject.optString("url").trim();
			String[] splitUrl=url.split("\\.");
			reqAPPID = splitUrl[1];
			logger.log(Level.SEVERE, "reqAPPID" + reqAPPID);
		}else{
			return "url is mandatory";
		}
		if(company.getCompanyId()==authCode&&reqAPPID.trim().equals(appID.trim())){
			int eva_invoiceID;
			try{
				eva_invoiceID=Integer.parseInt(mainObject.optString("eva_InvoiceId").trim());
			}catch(Exception e){
				e.printStackTrace();
				return "ERROR in Invoice Id";
			}
			List<AccountingInterface> accountingInterfaceList=ofy().load().type(AccountingInterface.class).filter("companyId", company.getCompanyId()).filter("documentType", "Invoice").filter("documentID",eva_invoiceID).list();
			List<AccountingInterface> accInterList=new ArrayList<AccountingInterface>();
			logger.log(Level.SEVERE,"accountingInterfaceList.size:::::"+accountingInterfaceList.size());
			/*
			 * Updating Accounting Interface
			 */
			for (AccountingInterface accountingInterface : accountingInterfaceList) {
				if(mainObject.optString("status").trim().equalsIgnoreCase("Failed")){
					accountingInterface.setStatus(AccountingInterface.FAILED);
					accountingInterface.setRemark(mainObject.optString("remark").trim());
				}else if(mainObject.optString("status").trim().equalsIgnoreCase("Synched")){
					accountingInterface.setStatus(AccountingInterface.TALLYSYNCED);
					accountingInterface.setRemark(mainObject.optString("remark").trim()+" and Invoice ID is : "+mainObject.optString("sap_InvoiceId").trim());
				}
				
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
				sdf.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				try {
					accountingInterface.setDateofSynch(sdf.parse(sdf.format(new Date())));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				accInterList.add(accountingInterface);
			}
			
			/**
			 * Updating Invoice
			 */
			Invoice invoice=ofy().load().type(Invoice.class).filter("companyId", company.getCompanyId()).filter("count", eva_invoiceID).first().now();
			logger.log(Level.SEVERE,"invoice:::::"+invoice);
			
			invoice.setSapInvoiceId(mainObject.optString("sap_InvoiceId").trim());
			
			
			/*
			 * If everything is OK then save.
			 */
			ofy().save().entities(accInterList);
			ofy().save().entity(invoice);
			
			
			
		}else{
			return "Authentication Failed";
		}
		return response;
	}

}
