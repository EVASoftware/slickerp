package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.simplesoftwares.client.library.ReturnFromServer;
import com.simplesoftwares.client.library.appstructure.SuperModel;
import com.slicktechnologies.client.login.LoginPresenter;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.client.views.contract.ContractPresenter;
import com.slicktechnologies.client.views.contract.ContractService;
import com.slicktechnologies.client.views.contract.ContractServiceAsync;
import com.slicktechnologies.client.views.device.StackDetailsTable;
import com.slicktechnologies.server.ContractServiceImplementor;
import com.slicktechnologies.server.GenricServiceImpl;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.server.SingletoneNumberGeneration;
import com.slicktechnologies.server.cronjobimpl.FumigationServiceMarkSuccessfull;
import com.slicktechnologies.server.utility.DateUtility;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.BranchWiseScheduling;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.FumigationServiceReport;
import com.slicktechnologies.shared.ProductOtherCharges;
import com.slicktechnologies.shared.SalesLineItem;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.StackMaster;
import com.slicktechnologies.shared.common.ModificationHistory;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.customerbranch.CustomerBranchDetails;
import com.slicktechnologies.shared.common.helperlayer.Address;
import com.slicktechnologies.shared.common.helperlayer.PersonInfo;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.paymentlayer.PaymentTerms;
import com.slicktechnologies.shared.common.productgroup.ServiceProductGroupList;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.productlayer.Tax;
import com.slicktechnologies.shared.common.salesprocess.BillingDocument;
import com.slicktechnologies.shared.common.salesprocess.SalesOrderProductLineItem;
import com.slicktechnologies.shared.common.salesprocess.TaxesAndCharges.ContractCharges;
import com.slicktechnologies.shared.common.service.StackDetails;
import com.slicktechnologies.shared.common.servicerelated.ServiceSchedule;

public class CreateServiceServlet extends HttpServlet {

	private static final long serialVersionUID = -6439239861478477088L;

	 public static final String APPID = "nbhcerp";  // Production

//	public static final String APPID = "evadev0010";  // UAT

	 public static final long AUTHCODE = Long.parseLong("5890065536385024");// Server
//	 public static final long AUTHCODE = Long.parseLong("5348024557502464");//

	 
	 public static final String customerId = "100007885"; // Server
//	 String customerId = "100000006"; // local

	/**
	 * Date 09-11-2019 by Vijay
	 * Des :- these global variable not required so commented
	 *  code updated
	 */
	// nbhcerp
//	public static final long AUTHCODE = Long.parseLong("5348024557502464");//
	// local
//	String servicingBranch;
//	String url;
//	String contractRefId;
//	String customerId = "100000006"; // hardcoded local
//	String sendResponse = null;
//	String productCode = null;
	// String contractDate;
	// String serviceTime;
	// String customerBranch;
	// String serviceEngineer;
	// String wareHouse;
	// String storageLocation;
	// String storageBin;
//	long companyId;
	// JSONArray prooductDetailsArray;
//	int contractPeriod = 0;
	// List<CreateServiceServletObjectClass> listOfProductDeatails;
	// JSONArray wareHouseDetailsJArray;
//	HashMap<String, String> serviceAddressMap = new HashMap<String, String>();
	// RegisterServiceImpl register;
//	private String sendRespone;
	 /**
	  * ends here
	  */
	 Logger logger = Logger.getLogger("CreateServiceServlet.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		doPost(req, resp);
	}

	/**
	 * Date 30-07-2019 by Vijay for NBHC CCPM Des :-
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String jsonString = req.getParameter("serviceDetails").trim();
		logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);
		JSONObject jObj = null;
		try {
			jObj = new JSONObject(jsonString);
		} catch (Exception e) {
			e.printStackTrace();
		}
		long companyId = Long.parseLong(jObj.optString("authCode").trim());
		String actionTask = jObj.optString("Actiontask").trim();
		if (actionTask.trim().equals("Inward")) {
			CreateContractAndUpdateStack(req, resp,companyId);
		} 
//		else if (actionTask.trim().equals("Outward")) {
//			updateStackStockAndServices(req, resp,companyId);
//		}
		/**
		 * @author Vijay Chougule
		 * Des :- added Reschedule/Suspend/Success/Failure Operation from CIO app To EVA ERP
		 */
		else if (actionTask.trim().equals("Reschedule")) {
			reactonReschedule(req, resp, jObj, companyId);
		} else if (actionTask.trim().equals("Suspend")) {
			reactonSuspend(req, resp, jObj, companyId);
		} else if (actionTask.trim().equals("Success")) {
			reactonServiceSuccess(req, resp, jObj, companyId);
		} else if (actionTask.trim().equals("Failure")) {
			reactonServiceFailure(req, resp, jObj, companyId);
		}

	}

	private void CreateContractAndUpdateStack(HttpServletRequest req, HttpServletResponse resp, long companyId) {
		// register = new RegisterServiceImpl();
		JSONArray wareHouseDetailsJArray = new JSONArray();
		String jsonString = req.getParameter("serviceDetails").trim();
		logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);
		JSONObject jObj = null;
		try {
			jObj = new JSONObject(jsonString);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		url = jObj.optString("url").trim();
//		logger.log(Level.SEVERE, "url" + url);
		String contractRefId = jObj.optString("contractId").trim();
		logger.log(Level.SEVERE, "contractId" + contractRefId);

		logger.log(Level.SEVERE, "customerId" + customerId);
	
		String contractDate = jObj.optString("contractDate").trim(); // Its a Deposite Date
		logger.log(Level.SEVERE, "contractDate" + contractDate);
		String serviceTime = jObj.optString("serviceTime").trim();
		logger.log(Level.SEVERE, "serviceTime" + serviceTime);
		String servicingBranch = jObj.optString("servicingBranch").trim();
		logger.log(Level.SEVERE, "servicingBranch" + servicingBranch);
		String customerBranch = jObj.optString("customerBranch").trim();
		JSONArray prooductDetailsArray = new JSONArray();
		try {
			prooductDetailsArray = jObj.getJSONArray("productDetails");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		logger.log(Level.SEVERE, "customerBranch" + customerBranch);
		String serviceEngineer = jObj.optString("serviceEngineer").trim();
		logger.log(Level.SEVERE, "serviceEngineer" + serviceEngineer);
		String wareHouse = jObj.optString("wareHouse").trim();
		logger.log(Level.SEVERE, "wareHosue" + wareHouse);
		// storageLocation = jObj.optString("storageLocation").trim();
		// logger.log(Level.SEVERE, "storageLocation" + storageLocation);
		// storageBin = jObj.optString("storageBin").trim();
		// logger.log(Level.SEVERE, "storageBin" + storageBin);

		/*** Date 01-05-2019 by Vijay for commodity Name mapping ****/

		String commodityName = jObj.optString("Commodity").trim();
		String clusterName = jObj.optString("ClusterName");
		List<CreateServiceServletObjectClass> listOfProductDeatails = new ArrayList<CreateServiceServletObjectClass>();
		listOfProductDeatails = new ArrayList<CreateServiceServletObjectClass>();
		
		/**
		 * Date 09-11-2019 By Vijay
		 * Des :- As disscussed with Vaishali Mam and Nitin Sir
		 * Deggassing Service will create when they complete the Fumigation Service based on Completion Date
		 */
//		/**
//		 * Date 08-11-2019 By Vijay
//		 * Des :- Deggasing Service added by hardcoded as per Nitin Sir
//		 */
//		CreateServiceServletObjectClass deggasingService = new CreateServiceServletObjectClass("DSSG", 1, 365);
//		listOfProductDeatails.add(deggasingService);
//		/**
//		 * ends here
//		 */
		int contractPeriod = 0;
		for (int i = 0; i < prooductDetailsArray.length(); i++) {
			JSONObject jpObj;
			try {
				jpObj = prooductDetailsArray.getJSONObject(i);
				if (!jpObj.optString("durationInDays").trim().equalsIgnoreCase("na")) {
					if (contractPeriod < Integer.parseInt(jpObj.optString("durationInDays").trim())) {
						contractPeriod = Integer.parseInt(jpObj.optString("durationInDays").trim());
					}
				} else {
					contractPeriod = 365;
				}
				String productCode = jpObj.optString("productCode");
				int noOfService;
				if (!jpObj.optString("noOfService").trim().equalsIgnoreCase("NA")) {
					noOfService = Integer.parseInt(jpObj.optString("noOfService").trim());
				} else {
					contractPeriod = 365;
					noOfService = 0;
				}
				int duration;
				if (!jpObj.optString("durationInDays").trim().equalsIgnoreCase("NA")) {
					duration = Integer.parseInt(jpObj.optString("durationInDays").trim());

				} else {
					contractPeriod = 365;
					duration = 0;
				}
				CreateServiceServletObjectClass productDetails = new CreateServiceServletObjectClass(productCode,
						noOfService, duration);
				listOfProductDeatails.add(i, productDetails);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		
		
		wareHouseDetailsJArray = jObj.optJSONArray("wareHouseDetails");

		String wareHouseName = null;
		String warehouseCode = null;
		for (int i = 0; i < wareHouseDetailsJArray.length(); i++) {
			try {
				JSONObject json = wareHouseDetailsJArray.getJSONObject(i);
				wareHouseName = json.optString("sWarehouseName");
				warehouseCode = json.optString("WHCode");

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Address serviceAddress = new Address();
		if(jObj.optString("serviceaddress")!=null){
			serviceAddress.setAddrLine1(jObj.optString("serviceaddress"));
		}
		if(jObj.optString("locality")!=null){
			serviceAddress.setLocality(jObj.optString("locality"));
		}
		if(jObj.optString("city")!=null){
			serviceAddress.setCity(jObj.optString("city"));
		}
		if(jObj.optString("state")!=null){
			serviceAddress.setState(jObj.optString("state"));
		}
		serviceAddress.setCountry(jObj.optString("India"));
		serviceAddress.setPin(jObj.optInt("pincode"));
		
		logger.log(Level.SEVERE, "jService add"+jObj.optString("serviceaddress"));
		logger.log(Level.SEVERE, "serviceAddress =="+serviceAddress.getCompleteAddress());

		/*** Date 06-08-2019 by Vijay for warehouse name flag ***/
		String sepeartor = jObj.optString("WHName_AND").trim();
		logger.log(Level.SEVERE, "sepeartor " + sepeartor);
		if (sepeartor.equals("1")) {
			String actualWarehouseName = wareHouseName.replace("AND", "&");
			wareHouseName = actualWarehouseName;
		}
		logger.log(Level.SEVERE, "warehouseName after seperator=" + wareHouseName);

		List<Contract> contractlist = ofy().load().type(Contract.class).filter("companyId", companyId).filter("status", Contract.APPROVED)
				.filter("refNo2", warehouseCode).filter("warehouseName", wareHouseName).list();
		logger.log(Level.SEVERE, "companyId =" + companyId);
		logger.log(Level.SEVERE, "warehouseCode =" + warehouseCode);
		logger.log(Level.SEVERE, "warehouseName =" + wareHouseName);
		logger.log(Level.SEVERE, "contract list of inward warehouse ==" + contractlist.size());
		/**
		 * Ends here
		 */
		if (contractlist.size() == 0) {
			createContract(customerId, req, resp,  contractRefId, commodityName, wareHouseName, warehouseCode,
					customerBranch, contractDate, wareHouseDetailsJArray, listOfProductDeatails,serviceAddress,servicingBranch,companyId,clusterName);
		} else {
			updateUniqueStackMasterCreateService(customerId, contractRefId, contractlist, req, resp, commodityName,
					wareHouseName,customerBranch, servicingBranch, wareHouseDetailsJArray, contractDate, listOfProductDeatails,serviceAddress
					,companyId,clusterName);
		}
	}

	private boolean createContract(String customerId, final HttpServletRequest req, final HttpServletResponse resp,
			String contractRefId, String commodityName, String wareHouseName, String warehouseCode,
			String customerBranch2, String contractDate, JSONArray wareHouseDetailsJArray2,
			List<CreateServiceServletObjectClass> listOfProductDeatails, Address serviceAddress, String servicingBranch, long companyId, String clusterName) {
		String sendResponse = null;
		 if ((companyId == AUTHCODE) ) {

		Customer customer = checkCustomerNameExist(customerId.trim(),companyId);
		if (customer != null) {
//			logger.log(Level.SEVERE, "Product Code before sending:::::::" + productCode);
			List<ServiceProduct> serviceProductList = new ArrayList<ServiceProduct>();
			serviceProductList = checkProductExists(listOfProductDeatails);
			if (serviceProductList != null && serviceProductList.size() != 0
					&& serviceProductList.size() == listOfProductDeatails.size()) {
				Contract contract = new Contract();
				contract.getCinfo().setCount(customer.getCount());
				if (customer.getCompanyName() != null && !(customer.getCompanyName().trim().equalsIgnoreCase(""))) {
					contract.getCinfo().setFullName(customer.getCompanyName().trim());
				} else {
					contract.getCinfo().setFullName(customer.getFullname().trim());
				}
				contract.getCinfo().setCellNumber(customer.getCellNumber1());
				contract.getCinfo().setPocName(customer.getFullname().trim());
				// contract.setReferenceNumber(contractRefId + "");
				contract.setStatus(Contract.APPROVED);
				contract.setBranch(servicingBranch);
				contract.setWarehouseName(wareHouseName.trim());
				contract.setRefNo2(warehouseCode.trim());
				contract.setRefNo(contractRefId + "");
				if(serviceAddress!=null){
					contract.setCustomerServiceAddress(serviceAddress);
					contract.setNewcustomerAddress(serviceAddress);
				}

				// PaymentTerms is hard coded because we assume that they
				// have paid full payment in advance

				List<PaymentTerms> payTermsList = new ArrayList<PaymentTerms>();
				PaymentTerms payTerms = new PaymentTerms();
				payTerms.setPayTermDays(0);
				payTerms.setPayTermPercent(100.0);
				payTerms.setPayTermComment("Full Payment");
				payTermsList.add(payTerms);
				contract.setPaymentTermsList(payTermsList);

				contract.setEmployee("Snehal Abhishek Palav");
				contract.setApproverName("Snehal Abhishek Palav");
				contract.setGroup("In-House");
				contract.setCategory("Service Charges - Service Tax Exempted");
				contract.setType("Rate Contract(New)");
				contract.setCreatedBy("Snehal Abhishek Palav");
				// contract.setStartDate(startDate);
				SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
				sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
				TimeZone.setDefault(TimeZone.getTimeZone("IST"));
				try {
					contract.setStartDate(sdfDate.parse(contractDate));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Calendar c = Calendar.getInstance();
				try {
					c.setTime(sdfDate.parse(contractDate));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/**
				 * Date 24-04-2019 by Vijay Contract end date updated to 9999
				 */
				// c.add(Calendar.DATE, contractPeriod);
				// Date date = c.getTime();

				Calendar c3 = Calendar.getInstance();
				c3.set(9999, Calendar.DECEMBER, 31);
				Date date = c3.getTime();
				try {
					contract.setEndDate(sdfDate.parse(sdfDate.format(date)));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// SimpleDateFormat sdf = null;

				/***
				 * @author Vijay Chougule Date 01-08-2019 Des :- Stack Master to
				 *         create services for every stack and maintain stack
				 *         master
				 */
				int serviceCount = 0;
				ArrayList<StackMaster> stackMasterlist = new ArrayList<StackMaster>();

				for (int i = 0; i < wareHouseDetailsJArray2.length(); i++) {
					try {
						StackMaster serviceStackMaster = new StackMaster();
						JSONObject json = wareHouseDetailsJArray2.getJSONObject(i);
//						String warehouseName = json.optString("sWarehouseName");
						String warehousecode = json.optString("WHCode");
						String shade = json.optString("ShedName");
						String compartment = json.optString("sCompartmentName");
						String stackNo = json.optString("StackName");
						String quantity = json.optString("nNetWeight");
						String UOM = json.optString("Unit");
//						serviceStackMaster.setWarehouseName(warehouseName);
						serviceStackMaster.setWarehouseName(wareHouseName);
						serviceStackMaster.setWarehouseCode(warehousecode);
						serviceStackMaster.setShade(shade);
						serviceStackMaster.setCompartment(compartment);
						serviceStackMaster.setStackNo(stackNo);
						serviceStackMaster.setQuantity(Double.parseDouble(quantity));
						serviceStackMaster.setUOM(UOM);
						serviceStackMaster.setCompanyId(companyId);
						serviceStackMaster.setStatus(true);
						if(json.optString("commodityName")!=null){
							serviceStackMaster.setCommodityName(json.optString("commodityName"));
						}
						if(clusterName!=null)
						serviceStackMaster.setClusterName(clusterName);
						
						stackMasterlist.add(serviceStackMaster);
						serviceCount++;
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				logger.log(Level.SEVERE, "stackMasterlist" + stackMasterlist.size());

				logger.log(Level.SEVERE, "No Of stack for Services" + serviceCount);
				/********************
				 * This is for produc table
				 ******************/
				List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
				int listIndex = -1;
				try {
					for (ServiceProduct serviceProductItem : serviceProductList) {
						listIndex = listIndex + 1;
						SalesLineItem lineItem = new SalesLineItem();
						lineItem.setPrduct(serviceProductItem);
						lineItem.getPrduct().setCount(serviceProductItem.getCount());
						lineItem.setProductCode(serviceProductItem.getProductCode());
						lineItem.setProductName(serviceProductItem.getProductName());
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						sdf.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));
						try {
							lineItem.setStartDate(sdf.parse(contractDate));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE, "Error ::::::::::::::::" + e);
							e.printStackTrace();
						}
						// We are setting duration
						lineItem.setProductSrNo(listIndex + 1);
						// by Default 90 days because we want to create
						// service on 365 days and only 1 services therefore
						// only 1 noOfServices are created
						if (listOfProductDeatails.get(listIndex).getDurationInDays() != 0) {
							lineItem.setDuration(listOfProductDeatails.get(listIndex).getDurationInDays());
						} else {
							lineItem.setDuration(365);
						}
						if (listOfProductDeatails.get(listIndex).getDurationInDays() != 0) {
							lineItem.setNumberOfService(listOfProductDeatails.get(listIndex).getNumberOfService());
						} else {
							lineItem.setNumberOfService(serviceCount); // number
																		// of
																		// services
						}
						lineItem.setQuantity(1.0);
						lineItem.setPrice(serviceProductItem.getPrice());
//						lineItem.setVatTax(serviceProductItem.getVatTax());
//						lineItem.setServiceTax(serviceProductItem.getServiceTax());
						lineItem.setVatTax(new Tax());
						lineItem.setServiceTax(new Tax());
						lineItem.setEndDate(contract.getEndDate());
						
						/**** Date 23-04-2019 by vijay updated ****/
						ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
						BranchWiseScheduling custBranchSchedule = new BranchWiseScheduling();
						custBranchSchedule.setBranchName(customerBranch2);
						custBranchSchedule.setCheck(true);
						custBranchSchedule.setServicingBranch(servicingBranch);
						custBranchSchedule.setArea(0);
						custBranchSchedule.setUnitOfMeasurement("");
						scheduleBranchlist.add(custBranchSchedule);
						HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						customerBranchlist.put(lineItem.getProductSrNo(), scheduleBranchlist);

						lineItem.setCustomerBranchSchedulingInfo(customerBranchlist);

						listofSaleLineItem.add(lineItem);
					}
					logger.log(Level.SEVERE, "listofSaleLineItem.size()::::::::::::" + listofSaleLineItem.size());
					contract.setItems(listofSaleLineItem);

					/**
					 * Service Schedule Pop Up
					 */
					List<ServiceSchedule> serviceScheduleList = new ArrayList<ServiceSchedule>();

					try {
						for (int i = 0; i < listofSaleLineItem.size(); i++) {
							String previousDate = "";
							int gapOfService = (int) (listofSaleLineItem.get(i).getDuration()
									/ (listofSaleLineItem.get(i).getNumberOfServices()
											* listofSaleLineItem.get(i).getQty()));
							logger.log(Level.SEVERE, "gapOfService:::::::" + gapOfService);
							for (int j = 0; j < listofSaleLineItem.get(i).getNumberOfServices()
									* listofSaleLineItem.get(i).getQty(); j++) {
								ServiceSchedule serviceSchedule = new ServiceSchedule();
								serviceSchedule.setSerSrNo(listofSaleLineItem.get(i).getProductSrNo());
								serviceSchedule.setScheduleProdId(listofSaleLineItem.get(i).getPrduct().getCount());
								serviceSchedule.setScheduleProQty((int) listofSaleLineItem.get(i).getQty());
								serviceSchedule
										.setScheduleProdName(listofSaleLineItem.get(i).getPrduct().getProductName());
								serviceSchedule.setScheduleDuration(listofSaleLineItem.get(i).getDuration());
								serviceSchedule
										.setScheduleNoOfServices(listofSaleLineItem.get(i).getNumberOfServices());
								serviceSchedule.setScheduleServiceNo(j + 1);
								logger.log(Level.SEVERE, "gapOfService:::::::" + gapOfService);
								serviceSchedule.setScheduleProdStartDate(listofSaleLineItem.get(i).getStartDate());
								serviceSchedule.setScheduleStartDate(listofSaleLineItem.get(i).getStartDate());
								// ***************Used for marking service
								// date as red/black Date : 2/1/2017********

								SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
								sdf.setTimeZone(TimeZone.getTimeZone("IST"));
								TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//								Date dateProdEnd = sdf.parse(sdf.format(listofSaleLineItem.get(i).getStartDate()));
//								Calendar c2 = Calendar.getInstance();
//								try {
//									c2.setTime(dateProdEnd);
//									c2.add(Calendar.DATE, listofSaleLineItem.get(i).getDuration());
//								} catch (Exception e1) {
//									// TODO Auto-generated catch block
//									logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);
//
//									e1.printStackTrace();
//								}
//								System.out
//										.println("dateProdEnd:::::::::::" + dateProdEnd + "cal::::::::" + c2.getTime());
//								serviceSchedule.setScheduleProdEndDate(c2.getTime());

								serviceSchedule.setScheduleProdEndDate(contract.getEndDate());
								// **************ends here
								// ******************************
								
								/**
								 * Date 09-11-2019
								 * Des :- As disscussed with Vaishali Mam and Nitin Sir
								 * Deggassing Service will create when they complete the Fumigation Service based on Completion Date
								 */
								
//								/**
//								 * Date 08-11-2019 By Vijay
//								 * Des :- For Degassing Service Service Date must be as T+52
//								 */
//								if(listofSaleLineItem.get(i).getProductCode().trim().equals("DSSG")){
//									Date tPlus45DaysDate = sdf.parse(contractDate);
//									Calendar c4 = Calendar.getInstance();
//									c4.setTime(tPlus45DaysDate);
//									c4.add(Calendar.DATE, +52);
//									logger.log(Level.SEVERE, "Date after 52 Days===" + tPlus45DaysDate);
//									tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
//									logger.log(Level.SEVERE, "Date after 52 Days" + tPlus45DaysDate);
//									SimpleDateFormat sdfDateP = new SimpleDateFormat("dd/MM/yyyy");
//									sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
//									TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//									serviceSchedule.setScheduleServiceDate(tPlus45DaysDate);
//									
//								}else{
									/***
									 * Date 27-04-2019 by Vijay for Services must be
									 * T + 45 for Fumigation and Propalactic
									 ****/
									Date tPlus45DaysDate = sdf.parse(contractDate);
									Calendar c4 = Calendar.getInstance();
									c4.setTime(tPlus45DaysDate);
									c4.add(Calendar.DATE, +45);
									logger.log(Level.SEVERE, "Date after 45 Days===" + tPlus45DaysDate);
									tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
									logger.log(Level.SEVERE, "Date after 45 Days" + tPlus45DaysDate);
									SimpleDateFormat sdfDateP = new SimpleDateFormat("dd/MM/yyyy");
									sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
									TimeZone.setDefault(TimeZone.getTimeZone("IST"));
									serviceSchedule.setScheduleServiceDate(tPlus45DaysDate);
//								}
								
								serviceSchedule.setScheduleServiceTime("Flexible");
								serviceSchedule.setTotalServicingTime(0.0);
								serviceSchedule.setScheduleProBranch(customerBranch2);
								serviceSchedule.setServicingBranch(servicingBranch);
								serviceSchedule.setWarehouseName(stackMasterlist.get(j).getWarehouseName());
								serviceSchedule.setWarehouseCode(stackMasterlist.get(j).getWarehouseCode());
								serviceSchedule.setShade(stackMasterlist.get(j).getShade());
								serviceSchedule.setCompartment(stackMasterlist.get(j).getCompartment());
								serviceSchedule.setStackNo(stackMasterlist.get(j).getStackNo());
								serviceSchedule.setStackQty(stackMasterlist.get(j).getQuantity());
								serviceSchedule.setCommodityName(stackMasterlist.get(j).getCommodityName());
								if(stackMasterlist.get(j).getClusterName()!=null)
								serviceSchedule.setClusterName(stackMasterlist.get(j).getClusterName());
								
								serviceScheduleList.add(serviceSchedule);
								logger.log(Level.SEVERE, "serviceScheduleList:::::::::::" + serviceScheduleList.size());
							}
						}
					} catch (Exception e) {
						logger.log(Level.SEVERE, "ERROR in Method:::::::::::" + e);
						e.printStackTrace();
					}
					logger.log(Level.SEVERE, "serviceScheduleList.size()::::::::::::" + serviceScheduleList.size());
					contract.setServiceScheduleList(serviceScheduleList);

					contract.setWarehouseName(wareHouseName);
					contract.setRefNo2(warehouseCode);

					SimpleDateFormat sdfContractDate = new SimpleDateFormat("dd-MM-yyyy");
					sdfContractDate.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					contract.setContractDate(sdfContractDate.parse(contractDate));
				} catch (Exception e) {
					logger.log(Level.SEVERE, "Error outside method before calling method:::::::::::::" + e);
					e.printStackTrace();
				}

				List<ProductOtherCharges> productOtherChargesList = new ArrayList<ProductOtherCharges>();
				double quantity = 1;
				double totalAmount = 0;
				double totalAmtFix = 0;
				double netPayFix = 0;
				double netPayable = 0;
				double assessableAmount = 0;

//				for (ServiceProduct serviceProductItem : serviceProductList) {
//
//					if (((serviceProductItem.getVatTax() != null) && (serviceProductItem.getVatTax().isInclusive()))
//							&& (serviceProductItem.getServiceTax() == null)) {
//
//						logger.log(Level.SEVERE, "Vat Tax Inclusive and Service Tax Not Present::::::::::::");
//						// this is the value on which the tax will get
//						// calculated
//						// Formula : (Price-Price*VAT%) .....Because it is
//						// inclusive
//						assessableAmount = ((serviceProductItem.getPrice() * quantity) - ((serviceProductItem.getPrice()
//								* quantity * serviceProductItem.getVatTax().getPercentage()) / 100));
//						logger.log(Level.SEVERE, "assessableAmount::::::::::::" + assessableAmount);
//						totalAmount = assessableAmount;
//						ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//						productOtherCharges.setChargeName(serviceProductItem.getVatTax().getTaxConfigName().trim());
//						productOtherCharges.setChargePercent(serviceProductItem.getVatTax().getPercentage());
//						productOtherCharges.setAssessableAmount(assessableAmount);
//						double chargePayable = assessableAmount * serviceProductItem.getVatTax().getPercentage() / 100;
//						productOtherCharges.setChargePayable(chargePayable);
//						logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayable);
//						totalAmtFix = totalAmtFix + totalAmount;
//						netPayable = serviceProductItem.getPrice();
//						netPayFix = netPayFix + netPayable;
//						productOtherChargesList.add(productOtherCharges);
//					} else if (((serviceProductItem.getVatTax() != null)
//							&& !(serviceProductItem.getVatTax().isInclusive()))
//							&& (serviceProductItem.getServiceTax() == null)) {
//
//						logger.log(Level.SEVERE, "Vat Tax Exclusive and Service Tax Not Present::::::::::::");
//						assessableAmount = serviceProductItem.getPrice() * quantity;
//						logger.log(Level.SEVERE, "assessableAmount::::::::::::" + assessableAmount);
//						totalAmount = serviceProductItem.getPrice() * quantity;
//						ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//						productOtherCharges.setChargeName(serviceProductItem.getVatTax().getTaxConfigName());
//						productOtherCharges.setChargePercent(serviceProductItem.getVatTax().getPercentage());
//						productOtherCharges.setAssessableAmount(assessableAmount);
//						double chargePayable = assessableAmount * serviceProductItem.getVatTax().getPercentage() / 100;
//						productOtherCharges.setChargePayable(chargePayable);
//						logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayable);
//						netPayable = totalAmount + chargePayable;
//						totalAmtFix = totalAmtFix + totalAmount;
//						netPayFix = netPayFix + netPayable;
//					} else if (((serviceProductItem.getServiceTax() != null)
//							&& (serviceProductItem.getServiceTax().isInclusive()))
//							&& (serviceProductItem.getVatTax() == null)) {
//
//						logger.log(Level.SEVERE, "Vat Tax Not Present and Service Tax inclusive::::::::::::");
//						assessableAmount = ((serviceProductItem.getPrice() * quantity) - ((serviceProductItem.getPrice()
//								* quantity * serviceProductItem.getServiceTax().getPercentage()) / 100));
//						totalAmount = assessableAmount;
//						logger.log(Level.SEVERE, "assessableAmount::::::::::::" + assessableAmount);
//						ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//						productOtherCharges.setChargeName(serviceProductItem.getServiceTax().getTaxConfigName());
//						productOtherCharges.setChargePercent(serviceProductItem.getServiceTax().getPercentage());
//						productOtherCharges.setAssessableAmount(assessableAmount);
//						double chargePayable = assessableAmount * serviceProductItem.getServiceTax().getPercentage()
//								/ 100;
//						productOtherCharges.setChargePayable(chargePayable);
//						logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayable);
//						netPayable = serviceProductItem.getPrice();
//						totalAmtFix = totalAmtFix + totalAmount;
//						netPayFix = netPayFix + netPayable;
//					} else if (((serviceProductItem.getServiceTax() != null)
//							&& !(serviceProductItem.getServiceTax().isInclusive()))
//							&& (serviceProductItem.getVatTax() == null)) {
//
//						logger.log(Level.SEVERE, "Vat Tax Not Present and Service Tax Exclusive::::::::::::");
//						assessableAmount = serviceProductItem.getPrice() * quantity;
//						logger.log(Level.SEVERE, "assessableAmount::::::::::::" + assessableAmount);
//						totalAmount = serviceProductItem.getPrice() * quantity;
//						ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//						productOtherCharges.setChargeName(serviceProductItem.getServiceTax().getTaxConfigName());
//						productOtherCharges.setChargePercent(serviceProductItem.getServiceTax().getPercentage());
//						productOtherCharges.setAssessableAmount(assessableAmount);
//						double chargePayable = assessableAmount * serviceProductItem.getServiceTax().getPercentage()
//								/ 100;
//						productOtherCharges.setChargePayable(chargePayable);
//						logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayable);
//						netPayable = totalAmount + chargePayable;
//						totalAmtFix = totalAmtFix + totalAmount;
//						netPayFix = netPayFix + netPayable;
//					} else if (serviceProductItem.getServiceTax() != null && serviceProductItem.getVatTax() != null) {
//
//						logger.log(Level.SEVERE, "Vat Tax Present  and Service Tax Present::::::::::::");
//
//						if (serviceProductItem.getServiceTax().isInclusive()
//								&& serviceProductItem.getVatTax().isInclusive()) {
//							logger.log(Level.SEVERE, "Vat Tax Inclusive and Service Tax Inclusive::::::::::::");
//
//							netPayable = serviceProductItem.getPrice() * quantity;
//							// For Tax
//							double assessableAmountForST = (netPayable * 100)
//									/ (100 + serviceProductItem.getServiceTax().getPercentage());
//							logger.log(Level.SEVERE, "assessableAmountForST::::::::::::" + assessableAmountForST);
//
//							double assessableAmountForVT = (assessableAmountForST * 100)
//									/ (100 + serviceProductItem.getVatTax().getPercentage());
//							logger.log(Level.SEVERE, "assessableAmountForVT::::::::::::" + assessableAmountForVT);
//							totalAmount = assessableAmountForVT;
//							totalAmtFix = totalAmtFix + totalAmount;
//							netPayFix = netPayFix + netPayable;
//
//							// For VAT
//							ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//							productOtherCharges.setChargeName(serviceProductItem.getVatTax().getTaxConfigName());
//							productOtherCharges.setChargePercent(serviceProductItem.getVatTax().getPercentage());
//							productOtherCharges.setAssessableAmount(assessableAmountForVT);
//							double chargePayableVT = assessableAmountForVT
//									* serviceProductItem.getVatTax().getPercentage() / 100;
//							productOtherCharges.setChargePayable(chargePayableVT);
//							logger.log(Level.SEVERE, "chargePayableVT::::::::::::" + chargePayableVT);
//							productOtherChargesList.add(productOtherCharges);
//
//							// For ST
//							ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
//							productOtherChargesST.setChargeName(serviceProductItem.getServiceTax().getTaxConfigName());
//							productOtherChargesST.setChargePercent(serviceProductItem.getServiceTax().getPercentage());
//							productOtherChargesST.setAssessableAmount(assessableAmountForST);
//							double chargePayableST = assessableAmountForST
//									* serviceProductItem.getServiceTax().getPercentage() / 100;
//							productOtherChargesST.setChargePayable(chargePayableST);
//							logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayableST);
//							productOtherChargesList.add(productOtherChargesST);
//
//						} else if (!(serviceProductItem.getServiceTax().isInclusive())
//								&& !(serviceProductItem.getVatTax().isInclusive())) {
//
//							logger.log(Level.SEVERE, "Vat Tax Exclusive and Service Tax Exclusive::::::::::::");
//							totalAmount = serviceProductItem.getPrice();
//							double assessableAmountForVT = totalAmount;
//							logger.log(Level.SEVERE, "assessableAmountForVT::::::::::::" + assessableAmountForVT);
//							ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//							productOtherCharges.setChargeName(serviceProductItem.getVatTax().getTaxConfigName());
//							productOtherCharges.setChargePercent(serviceProductItem.getVatTax().getPercentage());
//							productOtherCharges.setAssessableAmount(assessableAmountForVT);
//							double chargePayableVT = (assessableAmountForVT
//									* serviceProductItem.getVatTax().getPercentage()) / 100;
//							productOtherCharges.setChargePayable(chargePayableVT);
//							logger.log(Level.SEVERE, "chargePayableST::::::::::::" + chargePayableVT);
//							productOtherChargesList.add(productOtherCharges);
//
//							// For ST
//							double assessableAmountForST = totalAmount + chargePayableVT;
//
//							logger.log(Level.SEVERE, "assessableAmountForST::::::::::::" + assessableAmountForST);
//							ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
//							productOtherChargesST.setChargeName(serviceProductItem.getServiceTax().getTaxConfigName());
//							productOtherChargesST.setChargePercent(serviceProductItem.getServiceTax().getPercentage());
//							productOtherChargesST.setAssessableAmount(assessableAmountForST);
//							double chargePayableST = assessableAmountForST
//									* serviceProductItem.getServiceTax().getPercentage() / 100;
//							productOtherChargesST.setChargePayable(chargePayableST);
//							netPayable = assessableAmountForST + chargePayableST;
//							totalAmtFix = totalAmtFix + totalAmount;
//							netPayFix = netPayFix + netPayable;
//							logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayableST);
//							productOtherChargesList.add(productOtherChargesST);
//						} else if (serviceProductItem.getServiceTax().isInclusive()
//								&& !(serviceProductItem.getVatTax().isInclusive())) {
//
//							logger.log(Level.SEVERE, "Vat Tax Exclusive and Service Tax Inclusive::::::::::::");
//							// ServiceTax=Inclusive && VatTax=Exclusive....
//							// Here we are adding VAT TAX first because it
//							// is
//							// exclusive, and calculating NetPayable value.
//							// The
//							// we will do reverse calculation
//							double netPay = (serviceProductItem.getPrice()
//									* serviceProductItem.getVatTax().getPercentage()) / 100;
//							netPayable = netPay;
//
//							double assessableAmountST = (netPayable * 100)
//									/ (100 + serviceProductItem.getServiceTax().getPercentage());
//							double assessableAmountVT = (assessableAmountST * 100)
//									/ (100 + serviceProductItem.getVatTax().getPercentage());
//
//							totalAmount = assessableAmountVT;
//
//							// Now we will calculate VAT TAX
//							ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//							productOtherCharges.setChargeName(serviceProductItem.getVatTax().getTaxConfigName());
//							productOtherCharges.setChargePercent(serviceProductItem.getVatTax().getPercentage());
//							productOtherCharges.setAssessableAmount(assessableAmountVT);
//							productOtherCharges.setChargePayable(
//									(assessableAmountVT * serviceProductItem.getVatTax().getPercentage()) / 100);
//							productOtherChargesList.add(productOtherCharges);
//
//							// First we are calculating Service Tax of the
//							// product to find assessable amount to show in
//							// product
//							ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
//							productOtherChargesST.setChargeName(serviceProductItem.getServiceTax().getTaxConfigName());
//							productOtherChargesST.setChargePercent(serviceProductItem.getServiceTax().getPercentage());
//							productOtherChargesST.setAssessableAmount(assessableAmountST);
//							productOtherChargesST.setChargePayable(
//									(assessableAmountST * serviceProductItem.getServiceTax().getPercentage()) / 100);
//							productOtherChargesList.add(productOtherChargesST);
//							totalAmtFix = totalAmtFix + totalAmount;
//							netPayFix = netPayFix + netPayable;
//
//						} else if (!(serviceProductItem.getServiceTax().isInclusive())
//								&& (serviceProductItem.getVatTax().isInclusive())) {
//
//							logger.log(Level.SEVERE, "Vat Tax Inclusive and Service Tax Exclusive ::::::::::::");
//							// ServiceTax=Exclusive && VatTax=Inclusive....
//							double assesableAmountST = serviceProductItem.getPrice();
//							double assesableAmountVT = (assesableAmountST * 100)
//									/ (100 + serviceProductItem.getServiceTax().getPercentage());
//							totalAmount = assesableAmountVT;
//							netPayable = assesableAmountST;
//
//							netPayFix = netPayFix + netPayable;
//
//							totalAmtFix = totalAmtFix + totalAmount;
//
//							// Calculation of VAT TAX
//							ProductOtherCharges productOtherCharges = new ProductOtherCharges();
//							productOtherCharges.setChargeName(serviceProductItem.getVatTax().getTaxConfigName());
//							productOtherCharges.setChargePercent(serviceProductItem.getVatTax().getPercentage());
//							productOtherCharges.setAssessableAmount(assesableAmountVT);
//							double chargePayable = (assesableAmountVT * serviceProductItem.getVatTax().getPercentage())
//									/ 100;
//							productOtherCharges.setChargePayable(chargePayable);
//							logger.log(Level.SEVERE, "chargePayable::::::::::::" + chargePayable);
//							productOtherChargesList.add(productOtherCharges);
//							// Calculation for Service Tax
//							ProductOtherCharges productOtherChargesST = new ProductOtherCharges();
//							productOtherChargesST.setChargeName(serviceProductItem.getServiceTax().getTaxConfigName());
//							productOtherChargesST.setChargePercent(serviceProductItem.getServiceTax().getPercentage());
//							productOtherChargesST.setAssessableAmount(assesableAmountST);
//							double chargePayableST = (assesableAmountST
//									* serviceProductItem.getServiceTax().getPercentage()) / 100;
//							productOtherChargesST.setChargePayable(chargePayableST);
//							logger.log(Level.SEVERE, "chargePayableST::::::::::::" + chargePayableST);
//							productOtherChargesList.add(productOtherChargesST);
//						}
//
//					} 
//					else {
//						logger.log(Level.SEVERE, "Vat Tax Not Present and Service Tax Not Present::::::::::::");
//						totalAmount = serviceProductItem.getPrice();
//						netPayable = totalAmount;
//						totalAmtFix = totalAmtFix + totalAmount;
//						netPayFix = netPayFix + netPayable;
//					}
//				}
				/**
				 * Date 08-11-2019 by Vijay
				 * Des :- Above code Not required so commneted and Uodated code below for total amt and net payable  
				 */
				for (ServiceProduct serviceProductItem : serviceProductList) {
					totalAmount += serviceProductItem.getPrice();
//					netPayable += totalAmount;
//					totalAmtFix = totalAmtFix + totalAmount;
//					netPayFix = netPayFix + netPayable;
				}
				contract.setStatus(Service.APPROVED);
				contract.setCreationDate(new Date());
//				contract.getProductTaxes().addAll(productOtherChargesList);
				
				logger.log(Level.SEVERE, "Total Amount:::::::" + totalAmount);
				logger.log(Level.SEVERE, "netPayFix:::::::" + netPayFix);
//				contract.setTotalAmount(totalAmtFix);
//				contract.setNetpayable(netPayFix);
//				contract.setGrandTotal(netPayFix);
//				contract.setNetpayable(totalAmtFix);
//				contract.setGrandTotal(totalAmtFix);
				contract.setTotalAmount(totalAmount);
				contract.setNetpayable(totalAmount);
				contract.setGrandTotal(totalAmount);
				contract.setNetpayable(totalAmount);
				contract.setGrandTotal(totalAmount);
				contract.setCompanyId(companyId);
				Exception exep = null;
				ReturnFromServer server = new ReturnFromServer();
				try {
					GenricServiceImpl impl = new GenricServiceImpl();
					server = impl.save(contract);
				} catch (Exception e) {
					exep = e;
				}
				logger.log(Level.SEVERE, "Exception if any While contract Saving" + exep);
				if (exep == null) {
					logger.log(Level.SEVERE, "Contract Id inside value:::::::::::" + server.count);
//					createService(req, resp, server.count,companyId);
					createService(req, resp, contract,companyId);

					CreateStockMaster(stackMasterlist);

					// createBilling(req, resp, companyId, contractId);
				}

			} else {
				sendResponse = "Product does not exists";
			}

		} else {
			sendResponse = "Customer Does not exist!!";
		}
		 } else {
		 sendResponse = "Authentication Failed";
		 }
		if (sendResponse != null) {
			try {
				resp.getWriter().println(sendResponse);
			} catch (IOException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "ERROR in sending respone" + e);
			}
		}

		return true;
	}

	// private List<ServiceSchedule>
	// getServiceScheduleListPopUp(List<SalesLineItem> listofSaleLineItem) {
	// // TODO Auto-generated method stub
	// logger.log(Level.SEVERE, "Inside pop up!!!!!!!!!!!!!!!!!!");
	//
	// ArrayList<ServiceSchedule> serviceScheduleList2 = new
	// ArrayList<ServiceSchedule>();
	// try {
	// for (int i = 0; i < listofSaleLineItem.size(); i++) {
	// logger.log(Level.SEVERE, "inside sale line item:::::::::::" + i);
	// String previousDate = "";
	// int gapOfService = (int) (listofSaleLineItem.get(i).getDuration()
	// / (listofSaleLineItem.get(i).getNumberOfServices() *
	// listofSaleLineItem.get(i).getQty()));
	//
	// for (int j = 0; j < listofSaleLineItem.get(i).getNumberOfServices()
	// * listofSaleLineItem.get(i).getQty(); j++) {
	// logger.log(Level.SEVERE, "inside No of Services:::::::::::" + j);
	// logger.log(Level.SEVERE, "gapOfService:::::::" + gapOfService);
	// ServiceSchedule serviceSchedule2 = new ServiceSchedule();
	// serviceSchedule2.setSerSrNo(listofSaleLineItem.get(i).getProductSrNo());
	// serviceSchedule2.setScheduleProdId(listofSaleLineItem.get(i).getPrduct().getCount());
	// serviceSchedule2.setScheduleProdName(listofSaleLineItem.get(i).getPrduct().getProductName());
	// serviceSchedule2.setScheduleDuration(listofSaleLineItem.get(i).getDuration());
	// serviceSchedule2.setScheduleNoOfServices(listofSaleLineItem.get(i).getNumberOfServices());
	// serviceSchedule2.setScheduleServiceNo(j + 1);
	// logger.log(Level.SEVERE, "gapOfService:::::::" + gapOfService);
	//
	// // ***************Used for marking service date as red/black
	// // Date : 2/1/2017********
	//
	// SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	// sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	// Date dateProdEnd =
	// sdf.parse(sdf.format(listofSaleLineItem.get(i).getStartDate()));
	// Calendar c2 = Calendar.getInstance();
	// try {
	// c2.setTime(dateProdEnd);
	// c2.add(Calendar.DATE, listofSaleLineItem.get(i).getDuration());
	// } catch (Exception e1) {
	// // TODO Auto-generated catch block
	// logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);
	//
	// e1.printStackTrace();
	// }
	// System.out.println("dateProdEnd:::::::::::" + dateProdEnd + "cal::::::::"
	// + c2.getTime());
	// serviceSchedule2.setScheduleProdEndDate(c2.getTime());
	//
	// // **************ends here ******************************
	//
	// if (j + 1 == 1) {
	// SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	// sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
	// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	//
	// try {
	// serviceSchedule2.setScheduleServiceDate(sdfDate.parse(contractDate));
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// logger.log(Level.SEVERE, "Error in j==1" + e);
	//
	// e.printStackTrace();
	// }
	// previousDate = contractDate;
	// } else {
	// SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	// sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
	// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	//
	// Calendar c = Calendar.getInstance();
	// try {
	// c.setTime(sdfDate.parse(previousDate));
	// } catch (ParseException e1) {
	// // TODO Auto-generated catch block
	// logger.log(Level.SEVERE, "Error inn j>1:::::::" + e1);
	//
	// e1.printStackTrace();
	// }
	// logger.log(Level.SEVERE, "gapOfService:::::::" + gapOfService);
	// try {
	// c.add(Calendar.DATE, gapOfService);
	// Date date = c.getTime();
	// // serviceSchedule2.setScheduleServiceDay();
	//
	// serviceSchedule2.setScheduleServiceDate(sdfDate.parse(sdfDate.format(date)));
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// logger.log(Level.SEVERE, "Error in Adding days:::::::" + e);
	// e.printStackTrace();
	// }
	// }
	// serviceSchedule2.setScheduleServiceTime("Flexible");
	// serviceSchedule2.setTotalServicingTime(0.0);
	// serviceSchedule2.setScheduleProBranch(customerBranch);
	// serviceScheduleList2.add(serviceSchedule2);
	// logger.log(Level.SEVERE, "serviceScheduleList:::::::::::" +
	// serviceScheduleList2.size());
	// }
	// }
	// } catch (Exception e) {
	// logger.log(Level.SEVERE, "ERROR in Method:::::::::::" + e);
	// e.printStackTrace();
	// }
	// return serviceScheduleList2;
	// }

	private void createService(HttpServletRequest req, HttpServletResponse resp, Contract contract, long companyId) {
//		/***
//		 * Date 23-04-2019 by Vijay Des :- Services not creating so added timer
//		 * to execute
//		 */
//		String sendResponse =null;
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//			logger.log(Level.SEVERE, "In TIMER");
//		}
//		/**
//		 * ends here
//		 */
//		logger.log(Level.SEVERE, "After TIMER");
//
////		Company company = ofy().load().type(Company.class).filter("companyId", companyId)
////				.filter("accessUrl", accessUrl.trim()).first().now();
//
//		// if ((company.getCompanyId() == AUTHCODE)
//		// && (applicationId.trim().equalsIgnoreCase(APPID))) {
//		Contract contract = ofy().load().type(Contract.class).filter("companyId", companyId).filter("count", contractId)
//				.first().now();

//		logger.log(Level.SEVERE, "contract in create service::" + contract);
//		if (contract != null) {
//			// createService();
//			sendResponse = createServicesByContract(contract);
//		} else {
//			logger.log(Level.SEVERE, "Contract not found");
//		}
		
		String sendResponse =null;
		sendResponse = createServicesByContract(contract);
		
		if (sendResponse != null) {
			try {
				resp.getWriter().print(sendResponse);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

//	private String createServicesByContract(Contract model) {
//		sendRespone = null;
//		ContractServiceImplementor impl = new ContractServiceImplementor();
//		try {
//			impl.makeServices(model);
//			sendRespone = "Services Created Successfull";
//		} catch (Exception e) {
//			sendRespone = "Services not created:::::" + e;
//			e.printStackTrace();
//		}
//		return sendRespone;
//	}

	// private void createService() {
	// TODO Auto-generated method stub
	//
	// if (checkCustomerNameExist(contract, clientName)) {
	//
	// if (checkCustomerAddress_Branch(contract, company, customerBranch)) {
	// NumberGeneration ng = new NumberGeneration();
	// ng = ofy().load().type(NumberGeneration.class)
	// .filter("companyId", companyId)
	// .filter("processName", "Service")
	// .filter("status", true).first().now();
	//
	// long number = ng.getNumber();
	// int count = (int) number;
	// logger.log(Level.SEVERE, "Count :: " + count);
	//
	// Service service = new Service();
	//
	// service.setContractCount(contract.getCount());
	// service.setContractStartDate(contract.getStartDate());
	// service.setContractEndDate(contract.getEndDate());
	// // service.setWareHouse(wareHouse);
	// // service.setStorageLocation(storageLocation);
	// // service.setStorageBin(storageBin);
	//
	// // Customer Info
	// PersonInfo person = new PersonInfo();
	// person.setCount(contract.getCinfo().getCount());
	// person.setCellNumber(contract.getCinfo().getCellNumber());
	// person.setPocName(contract.getCinfo().getPocName());
	// person.setFullName(contract.getCinfo().getFullName().trim());
	// service.setPersonInfo(person);
	//
	// /**
	// * Harded Coded serial number as one because of contract. and
	// * there will be only one service created against one contract .
	// */
	// service.setStackDetailsList(contract.getStackDetailsList());
	// service.setServiceSerialNo(1);
	// service.setEmployee(serviceEngineer);
	// service.setBranch(servicingBranch);
	// SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	// sdf.setTimeZone(TimeZone.getTimeZone("IST"));
	// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	// try {
	// service.setServiceDate(sdf.parse(serviceDate));
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// service.setBranch(servicingBranch);
	//
	// Address address = new Address();
	// address.setAddrLine1(serviceAddressMap.get("addressLine1"));
	// address.setAddrLine2(serviceAddressMap.get("addressLine2"));
	// address.setLandmark(serviceAddressMap.get("landmark"));
	// address.setLocality(serviceAddressMap.get("locality"));
	// address.setCity(serviceAddressMap.get("city"));
	// address.setState(serviceAddressMap.get("state"));
	// address.setCountry(serviceAddressMap.get("country"));
	// address.setPin(Long.parseLong(serviceAddressMap.get("pin")));
	//
	// service.setAddress(address);
	// service.setServiceTime(serviceTime);
	//
	// service.setCompanyId(companyId);
	// // Warehouse code / Shed Name / Multiple stack No
	//
	// service.setStatus(Service.SERVICESTATUSSCHEDULE);
	//
	// service.setCount(count + 1);
	//
	// ng.setNumber(count + 1);
	//
	// GenricServiceImpl impl = new GenricServiceImpl();
	// impl.save(ng);
	// ofy().save().entity(service).now();
	//
	// sendResponse = "SuccessFull";
	// } else {
	// sendResponse = "Customer Branch does not Exist!!";
	// }
	// // } else {
	// // sendResponse = "Product does not Exist!!";
	// // }
	// // } else {
	// // sendResponse = "Clientname does not match in Contract!!";
	// // }
	// // }
	// // else {
	// // sendResponse = "Contract with id " + contractId
	// // + " does not exist";
	// // }
	// // } else {
	// // sendResponse = "Authentication Failed";
	// // }
	//
	// if (sendResponse != null) {
	// try {
	// resp.getWriter().println(sendResponse);
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	//
	// }

	// private void createBilling(HttpServletRequest req, HttpServletResponse
	// resp, long companyId, int contractId) {
	// logger.log(Level.SEVERE, "Creating Billing Doc");
	// Contract contract = ofy().load().type(Contract.class).filter("companyId",
	// companyId).filter("count", contractId)
	// .first().now();
	// logger.log(Level.SEVERE, "contract in create billing::" + contract);
	// // Customer customer = ofy().load().type(Customer.class)
	// // .filter("companyId", companyId)
	// // .filter("count", contract.getCustomerId()).first().now();
	// if (contract != null) {
	// BillingDocument billingDoc = new BillingDocument();
	// // Setting customer information
	// PersonInfo personInfo = new PersonInfo();
	// personInfo.setCount(contract.getCinfo().getCount());
	// personInfo.setFullName(contract.getCinfo().getFullName());
	// personInfo.setCellNumber(contract.getCinfo().getCellNumber());
	// personInfo.setPocName(contract.getCinfo().getPocName());
	// billingDoc.setPersonInfo(personInfo);
	// // Setting contract information
	// /*** Order Informaton *****/
	// billingDoc.setContractCount(contract.getCount());
	// System.out.println("The Contract Count is" + contract.getCount());
	// billingDoc.setTotalSalesAmount(contract.getNetpayable());
	//
	// System.out.println("Net Payable1 is :" + contract.getNetpayable());
	//
	// billingDoc.setAccountType("AR");
	// /*** Done with Order Informaton *****/
	//
	// /********* Payment Terms are Hard Coded ********************/
	// ArrayList<PaymentTerms> listofpayterms = new ArrayList<PaymentTerms>();
	// PaymentTerms paymentterms = new PaymentTerms();
	// paymentterms.setPayTermDays(0);
	// paymentterms.setPayTermPercent(100d);
	// paymentterms.setPayTermComment("Default by Batch");
	// listofpayterms.add(paymentterms);
	// billingDoc.setArrPayTerms(listofpayterms);
	// /************ Done with Payment Terms *********************/
	//
	// /**** Billing Info **********/
	// NumberGeneration ngforbilling =
	// ofy().load().type(NumberGeneration.class).filter("companyId", companyId)
	// .filter("processName", "BillingDocument").filter("status",
	// true).first().now();
	//
	// long number1 = ngforbilling.getNumber();
	// int countforbilling = (int) number1;
	// // billingDoc.setCount(countforbilling + 1);
	// SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
	// sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
	// TimeZone.setDefault(TimeZone.getTimeZone("IST"));
	// try {
	// billingDoc.setBillingDate(sdfDate.parse(contractDate));
	// } catch (ParseException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// billingDoc.setStatus(BillingDocument.CREATED);
	// billingDoc.setApproverName(contract.getEmployee());
	// billingDoc.setEmployee(contract.getEmployee());
	// billingDoc.setBranch(servicingBranch);
	// /**** Done with Billing Info **********/
	//
	// /********** Billing Document Information ****************/
	// billingDoc.getBillingDocumentInfo().setBillId(countforbilling + 1);
	// billingDoc.getBillingDocumentInfo().setBillingDate(new Date());
	// billingDoc.getBillingDocumentInfo().setBillAmount(contract.getNetpayable());
	// System.out.println("net payable amount 2:" + contract.getNetpayable());
	// billingDoc.getBillingDocumentInfo().setBillstatus(BillingDocument.CREATED);
	// /********** Billing Document Information ****************/
	//
	// /************ Product Details ***************/
	//
	// int noOfProduct = contract.getItems().size();
	// System.out.println("The No of Product" + noOfProduct);
	//
	// ArrayList<SalesOrderProductLineItem> productTable = new
	// ArrayList<SalesOrderProductLineItem>();
	//
	// for (int i = 0; i < noOfProduct; i++) {
	//
	// SalesOrderProductLineItem product = new SalesOrderProductLineItem();
	//
	// System.out.println("product.setProdId(contract.getItems().get(i).getPrduct().getCount())"
	// + (contract.getItems().get(i).getPrduct().getCount()));
	//
	// if (contract.getItems().get(i).getPrduct().getCount() != 0) {
	//
	// product.setProdId(contract.getItems().get(i).getPrduct().getCount());
	//
	// }
	//
	// product.setProdCategory(contract.getItems().get(i).getProductCategory());
	// product.setProdCode(contract.getItems().get(i).getProductCode());
	// product.setProdName(contract.getItems().get(i).getProductName());
	// product.setQuantity(contract.getItems().get(i).getQty());
	// product.setPrice(contract.getItems().get(i).getPrice());
	// product.setVatTax(contract.getItems().get(i).getVatTax());
	// product.setServiceTax(contract.getItems().get(i).getServiceTax());
	// // Tax tax=new Tax();
	// // tax.setPercentage(14.5);
	// // product.setServiceTax(tax);
	// product.setTotalAmount(contract.getItems().get(i).getPrice());
	// product.setBaseBillingAmount(
	// (contract.getItems().get(i).getQty()) *
	// (contract.getItems().get(i).getPrice()));
	// product.setPaymentPercent(100);
	// // PAyable amount bacha hai
	//
	// productTable.add(product);
	// }
	//
	// billingDoc.setSalesOrderProducts(productTable);
	// System.out.println("contract.getNetpayable()" +
	// contract.getNetpayable());
	// billingDoc.setTotalBillingAmount(contract.getNetpayable());
	//
	// // billing.setTotalSalesAmount(billing.getTotalBillingAmount());
	// // System.out.println("The Total Value is
	// // :"+billing.getTotalBillingAmount());
	// /************ Finish with Product Details ***************/
	//
	// /***** Tax Table *************/
	//
	// List<ContractCharges> taxTableList = new ArrayList<ContractCharges>();
	//
	// for (int i = 0; i < contract.getProductTaxes().size(); i++) {
	// ContractCharges taxTable = new ContractCharges();
	// taxTable.setTaxChargeName(contract.getProductTaxes().get(i).getChargeName());
	// taxTable.setTaxChargePercent(contract.getProductTaxes().get(i).getChargePercent());
	// taxTable.setTaxChargeAssesVal(contract.getProductTaxes().get(i).getAssessableAmount());
	// taxTable.setPayableAmt(contract.getProductTaxes().get(i).getChargePayable());
	// taxTableList.add(taxTable);
	// }
	// billingDoc.setBillingTaxes(taxTableList);
	// /**** Done with Tax Table **************/
	//
	// // Saving Billing Document
	// billingDoc.setTypeOfOrder(AppConstants.ORDERTYPESERVICE);
	// billingDoc.setCompanyId(companyId);
	// GenricServiceImpl genSerImpl = new GenricServiceImpl();
	// genSerImpl.save(billingDoc);
	// }
	// }

	private List<ServiceProduct> checkProductExists(List<CreateServiceServletObjectClass> listOfProductDeatails2) {
		// TODO Auto-generated method stub
		List<ServiceProduct> list = new ArrayList<ServiceProduct>();
		for (int i = 0; i < listOfProductDeatails2.size(); i++) {
			logger.log(Level.SEVERE, "Product Code::::::::::" + listOfProductDeatails2.get(i).getProductCode().trim());
			ServiceProduct serviceProduct = ofy().load().type(ServiceProduct.class).filter("companyId", AUTHCODE)
					.filter("productCode", listOfProductDeatails2.get(i).getProductCode().trim()).first().now();
			if (serviceProduct != null) {
				// Updated by vijay
				serviceProduct.setVatTax(null);
				serviceProduct.setVatTax(null);

				list.add(serviceProduct);
			}

		}

		return list;
	}

//	private boolean checkCustomerAddress_Branch(Contract contract, Company company, String customerBranch) {
//		// TODO Auto-generated method stub
//		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId)
//				.filter("count", contract.getCustomerId()).first().now();
//
//		CustomerBranchDetails customerBranchDetails = ofy().load().type(CustomerBranchDetails.class)
//				.filter("companyId", companyId).filter("cinfo.count", contract.getCustomerId()).first().now();
//		if (customerBranch.trim().equalsIgnoreCase("ServiceAddress")) {
//
//			serviceAddressMap.put("addressLine1", customer.getSecondaryAdress().getAddrLine1().trim());
//			if (customer.getSecondaryAdress().getAddrLine2() != null) {
//				serviceAddressMap.put("addressLine2", customer.getSecondaryAdress().getAddrLine2().trim());
//			} else {
//				serviceAddressMap.put("addressLine2", " ");
//			}
//			if (customer.getSecondaryAdress().getLandmark() != null) {
//				serviceAddressMap.put("landmark", customer.getSecondaryAdress().getLandmark().trim());
//			} else {
//				serviceAddressMap.put("landmark", " ");
//			}
//
//			if (customer.getSecondaryAdress().getLocality() != null) {
//				serviceAddressMap.put("locality", customer.getSecondaryAdress().getLocality().trim());
//			} else {
//				serviceAddressMap.put("locality", " ");
//			}
//			serviceAddressMap.put("country", customer.getSecondaryAdress().getCountry().trim());
//			serviceAddressMap.put("state", customer.getSecondaryAdress().getState().trim());
//			serviceAddressMap.put("city", customer.getSecondaryAdress().getCity().trim());
//			serviceAddressMap.put("pin", customer.getSecondaryAdress().getPin() + "");
//			return true;
//		} else if (customerBranchDetails != null) {
//			serviceAddressMap.put("addressLine1", customerBranchDetails.getAddress().getAddrLine1().trim());
//			if (customerBranchDetails.getAddress().getAddrLine2() != null) {
//				serviceAddressMap.put("addressLine2", customerBranchDetails.getAddress().getAddrLine2().trim());
//			} else {
//				serviceAddressMap.put("addressLine2", " ");
//			}
//			if (customerBranchDetails.getAddress().getLandmark() != null) {
//				serviceAddressMap.put("landmark", customerBranchDetails.getAddress().getLandmark().trim());
//			} else {
//				serviceAddressMap.put("landmark", " ");
//			}
//
//			if (customerBranchDetails.getAddress().getLocality() != null) {
//				serviceAddressMap.put("locality", customerBranchDetails.getAddress().getLocality().trim());
//			} else {
//				serviceAddressMap.put("locality", " ");
//			}
//			serviceAddressMap.put("country", customerBranchDetails.getAddress().getCountry().trim());
//			serviceAddressMap.put("state", customerBranchDetails.getAddress().getState().trim());
//			serviceAddressMap.put("city", customerBranchDetails.getAddress().getCity().trim());
//			serviceAddressMap.put("pin", customerBranchDetails.getAddress().getPin() + "");
//			return true;
//		}
//
//		return false;
//	}

	private Customer checkCustomerNameExist(String customerId, long companyId) {
		// TODO Auto-generated method stub
		// Customer customer = ofy().load().type(Customer.class)
		// .filter("companyId", companyId)
		// .filter("refrNumber1", customerId.trim()).first().now();
		int customerid = Integer.parseInt(customerId);

		Customer customer = ofy().load().type(Customer.class).filter("companyId", companyId).filter("count", customerid)
				.first().now();
		if (customer != null) {
			logger.log(Level.SEVERE, "Customer Exist");
			return customer;
		} else {
			logger.log(Level.SEVERE, "Customer not Exist. customerid =" + customerid + "companyId=" + companyId);

		}
		// else {
		// logger.log(Level.SEVERE, "Inside else of Customer!!!!!!!!!!!!!!!");
		// if(isCompany.trim().equalsIgnoreCase("Yes")){
		// customer = ofy().load().type(Customer.class)
		// .filter("companyId", companyId)
		// .filter("companyName", companyName).first().now();
		// }else{
		// customer = ofy().load().type(Customer.class)
		// .filter("companyId", companyId)
		// .filter("fullname", name).first().now();
		// }
		//
		// if(customer==null){
		// if (createNewCustomer(customerId.trim())) {
		// customer = ofy().load().type(Customer.class)
		// .filter("companyId", companyId)
		// .filter("refrNumber1", customerId.trim()).first().now();
		// logger.log(Level.SEVERE, "Customer::::::::::" + customer);
		// return customer;
		// }
		// }else{
		// return checkExistingCustomer(customerId.trim(),customer);
		// }
		// }
		return null;
	}

	/**
	 * This is method is written to maps the cases of customer duplication and
	 * creation. This was created for NBHC They have different reference Id at
	 * different branches. Created By: Rahul Verma Creation Date: 07-11-2016
	 * 
	 * @param customerId
	 * @param customer
	 * @return
	 */
	// private Customer checkExistingCustomer(String customerId, Customer
	// customer) {
	// // TODO Auto-generated method stub
	// if(customer.getAdress().getAddrLine1().trim().equalsIgnoreCase(saddressLine1)&&customer.getAdress().getCity().trim().equalsIgnoreCase(scity)){
	// return customer;
	// }else{
	// if (createNewCustomer(customerId.trim())) {
	// customer = ofy().load().type(Customer.class)
	// .filter("companyId", companyId)
	// .filter("refrNumber1", customerId.trim()).first().now();
	// logger.log(Level.SEVERE, "Customer::::::::::" + customer);
	// return customer;
	// }
	// }
	// return null;
	// }
	//
	// private boolean createNewCustomer(String customerIdStr) {
	// // TODO Auto-generated method stub
	// logger.log(Level.SEVERE, "Creating new Customer");
	// Customer customer = new Customer();
	// if (isCompany.equalsIgnoreCase("Yes")) {
	// customer.setCompany(true);
	// customer.setCompanyName(companyName);
	// customer.setFullname(name);
	// } else {
	// customer.setFullname(name);
	// }
	// customer.setEmail(email);
	// try {
	// customer.setCellNumber1(Long.parseLong(cellNo.trim()));
	// } catch (Exception e) {
	// e.printStackTrace();
	// logger.log(Level.SEVERE, "ERROR cell No is not Long:::::" + e);
	// }
	//
	// customer.setBranch(servicingBranch);
	// customer.setEmployee("Batch Employee");
	// customer.setCategory(customerCategory);
	// customer.setType(customerType);
	// customer.setRefrNumber1(customerIdStr);
	// Address address = new Address();
	// address.setAddrLine1(addressLine1);
	// address.setCountry(country);
	// address.setState(state);
	// address.setCity(city);
	// try {
	// address.setPin(Long.parseLong(pin));
	// } catch (Exception e) {
	// e.printStackTrace();
	// logger.log(Level.SEVERE, "ERROR Primary Pin is not Long:::::" + e);
	// }
	// customer.setAdress(address);
	// customer.setStatus(Customer.CUSTOMERCREATED);
	// Address addressSec = new Address();
	// addressSec.setAddrLine1(saddressLine1);
	// addressSec.setCountry(scountry);
	// addressSec.setState(sstate);
	// addressSec.setCity(scity);
	// try {
	// addressSec.setPin(Long.parseLong(spin));
	// } catch (Exception e) {
	// e.printStackTrace();
	// logger.log(Level.SEVERE, "ERROR Secondary Pin is not Long:::::" + e);
	// }
	// customer.setSecondaryAdress(addressSec);
	// customer.setCompanyId(companyId);
	//
	// GenricServiceImpl impl = new GenricServiceImpl();
	// impl.save(customer);
	// ReturnFromServer server = new ReturnFromServer();
	// int count = server.count;
	// logger.log(Level.SEVERE, "count of customer:::::::" + count);
	// // if(count!=0){
	// // return true;
	// // }
	// return true;
	// }

	private boolean checkProductExist(Contract contract, int productId, String productName) {
		// TODO Auto-generated method stub

		for (int i = 0; i < contract.getItems().size(); i++) {
			logger.log(Level.SEVERE, contract.getItems().get(i).getPrduct().getCount() + "::==::" + productId);
			logger.log(Level.SEVERE, contract.getItems().get(i).getProductName() + "::==::" + productName);
			if ((contract.getItems().get(i).getPrduct().getCount() == productId)
					&& (contract.getItems().get(i).getProductName().trim().equalsIgnoreCase(productName.trim()))) {
//				productCode = contract.getItems().get(i).getProductCode().trim();
				return true;
			}
		}
		return false;
	}

	/***
	 * Date 26-04-2019 by Vijay Des :- If contract exist for the warehouse then
	 * updating stack details
	 * 
	 * @param contractRefId2
	 * 
	 * @param customerId2
	 * @param commodityName
	 * @param servicingBranch2
	 * @param customerBranch2
	 * @param wareHouseDetailsJArray2
	 * @param contractDate2
	 * @param listOfProductDeatails
	 * @param serviceAddress 
	 * @param companyId 
	 * @param clusterName 
	 * @param warehouseCode2
	 * @param wareHouseName2
	 */

	private void updateUniqueStackMasterCreateService(String customerId, String contractRefId2,
			List<Contract> contractlist, HttpServletRequest req, HttpServletResponse resp, String commodityName,
			String wareHouseName,String customerBranch2, String servicingBranch2, JSONArray wareHouseDetailsJArray2, String contractDate2,
			List<CreateServiceServletObjectClass> listOfProductDeatails, Address serviceAddress, long companyId, String clusterName) {
		logger.log(Level.SEVERE, "Updating Stack details for existing contracts");
		
		String sendResponse = null;

		 if (companyId == AUTHCODE) {

		List<ServiceProduct> serviceProductList = new ArrayList<ServiceProduct>();
		serviceProductList = checkProductExists(listOfProductDeatails);
		if (serviceProductList != null && serviceProductList.size() != 0
				&& serviceProductList.size() == listOfProductDeatails.size()) {

			/***
			 * @author Vijay Chougule Date 01-08-2019 Des :- Stack Master to
			 *         create services for every stack and maintain stack master
			 */

			ArrayList<StackMaster> stackMasterlist = new ArrayList<StackMaster>();
			
			for (int i = 0; i < wareHouseDetailsJArray2.length(); i++) {
				try {
					StackMaster serviceStackMaster = new StackMaster();
					JSONObject json = wareHouseDetailsJArray2.getJSONObject(i);
//					String warehouseName = json.optString("sWarehouseName");
					String warehousecode = json.optString("WHCode");
					String shade = json.optString("ShedName");
					String compartment = json.optString("sCompartmentName");
					String stackNo = json.optString("StackName");
					String quantity = json.optString("nNetWeight");
					String UOM = json.optString("Unit");
//					serviceStackMaster.setWarehouseName(warehouseName);
					serviceStackMaster.setWarehouseName(wareHouseName);
					serviceStackMaster.setWarehouseCode(warehousecode);
					serviceStackMaster.setShade(shade);
					serviceStackMaster.setCompartment(compartment);
					serviceStackMaster.setStackNo(stackNo);
					serviceStackMaster.setQuantity(Double.parseDouble(quantity));
					serviceStackMaster.setUOM(UOM);
					serviceStackMaster.setCompanyId(companyId);
					serviceStackMaster.setStatus(true);
					if(json.optString("commodityName")!=null){
						serviceStackMaster.setCommodityName(json.optString("commodityName"));
					}
					if(clusterName!=null){
						serviceStackMaster.setClusterName(clusterName);
					}
					
					stackMasterlist.add(serviceStackMaster);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			String whName = stackMasterlist.get(0).getWarehouseName();
			String whCode = stackMasterlist.get(0).getWarehouseCode();

			List<StackMaster> stackMasterStocklist = ofy().load().type(StackMaster.class)
					.filter("warehouseName", whName).filter("warehouseCode", whCode).filter("status", true).list();
			logger.log(Level.SEVERE, "stackMasterStocklist size" + stackMasterStocklist.size());
			ArrayList<StackMaster> newStackMasterlist = new ArrayList<StackMaster>();
			newStackMasterlist.addAll(stackMasterlist);
			logger.log(Level.SEVERE, "newStackMasterlist size" + newStackMasterlist.size());
			
			ArrayList<StackMaster> updatedStackMaster = new ArrayList<StackMaster>();
			for (StackMaster stackMaster : stackMasterlist) {
				for (StackMaster stackStockMaster : stackMasterStocklist) {
					if (stackStockMaster.isStatus() && stackMaster.getWarehouseCode().trim().equals(stackStockMaster.getWarehouseCode().trim())
							&& stackMaster.getShade().trim().equals(stackStockMaster.getShade().trim()) && stackMaster.getCompartment().trim().equals(stackStockMaster.getCompartment().trim())
							&& stackMaster.getStackNo().trim().equals(stackStockMaster.getStackNo().trim())) {
						stackStockMaster.setQuantity(stackMaster.getQuantity());
						updatedStackMaster.add(stackStockMaster);
						newStackMasterlist.remove(stackMaster); // this list to
																// create new
																// stack master
																// records for
																// new stacks
						logger.log(Level.SEVERE, "stackMaster stack no"+stackMaster.getStackNo());
						
						
					}
				}
			}

			ofy().save().entities(updatedStackMaster).now();
			logger.log(Level.SEVERE, "Stack Stock Master updated");
			logger.log(Level.SEVERE, "updatedStackMaster size" + updatedStackMaster.size());
			if (updatedStackMaster.size() != 0){
				sendResponse = updateServicesQty(updatedStackMaster, companyId);
				logger.log(Level.SEVERE, "Stack Qty Updation response"+sendResponse);
			}	
			int serviceCount = newStackMasterlist.size();
			/**
			 * @author Anil, Date : 28-04-2020
			 * new stack with zero quantity should not be created, issue raised by devesh and amit on nbhc side data is not updated therefore they request same service for cancellation but for us that was done and statck is inactive
			 */
			if(serviceCount>0) {
				if(newStackMasterlist.get(0).getQuantity()==0) {
					sendResponse="Can not create new stack with zero quantity.";
					try {
						resp.getWriter().println(sendResponse);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					logger.log(Level.SEVERE, "Can not create new stack with zero quantity. " + serviceCount);
					return;
				}
			}
			logger.log(Level.SEVERE, "No Of stack for Services" + serviceCount);
			if (serviceCount > 0) {
				try {
					/********************
					 * This is for produc table
					 ******************/
					List<SalesLineItem> listofSaleLineItem = new ArrayList<SalesLineItem>();
					int listIndex = -1;
					for (ServiceProduct serviceProductItem : serviceProductList) {
						listIndex = listIndex + 1;
						SalesLineItem lineItem = new SalesLineItem();
						lineItem.setPrduct(serviceProductItem);
						lineItem.getPrduct().setCount(serviceProductItem.getCount());
						lineItem.setProductCode(serviceProductItem.getProductCode());
						lineItem.setProductName(serviceProductItem.getProductName());
						SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
						sdf.setTimeZone(TimeZone.getTimeZone("IST"));
						TimeZone.setDefault(TimeZone.getTimeZone("IST"));
						try {
							lineItem.setStartDate(sdf.parse(contractDate2));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							logger.log(Level.SEVERE, "Error ::::::::::::::::" + e);
							e.printStackTrace();
						}
						// We are setting duration
						lineItem.setProductSrNo(listIndex + 1);
						lineItem.setNumberOfService(serviceCount); // number of
																	// services
						lineItem.setEndDate(contractlist.get(0).getEndDate());
						lineItem.setQuantity(1.0);
						ArrayList<BranchWiseScheduling> scheduleBranchlist = new ArrayList<BranchWiseScheduling>();
						BranchWiseScheduling custBranchSchedule = new BranchWiseScheduling();
						custBranchSchedule.setBranchName(customerBranch2);
						custBranchSchedule.setCheck(true);
						custBranchSchedule.setServicingBranch(servicingBranch2);
						custBranchSchedule.setArea(0);
						custBranchSchedule.setUnitOfMeasurement("");
						scheduleBranchlist.add(custBranchSchedule);
						HashMap<Integer, ArrayList<BranchWiseScheduling>> customerBranchlist = new HashMap<Integer, ArrayList<BranchWiseScheduling>>();
						customerBranchlist.put(lineItem.getProductSrNo(), scheduleBranchlist);

						lineItem.setCustomerBranchSchedulingInfo(customerBranchlist);

						listofSaleLineItem.add(lineItem);
					}
					logger.log(Level.SEVERE, "listofSaleLineItem.size()::::::::::::" + listofSaleLineItem.size());

					/**
					 * Service Schedule Pop Up
					 */
					List<ServiceSchedule> serviceScheduleList = new ArrayList<ServiceSchedule>();

					for (int i = 0; i < listofSaleLineItem.size(); i++) {
						for (int j = 0; j < listofSaleLineItem.get(i).getNumberOfServices()
								* listofSaleLineItem.get(i).getQty(); j++) {
							ServiceSchedule serviceSchedule = new ServiceSchedule();
							serviceSchedule.setSerSrNo(listofSaleLineItem.get(i).getProductSrNo());
							serviceSchedule.setScheduleProdId(listofSaleLineItem.get(i).getPrduct().getCount());
							serviceSchedule.setScheduleProQty((int) listofSaleLineItem.get(i).getQty());
							serviceSchedule.setScheduleProdName(listofSaleLineItem.get(i).getPrduct().getProductName());
							serviceSchedule.setScheduleDuration(listofSaleLineItem.get(i).getDuration());
							serviceSchedule.setScheduleNoOfServices(listofSaleLineItem.get(i).getNumberOfServices());
							serviceSchedule.setScheduleServiceNo(j + 1);
							serviceSchedule.setScheduleProdStartDate(listofSaleLineItem.get(i).getStartDate());
							serviceSchedule.setScheduleStartDate(listofSaleLineItem.get(i).getStartDate());
							SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
							sdf.setTimeZone(TimeZone.getTimeZone("IST"));
							TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//							Date dateProdEnd = sdf.parse(sdf.format(listofSaleLineItem.get(i).getStartDate()));
//							Calendar c2 = Calendar.getInstance();
//							try {
//								c2.setTime(dateProdEnd);
//								c2.add(Calendar.DATE, listofSaleLineItem.get(i).getDuration());
//							} catch (Exception e1) {
//								// TODO Auto-generated catch block
//								e1.printStackTrace();
//							}
//							serviceSchedule.setScheduleProdEndDate(c2.getTime());
							serviceSchedule.setScheduleProdEndDate(contractlist.get(0).getEndDate());
							/**
							 * Date 09-11-2019 By Vijay
							 * Des :- As disscussed with Vaishali Mam and Nitin Sir
							 * Deggassing Service will create when they complete the Fumigation Service based on Completion Date
							 */
//							/**
//							 * Date 08-11-2019 By Vijay
//							 * Des :- For Degassing Service Service Date must be as T+52
//							 */
//							if(listofSaleLineItem.get(i).getProductCode().trim().equals("DSSG")){
//								Date tPlus45DaysDate = sdf.parse(contractDate2);
//								Calendar c4 = Calendar.getInstance();
//								c4.setTime(tPlus45DaysDate);
//								c4.add(Calendar.DATE, +52);
//								logger.log(Level.SEVERE, "Date after 52 Days===" + tPlus45DaysDate);
//								tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
//								logger.log(Level.SEVERE, "Date after 52 Days" + tPlus45DaysDate);
//								SimpleDateFormat sdfDateP = new SimpleDateFormat("dd/MM/yyyy");
//								sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
//								TimeZone.setDefault(TimeZone.getTimeZone("IST"));
//								serviceSchedule.setScheduleServiceDate(tPlus45DaysDate);
//								
//							}else{
								/***
								 * Date 27-04-2019 by Vijay for Services must be T +
								 * 45
								 ****/
								Date tPlus45DaysDate = sdf.parse(contractDate2);
								Calendar c4 = Calendar.getInstance();
								c4.setTime(tPlus45DaysDate);
								c4.add(Calendar.DATE, +45);
								logger.log(Level.SEVERE, "Date after 45 Days===" + tPlus45DaysDate);
								tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
								logger.log(Level.SEVERE, "Date after 45 Days" + tPlus45DaysDate);
								SimpleDateFormat sdfDateP = new SimpleDateFormat("dd/MM/yyyy");
								sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
								TimeZone.setDefault(TimeZone.getTimeZone("IST"));
								serviceSchedule.setScheduleServiceDate(tPlus45DaysDate);
//							}
							serviceSchedule.setScheduleServiceTime("Flexible");
							serviceSchedule.setTotalServicingTime(0.0);
							serviceSchedule.setScheduleProBranch(customerBranch2);
							serviceSchedule.setServicingBranch(servicingBranch2);
							serviceSchedule.setWarehouseName(newStackMasterlist.get(j).getWarehouseName());
							serviceSchedule.setWarehouseCode(newStackMasterlist.get(j).getWarehouseCode());
							serviceSchedule.setShade(newStackMasterlist.get(j).getShade());
							serviceSchedule.setCompartment(newStackMasterlist.get(j).getCompartment());
							serviceSchedule.setStackNo(newStackMasterlist.get(j).getStackNo());
							serviceSchedule.setStackQty(newStackMasterlist.get(j).getQuantity());
							if(newStackMasterlist.get(j).getCommodityName()!=null)
							serviceSchedule.setCommodityName(newStackMasterlist.get(j).getCommodityName());
							if(newStackMasterlist.get(j).getClusterName()!=null){
								serviceSchedule.setClusterName(newStackMasterlist.get(j).getClusterName());
							}
							
							serviceScheduleList.add(serviceSchedule);
						}
					}
					SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
					sdfDate.setTimeZone(TimeZone.getTimeZone("IST"));
					TimeZone.setDefault(TimeZone.getTimeZone("IST"));

					Calendar c3 = Calendar.getInstance();
					c3.set(9999, Calendar.DECEMBER, 31);
					Date date = c3.getTime();
					Date endDate = null;
					try {
						endDate = sdfDate.parse(sdfDate.format(date));
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					/** Date 28-12-2019 by Vijay storing fumigationID and Prophylactic Id ***/
					 int totalservices = serviceScheduleList.size();
						SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
//						int fumigationCount=(int) numGen.getLastUpdatedNumber("Service", companyId, (long) totalservices);
						int fumigationCount=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, companyId, (long) totalservices);
						int prophylacticCount = fumigationCount + (totalservices/listofSaleLineItem.size());
						int fumigationId = fumigationCount;
						int prophylacticId = prophylacticCount;
						
						logger.log(Level.SEVERE, "fumigationCount"+fumigationCount);
						logger.log(Level.SEVERE, "prophylacticCount"+prophylacticCount);
						logger.log(Level.SEVERE, "fumigationId"+fumigationId);
						logger.log(Level.SEVERE, "prophylacticId"+prophylacticId);

					for (int i = 0; i < listofSaleLineItem.size(); i++) {
						for (int j = 0; j < serviceScheduleList.size(); j++) {

							if (listofSaleLineItem.get(i).getProductSrNo() == serviceScheduleList.get(j).getSerSrNo()) {

								Service temp = new Service();
								temp.setRefNo(contractRefId2); // inward
																// transaction
																// id
								temp.setPersonInfo(contractlist.get(0).getCinfo());
								temp.setContractCount(contractlist.get(0).getCount());
								temp.setProduct((ServiceProduct) listofSaleLineItem.get(i).getPrduct());
								/** Date 28-12-2019 by Vijay storing fumigationID and Prophylactic Id ***/
								if(temp.getProduct().getProductCode().equals("STK-01")){
									temp.setCount(fumigationCount++);
									temp.setProphylacticServiceId(prophylacticCount++);
									logger.log(Level.SEVERE, "Fumigation Service ID = "+temp.getCount());
									logger.log(Level.SEVERE, "prophylactic Service ID = "+temp.getProphylacticServiceId());
								}
								else{
									temp.setCount(prophylacticId++);
									temp.setFumigationServiceId(fumigationId++);
									logger.log(Level.SEVERE, "prophylactic Service ID"+temp.getCount());
									logger.log(Level.SEVERE, "Fumigation Service ID"+temp.getFumigationServiceId());
								}
								
								temp.setBranch(contractlist.get(0).getBranch());
								temp.setStatus(Service.SERVICESTATUSSCHEDULE);
								temp.setContractStartDate(contractlist.get(0).getStartDate());
								temp.setContractEndDate(endDate);
								temp.setAdHocService(false);
								temp.setServiceIndexNo(i + 1);
								temp.setServiceType("Periodic");

								temp.setServiceSerialNo(serviceScheduleList.get(j).getScheduleServiceNo());
								temp.setServiceDate(serviceScheduleList.get(j).getScheduleServiceDate());
								temp.setOldServiceDate(serviceScheduleList.get(j).getScheduleServiceDate());
								if(serviceAddress!=null)
								temp.setAddress(serviceAddress);

								temp.setServiceTime(serviceScheduleList.get(j).getScheduleServiceTime());
								temp.setServiceBranch(serviceScheduleList.get(j).getScheduleProBranch());
								temp.setServiceDateDay(serviceScheduleList.get(j).getScheduleServiceDay());

								temp.setBranch(serviceScheduleList.get(j).getServicingBranch());
								temp.setServicingTime(serviceScheduleList.get(j).getTotalServicingTime());

								temp.setServiceSrNo(listofSaleLineItem.get(i).getProductSrNo());
								temp.setCompanyId(AUTHCODE);
								/**
								 * Date 01-08-2019 by Vijay for NBHC CCPM
								 * Fumigation Services for every Stack This code
								 * only executed from Web Services of NBHC
								 * Fumigation service
								 */
								temp.setWareHouse(serviceScheduleList.get(j).getWarehouseName());
								temp.setRefNo2(serviceScheduleList.get(j).getWarehouseCode());
								temp.setShade(serviceScheduleList.get(j).getShade());
								temp.setCompartment(serviceScheduleList.get(j).getCompartment());
								temp.setStackNo(serviceScheduleList.get(j).getStackNo());
								if(serviceScheduleList.get(j).getCommodityName()!=null)
								temp.setCommodityName(serviceScheduleList.get(j).getCommodityName());
								temp.setStackQty(serviceScheduleList.get(j).getStackQty());

								SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
								sdf.setTimeZone(TimeZone.getTimeZone("IST"));
								TimeZone.setDefault(TimeZone.getTimeZone("IST"));
								Date depositeDate = sdf.parse(contractDate2);

								temp.setDepositeDate(depositeDate);
								
								SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
								String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (temp.getServiceDate())));
								temp.setServiceDay(serviceDay);
								temp.setClusterName(serviceScheduleList.get(j).getClusterName());
								
								 temp.setWmsServiceFlag(true);
								 temp.setCompanyId(companyId);
								 ofy().save().entity(temp);
							 	/**
								 * @author Vijay Chougule Date 21-12-2019
								 * Project :- NBHC CCPM fumigation Tracker
								 * Des :- for Report data saving in another entity
								 */
								 if(temp.getProduct().getProductCode().equals("STK-01")){
									String returnmark = CreateFumigationReport(temp,-1);
									logger.log(Level.SEVERE,"remark =="+returnmark);
								 }
								sendResponse = "Services created successfully";
								 
							}
						}

					}
					
					if (newStackMasterlist.size() != 0) {
						CreateStockMaster(newStackMasterlist);
					} else {
						sendResponse = "Stack Updated successfully";
						logger.log(Level.SEVERE, "Stack Updated successfully");
					}

				} catch (Exception e) {
					logger.log(Level.SEVERE, "ERROR in Method:::::::::::" + e);
					e.printStackTrace();
					sendResponse = e.getLocalizedMessage();
				}
			} else {
				sendResponse = "Existing Stack Updated successfully";
				logger.log(Level.SEVERE, "Existing Stack Updated successfully");

			}
		} else {
			sendResponse = "Product does not exist";
		}

		try {
			resp.getWriter().println(sendResponse);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 }
	}

	/**
	 * @author Vijay Chougule Des :- NBHC CCPM Stack Master for every
	 */

	private void CreateStockMaster(ArrayList<StackMaster> stackMasterlist) {
		ArrayList<SuperModel> modellist = new ArrayList<SuperModel>();
		modellist.addAll(stackMasterlist);

		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(modellist);

		logger.log(Level.SEVERE, "Stack Master Created successfully");
	}

	/**
	 * Date 09-08-2019 by Vijay for Outward stack stock master update and
	 * services qty update
	 * 
	 * @param req
	 * @param resp
	 * @param companyId 
	 */
//	private void updateStackStockAndServices(HttpServletRequest req, HttpServletResponse resp, long companyId) {
//		// TODO Auto-generated method stub
//		logger.log(Level.SEVERE, "For Outward Stack stock update");
//		JSONArray wareHouseDetailsJArray = new JSONArray();
//		String jsonString = req.getParameter("serviceDetails").trim();
//		logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);
//		JSONObject jObj = null;
//		try {
//			jObj = new JSONObject(jsonString);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		String contractRefId = jObj.optString("contractId").trim();
//		logger.log(Level.SEVERE, "contractId" + contractRefId);
//
//		logger.log(Level.SEVERE, "customerId" + customerId);
////		productCode = jObj.optString("productCode").trim();
////		logger.log(Level.SEVERE, "productCode" + productCode);
//		String contractDate = jObj.optString("contractDate").trim();
//		logger.log(Level.SEVERE, "contractDate" + contractDate);
//		String serviceTime = jObj.optString("serviceTime").trim();
//		logger.log(Level.SEVERE, "serviceTime" + serviceTime);
//		String servicingBranch = jObj.optString("servicingBranch").trim();
//		logger.log(Level.SEVERE, "servicingBranch" + servicingBranch);
//		String customerBranch = jObj.optString("customerBranch").trim();
//		JSONArray prooductDetailsArray = new JSONArray();
//		try {
//			prooductDetailsArray = jObj.getJSONArray("productDetails");
//		} catch (JSONException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		logger.log(Level.SEVERE, "customerBranch" + customerBranch);
//		String serviceEngineer = jObj.optString("serviceEngineer").trim();
//		logger.log(Level.SEVERE, "serviceEngineer" + serviceEngineer);
//		String wareHouse = jObj.optString("wareHouse").trim();
//		logger.log(Level.SEVERE, "wareHosue" + wareHouse);
//
//		/*** Date 01-05-2019 by Vijay for commodity Name mapping ****/
//
//		String commodityName = jObj.optString("Commodity").trim();
//		//
//		int contractPeriod = 0;
//		
//		List<CreateServiceServletObjectClass> listOfProductDeatails = new ArrayList<CreateServiceServletObjectClass>();
//		for (int i = 0; i < prooductDetailsArray.length(); i++) {
//			JSONObject jpObj;
//			try {
//				jpObj = prooductDetailsArray.getJSONObject(i);
//				if (!jpObj.optString("durationInDays").trim().equalsIgnoreCase("na")) {
//					if (contractPeriod < Integer.parseInt(jpObj.optString("durationInDays").trim())) {
//						contractPeriod = Integer.parseInt(jpObj.optString("durationInDays").trim());
//					}
//				} else {
//					contractPeriod = 365;
//				}
//				String productCode = jpObj.optString("productCode");
//				int noOfService;
//				if (!jpObj.optString("noOfService").trim().equalsIgnoreCase("NA")) {
//					noOfService = Integer.parseInt(jpObj.optString("noOfService").trim());
//				} else {
//					contractPeriod = 365;
//					noOfService = 0;
//				}
//				int duration;
//				if (!jpObj.optString("durationInDays").trim().equalsIgnoreCase("NA")) {
//					duration = Integer.parseInt(jpObj.optString("durationInDays").trim());
//
//				} else {
//					contractPeriod = 365;
//					duration = 0;
//				}
//				CreateServiceServletObjectClass productDetails = new CreateServiceServletObjectClass(productCode,
//						noOfService, duration);
//				listOfProductDeatails.add(i, productDetails);
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//		}
//		wareHouseDetailsJArray = jObj.optJSONArray("wareHouseDetails");
//
//		String wareHouseName = null;
//		String warehouseCode = null;
//		for (int i = 0; i < wareHouseDetailsJArray.length(); i++) {
//			try {
//				JSONObject json = wareHouseDetailsJArray.getJSONObject(i);
//				wareHouseName = json.optString("sWarehouseName");
//				warehouseCode = json.optString("WHCode");
//
//			} catch (JSONException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//
//		/*** Date 06-08-2019 by Vijay for warehouse name flag ***/
//		String sepeartor = jObj.optString("WHName_AND").trim();
//		logger.log(Level.SEVERE, "sepeartor " + sepeartor);
//		if (sepeartor.equals("1")) {
//			String actualWarehouseName = wareHouseName.replace("AND", "&");
//			wareHouseName = actualWarehouseName;
//		}
//		logger.log(Level.SEVERE, "warehouseName after seperator=" + wareHouseName);
//		logger.log(Level.SEVERE, "companyId =" + companyId);
//		logger.log(Level.SEVERE, "warehouseCode =" + warehouseCode);
//		logger.log(Level.SEVERE, "warehouseName =" + wareHouseName);
//		/**
//		 * Ends here
//		 */
//
//		logger.log(Level.SEVERE, "Updating Stack details for existing contracts");
////		String[] urlArray = url.split("\\.");
////
////		String accessUrl = urlArray[0];
////		logger.log(Level.SEVERE, "accessUrl" + accessUrl);
////		String applicationId = urlArray[1];
////		logger.log(Level.SEVERE, "applicationId" + applicationId);
////		Company company = ofy().load().type(Company.class).filter("accessUrl", accessUrl.trim()).first().now();
////		String sendResponse = null;
//
//		if (companyId == AUTHCODE) {
//
////			logger.log(Level.SEVERE, "Product Code before sending:::::::" + productCode);
//			List<ServiceProduct> serviceProductList = new ArrayList<ServiceProduct>();
//			serviceProductList = checkProductExists(listOfProductDeatails);
//			if (serviceProductList != null && serviceProductList.size() != 0
//					&& serviceProductList.size() == listOfProductDeatails.size()) {
//
//			}
//
//			/***
//			 * @author Vijay Chougule Date 01-08-2019 Des :- Stack Master to
//			 *         create services for every stack and maintain stack master
//			 */
//
//			ArrayList<StackMaster> stackMasterlist = new ArrayList<StackMaster>();
//
//			for (int i = 0; i < wareHouseDetailsJArray.length(); i++) {
//				try {
//					StackMaster serviceStackMaster = new StackMaster();
//					JSONObject json = wareHouseDetailsJArray.getJSONObject(i);
//					String warehouseName = json.optString("sWarehouseName");
//					String warehousecode = json.optString("WHCode");
//					String shade = json.optString("ShedName");
//					String compartment = json.optString("sCompartmentName");
//					String stackNo = json.optString("StackName");
//					String quantity = json.optString("nNetWeight");
//					String UOM = json.optString("Unit");
//					serviceStackMaster.setWarehouseName(warehouseName);
//					serviceStackMaster.setWarehouseCode(warehousecode);
//					serviceStackMaster.setShade(shade);
//					serviceStackMaster.setCompartment(compartment);
//					serviceStackMaster.setStackNo(stackNo);
//					serviceStackMaster.setQuantity(Double.parseDouble(quantity));
//					serviceStackMaster.setUOM(UOM);
//					serviceStackMaster.setCompanyId(companyId);
//					stackMasterlist.add(serviceStackMaster);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//
//			String whName = stackMasterlist.get(0).getWarehouseName();
//			String whCode = stackMasterlist.get(0).getWarehouseCode();
//			String shade = stackMasterlist.get(0).getShade();
//
//			List<StackMaster> stackMasterStocklist = ofy().load().type(StackMaster.class)
//					.filter("warehouseName", whName).filter("warehouseCode", whCode).filter("shade", shade).list();
//			logger.log(Level.SEVERE, "stackMasterStocklist size" + stackMasterStocklist.size());
//
//			ArrayList<StackMaster> updatedStackMaster = new ArrayList<StackMaster>();
//			for (StackMaster stackMaster : stackMasterlist) {
//				for (StackMaster stackStockMaster : stackMasterStocklist) {
//					if (stackMaster.getWarehouseCode().trim().equals(stackStockMaster.getWarehouseCode().trim())
//							&& stackMaster.getShade().trim().equals(stackStockMaster.getShade().trim())
//							&& stackMaster.getStackNo().trim().equals(stackStockMaster.getStackNo().trim())) {
//						stackStockMaster.setQuantity(stackMaster.getQuantity());
//						updatedStackMaster.add(stackStockMaster);
//					}
//				}
//			}
//			ofy().save().entities(updatedStackMaster).now();
//			logger.log(Level.SEVERE, "Stack Stock Master updated");
//			logger.log(Level.SEVERE, "updatedStackMaster size" + updatedStackMaster.size());
//			if (updatedStackMaster.size() != 0) {
//				updateServicesQty(updatedStackMaster, companyId);
//			}
//		}
//
//	}

	private String updateServicesQty(ArrayList<StackMaster> updatedStackMaster, long companyId2) {
		String response = "";
		
		ArrayList<StackMaster> stackQtyZeroList = new ArrayList<StackMaster>();
		String warehouseCode = updatedStackMaster.get(0).getWarehouseCode();

		HashSet<String> hscompartment = new HashSet<String>();
		HashSet<String> hsStackNo = new HashSet<String>();
		HashSet<String> hsShadeName = new HashSet<String>();
		
		for(StackMaster stackmaster : updatedStackMaster){
			hscompartment.add(stackmaster.getCompartment());
			hsStackNo.add(stackmaster.getStackNo());
			hsShadeName.add(stackmaster.getShade());
		}
		
		ArrayList<String> compartmentlist = new ArrayList<String>(hscompartment);
		ArrayList<String> stackNolist = new ArrayList<String>(hsStackNo);
		ArrayList<String> shadeNamelist = new ArrayList<String>(hsShadeName);

		ArrayList<String> servicestatuslist = new ArrayList<String>();
		servicestatuslist.add(Service.SERVICESTATUSRESCHEDULE);
		servicestatuslist.add(Service.SERVICESTATUSSCHEDULE);

//		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId2)
//				.filter("refNo2", warehouseCode)
//				.filter("status IN",servicestatuslist).list();
		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId2)
				.filter("refNo2", warehouseCode).filter("shade IN", shadeNamelist).filter("compartment IN", compartmentlist).filter("stackNo IN", stackNolist)
				.filter("status IN",servicestatuslist).list();
		logger.log(Level.SEVERE, "servicelist size " + servicelist.size());
		if (servicelist.size() != 0) {
			for (Service service : servicelist) {
				for (StackMaster stackmaster : updatedStackMaster) {
					if (service.getWareHouse().trim().equals(stackmaster.getWarehouseName().trim())
							&& service.getRefNo2().trim().equals(stackmaster.getWarehouseCode().trim())
							&& service.getShade().trim().equals(stackmaster.getShade().trim())
							&& service.getCompartment().trim().equals(stackmaster.getCompartment().trim())
							&& service.getStackNo().trim().equals(stackmaster.getStackNo().trim())) {
						if(stackmaster.getQuantity()==0){
							service.setStatus(Service.SERVICESTATUSCANCELLED);
							service.setActionBy("System");
							service.setActionRemark("Stack Qty zero");
							service.setStackQty(stackmaster.getQuantity());
							service.setCancellationDate(DateUtility.getDateWithTimeZone("IST", new Date()));
							stackQtyZeroList.add(stackmaster);
							if(service.getProduct().getProductCode()!=null && service.getProduct().getProductCode().equals("STK-01")){
								updateServiceCancelledInFumigationReport(service);
							}
						}
						service.setStackQty(stackmaster.getQuantity());
					}
				}
			}
			ofy().save().entities(servicelist).now();
			logger.log(Level.SEVERE, "Service stack qty updated sucessfully!");
			for(StackMaster stackQtyZeromaster : stackQtyZeroList){
				stackQtyZeromaster.setStatus(false);
			}
			if(stackQtyZeroList.size()!=0){
			ofy().save().entities(stackQtyZeroList);
			logger.log(Level.SEVERE, "Stack Qty Zero Services stack master updated status to false");
			response="Zero Stack Qty Services Cancelled Sucessfull";
			}
			/**
			 * @author Vijay Chougule
			 * Date :- 01-07-2020
			 * Des :- Updating degassing cancelled status in fumigation service entity
			 */
			for(Service serviceEntity : servicelist){
				if(serviceEntity.getProduct().getProductCode()!=null && serviceEntity.getProduct().getProductCode().equals("DSSG")
						&& serviceEntity.getStatus().equals(Service.CANCELLED)){
					Service fumigationService = ofy().load().type(Service.class).filter("companyId", serviceEntity.getCompanyId())
							.filter("count", serviceEntity.getFumigationServiceId()).first().now();
					if(fumigationService!=null){
						fumigationService.setDegassingServiceStatus(Service.CANCELLED);
						ofy().save().entity(fumigationService);
						logger.log(Level.SEVERE, "Deggssing service cancelled status updated in fumigation Service entity");
					}
				}
			}
			
		}
		return response;

	}

	

	

	private void reactonSuspend(HttpServletRequest req, HttpServletResponse resp, JSONObject jObj, long companyId2) {

		SimpleDateFormat sdfDateP = new SimpleDateFormat("dd/MM/yyyy");
		sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		JSONArray serviceDataArray = new JSONArray();
		try {
			serviceDataArray = jObj.getJSONArray("ServiceData");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (int i = 0; i < serviceDataArray.length(); i++) {
			try {

				JSONObject jsonboject = serviceDataArray.getJSONObject(i);

				int serviceid = 0;
				String warehouseCode = "";
				String shade = "";
				String userName = "";
				String remark = "";
				int contractId=0;
				serviceid = jsonboject.getInt("ServiceID");
				warehouseCode = jsonboject.getString("WHCode");
				shade = jsonboject.getString("ShedName");
				userName = jsonboject.getString("UserName");
				remark = jsonboject.getString("Remaks");
				contractId = jsonboject.getInt("ContractId");
				logger.log(Level.SEVERE, "serviceid ==" + serviceid);
				logger.log(Level.SEVERE, "contractId ==" + contractId);


				if (serviceid != 0) {
					Service serviceEntity = ofy().load().type(Service.class).filter("companyId", companyId2).filter("wmsServiceFlag", true)
							.filter("count", serviceid).filter("contractCount", contractId).filter("refNo2", warehouseCode).filter("shade", shade).first()
							.now();
					logger.log(Level.SEVERE, "serviceEntity for suspend" + serviceEntity);
					if (serviceEntity != null) {
						serviceEntity.setStatus(Service.SERVICESUSPENDED);
						serviceEntity.setActionRemark(remark);
						serviceEntity.setFumigationStatusDate(DateUtility.getDateWithTimeZone("IST", new Date()));
						serviceEntity.setActionBy(userName);
						ofy().save().entity(serviceEntity);
						logger.log(Level.SEVERE, "Service SERVICESUSPENDED successfully");
						resp.getWriter().println("Service Suspended Successfully");
						
						if(serviceEntity.getProduct().getProductCode().equals("STK-01")){
							ArrayList<String> servicestatuslist = new ArrayList<String>();
							servicestatuslist.add(Service.SERVICESTATUSRESCHEDULE);
							servicestatuslist.add(Service.SERVICESTATUSSCHEDULE);
							
						Service prophylacticService = ofy().load().type(Service.class).filter("count", serviceEntity.getProphylacticServiceId()).filter("wmsServiceFlag", true)
														.filter("companyId", serviceEntity.getCompanyId()).filter("status IN", servicestatuslist).first().now(); //
						logger.log(Level.SEVERE, "prophylacticService"+prophylacticService);
						if(prophylacticService!=null){
							logger.log(Level.SEVERE, "prophylacticService"+prophylacticService.getStatus());

							if(prophylacticService.getStatus().equals(Service.SERVICESTATUSSCHEDULE) 
									|| prophylacticService.getStatus().equals(Service.SERVICESTATUSRESCHEDULE)){
								prophylacticService.setStatus(Service.SERVICESUSPENDED);
								prophylacticService.setActionRemark(remark);
								prophylacticService.setFumigationStatusDate(DateUtility.getDateWithTimeZone("IST", new Date()));
								prophylacticService.setActionBy(userName);
								ofy().save().entity(prophylacticService);
								logger.log(Level.SEVERE, "Prophylactic Service SERVICESUSPENDED successfully");
							}
						}
						String message = createFumigationAndProphalacticSuspendService(serviceEntity);
						logger.log(Level.SEVERE, "new service reponse"+message);
						}
						
						updateServiceSuspendStatusInFumigationReport(serviceEntity);
						
					} else {
						resp.getWriter().println("Serivce id not found!");
					}

				} else {
					resp.getWriter().println("Serivce id can not be zero!");

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	

	

	private void reactonReschedule(HttpServletRequest req, HttpServletResponse resp, JSONObject jObj, long companyId2) {

		SimpleDateFormat sdfDateP = new SimpleDateFormat("dd/MM/yyyy");
		sdfDateP.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		JSONArray serviceDataArray = new JSONArray();
		try {
			serviceDataArray = jObj.getJSONArray("ServiceData");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		logger.log(Level.SEVERE, "serviceDataArray length"+serviceDataArray.length() );
		for (int i = 0; i < serviceDataArray.length(); i++) {
			try {

				JSONObject jsonboject = serviceDataArray.getJSONObject(i);

				int serviceid = 0;
				String warehouseCode = "";
				String shade = "";
				String userName = "";
				String remark = "";
				String strrescheduleDate = null;
				int contractId=0;

				serviceid = jsonboject.getInt("ServiceID");
				warehouseCode = jsonboject.getString("WHCode");
				shade = jsonboject.getString("ShedName");
				strrescheduleDate = jsonboject.getString("ServiceDate");
				userName = jsonboject.getString("UserName");
				remark = jsonboject.getString("Remaks");
				contractId = jsonboject.getInt("ContractId");
				
				Date rescheduleDate = sdfDateP.parse(strrescheduleDate);
				logger.log(Level.SEVERE, "rescheduleDate " + rescheduleDate);
				logger.log(Level.SEVERE, "Service Id"+serviceid);
				logger.log(Level.SEVERE, "contractId Id"+contractId);
				if (serviceid !=0) {
					Service serviceEntity = ofy().load().type(Service.class).filter("companyId", companyId2).filter("wmsServiceFlag", true)
							.filter("count", serviceid).filter("contractCount", contractId).filter("refNo2", warehouseCode).filter("shade", shade).first()
							.now();
					logger.log(Level.SEVERE, "serviceEntity for Reschedule" + serviceEntity);
					if (serviceEntity != null) {
						/**
						 * @author Anil , Date : 13-04-2020
						 * validating suspended services from being reschedule
						 */
						if(serviceEntity.getStatus().equals(Service.SERVICESUSPENDED)){
							resp.getWriter().println("Can not reschedule suspended service");
							return;
						}
						if(!serviceEntity.getStatus().equals(Service.SERVICESTATUSCOMPLETED)){
							serviceEntity.setStatus(Service.SERVICESTATUSRESCHEDULE);
							ModificationHistory history = new ModificationHistory();
							history.setOldServiceDate(serviceEntity.getServiceDate());
							serviceEntity.setServiceDate(rescheduleDate);
							history.setResheduleDate(rescheduleDate);
							history.setResheduleBranch(serviceEntity.getBranch());
							history.setReason(remark);
							history.setUserName(userName);
							history.setSystemDate(DateUtility.getDateWithTimeZone("IST", new Date()));
							serviceEntity.getListHistory().add(history);
							/** Date 07-01-2019 for In EVA ERP stack can be reschedule upto reschedule + 5 days only **/
							if(serviceEntity.getProduct().getProductCode().trim().equals("STK-01")){
								serviceEntity.setCioRescheduleDate(rescheduleDate);
							}
							ofy().save().entity(serviceEntity);
							logger.log(Level.SEVERE, "Service Rescheduled successfully");
							resp.getWriter().println("Service Rescheduled successfully");
						}
						else{
							logger.log(Level.SEVERE, "Service status is completed");
							resp.getWriter().println("Can not reschedule completed service");
						}
						
					} else {
						logger.log(Level.SEVERE, "Serivce id not found!");
						resp.getWriter().println("Serivce id not found!");
					}

				} else {
					logger.log(Level.SEVERE, "Serivce id can not be zero!");
					resp.getWriter().println("Serivce id can not be zero!");

				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE, "Exception ="+e);
			}
		}

	}

	private void reactonServiceSuccess(HttpServletRequest req, HttpServletResponse resp, JSONObject jObj,
			long companyId2) {

		

		JSONArray serviceDataArray = new JSONArray();
		try {
			serviceDataArray = jObj.getJSONArray("ServiceData");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		for (int i = 0; i < serviceDataArray.length(); i++) {
			try {
				JSONObject jsonobject = serviceDataArray.getJSONObject(i);

				int serviceID = 0;
				String warehouseCode = "";
				String shade = "";
				int contractId = 0;

				serviceID = jsonobject.getInt("ServiceID");
				warehouseCode = jsonobject.getString("WHCode");
				shade = jsonobject.getString("ShedName");
				
				String userName = "";
				String remark = "";
				userName = jsonobject.getString("UserName");
				remark = jsonobject.getString("Remaks");
				contractId = jsonobject.getInt("ContractId");
				
				logger.log(Level.SEVERE, "serviceID" + serviceID);
				logger.log(Level.SEVERE, "contractId" + contractId);

				if (serviceID != 0) {
					Service serviceEntity = ofy().load().type(Service.class).filter("companyId", companyId2).filter("wmsServiceFlag", true)
							.filter("count", serviceID).filter("contractCount", contractId).filter("refNo2", warehouseCode).filter("shade", shade).first()
							.now();
					logger.log(Level.SEVERE, "fumigation Service for Success" + serviceEntity);

					if (serviceEntity != null) {
						String response = createFumigationAndProphalacticService(serviceEntity,userName,remark,false);

						resp.getWriter().println(response);

					} else {
						resp.getWriter().println("Serivce id not found!");
					}
				} else {
					resp.getWriter().println("Serivce id can not be zero!");
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.log(Level.SEVERE, "Exception while success" + e);

			}
		}

	}

	
	private void reactonServiceFailure(HttpServletRequest req, HttpServletResponse resp, JSONObject jObj,
			long companyId2) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));

		JSONArray serviceDataArray = new JSONArray();
		try {
			serviceDataArray = jObj.getJSONArray("ServiceData");

			for (int i = 0; i < serviceDataArray.length(); i++) {
				JSONObject jsonobject = serviceDataArray.getJSONObject(i);

				int serviceID = 0;
				String warehouseCode = "";
				String shade = "";
				int contractId = 0;
				
				serviceID = jsonobject.getInt("ServiceID");
				warehouseCode = jsonobject.getString("WHCode");
				shade = jsonobject.getString("ShedName");
				String userName = "";
				String remark = "";
				userName = jsonobject.getString("UserName");
				remark = jsonobject.getString("Remaks");
				contractId = jsonobject.getInt("ContractId");

				if (serviceID != 0) {
					Service serviceEntity = ofy().load().type(Service.class).filter("companyId", companyId2).filter("wmsServiceFlag", true)
							.filter("count", serviceID).filter("contractCount", contractId).filter("refNo2", warehouseCode).filter("shade", shade).first()
							.now();
					logger.log(Level.SEVERE, "serviceEntity for failure" + serviceEntity);

				
					if (serviceEntity != null) {

						Service temp = new Service();
						temp.setRefNo(serviceEntity.getRefNo()); // inward
																	// transaction
																	// id
						temp.setPersonInfo(serviceEntity.getPersonInfo());
						temp.setContractCount(serviceEntity.getContractCount());
						temp.setProduct((ServiceProduct) serviceEntity.getProduct());
						temp.setBranch(serviceEntity.getBranch());
						temp.setStatus(Service.SERVICESTATUSSCHEDULE);
						temp.setContractStartDate(serviceEntity.getContractStartDate());
						temp.setContractEndDate(serviceEntity.getContractEndDate());
						temp.setAdHocService(false);
						temp.setServiceIndexNo(1);
						temp.setServiceType("Periodic");

						temp.setServiceSerialNo(serviceEntity.getServiceSerialNo() + 1);

//						Date tPlus1DaysDate = serviceEntity.getServiceDate();
//						Calendar c4 = Calendar.getInstance();
//						c4.setTime(tPlus1DaysDate);
//						c4.add(Calendar.DATE, +1);
//						logger.log(Level.SEVERE, "Date after 1 Days===" + tPlus1DaysDate);
//						tPlus1DaysDate = sdf.parse(sdf.format(c4.getTime()));
//						logger.log(Level.SEVERE, "Date after 1 Days" + tPlus1DaysDate);

						Date tPlus1DaysDate=DateUtility.getDateWithTimeZone("IST", new Date());
						Calendar c4 = Calendar.getInstance();
						c4.setTime(tPlus1DaysDate);
						c4.add(Calendar.DATE, +1);
						logger.log(Level.SEVERE, "Date after 1 Days===" + tPlus1DaysDate);
						tPlus1DaysDate = sdf.parse(sdf.format(c4.getTime()));
						logger.log(Level.SEVERE, "Date after 1 Days" + tPlus1DaysDate);
						
						temp.setServiceDate(tPlus1DaysDate);
						temp.setOldServiceDate(tPlus1DaysDate);
						temp.setAddress(serviceEntity.getAddress());

						temp.setServiceTime(serviceEntity.getServiceTime());
						temp.setServiceBranch(serviceEntity.getServiceBranch());
						temp.setServiceDateDay(serviceEntity.getServiceDateDay());

						temp.setBranch(serviceEntity.getBranch());
						temp.setServicingTime(serviceEntity.getServicingTime());

						temp.setServiceSrNo(serviceEntity.getServiceSerialNo());
						temp.setCompanyId(serviceEntity.getCompanyId());
						/**
						 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation
						 * Services for every Stack This code only executed from
						 * Web Services of NBHC Fumigation service
						 */
						temp.setWareHouse(serviceEntity.getWareHouse());
						temp.setRefNo2(serviceEntity.getRefNo2());
						temp.setShade(serviceEntity.getShade());
						temp.setCompartment(serviceEntity.getCompartment());
						temp.setStackNo(serviceEntity.getStackNo());
						temp.setStackQty(serviceEntity.getStackQty());
						temp.setCommodityName(serviceEntity.getCommodityName());
						if (serviceEntity.getDepositeDate() != null)
							temp.setDepositeDate(serviceEntity.getDepositeDate());
						temp.setWmsServiceFlag(true);
						
						/**
						 * Date 25-01-2020 by Vijay
						 * Des :- When Fumigation Service marked as failure then its original fumigation
						 * id storing new fumigation service.
						 */
						if(serviceEntity.getFumigationServiceId()==0){
							temp.setFumigationServiceId(serviceEntity.getCount());
						}
						else{
							temp.setFumigationServiceId(serviceEntity.getFumigationServiceId());
						}
						/**
						 * ends here
						 */
						
						temp.setProphylacticServiceId(serviceEntity.getProphylacticServiceId());
						
						SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
//						int serviceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), (long) 1);
						int serviceId=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, serviceEntity.getCompanyId(), (long) 1);
						temp.setCount(serviceId);
						ofy().save().entities(temp);
//						GenricServiceImpl impl = new GenricServiceImpl();
//						impl.save(temp);
						logger.log(Level.SEVERE, "New service scheduled successfully with next Day");
						resp.getWriter().println("New service scheduled successfully with next Day");
						
						serviceEntity.setFumigationStatus(Service.SERVICESTATUSFAILURE);
						Date today=DateUtility.getDateWithTimeZone("IST", new Date());
						serviceEntity.setFumigationStatusDate(today);
						serviceEntity.setActionBy(userName);
						serviceEntity.setActionRemark(remark);
						ofy().save().entity(serviceEntity);
						
						updateFumigationSucessfullStatuInFumigationReport(serviceEntity);
						/** Date 24-01-2020 by vijay for failure service adding new fumigation report entry in entity **/
						CreateFumigationReport(temp,temp.getFumigationServiceId());

					} else {
						resp.getWriter().println("Serivce id not found!");
					}
				} else {
					resp.getWriter().println("Serivce id can not be zero!");
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private String createServicesByContract(Contract con) {
		String sendResponse = null;
		try {
			
			 boolean confiFlag  =  ServerAppUtility.checkForProcessConfigurartionIsActiveOrNot("Service", "ContractCategoryAsServiceType", con.getCompanyId());
			 int totalservices = con.getServiceScheduleList().size();
			SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
//			int fumigationCount=(int) numGen.getLastUpdatedNumber("Service", con.getCompanyId(), (long) totalservices);
			int fumigationCount=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, con.getCompanyId(), (long) totalservices);
			int prophylacticCount = fumigationCount + (totalservices/con.getItems().size());
			int fumigationId = fumigationCount;
			int prophylacticId = prophylacticCount;
			 for(SalesLineItem item:con.getItems())
				{
					for(int k=0;k<con.getServiceScheduleList().size();k++)
					{
						if(item.getProductSrNo()==con.getServiceScheduleList().get(k).getSerSrNo()){
							Service temp=new Service();
							temp.setPersonInfo(con.getCinfo());
							temp.setContractCount(con.getCount());
							temp.setProduct((ServiceProduct) item.getPrduct());
							
							if(temp.getProduct().getProductCode().equals("STK-01")){
								temp.setCount(fumigationCount++);
								temp.setProphylacticServiceId(prophylacticCount++);
							}
							else{
								temp.setCount(prophylacticId++);
								temp.setFumigationServiceId(fumigationId++);
							}
							
							String servicingBranch="";
							if(con.getServiceScheduleList().get(k).getServicingBranch()!=null
									&&!con.getServiceScheduleList().get(k).getServicingBranch().equals("")){
								servicingBranch=con.getServiceScheduleList().get(k).getServicingBranch();
							}else{
								servicingBranch=con.getBranch();
							}
							temp.setBranch(servicingBranch);
							temp.setStatus(Service.SERVICESTATUSSCHEDULE);
							temp.setContractStartDate(con.getStartDate());
							temp.setContractEndDate(con.getEndDate());
							temp.setAdHocService(false);
							temp.setServiceIndexNo(k+1);
							temp.setServiceType("Periodic");
							temp.setRefNo2(con.getRefNo2());
							temp.setRefNo(con.getRefNo());					
							temp.setServiceSerialNo(con.getServiceScheduleList().get(k).getScheduleServiceNo());
							temp.setServiceDate(con.getServiceScheduleList().get(k).getScheduleServiceDate());
							temp.setOldServiceDate(con.getServiceScheduleList().get(k).getScheduleServiceDate());
					    	if(con.getServiceScheduleList().get(k).getScheduleServiceDay()!=null){
								temp.setServiceDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
							}
							temp.setServiceTime(con.getServiceScheduleList().get(k).getScheduleServiceTime());
							temp.setServiceBranch(con.getServiceScheduleList().get(k).getScheduleProBranch());
							temp.setServiceDateDay(con.getServiceScheduleList().get(k).getScheduleServiceDay());
							temp.setBranch(con.getServiceScheduleList().get(k).getServicingBranch());
							temp.setServicingTime(con.getServiceScheduleList().get(k).getTotalServicingTime());
							if(con.getStackDetailsList()!=null)
							temp.setStackDetailsList(con.getStackDetailsList());
							if(con.getNumberRange()!=null)
								temp.setNumberRange(con.getNumberRange());
							temp.setRemark(con.getServiceScheduleList().get(k).getServiceRemark());
							temp.setServiceWeekNo(con.getServiceScheduleList().get(k).getWeekNo());
							temp.setEmployee(con.getTechnicianName());
							temp.setServiceValue(Double.parseDouble(String.format("%.2f",(item.getTotalAmount()/(item.getNumberOfServices()*item.getQty())))));
							temp.setServiceWiseBilling(con.isServiceWiseBilling());
							temp.setServiceSrNo(item.getProductSrNo());
							if(confiFlag){
								temp.setServiceType(con.getCategory());
								
							}
							if(con.getPocName()!=null && !con.getPocName().trim().equals("")){
								temp.getPersonInfo().setPocName(con.getPocName());
							}
							temp.setRenewContractFlag(con.isRenewContractFlag());
							
							if(item.getPremisesDetails()!=null && !item.getPremisesDetails().equals("") ){
								temp.setPremises(item.getPremisesDetails());
							}
							
							if(item.getProModelNo() != null){
								temp.setProModelNo(item.getProModelNo());
							}
							if(item.getProSerialNo()!=null){
								temp.setProSerialNo(item.getProSerialNo());
							}
							/**
							 * end
							 */
							
							SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
							String serviceDay = outFormat.format(DateUtility.getDateWithTimeZone("IST", (temp.getServiceDate())));
							temp.setServiceDay(serviceDay);
							
							/**
							 * Date 01-08-2019 by Vijay for NBHC CCPM
							 * Fumigation Services for every Stack
							 * This code only executed from Web Services of NBHC Fumigation service
							 */
							 temp.setWareHouse(con.getServiceScheduleList().get(k).getWarehouseName());
							 temp.setShade(con.getServiceScheduleList().get(k).getShade());
							 temp.setCompartment(con.getServiceScheduleList().get(k).getCompartment());
							 temp.setStackNo(con.getServiceScheduleList().get(k).getStackNo());
							 temp.setStackQty(con.getServiceScheduleList().get(k).getStackQty());
							 if(con.getServiceScheduleList().get(k).getCommodityName()!=null)
							 temp.setCommodityName(con.getServiceScheduleList().get(k).getCommodityName());
							 if(con.getStartDate()!=null)
							 temp.setDepositeDate(con.getStartDate());
							 temp.setAddress(con.getCustomerServiceAddress()); 
							 temp.setCompanyId(con.getCompanyId());
							 temp.setWmsServiceFlag(true);
							 if(con.getServiceScheduleList().get(k).getClusterName()!=null)
							 temp.setClusterName(con.getServiceScheduleList().get(k).getClusterName());

							 ofy().save().entity(temp).now();
							 
						 	/**
							 * @author Vijay Chougule Date 21-12-2019
							 * Project :- NBHC CCPM fumigation Tracker
							 * Des :- for Report data saving in another entity
							 */
							 if(temp.getProduct().getProductCode().equals("STK-01")){
								 String remark = CreateFumigationReport(temp,-1);
								 logger.log(Level.SEVERE, "remark =="+remark);
							 }
						}
					}	   
				}
			 
			 logger.log(Level.SEVERE, "Services Created Successfull");	 
			 sendResponse = "Services Created Successfull";
			 
		} catch (Exception e) {
			sendResponse = "Services not created:::::" + e;
			e.printStackTrace();
		}
		return sendResponse;
	}

	

	

	

	

	/**
	 *  Date 13-11-2019
	 *  Des :- When Fumigation Service mark successfull then new service created with 45 days
	 *  and also create prophylactic service
	 * @param remark 
	 * @param userName 
	 * @param string 
	 */
	public String createFumigationAndProphalacticService(Service serviceEntity,String userName, String remark, boolean autosucessFlag) throws Exception {
		String response = "";
		
		try {
			
		
		if(serviceEntity.getStackQty()>0){
			logger.log(Level.SEVERE, "Inside Fumigation Service Creation method");
			Service prophylacticService = ofy().load().type(Service.class).filter("companyId", serviceEntity.getCompanyId()).filter("wmsServiceFlag", true)
										.filter("count", serviceEntity.getProphylacticServiceId()).first().now();
			logger.log(Level.SEVERE, "serviceProduct =="+prophylacticService);
			if(prophylacticService!=null){
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
//				int serviceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), (long) 2);
				int serviceId=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, serviceEntity.getCompanyId(), (long) 2);
				int fumigationServiceId = serviceId;
				int prophylacticServiceId = ++serviceId;

				for(int i=0;i<2;i++){
					if(i==0){
						Service service = new Service();
						service = getServiceDetails(prophylacticService,serviceEntity);
						service.setCount(prophylacticServiceId);
						service.setFumigationServiceId(fumigationServiceId);
						ofy().save().entity(service).now();
					}
					else{
						Service service = new Service();
						service = getServiceDetails(serviceEntity,serviceEntity);
						service.setCount(fumigationServiceId);
						service.setProphylacticServiceId(prophylacticServiceId);
						ofy().save().entity(service).now();
						CreateFumigationReport(service,-1);
						
						/*** Date 11-11-2019 By Vijay Fumigation Service marking as Successfull ***/
						serviceEntity.setFumigationStatus(Service.SERVICESTATUSSUCCESS);
						Date today=DateUtility.getDateWithTimeZone("IST", new Date());
						serviceEntity.setFumigationStatusDate(today);
						if(userName!=null){
							serviceEntity.setActionBy(userName);
						}
						else{
							serviceEntity.setActionBy("Cron Job");
						}
						if(remark!=null){
							serviceEntity.setActionRemark(remark);
						}
						else{
							serviceEntity.setActionRemark("This service marked as successful by System");
						}
						ofy().save().entity(serviceEntity);
						response = "New service scheduled successfully with 45 Days";
						logger.log(Level.SEVERE, "New service scheduled successfully with 45 Days");
						updateFumigationSucessfullStatuInFumigationReport(serviceEntity);
					}
				}
			}
			else{
				response = "Propholactic Service not found for this fumigation service so future services not created";
			}
			
		}
		else{
			logger.log(Level.SEVERE, "There no stack Qty so futre services not created");
			response = "There no stack qty so future services not created";
			/*** Date 11-11-2019 By Vijay Fumigation Service marking as Successfull ***/
			serviceEntity.setFumigationStatus(Service.SERVICESTATUSSUCCESS);
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			serviceEntity.setFumigationStatusDate(today);
			if(userName!=null){
				serviceEntity.setActionBy(userName);
			}
			else{
				serviceEntity.setActionBy("Cron Job");
			}
			if(remark!=null){
				serviceEntity.setActionRemark(remark);
			}
			else{
				serviceEntity.setActionRemark("This service marked as successful by System");
			}
			ofy().save().entity(serviceEntity);
			logger.log(Level.SEVERE, "Stack Qty Zero so new service not created");
			updateFumigationSucessfullStatuInFumigationReport(serviceEntity);

		}
		
		} catch (Exception e) {
			logger.log(Level.SEVERE, "inside createFumigationAndProphalacticService method Exception "+e.getMessage());
			String strSubject = "Inward";
			if(autosucessFlag){
				strSubject = "Auto Success";
			}
			FumigationServiceMarkSuccessfull autoSucesscronjob = new FumigationServiceMarkSuccessfull();
			autoSucesscronjob.sendEmailAutoSucessFumigationServiceCreationFailure(serviceEntity,strSubject);
		}
		
		return response;
		
	}

	
	private Service getServiceDetails(Service serviceEntity, Service fumigationService)throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Service temp = new Service();
		temp.setRefNo(serviceEntity.getRefNo()); // inward // transaction // id
		temp.setPersonInfo(serviceEntity.getPersonInfo());
		temp.setContractCount(serviceEntity.getContractCount());
		temp.setProduct((ServiceProduct) serviceEntity.getProduct());
		temp.setBranch(serviceEntity.getBranch());
		temp.setStatus(Service.SERVICESTATUSSCHEDULE);
		temp.setContractStartDate(serviceEntity.getContractStartDate());
		temp.setContractEndDate(serviceEntity.getContractEndDate());
		temp.setAdHocService(false);
		temp.setServiceIndexNo(serviceEntity.getServiceIndexNo()+1);
		temp.setServiceType(serviceEntity.getServiceType());

		temp.setServiceSerialNo(serviceEntity.getServiceSerialNo()+1);
		/**
		 * Date 11-12-2019 by Vijay
		 * Des :- New service will create based on Degassing Original Service Date + 45 Days
		 */
//		Date tPlus45DaysDate = serviceEntity.getServiceCompletionDate();
		Date tPlus45DaysDate = null;
		Service deggassingService = ofy().load().type(Service.class).filter("companyId", fumigationService.getCompanyId())
									.filter("count", fumigationService.getDeggassingServiceId()).first().now();
		if(deggassingService!=null){
			logger.log(Level.SEVERE, "deggassingService"+deggassingService.getOldServiceDate());
			tPlus45DaysDate = deggassingService.getOldServiceDate();
		}
		try {
		Calendar c4 = Calendar.getInstance();
		c4.setTime(tPlus45DaysDate);
		c4.add(Calendar.DATE, +45);
		logger.log(Level.SEVERE, "Date after 45 Days===" + tPlus45DaysDate);
	
			tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
		} catch (ParseException e) {
			logger.log(Level.SEVERE, " Deggasing service not found");
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "Date after 45 Days" + tPlus45DaysDate);

		temp.setServiceDate(tPlus45DaysDate);
		temp.setOldServiceDate(tPlus45DaysDate);
		temp.setAddress(serviceEntity.getAddress());

		temp.setServiceTime(serviceEntity.getServiceTime());
		temp.setServiceBranch(serviceEntity.getServiceBranch());
		temp.setServiceDateDay(serviceEntity.getServiceDateDay());

		temp.setBranch(serviceEntity.getBranch());
		temp.setServicingTime(serviceEntity.getServicingTime());

		temp.setServiceSrNo(serviceEntity.getServiceSerialNo());
		temp.setCompanyId(serviceEntity.getCompanyId());
		/**
		 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation
		 * Services for every Stack This code only executed from
		 * Web Services of NBHC Fumigation service
		 */
		temp.setWareHouse(serviceEntity.getWareHouse());
		temp.setRefNo2(serviceEntity.getRefNo2());
		temp.setShade(serviceEntity.getShade());
		temp.setCompartment(serviceEntity.getCompartment());
		temp.setStackNo(serviceEntity.getStackNo());
		temp.setStackQty(serviceEntity.getStackQty());
		if (serviceEntity.getDepositeDate() != null)
			temp.setDepositeDate(serviceEntity.getDepositeDate());
		temp.setCommodityName(serviceEntity.getCommodityName());
		
		temp.setWmsServiceFlag(true);

		return temp;
	}
	
	private String createFumigationAndProphalacticSuspendService(Service serviceEntity) {

		String response = "";
		if(serviceEntity.getStackQty()>0){
			logger.log(Level.SEVERE, "Inside Fumigation Service Creation method");
			Service prophylacticService = ofy().load().type(Service.class).filter("companyId", serviceEntity.getCompanyId()).filter("contractCount", serviceEntity.getContractCount())
										.filter("wmsServiceFlag", true).filter("count", serviceEntity.getProphylacticServiceId()).first().now();
			logger.log(Level.SEVERE, "serviceProduct =="+prophylacticService);
			if(prophylacticService!=null){
				SingletoneNumberGeneration numGen=SingletoneNumberGeneration.getSingletonInstance();
//				int serviceId=(int) numGen.getLastUpdatedNumber("Service", serviceEntity.getCompanyId(), (long) 2);
				int serviceId=(int) numGen.getLastUpdatedNumber(AppConstants.INHOUSESERVICE, serviceEntity.getCompanyId(), (long) 2);
				int fumigationServiceId = ++serviceId;
				int prophylacticServiceId = ++serviceId;
				
				for(int i=0;i<2;i++){
					if(i==0){
						Service service = new Service();
						service = getServiceDetailsforSuspendService(prophylacticService,serviceEntity);
						service.setCount(prophylacticServiceId);
						service.setFumigationServiceId(fumigationServiceId);
						ofy().save().entity(service).now();
					}
					else{
						Service service = new Service();
						service = getServiceDetailsforSuspendService(serviceEntity,serviceEntity);
						service.setCount(fumigationServiceId);
						service.setProphylacticServiceId(prophylacticServiceId);
						ofy().save().entity(service).now();
						CreateFumigationReport(service,-1);
						response = "New service scheduled successfully with 45 Days";
						logger.log(Level.SEVERE, "New service scheduled successfully with 45 Days");
					}
				}
			}
			else{
				response = "Propholactic Service not found for this fumigation service so future services not created";
				logger.log(Level.SEVERE, "Propholactic Service not found for this fumigation service so future services not created");

			}
			
		}
		else{
			response = "There no stack qty so future services not created";
			logger.log(Level.SEVERE, "There no stack Qty so futre services not created");
		}
		
		return response;
		
	
	}

	private Service getServiceDetailsforSuspendService(Service prophylacticService, Service serviceEntity) { 

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		
		Service temp = new Service();
		temp.setRefNo(prophylacticService.getRefNo()); // inward // transaction // id
		temp.setPersonInfo(prophylacticService.getPersonInfo());
		temp.setContractCount(prophylacticService.getContractCount());
		temp.setProduct((ServiceProduct) prophylacticService.getProduct());
		temp.setBranch(prophylacticService.getBranch());
		temp.setStatus(Service.SERVICESTATUSSCHEDULE);
		temp.setContractStartDate(prophylacticService.getContractStartDate());
		temp.setContractEndDate(prophylacticService.getContractEndDate());
		temp.setAdHocService(false);
		temp.setServiceIndexNo(1);
		temp.setServiceType(prophylacticService.getServiceType());

		temp.setServiceSerialNo(prophylacticService.getServiceSerialNo() + 1);
		/**
		 * Date 11-12-2019 by Vijay
		 * Des :- New service will create based on Final Service Date for suspend Case as per devesh and email
		 */
		Date tPlus45DaysDate = serviceEntity.getServiceDate();
		
		Calendar c4 = Calendar.getInstance();
		c4.setTime(tPlus45DaysDate);
		c4.add(Calendar.DATE, +45);
		logger.log(Level.SEVERE, "Date after 45 Days===" + tPlus45DaysDate);
		try {
			tPlus45DaysDate = sdf.parse(sdf.format(c4.getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		logger.log(Level.SEVERE, "Date after 45 Days" + tPlus45DaysDate);

		temp.setServiceDate(tPlus45DaysDate);
		temp.setOldServiceDate(tPlus45DaysDate);
		temp.setAddress(serviceEntity.getAddress());

		temp.setServiceTime(prophylacticService.getServiceTime());
		temp.setServiceBranch(prophylacticService.getServiceBranch());
		temp.setServiceDateDay(prophylacticService.getServiceDateDay());

		temp.setBranch(prophylacticService.getBranch());
		temp.setServicingTime(prophylacticService.getServicingTime());

		temp.setServiceSrNo(prophylacticService.getServiceSerialNo());
		temp.setCompanyId(prophylacticService.getCompanyId());
		/**
		 * Date 01-08-2019 by Vijay for NBHC CCPM Fumigation
		 * Services for every Stack This code only executed from
		 * Web Services of NBHC Fumigation service
		 */
		temp.setWareHouse(prophylacticService.getWareHouse());
		temp.setRefNo2(prophylacticService.getRefNo2());
		temp.setShade(prophylacticService.getShade());
		temp.setCompartment(prophylacticService.getCompartment());
		temp.setStackNo(prophylacticService.getStackNo());
		temp.setStackQty(prophylacticService.getStackQty());
		if (prophylacticService.getDepositeDate() != null)
			temp.setDepositeDate(prophylacticService.getDepositeDate());
		temp.setCommodityName(prophylacticService.getCommodityName());
		
		temp.setWmsServiceFlag(true);

		return temp;
	
	}

	
	private String CreateFumigationReport(Service service, int originalfumigationServiceId) {
		String remark = "";
		logger.log(Level.SEVERE, "In the fumigation report entity prophylactic Service ID"+service.getProphylacticServiceId());
		logger.log(Level.SEVERE, "In the fumigation report entity Fumigation Service ID"+service.getCount());

		FumigationServiceReport fumigationReport = new FumigationServiceReport();
		fumigationReport.setCompanyId(service.getCompanyId());
		fumigationReport.setWarehouseName(service.getWareHouse());
		fumigationReport.setWarehouseCode(service.getRefNo2());
		fumigationReport.setCompartment(service.getCompartment());
		fumigationReport.setShade(service.getShade());
		fumigationReport.setStackNumber(service.getStackNo());
		fumigationReport.setStackQuantity(service.getStackQty());
		fumigationReport.setFumigationServiceId(service.getCount());
		fumigationReport.setFumigationServiceDate(service.getOldServiceDate());
		Service serviceEntity = ofy().load().type(Service.class).filter("count", service.getProphylacticServiceId())
								.filter("companyId", service.getCompanyId()).filter("contractCount", service.getContractCount())
								.first().now();
		logger.log(Level.SEVERE,"prophylactic service serviceEntity"+serviceEntity);
		if(serviceEntity!=null){
			fumigationReport.setProphylacticServiceDate(serviceEntity.getOldServiceDate());
			fumigationReport.setProphylacticServiceId(service.getProphylacticServiceId());
		}
		fumigationReport.setContractId(service.getContractCount());
		fumigationReport.setFumigationServiceStatus(Service.SERVICESTATUSSCHEDULE);
		if(originalfumigationServiceId!=-1){
		fumigationReport.setOriginalfumigationId(originalfumigationServiceId);
		}
		GenricServiceImpl impl = new GenricServiceImpl();
		impl.save(fumigationReport);
		logger.log(Level.SEVERE, "Fumigation Report created for new fumigation service");
		remark = "Fumigation Report created for new fumigation service";
		return remark;
	}
	
	private void updateServiceSuspendStatusInFumigationReport(Service service) {

		if(service.getProduct().getProductCode().equals("STK-01")){
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				fumigationReport.setSuspendedDate(service.getFumigationStatusDate());
				fumigationReport.setSuspendedBy(service.getActionBy());
				fumigationReport.setSuspendedRemark(service.getActionRemark());
				fumigationReport.setStackQuantity(service.getStackQty());
				fumigationReport.setFumigationStatus(Service.SERVICESUSPENDED);
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
	
	}
	
	private void updateFumigationSucessfullStatuInFumigationReport(Service service) {
		if(service.getProduct().getProductCode().equals("STK-01")){
			FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
					.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
					.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
					.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first().now();
			if(fumigationReport!=null){
				logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
				fumigationReport.setFumigationStatusDate(service.getFumigationStatusDate());
				fumigationReport.setFumigationRemark(service.getActionRemark());
				fumigationReport.setFumigationStatus(service.getFumigationStatus());
				fumigationReport.setStackQuantity(service.getStackQty());
				fumigationReport.setSuccessFailureBy(service.getActionBy());
				ofy().save().entity(fumigationReport);
				logger.log(Level.SEVERE, "fumigationReport updated successfully");
			}
		}
	}

	/**
	 * Date 25-01-2020 by vijay
	 * Des :- when stack qty is zero then its open service will get cancelled so the same status i have updated in
	 *  fumigation report
	 */
	private void updateServiceCancelledInFumigationReport(Service service) {

		FumigationServiceReport fumigationReport = ofy().load().type(FumigationServiceReport.class)
				.filter("companyId", service.getCompanyId()).filter("contractId", service.getContractCount())
				.filter("warehouseCode", service.getRefNo2()).filter("compartment", service.getCompartment())
				.filter("shade", service.getShade()).filter("fumigationServiceId", service.getCount()).first().now();
		logger.log(Level.SEVERE, "fumigationReport "+fumigationReport);
		if(fumigationReport!=null){
			logger.log(Level.SEVERE, "Fumigation Report id"+fumigationReport.getCount());
			Date today=DateUtility.getDateWithTimeZone("IST", new Date());
			fumigationReport.setFumigationStatusDate(today);
			fumigationReport.setFumigationRemark(service.getActionRemark());
			fumigationReport.setFumigationStatus(Service.SERVICESTATUSCANCELLED);
			fumigationReport.setStackQuantity(service.getStackQty());
			fumigationReport.setSuccessFailureBy(service.getActionBy());
			ofy().save().entity(fumigationReport);
			logger.log(Level.SEVERE, "fumigationReport updated successfully");
		}
	
	}
	
}
