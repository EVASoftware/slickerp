package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
//import org.json.simple.JSONObject;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.server.utility.ServerAppUtility;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;

public class GetClientLicenseInfo extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5567191809997227058L;

	
	Logger logger=Logger.getLogger("UpdateClientsLicense.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		
		String data = req.getParameter("jsonString");
		System.out.println("DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		ServerAppUtility utility = new ServerAppUtility();

		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "comp"+comp.getBusinessUnitName());
		if(comp!=null){
			
			
			JSONObject object = null;
			try {
				object = new JSONObject(screenData.trim());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			String actionTask=object.optString("action_task").trim();
			
			if(actionTask.equals(AppConstants.GETLICENSEINFO)){
				
				logger.log(Level.SEVERE, "comp.getLicenseDetailsList() size "+comp.getLicenseDetailsList().size());

				if(comp.getLicenseDetailsList().size()>0){
					Gson gson = new Gson();  
					JSONObject jObj=new JSONObject();
					JSONArray jsonArray = null;
					try {
						jsonArray = new JSONArray(gson.toJson(comp.getLicenseDetailsList()));
					} catch (JSONException e) {
						e.printStackTrace();
						logger.log(Level.SEVERE,"Json array exception : "+e);
					}
					try {
						jObj.put("license_list",jsonArray);
					} catch (JSONException e) {
					}

//					String licensedata = gson.toJson(comp.getLicenseDetailsList());
					
					String jsonstring = jObj.toString().replaceAll("\\\\", "");

					logger.log(Level.SEVERE,"License Data:: "+jsonstring);
					resp.getWriter().println(jsonstring);
				}
				else{
					resp.getWriter().println(utility.getMessageInJson("license details not found"));
				}
			}

		}
		else{
			resp.getWriter().println(utility.getMessageInJson("Company not found"));
		}
		
	}	
}
