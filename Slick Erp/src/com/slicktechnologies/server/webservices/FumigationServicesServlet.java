package com.slicktechnologies.server.webservices;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.slicktechnologies.shared.Service;

import static com.googlecode.objectify.ObjectifyService.ofy;

/**
 * Date 31-07-2019
 * @author Vijay Chougule
 * NBHC Fumigation Services to show in CIO App
 */
public class FumigationServicesServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1129849362945086982L;
	
	Logger logger = Logger.getLogger("Logger");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		
		HashSet<String> hswarehouseCode = new HashSet<String>();
		
		String jsonString = req.getParameter("warehouseDetails");
		logger.log(Level.SEVERE, "jsonString::::::::::" + jsonString);

		JSONObject jsonobj = null;
		JSONObject jsonwarehousecode = null;
		try {
			jsonobj = new JSONObject(jsonString);
			jsonwarehousecode = new JSONObject();
			jsonwarehousecode = jsonobj.getJSONObject("warehouseCode");
			
			JSONArray warehouseCodesArray = new JSONArray();
			warehouseCodesArray = jsonwarehousecode.getJSONArray("objWarehouse");
			System.out.println("warehouseCodesArray"+warehouseCodesArray.length());
			for(int i=0;i<warehouseCodesArray.length();i++){
				JSONObject jsonobjwarehousecode = warehouseCodesArray.getJSONObject(i);
				hswarehouseCode.add(jsonobjwarehousecode.optString("sWareHouseCode"));
			}
			logger.log(Level.SEVERE, "hswarehouseCode list"+hswarehouseCode.size());
			logger.log(Level.SEVERE, "hswarehouseCode list"+hswarehouseCode);

	   
		ArrayList<String> warehousecodelist = new ArrayList<String>(hswarehouseCode);
		 long companyId =	jsonobj.getLong("authCode");
			System.out.println("companyId"+companyId);
			logger.log(Level.SEVERE, "company Id"+companyId);
			System.out.println("comp"+companyId);
		/**
		 * Date 27-02-2020
		 * Des :- When Stack Qty gets Zero in EVA ERP services will get cancelled
		 * so they need cancelled services also so here i am sending all data without status	
		 */
//		ArrayList<String> statuslist = new ArrayList<String>();
//		statuslist.add(Service.SERVICESTATUSRESCHEDULE);
//		statuslist.add(Service.SERVICESTATUSSCHEDULE);
//		statuslist.add(Service.SERVICESTATUSSTARTED);
//		statuslist.add(Service.SERVICESTATUSREPORTED);
//		statuslist.add(Service.SERVICETCOMPLETED);
//		statuslist.add(Service.SERVICESTATUSCOMPLETED);
//		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
//				.filter("status IN", statuslist).filter("refNo2 IN",warehousecodelist)
//				.filter("wmsServiceFlag", true).list();
		
		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("refNo2 IN",warehousecodelist).filter("wmsServiceFlag", true).list();
		logger.log(Level.SEVERE, "servicelist size"+ servicelist.size());

		
//		List<Service> servicelist = ofy().load().type(Service.class).filter("companyId", companyId).filter("fumigationStatus", "NA")
//									.filter("status IN", statuslist).filter("refNo2 IN",warehousecodelist).list();
//		logger.log(Level.SEVERE, "servicelist size"+ servicelist.size());
//		
//		ArrayList<String> serviceUpdationstatuslist = new ArrayList<String>();
//		serviceUpdationstatuslist.add(Service.SERVICETCOMPLETED);
//		serviceUpdationstatuslist.add(Service.SERVICESTATUSCOMPLETED);
//		serviceUpdationstatuslist.add("");
//		
//		List<Service> serviceUpdationlist = ofy().load().type(Service.class).filter("companyId", companyId).filter("fumigationStatus","NA")
//				.filter("status IN",serviceUpdationstatuslist).filter("degassingServiceStatus IN",serviceUpdationstatuslist).filter("refNo2 IN",warehousecodelist).list();
//		logger.log(Level.SEVERE, "serviceUpdationlist size"+ serviceUpdationlist.size());
//		if(serviceUpdationlist.size()!=0){
//			servicelist.addAll(serviceUpdationlist);
//		}
//		logger.log(Level.SEVERE, "servicelist Total size"+ servicelist.size());

		if(servicelist.size()==0 ){
			resp.getWriter().println("No Services found");
		}
		else{
			JSONObject jsonobject = new JSONObject();
			JSONArray jsonArray = new JSONArray();
			for(Service service : servicelist){
				JSONObject json = new JSONObject();
				json.put("StackName", service.getStackNo());
				json.put("ServiceId", service.getCount());
				json.put("Quantity", service.getStackQty());
				json.put("ServiceName", service.getProductName());
				json.put("ServiceDate",dateFormat.format(service.getServiceDate()));
				json.put("Status", service.getStatus());
				json.put("WHCode", service.getRefNo2());
				json.put("ShedName", service.getShade());
				json.put("CompartmentName", service.getCompartment());
				if(service.getDepositeDate()!=null){
				json.put("DepositeDate", dateFormat.format(service.getDepositeDate()));
				}
				if(service.getOldServiceDate()!=null){
					json.put("ServiceScheduleDate",dateFormat.format(service.getOldServiceDate()));
				}
				if(service.getDegassingServiceStatus()!=null){
					json.put("DegassingStatus", service.getDegassingServiceStatus());
				}
				json.put("ContractId", service.getContractCount());
				if(service.getCommodityName()!=null){
					json.put("CommodityName", service.getCommodityName());
				}
				
				if(service.getCreationDate()!=null){
					json.put("CreationDate", dateFormat.format(service.getCreationDate()));
				}
				else{
					json.put("CreationDate", "");
				}
				json.put("DegassingServiceId", service.getDeggassingServiceId());
				
				jsonArray.put(json);
			}
			jsonobject.put("ServiceDetails", jsonArray);
			String jsonstring = jsonobject.toString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE, "Json"+jsonstring);
			resp.getWriter().println(jsonstring);
		}
		
		} catch (JSONException e) {
			e.printStackTrace();
			logger.log(Level.SEVERE, "Exception =="+ e.getLocalizedMessage());

		}
		
	}
	
	

}
