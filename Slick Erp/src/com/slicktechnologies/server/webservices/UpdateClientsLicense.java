package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slicktechnologies.client.utility.AppConstants;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.businessunitlayer.LicenseDetails;

public class UpdateClientsLicense   extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4527163009412648709L;
	
	Logger logger=Logger.getLogger("UpdateClientsLicense.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		// TODO Auto-generated method stub
//		super.doGet(req, resp);
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
//		super.doPost(req, resp);
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		
		String data = req.getParameter("jsonString");
		logger.log(Level.SEVERE,"DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		String url = urlCalled.replace("http://", "");
		String[] splitUrl = url.split("\\.");
		System.out.println("SplitURL : "+splitUrl[0]);
		Exception exep = null;
		// try {
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One"+screenData);
		long companyId=0l;
		if(comp!=null){
			companyId= comp.getCompanyId();
			Gson gson = new Gson();
			JSONObject object = null;
			try {
				object = new JSONObject(screenData.trim());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			JSONArray license_array = null;
			try {
				license_array = object.getJSONArray("license_list");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			Type type = new TypeToken<ArrayList<LicenseDetails>>(){}.getType();
			ArrayList<LicenseDetails> licList=new ArrayList<LicenseDetails>();
			licList=gson.fromJson(license_array.toString(), type);
			
			System.out.println("LIST SIZE : "+licList.size());
			String actionTask=object.optString("action_task").trim();
			long contractId=0;
			try {
//				contractId=Integer.parseInt(object.optString("contract_id").trim());
				contractId=Long.parseLong(object.optString("contract_id").trim()); //Ashwini Patil Date:22-12-2023 changed type from int to long, to store zoho sales order id which is long
			} catch (Exception e) {
				resp.getWriter().print("Please send contract_id");
				return;
			}
			
			logger.log(Level.SEVERE, "after contract id validation");

			if(AppConstants.LICENSETYPELIST.ACTION_TASK_CREATE.equals(actionTask)){
				
				/**
				 * @author Vijay Date :- 14-01-2023
				 * Des :- Deleting existing entries & recreate as per Contract
				 */
				if(comp.getLicenseDetailsList()!=null && comp.getLicenseDetailsList().size()>0){
					ArrayList<LicenseDetails> updatedlist = new ArrayList<LicenseDetails>();
					updatedlist.addAll(comp.getLicenseDetailsList());
					logger.log(Level.SEVERE, "updatedlist size"+updatedlist.size());
					for(LicenseDetails licenseinfo : comp.getLicenseDetailsList()){
						if(licenseinfo.getContractCount()==contractId){
							updatedlist.remove(licenseinfo);
							logger.log(Level.SEVERE, "removing license details of contract id"+contractId);

						}
					}
					comp.setLicenseDetailsList(updatedlist);
					logger.log(Level.SEVERE, "after removing existing contract updatedlist size"+updatedlist.size());

				}
				/**
				 * ends here
				 */
				
				boolean validCall=checkValidCall(contractId,comp.getLicenseDetailsList());
				if(validCall){
					logger.log(Level.SEVERE, "Updating License");
					/**
					 * @author Anil
					 * @since 11-02-2022
					 * Suppose we have new contract for some other license but we are now adding another license type
					 * on same contract. such type of licenses are not getting updated
					 */
					ArrayList<LicenseDetails> upLicList=new ArrayList<LicenseDetails>();
					for(LicenseDetails obj1:licList){
						boolean updateFlag=false;
						for(LicenseDetails obj2:comp.getLicenseDetailsList()){
							if(obj1.getContractCount()==obj2.getContractCount()
									&&obj1.getLicenseType().equals(obj2.getLicenseType())){
								updateFlag=true;
								obj2.setStartDate(obj1.getStartDate());
								obj2.setEndDate(obj1.getEndDate());
								obj2.setNoOfLicense(obj1.getNoOfLicense());
								obj2.setCreatedBy(obj1.getCreatedBy());
								obj2.setDuration(obj1.getDuration());
								obj2.setStatus(true);
								
								logger.log(Level.SEVERE, "Updating license");

							}
						}
						if(!updateFlag){
							upLicList.add(obj1);
						}
					}
					
					if(upLicList.size()>0){
						comp.getLicenseDetailsList().addAll(upLicList);
					}
				}else{
					logger.log(Level.SEVERE, "Adding License");
					comp.getLicenseDetailsList().addAll(licList);
				}
				
			}
//			else if(AppConstants.LICENSETYPELIST.ACTION_TASK_UPDATE.equals(actionTask)){
//				boolean validCall=checkValidCall(contractId,comp.getLicenseDetailsList());
//				
//				if(validCall){
//					for(LicenseDetails obj1:licList){
//						for(LicenseDetails obj2:comp.getLicenseDetailsList()){
//							if(obj1.getContractCount()==obj2.getContractCount()
//									&&obj1.getLicenseType().equals(obj2.getLicenseType())){
//								obj2.setStartDate(obj1.getStartDate());
//								obj2.setEndDate(obj1.getEndDate());
//								obj2.setNoOfLicense(obj1.getNoOfLicense());
//							}
//						}
//					}
//				}else{
//					resp.getWriter().print("Not a valid contract.");
//					return;
//				}
//			}
			else if(AppConstants.LICENSETYPELIST.ACTION_TASK_CANCEL.equals(actionTask)){
				boolean validCall=checkValidCall(contractId,comp.getLicenseDetailsList());
				
				if(validCall){
					logger.log(Level.SEVERE, "Cancelling License");
					for(LicenseDetails obj1:licList){
						for(LicenseDetails obj2:comp.getLicenseDetailsList()){
							if(obj1.getContractCount()==obj2.getContractCount()
									&&obj1.getLicenseType().equals(obj2.getLicenseType())){
								obj2.setStartDate(obj1.getStartDate());
								obj2.setEndDate(obj1.getEndDate());
								obj2.setNoOfLicense(obj1.getNoOfLicense());
								obj2.setStatus(false);
								obj2.setCreatedBy(obj1.getCreatedBy());
								obj2.setDuration(obj1.getDuration());
							}
						}
					}
				}else{
					resp.getWriter().print("Not a valid contract.");
					return;
				}
			}
			else if(AppConstants.DELETECLIENTLICENSE.equals(actionTask)){
				logger.log(Level.SEVERE, "For Deleting client license info");

				ArrayList<LicenseDetails> updatedlist = new ArrayList<LicenseDetails>();
				updatedlist.addAll(comp.getLicenseDetailsList());
				logger.log(Level.SEVERE, "updatedlist size"+updatedlist.size());
				for(LicenseDetails licenseinfo : comp.getLicenseDetailsList()){
					if(licenseinfo.getContractCount()==contractId){
						updatedlist.remove(licenseinfo);
						logger.log(Level.SEVERE, "removing license details of contract id"+contractId);

					}
				}
				comp.setLicenseDetailsList(updatedlist);
				logger.log(Level.SEVERE, "after removing existing contract updatedlist size"+updatedlist.size());

				
			}
			
			
			ofy().save().entity(comp).now();
			
//			getSuccessMsg(comp.getLicenseDetailsList());
			
			resp.getWriter().print(getSuccessMsg(comp.getLicenseDetailsList()));
			return;
			
		}else{
			resp.getWriter().print("Company not found.");
			return;
//			companyId=Long.parseLong("5348024557502464");
		}
		
		
//		resp.getWriter().print("Its Done");
//		String screenName = object.optString("screenName").trim();
	}

	private String getSuccessMsg(ArrayList<LicenseDetails> licenseDetailsList) {
		SimpleDateFormat fmt= new SimpleDateFormat("dd-MMM-yyyy");
		fmt.setTimeZone(TimeZone.getTimeZone("IST"));
		TimeZone.setDefault(TimeZone.getTimeZone("IST"));
		StringBuilder sb=new StringBuilder();
		for(LicenseDetails lcObj:licenseDetailsList){
			String lic="CONTRACT COUNT : "+lcObj.getContractCount()+" LICENSE TYPE : "+lcObj.getLicenseType()+" START DATE : "+fmt.format(lcObj.getStartDate())+" DURATION : "+lcObj.getDuration()+" END DATE : "+fmt.format(lcObj.getEndDate())+" STATUS : "+lcObj.getStatus()+"\n";
			sb.append(lic);
		}
		return sb.toString();
	}

	private boolean checkValidCall(long contractId,ArrayList<LicenseDetails> licenseDetailsList) {
		if(licenseDetailsList==null&&licenseDetailsList.size()==0){
			return false;
		}else{
			for(LicenseDetails obj: licenseDetailsList){
				if(obj.getContractCount()==contractId){
					return true;
				}
			}
		}
		return false;
	}
	
	


}
