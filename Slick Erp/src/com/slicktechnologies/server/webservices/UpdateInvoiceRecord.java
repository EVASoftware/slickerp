package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.salesprocess.Invoice;

public class UpdateInvoiceRecord extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1036520059050778479L;

	Logger logger=Logger.getLogger("UpdateInvoiceRecord.class");
	Company comp;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String invoiceCount=req.getParameter("invoiceCount");
		String response="";
		String urlCalled=req.getRequestURL().toString().trim();
		String url=urlCalled.replace("http://", "");
		String[] splitUrl=url.split("\\.");
		
		comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();	
		
		Invoice invoice=ofy().load().type(Invoice.class).filter("companyId",comp.getCompanyId()).filter("count", Integer.parseInt(invoiceCount)).first().now();
	
		if(invoice!=null){
		
		Contract contract=ofy().load().type(Contract.class).filter("companyId", comp.getCompanyId()).filter("count", invoice.getContractCount()).first().now();
		for (int i = 0; i < contract.getItems().size(); i++) {
			for (int j = 0; j < invoice.getSalesOrderProducts().size(); j++) { 
			
				if ((invoice.getSalesOrderProducts().get(j).getPrduct().getCount() == contract.getItems().get(i).getPrduct().getCount())
					&&(invoice.getSalesOrderProducts().get(j).getOrderDuration() == contract.getItems().get(i).getDuration()))
				{
					if(invoice.getSalesOrderProducts().get(j).getProdName().trim().equalsIgnoreCase(contract.getItems().get(i).getProductName().trim())){
						invoice.getSalesOrderProducts().get(j).setOrderDuration(contract.getItems().get(i).getDuration());
						invoice.getSalesOrderProducts().get(j).setOrderServices(contract.getItems().get(i).getNumberOfServices());
						
					}
				}
			}
			
		}
		ofy().save().entity(invoice);
		
		}else{
			resp.getWriter().print("No invoice found!!");
		}
		
	}
}
