package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.common.businessprocesslayer.Customer;
import com.slicktechnologies.shared.common.businessunitlayer.Company;

public class FetchCustomerDetails extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7771627182872512998L;

	Logger logger=Logger.getLogger("FetchCustomerDetails.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doPost(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String stateName="",locationName="";
		try{
		stateName=req.getParameter("state");
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
		locationName=req.getParameter("location");
		}catch(Exception e){
			e.printStackTrace();
		}
		String response=getCustomerData(locationName,stateName,req,resp);
		
		resp.getWriter().print(response);
		
	}

	private String getCustomerData(String locationName, String stateName,
			HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		
		if(locationName.trim().equalsIgnoreCase("NA")&&stateName.trim().equalsIgnoreCase("NA")){
			return "Please provide locality or state!!";
		}
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		
		String urlCalled=req.getRequestURL().toString().trim();
		String url=urlCalled.replace("http://", "");
		String[] splitUrl=url.split("\\.");
		String accessUrl = splitUrl[0];
		String appID=splitUrl[1];
		logger.log(Level.SEVERE, "accessUrl" + accessUrl+"appID "+appID);
		logger.log(Level.SEVERE, "localityName" + locationName+"stateName "+stateName);
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl.trim()).first().now();
		logger.log(Level.SEVERE, "company" + company);
		if(company==null){
			company = ofy().load().type(Company.class)
					.filter("ganesh", accessUrl.trim()).first().now();
		}
		long companyIDRecieved=0;
		try{
			companyIDRecieved=Long.parseLong(req.getParameter("token").trim());
		}catch(Exception e){
			
		}
		if(company.getCompanyId()!=companyIDRecieved){
			return "Token is not valid";
		}
		List<Customer> customerList;
		if(!stateName.equalsIgnoreCase("NA")&&locationName.equalsIgnoreCase("NA")){
			customerList=ofy().load().type(Customer.class).filter("companyId",company.getCompanyId()).filter("contacts.address.state", stateName.trim()).list();
		}else if(!locationName.equalsIgnoreCase("NA")&&stateName.equalsIgnoreCase("NA")){
			customerList=ofy().load().type(Customer.class).filter("companyId",company.getCompanyId()).filter("businessProcess.branch", locationName.trim()).list();
		}else{
			customerList=ofy().load().type(Customer.class).filter("companyId",company.getCompanyId()).filter("businessProcess.branch", locationName.trim()).filter("contacts.address.state", stateName.trim()).list();
		}
		logger.log(Level.SEVERE, "customerList size" + customerList.size());
		
		Gson gson=new Gson();
		JSONArray jArray=new JSONArray();
		for (Customer customer : customerList) {
			JSONObject jsonObj=new JSONObject();
			try {
				jsonObj.put("companyName", customer.getCompanyName());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObj.put("customerName", customer.getFullname());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObj.put("address", customer.getAdress().getCompleteAddress());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				jsonObj.put("email", customer.getEmail());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JSONArray contactDetailsList=new JSONArray();
			JSONObject objNumberList=new JSONObject();
			try {
				objNumberList.put("cellNumber1",customer.getCellNumber1());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				objNumberList.put("cellNumber2", customer.getCellNumber2());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				objNumberList.put("landLine", customer.getLandline());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			contactDetailsList.put(objNumberList);
			try {
				jsonObj.put("contactDetailsList",contactDetailsList);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObj.put("pocName",customer.getFullname());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				jsonObj.put("clientCode",customer.getCount());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			jArray.put(jsonObj);
		}
		String jsonStr=jArray.toString().replace("\\\\", "");//gson.toJson(customerList).replace("\\\\", "");
		
		return jsonStr;
	}
}
