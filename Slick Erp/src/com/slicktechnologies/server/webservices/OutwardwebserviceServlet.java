package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.Contract;
import com.slicktechnologies.shared.Service;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
//
public class OutwardwebserviceServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4687334484919106216L;
	 public static final String APPID = "nbhcerp";
//		public static final String APPID = "evadev016";
		 public static final long AUTHCODE = Long.parseLong("5890065536385024");//evaerptest2
	String url;
	int contractId;
	String contractRefId;
	String wareHouse;
	String storageLocation;
	String stackNo;
	String quantity;
	String sendResponse = null;
	long companyId;
	Logger logger = Logger.getLogger("CreateServiceServlet.class");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		RegisterServiceImpl registerImpl = new RegisterServiceImpl();
		url = req.getParameter("url");
		if (req.getParameter("contractId") != null
				&& req.getParameter("contractId") != 0 + "") {
			contractId = Integer.parseInt(req.getParameter("contractId"));
		} else {
			contractId = 0;
		}
		contractRefId = req.getParameter("contractRefId").trim();
		wareHouse = req.getParameter("wareHouse").trim();
		storageLocation = req.getParameter("storageLocation").trim();
		stackNo = req.getParameter("stackNo").trim();
		quantity = req.getParameter("quantity").trim();
		String[] urlArray = url.split("\\.");
		String accessUrl = urlArray[0];
		logger.log(Level.SEVERE, "accessUrl" + accessUrl);
		String applicationId = urlArray[1];
		logger.log(Level.SEVERE, "applicationId" + applicationId);
		Company company = ofy().load().type(Company.class)
				.filter("accessUrl", accessUrl.trim()).first().now();
		String sendResponse = null;

		companyId = company.getCompanyId();

		if (companyId == AUTHCODE) {
			try {
				sendResponse = updateQuantityDetails(companyId, contractId,
						contractRefId, wareHouse, storageLocation, stackNo,
						quantity);
			} catch (Exception e) {
				e.printStackTrace();
				sendResponse = "Unknown Exception";
			}
		} else {
			sendResponse = "Authentication Failed";
		}
		resp.getWriter().print(sendResponse);
	}

	/**
	 * Description: This method will authenticate and update the contract
	 * Outward for stack No.
	 * 
	 * @param companyId2
	 * @param contractId2
	 * @param contractRefId2
	 * @param wareHouse2
	 * @param storageLocation2
	 * @param stackNo2
	 * @param quantity2
	 *            Created on :12-10-2016 Created By : Rahul Verma Created for:
	 *            NBHC
	 */
	private String updateQuantityDetails(long companyId2, int contractId2,
			String contractRefId2, String wareHouse2, String storageLocation2,
			String stackNo2, String quantity2) {
		// TODO Auto-generated method stub
		Contract contract = null;
		if (contractId2 != 0) {
			contract = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("count", contractId2).first().now();
		} else {
			contract = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("refNo", contractRefId2).first().now();
		}

		if (contract != null) {
			for (int i = 0; i < contract.getStackDetailsList().size(); i++) {
				if (contract.getStackDetailsList().get(i).getWareHouse()
						.equalsIgnoreCase(wareHouse2.trim())
						&& contract.getStackDetailsList().get(i)
								.getStorageLocation()
								.equalsIgnoreCase(storageLocation2.trim())
						&& contract.getStackDetailsList().get(i).getStackNo()
								.equalsIgnoreCase(stackNo2.trim())) {
					contract.getStackDetailsList().get(i)
							.setQauntity(Double.parseDouble(quantity2));
				}
			}
			ofy().save().entity(contract);

			List<Service> serviceList = new ArrayList<Service>();
			serviceList = ofy().load().type(Service.class)
					.filter("contractCount", contract.getCount()).list();

			for (Service service : serviceList) {
				for (int i = 0; i < service.getStackDetailsList().size(); i++) {
					if (service.getStackDetailsList().get(i).getWareHouse()
							.equalsIgnoreCase(wareHouse2.trim())
							&& service.getStackDetailsList().get(i)
									.getStorageLocation()
									.equalsIgnoreCase(storageLocation2.trim())
							&& service.getStackDetailsList().get(i)
									.getStackNo()
									.equalsIgnoreCase(stackNo2.trim())) {
						service.getStackDetailsList().get(i)
								.setQauntity(Double.parseDouble(quantity2));
					}
				}
				ofy().save().entity(service);
			}

			checkWhetherStackZero(contractId2, contractRefId2);
			return "Succesfully Updated";

		} else {
			return "Contract Does not Exist";
		}

		// return "Failed to Update";
	}

	private void checkWhetherStackZero(int contractId2, String contractRefId2) {
		// TODO Auto-generated method stub
		Contract contract;
		if (contractId2 != 0) {
			contract = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("count", contractId2).first().now();
		} else {
			contract = ofy().load().type(Contract.class)
					.filter("companyId", companyId)
					.filter("refNo", contractRefId2).first().now();
		}

		boolean checkFlag = false;
		for (int i = 0; i < contract.getStackDetailsList().size(); i++) {
			if (contract.getStackDetailsList().get(i).getQauntity() != 0) {
				checkFlag = true;
				break;
			} else {
				System.out.println("Do not create service");
			}
			if (i + 1 == contract.getStackDetailsList().size()) {
				if (checkFlag == false) {
					cancelFutureService(contract);
				}
			}
		}
	}

	private boolean cancelFutureService(Contract contract) {
		// TODO Auto-generated method stub
		try{
		ArrayList<String> statusList=new ArrayList<String>();
		statusList.add(Service.SERVICESTATUSSCHEDULE);
		statusList.add(Service.SERVICESTATUSRESCHEDULE);
		List<Service> serviceList = new ArrayList<Service>();
		serviceList = ofy().load().type(Service.class).filter("companyId", companyId)
				.filter("contractCount", contract.getCount()).filter("status IN", statusList).list();
		if(serviceList.size()>0){
		for (Service service : serviceList) {
			service.setStatus(Service.CANCELLED);
			ofy().save().entity(service);
		}
		}
		return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
}
