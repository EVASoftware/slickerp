package com.slicktechnologies.server.webservices;

import static com.googlecode.objectify.ObjectifyService.ofy;

import java.io.IOException;
//import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.slicktechnologies.server.RegisterServiceImpl;
import com.slicktechnologies.shared.GLAccount;
import com.slicktechnologies.shared.ScreenName;
import com.slicktechnologies.shared.TaxDetails;
import com.slicktechnologies.shared.common.RoleDefinition;
import com.slicktechnologies.shared.common.businessunitlayer.Company;
import com.slicktechnologies.shared.common.cronjobcongiration.CronJobConfigration;
import com.slicktechnologies.shared.common.email.EmailTemplate;
import com.slicktechnologies.shared.common.email.EmailTemplateConfiguration;
import com.slicktechnologies.shared.common.helperlayer.City;
import com.slicktechnologies.shared.common.helperlayer.Config;
import com.slicktechnologies.shared.common.helperlayer.ConfigCategory;
import com.slicktechnologies.shared.common.helperlayer.SmsTemplate;
import com.slicktechnologies.shared.common.helperlayer.State;
import com.slicktechnologies.shared.common.helperlayer.Type;
import com.slicktechnologies.shared.common.hrtaxstructure.Investment;
import com.slicktechnologies.shared.common.hrtaxstructure.TaxSlab;
import com.slicktechnologies.shared.common.humanresourcelayer.Bonus;
import com.slicktechnologies.shared.common.humanresourcelayer.Country;
import com.slicktechnologies.shared.common.humanresourcelayer.Esic;
import com.slicktechnologies.shared.common.humanresourcelayer.PaidLeave;
import com.slicktechnologies.shared.common.humanresourcelayer.ProvidentFund;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveGroup;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.LeaveType;
import com.slicktechnologies.shared.common.humanresourcelayer.leavelayer.Overtime;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.CtcComponent;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.LWF;
import com.slicktechnologies.shared.common.humanresourcelayer.payrolllayer.ProfessionalTax;
import com.slicktechnologies.shared.common.humanresourcelayer.shiftalayer.Shift;
import com.slicktechnologies.shared.common.inventory.MaterialMovementType;
import com.slicktechnologies.shared.common.inventory.StorageLocation;
import com.slicktechnologies.shared.common.inventory.Storagebin;
import com.slicktechnologies.shared.common.inventory.WareHouse;
import com.slicktechnologies.shared.common.numbergenerator.NumberGeneration;
import com.slicktechnologies.shared.common.numbergenerator.ProcessName;
import com.slicktechnologies.shared.common.processconfiguration.ProcessConfiguration;
import com.slicktechnologies.shared.common.processconfiguration.ProcessTypeDetails;
import com.slicktechnologies.shared.common.productlayer.Category;
import com.slicktechnologies.shared.common.productlayer.ItemProduct;
import com.slicktechnologies.shared.common.productlayer.ServiceProduct;
import com.slicktechnologies.shared.common.role.UserRole;
import com.slicktechnologies.shared.screenmenuconfiguration.ScreenMenuConfiguration;

public class ConfigurationCopyService extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5163683750549946650L;
	Logger logger=Logger.getLogger("ConfigurationCopyService.class");
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO GET: ");
		doPost(req, resp);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		logger.log(Level.SEVERE,"INSIDE DO POST: ");
		
		RegisterServiceImpl impl=new RegisterServiceImpl();
		impl.getClass();
		
		String data = req.getParameter("jsonString");
		logger.log(Level.SEVERE,"DATA : "+data);
		String screenData=data.trim().replace("#and", "&");
		String urlCalled = req.getRequestURL().toString().trim();
		logger.log(Level.SEVERE,"urlCalled : "+urlCalled);
		String url = urlCalled.replace("http://", "");
		url = url.replace("https://", "");
		String[] splitUrl = url.split("\\.");
		logger.log(Level.SEVERE,"SplitURL : "+splitUrl[0]);
		Company comp = ofy().load().type(Company.class).filter("accessUrl", splitUrl[0]).first().now();
		logger.log(Level.SEVERE, "Stage One"+screenData);
		long companyId=0l;
		if(comp!=null){
			companyId= comp.getCompanyId();
			Gson gson = new Gson();
			JSONObject object = null;
			try {
				object = new JSONObject(screenData.trim());
			} catch (JSONException e) {
				e.printStackTrace();
			}
			JSONArray license_array = null;
			try {
				license_array = object.getJSONArray("entity");
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
			
			java.lang.reflect.Type type = new TypeToken<ArrayList<ScreenName>>(){}.getType();
			ArrayList<ScreenName> entityList=new ArrayList<ScreenName>();
			entityList=gson.fromJson(license_array.toString(), type);
			
			logger.log(Level.SEVERE, "LIST SIZE : "+entityList.size());
			
			String reqCompIdStr=object.optString("companyId").trim();
			String fromLink=object.optString("fromLink").trim();
			String toLink=object.optString("toLink").trim();
			
			logger.log(Level.SEVERE, "Comp Id: "+reqCompIdStr+" From Link: "+fromLink+" To Link: "+toLink);
			
			org.json.simple.JSONObject jObj=new org.json.simple.JSONObject();
			jObj.put("companyId", companyId);
			jObj.put("fromLink", fromLink);
			jObj.put("toLink", toLink);
			
		
			
			
			for(ScreenName entity:entityList){

				
				if(entity.getScreenName().equals("Number Generation")){
				  try{
					List<NumberGeneration> numberGenerationList=ofy().load().type(NumberGeneration.class).filter("companyId", companyId).filter("status", true).list();
					if(numberGenerationList!=null&&numberGenerationList.size()!=0) {
						logger.log(Level.SEVERE, "Number Generation SIZE : "+numberGenerationList.size());
						String numberGen = gson.toJson(numberGenerationList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(numberGen);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("numberGeneration", jarray);
					}
				  }catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Config")){
					try{
					List<Config> configList=ofy().load().type(Config.class).filter("companyId", companyId).filter("status", true).list();
					if(configList!=null&&configList.size()!=0) {
						logger.log(Level.SEVERE, "configList SIZE : "+configList.size());
						String config = gson.toJson(configList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(config);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("config", jarray);
					}
					 }catch (Exception e) {

						}
				}
				
				if(entity.getScreenName().equals("ConfigCategory")){
					try{
					List<ConfigCategory> configCatList=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("status", true).list();
					if(configCatList!=null&&configCatList.size()!=0) {
						logger.log(Level.SEVERE, "ConfigCategory SIZE : "+configCatList.size());
						String configCat = gson.toJson(configCatList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(configCat);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("configCategory", jarray);
					}
					 }catch (Exception e) {

						}
				}
				
				if(entity.getScreenName().equals("Type")){
					try{
					List<Type> typeList=ofy().load().type(Type.class).filter("companyId", companyId).filter("status", true).list();
					if(typeList!=null&&typeList.size()!=0) {
						logger.log(Level.SEVERE, "Type SIZE : "+typeList.size());
						String type1 = gson.toJson(typeList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(type1);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("type", jarray);
					}
					  }catch (Exception e) {

						}
				}
				
				if(entity.getScreenName().equals("ServiceProduct")){
					try{
					List<ServiceProduct> serviceProdList=ofy().load().type(ServiceProduct.class).filter("companyId", companyId).filter("status", true).list();
					if(serviceProdList!=null&&serviceProdList.size()!=0) {
						logger.log(Level.SEVERE, " ServiceProduct : "+serviceProdList.size());
						String serviceProd = gson.toJson(serviceProdList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(serviceProd);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("serviceProduct", jarray);
					}
					  }catch (Exception e) {

						}
				}
				
				if(entity.getScreenName().equals("ItemProduct")){
					try{
					List<ItemProduct> itemProdList=ofy().load().type(ItemProduct.class).filter("companyId", companyId).filter("status", true).list();
					if(itemProdList!=null&&itemProdList.size()!=0) {
						logger.log(Level.SEVERE, "ItemProduct SIZE : "+itemProdList.size());
						String itemProd = gson.toJson(itemProdList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(itemProd);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("itemProduct", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Screen Menu Configuration")){
					try{
					List<ScreenMenuConfiguration> screenmenuConfigList=ofy().load().type(ScreenMenuConfiguration.class).filter("companyId", companyId).filter("status", true).list();
					if(screenmenuConfigList!=null&&screenmenuConfigList.size()!=0) {
						logger.log(Level.SEVERE, "Screen Menu Configuration SIZE : "+screenmenuConfigList.size());
						String screenmenuCon = gson.toJson(screenmenuConfigList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(screenmenuCon);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("screenmenuConfig", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("SMS Template")){
					try{
					List<SmsTemplate> smsTemplateList=ofy().load().type(SmsTemplate.class).filter("companyId", companyId).filter("status", true).list();
					if(smsTemplateList!=null&&smsTemplateList.size()!=0) {
						logger.log(Level.SEVERE, "SMS Template SIZE : "+smsTemplateList.size());
						String smsTemp = gson.toJson(smsTemplateList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(smsTemp);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("smsTemplate", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Email template")){
					try{
					List<EmailTemplate> emailTemplateList=ofy().load().type(EmailTemplate.class).filter("companyId", companyId).filter("templateStatus", true).list();
					if(emailTemplateList!=null&&emailTemplateList.size()!=0) {
						logger.log(Level.SEVERE, "Email template SIZE : "+emailTemplateList.size());						
						
						String temp = gson.toJson(emailTemplateList);
						String emailTemp=temp.replace("\\n", "<slashn>");
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(emailTemp);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("emailTemplate", jarray);
					}
					}catch (Exception e) {
						logger.log(Level.SEVERE, "Email template Exception : ");
						e.printStackTrace();
					}
				}
				
				if(entity.getScreenName().equals("User Authorization")){
					try{
					List<UserRole> userRoleList=ofy().load().type(UserRole.class).filter("companyId", companyId).filter("roleStatus", true).list();
					if(userRoleList!=null&&userRoleList.size()!=0) {
						logger.log(Level.SEVERE, "User Authorization SIZE : "+userRoleList.size());
						String userRole = gson.toJson(userRoleList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(userRole);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("userRole", jarray);
					}
					}catch (Exception e) {
						logger.log(Level.SEVERE, "User Authorization Exception: ");
						e.printStackTrace();
					}
				}
			
				
				if(entity.getScreenName().equals("Process Name")){
					try{
					List<ProcessName> processNameList=ofy().load().type(ProcessName.class).filter("companyId", companyId).filter("status", true).list();
					if(processNameList!=null&&processNameList.size()!=0) {
						logger.log(Level.SEVERE, "Process Name SIZE : "+processNameList.size());
						String processName = gson.toJson(processNameList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(processName);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("processName", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Process Configurations")){
					try{
					List<ProcessConfiguration> processConfigList=ofy().load().type(ProcessConfiguration.class).filter("companyId", companyId).filter("configStatus", true).list();
					if(processConfigList!=null&&processConfigList.size()!=0) {
						logger.log(Level.SEVERE, "Process Configurations SIZE : "+processConfigList.size());
						String processConfiguration = gson.toJson(processConfigList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(processConfiguration);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("processConfig", jarray);
					}
					}catch (Exception e) {
						logger.log(Level.SEVERE, "Process Configurations Exception : ");
						e.printStackTrace();
					}
				}
				
				
				if(entity.getScreenName().equals("Country")){
					try{
					List<Country> countryList=ofy().load().type(Country.class).filter("companyId", companyId).list();
					if(countryList!=null&&countryList.size()!=0) {
						logger.log(Level.SEVERE, "Country SIZE : "+countryList.size());
						String country = gson.toJson(countryList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(country);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("country", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("State")){
					try{
					List<State> stateList=ofy().load().type(State.class).filter("companyId", companyId).filter("status", true).list();
					if(stateList!=null&&stateList.size()!=0) {
						logger.log(Level.SEVERE, "State SIZE : "+stateList.size());
						String state = gson.toJson(stateList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(state);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("state", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("City")){
					try{
					List<City> cityList=ofy().load().type(City.class).filter("companyId", companyId).filter("status", true).list();
					if(cityList!=null&&cityList.size()!=0) {
						logger.log(Level.SEVERE, "City SIZE : "+cityList.size());
						String city = gson.toJson(cityList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(city);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("city", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Product Category")){
					try{
					List<Category> prodCategoryList=ofy().load().type(Category.class).filter("companyId", companyId).filter("status", true).list();
					if(prodCategoryList!=null&&prodCategoryList.size()!=0) {
						logger.log(Level.SEVERE, "Product Category SIZE : "+prodCategoryList.size());
						String productCategory = gson.toJson(prodCategoryList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(productCategory);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("productCategory", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Role Definition")){
					try{
					List<RoleDefinition> roleDefList=ofy().load().type(RoleDefinition.class).filter("companyId", companyId).filter("status", true).list();
					if(roleDefList!=null&&roleDefList.size()!=0) {
						logger.log(Level.SEVERE, "Role Definition SIZE : "+roleDefList.size());
						String roleDef = gson.toJson(roleDefList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(roleDef);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("roleDefinition", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Communication Configuration")){
					try{
					List<EmailTemplateConfiguration> communicationConfigList=ofy().load().type(EmailTemplateConfiguration.class).filter("companyId", companyId).filter("status", true).list();
					if(communicationConfigList!=null&&communicationConfigList.size()!=0) {
						logger.log(Level.SEVERE, "Communication Configuration SIZE : "+communicationConfigList.size());
						String communicationConfig = gson.toJson(communicationConfigList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(communicationConfig);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("communicationConfig", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				/***
				 * 
				 */
				if(entity.getScreenName().equals("Warehouse/Storage Location/Storage Bin - Central Warehouse")){
					List<WareHouse> WH=ofy().load().type(WareHouse.class).filter("companyId", companyId).filter("buisnessUnitName", "Central Warehouse").filter("status", true).list();
					if(WH!=null){
						String wh = gson.toJson(WH);
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(wh);
							jObj.put("warehouse", jarray);
						} catch(JSONException e) {
							e.printStackTrace();
						}
						List<StorageLocation> SL=ofy().load().type(StorageLocation.class).filter("companyId", companyId).filter("buisnessUnitName", "Central Warehouse").filter("warehouseName", "Central Warehouse").filter("status", true).list();
						if(SL!=null){
							String sl = gson.toJson(SL);
							JSONArray jarraySL = null;
							try {
								jarraySL = new JSONArray(sl);
								jObj.put("storageLocation", jarraySL);
							} catch (JSONException e) {
								e.printStackTrace();
							}
							
							List<Storagebin> SB=ofy().load().type(Storagebin.class).filter("companyId", companyId).filter("buisnessUnitName", "Central Warehouse").filter("warehouseName", "Central Warehouse").filter("storagelocation", "Central Warehouse").filter("binName", "Central Warehouse").filter("status", true).list();
							if(SB!=null){
								String sb = gson.toJson(SB);
								JSONArray jarraySB = null;
								try {
									jarraySB = new JSONArray(sb);
									jObj.put("storageBin", jarraySB);
								} catch(JSONException e) {
									e.printStackTrace();
								  
								}
									
							}
						}
					}
					
				} 
				
				if(entity.getScreenName().equals("Tax Details")){
					try{
					List<TaxDetails> taxDetailsList=ofy().load().type(TaxDetails.class).filter("companyId", companyId).filter("taxChargeStatus", true).list();
					if(taxDetailsList!=null&&taxDetailsList.size()!=0) {
						logger.log(Level.SEVERE, "Tax Details SIZE : "+taxDetailsList.size());
						String taxDetails = gson.toJson(taxDetailsList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(taxDetails);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("taxDetails", jarray);
					}
					}catch (Exception e) {

					}
					
					/*
					 * Ashwini Patil 
					 * Date:8-03-2024
					 * GL Account entity was not getting copied from reference link so copying it along with TAX details
					 * 
					 */
					
					try{
						List<GLAccount> GLAccountList=ofy().load().type(GLAccount.class).filter("companyId", companyId).filter("status", "Active").list();
						if(GLAccountList!=null&&GLAccountList.size()!=0) {
							logger.log(Level.SEVERE, "GLAccountList SIZE : "+GLAccountList.size());
							String glAccount = gson.toJson(GLAccountList);
							
							JSONArray jarray = null;
							try {
								jarray = new JSONArray(glAccount);
							} catch (JSONException e) {
								e.printStackTrace();
							}
							jObj.put("GLAccount", jarray);
						}
						}catch (Exception e) {

						}
				}
				
				if(entity.getScreenName().equals("Shift")){
					
					List<Shift> shiftList=ofy().load().type(Shift.class).filter("companyId", companyId).filter("status", true).list();
					if( shiftList!=null&& shiftList.size()!=0) {
						logger.log(Level.SEVERE, "Shift SIZE : "+ shiftList.size());
						String shift = gson.toJson( shiftList);
				
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(shift);
						} catch (JSONException e) {
							e.printStackTrace();
			
						jObj.put("shift", jarray);
					}
					  

					}
				}
				
				if(entity.getScreenName().equals("Leave Type")){
					try{
					List<LeaveType> leavetypeList=ofy().load().type(LeaveType.class).filter("companyId", companyId).filter("leaveStatus", true).list();
					if( leavetypeList!=null&& leavetypeList.size()!=0) {
						logger.log(Level.SEVERE, "Leave Type SIZE : "+ leavetypeList.size());
						String leavetype = gson.toJson( leavetypeList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(leavetype);
						} catch (JSONException e) {
							e.printStackTrace();
						}
						jObj.put("leaveType", jarray);
					}
					}catch (Exception e) {
						logger.log(Level.SEVERE, "Leave Type Exception : ");
						e.printStackTrace();
					}
				}
				
				if(entity.getScreenName().equals("Leave Group")){
					try{
					List<LeaveGroup> leavegroupList=ofy().load().type(LeaveGroup.class).filter("companyId", companyId).list();
					if(leavegroupList!=null&&leavegroupList.size()!=0) {
						logger.log(Level.SEVERE, " Leave Group : "+leavegroupList.size());
						String leavegroup = gson.toJson(leavegroupList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(leavegroup);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("leaveGroup", jarray);
					}
					}catch (Exception e) {
						logger.log(Level.SEVERE, " Leave Group Exception: ");
						e.printStackTrace();
					}
				}
				
				if(entity.getScreenName().equals("Overtime")){
					try{
					List<Overtime> overtimeList=ofy().load().type(Overtime.class).filter("companyId", companyId).filter("status", true).list();
					if(overtimeList!=null&&overtimeList.size()!=0) {
						logger.log(Level.SEVERE, " Overtime : "+overtimeList.size());
						String overtime = gson.toJson(overtimeList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(overtime);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("overtime", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("CTC component")){
					try{
					List<CtcComponent> ctcComponentList=ofy().load().type(CtcComponent.class).filter("companyId", companyId).filter("status", true).filter("isDeduction", false).list();
					if(ctcComponentList!=null&&ctcComponentList.size()!=0) {
						logger.log(Level.SEVERE, " CTC component : "+ctcComponentList.size());
						String ctcComp = gson.toJson(ctcComponentList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(ctcComp);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("ctcComponent", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Deduction")){
					try{
					List<CtcComponent> deductionList=ofy().load().type(CtcComponent.class).filter("companyId", companyId).filter("status", true).filter("isDeduction", true).list();
					if(deductionList!=null&&deductionList.size()!=0) {
						logger.log(Level.SEVERE, " Deduction : "+deductionList.size());
						String deduction = gson.toJson(deductionList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(deduction);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("deduction", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Tax Slab Configuration")){
					try{
					List<TaxSlab> taxSlabList=ofy().load().type(TaxSlab.class).filter("companyId", companyId).filter("status", true).list();
					if(taxSlabList!=null&&taxSlabList.size()!=0) {
						logger.log(Level.SEVERE, "Tax Slab Configuration : "+taxSlabList.size());
						String taxSlab = gson.toJson(taxSlabList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(taxSlab);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("taxslabConfig", jarray);
					}
					}catch (Exception e) {
						logger.log(Level.SEVERE, "Tax Slab Configuration Exception : ");
						e.printStackTrace();
					}
				}
				
				if(entity.getScreenName().equals("Investment Configuration")){
					try{
					List<Investment> investmentConfigList=ofy().load().type(Investment.class).filter("companyId", companyId).filter("status", true).list();
					if(investmentConfigList!=null&&investmentConfigList.size()!=0) {
						logger.log(Level.SEVERE, "Investment Configuration : "+investmentConfigList.size());
						String investmentConfig = gson.toJson(investmentConfigList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(investmentConfig);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("investmentConfig", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Paid Leave")){
					try{
					List<PaidLeave> paidLeaveList=ofy().load().type(PaidLeave.class).filter("companyId", companyId).filter("status", true).list();
					if(paidLeaveList!=null&&paidLeaveList.size()!=0) {
						logger.log(Level.SEVERE, "Paid Leave : "+paidLeaveList.size());
						String paidLeave = gson.toJson(paidLeaveList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(paidLeave);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("paidLeave", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("Bonus")){
					try{
					List<Bonus> bonusList=ofy().load().type(Bonus.class).filter("companyId", companyId).filter("status", true).list();
					if(bonusList!=null&&bonusList.size()!=0) {
						logger.log(Level.SEVERE, "Bonus: "+bonusList.size());
						String bonus = gson.toJson(bonusList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(bonus);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("bonus", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("PT")){
					try{
					List<ProfessionalTax> PTList=ofy().load().type(ProfessionalTax.class).filter("companyId", companyId).filter("status", true).list();
					if(PTList!=null&&PTList.size()!=0) {
						logger.log(Level.SEVERE, "PT: "+PTList.size());
						String PT = gson.toJson(PTList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(PT);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("pt", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("PF")){
					try{
					List<ProvidentFund> PFList=ofy().load().type(ProvidentFund.class).filter("companyId", companyId).filter("status", true).list();
					if(PFList!=null&&PFList.size()!=0) {
						logger.log(Level.SEVERE, "PF: "+PFList.size());
						String PF = gson.toJson(PFList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(PF);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("pf", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("LWF")){
					try{
					List<LWF> LWFList=ofy().load().type(LWF.class).filter("companyId", companyId).filter("status", true).list();
					if(LWFList!=null&&LWFList.size()!=0) {
						logger.log(Level.SEVERE, "LWF: "+LWFList.size());
						String LWF = gson.toJson(LWFList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(LWF);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("lwf", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				if(entity.getScreenName().equals("ESIC")){
					try{
					List<Esic> EsicList=ofy().load().type(Esic.class).filter("companyId", companyId).filter("status", true).list();
					if(EsicList!=null&&EsicList.size()!=0) {
						logger.log(Level.SEVERE, "LWF: "+EsicList.size());
						String Esic = gson.toJson(EsicList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(Esic);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("esic", jarray);
					}
					}catch (Exception e) {

					}
				}
				
				//Ashwini Patil Date:5-08-2022
				if(entity.getScreenName().equals("Cron Job")){
					try{
					List<CronJobConfigration> cronJobConfigrationList=ofy().load().type(CronJobConfigration.class).filter("companyId", companyId).filter("configStatus", true).list();
					if(cronJobConfigrationList!=null&&cronJobConfigrationList.size()!=0) {
						logger.log(Level.SEVERE, "cronJobConfigrationList: "+cronJobConfigrationList.size());
						String cronJobConfigrations = gson.toJson(cronJobConfigrationList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(cronJobConfigrations);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("cronJobConfigrations", jarray);
					}
					}catch (Exception e) {

					}
					//Ashwini Patil Date:20-12-2022 required to send cron job names along with reminder settings
					try{
						List<ConfigCategory> cronJobNameList=ofy().load().type(ConfigCategory.class).filter("companyId", companyId).filter("status", true).filter("internalType", 29).list();
						if(cronJobNameList!=null&&cronJobNameList.size()!=0) {
							logger.log(Level.SEVERE, "cronJobNameList: "+cronJobNameList.size());
							String cronJobConfigrations = gson.toJson(cronJobNameList);
							
							JSONArray jarray = null;
							try {
								jarray = new JSONArray(cronJobConfigrations);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							jObj.put("cronJobNames", jarray);
						}
						}catch (Exception e) {

						}
					
				}
				
				//Ashwini Patil Date:13-08-2022
				if(entity.getScreenName().equals("Material Movement Type")){
					try{
					List<MaterialMovementType> materialMovementTypeList=ofy().load().type(MaterialMovementType.class).filter("companyId", companyId).filter("mmtStatus", true).list();
					if(materialMovementTypeList!=null&&materialMovementTypeList.size()!=0) {
						logger.log(Level.SEVERE, "materialMovementTypeList: "+materialMovementTypeList.size());
						String materialMovementTypeConfigrations = gson.toJson(materialMovementTypeList);
						
						JSONArray jarray = null;
						try {
							jarray = new JSONArray(materialMovementTypeConfigrations);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jObj.put("MaterialMovementTypeConfigrations", jarray);
					}
					}catch (Exception e) {

					}
					
				}
				
				
			}
		
			
			
			
			
			String jsonString=jObj.toJSONString().replaceAll("\\\\", "");
			logger.log(Level.SEVERE,"jsonString:: "+jsonString);
			
		
			resp.getWriter().print(jsonString);
			
		}else{
			resp.getWriter().print("Company not found.");
			return;
		}
	}

}
